package com.softmeasures.eventezlyadminplus.Modules;

/**
 * Created by Mai Thanh Hiep on 4/3/2016.
 */
public class Durationstep {
    public String text;
    public int value;

    public Durationstep(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
