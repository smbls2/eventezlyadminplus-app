package com.softmeasures.eventezlyadminplus.Modules;

import com.google.android.gms.maps.model.LatLng;
import com.softmeasures.eventezlyadminplus.java.item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mai Thanh Hiep on 4/3/2016.
 */
public class Route {
    public Distance distance;
    public Duration duration;
    public String endAddress;
    public String status;
    public LatLng endLocation;
    public String startAddress;
    public LatLng startLocation;
    public List<LatLng> points;
    public ArrayList<item> step;

}
