package com.softmeasures.eventezlyadminplus.Modules;

/**
 * Created by Mai Thanh Hiep on 4/3/2016.
 */
public class Distancestep {
    public String text;
    public int value;

    public Distancestep(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
