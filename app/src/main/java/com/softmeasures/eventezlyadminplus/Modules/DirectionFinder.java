package com.softmeasures.eventezlyadminplus.Modules;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.softmeasures.eventezlyadminplus.java.item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mai Thanh Hiep on 4/3/2016.
 * 1
 */
public class DirectionFinder {
    private static final String DIRECTION_URL_API = "https://maps.googleapis.com/maps/api/directions/json?";
    private static final String GOOGLE_API_KEY = "AIzaSyDMTEZbeXseNWilaRo7lVKxrI2UMrc_i3o";
    ArrayList<item> dist = new ArrayList<>();
    String s;
    private DirectionFinderListener listener;
    private String origin;
    private String destination;
    private String mode = "null";
    String waypoit = "";

    public DirectionFinder(DirectionFinderListener listener, String origin, String destination) {
        this.listener = listener;
        this.origin = origin;
        this.destination = destination;
    }

    public DirectionFinder(DirectionFinderListener listener, String origin, String destination, String mode) {
        this.listener = listener;
        this.origin = origin;
        this.destination = destination;
        this.mode = mode;
    }

    public DirectionFinder(DirectionFinderListener listener, String origin, String destination, String mode, String waypoiny) {
        this.listener = listener;
        this.origin = origin;
        this.destination = destination;
        this.mode = mode;
        this.waypoit = waypoiny;
    }

    public void execute() throws UnsupportedEncodingException {
        listener.onDirectionFinderStart();
        new DownloadRawData().execute(createUrl());
    }

    private String createUrl() throws UnsupportedEncodingException {
        String urlOrigin = URLEncoder.encode(origin, "utf-8");
        String urlDestination = URLEncoder.encode(destination, "utf-8");
        mode.toLowerCase();
        String mo = URLEncoder.encode(mode, "utf-8");
        String url = null;
        if (mode.equals("null")) {
            mode = "driving";
            url = DIRECTION_URL_API + "origin=" + urlOrigin + "&destination=" + urlDestination + "&mode=" + mo + "&sensor=false" + "&key=" + GOOGLE_API_KEY;
        } else {
            if (mode.equals("transit-bus")) {
                url = DIRECTION_URL_API + "origin=" + urlOrigin + "&destination=" + urlDestination + "&transit_mode=bus" + "&mode=transit" + "&sensor=false" + "&key=" + GOOGLE_API_KEY;
            } else if (mode.equals("transit-subway")) {
                url = DIRECTION_URL_API + "origin=" + urlOrigin + "&destination=" + urlDestination + "&transit_mode=subway" + "&mode=transit" + "&sensor=false" + "&key=" + GOOGLE_API_KEY;
            } else if (mode.equals("transit-train")) {
                url = DIRECTION_URL_API + "origin=" + urlOrigin + "&destination=" + urlDestination + "&transit_mode=train" + "&mode=transit" + "&sensor=false" + "&key=" + GOOGLE_API_KEY;
                // Log.e("directiontarin", DIRECTION_URL_API + "origin=" + urlOrigin + "&destination=" + urlDestination + "&transit_mode=train" + "&mode=transit" + "&sensor=false" + "&key=" + GOOGLE_API_KEY);
            } else if (mode.equals("transit-tram")) {
                url = DIRECTION_URL_API + "origin=" + urlOrigin + "&destination=" + urlDestination + "&transit_mode=tram" + "&mode=transit" + "&sensor=false" + "&key=" + GOOGLE_API_KEY;
            } else if (mode.equals("transit-rail")) {
                url = DIRECTION_URL_API + "origin=" + urlOrigin + "&destination=" + urlDestination + "&transit_mode=rail" + "&mode=transit" + "&sensor=false" + "&key=" + GOOGLE_API_KEY;
                // Log.e("directions", DIRECTION_URL_API + "origin=" + urlOrigin + "&destination=" + urlDestination + "&transit_mode=rail" + "&mode=transit" + "&sensor=false" + "&key=" + GOOGLE_API_KEY);
            } else {
                url = DIRECTION_URL_API + "origin=" + urlOrigin + "&destination=" + urlDestination + "&mode=" + mo + "&sensor=false" + "&key=" + GOOGLE_API_KEY;
                //Log.e("directions", DIRECTION_URL_API + "origin=" + urlOrigin + "&destination=" + urlDestination + "&mode=" + mo + "&sensor=false" + "&key=" + GOOGLE_API_KEY);
            }

            if (!waypoit.equals("")) {
                url = url + "&waypoints=" + waypoit;
            }

            Log.e("url", url);
        }
        return url;
    }

    private void parseJSon(String data) throws JSONException {
        if (data == null)
            return;

        List<Route> routes = new ArrayList<Route>();
        JSONObject jsonData = new JSONObject(data);
        String status = jsonData.getString("status");
        JSONArray jsonRoutes = jsonData.getJSONArray("routes");
        for (int i = 0; i < jsonRoutes.length(); i++) {
            JSONObject jsonRoute = jsonRoutes.getJSONObject(i);
            Route route = new Route();

            JSONObject overview_polylineJson = jsonRoute.getJSONObject("overview_polyline");
            JSONArray jsonLegs = jsonRoute.getJSONArray("legs");
            JSONObject jsonLeg = jsonLegs.getJSONObject(0);
            JSONObject jsonDistance = jsonLeg.getJSONObject("distance");
            JSONObject jsonDuration = jsonLeg.getJSONObject("duration");
            JSONObject jsonEndLocation = jsonLeg.getJSONObject("end_location");
            JSONObject jsonStartLocation = jsonLeg.getJSONObject("start_location");
            s = jsonLeg.getString("steps");
            JSONArray jsonRoutes1 = new JSONArray(s);
            for (int j = 0; j < jsonRoutes1.length(); j++) {
                JSONObject jsonRoute1 = jsonRoutes1.getJSONObject(j);
                JSONObject jsonDistance1 = jsonRoute1.getJSONObject("distance");
                JSONObject jsonDuration1 = jsonRoute1.getJSONObject("duration");
                String st = jsonRoute1.getString("html_instructions");
                route.step = dis(jsonDistance1.getString("text"), jsonDuration1.getString("text"), st);
                routes.add(route);
            }
            route.distance = new Distance(jsonDistance.getString("text"), jsonDistance.getInt("value"));
            route.duration = new Duration(jsonDuration.getString("text"), jsonDuration.getInt("value"));
            route.endAddress = jsonLeg.getString("end_address");
            route.startAddress = jsonLeg.getString("start_address");
            route.startLocation = new LatLng(jsonStartLocation.getDouble("lat"), jsonStartLocation.getDouble("lng"));
            route.endLocation = new LatLng(jsonEndLocation.getDouble("lat"), jsonEndLocation.getDouble("lng"));
            route.points = decodePoly(overview_polylineJson.getString("points"));
            route.status = status;
            routes.add(route);
        }
        if (status.equals("ZERO_RESULTS") || status.equals("INVALID_REQUEST")) {
            Route route = new Route();
            route.status = status;
            routes.add(route);
        }
        if (routes.size() > 0) {
            listener.onDirectionFinderSuccess(routes);
        } else {
            listener.onDirectionFondererror();
        }
    }

    private ArrayList<item> dis(String dis, String dur, String st) {
        item i = new item();
        i.setDistancea(dis);
        i.setDuration(dur);
        i.setStep(st);
        dist.add(i);
        return dist;
    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;
        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }

    private List<LatLng> decodePolyLine(final String poly) {
        int len = poly.length();
        int index = 0;
        List<LatLng> decoded = new ArrayList<LatLng>();
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int b;
            int shift = 0;
            int result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            decoded.add(new LatLng(
                    lat / 100000d, lng / 100000d
            ));
        }

        return decoded;
    }

    private class DownloadRawData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String link = params[0];
            try {
                URL url = new URL(link);
                URLConnection c = url.openConnection();
                c.setConnectTimeout(3000);
                c.setReadTimeout(5000);
                c.getInputStream();
                InputStream is = c.getInputStream();
                StringBuffer buffer = new StringBuffer();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                return buffer.toString();

            } catch (Exception e) {
                System.gc();
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            try {
                parseJSon(res);
            } catch (JSONException e) {
                listener.onDirectionFondererror();
                e.printStackTrace();
            }
        }
    }
}
