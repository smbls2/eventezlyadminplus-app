package com.softmeasures.eventezlyadminplus;

import android.app.Application;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.multidex.MultiDex;

import com.androidnetworking.AndroidNetworking;
import com.google.android.gms.maps.model.LatLng;
import com.softmeasures.eventezlyadminplus.frament.paidparkhere;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.models.Manager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by WS on 2/22/2017.
 */

public class MyApplication extends Application {

    private static MyApplication ourInstance;

    public static MyApplication getInstance() {
        return ourInstance;
    }

    private static String PREF_LOGIN = "login", PREF_ID = "id", PREF_USER_ROLE = "user_role",
            PREF_ROLE = "role", PREF_OAUTH = "auth", PREF_OAUTG_REFRESH_TOKEN = "refresh_token";
    public SharedPreferences prefLoginDetails;
    public ConnectionDetector cd;
    public ArrayList<String> privateRuleIds = new ArrayList<>();
    public ArrayList<String> commercialRuleIds = new ArrayList<>();
    public paidparkhere paidparkhereFragment;
    public Manager selectedManager;
    public boolean isLocationAdded = false;
    private boolean menuDefault = true;

    public Manager getSelectedManager() {
        return selectedManager;
    }

    public void setSelectedManager(Manager selectedManager) {
        this.selectedManager = selectedManager;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        ourInstance = this;
//        cd = new ConnectionDetector(getApplicationContext());
        prefLoginDetails = getSharedPreferences(PREF_LOGIN, Context.MODE_PRIVATE);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidNetworking.initialize(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());
    }

    public String getUserId() {
        return prefLoginDetails.getString(PREF_ID, "null");
    }

    public void setUserId(String userId) {
        prefLoginDetails.edit().putString(PREF_ID, userId).apply();
    }

    public String getUserRole() {
        return prefLoginDetails.getString(PREF_ROLE, "");
    }

    public String getRole() {
        return prefLoginDetails.getString(PREF_USER_ROLE, "");
    }

    public void setUserRole(String userRole) {
        prefLoginDetails.edit().putString(PREF_ROLE, userRole).apply();
    }

    public String getOAuth() {
        return prefLoginDetails.getString(PREF_OAUTH, "null");
    }

    public void setOAuth(String auth) {
        prefLoginDetails.edit().putString(PREF_OAUTH, auth).apply();
    }

    public String getOAuthRefresh() {
        return prefLoginDetails.getString(PREF_OAUTG_REFRESH_TOKEN, "null");
    }

    public void setOAuthRefreshToken(String auth) {
        prefLoginDetails.edit().putString(PREF_OAUTG_REFRESH_TOKEN, auth).apply();
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }

            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }

    public void copyText(String label, String text) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(label, text);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(ourInstance, "Text copied to clipboard!", Toast.LENGTH_SHORT).show();
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean hasCamera() {
        return (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT));
    }

    public boolean isMenuDefault() {
        return menuDefault;
    }

    public void setMenuDefault(boolean menuDefault) {
        this.menuDefault = menuDefault;
    }

}

