package com.softmeasures.eventezlyadminplus.java;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


public class ConnectionDetector {

    boolean result;
    public static Context _context;
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;

    public ConnectionDetector(Context context) {
        this._context = context;
    }

  /*  public boolean isConnectingToInternet() {

        ConnectivityManager connec =
                (ConnectivityManager) _context.getSystemService(_context.CONNECTIVITY_SERVICE);

        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED || connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING || connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING || connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {
            result = true;
        } else if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED || connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {
            result = false;
        }

        return result;
    }*/


    public static int getConnectivityStatus() {
        ConnectivityManager cm = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public boolean isConnectingToInternet() {
        int conn = ConnectionDetector.getConnectivityStatus();
        boolean status = false;
        if (conn == ConnectionDetector.TYPE_WIFI) {
            status = true;
        } else if (conn == ConnectionDetector.TYPE_MOBILE) {
            status = true;
        } else if (conn == ConnectionDetector.TYPE_NOT_CONNECTED) {
            status = false;
        }
        return status;
    }
}
