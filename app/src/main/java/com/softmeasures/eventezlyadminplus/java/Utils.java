package com.softmeasures.eventezlyadminplus.java;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;

public class Utils {

    public static NotificationManager mManager;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressWarnings("static-access")
    public static void generateNotification(Context context, String mins) {

        mManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        Intent intent1 = new Intent(context, vchome.class);
        Notification notification = new Notification(R.mipmap.icon, "This is a test message!", System.currentTimeMillis());
        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingNotificationIntent = PendingIntent.getActivity(context, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Builder builder = new Notification.Builder(context);
        String strtext = "Alert Parkezly! Parking is going to expire in " + String.valueOf(mins) + " mints.";
        builder.setAutoCancel(false);
        builder.setContentTitle("Parkezly");
        builder.setContentText(strtext);
        builder.setSmallIcon(R.mipmap.icon);
        builder.setContentIntent(pendingNotificationIntent);
        builder.setOngoing(false);
        builder.build();
        notification = builder.getNotification();
        mManager.notify(0, notification);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        //notification.setLatestEventInfo(context, "AlarmManagerDemo", "This is a test message!", pendingNotificationIntent);
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;

        mManager.notify(0, notification);
    }
}
