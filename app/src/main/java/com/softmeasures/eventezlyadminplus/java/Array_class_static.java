package com.softmeasures.eventezlyadminplus.java;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Significant Infotech on 10/2/2017.
 */

public class Array_class_static {
    public static String duration_unit[] = {"Select duration unit", "Minute", "Hour", "Day", "Week", "Month"};
    public static String pricing_duration_12[] = {"", "Select pricing duration", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
    public static String pricing_duration_24[] = {"", "Select pricing duration", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"};
    public static String start_hour_array[] = {"", "Select start hour for rate", "12 Midnight", "1 AM", "2 AM", "3 AM", "4 AM", "5 AM", "6 AM", "7 AM", "8 AM", "9 AM", "10 AM", "11 AM", "12 Noon", "1 PM", "2 PM", "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM", "11 PM"};
    public static String end_hour_array[] = {"", "Select end hour for rate", "12 Midnight", "1 AM", "2 AM", "3 AM", "4 AM", "5 AM", "6 AM", "7 AM", "8 AM", "9 AM", "10 AM", "11 AM", "12 Noon", "1 PM", "2 PM", "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM", "11 PM"};
    public static String pricing_duration_day[] = {"", "Select pricing duration", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"};
    public static String pricing_duration_month[] = {"", "Select pricing duration", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
    public static String pricing_duration_week[] = {"", "Select pricing duration", "1", "2", "3", "4"};
    public static String pricing_duration_minutes[] = {"", "Select pricing duration", "10", "20", "30", "40", "50", "60"};
    private static final String ALLOWED_CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";


    public static ArrayList<item> day_list() {

        ArrayList<item> day_list = new ArrayList<>();
        item i1 = new item();
        i1.setThis_day("EVERYDAY");
        i1.setIsslected(true);
        day_list.add(i1);

        item i2 = new item();
        i2.setThis_day("WEEKDAYS");
        i2.setIsslected(false);
        day_list.add(i2);

        item i3 = new item();
        i3.setThis_day("WEEKENDS");
        i3.setIsslected(false);
        day_list.add(i3);

        item i4 = new item();
        i4.setThis_day("MON");
        i4.setIsslected(false);
        day_list.add(i4);

        item i5 = new item();
        i5.setThis_day("TUE");
        i5.setIsslected(false);
        day_list.add(i5);


        item i6 = new item();
        i6.setThis_day("WED");
        i6.setIsslected(false);
        day_list.add(i6);

        item i7 = new item();
        i7.setThis_day("THD");
        i7.setIsslected(false);
        day_list.add(i7);

        item i8 = new item();
        i8.setThis_day("FRI");
        i8.setIsslected(false);
        day_list.add(i8);

        item i9 = new item();
        i9.setThis_day("SAT");
        i9.setIsslected(false);
        day_list.add(i9);

        return day_list;
    }

    //genrate random string from a to z , A to Z and 1 to 10..............................
    public static String getRandomString(final int sizeOfRandomString) {
        final Random random = new Random();
        final StringBuilder sb = new StringBuilder(sizeOfRandomString);
        for (int i = 0; i < sizeOfRandomString; ++i)
            sb.append(ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length())));
        return sb.toString();
    }
}
