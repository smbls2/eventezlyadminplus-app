package com.softmeasures.eventezlyadminplus.java;

import android.os.Handler;
import android.os.SystemClock;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;

import com.google.android.gms.maps.model.Marker;

/**
 * Created by WS on 1/30/2017.
 */
public class BounceAnimation implements Runnable {

    public final long mStart, mDuration;
    public final Interpolator mInterpolator;
    public final Marker mMarker;
    public final Handler mHandler;

    public BounceAnimation(long start, long duration, Marker marker, Handler handler) {
        mStart = start;
        mDuration = duration;
        mMarker = marker;
        mHandler = handler;
        mInterpolator = new BounceInterpolator();
    }

    @Override
    public void run() {
        long elapsed = SystemClock.uptimeMillis() - mStart;
        float t = Math.max(1 - mInterpolator.getInterpolation((float) elapsed / mDuration), 0f);
        mMarker.setAnchor(0.5f, 1.0f + 1.2f * t);

        if (t > 0.0) {
            // Post again 16ms later.
            mHandler.postDelayed(this, 16L);
        }
    }
}