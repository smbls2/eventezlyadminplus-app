package com.softmeasures.eventezlyadminplus.java;

/**
 * Created by Significant Infotech on 11/10/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class State_model {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("country_name")
    @Expose
    private String countryName;
    @SerializedName("country_num")
    @Expose
    private Object countryNum;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("state_num")
    @Expose
    private Object stateNum;
    @SerializedName("state_name")
    @Expose
    private String stateName;
    @SerializedName("state_3_code")
    @Expose
    private String state3Code;
    @SerializedName("state_2_code")
    @Expose
    private String state2Code;
    @SerializedName("state_2_num_code")
    @Expose
    private Integer state2NumCode;
    private final static long serialVersionUID = -9143291802290696510L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Object getCountryNum() {
        return countryNum;
    }

    public void setCountryNum(Object countryNum) {
        this.countryNum = countryNum;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Object getStateNum() {
        return stateNum;
    }

    public void setStateNum(Object stateNum) {
        this.stateNum = stateNum;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getState3Code() {
        return state3Code;
    }

    public void setState3Code(String state3Code) {
        this.state3Code = state3Code;
    }

    public String getState2Code() {
        return state2Code;
    }

    public void setState2Code(String state2Code) {
        this.state2Code = state2Code;
    }

    public Integer getState2NumCode() {
        return state2NumCode;
    }

    public void setState2NumCode(Integer state2NumCode) {
        this.state2NumCode = state2NumCode;
    }

}
