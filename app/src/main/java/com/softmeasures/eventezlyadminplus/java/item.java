package com.softmeasures.eventezlyadminplus.java;

import android.graphics.Bitmap;
import android.text.TextUtils;

import java.io.File;

/**
 * Created by WS on 5/31/2016.
 */
public class item {

    String registered_state;
    String plate, state, address, exdate, name, amount, paidamount, loationname;
    String respdate, fees, city, lat, lang, vicinity, googlelat, googlelan;
    String plate_no, violation_detail, hearing_date, hearing_location, email;
    String officer_name, officer_id, hearingtime, violation_location, hearing_address;
    String paid_date, violationdescription, id, title, url, html, category, marker, title_sate, location_code, distancea;
    String date_time, time_rule, day_rule, max_time, enforced, date_special_enforce, custom_notice, active, date, pricing, pricing_duration, zip_code, start_time, end_time, this_day, parking_times, no_parking_times, start_end_rule, this_hour, max_hours, start_hour, end_hour, area_level, area_value, marker_type, loationcode1;
    Boolean isslected = false;
    String row_no, roe_name, occupied, lots_id, guest_expi_date, townshipcode;
    String violoation_code, violation_fess, violation_desciprtion;
    String court_id, addesss1;
    String ticket_satus;
    String title_address, ticketed_status, country, exit_time;
    private long reservationEntryTime = 0, reservationExitTime = 0;
    private int event_on_location, event_only;
    private String event_dates;

    private int event_id;
    private String event_code, event_name, event_specific_date, event_multi_dates, event_dt_short_info;
    private boolean multi_day, multi_day_reserve_allowed, multi_day_parking_allowed;
    private String max_days;
    String icon_client_pass_type_link;

    private String menu_icon, menu_icon_pixel, menu_font, menu_font_size, menu_color, menu_style,
            menu_bg_color, menu_bg_img;

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public String getEvent_code() {
        return event_code;
    }

    public void setEvent_code(String event_code) {
        this.event_code = event_code;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_specific_date() {
        return event_specific_date;
    }

    public void setEvent_specific_date(String event_specific_date) {
        this.event_specific_date = event_specific_date;
    }

    public String getEvent_multi_dates() {
        return event_multi_dates;
    }

    public void setEvent_multi_dates(String event_multi_dates) {
        this.event_multi_dates = event_multi_dates;
    }

    public String getEvent_dt_short_info() {
        return event_dt_short_info;
    }

    public void setEvent_dt_short_info(String event_dt_short_info) {
        this.event_dt_short_info = event_dt_short_info;
    }

    public boolean isMulti_day() {
        return multi_day;
    }

    public void setMulti_day(boolean multi_day) {
        this.multi_day = multi_day;
    }

    public boolean isMulti_day_reserve_allowed() {
        return multi_day_reserve_allowed;
    }

    public void setMulti_day_reserve_allowed(boolean multi_day_reserve_allowed) {
        this.multi_day_reserve_allowed = multi_day_reserve_allowed;
    }

    public boolean isMulti_day_parking_allowed() {
        return multi_day_parking_allowed;
    }

    public void setMulti_day_parking_allowed(boolean multi_day_parking_allowed) {
        this.multi_day_parking_allowed = multi_day_parking_allowed;
    }

    public String getMax_days() {
        return max_days;
    }

    public void setMax_days(String max_days) {
        this.max_days = max_days;
    }

    public int getEvent_on_location() {
        return event_on_location;
    }

    public void setEvent_on_location(int event_on_location) {
        this.event_on_location = event_on_location;
    }

    public int getEvent_only() {
        return event_only;
    }

    public void setEvent_only(int event_only) {
        this.event_only = event_only;
    }

    public String getEvent_dates() {
        return event_dates;
    }

    public void setEvent_dates(String event_dates) {
        this.event_dates = event_dates;
    }

    public boolean isIsselect() {
        return isselect;
    }

    public boolean isIsclcik() {
        return isclcik;
    }

    public String getYear() {
        return year;
    }

    public String getPermit_status_image() {
        return permit_status_image;
    }

    public String getContact_title() {
        return contact_title;
    }

    public long getReservationEntryTime() {
        return reservationEntryTime;
    }

    public void setReservationEntryTime(long reservationEntryTime) {
        this.reservationEntryTime = reservationEntryTime;
    }

    public long getReservationExitTime() {
        return reservationExitTime;
    }

    public void setReservationExitTime(long reservationExitTime) {
        this.reservationExitTime = reservationExitTime;
    }

    String distances, parking_status;
    String duration, parking_type, mismatch;

    String step, icon, photo_reference, lot_no, lot_row, open_now, day_start, day_end, end_start, end_end, tr_free, tr_percentage, Plead_Not_Guilty;
    String place_id, weekday_hours, weekday_hourly_price, weekday_12hr_price, weekday_24hr_price, weekend_hours, weekend_hourly_price, weekend_12hr_price, weekend_24hr_price, off_peak_start, off_peak_end, total_lots, lots_aval, location_place_id;
    Bitmap bitmap;

    boolean isselect = false;
    boolean checkboxselected = false;
    boolean isclcik = false;

    long diffent_time;
    double distnaces_values;
    File file_body;
    String file_name, menu_title;
    int menu_img_id;
    String duration_unit, max_duration, effect, no_parking_time, parking_time, renew, week_ebd_discount, off_peak_discount;
    String photo_comments, av_comments, text_comments, manager_type, lot_manager, township_logo, official_logo, township_id, total_rows, spaces_mark_reqd, total_spaces, labeling;
    String display, isparked, second_court_date_time, third_court_date_time, fourth_court_date_time, fifth_court_date_time, sixth_court_date_time, seventh_court_date_time, eighth_court_date_time, ninth_court_date_time, tenth_court_date_time, eleventh_court_date_time, twelfth_court_date_time, payment_method;
    String v_user_id, v_platform, vehicle_country, ticket_no;
    String selected_duration;
    boolean selected_parking;
    int img_id;
    private String permit_name;
    private String permit_type;
    private String covered_locations;
    private String cost;
    private String year;
    private String location_address;
    private String scheme_type;
    private String permit_id;
    private String location_id;
    private String location_name;
    private String township_name;
    private String user_id;
    private String township_code;
    private String residency_proof;
    private String approved;
    private String status;
    private String paid;
    private String user_comments;
    private String town_comments;
    private String logo;
    private String first_contact_date;
    private String permit_status_image;
    private String rate;
    private String user_name;
    private String signature;
    private String bgColor;
    private String drv_lic_proof;
    private String permit_prefix;
    private String permit_nextnum;
    private String expires_by;
    private String renewable;
    private String permit_num;
    private String full_address;
    private String full_name;
    private String phone;
    private String requirements;
    private String appl_req_download;
    private String permit_validity;
    private String permit_expires_on;
    private String expired;
    private String doc_requirements;
    private String permit_subscription_id;
    private String profile_name;
    private String profile_type;
    private String comments;
    private String in_effect;
    private String lng;
    private String lots_avbl;
    private String lots_total;
    private String off_peak_ends;
    private String off_peak_starts;
    private String weekend_special_diff;
    private String isKiosk;
    private String twp_image1;
    private String twp_image2;
    private String relatedLocations;
    private String app_pdf_upload;
    private String twp_id;
    private String relatedPermits;
    private String relatedPermitIssued;
    private String date_action;
    private String tr_fee;
    private String service_fee;
    private String total_paid;
    private String country_code;
    private String country_name;
    private String vehicle_color;
    private String vehicle_make;
    private String vehicle_model;
    private String vehicle_year;
    private String manager_type_id;
    private String manager_id;
    private String category_id;
    private String company_type_id;
    private String company_type;
    private String company_id;
    private String industry_type_id;
    private String industry_type;
    private String company_name;
    private String company_code;
    private String zip;
    private String contact_person;
    private String contact_title;
    private String contact_phone;
    private String contact_email;
    private String company_logo;
    private String company_blob_image;
    private String division;
    private String dept;

    private boolean ReservationAllowed, PrePymntReqd_for_Reservation, CanCancel_Reservation,
            ParkNow_PostPaymentAllowed, before_reserve_verify_lot_avbl, include_reservations_for_avbl_calc,
            ParkedNow_PayLater;
    private String Cancellation_Charge, Reservation_PostPayment_term, Reservation_PostPayment_LateFee,
            ParkNow_PostPayment_term, ParkNow_PostPayment_LateFee, Reservation_PostPayment_Fee,
            ParkNow_PostPayment_Fee, number_of_reservable_spots, ParkNow_PostPayment_DueAmount, ParkNow_PostPayment_Status;
    private String reservation_date_time, reserve_entry_time, reserve_exit_date_time, reserve_payment_condition,
            reserve_payment_due_by, reserve_payment_paid, reservation_status, entry_date_time, exit_date_time,
            expiry_time, modified_time, exit_overnight, exit_forced, user_id_exit, reserve_expiry_time;

    private String twp_type, street, state_name, official_web, official_fb, official_twitter, twp_blob_image;

    private String location_lot_id;

    private String pl_state, parking_total;

    private String reservation_id;

    public String getReservation_id() {
        return reservation_id;
    }

    public void setReservation_id(String reservation_id) {
        this.reservation_id = reservation_id;
    }

    public String getParking_total() {
        return parking_total;
    }

    public void setParking_total(String parking_total) {
        this.parking_total = parking_total;
    }

    public void setPl_state(String pl_state) {
        this.pl_state = pl_state;
    }

    public String getPl_state() {
        return pl_state;
    }

    public boolean isParkedNow_PayLater() {
        return ParkedNow_PayLater;
    }

    public void setParkedNow_PayLater(boolean parkedNow_PayLater) {
        ParkedNow_PayLater = parkedNow_PayLater;
    }

    public String getParkNow_PostPayment_DueAmount() {
        return ParkNow_PostPayment_DueAmount;
    }

    public void setParkNow_PostPayment_DueAmount(String parkNow_PostPayment_DueAmount) {
        ParkNow_PostPayment_DueAmount = parkNow_PostPayment_DueAmount;
    }

    public String getParkNow_PostPayment_Status() {
        return ParkNow_PostPayment_Status;
    }

    public void setParkNow_PostPayment_Status(String parkNow_PostPayment_Status) {
        ParkNow_PostPayment_Status = parkNow_PostPayment_Status;
    }

    public String getLocation_lot_id() {
        return location_lot_id;
    }

    public void setLocation_lot_id(String location_lot_id) {
        this.location_lot_id = location_lot_id;
    }

    public String getTwp_type() {
        return twp_type;
    }

    public void setTwp_type(String twp_type) {
        this.twp_type = twp_type;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public String getOfficial_web() {
        return official_web;
    }

    public void setOfficial_web(String official_web) {
        this.official_web = official_web;
    }

    public String getOfficial_fb() {
        return official_fb;
    }

    public void setOfficial_fb(String official_fb) {
        this.official_fb = official_fb;
    }

    public String getOfficial_twitter() {
        return official_twitter;
    }

    public void setOfficial_twitter(String official_twitter) {
        this.official_twitter = official_twitter;
    }

    public String getTwp_blob_image() {
        return twp_blob_image;
    }

    public void setTwp_blob_image(String twp_blob_image) {
        this.twp_blob_image = twp_blob_image;
    }

    public String getReservation_PostPayment_Fee() {
        return Reservation_PostPayment_Fee;
    }

    public void setReservation_PostPayment_Fee(String reservation_PostPayment_Fee) {
        Reservation_PostPayment_Fee = reservation_PostPayment_Fee;
    }

    public String getParkNow_PostPayment_Fee() {
        return ParkNow_PostPayment_Fee;
    }

    public void setParkNow_PostPayment_Fee(String parkNow_PostPayment_Fee) {
        ParkNow_PostPayment_Fee = parkNow_PostPayment_Fee;
    }

    public String getNumber_of_reservable_spots() {
        return number_of_reservable_spots;
    }

    public void setNumber_of_reservable_spots(String number_of_reservable_spots) {
        this.number_of_reservable_spots = number_of_reservable_spots;
    }

    public String getReserve_expiry_time() {
        return reserve_expiry_time;
    }

    public void setReserve_expiry_time(String reserve_expiry_time) {
        this.reserve_expiry_time = reserve_expiry_time;
    }

    public String getReservation_date_time() {
        return reservation_date_time;
    }

    public void setReservation_date_time(String reservation_date_time) {
        this.reservation_date_time = reservation_date_time;
    }

    public String getReserve_entry_time() {
        return reserve_entry_time;
    }

    public void setReserve_entry_time(String reserve_entry_time) {
        this.reserve_entry_time = reserve_entry_time;
    }

    public String getReserve_exit_date_time() {
        return reserve_exit_date_time;
    }

    public void setReserve_exit_date_time(String reserve_exit_date_time) {
        this.reserve_exit_date_time = reserve_exit_date_time;
    }

    public String getReserve_payment_condition() {
        return reserve_payment_condition;
    }

    public void setReserve_payment_condition(String reserve_payment_condition) {
        this.reserve_payment_condition = reserve_payment_condition;
    }

    public String getReserve_payment_due_by() {
        return reserve_payment_due_by;
    }

    public void setReserve_payment_due_by(String reserve_payment_due_by) {
        this.reserve_payment_due_by = reserve_payment_due_by;
    }

    public String getReserve_payment_paid() {
        return reserve_payment_paid;
    }

    public void setReserve_payment_paid(String reserve_payment_paid) {
        this.reserve_payment_paid = reserve_payment_paid;
    }

    public String getReservation_status() {
        return reservation_status;
    }

    public void setReservation_status(String reservation_status) {
        this.reservation_status = reservation_status;
    }

    public String getEntry_date_time() {
        return entry_date_time;
    }

    public void setEntry_date_time(String entry_date_time) {
        this.entry_date_time = entry_date_time;
    }

    public String getExit_date_time() {
        return exit_date_time;
    }

    public void setExit_date_time(String exit_date_time) {
        this.exit_date_time = exit_date_time;
    }

    public String getExpiry_time() {
        return expiry_time;
    }

    public void setExpiry_time(String expiry_time) {
        this.expiry_time = expiry_time;
    }

    public String getModified_time() {
        return modified_time;
    }

    public void setModified_time(String modified_time) {
        this.modified_time = modified_time;
    }

    public String getExit_overnight() {
        return exit_overnight;
    }

    public void setExit_overnight(String exit_overnight) {
        this.exit_overnight = exit_overnight;
    }

    public String getExit_forced() {
        return exit_forced;
    }

    public void setExit_forced(String exit_forced) {
        this.exit_forced = exit_forced;
    }

    public String getUser_id_exit() {
        return user_id_exit;
    }

    public void setUser_id_exit(String user_id_exit) {
        this.user_id_exit = user_id_exit;
    }

    public boolean isReservationAllowed() {
        return ReservationAllowed;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public void setReservationAllowed(boolean reservationAllowed) {
        ReservationAllowed = reservationAllowed;
    }

    public boolean isPrePymntReqd_for_Reservation() {
        return PrePymntReqd_for_Reservation;
    }

    public void setPrePymntReqd_for_Reservation(boolean prePymntReqd_for_Reservation) {
        PrePymntReqd_for_Reservation = prePymntReqd_for_Reservation;
    }

    public boolean isCanCancel_Reservation() {
        return CanCancel_Reservation;
    }

    public void setCanCancel_Reservation(boolean canCancel_Reservation) {
        CanCancel_Reservation = canCancel_Reservation;
    }

    public boolean isParkNow_PostPaymentAllowed() {
        return ParkNow_PostPaymentAllowed;
    }

    public void setParkNow_PostPaymentAllowed(boolean parkNow_PostPaymentAllowed) {
        ParkNow_PostPaymentAllowed = parkNow_PostPaymentAllowed;
    }

    public boolean isBefore_reserve_verify_lot_avbl() {
        return before_reserve_verify_lot_avbl;
    }

    public void setBefore_reserve_verify_lot_avbl(boolean before_reserve_verify_lot_avbl) {
        this.before_reserve_verify_lot_avbl = before_reserve_verify_lot_avbl;
    }

    public boolean isInclude_reservations_for_avbl_calc() {
        return include_reservations_for_avbl_calc;
    }

    public void setInclude_reservations_for_avbl_calc(boolean include_reservations_for_avbl_calc) {
        this.include_reservations_for_avbl_calc = include_reservations_for_avbl_calc;
    }

    public String getCancellation_Charge() {
        return Cancellation_Charge;
    }

    public void setCancellation_Charge(String cancellation_Charge) {
        Cancellation_Charge = cancellation_Charge;
    }

    public String getReservation_PostPayment_term() {
        return Reservation_PostPayment_term;
    }

    public void setReservation_PostPayment_term(String reservation_PostPayment_term) {
        Reservation_PostPayment_term = reservation_PostPayment_term;
    }

    public String getReservation_PostPayment_LateFee() {
        return Reservation_PostPayment_LateFee;
    }

    public void setReservation_PostPayment_LateFee(String reservation_PostPayment_LateFee) {
        Reservation_PostPayment_LateFee = reservation_PostPayment_LateFee;
    }

    public String getParkNow_PostPayment_term() {
        return ParkNow_PostPayment_term;
    }

    public void setParkNow_PostPayment_term(String parkNow_PostPayment_term) {
        ParkNow_PostPayment_term = parkNow_PostPayment_term;
    }

    public String getParkNow_PostPayment_LateFee() {
        return ParkNow_PostPayment_LateFee;
    }

    public void setParkNow_PostPayment_LateFee(String parkNow_PostPayment_LateFee) {
        ParkNow_PostPayment_LateFee = parkNow_PostPayment_LateFee;
    }

    public String getVehicle_country() {
        return vehicle_country;
    }

    public void setVehicle_country(String vehicle_country) {
        this.vehicle_country = vehicle_country;
    }

    public String getTicket_no() {
        return ticket_no;
    }

    public void setTicket_no(String ticket_no) {
        this.ticket_no = ticket_no;
    }

    public int getImg_id() {
        return img_id;
    }

    public void setImg_id(int img_id) {
        this.img_id = img_id;
    }

    public String getV_platform() {
        return v_platform;
    }

    public void setV_platform(String v_platform) {
        this.v_platform = v_platform;
    }

    public String getV_user_id() {
        return v_user_id;
    }

    public void setV_user_id(String v_user_id) {
        this.v_user_id = v_user_id;
    }


    public void setPlateno(String plantno) {
        this.plate = plantno;
    }

    public String getPlantno() {
        return plate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getExdate() {
        return exdate;
    }

    public void setExdate(String exdate) {
        this.exdate = exdate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPaidamount() {
        return paidamount;
    }

    public void setPaidamount(String paidamount) {
        this.paidamount = paidamount;
    }

    public String getLoationname() {
        return loationname;
    }

    public void setLoationname(String loationname) {
        this.loationname = loationname;
    }

    public String getRespdate() {
        return respdate;
    }

    public void setRespdate(String respdate) {
        this.respdate = respdate;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public String getGooglelan() {
        return googlelan;
    }

    public void setGooglelan(String googlelan) {
        this.googlelan = googlelan;
    }

    public String getGooglelat() {
        return googlelat;
    }

    public void setGooglelat(String googlelat) {
        this.googlelat = googlelat;
    }

    public String getEmail() {
        return email;

    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHearing_address() {
        return hearing_address;
    }

    public void setHearing_address(String hearing_address) {
        this.hearing_address = hearing_address;
    }

    public String getHearing_date() {
        return hearing_date;
    }

    public void setHearing_date(String hearing_date) {
        this.hearing_date = hearing_date;
    }

    public String getHearing_location() {
        return hearing_location;
    }

    public void setHearing_location(String hearing_location) {
        this.hearing_location = hearing_location;
    }

    public String getOfficer_id() {
        return officer_id;
    }

    public void setOfficer_id(String officer_id) {
        this.officer_id = officer_id;
    }

    public String getOfficer_name() {
        return officer_name;
    }

    public void setOfficer_name(String officer_name) {
        this.officer_name = officer_name;
    }

    public String getPaid_date() {
        return paid_date;
    }

    public void setPaid_date(String paid_date) {
        this.paid_date = paid_date;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getPlate_no() {
        return plate_no;
    }

    public void setPlate_no(String plate_no) {
        this.plate_no = plate_no;
    }

    public String getHearingtime() {
        return hearingtime;
    }

    public void setHearingtime(String violation_description) {
        this.hearingtime = violation_description;
    }

    public String getViolation_detail() {
        return violation_detail;
    }

    public void setViolation_detail(String violation_detail) {
        this.violation_detail = violation_detail;
    }

    public String getViolation_location() {
        return violation_location;
    }

    public void setViolation_location(String violation_location) {
        this.violation_location = violation_location;
    }

    public String getViolationdescription() {
        return violationdescription;
    }

    public void setViolationdescription(String violationdescription) {
        this.violationdescription = violationdescription;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getLocation_code() {
        return location_code;
    }

    public void setLocation_code(String location_code) {
        this.location_code = location_code;
    }

    public String getMarker() {
        return marker;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle_sate() {
        return title_sate;
    }

    public void setTitle_sate(String title_sate) {
        this.title_sate = title_sate;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDistancea() {
        return distancea;
    }

    public void setDistancea(String distancea) {
        this.distancea = distancea;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getArea_level() {
        return area_level;
    }

    public void setArea_level(String area_level) {
        this.area_level = area_level;
    }

    public String getArea_value() {
        return area_value;
    }

    public void setArea_value(String area_value) {
        this.area_value = area_value;
    }

    public String getCustom_notice() {
        return custom_notice;
    }

    public void setCustom_notice(String custom_notice) {
        this.custom_notice = custom_notice;
    }

    public String getDate_special_enforce() {
        return date_special_enforce;
    }

    public void setDate_special_enforce(String date_special_enforce) {
        this.date_special_enforce = date_special_enforce;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getDay_rule() {
        return day_rule;
    }

    public void setDay_rule(String day_rule) {
        this.day_rule = day_rule;
    }

    public String getEnd_hour() {
        return end_hour;
    }

    public void setEnd_hour(String end_hour) {
        this.end_hour = end_hour;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getTime_rule() {
        return time_rule;
    }

    public void setTime_rule(String time_rule) {
        this.time_rule = time_rule;
    }

    public String getEnforced() {
        return enforced;
    }

    public void setEnforced(String enforced) {
        this.enforced = enforced;
    }

    public String getMax_time() {
        return max_time;
    }

    public void setMax_time(String max_time) {
        this.max_time = max_time;
    }

    public String getMarker_type() {
        return marker_type;
    }

    public void setMarker_type(String marker_type) {
        this.marker_type = marker_type;
    }

    public String getMax_hours() {
        return max_hours;
    }

    public void setMax_hours(String max_hours) {
        this.max_hours = max_hours;
    }

    public String getNo_parking_times() {
        return no_parking_times;
    }

    public void setNo_parking_times(String no_parking_times) {
        this.no_parking_times = no_parking_times;
    }

    public String getParking_times() {
        return parking_times;
    }

    public void setParking_times(String parking_times) {
        this.parking_times = parking_times;
    }

    public String getPricing() {
        return pricing;
    }

    public void setPricing(String pricing) {
        this.pricing = pricing;
    }

    public String getPricing_duration() {
        return pricing_duration;
    }

    public void setPricing_duration(String pricing_duration) {
        this.pricing_duration = pricing_duration;
    }

    public String getStart_end_rule() {
        return start_end_rule;
    }

    public void setStart_end_rule(String start_end_rule) {
        this.start_end_rule = start_end_rule;
    }

    public String getStart_hour() {
        return start_hour;
    }

    public void setStart_hour(String start_hour) {
        this.start_hour = start_hour;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getThis_day() {
        return this_day;
    }

    public void setThis_day(String this_day) {
        this.this_day = this_day;
    }

    public String getThis_hour() {
        return this_hour;
    }

    public void setThis_hour(String this_hour) {
        this.this_hour = this_hour;
    }

    public String getZip_code() {
        return zip_code;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }

    public String getLoationcode1() {
        return loationcode1;
    }

    public void setLoationcode1(String loationcode1) {
        this.loationcode1 = loationcode1;
    }

    public Boolean getIsslected() {
        return isslected;
    }

    public void setIsslected(Boolean isslected) {
        this.isslected = isslected;
    }

    public String getRow_no() {
        return row_no;
    }

    public void setRow_no(String row_no) {
        this.row_no = row_no;
    }

    public String getRoe_name() {
        return roe_name;
    }

    public void setRoe_name(String roe_name) {
        this.roe_name = roe_name;
    }

    public String getGuest_expi_date() {
        return guest_expi_date;
    }

    public void setGuest_expi_date(String guest_expi_date) {
        this.guest_expi_date = guest_expi_date;
    }

    public String getLots_id() {
        return lots_id;
    }

    public void setLots_id(String lots_id) {
        this.lots_id = lots_id;
    }

    public String getOccupied() {
        return occupied;
    }

    public void setOccupied(String occupied) {
        this.occupied = occupied;
    }

    public String getTownshipcode() {
        return townshipcode;
    }

    public void setTownshipcode(String townshipcode) {
        this.townshipcode = townshipcode;
    }

    public String getViolation_desciprtion() {
        return violation_desciprtion;
    }

    public void setViolation_desciprtion(String violation_desciprtion) {
        this.violation_desciprtion = violation_desciprtion;
    }

    public String getViolation_fess() {
        return violation_fess;
    }

    public void setViolation_fess(String violation_fess) {
        this.violation_fess = violation_fess;
    }

    public String getVioloation_code() {
        return violoation_code;
    }

    public void setVioloation_code(String violoation_code) {
        this.violoation_code = violoation_code;
    }

    public String getCourt_id() {
        return court_id;
    }

    public void setCourt_id(String court_id) {
        this.court_id = court_id;
    }

    public String getTicket_satus() {
        return ticket_satus;
    }

    public void setTicket_satus(String ticket_satus) {
        this.ticket_satus = ticket_satus;
    }


    public String getTitle_address() {
        return title_address;
    }

    public void setTitle_address(String title_address) {
        this.title_address = title_address;
    }

    public String getDistances() {
        return distances;
    }

    public void setDistances(String distances) {
        this.distances = distances;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public boolean Getisselect() {
        return isselect;
    }

    public void setIsselect(boolean isselect) {
        this.isselect = isselect;
    }

    public boolean isCheckboxselected() {
        return checkboxselected;
    }

    public void setCheckboxselected(boolean checkboxselected) {
        this.checkboxselected = checkboxselected;
    }

    public String getAddesss1() {
        return addesss1;
    }

    public void setAddesss1(String addesss1) {
        this.addesss1 = addesss1;
    }

    public String getTicketed_status() {
        return ticketed_status;
    }

    public void setTicketed_status(String ticketed_status) {
        this.ticketed_status = ticketed_status;
    }

    public boolean isclcik() {
        return isclcik;
    }

    public void setIsclcik(boolean isclcik) {
        this.isclcik = isclcik;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getExit_time() {
        return exit_time;
    }

    public void setExit_time(String exit_time) {
        this.exit_time = exit_time;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getPhoto_reference() {
        return photo_reference;
    }

    public void setPhoto_reference(String photo_reference) {
        this.photo_reference = photo_reference;
    }

    public String getParking_type() {
        return parking_type;
    }

    public void setParking_type(String parking_type) {
        this.parking_type = parking_type;
    }

    public String getLot_no() {
        return lot_no;
    }

    public void setLot_no(String lot_no) {
        this.lot_no = lot_no;
    }

    public String getLot_row() {
        return lot_row;
    }

    public void setLot_row(String lot_row) {
        this.lot_row = lot_row;
    }

    public String getOpen_now() {
        return open_now;
    }

    public void setOpen_now(String open_now) {
        this.open_now = open_now;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public String getWeekday_12hr_price() {
        return weekday_12hr_price;
    }

    public void setWeekday_12hr_price(String weekday_12hr_price) {
        this.weekday_12hr_price = weekday_12hr_price;
    }

    public String getWeekday_24hr_price() {
        return weekday_24hr_price;
    }

    public void setWeekday_24hr_price(String weekday_24hr_price) {
        this.weekday_24hr_price = weekday_24hr_price;
    }

    public String getWeekday_hourly_price() {
        return weekday_hourly_price;
    }

    public void setWeekday_hourly_price(String weekday_hourly_price) {
        this.weekday_hourly_price = weekday_hourly_price;
    }

    public String getWeekday_hours() {
        return weekday_hours;
    }

    public void setWeekday_hours(String weekday_hours) {
        this.weekday_hours = weekday_hours;
    }

    public String getWeekend_12hr_price() {
        return weekend_12hr_price;
    }

    public void setWeekend_12hr_price(String weekend_12hr_price) {
        this.weekend_12hr_price = weekend_12hr_price;
    }

    public String getWeekend_24hr_price() {
        return weekend_24hr_price;
    }

    public void setWeekend_24hr_price(String weekend_24hr_price) {
        this.weekend_24hr_price = weekend_24hr_price;
    }

    public String getWeekend_hourly_price() {
        return weekend_hourly_price;
    }

    public void setWeekend_hourly_price(String weekend_hourly_price) {
        this.weekend_hourly_price = weekend_hourly_price;
    }

    public String getWeekend_hours() {
        return weekend_hours;
    }

    public void setWeekend_hours(String weekend_hours) {
        this.weekend_hours = weekend_hours;
    }

    public String getDuration_unit() {
        return duration_unit;
    }

    public void setDuration_unit(String duration_unit) {
        this.duration_unit = duration_unit;
    }

    public String getMax_duration() {
        return max_duration;
    }

    public void setMax_duration(String max_duration) {
        this.max_duration = max_duration;
    }

    public String getParking_status() {
        return parking_status;
    }

    public void setParking_status(String parking_status) {
        this.parking_status = parking_status;
    }

    public String getDay_end() {
        return day_end;
    }

    public void setDay_end(String day_end) {
        this.day_end = day_end;
    }

    public String getDay_start() {
        return day_start;
    }

    public void setDay_start(String day_start) {
        this.day_start = day_start;
    }

    public String getEnd_end() {
        return end_end;
    }

    public void setEnd_end(String end_end) {
        this.end_end = end_end;
    }

    public String getEnd_start() {
        return end_start;
    }

    public void setEnd_start(String end_start) {
        this.end_start = end_start;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public String getNo_parking_time() {
        return no_parking_time;
    }

    public void setNo_parking_time(String no_parking_time) {
        this.no_parking_time = no_parking_time;
    }

    public String getOff_peak_discount() {
        return off_peak_discount;
    }

    public void setOff_peak_discount(String off_peak_discount) {
        this.off_peak_discount = off_peak_discount;
    }

    public String getParking_time() {
        return parking_time;
    }

    public void setParking_time(String parking_time) {
        this.parking_time = parking_time;
    }

    public String getRenew() {
        return renew;
    }

    public void setRenew(String renew) {
        this.renew = renew;
    }

    public String getWeek_ebd_discount() {
        return week_ebd_discount;
    }

    public void setWeek_ebd_discount(String week_ebd_discount) {
        this.week_ebd_discount = week_ebd_discount;
    }

    public String getMismatch() {
        return mismatch;
    }

    public void setMismatch(String mismatch) {
        this.mismatch = mismatch;
    }

    public String getTr_free() {
        return tr_free;
    }

    public void setTr_free(String tr_free) {
        this.tr_free = tr_free;
    }

    public String getTr_percentage() {
        return tr_percentage;
    }

    public void setTr_percentage(String tr_percentage) {
        this.tr_percentage = tr_percentage;
    }

    public String getPlead_Not_Guilty() {
        return Plead_Not_Guilty;
    }

    public void setPlead_Not_Guilty(String plead_Not_Guilty) {
        Plead_Not_Guilty = plead_Not_Guilty;
    }

    public String getLots_aval() {
        return lots_aval;
    }

    public void setLots_aval(String lots_aval) {
        this.lots_aval = lots_aval;
    }

    public String getOff_peak_end() {
        return off_peak_end;
    }

    public void setOff_peak_end(String off_peak_end) {
        this.off_peak_end = off_peak_end;
    }

    public String getOff_peak_start() {
        return off_peak_start;
    }

    public void setOff_peak_start(String off_peak_start) {
        this.off_peak_start = off_peak_start;
    }

    public String getTotal_lots() {
        return total_lots;
    }

    public void setTotal_lots(String total_lots) {
        this.total_lots = total_lots;
    }

    public long getDiffent_time() {
        return diffent_time;
    }

    public void setDiffent_time(long diffent_time) {
        this.diffent_time = diffent_time;
    }

    public File getFile_body() {
        return file_body;
    }

    public void setFile_body(File file_body) {
        this.file_body = file_body;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getAv_comments() {
        return av_comments;
    }

    public void setAv_comments(String av_comments) {
        this.av_comments = av_comments;
    }

    public String getPhoto_comments() {
        return photo_comments;
    }

    public void setPhoto_comments(String photo_comments) {
        this.photo_comments = photo_comments;
    }

    public String getText_comments() {
        return text_comments;
    }

    public void setText_comments(String text_comments) {
        this.text_comments = text_comments;
    }


    public String getLot_manager() {
        return lot_manager;
    }

    public String getManager_type() {
        return manager_type;
    }

    public String getOfficial_logo() {
        return official_logo;
    }

    public String getTownship_logo() {
        return township_logo;
    }

    public void setLot_manager(String lot_manager) {
        this.lot_manager = lot_manager;
    }

    public void setManager_type(String manager_type) {
        this.manager_type = manager_type;
    }

    public void setOfficial_logo(String official_logo) {
        this.official_logo = official_logo;
    }

    public void setTownship_logo(String township_logo) {
        this.township_logo = township_logo;
    }

    public String getTownship_id() {
        return township_id;
    }

    public void setTownship_id(String township_id) {
        this.township_id = township_id;
    }

    public int getMenu_img_id() {
        return menu_img_id;
    }


    public void setMenu_img_id(int menu_img_id) {
        this.menu_img_id = menu_img_id;
    }
    public String getMenu_title() {
        return menu_title;
    }

    public void setMenu_title(String menu_title) {
        this.menu_title = menu_title;
    }

    public String getMenu_icon() {
        return menu_icon;
    }

    public void setMenu_icon(String menu_icon) {
        this.menu_icon = menu_icon;
    }

    public String getMenu_icon_pixel() {
        return menu_icon_pixel;
    }

    public void setMenu_icon_pixel(String menu_icon_pixel) {
        this.menu_icon_pixel = menu_icon_pixel;
    }

    public String getMenu_font() {
        return menu_font;
    }

    public void setMenu_font(String menu_font) {
        this.menu_font = menu_font;
    }

    public String getMenu_font_size() {
        return menu_font_size;
    }

    public void setMenu_font_size(String menu_font_size) {
        this.menu_font_size = menu_font_size;
    }

    public String getMenu_color() {
        return menu_color;
    }

    public void setMenu_color(String menu_color) {
        this.menu_color = menu_color;
    }

    public String getMenu_style() {
        return menu_style;
    }

    public void setMenu_style(String menu_style) {
        this.menu_style = menu_style;
    }

    public String getMenu_bg_color() {
        return menu_bg_color;
    }

    public void setMenu_bg_color(String menu_bg_color) {
        this.menu_bg_color = menu_bg_color;
    }

    public String getMenu_bg_img() {
        return menu_bg_img;
    }

    public void setMenu_bg_img(String menu_bg_img) {
        this.menu_bg_img = menu_bg_img;
    }

    public String getLabeling() {
        return labeling;
    }

    public String getSpaces_mark_reqd() {
        return spaces_mark_reqd;
    }

    public String getTotal_rows() {
        return total_rows;
    }

    public String getTotal_spaces() {
        return total_spaces;
    }

    public void setLabeling(String labeling) {
        this.labeling = labeling;
    }

    public void setSpaces_mark_reqd(String spaces_mark_reqd) {
        this.spaces_mark_reqd = spaces_mark_reqd;
    }

    public void setTotal_rows(String total_rows) {
        this.total_rows = total_rows;
    }

    public void setTotal_spaces(String total_spaces) {
        this.total_spaces = total_spaces;
    }

    public boolean isselect() {
        return isselect;
    }

    public String getDisplay() {
        return display;
    }

    public String getIsparked() {
        return isparked;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public void setIsparked(String isparked) {
        this.isparked = isparked;
    }

    public String getLocation_place_id() {
        return location_place_id;
    }

    public void setLocation_place_id(String location_place_id) {
        this.location_place_id = location_place_id;
    }

    public double getDistnaces_values() {
        return distnaces_values;
    }

    public void setDistnaces_values(double distnaces_values) {
        this.distnaces_values = distnaces_values;
    }

    public String getEighth_court_date_time() {
        return eighth_court_date_time;
    }

    public String getEleventh_court_date_time() {
        return eleventh_court_date_time;
    }

    public String getFifth_court_date_time() {
        return fifth_court_date_time;
    }

    public String getFourth_court_date_time() {
        return fourth_court_date_time;
    }

    public String getNinth_court_date_time() {
        return ninth_court_date_time;
    }

    public String getSecond_court_date_time() {
        return second_court_date_time;
    }

    public String getSeventh_court_date_time() {
        return seventh_court_date_time;
    }

    public String getSixth_court_date_time() {
        return sixth_court_date_time;
    }

    public String getTenth_court_date_time() {
        return tenth_court_date_time;
    }

    public String getThird_court_date_time() {
        return third_court_date_time;
    }

    public String getTwelfth_court_date_time() {
        return twelfth_court_date_time;
    }

    public void setEighth_court_date_time(String eighth_court_date_time) {
        this.eighth_court_date_time = eighth_court_date_time;
    }

    public void setEleventh_court_date_time(String eleventh_court_date_time) {
        this.eleventh_court_date_time = eleventh_court_date_time;
    }

    public void setFifth_court_date_time(String fifth_court_date_time) {
        this.fifth_court_date_time = fifth_court_date_time;
    }

    public void setFourth_court_date_time(String fourth_court_date_time) {
        this.fourth_court_date_time = fourth_court_date_time;
    }

    public void setNinth_court_date_time(String ninth_court_date_time) {
        this.ninth_court_date_time = ninth_court_date_time;
    }

    public void setSecond_court_date_time(String second_court_date_time) {
        this.second_court_date_time = second_court_date_time;
    }

    public void setSeventh_court_date_time(String seventh_court_date_time) {
        this.seventh_court_date_time = seventh_court_date_time;
    }

    public void setSixth_court_date_time(String sixth_court_date_time) {
        this.sixth_court_date_time = sixth_court_date_time;
    }

    public void setTenth_court_date_time(String tenth_court_date_time) {
        this.tenth_court_date_time = tenth_court_date_time;
    }

    public void setThird_court_date_time(String third_court_date_time) {
        this.third_court_date_time = third_court_date_time;
    }

    public void setTwelfth_court_date_time(String twelfth_court_date_time) {
        this.twelfth_court_date_time = twelfth_court_date_time;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public boolean isSelected_parking() {
        return selected_parking;
    }

    public void setSelected_parking(boolean selected_parking) {
        this.selected_parking = selected_parking;
    }

    public String getSelected_duration() {
        return selected_duration;
    }

    public void setSelected_duration(String selected_duration) {
        this.selected_duration = selected_duration;
    }

    public void setPermit_name(String permit_name) {
        this.permit_name = permit_name;
    }

    public void setPermit_type(String permit_type) {
        this.permit_type = permit_type;
    }

    public void setCovered_locations(String covered_locations) {
        this.covered_locations = covered_locations;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setLocation_address(String location_address) {
        this.location_address = location_address;
    }

    public void setScheme_type(String scheme_type) {
        this.scheme_type = scheme_type;
    }

    public String getCovered_locations() {
        return covered_locations;
    }

    public String getCost() {
        return cost;
    }

    public void setPermit_id(String permit_id) {
        this.permit_id = permit_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public void setTownship_name(String township_name) {
        this.township_name = township_name;
    }

    public String getLocation_name() {
        return location_name;
    }

    public String getTownship_name() {
        return township_name;
    }

    public String getLocation_id() {
        return location_id;
    }

    public String getPermit_id() {
        return permit_id;
    }

    public String getPermit_type() {
        return permit_type;
    }

    public String getPermit_name() {
        return permit_name;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setTownship_code(String township_code) {
        this.township_code = township_code;
    }

    public void setResidency_proof(String residency_proof) {
        this.residency_proof = residency_proof;
    }

    public void setApproved(String approved) {
        this.approved = approved;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public void setUser_comments(String user_comments) {
        this.user_comments = user_comments;
    }

    public void setTown_comments(String town_comments) {
        this.town_comments = town_comments;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setFirst_contact_date(String first_contact_date) {
        this.first_contact_date = first_contact_date;
    }

    public void setPermit_status_image(String permit_status_image) {
        this.permit_status_image = permit_status_image;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getApproved() {
        return approved;
    }

    public String getTownship_code() {
        return township_code;
    }

    public String getScheme_type() {
        return scheme_type;
    }

    public String getPaid() {
        return paid;
    }

    public String getRate() {
        return rate;
    }

    public String getSignature() {
        return signature;
    }

    public String getStatus() {
        return status;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getResidency_proof() {
        return residency_proof;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getLogo() {
        return logo;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setDrv_lic_proof(String drv_lic_proof) {
        this.drv_lic_proof = drv_lic_proof;
    }

    public String getDrv_lic_proof() {
        return drv_lic_proof;
    }

    public void setPermit_prefix(String permit_prefix) {
        this.permit_prefix = permit_prefix;
    }

    public String getPermit_prefix() {
        return permit_prefix;
    }

    public void setPermit_nextnum(String permit_nextnum) {
        this.permit_nextnum = permit_nextnum;
    }

    public String getPermit_nextnum() {
        return permit_nextnum;
    }


    public void setExpires_by(String expires_by) {
        this.expires_by = expires_by;
    }

    public String getExpires_by() {
        return expires_by;
    }

    public void setRenewable(String renewable) {
        this.renewable = renewable;
    }

    public String getRenewable() {
        return renewable;
    }

    public void setPermit_num(String permit_num) {
        this.permit_num = permit_num;
    }

    public String getPermit_num() {
        return permit_num;
    }

    public void setFull_address(String full_address) {
        this.full_address = full_address;
    }

    public String getFull_address() {
        return full_address;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public String getFirst_contact_date() {
        return first_contact_date;
    }

    public String getUser_comments() {
        return user_comments;
    }

    public String getTown_comments() {
        return town_comments;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setAppl_req_download(String appl_req_download) {
        this.appl_req_download = appl_req_download;
    }

    public String getAppl_req_download() {
        return appl_req_download;
    }

    public void setPermit_validity(String permit_validity) {
        this.permit_validity = permit_validity;
    }

    public String getPermit_validity() {
        return permit_validity;
    }

    public void setPermit_expires_on(String permit_expires_on) {
        this.permit_expires_on = permit_expires_on;
    }

    public String getPermit_expires_on() {
        return permit_expires_on;
    }

    public void setExpired(String expired) {
        this.expired = expired;
    }

    public String getExpired() {
        return expired;
    }

    public void setDoc_requirements(String doc_requirements) {
        this.doc_requirements = doc_requirements;
    }

    public String getDoc_requirements() {
        return doc_requirements;
    }

    public void setPermit_subscription_id(String permit_subscription_id) {
        this.permit_subscription_id = permit_subscription_id;
    }

    public String getPermit_subscription_id() {
        return permit_subscription_id;
    }

    public void setProfile_name(String profile_name) {
        this.profile_name = profile_name;
    }

    public String getProfile_name() {
        return profile_name;
    }

    public void setProfile_type(String profile_type) {
        this.profile_type = profile_type;
    }

    public String getProfile_type() {
        return profile_type;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getComments() {
        return comments;
    }

    public void setIn_effect(String in_effect) {
        this.in_effect = in_effect;
    }

    public String getIn_effect() {
        return in_effect;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLng() {
        return lng;
    }

    public void setLots_avbl(String lots_avbl) {
        this.lots_avbl = lots_avbl;
    }

    public String getLots_avbl() {
        return lots_avbl;
    }

    public void setLots_total(String lots_total) {
        this.lots_total = lots_total;
    }

    public String getLots_total() {
        return lots_total;
    }

    public void setOff_peak_ends(String off_peak_ends) {
        this.off_peak_ends = off_peak_ends;
    }

    public String getOff_peak_ends() {
        return off_peak_ends;
    }

    public void setOff_peak_starts(String off_peak_starts) {
        this.off_peak_starts = off_peak_starts;
    }

    public String getOff_peak_starts() {
        return off_peak_starts;
    }

    public void setWeekend_special_diff(String weekend_special_diff) {
        this.weekend_special_diff = weekend_special_diff;
    }

    public String getWeekend_special_diff() {
        return weekend_special_diff;
    }

    public void setIsKiosk(String isKiosk) {
        this.isKiosk = isKiosk;
    }

    public String getIsKiosk() {
        return isKiosk;
    }

    public void setTwp_image1(String twp_image1) {
        this.twp_image1 = twp_image1;
    }

    public String getTwp_image1() {
        return twp_image1;
    }

    public void setTwp_image2(String twp_image2) {
        this.twp_image2 = twp_image2;
    }

    public String getTwp_image2() {
        return twp_image2;
    }

    public void setRelatedLocations(String relatedLocations) {
        this.relatedLocations = relatedLocations;
    }

    public String getRelatedLocations() {
        return relatedLocations;
    }

    public void setApp_pdf_upload(String app_pdf_upload) {
        this.app_pdf_upload = app_pdf_upload;
    }

    public String getApp_pdf_upload() {
        return app_pdf_upload;
    }

    public void setTwp_id(String twp_id) {
        this.twp_id = twp_id;
    }

    public String getTwp_id() {
        return twp_id;
    }

    public void setRelatedPermits(String relatedPermits) {
        this.relatedPermits = relatedPermits;
    }

    public String getRelatedPermits() {
        return relatedPermits;
    }

    public void setRelatedPermitIssued(String relatedPermitIssued) {
        this.relatedPermitIssued = relatedPermitIssued;
    }

    public String getRelatedPermitIssued() {
        return relatedPermitIssued;
    }

    public void setDate_action(String date_action) {
        this.date_action = date_action;
    }

    public String getDate_action() {
        return date_action;
    }

    public void setTr_fee(String tr_fee) {
        this.tr_fee = tr_fee;
    }

    public String getTr_fee() {
        return tr_fee;
    }

    public void setService_fee(String service_fee) {
        this.service_fee = service_fee;
    }

    public String getService_fee() {
        return service_fee;
    }

    public void setTotal_paid(String total_paid) {
        this.total_paid = total_paid;
    }

    public String getTotal_paid() {
        return total_paid;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setVehicle_color(String vehicle_color) {
        this.vehicle_color = vehicle_color;
    }

    public String getVehicle_color() {
        return vehicle_color;
    }

    public void setVehicle_make(String vehicle_make) {
        this.vehicle_make = vehicle_make;
    }

    public String getVehicle_make() {
        return vehicle_make;
    }

    public void setVehicle_model(String vehicle_model) {
        this.vehicle_model = vehicle_model;
    }

    public String getVehicle_model() {
        return vehicle_model;
    }

    public void setVehicle_year(String vehicle_year) {
        this.vehicle_year = vehicle_year;
    }

    public String getVehicle_year() {
        return vehicle_year;
    }

    public void setManager_type_id(String manager_type_id) {
        this.manager_type_id = manager_type_id;
    }

    public String getManager_type_id() {
        return manager_type_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_id() {
        return manager_id;
    }

    public String getLocation_address() {
        return location_address;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCompany_type_id(String company_type_id) {
        this.company_type_id = company_type_id;
    }

    public String getCompany_type_id() {
        return company_type_id;
    }

    public void setCompany_type(String company_type) {
        this.company_type = company_type;
    }

    public String getCompany_type() {
        return company_type;
    }

    public void setIndustry_type_id(String industry_type_id) {
        this.industry_type_id = industry_type_id;
    }

    public String getIndustry_type_id() {
        return industry_type_id;
    }

    public void setIndustry_type(String industry_type) {
        this.industry_type = industry_type;
    }

    public String getIndustry_type() {
        return industry_type;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getZip() {
        return zip;
    }

    public void setContact_person(String contact_person) {
        this.contact_person = contact_person;
    }

    public String getContact_person() {
        return contact_person;
    }

    public void setContact_title(String contact_title) {
        this.contact_title = contact_title;
    }

    public void setContact_phone(String contact_phone) {
        this.contact_phone = contact_phone;
    }

    public String getContact_phone() {
        return contact_phone;
    }

    public void setContact_email(String contact_email) {
        this.contact_email = contact_email;
    }

    public String getContact_email() {
        return contact_email;
    }

    public void setCompany_logo(String company_logo) {
        this.company_logo = company_logo;
    }

    public String getCompany_logo() {
        return company_logo;
    }

    public void setCompany_blob_image(String company_blob_image) {
        this.company_blob_image = company_blob_image;
    }

    public String getCompany_blob_image() {
        return company_blob_image;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getDivision() {
        return division;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getDept() {
        return dept;
    }

    public String getRegistered_state() {
        return registered_state;
    }

    public void setRegistered_state(String registered_state) {
        this.registered_state = registered_state;
    }

    private String lot_numbering_type, lot_numbering_description, division_id, dept_id,
            day_type, premium_pricing, currency, currency_symbol;
    private boolean reservable;

    public String getCurrency_symbol() {
        return currency_symbol;
    }

    public void setCurrency_symbol(String currency_symbol) {
        this.currency_symbol = currency_symbol;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPremium_pricing() {
        return premium_pricing;
    }

    public void setPremium_pricing(String premium_pricing) {
        this.premium_pricing = premium_pricing;
    }

    public String getDay_type() {
        return day_type;
    }

    public void setDay_type(String day_type) {
        this.day_type = day_type;
    }

    public String getDept_id() {
        return dept_id;
    }

    public void setDept_id(String dept_id) {
        this.dept_id = dept_id;
    }

    public String getLot_numbering_description() {
        return lot_numbering_description;
    }

    public void setLot_numbering_description(String lot_numbering_description) {
        this.lot_numbering_description = lot_numbering_description;
    }

    public String getLot_numbering_type() {
        return lot_numbering_type;
    }

    public String getDivision_id() {
        return division_id;
    }

    public void setDivision_id(String division_id) {
        this.division_id = division_id;
    }

    public void setLot_numbering_type(String lot_numbering_type) {
        this.lot_numbering_type = lot_numbering_type;
    }

    public boolean isReservable() {
        return reservable;
    }

    public void setReservable(boolean reservable) {
        this.reservable = reservable;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof item) {
            if (!TextUtils.isEmpty(((item) obj).getLat()) &&
                    !TextUtils.isEmpty(((item) obj).getLng())
                    && this.lat != null
                    && !TextUtils.isEmpty(this.lat)) {
                return ((item) obj).getLat().equals(this.lat)
                        && ((item) obj).getLng().equals(this.lng);
            } else if (!TextUtils.isEmpty(((item) obj).getLocation_code())) {
                return ((item) obj).getLocation_code().equalsIgnoreCase(this.location_code);
            } else {
                return super.equals(obj);
            }
        } else {
            return super.equals(obj);
        }
    }

    private String marker_address1;

    public String getMarker_address1() {
        return marker_address1;
    }

    public void setMarker_address1(String marker_address1) {
        this.marker_address1 = marker_address1;
    }


    public String getIcon_client_pass_type_link() {
        return icon_client_pass_type_link;
    }

    public void setIcon_client_pass_type_link(String icon_client_pass_type_link) {
        this.icon_client_pass_type_link = icon_client_pass_type_link;
    }

}


