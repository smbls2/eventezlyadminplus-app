package com.softmeasures.eventezlyadminplus.common;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.MyApplication;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.interfaces.FetchEventListener;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.users.TownshipUser;
import com.softmeasures.eventezlyadminplus.models.users.UserProfile;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public abstract class BaseFragment extends Fragment {
    protected MyApplication myApp = MyApplication.getInstance();

    protected abstract void updateViews();

    protected abstract void setListeners();

    protected abstract void initViews(View v);

    public void replaceFragment(Fragment fragment, String tag) {
        ((vchome) getActivity()).show_back_button();
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.add(R.id.My_Container_1_ID, fragment, tag);
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(fragment);
        ft.commitAllowingStateLoss();
    }

    public class fetchEventsCall extends AsyncTask<String, String, ArrayList<EventDefinition>> {
        JSONObject json;
        JSONArray json1;
        String eventDefUrl = "";
        ArrayList<EventDefinition> eventDefinitions;
        FetchEventListener listener;

        public fetchEventsCall(String eventDefUrl, FetchEventListener listener) {
            this.eventDefUrl = eventDefUrl;
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            eventDefinitions = new ArrayList<>();
        }

        @Override
        protected ArrayList<EventDefinition> doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + eventDefUrl;
            Log.e("#DEBUG", "      fetchEvents:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");

                    for (int j = 0; j < jsonArray.length(); j++) {
                        EventDefinition eventDefinition = new EventDefinition();
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            eventDefinition.setId(object.getInt("id"));
                            eventDefinition.setEvent_id(object.getInt("id"));
                        }
                        if (object.has("date_time")
                                && !TextUtils.isEmpty(object.getString("date_time"))
                                && !object.getString("date_time").equals("null")) {
                            eventDefinition.setDate_time(object.getString("date_time"));
                        }
                        if (object.has("manager_type")
                                && !TextUtils.isEmpty(object.getString("manager_type"))
                                && !object.getString("manager_type").equals("null")) {
                            eventDefinition.setManager_type(object.getString("manager_type"));
                        }
                        if (object.has("manager_type_id")
                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
                                && !object.getString("manager_type_id").equals("null")) {
                            eventDefinition.setManager_type_id(object.getInt("manager_type_id"));
                        }
                        if (object.has("twp_id")
                                && !TextUtils.isEmpty(object.getString("twp_id"))
                                && !object.getString("twp_id").equals("null")) {
                            eventDefinition.setTwp_id(object.getInt("twp_id"));
                        }
                        if (object.has("township_code")
                                && !TextUtils.isEmpty(object.getString("township_code"))
                                && !object.getString("township_code").equals("null")) {
                            eventDefinition.setTownship_code(object.getString("township_code"));
                        }
                        if (object.has("township_name")
                                && !TextUtils.isEmpty(object.getString("township_name"))
                                && !object.getString("township_name").equals("null")) {
                            eventDefinition.setTownship_name(object.getString("township_name"));
                        }
                        if (object.has("company_id")
                                && !TextUtils.isEmpty(object.getString("company_id"))
                                && !object.getString("company_id").equals("null")) {
                            eventDefinition.setCompany_id(object.getInt("company_id"));
                        }
                        if (object.has("company_code")
                                && !TextUtils.isEmpty(object.getString("company_code"))
                                && !object.getString("company_code").equals("null")) {
                            eventDefinition.setCompany_code(object.getString("company_code"));
                        }
                        if (object.has("company_name")
                                && !TextUtils.isEmpty(object.getString("company_name"))
                                && !object.getString("company_name").equals("null")) {
                            eventDefinition.setCompany_name(object.getString("company_name"));
                        }
                        if (object.has("event_type")
                                && !TextUtils.isEmpty(object.getString("event_type"))
                                && !object.getString("event_type").equals("null")) {
                            eventDefinition.setEvent_type(object.getString("event_type"));
                        }
                        if (object.has("event_name")
                                && !TextUtils.isEmpty(object.getString("event_name"))
                                && !object.getString("event_name").equals("null")) {
                            eventDefinition.setEvent_name(object.getString("event_name"));
                        }

                        if (object.has("event_category_type")
                                && !TextUtils.isEmpty(object.getString("event_category_type"))
                                && !object.getString("event_category_type").equals("null")) {
                            eventDefinition.setEvent_category_type(object.getString("event_category_type"));
                        }
                        if (object.has("event_short_description")
                                && !TextUtils.isEmpty(object.getString("event_short_description"))
                                && !object.getString("event_short_description").equals("null")) {
                            eventDefinition.setEvent_short_description(object.getString("event_short_description"));
                        }
                        if (object.has("event_long_description")
                                && !TextUtils.isEmpty(object.getString("event_long_description"))
                                && !object.getString("event_long_description").equals("null")) {
                            eventDefinition.setEvent_long_description(object.getString("event_long_description"));
                        }
                        if (object.has("event_link_on_web")
                                && !TextUtils.isEmpty(object.getString("event_link_on_web"))
                                && !object.getString("event_link_on_web").equals("null")) {
                            eventDefinition.setEvent_link_on_web(object.getString("event_link_on_web"));
                        }
                        if (object.has("event_link_twitter")
                                && !TextUtils.isEmpty(object.getString("event_link_twitter"))
                                && !object.getString("event_link_twitter").equals("null")) {
                            eventDefinition.setEvent_link_twitter(object.getString("event_link_twitter"));
                        }
                        if (object.has("event_link_whatsapp")
                                && !TextUtils.isEmpty(object.getString("event_link_whatsapp"))
                                && !object.getString("event_link_whatsapp").equals("null")) {
                            eventDefinition.setEvent_link_whatsapp(object.getString("event_link_whatsapp"));
                        }
                        if (object.has("event_link_other_media")
                                && !TextUtils.isEmpty(object.getString("event_link_other_media"))
                                && !object.getString("event_link_other_media").equals("null")) {
                            eventDefinition.setEvent_link_other_media(object.getString("event_link_other_media"));
                        }
                        if (object.has("event_link_ytube")
                                && !TextUtils.isEmpty(object.getString("event_link_ytube"))
                                && !object.getString("event_link_ytube").equals("null")) {
                            eventDefinition.setEvent_link_ytube(object.getString("event_link_ytube"));
                        }
                        if (object.has("event_link_zoom")
                                && !TextUtils.isEmpty(object.getString("event_link_zoom"))
                                && !object.getString("event_link_zoom").equals("null")) {
                            eventDefinition.setEvent_link_zoom(object.getString("event_link_zoom"));
                        }
                        if (object.has("event_link_googlemeet")
                                && !TextUtils.isEmpty(object.getString("event_link_googlemeet"))
                                && !object.getString("event_link_googlemeet").equals("null")) {
                            eventDefinition.setEvent_link_googlemeet(object.getString("event_link_googlemeet"));
                        }
                        if (object.has("event_link_googleclassroom")
                                && !TextUtils.isEmpty(object.getString("event_link_googleclassroom"))
                                && !object.getString("event_link_googleclassroom").equals("null")) {
                            eventDefinition.setEvent_link_googleclassroom(object.getString("event_link_googleclassroom"));
                        }
                        if (object.has("event_logi_type")
                                && !TextUtils.isEmpty(object.getString("event_logi_type"))
                                && !object.getString("event_logi_type").equals("null")) {
                            eventDefinition.setEvent_logi_type(object.getInt("event_logi_type"));
                        } else {
                            eventDefinition.setEvent_logi_type(1);
                        }
                        if (object.has("event_regn_limit")
                                && !TextUtils.isEmpty(object.getString("event_regn_limit"))
                                && !object.getString("event_regn_limit").equals("null")) {
                            eventDefinition.setEvent_regn_limit(object.getString("event_regn_limit"));
                        }
                        if (object.has("web_event_full_regn_fee")
                                && !TextUtils.isEmpty(object.getString("web_event_full_regn_fee"))
                                && !object.getString("web_event_full_regn_fee").equals("null")) {
                            eventDefinition.setWeb_event_full_regn_fee(object.getString("web_event_full_regn_fee"));
                        } else {
                            eventDefinition.setWeb_event_full_regn_fee("0");
                        }
                        if (object.has("web_event_regn_limit")
                                && !TextUtils.isEmpty(object.getString("web_event_regn_limit"))
                                && !object.getString("web_event_regn_limit").equals("null")) {
                            eventDefinition.setWeb_event_regn_limit(object.getString("web_event_regn_limit"));
                        }
                        if (object.has("web_event_location_to_show")
                                && !TextUtils.isEmpty(object.getString("web_event_location_to_show"))
                                && !object.getString("web_event_location_to_show").equals("null")) {
                            eventDefinition.setWeb_event_location_to_show(object.getInt("web_event_location_to_show") == 1);
                        }
                        if (object.has("free_web_event")
                                && !TextUtils.isEmpty(object.getString("free_web_event"))
                                && !object.getString("free_web_event").equals("null")) {
                            eventDefinition.setFree_web_event(object.getBoolean("free_web_event"));
                        }
                        if (object.has("company_logo")
                                && !TextUtils.isEmpty(object.getString("company_logo"))
                                && !object.getString("company_logo").equals("null")) {
                            eventDefinition.setCompany_logo(object.getString("company_logo"));
                        }
                        if (object.has("event_logo")
                                && !TextUtils.isEmpty(object.getString("event_logo"))
                                && !object.getString("event_logo").equals("null")) {
                            eventDefinition.setEvent_logo(object.getString("event_logo"));
                        }
                        if (object.has("event_image1")
                                && !TextUtils.isEmpty(object.getString("event_image1"))
                                && !object.getString("event_image1").equals("null")) {
                            eventDefinition.setEvent_image1(object.getString("event_image1"));
                        }
                        if (object.has("event_image2")
                                && !TextUtils.isEmpty(object.getString("event_image2"))
                                && !object.getString("event_image2").equals("null")) {
                            eventDefinition.setEvent_image2(object.getString("event_image2"));
                        }
                        if (object.has("event_blob_image")
                                && !TextUtils.isEmpty(object.getString("event_blob_image"))
                                && !object.getString("event_blob_image").equals("null")) {
                            eventDefinition.setEvent_blob_image(object.getString("event_blob_image"));
                        }
                        if (object.has("event_address")
                                && !TextUtils.isEmpty(object.getString("event_address"))
                                && !object.getString("event_address").equals("null")) {
                            eventDefinition.setEvent_address(object.getString("event_address"));
                        }
                        if (object.has("covered_locations")
                                && !TextUtils.isEmpty(object.getString("covered_locations"))
                                && !object.getString("covered_locations").equals("null")) {
                            eventDefinition.setCovered_locations(object.getString("covered_locations"));
                        }
                        if (object.has("requirements")
                                && !TextUtils.isEmpty(object.getString("requirements"))
                                && !object.getString("requirements").equals("null")) {
                            eventDefinition.setRequirements(object.getString("requirements"));
                        }
                        if (object.has("appl_req_download")
                                && !TextUtils.isEmpty(object.getString("appl_req_download"))
                                && !object.getString("appl_req_download").equals("null")) {
                            eventDefinition.setAppl_req_download(object.getString("appl_req_download"));
                        }
                        if (object.has("cost")
                                && !TextUtils.isEmpty(object.getString("cost"))
                                && !object.getString("cost").equals("null")) {
                            eventDefinition.setCost(object.getString("cost"));
                        }
                        if (object.has("year")
                                && !TextUtils.isEmpty(object.getString("year"))
                                && !object.getString("year").equals("null")) {
                            eventDefinition.setYear(object.getString("year"));
                        }
                        if (object.has("location_address")
                                && !TextUtils.isEmpty(object.getString("location_address"))
                                && !object.getString("location_address").equals("null")) {
                            eventDefinition.setLocation_address(object.getString("location_address"));
                        }
                        if (object.has("scheme_type")
                                && !TextUtils.isEmpty(object.getString("scheme_type"))
                                && !object.getString("scheme_type").equals("null")) {
                            eventDefinition.setScheme_type(object.getString("scheme_type"));
                        }
                        if (object.has("event_prefix")
                                && !TextUtils.isEmpty(object.getString("event_prefix"))
                                && !object.getString("event_prefix").equals("null")) {
                            eventDefinition.setEvent_prefix(object.getString("event_prefix"));
                        }
                        if (object.has("event_nextnum")
                                && !TextUtils.isEmpty(object.getString("event_nextnum"))
                                && !object.getString("event_nextnum").equals("null")) {
                            eventDefinition.setEvent_nextnum(object.getString("event_nextnum"));
                        }
                        if (object.has("event_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_begins_date_time"))
                                && !object.getString("event_begins_date_time").equals("null")) {
                            eventDefinition.setEvent_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_begins_date_time")));
                        }
                        if (object.has("event_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_ends_date_time"))
                                && !object.getString("event_ends_date_time").equals("null")) {
                            eventDefinition.setEvent_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_ends_date_time")));
                        }
                        if (object.has("parking_reservation_limit")
                                && !TextUtils.isEmpty(object.getString("parking_reservation_limit"))
                                && !object.getString("parking_reservation_limit").equals("null")) {
                            eventDefinition.setParking_reservation_limit(object.getString("parking_reservation_limit"));
                        }
                        if (object.has("event_parking_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_parking_begins_date_time"))
                                && !object.getString("event_parking_begins_date_time").equals("null")) {
                            eventDefinition.setEvent_parking_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_parking_begins_date_time")));
                        }
                        if (object.has("event_parking_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_parking_ends_date_time"))
                                && !object.getString("event_parking_ends_date_time").equals("null")) {
                            eventDefinition.setEvent_parking_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_parking_ends_date_time")));
                        }

                        if (object.has("event_multi_dates")
                                && !TextUtils.isEmpty(object.getString("event_multi_dates"))
                                && !object.getString("event_multi_dates").equals("null")) {
                            eventDefinition.setEvent_multi_dates(object.getString("event_multi_dates"));
                            String currentDate = "";
                            currentDate = DateTimeUtils.getCurrentDateYYYYMMDDHHMMSS();
                            long currentDateMs = DateTimeUtils.getMillisecondsWithTime(currentDate);
//                            Log.e("#DEBUG", "   fetchEvent: currentDate:  " + currentDate);
//                            Log.e("#DEBUG", "   fetchEvent: currentDateMS:  " + currentDateMs);

                            try {
//                                Log.e("#DEBUG", "   fetchEvent:  eventDate:  " + eventDefinition.getEvent_begins_date_time());
//                                Log.e("#DEBUG", "  fetchEvent:  eventDateMS: " + DateTimeUtils.getMillisecondsWithTime(eventDefinition.getEvent_begins_date_time()));

                                if (currentDateMs == DateTimeUtils.getMillisecondsWithTime(eventDefinition.getEvent_begins_date_time())) {
                                    eventDefinition.setEventCategory("current");
                                } else if (currentDateMs > DateTimeUtils.getMillisecondsWithTime(eventDefinition.getEvent_begins_date_time())) {
                                    if (currentDateMs <= DateTimeUtils.getMillisecondsWithTime(eventDefinition.getEvent_ends_date_time())) {
                                        eventDefinition.setEventCategory("current");
                                    } else {
                                        eventDefinition.setEventCategory("past");
                                    }
                                } else if (currentDateMs < DateTimeUtils.getMillisecondsWithTime(eventDefinition.getEvent_begins_date_time())) {
                                    eventDefinition.setEventCategory("upcoming");
                                } else {
                                    eventDefinition.setEventCategory("");
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                eventDefinition.setEventCategory("");
                            }
                        } else {
                            eventDefinition.setEventCategory("");
                        }

                        if (object.has("event_parking_timings")
                                && !TextUtils.isEmpty(object.getString("event_parking_timings"))
                                && !object.getString("event_parking_timings").equals("null")) {
                            eventDefinition.setEvent_parking_timings(object.getString("event_parking_timings"));
                        }

                        if (object.has("event_event_timings")
                                && !TextUtils.isEmpty(object.getString("event_event_timings"))
                                && !object.getString("event_event_timings").equals("null")) {
                            eventDefinition.setEvent_event_timings(object.getString("event_event_timings"));
                        }

                        if (object.has("expires_by")
                                && !TextUtils.isEmpty(object.getString("expires_by"))
                                && !object.getString("expires_by").equals("null")) {
                            eventDefinition.setExpires_by(object.getString("expires_by"));
                        }
                        if (object.has("regn_reqd")
                                && !TextUtils.isEmpty(object.getString("regn_reqd"))
                                && !object.getString("regn_reqd").equals("null")) {
                            eventDefinition.setRegn_reqd(object.getBoolean("regn_reqd"));
                        }
                        if (object.has("regn_reqd_for_parking")
                                && !TextUtils.isEmpty(object.getString("regn_reqd_for_parking"))
                                && !object.getString("regn_reqd_for_parking").equals("null")) {
                            eventDefinition.setRegn_reqd_for_parking(object.getBoolean("regn_reqd_for_parking"));
                        }
                        if (object.has("renewable")
                                && !TextUtils.isEmpty(object.getString("renewable"))
                                && !object.getString("renewable").equals("null")) {
                            eventDefinition.setRenewable(object.getBoolean("renewable"));
                        }
                        if (object.has("active")
                                && !TextUtils.isEmpty(object.getString("active"))
                                && !object.getString("active").equals("null")) {
                            eventDefinition.setActive(object.getBoolean("active"));
                        }

                        if (object.has("event_multi_sessions")
                                && !TextUtils.isEmpty(object.getString("event_multi_sessions"))
                                && !object.getString("event_multi_sessions").equals("null")) {
                            eventDefinition.setEvent_multi_sessions(object.getBoolean("event_multi_sessions"));
                        }

                        if (object.has("event_indiv_session_regn_allowed")
                                && !TextUtils.isEmpty(object.getString("event_indiv_session_regn_allowed"))
                                && !object.getString("event_indiv_session_regn_allowed").equals("null")) {
                            eventDefinition.setEvent_indiv_session_regn_allowed(object.getBoolean("event_indiv_session_regn_allowed"));
                        }

                        if (object.has("event_indiv_session_regn_required")
                                && !TextUtils.isEmpty(object.getString("event_indiv_session_regn_required"))
                                && !object.getString("event_indiv_session_regn_required").equals("null")) {
                            eventDefinition.setEvent_indiv_session_regn_required(object.getBoolean("event_indiv_session_regn_required"));
                        }

                        if (object.has("event_full_regn_required")
                                && !TextUtils.isEmpty(object.getString("event_full_regn_required"))
                                && !object.getString("event_full_regn_required").equals("null")) {
                            eventDefinition.setEvent_full_regn_required(object.getBoolean("event_full_regn_required"));
                        }

                        if (object.has("event_full_regn_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_regn_fee"))
                                && !object.getString("event_full_regn_fee").equals("null")) {
                            eventDefinition.setEvent_full_regn_fee(object.getString("event_full_regn_fee"));
                        } else {
                            eventDefinition.setEvent_full_regn_fee("0");
                        }

                        if (object.has("event_parking_fee")
                                && !TextUtils.isEmpty(object.getString("event_parking_fee"))
                                && !object.getString("event_parking_fee").equals("null")) {
                            eventDefinition.setEvent_parking_fee(object.getString("event_parking_fee"));
                        } else {
                            eventDefinition.setEvent_parking_fee("0");
                        }

                        if (object.has("location_lat_lng")
                                && !TextUtils.isEmpty(object.getString("location_lat_lng"))
                                && !object.getString("location_lat_lng").equals("null")) {
                            eventDefinition.setLocation_lat_lng(object.getString("location_lat_lng"));
                        }

                        if (object.has("regn_user_info_reqd")
                                && !TextUtils.isEmpty(object.getString("regn_user_info_reqd"))
                                && !object.getString("regn_user_info_reqd").equals("null")) {
                            eventDefinition.setRegn_user_info_reqd(object.getBoolean("regn_user_info_reqd"));
                        }

                        if (object.has("regn_addl_user_info_reqd")
                                && !TextUtils.isEmpty(object.getString("regn_addl_user_info_reqd"))
                                && !object.getString("regn_addl_user_info_reqd").equals("null")) {
                            eventDefinition.setRegn_addl_user_info_reqd(object.getBoolean("regn_addl_user_info_reqd"));
                        }

                        if (object.has("event_full_youth_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_youth_fee"))
                                && !object.getString("event_full_youth_fee").equals("null")) {
                            eventDefinition.setEvent_full_youth_fee(object.getString("event_full_youth_fee"));
                        } else {
                            eventDefinition.setEvent_full_youth_fee("0");
                        }

                        if (object.has("event_full_child_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_child_fee"))
                                && !object.getString("event_full_child_fee").equals("null")) {
                            eventDefinition.setEvent_full_child_fee(object.getString("event_full_child_fee"));
                        } else {
                            eventDefinition.setEvent_full_child_fee("0");
                        }

                        if (object.has("event_full_student_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_student_fee"))
                                && !object.getString("event_full_student_fee").equals("null")) {
                            eventDefinition.setEvent_full_student_fee(object.getString("event_full_student_fee"));
                        } else {
                            eventDefinition.setEvent_full_student_fee("0");
                        }

                        if (object.has("event_full_minister_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_minister_fee"))
                                && !object.getString("event_full_minister_fee").equals("null")) {
                            eventDefinition.setEvent_full_minister_fee(object.getString("event_full_minister_fee"));
                        } else {
                            eventDefinition.setEvent_full_minister_fee("0");
                        }

                        if (object.has("event_full_clergy_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_clergy_fee"))
                                && !object.getString("event_full_clergy_fee").equals("null")) {
                            eventDefinition.setEvent_full_clergy_fee(object.getString("event_full_clergy_fee"));
                        } else {
                            eventDefinition.setEvent_full_clergy_fee("0");
                        }

                        if (object.has("event_full_promo_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_promo_fee"))
                                && !object.getString("event_full_promo_fee").equals("null")) {
                            eventDefinition.setEvent_full_promo_fee(object.getString("event_full_promo_fee"));
                        } else {
                            eventDefinition.setEvent_full_promo_fee("0");
                        }

                        if (object.has("event_full_senior_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_senior_fee"))
                                && !object.getString("event_full_senior_fee").equals("null")) {
                            eventDefinition.setEvent_full_senior_fee(object.getString("event_full_senior_fee"));
                        } else {
                            eventDefinition.setEvent_full_senior_fee("0");
                        }

                        if (object.has("event_full_staff_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_staff_fee"))
                                && !object.getString("event_full_staff_fee").equals("null")) {
                            eventDefinition.setEvent_full_staff_fee(object.getString("event_full_staff_fee"));
                        } else {
                            eventDefinition.setEvent_full_staff_fee("0");
                        }

                        if (object.has("free_event")
                                && !TextUtils.isEmpty(object.getString("free_event"))
                                && !object.getString("free_event").equals("null")) {
                            eventDefinition.setFree_event(object.getBoolean("free_event"));
                        }

                        if (object.has("free_event_parking")
                                && !TextUtils.isEmpty(object.getString("free_event_parking"))
                                && !object.getString("free_event_parking").equals("null")) {
                            eventDefinition.setFree_event_parking(object.getBoolean("free_event_parking"));
                        }

                        if (object.has("is_parking_allowed")
                                && !TextUtils.isEmpty(object.getString("is_parking_allowed"))
                                && !object.getString("is_parking_allowed").equals("null")) {
                            eventDefinition.setIs_parking_allowed(object.getBoolean("is_parking_allowed"));
                        }

                        if (object.has("local_timezone")
                                && !TextUtils.isEmpty(object.getString("local_timezone"))
                                && !object.getString("local_timezone").equals("null")) {
                            eventDefinition.setLocal_timezone(object.getString("local_timezone"));
                        }

                        if (object.has("local_time_event_start")
                                && !TextUtils.isEmpty(object.getString("local_time_event_start"))
                                && !object.getString("local_time_event_start").equals("null")) {
                            eventDefinition.setLocal_time_event_start(object.getString("local_time_event_start"));
                        }

                        if (object.has("reqd_local_event_regn")
                                && !TextUtils.isEmpty(object.getString("reqd_local_event_regn"))
                                && !object.getString("reqd_local_event_regn").equals("null")) {
                            eventDefinition.setReqd_local_event_regn(object.getBoolean("reqd_local_event_regn"));
                        } else eventDefinition.setReqd_local_event_regn(false);

                        if (object.has("reqd_web_event_regn")
                                && !TextUtils.isEmpty(object.getString("reqd_web_event_regn"))
                                && !object.getString("reqd_web_event_regn").equals("null")) {
                            eventDefinition.setReqd_web_event_regn(object.getBoolean("reqd_web_event_regn"));
                        } else eventDefinition.setReqd_web_event_regn(false);

                        if (object.has("free_local_event")
                                && !TextUtils.isEmpty(object.getString("free_local_event"))
                                && !object.getString("free_local_event").equals("null")) {
                            eventDefinition.setFree_local_event(object.getBoolean("free_local_event"));
                        } else eventDefinition.setFree_local_event(false);

                        if (object.has("free_web_event")
                                && !TextUtils.isEmpty(object.getString("free_web_event"))
                                && !object.getString("free_web_event").equals("null")) {
                            eventDefinition.setFree_web_event(object.getBoolean("free_web_event"));
                        } else eventDefinition.setFree_web_event(false);

                        if (object.has("event_link_array")
                                && !TextUtils.isEmpty(object.getString("event_link_array"))
                                && !object.getString("event_link_array").equals("null")) {
                            eventDefinition.setEvent_link_array(object.getString("event_link_array"));
                        }

                        eventDefinitions.add(eventDefinition);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return eventDefinitions;
        }

        @Override
        protected void onPostExecute(ArrayList<EventDefinition> s) {
            super.onPostExecute(s);
            if (listener != null) {
                listener.onSuccess(s);
            }
        }
    }

    public ArrayList<TownshipUser> getTownshipUserInfo(String responseStr) {
        ArrayList<TownshipUser> users = new ArrayList<>();
        try {
            JSONObject json = new JSONObject(responseStr);
            JSONArray jsonArray = json.getJSONArray("resource");
            for (int j = 0; j < jsonArray.length(); j++) {
                JSONObject object = jsonArray.getJSONObject(j);
                TownshipUser user = new TownshipUser();
                if (object.has("id")
                        && !TextUtils.isEmpty(object.getString("id"))
                        && !object.getString("id").equals("null")) {
                    user.setId(object.getInt("id"));
                }
                if (object.has("date_time")
                        && !TextUtils.isEmpty(object.getString("date_time"))
                        && !object.getString("date_time").equals("null")) {
                    user.setDateTime(object.getString("date_time"));
                }
                if (object.has("user_id")
                        && !TextUtils.isEmpty(object.getString("user_id"))
                        && !object.getString("user_id").equals("null")) {
                    user.setUserId(object.getInt("user_id"));
                }
                if (object.has("user_name")
                        && !TextUtils.isEmpty(object.getString("user_name"))
                        && !object.getString("user_name").equals("null")) {
                    user.setUserName(object.getString("user_name"));
                }
                if (object.has("twp_id")
                        && !TextUtils.isEmpty(object.getString("twp_id"))
                        && !object.getString("twp_id").equals("null")) {
                    user.setTwpId(object.getInt("twp_id"));
                }
                if (object.has("township_code")
                        && !TextUtils.isEmpty(object.getString("township_code"))
                        && !object.getString("township_code").equals("null")) {
                    user.setTownshipCode(object.getString("township_code"));
                }
                if (object.has("township_name")
                        && !TextUtils.isEmpty(object.getString("township_name"))
                        && !object.getString("township_name").equals("null")) {
                    user.setTownshipName(object.getString("township_name"));
                }
                if (object.has("profile_name")
                        && !TextUtils.isEmpty(object.getString("profile_name"))
                        && !object.getString("profile_name").equals("null")) {
                    user.setProfileName(object.getString("profile_name"));
                }
                if (object.has("status")
                        && !TextUtils.isEmpty(object.getString("status"))
                        && !object.getString("status").equals("null")) {
                    user.setStatus(object.getString("status"));
                }
                if (object.has("email")
                        && !TextUtils.isEmpty(object.getString("email"))
                        && !object.getString("email").equals("null")) {
                    user.setEmail(object.getString("email"));
                }
                users.add(user);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return users;
    }

    public ArrayList<UserProfile.UserProfileData> getUserInfo(String responseStr) {
        ArrayList<UserProfile.UserProfileData> users = new ArrayList<>();
        try {
            JSONObject json = new JSONObject(responseStr);
            JSONArray jsonArray = json.getJSONArray("resource");
            for (int j = 0; j < jsonArray.length(); j++) {
                JSONObject object = jsonArray.getJSONObject(j);
                if (object.has("user_id")
                        && !TextUtils.isEmpty(object.getString("user_id"))
                        && !object.getString("user_id").equals("null")) {
                    UserProfile.UserProfileData user = new UserProfile.UserProfileData();
                    user.setUser_id(object.getInt("user_id"));
                    if (object.has("id")
                            && !TextUtils.isEmpty(object.getString("id"))
                            && !object.getString("id").equals("null")) {
                        user.setId(object.getInt("id"));
                    }
                    if (object.has("date_time")
                            && !TextUtils.isEmpty(object.getString("date_time"))
                            && !object.getString("date_time").equals("null")) {
                        user.setDate_time(object.getString("date_time"));
                    }
                    if (object.has("fname")
                            && !TextUtils.isEmpty(object.getString("fname"))
                            && !object.getString("fname").equals("null")) {
                        user.setFname(object.getString("fname"));
                    }
                    if (object.has("lname")
                            && !TextUtils.isEmpty(object.getString("lname"))
                            && !object.getString("lname").equals("null")) {
                        user.setLname(object.getString("lname"));
                    }
                    if (object.has("email")
                            && !TextUtils.isEmpty(object.getString("email"))
                            && !object.getString("email").equals("null")) {
                        user.setEmail(object.getString("email"));
                    }
                    if (object.has("mobile")
                            && !TextUtils.isEmpty(object.getString("mobile"))
                            && !object.getString("mobile").equals("null")) {
                        user.setMobile(object.getString("mobile"));
                    }
                    if (object.has("lphone")
                            && !TextUtils.isEmpty(object.getString("lphone"))
                            && !object.getString("lphone").equals("null")) {
                        user.setLphone(object.getString("lphone"));
                    }
                    if (object.has("address")
                            && !TextUtils.isEmpty(object.getString("address"))
                            && !object.getString("address").equals("null")) {
                        user.setAddress(object.getString("address"));
                    }
                    if (object.has("city")
                            && !TextUtils.isEmpty(object.getString("city"))
                            && !object.getString("city").equals("null")) {
                        user.setCity(object.getString("city"));
                    }
                    if (object.has("state")
                            && !TextUtils.isEmpty(object.getString("state"))
                            && !object.getString("state").equals("null")) {
                        user.setState(object.getString("state"));
                    }
                    if (object.has("zip")
                            && !TextUtils.isEmpty(object.getString("zip"))
                            && !object.getString("zip").equals("null")) {
                        user.setZip(object.getString("zip"));
                    }
                    if (object.has("country")
                            && !TextUtils.isEmpty(object.getString("country"))
                            && !object.getString("country").equals("null")) {
                        user.setCountry(object.getString("country"));
                    }
                    if (object.has("full_address")
                            && !TextUtils.isEmpty(object.getString("full_address"))
                            && !object.getString("full_address").equals("null")) {
                        user.setFull_address(object.getString("full_address"));
                    }
                    if (object.has("ad_proof")
                            && !TextUtils.isEmpty(object.getString("ad_proof"))
                            && !object.getString("ad_proof").equals("null")) {
                        user.setAd_proof(object.getString("ad_proof"));
                    }
                    if (object.has("user_name")
                            && !TextUtils.isEmpty(object.getString("user_name"))
                            && !object.getString("user_name").equals("null")) {
                        user.setUser_name(object.getString("user_name"));
                    }

                    if (object.has("gender")
                            && !TextUtils.isEmpty(object.getString("gender"))
                            && !object.getString("gender").equals("null")) {
                        user.setGender(object.getString("gender"));
                    }
                    if (object.has("ip")
                            && !TextUtils.isEmpty(object.getString("ip"))
                            && !object.getString("ip").equals("null")) {
                        user.setIp(object.getString("ip"));
                    }
                    if (object.has("township_code")
                            && !TextUtils.isEmpty(object.getString("township_code"))
                            && !object.getString("township_code").equals("null")) {
                        user.setTownship_code(object.getString("township_code"));
                    }
                    if (object.has("firebase_token")
                            && !TextUtils.isEmpty(object.getString("firebase_token"))
                            && !object.getString("firebase_token").equals("null")) {
                        user.setFirebase_token(object.getString("firebase_token"));
                    }
                    if (user.getEmail() != null && user.getMobile() != null)
                        users.add(user);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return users;
    }

}
