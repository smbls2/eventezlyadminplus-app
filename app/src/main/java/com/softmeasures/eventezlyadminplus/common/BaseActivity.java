package com.softmeasures.eventezlyadminplus.common;

import androidx.appcompat.app.AppCompatActivity;

import com.softmeasures.eventezlyadminplus.MyApplication;

public abstract class BaseActivity extends AppCompatActivity {
    protected MyApplication myApp = MyApplication.getInstance();
    protected AppCompatActivity activity = this;

    protected abstract void updateViews();

    protected abstract void setListeners();

    protected abstract void initViews();

}
