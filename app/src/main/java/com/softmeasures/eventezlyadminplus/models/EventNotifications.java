package com.softmeasures.eventezlyadminplus.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EventNotifications {
    @SerializedName("resource")
    public List<EventNotificationData> resource;

    public List<EventNotificationData> getResource() {
        return resource;
    }

    public void setResource(List<EventNotificationData> resource) {
        this.resource = resource;
    }

    public class EventNotificationData {
        @SerializedName("id")
        private int id;
        private String date_created;
        private int group_id;
        private String group_code;
        private String group_name;
        private int user_id;
        private String email;
        private int event_id;
        private String event_name;
        private String event_date;
        private String event_type;
        private String event_company;
        private int event_company_id;
        private String event_company_type;
        private String notification_type;
        private String notification_data_type;
        private String notification_data;
        private String event_notif_image;
        private String notify_first_condition;
        private String notify_first_data;
        private String notify_interval;
        private String notify_last_condition;
        private String notify_last_data;
        private String sent_by_id;
        private String sent_by_email;
        private String accepted;
        private String accepted_date;
        private boolean grp_bcc;
        private boolean active;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDateCreated() {
            return date_created;
        }

        public void setDateCreated(String date_created) {
            this.date_created = date_created;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public boolean getActive() {
            return active;
        }

        public void setActive(boolean active) {
            this.active = active;
        }

        public int getGroup_id() {
            return group_id;
        }

        public void setGroup_id(int group_id) {
            this.group_id = group_id;
        }

        public String getGroup_code() {
            return group_code;
        }

        public void setGroup_code(String group_code) {
            this.group_code = group_code;
        }

        public String getGroup_name() {
            return group_name;
        }

        public void setGroup_name(String group_name) {
            this.group_name = group_name;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getEvent_id() {
            return event_id;
        }

        public void setEvent_id(int event_id) {
            this.event_id = event_id;
        }

        public String getEvent_name() {
            return event_name;
        }

        public void setEvent_name(String event_name) {
            this.event_name = event_name;
        }

        public String getEvent_date() {
            return event_date;
        }

        public void setEvent_date(String event_date) {
            this.event_date = event_date;
        }

        public String getEvent_type() {
            return event_type;
        }

        public void setEvent_type(String event_type) {
            this.event_type = event_type;
        }

        public String getEvent_company() {
            return event_company;
        }

        public void setEvent_company(String event_company) {
            this.event_company = event_company;
        }

        public int getEvent_company_id() {
            return event_company_id;
        }

        public void setEvent_company_id(int event_company_id) {
            this.event_company_id = event_company_id;
        }

        public String getEvent_company_type() {
            return event_company_type;
        }

        public void setEvent_company_type(String event_company_type) {
            this.event_company_type = event_company_type;
        }

        public String getNotification_type() {
            return notification_type;
        }

        public void setNotification_type(String notification_type) {
            this.notification_type = notification_type;
        }

        public String getNotification_data_type() {
            return notification_data_type;
        }

        public void setNotification_data_type(String notification_data_type) {
            this.notification_data_type = notification_data_type;
        }

        public String getNotification_data() {
            return notification_data;
        }

        public void setNotification_data(String notification_data) {
            this.notification_data = notification_data;
        }

        public String getNotify_first_condition() {
            return notify_first_condition;
        }

        public void setNotify_first_condition(String notify_first_condition) {
            this.notify_first_condition = notify_first_condition;
        }

        public String getNotify_first_data() {
            return notify_first_data;
        }

        public void setNotify_first_data(String notify_first_data) {
            this.notify_first_data = notify_first_data;
        }

        public String getNotify_interval() {
            return notify_interval;
        }

        public void setNotify_interval(String notify_interval) {
            this.notify_interval = notify_interval;
        }

        public String getNotify_last_condition() {
            return notify_last_condition;
        }

        public void setNotify_last_condition(String notify_last_condition) {
            this.notify_last_condition = notify_last_condition;
        }

        public String getNotify_last_data() {
            return notify_last_data;
        }

        public void setNotify_last_data(String notify_last_data) {
            this.notify_last_data = notify_last_data;
        }

        public boolean isGrp_bcc() {
            return grp_bcc;
        }

        public void setGrp_bcc(boolean grp_bcc) {
            this.grp_bcc = grp_bcc;
        }

        public String getEvent_notif_image() {
            return event_notif_image;
        }

        public void setEvent_notif_image(String event_notif_image) {
            this.event_notif_image = event_notif_image;
        }

        public String getSent_by_id() {
            return sent_by_id;
        }

        public void setSent_by_id(String sent_by_id) {
            this.sent_by_id = sent_by_id;
        }

        public String getSent_by_email() {
            return sent_by_email;
        }

        public void setSent_by_email(String sent_by_email) {
            this.sent_by_email = sent_by_email;
        }

        public String getAccepted() {
            return accepted;
        }

        public void setAccepted(String accepted) {
            this.accepted = accepted;
        }

        public String getAccepted_date() {
            return accepted_date;
        }

        public void setAccepted_date(String accepted_date) {
            this.accepted_date = accepted_date;
        }
    }
}