package com.softmeasures.eventezlyadminplus.models;

public class R_Parking {
    private String maxTime, parkingRate, parkingDurationUnit, parkingDuration, parkingAvailability,
            parkingLat, parkingLng, parkingAddress, parkingCity, parkingState, parkingZip, parkingCountry,
            markerAddress, markerLat, markerLng, markerCity, markerState, markerZip, markerCountry,
            markerType,
            locationCode, locationName, locationId, townshipCode,
            companyId, twpId, managerTypeId, managerType;

    private int event_id;
    private String event_code, event_name, event_specific_date, event_multi_dates, event_dt_short_info;
    private boolean multi_day, multi_day_reserve_allowed, multi_day_parking_allowed;
    private String max_days;

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public String getEvent_code() {
        return event_code;
    }

    public void setEvent_code(String event_code) {
        this.event_code = event_code;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_specific_date() {
        return event_specific_date;
    }

    public void setEvent_specific_date(String event_specific_date) {
        this.event_specific_date = event_specific_date;
    }

    public String getEvent_multi_dates() {
        return event_multi_dates;
    }

    public void setEvent_multi_dates(String event_multi_dates) {
        this.event_multi_dates = event_multi_dates;
    }

    public String getEvent_dt_short_info() {
        return event_dt_short_info;
    }

    public void setEvent_dt_short_info(String event_dt_short_info) {
        this.event_dt_short_info = event_dt_short_info;
    }

    public boolean isMulti_day() {
        return multi_day;
    }

    public void setMulti_day(boolean multi_day) {
        this.multi_day = multi_day;
    }

    public boolean isMulti_day_reserve_allowed() {
        return multi_day_reserve_allowed;
    }

    public void setMulti_day_reserve_allowed(boolean multi_day_reserve_allowed) {
        this.multi_day_reserve_allowed = multi_day_reserve_allowed;
    }

    public boolean isMulti_day_parking_allowed() {
        return multi_day_parking_allowed;
    }

    public void setMulti_day_parking_allowed(boolean multi_day_parking_allowed) {
        this.multi_day_parking_allowed = multi_day_parking_allowed;
    }

    public String getMax_days() {
        return max_days;
    }

    public void setMax_days(String max_days) {
        this.max_days = max_days;
    }

    public String getManagerTypeId() {
        return managerTypeId;
    }

    public void setManagerTypeId(String managerTypeId) {
        this.managerTypeId = managerTypeId;
    }

    public String getManagerType() {
        return managerType;
    }

    public void setManagerType(String managerType) {
        this.managerType = managerType;
    }

    public String getTwpId() {
        return twpId;
    }

    public void setTwpId(String twpId) {
        this.twpId = twpId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getParkingAvailability() {
        return parkingAvailability;
    }

    public void setParkingAvailability(String parkingAvailability) {
        this.parkingAvailability = parkingAvailability;
    }

    public String getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(String maxTime) {
        this.maxTime = maxTime;
    }

    public String getParkingRate() {
        return parkingRate;
    }

    public void setParkingRate(String parkingRate) {
        this.parkingRate = parkingRate;
    }

    public String getParkingDurationUnit() {
        return parkingDurationUnit;
    }

    public void setParkingDurationUnit(String parkingDurationUnit) {
        this.parkingDurationUnit = parkingDurationUnit;
    }

    public String getParkingDuration() {
        return parkingDuration;
    }

    public void setParkingDuration(String parkingDuration) {
        this.parkingDuration = parkingDuration;
    }

    public String getParkingLat() {
        return parkingLat;
    }

    public void setParkingLat(String parkingLat) {
        this.parkingLat = parkingLat;
    }

    public String getParkingLng() {
        return parkingLng;
    }

    public void setParkingLng(String parkingLng) {
        this.parkingLng = parkingLng;
    }

    public String getParkingAddress() {
        return parkingAddress;
    }

    public void setParkingAddress(String parkingAddress) {
        this.parkingAddress = parkingAddress;
    }

    public String getParkingCity() {
        return parkingCity;
    }

    public void setParkingCity(String parkingCity) {
        this.parkingCity = parkingCity;
    }

    public String getParkingState() {
        return parkingState;
    }

    public void setParkingState(String parkingState) {
        this.parkingState = parkingState;
    }

    public String getParkingZip() {
        return parkingZip;
    }

    public void setParkingZip(String parkingZip) {
        this.parkingZip = parkingZip;
    }

    public String getParkingCountry() {
        return parkingCountry;
    }

    public void setParkingCountry(String parkingCountry) {
        this.parkingCountry = parkingCountry;
    }

    public String getMarkerAddress() {
        return markerAddress;
    }

    public void setMarkerAddress(String markerAddress) {
        this.markerAddress = markerAddress;
    }

    public String getMarkerLat() {
        return markerLat;
    }

    public void setMarkerLat(String markerLat) {
        this.markerLat = markerLat;
    }

    public String getMarkerLng() {
        return markerLng;
    }

    public void setMarkerLng(String markerLng) {
        this.markerLng = markerLng;
    }

    public String getMarkerCity() {
        return markerCity;
    }

    public void setMarkerCity(String markerCity) {
        this.markerCity = markerCity;
    }

    public String getMarkerState() {
        return markerState;
    }

    public void setMarkerState(String markerState) {
        this.markerState = markerState;
    }

    public String getMarkerZip() {
        return markerZip;
    }

    public void setMarkerZip(String markerZip) {
        this.markerZip = markerZip;
    }

    public String getMarkerCountry() {
        return markerCountry;
    }

    public void setMarkerCountry(String markerCountry) {
        this.markerCountry = markerCountry;
    }

    public String getMarkerType() {
        return markerType;
    }

    public void setMarkerType(String markerType) {
        this.markerType = markerType;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getTownshipCode() {
        return townshipCode;
    }

    public void setTownshipCode(String townshipCode) {
        this.townshipCode = townshipCode;
    }
}
