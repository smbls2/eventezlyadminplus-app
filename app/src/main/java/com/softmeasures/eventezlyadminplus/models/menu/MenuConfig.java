package com.softmeasures.eventezlyadminplus.models.menu;

public class MenuConfig {
    private int id, menu_id, app_id, profile_type_id, company_id, parent_id, order_id, menu_level;
    private String menu_name, menu_in_app, app_name, profile_type, company_name, compaay_type,
            parent_name;
    private boolean access, shown;
    private String menu_icon, menu_icon_pixel, menu_font, menu_font_size, menu_color, menu_style,
            menu_bg_color, menu_bg_img;
    private boolean isPermit;

    public boolean isPermit() {
        return isPermit;
    }

    public void setPermit(boolean permit) {
        isPermit = permit;
    }

    public MenuConfig() {
    }

    public int getMenu_level() {
        return menu_level;
    }

    public void setMenu_level(int menu_level) {
        this.menu_level = menu_level;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public String getMenu_icon() {
        return menu_icon;
    }

    public void setMenu_icon(String menu_icon) {
        this.menu_icon = menu_icon;
    }

    public String getMenu_icon_pixel() {
        return menu_icon_pixel;
    }

    public void setMenu_icon_pixel(String menu_icon_pixel) {
        this.menu_icon_pixel = menu_icon_pixel;
    }

    public String getMenu_font() {
        return menu_font;
    }

    public void setMenu_font(String menu_font) {
        this.menu_font = menu_font;
    }

    public String getMenu_font_size() {
        return menu_font_size;
    }

    public void setMenu_font_size(String menu_font_size) {
        this.menu_font_size = menu_font_size;
    }

    public String getMenu_color() {
        return menu_color;
    }

    public void setMenu_color(String menu_color) {
        this.menu_color = menu_color;
    }

    public String getMenu_style() {
        return menu_style;
    }

    public void setMenu_style(String menu_style) {
        this.menu_style = menu_style;
    }

    public String getMenu_bg_color() {
        return menu_bg_color;
    }

    public void setMenu_bg_color(String menu_bg_color) {
        this.menu_bg_color = menu_bg_color;
    }

    public String getMenu_bg_img() {
        return menu_bg_img;
    }

    public void setMenu_bg_img(String menu_bg_img) {
        this.menu_bg_img = menu_bg_img;
    }

    public MenuConfig(boolean access, boolean shown, String menu_name) {
        this.access = access;
        this.shown = shown;
        this.menu_name = menu_name;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public String getParent_name() {
        return parent_name;
    }

    public void setParent_name(String parent_name) {
        this.parent_name = parent_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(int menu_id) {
        this.menu_id = menu_id;
    }

    public int getApp_id() {
        return app_id;
    }

    public void setApp_id(int app_id) {
        this.app_id = app_id;
    }

    public int getProfile_type_id() {
        return profile_type_id;
    }

    public void setProfile_type_id(int profile_type_id) {
        this.profile_type_id = profile_type_id;
    }

    public boolean isAccess() {
        return access;
    }

    public void setAccess(boolean access) {
        this.access = access;
    }

    public boolean isShown() {
        return shown;
    }

    public void setShown(boolean shown) {
        this.shown = shown;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public String getMenu_in_app() {
        return menu_in_app;
    }

    public void setMenu_in_app(String menu_in_app) {
        this.menu_in_app = menu_in_app;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public String getProfile_type() {
        return profile_type;
    }

    public void setProfile_type(String profile_type) {
        this.profile_type = profile_type;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompaay_type() {
        return compaay_type;
    }

    public void setCompaay_type(String compaay_type) {
        this.compaay_type = compaay_type;
    }
}
