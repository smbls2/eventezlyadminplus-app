package com.softmeasures.eventezlyadminplus.models;

import java.util.ArrayList;

public class MainManager {
    private ArrayList<Manager> resource = new ArrayList<>();

    public ArrayList<Manager> getResource() {
        return resource;
    }

    public void setResource(ArrayList<Manager> resource) {
        this.resource = resource;
    }
}
