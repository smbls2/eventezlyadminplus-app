package com.softmeasures.eventezlyadminplus.models.users;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserProfile {
    @SerializedName("resource")
    public List<UserProfileData> resource;

    public List<UserProfileData> getResource() {
        return resource;
    }

    public void setResource(List<UserProfileData> resource) {
        this.resource = resource;
    }

    public static class UserProfileData {
        @SerializedName("id")
        private int id;
        private String date_time, fname, lname, full_name, email, mobile, lphone, address, city, state, zip, country, full_address, ad_proof;
        private String user_name, password;
        private int user_id, selected_id;
        private String gender, ip, township_code, firebase_token, role;
        private boolean isSelect;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getLname() {
            return lname;
        }

        public void setLname(String lname) {
            this.lname = lname;
        }

        public String getFull_name() {
            return full_name;
        }

        public void setFull_name(String full_name) {
            this.full_name = full_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getLphone() {
            return lphone;
        }

        public void setLphone(String lphone) {
            this.lphone = lphone;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getZip() {
            return zip;
        }

        public void setZip(String zip) {
            this.zip = zip;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getFull_address() {
            return full_address;
        }

        public void setFull_address(String full_address) {
            this.full_address = full_address;
        }

        public String getAd_proof() {
            return ad_proof;
        }

        public void setAd_proof(String ad_proof) {
            this.ad_proof = ad_proof;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getTownship_code() {
            return township_code;
        }

        public void setTownship_code(String township_code) {
            this.township_code = township_code;
        }

        public String getFirebase_token() {
            return firebase_token;
        }

        public void setFirebase_token(String firebase_token) {
            this.firebase_token = firebase_token;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public int getSelected_id() {
            return selected_id;
        }

        public void setSelected_id(int selected_id) {
            this.selected_id = selected_id;
        }

        public boolean isSelect() {
            return isSelect;
        }

        public void setSelect(boolean select) {
            isSelect = select;
        }
    }
}
