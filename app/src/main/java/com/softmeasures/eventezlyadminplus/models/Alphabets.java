package com.softmeasures.eventezlyadminplus.models;

public class Alphabets {
    private String name;
    private boolean isSelected;

    public Alphabets() {
    }

    public Alphabets(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Alphabets) {
            return ((Alphabets) obj).name.equals(this.name);
        } else return super.equals(obj);
    }
}
