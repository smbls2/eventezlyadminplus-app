package com.softmeasures.eventezlyadminplus.models.call_chat;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GroupConfSettings {
    @SerializedName("resource")
    public List<GroupSettingData> resource;

    public List<GroupSettingData> getResource() {
        return resource;
    }

    public void setResource(List<GroupSettingData> resource) {
        this.resource = resource;
    }

    public static class GroupSettingData {

        @SerializedName("id")
        private int id;
        private String datetime;
        private int company_id, user_id,company_type_id;
        private String company_name, user_email, can_add_group_type, can_see_group_type, can_add_user_type, can_see_user_type;
        private int group_type;


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public int getCompany_id() {
            return company_id;
        }

        public void setCompany_id(int company_id) {
            this.company_id = company_id;
        }

        public String getCompany_name() {
            return company_name;
        }

        public void setCompany_name(String company_name) {
            this.company_name = company_name;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getUser_email() {
            return user_email;
        }

        public void setUser_email(String user_email) {
            this.user_email = user_email;
        }

        public String getCan_add_group_type() {
            return can_add_group_type;
        }

        public void setCan_add_group_type(String can_add_group_type) {
            this.can_add_group_type = can_add_group_type;
        }

        public String getCan_see_group_type() {
            return can_see_group_type;
        }

        public void setCan_see_group_type(String can_see_group_type) {
            this.can_see_group_type = can_see_group_type;
        }

        public int getGroup_type() {
            return group_type;
        }

        public void setGroup_type(int group_type) {
            this.group_type = group_type;
        }

        public String getCan_see_user_type() {
            return can_see_user_type;
        }

        public void setCan_see_user_type(String can_see_user_type) {
            this.can_see_user_type = can_see_user_type;
        }

        public String getCan_add_user_type() {
            return can_add_user_type;
        }

        public void setCan_add_user_type(String can_add_user_type) {
            this.can_add_user_type = can_add_user_type;
        }

        public int getCompany_type_id() {
            return company_type_id;
        }

        public void setCompany_type_id(int company_type_id) {
            this.company_type_id = company_type_id;
        }
    }
}