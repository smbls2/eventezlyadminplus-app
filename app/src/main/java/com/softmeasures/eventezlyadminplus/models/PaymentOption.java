package com.softmeasures.eventezlyadminplus.models;

public class PaymentOption {
    private int id;
    private String payment_option, payment_option_description;

    public PaymentOption() {
    }

    public PaymentOption(String payment_option) {
        this.payment_option = payment_option;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPayment_option() {
        return payment_option;
    }

    public void setPayment_option(String payment_option) {
        this.payment_option = payment_option;
    }

    public String getPayment_option_description() {
        return payment_option_description;
    }

    public void setPayment_option_description(String payment_option_description) {
        this.payment_option_description = payment_option_description;
    }

    @Override
    public String toString() {
        return payment_option_description;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PaymentOption)
            return ((PaymentOption) obj).getPayment_option().equals(this.payment_option);
        else return super.equals(obj);
    }
}
