package com.softmeasures.eventezlyadminplus.models;

public class FeeCategory {
    private String name, fee;

    public FeeCategory(String name, String fee) {
        this.name = name;
        this.fee = fee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    @Override
    public String toString() {
        return this.name + ", $" + this.fee;
    }
}
