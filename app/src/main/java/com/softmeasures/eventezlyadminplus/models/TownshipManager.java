package com.softmeasures.eventezlyadminplus.models;

public class TownshipManager {
    private int id, user_id, manager_type_id;
    private String date_time, manager_id, manager_type, twp_type, lot_manager, street, city, state,
            state_name, zip, country, address, contact_person, contact_title, contact_phone,
            contact_email, township_logo, official_logo, twp_image1, twp_image2, official_web,
            official_fb, official_twitter, twp_blob_image, active;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getManager_type_id() {
        return manager_type_id;
    }

    public void setManager_type_id(int manager_type_id) {
        this.manager_type_id = manager_type_id;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_type() {
        return manager_type;
    }

    public void setManager_type(String manager_type) {
        this.manager_type = manager_type;
    }

    public String getTwp_type() {
        return twp_type;
    }

    public void setTwp_type(String twp_type) {
        this.twp_type = twp_type;
    }

    public String getLot_manager() {
        return lot_manager;
    }

    public void setLot_manager(String lot_manager) {
        this.lot_manager = lot_manager;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact_person() {
        return contact_person;
    }

    public void setContact_person(String contact_person) {
        this.contact_person = contact_person;
    }

    public String getContact_title() {
        return contact_title;
    }

    public void setContact_title(String contact_title) {
        this.contact_title = contact_title;
    }

    public String getContact_phone() {
        return contact_phone;
    }

    public void setContact_phone(String contact_phone) {
        this.contact_phone = contact_phone;
    }

    public String getContact_email() {
        return contact_email;
    }

    public void setContact_email(String contact_email) {
        this.contact_email = contact_email;
    }

    public String getTownship_logo() {
        return township_logo;
    }

    public void setTownship_logo(String township_logo) {
        this.township_logo = township_logo;
    }

    public String getOfficial_logo() {
        return official_logo;
    }

    public void setOfficial_logo(String official_logo) {
        this.official_logo = official_logo;
    }

    public String getTwp_image1() {
        return twp_image1;
    }

    public void setTwp_image1(String twp_image1) {
        this.twp_image1 = twp_image1;
    }

    public String getTwp_image2() {
        return twp_image2;
    }

    public void setTwp_image2(String twp_image2) {
        this.twp_image2 = twp_image2;
    }

    public String getOfficial_web() {
        return official_web;
    }

    public void setOfficial_web(String official_web) {
        this.official_web = official_web;
    }

    public String getOfficial_fb() {
        return official_fb;
    }

    public void setOfficial_fb(String official_fb) {
        this.official_fb = official_fb;
    }

    public String getOfficial_twitter() {
        return official_twitter;
    }

    public void setOfficial_twitter(String official_twitter) {
        this.official_twitter = official_twitter;
    }

    public String getTwp_blob_image() {
        return twp_blob_image;
    }

    public void setTwp_blob_image(String twp_blob_image) {
        this.twp_blob_image = twp_blob_image;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}
