package com.softmeasures.eventezlyadminplus.models;

public class EventRegistration {
    private int id, manager_type_id, twp_id, company_id, event_id, event_session_id,
            user_id, event_regn_fee_category_id, regd_for_people_number;
    private String date_time, manager_type, township_code, township_name, company_code, company_name,
            event_type, event_code, event_name, event_session_type, event_session_code,
            event_session_name, user_phone_num, user_device_num, user_email, username,
            event_begins_date_time, event_ends_date_time, event_session_begins_date_time,
            event_session_ends_date_time, lat, lng, address1, event_regn_status,
            payment_method, event_rate, event_price_total, event_regn_fee_category, regd_fee_combined;
    private boolean full_event_regn, session_regn, event_parking_reserved, session_parking_reserved,
            event_regn_approval_reqd;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getManager_type_id() {
        return manager_type_id;
    }

    public void setManager_type_id(int manager_type_id) {
        this.manager_type_id = manager_type_id;
    }

    public int getTwp_id() {
        return twp_id;
    }

    public void setTwp_id(int twp_id) {
        this.twp_id = twp_id;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public int getEvent_session_id() {
        return event_session_id;
    }

    public void setEvent_session_id(int event_session_id) {
        this.event_session_id = event_session_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getEvent_regn_fee_category_id() {
        return event_regn_fee_category_id;
    }

    public void setEvent_regn_fee_category_id(int event_regn_fee_category_id) {
        this.event_regn_fee_category_id = event_regn_fee_category_id;
    }

    public int getRegd_for_people_number() {
        return regd_for_people_number;
    }

    public void setRegd_for_people_number(int regd_for_people_number) {
        this.regd_for_people_number = regd_for_people_number;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getManager_type() {
        return manager_type;
    }

    public void setManager_type(String manager_type) {
        this.manager_type = manager_type;
    }

    public String getTownship_code() {
        return township_code;
    }

    public void setTownship_code(String township_code) {
        this.township_code = township_code;
    }

    public String getTownship_name() {
        return township_name;
    }

    public void setTownship_name(String township_name) {
        this.township_name = township_name;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEvent_code() {
        return event_code;
    }

    public void setEvent_code(String event_code) {
        this.event_code = event_code;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_session_type() {
        return event_session_type;
    }

    public void setEvent_session_type(String event_session_type) {
        this.event_session_type = event_session_type;
    }

    public String getEvent_session_code() {
        return event_session_code;
    }

    public void setEvent_session_code(String event_session_code) {
        this.event_session_code = event_session_code;
    }

    public String getEvent_session_name() {
        return event_session_name;
    }

    public void setEvent_session_name(String event_session_name) {
        this.event_session_name = event_session_name;
    }

    public String getUser_phone_num() {
        return user_phone_num;
    }

    public void setUser_phone_num(String user_phone_num) {
        this.user_phone_num = user_phone_num;
    }

    public String getUser_device_num() {
        return user_device_num;
    }

    public void setUser_device_num(String user_device_num) {
        this.user_device_num = user_device_num;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEvent_begins_date_time() {
        return event_begins_date_time;
    }

    public void setEvent_begins_date_time(String event_begins_date_time) {
        this.event_begins_date_time = event_begins_date_time;
    }

    public String getEvent_ends_date_time() {
        return event_ends_date_time;
    }

    public void setEvent_ends_date_time(String event_ends_date_time) {
        this.event_ends_date_time = event_ends_date_time;
    }

    public String getEvent_session_begins_date_time() {
        return event_session_begins_date_time;
    }

    public void setEvent_session_begins_date_time(String event_session_begins_date_time) {
        this.event_session_begins_date_time = event_session_begins_date_time;
    }

    public String getEvent_session_ends_date_time() {
        return event_session_ends_date_time;
    }

    public void setEvent_session_ends_date_time(String event_session_ends_date_time) {
        this.event_session_ends_date_time = event_session_ends_date_time;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getEvent_regn_status() {
        return event_regn_status;
    }

    public void setEvent_regn_status(String event_regn_status) {
        this.event_regn_status = event_regn_status;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getEvent_rate() {
        return event_rate;
    }

    public void setEvent_rate(String event_rate) {
        this.event_rate = event_rate;
    }

    public String getEvent_price_total() {
        return event_price_total;
    }

    public void setEvent_price_total(String event_price_total) {
        this.event_price_total = event_price_total;
    }

    public String getEvent_regn_fee_category() {
        return event_regn_fee_category;
    }

    public void setEvent_regn_fee_category(String event_regn_fee_category) {
        this.event_regn_fee_category = event_regn_fee_category;
    }

    public String getRegd_fee_combined() {
        return regd_fee_combined;
    }

    public void setRegd_fee_combined(String regd_fee_combined) {
        this.regd_fee_combined = regd_fee_combined;
    }

    public boolean isFull_event_regn() {
        return full_event_regn;
    }

    public void setFull_event_regn(boolean full_event_regn) {
        this.full_event_regn = full_event_regn;
    }

    public boolean isSession_regn() {
        return session_regn;
    }

    public void setSession_regn(boolean session_regn) {
        this.session_regn = session_regn;
    }

    public boolean isEvent_parking_reserved() {
        return event_parking_reserved;
    }

    public void setEvent_parking_reserved(boolean event_parking_reserved) {
        this.event_parking_reserved = event_parking_reserved;
    }

    public boolean isSession_parking_reserved() {
        return session_parking_reserved;
    }

    public void setSession_parking_reserved(boolean session_parking_reserved) {
        this.session_parking_reserved = session_parking_reserved;
    }

    public boolean isEvent_regn_approval_reqd() {
        return event_regn_approval_reqd;
    }

    public void setEvent_regn_approval_reqd(boolean event_regn_approval_reqd) {
        this.event_regn_approval_reqd = event_regn_approval_reqd;
    }
}
