package com.softmeasures.eventezlyadminplus.models;

import java.util.ArrayList;

public class LocationLotMain {
    private ArrayList<LocationLotSub> locationLots = new ArrayList<>();

    public ArrayList<LocationLotSub> getLocationLots() {
        return locationLots;
    }

    public void setLocationLots(ArrayList<LocationLotSub> locationLots) {
        this.locationLots = locationLots;
    }
}
