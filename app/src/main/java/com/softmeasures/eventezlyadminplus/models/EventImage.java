package com.softmeasures.eventezlyadminplus.models;

public class EventImage {
    private String path;

    public EventImage() {
    }

    public EventImage(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
