package com.softmeasures.eventezlyadminplus.models;

import android.text.TextUtils;

public class ReserveParking {

    public ReserveParking() {
    }

    public ReserveParking(String location_code) {
        this.location_code = location_code;
    }

    private String id, company_type_id, company_type, manager_type_id, manager_type, company_id,
            parking_type, twp_id, township_code, location_id, location_code, lot_row, lot_number,
            lot_id, reservation_date_time, reserve_entry_time, reserve_expiry_time,
            reserve_exit_date_time, reserve_payment_condition, reserve_payment_due_by,
            reserve_payment_paid, reservation_status, entry_date_time, exit_date_time,
            expiry_time, user_id, permit_id, subscription_id, plate_no, pl_state, pl_country,
            lat, lng, address1, address2, city, state, zip, country, marker_lng,
            marker_lat, marker_address1, marker_address2, marker_city, marker_state,
            marker_zip, marker_country, distance_to_marker, ip, parking_token,
            parking_status, payment_method, parking_rate, parking_units, parking_qty,
            parking_subtotal, wallet_trx_id, tr_percent, tr_fee, parking_total, ipn_custom,
            ipn_txn_id, ipn_payment, ipn_status, ipn_address, ticket_status, expiry_status,
            notified_status, user_id_ref, mismatch, payment_choice, bursar_trx_id, platform,
            duration_unit, max_duration, selected_duration, user_id_exit, exit_forced,
            towed, exit_overnight, modified_time, vehicle_country, location_lot_id;
    private boolean CanCancel_Reservation;
    private String Reservation_PostPayment_term, Cancellation_Charge, Reservation_PostPayment_LateFee;
    private String location_name, marker_district, district;
    private long reservationEntryTime = 0, reservationExitTime = 0;

    public long getReservationEntryTime() {
        return reservationEntryTime;
    }

    public void setReservationEntryTime(long reservationEntryTime) {
        this.reservationEntryTime = reservationEntryTime;
    }

    public long getReservationExitTime() {
        return reservationExitTime;
    }

    public void setReservationExitTime(long reservationExitTime) {
        this.reservationExitTime = reservationExitTime;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getMarker_district() {
        return marker_district;
    }

    public void setMarker_district(String marker_district) {
        this.marker_district = marker_district;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getLocation_lot_id() {
        return location_lot_id;
    }

    public void setLocation_lot_id(String location_lot_id) {
        this.location_lot_id = location_lot_id;
    }

    public boolean isCanCancel_Reservation() {
        return CanCancel_Reservation;
    }

    public void setCanCancel_Reservation(boolean canCancel_Reservation) {
        CanCancel_Reservation = canCancel_Reservation;
    }

    public String getReservation_PostPayment_term() {
        return Reservation_PostPayment_term;
    }

    public void setReservation_PostPayment_term(String reservation_PostPayment_term) {
        Reservation_PostPayment_term = reservation_PostPayment_term;
    }

    public String getCancellation_Charge() {
        return Cancellation_Charge;
    }

    public void setCancellation_Charge(String cancellation_Charge) {
        Cancellation_Charge = cancellation_Charge;
    }

    public String getReservation_PostPayment_LateFee() {
        return Reservation_PostPayment_LateFee;
    }

    public void setReservation_PostPayment_LateFee(String reservation_PostPayment_LateFee) {
        Reservation_PostPayment_LateFee = reservation_PostPayment_LateFee;
    }

    public String getVehicle_country() {
        return vehicle_country;
    }

    public void setVehicle_country(String vehicle_country) {
        this.vehicle_country = vehicle_country;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompany_type_id() {
        return company_type_id;
    }

    public void setCompany_type_id(String company_type_id) {
        this.company_type_id = company_type_id;
    }

    public String getCompany_type() {
        return company_type;
    }

    public void setCompany_type(String company_type) {
        this.company_type = company_type;
    }

    public String getManager_type_id() {
        return manager_type_id;
    }

    public void setManager_type_id(String manager_type_id) {
        this.manager_type_id = manager_type_id;
    }

    public String getManager_type() {
        return manager_type;
    }

    public void setManager_type(String manager_type) {
        this.manager_type = manager_type;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getParking_type() {
        return parking_type;
    }

    public void setParking_type(String parking_type) {
        this.parking_type = parking_type;
    }

    public String getTwp_id() {
        return twp_id;
    }

    public void setTwp_id(String twp_id) {
        this.twp_id = twp_id;
    }

    public String getTownship_code() {
        return township_code;
    }

    public void setTownship_code(String township_code) {
        this.township_code = township_code;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getLocation_code() {
        return location_code;
    }

    public void setLocation_code(String location_code) {
        this.location_code = location_code;
    }

    public String getLot_row() {
        return lot_row;
    }

    public void setLot_row(String lot_row) {
        this.lot_row = lot_row;
    }

    public String getLot_number() {
        return lot_number;
    }

    public void setLot_number(String lot_number) {
        this.lot_number = lot_number;
    }

    public String getLot_id() {
        return lot_id;
    }

    public void setLot_id(String lot_id) {
        this.lot_id = lot_id;
    }

    public String getReservation_date_time() {
        return reservation_date_time;
    }

    public void setReservation_date_time(String reservation_date_time) {
        this.reservation_date_time = reservation_date_time;
    }

    public String getReserve_entry_time() {
        return reserve_entry_time;
    }

    public void setReserve_entry_time(String reserve_entry_time) {
        this.reserve_entry_time = reserve_entry_time;
    }

    public String getReserve_expiry_time() {
        return reserve_expiry_time;
    }

    public void setReserve_expiry_time(String reserve_expiry_time) {
        this.reserve_expiry_time = reserve_expiry_time;
    }

    public String getReserve_exit_date_time() {
        return reserve_exit_date_time;
    }

    public void setReserve_exit_date_time(String reserve_exit_date_time) {
        this.reserve_exit_date_time = reserve_exit_date_time;
    }

    public String getReserve_payment_condition() {
        return reserve_payment_condition;
    }

    public void setReserve_payment_condition(String reserve_payment_condition) {
        this.reserve_payment_condition = reserve_payment_condition;
    }

    public String getReserve_payment_due_by() {
        return reserve_payment_due_by;
    }

    public void setReserve_payment_due_by(String reserve_payment_due_by) {
        this.reserve_payment_due_by = reserve_payment_due_by;
    }

    public String getReserve_payment_paid() {
        return reserve_payment_paid;
    }

    public void setReserve_payment_paid(String reserve_payment_paid) {
        this.reserve_payment_paid = reserve_payment_paid;
    }

    public String getReservation_status() {
        return reservation_status;
    }

    public void setReservation_status(String reservation_status) {
        this.reservation_status = reservation_status;
    }

    public String getEntry_date_time() {
        return entry_date_time;
    }

    public void setEntry_date_time(String entry_date_time) {
        this.entry_date_time = entry_date_time;
    }

    public String getExit_date_time() {
        return exit_date_time;
    }

    public void setExit_date_time(String exit_date_time) {
        this.exit_date_time = exit_date_time;
    }

    public String getExpiry_time() {
        return expiry_time;
    }

    public void setExpiry_time(String expiry_time) {
        this.expiry_time = expiry_time;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPermit_id() {
        return permit_id;
    }

    public void setPermit_id(String permit_id) {
        this.permit_id = permit_id;
    }

    public String getSubscription_id() {
        return subscription_id;
    }

    public void setSubscription_id(String subscription_id) {
        this.subscription_id = subscription_id;
    }

    public String getPlate_no() {
        return plate_no;
    }

    public void setPlate_no(String plate_no) {
        this.plate_no = plate_no;
    }

    public String getPl_state() {
        return pl_state;
    }

    public void setPl_state(String pl_state) {
        this.pl_state = pl_state;
    }

    public String getPl_country() {
        return pl_country;
    }

    public void setPl_country(String pl_country) {
        this.pl_country = pl_country;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMarker_lng() {
        return marker_lng;
    }

    public void setMarker_lng(String marker_lng) {
        this.marker_lng = marker_lng;
    }

    public String getMarker_lat() {
        return marker_lat;
    }

    public void setMarker_lat(String marker_lat) {
        this.marker_lat = marker_lat;
    }

    public String getMarker_address1() {
        return marker_address1;
    }

    public void setMarker_address1(String marker_address1) {
        this.marker_address1 = marker_address1;
    }

    public String getMarker_address2() {
        return marker_address2;
    }

    public void setMarker_address2(String marker_address2) {
        this.marker_address2 = marker_address2;
    }

    public String getMarker_city() {
        return marker_city;
    }

    public void setMarker_city(String marker_city) {
        this.marker_city = marker_city;
    }

    public String getMarker_state() {
        return marker_state;
    }

    public void setMarker_state(String marker_state) {
        this.marker_state = marker_state;
    }

    public String getMarker_zip() {
        return marker_zip;
    }

    public void setMarker_zip(String marker_zip) {
        this.marker_zip = marker_zip;
    }

    public String getMarker_country() {
        return marker_country;
    }

    public void setMarker_country(String marker_country) {
        this.marker_country = marker_country;
    }

    public String getDistance_to_marker() {
        return distance_to_marker;
    }

    public void setDistance_to_marker(String distance_to_marker) {
        this.distance_to_marker = distance_to_marker;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getParking_token() {
        return parking_token;
    }

    public void setParking_token(String parking_token) {
        this.parking_token = parking_token;
    }

    public String getParking_status() {
        return parking_status;
    }

    public void setParking_status(String parking_status) {
        this.parking_status = parking_status;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getParking_rate() {
        return parking_rate;
    }

    public void setParking_rate(String parking_rate) {
        this.parking_rate = parking_rate;
    }

    public String getParking_units() {
        return parking_units;
    }

    public void setParking_units(String parking_units) {
        this.parking_units = parking_units;
    }

    public String getParking_qty() {
        return parking_qty;
    }

    public void setParking_qty(String parking_qty) {
        this.parking_qty = parking_qty;
    }

    public String getParking_subtotal() {
        return parking_subtotal;
    }

    public void setParking_subtotal(String parking_subtotal) {
        this.parking_subtotal = parking_subtotal;
    }

    public String getWallet_trx_id() {
        return wallet_trx_id;
    }

    public void setWallet_trx_id(String wallet_trx_id) {
        this.wallet_trx_id = wallet_trx_id;
    }

    public String getTr_percent() {
        return tr_percent;
    }

    public void setTr_percent(String tr_percent) {
        this.tr_percent = tr_percent;
    }

    public String getTr_fee() {
        return tr_fee;
    }

    public void setTr_fee(String tr_fee) {
        this.tr_fee = tr_fee;
    }

    public String getParking_total() {
        return parking_total;
    }

    public void setParking_total(String parking_total) {
        this.parking_total = parking_total;
    }

    public String getIpn_custom() {
        return ipn_custom;
    }

    public void setIpn_custom(String ipn_custom) {
        this.ipn_custom = ipn_custom;
    }

    public String getIpn_txn_id() {
        return ipn_txn_id;
    }

    public void setIpn_txn_id(String ipn_txn_id) {
        this.ipn_txn_id = ipn_txn_id;
    }

    public String getIpn_payment() {
        return ipn_payment;
    }

    public void setIpn_payment(String ipn_payment) {
        this.ipn_payment = ipn_payment;
    }

    public String getIpn_status() {
        return ipn_status;
    }

    public void setIpn_status(String ipn_status) {
        this.ipn_status = ipn_status;
    }

    public String getIpn_address() {
        return ipn_address;
    }

    public void setIpn_address(String ipn_address) {
        this.ipn_address = ipn_address;
    }

    public String getTicket_status() {
        return ticket_status;
    }

    public void setTicket_status(String ticket_status) {
        this.ticket_status = ticket_status;
    }

    public String getExpiry_status() {
        return expiry_status;
    }

    public void setExpiry_status(String expiry_status) {
        this.expiry_status = expiry_status;
    }

    public String getNotified_status() {
        return notified_status;
    }

    public void setNotified_status(String notified_status) {
        this.notified_status = notified_status;
    }

    public String getUser_id_ref() {
        return user_id_ref;
    }

    public void setUser_id_ref(String user_id_ref) {
        this.user_id_ref = user_id_ref;
    }

    public String getMismatch() {
        return mismatch;
    }

    public void setMismatch(String mismatch) {
        this.mismatch = mismatch;
    }

    public String getPayment_choice() {
        return payment_choice;
    }

    public void setPayment_choice(String payment_choice) {
        this.payment_choice = payment_choice;
    }

    public String getBursar_trx_id() {
        return bursar_trx_id;
    }

    public void setBursar_trx_id(String bursar_trx_id) {
        this.bursar_trx_id = bursar_trx_id;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getDuration_unit() {
        return duration_unit;
    }

    public void setDuration_unit(String duration_unit) {
        this.duration_unit = duration_unit;
    }

    public String getMax_duration() {
        return max_duration;
    }

    public void setMax_duration(String max_duration) {
        this.max_duration = max_duration;
    }

    public String getSelected_duration() {
        return selected_duration;
    }

    public void setSelected_duration(String selected_duration) {
        this.selected_duration = selected_duration;
    }

    public String getUser_id_exit() {
        return user_id_exit;
    }

    public void setUser_id_exit(String user_id_exit) {
        this.user_id_exit = user_id_exit;
    }

    public String getExit_forced() {
        return exit_forced;
    }

    public void setExit_forced(String exit_forced) {
        this.exit_forced = exit_forced;
    }

    public String getTowed() {
        return towed;
    }

    public void setTowed(String towed) {
        this.towed = towed;
    }

    public String getExit_overnight() {
        return exit_overnight;
    }

    public void setExit_overnight(String exit_overnight) {
        this.exit_overnight = exit_overnight;
    }

    public String getModified_time() {
        return modified_time;
    }

    public void setModified_time(String modified_time) {
        this.modified_time = modified_time;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ReserveParking) {
            return !TextUtils.isEmpty(((ReserveParking) obj).getLocation_code())
                    && !TextUtils.isEmpty(this.location_code)
                    && ((ReserveParking) obj).getLocation_code().equals(this.location_code);
        } else {
            return super.equals(obj);
        }
    }
}
