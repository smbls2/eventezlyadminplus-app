package com.softmeasures.eventezlyadminplus.models;

import java.util.ArrayList;

public class EventSession {
//    private int id, manager_type_id, twp_id, company_id, event_id, event_session_id, event_nextnum,
//            event_session_nextnum;
//    private String date_time, manager_type, township_code, township_name, company_code, company_name,
//            event_type, event_code, event_name, event_session_type, event_session_code, event_session_name,
//            event_session_short_description, event_session_long_description, event_session_link_on_web,
//            event_session_link_twitter, event_session_link_whatsapp, event_session_link_other_media,
//            company_logo, event_logo, event_session_logo, event_session_image1, event_session_image2,
//            event_session_blob_image, event_session_address, covered_locations,
//            requirements, appl_req_download, cost, year, location_address, scheme_type, event_prefix,
//            event_session_prefix, event_session_begins_date_time,
//            event_session_ends_date_time, event_session_parking_begins_date_time, event_session_parking_ends_date_time,
//            event_session_multi_dates, event_session_parking_timings, event_session_timings, expires_by,
//            event_session_regn_fee, event_session_led_by, event_session_leaders_bio;
//    private boolean regn_reqd, event_session_regn_allowed, regn_reqd_for_parking, renewable, active, session_regn_needed_approval;
//    private String event_session_parking_fee, local_time_event_session_start, local_timezone;
//
//    private String event_session_youth_fee, event_session_child_fee, event_session_student_fee,
//            event_session_minister_fee, event_session_clergy_fee, event_session_promo_fee,
//            event_session_senior_fee, event_session_staff_fee, event_session_family_fee;
//    private String eventCategory;
//    private boolean free_session, free_session_parking;
//    private String event_session_link_ytube, event_session_link_zoom, event_session_link_googlemeet,
//            event_session_link_googleclassroom, event_session_link_facebook;
//    private int event_session_logi_type;
//    private String event_session_regn_limit, web_event_session_regn_fee, web_event_session_regn_limit ,event_session_parking_reserv_limit;
//    private boolean web_event_session_location_to_show, free_web_session;
//    private boolean free_local_session, reqd_web_session_regn, reqd_local_session_regn,
//            is_parking_allowed;
//    private ArrayList<EventImage> eventImages = new ArrayList<>();
//    private String location_lat_lng;
//    private String session_link_array;
//
//    public String getSession_link_array() {
//        return session_link_array;
//    }
//
//    public void setSession_link_array(String session_link_array) {
//        this.session_link_array = session_link_array;
//    }
//
//    public String getLocation_lat_lng() {
//        return location_lat_lng;
//    }
//
//    public void setLocation_lat_lng(String location_lat_lng) {
//        this.location_lat_lng = location_lat_lng;
//    }
//
//    public ArrayList<EventImage> getEventImages() {
//        return eventImages;
//    }
//
//    public void setEventImages(ArrayList<EventImage> eventImages) {
//        this.eventImages = eventImages;
//    }
//
//    public String getLocal_timezone() {
//        return local_timezone;
//    }
//
//    public void setLocal_timezone(String local_timezone) {
//        this.local_timezone = local_timezone;
//    }
//
//    public String getLocal_time_event_session_start() {
//        return local_time_event_session_start;
//    }
//
//    public void setLocal_time_event_session_start(String local_time_event_session_start) {
//        this.local_time_event_session_start = local_time_event_session_start;
//    }
//
//    public boolean isIs_parking_allowed() {
//        return is_parking_allowed;
//    }
//
//    public void setIs_parking_allowed(boolean is_parking_allowed) {
//        this.is_parking_allowed = is_parking_allowed;
//    }
//
//    public String getEvent_session_family_fee() {
//        return event_session_family_fee;
//    }
//
//    public void setEvent_session_family_fee(String event_session_family_fee) {
//        this.event_session_family_fee = event_session_family_fee;
//    }
//
//    public boolean isReqd_local_session_regn() {
//        return reqd_local_session_regn;
//    }
//
//    public void setReqd_local_session_regn(boolean reqd_local_session_regn) {
//        this.reqd_local_session_regn = reqd_local_session_regn;
//    }
//
//    public boolean isReqd_web_session_regn() {
//        return reqd_web_session_regn;
//    }
//
//    public void setReqd_web_session_regn(boolean reqd_web_session_regn) {
//        this.reqd_web_session_regn = reqd_web_session_regn;
//    }
//
//    public boolean isFree_local_session() {
//        return free_local_session;
//    }
//
//    public void setFree_local_session(boolean free_local_session) {
//        this.free_local_session = free_local_session;
//    }
//
//    public boolean isFree_web_session() {
//        return free_web_session;
//    }
//
//    public void setFree_web_session(boolean free_web_session) {
//        this.free_web_session = free_web_session;
//    }
//
//    public String getEvent_session_regn_limit() {
//        return event_session_regn_limit;
//    }
//
//    public void setEvent_session_regn_limit(String event_session_regn_limit) {
//        this.event_session_regn_limit = event_session_regn_limit;
//    }
//
//    public String getWeb_event_session_regn_fee() {
//        return web_event_session_regn_fee;
//    }
//
//    public void setWeb_event_session_regn_fee(String web_event_session_regn_fee) {
//        this.web_event_session_regn_fee = web_event_session_regn_fee;
//    }
//
//    public String getWeb_event_session_regn_limit() {
//        return web_event_session_regn_limit;
//    }
//
//    public void setWeb_event_session_regn_limit(String web_event_session_regn_limit) {
//        this.web_event_session_regn_limit = web_event_session_regn_limit;
//    }
//
//    public String getEvent_session_parking_reserv_limit() {
//        return event_session_parking_reserv_limit;
//    }
//
//    public void setEvent_session_parking_reserv_limit(String event_session_parking_reserv_limit) {
//        this.event_session_parking_reserv_limit = event_session_parking_reserv_limit;
//    }
//
//    public boolean isWeb_event_session_location_to_show() {
//        return web_event_session_location_to_show;
//    }
//
//    public void setWeb_event_session_location_to_show(boolean web_event_session_location_to_show) {
//        this.web_event_session_location_to_show = web_event_session_location_to_show;
//    }
//
//    public int getEvent_session_logi_type() {
//        return event_session_logi_type;
//    }
//
//    public void setEvent_session_logi_type(int event_session_logi_type) {
//        this.event_session_logi_type = event_session_logi_type;
//    }
//
//    public String getEvent_session_link_ytube() {
//        return event_session_link_ytube;
//    }
//
//    public void setEvent_session_link_ytube(String event_session_link_ytube) {
//        this.event_session_link_ytube = event_session_link_ytube;
//    }
//
//    public String getEvent_session_link_zoom() {
//        return event_session_link_zoom;
//    }
//
//    public void setEvent_session_link_zoom(String event_session_link_zoom) {
//        this.event_session_link_zoom = event_session_link_zoom;
//    }
//
//    public String getEvent_session_link_googlemeet() {
//        return event_session_link_googlemeet;
//    }
//
//    public void setEvent_session_link_googlemeet(String event_session_link_googlemeet) {
//        this.event_session_link_googlemeet = event_session_link_googlemeet;
//    }
//
//    public String getEvent_session_link_googleclassroom() {
//        return event_session_link_googleclassroom;
//    }
//
//    public void setEvent_session_link_googleclassroom(String event_session_link_googleclassroom) {
//        this.event_session_link_googleclassroom = event_session_link_googleclassroom;
//    }
//
//    public String getEvent_session_link_facebook() {
//        return event_session_link_facebook;
//    }
//
//    public void setEvent_session_link_facebook(String event_session_link_facebook) {
//        this.event_session_link_facebook = event_session_link_facebook;
//    }
//
//    public boolean isFree_session() {
//        return free_session;
//    }
//
//    public void setFree_session(boolean free_session) {
//        this.free_session = free_session;
//    }
//
//    public boolean isFree_session_parking() {
//        return free_session_parking;
//    }
//
//    public void setFree_session_parking(boolean free_session_parking) {
//        this.free_session_parking = free_session_parking;
//    }
//
//    public String getEvent_session_youth_fee() {
//        return event_session_youth_fee;
//    }
//
//    public void setEvent_session_youth_fee(String event_session_youth_fee) {
//        this.event_session_youth_fee = event_session_youth_fee;
//    }
//
//    public String getEvent_session_child_fee() {
//        return event_session_child_fee;
//    }
//
//    public void setEvent_session_child_fee(String event_session_child_fee) {
//        this.event_session_child_fee = event_session_child_fee;
//    }
//
//    public String getEvent_session_student_fee() {
//        return event_session_student_fee;
//    }
//
//    public void setEvent_session_student_fee(String event_session_student_fee) {
//        this.event_session_student_fee = event_session_student_fee;
//    }
//
//    public String getEvent_session_minister_fee() {
//        return event_session_minister_fee;
//    }
//
//    public void setEvent_session_minister_fee(String event_session_minister_fee) {
//        this.event_session_minister_fee = event_session_minister_fee;
//    }
//
//    public String getEvent_session_clergy_fee() {
//        return event_session_clergy_fee;
//    }
//
//    public void setEvent_session_clergy_fee(String event_session_clergy_fee) {
//        this.event_session_clergy_fee = event_session_clergy_fee;
//    }
//
//    public String getEvent_session_promo_fee() {
//        return event_session_promo_fee;
//    }
//
//    public void setEvent_session_promo_fee(String event_session_promo_fee) {
//        this.event_session_promo_fee = event_session_promo_fee;
//    }
//
//    public String getEvent_session_senior_fee() {
//        return event_session_senior_fee;
//    }
//
//    public void setEvent_session_senior_fee(String event_session_senior_fee) {
//        this.event_session_senior_fee = event_session_senior_fee;
//    }
//
//    public String getEvent_session_staff_fee() {
//        return event_session_staff_fee;
//    }
//
//    public void setEvent_session_staff_fee(String event_session_staff_fee) {
//        this.event_session_staff_fee = event_session_staff_fee;
//    }
//
//    public String getEventCategory() {
//        return eventCategory;
//    }
//
//    public void setEventCategory(String eventCategory) {
//        this.eventCategory = eventCategory;
//    }
//
//    public String getEvent_session_parking_fee() {
//        return event_session_parking_fee;
//    }
//
//    public void setEvent_session_parking_fee(String event_session_parking_fee) {
//        this.event_session_parking_fee = event_session_parking_fee;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public int getManager_type_id() {
//        return manager_type_id;
//    }
//
//    public void setManager_type_id(int manager_type_id) {
//        this.manager_type_id = manager_type_id;
//    }
//
//    public int getTwp_id() {
//        return twp_id;
//    }
//
//    public void setTwp_id(int twp_id) {
//        this.twp_id = twp_id;
//    }
//
//    public int getCompany_id() {
//        return company_id;
//    }
//
//    public void setCompany_id(int company_id) {
//        this.company_id = company_id;
//    }
//
//    public int getEvent_id() {
//        return event_id;
//    }
//
//    public void setEvent_id(int event_id) {
//        this.event_id = event_id;
//    }
//
//    public int getEvent_session_id() {
//        return event_session_id;
//    }
//
//    public void setEvent_session_id(int event_session_id) {
//        this.event_session_id = event_session_id;
//    }
//
//    public int getEvent_nextnum() {
//        return event_nextnum;
//    }
//
//    public void setEvent_nextnum(int event_nextnum) {
//        this.event_nextnum = event_nextnum;
//    }
//
//    public int getEvent_session_nextnum() {
//        return event_session_nextnum;
//    }
//
//    public void setEvent_session_nextnum(int event_session_nextnum) {
//        this.event_session_nextnum = event_session_nextnum;
//    }
//
//    public String getDate_time() {
//        return date_time;
//    }
//
//    public void setDate_time(String date_time) {
//        this.date_time = date_time;
//    }
//
//    public String getManager_type() {
//        return manager_type;
//    }
//
//    public void setManager_type(String manager_type) {
//        this.manager_type = manager_type;
//    }
//
//    public String getTownship_code() {
//        return township_code;
//    }
//
//    public void setTownship_code(String township_code) {
//        this.township_code = township_code;
//    }
//
//    public String getTownship_name() {
//        return township_name;
//    }
//
//    public void setTownship_name(String township_name) {
//        this.township_name = township_name;
//    }
//
//    public String getCompany_code() {
//        return company_code;
//    }
//
//    public void setCompany_code(String company_code) {
//        this.company_code = company_code;
//    }
//
//    public String getCompany_name() {
//        return company_name;
//    }
//
//    public void setCompany_name(String company_name) {
//        this.company_name = company_name;
//    }
//
//    public String getEvent_type() {
//        return event_type;
//    }
//
//    public void setEvent_type(String event_type) {
//        this.event_type = event_type;
//    }
//
//    public String getEvent_code() {
//        return event_code;
//    }
//
//    public void setEvent_code(String event_code) {
//        this.event_code = event_code;
//    }
//
//    public String getEvent_name() {
//        return event_name;
//    }
//
//    public void setEvent_name(String event_name) {
//        this.event_name = event_name;
//    }
//
//    public String getEvent_session_type() {
//        return event_session_type;
//    }
//
//    public void setEvent_session_type(String event_session_type) {
//        this.event_session_type = event_session_type;
//    }
//
//    public String getEvent_session_code() {
//        return event_session_code;
//    }
//
//    public void setEvent_session_code(String event_session_code) {
//        this.event_session_code = event_session_code;
//    }
//
//    public String getEvent_session_name() {
//        return event_session_name;
//    }
//
//    public void setEvent_session_name(String event_session_name) {
//        this.event_session_name = event_session_name;
//    }
//
//    public String getEvent_session_short_description() {
//        return event_session_short_description;
//    }
//
//    public void setEvent_session_short_description(String event_session_short_description) {
//        this.event_session_short_description = event_session_short_description;
//    }
//
//    public String getEvent_session_long_description() {
//        return event_session_long_description;
//    }
//
//    public void setEvent_session_long_description(String event_session_long_description) {
//        this.event_session_long_description = event_session_long_description;
//    }
//
//    public String getEvent_session_link_on_web() {
//        return event_session_link_on_web;
//    }
//
//    public void setEvent_session_link_on_web(String event_session_link_on_web) {
//        this.event_session_link_on_web = event_session_link_on_web;
//    }
//
//    public String getEvent_session_link_twitter() {
//        return event_session_link_twitter;
//    }
//
//    public void setEvent_session_link_twitter(String event_session_link_twitter) {
//        this.event_session_link_twitter = event_session_link_twitter;
//    }
//
//    public String getEvent_session_link_whatsapp() {
//        return event_session_link_whatsapp;
//    }
//
//    public void setEvent_session_link_whatsapp(String event_session_link_whatsapp) {
//        this.event_session_link_whatsapp = event_session_link_whatsapp;
//    }
//
//    public String getEvent_session_link_other_media() {
//        return event_session_link_other_media;
//    }
//
//    public void setEvent_session_link_other_media(String event_session_link_other_media) {
//        this.event_session_link_other_media = event_session_link_other_media;
//    }
//
//    public String getCompany_logo() {
//        return company_logo;
//    }
//
//    public void setCompany_logo(String company_logo) {
//        this.company_logo = company_logo;
//    }
//
//    public String getEvent_logo() {
//        return event_logo;
//    }
//
//    public void setEvent_logo(String event_logo) {
//        this.event_logo = event_logo;
//    }
//
//    public String getEvent_session_logo() {
//        return event_session_logo;
//    }
//
//    public void setEvent_session_logo(String event_session_logo) {
//        this.event_session_logo = event_session_logo;
//    }
//
//    public String getEvent_session_image1() {
//        return event_session_image1;
//    }
//
//    public void setEvent_session_image1(String event_session_image1) {
//        this.event_session_image1 = event_session_image1;
//    }
//
//    public String getEvent_session_image2() {
//        return event_session_image2;
//    }
//
//    public void setEvent_session_image2(String event_session_image2) {
//        this.event_session_image2 = event_session_image2;
//    }
//
//    public String getEvent_session_blob_image() {
//        return event_session_blob_image;
//    }
//
//    public void setEvent_session_blob_image(String event_session_blob_image) {
//        this.event_session_blob_image = event_session_blob_image;
//    }
//
//    public String getEvent_session_address() {
//        return event_session_address;
//    }
//
//    public void setEvent_session_address(String event_session_address) {
//        this.event_session_address = event_session_address;
//    }
//
//    public String getCovered_locations() {
//        return covered_locations;
//    }
//
//    public void setCovered_locations(String covered_locations) {
//        this.covered_locations = covered_locations;
//    }
//
//    public boolean isSession_regn_needed_approval() {
//        return session_regn_needed_approval;
//    }
//
//    public void setSession_regn_needed_approval(boolean session_regn_needed_approval) {
//        this.session_regn_needed_approval = session_regn_needed_approval;
//    }
//
//    public String getRequirements() {
//        return requirements;
//    }
//
//    public void setRequirements(String requirements) {
//        this.requirements = requirements;
//    }
//
//    public String getAppl_req_download() {
//        return appl_req_download;
//    }
//
//    public void setAppl_req_download(String appl_req_download) {
//        this.appl_req_download = appl_req_download;
//    }
//
//    public String getCost() {
//        return cost;
//    }
//
//    public void setCost(String cost) {
//        this.cost = cost;
//    }
//
//    public String getYear() {
//        return year;
//    }
//
//    public void setYear(String year) {
//        this.year = year;
//    }
//
//    public String getLocation_address() {
//        return location_address;
//    }
//
//    public void setLocation_address(String location_address) {
//        this.location_address = location_address;
//    }
//
//    public String getScheme_type() {
//        return scheme_type;
//    }
//
//    public void setScheme_type(String scheme_type) {
//        this.scheme_type = scheme_type;
//    }
//
//    public String getEvent_prefix() {
//        return event_prefix;
//    }
//
//    public void setEvent_prefix(String event_prefix) {
//        this.event_prefix = event_prefix;
//    }
//
//    public String getEvent_session_prefix() {
//        return event_session_prefix;
//    }
//
//    public void setEvent_session_prefix(String event_session_prefix) {
//        this.event_session_prefix = event_session_prefix;
//    }
//
//    public String getEvent_session_begins_date_time() {
//        return event_session_begins_date_time;
//    }
//
//    public void setEvent_session_begins_date_time(String event_session_begins_date_time) {
//        this.event_session_begins_date_time = event_session_begins_date_time;
//    }
//
//    public String getEvent_session_ends_date_time() {
//        return event_session_ends_date_time;
//    }
//
//    public void setEvent_session_ends_date_time(String event_session_ends_date_time) {
//        this.event_session_ends_date_time = event_session_ends_date_time;
//    }
//
//    public String getEvent_session_parking_begins_date_time() {
//        return event_session_parking_begins_date_time;
//    }
//
//    public void setEvent_session_parking_begins_date_time(String event_session_parking_begins_date_time) {
//        this.event_session_parking_begins_date_time = event_session_parking_begins_date_time;
//    }
//
//    public String getEvent_session_parking_ends_date_time() {
//        return event_session_parking_ends_date_time;
//    }
//
//    public void setEvent_session_parking_ends_date_time(String event_session_parking_ends_date_time) {
//        this.event_session_parking_ends_date_time = event_session_parking_ends_date_time;
//    }
//
//    public String getEvent_session_multi_dates() {
//        return event_session_multi_dates;
//    }
//
//    public void setEvent_session_multi_dates(String event_session_multi_dates) {
//        this.event_session_multi_dates = event_session_multi_dates;
//    }
//
//    public String getEvent_session_parking_timings() {
//        return event_session_parking_timings;
//    }
//
//    public void setEvent_session_parking_timings(String event_session_parking_timings) {
//        this.event_session_parking_timings = event_session_parking_timings;
//    }
//
//    public String getEvent_session_timings() {
//        return event_session_timings;
//    }
//
//    public void setEvent_session_timings(String event_session_timings) {
//        this.event_session_timings = event_session_timings;
//    }
//
//    public String getExpires_by() {
//        return expires_by;
//    }
//
//    public void setExpires_by(String expires_by) {
//        this.expires_by = expires_by;
//    }
//
//    public String getEvent_session_regn_fee() {
//        return event_session_regn_fee;
//    }
//
//    public void setEvent_session_regn_fee(String event_session_regn_fee) {
//        this.event_session_regn_fee = event_session_regn_fee;
//    }
//
//    public String getEvent_session_led_by() {
//        return event_session_led_by;
//    }
//
//    public void setEvent_session_led_by(String event_session_led_by) {
//        this.event_session_led_by = event_session_led_by;
//    }
//
//    public String getEvent_session_leaders_bio() {
//        return event_session_leaders_bio;
//    }
//
//    public void setEvent_session_leaders_bio(String event_session_leaders_bio) {
//        this.event_session_leaders_bio = event_session_leaders_bio;
//    }
//
//    public boolean isRegn_reqd() {
//        return regn_reqd;
//    }
//
//    public void setRegn_reqd(boolean regn_reqd) {
//        this.regn_reqd = regn_reqd;
//    }
//
//    public boolean isEvent_session_regn_allowed() {
//        return event_session_regn_allowed;
//    }
//
//    public void setEvent_session_regn_allowed(boolean event_session_regn_allowed) {
//        this.event_session_regn_allowed = event_session_regn_allowed;
//    }
//
//    public boolean isRegn_reqd_for_parking() {
//        return regn_reqd_for_parking;
//    }
//
//    public void setRegn_reqd_for_parking(boolean regn_reqd_for_parking) {
//        this.regn_reqd_for_parking = regn_reqd_for_parking;
//    }
//
//    public boolean isRenewable() {
//        return renewable;
//    }
//
//    public void setRenewable(boolean renewable) {
//        this.renewable = renewable;
//    }
//
//    public boolean isActive() {
//        return active;
//    }
//
//    public void setActive(boolean active) {
//        this.active = active;
//    }
private int id, manager_type_id, twp_id, company_id, event_id, event_session_id, event_nextnum,
        event_session_nextnum, parking_reservation_limit;
    private String date_time, manager_type, township_code, township_name, company_code, company_name,
            event_type, event_code, event_name, event_session_type, event_session_code, event_session_name,
            event_session_short_description, event_session_long_description, event_session_link_on_web,
            event_session_link_twitter, event_session_link_whatsapp, event_session_link_other_media,
            company_logo, event_logo, event_session_logo, event_session_image1, event_session_image2,
            event_session_blob_image, event_session_address, covered_locations,
            requirements, appl_req_download, cost, year, location_address, scheme_type, event_prefix,
            event_session_prefix, event_session_begins_date_time,
            event_session_ends_date_time, event_session_parking_begins_date_time, event_session_parking_ends_date_time,
            event_session_multi_dates, event_session_parking_timings, event_session_timings, expires_by,
            event_session_regn_fee, event_session_led_by, event_session_leaders_bio;
    private boolean regn_reqd, event_session_regn_allowed, regn_reqd_for_parking, renewable, active, session_regn_needed_approval;
    private String event_session_parking_fee, local_time_event_session_start, local_timezone, session_parking_fee_duration;

    private String event_session_youth_fee, event_session_child_fee, event_session_student_fee,
            event_session_minister_fee, event_session_clergy_fee, event_session_promo_fee,
            event_session_senior_fee, event_session_staff_fee, event_session_family_fee;
    private String eventCategory;
    private boolean free_session, free_session_parking;
    private String event_session_link_ytube, event_session_link_zoom, event_session_link_googlemeet,
            event_session_link_googleclassroom, event_session_link_facebook;
    private int event_session_logi_type;
    private String event_session_regn_limit, web_event_session_regn_fee, web_event_session_regn_limit;
    private boolean web_event_session_location_to_show, free_web_session;
    private boolean free_local_session, reqd_web_session_regn, reqd_local_session_regn,
            is_parking_allowed;
    private ArrayList<EventImage> eventImages = new ArrayList<>();
    private String location_lat_lng;
    private String session_link_array;

    public String getSession_link_array() {
        return session_link_array;
    }

    public void setSession_link_array(String session_link_array) {
        this.session_link_array = session_link_array;
    }

    public String getLocation_lat_lng() {
        return location_lat_lng;
    }

    public void setLocation_lat_lng(String location_lat_lng) {
        this.location_lat_lng = location_lat_lng;
    }

    public ArrayList<EventImage> getEventImages() {
        return eventImages;
    }

    public void setEventImages(ArrayList<EventImage> eventImages) {
        this.eventImages = eventImages;
    }

    public String getLocal_timezone() {
        return local_timezone;
    }

    public void setLocal_timezone(String local_timezone) {
        this.local_timezone = local_timezone;
    }

    public String getLocal_time_event_session_start() {
        return local_time_event_session_start;
    }

    public void setLocal_time_event_session_start(String local_time_event_session_start) {
        this.local_time_event_session_start = local_time_event_session_start;
    }

    public boolean isParking_allowed() {
        return is_parking_allowed;
    }

    public void setIs_parking_allowed(boolean is_parking_allowed) {
        this.is_parking_allowed = is_parking_allowed;
    }

    public String getEvent_session_family_fee() {
        return event_session_family_fee;
    }

    public void setEvent_session_family_fee(String event_session_family_fee) {
        this.event_session_family_fee = event_session_family_fee;
    }

    public boolean isReqd_local_session_regn() {
        return reqd_local_session_regn;
    }

    public void setReqd_local_session_regn(boolean reqd_local_session_regn) {
        this.reqd_local_session_regn = reqd_local_session_regn;
    }

    public boolean isReqd_web_session_regn() {
        return reqd_web_session_regn;
    }

    public void setReqd_web_session_regn(boolean reqd_web_session_regn) {
        this.reqd_web_session_regn = reqd_web_session_regn;
    }

    public boolean isFree_local_session() {
        return free_local_session;
    }

    public void setFree_local_session(boolean free_local_session) {
        this.free_local_session = free_local_session;
    }

    public boolean isFree_web_session() {
        return free_web_session;
    }

    public void setFree_web_session(boolean free_web_session) {
        this.free_web_session = free_web_session;
    }

    public String getEvent_session_regn_limit() {
        return event_session_regn_limit;
    }

    public void setEvent_session_regn_limit(String event_session_regn_limit) {
        this.event_session_regn_limit = event_session_regn_limit;
    }

    public String getWeb_event_session_regn_fee() {
        return web_event_session_regn_fee;
    }

    public void setWeb_event_session_regn_fee(String web_event_session_regn_fee) {
        this.web_event_session_regn_fee = web_event_session_regn_fee;
    }

    public String getWeb_event_session_regn_limit() {
        return web_event_session_regn_limit;
    }

    public void setWeb_event_session_regn_limit(String web_event_session_regn_limit) {
        this.web_event_session_regn_limit = web_event_session_regn_limit;
    }

    public boolean isWeb_event_session_location_to_show() {
        return web_event_session_location_to_show;
    }

    public void setWeb_event_session_location_to_show(boolean web_event_session_location_to_show) {
        this.web_event_session_location_to_show = web_event_session_location_to_show;
    }

    public int getEvent_session_logi_type() {
        return event_session_logi_type;
    }

    public void setEvent_session_logi_type(int event_session_logi_type) {
        this.event_session_logi_type = event_session_logi_type;
    }

    public String getEvent_session_link_ytube() {
        return event_session_link_ytube;
    }

    public void setEvent_session_link_ytube(String event_session_link_ytube) {
        this.event_session_link_ytube = event_session_link_ytube;
    }

    public String getEvent_session_link_zoom() {
        return event_session_link_zoom;
    }

    public void setEvent_session_link_zoom(String event_session_link_zoom) {
        this.event_session_link_zoom = event_session_link_zoom;
    }

    public String getEvent_session_link_googlemeet() {
        return event_session_link_googlemeet;
    }

    public void setEvent_session_link_googlemeet(String event_session_link_googlemeet) {
        this.event_session_link_googlemeet = event_session_link_googlemeet;
    }

    public String getEvent_session_link_googleclassroom() {
        return event_session_link_googleclassroom;
    }

    public void setEvent_session_link_googleclassroom(String event_session_link_googleclassroom) {
        this.event_session_link_googleclassroom = event_session_link_googleclassroom;
    }

    public String getEvent_session_link_facebook() {
        return event_session_link_facebook;
    }

    public void setEvent_session_link_facebook(String event_session_link_facebook) {
        this.event_session_link_facebook = event_session_link_facebook;
    }

    public boolean isFree_session() {
        return free_session;
    }

    public void setFree_session(boolean free_session) {
        this.free_session = free_session;
    }

    public boolean isFree_session_parking() {
        return free_session_parking;
    }

    public void setFree_session_parking(boolean free_session_parking) {
        this.free_session_parking = free_session_parking;
    }

    public String getEvent_session_youth_fee() {
        return event_session_youth_fee;
    }

    public void setEvent_session_youth_fee(String event_session_youth_fee) {
        this.event_session_youth_fee = event_session_youth_fee;
    }

    public String getEvent_session_child_fee() {
        return event_session_child_fee;
    }

    public void setEvent_session_child_fee(String event_session_child_fee) {
        this.event_session_child_fee = event_session_child_fee;
    }

    public String getEvent_session_student_fee() {
        return event_session_student_fee;
    }

    public void setEvent_session_student_fee(String event_session_student_fee) {
        this.event_session_student_fee = event_session_student_fee;
    }

    public String getEvent_session_minister_fee() {
        return event_session_minister_fee;
    }

    public void setEvent_session_minister_fee(String event_session_minister_fee) {
        this.event_session_minister_fee = event_session_minister_fee;
    }

    public String getEvent_session_clergy_fee() {
        return event_session_clergy_fee;
    }

    public void setEvent_session_clergy_fee(String event_session_clergy_fee) {
        this.event_session_clergy_fee = event_session_clergy_fee;
    }

    public String getEvent_session_promo_fee() {
        return event_session_promo_fee;
    }

    public void setEvent_session_promo_fee(String event_session_promo_fee) {
        this.event_session_promo_fee = event_session_promo_fee;
    }

    public String getEvent_session_senior_fee() {
        return event_session_senior_fee;
    }

    public void setEvent_session_senior_fee(String event_session_senior_fee) {
        this.event_session_senior_fee = event_session_senior_fee;
    }

    public String getEvent_session_staff_fee() {
        return event_session_staff_fee;
    }

    public void setEvent_session_staff_fee(String event_session_staff_fee) {
        this.event_session_staff_fee = event_session_staff_fee;
    }

    public String getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(String eventCategory) {
        this.eventCategory = eventCategory;
    }

    public String getEvent_session_parking_fee() {
        return event_session_parking_fee;
    }

    public void setEvent_session_parking_fee(String event_session_parking_fee) {
        this.event_session_parking_fee = event_session_parking_fee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getManager_type_id() {
        return manager_type_id;
    }

    public void setManager_type_id(int manager_type_id) {
        this.manager_type_id = manager_type_id;
    }

    public int getTwp_id() {
        return twp_id;
    }

    public void setTwp_id(int twp_id) {
        this.twp_id = twp_id;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public int getEvent_session_id() {
        return event_session_id;
    }

    public void setEvent_session_id(int event_session_id) {
        this.event_session_id = event_session_id;
    }

    public int getEvent_nextnum() {
        return event_nextnum;
    }

    public void setEvent_nextnum(int event_nextnum) {
        this.event_nextnum = event_nextnum;
    }

    public int getEvent_session_nextnum() {
        return event_session_nextnum;
    }

    public void setEvent_session_nextnum(int event_session_nextnum) {
        this.event_session_nextnum = event_session_nextnum;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getManager_type() {
        return manager_type;
    }

    public void setManager_type(String manager_type) {
        this.manager_type = manager_type;
    }

    public String getTownship_code() {
        return township_code;
    }

    public void setTownship_code(String township_code) {
        this.township_code = township_code;
    }

    public String getTownship_name() {
        return township_name;
    }

    public void setTownship_name(String township_name) {
        this.township_name = township_name;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEvent_code() {
        return event_code;
    }

    public void setEvent_code(String event_code) {
        this.event_code = event_code;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_session_type() {
        return event_session_type;
    }

    public void setEvent_session_type(String event_session_type) {
        this.event_session_type = event_session_type;
    }

    public String getEvent_session_code() {
        return event_session_code;
    }

    public void setEvent_session_code(String event_session_code) {
        this.event_session_code = event_session_code;
    }

    public String getEvent_session_name() {
        return event_session_name;
    }

    public void setEvent_session_name(String event_session_name) {
        this.event_session_name = event_session_name;
    }

    public String getEvent_session_short_description() {
        return event_session_short_description;
    }

    public void setEvent_session_short_description(String event_session_short_description) {
        this.event_session_short_description = event_session_short_description;
    }

    public String getEvent_session_long_description() {
        return event_session_long_description;
    }

    public void setEvent_session_long_description(String event_session_long_description) {
        this.event_session_long_description = event_session_long_description;
    }

    public String getEvent_session_link_on_web() {
        return event_session_link_on_web;
    }

    public void setEvent_session_link_on_web(String event_session_link_on_web) {
        this.event_session_link_on_web = event_session_link_on_web;
    }

    public String getEvent_session_link_twitter() {
        return event_session_link_twitter;
    }

    public void setEvent_session_link_twitter(String event_session_link_twitter) {
        this.event_session_link_twitter = event_session_link_twitter;
    }

    public String getEvent_session_link_whatsapp() {
        return event_session_link_whatsapp;
    }

    public void setEvent_session_link_whatsapp(String event_session_link_whatsapp) {
        this.event_session_link_whatsapp = event_session_link_whatsapp;
    }

    public String getEvent_session_link_other_media() {
        return event_session_link_other_media;
    }

    public void setEvent_session_link_other_media(String event_session_link_other_media) {
        this.event_session_link_other_media = event_session_link_other_media;
    }

    public String getCompany_logo() {
        return company_logo;
    }

    public void setCompany_logo(String company_logo) {
        this.company_logo = company_logo;
    }

    public String getEvent_logo() {
        return event_logo;
    }

    public void setEvent_logo(String event_logo) {
        this.event_logo = event_logo;
    }

    public String getEvent_session_logo() {
        return event_session_logo;
    }

    public void setEvent_session_logo(String event_session_logo) {
        this.event_session_logo = event_session_logo;
    }

    public String getEvent_session_image1() {
        return event_session_image1;
    }

    public void setEvent_session_image1(String event_session_image1) {
        this.event_session_image1 = event_session_image1;
    }

    public String getEvent_session_image2() {
        return event_session_image2;
    }

    public void setEvent_session_image2(String event_session_image2) {
        this.event_session_image2 = event_session_image2;
    }

    public String getEvent_session_blob_image() {
        return event_session_blob_image;
    }

    public void setEvent_session_blob_image(String event_session_blob_image) {
        this.event_session_blob_image = event_session_blob_image;
    }

    public String getEvent_session_address() {
        return event_session_address;
    }

    public void setEvent_session_address(String event_session_address) {
        this.event_session_address = event_session_address;
    }

    public String getCovered_locations() {
        return covered_locations;
    }

    public void setCovered_locations(String covered_locations) {
        this.covered_locations = covered_locations;
    }

    public boolean isSession_regn_needed_approval() {
        return session_regn_needed_approval;
    }

    public void setSession_regn_needed_approval(boolean session_regn_needed_approval) {
        this.session_regn_needed_approval = session_regn_needed_approval;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public String getAppl_req_download() {
        return appl_req_download;
    }

    public void setAppl_req_download(String appl_req_download) {
        this.appl_req_download = appl_req_download;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getLocation_address() {
        return location_address;
    }

    public void setLocation_address(String location_address) {
        this.location_address = location_address;
    }

    public String getScheme_type() {
        return scheme_type;
    }

    public void setScheme_type(String scheme_type) {
        this.scheme_type = scheme_type;
    }

    public String getEvent_prefix() {
        return event_prefix;
    }

    public void setEvent_prefix(String event_prefix) {
        this.event_prefix = event_prefix;
    }

    public String getEvent_session_prefix() {
        return event_session_prefix;
    }

    public void setEvent_session_prefix(String event_session_prefix) {
        this.event_session_prefix = event_session_prefix;
    }

    public String getEvent_session_begins_date_time() {
        return event_session_begins_date_time;
    }

    public void setEvent_session_begins_date_time(String event_session_begins_date_time) {
        this.event_session_begins_date_time = event_session_begins_date_time;
    }

    public String getEvent_session_ends_date_time() {
        return event_session_ends_date_time;
    }

    public void setEvent_session_ends_date_time(String event_session_ends_date_time) {
        this.event_session_ends_date_time = event_session_ends_date_time;
    }

    public String getEvent_session_parking_begins_date_time() {
        return event_session_parking_begins_date_time;
    }

    public void setEvent_session_parking_begins_date_time(String event_session_parking_begins_date_time) {
        this.event_session_parking_begins_date_time = event_session_parking_begins_date_time;
    }

    public String getEvent_session_parking_ends_date_time() {
        return event_session_parking_ends_date_time;
    }

    public void setEvent_session_parking_ends_date_time(String event_session_parking_ends_date_time) {
        this.event_session_parking_ends_date_time = event_session_parking_ends_date_time;
    }

    public String getEvent_session_multi_dates() {
        return event_session_multi_dates;
    }

    public void setEvent_session_multi_dates(String event_session_multi_dates) {
        this.event_session_multi_dates = event_session_multi_dates;
    }

    public String getEvent_session_parking_timings() {
        return event_session_parking_timings;
    }

    public void setEvent_session_parking_timings(String event_session_parking_timings) {
        this.event_session_parking_timings = event_session_parking_timings;
    }

    public String getEvent_session_timings() {
        return event_session_timings;
    }

    public void setEvent_session_timings(String event_session_timings) {
        this.event_session_timings = event_session_timings;
    }

    public String getExpires_by() {
        return expires_by;
    }

    public void setExpires_by(String expires_by) {
        this.expires_by = expires_by;
    }

    public String getEvent_session_regn_fee() {
        return event_session_regn_fee;
    }

    public void setEvent_session_regn_fee(String event_session_regn_fee) {
        this.event_session_regn_fee = event_session_regn_fee;
    }

    public String getEvent_session_led_by() {
        return event_session_led_by;
    }

    public void setEvent_session_led_by(String event_session_led_by) {
        this.event_session_led_by = event_session_led_by;
    }

    public String getEvent_session_leaders_bio() {
        return event_session_leaders_bio;
    }

    public void setEvent_session_leaders_bio(String event_session_leaders_bio) {
        this.event_session_leaders_bio = event_session_leaders_bio;
    }

    public boolean isRegn_reqd() {
        return regn_reqd;
    }

    public void setRegn_reqd(boolean regn_reqd) {
        this.regn_reqd = regn_reqd;
    }

    public boolean isEvent_session_regn_allowed() {
        return event_session_regn_allowed;
    }

    public void setEvent_session_regn_allowed(boolean event_session_regn_allowed) {
        this.event_session_regn_allowed = event_session_regn_allowed;
    }

    public boolean isRegn_reqd_for_parking() {
        return regn_reqd_for_parking;
    }

    public void setRegn_reqd_for_parking(boolean regn_reqd_for_parking) {
        this.regn_reqd_for_parking = regn_reqd_for_parking;
    }

    public boolean isRenewable() {
        return renewable;
    }

    public void setRenewable(boolean renewable) {
        this.renewable = renewable;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getSession_parking_fee_duration() {
        return session_parking_fee_duration;
    }

    public void setSession_parking_fee_duration(String session_parking_fee_duration) {
        this.session_parking_fee_duration = session_parking_fee_duration;
    }

    public int getParking_reservation_limit() {
        return parking_reservation_limit;
    }

    public void setParking_reservation_limit(int parking_reservation_limit) {
        this.parking_reservation_limit = parking_reservation_limit;
    }
}
