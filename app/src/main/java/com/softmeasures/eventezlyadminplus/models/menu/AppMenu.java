package com.softmeasures.eventezlyadminplus.models.menu;

public class AppMenu {
    private int id, app_id;
    private String menu_id, menu_name, menu_description, menu_icon, menu_font, menu_color,
            menu_style, menu_bg_color, menu_bg_img, app_name;

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public String getMenu_description() {
        return menu_description;
    }

    public void setMenu_description(String menu_description) {
        this.menu_description = menu_description;
    }

    public String getMenu_icon() {
        return menu_icon;
    }

    public void setMenu_icon(String menu_icon) {
        this.menu_icon = menu_icon;
    }

    public String getMenu_font() {
        return menu_font;
    }

    public void setMenu_font(String menu_font) {
        this.menu_font = menu_font;
    }

    public String getMenu_color() {
        return menu_color;
    }

    public void setMenu_color(String menu_color) {
        this.menu_color = menu_color;
    }

    public String getMenu_style() {
        return menu_style;
    }

    public void setMenu_style(String menu_style) {
        this.menu_style = menu_style;
    }

    public String getMenu_bg_color() {
        return menu_bg_color;
    }

    public void setMenu_bg_color(String menu_bg_color) {
        this.menu_bg_color = menu_bg_color;
    }

    public String getMenu_bg_img() {
        return menu_bg_img;
    }

    public void setMenu_bg_img(String menu_bg_img) {
        this.menu_bg_img = menu_bg_img;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getApp_id() {
        return app_id;
    }

    public void setApp_id(int app_id) {
        this.app_id = app_id;
    }
}
