package com.softmeasures.eventezlyadminplus.models;

public class ParkingPartner {
    private int id, location_id, active, in_effect, lots_avbl, lots_total, off_peak_discount,
            renewable, user_id, weekend_special_diff, twp_id, division_id, company_id, dept_id,
            isKiosk, manager_type_id, company_type_id, event_on_location;
    private String location_place_id, location_code, location_name, place_id, address,
            custom_notice, lot_numbering_type, lot_numbering_description, marker_type, no_parking_times,
            off_peak_ends, off_peak_starts, parking_times, title, township_id, township_code,
            manager_id;
    private double lat, lng;
    private String event_dates;

    public String getEvent_dates() {
        return event_dates;
    }

    public void setEvent_dates(String event_dates) {
        this.event_dates = event_dates;
    }

    public int getEvent_on_location() {
        return event_on_location;
    }

    public void setEvent_on_location(int event_on_location) {
        this.event_on_location = event_on_location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getIn_effect() {
        return in_effect;
    }

    public void setIn_effect(int in_effect) {
        this.in_effect = in_effect;
    }

    public int getLots_avbl() {
        return lots_avbl;
    }

    public void setLots_avbl(int lots_avbl) {
        this.lots_avbl = lots_avbl;
    }

    public int getLots_total() {
        return lots_total;
    }

    public void setLots_total(int lots_total) {
        this.lots_total = lots_total;
    }

    public int getOff_peak_discount() {
        return off_peak_discount;
    }

    public void setOff_peak_discount(int off_peak_discount) {
        this.off_peak_discount = off_peak_discount;
    }

    public int getRenewable() {
        return renewable;
    }

    public void setRenewable(int renewable) {
        this.renewable = renewable;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getWeekend_special_diff() {
        return weekend_special_diff;
    }

    public void setWeekend_special_diff(int weekend_special_diff) {
        this.weekend_special_diff = weekend_special_diff;
    }

    public int getTwp_id() {
        return twp_id;
    }

    public void setTwp_id(int twp_id) {
        this.twp_id = twp_id;
    }

    public int getDivision_id() {
        return division_id;
    }

    public void setDivision_id(int division_id) {
        this.division_id = division_id;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public int getDept_id() {
        return dept_id;
    }

    public void setDept_id(int dept_id) {
        this.dept_id = dept_id;
    }

    public int getIsKiosk() {
        return isKiosk;
    }

    public void setIsKiosk(int isKiosk) {
        this.isKiosk = isKiosk;
    }

    public int getManager_type_id() {
        return manager_type_id;
    }

    public void setManager_type_id(int manager_type_id) {
        this.manager_type_id = manager_type_id;
    }

    public int getCompany_type_id() {
        return company_type_id;
    }

    public void setCompany_type_id(int company_type_id) {
        this.company_type_id = company_type_id;
    }

    public String getLocation_place_id() {
        return location_place_id;
    }

    public void setLocation_place_id(String location_place_id) {
        this.location_place_id = location_place_id;
    }

    public String getLocation_code() {
        return location_code;
    }

    public void setLocation_code(String location_code) {
        this.location_code = location_code;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCustom_notice() {
        return custom_notice;
    }

    public void setCustom_notice(String custom_notice) {
        this.custom_notice = custom_notice;
    }

    public String getLot_numbering_type() {
        return lot_numbering_type;
    }

    public void setLot_numbering_type(String lot_numbering_type) {
        this.lot_numbering_type = lot_numbering_type;
    }

    public String getLot_numbering_description() {
        return lot_numbering_description;
    }

    public void setLot_numbering_description(String lot_numbering_description) {
        this.lot_numbering_description = lot_numbering_description;
    }

    public String getMarker_type() {
        return marker_type;
    }

    public void setMarker_type(String marker_type) {
        this.marker_type = marker_type;
    }

    public String getNo_parking_times() {
        return no_parking_times;
    }

    public void setNo_parking_times(String no_parking_times) {
        this.no_parking_times = no_parking_times;
    }

    public String getOff_peak_ends() {
        return off_peak_ends;
    }

    public void setOff_peak_ends(String off_peak_ends) {
        this.off_peak_ends = off_peak_ends;
    }

    public String getOff_peak_starts() {
        return off_peak_starts;
    }

    public void setOff_peak_starts(String off_peak_starts) {
        this.off_peak_starts = off_peak_starts;
    }

    public String getParking_times() {
        return parking_times;
    }

    public void setParking_times(String parking_times) {
        this.parking_times = parking_times;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTownship_id() {
        return township_id;
    }

    public void setTownship_id(String township_id) {
        this.township_id = township_id;
    }

    public String getTownship_code() {
        return township_code;
    }

    public void setTownship_code(String township_code) {
        this.township_code = township_code;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ParkingPartner) {
            return ((ParkingPartner) obj).getLat() == this.lat
                    && ((ParkingPartner) obj).getLng() == this.lng;
        } else {
            return super.equals(obj);
        }
    }
}
