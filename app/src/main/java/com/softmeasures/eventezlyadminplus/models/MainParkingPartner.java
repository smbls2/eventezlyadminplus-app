package com.softmeasures.eventezlyadminplus.models;

import java.util.ArrayList;

public class MainParkingPartner {

    private ArrayList<ParkingPartner> resource = new ArrayList<>();

    public ArrayList<ParkingPartner> getResource() {
        return resource;
    }

    public void setResource(ArrayList<ParkingPartner> resource) {
        this.resource = resource;
    }
}
