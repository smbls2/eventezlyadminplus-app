package com.softmeasures.eventezlyadminplus.models.rest;

import com.google.gson.JsonObject;
import com.softmeasures.eventezlyadminplus.models.EventInvitations;
import com.softmeasures.eventezlyadminplus.models.EventNotifications;
import com.softmeasures.eventezlyadminplus.models.call_chat.ChatConfSettings;
import com.softmeasures.eventezlyadminplus.models.call_chat.ChatLogs;
import com.softmeasures.eventezlyadminplus.models.call_chat.GroupConfSettings;
import com.softmeasures.eventezlyadminplus.models.users.CompanyUser;
import com.softmeasures.eventezlyadminplus.models.users.DeptUser;
import com.softmeasures.eventezlyadminplus.models.users.GeneralCompanyUser;
import com.softmeasures.eventezlyadminplus.models.users.TownshipUser;
import com.softmeasures.eventezlyadminplus.models.users.UserSession;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {
    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/chat_instant_messaging_logs")
    Call<Object> addUserChat(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/chat_instant_messaging_logs")
    Call<Object> updateUserChat(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/chat_instant_messaging_definitions")
    Call<Object> updateUserChatDefination(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/chat_instant_messaging_settings")
    Call<Object> updateUserChatSettings(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/chat_instant_messaging_settings")
    Call<Object> addUserChatSettings(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
    })
    @GET("new_pzly02live7/_table/chat_instant_messaging_settings")
    Call<ChatConfSettings> getUserChatSetting(@Query("filter") String userid);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/group_settings")
    Call<Object> updateGroupSettings(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/group_settings")
    Call<Object> addGroupSettings(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
    })
    @GET("new_pzly02live7/_table/group_settings")
    Call<GroupConfSettings> getGroupSetting(@Query("filter") String userid);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
    })
    @GET("new_pzly02live7/_table/chat_instant_messaging_logs")
    Call<ChatLogs> getUserChat(@Query("filter") String userid);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
    })
    @GET("new_pzly02live7/_table/township_users")
    Call<TownshipUser> getTownshipUsers();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
    })
    @GET("new_pzly02live7/_table/company_users")
    Call<CompanyUser> getCompanyUsers();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
    })
    @GET("new_pzly02live7/_table/general_company_users")
    Call<GeneralCompanyUser> getGeneralCompanyUsers();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
    })
    @GET("new_pzly02live7/_table/dept_users")
    Call<DeptUser> getDeptUsers();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("user/session")
    Call<UserSession> loginUser(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
    })
    @GET("new_pzly02live7/_table/township_users")
    Call<TownshipUser> getTownshipUsers(@Query("filter") String userid);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("user/register?login=false")
    Call<Object> registerUser(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/user_profile")
    Call<Object> addUserProfile(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/menu_config_global?filter=app_id=22&order_by=order_id")
    Call<Object> fetchMenuConfigGlobal();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/parked_cars")
    Call<Object> getParkedCars(@Query("filter") String query);


    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/{path}")
    Call<Object> getDataByTable(@Path("path") String path);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/{path}")
    Call<Object> getDataByTable(@Path("path") String path,
                                @Query("filter") String query);


    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_proc/{path}")
    Call<Object> getDataByProc(@Path("path") String path,
                               @Query("in_lat") double lat,
                               @Query("in_lng") double lang);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/manage_locations")
    Call<Object> getManagedLocation(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/manage_locations")
    Call<Object> getManagedLocation();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/event_definitions")
    Call<Object> getEventDefination();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/event_definitions")
    Call<Object> getEventDefinationByFilter(@Query("filter") String Query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_proc/{path}")
    Call<Object> findReserveParkingPartnerNearBy(@Path("path") String path);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_proc/find_nearby_events")
    Call<Object> findNearByEvents(@Query("in_lat") Double lat,
                                  @Query("in_lng") Double lang);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_proc/find_nearby_events")
    Call<Object> getEventParkingRules(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/offered_cities")
    Call<Object> getCityList();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/event_registration")
    Call<Object> registeredEvent(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/user_wallet?order=date_time%20DESC")
    Call<Object> getWalletBalance();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/user_wallet")
    Call<Object> upateWalletBalanced(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/event_regn_user_profile")
    Call<Object> addRegisteredEventUserProfile(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/event_registration")
    Call<Object> getUserRegisteredEvent(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/event_regn_user_profile")
    Call<Object> getRegisteredEventUserProfile(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/event_attendance")
    Call<Object> addUserEventAttandance(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_proc/find_other_parking_partners_nearby")
    Call<Object> getOtherParkingPartnerNearBy(@Query("in_lat") double lat,
                                              @Query("in_lng") double lang);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_proc/find_other_parking_partners")
    Call<Object> getOtherParkingPartner();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_proc/find_township_parking_partners_nearby")
    Call<Object> getTownshipParkingPartnerNearBy(@Query("in_lat") double lat,
                                                 @Query("in_lng") double lang);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/google_parking_partners")
    Call<Object> getGoogleParkingPartners();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/google_parking_partners")
    Call<Object> getGoogleParkingPartners(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/other_parking_partners")
    Call<Object> getOtherParkingPartners(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/user_locations")
    Call<Object> addUserLocation(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/user_locations")
    Call<Object> updateUserLocation(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @DELETE("new_pzly02live7/_table/user_locations")
    Call<Object> deleteUserLocation(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/user_vehicles")
    Call<Object> addUserVehicle(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/user_vehicles")
    Call<Object> updateUserVehicle(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @DELETE("new_pzly02live7/_table/user_vehicles")
    Call<Object> deleteUserVehicle(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/user_vehicles")
    Call<Object> getUserVehicle(@Query("filter") String id);


    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/user_wallet?order=id DESC")
//http://34.226.69.26/api/v2/new_pzly02live7/_table/user_wallet?order=id DESC&user_id=187
    Call<Object> getUserWallet();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/user_wallet")
    Call<Object> getUserWalletBalance(@Query("filter") String id);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/user_wallet")
    Call<Object> addUserWallet(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/service_charges")
    Call<Object> getServiceCharges(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/event_definitions")
    Call<Object> getEventDefination(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/township_parking_partners")
    Call<Object> getTownshipParkinPartners(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/township_parking_partners")
    Call<Object> getTownshipParkinPartners();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/event_session_definitions")
    Call<Object> getEventSessionDefinitions();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/event_session_definitions")
    Call<Object> getEventSessionDefinitions(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/event_type")
    Call<Object> getEventType();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/townships_manager")
    Call<Object> getTownshipManager();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/commercial_manager")
    Call<Object> getCommercialManager();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/other_private_manager")
    Call<Object> getOtherPrivateManager();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("user/password")
    Call<Object> forgotPassword(@Query("reset") boolean flag,
                                @Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("user/password")
    Call<Object> changePassword(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/parking_permits_by_manager_type_view")
    Call<Object> getParkingPermitByMangerTypeView();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/parking_permits")
    Call<Object> getParkingPermit(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/locations_permit")
    Call<Object> getLocationPermit(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/permit_subscription")
    Call<Object> addPermitSubscriptiont(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
    })
    @GET("new_pzly02live7/_table/user_profile")
    Call<Object> getUserProfile(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/user_profile")
    Call<Object> updateUserProfile(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/user_profile")
    Call<Object> updateUserProfileToken(@Query("filter") String id,
                                        @Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/permit_subscription")
    Call<Object> updatePermitSubcription(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/permit_subscription_archive")
    Call<Object> addPermitSubcriptionArchive(@Body JsonObject jsonObject);


    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/user_wallet")
    Call<Object> updateUserWallet(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/communications_thread")
    Call<Object> addCommunicationsThread(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/communications_thread")
    Call<Object> getCommunicationsThread(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/permit_subscription")
    Call<Object> getPermitSubscription(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/permit_subscription_archive")
    Call<Object> updatePermitSubcriptionArchive(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/event_reserved_parkings")
    Call<Object> getEventReservedParking(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/event_reserved_parkings")
    Call<Object> getEventReservedParking();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/parked_cars")
    Call<Object> updateParkedCars(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/event_reserved_parkings")
    Call<Object> addEventReservedParking(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/reserved_parkings")
    Call<Object> getReservedParking(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/reserved_parkings")
    Call<Object> getReservedParking();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/reserved_parkings")
    Call<Object> updateReservedParking(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/permit_subscription_by_type_view")
    Call<Object> getPermitSubscriptionByTypeView(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/country_states")
    Call<Object> getCountryStateList();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/township_parking_rules")
    Call<Object> getTownshipParkingRules();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/township_parking_rules")
    Call<Object> getTownshipParkingRules(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/permit_subscr_loc_by_user_by_other_view")
    Call<Object> getPermitSubscrLocByUserByOtherView(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/permit_subscr_loc_by_user_by_comm_view")
    Call<Object> getPermitSubscrLocByUserByCommView(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/permit_subscr_loc_by_user_by_twp_view")
    Call<Object> getPermitSubscrLocByUserByTwpView(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/permit_subscr_loc_user_by_permit_view")
    Call<Object> getPermitSubscrLocUserByPermitView(@Query("filter") String query);


    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/township_parking_partners")
    Call<Object> getTownshipParkinPartnersAll();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/permit_subscription_by_all_view")
    Call<Object> getPermitSubscriptionByAllView(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/parked_cars")
    Call<Object> addParkedCar(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/location_lot")
    Call<Object> addLocationLot(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @POST("new_pzly02live7/_proc/managed_lot_status?&order=(lot_row%20ASC%2C%20lot_number%20ASC)")
    Call<Object> getManagedLotStatus(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @POST("new_pzly02live7/_proc/managed_commercial_lot_status?&order=(lot_row%20ASC%2C%20lot_number%20ASC)")
    Call<Object> getManagedCommercialLotStatus(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @POST("new_pzly02live7/_proc/managed_other_lot_status?&order=(lot_row%20ASC%2C%20lot_number%20ASC)")
    Call<Object> getManagedOtherLotStatus(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_table/reserved_parkings")
    Call<Object> addReservedParking(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/location_lot")
    Call<Object> updateLocationLot(@Body JsonObject jsonObject);


    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/location_commercial_lot")
    Call<Object> updateLocationCommercialLot(@Body JsonObject jsonObject);


    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/location_other_lot")
    Call<Object> updateLocationOtherLot(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/subscriptions")
    Call<Object> getSubscription(@Query("filter") String str);


    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/township_parking_rules")
    Call<Object> getEventParkingRules();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/other_parking_rules")
    Call<Object> getOtherParkingRules();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/other_parking_rules")
    Call<Object> getOtherParkingRules(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/google_parking_rules")
    Call<Object> getGoogleParkingRules();

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_proc/user_parking_violations")
    Call<Object> addUserParkingViolations(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_proc/township_parking_violations")
    Call<Object> addTownshipParkingViolations(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_proc/inspector_parking_violations")
    Call<Object> addInspectorParkingViolations(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/parking_violations")
    Call<Object> getParkingViolation(@Query("filter") String str);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/parking_violations")
    Call<Object> updateParkingViolation(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_proc/find_parking_nearby")
    Call<Object> addFindParkingNearby(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/location_lot")
    Call<Object> getLocationLot(@Query("filter") String query);


   /* @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/user_profile")
    Call<Object> updateUserToken(@Body JsonObject jsonObject);*/

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/group_member_contact_info")
    Call<Object> updateMemberToken(@Query("filter") String query,
                                   @Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @POST("new_pzly02live7/_proc/get_total_hours_parked_today")
    Call<Object> getTotalHoursParkedToday(@Body JsonObject jsonObject);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/group_memberships")
    Call<Object> getGroupMemberships(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/group_member_contact_info")
    Call<Object> getGroupMemberContectInfo(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/event_invitations")
    Call<EventInvitations> getEventInvitation(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/event_invitations")
    Call<EventInvitations> fetchInvitation(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41"
    })
    @GET("new_pzly02live7/_table/event_notifications")
    Call<EventNotifications> fetchNotifications(@Query("filter") String query);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/event_notifications")
    Call<Object> updateNotificationStatus(@Body JsonObject json);

    @Headers({
            "X-DreamFactory-Application-Name: parkezly",
            "X-DreamFactory-Api-Key: bd4290ad583ae46c39ab478e34d6b4be8e996b215417b657859c54eb7311bc41",
            "Content-Type:application/json"
    })
    @PUT("new_pzly02live7/_table/event_invitations")
    Call<Object> updateInvitations(@Body JsonObject json);
}