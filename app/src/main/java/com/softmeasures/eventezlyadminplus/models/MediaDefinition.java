package com.softmeasures.eventezlyadminplus.models;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class MediaDefinition {
//    private int id;
//    private String date_time, media_platform, media_domain_url, media_app_name,
//            media_app_id, media_app_key, media_event_link;
//    private boolean active;
//
//    private String linkType;
//    private String youtubeStreamUrl;
//    private String youtubeStreamTitle;
//    private String zoomSpecialUrl, zoomMeetingId, zoomPhone, zoomPassword;
//    private String googleMeetUrl;
//    private String googleMeetPhoneNumber;
//    private String googleMeetPassword;
//    private String googleClasstroomCode;
//    private String skypeStreamUrl;
//    private String facebookServerUrl, facebookStreamKey;
//    private String pdfPassword;
//    private String pdfFilePath;
//    private String pptFilePath;
//    private String filePath;
//    private String teleConfCountryCode, teleConfMeetingId, teleConfMeetingPassword;
//    private ArrayList<String> teleConfNumbers;
//    private String audioVideoType;
//    private String audioVideoPath;
//
//    public String getLinkType() {
//        return linkType;
//    }
//
//    public void setLinkType(String linkType) {
//        this.linkType = linkType;
//    }
//
//    public String getAudioVideoPath() {
//        return audioVideoPath;
//    }
//
//    public void setAudioVideoPath(String audioVideoPath) {
//        this.audioVideoPath = audioVideoPath;
//    }
//
//    public String getAudioVideoType() {
//        return audioVideoType;
//    }
//
//    public void setAudioVideoType(String audioVideoType) {
//        this.audioVideoType = audioVideoType;
//    }
//
//    public String getTeleConfCountryCode() {
//        return teleConfCountryCode;
//    }
//
//    public void setTeleConfCountryCode(String teleConfCountryCode) {
//        this.teleConfCountryCode = teleConfCountryCode;
//    }
//
//    public String getTeleConfMeetingId() {
//        return teleConfMeetingId;
//    }
//
//    public void setTeleConfMeetingId(String teleConfMeetingId) {
//        this.teleConfMeetingId = teleConfMeetingId;
//    }
//
//    public String getTeleConfMeetingPassword() {
//        return teleConfMeetingPassword;
//    }
//
//    public void setTeleConfMeetingPassword(String teleConfMeetingPassword) {
//        this.teleConfMeetingPassword = teleConfMeetingPassword;
//    }
//
//    public ArrayList<String> getTeleConfNumbers() {
//        return teleConfNumbers;
//    }
//
//    public void setTeleConfNumbers(ArrayList<String> teleConfNumbers) {
//        this.teleConfNumbers = teleConfNumbers;
//    }
//
//    public String getPdfFilePath() {
//        return pdfFilePath;
//    }
//
//    public void setPdfFilePath(String pdfFilePath) {
//        this.pdfFilePath = pdfFilePath;
//    }
//
//    public String getPptFilePath() {
//        return pptFilePath;
//    }
//
//    public void setPptFilePath(String pptFilePath) {
//        this.pptFilePath = pptFilePath;
//    }
//
//    public String getFilePath() {
//        return filePath;
//    }
//
//    public void setFilePath(String filePath) {
//        this.filePath = filePath;
//    }
//
//    public String getPdfPassword() {
//        return pdfPassword;
//    }
//
//    public void setPdfPassword(String pdfPassword) {
//        this.pdfPassword = pdfPassword;
//    }
//
//    public String getGoogleMeetPhoneNumber() {
//        return googleMeetPhoneNumber;
//    }
//
//    public void setGoogleMeetPhoneNumber(String googleMeetPhoneNumber) {
//        this.googleMeetPhoneNumber = googleMeetPhoneNumber;
//    }
//
//    public String getGoogleMeetPassword() {
//        return googleMeetPassword;
//    }
//
//    public void setGoogleMeetPassword(String googleMeetPassword) {
//        this.googleMeetPassword = googleMeetPassword;
//    }
//
//    public String getYoutubeStreamUrl() {
//        return youtubeStreamUrl;
//    }
//
//    public void setYoutubeStreamUrl(String youtubeStreamUrl) {
//        this.youtubeStreamUrl = youtubeStreamUrl;
//    }
//
//    public String getYoutubeStreamTitle() {
//        return youtubeStreamTitle;
//    }
//
//    public void setYoutubeStreamTitle(String youtubeStreamTitle) {
//        this.youtubeStreamTitle = youtubeStreamTitle;
//    }
//
//    public String getZoomSpecialUrl() {
//        return zoomSpecialUrl;
//    }
//
//    public void setZoomSpecialUrl(String zoomSpecialUrl) {
//        this.zoomSpecialUrl = zoomSpecialUrl;
//    }
//
//    public String getZoomMeetingId() {
//        return zoomMeetingId;
//    }
//
//    public void setZoomMeetingId(String zoomMeetingId) {
//        this.zoomMeetingId = zoomMeetingId;
//    }
//
//    public String getZoomPhone() {
//        return zoomPhone;
//    }
//
//    public void setZoomPhone(String zoomPhone) {
//        this.zoomPhone = zoomPhone;
//    }
//
//    public String getZoomPassword() {
//        return zoomPassword;
//    }
//
//    public void setZoomPassword(String zoomPassword) {
//        this.zoomPassword = zoomPassword;
//    }
//
//    public String getGoogleMeetUrl() {
//        return googleMeetUrl;
//    }
//
//    public void setGoogleMeetUrl(String googleMeetUrl) {
//        this.googleMeetUrl = googleMeetUrl;
//    }
//
//    public String getGoogleClasstroomCode() {
//        return googleClasstroomCode;
//    }
//
//    public void setGoogleClasstroomCode(String googleClasstroomCode) {
//        this.googleClasstroomCode = googleClasstroomCode;
//    }
//
//    public String getSkypeStreamUrl() {
//        return skypeStreamUrl;
//    }
//
//    public void setSkypeStreamUrl(String skypeStreamUrl) {
//        this.skypeStreamUrl = skypeStreamUrl;
//    }
//
//    public String getFacebookServerUrl() {
//        return facebookServerUrl;
//    }
//
//    public void setFacebookServerUrl(String facebookServerUrl) {
//        this.facebookServerUrl = facebookServerUrl;
//    }
//
//    public String getFacebookStreamKey() {
//        return facebookStreamKey;
//    }
//
//    public void setFacebookStreamKey(String facebookStreamKey) {
//        this.facebookStreamKey = facebookStreamKey;
//    }
//
//    public String getMedia_event_link() {
//        return media_event_link;
//    }
//
//    public void setMedia_event_link(String media_event_link) {
//        this.media_event_link = media_event_link;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getDate_time() {
//        return date_time;
//    }
//
//    public void setDate_time(String date_time) {
//        this.date_time = date_time;
//    }
//
//    public String getMedia_platform() {
//        return media_platform;
//    }
//
//    public void setMedia_platform(String media_platform) {
//        this.media_platform = media_platform;
//    }
//
//    public String getMedia_domain_url() {
//        return media_domain_url;
//    }
//
//    public void setMedia_domain_url(String media_domain_url) {
//        this.media_domain_url = media_domain_url;
//    }
//
//    public String getMedia_app_name() {
//        return media_app_name;
//    }
//
//    public void setMedia_app_name(String media_app_name) {
//        this.media_app_name = media_app_name;
//    }
//
//    public String getMedia_app_id() {
//        return media_app_id;
//    }
//
//    public void setMedia_app_id(String media_app_id) {
//        this.media_app_id = media_app_id;
//    }
//
//    public String getMedia_app_key() {
//        return media_app_key;
//    }
//
//    public void setMedia_app_key(String media_app_key) {
//        this.media_app_key = media_app_key;
//    }
//
//    public boolean isActive() {
//        return active;
//    }
//
//    public void setActive(boolean active) {
//        this.active = active;
//    }
//
//    @NonNull
//    @Override
//    public String toString() {
//        return this.media_platform;
//    }
//
//    @Override
//    public boolean equals(@Nullable Object obj) {
//        if (obj instanceof MediaDefinition) {
//            return ((MediaDefinition) obj).getMedia_platform().equalsIgnoreCase(this.media_platform);
//        }
//        return super.equals(obj);
//    }
private int id;
    private String date_time, media_platform, media_domain_url, media_app_name,
            media_app_id, media_app_key, media_event_link;
    private boolean active;

    private String linkType;
    private String youtubeStreamUrl, youtubeStreamTitle;
    private String zoomSpecialUrl, zoomMeetingId, zoomPhone, zoomPassword, zoomMeetingRoomOption;
    private String googleMeetUrl, googleMeetPhoneNumber, googleMeetPassword;
    private String googleClasstroomCode;
    private String skypeStreamUrl;
    private String facebookServerUrl, facebookStreamKey;
    private String pdfPassword;
    private String pdfFilePath;
    private String pptFilePath;
    private String filePath;
    private String teleConfCountryCode, teleConfMeetingId, teleConfMeetingPassword;
    private ArrayList<String> teleConfNumbers; //= new ArrayList<>();
    private String audioVideoType;
    private String audioVideoPath;
    private String media_short_description;
    private String media_page_pin;
    private String icon_media_type_link;

    private String meeting_id, meeting_type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getMedia_platform() {
        return media_platform;
    }

    public void setMedia_platform(String media_platform) {
        this.media_platform = media_platform;
    }

    public String getMedia_domain_url() {
        return media_domain_url;
    }

    public void setMedia_domain_url(String media_domain_url) {
        this.media_domain_url = media_domain_url;
    }

    public String getMedia_app_name() {
        return media_app_name;
    }

    public void setMedia_app_name(String media_app_name) {
        this.media_app_name = media_app_name;
    }

    public String getMedia_app_id() {
        return media_app_id;
    }

    public void setMedia_app_id(String media_app_id) {
        this.media_app_id = media_app_id;
    }

    public String getMedia_app_key() {
        return media_app_key;
    }

    public void setMedia_app_key(String media_app_key) {
        this.media_app_key = media_app_key;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getIcon_media_type_link() {
        return icon_media_type_link;
    }

    public void setIcon_media_type_link(String icon_media_type_link) {
        this.icon_media_type_link = icon_media_type_link;
    }

    @NonNull
    @Override
    public String toString() {
        return this.media_platform;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof MediaDefinition) {
            return ((MediaDefinition) obj).getMedia_platform().equalsIgnoreCase(this.media_platform);
        }
        return super.equals(obj);
    }

    public String getPdfFilePath() {
        return pdfFilePath;
    }

    public void setPdfFilePath(String pdfFilePath) {
        this.pdfFilePath = pdfFilePath;
    }

    public String getPptFilePath() {
        return pptFilePath;
    }

    public void setPptFilePath(String pptFilePath) {
        this.pptFilePath = pptFilePath;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getPdfPassword() {
        return pdfPassword;
    }

    public void setPdfPassword(String pdfPassword) {
        this.pdfPassword = pdfPassword;
    }


    public String getMedia_page_pin() {
        return media_page_pin;
    }

    public void setMedia_page_pin(String media_page_pin) {
        this.media_page_pin = media_page_pin;
    }

    public String getMedia_short_description() {
        return media_short_description;
    }

    public void setMedia_short_description(String media_short_description) {
        this.media_short_description = media_short_description;
    }

    public String getAudioVideoType() {
        return audioVideoType;
    }

    public void setAudioVideoType(String audioVideoType) {
        this.audioVideoType = audioVideoType;
    }

    public String getAudioVideoPath() {
        return audioVideoPath;
    }

    public void setAudioVideoPath(String audioVideoPath) {
        this.audioVideoPath = audioVideoPath;
    }


    public String getGoogleMeetPhoneNumber() {
        return googleMeetPhoneNumber;
    }

    public void setGoogleMeetPhoneNumber(String googleMeetPhoneNumber) {
        this.googleMeetPhoneNumber = googleMeetPhoneNumber;
    }

    public String getGoogleMeetPassword() {
        return googleMeetPassword;
    }

    public void setGoogleMeetPassword(String googleMeetPassword) {
        this.googleMeetPassword = googleMeetPassword;
    }

    public String getTeleConfCountryCode() {
        return teleConfCountryCode;
    }

    public void setTeleConfCountryCode(String teleConfCountryCode) {
        this.teleConfCountryCode = teleConfCountryCode;
    }

    public String getTeleConfMeetingId() {
        return teleConfMeetingId;
    }

    public void setTeleConfMeetingId(String teleConfMeetingId) {
        this.teleConfMeetingId = teleConfMeetingId;
    }

    public String getTeleConfMeetingPassword() {
        return teleConfMeetingPassword;
    }

    public void setTeleConfMeetingPassword(String teleConfMeetingPassword) {
        this.teleConfMeetingPassword = teleConfMeetingPassword;
    }

    public ArrayList<String> getTeleConfNumbers() {
        return teleConfNumbers;
    }

    public void setTeleConfNumbers(ArrayList<String> teleConfNumbers) {
        this.teleConfNumbers = teleConfNumbers;
    }

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    public String getYoutubeStreamUrl() {
        return youtubeStreamUrl;
    }

    public void setYoutubeStreamUrl(String youtubeStreamUrl) {
        this.youtubeStreamUrl = youtubeStreamUrl;
    }

    public String getYoutubeStreamTitle() {
        return youtubeStreamTitle;
    }

    public void setYoutubeStreamTitle(String youtubeStreamTitle) {
        this.youtubeStreamTitle = youtubeStreamTitle;
    }

    public String getZoomSpecialUrl() {
        return zoomSpecialUrl;
    }

    public void setZoomSpecialUrl(String zoomSpecialUrl) {
        this.zoomSpecialUrl = zoomSpecialUrl;
    }

    public String getZoomMeetingId() {
        return zoomMeetingId;
    }

    public void setZoomMeetingId(String zoomMeetingId) {
        this.zoomMeetingId = zoomMeetingId;
    }

    public String getZoomPhone() {
        return zoomPhone;
    }

    public void setZoomPhone(String zoomPhone) {
        this.zoomPhone = zoomPhone;
    }

    public String getZoomPassword() {
        return zoomPassword;
    }

    public void setZoomPassword(String zoomPassword) {
        this.zoomPassword = zoomPassword;
    }

    public String getGoogleMeetUrl() {
        return googleMeetUrl;
    }

    public void setGoogleMeetUrl(String googleMeetUrl) {
        this.googleMeetUrl = googleMeetUrl;
    }

    public String getGoogleClasstroomCode() {
        return googleClasstroomCode;
    }

    public void setGoogleClasstroomCode(String googleClasstroomCode) {
        this.googleClasstroomCode = googleClasstroomCode;
    }

    public String getSkypeStreamUrl() {
        return skypeStreamUrl;
    }

    public void setSkypeStreamUrl(String skypeStreamUrl) {
        this.skypeStreamUrl = skypeStreamUrl;
    }

    public String getFacebookServerUrl() {
        return facebookServerUrl;
    }

    public void setFacebookServerUrl(String facebookServerUrl) {
        this.facebookServerUrl = facebookServerUrl;
    }

    public String getFacebookStreamKey() {
        return facebookStreamKey;
    }

    public void setFacebookStreamKey(String facebookStreamKey) {
        this.facebookStreamKey = facebookStreamKey;
    }

    public String getMedia_event_link() {
        return media_event_link;
    }

    public void setMedia_event_link(String media_event_link) {
        this.media_event_link = media_event_link;
    }

    public String getZoomMeetingRoomOption() {
        return zoomMeetingRoomOption;
    }

    public void setZoomMeetingRoomOption(String zoomMeetingRoomOption) {
        this.zoomMeetingRoomOption = zoomMeetingRoomOption;
    }

    public String getMeeting_type() {
        return meeting_type;
    }

    public void setMeeting_type(String meeting_type) {
        this.meeting_type = meeting_type;
    }

    public String getMeeting_id() {
        return meeting_id;
    }

    public void setMeeting_id(String meeting_id) {
        this.meeting_id = meeting_id;
    }
}

