package com.softmeasures.eventezlyadminplus.models;

import java.util.ArrayList;

public class LocationLotSub {
    private ArrayList<LocationLot> locationLots = new ArrayList<>();

    public ArrayList<LocationLot> getLocationLots() {
        return locationLots;
    }

    public void setLocationLots(ArrayList<LocationLot> locationLots) {
        this.locationLots = locationLots;
    }
}
