package com.softmeasures.eventezlyadminplus.models;

public class LocationLot {
    private int id, location_id;
    private String date_time, township_code, location_code, location_name, lot_row,
            lot_number, occupied, plate_no, plate_state, lot_id,
            marker, isparked;
    private boolean lot_reservable, expired, premium_lot, special_need_handi_lot, lot_reservable_only,
            is_reserved;
    private String company_type_id, manager_type_id, company_id;
    private String entry_date_time, exit_date_time, expiry_time, parking_type, parking_status;

    public boolean isIs_reserved() {
        return is_reserved;
    }

    public void setIs_reserved(boolean is_reserved) {
        this.is_reserved = is_reserved;
    }

    public boolean isPremium_lot() {
        return premium_lot;
    }

    public void setPremium_lot(boolean premium_lot) {
        this.premium_lot = premium_lot;
    }

    public boolean isSpecial_need_handi_lot() {
        return special_need_handi_lot;
    }

    public void setSpecial_need_handi_lot(boolean special_need_handi_lot) {
        this.special_need_handi_lot = special_need_handi_lot;
    }

    public boolean isLot_reservable_only() {
        return lot_reservable_only;
    }

    public void setLot_reservable_only(boolean lot_reservable_only) {
        this.lot_reservable_only = lot_reservable_only;
    }

    public String getEntry_date_time() {
        return entry_date_time;
    }

    public void setEntry_date_time(String entry_date_time) {
        this.entry_date_time = entry_date_time;
    }

    public String getExit_date_time() {
        return exit_date_time;
    }

    public void setExit_date_time(String exit_date_time) {
        this.exit_date_time = exit_date_time;
    }

    public String getExpiry_time() {
        return expiry_time;
    }

    public void setExpiry_time(String expiry_time) {
        this.expiry_time = expiry_time;
    }

    public String getParking_type() {
        return parking_type;
    }

    public void setParking_type(String parking_type) {
        this.parking_type = parking_type;
    }

    public String getParking_status() {
        return parking_status;
    }

    public void setParking_status(String parking_status) {
        this.parking_status = parking_status;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    public LocationLot() {
    }

    public LocationLot(int id) {
        this.id = id;
    }

    public String getMarker() {
        return marker;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }

    public String getIsparked() {
        return isparked;
    }

    public void setIsparked(String isparked) {
        this.isparked = isparked;
    }

    public String getCompany_type_id() {
        return company_type_id;
    }

    public void setCompany_type_id(String company_type_id) {
        this.company_type_id = company_type_id;
    }

    public String getManager_type_id() {
        return manager_type_id;
    }

    public void setManager_type_id(String manager_type_id) {
        this.manager_type_id = manager_type_id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getTownship_code() {
        return township_code;
    }

    public void setTownship_code(String township_code) {
        this.township_code = township_code;
    }

    public String getLocation_code() {
        return location_code;
    }

    public void setLocation_code(String location_code) {
        this.location_code = location_code;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getLot_row() {
        return lot_row;
    }

    public void setLot_row(String lot_row) {
        this.lot_row = lot_row;
    }

    public String getLot_number() {
        return lot_number;
    }

    public void setLot_number(String lot_number) {
        this.lot_number = lot_number;
    }

    public String getOccupied() {
        return occupied;
    }

    public void setOccupied(String occupied) {
        this.occupied = occupied;
    }

    public String getPlate_no() {
        return plate_no;
    }

    public void setPlate_no(String plate_no) {
        this.plate_no = plate_no;
    }

    public String getPlate_state() {
        return plate_state;
    }

    public void setPlate_state(String plate_state) {
        this.plate_state = plate_state;
    }

    public String getLot_id() {
        return lot_id;
    }

    public void setLot_id(String lot_id) {
        this.lot_id = lot_id;
    }

    public boolean isLot_reservable() {
        return lot_reservable;
    }

    public void setLot_reservable(boolean lot_reservable) {
        this.lot_reservable = lot_reservable;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof LocationLot)
            return ((LocationLot) obj).getId() == this.id;
        return super.equals(obj);
    }
}
