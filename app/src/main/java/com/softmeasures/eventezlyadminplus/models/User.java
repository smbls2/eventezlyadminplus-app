package com.softmeasures.eventezlyadminplus.models;

public class User {
    private String firstName, lastName, email, mobile, age, gender, address;
    private String category, fee;
    private int event_registration_id;
    private String event_fee_selected_type, event_fee_selected;

    public String getEvent_fee_selected_type() {
        return event_fee_selected_type;
    }

    public void setEvent_fee_selected_type(String event_fee_selected_type) {
        this.event_fee_selected_type = event_fee_selected_type;
    }

    public String getEvent_fee_selected() {
        return event_fee_selected;
    }

    public void setEvent_fee_selected(String event_fee_selected) {
        this.event_fee_selected = event_fee_selected;
    }

    public int getEvent_registration_id() {
        return event_registration_id;
    }

    public void setEvent_registration_id(int event_registration_id) {
        this.event_registration_id = event_registration_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
