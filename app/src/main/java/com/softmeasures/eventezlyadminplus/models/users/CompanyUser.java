package com.softmeasures.eventezlyadminplus.models.users;

import java.util.List;

public class CompanyUser {


    private List<CompanyUserData> resource = null;

    public List<CompanyUserData> getResource() {
        return resource;
    }

    public void setResource(List<CompanyUserData> resource) {
        this.resource = resource;
    }

    public class CompanyUserData {

       private int id;
       private String dateTime;
       private int userId;
       private String userName;
       private String email;
       private int companyId;
       private String companyCode;
       private String companyName;
       private boolean companyTypeId;
       private String companyType;
       private int managerTypeId;
       private String managerType;
       private String profileName;
       private String status;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDateTime() {
            return dateTime;
        }

        public void setDateTime(String dateTime) {
            this.dateTime = dateTime;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public int getCompanyId() {
            return companyId;
        }

        public void setCompanyId(int companyId) {
            this.companyId = companyId;
        }

        public String getCompanyCode() {
            return companyCode;
        }

        public void setCompanyCode(String companyCode) {
            this.companyCode = companyCode;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public boolean getCompanyTypeId() {
            return companyTypeId;
        }

        public void setCompanyTypeId(boolean companyTypeId) {
            this.companyTypeId = companyTypeId;
        }

        public String getCompanyType() {
            return companyType;
        }

        public void setCompanyType(String companyType) {
            this.companyType = companyType;
        }

        public int getManagerTypeId() {
            return managerTypeId;
        }

        public void setManagerTypeId(int managerTypeId) {
            this.managerTypeId = managerTypeId;
        }

        public String getManagerType() {
            return managerType;
        }

        public void setManagerType(String managerType) {
            this.managerType = managerType;
        }

        public String getProfileName() {
            return profileName;
        }

        public void setProfileName(String profileName) {
            this.profileName = profileName;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    }
}
