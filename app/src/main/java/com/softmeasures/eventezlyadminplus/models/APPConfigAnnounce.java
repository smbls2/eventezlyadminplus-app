package com.softmeasures.eventezlyadminplus.models;

public class APPConfigAnnounce {
    private int id;
    private String dateAdded;
    private int appId;
    private String appName;
    private String appTable;
    private String appField1;
    private boolean appField1Value;
    private String appField2;
    private boolean appField2Value;
    private String appFieldAlertMessage;
    private String appFieldAlertProceedLink;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppTable() {
        return appTable;
    }

    public void setAppTable(String appTable) {
        this.appTable = appTable;
    }

    public String getAppField1() {
        return appField1;
    }

    public void setAppField1(String appField1) {
        this.appField1 = appField1;
    }

    public boolean getAppField1Value() {
        return appField1Value;
    }

    public void setAppField1Value(boolean appField1Value) {
        this.appField1Value = appField1Value;
    }

    public String getAppField2() {
        return appField2;
    }

    public void setAppField2(String appField2) {
        this.appField2 = appField2;
    }

    public boolean getAppField2Value() {
        return appField2Value;
    }

    public void setAppField2Value(boolean appField2Value) {
        this.appField2Value = appField2Value;
    }

    public String getAppFieldAlertMessage() {
        return appFieldAlertMessage;
    }

    public void setAppFieldAlertMessage(String appFieldAlertMessage) {
        this.appFieldAlertMessage = appFieldAlertMessage;
    }

    public String getAppFieldAlertProceedLink() {
        return appFieldAlertProceedLink;
    }

    public void setAppFieldAlertProceedLink(String appFieldAlertProceedLink) {
        this.appFieldAlertProceedLink = appFieldAlertProceedLink;
    }

}
