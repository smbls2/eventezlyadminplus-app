package com.softmeasures.eventezlyadminplus.models;

import android.text.TextUtils;

public class EventType {
    private int id;
    private String event_type, icon_event_type_link;

    public EventType() {
    }

    public EventType(String event_type) {
        this.event_type = event_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getIcon_event_type_link() {
        return icon_event_type_link;
    }

    public void setIcon_event_type_link(String icon_event_type_link) {
        this.icon_event_type_link = icon_event_type_link;
    }

    @Override
    public String toString() {
        return this.event_type;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof EventType) {
            return !TextUtils.isEmpty(((EventType) obj).getEvent_type())
                    && !TextUtils.isEmpty(this.event_type)
                    && ((EventType) obj).getEvent_type().equals(this.event_type);
        } else {
            return super.equals(obj);
        }
    }
}
