package com.softmeasures.eventezlyadminplus.models.call_chat;

public class CallsEventAudioConfLogs {
    int id, event_id, host_user_id, app_id;
    int maximum_users, attended_users, hosted_users, total_users, local_rec_guests_list;
    boolean recording_local, recording_cloud;
    String datetime, event_name, host_user_name, all_hosts_users_list, all_guest_users_list, app_name, meeting_id, password, event_audio_begins_datetime, event_audio_ends_datetime, local_rec_hosts_list, cloud_rec_hosts_list, cloud_rec_guests_list, cloud_rec_location;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public int getHost_user_id() {
        return host_user_id;
    }

    public void setHost_user_id(int host_user_id) {
        this.host_user_id = host_user_id;
    }

    public int getApp_id() {
        return app_id;
    }

    public void setApp_id(int app_id) {
        this.app_id = app_id;
    }

    public int getMaximum_users() {
        return maximum_users;
    }

    public void setMaximum_users(int maximum_users) {
        this.maximum_users = maximum_users;
    }

    public int getAttended_users() {
        return attended_users;
    }

    public void setAttended_users(int attended_users) {
        this.attended_users = attended_users;
    }

    public int getHosted_users() {
        return hosted_users;
    }

    public void setHosted_users(int hosted_users) {
        this.hosted_users = hosted_users;
    }

    public int getTotal_users() {
        return total_users;
    }

    public void setTotal_users(int total_users) {
        this.total_users = total_users;
    }

    public int getLocal_rec_guests_list() {
        return local_rec_guests_list;
    }

    public void setLocal_rec_guests_list(int local_rec_guests_list) {
        this.local_rec_guests_list = local_rec_guests_list;
    }

    public boolean isRecording_local() {
        return recording_local;
    }

    public void setRecording_local(boolean recording_local) {
        this.recording_local = recording_local;
    }

    public boolean isRecording_cloud() {
        return recording_cloud;
    }

    public void setRecording_cloud(boolean recording_cloud) {
        this.recording_cloud = recording_cloud;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getHost_user_name() {
        return host_user_name;
    }

    public void setHost_user_name(String host_user_name) {
        this.host_user_name = host_user_name;
    }

    public String getAll_hosts_users_list() {
        return all_hosts_users_list;
    }

    public void setAll_hosts_users_list(String all_hosts_users_list) {
        this.all_hosts_users_list = all_hosts_users_list;
    }

    public String getAll_guest_users_list() {
        return all_guest_users_list;
    }

    public void setAll_guest_users_list(String all_guest_users_list) {
        this.all_guest_users_list = all_guest_users_list;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public String getMeeting_id() {
        return meeting_id;
    }

    public void setMeeting_id(String meeting_id) {
        this.meeting_id = meeting_id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEvent_audio_begins_datetime() {
        return event_audio_begins_datetime;
    }

    public void setEvent_audio_begins_datetime(String event_audio_begins_datetime) {
        this.event_audio_begins_datetime = event_audio_begins_datetime;
    }

    public String getEvent_audio_ends_datetime() {
        return event_audio_ends_datetime;
    }

    public void setEvent_audio_ends_datetime(String event_audio_ends_datetime) {
        this.event_audio_ends_datetime = event_audio_ends_datetime;
    }

    public String getLocal_rec_hosts_list() {
        return local_rec_hosts_list;
    }

    public void setLocal_rec_hosts_list(String local_rec_hosts_list) {
        this.local_rec_hosts_list = local_rec_hosts_list;
    }

    public String getCloud_rec_hosts_list() {
        return cloud_rec_hosts_list;
    }

    public void setCloud_rec_hosts_list(String cloud_rec_hosts_list) {
        this.cloud_rec_hosts_list = cloud_rec_hosts_list;
    }

    public String getCloud_rec_guests_list() {
        return cloud_rec_guests_list;
    }

    public void setCloud_rec_guests_list(String cloud_rec_guests_list) {
        this.cloud_rec_guests_list = cloud_rec_guests_list;
    }

    public String getCloud_rec_location() {
        return cloud_rec_location;
    }

    public void setCloud_rec_location(String cloud_rec_location) {
        this.cloud_rec_location = cloud_rec_location;
    }
}
