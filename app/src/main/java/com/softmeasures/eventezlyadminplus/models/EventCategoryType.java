package com.softmeasures.eventezlyadminplus.models;

import android.text.TextUtils;

public class EventCategoryType {
    private int id;
    private String event_category_type, icon_event_category_type_link;

    public EventCategoryType() {}

    public EventCategoryType(String event_category_type) {
        this.event_category_type = event_category_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEvent_category_type() {
        return event_category_type;
    }

    public void setEvent_category_type(String event_category_type) {
        this.event_category_type = event_category_type;
    }

    public String getIcon_event_category_type_link() {
        return icon_event_category_type_link;
    }

    public void setIcon_event_category_type_link(String icon_event_category_type_link) {
        this.icon_event_category_type_link = icon_event_category_type_link;
    }

    @Override
    public String toString() {
        return this.event_category_type;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof EventCategoryType) {
            return !TextUtils.isEmpty(((EventCategoryType) obj).getEvent_category_type())
                    && !TextUtils.isEmpty(this.event_category_type)
                    && ((EventCategoryType) obj).getEvent_category_type().equals(this.event_category_type);
        } else {
            return super.equals(obj);
        }
    }
}