package com.softmeasures.eventezlyadminplus.models.call_chat;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChatConfSettings {
    @SerializedName("resource")
    public List<ChatConfSettingData> resource;

    public List<ChatConfSettingData> getResource() {
        return resource;
    }

    public void setResource(List<ChatConfSettingData> resource) {
        this.resource = resource;
    }

    public static class ChatConfSettingData {

        @SerializedName("id")
        private int id;
        private String datetime;
        private int company_type_id, company_id, user_id, app_id;
        private String company_code, company_name, user_profile_type, app_name;
        private int user_access_level;

        private boolean chat_audio_recording_local, chat_audio_recording_cloud, chat_video_recording_local, chat_video_recording_cloud;
        private String region_name, bucket_name, cloud_rec_dir, local_rec_dir;
        private boolean rec_allowed_host, rec_allowed_guest, attach_live_camera, attach_live_mic, display_attach_doc, display_attach_video, display_attach_audio, display_attach_image;
        private boolean preview_attach_video, preview_attach_audio, preview_attach_image, play_attached_video,
                play_attach_audio, play_attach_image, play_attach_slide, chat_file_attach_local,
                chat_file_attach_cloud, chat_photo_attach_local, chat_photo_attach_cloud;
        private boolean attach_zip, attach_doc, attach_video, attach_audio, attach_image;
        private int cloud_rec_size_session_limit, cloud_rec_size_total_limit, cloud_attach_size_session_limit, cloud_attach_size_total_limit;

        private int maximum_users;
        private String meeting_id_sourcetype;
        private boolean total_time_unlimited;
        private int total_time_limit;
        private boolean session_time_unlimited;
        private int session_time_limit;
        private boolean allow_share, allow_download, allow_copying, allow_forward, local_data, cloud_data;

        public int getUser_access_level() {
            return user_access_level;
        }

        public void setUser_access_level(int user_access_level) {
            this.user_access_level = user_access_level;
        }

        public boolean isChat_audio_recording_local() {
            return chat_audio_recording_local;
        }

        public void setChat_audio_recording_local(boolean chat_audio_recording_local) {
            this.chat_audio_recording_local = chat_audio_recording_local;
        }

        public boolean isChat_audio_recording_cloud() {
            return chat_audio_recording_cloud;
        }

        public void setChat_audio_recording_cloud(boolean chat_audio_recording_cloud) {
            this.chat_audio_recording_cloud = chat_audio_recording_cloud;
        }

        public boolean isChat_video_recording_local() {
            return chat_video_recording_local;
        }

        public void setChat_video_recording_local(boolean chat_video_recording_local) {
            this.chat_video_recording_local = chat_video_recording_local;
        }

        public boolean isChat_video_recording_cloud() {
            return chat_video_recording_cloud;
        }

        public void setChat_video_recording_cloud(boolean chat_video_recording_cloud) {
            this.chat_video_recording_cloud = chat_video_recording_cloud;
        }

        public String getRegion_name() {
            return region_name;
        }

        public void setRegion_name(String region_name) {
            this.region_name = region_name;
        }

        public String getBucket_name() {
            return bucket_name;
        }

        public void setBucket_name(String bucket_name) {
            this.bucket_name = bucket_name;
        }

        public String getCloud_rec_dir() {
            return cloud_rec_dir;
        }

        public void setCloud_rec_dir(String cloud_rec_dir) {
            this.cloud_rec_dir = cloud_rec_dir;
        }

        public String getLocal_rec_dir() {
            return local_rec_dir;
        }

        public void setLocal_rec_dir(String local_rec_dir) {
            this.local_rec_dir = local_rec_dir;
        }

        public boolean isRec_allowed_host() {
            return rec_allowed_host;
        }

        public void setRec_allowed_host(boolean rec_allowed_host) {
            this.rec_allowed_host = rec_allowed_host;
        }

        public boolean isRec_allowed_guest() {
            return rec_allowed_guest;
        }

        public void setRec_allowed_guest(boolean rec_allowed_guest) {
            this.rec_allowed_guest = rec_allowed_guest;
        }

        public boolean isAttach_live_camera() {
            return attach_live_camera;
        }

        public void setAttach_live_camera(boolean attach_live_camera) {
            this.attach_live_camera = attach_live_camera;
        }

        public boolean isAttach_live_mic() {
            return attach_live_mic;
        }

        public void setAttach_live_mic(boolean attach_live_mic) {
            this.attach_live_mic = attach_live_mic;
        }

        public boolean isDisplay_attach_doc() {
            return display_attach_doc;
        }

        public void setDisplay_attach_doc(boolean display_attach_doc) {
            this.display_attach_doc = display_attach_doc;
        }

        public boolean isDisplay_attach_video() {
            return display_attach_video;
        }

        public void setDisplay_attach_video(boolean display_attach_video) {
            this.display_attach_video = display_attach_video;
        }

        public boolean isDisplay_attach_audio() {
            return display_attach_audio;
        }

        public void setDisplay_attach_audio(boolean display_attach_audio) {
            this.display_attach_audio = display_attach_audio;
        }

        public boolean isDisplay_attach_image() {
            return display_attach_image;
        }

        public void setDisplay_attach_image(boolean display_attach_image) {
            this.display_attach_image = display_attach_image;
        }

        public boolean isPreview_attach_video() {
            return preview_attach_video;
        }

        public void setPreview_attach_video(boolean preview_attach_video) {
            this.preview_attach_video = preview_attach_video;
        }

        public boolean isPreview_attach_audio() {
            return preview_attach_audio;
        }

        public void setPreview_attach_audio(boolean preview_attach_audio) {
            this.preview_attach_audio = preview_attach_audio;
        }

        public boolean isPreview_attach_image() {
            return preview_attach_image;
        }

        public void setPreview_attach_image(boolean preview_attach_image) {
            this.preview_attach_image = preview_attach_image;
        }

        public boolean isPlay_attached_video() {
            return play_attached_video;
        }

        public void setPlay_attached_video(boolean play_attached_video) {
            this.play_attached_video = play_attached_video;
        }

        public boolean isPlay_attach_audio() {
            return play_attach_audio;
        }

        public void setPlay_attach_audio(boolean play_attach_audio) {
            this.play_attach_audio = play_attach_audio;
        }

        public boolean isPlay_attach_image() {
            return play_attach_image;
        }

        public void setPlay_attach_image(boolean play_attach_image) {
            this.play_attach_image = play_attach_image;
        }

        public boolean isPlay_attach_slide() {
            return play_attach_slide;
        }

        public void setPlay_attach_slide(boolean play_attach_slide) {
            this.play_attach_slide = play_attach_slide;
        }

        public boolean isChat_file_attach_local() {
            return chat_file_attach_local;
        }

        public void setChat_file_attach_local(boolean chat_file_attach_local) {
            this.chat_file_attach_local = chat_file_attach_local;
        }

        public boolean isChat_file_attach_cloud() {
            return chat_file_attach_cloud;
        }

        public void setChat_file_attach_cloud(boolean chat_file_attach_cloud) {
            this.chat_file_attach_cloud = chat_file_attach_cloud;
        }

        public boolean isChat_photo_attach_local() {
            return chat_photo_attach_local;
        }

        public void setChat_photo_attach_local(boolean chat_photo_attach_local) {
            this.chat_photo_attach_local = chat_photo_attach_local;
        }

        public boolean isChat_photo_attach_cloud() {
            return chat_photo_attach_cloud;
        }

        public void setChat_photo_attach_cloud(boolean chat_photo_attach_cloud) {
            this.chat_photo_attach_cloud = chat_photo_attach_cloud;
        }

        public boolean isAttach_zip() {
            return attach_zip;
        }

        public void setAttach_zip(boolean attach_zip) {
            this.attach_zip = attach_zip;
        }

        public boolean isAttach_doc() {
            return attach_doc;
        }

        public void setAttach_doc(boolean attach_doc) {
            this.attach_doc = attach_doc;
        }

        public boolean isAttach_video() {
            return attach_video;
        }

        public void setAttach_video(boolean attach_video) {
            this.attach_video = attach_video;
        }

        public boolean isAttach_audio() {
            return attach_audio;
        }

        public void setAttach_audio(boolean attach_audio) {
            this.attach_audio = attach_audio;
        }

        public boolean isAttach_image() {
            return attach_image;
        }

        public void setAttach_image(boolean attach_image) {
            this.attach_image = attach_image;
        }

        public int getCloud_rec_size_session_limit() {
            return cloud_rec_size_session_limit;
        }

        public void setCloud_rec_size_session_limit(int cloud_rec_size_session_limit) {
            this.cloud_rec_size_session_limit = cloud_rec_size_session_limit;
        }

        public int getCloud_rec_size_total_limit() {
            return cloud_rec_size_total_limit;
        }

        public void setCloud_rec_size_total_limit(int cloud_rec_size_total_limit) {
            this.cloud_rec_size_total_limit = cloud_rec_size_total_limit;
        }

        public int getCloud_attach_size_session_limit() {
            return cloud_attach_size_session_limit;
        }

        public void setCloud_attach_size_session_limit(int cloud_attach_size_session_limit) {
            this.cloud_attach_size_session_limit = cloud_attach_size_session_limit;
        }

        public int getCloud_attach_size_total_limit() {
            return cloud_attach_size_total_limit;
        }

        public void setCloud_attach_size_total_limit(int cloud_attach_size_total_limit) {
            this.cloud_attach_size_total_limit = cloud_attach_size_total_limit;
        }

        public int getMaximum_users() {
            return maximum_users;
        }

        public void setMaximum_users(int maximum_users) {
            this.maximum_users = maximum_users;
        }

        public String getMeeting_id_sourcetype() {
            return meeting_id_sourcetype;
        }

        public void setMeeting_id_sourcetype(String meeting_id_sourcetype) {
            this.meeting_id_sourcetype = meeting_id_sourcetype;
        }

        public boolean isTotal_time_unlimited() {
            return total_time_unlimited;
        }

        public void setTotal_time_unlimited(boolean total_time_unlimited) {
            this.total_time_unlimited = total_time_unlimited;
        }

        public int getTotal_time_limit() {
            return total_time_limit;
        }

        public void setTotalTime_limit(int total_time_limit) {
            this.total_time_limit = total_time_limit;
        }

        public boolean isSessionTime_unlimited() {
            return session_time_unlimited;
        }

        public void setSessionTime_unlimited(boolean session_time_unlimited) {
            this.session_time_unlimited = session_time_unlimited;
        }

        public int getSessionTime_limit() {
            return session_time_limit;
        }

        public void setSessionTime_limit(int session_time_limit) {
            this.session_time_limit = session_time_limit;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public int getCompany_type_id() {
            return company_type_id;
        }

        public void setCompany_type_id(int company_type_id) {
            this.company_type_id = company_type_id;
        }

        public int getCompany_id() {
            return company_id;
        }

        public void setCompany_id(int company_id) {
            this.company_id = company_id;
        }

        public String getCompany_code() {
            return company_code;
        }

        public void setCompany_code(String company_code) {
            this.company_code = company_code;
        }

        public String getCompany_name() {
            return company_name;
        }

        public void setCompany_name(String company_name) {
            this.company_name = company_name;
        }

        public String getUserProfile_type() {
            return user_profile_type;
        }

        public void setUser_profile_type(String user_profile_type) {
            this.user_profile_type = user_profile_type;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getApp_name() {
            return app_name;
        }

        public void setApp_name(String app_name) {
            this.app_name = app_name;
        }

        public int getApp_id() {
            return app_id;
        }

        public void setApp_id(int app_id) {
            this.app_id = app_id;
        }

        public boolean isAllow_forward() {
            return allow_forward;
        }

        public void setAllow_forward(boolean allow_forward) {
            this.allow_forward = allow_forward;
        }

        public boolean isAllow_copying() {
            return allow_copying;
        }

        public void setAllow_copying(boolean allow_copying) {
            this.allow_copying = allow_copying;
        }

        public boolean isAllow_download() {
            return allow_download;
        }

        public void setAllow_download(boolean allow_download) {
            this.allow_download = allow_download;
        }

        public boolean isAllow_share() {
            return allow_share;
        }

        public void setAllow_share(boolean allow_share) {
            this.allow_share = allow_share;
        }

        public boolean isCloud_data() {
            return cloud_data;
        }

        public void setCloud_data(boolean cloud_data) {
            this.cloud_data = cloud_data;
        }

        public boolean isLocal_data() {
            return local_data;
        }

        public void setLocal_data(boolean local_data) {
            this.local_data = local_data;
        }
    }
}