package com.softmeasures.eventezlyadminplus.models;

import java.util.ArrayList;

public class EventDefinition {
    private int id, manager_type_id, twp_id, company_id, event_id, event_type_id, event_category_type_id;
    private String date_time, manager_type, township_code, township_name, company_code, company_name, company_type,
            event_category_type, event_type, event_name, event_short_description, event_long_description,
            event_link_on_web, event_link_twitter, event_link_whatsapp, event_link_other_media,
            company_logo, event_logo, event_image1, event_image2, event_blob_image,
            event_address, covered_locations, requirements, appl_req_download, cost, year,
            location_address, scheme_type, event_prefix, event_nextnum, event_begins_date_time,
            event_ends_date_time, event_parking_begins_date_time, event_parking_ends_date_time,
            expires_by, event_multi_dates, event_parking_timings, event_event_timings, event_code;

    private boolean regn_reqd, regn_reqd_for_parking, renewable, active,
            event_multi_sessions, event_indiv_session_regn_allowed, event_indiv_session_regn_required,
            event_full_regn_required, regn_needed_approval, regn_user_info_reqd, regn_addl_user_info_reqd;
    private String event_led_by, event_leaders_bio;
    private String event_full_regn_fee, event_parking_fee, event_parking_fee_duration;
    private String location_lat_lng;
    private String lat, lng;
    private double distance;
    private String display_address;
    private String event_full_youth_fee, event_full_child_fee, event_full_student_fee,
            event_full_minister_fee, event_full_clergy_fee, event_full_promo_fee,
            event_full_senior_fee, event_full_staff_fee;
    private String eventCategory;
    private boolean free_event, free_event_parking;
    private String event_link_ytube, event_link_zoom, event_link_googlemeet, event_link_googleclassroom, event_link_facebook;
    private int event_logi_type;
    private String event_regn_limit, web_event_full_regn_fee, web_event_regn_limit, parking_reservation_limit;
    private boolean web_event_location_to_show, free_web_event;
    private ArrayList<String> eventDates = new ArrayList<>();
    public boolean is_parking_allowed;
    private String local_timezone, local_time_event_start;
    private ArrayList<String> eventMultiDates = new ArrayList<>();
    private ArrayList<EventMultiTime> parkingMultiTimes = new ArrayList<>();
    private ArrayList<EventMultiTime> eventMultiTimes = new ArrayList<>();
    private String event_link_array;

    private boolean event_public, event_unlisted, event_tp_sharable, event_invite_only, event_regn_invite_only,
            show_live_regd_only, show_live_invite_only, show_past_regd_only, show_past_invite_only,
            geo_restrict_countries, geo_allow_countres, geo_restrict_states, geo_allow_states, geo_restrict_lat_lng, geo_allow_lat_lng;

    private boolean allow_audio_conf, allow_audio_condition_no, allow_audio_conf_rec, allow_audio_rec_condition, allow_video_conf,
            allow_video_condition_no, allow_video_conf_rec, allow_video_rec_condition, allow_livechat, allow_livechat_condition_no, archive_access;

    public ArrayList<String> getEventMultiDates() {
        return eventMultiDates;
    }

    public void setEventMultiDates(ArrayList<String> eventMultiDates) {
        this.eventMultiDates = eventMultiDates;
    }

    public ArrayList<EventMultiTime> getParkingMultiTimes() {
        return parkingMultiTimes;
    }

    public void setParkingMultiTimes(ArrayList<EventMultiTime> parkingMultiTimes) {
        this.parkingMultiTimes = parkingMultiTimes;
    }

    public ArrayList<EventMultiTime> getEventMultiTimes() {
        return eventMultiTimes;
    }

    public void setEventMultiTimes(ArrayList<EventMultiTime> eventMultiTimes) {
        this.eventMultiTimes = eventMultiTimes;
    }

    private boolean reqd_local_event_regn, reqd_web_event_regn, free_local_event;
    private ArrayList<EventImage> eventImages = new ArrayList<>();

    public boolean isFree_local_event() {
        return free_local_event;
    }

    public void setFree_local_event(boolean free_local_event) {
        this.free_local_event = free_local_event;
    }

    public ArrayList<EventImage> getEventImages() {
        return eventImages;
    }

    public void setEventImages(ArrayList<EventImage> eventImages) {
        this.eventImages = eventImages;
    }

    public boolean isReqd_local_event_regn() {
        return reqd_local_event_regn;
    }

    public void setReqd_local_event_regn(boolean reqd_local_event_regn) {
        this.reqd_local_event_regn = reqd_local_event_regn;
    }

    public boolean isReqd_web_event_regn() {
        return reqd_web_event_regn;
    }

    public void setReqd_web_event_regn(boolean reqd_web_event_regn) {
        this.reqd_web_event_regn = reqd_web_event_regn;
    }

    public boolean isParking_allowed() {
        return is_parking_allowed;
    }

    public void setIs_parking_allowed(boolean is_parking_allowed) {
        this.is_parking_allowed = is_parking_allowed;
    }

    public String getLocal_timezone() {
        return local_timezone;
    }

    public void setLocal_timezone(String local_timezone) {
        this.local_timezone = local_timezone;
    }

    public String getLocal_time_event_start() {
        return local_time_event_start;
    }

    public void setLocal_time_event_start(String local_time_event_start) {
        this.local_time_event_start = local_time_event_start;
    }

    public ArrayList<String> getEventDates() {
        return eventDates;
    }

    public void setEventDates(ArrayList<String> eventDates) {
        this.eventDates = eventDates;
    }

    public String getEvent_regn_limit() {
        return event_regn_limit;
    }

    public void setEvent_regn_limit(String event_regn_limit) {
        this.event_regn_limit = event_regn_limit;
    }

    public String getWeb_event_full_regn_fee() {
        return web_event_full_regn_fee;
    }

    public void setWeb_event_full_regn_fee(String web_event_full_regn_fee) {
        this.web_event_full_regn_fee = web_event_full_regn_fee;
    }

    public String getWeb_event_regn_limit() {
        return web_event_regn_limit;
    }

    public void setWeb_event_regn_limit(String web_event_regn_limit) {
        this.web_event_regn_limit = web_event_regn_limit;
    }

    public boolean isFree_web_event() {
        return free_web_event;
    }

    public void setFree_web_event(boolean free_web_event) {
        this.free_web_event = free_web_event;
    }

    public boolean isWeb_event_location_to_show() {
        return web_event_location_to_show;
    }

    public void setWeb_event_location_to_show(boolean web_event_location_to_show) {
        this.web_event_location_to_show = web_event_location_to_show;
    }

    public String getEvent_link_facebook() {
        return event_link_facebook;
    }

    public void setEvent_link_facebook(String event_link_facebook) {
        this.event_link_facebook = event_link_facebook;
    }

    public int getEvent_logi_type() {
        return event_logi_type;
    }

    public void setEvent_logi_type(int event_logi_type) {
        this.event_logi_type = event_logi_type;
    }

    public String getEvent_link_ytube() {
        return event_link_ytube;
    }

    public void setEvent_link_ytube(String event_link_ytube) {
        this.event_link_ytube = event_link_ytube;
    }

    public String getEvent_link_zoom() {
        return event_link_zoom;
    }

    public void setEvent_link_zoom(String event_link_zoom) {
        this.event_link_zoom = event_link_zoom;
    }

    public String getEvent_link_googlemeet() {
        return event_link_googlemeet;
    }

    public void setEvent_link_googlemeet(String event_link_googlemeet) {
        this.event_link_googlemeet = event_link_googlemeet;
    }

    public String getEvent_link_googleclassroom() {
        return event_link_googleclassroom;
    }

    public void setEvent_link_googleclassroom(String event_link_googleclassroom) {
        this.event_link_googleclassroom = event_link_googleclassroom;
    }

    public boolean isFree_event() {
        return free_event;
    }

    public void setFree_event(boolean free_event) {
        this.free_event = free_event;
    }

    public boolean isFree_event_parking() {
        return free_event_parking;
    }

    public void setFree_event_parking(boolean free_event_parking) {
        this.free_event_parking = free_event_parking;
    }

    public String getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(String eventCategory) {
        this.eventCategory = eventCategory;
    }

    public String getEvent_full_youth_fee() {
        return event_full_youth_fee;
    }

    public void setEvent_full_youth_fee(String event_full_youth_fee) {
        this.event_full_youth_fee = event_full_youth_fee;
    }

    public String getEvent_full_child_fee() {
        return event_full_child_fee;
    }

    public void setEvent_full_child_fee(String event_full_child_fee) {
        this.event_full_child_fee = event_full_child_fee;
    }

    public String getEvent_full_student_fee() {
        return event_full_student_fee;
    }

    public void setEvent_full_student_fee(String event_full_student_fee) {
        this.event_full_student_fee = event_full_student_fee;
    }

    public String getEvent_full_minister_fee() {
        return event_full_minister_fee;
    }

    public void setEvent_full_minister_fee(String event_full_minister_fee) {
        this.event_full_minister_fee = event_full_minister_fee;
    }

    public String getEvent_full_clergy_fee() {
        return event_full_clergy_fee;
    }

    public void setEvent_full_clergy_fee(String event_full_clergy_fee) {
        this.event_full_clergy_fee = event_full_clergy_fee;
    }

    public String getEvent_full_promo_fee() {
        return event_full_promo_fee;
    }

    public void setEvent_full_promo_fee(String event_full_promo_fee) {
        this.event_full_promo_fee = event_full_promo_fee;
    }

    public String getEvent_full_senior_fee() {
        return event_full_senior_fee;
    }

    public void setEvent_full_senior_fee(String event_full_senior_fee) {
        this.event_full_senior_fee = event_full_senior_fee;
    }

    public String getEvent_full_staff_fee() {
        return event_full_staff_fee;
    }

    public void setEvent_full_staff_fee(String event_full_staff_fee) {
        this.event_full_staff_fee = event_full_staff_fee;
    }

    public boolean isRegn_user_info_reqd() {
        return regn_user_info_reqd;
    }

    public void setRegn_user_info_reqd(boolean regn_user_info_reqd) {
        this.regn_user_info_reqd = regn_user_info_reqd;
    }

    public boolean isRegn_addl_user_info_reqd() {
        return regn_addl_user_info_reqd;
    }

    public void setRegn_addl_user_info_reqd(boolean regn_addl_user_info_reqd) {
        this.regn_addl_user_info_reqd = regn_addl_user_info_reqd;
    }

    public String getDisplay_address() {
        return display_address;
    }

    public void setDisplay_address(String display_address) {
        this.display_address = display_address;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public boolean isRegn_needed_approval() {
        return regn_needed_approval;
    }

    public void setRegn_needed_approval(boolean regn_needed_approval) {
        this.regn_needed_approval = regn_needed_approval;
    }

    public String getEvent_led_by() {
        return event_led_by;
    }

    public void setEvent_led_by(String event_led_by) {
        this.event_led_by = event_led_by;
    }

    public String getEvent_leaders_bio() {
        return event_leaders_bio;
    }

    public void setEvent_leaders_bio(String event_leaders_bio) {
        this.event_leaders_bio = event_leaders_bio;
    }

    public String getLocation_lat_lng() {
        return location_lat_lng;
    }

    public void setLocation_lat_lng(String location_lat_lng) {
        this.location_lat_lng = location_lat_lng;
    }

    public String getEvent_parking_fee() {
        return event_parking_fee;
    }

    public void setEvent_parking_fee(String event_parking_fee) {
        this.event_parking_fee = event_parking_fee;
    }

    public boolean isEvent_multi_sessions() {
        return event_multi_sessions;
    }

    public void setEvent_multi_sessions(boolean event_multi_sessions) {
        this.event_multi_sessions = event_multi_sessions;
    }

    public boolean isEvent_indiv_session_regn_allowed() {
        return event_indiv_session_regn_allowed;
    }

    public void setEvent_indiv_session_regn_allowed(boolean event_indiv_session_regn_allowed) {
        this.event_indiv_session_regn_allowed = event_indiv_session_regn_allowed;
    }

    public boolean isEvent_indiv_session_regn_required() {
        return event_indiv_session_regn_required;
    }

    public void setEvent_indiv_session_regn_required(boolean event_indiv_session_regn_required) {
        this.event_indiv_session_regn_required = event_indiv_session_regn_required;
    }

    public boolean isEvent_full_regn_required() {
        return event_full_regn_required;
    }

    public void setEvent_full_regn_required(boolean event_full_regn_required) {
        this.event_full_regn_required = event_full_regn_required;
    }

    public String getEvent_full_regn_fee() {
        return event_full_regn_fee;
    }

    public void setEvent_full_regn_fee(String event_full_regn_fee) {
        this.event_full_regn_fee = event_full_regn_fee;
    }

    public String getEvent_code() {
        return event_code;
    }

    public void setEvent_code(String event_code) {
        this.event_code = event_code;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public String getEvent_parking_timings() {
        return event_parking_timings;
    }

    public void setEvent_parking_timings(String event_parking_timings) {
        this.event_parking_timings = event_parking_timings;
    }

    public String getEvent_event_timings() {
        return event_event_timings;
    }

    public void setEvent_event_timings(String event_event_timings) {
        this.event_event_timings = event_event_timings;
    }

    public String getEvent_multi_dates() {
        return event_multi_dates;
    }

    public void setEvent_multi_dates(String event_multi_dates) {
        this.event_multi_dates = event_multi_dates;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getManager_type_id() {
        return manager_type_id;
    }

    public void setManager_type_id(int manager_type_id) {
        this.manager_type_id = manager_type_id;
    }

    public int getTwp_id() {
        return twp_id;
    }

    public void setTwp_id(int twp_id) {
        this.twp_id = twp_id;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getManager_type() {
        return manager_type;
    }

    public void setManager_type(String manager_type) {
        this.manager_type = manager_type;
    }

    public String getTownship_code() {
        return township_code;
    }

    public void setTownship_code(String township_code) {
        this.township_code = township_code;
    }

    public String getTownship_name() {
        return township_name;
    }

    public void setTownship_name(String township_name) {
        this.township_name = township_name;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_short_description() {
        return event_short_description;
    }

    public void setEvent_short_description(String event_short_description) {
        this.event_short_description = event_short_description;
    }

    public String getEvent_long_description() {
        return event_long_description;
    }

    public void setEvent_long_description(String event_long_description) {
        this.event_long_description = event_long_description;
    }

    public String getEvent_link_on_web() {
        return event_link_on_web;
    }

    public void setEvent_link_on_web(String event_link_on_web) {
        this.event_link_on_web = event_link_on_web;
    }

    public String getEvent_link_twitter() {
        return event_link_twitter;
    }

    public void setEvent_link_twitter(String event_link_twitter) {
        this.event_link_twitter = event_link_twitter;
    }

    public String getEvent_link_whatsapp() {
        return event_link_whatsapp;
    }

    public void setEvent_link_whatsapp(String event_link_whatsapp) {
        this.event_link_whatsapp = event_link_whatsapp;
    }

    public String getEvent_link_other_media() {
        return event_link_other_media;
    }

    public void setEvent_link_other_media(String event_link_other_media) {
        this.event_link_other_media = event_link_other_media;
    }

    public String getCompany_logo() {
        return company_logo;
    }

    public void setCompany_logo(String company_logo) {
        this.company_logo = company_logo;
    }

    public String getEvent_logo() {
        return event_logo;
    }

    public void setEvent_logo(String event_logo) {
        this.event_logo = event_logo;
    }

    public String getEvent_image1() {
        return event_image1;
    }

    public void setEvent_image1(String event_image1) {
        this.event_image1 = event_image1;
    }

    public String getEvent_image2() {
        return event_image2;
    }

    public void setEvent_image2(String event_image2) {
        this.event_image2 = event_image2;
    }

    public String getEvent_blob_image() {
        return event_blob_image;
    }

    public void setEvent_blob_image(String event_blob_image) {
        this.event_blob_image = event_blob_image;
    }

    public String getEvent_address() {
        return event_address;
    }

    public void setEvent_address(String event_address) {
        this.event_address = event_address;
    }

    public String getCovered_locations() {
        return covered_locations;
    }

    public void setCovered_locations(String covered_locations) {
        this.covered_locations = covered_locations;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public String getAppl_req_download() {
        return appl_req_download;
    }

    public void setAppl_req_download(String appl_req_download) {
        this.appl_req_download = appl_req_download;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getLocation_address() {
        return location_address;
    }

    public void setLocation_address(String location_address) {
        this.location_address = location_address;
    }

    public String getScheme_type() {
        return scheme_type;
    }

    public void setScheme_type(String scheme_type) {
        this.scheme_type = scheme_type;
    }

    public String getEvent_prefix() {
        return event_prefix;
    }

    public void setEvent_prefix(String event_prefix) {
        this.event_prefix = event_prefix;
    }

    public String getEvent_nextnum() {
        return event_nextnum;
    }

    public void setEvent_nextnum(String event_nextnum) {
        this.event_nextnum = event_nextnum;
    }

    public String getEvent_begins_date_time() {
        return event_begins_date_time;
    }

    public void setEvent_begins_date_time(String event_begins_date_time) {
        this.event_begins_date_time = event_begins_date_time;
    }

    public String getEvent_ends_date_time() {
        return event_ends_date_time;
    }

    public void setEvent_ends_date_time(String event_ends_date_time) {
        this.event_ends_date_time = event_ends_date_time;
    }

    public String getEvent_parking_begins_date_time() {
        return event_parking_begins_date_time;
    }

    public void setEvent_parking_begins_date_time(String event_parking_begins_date_time) {
        this.event_parking_begins_date_time = event_parking_begins_date_time;
    }

    public String getEvent_parking_ends_date_time() {
        return event_parking_ends_date_time;
    }

    public void setEvent_parking_ends_date_time(String event_parking_ends_date_time) {
        this.event_parking_ends_date_time = event_parking_ends_date_time;
    }

    public String getExpires_by() {
        return expires_by;
    }

    public void setExpires_by(String expires_by) {
        this.expires_by = expires_by;
    }

    public boolean isRegn_reqd() {
        return regn_reqd;
    }

    public void setRegn_reqd(boolean regn_reqd) {
        this.regn_reqd = regn_reqd;
    }

    public boolean isRegn_reqd_for_parking() {
        return regn_reqd_for_parking;
    }

    public void setRegn_reqd_for_parking(boolean regn_reqd_for_parking) {
        this.regn_reqd_for_parking = regn_reqd_for_parking;
    }

    public boolean isRenewable() {
        return renewable;
    }

    public void setRenewable(boolean renewable) {
        this.renewable = renewable;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getEvent_type_id() {
        return event_type_id;
    }

    public void setEvent_type_id(int event_type_id) {
        this.event_type_id = event_type_id;
    }

    public String getCompany_type() {
        return company_type;
    }

    public void setCompany_type(String company_type) {
        this.company_type = company_type;
    }

    public int getEvent_category_type_id() {
        return event_category_type_id;
    }

    public void setEvent_category_type_id(int event_category_type_id) {
        this.event_category_type_id = event_category_type_id;
    }

    public String getEvent_category_type() {
        return event_category_type;
    }

    public void setEvent_category_type(String event_category_type) {
        this.event_category_type = event_category_type;
    }

    public boolean isEvent_tp_sharable() {
        return event_tp_sharable;
    }

    public void setEvent_tp_sharable(boolean event_tp_sharable) {
        this.event_tp_sharable = event_tp_sharable;
    }

    public String getEvent_parking_fee_duration() {
        return event_parking_fee_duration;
    }

    public void setEvent_parking_fee_duration(String event_parking_fee_duration) {
        this.event_parking_fee_duration = event_parking_fee_duration;
    }

    public boolean isEvent_public() {
        return event_public;
    }

    public void setEvent_public(boolean event_public) {
        this.event_public = event_public;
    }

    public boolean isEvent_unlisted() {
        return event_unlisted;
    }

    public void setEvent_unlisted(boolean event_unlisted) {
        this.event_unlisted = event_unlisted;
    }

    public boolean isEvent_invite_only() {
        return event_invite_only;
    }

    public void setEvent_invite_only(boolean event_invite_only) {
        this.event_invite_only = event_invite_only;
    }

    public boolean isEvent_regn_invite_only() {
        return event_regn_invite_only;
    }

    public void setEvent_regn_invite_only(boolean event_regn_invite_only) {
        this.event_regn_invite_only = event_regn_invite_only;
    }

    public boolean isShow_live_regd_only() {
        return show_live_regd_only;
    }

    public void setShow_live_regd_only(boolean show_live_regd_only) {
        this.show_live_regd_only = show_live_regd_only;
    }

    public boolean isShow_live_invite_only() {
        return show_live_invite_only;
    }

    public void setShow_live_invite_only(boolean show_live_invite_only) {
        this.show_live_invite_only = show_live_invite_only;
    }

    public boolean isShow_past_regd_only() {
        return show_past_regd_only;
    }

    public void setShow_past_regd_only(boolean show_past_regd_only) {
        this.show_past_regd_only = show_past_regd_only;
    }

    public boolean isShow_past_invite_only() {
        return show_past_invite_only;
    }

    public void setShow_past_invite_only(boolean show_past_invite_only) {
        this.show_past_invite_only = show_past_invite_only;
    }

    public boolean isGeo_restrict_countries() {
        return geo_restrict_countries;
    }

    public void setGeo_restrict_countries(boolean geo_restrict_countries) {
        this.geo_restrict_countries = geo_restrict_countries;
    }

    public boolean isGeo_allow_countres() {
        return geo_allow_countres;
    }

    public void setGeo_allow_countres(boolean geo_allow_countres) {
        this.geo_allow_countres = geo_allow_countres;
    }

    public boolean isGeo_restrict_states() {
        return geo_restrict_states;
    }

    public void setGeo_restrict_states(boolean geo_restrict_states) {
        this.geo_restrict_states = geo_restrict_states;
    }

    public boolean isGeo_allow_states() {
        return geo_allow_states;
    }

    public void setGeo_allow_states(boolean geo_allow_states) {
        this.geo_allow_states = geo_allow_states;
    }

    public boolean isGeo_restrict_lat_lng() {
        return geo_restrict_lat_lng;
    }

    public void setGeo_restrict_lat_lng(boolean geo_restrict_lat_lng) {
        this.geo_restrict_lat_lng = geo_restrict_lat_lng;
    }

    public boolean isGeo_allow_lat_lng() {
        return geo_allow_lat_lng;
    }

    public void setGeo_allow_lat_lng(boolean geo_allow_lat_lng) {
        this.geo_allow_lat_lng = geo_allow_lat_lng;
    }

    public String getEvent_link_array() {
        return event_link_array;
    }

    public void setEvent_link_array(String event_link_array) {
        this.event_link_array = event_link_array;
    }

    public boolean isAllow_audio_conf() {
        return allow_audio_conf;
    }

    public void setAllow_audio_conf(boolean allow_audio_conf) {
        this.allow_audio_conf = allow_audio_conf;
    }

    public boolean isAllow_audio_condition_no() {
        return allow_audio_condition_no;
    }

    public void setAllow_audio_condition_no(boolean allow_audio_condition_no) {
        this.allow_audio_condition_no = allow_audio_condition_no;
    }

    public boolean isAllow_audio_conf_rec() {
        return allow_audio_conf_rec;
    }

    public void setAllow_audio_conf_rec(boolean allow_audio_conf_rec) {
        this.allow_audio_conf_rec = allow_audio_conf_rec;
    }

    public boolean isAllow_audio_rec_condition() {
        return allow_audio_rec_condition;
    }

    public void setAllow_audio_rec_condition(boolean allow_audio_rec_condition) {
        this.allow_audio_rec_condition = allow_audio_rec_condition;
    }

    public boolean isAllow_video_conf() {
        return allow_video_conf;
    }

    public void setAllow_video_conf(boolean allow_video_conf) {
        this.allow_video_conf = allow_video_conf;
    }

    public boolean isAllow_video_condition_no() {
        return allow_video_condition_no;
    }

    public void setAllow_video_condition_no(boolean allow_video_condition_no) {
        this.allow_video_condition_no = allow_video_condition_no;
    }

    public boolean isAllow_video_conf_rec() {
        return allow_video_conf_rec;
    }

    public void setAllow_video_conf_rec(boolean allow_video_conf_rec) {
        this.allow_video_conf_rec = allow_video_conf_rec;
    }

    public boolean isAllow_video_rec_condition() {
        return allow_video_rec_condition;
    }

    public void setAllow_video_rec_condition(boolean allow_video_rec_condition) {
        this.allow_video_rec_condition = allow_video_rec_condition;
    }

    public boolean isAllow_livechat() {
        return allow_livechat;
    }

    public void setAllow_livechat(boolean allow_livechat) {
        this.allow_livechat = allow_livechat;
    }

    public boolean isAllow_livechat_condition_no() {
        return allow_livechat_condition_no;
    }

    public void setAllow_livechat_condition_no(boolean allow_livechat_condition_no) {
        this.allow_livechat_condition_no = allow_livechat_condition_no;
    }

    public String getParking_reservation_limit() {
        return parking_reservation_limit;
    }

    public void setParking_reservation_limit(String parking_reservation_limit) {
        this.parking_reservation_limit = parking_reservation_limit;
    }

    public boolean isArchive_access() {
        return archive_access;
    }

    public void setArchive_access(boolean archive_access) {
        this.archive_access = archive_access;
    }

}
