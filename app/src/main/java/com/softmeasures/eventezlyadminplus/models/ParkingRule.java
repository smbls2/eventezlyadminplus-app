package com.softmeasures.eventezlyadminplus.models;

public class ParkingRule {
    private int id, location_id, twp_id, manager_type_id, company_type_id, active, in_effect,
            max_duration, number_of_reservable_spots;
    private String location_place_id, location_code, location_name, township_code,
            custom_notice, marker_type, day_type, pricing, premium_pricing, pricing_duration,
            duration_unit, start_hour, start_time, end_hour, end_time, time_rule, lot_numbering_type,
            lot_numbering_description, currency, currency_symbol, Cancellation_Charge,
            Reservation_PostPayment_term, Reservation_PostPayment_LateFee,
            ParkNow_PostPayment_term, ParkNow_PostPayment_LateFee;
    private boolean ReservationAllowed, reservable, spot_specific_reservable, PrePymntReqd_for_Reservation,
            CanCancel_Reservation, ParkNow_PostPaymentAllowed, before_reserve_verify_lot_avbl,
            include_reservations_for_avbl_calc;
    private double Reservation_PostPayment_Fee, ParkNow_PostPayment_Fee;

    private int event_id;
    private String event_code, event_name, event_specific_date, event_specific_time, event_multi_dates,
            event_multi_timings, event_dt_short_info;
    private boolean multi_day, multi_day_reserve_allowed, multi_day_parking_allowed;

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public String getEvent_code() {
        return event_code;
    }

    public void setEvent_code(String event_code) {
        this.event_code = event_code;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_specific_date() {
        return event_specific_date;
    }

    public void setEvent_specific_date(String event_specific_date) {
        this.event_specific_date = event_specific_date;
    }

    public String getEvent_specific_time() {
        return event_specific_time;
    }

    public void setEvent_specific_time(String event_specific_time) {
        this.event_specific_time = event_specific_time;
    }

    public String getEvent_multi_dates() {
        return event_multi_dates;
    }

    public void setEvent_multi_dates(String event_multi_dates) {
        this.event_multi_dates = event_multi_dates;
    }

    public String getEvent_multi_timings() {
        return event_multi_timings;
    }

    public void setEvent_multi_timings(String event_multi_timings) {
        this.event_multi_timings = event_multi_timings;
    }

    public String getEvent_dt_short_info() {
        return event_dt_short_info;
    }

    public void setEvent_dt_short_info(String event_dt_short_info) {
        this.event_dt_short_info = event_dt_short_info;
    }

    public boolean isMulti_day() {
        return multi_day;
    }

    public void setMulti_day(boolean multi_day) {
        this.multi_day = multi_day;
    }

    public boolean isMulti_day_reserve_allowed() {
        return multi_day_reserve_allowed;
    }

    public void setMulti_day_reserve_allowed(boolean multi_day_reserve_allowed) {
        this.multi_day_reserve_allowed = multi_day_reserve_allowed;
    }

    public boolean isMulti_day_parking_allowed() {
        return multi_day_parking_allowed;
    }

    public void setMulti_day_parking_allowed(boolean multi_day_parking_allowed) {
        this.multi_day_parking_allowed = multi_day_parking_allowed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public int getTwp_id() {
        return twp_id;
    }

    public void setTwp_id(int twp_id) {
        this.twp_id = twp_id;
    }

    public int getManager_type_id() {
        return manager_type_id;
    }

    public void setManager_type_id(int manager_type_id) {
        this.manager_type_id = manager_type_id;
    }

    public int getCompany_type_id() {
        return company_type_id;
    }

    public void setCompany_type_id(int company_type_id) {
        this.company_type_id = company_type_id;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getIn_effect() {
        return in_effect;
    }

    public void setIn_effect(int in_effect) {
        this.in_effect = in_effect;
    }

    public int getMax_duration() {
        return max_duration;
    }

    public void setMax_duration(int max_duration) {
        this.max_duration = max_duration;
    }

    public int getNumber_of_reservable_spots() {
        return number_of_reservable_spots;
    }

    public void setNumber_of_reservable_spots(int number_of_reservable_spots) {
        this.number_of_reservable_spots = number_of_reservable_spots;
    }

    public String getLocation_place_id() {
        return location_place_id;
    }

    public void setLocation_place_id(String location_place_id) {
        this.location_place_id = location_place_id;
    }

    public String getLocation_code() {
        return location_code;
    }

    public void setLocation_code(String location_code) {
        this.location_code = location_code;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getTownship_code() {
        return township_code;
    }

    public void setTownship_code(String township_code) {
        this.township_code = township_code;
    }

    public String getCustom_notice() {
        return custom_notice;
    }

    public void setCustom_notice(String custom_notice) {
        this.custom_notice = custom_notice;
    }

    public String getMarker_type() {
        return marker_type;
    }

    public void setMarker_type(String marker_type) {
        this.marker_type = marker_type;
    }

    public String getDay_type() {
        return day_type;
    }

    public void setDay_type(String day_type) {
        this.day_type = day_type;
    }

    public String getPricing() {
        return pricing;
    }

    public void setPricing(String pricing) {
        this.pricing = pricing;
    }

    public String getPremium_pricing() {
        return premium_pricing;
    }

    public void setPremium_pricing(String premium_pricing) {
        this.premium_pricing = premium_pricing;
    }

    public String getPricing_duration() {
        return pricing_duration;
    }

    public void setPricing_duration(String pricing_duration) {
        this.pricing_duration = pricing_duration;
    }

    public String getDuration_unit() {
        return duration_unit;
    }

    public void setDuration_unit(String duration_unit) {
        this.duration_unit = duration_unit;
    }

    public String getStart_hour() {
        return start_hour;
    }

    public void setStart_hour(String start_hour) {
        this.start_hour = start_hour;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_hour() {
        return end_hour;
    }

    public void setEnd_hour(String end_hour) {
        this.end_hour = end_hour;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getTime_rule() {
        return time_rule;
    }

    public void setTime_rule(String time_rule) {
        this.time_rule = time_rule;
    }

    public String getLot_numbering_type() {
        return lot_numbering_type;
    }

    public void setLot_numbering_type(String lot_numbering_type) {
        this.lot_numbering_type = lot_numbering_type;
    }

    public String getLot_numbering_description() {
        return lot_numbering_description;
    }

    public void setLot_numbering_description(String lot_numbering_description) {
        this.lot_numbering_description = lot_numbering_description;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency_symbol() {
        return currency_symbol;
    }

    public void setCurrency_symbol(String currency_symbol) {
        this.currency_symbol = currency_symbol;
    }

    public String getCancellation_Charge() {
        return Cancellation_Charge;
    }

    public void setCancellation_Charge(String cancellation_Charge) {
        Cancellation_Charge = cancellation_Charge;
    }

    public String getReservation_PostPayment_term() {
        return Reservation_PostPayment_term;
    }

    public void setReservation_PostPayment_term(String reservation_PostPayment_term) {
        Reservation_PostPayment_term = reservation_PostPayment_term;
    }

    public String getReservation_PostPayment_LateFee() {
        return Reservation_PostPayment_LateFee;
    }

    public void setReservation_PostPayment_LateFee(String reservation_PostPayment_LateFee) {
        Reservation_PostPayment_LateFee = reservation_PostPayment_LateFee;
    }

    public String getParkNow_PostPayment_term() {
        return ParkNow_PostPayment_term;
    }

    public void setParkNow_PostPayment_term(String parkNow_PostPayment_term) {
        ParkNow_PostPayment_term = parkNow_PostPayment_term;
    }

    public String getParkNow_PostPayment_LateFee() {
        return ParkNow_PostPayment_LateFee;
    }

    public void setParkNow_PostPayment_LateFee(String parkNow_PostPayment_LateFee) {
        ParkNow_PostPayment_LateFee = parkNow_PostPayment_LateFee;
    }

    public boolean isReservationAllowed() {
        return ReservationAllowed;
    }

    public void setReservationAllowed(boolean reservationAllowed) {
        ReservationAllowed = reservationAllowed;
    }

    public boolean isReservable() {
        return reservable;
    }

    public void setReservable(boolean reservable) {
        this.reservable = reservable;
    }

    public boolean isSpot_specific_reservable() {
        return spot_specific_reservable;
    }

    public void setSpot_specific_reservable(boolean spot_specific_reservable) {
        this.spot_specific_reservable = spot_specific_reservable;
    }

    public boolean isPrePymntReqd_for_Reservation() {
        return PrePymntReqd_for_Reservation;
    }

    public void setPrePymntReqd_for_Reservation(boolean prePymntReqd_for_Reservation) {
        PrePymntReqd_for_Reservation = prePymntReqd_for_Reservation;
    }

    public boolean isCanCancel_Reservation() {
        return CanCancel_Reservation;
    }

    public void setCanCancel_Reservation(boolean canCancel_Reservation) {
        CanCancel_Reservation = canCancel_Reservation;
    }

    public boolean isParkNow_PostPaymentAllowed() {
        return ParkNow_PostPaymentAllowed;
    }

    public void setParkNow_PostPaymentAllowed(boolean parkNow_PostPaymentAllowed) {
        ParkNow_PostPaymentAllowed = parkNow_PostPaymentAllowed;
    }

    public boolean isBefore_reserve_verify_lot_avbl() {
        return before_reserve_verify_lot_avbl;
    }

    public void setBefore_reserve_verify_lot_avbl(boolean before_reserve_verify_lot_avbl) {
        this.before_reserve_verify_lot_avbl = before_reserve_verify_lot_avbl;
    }

    public boolean isInclude_reservations_for_avbl_calc() {
        return include_reservations_for_avbl_calc;
    }

    public void setInclude_reservations_for_avbl_calc(boolean include_reservations_for_avbl_calc) {
        this.include_reservations_for_avbl_calc = include_reservations_for_avbl_calc;
    }

    public double getReservation_PostPayment_Fee() {
        return Reservation_PostPayment_Fee;
    }

    public void setReservation_PostPayment_Fee(double reservation_PostPayment_Fee) {
        Reservation_PostPayment_Fee = reservation_PostPayment_Fee;
    }

    public double getParkNow_PostPayment_Fee() {
        return ParkNow_PostPayment_Fee;
    }

    public void setParkNow_PostPayment_Fee(double parkNow_PostPayment_Fee) {
        ParkNow_PostPayment_Fee = parkNow_PostPayment_Fee;
    }
}
