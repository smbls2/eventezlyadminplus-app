package com.softmeasures.eventezlyadminplus.models.call_chat;

public class ChatDefinitions {
    private int id;
    private int senderUserId, receiverUserId;
    private String message_texts, senderUserName, senderEmail, receiverUserName, receiverEmail;
    private String last_message_time, sender_last_seen_time, receiver_last_seen_time;
    int isNew;
    private boolean allow_share, allow_download, allow_copying, allow_forward;
    private String datetime;
    private int callId;
    private int callType;
    private boolean callScheduled;
    private boolean callRepeated;
    private String callBeginsDatetime;
    private String callEndsDatetime;
    private String callRepeatedCondition;
    private boolean countVisible;
    private boolean attendeesVisible;
    private boolean audioOnly;
    private boolean videoOnly;
    private boolean broadcastOnly;
    private boolean screenSharingAllow;
    private boolean interactiveOff;
    private boolean chatOff;
    private boolean recAllowedHost;
    private boolean recAllowedGuest;
    private String meetingIdSourcetype;
    private boolean recordingLocal;
    private boolean recordingCloud;
    private String regionName;
    private String bucketName;
    private String cloudRecDir;
    private String localRecDir;
    private int maximumParticipants;
    private int companyTypeId;
    private int companyId;
    private String companyCode;
    private String companyName;
    private String userProfileType;
    private int userId;
    private String appName;
    private int appId;
    private String meetingId, meetingTitle;
    private String password;
    private boolean active;
    private boolean chatType, attachLiveCamera,
            attachLiveMic, attachDoc, attachVideo, attachAudio, attachImage, attachZip,
            displayAttachVideo, displayAttachAudio, displayAttachImage, displayAttachDoc; // guestChatGroupOn, guestChatIndivOn,
    private boolean guestChatGroupOn, guestChatIndivOn;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public int getSenderUserId() {
        return senderUserId;
    }

    public void setSenderUserId(int senderUserId) {
        this.senderUserId = senderUserId;
    }

    public int getReceiverUserId() {
        return receiverUserId;
    }

    public void setReceiverUserId(int receiverUserId) {
        this.receiverUserId = receiverUserId;
    }

    public String getMessage_texts() {
        return message_texts;
    }

    public void setMessage_texts(String message_texts) {
        this.message_texts = message_texts;
    }

    public String getSenderUserName() {
        return senderUserName;
    }

    public void setSenderUserName(String senderUserName) {
        this.senderUserName = senderUserName;
    }

    public String getReceiverUserName() {
        return receiverUserName;
    }

    public void setReceiverUserName(String receiverUserName) {
        this.receiverUserName = receiverUserName;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getReceiverEmail() {
        return receiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        this.receiverEmail = receiverEmail;
    }

    public String getReceiver_last_seen_time() {
        return receiver_last_seen_time;
    }

    public void setReceiver_last_seen_time(String receiver_last_seen_time) {
        this.receiver_last_seen_time = receiver_last_seen_time;
    }

    public String getSender_last_seen_time() {
        return sender_last_seen_time;
    }

    public void setSender_last_seen_time(String sender_last_seen_time) {
        this.sender_last_seen_time = sender_last_seen_time;
    }

    public String getLast_message_time() {
        return last_message_time;
    }

    public void setLast_message_time(String last_message_time) {
        this.last_message_time = last_message_time;
    }

    public int getIsNew() {
        return isNew;
    }

    public void setIsNew(int isNew) {
        this.isNew = isNew;
    }

    public boolean isChatType() {
        return chatType;
    }

    public void setChatType(boolean chatType) {
        this.chatType = chatType;
    }

    public boolean isGuestChatGroupOn() {
        return guestChatGroupOn;
    }

    public void setGuestChatGroupOn(boolean guestChatGroupOn) {
        this.guestChatGroupOn = guestChatGroupOn;
    }

    public boolean isGuestChatIndivOn() {
        return guestChatIndivOn;
    }

    public void setGuestChatIndivOn(boolean guestChatIndivOn) {
        this.guestChatIndivOn = guestChatIndivOn;
    }

    public boolean isAttachLiveCamera() {
        return attachLiveCamera;
    }

    public void setAttachLiveCamera(boolean attachLiveCamera) {
        this.attachLiveCamera = attachLiveCamera;
    }

    public boolean isAttachLiveMic() {
        return attachLiveMic;
    }

    public void setAttachLiveMic(boolean attachLiveMic) {
        this.attachLiveMic = attachLiveMic;
    }

    public boolean isAttachDoc() {
        return attachDoc;
    }

    public void setAttachDoc(boolean attachDoc) {
        this.attachDoc = attachDoc;
    }

    public boolean isAttachVideo() {
        return attachVideo;
    }

    public void setAttachVideo(boolean attachVideo) {
        this.attachVideo = attachVideo;
    }

    public boolean isAttachAudio() {
        return attachAudio;
    }

    public void setAttachAudio(boolean attachAudio) {
        this.attachAudio = attachAudio;
    }

    public boolean isAttachImage() {
        return attachImage;
    }

    public void setAttachImage(boolean attachImage) {
        this.attachImage = attachImage;
    }

    public boolean isAttachZip() {
        return attachZip;
    }

    public void setAttachZip(boolean attachZip) {
        this.attachZip = attachZip;
    }

    public boolean isDisplayAttachVideo() {
        return displayAttachVideo;
    }

    public void setDisplayAttachVideo(boolean displayAttachVideo) {
        this.displayAttachVideo = displayAttachVideo;
    }

    public boolean isDisplayAttachAudio() {
        return displayAttachAudio;
    }

    public void setDisplayAttachAudio(boolean displayAttachAudio) {
        this.displayAttachAudio = displayAttachAudio;
    }

    public boolean isDisplayAttachImage() {
        return displayAttachImage;
    }

    public void setDisplayAttachImage(boolean displayAttachImage) {
        this.displayAttachImage = displayAttachImage;
    }

    public boolean isDisplayAttachDoc() {
        return displayAttachDoc;
    }

    public void setDisplayAttachDoc(boolean displayAttachDoc) {
        this.displayAttachDoc = displayAttachDoc;
    }


    public int getCallId() {
        return callId;
    }

    public void setCallId(int callId) {
        this.callId = callId;
    }

    public int getCallType() {
        return callType;
    }

    public void setCallType(int callType) {
        this.callType = callType;
    }

    public boolean isCallScheduled() {
        return callScheduled;
    }

    public void setCallScheduled(boolean callScheduled) {
        this.callScheduled = callScheduled;
    }

    public boolean isCallRepeated() {
        return callRepeated;
    }

    public void setCallRepeated(boolean callRepeated) {
        this.callRepeated = callRepeated;
    }

    public String getCallBeginsDatetime() {
        return callBeginsDatetime;
    }

    public void setCallBeginsDatetime(String callBeginsDatetime) {
        this.callBeginsDatetime = callBeginsDatetime;
    }

    public String getCallEndsDatetime() {
        return callEndsDatetime;
    }

    public void setCallEndsDatetime(String callEndsDatetime) {
        this.callEndsDatetime = callEndsDatetime;
    }

    public String getCallRepeatedCondition() {
        return callRepeatedCondition;
    }

    public void setCallRepeatedCondition(String callRepeatedCondition) {
        this.callRepeatedCondition = callRepeatedCondition;
    }

    public boolean isCountVisible() {
        return countVisible;
    }

    public void setCountVisible(boolean countVisible) {
        this.countVisible = countVisible;
    }

    public boolean isAttendeesVisible() {
        return attendeesVisible;
    }

    public void setAttendeesVisible(boolean attendeesVisible) {
        this.attendeesVisible = attendeesVisible;
    }

    public boolean isAudioOnly() {
        return audioOnly;
    }

    public void setAudioOnly(boolean audioOnly) {
        this.audioOnly = audioOnly;
    }

    public boolean isVideoOnly() {
        return videoOnly;
    }

    public void setVideoOnly(boolean videoOnly) {
        this.videoOnly = videoOnly;
    }

    public boolean isBroadcastOnly() {
        return broadcastOnly;
    }

    public void setBroadcastOnly(boolean broadcastOnly) {
        this.broadcastOnly = broadcastOnly;
    }

    public boolean isScreenSharingAllow() {
        return screenSharingAllow;
    }

    public void setScreenSharingAllow(boolean screenSharingAllow) {
        this.screenSharingAllow = screenSharingAllow;
    }

    public boolean isInteractiveOff() {
        return interactiveOff;
    }

    public void setInteractiveOff(boolean interactiveOff) {
        this.interactiveOff = interactiveOff;
    }

    public boolean isChatOff() {
        return chatOff;
    }

    public void setChatOff(boolean chatOff) {
        this.chatOff = chatOff;
    }

    public boolean isRecAllowedHost() {
        return recAllowedHost;
    }

    public void setRecAllowedHost(boolean recAllowedHost) {
        this.recAllowedHost = recAllowedHost;
    }

    public boolean isRecAllowedGuest() {
        return recAllowedGuest;
    }

    public void setRecAllowedGuest(boolean recAllowedGuest) {
        this.recAllowedGuest = recAllowedGuest;
    }

    public String getMeetingIdSourcetype() {
        return meetingIdSourcetype;
    }

    public void setMeetingIdSourcetype(String meetingIdSourcetype) {
        this.meetingIdSourcetype = meetingIdSourcetype;
    }

    public boolean isRecordingLocal() {
        return recordingLocal;
    }

    public void setRecordingLocal(boolean recordingLocal) {
        this.recordingLocal = recordingLocal;
    }

    public boolean isRecordingCloud() {
        return recordingCloud;
    }

    public void setRecordingCloud(boolean recordingCloud) {
        this.recordingCloud = recordingCloud;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getCloudRecDir() {
        return cloudRecDir;
    }

    public void setCloudRecDir(String cloudRecDir) {
        this.cloudRecDir = cloudRecDir;
    }

    public String getLocalRecDir() {
        return localRecDir;
    }

    public void setLocalRecDir(String localRecDir) {
        this.localRecDir = localRecDir;
    }

    public int getMaximumParticipants() {
        return maximumParticipants;
    }

    public void setMaximumParticipants(int maximumParticipants) {
        this.maximumParticipants = maximumParticipants;
    }

    public int getCompanyTypeId() {
        return companyTypeId;
    }

    public void setCompanyTypeId(int companyTypeId) {
        this.companyTypeId = companyTypeId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUserProfileType() {
        return userProfileType;
    }

    public void setUserProfileType(String userProfileType) {
        this.userProfileType = userProfileType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public String getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(String meetingId) {
        this.meetingId = meetingId;
    }

    public String getMeetingTitle() {
        return meetingTitle;
    }

    public void setMeetingTitle(String meetingTitle) {
        this.meetingTitle = meetingTitle;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isAllow_forward() {
        return allow_forward;
    }

    public void setAllow_forward(boolean allow_forward) {
        this.allow_forward = allow_forward;
    }

    public boolean isAllow_copying() {
        return allow_copying;
    }

    public void setAllow_copying(boolean allow_copying) {
        this.allow_copying = allow_copying;
    }

    public boolean isAllow_download() {
        return allow_download;
    }

    public void setAllow_download(boolean allow_download) {
        this.allow_download = allow_download;
    }

    public boolean isAllow_share() {
        return allow_share;
    }

    public void setAllow_share(boolean allow_share) {
        this.allow_share = allow_share;
    }
}
