package com.softmeasures.eventezlyadminplus.models.rest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleMapAPIInterface {
    //url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=40.74599864,-73.9825673&rankby=distance&distance=50000&sensor=false&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&types=parking";
    @GET("place/nearbysearch/json")
    Call<Object> getNearBySearchPlace(@Query("location") String location,
                                      @Query("rankby") String rankby,
                                      @Query("distance") int distance,
                                      @Query("sensor") boolean sensor,
                                      @Query("key") String key,
                                      @Query("types") String types);

    //url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&rankby=distance&distance=40000.0&sensor=true&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&name=" + categoriy + "";
    @GET("place/nearbysearch/json")
    Call<Object> getNearBySearchPlaceDetails(@Query("location") String id,
                                             @Query("rankby") String rankby,
                                             @Query("distance") double distance,
                                             @Query("sensor") boolean sensor,
                                             @Query("key") String key,
                                             @Query("name") String name);

    //  url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + lat + "," + lang + "&radius=50000.0&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&types=" + categoriy + "";
    @GET("place/nearbysearch/json")
    Call<Object> getNearBySearchedPlaceDetails(@Query("location") String location,
                                               @Query("radius") double radius,
                                               @Query("key") String key,
                                               @Query("types") String types);

    @GET("place/details/json")
    Call<Object> getNearByPlaceDetails(@Query("placeId") String id,
                                       @Query("key") String key);

    @GET("geocode/json")
    Call<Object> getLocationDetails(@Query("latlng") String latlng,
                                    @Query("sensor") boolean sensor,
                                    @Query("key") String key);

    @GET("geocode/json")
    Call<Object> getLatLangFromPlace(@Query("address") String place,
                                     @Query("sensor") boolean sensor,
                                     @Query("key") String key);

    @GET("weather")
    Call<Object> getLatLangFromPlace(@Query("lat") String lat,
                                     @Query("lon") String lon,
                                     @Query("appid") String appid);
}