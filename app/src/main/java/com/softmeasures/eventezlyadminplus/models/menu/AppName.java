package com.softmeasures.eventezlyadminplus.models.menu;

public class AppName {
    private int id;
    private String app_id, app_name, app_description;

    private String menu_icon, menu_icon_pixel, menu_font, menu_font_size, menu_color, menu_style,
            menu_bg_color, menu_bg_img, menu_font_style, menu_screen_size, menu_list_alignment;
    private boolean menu_default;

    public boolean isMenu_default() {
        return menu_default;
    }

    public void setMenu_default(boolean menu_default) {
        this.menu_default = menu_default;
    }

    public String getMenu_screen_size() {
        return menu_screen_size;
    }

    public void setMenu_screen_size(String menu_screen_size) {
        this.menu_screen_size = menu_screen_size;
    }

    public String getMenu_list_alignment() {
        return menu_list_alignment;
    }

    public void setMenu_list_alignment(String menu_list_alignment) {
        this.menu_list_alignment = menu_list_alignment;
    }

    public String getMenu_font_style() {
        return menu_font_style;
    }

    public void setMenu_font_style(String menu_font_style) {
        this.menu_font_style = menu_font_style;
    }

    public String getMenu_icon() {
        return menu_icon;
    }

    public void setMenu_icon(String menu_icon) {
        this.menu_icon = menu_icon;
    }

    public String getMenu_icon_pixel() {
        return menu_icon_pixel;
    }

    public void setMenu_icon_pixel(String menu_icon_pixel) {
        this.menu_icon_pixel = menu_icon_pixel;
    }

    public String getMenu_font() {
        return menu_font;
    }

    public void setMenu_font(String menu_font) {
        this.menu_font = menu_font;
    }

    public String getMenu_font_size() {
        return menu_font_size;
    }

    public void setMenu_font_size(String menu_font_size) {
        this.menu_font_size = menu_font_size;
    }

    public String getMenu_color() {
        return menu_color;
    }

    public void setMenu_color(String menu_color) {
        this.menu_color = menu_color;
    }

    public String getMenu_style() {
        return menu_style;
    }

    public void setMenu_style(String menu_style) {
        this.menu_style = menu_style;
    }

    public String getMenu_bg_color() {
        return menu_bg_color;
    }

    public void setMenu_bg_color(String menu_bg_color) {
        this.menu_bg_color = menu_bg_color;
    }

    public String getMenu_bg_img() {
        return menu_bg_img;
    }

    public void setMenu_bg_img(String menu_bg_img) {
        this.menu_bg_img = menu_bg_img;
    }

    public AppName() {
    }

    public AppName(String app_name) {
        this.app_name = app_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public String getApp_description() {
        return app_description;
    }

    public void setApp_description(String app_description) {
        this.app_description = app_description;
    }
}
