package com.softmeasures.eventezlyadminplus.models.call_chat;

import androidx.annotation.NonNull;

public class CallsAVWebConfSettings implements Cloneable {

    private int id;
    private String datetime;
    private int companyTypeId;
    private int companyId;
    private String companyCode;
    private String companyName;
    private String userProfileType;
    private int userId;
    private String appName;
    private int appId;
    private boolean eventAudioRecordingLocal;
    private boolean eventAudioRecordingCloud;
    private boolean eventVideoRecordingLocal;
    private boolean eventVideoRecordingCloud;
    private boolean instantAudioRecordingLocal;
    private boolean instantVideoRecordingCloud;
    private boolean eventWebinarRecordingLocal;
    private boolean eventWebinarRecordingCloud;
    private String regionName;
    private String bucketName;
    private String cloudRecDir;
    private String localRecDir;
    private boolean recAllowedHost;
    private boolean recAllowedGuest;
    private boolean attachLiveCamera;
    private boolean attachLiveMic;
    private boolean displayAttachDoc;
    private boolean displayAttachVideo;
    private boolean displayAttachAudio;
    private boolean displayAttachImage;
    private boolean attachZip;
    private boolean attachDoc;
    private boolean attachVideo;
    private boolean attachAudio;
    private boolean attachImage;
    private int cloudRecSizeSessionLimit;
    private int cloudRecSizeTotalLimit;
    private int maximumUsers;
    private String meetingIdSourcetype;
    private boolean totalTimeUnlimited;
    private int totalTimeLimit;
    private boolean sessionTimeUnlimited;
    private int sessionTimeLimit;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public int getCompanyTypeId() {
        return companyTypeId;
    }

    public void setCompanyTypeId(int companyTypeId) {
        this.companyTypeId = companyTypeId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUserProfileType() {
        return userProfileType;
    }

    public void setUserProfileType(String userProfileType) {
        this.userProfileType = userProfileType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public boolean isEventAudioRecordingLocal() {
        return eventAudioRecordingLocal;
    }

    public void setEventAudioRecordingLocal(boolean eventAudioRecordingLocal) {
        this.eventAudioRecordingLocal = eventAudioRecordingLocal;
    }

    public boolean isEventAudioRecordingCloud() {
        return eventAudioRecordingCloud;
    }

    public void setEventAudioRecordingCloud(boolean eventAudioRecordingCloud) {
        this.eventAudioRecordingCloud = eventAudioRecordingCloud;
    }

    public boolean isEventVideoRecordingLocal() {
        return eventVideoRecordingLocal;
    }

    public void setEventVideoRecordingLocal(boolean eventVideoRecordingLocal) {
        this.eventVideoRecordingLocal = eventVideoRecordingLocal;
    }

    public boolean isEventVideoRecordingCloud() {
        return eventVideoRecordingCloud;
    }

    public void setEventVideoRecordingCloud(boolean eventVideoRecordingCloud) {
        this.eventVideoRecordingCloud = eventVideoRecordingCloud;
    }

    public boolean isInstantAudioRecordingLocal() {
        return instantAudioRecordingLocal;
    }

    public void setInstantAudioRecordingLocal(boolean instantAudioRecordingLocal) {
        this.instantAudioRecordingLocal = instantAudioRecordingLocal;
    }

    public boolean isInstantVideoRecordingCloud() {
        return instantVideoRecordingCloud;
    }

    public void setInstantVideoRecordingCloud(boolean instantVideoRecordingCloud) {
        this.instantVideoRecordingCloud = instantVideoRecordingCloud;
    }

    public boolean isEventWebinarRecordingLocal() {
        return eventWebinarRecordingLocal;
    }

    public void setEventWebinarRecordingLocal(boolean eventWebinarRecordingLocal) {
        this.eventWebinarRecordingLocal = eventWebinarRecordingLocal;
    }

    public boolean isEventWebinarRecordingCloud() {
        return eventWebinarRecordingCloud;
    }

    public void setEventWebinarRecordingCloud(boolean eventWebinarRecordingCloud) {
        this.eventWebinarRecordingCloud = eventWebinarRecordingCloud;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getCloudRecDir() {
        return cloudRecDir;
    }

    public void setCloudRecDir(String cloudRecDir) {
        this.cloudRecDir = cloudRecDir;
    }

    public String getLocalRecDir() {
        return localRecDir;
    }

    public void setLocalRecDir(String localRecDir) {
        this.localRecDir = localRecDir;
    }

    public boolean isRecAllowedHost() {
        return recAllowedHost;
    }

    public void setRecAllowedHost(boolean recAllowedHost) {
        this.recAllowedHost = recAllowedHost;
    }

    public boolean isRecAllowedGuest() {
        return recAllowedGuest;
    }

    public void setRecAllowedGuest(boolean recAllowedGuest) {
        this.recAllowedGuest = recAllowedGuest;
    }

    public boolean isAttachLiveCamera() {
        return attachLiveCamera;
    }

    public void setAttachLiveCamera(boolean attachLiveCamera) {
        this.attachLiveCamera = attachLiveCamera;
    }

    public boolean isAttachLiveMic() {
        return attachLiveMic;
    }

    public void setAttachLiveMic(boolean attachLiveMic) {
        this.attachLiveMic = attachLiveMic;
    }

    public boolean isDisplayAttachDoc() {
        return displayAttachDoc;
    }

    public void setDisplayAttachDoc(boolean displayAttachDoc) {
        this.displayAttachDoc = displayAttachDoc;
    }

    public boolean isDisplayAttachVideo() {
        return displayAttachVideo;
    }

    public void setDisplayAttachVideo(boolean displayAttachVideo) {
        this.displayAttachVideo = displayAttachVideo;
    }

    public boolean isDisplayAttachAudio() {
        return displayAttachAudio;
    }

    public void setDisplayAttachAudio(boolean displayAttachAudio) {
        this.displayAttachAudio = displayAttachAudio;
    }

    public boolean isDisplayAttachImage() {
        return displayAttachImage;
    }

    public void setDisplayAttachImage(boolean displayAttachImage) {
        this.displayAttachImage = displayAttachImage;
    }

    public boolean isAttachZip() {
        return attachZip;
    }

    public void setAttachZip(boolean attachZip) {
        this.attachZip = attachZip;
    }

    public boolean isAttachDoc() {
        return attachDoc;
    }

    public void setAttachDoc(boolean attachDoc) {
        this.attachDoc = attachDoc;
    }

    public boolean isAttachVideo() {
        return attachVideo;
    }

    public void setAttachVideo(boolean attachVideo) {
        this.attachVideo = attachVideo;
    }

    public boolean isAttachAudio() {
        return attachAudio;
    }

    public void setAttachAudio(boolean attachAudio) {
        this.attachAudio = attachAudio;
    }

    public boolean isAttachImage() {
        return attachImage;
    }

    public void setAttachImage(boolean attachImage) {
        this.attachImage = attachImage;
    }

    public int getCloudRecSizeSessionLimit() {
        return cloudRecSizeSessionLimit;
    }

    public void setCloudRecSizeSessionLimit(int cloudRecSizeSessionLimit) {
        this.cloudRecSizeSessionLimit = cloudRecSizeSessionLimit;
    }

    public int getCloudRecSizeTotalLimit() {
        return cloudRecSizeTotalLimit;
    }

    public void setCloudRecSizeTotalLimit(int cloudRecSizeTotalLimit) {
        this.cloudRecSizeTotalLimit = cloudRecSizeTotalLimit;
    }

    public int getMaximumUsers() {
        return maximumUsers;
    }

    public void setMaximumUsers(int maximumUsers) {
        this.maximumUsers = maximumUsers;
    }

    public String getMeetingIdSourcetype() {
        return meetingIdSourcetype;
    }

    public void setMeetingIdSourcetype(String meetingIdSourcetype) {
        this.meetingIdSourcetype = meetingIdSourcetype;
    }

    public boolean isTotalTimeUnlimited() {
        return totalTimeUnlimited;
    }

    public void setTotalTimeUnlimited(boolean totalTimeUnlimited) {
        this.totalTimeUnlimited = totalTimeUnlimited;
    }

    public int getTotalTimeLimit() {
        return totalTimeLimit;
    }

    public void setTotalTimeLimit(int totalTimeLimit) {
        this.totalTimeLimit = totalTimeLimit;
    }

    public boolean isSessionTimeUnlimited() {
        return sessionTimeUnlimited;
    }

    public void setSessionTimeUnlimited(boolean sessionTimeUnlimited) {
        this.sessionTimeUnlimited = sessionTimeUnlimited;
    }

    public int getSessionTimeLimit() {
        return sessionTimeLimit;
    }

    public void setSessionTimeLimit(int sessionTimeLimit) {
        this.sessionTimeLimit = sessionTimeLimit;
    }

}