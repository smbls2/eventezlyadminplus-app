package com.softmeasures.eventezlyadminplus.models.menu;

public class MenuFont {
    private int id;
    private String name, path;

    public MenuFont(int id, String name, String path) {
        this.id = id;
        this.name = name;
        this.path = path;
    }

    public MenuFont(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MenuFont) {
            return ((MenuFont) obj).getName().equals(this.name);
        } else
            return super.equals(obj);
    }
}
