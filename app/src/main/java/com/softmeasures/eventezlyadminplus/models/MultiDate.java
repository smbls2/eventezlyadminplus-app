package com.softmeasures.eventezlyadminplus.models;

public class MultiDate {
    private String date, startTime, endTime;
    private boolean isSelected;
    private String startHour, endHour;
    private String startMinute, endMinute;
    private String startAMPM, endAMPM;

    public String getStartHour() {
        return startHour;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getEndHour() {
        return endHour;
    }

    public void setEndHour(String endHour) {
        this.endHour = endHour;
    }

    public String getStartMinute() {
        return startMinute;
    }

    public void setStartMinute(String startMinute) {
        this.startMinute = startMinute;
    }

    public String getEndMinute() {
        return endMinute;
    }

    public void setEndMinute(String endMinute) {
        this.endMinute = endMinute;
    }

    public String getStartAMPM() {
        return startAMPM;
    }

    public void setStartAMPM(String startAMPM) {
        this.startAMPM = startAMPM;
    }

    public String getEndAMPM() {
        return endAMPM;
    }

    public void setEndAMPM(String endAMPM) {
        this.endAMPM = endAMPM;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
