package com.softmeasures.eventezlyadminplus.models.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    public static final String BASE_URL = "http://34.226.69.26/api/v2/";
    private static Retrofit retrofit = null;
    private static Retrofit googleRetrofit = null;

    public static Retrofit getgoogleClient() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        if (googleRetrofit == null) {
            googleRetrofit = new Retrofit.Builder()
                    .baseUrl("https://maps.googleapis.com/maps/api/")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return googleRetrofit;
    }

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}