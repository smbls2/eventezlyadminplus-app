package com.softmeasures.eventezlyadminplus.models.users;

import java.util.List;

public class DeptUser {

    private List<DeptUserData> resource = null;

    public List<DeptUserData> getResource() {
        return resource;
    }

    public void setResource(List<DeptUserData> resource) {
        this.resource = resource;
    }

    public class DeptUserData {

        private int id;
        private String dateTime;
        private int userId;
        private String userName;
        private String email;
        private int deptNum;
        private String deptCode;
        private String dept;
        private String profileName;
        private String status;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDateTime() {
            return dateTime;
        }

        public void setDateTime(String dateTime) {
            this.dateTime = dateTime;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public int getDeptNum() {
            return deptNum;
        }

        public void setDeptNum(int deptNum) {
            this.deptNum = deptNum;
        }

        public String getDeptCode() {
            return deptCode;
        }

        public void setDeptCode(String deptCode) {
            this.deptCode = deptCode;
        }

        public String getDept() {
            return dept;
        }

        public void setDept(String dept) {
            this.dept = dept;
        }

        public String getProfileName() {
            return profileName;
        }

        public void setProfileName(String profileName) {
            this.profileName = profileName;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
