package com.softmeasures.eventezlyadminplus.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.MyApplication;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.models.menu.AppName;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

public class MainActivity extends AppCompatActivity {
    Double latitude = 0.0, longitude = 0.0;
    private AppName appName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Fabric.with(this, new Crashlytics());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        setContentView(R.layout.activity_flashscreen);

        if (!isInernet(getApplicationContext())) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setCancelable(false);
            alertDialog.setTitle("No internet Connection.");
            alertDialog.setMessage("Please check your connection status and try again.");
            alertDialog.setPositiveButton("Exit", (dialog, which) -> finish());
            alertDialog.show();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (permissions.length == 1 && permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && permissions.length == 1 && permissions[1] == Manifest.permission.WRITE_EXTERNAL_STORAGE && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && permissions[2] == Manifest.permission.VIBRATE && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && permissions[2] == Manifest.permission.CAMERA && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && permissions[2] == Manifest.permission.RECORD_AUDIO && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && permissions[2] == Manifest.permission.READ_EXTERNAL_STORAGE && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                Log.e("pe", "yes");
            } else {

            }
        } else {
            Log.e("pe1", "no1");
        }
    }

    public void currentlocation() {
        GPSTracker tracker = new GPSTracker(this);
        if (tracker.canGetLocation()) {
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
            System.gc();
            check_permission();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Failed to fetch user location!");
            alertDialog.setMessage("Please enable user location for app, location is required for app to work properly");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            alertDialog.show();
        }
    }

    @Override
    protected void onResume() {
        if (latitude == 0.0) {
            currentlocation();
        } else {
            check_permission();
        }
        super.onResume();
    }

    public void check_permission() {
        if ((ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {

            if ((ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) && (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) && (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) && (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.VIBRATE)) && (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.CAMERA))) {

                new fetchAppMenuConfig().execute();

            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.VIBRATE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE}, 1);
            }
        } else {
            if (latitude != 0.0) {
                new fetchAppMenuConfig().execute();
            } else {
                currentlocation();
                System.gc();
            }
        }
    }

    public static boolean isInernet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public class fetchAppMenuConfig extends AsyncTask<String, String, String> {
        JSONObject json = null;
        String appList = "_table/app_menu_config";
        JSONArray array;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = null;
            try {
                url = getString(R.string.api) + getString(R.string.povlive) + appList + "?filter=(app_id%3D'" + URLEncoder.encode("23", "utf-8") + "')";
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    Log.e("response ", String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        appName = new AppName();
                        appName.setId(c.getInt("id"));
                        appName.setApp_id(c.getString("app_id"));
                        appName.setApp_name(c.getString("app_name"));

                        if (c.has("menu_icon_pixel")
                                && !TextUtils.isEmpty(c.getString("menu_icon_pixel"))
                                && !c.getString("menu_icon_pixel").equals("null")) {
                            appName.setMenu_icon_pixel(c.getString("menu_icon_pixel"));
                        }

                        if (c.has("menu_font")
                                && !TextUtils.isEmpty(c.getString("menu_font"))
                                && !c.getString("menu_font").equals("null")) {
                            appName.setMenu_font(c.getString("menu_font"));
                        }

                        if (c.has("menu_font_size")
                                && !TextUtils.isEmpty(c.getString("menu_font_size"))
                                && !c.getString("menu_font_size").equals("null")) {
                            appName.setMenu_font_size(c.getString("menu_font_size"));
                        }

                        if (c.has("menu_font_style")
                                && !TextUtils.isEmpty(c.getString("menu_font_style"))
                                && !c.getString("menu_font_style").equals("null")) {
                            appName.setMenu_font_style(c.getString("menu_font_style"));
                        }

                        if (c.has("menu_color")
                                && !TextUtils.isEmpty(c.getString("menu_color"))
                                && !c.getString("menu_color").equals("null")) {
                            appName.setMenu_color(c.getString("menu_color"));
                        }

                        if (c.has("menu_style")
                                && !TextUtils.isEmpty(c.getString("menu_style"))
                                && !c.getString("menu_style").equals("null")) {
                            appName.setMenu_style(c.getString("menu_style"));
                        }

                        if (c.has("menu_bg_color")
                                && !TextUtils.isEmpty(c.getString("menu_bg_color"))
                                && !c.getString("menu_bg_color").equals("null")) {
                            appName.setMenu_bg_color(c.getString("menu_bg_color"));
                        }

                        if (c.has("menu_bg_img")
                                && !TextUtils.isEmpty(c.getString("menu_bg_img"))
                                && !c.getString("menu_bg_img").equals("null")) {
                            appName.setMenu_bg_img(c.getString("menu_bg_img"));
                        }

                        if (c.has("menu_screen_size")
                                && !TextUtils.isEmpty(c.getString("menu_screen_size"))
                                && !c.getString("menu_screen_size").equals("null")) {
                            appName.setMenu_screen_size(c.getString("menu_screen_size"));
                        }

                        if (c.has("menu_list_alignment")
                                && !TextUtils.isEmpty(c.getString("menu_list_alignment"))
                                && !c.getString("menu_list_alignment").equals("null")) {
                            appName.setMenu_list_alignment(c.getString("menu_list_alignment"));
                        }

                        if (c.has("menu_default")) {
                            appName.setMenu_default(c.getBoolean("menu_default"));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            rlProgressbar.setVisibility(View.GONE);
            if (appName != null) {
                vchome.appName = appName;
                Log.e("#DEBUG", "   AppName:  " + new Gson().toJson(appName));
                if (appName.isMenu_default()) {
                    MyApplication.getInstance().setMenuDefault(true);
                } else {
                    MyApplication.getInstance().setMenuDefault(false);
                }

                if (!TextUtils.isEmpty(appName.getMenu_bg_img())) {
//                    TODO replace contains("http")
                    if (appName.getMenu_bg_img().contains("http")) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    URL url = new URL(appName.getMenu_bg_img());
                                    Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                                    vchome.menuBgDrawable = new BitmapDrawable(getResources(), image);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Intent i = new Intent(MainActivity.this, vchome.class);
                                            startActivity(i);
                                            finish();
                                        }
                                    });
                                } catch (IOException e) {
                                    System.out.println(e);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Intent i = new Intent(MainActivity.this, vchome.class);
                                            startActivity(i);
                                            finish();
                                        }
                                    });
                                    Log.e("#DEBUG", "   onPost:  imageLoadError: " + e.getMessage());
                                }
                            }
                        }).start();
                    } else {
                        Intent i = new Intent(MainActivity.this, vchome.class);
                        startActivity(i);
                        finish();
                    }
                } else {
                    Intent i = new Intent(MainActivity.this, vchome.class);
                    startActivity(i);
                    finish();
                }
            }
        }
    }
}