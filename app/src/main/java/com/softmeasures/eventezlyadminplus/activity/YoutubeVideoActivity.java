package com.softmeasures.eventezlyadminplus.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.databinding.ActivityYoutubeVideoBinding;

public class YoutubeVideoActivity extends AppCompatActivity {
    private String API_KEY = "AIzaSyC8Fb51iw17d7XzXys8P2VePXjCNAksE7k";
    //https://www.youtube.com/watch?v=<VIDEO_ID>
    public String VIDEO_ID = "-m3V8w_7vhk";

    ActivityYoutubeVideoBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_youtube_video);

        Intent videoIntent = getIntent();
        String url = videoIntent.getStringExtra("videoURL");
        if (url.contains("=")) {
            String ary[] = url.split("=");
            VIDEO_ID = ary[ary.length - 1];
        } else {
            String ary[] = url.split("/");
            VIDEO_ID = ary[ary.length - 1];
        }

        Log.d("VideoId", "url" + url);
        Log.d("VideoId", VIDEO_ID);
        //Initializing and adding YouTubePlayerFragment
        FragmentManager fm = getFragmentManager();
        String tag = YouTubePlayerFragment.class.getSimpleName();
        YouTubePlayerFragment playerFragment = (YouTubePlayerFragment) fm.findFragmentByTag(tag);
        if (playerFragment == null) {
            FragmentTransaction ft = fm.beginTransaction();
            playerFragment = YouTubePlayerFragment.newInstance();
            ft.add(R.id.video_content, playerFragment, tag);
            ft.commit();
        }

        playerFragment.initialize(API_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.cueVideo(VIDEO_ID);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Toast.makeText(YoutubeVideoActivity.this, "Error while initializing YouTubePlayer.", Toast.LENGTH_SHORT).show();
            }
        });

        binding.rlBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}