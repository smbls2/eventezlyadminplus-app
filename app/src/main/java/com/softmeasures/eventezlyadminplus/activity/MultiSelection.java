package com.softmeasures.eventezlyadminplus.activity;

public class MultiSelection {

    String f_Path, f_Name, f_Com;

    public MultiSelection() {

    }

    public String getF_Path() {
        return f_Path;
    }

    public void setF_Path(String f_Path) {
        this.f_Path = f_Path;
    }

    public String getF_Name() {
        return f_Name;
    }

    public void setF_Name(String f_Name) {
        this.f_Name = f_Name;
    }

    public String getF_Com() {
        return f_Com;
    }

    public void setF_Com(String f_Com) {
        this.f_Com = f_Com;
    }
}
