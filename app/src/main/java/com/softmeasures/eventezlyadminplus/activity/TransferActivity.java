package com.softmeasures.eventezlyadminplus.activity;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseActivity;
import com.softmeasures.eventezlyadminplus.databinding.ActivityTransferBinding;
import com.softmeasures.eventezlyadminplus.frament.g_classroom.GClassRoomAddCourseFragment;
import com.softmeasures.eventezlyadminplus.frament.g_classroom.GClassRoomCourseDetailsFragment;
import com.softmeasures.eventezlyadminplus.frament.g_classroom.GClassRoomCoursesFragment;
import com.softmeasures.eventezlyadminplus.frament.g_youtube.GYoutubeLiveStreamsFragment;
import com.softmeasures.eventezlyadminplus.frament.g_youtube.GYoutubeScheduleStreamFragment;

public class TransferActivity extends BaseActivity {

    private ActivityTransferBinding binding;

    private String type = "";
    private Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_transfer);

        type = getIntent().getStringExtra("type");

        initViews();
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {

        if (type.equalsIgnoreCase("courses")) {
            fragment = new GClassRoomCoursesFragment();
            if (getIntent().getExtras() != null) {
                fragment.setArguments(getIntent().getExtras());
            }
        } else if (type.equalsIgnoreCase("addCourse")) {
            fragment = new GClassRoomAddCourseFragment();
            if (getIntent().getExtras() != null) {
                fragment.setArguments(getIntent().getExtras());
            }
        } else if (type.equalsIgnoreCase("courseDetails")) {
            fragment = new GClassRoomCourseDetailsFragment();
            if (getIntent().getExtras() != null) {
                fragment.setArguments(getIntent().getExtras());
            }
        } else if (type.equalsIgnoreCase("youtubeLiveStreams")) {
            fragment = new GYoutubeLiveStreamsFragment();
            if (getIntent().getExtras() != null) {
                fragment.setArguments(getIntent().getExtras());
            }
        } else if (type.equalsIgnoreCase("youtubeScheduleStream")) {
            fragment = new GYoutubeScheduleStreamFragment();
            if (getIntent().getExtras() != null) {
                fragment.setArguments(getIntent().getExtras());
            }
        }

        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.flContainer, fragment)
                    .commitAllowingStateLoss();
        }

    }

    @Override
    protected void setListeners() {

        binding.rlBtnBack.setOnClickListener(view -> {
            onBackPressed();
        });

    }

    @Override
    protected void initViews() {

    }
}