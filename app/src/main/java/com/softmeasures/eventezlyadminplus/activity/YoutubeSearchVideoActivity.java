package com.softmeasures.eventezlyadminplus.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.bumptech.glide.Glide;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseActivity;
import com.softmeasures.eventezlyadminplus.databinding.ActivityYoutubeSearchVideoBinding;
import com.softmeasures.eventezlyadminplus.models.VideoItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class YoutubeSearchVideoActivity extends BaseActivity {
    ActivityYoutubeSearchVideoBinding binding;
    private ProgressDialog progressDialog;
    private VideoAdapter youtubeAdapter;
    private ArrayList<VideoItem> videos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_youtube_search_video);
        initViews();
        updateViews();
        setListeners();
    }

    private void searchVideo(String query) {

        progressDialog.show();
        videos.clear();
        AndroidNetworking.get("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=25&q=" + query + "&key=" + getString(R.string.key))
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject json = new JSONObject(response);
                            JSONArray array = json.getJSONArray("items");
                            Log.d("ResponseMSG", "Length: " + array.length());
                            for (int i = 0; i < array.length(); i++) {
                                VideoItem item = new VideoItem();
                                JSONObject josnget = array.getJSONObject(i);
                                JSONObject id = josnget.getJSONObject("id");
                                if (id.has("videoId")) {
                                    JSONObject snippet = josnget.getJSONObject("snippet");
                                    JSONObject thumbnails = snippet.getJSONObject("thumbnails");
                                    JSONObject defaultObj = thumbnails.getJSONObject("medium");
                                    item.setId(id.getString("videoId"));
                                    item.setTitle(snippet.getString("title"));
                                    item.setDescription(snippet.getString("description"));
                                    item.setThumbnailURL(defaultObj.getString("url"));
                                    videos.add(item);
                                }
                            }
                            youtubeAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                    }
                });
    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {
        binding.rlBtnBack.setOnClickListener(v -> finish());

        binding.searchVideo.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                binding.searchVideo.clearFocus();
                searchVideo(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    @Override
    protected void initViews() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Searching...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        youtubeAdapter = new VideoAdapter();
        binding.recyclerView.setAdapter(youtubeAdapter);
    }

    public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.MyViewHolder> {

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(LayoutInflater.from(activity).inflate(R.layout.video_item, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            VideoItem singleVideo = videos.get(position);

            holder.video_title.setText(singleVideo.getTitle());
            holder.video_description.setText(singleVideo.getDescription());
            Glide.with(activity)
                    .load(singleVideo.getThumbnailURL())
                    .centerCrop()
                    .into(holder.thumbnail);
        }

        @Override
        public int getItemCount() {
            return videos.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView thumbnail;
            public TextView video_title, video_description;

            public MyViewHolder(@NonNull View view) {
                super(view);
                thumbnail = (ImageView) view.findViewById(R.id.video_thumbnail);
                video_title = (TextView) view.findViewById(R.id.video_title);
                video_description = (TextView) view.findViewById(R.id.video_description);

                itemView.setOnClickListener(view1 -> {
//                    myApp.setVideoId(videos.get(getAdapterPosition()).getId());
                    setResult(RESULT_OK, new Intent()
                            .putExtra("URL", videos.get(getAdapterPosition()).getId())
                            .putExtra("Title", videos.get(getAdapterPosition()).getTitle())
                    );
                    finish();
                });
            }
        }
    }
}