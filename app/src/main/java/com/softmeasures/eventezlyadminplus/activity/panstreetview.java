package com.softmeasures.eventezlyadminplus.activity;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;
import com.google.android.gms.maps.model.StreetViewPanoramaLocation;
import com.google.android.gms.maps.model.StreetViewPanoramaOrientation;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;

public class panstreetview extends AppCompatActivity implements StreetViewPanorama.OnStreetViewPanoramaChangeListener, StreetViewPanorama.OnStreetViewPanoramaCameraChangeListener, StreetViewPanorama.OnStreetViewPanoramaClickListener, StreetViewPanorama.OnStreetViewPanoramaLongClickListener {

    SupportStreetViewPanoramaFragment streetViewPanoramaFragment;
    ConnectionDetector cd;
    RelativeLayout rlback;
    private StreetViewPanorama mStreetViewPanorama;
    TextView texttitleaddrtess;
    LatLng SYDNEY;
    String address1, address;
    RelativeLayout rl_progressbar;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.street_view_panorama_events_demo);
        texttitleaddrtess = (TextView) findViewById(R.id.addresstitle);
        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeue-Light.otf");
        texttitleaddrtess.setTypeface(type);
        rl_progressbar = (RelativeLayout) findViewById(R.id.rl_progressbar);
        cd = new ConnectionDetector(this);
        if (cd.isConnectingToInternet()) {
            try {
                if (getIntent().getExtras() != null) {
                    Double lat = Double.parseDouble(getIntent().getStringExtra("lat"));
                    Double lang = Double.parseDouble(getIntent().getStringExtra("lang"));
                    address = getIntent().getStringExtra("title");
                    address1 = getIntent().getStringExtra("address");
                    texttitleaddrtess.setText(address + "\n" + address1);
                    SYDNEY = new LatLng(lat, lang);
                } else {
                    SYDNEY = new LatLng(-33.87365, 151.20689);
                }
            } catch (Exception e) {

                SYDNEY = new LatLng(-33.87365, 151.20689);
            }

            rlback = (RelativeLayout) findViewById(R.id.back);
            rlback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });


            streetViewPanoramaFragment =
                    (SupportStreetViewPanoramaFragment)
                            getSupportFragmentManager().findFragmentById(R.id.streetviewpanorama);
            streetViewPanoramaFragment.getStreetViewPanoramaAsync(
                    new OnStreetViewPanoramaReadyCallback() {
                        @Override
                        public void onStreetViewPanoramaReady(StreetViewPanorama panorama) {
                            mStreetViewPanorama = panorama;
                            mStreetViewPanorama.setOnStreetViewPanoramaChangeListener(
                                    panstreetview.this);
                            mStreetViewPanorama.setOnStreetViewPanoramaCameraChangeListener(
                                    panstreetview.this);
                            mStreetViewPanorama.setOnStreetViewPanoramaClickListener(
                                    panstreetview.this);
                            mStreetViewPanorama.setOnStreetViewPanoramaLongClickListener(
                                    panstreetview.this);
                            if (savedInstanceState == null) {
                                mStreetViewPanorama.setPosition(SYDNEY);
                            }
                        }
                    });
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }
    }

    @Override
    public void onStreetViewPanoramaChange(StreetViewPanoramaLocation streetViewPanoramaLocation) {
        if (streetViewPanoramaLocation != null && streetViewPanoramaLocation.links != null) {
            Log.e("park for free", "location  changed");
            rl_progressbar.setVisibility(View.GONE);
        } else {

            Log.e("park for free", "location  not chmage");
            rl_progressbar.setVisibility(View.VISIBLE);
            final Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (rl_progressbar.getVisibility()==View.VISIBLE)
                        //  Toast.makeText(panstreetview.this, "Streetview not Available for this location.", Toast.LENGTH_LONG).show();
                        Log.d("10_sec_completed","====");
                        rl_progressbar.setVisibility(View.GONE);
                    Toast.makeText(panstreetview.this, "Streetview not available for this location. ", Toast.LENGTH_SHORT).show();
                    panstreetview.super.onBackPressed();
                }
            }, 10000);
        }
    }

    @Override
    public void onStreetViewPanoramaCameraChange(StreetViewPanoramaCamera camera) {
        //  mPanoCameraChangeTextView.setText("Times camera changed=" + ++mPanoCameraChangeTimes);
    }

    @Override
    public void onStreetViewPanoramaClick(StreetViewPanoramaOrientation orientation) {
        Point point = mStreetViewPanorama.orientationToPoint(orientation);
        if (point != null) {
            mStreetViewPanorama.animateTo(
                    new StreetViewPanoramaCamera.Builder()
                            .orientation(orientation)
                            .zoom(mStreetViewPanorama.getPanoramaCamera().zoom)
                            .build(), 1000);
        }
    }

    @Override
    public void onStreetViewPanoramaLongClick(StreetViewPanoramaOrientation orientation) {
        Point point = mStreetViewPanorama.orientationToPoint(orientation);
        if (point != null) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
