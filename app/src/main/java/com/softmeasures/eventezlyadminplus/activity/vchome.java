package com.softmeasures.eventezlyadminplus.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.multidex.MultiDex;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseActivity;
import com.softmeasures.eventezlyadminplus.frament.Forgot_Password;
import com.softmeasures.eventezlyadminplus.frament.config_setting.ConfigSettingTypeFragment;
import com.softmeasures.eventezlyadminplus.frament.event.AllEventsFragment;
import com.softmeasures.eventezlyadminplus.frament.event_reg.EventsMapFragment;
import com.softmeasures.eventezlyadminplus.frament.login_fragment;
import com.softmeasures.eventezlyadminplus.frament.user_profile.Frg_User_profile;
import com.softmeasures.eventezlyadminplus.java.State_model;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.Manager;
import com.softmeasures.eventezlyadminplus.models.MenuConfig;
import com.softmeasures.eventezlyadminplus.models.menu.AppName;
import com.softmeasures.eventezlyadminplus.models.menu.MenuFont;
import com.softmeasures.eventezlyadminplus.services.Constants;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import static com.softmeasures.eventezlyadminplus.services.Constants.MENU_BG_COLOR;
import static com.softmeasures.eventezlyadminplus.services.Constants.MENU_BG_CUSTOM;
import static com.softmeasures.eventezlyadminplus.services.Constants.MENU_BG_MAP;

public class vchome extends BaseActivity implements login_fragment.Login_listener {

    //    static Context cont;
    static int notification_id = 0;
    RelativeLayout menu, loginmenu, myaccountmenu, imgmenu, rl_progressbar, notificationsmenu;
    ListView list_loging_menu, list_account_menu, list_without_login_menu, list_notifications_menu;
    RecyclerView rvLoginMenu, rvNotificationMenu, rvAccountMenu;
    ArrayList<item> menu_array_list = new ArrayList<>();
    ArrayList<item> account_menu_array_list = new ArrayList<>();
    ArrayList<item> notifications_menu_array_list = new ArrayList<>();
    ArrayList<item> array_list_without_login = new ArrayList<>();
    TextView txtactionbartitle;
    boolean result = true;
    String session_id, role, user_id, email;
    //    FragmentTransaction transaction;
    FragmentManager manager;
    FrameLayout fram;
    RelativeLayout btnsearch, btnback;
    ProgressBar progressBar;
    //int h = 0, z = 0;
    boolean currenlocationis = true;
    /*Double latitude=33.517694,longitude=-86.804991;*/
    public static Double latitude = 0.0, longitude = 0.0;
    String lat = "null", lang = "null";
    Double centerlat = 0.0, centerlang = 0.0;
    FrameLayout view;
    Animation animation;
    public static boolean parkhere = false, markerclick = false;//, Doyouwatregister = false;
    boolean mapisloadcallback = false, managedmapisloaded = false, managedpinisclick = false;
    //    boolean Townshipback = false;
    /*  static String parking_id = "0";
      private Handler mHandler = null;
      private Runnable mAnimation;
      boolean Allparking_Nearby_click = false;*/
    Context context;

    static public Stack<Fragment> fragmentStack;
    public static String adrreslocation;
    public static ArrayList<State_model> all_state = new ArrayList<>();
    SharedPreferences logindeatl;

    private List<MenuConfig> menuConfigsAll = new ArrayList<>();
    private List<MenuConfig> menuConfigsMainMenu = new ArrayList<>();
    private List<MenuConfig> menuConfigsSubMenu = new ArrayList<>();
//    private String location_id = "";

    //Event Search
    private RelativeLayout rlBtnSearchEvent;
    private ImageView ivBtnEventSearch;
    protected Manager twpManager;

    //menu
    public static AppName appName = null;
    public static Drawable menuBgDrawable = null;
    //    public static int USER_TYPE = 0;
    public static boolean isMenuListing = false;
    private ArrayList<MenuFont> menuFonts = new ArrayList<>();
    private int menuFontPosition = 0, bgImageOption = 0;
    public AlertDialog gridMenuDialog;
    private boolean isMyAccountItemClicked = false, isHasBgMenuImage = false;
    private String menuClick = "home";
    private boolean is_menu_opne = false;
    private boolean isNotificationShowing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MultiDex.install(this);
        setContentView(R.layout.activity_main);
        /*context = this;
        cont = this;*/
        initViews();

        updateViews();
        setListeners();
        //ActionStartsHere();
        logindeatl = getSharedPreferences("login", MODE_PRIVATE);
        user_id = logindeatl.getString("id", "null");
        role = logindeatl.getString("role", "");
        email = logindeatl.getString("email", "");
        session_id = logindeatl.getString("session_id", null);

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        //  checkCurrentUser();
        if (user_id.equals("null")) {
            imgmenu.setVisibility(View.GONE);
            btnsearch.setVisibility(View.GONE);
            fragmentStack.clear();
            login_fragment pay = new login_fragment();
            pay.registerForListener(this);
            FragmentTransaction ft_schedule = manager.beginTransaction();
            ft_schedule.add(R.id.My_Container_1_ID, pay, "home");
            fragmentStack.push(pay);
            ft_schedule.commitAllowingStateLoss();
        } else {
            new fetchMenus().execute("TwpAdmin"); //SuperAdmin
            new fetchManagers().execute();

            fragmentStack.clear();
            EventsMapFragment pay = new EventsMapFragment();
            Bundle bundle = new Bundle();
            bundle.putString("parkingType", "All");
            pay.setArguments(bundle);
            FragmentTransaction ft_schedule = manager.beginTransaction();
            ft_schedule.add(R.id.My_Container_1_ID, pay, "home");
            fragmentStack.push(pay);
            ft_schedule.commitAllowingStateLoss();
        }

        if (myApp.cd.isConnectingToInternet()) {
            //  new fetchMenus().execute("TwpAdmin");
            array_list_without_login = Login_menu();
            if (array_list_without_login != null) {
                list_without_login_menu.setAdapter(new CustomAdapterMenu(vchome.this, array_list_without_login, getResources()));
            }
            list_without_login_menu.setOnItemClickListener((parent, view1, position, id) -> Login_click(position));
            list_loging_menu.setOnItemClickListener((parent, view1, position, id) -> TwpAdmin_Click(position));
            /*if (user_id.equals("null")) {
                animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.sidepannelright);
                menu.setVisibility(View.VISIBLE); //Login Menu
                menu.startAnimation(animation);
                loginmenu.setVisibility(View.GONE);
                btnback.setVisibility(View.VISIBLE);
                btnsearch.setVisibility(View.GONE);
            } else {
                animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.sidepannelright);
                menu.setVisibility(View.GONE);
                 loginmenu.setVisibility(View.VISIBLE);
                loginmenu.startAnimation(animation);
                btnback.setVisibility(View.VISIBLE);
                btnsearch.setVisibility(View.GONE);
                Log.d("#DEBUG", "userId" + user_id + " " + myApp.getUserId());
            }*/
            btnback.setOnClickListener((v) -> onBackPressed());
            //menu buuton
            imgmenu.setOnClickListener((v) -> {
                if (!isMenuListing) {
                    showFullMenuGridDialog();
                } else {
                    menu();
                    /*animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.sidepannelright);
                    btnback.setVisibility(View.VISIBLE);
                    btnsearch.setVisibility(View.GONE);
                    myaccountmenu.setVisibility(View.GONE);
                    notificationsmenu.setVisibility(View.GONE);
                    loginmenu.setVisibility(View.VISIBLE);
                    loginmenu.startAnimation(animation);
                    result = true;*/
                }
            });
        } else showDialogInternetRequired();
    }

    @Override
    public void onBackPressed() {
        Log.d("#DEBUG", "Result: " + result);
        EventsMapFragment eventsMapFragment = (EventsMapFragment) getSupportFragmentManager().findFragmentByTag("event_map");
        if (eventsMapFragment != null && eventsMapFragment.binding != null) {
            Log.e("#DEBUG", "1");
            rlBtnSearchEvent.setVisibility(View.GONE);
            if (eventsMapFragment.binding.llEventSearch.getVisibility() == View.VISIBLE) {
                eventsMapFragment.binding.llEventSearch.setVisibility(View.GONE);
                ivBtnEventSearch.setImageDrawable(ContextCompat.getDrawable(vchome.this, R.drawable.places_ic_search));
                return;
            }
        }

        View view = this.getCurrentFocus();
        if (view != null) {
            Log.e("#DEBUG", "2");
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        if ((gridMenuDialog != null && gridMenuDialog.isShowing())) {
            if (gridMenuDialog != null && gridMenuDialog.isShowing()) {
                gridMenuDialog.dismiss();
            }
            if (fragmentStack.size() > 1) {
                btnback.setVisibility(View.VISIBLE);
            } else {
                btnback.setVisibility(View.GONE);
                Log.e("#DEBUG", "10");
                androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", (dialog1, which) -> finish())
                        .setNegativeButton("No", (dialog1, which) -> dialog1.cancel());
                androidx.appcompat.app.AlertDialog alert = builder.create();
                alert.show();
            }
        } else if (fragmentStack.size() > 1) {
            Log.e("#DEBUG", "3 " + fragmentStack.size());
            if (fragmentStack.size() == 2) {
                imgmenu.performClick();
//                btnback.setVisibility(View.GONE);
            }
            FragmentTransaction ft = manager.beginTransaction();
            fragmentStack.lastElement().onPause();
            ft.remove(fragmentStack.pop());
            fragmentStack.lastElement().onResume();
            ft.show(fragmentStack.lastElement());
            ft.commit();
        } else {
            Log.e("#DEBUG", "4");
            animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.sidepannelleft);
            btnback.setVisibility(View.GONE);
            if (fram.getVisibility() == View.VISIBLE && result == true) {
                Log.e("#DEBUG", "5");
                /*fram.setVisibility(View.VISIBLE);
                fram.startAnimation(animation);*/
                loginmenu.setVisibility(View.GONE);
                myaccountmenu.setVisibility(View.GONE);
                menu.setVisibility(View.GONE);
                notificationsmenu.setVisibility(View.GONE);
                btnsearch.setVisibility(View.GONE);
                txtactionbartitle.setVisibility(View.VISIBLE);

                isNotificationShowing = false;
                result = false;
            } else if (result == true) {
                Log.e("#DEBUG", "6");
                fram.setVisibility(View.VISIBLE);
                fram.startAnimation(animation);
                menu.setVisibility(View.GONE);
                if (myaccountmenu.getVisibility() == View.VISIBLE) {
                    Log.e("#DEBUG", "7");
                    myaccountmenu.setVisibility(View.GONE);
                    btnsearch.setVisibility(View.VISIBLE);
                } else if (notificationsmenu.getVisibility() == View.VISIBLE) {
                    Log.e("#DEBUG", "8");
                    notificationsmenu.setVisibility(View.GONE);
                    btnsearch.setVisibility(View.VISIBLE);
                } else {
                    Log.e("#DEBUG", "9");
                    loginmenu.setVisibility(View.GONE);
                    btnsearch.setVisibility(View.VISIBLE);
                }
                txtactionbartitle.setVisibility(View.VISIBLE);

                isNotificationShowing = false;
                result = false;
            } else {
                Log.e("#DEBUG", "10");
                androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", (dialog1, which) -> finish())
                        .setNegativeButton("No", (dialog1, which) -> dialog1.cancel());
                androidx.appcompat.app.AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    @Override
    protected void onResume() {
        /*SharedPreferences sh = getSharedPreferences("login", MODE_PRIVATE);
        session_id = sh.getString("session_id", null);
        String role = sh.getString("role", "");*/
        if ((ContextCompat.checkSelfPermission(vchome.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(vchome.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(vchome.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {

            if ((ActivityCompat.shouldShowRequestPermissionRationale(vchome.this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) && (ActivityCompat.shouldShowRequestPermissionRationale(vchome.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) && (ActivityCompat.shouldShowRequestPermissionRationale(vchome.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) && (ActivityCompat.shouldShowRequestPermissionRationale(vchome.this,
                    Manifest.permission.VIBRATE)) && (ActivityCompat.shouldShowRequestPermissionRationale(vchome.this,
                    Manifest.permission.CAMERA))) {
                Log.d("permission", "yes");
                SharedPreferences payment = getSharedPreferences("paypalpaymentback", Context.MODE_PRIVATE);
                String paypal = payment.getString("paypalback", "null");
                if (!paypal.equals("null")) {
                    payment.edit().clear().commit();
                } else {
                    if (latitude == 0.0) {
                        int version = getAndroidVersion();
                        if (version >= 21) {
                            Log.e("version_is", "5");
                            int hh = getLocationMode();
                            if (hh == 3) {
                                currentlocation();
                                if (role.equals("TwpInspector") || role.equals("TwpAdmin")) {
                                    mapisloadcallback = false;
                                    managedmapisloaded = false;
                                    managedpinisclick = true;
                                } else {

                                }
                            } else {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                                alertDialog.setTitle("Failed to fetch user location!");
                                alertDialog.setMessage("Change your location mode to \"High Accuracy\"");
                                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                        startActivity(intent);
                                    }
                                });
                                alertDialog.show();
                            }
                        } else {
                            currentlocation();
                            if (role.equals("TwpInspector") || role.equals("TwpAdmin")) {
                                mapisloadcallback = false;
                                managedmapisloaded = false;
                                managedpinisclick = true;

                            } else {
                            }
                            Log.e("version_is", "4");
                        }
                    }
                    if (currenlocationis == true) {
                        if (latitude != 0.0) {
                            currenlocationis = false;
                        } else {
                            onResume();
                            Log.e("version_is", "on_Resume_call");
                        }
                    }
                }
            } else {
                ActivityCompat.requestPermissions(vchome.this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.VIBRATE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        } else {
            SharedPreferences payment = getSharedPreferences("paypalpaymentback", Context.MODE_PRIVATE);
            String paypal = payment.getString("paypalback", "null");
            if (!paypal.equals("null")) {
                payment.edit().clear().commit();
            } else {
                if (latitude == 0.0) {
                    int version = getAndroidVersion();
                    if (version >= 21) {
                        Log.e("version_is", "5");
                        int hh = getLocationMode();
                        if (hh == 3) {
                            currentlocation();
                            if (role.equals("TwpInspector") || role.equals("TwpAdmin")) {
                                mapisloadcallback = false;
                                managedmapisloaded = false;
                                managedpinisclick = true;
                            } else {
                            }
                            // new getparkingpin(String.valueOf(latitude), String.valueOf(longitude)).execute();
                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                            alertDialog.setTitle("Failed to fetch user location!");
                            alertDialog.setMessage("Change your location mode to \"High Accuracy\"");
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivity(intent);
                                }
                            });
                            alertDialog.show();
                        }
                    } else {
                        currentlocation();
                        //   new getparkingpin(String.valueOf(latitude), String.valueOf(longitude)).execute();
                        if (role.equals("TwpInspector") || role.equals("TwpAdmin")) {
                            mapisloadcallback = false;
                            managedmapisloaded = false;
                            managedpinisclick = true;
                        } else {

                        }
                        Log.e("version_is", "4");
                    }
                } else {
                    if (currenlocationis == true) {
                        if (latitude != 0.0) {

                            currenlocationis = false;
                        } else {
                            onResume();
                            Log.e("version_is", "on_Resume_call");
                        }
                    }
                }
            }
            Log.d("permission", "no");
        }
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (permissions.length == 1 && permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && permissions.length == 1 && permissions[1] == Manifest.permission.WRITE_EXTERNAL_STORAGE && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && permissions[2] == Manifest.permission.VIBRATE && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && permissions[2] == Manifest.permission.CAMERA && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && permissions[2] == Manifest.permission.RECORD_AUDIO && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && permissions[2] == Manifest.permission.READ_EXTERNAL_STORAGE && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                Log.e("pe", "yes");
            } else {
                Log.e("pe", "no");
            }
        } else {
            Log.e("pe1", "no1");
        }
    }

    /*  public void regiser() {
          fram.setVisibility(View.GONE);
          layoutmap.setVisibility(View.VISIBLE);
          txtactionbartitle.setVisibility(View.VISIBLE);
          txtsearchlabel.setVisibility(View.GONE);
          txtgo.setVisibility(View.GONE);
          editdirection.setVisibility(View.GONE);
          menu.setVisibility(View.GONE);

          //transitmenu.setVisibility(View.GONE);

          layoutmap.setVisibility(View.GONE);
          btnback.setVisibility(View.VISIBLE);
          btnsearch.setVisibility(View.GONE);
          Doyouwatregister = true;
          result = false;
      }*/
    /*
    public void ActionStartsHere() {
        againStartGPSAndSendFile();
    }

    public void againStartGPSAndSendFile() {
        new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                // Display Data by Every Ten Second
            }

            @Override
            public void onFinish() {
                currentlocation1();
                // Log.e("lat", String.valueOf(latitude));
                // Log.e("lang", String.valueOf(longitude));
                //  ActionStartsHere();

            }

        }.start();
    }

    public void dd() {
        ActionStartsHereNotification();
    }

    public static void ActionStartsHereNotification() {
        againStartGPSAndSendFilenotification();
    }

    public static void againStartGPSAndSendFilenotification() {
        new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                long s = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));// Display Data by Every Ten Second
                System.out.println("count_down" + s);
            }

            @Override
            public void onFinish() {
                Notification1(1, cont, parking_id);
            }

        }.start();
    }
*/
    @Override
    public void onLogin_success(String user_role) {
        //        new fetchMenus().execute("TwpAdmin");
        user_id = logindeatl.getString("id", "null");
        imgmenu.setVisibility(View.VISIBLE);
        new fetchMenus().execute("TwpAdmin"); //SuperAdmin
        new fetchManagers().execute();
        fragmentStack.clear();
        EventsMapFragment pay = new EventsMapFragment();
        Bundle bundle = new Bundle();
        bundle.putString("parkingType", "All");
        pay.setArguments(bundle);
        FragmentTransaction ft_schedule = manager.beginTransaction();
        ft_schedule.replace(R.id.My_Container_1_ID, pay, "home");
        fragmentStack.push(pay);
        ft_schedule.commitAllowingStateLoss();
        result = true;

        centerlang = 0.0;
        centerlat = 0.0;
        mapisloadcallback = false;
        managedmapisloaded = false;
        managedpinisclick = true;
        onBackPressed();
    }

    @Override
    public void onRegister_success() {
        new fetchMenus().execute("TwpAdmin");
//        checkCurrentUser();
//        menu_array_list = VehicleUser();
//        if (menu_array_list != null) {
//            list_loging_menu.setAdapter(new CustomAdapterMenu(vchome.this, menu_array_list, getResources()));
//        }
    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {

        rlBtnSearchEvent.setOnClickListener(v -> {

            try {
                EventsMapFragment eventsMapFragment = (EventsMapFragment) getSupportFragmentManager().findFragmentByTag("event_map");
                if (eventsMapFragment != null) {
                    if (eventsMapFragment.binding != null) {
                        if (eventsMapFragment.binding.llEventSearch.getVisibility() == View.VISIBLE) {
                            eventsMapFragment.binding.llEventSearch.setVisibility(View.GONE);
                            ivBtnEventSearch.setImageDrawable(ContextCompat.getDrawable(vchome.this, R.drawable.places_ic_search));
                        } else {
                            ivBtnEventSearch.setImageDrawable(ContextCompat.getDrawable(vchome.this, R.drawable.ic_close_white_24dp));
                            eventsMapFragment.searchClick();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
    }

    @Override
    protected void initViews() {

        rlBtnSearchEvent = findViewById(R.id.rlBtnSearchEvent);
        ivBtnEventSearch = findViewById(R.id.ivBtnEventSearch);

//        mHandler = new Handler();
        view = (FrameLayout) findViewById(R.id.My_Container_1_ID);
        fragmentStack = new Stack<Fragment>();
        //menu button
        imgmenu = findViewById(R.id.menu);
        menu = findViewById(R.id.loginmenu);
        loginmenu = findViewById(R.id.loginwithmenu);
        list_loging_menu = findViewById(R.id.list_loging_menu);
        rvLoginMenu = findViewById(R.id.rvLoginMenu);

        list_account_menu = findViewById(R.id.list_account);
        rvAccountMenu = findViewById(R.id.rvAccountMenu);

        list_notifications_menu = findViewById(R.id.list_notifications);
        rvNotificationMenu = findViewById(R.id.rvNotificationMenu);
        list_without_login_menu = findViewById(R.id.list_without_menu);

        progressBar = findViewById(R.id.progressbar);
        progressBar.setClickable(false);
        rl_progressbar = findViewById(R.id.rl_progressbar1);

        //action bar
        fram = (FrameLayout) findViewById(R.id.My_Container_1_ID);
        btnback = findViewById(R.id.back);
        btnsearch = findViewById(R.id.search);
        txtactionbartitle = findViewById(R.id.actionbartitle);
        final Typeface type = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeue-Thin.ttf");
        txtactionbartitle.setTypeface(type);

        //my account menu
        myaccountmenu = findViewById(R.id.myaccountmenu);
        //notifications menu
        notificationsmenu = findViewById(R.id.notificationsmenu);
        //drvive ezly menu opition with login
//        menudriveezly = findViewById(R.id.menudriveezly);
        manager = getSupportFragmentManager();
        menuFonts.clear();
        menuFonts.add(new MenuFont(0, "Helvetica-Neue-Light", "fonts/helvetica-neue-light.otf"));
        menuFonts.add(new MenuFont(1, "Helvetica-Neue-Medium", "fonts/helvetica-neue-medium.ttf"));
        menuFonts.add(new MenuFont(2, "Helvetica-Neue-Thin", "fonts/helvetica-neue-thin.ttf"));
        menuFonts.add(new MenuFont(3, "Helvetica-Normal", "fonts/helvetica-normal.ttf"));
        menuFonts.add(new MenuFont(4, "Lato-Regular", "fonts/lato-regular.ttf"));
        menuFonts.add(new MenuFont(5, "PlayFairDisplay-Regular", "fonts/playfair-display-regular.ttf"));
        menuFonts.add(new MenuFont(6, "Poppins-Regular", "fonts/poppins-regular.ttf"));
        menuFonts.add(new MenuFont(7, "PT-Serif-Web-Regular", "fonts/pt_serif-web-regular.ttf"));
        menuFonts.add(new MenuFont(8, "Roboto-Regular", "fonts/roboto-regular.ttf"));
    }

    @Override
    protected void onDestroy() {
        System.gc();
        super.onDestroy();
    }

    public void hide() {

        fragmentStack.clear();
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.sidepannelleft);
        fram.startAnimation(animation);
        fram.setVisibility(View.GONE);
        btnback.setVisibility(View.GONE);
        btnsearch.setVisibility(View.VISIBLE);

    }

    public void showmapandhidefragment() {
        SharedPreferences timere = getSharedPreferences("timer", Context.MODE_PRIVATE);
        String platno1 = timere.getString("platno", "");
        animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.sidepannelleft);
        fram.startAnimation(animation);

        fram.setVisibility(View.GONE);
        //googleMap.clear();
        btnback.setVisibility(View.GONE);
        btnsearch.setVisibility(View.VISIBLE);
        //  viewmap="null";
        //  showmap();
    }

    /*public void getcurrentlocation() {
        GPSTracker tracker = new GPSTracker(getApplicationContext());
        if (tracker.canGetLocation()) {

            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();

        }
    }*/
    public void popupopen() {
        parkhere = false;
    }

    /*public static class CounterClass extends CountDownTimer {
        String id;

        public CounterClass(long millisInFuture, long countDownInterval, String id) {
            super(millisInFuture, countDownInterval);
            this.id = id;
            parking_id = id;
        }

        @Override
        public void onFinish() {

        }


        @Override
        public void onTick(long millisUntilFinished) {

            long millis = millisUntilFinished;
            String hms = String.format("%02d : %02d : %02d", TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
            System.out.println(hms);
            long h = TimeUnit.MILLISECONDS.toHours(millis);
            long min = TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis));

            long s = TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis));
            if (h == 00 && min == 46 && s == 0) {
                //Notification(min - 1, cont,id);
            } else if (h == 00 && min == 31 && s == 0) {
                Notification(min - 1, cont, id);
            } else if (h == 00 && min == 16 && s == 0) {
                Notification(min - 1, cont, id);
            } else if (h == 00 && min == 6 && s == 0) {
                Notification(min - 1, cont, id);
            } else if (h == 00 && min == 00 && s == 00) {
                ActionStartsHereNotification();
            }
            System.out.println("home min" + min);
            System.out.println("home" + s);

        }
    }*/
    /* public double getDistance(LatLng LatLng1) {
        double distance = 0;
        Location locationA = new Location("A");
        locationA.setLatitude(LatLng1.latitude);
        locationA.setLongitude(LatLng1.longitude);
        Location locationB = new Location("B");
        locationB.setLatitude(latitude);
        locationB.setLongitude(longitude);
        distance = locationA.distanceTo(locationB);
        return distance;
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }
*/
    public int getAndroidVersion() {
        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        return sdkVersion;
    }

    public int getLocationMode() {
        try {
            return Settings.Secure.getInt(this.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static void Notification(long min, Context context, String id) {
        String strtitle = context.getString(R.string.customnotificationtitle);
        // Set Notification Text
        String strtext = "Alert Parkezly! Parking is going to expire in " + String.valueOf(min) + " mints.";
        Intent intent = new Intent(context.getApplicationContext(), vchome.class);
        intent.putExtra("title", strtitle);
        intent.putExtra("text", strtext);
        intent.putExtra("id", id);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        long[] vibrate = {0, 100, 200, 300};
        androidx.core.app.NotificationCompat.Builder builder = new NotificationCompat.Builder(context.getApplicationContext())
                .setSmallIcon(R.drawable.icon1)
                .setTicker(context.getString(R.string.customnotificationticker))
                .setVibrate(vibrate)
                .setContentIntent(pIntent)
                .setContentTitle(strtitle)
                .setContentText(strtext)
                .setAutoCancel(true);

        // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager

        notification_id++;
        notificationmanager.notify(100 + notification_id, builder.build());
        try {
            Uri notification1 = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context.getApplicationContext(), notification1);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void Notification1(long min, Context context, String id) {
        String strtitle = context.getString(R.string.customnotificationtitle);
        // Set Notification Text
        String strtext = "Alert Parkezly! Parking is almost expired.";
        Intent intent = new Intent(context.getApplicationContext(), vchome.class);
        intent.putExtra("title", strtitle);
        intent.putExtra("text", strtext);
        intent.putExtra("id", id);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        long[] vibrate = {0, 100, 200, 300};
        androidx.core.app.NotificationCompat.Builder builder = new NotificationCompat.Builder(context.getApplicationContext())
                .setSmallIcon(R.drawable.icon1)
                .setTicker(context.getString(R.string.customnotificationticker))
                .setVibrate(vibrate)
                .setContentIntent(pIntent)
                .setContentTitle(strtitle)
                .setContentText(strtext)
                .setAutoCancel(true);

        // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager

        notification_id++;
        notificationmanager.notify(100 + notification_id, builder.build());
        try {
            Uri notification1 = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context.getApplicationContext(), notification1);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void currentlocation() {
        GPSTracker tracker = new GPSTracker(this);
        if (tracker.canGetLocation()) {

            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
            // latitude =40.7333;
            // longitude = -73.445;
            currenlocationis = true;
            new fetchLatLongFromaddress1(String.valueOf(latitude), String.valueOf(longitude)).execute();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Failed to fetch user location!");
            alertDialog.setMessage("Please enable user location for app, location is required for app to work properly");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            alertDialog.show();
        }
    }

    public void currentlocation1() {
        GPSTracker tracker = new GPSTracker(this);
        if (tracker.canGetLocation()) {

            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
            // latitude =40.7333;
            // longitude = -73.445;

            currenlocationis = true;

        }
    }

    //menu coding
    public void menu() {
        Log.d("#DEBUG", "result: " + result);
        if (result) {
            animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.sidepannelright);
            if (user_id == null) {
                menu.setVisibility(View.VISIBLE);
                menu.startAnimation(animation);
                myaccountmenu.setVisibility(View.GONE);
                notificationsmenu.setVisibility(View.GONE);
                loginmenu.setVisibility(View.GONE);
            } else {
                menu.setVisibility(View.GONE);
                loginmenu.setVisibility(View.VISIBLE);
                loginmenu.startAnimation(animation);
//                menudriveezly.setVisibility(View.GONE);
                //transitmenu.setVisibility(View.GONE);
                myaccountmenu.setVisibility(View.GONE);
                notificationsmenu.setVisibility(View.GONE);
            }
            if (view.getVisibility() == View.VISIBLE) {
                btnback.setVisibility(View.VISIBLE);
                btnsearch.setVisibility(View.GONE);
            } else {
                btnback.setVisibility(View.GONE);
                btnsearch.setVisibility(View.VISIBLE);
            }
//            result = false;
        } else {
            animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.sidepannelright);
            if (user_id == null) {
                menu.setVisibility(View.VISIBLE);
                menu.startAnimation(animation);
            } else {
                loginmenu.setVisibility(View.VISIBLE);
                loginmenu.startAnimation(animation);
            }
            btnback.setVisibility(View.VISIBLE);
            btnsearch.setVisibility(View.GONE);
            result = true;
        }
    }

    private void checkCurrentUser() {
        Fragment fragment = null;
        Log.e("#DEBUG", "checkCurrentUser:  id:  " + myApp.getUserId()
                + "  name:  " + logindeatl.getString("name", null)
                + "   twcode:  " + logindeatl.getString("twcode", null)
                + "   role:  " + myApp.getRole() + "Login_role: " + myApp.getUserRole()
                + "   companyId:  " + logindeatl.getString("company_id", null));

        if (myApp.getUserId().equals(null) || myApp.getUserId().equals("null") || myApp.getUserId().equals("")) {
//            open_login_screen();
            btnsearch.setVisibility(View.GONE);
            fragmentStack.clear();
            fragment = new login_fragment();
            ((login_fragment) fragment).registerForListener(this);
            result = false;
        } else if (myApp.getUserRole().equalsIgnoreCase("SuperAdmin")) {
            new fetchMenus().execute("TwpAdmin"); //SuperAdmin
            new fetchManagers().execute();
            fragmentStack.clear();
            fragment = new EventsMapFragment();
            Bundle bundle = new Bundle();
            bundle.putString("parkingType", "All");
            fragment.setArguments(bundle);
        } else if (myApp.getUserRole().equalsIgnoreCase("TwpAdmin")) {
            new fetchMenus().execute("TwpAdmin");
            new fetchManagers().execute();
            fragmentStack.clear();
            fragment = new EventsMapFragment();
            Bundle bundle = new Bundle();
            bundle.putString("parkingType", "All");
            fragment.setArguments(bundle);
        }
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.My_Container_1_ID, fragment, "home")
                    .commitAllowingStateLoss();
            fragmentStack.push(fragment);
        }
    }

    public void showback() {
        btnback.setVisibility(View.VISIBLE);
        btnsearch.setVisibility(View.GONE);
    }

    public void menuitemclick() {
        currentlocation1();
        fram.setVisibility(View.GONE);

        SharedPreferences ticket = getSharedPreferences("ticket", MODE_PRIVATE);
        ticket.edit().clear().apply();
        SharedPreferences sh = getSharedPreferences("iscrateticketopen", MODE_PRIVATE);
        sh.edit().clear().apply();
        SharedPreferences gg = getSharedPreferences("vehicleinfo", MODE_PRIVATE);
        gg.edit().clear().apply();
        SharedPreferences add = getSharedPreferences("addvehicleback", Context.MODE_PRIVATE);
        add.edit().clear().apply();
        SharedPreferences violation = getSharedPreferences("violation", Context.MODE_PRIVATE);
        violation.edit().clear().apply();
        SharedPreferences sd = getSharedPreferences("directionback", Context.MODE_PRIVATE);
        sd.edit().clear().apply();
        SharedPreferences transit = getSharedPreferences("transitback", Context.MODE_PRIVATE);
        transit.edit().clear().apply();
        SharedPreferences cl = getSharedPreferences("priview", MODE_PRIVATE);
        SharedPreferences.Editor d = cl.edit();
        d.clear();
        d.apply();
        SharedPreferences backtotimer2 = getSharedPreferences("backtotimer", Context.MODE_PRIVATE);
        backtotimer2.edit().clear().apply();
        SharedPreferences sdg = getSharedPreferences("commercial_edit_back", Context.MODE_PRIVATE);
        sdg.edit().clear().apply();
        SharedPreferences sdg2 = getSharedPreferences("commercial_parking_rule", Context.MODE_PRIVATE);
        sdg2.edit().clear().apply();
        SharedPreferences ss = getSharedPreferences("back_rules", Context.MODE_PRIVATE);
        ss.edit().clear().apply();
        SharedPreferences editback = getSharedPreferences("private_edit_back", Context.MODE_PRIVATE);
        editback.edit().clear().apply();
        SharedPreferences private_editback = getSharedPreferences("Private_rules_back", Context.MODE_PRIVATE);
        private_editback.edit().clear().apply();
        SharedPreferences private_select = getSharedPreferences("private_select_location", Context.MODE_PRIVATE);
        private_select.edit().clear().apply();
        SharedPreferences lo = getSharedPreferences("location_code", Context.MODE_PRIVATE);
        lo.edit().clear().apply();
        SharedPreferences add_voice_back = getSharedPreferences("back_ticket", Context.MODE_PRIVATE);
        add_voice_back.edit().clear().apply();
        SharedPreferences getdatatt = getSharedPreferences("file_url", MODE_PRIVATE);
        getdatatt.edit().clear().apply();
        SharedPreferences audio = getSharedPreferences("back_add_audio", Context.MODE_PRIVATE);
        audio.edit().clear().apply();
        SharedPreferences back_select = getSharedPreferences("back_parking_info", Context.MODE_PRIVATE);
        back_select.edit().clear().apply();
        SharedPreferences back_select_vehcle = getSharedPreferences("back_paid_parking", Context.MODE_PRIVATE);
        back_select_vehcle.edit().clear().apply();
        SharedPreferences back_select_free = getSharedPreferences("back_free_parking", Context.MODE_PRIVATE);
        back_select_free.edit().clear().apply();
        SharedPreferences ff = getSharedPreferences("show_proof_layout", Context.MODE_PRIVATE);
        ff.edit().clear().apply();
        SharedPreferences shs = getSharedPreferences("parkezly_parking", Context.MODE_PRIVATE);
        shs.edit().clear().apply();
        SharedPreferences a = getSharedPreferences("create_ticket_opne", Context.MODE_PRIVATE);
        a.edit().clear().apply();
        SharedPreferences a1 = getSharedPreferences("create_ticket_opne", Context.MODE_PRIVATE);
        a1.edit().clear().apply();
        SharedPreferences ads = getSharedPreferences("create_ticket_to_proof_back", Context.MODE_PRIVATE);
        ads.edit().clear().apply();
        mapisloadcallback = false;
        managedmapisloaded = false;
        centerlat = 0.0;
    }

    public void TwpAdmin_Click(int postion) {
        if (myApp.cd.isConnectingToInternet()) {
            //   menuitemclick();
            txtactionbartitle.setVisibility(View.VISIBLE);
            centerlat = 0.0;
            centerlang = 0.0;
            loginmenu.setVisibility(View.GONE);
            menu.setVisibility(View.GONE);
            myaccountmenu.setVisibility(View.GONE);
            notificationsmenu.setVisibility(View.GONE);
            result = false;
            MenuConfig menuConfig = menuConfigsMainMenu.get(postion);
            if (menuConfig.getMenu_id() == 2) {
                //Logout
                lang = "null";
                lat = "null";
                // logout......................................................................................
//                SharedPreferences sh = getSharedPreferences("login", MODE_PRIVATE);
                SharedPreferences.Editor ed = logindeatl.edit();
                ed.clear();
                ed.commit();
                /*loginmenu.setVisibility(View.GONE);
//                menu.setVisibility(View.VISIBLE);
                myaccountmenu.setVisibility(View.GONE);
                notificationsmenu.setVisibility(View.GONE);
                result = true;*/
                //vehicle timer status
                result = true;
                loginmenu.setVisibility(View.GONE);
                btnsearch.setVisibility(View.GONE);
                btnback.setVisibility(View.GONE);
                imgmenu.setVisibility(View.GONE);
                fragmentStack.clear();
                login_fragment pay = new login_fragment();
                pay.registerForListener(this);
                FragmentTransaction ft_schedule = manager.beginTransaction();
                ft_schedule.add(R.id.My_Container_1_ID, pay, "home");
                fragmentStack.push(pay);
                ft_schedule.commitAllowingStateLoss();
//                checkCurrentUser();
            } else if (menuConfig.getMenu_id() == 1) {
                //Event Definitions
//                new fetchManagers().execute();
                Log.d("testclick","==Work");
                fram.setVisibility(View.VISIBLE);
                btnback.setVisibility(View.VISIBLE);
                btnsearch.setVisibility(View.GONE);
                AllEventsFragment frag = new AllEventsFragment();
                Bundle bundle = new Bundle();
                bundle.putString("parkingType", "Township");
                bundle.putString("selectedManager", new Gson().toJson(twpManager));
                frag.setArguments(bundle);
                FragmentTransaction ft_schedule = manager.beginTransaction();
                ft_schedule.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft_schedule.add(R.id.My_Container_1_ID, frag, "partner_types");
                fragmentStack.push(frag);
                ft_schedule.commitAllowingStateLoss();
                result = false;
                /*fram.setVisibility(View.VISIBLE);
                btnback.setVisibility(View.VISIBLE);
                btnsearch.setVisibility(View.GONE);
                AllEventsFragment frag = new AllEventsFragment();
                Bundle bundle = new Bundle();
                bundle.putString("parkingType", "Township");
                bundle.putString("selectedManager", new Gson().toJson(twpManager));
                frag.setArguments(bundle);
                FragmentTransaction ft_schedule = manager.beginTransaction();
                ft_schedule.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft_schedule.add(R.id.My_Container_1_ID, frag, "partner_types");
                fragmentStack.push(frag);
                ft_schedule.commitAllowingStateLoss();
                result = false;*/
            } else if (menuConfig.getMenu_id() == 3) {
                //User Profile
                fram.setVisibility(View.VISIBLE);
                btnback.setVisibility(View.VISIBLE);
                btnsearch.setVisibility(View.GONE);

                Frg_User_profile userProfileFarg = new Frg_User_profile();
                Bundle bundle = new Bundle();
                bundle.putString("menu", menuConfig.getMenu_name());
                userProfileFarg.setArguments(bundle);
                FragmentTransaction ft_schedule = manager.beginTransaction();
                ft_schedule.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft_schedule.add(R.id.My_Container_1_ID, userProfileFarg, "user_profile");
                fragmentStack.lastElement().onPause();
                ft_schedule.hide(fragmentStack.lastElement());
                fragmentStack.push(userProfileFarg);
                ft_schedule.commitAllowingStateLoss();
                result = false;
            } else if (menuConfig.getMenu_id() == 4) {
                //Config Settings
                fram.setVisibility(View.VISIBLE);
                btnback.setVisibility(View.VISIBLE);
                btnsearch.setVisibility(View.GONE);
                Log.d("config_clicked","=====");
                ConfigSettingTypeFragment configFrag = new ConfigSettingTypeFragment();
                Bundle bundle = new Bundle();
                bundle.putString("menu", menuConfig.getMenu_name());
                bundle.putString("Config_type", "Call Config");
                configFrag.setArguments(bundle);
                FragmentTransaction ft_schedule = manager.beginTransaction();
                ft_schedule.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft_schedule.add(R.id.My_Container_1_ID, configFrag, "config_settings");
                fragmentStack.lastElement().onPause();
                ft_schedule.hide(fragmentStack.lastElement());
                fragmentStack.push(configFrag);
                ft_schedule.commitAllowingStateLoss();

                result = false;
            } else if (menuConfig.getMenu_id() == 5) {
                //Notifications Menu
                fram.setVisibility(View.VISIBLE);
                animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.sidepannelright);
                txtactionbartitle.setVisibility(View.VISIBLE);
                isNotificationShowing = true;
                menuClick = "notifications";
                if (!isMenuListing)
                    showFullMenuGridDialog();
                else {
                    loginmenu.setVisibility(View.GONE);
                    myaccountmenu.setVisibility(View.GONE);
                    notificationsmenu.setVisibility(View.VISIBLE);
                    notificationsmenu.startAnimation(animation);
                }
                result = true;
            } else if (menuConfig.getMenu_id() == 6) {
                //Chat config Setting
                fram.setVisibility(View.VISIBLE);
                btnback.setVisibility(View.VISIBLE);
                btnsearch.setVisibility(View.GONE);

                ConfigSettingTypeFragment configFrag = new ConfigSettingTypeFragment();
                Bundle bundle = new Bundle();
                bundle.putString("menu", menuConfig.getMenu_name());
                bundle.putString("Config_type", "Chat Config");
                configFrag.setArguments(bundle);
                FragmentTransaction ft_schedule = manager.beginTransaction();
                ft_schedule.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft_schedule.add(R.id.My_Container_1_ID, configFrag, "config_settings");
                fragmentStack.lastElement().onPause();
                ft_schedule.hide(fragmentStack.lastElement());
                fragmentStack.push(configFrag);
                ft_schedule.commitAllowingStateLoss();

                result = false;
            } else if (menuConfig.getMenu_id() == 7) {
                //Group Settings
                fram.setVisibility(View.VISIBLE);
                btnback.setVisibility(View.VISIBLE);
                btnsearch.setVisibility(View.GONE);

                ConfigSettingTypeFragment configFrag = new ConfigSettingTypeFragment();
                Bundle bundle = new Bundle();
                bundle.putString("menu", menuConfig.getMenu_name());
                bundle.putString("Config_type", "Group Config");
                configFrag.setArguments(bundle);
                FragmentTransaction ft_schedule = manager.beginTransaction();
                ft_schedule.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft_schedule.add(R.id.My_Container_1_ID, configFrag, "config_settings");
                fragmentStack.lastElement().onPause();
                ft_schedule.hide(fragmentStack.lastElement());
                fragmentStack.push(configFrag);
                ft_schedule.commitAllowingStateLoss();

                result = false;
            }
        } else showDialogInternetRequired();
    }

    public void Notifications_click(int ii) {
        if (myApp.cd.isConnectingToInternet()) {
            loginmenu.setVisibility(View.GONE);
            menu.setVisibility(View.GONE);
            myaccountmenu.setVisibility(View.GONE);
            notificationsmenu.setVisibility(View.GONE);
            btnsearch.setVisibility(View.GONE);
            isNotificationShowing = false;
            result = false;

            MenuConfig menuConfig = menuConfigsSubMenu.get(ii);
            if (menuConfig.getMenu_level() == 2) {
                //Open sub menu item

            } else {
                //Open main menu item
            }

        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(vchome.this);
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }
    }

    public void Login_click(int ii) {
        Log.d("#DEBUG", " Login_click");
        if (myApp.cd.isConnectingToInternet()) {

            menu.setVisibility(View.GONE);
            myaccountmenu.setVisibility(View.GONE);
            notificationsmenu.setVisibility(View.GONE);
//            menuitemclick();
            centerlat = 0.0;
            centerlang = 0.0;
            if (ii == 0) {
                fram.setVisibility(View.VISIBLE);
                btnback.setVisibility(View.VISIBLE);
                btnsearch.setVisibility(View.GONE);
                login_fragment pay = new login_fragment();
                FragmentTransaction ft_schedule = manager.beginTransaction();
                pay.registerForListener(this);
                ft_schedule.add(R.id.My_Container_1_ID, pay, "login");
                fragmentStack.push(pay);
                ft_schedule.commitAllowingStateLoss();
                result = false;
            } else if (ii == 1) {
                Constants.mForgot = "Reset";
//                fram.setVisibility(View.VISIBLE);
                btnback.setVisibility(View.VISIBLE);
                btnsearch.setVisibility(View.GONE);
                Forgot_Password pay = new Forgot_Password();
                fragmentStack.clear();
                FragmentTransaction ft_schedule = manager.beginTransaction();
                ft_schedule.add(R.id.My_Container_1_ID, pay, "home");
                fragmentStack.push(pay);
                ft_schedule.commitAllowingStateLoss();
                result = false;
            }
        } else showDialogInternetRequired();
    }

    public void open_login_screen() {
        fram.setVisibility(View.VISIBLE);
        imgmenu.setVisibility(View.GONE);
        btnback.setVisibility(View.GONE);
        btnsearch.setVisibility(View.GONE);

        login_fragment pay = new login_fragment();
        FragmentTransaction ft_schedule = manager.beginTransaction();
        pay.registerForListener(this);
        ft_schedule.add(R.id.My_Container_1_ID, pay, "login_fragment");
        fragmentStack.push(pay);
        ft_schedule.commitAllowingStateLoss();
        result = false;
    }

    public void show_back_button() {
        btnback.setVisibility(View.VISIBLE);
        btnsearch.setVisibility(View.GONE);
    }

    private void showDialogInternetRequired() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(vchome.this);
        alertDialog.setTitle("Internet Connection Required");
        alertDialog.setMessage("Please connect to working Internet connection");
        alertDialog.setPositiveButton("Ok", (dialog, which) -> {
        });
        alertDialog.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        alertDialog.show();
    }

    private void showFullMenuGridDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(vchome.this);//, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen);
        View dialogView;
        if (appName != null && !TextUtils.isEmpty(appName.getMenu_screen_size())) {
            switch (appName.getMenu_screen_size()) {
                case "CENTER":
                    dialogView = LayoutInflater.from(vchome.this).inflate(R.layout.dialog_grid_menu_center, null);
                    break;
                case "FULL":
                    builder = new AlertDialog.Builder(vchome.this, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
                    dialogView = LayoutInflater.from(vchome.this).inflate(R.layout.dialog_grid_menu, null);
                    break;
                default:
                    dialogView = LayoutInflater.from(vchome.this).inflate(R.layout.dialog_grid_menu_center, null);
                    break;
            }
        } else {
            dialogView = LayoutInflater.from(vchome.this).inflate(R.layout.dialog_grid_menu_center, null);
        }
        RecyclerView recyclerView = dialogView.findViewById(R.id.rvLoginMenu);
        RelativeLayout rlParent = dialogView.findViewById(R.id.rlParent);

        if (bgImageOption == MENU_BG_CUSTOM) {
            if (menuBgDrawable != null) {
                rlParent.setBackground(menuBgDrawable);
            } else {
                rlParent.setBackgroundColor(0xe9000000);
            }
        } else if (bgImageOption == MENU_BG_MAP) {
            rlParent.setBackgroundColor(0x17000000);
        } else {
            if (appName != null && !TextUtils.isEmpty(appName.getMenu_bg_color())) {
                rlParent.setBackgroundColor(Color.parseColor(appName.getMenu_bg_color()));
            } else {
                rlParent.setBackgroundColor(0xe9000000);
            }
        }

        if (appName != null && !TextUtils.isEmpty(appName.getMenu_list_alignment())) {
            RelativeLayout.LayoutParams layoutParams1 = (RelativeLayout.LayoutParams) recyclerView.getLayoutParams();
            switch (appName.getMenu_list_alignment()) {
                case "TOP":
                    layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                    recyclerView.setLayoutParams(layoutParams1);
                    break;
                case "MIDDLE":
                    layoutParams1.addRule(RelativeLayout.CENTER_IN_PARENT);
                    recyclerView.setLayoutParams(layoutParams1);
                    break;
                case "BOTTOM":
                    layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    recyclerView.setLayoutParams(layoutParams1);
                    break;
            }
        }

        recyclerView.setLayoutManager(new GridLayoutManager(vchome.this, 3, LinearLayoutManager.VERTICAL, false));
        if (menuClick.equals("myaccount"))
            recyclerView.setAdapter(new CustomMenuAdapter(vchome.this, isMenuListing, account_menu_array_list));
        else if (menuClick.equals("notifications"))
            recyclerView.setAdapter(new CustomMenuAdapter(vchome.this, isMenuListing, notifications_menu_array_list));
        else
            recyclerView.setAdapter(new CustomMenuAdapter(vchome.this, isMenuListing, menu_array_list));
        menuClick = "home";
        builder.setView(dialogView);

        gridMenuDialog = builder.create();
        gridMenuDialog.show();

        gridMenuDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        gridMenuDialog.setOnCancelListener(dialog -> Log.e("#DEBUG", "   onCancel"));

        gridMenuDialog.setOnKeyListener((dialog, keyCode, event) -> {
            // TODO Auto-generated method stub
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                Log.e("#DEBUG", "   dismiss: back");
                btnsearch.setVisibility(View.GONE);
                btnback.setVisibility(View.GONE);
                gridMenuDialog.dismiss();
            }
            return true;
        });

        switch (appName != null ? appName.getMenu_screen_size() : "CENTER") {
            case "CENTER":
                gridMenuDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                gridMenuDialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
                break;
            case "FULL":
                gridMenuDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                gridMenuDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                break;
            default:
                break;
        }
//        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialog);
        gridMenuDialog.setOnDismissListener(dialog -> {
            myaccountmenu.setVisibility(View.GONE);
            if (!isMyAccountItemClicked) {

            } else {
                isMyAccountItemClicked = false;
            }
        });
    }

    public class fetchManagers extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String twpCode = "";
        String parkingurl = "_table/townships_manager";
//        Manager twpManager;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rl_progressbar.setVisibility(View.VISIBLE);
            twpCode = logindeatl.getString("twcode", "");
            parkingurl = "_table/townships_manager?filter=manager_id=" + twpCode;

        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("#DEBUG", "   fetchManagers:  URL:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "    fetchManagers:  Response: " + responseStr);
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        twpManager = new Manager();
                        twpManager.setId(c.getInt("id"));
                        twpManager.setDate_time(c.getString("date_time"));
                        twpManager.setManager_id(c.getString("manager_id"));
                        twpManager.setManager_type(c.getString("manager_type"));
                        twpManager.setManager_type_id(c.getInt("manager_type_id"));
                        twpManager.setLot_manager(c.getString("lot_manager"));
                        twpManager.setStreet(c.getString("street"));
                        twpManager.setAddress(c.getString("address"));
                        twpManager.setState(c.getString("state"));
                        twpManager.setState_name(c.getString("state_name"));
                        twpManager.setCity(c.getString("city"));
                        twpManager.setCountry(c.getString("country"));
                        twpManager.setZip(c.getString("zip"));
                        twpManager.setContact_person(c.getString("contact_person"));
                        twpManager.setContact_title(c.getString("contact_title"));
                        twpManager.setContact_phone(c.getString("contact_phone"));
                        twpManager.setContact_email(c.getString("contact_email"));
                        twpManager.setTownship_logo(c.getString("township_logo"));
                        twpManager.setOfficial_logo(c.getString("official_logo"));
                        twpManager.setUser_id(c.getInt("user_id"));
                        myApp.setSelectedManager(twpManager);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            rl_progressbar.setVisibility(View.GONE);
            Log.e("#DEBUG", "    fetchManagers:  " + new Gson().toJson(twpManager));
            if (vchome.this != null) {
                if (twpManager != null) {
                    /*fram.setVisibility(View.VISIBLE);
                    btnback.setVisibility(View.VISIBLE);
                    btnsearch.setVisibility(View.GONE);
                    AllEventsFragment frag = new AllEventsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("parkingType", "Township");
                    bundle.putString("selectedManager", new Gson().toJson(twpManager));
                    frag.setArguments(bundle);
                    FragmentTransaction ft_schedule = manager.beginTransaction();
                    ft_schedule.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                    ft_schedule.add(R.id.My_Container_1_ID, frag, "partner_types");
                    fragmentStack.push(frag);
                    ft_schedule.commitAllowingStateLoss();
                    result = false;*/
                } else {
                   /* Toast.makeText(vchome.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    fram.setVisibility(View.VISIBLE);
                    btnback.setVisibility(View.VISIBLE);
                    btnsearch.setVisibility(View.GONE);
                    AllEventsFragment frag = new AllEventsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("parkingType", "Township");
                    bundle.putString("selectedManager", new Gson().toJson(twpManager));
                    frag.setArguments(bundle);
                    FragmentTransaction ft_schedule = manager.beginTransaction();
                    ft_schedule.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                    ft_schedule.add(R.id.My_Container_1_ID, frag, "partner_types");
                    fragmentStack.push(frag);
                    ft_schedule.commitAllowingStateLoss();
                    result = false;*/
                }
            }
            super.onPostExecute(s);
        }
    }

    public class fetchLatLongFromaddress1 extends AsyncTask<Void, Void, StringBuilder> {

        String lat1, lang1;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        public fetchLatLongFromaddress1(String lat, String lang) {
            super();
            this.lat1 = lat;
            this.lang1 = lang;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat1 + "," + lang1 + "&key=AIzaSyBRdCWdlUMy3X5dc-9jMe0jRXJENibjdN8";

                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            rl_progressbar.setVisibility(View.GONE);
            try {
                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");
                if (status.equals("OK")) {
                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray
                            .getJSONObject(0);
                    adrreslocation = before_geometry_jsonObj.getString("formatted_address");
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(vchome.this);
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public class fetchMenus extends AsyncTask<String, String, String> {
        JSONObject json = null;
        String appList = "_table/menu_config_global";
        JSONArray array;

        @Override
        protected void onPreExecute() {
//            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            menuConfigsAll.clear();
            String url = null;
            try {
                String profileType = "";
                if (params[0].equalsIgnoreCase("SuperAdmin"))
                    profileType = "1";
                else if (params[0].equalsIgnoreCase("TwpAdmin"))
                    profileType = "3";
                else if (params[0].equalsIgnoreCase("TwpInspector"))
                    profileType = "3";
                else if (params[0].equalsIgnoreCase("User"))
                    profileType = "9";
                else if (params[0].equalsIgnoreCase("Bursar"))
                    profileType = "7";
                url = getString(R.string.api) + getString(R.string.povlive) + appList
                        + "?filter=(app_id%3D'" + URLEncoder.encode(String.valueOf(23), "utf-8") + "')"
                        + "%20AND%20(profile_type_id%3D'" + URLEncoder.encode(profileType, "utf-8") + "')";
                Log.e("#DEBUG", "   fetchMenus:   URL:  " + url);
                Log.e("#DEBUG", "   fetchMenus:   params:  " + params[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    Log.e("response: ", String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        MenuConfig menuConfig = new MenuConfig();
                        menuConfig.setId(c.getInt("id"));
                        menuConfig.setMenu_id(c.getInt("menu_id"));
                        menuConfig.setMenu_name(c.getString("menu_name"));
                        menuConfig.setMenu_in_app(c.getString("menu_in_app"));
                        menuConfig.setApp_id(c.getInt("app_id"));
                        menuConfig.setApp_name(c.getString("app_name"));
                        menuConfig.setParent_id(c.getInt("parent_id"));
                        menuConfig.setParent_name(c.getString("parent_name"));
                        menuConfig.setOrder_id(c.getInt("order_id"));
                        if (c.has("profile_type_id"))
                            menuConfig.setProfile_type_id(c.getInt("profile_type_id"));
                        if (c.has("profile_type"))
                            menuConfig.setProfile_type(c.getString("profile_type"));
                        menuConfig.setAccess(c.getBoolean("access"));
                        menuConfig.setShown(c.getBoolean("shown"));
                        if (params[0].equalsIgnoreCase("SuperAdmin")
                                && menuConfig.getProfile_type_id() == 1
                                && menuConfig.isShown())
                            menuConfigsAll.add(menuConfig);
                        else if (params[0].equalsIgnoreCase("TwpAdmin")
                                && menuConfig.getProfile_type_id() == 3
                                && menuConfig.isShown())
                            menuConfigsAll.add(menuConfig);
                        else if (params[0].equalsIgnoreCase("TwpInspector")
                                && menuConfig.getProfile_type_id() == 3
                                && menuConfig.isShown())
                            menuConfigsAll.add(menuConfig);
                        else if (params[0].equalsIgnoreCase("User")
                                && menuConfig.getProfile_type_id() == 9
                                && menuConfig.isShown())
                            menuConfigsAll.add(menuConfig);
                        else if (params[0].equalsIgnoreCase("Bursar")
                                && menuConfig.getProfile_type_id() == 7
                                && menuConfig.isShown())
                            menuConfigsAll.add(menuConfig);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "    fetchMenu  Error:  " + e.getMessage());
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
//            rl_progressbar.setVisibility(View.GONE);
            Log.e("#DEBUG", "    fetchMenus:  " + new Gson().toJson(menuConfigsAll));
            Collections.sort(menuConfigsAll, (menuConfig, t1) -> menuConfig.getOrder_id() - t1.getOrder_id()); //for Ascending order
            if (menuConfigsAll.size() != 0) {
                menu_array_list.clear();
                menu_array_list.addAll(getHomeMainMenu());
               /* list_loging_menu.setAdapter(new CustomAdapterMenu(vchome.this, menu_array_list, getResources()));

                account_menu_array_list.clear();
                account_menu_array_list.addAll(getHomeSubMenu("MyAccount"));
                list_account_menu.setAdapter(new CustomAdapterMenu(vchome.this, account_menu_array_list, getResources()));

                notifications_menu_array_list.clear();
                notifications_menu_array_list.addAll(getHomeSubMenu("Notifications"));
                list_notifications_menu.setAdapter(new CustomAdapterMenu(vchome.this, notifications_menu_array_list, getResources()));*/
                isMenuListing = myApp.isMenuDefault();
                if (appName != null) {
                    if (!myApp.isMenuDefault() && !TextUtils.isEmpty(appName.getMenu_style()))
                        isMenuListing = appName.getMenu_style().equals("listing");

                    if (!TextUtils.isEmpty(appName.getMenu_font()))
                        menuFontPosition = menuFonts.indexOf(new MenuFont(appName.getMenu_font()));

                    if (isMenuListing) {
                        //binding.rvUsers.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                        rvLoginMenu.setLayoutManager(new LinearLayoutManager(vchome.this, LinearLayoutManager.VERTICAL, false));
                        rvLoginMenu.setAdapter(new CustomMenuAdapter(vchome.this, isMenuListing, menu_array_list));
                    } else {
                        rvLoginMenu.setLayoutManager(new GridLayoutManager(vchome.this, 3, LinearLayoutManager.VERTICAL, false));
                        rvLoginMenu.setAdapter(new CustomMenuAdapter(vchome.this, isMenuListing, menu_array_list));
                    }

                    account_menu_array_list.clear();
                    account_menu_array_list.addAll(getHomeSubMenu("MyAccount"));
                    imgmenu.setVisibility(View.VISIBLE);
                    if (isMenuListing) {
                        rvAccountMenu.setLayoutManager(new LinearLayoutManager(vchome.this, LinearLayoutManager.VERTICAL, false));
                        rvAccountMenu.setAdapter(new CustomMenuAdapter(vchome.this, isMenuListing, account_menu_array_list));
                    } else {
                        rvAccountMenu.setLayoutManager(new GridLayoutManager(vchome.this, 3, LinearLayoutManager.VERTICAL, false));
                        rvAccountMenu.setAdapter(new CustomMenuAdapter(vchome.this, isMenuListing, account_menu_array_list));
                    }

                    notifications_menu_array_list.clear();
                    notifications_menu_array_list.addAll(getHomeSubMenu("Notifications"));
                    imgmenu.setVisibility(View.VISIBLE);
                    if (isMenuListing) {
                        rvNotificationMenu.setLayoutManager(new LinearLayoutManager(vchome.this, LinearLayoutManager.VERTICAL, false));
                        rvNotificationMenu.setAdapter(new CustomMenuAdapter(vchome.this, isMenuListing, notifications_menu_array_list));
                    } else {
                        rvNotificationMenu.setLayoutManager(new GridLayoutManager(vchome.this, 3, LinearLayoutManager.VERTICAL, false));
                        rvNotificationMenu.setAdapter(new CustomMenuAdapter(vchome.this, isMenuListing, notifications_menu_array_list));
                    }

                    if (!TextUtils.isEmpty(appName.getMenu_bg_img())) {
                        if (appName.getMenu_bg_img().contains("http")) {
                            isHasBgMenuImage = true;
                            bgImageOption = MENU_BG_CUSTOM;
                            Log.e("#DEBUG", "    onPost:  menu_image:  " + appName.getMenu_bg_img());
                            rl_progressbar.setVisibility(View.GONE);
                            imgmenu.performClick();
                        } else if (appName.getMenu_bg_img().equalsIgnoreCase("map")) {
                            isHasBgMenuImage = false;
                            bgImageOption = MENU_BG_MAP;
                            rl_progressbar.setVisibility(View.GONE);
                            imgmenu.performClick();
                        } else if (appName.getMenu_bg_img().equalsIgnoreCase("color")) {
                            isHasBgMenuImage = false;
                            bgImageOption = MENU_BG_COLOR;
                            rl_progressbar.setVisibility(View.GONE);
                            imgmenu.performClick();
                        }
                    } else {
                        if (!isHasBgMenuImage) {
                            rl_progressbar.setVisibility(View.GONE);
                            imgmenu.performClick();
                        }
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public ArrayList<item> Login_menu() {
        ArrayList<item> temp_values = new ArrayList<>();
        String temvalues[] = {"Login or Register", "Reset Password"};
//        , "Find Parking",
//                "Park Here & Track", "Find My Vehicle", "Drive EZly", "All Parking Options"};
        Integer tempimg[] = {R.mipmap.login_register, R.mipmap.login_register};
//        ,         R.mipmap.findparking, R.mipmap.directions, R.mipmap.show_timer,
//                R.mipmap.drive_ezly, R.mipmap.other_parking_options};
        for (int i = 0; i < temvalues.length; i++) {
            item ii = new item();
            ii.setMenu_title(temvalues[i]);
            ii.setMenu_img_id(tempimg[i].intValue());
            temp_values.add(ii);
        }
        return temp_values;
    }

    public ArrayList<item> getHomeMainMenu() {
        ArrayList<item> temp_values = new ArrayList<>();
        menuConfigsMainMenu.clear();
        int j = 0;
        for (int i = 0; i < menuConfigsAll.size(); i++) {
            if (menuConfigsAll.get(i).getParent_id() == 0) {
                item ii = new item();
                ii.setMenu_title(menuConfigsAll.get(i).getMenu_name());
                if (menuConfigsAll.get(i).getMenu_name().equals("Logout"))
                    ii.setMenu_img_id(R.mipmap.login_register);
                else if (menuConfigsAll.get(i).getMenu_name().equals("User Profile"))
                    ii.setMenu_img_id(R.mipmap.user_profile);
                else if (menuConfigsAll.get(i).getMenu_name().equals("Config Settings"))
                    ii.setMenu_img_id(R.mipmap.call_settings);
                else if (menuConfigsAll.get(i).getMenu_name().equals("Chat Config Settings"))
                    ii.setMenu_img_id(R.mipmap.chat_setting_1);
                else if (menuConfigsAll.get(i).getMenu_name().equals("Group Settings"))
                    ii.setMenu_img_id(R.mipmap.gp_setting);
                else ii.setMenu_img_id(R.mipmap.view);
                if (!TextUtils.isEmpty(menuConfigsAll.get(i).getMenu_font()))
                    ii.setMenu_font(menuConfigsAll.get(i).getMenu_font());
                if (!TextUtils.isEmpty(menuConfigsAll.get(i).getMenu_icon())) {
                    ii.setMenu_icon(menuConfigsAll.get(i).getMenu_icon());
                }
                if (!TextUtils.isEmpty(menuConfigsAll.get(i).getMenu_color())) {
                    ii.setMenu_color(menuConfigsAll.get(i).getMenu_color());
                }
                if (!TextUtils.isEmpty(menuConfigsAll.get(i).getMenu_bg_color())) {
                    ii.setMenu_bg_color(menuConfigsAll.get(i).getMenu_bg_color());
                }
                temp_values.add(ii);
                menuConfigsMainMenu.add(menuConfigsAll.get(i));
            }
        }
        return temp_values;
    }

    public ArrayList<item> getHomeSubMenu(String menu) {
        ArrayList<item> temp_values = new ArrayList<>();
        menuConfigsSubMenu.clear();
        for (int i = 0; i < menuConfigsAll.size(); i++) {
            if (menu.equals("MyAccount")) {
                if (menuConfigsAll.get(i).getParent_name().equalsIgnoreCase("My Account")
                        || menuConfigsAll.get(i).getParent_name().equalsIgnoreCase("Managed Accounts")) {
                    item ii = new item();
                    ii.setMenu_title(menuConfigsAll.get(i).getMenu_name());
                    ii.setMenu_img_id(R.mipmap.view);
                    if (!TextUtils.isEmpty(menuConfigsAll.get(i).getMenu_font()))
                        ii.setMenu_font(menuConfigsAll.get(i).getMenu_font());
                    if (!TextUtils.isEmpty(menuConfigsAll.get(i).getMenu_icon())) {
                        ii.setMenu_icon(menuConfigsAll.get(i).getMenu_icon());
                    }
                    if (!TextUtils.isEmpty(menuConfigsAll.get(i).getMenu_color())) {
                        ii.setMenu_color(menuConfigsAll.get(i).getMenu_color());
                    }
                    if (!TextUtils.isEmpty(menuConfigsAll.get(i).getMenu_bg_color())) {
                        ii.setMenu_bg_color(menuConfigsAll.get(i).getMenu_bg_color());
                    }

                    temp_values.add(ii);
                    menuConfigsSubMenu.add(menuConfigsAll.get(i));
                }
            } else if (menu.equals("Notifications")) {
                if (menuConfigsAll.get(i).getParent_name().equalsIgnoreCase("Notifications")) {
                    item ii = new item();
                    ii.setMenu_title(menuConfigsAll.get(i).getMenu_name());
                    ii.setMenu_img_id(R.mipmap.view);
                    if (!TextUtils.isEmpty(menuConfigsAll.get(i).getMenu_font()))
                        ii.setMenu_font(menuConfigsAll.get(i).getMenu_font());
                    if (!TextUtils.isEmpty(menuConfigsAll.get(i).getMenu_icon())) {
                        ii.setMenu_icon(menuConfigsAll.get(i).getMenu_icon());
                    }
                    if (!TextUtils.isEmpty(menuConfigsAll.get(i).getMenu_color())) {
                        ii.setMenu_color(menuConfigsAll.get(i).getMenu_color());
                    }
                    if (!TextUtils.isEmpty(menuConfigsAll.get(i).getMenu_bg_color())) {
                        ii.setMenu_bg_color(menuConfigsAll.get(i).getMenu_bg_color());
                    }

                    temp_values.add(ii);
                    menuConfigsSubMenu.add(menuConfigsAll.get(i));
                }
            }
        }
        return temp_values;
    }

    public class CustomAdapterMenu extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdapterMenu(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txt_title;
            ImageView img;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;


            if (convertView == null) {

                vi = inflater.inflate(R.layout.adapter_menu, null);
                /****** View Holder Object to contain tabitem.xml file elements ******/

                holder = new ViewHolder();
                holder.img = vi.findViewById(R.id.imgloginfindparking);
                holder.txt_title = vi.findViewById(R.id.loginfindparking);


                /************  Set holder with LayoutInflater ************/
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {


            } else {
                Typeface type = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeue-Thin.ttf");
                holder.txt_title.setTypeface(type);
                holder.txt_title.setText(data.get(position).getMenu_title());
                holder.img.setBackgroundResource(data.get(position).getMenu_img_id());

                if (!TextUtils.isEmpty(data.get(position).getMenu_color())) {
                    holder.txt_title.setTextColor(Integer.parseInt(data.get(position).getMenu_color()));
                } else
                    holder.txt_title.setTextColor(ContextCompat.getColor(activity, R.color.white));

                if (!TextUtils.isEmpty(data.get(position).getMenu_icon())) {
                    Picasso.with(activity).load(data.get(position).getMenu_icon())
                            .placeholder(R.mipmap.view)
                            .into(holder.img);
                } else {
                    holder.img.setBackgroundResource(data.get(position).getMenu_img_id());
                }

                if (!TextUtils.isEmpty(data.get(position).getMenu_font())) {
                    holder.txt_title.setTextSize(TypedValue.COMPLEX_UNIT_PX, Float.parseFloat(data.get(position).getMenu_font()));
                }/* else {
                    holder.txt_title.setTextSize(TypedValue.COMPLEX_UNIT_PX, 36f);
                }*/
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

    private class CustomMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private Activity activity;
        private int TYPE_LIST = 0, TYPE_GRID = 1;
        private boolean isList = true;
        private ArrayList<item> menuItems = new ArrayList<>();

        public CustomMenuAdapter(Activity activity, boolean isList, ArrayList<item> menuItems) {
            this.activity = activity;
            this.isList = isList;
            this.menuItems = menuItems;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == 0) {
                return new MenuListHolder(LayoutInflater.from(activity).inflate(R.layout.item_menu_list, parent, false));
            } else if (viewType == 1) {
                return new MenuGridHolder(LayoutInflater.from(activity).inflate(R.layout.item_menu_grid, parent, false));
            } else return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            item menuItem = menuItems.get(position);
            if (holder.getItemViewType() == 0) {
                MenuListHolder listHolder = (MenuListHolder) holder;
                listHolder.tvMenuName.setText(menuItem.getMenu_title());
                listHolder.ivIcon.setImageDrawable(ContextCompat.getDrawable(activity, menuItem.getMenu_img_id()));

                if (!myApp.isMenuDefault()) {
                    if (!TextUtils.isEmpty(menuItem.getMenu_icon())) {
                        Glide.with(activity).load(menuItem.getMenu_icon())
                                .placeholder(R.mipmap.view).into(listHolder.ivIcon);
                        /*Picasso.with(activity).load(menuItem.getMenu_icon())
                                .placeholder(R.mipmap.view)
                                .into(listHolder.ivIcon);*/
                    } else if (!TextUtils.isEmpty(menuItem.getIcon_client_pass_type_link())) {
                        Glide.with(getApplicationContext()).load(menuItem.getIcon_client_pass_type_link())
                                .placeholder(R.mipmap.view)
                                .into(listHolder.ivIcon);
                        /*Picasso.with(activity).load(menuItem.getIcon_client_pass_type_link())
                                .placeholder(R.mipmap.view)
                                .into(listHolder.ivIcon);*/
                    }

                    Typeface type = Typeface.createFromAsset(getAssets(), menuFonts.get(menuFontPosition).getPath());
                    listHolder.tvMenuName.setTypeface(type);

                    if (appName != null) {
                        if (!TextUtils.isEmpty(appName.getMenu_color())) {
                            listHolder.tvMenuName.setTextColor(Color.parseColor(appName.getMenu_color()));
                        } else
                            listHolder.tvMenuName.setTextColor(ContextCompat.getColor(activity, R.color.white));

                        if (!TextUtils.isEmpty(appName.getMenu_font_size())) {
                            listHolder.tvMenuName.setTextSize(TypedValue.COMPLEX_UNIT_PX, Float.parseFloat(appName.getMenu_font_size()));
                        } else {
                            listHolder.tvMenuName.setTextSize(TypedValue.COMPLEX_UNIT_PX, 36f);
                        }

                        if (!TextUtils.isEmpty(appName.getMenu_bg_color())) {
                            listHolder.llParent.setBackgroundColor(Color.parseColor(appName.getMenu_bg_color()));
                        } else {
                            listHolder.llParent.setBackgroundColor(0xe9000000);
                        }

                        if (!TextUtils.isEmpty(appName.getMenu_font_style())) {
                            switch (appName.getMenu_font_style()) {
                                case "normal":
                                    listHolder.tvMenuName.setTypeface(listHolder.tvMenuName.getTypeface(), Typeface.NORMAL);
                                    break;
                                case "bold":
                                    listHolder.tvMenuName.setTypeface(listHolder.tvMenuName.getTypeface(), Typeface.BOLD);
                                    break;
                                case "italic":
                                    listHolder.tvMenuName.setTypeface(listHolder.tvMenuName.getTypeface(), Typeface.ITALIC);
                                    break;
                            }
                        }
                    }
                } else {
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeue-Thin.ttf");
                    listHolder.tvMenuName.setTypeface(type);
                }

            } else if (holder.getItemViewType() == 1) {
                MenuGridHolder gridHolder = (MenuGridHolder) holder;
                gridHolder.tvMenuName.setText(menuItem.getMenu_title());
                gridHolder.ivIcon.setImageDrawable(ContextCompat.getDrawable(activity, menuItem.getMenu_img_id()));

                Typeface type = Typeface.createFromAsset(getAssets(), menuFonts.get(menuFontPosition).getPath());
                gridHolder.tvMenuName.setTypeface(type);

                if (!TextUtils.isEmpty(menuItem.getMenu_icon())) {
                    Picasso.with(activity).load(menuItem.getMenu_icon())
                            .placeholder(R.mipmap.view)
                            .into(gridHolder.ivIcon);
                } else if (!TextUtils.isEmpty(menuItem.getIcon_client_pass_type_link())) {
                    Picasso.with(activity).load(menuItem.getIcon_client_pass_type_link())
                            .placeholder(R.mipmap.view)
                            .into(gridHolder.ivIcon);
                }

                if (appName != null) {
                    if (!TextUtils.isEmpty(appName.getMenu_color())) {
                        gridHolder.tvMenuName.setTextColor(Color.parseColor(appName.getMenu_color()));
                    } else
                        gridHolder.tvMenuName.setTextColor(ContextCompat.getColor(activity, R.color.white));

                    if (!TextUtils.isEmpty(appName.getMenu_font_size())) {
                        gridHolder.tvMenuName.setTextSize(TypedValue.COMPLEX_UNIT_PX, Float.parseFloat(appName.getMenu_font_size()));

                    } else {
                        gridHolder.tvMenuName.setTextSize(TypedValue.COMPLEX_UNIT_PX, 36f);
                    }

//                    if (!TextUtils.isEmpty(appName.getMenu_bg_color())) {
//                        gridHolder.llParent.setBackgroundColor(Color.parseColor(appName.getMenu_bg_color()));
//                    } else {
//                        gridHolder.llParent.setBackgroundColor(0xe9000000);
//                    }

                    if (!TextUtils.isEmpty(appName.getMenu_font_style())) {
                        switch (appName.getMenu_font_style()) {
                            case "normal":
                                gridHolder.tvMenuName.setTypeface(gridHolder.tvMenuName.getTypeface(), Typeface.NORMAL);
                                break;
                            case "bold":
                                gridHolder.tvMenuName.setTypeface(gridHolder.tvMenuName.getTypeface(), Typeface.BOLD);
                                break;
                            case "italic":
                                gridHolder.tvMenuName.setTypeface(gridHolder.tvMenuName.getTypeface(), Typeface.ITALIC);
                                break;
                        }
                    }
                }
            }

        }

        @Override
        public int getItemCount() {
            return menuItems.size();
        }

        @Override
        public int getItemViewType(int position) {
            return isList ? TYPE_LIST : TYPE_GRID;
        }

        class MenuListHolder extends RecyclerView.ViewHolder {
            TextView tvMenuName;
            ImageView ivIcon;
            LinearLayout llParent;

            MenuListHolder(View itemView) {
                super(itemView);
                tvMenuName = itemView.findViewById(R.id.tvMenuName);
                ivIcon = itemView.findViewById(R.id.ivIcon);
                llParent = itemView.findViewById(R.id.llParent);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (gridMenuDialog != null && gridMenuDialog.isShowing()) {
                            gridMenuDialog.dismiss();
                        }
                        if (notificationsmenu.getVisibility() == View.VISIBLE || isNotificationShowing) {
                            if (myApp.getUserRole().equalsIgnoreCase("SuperAdmin")) {
                                Notifications_click(getBindingAdapterPosition());
                            } else if (myApp.getUserRole().equalsIgnoreCase("TwpAdmin")) {
                                Notifications_click(getBindingAdapterPosition());
                            }
                        } else {
                            if (!myApp.getUserId().equals("null")) {
                                if (myApp.getUserRole().equalsIgnoreCase("SuperAdmin")) {
                                    TwpAdmin_Click(getBindingAdapterPosition());
                                } else if (myApp.getUserRole().equalsIgnoreCase("TwpAdmin")) {
                                    TwpAdmin_Click(getBindingAdapterPosition());
                                }
                            }
                        }
                    }
                });
            }
        }

        class MenuGridHolder extends RecyclerView.ViewHolder {
            TextView tvMenuName;
            ImageView ivIcon;
            LinearLayout llParent;

            MenuGridHolder(View itemView) {
                super(itemView);
                tvMenuName = itemView.findViewById(R.id.tvMenuName);
                ivIcon = itemView.findViewById(R.id.ivIcon);
                llParent = itemView.findViewById(R.id.llParent);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (gridMenuDialog != null && gridMenuDialog.isShowing()) {
                            gridMenuDialog.dismiss();
                        }
                        if (notificationsmenu.getVisibility() == View.VISIBLE || isNotificationShowing) {
                            if (myApp.getUserRole().equalsIgnoreCase("SuperAdmin")) {
                                Notifications_click(getBindingAdapterPosition());
                            } else if (myApp.getUserRole().equalsIgnoreCase("TwpAdmin")) {
                                Notifications_click(getBindingAdapterPosition());
                            }
                        } else {
                            Log.d("#DEBUG", " role: " + myApp.getUserRole());
                            if (!myApp.getUserId().equals("null")) {
                                if (myApp.getUserRole().equalsIgnoreCase("SuperAdmin")) {
                                    TwpAdmin_Click(getBindingAdapterPosition());
                                } else if (myApp.getUserRole().equalsIgnoreCase("TwpAdmin")) {
                                    TwpAdmin_Click(getBindingAdapterPosition());
                                }
                            }
                        }
                    }
                });
            }
        }
    }
}