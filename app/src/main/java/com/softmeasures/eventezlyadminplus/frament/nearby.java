package com.softmeasures.eventezlyadminplus.frament;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsoluteLayout;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.softmeasures.eventezlyadminplus.Custom_contorl.MapWrapperLayout;
import com.softmeasures.eventezlyadminplus.Custom_contorl.OnInfoWindowElemTouchListener;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;
import com.squareup.picasso.Callback;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;


public class nearby extends Fragment implements GoogleMap.OnInfoWindowClickListener {
    private static final String LOG_TAG = "ExampleApp";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyC6UabVHdti4zlU6E1iajVuNi6ZdKhDK5g";
    TextView txttitle;
    GoogleMap googleMap;
    MarkerOptions options;
    double latitude, longitude;
    String filterAddress = "", categoriy;
    ConnectionDetector cd;
    ListView list_destination, list_cat;
    RelativeLayout rl_editname;
    TextView btn_destination, txt_get_cat;
    EditText edit_destition_address, txtaddress;
    TextView txt_search;
    boolean opendes = false;
    String adrreslocation, addressti, addressin;
    String addrsstitle, addresssub;
    ArrayList<item> searcharray = new ArrayList<>();
    ArrayList<item> oldarraylist = new ArrayList<>();
    ArrayList<item> otherparking = new ArrayList<>();
    ArrayList<item> directionarray = new ArrayList<>();
    String categories[] = {"All Categories", "Airport", "Amusement Park", "Aquarium", "ATM", "Bakery", "Bank",
            "Bicycle Store", "Book Store", "Bus Station", "Cafe",
            "Camp Ground", "Car Rental", "Car Repair",
            "Car Wash", "Church", "City Hall", "Convenience Store",
            "Dentist", "Doctor", "Fire Station", "Florist", "Gas Station",
            "Home Goods Store", "Hospital", "Library", "Locksmith",
            "Lodging", "Meal Delivery", "Meal Takeaway", "Museum",
            "Park", "Pharmacy", "Police", "Post Office", "Restaurant",
            "RV Park", "School", "Shopping Mall", "Stadium", "Subway Station", "Taxi Stand",
            "Train Station", "Transit Station", "University", "Veterinary Care", "Zoo"};
    String lat = "null", lang = "null";
    ProgressBar progressBar;
    RelativeLayout rl_progressbar;
    Bitmap mypic;
    ArrayList<item> cat = new ArrayList<>();
    boolean not_first_time_showing_info_window = false, cast_open = false, windowopen = false, mapisloaded = false;
    ArrayList<item> searchplace = new ArrayList<>();
    AutoCompleteTextView autoCompView_des, autoCompView_sor;
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private ViewGroup infoWindow;
    static MapWrapperLayout mapWrapperLayout;
    TextView txt_title, txt_lat, txt_lang;
    Button txt_direction, txt_maps;
    ImageView img_photo;
    private OnInfoWindowElemTouchListener infoButtonListener;
    private OnInfoWindowElemTouchListener infoButtonListener_map;


    private static final int POPUP_POSITION_REFRESH_INTERVAL = 16;
    //длительность анимации перемещения карты
    private static final int ANIMATION_DURATION = 500;
    private int markerHeight = 40;
    private AbsoluteLayout.LayoutParams overlayLayoutParams;

    //слушатель, который будет обновлять смещения
    private ViewTreeObserver.OnGlobalLayoutListener infoWindowLayoutListener;

    private Handler handler;

    //Runnable, который обновляет положение окна
    private Runnable positionUpdaterRunnable;

    private LatLng trackedPosition;
    private View infoWindowContainer;
    private int popupXOffset;
    private int popupYOffset;

    public static ArrayList<item> autocomplete(String input) {
        ArrayList<item> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
            resultList = new ArrayList<item>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                String cghcbdc = predsJsonArray.getJSONObject(i).getString("description");
                if (cghcbdc.contains(",")) {
                    String street = cghcbdc.substring(0, cghcbdc.indexOf(","));
                    String addrress = cghcbdc.substring(cghcbdc.indexOf(",") + 1);
                    item ii = new item();
                    ii.setLoationname(street);
                    ii.setAddress(addrress);
                    resultList.add(ii);
                }
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    String click_on_move, is_click_menu = "no";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nearby, container, false);
        autoCompView_des = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteTextView);
        txttitle = (TextView) view.findViewById(R.id.txttitte);
        txtaddress = (EditText) view.findViewById(R.id.address);
        mapWrapperLayout = (MapWrapperLayout) view.findViewById(R.id.map_relative_layout);
        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Thin.ttf");
        txttitle.setTypeface(type);
        txtaddress.setTypeface(type);
        rl_editname = (RelativeLayout) view.findViewById(R.id.rl_edit_name_with_search);
        list_destination = (ListView) view.findViewById(R.id.list_destinatin);
        btn_destination = (TextView) view.findViewById(R.id.btn_destination);
        edit_destition_address = (EditText) view.findViewById(R.id.edit_destination);
        txt_search = (TextView) view.findViewById(R.id.txt_search);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar1);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        txt_get_cat = (TextView) view.findViewById(R.id.btn_cat);
        list_cat = (ListView) view.findViewById(R.id.list_cat);
        RelativeLayout rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            click_on_move = bundle.getString("is_gas");
            is_click_menu = bundle.getString("is_menu_click");
        }
        for (int i = 0; i < categories.length; i++) {
            item ii = new item();
            ii.setCategory(categories[i].toString());
            String title = categories[i].toString();
            if (click_on_move.equals("yes")) {
                if (title.equals("Gas Station")) {
                    ii.setIsselect(true);
                    txt_get_cat.setText("Gas Station");
                } else {
                    ii.setIsselect(false);
                }
            } else {
                if (i == 0) {
                    ii.setIsselect(true);
                } else {
                    ii.setIsselect(false);
                }
            }
            cat.add(ii);
        }
        cd = new ConnectionDetector(getActivity());
        infoWindowContainer = view.findViewById(R.id.container_popup);
        infoWindowLayoutListener = new InfoWindowLayoutListener();
        infoWindowContainer.getViewTreeObserver().addOnGlobalLayoutListener(infoWindowLayoutListener);
        overlayLayoutParams = (AbsoluteLayout.LayoutParams) infoWindowContainer.getLayoutParams();

        txt_direction = (Button) infoWindowContainer.findViewById(R.id.txt_get_directions);
        txt_title = (TextView) infoWindowContainer.findViewById(R.id.text_nearby_name);
        txt_lat = (TextView) infoWindowContainer.findViewById(R.id.txt_lat);
        txt_lang = (TextView) infoWindowContainer.findViewById(R.id.txt_lang);
        txt_maps = (Button) infoWindowContainer.findViewById(R.id.txt_map_apps);
        img_photo = (ImageView) infoWindowContainer.findViewById(R.id.img_photos);


        txt_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!is_click_menu.equals("yes")) {
                    //  Toast.makeText(getActivity(), txt_title.getText().toString(), Toast.LENGTH_SHORT).show();
                    String lat = txt_lat.getText().toString();
                    String lang = txt_lang.getText().toString();
                    f_direction mParentFragment = (f_direction) getParentFragment();
                    mParentFragment.show_end_address(String.valueOf(lat), String.valueOf(lang));
                    infoWindowContainer.setVisibility(INVISIBLE);
                } else {
                    String lat = txt_lat.getText().toString();
                    String lang = txt_lang.getText().toString();
                    SharedPreferences sh1 = getActivity().getSharedPreferences("directiontomydirection", Context.MODE_PRIVATE);
                    SharedPreferences.Editor ed1 = sh1.edit();
                    ed1.putString("lat", String.valueOf(lat));
                    ed1.putString("lang", String.valueOf(lang));
                    ed1.commit();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    f_direction pay = new f_direction();
                    fragmentStack.clear();
                    ft.add(R.id.My_Container_1_ID, pay, "home");
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                }
            }
        });
        txt_maps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String lat = txt_lat.getText().toString();
                String lang = txt_lang.getText().toString();
                Uri gmmIntentUri = Uri.parse("geo:" + lat + "," + lang + "");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });
        if (cd.isConnectingToInternet()) {
            GPSTracker tracker = new GPSTracker(getActivity().getApplicationContext());
            if (tracker.canGetLocation()) {

                latitude = tracker.getLatitude();
                longitude = tracker.getLongitude();
                String d = String.valueOf(tracker.getLocation());
                // getaddress1(latitude, longitude);
                new fetchLatLongFromaddress(String.valueOf(latitude), String.valueOf(longitude)).execute();

                showmap();

                txt_search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String a = edit_destition_address.getText().toString();
                        if (!a.equals("")) {
                            getaddress(a);
                            if (v != null) {
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                            }
                        }
                    }
                });
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Failed to fetch user location!");
                alertDialog.setMessage("Please enable user location for app, location is required for app to work properly");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });


                alertDialog.show();
            }
            btn_destination.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (opendes == false) {
                        Resources rs;
                        CustomAdaptercity adpater = new CustomAdaptercity(getActivity(), directionarray, rs = getResources());
                        list_destination.setAdapter(adpater);
                        rl_editname.setVisibility(View.GONE);
                        opendes = true;
                        list_destination.setVisibility(View.VISIBLE);
                    } else {
                        opendes = false;
                        list_destination.setVisibility(View.GONE);
                    }
                }
            });


            txt_get_cat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cast_open == false) {
                        Resources rs;
                        CustomAdaptercat adpater = new CustomAdaptercat(getActivity(), cat, rs = getResources());
                        list_cat.setAdapter(adpater);
                        cast_open = true;
                        list_cat.setVisibility(View.VISIBLE);
                    } else {
                        cast_open = false;
                        list_cat.setVisibility(View.GONE);
                    }
                }
            });
            categoriy = "airport%Camusement_park%Caquarium%Catm%Cbakery%Cbank%Cbicycle_store%Cbook_store%Cbus_station%Ccafe%Ccampground%Ccar_rental%Ccar_repair%Ccar_wash%Cchurch%Ccity_hall%Cconvenience_store%Cdentist%Cdoctor%Cfire_station%Cflorist%Cgas_station%Chome_goods_store%Chospital%Clibrary%Clocksmith%Clodging%Cmeal_delivery%Cmeal_takeaway%Cmuseum%Cpark%Cpharmacy%Cpolice%Cpost_office%Crestaurant%Crv_park%Cschool%Cshopping_mall%Cstadium%Csubway_station%Ctaxi_stand%Ctrain_station%Ctransit_station%Cuniversity%Cveterinary_care%Czoo";
            try {
                categoriy = URLEncoder.encode(categoriy.toLowerCase(), "UTF-8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (click_on_move.equals("yes")) {
                categoriy = "gas_station";
            }
            autoCompView_des.setAdapter(new CustomAutoplace(getActivity(), getResources()));
            autoCompView_des.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String str1 = searchplace.get(position).getLoationname();
                    String str = searchplace.get(position).getAddress();
                    txttitle.setText(str1 + "," + str);
                    rl_editname.setVisibility(View.GONE);
                    autoCompView_des.setVisibility(View.GONE);
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    new fetchLatLongFromService1(str1 + "," + str).execute();

                }
            });

        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();

        }
        return view;
    }

    private void getaddress1(Double lat, Double lon) {
        Geocoder gc = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = gc.getFromLocation(lat, lon, 1);
            StringBuilder sb = new StringBuilder();
            StringBuilder addressl = new StringBuilder();
            StringBuilder country = new StringBuilder();
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getLatitude()).append("\n");
                    sb.append(address.getLongitude());
                    addressl.append(address.getAddressLine(0)).append(", ");
                    addressl.append(address.getAddressLine(1));
                    country.append(address.getAddressLine(2));
                    break;
                }
                String locationAddress = sb.toString();
                String loc = addressl.toString();
                Log.e("addrrss", loc);
                String countyname = country.toString();
                String addressti1 = loc.substring(0, loc.indexOf(","));
                String addressin1 = loc.substring(loc.indexOf(",") + 1);
                if (countyname.equals("null")) {
                    this.adrreslocation = addressti1 + ", " + addressin1;
                } else {
                    this.adrreslocation = addressti1 + "," + addressin1 + "," + countyname;
                }
                Resources rs;
                new getdestination().execute();
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Address not found!");
                alertDialog.setMessage("Could not find location. Try including the exact location information");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class fetchLatLongFromaddress extends AsyncTask<Void, Void, StringBuilder> {

        String lat1, lang1;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        public fetchLatLongFromaddress(String lat, String lang) {
            super();
            this.lat1 = lat;
            this.lang1 = lang;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {

                //  new getsearchotherlocation().execute();
                //
                // ();

                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat1 + "," + lang1 + "&sensor=true&key=AIzaSyBRdCWdlUMy3X5dc-9jMe0jRXJENibjdN8";

                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
                rl_progressbar.setVisibility(View.GONE);

                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");

                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray
                            .getJSONObject(0);
                    adrreslocation = before_geometry_jsonObj.getString("formatted_address");
                    JSONObject geometry_jsonObj = before_geometry_jsonObj
                            .getJSONObject("geometry");

                    JSONObject location_jsonObj = geometry_jsonObj
                            .getJSONObject("location");

                    lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);

                    lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);


                    new getdestination().execute();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void hideprogress() {

        txtaddress.setText(filterAddress);
    }

    private void getaddress(String address1) {
        searcharray.clear();
        Geocoder gc = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = gc.getFromLocationName(address1, 1);
            StringBuilder sb = new StringBuilder();
            StringBuilder addressl = new StringBuilder();
            StringBuilder country = new StringBuilder();
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getLatitude()).append("\n");
                    sb.append(address.getLongitude());
                    addressl.append(address.getAddressLine(0)).append(", ");
                    addressl.append(address.getAddressLine(1));
                    country.append(address.getAddressLine(2));
                    break;
                }
                String locationAddress = sb.toString();
                String loc = addressl.toString();
                String countyname = country.toString();
                lang = locationAddress.substring(locationAddress.indexOf('\n') + 1);
                lat = locationAddress.substring(0, locationAddress.indexOf('\n'));
                addressti = loc.substring(0, loc.indexOf(","));
                addressin = loc.substring(loc.indexOf(",") + 1);
                if (countyname.equals("null")) {
                    this.adrreslocation = addressti + ", " + addressin;
                } else {
                    this.adrreslocation = addressin + ", " + adrreslocation + ", " + countyname;
                }
                item i = new item();
                i.setTitle_address(addressti);
                i.setAddress(addressin);
                searcharray.add(i);
                Resources rs;
                rl_editname.setVisibility(View.GONE);
                list_destination.setVisibility(View.VISIBLE);
                CustomAdaptercity adpater = new CustomAdaptercity(getActivity(), searcharray, rs = getResources());
                list_destination.setAdapter(adpater);

            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Address not found!");
                alertDialog.setMessage("Could not find location. Try including the exact location information");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alertDialog.show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showmap() {
        final List<Integer> skipIds = new ArrayList<>();
        SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        fm.getMapAsync(googleMap1 -> {
            googleMap = googleMap1;
            for (int i = 0; i < otherparking.size(); i++) {
                LatLng position = new LatLng(Double.parseDouble(otherparking.get(i).getGooglelat()), Double.parseDouble(otherparking.get(i).getGooglelan()));
                final MarkerOptions options = new MarkerOptions();
                options.position(position);
                options.icon(BitmapDescriptorFactory.fromBitmap(oldarraylist.get(i).getBitmap()));
                options.title(otherparking.get(i).getName());
                options.snippet(otherparking.get(i).getPhoto_reference());
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                googleMap.setMyLocationEnabled(true);
                googleMap.addMarker(options);
            }
            LatLng position = null;
            if (lang.equals("null") && lat.equals("null")) {
                position = new LatLng(latitude, longitude);
            } else {
                position = new LatLng(Double.parseDouble(lat), Double.parseDouble(lang));
            }
            options = new MarkerOptions();
            options.position(position);
            mapWrapperLayout.init(googleMap, getPixelsFromDp(getActivity(), 39 + 20));
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
            }
            googleMap.setMyLocationEnabled(true);
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(position));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(12));
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            View locationButton = ((View) fm.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            rlp.setMargins(0, 0, 30, 50);
            googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {

                    LatLng center = googleMap.getCameraPosition().target;
                    double laa = center.latitude;
                    double l = center.longitude;
                    System.out.println(laa);
                    System.out.println(l);
                    filterAddress = "";
                    Geocoder geoCoder = new Geocoder(getActivity().getBaseContext(), Locale.getDefault());
                    try {
                        List<Address> addresses = geoCoder.getFromLocation(laa, l, 1);
                        if (addresses.size() > 0) {
                            filterAddress = addresses.get(0).getAddressLine(0);
                            String add = txtaddress.getText().toString();
                            if (!add.equals(filterAddress)) {
                                if (mapisloaded) {
                                    if (windowopen == false) {
                                        lat = String.valueOf(laa);
                                        lang = String.valueOf(l);
                                        new getotherparkinglist().execute();
                                    } else {
                                        Log.e("windowsopen", "no");
                                    }
                                }
                            }
                            hideprogress();
                        }
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            });

            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    windowopen = true;
                    trackedPosition = marker.getPosition();
                    Projection projection = googleMap.getProjection();
                    Point trackedPoint = projection.toScreenLocation(trackedPosition);
                    trackedPoint.y -= popupYOffset / 2;
                    LatLng newCameraLocation = projection.fromScreenLocation(trackedPoint);
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(newCameraLocation), ANIMATION_DURATION, null);
                    txt_title.setText(marker.getTitle());
                    double lat = marker.getPosition().latitude;
                    double lang = marker.getPosition().longitude;
                    txt_lat.setText(String.valueOf(lat));
                    txt_lang.setText(String.valueOf(lang));
                    String url = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=100&photoreference=" + marker.getSnippet() + "&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA";
                    Glide.with(getActivity()).load(url).into(img_photo);
                    infoWindowContainer.setVisibility(VISIBLE);
                    return true;
                }
            });
            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    windowopen = false;
                    mapisloaded = true;
                    Log.e("openwindows", String.valueOf(windowopen));
                    infoWindowContainer.setVisibility(INVISIBLE);
                }
            });

            googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    Log.e("mapisloades", "yes");
                    mapisloaded = true;

                }
            });
        });
        //googleMap.setOnInfoWindowClickListener(this);
       /* googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker arg0) {
                View v = getActivity().getLayoutInflater().inflate(R.layout.nearbypin, null);
                LatLng latLng = arg0.getPosition();
                TextView txttitle = (TextView) v.findViewById(R.id.text_nearby_name);
                TextView txt_direction = (TextView) v.findViewById(R.id.txt_get_directions);
                String url = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=100&photoreference=" + arg0.getSnippet() + "&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA";
                Log.e("url", url);
                ImageView img = (ImageView) v.findViewById(R.id.img_photos);
                if (not_first_time_showing_info_window) {
                    Picasso.with(getActivity()).load(url).into(img);
                    not_first_time_showing_info_window = false;
                } else {
                    not_first_time_showing_info_window = true;
                    Picasso.with(getActivity()).load(url).into(img, new InfoWindowRefresher(arg0));
                }
                txt_direction.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getActivity(), "hello", Toast.LENGTH_LONG).show();
                    }
                });

                windowopen = true;
                Log.e("openwindow", String.valueOf(windowopen));
                txttitle.setText(arg0.getTitle());
                return v;
            }
        });*/


        /*googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Setting up the infoWindow with current's marker info
                //infoTitle.setText(marker.getTitle());
                //  infoSnippet.setText(marker.getSnippet());
                windowopen = true;
                txt_title.setText(marker.getTitle());
                String url = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=100&photoreference=" + marker.getSnippet() + "&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA";
                infoButtonListener.setMarker(marker);
                infoButtonListener_map.setMarker(marker);
                Picasso.with(getActivity()).load(url).into(img_photo, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                    }
                });
                //  infoButtonListener1.setMarker(marker);

                // We must call this to set the current marker and infoWindow references
                // to the MapWrapperLayout
                mapWrapperLayout.setMarkerWithInfoWindow(marker, infoWindow);
                return infoWindow;
            }
        });*/
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //очистка
        handler = new Handler(Looper.getMainLooper());
        positionUpdaterRunnable = new PositionUpdaterRunnable();
        handler.post(positionUpdaterRunnable);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        infoWindowContainer.getViewTreeObserver().removeGlobalOnLayoutListener(infoWindowLayoutListener);
        handler.removeCallbacks(positionUpdaterRunnable);
        handler = null;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        windowopen = false;
        mapisloaded = true;
        double lat = marker.getPosition().latitude;
        double lang = marker.getPosition().longitude;
        SharedPreferences sh1 = getActivity().getSharedPreferences("directiontomydirection", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed1 = sh1.edit();
        ed1.putString("lat", String.valueOf(lat));
        ed1.putString("lang", String.valueOf(lang));
        ed1.commit();
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
       /* ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.replace(R.id.My_Container_1_ID, new f_direction(), "NewFragmentTag");
        ft.commit();*/

        f_direction pay = new f_direction();
        fragmentStack.clear();
        // parking_first_screen.registerForListener(Vchome.this);
        ft.add(R.id.My_Container_1_ID, pay, "home");
        fragmentStack.push(pay);
        ft.commitAllowingStateLoss();

    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public class fetchLatLongFromService1 extends AsyncTask<Void, Void, StringBuilder> {
        String place;
        String end;


        fetchLatLongFromService1(String place) {
            super();
            this.place = place;
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?address=" + this.place + "&sensor=false";
                googleMapUrl = googleMapUrl.replaceAll(" ", "%20");
                URL url = new URL(googleMapUrl);
                URLConnection c = url.openConnection();
                c.setConnectTimeout(5000);
                c.setReadTimeout(10000);
                c.getInputStream();
                InputStreamReader in = new InputStreamReader(
                        c.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");

                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray.getJSONObject(0);
                    JSONObject geometry_jsonObj = before_geometry_jsonObj.getJSONObject("geometry");
                    JSONObject location_jsonObj = geometry_jsonObj.getJSONObject("location");
                    lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);

                    lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);
                    LatLng point = new LatLng(law, lng);
                    addrsstitle = place.substring(0, place.indexOf(","));
                    addresssub = place.substring(place.indexOf(",") + 1);
                    new getotherparkinglist().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                } else {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                rl_progressbar.setVisibility(View.GONE);
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (Exception e) {
                rl_progressbar.setVisibility(View.GONE);
            }
        }
    }

    public class CustomAutoplace extends BaseAdapter implements View.OnClickListener, Filterable {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAutoplace(Activity a, Resources resLocal) {

            activity = a;
            context = a;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.layout, null);
                holder = new ViewHolder();

                holder.txt_commerical_name = (TextView) vi.findViewById(R.id.txt_name);
                holder.txt_deatil = (TextView) vi.findViewById(R.id.address);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }
            if (data.size() <= 0) {


            } else {
                try {
                    tempValues = null;
                    tempValues = (item) data.get(position);
                    holder.txt_commerical_name.setText(tempValues.getLoationname());
                    holder.txt_deatil.setText(tempValues.getAddress());
                    int k = 0;
                } catch (IndexOutOfBoundsException e) {

                } catch (Exception e) {
                }
            }

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        data = autocomplete(constraint.toString());
                        filterResults.values = data;
                        filterResults.count = data.size();
                        searchplace.clear();
                        searchplace.addAll(data);
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;

        }


        public class ViewHolder {

            public TextView txt_commerical_name, txt_deatil;
        }
    }

    private class InfoWindowRefresher implements Callback {
        private Marker markerToRefresh;

        private InfoWindowRefresher(Marker markerToRefresh) {
            this.markerToRefresh = markerToRefresh;
        }

        @Override
        public void onSuccess() {
            markerToRefresh.showInfoWindow();

        }

        @Override
        public void onError() {
        }
    }

    public class gtbitmmp extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(Bitmap... params) {
            try {
                oldarraylist.clear();
                for (int i = 0; i < otherparking.size(); i++) {
                    URL url = new URL(otherparking.get(i).getIcon());
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap myBitmap = BitmapFactory.decodeStream(input);
                    mypic = myBitmap;
                    item ii = new item();
                    Bitmap mybit = getResizedBitmap(myBitmap, 40, 40);
                    Log.e("bitmap", String.valueOf(mybit));
                    ii.setBitmap(mybit);
                    oldarraylist.add(ii);
                }
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            if (getActivity() != null && oldarraylist.size() > 0) {
                Activity activity = getActivity();
                if (activity == null) {
                } else {
                    showmap();
                }
            }
            super.onPostExecute(bitmap);
        }
    }

    public class getotherparkinglist extends AsyncTask<String, String, String> {
        JSONObject json, json1;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            otherparking.clear();

            String url1;
            if (latitude == 0.0) {
                url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=40.74599864,-73.9825673&radius=50000.0&sensor=true&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&types=" + categoriy + "";
            } else {
                if (lat.equals("null") || lang.equals("null")) {
                    url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&radius=50000.0&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&types=" + categoriy + "";
                } else {
                    url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + lat + "," + lang + "&radius=50000.0&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&types=" + categoriy + "";
                    Log.e("url search__location", url1);
                }
            }
            Log.e("url", url1);
            DefaultHttpClient client1 = new DefaultHttpClient();
            HttpGet post1 = new HttpGet(url1);
            post1.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post1.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            HttpResponse response1 = null;
            try {
                response1 = client1.execute(post1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response1 != null) {

                HttpEntity resEntity1 = response1.getEntity();
                String responseStr1 = null;
                try {
                    responseStr1 = EntityUtils.toString(resEntity1).trim();
                    json1 = new JSONObject(responseStr1);
                    JSONArray googleparking = json1.getJSONArray("results");

                    for (int j = 0; j < googleparking.length(); j++) {
                        item item = new item();
                        JSONObject google = googleparking.getJSONObject(j);
                        String icon = google.getString("icon");
                        String photore = null;
                        if (google.has("photos")) {
                            String photo = google.getString("photos");
                            JSONArray photor = new JSONArray(photo);
                            JSONObject google1 = photor.getJSONObject(0);
                            photore = google1.getString("photo_reference");
                            Log.e("php", photore);
                            Log.e("icon", icon);
                        }


                        String geo = google.getString("geometry");
                        JSONObject geometry = new JSONObject(geo);
                        String location = geometry.getString("location");
                        JSONObject lant = new JSONObject(location);
                        String lat = lant.getString("lat");
                        String lang = lant.getString("lng");
                        String name = google.getString("name");
                        String vicinity = google.getString("vicinity");
                        Log.e("VICIONIITY", name);
                        item.setGooglelat(lat);
                        item.setGooglelan(lang);
                        item.setName(name);
                        item.setVicinity(vicinity);
                        item.setIcon(icon);
                        item.setPhoto_reference(photore);
                        otherparking.add(item);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            Resources rs;
            if (getActivity() != null && json1 != null) {
                if (otherparking.size() > 0) {
                    googleMap.clear();
                    new gtbitmmp().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    rl_progressbar.setVisibility(View.GONE);
                }
            } else {
                rl_progressbar.setVisibility(View.GONE);
            }
            super.onPostExecute(s);
        }
    }

    public class CustomAdaptercity extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdaptercity(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.destinationad, null);
                holder = new ViewHolder();

                holder.txt_address = (TextView) vi.findViewById(R.id.text_address);
                holder.txt_address_title = (TextView) vi.findViewById(R.id.text_address_title);
                holder.txt_lat = (TextView) vi.findViewById(R.id.txt_lat);
                holder.txt_lang = (TextView) vi.findViewById(R.id.txt_lng);
                vi.setTag(holder);

                vi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String address = holder.txt_address_title.getText().toString();
                        if (address.equals("Enter New Address")) {
                            rl_editname.setVisibility(View.VISIBLE);
                            list_destination.setVisibility(View.GONE);
                        } else {
                            if (v != null) {
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                            }
                            addrsstitle = address;
                            String address1 = holder.txt_address.getText().toString();
                            addresssub = address1;
                            txttitle.setText(address + "," + address1);
                            list_destination.setVisibility(View.GONE);
                            lat = holder.txt_lat.getText().toString();
                            lang = holder.txt_lang.getText().toString();
                            new getotherparkinglist().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                });
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }
            if (data.size() <= 0) {
            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                holder.txt_address_title.setText(tempValues.getTitle_address());
                String d = tempValues.getTitle_address();
                if (!d.equals("null")) {
                    vi.setVisibility(View.VISIBLE);
                    holder.txt_address_title.setText(tempValues.getTitle_address());
                    if (d.equals("Enter New Address")) {
                        vi.setBackgroundColor(Color.parseColor("#90caf9"));
                        holder.txt_address.setText("");
                    } else {
                        if (tempValues.isclcik()) {
                            vi.setBackgroundColor(Color.parseColor("#2F5CF2"));
                        } else {
                            vi.setBackgroundColor(Color.parseColor("#ffffff"));
                        }
                        if (tempValues.getAddress().equals("null")) {
                            holder.txt_address.setText("");
                        } else {
                            holder.txt_address.setText(tempValues.getAddress());
                        }
                        holder.txt_lat.setText(tempValues.getLat());
                        holder.txt_lang.setText(tempValues.getLang());
                    }

                } else {
                    vi.setVisibility(View.GONE);
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txt_address_title, txt_address, txt_lat, txt_lang;
        }
    }

    public class CustomAdaptercat extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdaptercat(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.adapter_nearby, null);
                holder = new ViewHolder();
                holder.txt_cat = (TextView) vi.findViewById(R.id.txt_cat_name);
                holder.img_selected = (ImageView) vi.findViewById(R.id.img_selected);
                vi.setTag(holder);
                list_cat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        Log.e("click", "yes");
                        for (int i = 0; i < cat.size(); i++) {
                            if (i != position) {
                                cat.get(i).setIsselect(false);
                            }

                        }
                        cat.get(position).setIsselect(true);
                        categoriy = cat.get(position).getCategory();
                        txt_get_cat.setText(categoriy);
                        notifyDataSetChanged();

                        if (categoriy.equals("All Categories")) {
                            categoriy = "airport%Camusement_park%Caquarium%Catm%Cbakery%Cbank%Cbicycle_store%Cbook_store%Cbus_station%Ccafe%Ccampground%Ccar_rental%Ccar_repair%Ccar_wash%Cchurch%Ccity_hall%Cconvenience_store%Cdentist%Cdoctor%Cfire_station%Cflorist%Cgas_station%Chome_goods_store%Chospital%Clibrary%Clocksmith%Clodging%Cmeal_delivery%Cmeal_takeaway%Cmuseum%Cpark%Cpharmacy%Cpolice%Cpost_office%Crestaurant%Crv_park%Cschool%Cshopping_mall%Cstadium%Csubway_station%Ctaxi_stand%Ctrain_station%Ctransit_station%Cuniversity%Cveterinary_care%Czoo";
                        } else {
                            categoriy = categoriy.toLowerCase();

                        }
                        try {
                            categoriy = categoriy.replace(" ", "_");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        new getotherparkinglist().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        cast_open = false;
                        list_cat.setVisibility(View.GONE);
                    }
                });

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                holder.txt_cat.setText(tempValues.getCategory());
                if (tempValues.Getisselect()) {
                    holder.img_selected.setVisibility(View.VISIBLE);
                } else {
                    holder.img_selected.setVisibility(View.GONE);
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txt_cat;
            public ImageView img_selected;
        }
    }

    public class getdestination extends AsyncTask<String, String, String> {
        JSONObject json = null;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "0");
        String vehiclenourl = "_table/user_locations?filter=user_id%3D" + user_id + "";
        String res_id;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            directionarray.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("url_get_wather", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    item ii = new item();
                    ii.setTitle_address("Enter New Address");
                    ii.setAddress("null");
                    ii.setIsclcik(false);
                    directionarray.add(ii);
                    String addd = adrreslocation.substring(0, adrreslocation.indexOf(","));
                    String tit = adrreslocation.substring(adrreslocation.indexOf(",") + 1);
                    String hh = tit.substring(tit.indexOf(" ") + 1);
                    item cu = new item();
                    cu.setTitle_address(addd);
                    cu.setAddress(hh);
                    cu.setLat(String.valueOf(latitude));
                    cu.setLang(String.valueOf(longitude));
                    cu.setIsclcik(true);
                    directionarray.add(cu);

                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item iii = new item();
                        JSONObject c = array.getJSONObject(i);
                        res_id = c.getString("id");
                        String address = c.getString("location_address");
                        String addressname = c.getString("location_name");
                        String lat = c.getString("lat");
                        String lang = c.getString("lng");
                        boolean hr = lat.contains("Optional");
                        if (hr) {
                            Log.e("Optional", "yes");
                            String hr1 = lat.substring(lat.indexOf('(') + 1);
                            String hr2 = hr1.substring(0, hr1.indexOf(')'));
                            String hr3 = lang.substring(lang.indexOf('(') + 1);
                            String hr4 = hr3.substring(0, hr3.indexOf(')'));
                            lat = hr2;
                            lang = hr4;
                        }
                        iii.setTitle_address(addressname);
                        iii.setAddress(address);
                        iii.setLat(lat);
                        iii.setIsclcik(false);
                        iii.setLang(lang);
                        directionarray.add(iii);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity == null) {
                } else {
                    if (json != null) {
                        CustomAdaptercity adpater = new CustomAdaptercity(getActivity(), directionarray, rs = getResources());
                        list_destination.setAdapter(adpater);
                    } else {
                        item ii = new item();
                        ii.setTitle_address("Enter New Address");
                        ii.setIsclcik(false);
                        ii.setAddress("null");
                        directionarray.add(ii);
                        CustomAdaptercity adpater = new CustomAdaptercity(getActivity(), directionarray, rs = getResources());
                        list_destination.setAdapter(adpater);
                    }
                }
            } else {
                item ii = new item();
                ii.setTitle_address("Enter New Address");
                ii.setIsclcik(false);
                ii.setAddress("null");
                directionarray.add(ii);
                CustomAdaptercity adpater = new CustomAdaptercity(getActivity(), directionarray, rs = getResources());
                list_destination.setAdapter(adpater);
            }
            if (getActivity() != null)
                new getotherparkinglist().execute();
            super.onPostExecute(s);
        }
    }

    public static int getPixelsFromDp(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    private class InfoWindowLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {
        @Override
        public void onGlobalLayout() {
            //размеры окна изменились, обновляем смещения
            popupXOffset = infoWindowContainer.getWidth() / 2;
            popupYOffset = infoWindowContainer.getHeight();
        }
    }

    private class PositionUpdaterRunnable implements Runnable {
        private int lastXPosition = Integer.MIN_VALUE;
        private int lastYPosition = Integer.MIN_VALUE;

        @Override
        public void run() {
            SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            fm.getMapAsync(googleMap1 -> {
                //помещаем в очередь следующий цикл обновления
                handler.postDelayed(this, POPUP_POSITION_REFRESH_INTERVAL);

                //если всплывающее окно скрыто, ничего не делаем
                if (trackedPosition != null && infoWindowContainer.getVisibility() == VISIBLE) {
                    Point targetPosition = googleMap1.getProjection().toScreenLocation(trackedPosition);

                    //если положение окна не изменилось, ничего не делаем
                    if (lastXPosition != targetPosition.x || lastYPosition != targetPosition.y) {
                        //обновляем положение
                        overlayLayoutParams.x = targetPosition.x - popupXOffset;
                        overlayLayoutParams.y = targetPosition.y - popupYOffset - markerHeight - 30;
                        infoWindowContainer.setLayoutParams(overlayLayoutParams);

                        //запоминаем текущие координаты
                        lastXPosition = targetPosition.x;
                        lastYPosition = targetPosition.y;
                    }
                }

            });
        }
    }
}