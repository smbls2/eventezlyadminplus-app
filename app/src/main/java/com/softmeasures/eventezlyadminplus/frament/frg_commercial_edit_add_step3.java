package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1.commercial_loc_code;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1.commercial_title;

public class frg_commercial_edit_add_step3 extends BaseFragment {

    RelativeLayout rl_progressbar;
    TextView txt_rules_add;
    ListView list_commercial_parking_rules;
    ArrayList<item> commercial_parking_rules = new ArrayList<>();
    public static String parking_rules_location_code, parking_rules_id, marker_type, max_duation, duation_unit, pricing, pricing_duation, start_time, end_time, custome_notices;
    public static boolean isclickeditrules = false;
    TextView txt_commercial_sp3_next;
    private String township_type_sp = "";
    private RelativeLayout rl_main;
    private boolean isNewLocation = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getArguments() != null) {
            isNewLocation = getArguments().getBoolean("isNewLocation", isNewLocation);
            township_type_sp = getArguments().getString("township_type_sp");
        }
        return inflater.inflate(R.layout.commercial_parking_rules_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {

        rl_main.setOnClickListener(view -> {

        });
        txt_rules_add.setOnClickListener(v -> {
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            frg_commercial_edit_add_step4 pay = new frg_commercial_edit_add_step4();
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, pay);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(pay);
            ft.commitAllowingStateLoss();
        });

        list_commercial_parking_rules.setOnItemClickListener((parent, view, position, id) -> {
            isclickeditrules = true;
            max_duation = commercial_parking_rules.get(position).getMax_time();
            duation_unit = commercial_parking_rules.get(position).getDuration_unit();
            pricing = commercial_parking_rules.get(position).getPricing();
            pricing_duation = commercial_parking_rules.get(position).getPricing_duration();
            start_time = commercial_parking_rules.get(position).getStart_time();
            end_time = commercial_parking_rules.get(position).getEnd_time();
            custome_notices = commercial_parking_rules.get(position).getCustom_notice();
            parking_rules_location_code = commercial_parking_rules.get(position).getLocation_code();
            marker_type = commercial_parking_rules.get(position).getMarker_type();
            parking_rules_id = commercial_parking_rules.get(position).getId();
            frg_commercial_edit_add_step4.isReservationAllowed = commercial_parking_rules.get(position).isReservationAllowed();
            frg_commercial_edit_add_step4.isPrePaymentRequired = commercial_parking_rules.get(position).isPrePymntReqd_for_Reservation();
            frg_commercial_edit_add_step4.isPostPaymentAllowed = commercial_parking_rules.get(position).isParkNow_PostPaymentAllowed();
            frg_commercial_edit_add_step4.isCancellationAllowed = commercial_parking_rules.get(position).isCanCancel_Reservation();
            frg_commercial_edit_add_step4.isVerifyLotAvailability = commercial_parking_rules.get(position).isBefore_reserve_verify_lot_avbl();
            frg_commercial_edit_add_step4.isCountReservedSpot = commercial_parking_rules.get(position).isInclude_reservations_for_avbl_calc();
            frg_commercial_edit_add_step4.postPayTermReservation = commercial_parking_rules.get(position).getReservation_PostPayment_term();
            frg_commercial_edit_add_step4.postPayTerm = commercial_parking_rules.get(position).getParkNow_PostPayment_term();
            frg_commercial_edit_add_step4.postPayLateFees = commercial_parking_rules.get(position).getParkNow_PostPayment_LateFee();
            frg_commercial_edit_add_step4.postPayLateFeesReservation = commercial_parking_rules.get(position).getReservation_PostPayment_LateFee();
            frg_commercial_edit_add_step4.cancellationCharge = commercial_parking_rules.get(position).getCancellation_Charge();

            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            frg_commercial_edit_add_step4 pay = new frg_commercial_edit_add_step4();
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, pay);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(pay);
            ft.commitAllowingStateLoss();
        });

        txt_commercial_sp3_next.setOnClickListener(v -> {
            if (v != null) {

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
            if (commercial_parking_rules.size() > 0) {
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                frg_commercial_edit_add_step5 pay = new frg_commercial_edit_add_step5();
                Bundle bundle = getArguments();
                if (bundle == null) bundle = new Bundle();
                bundle.putBoolean("isNewLocation", isNewLocation);
                pay.setArguments(bundle);
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("No parking rule added, please add at least one rule for parking");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alertDialog.show();
            }
        });
    }

    @Override
    protected void initViews(View view) {
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        txt_rules_add = (TextView) view.findViewById(R.id.txt_commerical_ruels_add);
        list_commercial_parking_rules = (ListView) view.findViewById(R.id.list_commercial_rules);
        txt_commercial_sp3_next = (TextView) view.findViewById(R.id.txt_commercial_sp3);
        rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
    }

    public class CustomAdapterRules extends BaseAdapter implements View.OnClickListener {

        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdapterRules(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txt_title;
            ImageView IMG;
            private TextView tvRulePricing, tvRuleMaxParkingAllowed, tvRuleTime;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.rules_adapter, null);

                holder = new ViewHolder();
                holder.txt_title = (TextView) vi.findViewById(R.id.txt_location_name);
                holder.tvRulePricing = vi.findViewById(R.id.tvRulePricing);
                holder.tvRuleMaxParkingAllowed = vi.findViewById(R.id.tvRuleMaxParkingAllowed);
                holder.tvRuleTime = vi.findViewById(R.id.tvRuleTime);

                vi.setTag(holder);
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (data.size() <= 0) {
                vi.setVisibility(View.GONE);
            } else {
                vi.setVisibility(View.VISIBLE);
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String cat = tempValues.getAddress();
                if (tempValues.getLoationname().equals("null") || tempValues.getLoationname().equals("")) {
                    holder.txt_title.setText(commercial_title);
                } else {
                    holder.txt_title.setText(tempValues.getLoationname());
                }
                String time;
                int duration;
                String trails = null;
                if (!tempValues.getMax_time().equals("null") || !tempValues.getMax_time().equals("") || !tempValues.getMax_time().equals(null)) {
                    time = tempValues.getMax_time();
                    duration = Integer.parseInt(time);
                    trails = (duration < 2) ? "" : "s";
                } else {
                    time = "";
                    trails = "";
                }
                holder.tvRulePricing.setText(String.format("$%s/%s %s", tempValues.getPricing(),
                        tempValues.getPricing_duration(), tempValues.getDuration_unit()));
                holder.tvRuleMaxParkingAllowed.setText(String.format("MAX: %s %s",
                        tempValues.getMax_time(), tempValues.getDuration_unit()));
//                holder.txt_address.setText("$" + tempValues.getPricing_duration() + " @ " + time + tempValues.getDuration_unit() + trails);
                holder.tvRuleTime.setText(String.format("OPEN: %s", tempValues.getTime_rule()));
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

    //get commercial rules..............
    public class getcommercialrulesparking extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        String parkingurl = "";

        public getcommercialrulesparking(String loc) {
            this.location = loc;
            try {
                this.parkingurl = "_table/google_parking_rules?filter=location_code%3D" + URLEncoder.encode(loc, "utf-8") + "";
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            commercial_parking_rules.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setLoationname("");
                        item1.setTime_rule(c.getString("time_rule"));
                        item1.setPricing_duration(c.getString("pricing_duration"));
                        item1.setDuration_unit(c.getString("duration_unit"));
                        item1.setMax_time(c.getString("max_duration"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        String time_rules = c.getString("time_rule");
                        item1.setPricing(c.getString("pricing"));
                        item1.setEffect(c.getString("in_effect"));
                        item1.setId(c.getString("id"));
                        item1.setLocation_code(c.getString("location_code"));
                        if (time_rules.contains("-")) {
                            String start_time = time_rules.substring(0, time_rules.indexOf("-"));
                            String end_time = time_rules.substring(time_rules.indexOf("-") + 1);
                            item1.setStart_time(start_time);
                            item1.setEnd_time(end_time);
                        } else {
                            item1.setStart_time("null");
                            item1.setEnd_time("null");
                        }

                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item1.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }

                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item1.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }

                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item1.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }

                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item1.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }

                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item1.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }

                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item1.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item1.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }

                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item1.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }

                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item1.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }

                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item1.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }

                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item1.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }
                        commercial_parking_rules.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null) {
                if (json != null) {
                    list_commercial_parking_rules.setAdapter(new CustomAdapterRules(getActivity(), commercial_parking_rules, getActivity().getResources()));
                }
            }
            super.onPostExecute(s);
        }
    }

    @Override
    public void onResume() {
        if (myApp.isLocationAdded) {
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
        } else {
            new getcommercialrulesparking(commercial_loc_code).execute();
        }
        super.onResume();
    }
}
