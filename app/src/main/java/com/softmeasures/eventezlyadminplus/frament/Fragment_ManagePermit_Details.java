package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.Main_Activity;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.frament.user_profile.Frg_User_edit_profile_Permit;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

/**
 * Created by Significant Info on 3/20/2018.
 */

public class Fragment_ManagePermit_Details extends Fragment {

    View rootView;
    ConnectionDetector cd;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar;
    Context mContext;
    TextView txt_Print, txt_PERMIT, txt_ISSUED, txt_FEE, txt_ProfileDetails,
            txt_ExpiryTime, txt_User_comments, txt_Town_comments;
    TextView txt_PENDING, txt_ADDNOTE, txt_REJECT, txt_APPROVE, btn_ProfileEdit;
    ImageView img_PrintLogo, img_SignatureProof;

    String id, date_time, user_id, township_code, township_name, permit_type, permit_name, driver_proof, residency_proof,
            approved, status, paid, scheme_type, rate, user_name, signature, Logo,
            First_contact_date, Permit_nextnum, Full_name, Full_address, email, phone, User_comments,
            Town_comments, permit_id, permit_validity, permit_expires_on, expired, Doc_requirements;

    AmazonS3 s3;
    TransferUtility transferUtility;

    ArrayList<String> arrayListResidency, arrayListDriver;
    RecyclerView rv_horizontal, rv_DriverProof;
    CustomAdapter horizontalAdapter;
    DriverAdapter driverAdapter;
    int s_pos = 0, d_pos = 0;
    ArrayList<String> listResidency, listDriver;
    String timeStamp;
    Bitmap mSignature;
    SimpleDateFormat dateFormat, dateFormat1;
    String Expires_Date = "";
    int Permitnextnum;
    TextView txt_ValidUntil, txt_Requirements;
    public String file_download_path = "";
    File dir_File;
    String Current_datetime;
    Button btn_SeeMoreCommunication;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_managepermit_details, container, false);
        mContext = getContext();
        StrictMode.VmPolicy policy = new StrictMode.VmPolicy.Builder().detectAll().penaltyLog().build();
        StrictMode.setVmPolicy(policy);
        cd = new ConnectionDetector(mContext);
        s3 = new AmazonS3Client(new BasicAWSCredentials("AKIAJNG22KFRVUAICSEA", "NSrQR/yAg52RPD3kp3FMb3NO+Vrxuty4iAwPU4th"));
        transferUtility = new TransferUtility(s3, getActivity().getApplicationContext());
        arrayListResidency = new ArrayList<String>();
        arrayListDriver = new ArrayList<String>();

        file_download_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" +
                getActivity().getPackageName() + "/Images/";
        dir_File = new File(file_download_path);
        if (!dir_File.exists()) {
            if (!dir_File.mkdirs()) {
            }
        }

        init();

        if (getArguments() != null) {
            id = getArguments().getString("id");
            date_time = getArguments().getString("date_time");
            user_id = getArguments().getString("user_id");
            township_code = getArguments().getString("township_code");
            township_name = getArguments().getString("township_name");
            permit_type = getArguments().getString("permit_type");
            permit_name = getArguments().getString("permit_name");
            driver_proof = getArguments().getString("driver_proof");
            residency_proof = getArguments().getString("residency_proof");
            approved = getArguments().getString("approved");
            status = getArguments().getString("status");
            paid = getArguments().getString("paid");
            scheme_type = getArguments().getString("scheme_type");
            rate = getArguments().getString("rate");
            signature = getArguments().getString("signature");
            Logo = getArguments().getString("Logo");
            user_name = getArguments().getString("user_name");

            First_contact_date = getArguments().getString("First_contact_date");
            Permit_nextnum = getArguments().getString("Permit_nextnum");
            Full_name = getArguments().getString("Full_name");
            Full_address = getArguments().getString("Full_address");
            email = getArguments().getString("email");
            phone = getArguments().getString("phone");
            User_comments = getArguments().getString("User_comments");
            Town_comments = getArguments().getString("Town_comments");

            permit_id = getArguments().getString("permit_id");
            permit_validity = getArguments().getString("permit_validity");
            permit_expires_on = getArguments().getString("permit_expires_on");
            expired = getArguments().getString("expired");
            Doc_requirements = getArguments().getString("Doc_requirements");
        }

        txt_ValidUntil.setText(permit_validity);
        txt_Requirements.setText(Doc_requirements);

        if (!Full_name.equalsIgnoreCase("")) {
            txt_ProfileDetails.setText("Name: " + Full_name + "\nPhone : " + phone + " Email : " + email
                    + "\nAddress : " + Full_address);
        }

        if (!User_comments.equalsIgnoreCase("")) {
            txt_User_comments.setText(User_comments);
        }

        if (!Town_comments.equalsIgnoreCase("")) {
            txt_Town_comments.setText(Town_comments);
        }

        try {
            String str = Permit_nextnum.substring(Permit_nextnum.lastIndexOf("-") + 1);
            Permitnextnum = Integer.parseInt(str) + 1;
            Log.e("Permit nextnum : " + Permitnextnum, " : " + Permit_nextnum);
        } catch (Exception e) {
            e.printStackTrace();
        }

        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        Calendar getCurrent = Calendar.getInstance();
        String getDate = dateFormat.format(getCurrent.getTime());
        try {
            Date pDate = dateFormat.parse(getDate);
            if (permit_validity.equalsIgnoreCase("365 days")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);
                c.add(Calendar.DATE, 365);
                Date expDate = c.getTime();
                Expires_Date = dateFormat1.format(expDate) + " 23:59:59";
            } else if (permit_validity.equalsIgnoreCase("YearEnd")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);
                Expires_Date = String.valueOf(c.get(Calendar.YEAR)) + "-12-31 23:59:59";
            } else if (permit_validity.equalsIgnoreCase("MonthEnd")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);
                int thisMonth = c.get(Calendar.MONTH) + 1;
                Expires_Date = String.valueOf(c.get(Calendar.YEAR) + "-" + thisMonth
                        + "-" + c.getActualMaximum(Calendar.DATE)) + " 23:59:59";
            } else if (permit_validity.equalsIgnoreCase("30 Days")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);
                c.add(Calendar.DATE, 30);
                Date expDate = c.getTime();
                Expires_Date = dateFormat.format(expDate);
            } else if (permit_validity.equalsIgnoreCase("7 Days")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);
                c.add(Calendar.DATE, 7);
                Date expDate = c.getTime();
                Expires_Date = dateFormat.format(expDate);
            } else if (permit_validity.equalsIgnoreCase("Weekend")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);

                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                if (Calendar.MONDAY == dayOfWeek) {
                    c.add(Calendar.DATE, 6);
                } else if (Calendar.TUESDAY == dayOfWeek) {
                    c.add(Calendar.DATE, 5);
                } else if (Calendar.WEDNESDAY == dayOfWeek) {
                    c.add(Calendar.DATE, 4);
                } else if (Calendar.THURSDAY == dayOfWeek) {
                    c.add(Calendar.DATE, 3);
                } else if (Calendar.FRIDAY == dayOfWeek) {
                    c.add(Calendar.DATE, 2);
                } else if (Calendar.SATURDAY == dayOfWeek) {
                    c.add(Calendar.DATE, 1);
                } else if (Calendar.SUNDAY == dayOfWeek) {
                    c.add(Calendar.DATE, 0);
                }

                int thisMonth = c.get(Calendar.MONTH) + 1;
                Expires_Date = String.valueOf(c.get(Calendar.YEAR) + "-" + thisMonth
                        + "-" + c.get(Calendar.DAY_OF_MONTH)) + " 23:59:59";
            } else if (permit_validity.equalsIgnoreCase("1 Days")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);
                c.add(Calendar.DATE, 1);
                Date expDate = c.getTime();
                Expires_Date = dateFormat.format(expDate);
            } else if (permit_validity.equalsIgnoreCase("24 Hours")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);
                c.add(Calendar.DATE, 1);
                Date expDate = c.getTime();
                Expires_Date = dateFormat.format(expDate);
            } else if (permit_validity.equalsIgnoreCase("Daily")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);
                int thisMonth = c.get(Calendar.MONTH) + 1;
                Expires_Date = String.valueOf(c.get(Calendar.YEAR) + "-" + thisMonth
                        + "-" + c.get(Calendar.DAY_OF_MONTH)) + " 23:59:59";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        onClicks();
        txt_PERMIT.setText(township_code + "-" + permit_name);
        txt_ISSUED.setText(date_time);
        if (rate.equalsIgnoreCase("") || rate.equalsIgnoreCase("null") || rate.isEmpty()) {
            txt_FEE.setText("$0");
        } else {
            txt_FEE.setText("" + rate);
        }

        txt_ExpiryTime.setText(permit_expires_on);

        if (!signature.equalsIgnoreCase("")) {
            img_SignatureProof.setVisibility(View.VISIBLE);
            Signature();
        }

        if (!residency_proof.equalsIgnoreCase("")) {
            listResidency = new ArrayList<String>(Arrays.asList(residency_proof.split(",")));
            if (listResidency.size() > 0) {
                rv_horizontal.setVisibility(View.VISIBLE);
                Residence(0);
            } else {
                rv_horizontal.setVisibility(View.GONE);
            }
        } else {
            rv_horizontal.setVisibility(View.GONE);
        }

        if (!driver_proof.equalsIgnoreCase("")) {
            listDriver = new ArrayList<String>(Arrays.asList(driver_proof.split(",")));
            if (listDriver.size() > 0) {
                rv_DriverProof.setVisibility(View.VISIBLE);
                Driver(0);
            } else {
                rv_DriverProof.setVisibility(View.GONE);
            }
        } else {
            rv_DriverProof.setVisibility(View.GONE);
        }

        // https://parkezly-images.s3.amazonaws.com//data/data/com.softmeasures.parkezlyandroid/cache/images/20180320_184627.jpg


        return rootView;
    }

    public void init() {
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) rootView.findViewById(R.id.rl_progressbar);
        txt_Print = (TextView) rootView.findViewById(R.id.txt_Print);
        txt_PERMIT = (TextView) rootView.findViewById(R.id.txt_PERMIT);
        txt_ISSUED = (TextView) rootView.findViewById(R.id.txt_ISSUED);
        txt_FEE = (TextView) rootView.findViewById(R.id.txt_FEE);
        txt_ProfileDetails = (TextView) rootView.findViewById(R.id.txt_ProfileDetails);
        txt_ExpiryTime = (TextView) rootView.findViewById(R.id.txt_ExpiryTime);
        img_SignatureProof = (ImageView) rootView.findViewById(R.id.img_SignatureProof);

        //onClick
        txt_PENDING = (TextView) rootView.findViewById(R.id.txt_PENDING);
        txt_ADDNOTE = (TextView) rootView.findViewById(R.id.txt_ADDNOTE);
        txt_REJECT = (TextView) rootView.findViewById(R.id.txt_REJECT);
        txt_APPROVE = (TextView) rootView.findViewById(R.id.txt_APPROVE);
        img_PrintLogo = (ImageView) rootView.findViewById(R.id.img_PrintLogo);

        rv_horizontal = (RecyclerView) rootView.findViewById(R.id.rv_horizontal);
        horizontalAdapter = new CustomAdapter(getContext(), arrayListResidency);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rv_horizontal.setLayoutManager(horizontalLayoutManagaer);
        rv_horizontal.setAdapter(horizontalAdapter);

        rv_DriverProof = (RecyclerView) rootView.findViewById(R.id.rv_DriverProof);
        driverAdapter = new DriverAdapter(getContext(), arrayListDriver);
        LinearLayoutManager DriverProofLayoutManagaer = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rv_DriverProof.setLayoutManager(DriverProofLayoutManagaer);
        rv_DriverProof.setAdapter(driverAdapter);

        txt_User_comments = (TextView) rootView.findViewById(R.id.txt_User_comments);
        txt_Town_comments = (TextView) rootView.findViewById(R.id.txt_Town_comments);

        txt_ValidUntil = (TextView) rootView.findViewById(R.id.txt_ValidUntil);
        txt_Requirements = (TextView) rootView.findViewById(R.id.txt_Requirements);
        btn_ProfileEdit = (TextView) rootView.findViewById(R.id.btn_ProfileEdit);
        btn_SeeMoreCommunication = (Button) rootView.findViewById(R.id.btn_SeeMoreCommunication);

    }

    public void onClicks() {

        if (!Logo.equalsIgnoreCase("") && !Logo.equalsIgnoreCase("null")) {
            Picasso.with(getActivity()).load(Logo).into(img_PrintLogo);
        }
        if (approved.equalsIgnoreCase("0")) {
            txt_PENDING.setText("PENDING");
            txt_REJECT.setVisibility(View.VISIBLE);
        } else if (approved.equalsIgnoreCase("1")) {
            txt_PENDING.setText("PYMT DUE");
            txt_APPROVE.setText("APPROVED");
            if (paid.equalsIgnoreCase("1")) {
                txt_PENDING.setText("PAID");
            }
            txt_REJECT.setVisibility(View.GONE);
        } else if (approved.equalsIgnoreCase("2")) {
            txt_PENDING.setText("UNPAID");
            txt_REJECT.setText("REJECTED");
            txt_APPROVE.setVisibility(View.GONE);
        }


        txt_PENDING.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txt_PENDING.getText().toString().trim().equalsIgnoreCase("PENDING")) {
                    new getPermit_Subscription("0").execute();
                    txt_PENDING.setText("UNPAID");
                } else if (txt_PENDING.getText().toString().trim().equalsIgnoreCase("UNPAID")) {
                    txt_PENDING.setText("UNPAID");
                } else {
                    txt_PENDING.setText("PYMT DUE");
                }
            }
        });

        txt_APPROVE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txt_APPROVE.getText().toString().trim().equalsIgnoreCase("APPROVE")) {
                    new getPermit_Subscription("1").execute();
                    txt_PENDING.setText("PYMT DUE");
                    txt_APPROVE.setText("APPROVED");
                    if (paid.equalsIgnoreCase("1")) {
                        txt_PENDING.setText("PAID");
                    }
                } else {
                    txt_PENDING.setText("PYMT DUE");
                    txt_APPROVE.setText("APPROVED");
                    if (paid.equalsIgnoreCase("1")) {
                        txt_PENDING.setText("PAID");
                    }
                }
                txt_REJECT.setVisibility(View.GONE);
            }
        });

        txt_REJECT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_REJECT.setText("REJECTED");
                new getPermit_Subscription("2").execute();
            }
        });

        txt_ADDNOTE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNotes();
            }
        });

        txt_Print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPrintOptions(getActivity());
            }
        });

        btn_ProfileEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Frg_User_edit_profile_Permit pay = new Frg_User_edit_profile_Permit();
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        btn_SeeMoreCommunication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("id", id);

                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                MyPermit_VehicleUser_Details_Communications pay = new MyPermit_VehicleUser_Details_Communications();
                pay.setArguments(bundle);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        //new get_Communications_thread().execute();

    }

    @TargetApi(Build.VERSION_CODES.M)
    public void showPrintOptions(Activity context) {
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View layout = layoutInflater.inflate(R.layout.select_printer_option, viewGroup, false);
        final PopupWindow popup = new PopupWindow(context);
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(false);
        popup.setFocusable(false);
        popup.setAnimationStyle(R.style.animationName);
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

        ImageView img_close = (ImageView) layout.findViewById(R.id.img_close);
        TextView txt_pdf = (TextView) layout.findViewById(R.id.txt_pdf);
        TextView txt_bluetooth = (TextView) layout.findViewById(R.id.txt_bluetooth);

        final String ss = "Village Of Farmingdale\n\n" + "\nPermit: " + txt_PERMIT.getText().toString() + "\nApplied On: " + txt_ISSUED.getText().toString() +
                "\nFee: " + txt_FEE.getText().toString() + "\nName: " + Full_name + "\nPhone: " + phone +
                "\nEmail: " + email + "\nAddress: " + Full_address + "\nUser Comments: " + User_comments +
                "\nTown Comments: " + Town_comments + "\nExpiry Time: " + txt_ExpiryTime.getText().toString() +
                "";

        txt_bluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), Main_Activity.class);
                i.putExtra("date", ss);
                getActivity().startActivity(i);
                popup.dismiss();
            }
        });

        txt_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                createandDisplayPdf();
            }
        });

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

    }

    public void createandDisplayPdf() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        timeStamp = dateFormat.format(new Date());
        com.itextpdf.text.Document doc = new com.itextpdf.text.Document();

        try {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ParkEZly";
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();
            File file1 = new File(path, "logo1.PNG");
            if (file1.exists()) file1.delete();
            FileOutputStream outStream = null;
            try {
                Bitmap bitmap = ((BitmapDrawable) img_PrintLogo.getDrawable()).getBitmap();
                bitmap = Bitmap.createScaledBitmap(bitmap, 120, 120, false);
                outStream = new FileOutputStream(file1);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
                outStream.flush();
                outStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            File file = new File(dir, "view_permit" + timeStamp + ".pdf");
            FileOutputStream fOut = new FileOutputStream(file);
            Font paraFont = new Font(Font.FontFamily.COURIER, 20);

            PdfWriter.getInstance(doc, fOut);

            //open the document
            doc.open();

            String ad = "Village Of Farmingdale\n\n";
            Paragraph pp = new Paragraph();
            Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 22, Font.BOLD);
            pp.setFont(smallBold);
            pp.add(ad);
            pp.setAlignment(Paragraph.ALIGN_CENTER);
            pp.setPaddingTop(30);
            doc.add(pp);

            String getpath = Environment.getExternalStorageDirectory() + "/" + "ParkEZly" + "/" + "logo1.PNG";
            Image image = Image.getInstance(getpath);
            image.setAlignment(Image.MIDDLE | Image.TEXTWRAP);
            image.setWidthPercentage(20);

            doc.add(image);

            String ad1 = "\nPermit: " + txt_PERMIT.getText().toString();
            Paragraph pp1 = new Paragraph();
            Font smallBold1 = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
            pp1.setFont(smallBold1);
            pp1.add(ad1);
            pp1.setAlignment(Paragraph.ALIGN_LEFT);
            pp1.setPaddingTop(30);
            doc.add(pp1);

            Paragraph p1 = new Paragraph("Applied On: " + txt_ISSUED.getText().toString() +
                    "\nFee: " + txt_FEE.getText().toString() + "\nName: " + Full_name + "\nPhone: " + phone +
                    "\nEmail: " + email
                    + "\nAddress: " + Full_address + "\nUser Comments: " + User_comments +
                    "\nTown Comments: " + Town_comments + "\nExpiry Time: " + txt_ExpiryTime.getText().toString() +
                    "");
            p1.setFont(paraFont);
            p1.setAlignment(Paragraph.ALIGN_LEFT);
            p1.setPaddingTop(10);
            doc.add(p1);

            PdfPTable table = new PdfPTable(1);
            table.setPaddingTop(10);
            table.getDefaultCell().setBorder(0);
            table.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell("Residence Proof: ");
            PdfPCell cell = new PdfPCell();
            cell.setBorder(Rectangle.NO_BORDER);
            Paragraph pra1 = new Paragraph();
            for (int i = 0; i < arrayListResidency.size(); i++) {
                BitmapFactory.Options bmOptions2 = new BitmapFactory.Options();
                Bitmap bitmap2 = BitmapFactory.decodeFile(arrayListResidency.get(i), bmOptions2);
                bitmap2 = Bitmap.createScaledBitmap(bitmap2, 80, 80, true);
                ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
                bitmap2.compress(Bitmap.CompressFormat.PNG, 100, stream2);
                Image img3 = Image.getInstance(stream2.toByteArray());
                pra1.add(new Chunk(img3, 0, 0, true));
            }
            cell.addElement(pra1);
            table.addCell(cell);
            doc.add(table);

            PdfPTable table1 = new PdfPTable(1);
            table1.setPaddingTop(10);
            table1.getDefaultCell().setBorder(0);
            table1.setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell("Driver Proof: ");
            PdfPCell cell1 = new PdfPCell();
            cell1.setBorder(Rectangle.NO_BORDER);
            Paragraph pra2 = new Paragraph();
            for (int i = 0; i < arrayListDriver.size(); i++) {
                BitmapFactory.Options bmOptions2 = new BitmapFactory.Options();
                Bitmap bitmap2 = BitmapFactory.decodeFile(arrayListDriver.get(i), bmOptions2);
                bitmap2 = Bitmap.createScaledBitmap(bitmap2, 80, 80, true);
                ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
                bitmap2.compress(Bitmap.CompressFormat.PNG, 100, stream2);
                Image img3 = Image.getInstance(stream2.toByteArray());
                pra2.add(new Chunk(img3, 0, 0, true));
            }
            cell1.addElement(pra2);
            table1.addCell(cell1);
            doc.add(table1);

            PdfPTable table2 = new PdfPTable(1);
            table2.setPaddingTop(10);
            table2.getDefaultCell().setBorder(0);
            table2.setHorizontalAlignment(Element.ALIGN_LEFT);
            table2.addCell("Signature Proof: ");
            PdfPCell cell2 = new PdfPCell();
            cell2.setBorder(Rectangle.NO_BORDER);
            Paragraph pra3 = new Paragraph();
            for (int i = 0; i < 1; i++) {
                Bitmap bitmap2 = mSignature;
                //BitmapFactory.Options bmOptions2 = new BitmapFactory.Options();
                //bitmap2 = BitmapFactory.decodeFile(String.valueOf(bitmap2), bmOptions2);
                bitmap2 = Bitmap.createScaledBitmap(bitmap2, 100, 150, true);
                ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
                bitmap2.compress(Bitmap.CompressFormat.PNG, 100, stream2);
                Image img3 = Image.getInstance(stream2.toByteArray());
                pra3.add(new Chunk(img3, 0, 0, true));
            }
            cell2.addElement(pra3);
            table2.addCell(cell2);
            doc.add(table2);

        } catch (DocumentException de) {
            Log.e("PDFCreator", "DocumentException:" + de);
        } catch (IOException e) {
            Log.e("PDFCreator", "ioException:" + e);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            doc.close();
        }
        String getpath = Environment.getExternalStorageDirectory() + "/" + "parkEZly" + "/" + "view_permit" + timeStamp + ".pdf";
        try {
            manipulatePdf(getpath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    public void manipulatePdf(String src) throws IOException, DocumentException {
        try {
            PdfReader reader = new PdfReader(src);
            int n = reader.getNumberOfPages();
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ParkEZly";
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();

            File file = new File(dir, "View_permits" + timeStamp + ".pdf");
            FileOutputStream fOut = new FileOutputStream(file);
            PdfStamper stamper = new PdfStamper(reader, fOut);
            stamper.setRotateContents(false);
            // text watermark
           /* Font f = new Font(Font.FontFamily.HELVETICA, 30);
            Phrase p = new Phrase("My watermark (text)", f);*/
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File file1 = new File(path, "logo.PNG");
            FileOutputStream outStream = null;
            try {
                ImageView imgq = new ImageView(getActivity());
                imgq.setImageResource(R.drawable.rsz_1rsz_splashscreen);
                Bitmap bitmap = ((BitmapDrawable) imgq.getDrawable()).getBitmap();
                outStream = new FileOutputStream(file1);
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, outStream);
                outStream.flush();
                outStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String getpath = Environment.getExternalStorageDirectory() + "/" + "ParkEZly" + "/" + "logo.PNG";
            // image watermark
            Image img = Image.getInstance(getpath);
            float w = img.getScaledWidth();
            float h = img.getScaledHeight();

            // transparency
            PdfGState gs1 = new PdfGState();
            gs1.setFillOpacity(0.2f);
            // properties
            PdfContentByte over;
            Rectangle pagesize;
            float x, y;

            // loop over every page
            for (int i = 1; i <= n; i++) {
                pagesize = reader.getPageSize(i);

                x = (pagesize.getLeft() + pagesize.getRight()) / 2;
                y = (pagesize.getTop() + pagesize.getBottom()) / 2;
                over = stamper.getOverContent(i);
                over.saveState();
                over.setGState(gs1);
                //over.addImage(img,true);
                float gg = x - (w / 2);
                float hh = y - (h / 2);
                Log.e("dcd", String.valueOf(gg));

                Log.e("dcd", String.valueOf(hh));
                over.addImage(img, w, 0, 0, h, x - (w / 2), y - (h / 2));
                //  over.addImage(img, 200,0,200,0,0,0);
                over.restoreState();
            }
            stamper.close();
            reader.close();

            // viewPdf("parking_ticket" + plate_no +timeStamp+"1" + ".pdf", "parkEZly");
            onther_view(file);
            File file11 = new File(dir, "view_permit" + timeStamp + ".pdf");
            file11.delete();
        } catch (IOException os) {
            Log.e("vb", String.valueOf(os));
        }
    }

    public void onther_view(File file) {
        File pdfFile = file;
        try {
            if (pdfFile.exists()) {
                Uri path1 = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", pdfFile);
                Intent objIntent = new Intent(Intent.ACTION_VIEW);
                objIntent.setDataAndType(path1, "application/pdf");
                objIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(objIntent);
            } else {
                Toast.makeText(getActivity(), "The file not exists! ", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addNotes() {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getActivity());
        View mView = layoutInflaterAndroid.inflate(R.layout.add_notes_dialog_box, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(getActivity());
        alertDialogBuilderUserInput.setView(mView);

        final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogBox, int id) {
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
                        Current_datetime = dateformat.format(c.getTime());
                        String addNotes = userInputDialogEditText.getText().toString().trim();
                        if (!addNotes.equalsIgnoreCase("")) {
                            txt_Town_comments.setText(addNotes);
                            new addNotes_Subscription(addNotes).execute();
                        } else {
                            Toast.makeText(getContext(), "Please Enter Notes..", Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.show();
    }

    public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {
        ArrayList<String> dataSet;
        Context mContext;

        public CustomAdapter(Context context, ArrayList<String> arrayListSignature) {
            mContext = context;
            dataSet = arrayListSignature;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView imageViewIcon;
            TextView txt_Name;

            public MyViewHolder(View itemView) {
                super(itemView);
                this.imageViewIcon = (ImageView) itemView.findViewById(R.id.image);
                txt_Name = (TextView) itemView.findViewById(R.id.txt_Name);
            }
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapater_managepermit_items, parent, false);

            MyViewHolder myViewHolder = new MyViewHolder(view);
            return myViewHolder;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
            final String extension = dataSet.get(listPosition).toString().substring(dataSet.get(listPosition).toString().lastIndexOf("."));
            if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg")
                    || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".bmp")) {
                Bitmap bmp = BitmapFactory.decodeFile(dataSet.get(listPosition));
                holder.imageViewIcon.setImageBitmap(bmp);
            } else {
                holder.imageViewIcon.setImageDrawable(getResources().getDrawable(R.mipmap.docs));
            }

            holder.txt_Name.setText((new File(dataSet.get(listPosition)).getName()));

            holder.imageViewIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg")
                            || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".bmp")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(new File(dataSet.get(listPosition))), "image/*");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        MimeTypeMap myMime = MimeTypeMap.getSingleton();
                        Intent newIntent = new Intent(Intent.ACTION_VIEW);
                        String mimeType = myMime.getMimeTypeFromExtension(fileExt(String.valueOf(((dataSet.get(listPosition))))).substring(1));
                        newIntent.setDataAndType(Uri.fromFile((new File(dataSet.get(listPosition)))), mimeType);
                        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        try {
                            getActivity().startActivity(newIntent);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(getActivity(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
                        }
                    }

                }
            });
        }

        @Override
        public int getItemCount() {
            return dataSet.size();
        }
    }

    public class DriverAdapter extends RecyclerView.Adapter<DriverAdapter.MyViewHolder> {
        ArrayList<String> dataSet;
        Context mContext;

        public DriverAdapter(Context context, ArrayList<String> arrayListSignature) {
            mContext = context;
            dataSet = arrayListSignature;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView imageViewIcon;
            TextView txt_Name;

            public MyViewHolder(View itemView) {
                super(itemView);
                imageViewIcon = (ImageView) itemView.findViewById(R.id.image);
                txt_Name = (TextView) itemView.findViewById(R.id.txt_Name);
            }
        }

        @Override
        public DriverAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapater_managepermit_items, parent, false);

            DriverAdapter.MyViewHolder myViewHolder = new DriverAdapter.MyViewHolder(view);
            return myViewHolder;
        }

        @Override
        public void onBindViewHolder(final DriverAdapter.MyViewHolder holder, final int listPosition) {
            final String extension = dataSet.get(listPosition).toString().substring(dataSet.get(listPosition).toString().lastIndexOf("."));
            if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg")
                    || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".bmp")) {
                Bitmap bmp = BitmapFactory.decodeFile(dataSet.get(listPosition));
                holder.imageViewIcon.setImageBitmap(bmp);
            } else {
                holder.imageViewIcon.setImageDrawable(getResources().getDrawable(R.mipmap.docs));
            }

            holder.txt_Name.setText((new File(dataSet.get(listPosition)).getName()));

            holder.imageViewIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg")
                            || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".bmp")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(new File(dataSet.get(listPosition))), "image/*");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        MimeTypeMap myMime = MimeTypeMap.getSingleton();
                        Intent newIntent = new Intent(Intent.ACTION_VIEW);
                        String mimeType = myMime.getMimeTypeFromExtension(fileExt(String.valueOf(((dataSet.get(listPosition))))).substring(1));
                        newIntent.setDataAndType(Uri.fromFile((new File(dataSet.get(listPosition)))), mimeType);
                        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        try {
                            getActivity().startActivity(newIntent);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(getActivity(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
                        }
                    }

                }
            });
        }

        @Override
        public int getItemCount() {
            return dataSet.size();
        }
    }

    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();

        }
    }

    public void Residence(int current_pos) {
        s_pos = current_pos;
        try {
            final File file_exists = new File(file_download_path, listResidency.get(s_pos));
            if (file_exists.exists()) {
                arrayListResidency.add(file_exists.getAbsolutePath());
                horizontalAdapter.notifyDataSetChanged();
                if (listResidency.size() == s_pos + 1) {

                } else {
                    s_pos++;
                    Residence(s_pos);
                }
            } else {
                TransferObserver downloadObserver = transferUtility.download("parkezly-images", "/images/" + listResidency.get(s_pos), file_exists);
                downloadObserver.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (TransferState.COMPLETED == state) {
                            arrayListResidency.add(file_exists.getAbsolutePath());
                            horizontalAdapter.notifyDataSetChanged();
                            if (listResidency.size() == s_pos + 1) {

                            } else {
                                s_pos++;
                                Residence(s_pos);
                            }
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                        int percentDone = (int) percentDonef;
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        ex.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Driver(int current_pos) {
        d_pos = current_pos;
        try {
            final File file_exists = new File(file_download_path, listDriver.get(d_pos));
            if (file_exists.exists()) {
                arrayListDriver.add(file_exists.getAbsolutePath());
                driverAdapter.notifyDataSetChanged();
                if (listDriver.size() == d_pos + 1) {

                } else {
                    d_pos++;
                    Driver(d_pos);
                }
            } else {
                TransferObserver downloadObserver = transferUtility.download("parkezly-images", "/images/" + listDriver.get(d_pos), file_exists);

                downloadObserver.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (TransferState.COMPLETED == state) {
                            arrayListDriver.add(file_exists.getAbsolutePath());
                            driverAdapter.notifyDataSetChanged();
                            if (listDriver.size() == d_pos + 1) {

                            } else {
                                d_pos++;
                                Driver(d_pos);
                            }
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                        int percentDone = (int) percentDonef;
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        ex.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Signature() {
        try {
            final File file_exists = new File(file_download_path, signature);
            if (file_exists.exists()) {
                Bitmap bmp = BitmapFactory.decodeFile(file_exists.getAbsolutePath());
                //str_SignaturePath = file_exists.getAbsolutePath();
                mSignature = bmp;
                img_SignatureProof.setImageBitmap(bmp);
            } else {
                TransferObserver downloadObserver = transferUtility.download("parkezly-images",
                        "/images/" + signature, file_exists);

                downloadObserver.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (TransferState.COMPLETED == state) {
                            Bitmap bmp = BitmapFactory.decodeFile(file_exists.getAbsolutePath());
                            //str_SignaturePath = file_exists.getAbsolutePath();
                            mSignature = bmp;
                            img_SignatureProof.setImageBitmap(bmp);
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                        int percentDone = (int) percentDonef;
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        ex.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class addNotes_Subscription extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String responseStr = null;
        String managedlosturl = "_table/permit_subscription";
        Date current;
        String mAddNotes = "";

        public addNotes_Subscription(String addNotes) {
            mAddNotes = addNotes;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();

            jsonValues1.put("id", id);
            jsonValues1.put("town_comments", mAddNotes);

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return responseStr;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && responseStr != null) {
                boolean errors = responseStr.indexOf("error") > 0;
                if (errors) {
                    Toast.makeText(getContext(), "Some Error..", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getContext(), "Successfully added", Toast.LENGTH_LONG).show();
                    try {
                        JSONObject jsonObject = new JSONObject(responseStr);
                        JSONArray jsonArray = jsonObject.getJSONArray("resource");
                        String jsonObject1 = jsonArray.getJSONObject(0).getString("id");
                        new addNotes_Subscription_Archive(mAddNotes, jsonObject1).execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public class addNotes_Subscription_Archive extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String responseStr = null;
        String managedlosturl = "_table/permit_subscription_archive";
        Date current;
        String mAddNotes = "", permit_sbscription_id = "";

        public addNotes_Subscription_Archive(String addNotes, String permit_Id) {
            mAddNotes = addNotes;
            permit_sbscription_id = permit_Id;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("permit_sbscription_id", permit_sbscription_id);
            jsonValues1.put("id", id);
            jsonValues1.put("town_comments", mAddNotes);

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("responseStr ", " : " + responseStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return responseStr;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null)
                new addNotes_Communications_thread(mAddNotes, permit_sbscription_id).execute();
        }
    }

    public class addNotes_Communications_thread extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String responseStr = null;
        String managedlosturl = "_table/communications_thread";
        Date current;
        String mAddNotes = "", DateTime = "";

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String user_role = logindeatl.getString("user_role", "null");
        String name = logindeatl.getString("name", "null");

        public addNotes_Communications_thread(String addNotes, String dateTime) {
            mAddNotes = addNotes;
            DateTime = dateTime;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("date_time", Current_datetime);
            jsonValues1.put("user_id", user_id);
            jsonValues1.put("user_name", user_id);
            jsonValues1.put("email", email);
            jsonValues1.put("phone", phone);
            jsonValues1.put("permit_subscription_id", DateTime);
            jsonValues1.put("profile_name", name);
            jsonValues1.put("profile_type", user_role);
            jsonValues1.put("comments", mAddNotes);

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("responseStr ", " : " + responseStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return responseStr;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
        }
    }

    public class get_Communications_thread extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String responseStr = null;
        String managedlosturl = "_table/communications_thread";

        {
            try {
                managedlosturl = "_table/communications_thread?filter=permit_subscription_id%3D'" + URLEncoder.encode(id, "utf8") + "'";
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("responseStr ", " : " + responseStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return responseStr;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (responseStr != null) {

            }
        }
    }

    public class getPermit_Subscription extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1 = new JSONArray();
        String managedlosturl = "_table/permit_subscription";
        Date current;
        String approved = "0";
        String responseStr = null;

        public getPermit_Subscription(String s) {
            approved = s;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("id", id);
            jsonValues1.put("approved", approved);
            if (approved.equalsIgnoreCase("0")) {
                jsonValues1.put("paid", "0");
            } else if (approved.equalsIgnoreCase("1")) {
                jsonValues1.put("paid", "0");
                jsonValues1.put("permit_validity", permit_validity);
                jsonValues1.put("permit_expires_on", Expires_Date);
            } else if (approved.equalsIgnoreCase("2")) {
                jsonValues1.put("paid", "2");
            }

            JSONObject json1 = new JSONObject(jsonValues1);

            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("responseStr ", " : " + responseStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            try {
                Toast.makeText(getContext(), "Permits Updated...", Toast.LENGTH_LONG).show();
                JSONObject jsonObject = new JSONObject(responseStr);
                JSONArray jsonArray = jsonObject.getJSONArray("resource");
                String jsonObject1 = jsonArray.getJSONObject(0).getString("id");
                new getPermit_Subscription_Archive(approved, jsonObject1).execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class getPermit_Subscription_Archive extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1 = new JSONArray();
        String managedlosturl = "_table/permit_subscription_archive";
        Date current;
        String approved = "0", permit_sbscription_id;

        public getPermit_Subscription_Archive(String s, String permit_Id) {
            approved = s;
            permit_sbscription_id = permit_Id;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("permit_sbscription_id", permit_sbscription_id);
            jsonValues1.put("id", id);
            jsonValues1.put("approved", approved);
            if (approved.equalsIgnoreCase("0")) {
                jsonValues1.put("paid", "0");
            } else if (approved.equalsIgnoreCase("1")) {
                jsonValues1.put("paid", "0");
                jsonValues1.put("permit_validity", permit_validity);
                jsonValues1.put("permit_expires_on", Expires_Date);
            } else if (approved.equalsIgnoreCase("2")) {
                jsonValues1.put("paid", "2");
            }

            JSONObject json1 = new JSONObject(jsonValues1);

            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("responseStr ", " : " + responseStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            try {
                if (approved.equalsIgnoreCase("1")) {
                    new getPermitData().execute();
                } else {
                    //fragmentStack.clear();
                    //getActivity().onBackPressed();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class getPermitData extends AsyncTask<String, String, String> {

        String vehiclenourl = "_table/parking_permits";
        JSONObject json = null;

        public getPermitData() throws UnsupportedEncodingException {
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("get_vehicle_url", url);

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("id", permit_id);
            jsonValues1.put("permit_nextnum", Permitnextnum);

            JSONObject json1 = new JSONObject(jsonValues1);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;

            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity != null) {
                    //fragmentStack.clear();
                    //getActivity().onBackPressed();
                }
            }
        }
    }


}
