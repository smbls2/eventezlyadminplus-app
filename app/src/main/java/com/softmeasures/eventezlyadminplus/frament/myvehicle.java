package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.concurrent.NotThreadSafe;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class myvehicle extends Fragment implements vehicle_add.vehivcle_add_Click_listener {

    ProgressDialog pdialog;
    ArrayList<item> vehiclearry = new ArrayList<>();
    CustomAdaptercity adpater;
    ListView listofvehicleno;
    TextView txttitle, btnaddvehicle, txtshkip;
    Map<String, Object> statefullname;
    static RelativeLayout addvehicle;
    Spinner spsate;
    EditText editetxtplatno;
    String statename, platno, id;
    Button btnsave;
    int i = 1;
    String statearray[] = {"State", "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"};
    ConnectionDetector cd;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar, rl_list;
    Animation animation;
    ImageView btn_delete;
    Boolean getvehicle = false;

    String v_country = "", v_state = "", v_color = "", v_make = "", v_model = "", v_year = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_myvehicle, container, false);
        listofvehicleno = (ListView) view.findViewById(R.id.listofvehicle);
        txttitle = (TextView) view.findViewById(R.id.txttitte);
        addvehicle = (RelativeLayout) view.findViewById(R.id.addvehicle);
        btnaddvehicle = (TextView) view.findViewById(R.id.txtaddvehicle);
        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Thin.ttf");
        txttitle.setTypeface(type);
        spsate = (Spinner) view.findViewById(R.id.spinnerstate);
        editetxtplatno = (EditText) view.findViewById(R.id.edittext_vehicle);
        btnsave = (Button) view.findViewById(R.id.btnsave);
        txtshkip = (TextView) view.findViewById(R.id.textskip);
        txttitle.setText("My Vehicles");
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        rl_list = (RelativeLayout) view.findViewById(R.id.rl_list);
        btn_delete = (ImageView) view.findViewById(R.id.btn_delete);
        cd = new ConnectionDetector(getActivity());
        RelativeLayout main = (RelativeLayout) view.findViewById(R.id.main);

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        addvehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
            }
        });

        if (cd.isConnectingToInternet()) {
            new getvehiclenumber().execute();
            btnaddvehicle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    id = null;
                    SharedPreferences add = getActivity().getSharedPreferences("addvehicleback", Context.MODE_PRIVATE);
                    SharedPreferences.Editor ed = add.edit();
                    ed.putString("addvehicleback", "addvehicleback");
                    ed.commit();

                    vehicle_add pay = new vehicle_add();
                    pay.registerForListener(myvehicle.this);
                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                }
            });

            listofvehicleno.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    CheckBox check = (CheckBox) view.findViewById(R.id.chk_id);
                    for (int i = 0; i < vehiclearry.size(); i++) {
                        check.setVisibility(View.VISIBLE);
                        vehiclearry.get(i).setIsselect(true);
                        vehiclearry.get(i).setCheckboxselected(false);
                        adpater.notifyDataSetChanged();

                    }
                    return true;
                }
            });

            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, statearray);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spsate.setAdapter(spinnerArrayAdapter);
            spsate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    statename = (String) spsate.getSelectedItem();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            alertDialog.show();
        }

        statefullname = new HashMap<String, Object>();
        statefullname.put("AL", "Alabama");
        statefullname.put("AK", "Alaska");
        statefullname.put("AZ", "Arizona");
        statefullname.put("AR", "Arkansas");
        statefullname.put("CA", "California");
        statefullname.put("CO", "Colorado");
        statefullname.put("CT", "Connecticut");
        statefullname.put("DE", "Delaware");
        statefullname.put("DC", "District of Columbia");
        statefullname.put("FL", "Florida");
        statefullname.put("GA", "Georgia");
        statefullname.put("HI", "Hawaii");
        statefullname.put("ID", "Idaho");
        statefullname.put("IL", "Illinois");
        statefullname.put("IN", "Indiana");
        statefullname.put("IA", "Iowa");
        statefullname.put("KS", "Kansas");
        statefullname.put("KY", "Kentucky");
        statefullname.put("LA", "Louisiana");
        statefullname.put("ME", "Maine");
        statefullname.put("MD", "Maryland");
        statefullname.put("MA", "Massachusetts");
        statefullname.put("MI", "Michigan");
        statefullname.put("MN", "Minnesota");
        statefullname.put("MS", "Mississippi");
        statefullname.put("MO", "Missouri");
        statefullname.put("MT", "Montana");
        statefullname.put("NE", "Nebraska");
        statefullname.put("NV", "Nevada");
        statefullname.put("NH", "New Hampshire");
        statefullname.put("NJ", "New Jersey");
        statefullname.put("NM", "New Mexico");
        statefullname.put("NY", "New York");
        statefullname.put("NC", "North Carolina");
        statefullname.put("ND", "North Dakota");
        statefullname.put("OH", "Ohio");
        statefullname.put("OK", "Oklahoma");
        statefullname.put("OR", "Oregon");
        statefullname.put("PA", "Pennsylvani");
        statefullname.put("RI", "Rhode Island");
        statefullname.put("SC", "South Carolina");
        statefullname.put("SD", "South Dakota");
        statefullname.put("TN", "Tennessee");
        statefullname.put("TX", "Texas");
        statefullname.put("UT", "Utah");
        statefullname.put("VT", "Vermont");
        statefullname.put("VA", "Virginia");
        statefullname.put("WA", "Washington");
        statefullname.put("WV", "West virginia");
        statefullname.put("WI", "Wisconsin");
        statefullname.put("WY", "Wyoming");
        return view;
    }

    @Override
    public void Onitem_save() {
        getActivity().onBackPressed();
        new getvehiclenumber().execute();
    }

    @Override
    public void Onitem_delete() {
        getActivity().onBackPressed();
        new getvehiclenumber().execute();
    }

    public class deletevehicle extends AsyncTask<String, String, String> {
        String locationurl;
        String id;

        public deletevehicle(String id) {
            locationurl = "_table/user_vehicles";
            this.id = id;
        }

        String gf = "null";
        JSONObject json, json1;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        JSONArray array = null;

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", id);
            JSONObject json1 = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + locationurl;
            Log.e("delete_uel", url);

            try {
                HttpEntity entity = new StringEntity(json1.toString());
                HttpClient httpClient = new DefaultHttpClient();
                HttpDeleteWithBody httpDeleteWithBody = new HttpDeleteWithBody(url);
                httpDeleteWithBody.setHeader("X-DreamFactory-Application-Name", "parkezly");
                httpDeleteWithBody.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
                httpDeleteWithBody.setEntity(entity);

                HttpResponse response = httpClient.execute(httpDeleteWithBody);
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        gf = c.getString("id");
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json != null) {
                for (int i = 0; i < vehiclearry.size(); i++) {
                    if (vehiclearry.get(i).isCheckboxselected()) {
                        getvehicle = true;
                        delete();
                        break;
                    }
                }
                if (getvehicle != true) {
                    new getvehiclenumber().execute();
                }
            }
            super.onPostExecute(s);
        }
    }

    @NotThreadSafe
    class HttpDeleteWithBody extends HttpEntityEnclosingRequestBase {
        public static final String METHOD_NAME = "DELETE";

        public String getMethod() {
            return METHOD_NAME;
        }

        public HttpDeleteWithBody(final String uri) {
            super();
            setURI(URI.create(uri));
        }

        public HttpDeleteWithBody(final URI uri) {
            super();
            setURI(uri);
        }

        public HttpDeleteWithBody() {
            super();
        }
    }

    public class getvehiclenumber extends AsyncTask<String, String, String> {
        JSONObject json = null;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        String vehiclenourl = "_table/user_vehicles?filter=user_id%3D" + user_id + "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            vehiclearry.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("get_vehicle_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);
                    Log.e("rerpones", String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        String plate_no = c.getString("plate_no");
                        //String state = c.getString("registered_state");
                        String state = c.getString("pl_state");
                        String id = c.getString("id");

                        item.setVehicle_color(c.getString("vehicle_color"));
                        item.setVehicle_country(c.getString("vehicle_country"));
                        item.setVehicle_make(c.getString("vehicle_make"));
                        item.setVehicle_model(c.getString("vehicle_model"));
                        item.setVehicle_year(c.getString("vehicle_year"));
                        item.setState(state);
                        item.setPlateno(plate_no);
                        item.setId(id);
                        item.setIsselect(false);
                        item.setCheckboxselected(false);
                        vehiclearry.add(item);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity == null) {

                } else {
                    adpater = new CustomAdaptercity(getActivity(), vehiclearry, rs = getResources());
                    listofvehicleno.setAdapter(adpater);
                }
            }
            super.onPostExecute(s);
        }
    }

    public class CustomAdaptercity extends BaseAdapter {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdaptercity(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txtplatno, state, txtid;
            public RelativeLayout rl;
            CheckBox checkBox;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.listvehicle, null);
                holder = new ViewHolder();
                holder.txtplatno = (TextView) vi.findViewById(R.id.txtnoplatno);
                holder.state = (TextView) vi.findViewById(R.id.txtstate);
                holder.txtid = (TextView) vi.findViewById(R.id.id);
                holder.rl = (RelativeLayout) vi.findViewById(R.id.rl_m);
                holder.checkBox = (CheckBox) vi.findViewById(R.id.chk_id);
                /************  Set holder with LayoutInflater ************/
                listofvehicleno.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id1) {
                        SharedPreferences add = getActivity().getSharedPreferences("addvehicleback", Context.MODE_PRIVATE);
                        SharedPreferences.Editor ed = add.edit();
                        ed.putString("addvehicleback", "addvehicleback");
                        ed.commit();
                        txttitle.setText("My Vehicle");

                        String platno = vehiclearry.get(position).getPlantno();
                        statename = vehiclearry.get(position).getState();
                        id = vehiclearry.get(position).getId();
                        editetxtplatno.setText(platno);

                        /*Iterator myVeryOwnIterator = statefullname.keySet().iterator();
                        int k = 0;
                        while (myVeryOwnIterator.hasNext()) {
                            String key = (String) myVeryOwnIterator.next();
                            String value = (String) statefullname.get(key);
                            if (value.equals(statename)) {
                                for (int i = 0; i <= statearray.length; i++) {

                                    String state = statearray[i];
                                    if (state == key) {
                                        k = i;
                                        spsate.setSelection(i);
                                        statename = state;
                                        break;
                                    }
                                }
                            }
                        }*/
                        Bundle b = new Bundle();
                        b.putString("plate_no", platno);
                        b.putString("v_country", vehiclearry.get(position).getVehicle_country());
                        b.putString("v_state", vehiclearry.get(position).getState());
                        b.putString("v_color", vehiclearry.get(position).getVehicle_color());
                        b.putString("v_make", vehiclearry.get(position).getVehicle_make());
                        b.putString("v_model", vehiclearry.get(position).getVehicle_model());
                        b.putString("v_year", vehiclearry.get(position).getVehicle_year());
                        b.putString("id", id);
                        vehicle_add pay = new vehicle_add();
                        pay.setArguments(b);
                        pay.registerForListener(myvehicle.this);
                        ((vchome) getActivity()).show_back_button();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.add(R.id.My_Container_1_ID, pay);
                        fragmentStack.lastElement().onPause();
                        ft.hide(fragmentStack.lastElement());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                    }
                });
                vi.setTag(holder);
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
                holder.txtplatno.setText("No Data");
                holder.rl.setVisibility(View.GONE);
            } else {
                /***** Get each Model object from Arraylist ********/
                tempValues = null;
                tempValues = (item) vehiclearry.get(position);
                int k = 0;
                /************  Set Model values in Holder elements ***********/
                String cat = tempValues.getPlantno();
                if (cat == null) {
                    holder.rl.setVisibility(View.GONE);
                    holder.state.setVisibility(View.GONE);
                } else if (cat != null) {
                    holder.rl.setVisibility(View.VISIBLE);
                    Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvetica-neue-medium.ttf");
                    holder.txtplatno.setTypeface(type);
                    holder.state.setTypeface(type);
                    holder.txtplatno.setText(tempValues.getPlantno().toUpperCase());
                    holder.state.setText(tempValues.getState());
                    holder.txtid.setText(tempValues.getId());
                    if (tempValues.Getisselect() == false) {
                        holder.checkBox.setVisibility(View.GONE);
                    } else {
                        holder.checkBox.setVisibility(View.VISIBLE);
                    }
                    if (tempValues.isCheckboxselected()) {
                        holder.checkBox.setChecked(true);
                    } else {
                        holder.checkBox.setChecked(false);
                    }
                    holder.checkBox.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (vehiclearry.get(position).isCheckboxselected()) {
                                vehiclearry.get(position).setCheckboxselected(false);
                                holder.checkBox.setChecked(false);
                            } else {
                                vehiclearry.get(position).setCheckboxselected(true);
                                holder.checkBox.setChecked(true);
                                btn_delete.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
            }
            return vi;
        }
    }


    public void delete() {
        getvehicle = false;
        for (int i = 0; i < vehiclearry.size(); i++) {
            if (vehiclearry.get(i).isCheckboxselected()) {
                String id = vehiclearry.get(i).getId();
                String pno = vehiclearry.get(i).getPlantno();
                String state = vehiclearry.get(i).getState();
                vehiclearry.get(i).setCheckboxselected(false);
                String key = null;
                Iterator myVeryOwnIterator = statefullname.keySet().iterator();
                while (myVeryOwnIterator.hasNext()) {
                    key = (String) myVeryOwnIterator.next();
                    String value = (String) statefullname.get(key);
                    if (value.equals(state)) {
                        break;
                    }
                }
                new checkcardparkedornotwallet(pno, key, id).execute();
                break;
            }
        }
    }

    public class checkcardparkedornotwallet extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONArray array;
        String plno, state, pid;

        public checkcardparkedornotwallet(String plno, String state, String id) {
            this.plno = plno;
            this.state = state;
            this.pid = id;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String vechiclenourl = null;
            try {
                vechiclenourl = "_table/parked_cars?filter=(plate_no%3D" + URLEncoder.encode(plno, "utf-8") + ")%20AND%20(pl_state%3D" + state + ")%20AND%20(parking_status%3D'ENTRY')";
            } catch (Exception e) {
                e.printStackTrace();
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + vechiclenourl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (array.length() > 0) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Vehicle already parked!");
                    alertDialog.setMessage("Vehicle that you want to delete is currently parked, please exit from parking before deleting.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            delete();
                        }
                    });
                    alertDialog.show();
                } else {
                    new deletevehicle(pid).execute();
                }
            }
            super.onPostExecute(s);
        }
    }

}