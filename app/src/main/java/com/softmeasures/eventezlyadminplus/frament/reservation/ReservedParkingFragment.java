package com.softmeasures.eventezlyadminplus.frament.reservation;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.models.ReserveParking;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class ReservedParkingFragment extends BaseFragment {

    private TextView tvTitle, tvError;
    private RecyclerView rvParking;
    private RelativeLayout rlProgressbar;
    private ArrayList<ReserveParking> reserveParkings = new ArrayList<>();
    private ArrayList<ReserveParking> reserveParkingsMain = new ArrayList<>();
    private ParkingAdapter parkingAdapter;
    public static boolean isUpdated = false;
    private String locationCode = "";
    private TextView tvBtnFilterReservationStatus, tvBtnFilterPaymentStatus, tvBtnFilterActive;
    private String selectedFReservationStatus = "ACTIVE";
    private String selectedFPaymentStatus = "ALL";
    private String selectedFParkingStatus = "RESERVED";
    private boolean isMyReservations = false;
    private long currentTime = 0, reservationEntryTime = 0, reservationExitTime = 0;
    private SimpleDateFormat sdf;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            locationCode = getArguments().getString("locationCode");
            isMyReservations = getArguments().getBoolean("isMyReservations", false);
        }
        return inflater.inflate(R.layout.frag_reserved_parking, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        setListeners();
        updateViews();

        new fetchReservedParking().execute();
    }

    @Override
    public void onResume() {
        if (isUpdated) {
            new fetchReservedParking().execute();
            isUpdated = false;
        }
        super.onResume();
    }

    @Override
    protected void updateViews() {
        if (isMyReservations) {
            tvTitle.setText("My Parking Reservations");
            selectedFReservationStatus = "ALL";
            selectedFPaymentStatus = "ALL";
            selectedFParkingStatus = "ALL";
        } else tvTitle.setText("Parking Reservations");

        tvBtnFilterReservationStatus.setText(selectedFReservationStatus);
        tvBtnFilterPaymentStatus.setText(selectedFPaymentStatus);
        tvBtnFilterActive.setText(selectedFParkingStatus);
    }

    @Override
    protected void setListeners() {
        tvBtnFilterReservationStatus.setOnClickListener(v -> {
            switch (selectedFReservationStatus) {
                case "ALL":
                    selectedFReservationStatus = "ACTIVE";
                    tvBtnFilterReservationStatus.setText(selectedFReservationStatus);
                    updateFilter();
                    break;
                case "ACTIVE":
                    selectedFReservationStatus = "PARKED";
                    tvBtnFilterReservationStatus.setText(selectedFReservationStatus);
                    updateFilter();
                    break;
                case "PARKED":
                    selectedFReservationStatus = "CANCELLED";
                    tvBtnFilterReservationStatus.setText(selectedFReservationStatus);
                    updateFilter();
                    break;
                case "CANCELLED":
                    selectedFReservationStatus = "EXPIRED";
                    tvBtnFilterReservationStatus.setText(selectedFReservationStatus);
                    updateFilter();
                    break;
                case "EXPIRED":
                    selectedFReservationStatus = "ALL";
                    tvBtnFilterReservationStatus.setText(selectedFReservationStatus);
                    updateFilter();
                    break;
            }
        });

        tvBtnFilterPaymentStatus.setOnClickListener(v -> {
            switch (selectedFPaymentStatus) {
                case "ALL":
                    selectedFPaymentStatus = "PAID";
                    tvBtnFilterPaymentStatus.setText(selectedFPaymentStatus);
                    updateFilter();
                    break;
                case "PAID":
                    selectedFPaymentStatus = "UNPAID";
                    tvBtnFilterPaymentStatus.setText(selectedFPaymentStatus);
                    updateFilter();
                    break;
                case "UNPAID":
                    selectedFPaymentStatus = "ALL";
                    tvBtnFilterPaymentStatus.setText(selectedFPaymentStatus);
                    updateFilter();
                    break;
            }
        });

        tvBtnFilterActive.setOnClickListener(v -> {
            switch (selectedFParkingStatus) {
                case "ALL":
                    selectedFParkingStatus = "RESERVED";
                    tvBtnFilterActive.setText(selectedFParkingStatus);
                    updateFilter();
                    break;
                case "RESERVED":
                    selectedFParkingStatus = "ENTRY";
                    tvBtnFilterActive.setText(selectedFParkingStatus);
                    updateFilter();
                    break;
                case "ENTRY":
                    selectedFParkingStatus = "EXIT";
                    tvBtnFilterActive.setText(selectedFParkingStatus);
                    updateFilter();
                    break;
                case "EXIT":
                    selectedFParkingStatus = "CANCELLED";
                    tvBtnFilterActive.setText(selectedFParkingStatus);
                    updateFilter();
                    break;
                case "CANCELLED":
                    selectedFParkingStatus = "ALL";
                    tvBtnFilterActive.setText(selectedFParkingStatus);
                    updateFilter();
                    break;
            }
        });
    }

    private void updateFilter() {

        Log.e("#DEBUG", "  Filter:  selectedFReservationStatus: " + selectedFReservationStatus
                + "\n   selectedFPaymentStatus: " + selectedFPaymentStatus
                + "\n   selectedFParkingStatus: " + selectedFParkingStatus);

        if (selectedFReservationStatus.equals("ALL")
                && selectedFParkingStatus.equals("ALL")
                && selectedFPaymentStatus.equals("ALL")) {
            reserveParkings.clear();
            parkingAdapter.notifyDataSetChanged();
            reserveParkings.addAll(reserveParkingsMain);
            if (parkingAdapter != null) {
                Log.e("#DEBUG", "   Filter:  reserveParking  Size: " + reserveParkings.size());
                if (reserveParkings.size() == 0) {
                    tvError.setVisibility(View.VISIBLE);
                } else tvError.setVisibility(View.GONE);
                parkingAdapter.notifyDataSetChanged();
            }
        } else {
            reserveParkings.clear();
            parkingAdapter.notifyDataSetChanged();
            for (int i = 0; i < reserveParkingsMain.size(); i++) {
                if (!selectedFReservationStatus.equals("ALL")
                        && !selectedFParkingStatus.equals("ALL")
                        && !selectedFPaymentStatus.equals("ALL")) {
                    if (reserveParkingsMain.get(i).getReserve_payment_paid().equals(selectedFPaymentStatus)
                            && reserveParkingsMain.get(i).getReservation_status().equals(selectedFReservationStatus)
                            && reserveParkingsMain.get(i).getParking_status().equals(selectedFParkingStatus)) {
                        reserveParkings.add(reserveParkingsMain.get(i));
                    }
                } else if (selectedFReservationStatus.equals("ALL")
                        && !selectedFParkingStatus.equals("ALL")
                        && !selectedFPaymentStatus.equals("ALL")) {
                    if (reserveParkingsMain.get(i).getReserve_payment_paid().equals(selectedFPaymentStatus)
                            && reserveParkingsMain.get(i).getParking_status().equals(selectedFParkingStatus)) {
                        reserveParkings.add(reserveParkingsMain.get(i));
                    }
                } else if (!selectedFReservationStatus.equals("ALL")
                        && selectedFParkingStatus.equals("ALL")
                        && !selectedFPaymentStatus.equals("ALL")) {
                    if (reserveParkingsMain.get(i).getReserve_payment_paid().equals(selectedFPaymentStatus)
                            && reserveParkingsMain.get(i).getReservation_status().equals(selectedFReservationStatus)) {
                        reserveParkings.add(reserveParkingsMain.get(i));
                    }
                } else if (!selectedFReservationStatus.equals("ALL")
                        && !selectedFParkingStatus.equals("ALL")
                        && selectedFPaymentStatus.equals("ALL")) {
                    if (reserveParkingsMain.get(i).getReservation_status().equals(selectedFReservationStatus)
                            && reserveParkingsMain.get(i).getParking_status().equals(selectedFParkingStatus)) {
                        reserveParkings.add(reserveParkingsMain.get(i));
                    }
                } else if (selectedFReservationStatus.equals("ALL")
                        && selectedFParkingStatus.equals("ALL")
                        && !selectedFPaymentStatus.equals("ALL")) {
                    if (reserveParkingsMain.get(i).getReserve_payment_paid().equals(selectedFPaymentStatus)) {
                        reserveParkings.add(reserveParkingsMain.get(i));
                    }

                } else if (selectedFReservationStatus.equals("ALL")
                        && !selectedFParkingStatus.equals("ALL")
                        && selectedFPaymentStatus.equals("ALL")) {
                    if (reserveParkingsMain.get(i).getParking_status().equals(selectedFParkingStatus)) {
                        reserveParkings.add(reserveParkingsMain.get(i));
                    }

                } else if (!selectedFReservationStatus.equals("ALL")
                        && selectedFParkingStatus.equals("ALL")
                        && selectedFPaymentStatus.equals("ALL")) {
                    if (reserveParkingsMain.get(i).getReservation_status().equals(selectedFReservationStatus)) {
                        reserveParkings.add(reserveParkingsMain.get(i));
                    }
                }
//                if (reserveParkingsMain.get(i).getReserve_payment_paid().equals(selectedFPaymentStatus)) {
//                    Log.e("#DEBUG", "   Filter  equals  ");
//                    reserveParkings.add(reserveParkingsMain.get(i));
//                } else if (reserveParkingsMain.get(i).getReservation_status().equals(selectedFReservationStatus)) {
//                    reserveParkings.add(reserveParkingsMain.get(i));
//                } else if (reserveParkingsMain.get(i).getParking_status().equals(selectedFParkingStatus)) {
//                    reserveParkings.add(reserveParkingsMain.get(i));
//                }
            }
            if (parkingAdapter != null) {
                Log.e("#DEBUG", "   Filter:  reserveParking  Size: " + reserveParkings.size());
                if (reserveParkings.size() == 0) {
                    tvError.setVisibility(View.VISIBLE);
                } else tvError.setVisibility(View.GONE);
                parkingAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void initViews(View v) {
        rlProgressbar = v.findViewById(R.id.rlProgressbar);
        tvTitle = v.findViewById(R.id.tvTitle);
        rvParking = v.findViewById(R.id.rvParking);
        tvError = v.findViewById(R.id.tvError);

        tvBtnFilterReservationStatus = v.findViewById(R.id.tvBtnFilterReservationStatus);
        tvBtnFilterPaymentStatus = v.findViewById(R.id.tvBtnFilterPaymentStatus);
        tvBtnFilterActive = v.findViewById(R.id.tvBtnFilterActive);

        parkingAdapter = new ParkingAdapter();
        rvParking.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvParking.setAdapter(parkingAdapter);

        Calendar calendar = Calendar.getInstance();
        currentTime = calendar.getTimeInMillis();
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

    }

    private class fetchReservedParking extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        String tcode = logindeatl.getString("twcode", "FDV");
        String role = logindeatl.getString("role", "");
        JSONObject json;
        JSONArray json1;
        String reservedParkingUrl = "_table/reserved_parkings?filter=user_id%3D" + id + "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rlProgressbar.setVisibility(View.VISIBLE);

            if (!TextUtils.isEmpty(role) && role.equals("SuperAdmin")) {
                reservedParkingUrl = "_table/reserved_parkings?filter=location_code=" + locationCode;
            } else if (!TextUtils.isEmpty(role) && role.equals("TwpAdmin")) {
                reservedParkingUrl = "_table/reserved_parkings?filter=(township_code%3D" + tcode + ")%20AND%20(location_code%3D" + locationCode + ")";
            } else {
                reservedParkingUrl = "_table/reserved_parkings?filter=(user_id%3D" + id + ")%20AND%20(location_code%3D" + locationCode + ")";
            }

            reserveParkings.clear();
            reserveParkingsMain.clear();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + reservedParkingUrl;
            Log.e("#DEBUG", "      fetchReserveParking:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");

                    for (int j = 0; j < jsonArray.length(); j++) {
                        ReserveParking parkingUpdate = new ReserveParking();
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            parkingUpdate.setId(object.getString("id"));
                        }
                        if (object.has("company_type_id")
                                && !TextUtils.isEmpty(object.getString("company_type_id"))
                                && !object.getString("company_type_id").equals("null")) {
                            parkingUpdate.setCompany_type_id(object.getString("company_type_id"));
                        }
                        if (object.has("company_type")
                                && !TextUtils.isEmpty(object.getString("company_type"))
                                && !object.getString("company_type").equals("null")) {
                            parkingUpdate.setCompany_type(object.getString("company_type"));
                        }
                        if (object.has("manager_type_id")
                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
                                && !object.getString("manager_type_id").equals("null")) {
                            parkingUpdate.setManager_type_id(object.getString("manager_type_id"));
                        }
                        if (object.has("manager_type")
                                && !TextUtils.isEmpty(object.getString("manager_type"))
                                && !object.getString("manager_type").equals("null")) {
                            parkingUpdate.setManager_type(object.getString("manager_type"));
                        }
                        if (object.has("company_id")
                                && !TextUtils.isEmpty(object.getString("company_id"))
                                && !object.getString("company_id").equals("null")) {
                            parkingUpdate.setCompany_id(object.getString("company_id"));
                        }
                        if (object.has("parking_type")
                                && !TextUtils.isEmpty(object.getString("parking_type"))
                                && !object.getString("parking_type").equals("null")) {
                            parkingUpdate.setParking_type(object.getString("parking_type"));
                        }
                        if (object.has("twp_id")
                                && !TextUtils.isEmpty(object.getString("twp_id"))
                                && !object.getString("twp_id").equals("null")) {
                            parkingUpdate.setTwp_id(object.getString("twp_id"));
                        }
                        if (object.has("township_code")
                                && !TextUtils.isEmpty(object.getString("township_code"))
                                && !object.getString("township_code").equals("null")) {
                            parkingUpdate.setTownship_code(object.getString("township_code"));
                        }
                        if (object.has("location_id")
                                && !TextUtils.isEmpty(object.getString("location_id"))
                                && !object.getString("location_id").equals("null")) {
                            parkingUpdate.setLocation_id(object.getString("location_id"));
                        }
                        if (object.has("location_code")
                                && !TextUtils.isEmpty(object.getString("location_code"))
                                && !object.getString("location_code").equals("null")) {
                            parkingUpdate.setLocation_code(object.getString("location_code"));
                        }
                        if (object.has("location_name")
                                && !TextUtils.isEmpty(object.getString("location_name"))
                                && !object.getString("location_name").equals("null")) {
                            parkingUpdate.setLocation_name(object.getString("location_name"));
                        }
                        if (object.has("lot_row")
                                && !TextUtils.isEmpty(object.getString("lot_row"))
                                && !object.getString("lot_row").equals("null")) {
                            parkingUpdate.setLot_row(object.getString("lot_row"));
                        }
                        if (object.has("lot_number")
                                && !TextUtils.isEmpty(object.getString("lot_number"))
                                && !object.getString("lot_number").equals("null")) {
                            parkingUpdate.setLot_number(object.getString("lot_number"));
                        }
                        if (object.has("lot_id")
                                && !TextUtils.isEmpty(object.getString("lot_id"))
                                && !object.getString("lot_id").equals("null")) {
                            parkingUpdate.setLot_id(object.getString("lot_id"));
                        }
                        if (object.has("reservation_date_time")
                                && !TextUtils.isEmpty(object.getString("reservation_date_time"))
                                && !object.getString("reservation_date_time").equals("null")) {
                            parkingUpdate.setReservation_date_time(object.getString("reservation_date_time"));
                        }
                        if (object.has("reserve_entry_time")
                                && !TextUtils.isEmpty(object.getString("reserve_entry_time"))
                                && !object.getString("reserve_entry_time").equals("null")) {
                            parkingUpdate.setReserve_entry_time(object.getString("reserve_entry_time"));
                            try {
                                parkingUpdate.setReservationEntryTime(sdf.parse(
                                        object.getString("reserve_entry_time")).getTime());
                                reservationEntryTime = sdf.parse(
                                        object.getString("reserve_entry_time")).getTime();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        if (object.has("reserve_expiry_time")
                                && !TextUtils.isEmpty(object.getString("reserve_expiry_time"))
                                && !object.getString("reserve_expiry_time").equals("null")) {
                            parkingUpdate.setReserve_expiry_time(object.getString("reserve_expiry_time"));
                            try {
                                parkingUpdate.setReservationExitTime(sdf.parse(
                                        object.getString("reserve_expiry_time")).getTime());
                                reservationExitTime = sdf.parse(
                                        object.getString("reserve_expiry_time")).getTime();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        if (object.has("reserve_exit_date_time")
                                && !TextUtils.isEmpty(object.getString("reserve_exit_date_time"))
                                && !object.getString("reserve_exit_date_time").equals("null")) {
                            parkingUpdate.setReserve_exit_date_time(object.getString("reserve_exit_date_time"));
                        }
                        if (object.has("reserve_payment_condition")
                                && !TextUtils.isEmpty(object.getString("reserve_payment_condition"))
                                && !object.getString("reserve_payment_condition").equals("null")) {
                            parkingUpdate.setReserve_payment_condition(object.getString("reserve_payment_condition"));
                        }
                        if (object.has("reserve_payment_due_by")
                                && !TextUtils.isEmpty(object.getString("reserve_payment_due_by"))
                                && !object.getString("reserve_payment_due_by").equals("null")) {
                            parkingUpdate.setReserve_payment_due_by(object.getString("reserve_payment_due_by"));
                        }
                        if (object.has("reserve_payment_paid")
                                && !TextUtils.isEmpty(object.getString("reserve_payment_paid"))
                                && !object.getString("reserve_payment_paid").equals("null")) {
                            parkingUpdate.setReserve_payment_paid(object.getString("reserve_payment_paid"));
                        }
                        if (object.has("reservation_status")
                                && !TextUtils.isEmpty(object.getString("reservation_status"))
                                && !object.getString("reservation_status").equals("null")) {
                            parkingUpdate.setReservation_status(object.getString("reservation_status"));
                            if (object.getString("reservation_status").equals("ACTIVE")
                                    && currentTime > reservationExitTime) {
                                parkingUpdate.setReservation_status("EXPIRED");
                            }
                        }
                        if (object.has("entry_date_time")
                                && !TextUtils.isEmpty(object.getString("entry_date_time"))
                                && !object.getString("entry_date_time").equals("null")) {
                            parkingUpdate.setEntry_date_time(object.getString("entry_date_time"));
                        }
                        if (object.has("exit_date_time")
                                && !TextUtils.isEmpty(object.getString("exit_date_time"))
                                && !object.getString("exit_date_time").equals("null")) {
                            parkingUpdate.setExit_date_time(object.getString("exit_date_time"));
                        }
                        if (object.has("expiry_time")
                                && !TextUtils.isEmpty(object.getString("expiry_time"))
                                && !object.getString("expiry_time").equals("null")) {
                            parkingUpdate.setExpiry_time(object.getString("expiry_time"));
                        }
                        if (object.has("user_id")
                                && !TextUtils.isEmpty(object.getString("user_id"))
                                && !object.getString("user_id").equals("null")) {
                            parkingUpdate.setUser_id(object.getString("user_id"));
                        }
                        if (object.has("permit_id")
                                && !TextUtils.isEmpty(object.getString("permit_id"))
                                && !object.getString("permit_id").equals("null")) {
                            parkingUpdate.setPermit_id(object.getString("permit_id"));
                        }
                        if (object.has("subscription_id")
                                && !TextUtils.isEmpty(object.getString("subscription_id"))
                                && !object.getString("subscription_id").equals("null")) {
                            parkingUpdate.setSubscription_id(object.getString("subscription_id"));
                        }
                        if (object.has("plate_no")
                                && !TextUtils.isEmpty(object.getString("plate_no"))
                                && !object.getString("plate_no").equals("null")) {
                            parkingUpdate.setPlate_no(object.getString("plate_no"));
                        }
                        if (object.has("pl_state")
                                && !TextUtils.isEmpty(object.getString("pl_state"))
                                && !object.getString("pl_state").equals("null")) {
                            parkingUpdate.setPl_state(object.getString("pl_state"));
                        }
                        if (object.has("pl_country")
                                && !TextUtils.isEmpty(object.getString("pl_country"))
                                && !object.getString("pl_country").equals("null")) {
                            parkingUpdate.setPl_country(object.getString("pl_country"));
                        }
                        if (object.has("lat")
                                && !TextUtils.isEmpty(object.getString("lat"))
                                && !object.getString("lat").equals("null")) {
                            parkingUpdate.setLat(object.getString("lat"));
                        }
                        if (object.has("lng")
                                && !TextUtils.isEmpty(object.getString("lng"))
                                && !object.getString("lng").equals("null")) {
                            parkingUpdate.setLng(object.getString("lng"));
                        }
                        if (object.has("address1")
                                && !TextUtils.isEmpty(object.getString("address1"))
                                && !object.getString("address1").equals("null")) {
                            parkingUpdate.setAddress1(object.getString("address1"));
                        }
                        if (object.has("address2")
                                && !TextUtils.isEmpty(object.getString("address2"))
                                && !object.getString("address2").equals("null")) {
                            parkingUpdate.setAddress2(object.getString("address2"));
                        }
                        if (object.has("city")
                                && !TextUtils.isEmpty(object.getString("city"))
                                && !object.getString("city").equals("null")) {
                            parkingUpdate.setCity(object.getString("city"));
                        }
                        if (object.has("district")
                                && !TextUtils.isEmpty(object.getString("district"))
                                && !object.getString("district").equals("null")) {
                            parkingUpdate.setDistrict(object.getString("district"));
                        }
                        if (object.has("state")
                                && !TextUtils.isEmpty(object.getString("state"))
                                && !object.getString("state").equals("null")) {
                            parkingUpdate.setState(object.getString("state"));
                        }
                        if (object.has("zip")
                                && !TextUtils.isEmpty(object.getString("zip"))
                                && !object.getString("zip").equals("null")) {
                            parkingUpdate.setZip(object.getString("zip"));
                        }
                        if (object.has("country")
                                && !TextUtils.isEmpty(object.getString("country"))
                                && !object.getString("country").equals("null")) {
                            parkingUpdate.setCountry(object.getString("country"));
                        }
                        if (object.has("marker_lng")
                                && !TextUtils.isEmpty(object.getString("marker_lng"))
                                && !object.getString("marker_lng").equals("null")) {
                            parkingUpdate.setMarker_lng(object.getString("marker_lng"));
                        }
                        if (object.has("marker_lat")
                                && !TextUtils.isEmpty(object.getString("marker_lat"))
                                && !object.getString("marker_lat").equals("null")) {
                            parkingUpdate.setMarker_lat(object.getString("marker_lat"));
                        }
                        if (object.has("marker_address1")
                                && !TextUtils.isEmpty(object.getString("marker_address1"))
                                && !object.getString("marker_address1").equals("null")) {
                            parkingUpdate.setMarker_address1(object.getString("marker_address1"));
                        }
                        if (object.has("marker_address2")
                                && !TextUtils.isEmpty(object.getString("marker_address2"))
                                && !object.getString("marker_address2").equals("null")) {
                            parkingUpdate.setMarker_address2(object.getString("marker_address2"));
                        }
                        if (object.has("marker_city")
                                && !TextUtils.isEmpty(object.getString("marker_city"))
                                && !object.getString("marker_city").equals("null")) {
                            parkingUpdate.setMarker_city(object.getString("marker_city"));
                        }
                        if (object.has("marker_district")
                                && !TextUtils.isEmpty(object.getString("marker_district"))
                                && !object.getString("marker_district").equals("null")) {
                            parkingUpdate.setMarker_district(object.getString("marker_district"));
                        }
                        if (object.has("marker_state")
                                && !TextUtils.isEmpty(object.getString("marker_state"))
                                && !object.getString("marker_state").equals("null")) {
                            parkingUpdate.setMarker_state(object.getString("marker_state"));
                        }
                        if (object.has("marker_zip")
                                && !TextUtils.isEmpty(object.getString("marker_zip"))
                                && !object.getString("marker_zip").equals("null")) {
                            parkingUpdate.setMarker_zip(object.getString("marker_zip"));
                        }
                        if (object.has("marker_country")
                                && !TextUtils.isEmpty(object.getString("marker_country"))
                                && !object.getString("marker_country").equals("null")) {
                            parkingUpdate.setMarker_country(object.getString("marker_country"));
                        }
                        if (object.has("distance_to_marker")
                                && !TextUtils.isEmpty(object.getString("distance_to_marker"))
                                && !object.getString("distance_to_marker").equals("null")) {
                            parkingUpdate.setDistance_to_marker(object.getString("distance_to_marker"));
                        }
                        if (object.has("ip")
                                && !TextUtils.isEmpty(object.getString("ip"))
                                && !object.getString("ip").equals("null")) {
                            parkingUpdate.setIp(object.getString("ip"));
                        }
                        if (object.has("parking_token")
                                && !TextUtils.isEmpty(object.getString("parking_token"))
                                && !object.getString("parking_token").equals("null")) {
                            parkingUpdate.setParking_token(object.getString("parking_token"));
                        }
                        if (object.has("parking_status")
                                && !TextUtils.isEmpty(object.getString("parking_status"))
                                && !object.getString("parking_status").equals("null")) {
                            parkingUpdate.setParking_status(object.getString("parking_status"));
                        }
                        if (object.has("payment_method")
                                && !TextUtils.isEmpty(object.getString("payment_method"))
                                && !object.getString("payment_method").equals("null")) {
                            parkingUpdate.setPayment_method(object.getString("payment_method"));
                        }
                        if (object.has("parking_rate")
                                && !TextUtils.isEmpty(object.getString("parking_rate"))
                                && !object.getString("parking_rate").equals("null")) {
                            parkingUpdate.setParking_rate(object.getString("parking_rate"));
                        }
                        if (object.has("parking_units")
                                && !TextUtils.isEmpty(object.getString("parking_units"))
                                && !object.getString("parking_units").equals("null")) {
                            parkingUpdate.setParking_units(object.getString("parking_units"));
                        }
                        if (object.has("parking_qty")
                                && !TextUtils.isEmpty(object.getString("parking_qty"))
                                && !object.getString("parking_qty").equals("null")) {
                            parkingUpdate.setParking_qty(object.getString("parking_qty"));
                        }
                        if (object.has("parking_subtotal")
                                && !TextUtils.isEmpty(object.getString("parking_subtotal"))
                                && !object.getString("parking_subtotal").equals("null")) {
                            parkingUpdate.setParking_subtotal(object.getString("parking_subtotal"));
                        }
                        if (object.has("wallet_trx_id")
                                && !TextUtils.isEmpty(object.getString("wallet_trx_id"))
                                && !object.getString("wallet_trx_id").equals("null")) {
                            parkingUpdate.setWallet_trx_id(object.getString("wallet_trx_id"));
                        }
                        if (object.has("tr_percent")
                                && !TextUtils.isEmpty(object.getString("tr_percent"))
                                && !object.getString("tr_percent").equals("null")) {
                            parkingUpdate.setTr_percent(object.getString("tr_percent"));
                        }
                        if (object.has("tr_fee")
                                && !TextUtils.isEmpty(object.getString("tr_fee"))
                                && !object.getString("tr_fee").equals("null")) {
                            parkingUpdate.setTr_fee(object.getString("tr_fee"));
                        }
                        if (object.has("parking_total")
                                && !TextUtils.isEmpty(object.getString("parking_total"))
                                && !object.getString("parking_total").equals("null")) {
                            parkingUpdate.setParking_total(object.getString("parking_total"));
                        }
                        if (object.has("ipn_custom")
                                && !TextUtils.isEmpty(object.getString("ipn_custom"))
                                && !object.getString("ipn_custom").equals("null")) {
                            parkingUpdate.setIpn_custom(object.getString("ipn_custom"));
                        }
                        if (object.has("ipn_txn_id")
                                && !TextUtils.isEmpty(object.getString("ipn_txn_id"))
                                && !object.getString("ipn_txn_id").equals("null")) {
                            parkingUpdate.setIpn_txn_id(object.getString("ipn_txn_id"));
                        }
                        if (object.has("ipn_payment")
                                && !TextUtils.isEmpty(object.getString("ipn_payment"))
                                && !object.getString("ipn_payment").equals("null")) {
                            parkingUpdate.setIpn_payment(object.getString("ipn_payment"));
                        }
                        if (object.has("ipn_status")
                                && !TextUtils.isEmpty(object.getString("ipn_status"))
                                && !object.getString("ipn_status").equals("null")) {
                            parkingUpdate.setIpn_status(object.getString("ipn_status"));
                        }
                        if (object.has("ipn_address")
                                && !TextUtils.isEmpty(object.getString("ipn_address"))
                                && !object.getString("ipn_address").equals("null")) {
                            parkingUpdate.setIpn_address(object.getString("ipn_address"));
                        }
                        if (object.has("ticket_status")
                                && !TextUtils.isEmpty(object.getString("ticket_status"))
                                && !object.getString("ticket_status").equals("null")) {
                            parkingUpdate.setTicket_status(object.getString("ticket_status"));
                        }
                        if (object.has("expiry_status")
                                && !TextUtils.isEmpty(object.getString("expiry_status"))
                                && !object.getString("expiry_status").equals("null")) {
                            parkingUpdate.setExpiry_status(object.getString("expiry_status"));
                        }
                        if (object.has("notified_status")
                                && !TextUtils.isEmpty(object.getString("notified_status"))
                                && !object.getString("notified_status").equals("null")) {
                            parkingUpdate.setNotified_status(object.getString("notified_status"));
                        }
                        if (object.has("user_id_ref")
                                && !TextUtils.isEmpty(object.getString("user_id_ref"))
                                && !object.getString("user_id_ref").equals("null")) {
                            parkingUpdate.setUser_id_ref(object.getString("user_id_ref"));
                        }
                        if (object.has("mismatch")
                                && !TextUtils.isEmpty(object.getString("mismatch"))
                                && !object.getString("mismatch").equals("null")) {
                            parkingUpdate.setMismatch(object.getString("mismatch"));
                        }
                        if (object.has("payment_choice")
                                && !TextUtils.isEmpty(object.getString("payment_choice"))
                                && !object.getString("payment_choice").equals("null")) {
                            parkingUpdate.setPayment_choice(object.getString("payment_choice"));
                        }
                        if (object.has("bursar_trx_id")
                                && !TextUtils.isEmpty(object.getString("bursar_trx_id"))
                                && !object.getString("bursar_trx_id").equals("null")) {
                            parkingUpdate.setBursar_trx_id(object.getString("bursar_trx_id"));
                        }
                        if (object.has("platform")
                                && !TextUtils.isEmpty(object.getString("platform"))
                                && !object.getString("platform").equals("null")) {
                            parkingUpdate.setPlatform(object.getString("platform"));
                        }
                        if (object.has("duration_unit")
                                && !TextUtils.isEmpty(object.getString("duration_unit"))
                                && !object.getString("duration_unit").equals("null")) {
                            parkingUpdate.setDuration_unit(object.getString("duration_unit"));
                        }
                        if (object.has("max_duration")
                                && !TextUtils.isEmpty(object.getString("max_duration"))
                                && !object.getString("max_duration").equals("null")) {
                            parkingUpdate.setMax_duration(object.getString("max_duration"));
                        }
                        if (object.has("selected_duration")
                                && !TextUtils.isEmpty(object.getString("selected_duration"))
                                && !object.getString("selected_duration").equals("null")) {
                            parkingUpdate.setSelected_duration(object.getString("selected_duration"));
                        }
                        if (object.has("user_id_exit")
                                && !TextUtils.isEmpty(object.getString("user_id_exit"))
                                && !object.getString("user_id_exit").equals("null")) {
                            parkingUpdate.setUser_id_exit(object.getString("user_id_exit"));
                        }
                        if (object.has("exit_forced")
                                && !TextUtils.isEmpty(object.getString("exit_forced"))
                                && !object.getString("exit_forced").equals("null")) {
                            parkingUpdate.setExit_forced(object.getString("exit_forced"));
                        }
                        if (object.has("towed")
                                && !TextUtils.isEmpty(object.getString("towed"))
                                && !object.getString("towed").equals("null")) {
                            parkingUpdate.setTowed(object.getString("towed"));
                        }
                        if (object.has("exit_overnight")
                                && !TextUtils.isEmpty(object.getString("exit_overnight"))
                                && !object.getString("exit_overnight").equals("null")) {
                            parkingUpdate.setExit_overnight(object.getString("exit_overnight"));
                        }
                        if (object.has("modified_time")
                                && !TextUtils.isEmpty(object.getString("modified_time"))
                                && !object.getString("modified_time").equals("null")) {
                            parkingUpdate.setModified_time(object.getString("modified_time"));
                        }

                        if (object.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(object.getString("CanCancel_Reservation"))
                                && !object.getString("CanCancel_Reservation").equals("null")) {
                            parkingUpdate.setCanCancel_Reservation(object.getBoolean("CanCancel_Reservation"));
                        }

                        if (object.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(object.getString("Reservation_PostPayment_term"))
                                && !object.getString("Reservation_PostPayment_term").equals("null")) {
                            parkingUpdate.setReservation_PostPayment_term(object.getString("Reservation_PostPayment_term"));
                        }

                        if (object.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(object.getString("Cancellation_Charge"))
                                && !object.getString("Cancellation_Charge").equals("null")) {
                            parkingUpdate.setCancellation_Charge(object.getString("Cancellation_Charge"));
                        }

                        if (object.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(object.getString("Reservation_PostPayment_LateFee"))
                                && !object.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            parkingUpdate.setReservation_PostPayment_LateFee(object.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (object.has("location_lot_id")
                                && !TextUtils.isEmpty(object.getString("location_lot_id"))
                                && !object.getString("location_lot_id").equals("null")) {
                            parkingUpdate.setLocation_lot_id(object.getString("location_lot_id"));
                        }

                        if (!TextUtils.isEmpty(parkingUpdate.getReservation_status())) {
                            if (!isMyReservations) {
                                if (!parkingUpdate.getReservation_status().equals("PARKED")) {
                                    reserveParkingsMain.add(parkingUpdate);
                                }
                            } else {
                                reserveParkingsMain.add(parkingUpdate);
                            }
                        } else {
                            reserveParkingsMain.add(parkingUpdate);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            rlProgressbar.setVisibility(View.GONE);
            if (reserveParkingsMain.size() != 0) {
//                reserveParkings.clear();
//                reserveParkings.addAll(reserveParkingsMain);
//                parkingAdapter.notifyDataSetChanged();
                updateFilter();
                Log.e("#DEBUG", "   onResponse:  reserveParkingsMain Size: " + reserveParkingsMain.size());
                tvError.setVisibility(View.GONE);
            } else {
                tvError.setVisibility(View.VISIBLE);
                parkingAdapter.notifyDataSetChanged();
            }
        }
    }

    private class ParkingAdapter extends RecyclerView.Adapter<ParkingAdapter.ParkingHolder> {
        private SimpleDateFormat dateFormat = new SimpleDateFormat("dd MM yyyy, hh:mm", Locale.getDefault());

        @NonNull
        @Override
        public ParkingAdapter.ParkingHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new ParkingHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_reserved_parking, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ParkingAdapter.ParkingHolder parkingHolder, int i) {
            ReserveParking reserveParking = reserveParkings.get(i);
            if (reserveParking != null) {
                if (!TextUtils.isEmpty(reserveParking.getId())) {
                    parkingHolder.tvReservationIdPlatNo.setText(reserveParking.getId());
                    if (!TextUtils.isEmpty(reserveParking.getPlate_no())
                            && !TextUtils.isEmpty(reserveParking.getPl_state())) {
                        parkingHolder.tvReservationIdPlatNo.setText(String.format("%s",
                                reserveParking.getId()));
                    } else {
                        parkingHolder.tvReservationIdPlatNo.setText("");
                    }
                } else {
                    parkingHolder.tvReservationIdPlatNo.setText("");
                }

                if (!TextUtils.isEmpty(reserveParking.getPlate_no())) {
                    parkingHolder.tvPlatNo1.setText(String.format("%s (%s)",
                            reserveParking.getPlate_no(), reserveParking.getPl_state()));
                } else {
                    parkingHolder.tvPlatNo1.setText(" - ");
                }
                if (!TextUtils.isEmpty(reserveParking.getReservation_date_time())) {
                    parkingHolder.tvReservationTime.setText(DateTimeUtils.parseDateTimeNew(reserveParking.getReservation_date_time()));
                } else {
                    parkingHolder.tvReservationTime.setText("");
                }
                if (!TextUtils.isEmpty(reserveParking.getReserve_entry_time())) {
                    parkingHolder.tvReservationEntryTime.setText(DateTimeUtils.parseDateTimeNew(reserveParking.getReserve_entry_time()));
                    parkingHolder.tvReservationEntryTime1.setText(DateTimeUtils.parseDateTimeNew(reserveParking.getReserve_entry_time()));
                } else {
                    parkingHolder.tvReservationEntryTime.setText("");
                    parkingHolder.tvReservationEntryTime1.setText("");
                }
                if (!TextUtils.isEmpty(reserveParking.getReserve_exit_date_time())) {
                    parkingHolder.tvReservationExitTime.setText(DateTimeUtils.parseDateTimeNew(reserveParking.getReserve_exit_date_time()));
                    parkingHolder.tvReservationExitTime1.setText(DateTimeUtils.parseDateTimeNew(reserveParking.getReserve_exit_date_time()));
                } else {
                    parkingHolder.tvReservationExitTime.setText("");
                    parkingHolder.tvReservationExitTime1.setText("");
                }
                if (!TextUtils.isEmpty(reserveParking.getReserve_payment_paid())) {
                    parkingHolder.tvPaymentStatus.setText(reserveParking.getReserve_payment_paid());
                    parkingHolder.tvPaymentStatus1.setText(reserveParking.getReserve_payment_paid());
                    if (reserveParking.getReserve_payment_paid().equals("UNPAID")) {
                        if (!TextUtils.isEmpty(reserveParking.getParking_total()))
                            parkingHolder.tvAmountDue.setText(String.format("$%s", reserveParking.getParking_total()));
                        parkingHolder.tvAmountPaid.setText("$0");
                    } else {
                        if (!TextUtils.isEmpty(reserveParking.getParking_total()))
                            parkingHolder.tvAmountPaid.setText(String.format("$%s", reserveParking.getParking_total()));
                        parkingHolder.tvAmountDue.setText("$0");
                    }
                } else {
                    parkingHolder.tvPaymentStatus.setText("");
                    parkingHolder.tvPaymentStatus1.setText("");
                }
                if (!TextUtils.isEmpty(reserveParking.getReservation_status())) {
                    parkingHolder.tvReservationStatus.setText(reserveParking.getReservation_status());
                    parkingHolder.tvReservationStatus1.setText(reserveParking.getReservation_status());
                } else {
                    parkingHolder.tvReservationStatus.setText("");
                    parkingHolder.tvReservationStatus1.setText("");
                }
                if (!TextUtils.isEmpty(reserveParking.getMarker_address1())) {
                    parkingHolder.tvAddress.setText(reserveParking.getMarker_address1());
                } else {
                    parkingHolder.tvAddress.setText("");
                }
                if (!TextUtils.isEmpty(reserveParking.getCity())) {
                    parkingHolder.tvLocationName.setText(reserveParking.getCity().replace("null", ""));
                } else {
                    parkingHolder.tvLocationName.setText("");
                }
                if (!TextUtils.isEmpty(reserveParking.getParking_status())) {
                    parkingHolder.tvParkingStatus.setText(reserveParking.getParking_status());
                    parkingHolder.tvParkingStatus1.setText(reserveParking.getParking_status());
                } else {
                    parkingHolder.tvParkingStatus.setText("");
                    parkingHolder.tvParkingStatus1.setText("");
                }

                if (!TextUtils.isEmpty(reserveParking.getReserve_payment_due_by())) {
                    parkingHolder.tvPaymentDueBy.setText(DateTimeUtils.parseDateTimeNew(reserveParking.getReserve_payment_due_by()));
                } else {
                    parkingHolder.tvPaymentDueBy.setText("");
                }

                if (!TextUtils.isEmpty(reserveParking.getPlate_no())
                        && !TextUtils.isEmpty(reserveParking.getPl_state())) {
                    parkingHolder.tvPlatNo.setText(String.format("%s(%s)",
                            reserveParking.getPlate_no(), reserveParking.getPl_state()));
                } else {
                    parkingHolder.tvPlatNo.setText("Select Vehicle");
                }

                if (!TextUtils.isEmpty(reserveParking.getLocation_name())) {
                    parkingHolder.tvLocationName1.setText(reserveParking.getLocation_name());
                } else parkingHolder.tvLocationName1.setText("");

                if (!TextUtils.isEmpty(reserveParking.getLot_id())) {
                    parkingHolder.tvLotSpace1.setText(reserveParking.getLot_id());
                } else parkingHolder.tvLotSpace1.setText("");

            }

        }

        @Override
        public int getItemCount() {
            return reserveParkings.size();
        }

        class ParkingHolder extends RecyclerView.ViewHolder {
            private TextView tvReservationTime, tvReservationEntryTime, tvReservationExitTime,
                    tvPaymentStatus, tvReservationStatus, tvAddress, tvParkingStatus,
                    tvPaymentDueBy, tvPlatNo, tvAmountDue, tvAmountPaid, tvLocationName;
            private TextView tvReservationEntryTime1, tvReservationExitTime1, tvReservationIdPlatNo,
                    tvPaymentStatus1, tvReservationStatus1, tvParkingStatus1, tvLotSpace1,
                    tvLocationName1, tvPlatNo1;

            ParkingHolder(@NonNull View itemView) {
                super(itemView);
                tvReservationEntryTime1 = itemView.findViewById(R.id.tvReservationEntryTime1);
                tvReservationExitTime1 = itemView.findViewById(R.id.tvReservationExitTime1);
                tvReservationIdPlatNo = itemView.findViewById(R.id.tvReservationIdPlatNo);
                tvPaymentStatus1 = itemView.findViewById(R.id.tvPaymentStatus1);
                tvParkingStatus1 = itemView.findViewById(R.id.tvParkingStatus1);
                tvReservationStatus1 = itemView.findViewById(R.id.tvReservationStatus1);
                tvLotSpace1 = itemView.findViewById(R.id.tvLotSpace1);
                tvLocationName1 = itemView.findViewById(R.id.tvLocationName1);
                tvPlatNo1 = itemView.findViewById(R.id.tvPlatNo1);

                tvReservationTime = itemView.findViewById(R.id.tvReservationTime);
                tvReservationEntryTime = itemView.findViewById(R.id.tvReservationEntryTime);
                tvReservationExitTime = itemView.findViewById(R.id.tvReservationExitTime);
                tvPaymentStatus = itemView.findViewById(R.id.tvPaymentStatus);
                tvReservationStatus = itemView.findViewById(R.id.tvReservationStatus);
                tvAddress = itemView.findViewById(R.id.tvAddress);
                tvParkingStatus = itemView.findViewById(R.id.tvParkingStatus);
                tvPaymentDueBy = itemView.findViewById(R.id.tvPaymentDueBy);
                tvPlatNo = itemView.findViewById(R.id.tvPlatNo);
                tvAmountDue = itemView.findViewById(R.id.tvAmountDue);
                tvAmountPaid = itemView.findViewById(R.id.tvAmountPaid);
                tvLocationName = itemView.findViewById(R.id.tvLocationName);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            ReserveParking reserveParking = reserveParkings.get(position);

                            ((vchome) getActivity()).show_back_button();
                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ReservationParkingDetailsFragment pay = new ReservationParkingDetailsFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("reserveParking", new Gson().toJson(reserveParking));
                            pay.setArguments(bundle);
                            ft.add(R.id.My_Container_1_ID, pay);
                            fragmentStack.lastElement().onPause();
                            ft.hide(fragmentStack.lastElement());
                            fragmentStack.push(pay);
                            ft.commitAllowingStateLoss();
                        }
                    }
                });
            }
        }
    }
}
