package com.softmeasures.eventezlyadminplus.frament.event.AddEdit;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragSessionSettingsAddEditBinding;
import com.softmeasures.eventezlyadminplus.frament.event.session.SessionsFragment;
import com.softmeasures.eventezlyadminplus.models.APPConfigAnnounce;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.EventSession;
import com.softmeasures.eventezlyadminplus.models.Manager;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_LOCATION;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_VIRTUALLY;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_BOTH;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class SessionSettingsAddEditFragment extends BaseFragment {

//    public FragSessionSettingsAddEditBinding binding;
//    private boolean isEdit = false, isRepeat = false;
//    private EventDefinition eventDefinition;
//    private EventSession eventSession;
//    private Manager selectedManager;
//    private int eventSessionLogiType = EVENT_TYPE_AT_LOCATION;
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        if (getArguments() != null) {
//            eventSessionLogiType = getArguments().getInt("eventSessionLogiType", EVENT_TYPE_AT_LOCATION);
//            isEdit = getArguments().getBoolean("isEdit", false);
//            selectedManager = new Gson().fromJson(getArguments().getString("selectedManager"), Manager.class);
//            eventDefinition = new Gson().fromJson(getArguments().getString("eventDetails"), EventDefinition.class);
//            eventSession = new Gson().fromJson(getArguments().getString("eventSessionDetails"), EventSession.class);
//
//            if (eventSession == null) {
//                eventSession = new EventSession();
//                eventSession.setIs_parking_allowed(true);
//            } else {
//                eventSessionLogiType = eventSession.getEvent_session_logi_type();
//            }
//        }
//        binding = DataBindingUtil.inflate(inflater, R.layout.frag_session_settings_add_edit, container, false);
//        return binding.getRoot();
//    }
//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        initViews(view);
//        updateViews();
//        setListeners();
//    }
//
//    @Override
//    protected void updateViews() {
//
//        if (eventSessionLogiType == EVENT_TYPE_AT_LOCATION) {
//            //Web
//            binding.llWebRegReq.setVisibility(View.GONE);
//            binding.llWebEventFree.setVisibility(View.GONE);
//            binding.llVirtualRegLimit.setVisibility(View.GONE);
//
//            //Local
//            binding.llParkingAllowed.setVisibility(View.VISIBLE);
//            binding.llLocalRegReq.setVisibility(View.VISIBLE);
//            binding.llParkingRegistration.setVisibility(View.VISIBLE);
//            binding.llLocalEventFree.setVisibility(View.VISIBLE);
//            binding.llParkingFree.setVisibility(View.VISIBLE);
//            binding.llLocalRegLimit.setVisibility(View.VISIBLE);
//            binding.llParkingLimit.setVisibility(View.VISIBLE);
//
//        } else if (eventSessionLogiType == EVENT_TYPE_AT_VIRTUALLY) {
//            //Web
//            binding.llWebRegReq.setVisibility(View.VISIBLE);
//            binding.llWebEventFree.setVisibility(View.VISIBLE);
//            binding.llVirtualRegLimit.setVisibility(View.VISIBLE);
//
//            //Local
//            binding.llParkingAllowed.setVisibility(View.GONE);
//            binding.llLocalRegReq.setVisibility(View.GONE);
//            binding.llParkingRegistration.setVisibility(View.GONE);
//            binding.llLocalEventFree.setVisibility(View.GONE);
//            binding.llParkingFree.setVisibility(View.GONE);
//            binding.llLocalRegLimit.setVisibility(View.GONE);
//            binding.llParkingLimit.setVisibility(View.GONE);
//
//        } else if (eventSessionLogiType == EVENT_TYPE_BOTH) {
//            //Web
//            binding.llWebRegReq.setVisibility(View.VISIBLE);
//            binding.llWebEventFree.setVisibility(View.VISIBLE);
//            binding.llVirtualRegLimit.setVisibility(View.VISIBLE);
//
//            //Local
//            binding.llParkingAllowed.setVisibility(View.VISIBLE);
//            binding.llLocalRegReq.setVisibility(View.VISIBLE);
//            binding.llParkingRegistration.setVisibility(View.VISIBLE);
//            binding.llLocalEventFree.setVisibility(View.VISIBLE);
//            binding.llParkingFree.setVisibility(View.VISIBLE);
//            binding.llLocalRegLimit.setVisibility(View.VISIBLE);
//            binding.llParkingLimit.setVisibility(View.VISIBLE);
//        }
//
//        if (isEdit && eventSession != null) {
//            binding.switchParkingAllowed.setChecked(eventSession.isIs_parking_allowed());
//            binding.switchLocalEventRegReq.setChecked(eventSession.isReqd_local_session_regn());
//            binding.switchWebEventRegReq.setChecked(eventSession.isReqd_web_session_regn());
//            binding.switchRegReqParking.setChecked(eventSession.isRegn_reqd_for_parking());
//            binding.switchEventRegAllowed.setChecked(eventSession.isEvent_session_regn_allowed());
//            binding.switchEventRegNeedApproval.setChecked(eventSession.isSession_regn_needed_approval());
//            binding.switchRenewable.setChecked(eventSession.isRenewable());
//            binding.switchActive.setChecked(eventSession.isActive());
//            binding.switchFreeSession.setChecked(eventSession.isFree_local_session());
//            binding.switchFreeWebEvent.setChecked(eventSession.isFree_web_session());
//            binding.switchFreeSessionParking.setChecked(eventSession.isFree_session_parking());
//
//            if (!TextUtils.isEmpty(eventSession.getEvent_session_regn_limit())) {
//                binding.etEventRegLimit.setText(eventSession.getEvent_session_regn_limit());
//            }
//
//            if (!TextUtils.isEmpty(eventSession.getWeb_event_session_regn_limit())) {
//                binding.etWebEventRegLimit.setText(eventSession.getWeb_event_session_regn_limit());
//            }
//            if (!TextUtils.isEmpty(eventSession.getEvent_session_parking_reserv_limit())) {
//                binding.etParkingReservationLimit.setText(eventSession.getEvent_session_parking_reserv_limit());
//            }
//
//        }
//
//    }
//
//    @Override
//    protected void setListeners() {
//
//        binding.tvBtnNext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (validate()) {
//                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
//                    Fragment fragment = new SessionInfoAddEditFragment();
//                    Bundle bundle = new Bundle();
//                    bundle.putBoolean("isEdit", isEdit);
//                    bundle.putBoolean("isRepeat", isRepeat);
//                    bundle.putString("eventSession", new Gson().toJson(eventSession));
//                    fragment.setArguments(bundle);
//                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
//                    ft.add(R.id.My_Container_1_ID, fragment);
//                    fragmentStack.lastElement().onPause();
//                    ft.hide(fragmentStack.lastElement());
//                    fragmentStack.push(fragment);
//                    ft.commitAllowingStateLoss();
//                }
//            }
//        });
//
//        //Allowed Parking?
//        binding.switchParkingAllowed.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            eventSession.setIs_parking_allowed(isChecked);
//            if (isChecked) {
//                binding.llParkingRegistration.setVisibility(View.VISIBLE);
//                binding.llParkingFree.setVisibility(View.VISIBLE);
//                binding.llParkingLimit.setVisibility(View.VISIBLE);
//            } else {
//                binding.llParkingRegistration.setVisibility(View.GONE);
//                binding.llParkingFree.setVisibility(View.GONE);
//                binding.llParkingLimit.setVisibility(View.GONE);
//            }
//        });
//
//        //Registration Required for Local Event?
//        binding.switchLocalEventRegReq.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            eventSession.setReqd_local_session_regn(isChecked);
//        });
//
//        //Registration Required for Web Event?
//        binding.switchWebEventRegReq.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            eventSession.setReqd_web_session_regn(isChecked);
//        });
//
//        //Registration Required for Parking?
//        binding.switchRegReqParking.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            eventSession.setRegn_reqd_for_parking(isChecked);
//        });
//
//        //Is Renewable?
//        binding.switchRenewable.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            eventSession.setRenewable(isChecked);
//        });
//
//        //Is Active?
//        binding.switchActive.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            eventSession.setActive(isChecked);
//        });
//
////        //Required User Info?
////        binding.switchRegUserInfoReq.setOnCheckedChangeListener((buttonView, isChecked) -> {
////            eventSession.setRegn_user_info_reqd(isChecked);
////        });
////
////        //Required Additional User Info
////        binding.switchRegAdditionalUserInfoReq.setOnCheckedChangeListener((buttonView, isChecked) -> {
////            eventSession.setRegn_addl_user_info_reqd(isChecked);
////        });
//
//        //Is Local Event Free?
//        binding.switchFreeSession.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            eventSession.setFree_session(isChecked);
//            eventSession.setFree_local_session(isChecked);
//        });
//
//        //Is Web Event Free?
//        binding.switchFreeWebEvent.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            eventSession.setFree_web_session(isChecked);
//        });
//
//        //Is Parking Free?
//        binding.switchFreeSessionParking.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            eventSession.setFree_session_parking(isChecked);
//        });
//
//    }
//
//    private boolean validate() {
//        if (eventSessionLogiType == EVENT_TYPE_AT_LOCATION) {
//
//            if (TextUtils.isEmpty(binding.etEventRegLimit.getText())) {
//                binding.etEventRegLimit.setError("Required!");
//                binding.etEventRegLimit.requestFocus();
//                return false;
//            } else {
//                eventSession.setEvent_session_regn_limit(binding.etEventRegLimit.getText().toString());
//            }
//
//            if (eventSession.isIs_parking_allowed()) {
//                if (TextUtils.isEmpty(binding.etParkingReservationLimit.getText())) {
//                    binding.etParkingReservationLimit.setError("Required!");
//                    binding.etParkingReservationLimit.requestFocus();
//                    return false;
//                } else {
//                    //TODO
//                    eventSession.setEvent_session_parking_reserv_limit(binding.etParkingReservationLimit.getText().toString());
//                }
//            }
//
//        } else if (eventSessionLogiType == EVENT_TYPE_AT_VIRTUALLY) {
//
//            if (TextUtils.isEmpty(binding.etWebEventRegLimit.getText())) {
//                binding.etWebEventRegLimit.setError("Required!");
//                binding.etWebEventRegLimit.requestFocus();
//                return false;
//            } else {
//                eventSession.setWeb_event_session_regn_limit(binding.etWebEventRegLimit.getText().toString());
//            }
//
//        } else if (eventSessionLogiType == EVENT_TYPE_BOTH) {
//            if (TextUtils.isEmpty(binding.etEventRegLimit.getText())) {
//                binding.etEventRegLimit.setError("Required!");
//                binding.etEventRegLimit.requestFocus();
//                return false;
//            } else {
//                eventSession.setEvent_session_regn_limit(binding.etEventRegLimit.getText().toString());
//            }
//
//            if (TextUtils.isEmpty(binding.etWebEventRegLimit.getText())) {
//                binding.etWebEventRegLimit.setError("Required!");
//                binding.etWebEventRegLimit.requestFocus();
//                return false;
//            } else {
//                eventSession.setWeb_event_session_regn_limit(binding.etWebEventRegLimit.getText().toString());
//            }
//
//            if (eventSession.isIs_parking_allowed()) {
//                if (TextUtils.isEmpty(binding.etParkingReservationLimit.getText())) {
//                    binding.etParkingReservationLimit.setError("Required!");
//                    binding.etParkingReservationLimit.requestFocus();
//                    return false;
//                } else {
//                    //TODO
//                    eventSession.setEvent_session_parking_reserv_limit(binding.etParkingReservationLimit.getText().toString());
//                }
//            }
//        }
//
//        if (!isEdit) {
//            eventSession.setManager_type(eventDefinition.getManager_type());
//            eventSession.setManager_type_id(eventDefinition.getManager_type_id());
//            eventSession.setTwp_id(eventDefinition.getTwp_id());
//            eventSession.setTownship_code(eventDefinition.getTownship_code());
//            eventSession.setTownship_name(eventDefinition.getTownship_name());
//            eventSession.setCompany_id(eventDefinition.getCompany_id());
//            eventSession.setCompany_code(eventDefinition.getCompany_code());
//            eventSession.setCompany_name(eventDefinition.getCompany_name());
//            eventSession.setCompany_logo(eventDefinition.getCompany_logo());
//            eventSession.setLocation_address(eventDefinition.getLocation_address());
//            eventSession.setEvent_id(eventDefinition.getId());
//            eventSession.setEvent_type(eventDefinition.getEvent_type());
//            eventSession.setEvent_name(eventDefinition.getEvent_name());
//            eventSession.setEvent_logo(eventDefinition.getEvent_logo());
//            eventSession.setLocation_lat_lng(eventDefinition.getLocation_lat_lng());
//            eventSession.setEvent_session_address(eventDefinition.getEvent_address());
//            eventSession.setCovered_locations(eventDefinition.getCovered_locations());
//        }
//        eventSession.setEvent_session_logi_type(eventSessionLogiType);
//
//        return true;
//    }
//
//    @Override
//    protected void initViews(View v) {
//
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        if (SessionsFragment.isSessionUpdated && getActivity() != null) {
//            getActivity().onBackPressed();
//        }
//    }

    public FragSessionSettingsAddEditBinding binding;
    private boolean isEdit = false, isRepeat = false;
    private EventDefinition eventDefinition;
    private EventSession eventSession;
    //    private Manager selectedManager;
    private int eventSessionLogiType = EVENT_TYPE_AT_LOCATION;
    private String TAG = "#DEBUG SessSett";
    private String freeLocalEventText, freeLocalEventLink, freeOnlineEventText, freeOnlineEventLink, freeParkingText, freeParkingLink;
    private ArrayList<APPConfigAnnounce> appConfigAnnouncesList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            eventSessionLogiType = getArguments().getInt("eventLogiType", EVENT_TYPE_AT_LOCATION);
            isEdit = getArguments().getBoolean("isEdit", false);
            // selectedManager = new Gson().fromJson(getArguments().getString("selectedManager"), Manager.class);
            eventDefinition = new Gson().fromJson(getArguments().getString("eventDetails"), EventDefinition.class);
            eventSession = new Gson().fromJson(getArguments().getString("eventSessionDetails"), EventSession.class);
            if (eventSession == null) {
                eventSession = new EventSession();
                eventSession.setIs_parking_allowed(true);
            } else {
                eventSessionLogiType = eventSession.getEvent_session_logi_type();
            }
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_session_settings_add_edit, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {

        if (eventSessionLogiType == EVENT_TYPE_AT_LOCATION) {
            Log.d("in_update_view","=local=");
            //Web
            binding.llWebRegReq.setVisibility(View.GONE);
            binding.llWebEventFree.setVisibility(View.GONE);
            binding.llVirtualRegLimit.setVisibility(View.GONE);

            //Local
            binding.llParkingAllowed.setVisibility(View.VISIBLE);
            binding.llLocalRegReq.setVisibility(View.VISIBLE);
            binding.llParkingRegistration.setVisibility(View.VISIBLE);
            binding.llLocalEventFree.setVisibility(View.VISIBLE);
            binding.llParkingFree.setVisibility(View.VISIBLE);
            binding.llLocalRegLimit.setVisibility(View.VISIBLE);
            binding.llParkingLimit.setVisibility(View.VISIBLE);

        } else if (eventSessionLogiType == EVENT_TYPE_AT_VIRTUALLY) {
            Log.d("in_update_view","=online=");

            //Web
            binding.llWebRegReq.setVisibility(View.VISIBLE);
            binding.llWebEventFree.setVisibility(View.VISIBLE);
            binding.llVirtualRegLimit.setVisibility(View.VISIBLE);

            //Local
            binding.llParkingAllowed.setVisibility(View.GONE);
            binding.llLocalRegReq.setVisibility(View.GONE);
            binding.llParkingRegistration.setVisibility(View.GONE);
            binding.llLocalEventFree.setVisibility(View.GONE);
            binding.llParkingFree.setVisibility(View.GONE);
            binding.llLocalRegLimit.setVisibility(View.GONE);
            binding.llParkingLimit.setVisibility(View.GONE);

        } else if (eventSessionLogiType == EVENT_TYPE_BOTH) {
            Log.d("in_update_view","=both=");

            //Web
            binding.llWebRegReq.setVisibility(View.VISIBLE);
            binding.llWebEventFree.setVisibility(View.VISIBLE);
            binding.llVirtualRegLimit.setVisibility(View.VISIBLE);

            //Local
            binding.llParkingAllowed.setVisibility(View.VISIBLE);
            binding.llLocalRegReq.setVisibility(View.VISIBLE);
            binding.llParkingRegistration.setVisibility(View.VISIBLE);
            binding.llLocalEventFree.setVisibility(View.VISIBLE);
            binding.llParkingFree.setVisibility(View.VISIBLE);
            binding.llLocalRegLimit.setVisibility(View.VISIBLE);
            binding.llParkingLimit.setVisibility(View.VISIBLE);
        }

        if (isEdit && eventSession != null) {
            binding.switchParkingAllowed.setChecked(eventSession.isParking_allowed());
            binding.switchLocalEventRegReq.setChecked(eventSession.isReqd_local_session_regn());
            binding.switchWebEventRegReq.setChecked(eventSession.isReqd_web_session_regn());
            binding.switchRegReqParking.setChecked(eventSession.isRegn_reqd_for_parking());
            binding.switchEventRegAllowed.setChecked(eventSession.isEvent_session_regn_allowed());
            binding.switchEventRegNeedApproval.setChecked(eventSession.isSession_regn_needed_approval());
            binding.switchRenewable.setChecked(eventSession.isRenewable());
            binding.switchActive.setChecked(eventSession.isActive());
            binding.switchFreeSession.setChecked(eventSession.isFree_local_session());
            binding.switchFreeWebEvent.setChecked(eventSession.isFree_web_session());
            binding.switchFreeSessionParking.setChecked(eventSession.isFree_session_parking());

            if (!TextUtils.isEmpty(eventSession.getEvent_session_regn_limit())) {
                binding.etEventRegLimit.setText(eventSession.getEvent_session_regn_limit());
            }

            if (!TextUtils.isEmpty(eventSession.getWeb_event_session_regn_limit())) {
                binding.etWebEventRegLimit.setText(eventSession.getWeb_event_session_regn_limit());
            }

            if (eventSession.getParking_reservation_limit() != 0) {
                binding.etParkingReservationLimit.setText(Integer.toString(eventSession.getParking_reservation_limit()));
            }
        }
    }

    @Override
    protected void setListeners() {

        binding.tvBtnNext.setOnClickListener(v -> {
            if (validate()) {
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment fragment = new SessionInfoAddEditFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("isEdit", isEdit);
                bundle.putBoolean("isRepeat", isRepeat);
                bundle.putString("eventSession", new Gson().toJson(eventSession));
                fragment.setArguments(bundle);
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.add(R.id.My_Container_1_ID, fragment);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(fragment);
                ft.commitAllowingStateLoss();
            }
        });

        //Allowed Parking?
        binding.switchParkingAllowed.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setIs_parking_allowed(isChecked);
            if (isChecked) {
                Log.d("inside_checked","====");
                binding.llParkingRegistration.setVisibility(View.VISIBLE);
                binding.llParkingFree.setVisibility(View.VISIBLE);
                binding.llParkingLimit.setVisibility(View.VISIBLE);
            } else {
                binding.llParkingRegistration.setVisibility(View.GONE);
                binding.llParkingFree.setVisibility(View.GONE);
                binding.llParkingLimit.setVisibility(View.GONE);
            }
        });

        //Registration Required for Local Event?
        binding.switchLocalEventRegReq.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setReqd_local_session_regn(isChecked);
        });

        //Registration Required for Web Event?
        binding.switchWebEventRegReq.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setReqd_web_session_regn(isChecked);
        });

        //Registration Required for Parking?
        binding.switchRegReqParking.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setRegn_reqd_for_parking(isChecked);
        });

        //Is Renewable?
        binding.switchRenewable.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setRenewable(isChecked);
        });

        //Is Active?
        binding.switchActive.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setActive(isChecked);
        });
//        //Required User Info?
//        binding.switchRegUserInfoReq.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            eventSession.setRegn_user_info_reqd(isChecked);
//        });
//
//        //Required Additional User Info
//        binding.switchRegAdditionalUserInfoReq.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            eventSession.setRegn_addl_user_info_reqd(isChecked);
//        });

        //Is Local Event Free?
        /*binding.switchFreeSession.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setFree_session(isChecked);
            eventSession.setFree_local_session(isChecked);
        });

        //Is Web Event Free?
        binding.switchFreeWebEvent.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setFree_web_session(isChecked);
        });

        //Is Parking Free?
        binding.switchFreeSessionParking.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setFree_session_parking(isChecked);
        });*/
        //Is Local Event Free?
        binding.switchFreeSession.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setFree_local_session(isChecked);
            if (!isChecked)
                if (!TextUtils.isEmpty(freeLocalEventText))
                    showDialog(freeLocalEventText, freeLocalEventLink, binding.switchFreeSession);
        });

        //Is Web Event Free?
        binding.switchFreeWebEvent.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setFree_web_session(isChecked);
            if (!isChecked)
                if (!TextUtils.isEmpty(freeOnlineEventText))
                    showDialog(freeOnlineEventText, freeOnlineEventLink, binding.switchFreeWebEvent);
        });

        //Is Parking Free?
        binding.switchFreeSessionParking.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setFree_session_parking(isChecked);
            if (!isChecked)
                if (!TextUtils.isEmpty(freeParkingText))
                    showDialog(freeParkingText, freeParkingLink, binding.switchFreeSessionParking);
        });
        binding.etEventRegLimit.setOnClickListener(v -> {
            binding.etEventRegLimit.setError(null);
            showEnter_txt("Local Session Registration Limit", "Enter Value", binding.etEventRegLimit, "etEventRegLimit");
        });
        binding.etWebEventRegLimit.setOnClickListener(v -> {
            binding.etWebEventRegLimit.setError(null);
            showEnter_txt("Web Session Registration Limit", "Enter Value", binding.etWebEventRegLimit, "etWebEventRegLimit");
        });
        binding.etParkingReservationLimit.setOnClickListener(v -> {
            binding.etParkingReservationLimit.setError(null);
            showEnter_txt("Parking Reservation Limit", "Enter Value", binding.etParkingReservationLimit, "etParkingReservationLimit");
        });
    }

    private boolean validate() {
        if (eventSessionLogiType == EVENT_TYPE_AT_LOCATION) {

            eventSession.setFree_session(eventSession.isFree_local_session());
            if (TextUtils.isEmpty(binding.etEventRegLimit.getText())) {
                binding.etEventRegLimit.setError("Required!");
                binding.etEventRegLimit.requestFocus();
                return false;
            } else
                eventSession.setEvent_session_regn_limit(binding.etEventRegLimit.getText().toString());

            if (eventSession.isParking_allowed()) {
                if (TextUtils.isEmpty(binding.etParkingReservationLimit.getText())) {
                    binding.etParkingReservationLimit.setError("Required!");
                    binding.etParkingReservationLimit.requestFocus();
                    return false;
                } else
                    eventSession.setParking_reservation_limit(Integer.parseInt(binding.etParkingReservationLimit.getText().toString()));
            }

        } else if (eventSessionLogiType == EVENT_TYPE_AT_VIRTUALLY) {
            eventSession.setFree_session(eventSession.isFree_web_session());
            if (TextUtils.isEmpty(binding.etWebEventRegLimit.getText())) {
                binding.etWebEventRegLimit.setError("Required!");
                binding.etWebEventRegLimit.requestFocus();
                return false;
            } else {
                eventSession.setWeb_event_session_regn_limit(binding.etWebEventRegLimit.getText().toString());
            }

        } else if (eventSessionLogiType == EVENT_TYPE_BOTH) {
            eventSession.setFree_session(eventSession.isFree_local_session() && eventSession.isFree_web_session());
            if (TextUtils.isEmpty(binding.etEventRegLimit.getText())) {
                binding.etEventRegLimit.setError("Required!");
                binding.etEventRegLimit.requestFocus();
                return false;
            } else
                eventSession.setEvent_session_regn_limit(binding.etEventRegLimit.getText().toString());

            if (TextUtils.isEmpty(binding.etWebEventRegLimit.getText())) {
                binding.etWebEventRegLimit.setError("Required!");
                binding.etWebEventRegLimit.requestFocus();
                return false;
            } else
                eventSession.setWeb_event_session_regn_limit(binding.etWebEventRegLimit.getText().toString());

            if (eventSession.isParking_allowed()) {
                if (TextUtils.isEmpty(binding.etParkingReservationLimit.getText())) {
                    binding.etParkingReservationLimit.setError("Required!");
                    binding.etParkingReservationLimit.requestFocus();
                    return false;
                } else
                    eventSession.setParking_reservation_limit(Integer.parseInt(binding.etParkingReservationLimit.getText().toString()));
            }
        }

        if (!isEdit) {
            eventSession.setManager_type(eventDefinition.getManager_type());
            eventSession.setManager_type_id(eventDefinition.getManager_type_id());
            eventSession.setTwp_id(eventDefinition.getTwp_id());
            eventSession.setTownship_code(eventDefinition.getTownship_code());
            eventSession.setTownship_name(eventDefinition.getTownship_name());
            eventSession.setCompany_id(eventDefinition.getCompany_id());
            eventSession.setCompany_code(eventDefinition.getCompany_code());
            eventSession.setCompany_name(eventDefinition.getCompany_name());
            eventSession.setCompany_logo(eventDefinition.getCompany_logo());
            eventSession.setLocation_address(eventDefinition.getLocation_address());
            eventSession.setEvent_id(eventDefinition.getId());
            eventSession.setEvent_type(eventDefinition.getEvent_type());
            eventSession.setEvent_name(eventDefinition.getEvent_name());
            eventSession.setEvent_logo(eventDefinition.getEvent_logo());
            eventSession.setLocation_lat_lng(eventDefinition.getLocation_lat_lng());
            eventSession.setEvent_session_address(eventDefinition.getEvent_address());
            eventSession.setCovered_locations(eventDefinition.getCovered_locations());
        }
        eventSession.setEvent_session_logi_type(eventSessionLogiType);

        return true;
    }

    @Override
    protected void initViews(View v) {
        new fetchFreeEventAlertTaxt().execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SessionsFragment.isSessionUpdated && getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

    private void showEnter_txt(String title, String hintText, TextView etTextView, String selectedEditText) {
        RelativeLayout viewGroup = (RelativeLayout) getActivity().findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.enter_txt, viewGroup, false);
        final PopupWindow popup = new PopupWindow(getActivity());
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(true);
        popup.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        popup.setFocusable(true);
        popup.update();
        popup.showAtLocation(layout, Gravity.TOP, 0, 0);

        final EditText edit_text;
        final TextView text_title, tvPaste;
        final Button btn_done;
        RelativeLayout btn_cancel;
        LinearLayout rl_edit;
        ImageView ivPaste;

        tvPaste = (TextView) layout.findViewById(R.id.tvPaste);
        ivPaste = (ImageView) layout.findViewById(R.id.ivPaste);
        edit_text = (EditText) layout.findViewById(R.id.edit_nu);
        text_title = (TextView) layout.findViewById(R.id.txt_title);
        text_title.setText(title);
        edit_text.setInputType(InputType.TYPE_CLASS_NUMBER);
        edit_text.setHint(hintText);

        if (!TextUtils.isEmpty(etTextView.getText()))
            edit_text.setText(etTextView.getText());
        edit_text.setSelection(edit_text.getText().length());

        edit_text.setFocusable(true);
        btn_cancel = (RelativeLayout) layout.findViewById(R.id.close);
        btn_done = (Button) layout.findViewById(R.id.btn_done);
        rl_edit = (LinearLayout) layout.findViewById(R.id.rl_edit);

        edit_text.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        edit_text.setOnClickListener(v -> {
//            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            tvPaste.setVisibility(View.GONE);
        });
        rl_edit.setOnClickListener(v -> tvPaste.setVisibility(View.GONE));
        edit_text.setOnLongClickListener(v -> {
            tvPaste.setVisibility(View.VISIBLE);
            return true;
        });
        tvPaste.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData pData = clipboard.getPrimaryClip();
            if (pData != null) {
                ClipData.Item item = pData.getItemAt(0);
                String txtpaste = item.getText().toString();
                edit_text.getText().insert(edit_text.getSelectionStart(), txtpaste);
                edit_text.setText(edit_text.getText()+txtpaste);
            }
        });
        ivPaste.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData pData = clipboard.getPrimaryClip();


            try {

                ClipData.Item item = pData.getItemAt(0);
                String txtpaste = item.getText().toString();
                edit_text.getText().insert(edit_text.getSelectionStart(), txtpaste);
                edit_text.setText(edit_text.getText()+txtpaste);
            }catch (Exception e)
            {
               // Toast.makeText(myApp, "Clipboard is empty", Toast.LENGTH_SHORT).show();
            }

        });
        btn_done.setOnClickListener(v -> {
            imm.hideSoftInputFromWindow(edit_text.getWindowToken(), 0);
            etTextView.setText(edit_text.getText().toString());
            popup.dismiss();
            if (eventSessionLogiType == EVENT_TYPE_AT_LOCATION) {
                if (selectedEditText.equals("etEventRegLimit"))
                    showEnter_txt("Parking Reservation Limit", "Enter Value", binding.etParkingReservationLimit, "etParkingReservationLimit");
            }
            if (eventSessionLogiType == EVENT_TYPE_BOTH) {
                if (selectedEditText.equals("etEventRegLimit"))
                    showEnter_txt("Web Session Registration Limit", "Enter Value", binding.etWebEventRegLimit, "etWebEventRegLimit");
                if (selectedEditText.equals("etWebEventRegLimit"))
                    showEnter_txt("Parking Reservation Limit", "Enter Value", binding.etParkingReservationLimit, "etParkingReservationLimit");
            }
        });
        btn_cancel.setOnClickListener(v -> {
            InputMethodManager imm1 = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm1.hideSoftInputFromWindow(edit_text.getWindowToken(), 0);
            String txt = edit_text.getText().toString();
            popup.dismiss();
        });
    }

    private void showDialog(String title, String link, SwitchCompat switchFreeEvent) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(Html.fromHtml("<p>" + title + ".<br/><a href='" + link + "'>" + link + "</a></p>"));
        builder1.setCancelable(false);
        builder1.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                switchFreeEvent.setChecked(true);
            }
        });
        AlertDialog Alert1 = builder1.create();
        Alert1.show();
        ((TextView) Alert1.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }

    private class fetchFreeEventAlertTaxt extends AsyncTask<String, String, String> {
        JSONObject json;
        String apiUrl = "_table/app_config_announce";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            binding.rlProgressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + apiUrl;
            Log.e(TAG, "      fetchFreeEventTaxt:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");
                    for (int j = 0; j < jsonArray.length(); j++) {
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("app_id")
                                && !TextUtils.isEmpty(object.getString("app_id"))
                                && !object.getString("app_id").equals("null") && object.getInt("app_id") == 23) {
                            APPConfigAnnounce announce = new APPConfigAnnounce();
                            announce.setAppId(object.getInt("app_id"));
                            if (object.has("id")
                                    && !TextUtils.isEmpty(object.getString("id"))
                                    && !object.getString("id").equals("null")) {
                                announce.setId(object.getInt("id"));
                            }
                            if (object.has("app_name")
                                    && !TextUtils.isEmpty(object.getString("app_name"))
                                    && !object.getString("app_name").equals("null")) {
                                announce.setAppName(object.getString("app_name"));
                            }
                            if (object.has("app_table")
                                    && !TextUtils.isEmpty(object.getString("app_table"))
                                    && !object.getString("app_table").equals("null")) {
                                announce.setAppTable(object.getString("app_table"));
                            }
                            if (object.has("app_field_1")
                                    && !TextUtils.isEmpty(object.getString("app_field_1"))
                                    && !object.getString("app_field_1").equals("null")) {
                                announce.setAppField1(object.getString("app_field_1"));
                            }
                            if (object.has("app_field_1_value")
                                    && !TextUtils.isEmpty(object.getString("app_field_1_value"))
                                    && !object.getString("app_field_1_value").equals("null")) {
                                announce.setAppField1Value(object.getBoolean("app_field_1_value"));
                            }
                            if (object.has("app_field_2")
                                    && !TextUtils.isEmpty(object.getString("app_field_2"))
                                    && !object.getString("app_field_2").equals("null")) {
                                announce.setAppField2(object.getString("app_field_2"));
                            }
                            if (object.has("app_field_2_value")
                                    && !TextUtils.isEmpty(object.getString("app_field_2_value"))
                                    && !object.getString("app_field_2_value").equals("null")) {
                                announce.setAppField2Value(object.getBoolean("app_field_2_value"));
                            }
                            if (object.has("app_field_alert_message")
                                    && !TextUtils.isEmpty(object.getString("app_field_alert_message"))
                                    && !object.getString("app_field_alert_message").equals("null")) {
                                announce.setAppFieldAlertMessage(object.getString("app_field_alert_message"));
                            }

                            if (object.has("app_field_alert_proceed_link")
                                    && !TextUtils.isEmpty(object.getString("app_field_alert_proceed_link"))
                                    && !object.getString("app_field_alert_proceed_link").equals("null")) {
                                announce.setAppFieldAlertProceedLink(object.getString("app_field_alert_proceed_link"));
                            }

                            if (announce.getAppField1().equals("local_free_only") && announce.getAppField2().equals("local_paid")) {
                                if (announce.getAppField1Value() && !announce.getAppField2Value()) {
                                    freeLocalEventText = announce.getAppFieldAlertMessage();
                                    freeLocalEventLink = announce.getAppFieldAlertProceedLink();
                                }
                            }

                            if (announce.getAppField1().equals("web_free_only") && announce.getAppField2().equals("web_paid")) {
                                if (announce.getAppField1Value() && !announce.getAppField2Value()) {
                                    freeOnlineEventText = announce.getAppFieldAlertMessage();
                                    freeOnlineEventLink = announce.getAppFieldAlertProceedLink();
                                }
                            }

                            if (announce.getAppField1().equals("parking_free_only") && announce.getAppField2().equals("parking_paid")) {
                                if (announce.getAppField1Value() && !announce.getAppField2Value()) {
                                    freeParkingText = announce.getAppFieldAlertMessage();
                                    freeParkingLink = announce.getAppFieldAlertProceedLink();
                                }
                            }
                            appConfigAnnouncesList.add(announce);
                        }
                    }
                    Log.e("#DEBUG", "fetchFreeEventAlertTaxt " + jsonArray.length());
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            binding.rlProgressbar.setVisibility(View.GONE);
            if (getActivity() != null) {
               /* if (appConfigAnnouncesList.size() != 0) {
                    for (int i = 0; i < appConfigAnnouncesList.size(); i++) {
                        if (appConfigAnnouncesList.get(i).getAppField1().equals("local_free_only") && appConfigAnnouncesList.get(i).getAppField2().equals("local_paid")) {
                            if (appConfigAnnouncesList.get(i).getAppField1Value() && !appConfigAnnouncesList.get(i).getAppField2Value()) {
                                freeLocalEvent = i;
                            }
                        }

                        if (appConfigAnnouncesList.get(i).getAppField1().equals("web_free_only") && appConfigAnnouncesList.get(i).getAppField2().equals("web_paid")) {
                            if (appConfigAnnouncesList.get(i).getAppField1Value() && !appConfigAnnouncesList.get(i).getAppField2Value()) {
                                freeOnlineEvent = i;
                            }
                        }

                        if (appConfigAnnouncesList.get(i).getAppField1().equals("parking_free_only") && appConfigAnnouncesList.get(i).getAppField2().equals("parking_paid")) {
                            if (appConfigAnnouncesList.get(i).getAppField1Value() && !appConfigAnnouncesList.get(i).getAppField2Value()) {
                                freeParking = i;
                            }
                        }
                    }
                }*/
            }
        }
    }
}
