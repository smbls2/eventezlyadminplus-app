package com.softmeasures.eventezlyadminplus.frament.locations;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragEditPartnerBinding;
import com.softmeasures.eventezlyadminplus.models.ParkingPartner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class EditPartnerFragment extends BaseFragment {

    public static FragEditPartnerBinding binding;
    public static ParkingPartner parkingPartner;
    private String parkingType = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            parkingPartner = new Gson().fromJson(getArguments().getString("selectedSpot"), ParkingPartner.class);
            parkingType = getArguments().getString("parkingType");
            if (parkingPartner != null) {
                Log.e("#DEBUG", "  EditPartnerFragment:  parkingPartner:  " + new Gson().toJson(parkingPartner));
            }
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_edit_partner, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {
        if (parkingPartner != null) {
            if (!TextUtils.isEmpty(parkingPartner.getLocation_name())) {
                binding.etLocationTitle.setText(parkingPartner.getLocation_name());
            }
            if (!TextUtils.isEmpty(parkingPartner.getLocation_code())) {
                binding.etLocationCode.setText(parkingPartner.getLocation_code());
            }
            if (!TextUtils.isEmpty(parkingPartner.getMarker_type())) {
                binding.etLocationType.setText(parkingPartner.getMarker_type());
            }
            if (!TextUtils.isEmpty(parkingPartner.getAddress())) {
                binding.etLocationAddress.setText(parkingPartner.getAddress());
            }
            if (!TextUtils.isEmpty(parkingPartner.getParking_times())) {
                binding.etParkingTime.setText(parkingPartner.getParking_times());
            }
            if (!TextUtils.isEmpty(parkingPartner.getNo_parking_times())) {
                binding.etNoParkingTimes.setText(parkingPartner.getNo_parking_times());
            }

            if (!TextUtils.isEmpty(String.valueOf(parkingPartner.getOff_peak_discount()))) {
                binding.etOffPeakDiscount.setText(String.valueOf(parkingPartner.getOff_peak_discount()));
            }

            if (!TextUtils.isEmpty(parkingPartner.getOff_peak_starts())) {
                binding.etOffPeakStartHour.setText(parkingPartner.getOff_peak_starts());
            }
            if (!TextUtils.isEmpty(parkingPartner.getOff_peak_ends())) {
                binding.etOffPeakEndHour.setText(parkingPartner.getOff_peak_ends());
            }

            if (!TextUtils.isEmpty(String.valueOf(parkingPartner.getWeekend_special_diff()))) {
                binding.etWeekendDiff.setText(String.valueOf(parkingPartner.getWeekend_special_diff()));
            }

            if (parkingPartner.getRenewable() == 1) {
                binding.switchRenewable.setChecked(true);
            } else binding.switchRenewable.setChecked(false);

            if (parkingPartner.getIn_effect() == 1) {
                binding.switchInEffect.setChecked(true);
            } else binding.switchInEffect.setChecked(false);

            if (parkingPartner.getActive() == 1) {
                binding.switchActive.setChecked(true);
            } else binding.switchActive.setChecked(false);

            if (parkingPartner.getEvent_on_location() == 1) {
                binding.switchEvent.setChecked(true);
                binding.tvBtnEventRules.setVisibility(View.VISIBLE);
            } else {
                binding.switchEvent.setChecked(false);
                binding.tvBtnEventRules.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(parkingPartner.getCustom_notice())) {
                binding.etCustomNotice.setText(parkingPartner.getCustom_notice());
            }

        }
    }

    @Override
    protected void setListeners() {
        binding.switchActive.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) parkingPartner.setActive(1);
            else parkingPartner.setActive(0);
        });

        binding.switchInEffect.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) parkingPartner.setIn_effect(1);
            else parkingPartner.setIn_effect(0);
        });

        binding.switchRenewable.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) parkingPartner.setRenewable(1);
            else parkingPartner.setRenewable(0);
        });

        binding.switchEvent.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                parkingPartner.setEvent_on_location(1);
                binding.tvBtnEventRules.setVisibility(View.VISIBLE);
            } else {
                parkingPartner.setEvent_on_location(0);
                binding.tvBtnEventRules.setVisibility(View.GONE);
            }
        });

        binding.etLocationAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                SelectAddressFragment pay = new SelectAddressFragment();
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        binding.tvBtnUpdate.setOnClickListener(v -> {
            if (isValidate()) {
                updateParkingPartner();
            }
        });

        binding.tvBtnRules.setOnClickListener(v -> {
            openEditRules(false);
        });

        binding.tvBtnEventRules.setOnClickListener(v -> {
            openEditRules(true);
        });
    }

    private void openEditRules(boolean isEventRule) {
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString("type", parkingType);
        bundle.putBoolean("isEventRule", isEventRule);
        bundle.putString("selectedSpot", new Gson().toJson(parkingPartner));
        ParkingRulesFragment pay = new ParkingRulesFragment();
        pay.setArguments(bundle);
        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.add(R.id.My_Container_1_ID, pay);
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(pay);
        ft.commitAllowingStateLoss();
    }

    private void updateParkingPartner() {
        Log.e("#DEBUG", "   updateParkingPartner:  parkingPartner:  "
                + new Gson().toJson(parkingPartner));

        new updateParkingPartnerDetails().execute();
    }

    public class updateParkingPartnerDetails extends AsyncTask<String, String, String> {
        String updateUrl = "_table/township_parking_partners";
        JSONObject json12 = null;
        String id = "";

        @Override
        protected void onPreExecute() {
            binding.rlProgressbar.setVisibility(View.VISIBLE);
            if (parkingType.equalsIgnoreCase("Township")) {
                updateUrl = "_table/township_parking_partners";
            } else if (parkingType.equalsIgnoreCase("Commercial")) {
                updateUrl = "_table/other_parking_partners";
            } else if (parkingType.equalsIgnoreCase("Others")) {
                updateUrl = "_table/other_parking_partners";
            }
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            //TODO
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("id", parkingPartner.getId());
            jsonValues1.put("location_name", parkingPartner.getLocation_name());
            jsonValues1.put("title", parkingPartner.getTitle());
            jsonValues1.put("active", parkingPartner.getActive());
            jsonValues1.put("address", parkingPartner.getAddress());
            jsonValues1.put("custom_notice", parkingPartner.getCustom_notice());
            jsonValues1.put("in_effect", parkingPartner.getIn_effect());
            jsonValues1.put("lat", parkingPartner.getLat());
            jsonValues1.put("lng", parkingPartner.getLng());
            jsonValues1.put("no_parking_times", parkingPartner.getNo_parking_times());
            jsonValues1.put("off_peak_discount", parkingPartner.getOff_peak_discount());
            jsonValues1.put("off_peak_ends", parkingPartner.getOff_peak_ends());
            jsonValues1.put("off_peak_starts", parkingPartner.getOff_peak_starts());
            jsonValues1.put("parking_times", parkingPartner.getParking_times());
            jsonValues1.put("renewable", parkingPartner.getRenewable());
            jsonValues1.put("weekend_special_diff", parkingPartner.getWeekend_special_diff());
            jsonValues1.put("event_on_location", parkingPartner.getEvent_on_location());

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + updateUrl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            binding.rlProgressbar.setVisibility(View.GONE);
            //progressBar.setVisibility(View.GONE);
            if (getActivity() != null) {
                if (json12 != null) {
                    if (json12.has("error")) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setMessage("Error");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    } else {
                        if (!id.equals("")) {
                            LocationsByTypeFragment.isUpdated = true;
                            Toast.makeText(getActivity(), "Parking details updated!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    private boolean isValidate() {
        if (TextUtils.isEmpty(binding.etLocationTitle.getText().toString())) {
            showToastMessage("Please enter title!");
            return false;
        } else if (TextUtils.isEmpty(binding.etLocationCode.getText().toString())) {
            showToastMessage("Please enter location code!");
            return false;
        } else if (TextUtils.isEmpty(binding.etLocationCode.getText().toString())) {
            showToastMessage("Please enter location type!");
            return false;
        } else if (TextUtils.isEmpty(binding.etLocationAddress.getText().toString())) {
            showToastMessage("Please enter address!");
            return false;
        } else if (TextUtils.isEmpty(binding.etParkingTime.getText().toString())) {
            showToastMessage("Please enter parking time!");
            return false;
        } else if (TextUtils.isEmpty(binding.etNoParkingTimes.getText().toString())) {
            showToastMessage("Please enter no parking time!");
            return false;
        } else if (TextUtils.isEmpty(binding.etOffPeakDiscount.getText().toString())) {
            showToastMessage("Please enter off peak discount!");
            return false;
        } else if (TextUtils.isEmpty(binding.etOffPeakStartHour.getText().toString())) {
            showToastMessage("Please enter off peak start hour!");
            return false;
        } else if (TextUtils.isEmpty(binding.etOffPeakEndHour.getText().toString())) {
            showToastMessage("Please enter off peak end hour!");
            return false;
        } else if (TextUtils.isEmpty(binding.etWeekendDiff.getText().toString())) {
            showToastMessage("Please enter weekend special difference!");
            return false;
        } else if (TextUtils.isEmpty(binding.etCustomNotice.getText().toString())) {
            showToastMessage("Please enter custom notice!");
            return false;
        }

        parkingPartner.setLocation_name(binding.etLocationTitle.getText().toString());
        parkingPartner.setTitle(binding.etLocationTitle.getText().toString());
        parkingPartner.setLocation_code(binding.etLocationCode.getText().toString());
        parkingPartner.setMarker_type(binding.etLocationType.getText().toString());
        parkingPartner.setAddress(binding.etLocationAddress.getText().toString());
        parkingPartner.setParking_times(binding.etParkingTime.getText().toString());
        parkingPartner.setNo_parking_times(binding.etNoParkingTimes.getText().toString());
        parkingPartner.setOff_peak_discount(Integer.valueOf(binding.etOffPeakDiscount.getText().toString()));
        parkingPartner.setOff_peak_starts(binding.etOffPeakStartHour.getText().toString());
        parkingPartner.setOff_peak_ends(binding.etOffPeakEndHour.getText().toString());
        parkingPartner.setWeekend_special_diff(Integer.valueOf(binding.etWeekendDiff.getText().toString()));
        parkingPartner.setCustom_notice(binding.etCustomNotice.getText().toString());

        return true;
    }

    private void showToastMessage(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void initViews(View v) {

    }
}
