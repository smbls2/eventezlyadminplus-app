package com.softmeasures.eventezlyadminplus.frament.event.AddEdit;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.compressor.Compressor;
import com.softmeasures.eventezlyadminplus.databinding.FragEventParkingLocationAddEditBinding;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.MediaDefinition;
import com.softmeasures.eventezlyadminplus.models.MultiDate;
import com.softmeasures.eventezlyadminplus.models.Upload;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.softmeasures.eventezlyadminplus.frament.event.AddEdit.EventLocationAddEditFragment.selectedTimeZone;
import static com.softmeasures.eventezlyadminplus.frament.event.AllEventsFragment.isUpdated;

public class EventParkingLocationAddEditFragment extends BaseFragment {

    public FragEventParkingLocationAddEditBinding binding;

    private ProgressDialog progressDialog;
    private String imageOption = "";
    public static boolean isEventLogoChange = false;

    private StringBuilder eventDates = new StringBuilder();
    private StringBuilder eventTiming = new StringBuilder();
    AmazonS3 s3;
    TransferUtility transferUtility;
    private ArrayList<Upload> imageUploads = new ArrayList<>();
    public static ArrayList<String> selectedLocations = new ArrayList<>();
    private static final int REQUEST_PICK_IMAGE = 985;
    private ArrayList<MultiDate> multiDates = new ArrayList<>();
    private String selectedStartDateForMultiDate = "", selectedEndDateForMultiDate = "";

    private EventDefinition eventDefinition;
    private ArrayList<String> eventImageUploads = new ArrayList<>();
    private boolean isEdit = false, isRepeat = false;
    private boolean isDateChanged = false;

    private ArrayList<Upload> fileUploads = new ArrayList<>();
    List<MediaDefinition> mediaDefinitionArray = new ArrayList<>();
    MediaDefinition mediaDefinition;
    private int percentage = 0;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            isEdit = getArguments().getBoolean("isEdit", false);
            isRepeat = getArguments().getBoolean("isRepeat", false);
            eventDefinition = new Gson().fromJson(getArguments().getString("eventDefinition"), EventDefinition.class);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_event_parking_location_add_edit, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        s3 = new AmazonS3Client(new BasicAWSCredentials("AKIAJNG22KFRVUAICSEA", "NSrQR/yAg52RPD3kp3FMb3NO+Vrxuty4iAwPU4th"));
        transferUtility = new TransferUtility(s3, getActivity().getApplicationContext());

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {

        if (isEdit) {

            if (isRepeat) {
                binding.tvBtnAddEvent.setText("REPEAT");
            } else binding.tvBtnAddEvent.setText("UPDATE");

            if (eventDefinition != null) {

                if (eventDefinition.getEvent_parking_begins_date_time() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_parking_begins_date_time())) {
                    binding.etParkingBeginsTime.setText(eventDefinition.getEvent_parking_begins_date_time());
                    selectedStartDateForMultiDate = eventDefinition.getEvent_parking_begins_date_time();
                }

                if (eventDefinition.getEvent_parking_ends_date_time() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_parking_ends_date_time())) {
                    binding.etParkingEndTime.setText(eventDefinition.getEvent_parking_ends_date_time());
                    selectedEndDateForMultiDate = eventDefinition.getEvent_parking_ends_date_time();
                }

                if (eventDefinition.getEvent_parking_timings() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_parking_timings())) {
                    try {
                        JSONArray jsonArrayTime = null;
                        StringBuilder stringBuilder = new StringBuilder();
                        jsonArrayTime = new JSONArray(eventDefinition.getEvent_parking_timings());
                        for (int i = 0; i < jsonArrayTime.length(); i++) {
                            JSONArray jsonArray2 = jsonArrayTime.getJSONArray(i);
                            if (i != 0) {
                                stringBuilder.append("\n");
                            }
                            if (jsonArray2 != null && jsonArray2.length() > 0) {
//                                    stringBuilder.append(jsonArray2.get(0).toString());
                                if (jsonArray2.get(0).toString().length() > 30) {
                                    String[] s = jsonArray2.get(0).toString().split(" - ");
                                    for (int j = 0; j < s.length; j++) {
                                        if (j != 0) {
                                            stringBuilder.append("-");
                                        }
                                        stringBuilder.append(DateTimeUtils.gmtToLocalDateAMPM(s[j]));
                                    }
                                } else {
                                    stringBuilder.append(jsonArray2.get(0).toString());
                                }
                            }
                        }
                        binding.etParkingTiming.setText(stringBuilder.toString());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (eventDefinition.isParking_allowed()) {
                    binding.etParkingBeginsTime.setEnabled(true);
                    binding.etParkingEndTime.setEnabled(true);
                    binding.etParkingTiming.setEnabled(true);
                } else {
                    binding.etParkingBeginsTime.setEnabled(false);
                    binding.etParkingEndTime.setEnabled(false);
                    binding.etParkingTiming.setEnabled(false);
                }

            }

        } else {
            binding.tvBtnAddEvent.setText("ADD");
        }
    }

    @Override
    protected void setListeners() {
/*
        binding.tvBtnAddEvent.setOnClickListener(v -> {

            if (isValidate()) {
                Log.e("#DEBUG", "  isValid:  eventDefinition:  " + new Gson().toJson(eventDefinition));
                imageUploads.clear();
                if (EventInfoAddEditFragment.isEventLogoChange && !TextUtils.isEmpty(eventDefinition.getEvent_logo())) {
                    File ImageFile = new File(eventDefinition.getEvent_logo());
                    File compressedImageFile = null;
                    try {
                        compressedImageFile = new Compressor(getActivity()).compressToFile(ImageFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Upload upload = new Upload();
                    upload.setType("Logo");
                    upload.setLink(String.valueOf(compressedImageFile));
                    imageUploads.add(upload);
                }

                for (int i = 1; i < eventDefinition.getEventImages().size(); i++) {
                    if (!eventDefinition.getEventImages().get(i).getPath().contains("parkezly-images")) {
                        File ImageFile = new File(eventDefinition.getEventImages().get(i).getPath());
                        File compressedImageFile = null;
                        try {
                            compressedImageFile = new Compressor(getActivity()).compressToFile(ImageFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Upload upload = new Upload();
                        upload.setType("Image");
                        upload.setLink(String.valueOf(compressedImageFile));
                        imageUploads.add(upload);
                    } else {
                        eventImageUploads.add(eventDefinition.getEventImages().get(i).getPath());
                    }
                }

                if (imageUploads.size() > 0) {
                    upload_file();
                } else {
                    new updateEventDetails().execute();
                }
            }

        });*/
        binding.tvBtnAddEvent.setOnClickListener(v -> {
            if (isValidate()) {
                Log.e("#DEBUG", "  isValid  eventDefinition:  \n" + new Gson().toJson(eventDefinition));
                fileUploads.clear();
                if (EventInfoAddEditFragment.isEventLogoChange && !TextUtils.isEmpty(eventDefinition.getEvent_logo())) {
                    Log.e("#DEBUG", "  Event Logo Path:   " + eventDefinition.getEvent_logo());
                    if (!eventDefinition.getEvent_logo().contains("parkezly-images")) {
                        File ImageFile = new File(eventDefinition.getEvent_logo());
                        File compressedImageFile = null;
                        try {
                            compressedImageFile = new Compressor(getActivity()).compressToFile(ImageFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Upload upload = new Upload();
                        upload.setType("Logo");
                        upload.setLink(String.valueOf(compressedImageFile));
                        fileUploads.add(upload);
                    }
                }

                for (int i = 1; i < eventDefinition.getEventImages().size(); i++) {
                    if (!eventDefinition.getEventImages().get(i).getPath().contains("parkezly-images")) {
                        Log.e("#DEBUG", "         Event Image Path:   " + eventDefinition.getEventImages().get(i).getPath());
                        File ImageFile = new File(eventDefinition.getEventImages().get(i).getPath());
                        File compressedImageFile = null;
                        try {
                            compressedImageFile = new Compressor(getActivity()).compressToFile(ImageFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Upload upload = new Upload();
                        upload.setType("Image");
                        upload.setLink(String.valueOf(compressedImageFile));
                        fileUploads.add(upload);
                    } else {
                        eventImageUploads.add(eventDefinition.getEventImages().get(i).getPath());
                    }
                }

                Gson gson = new Gson();
                Type type = new TypeToken<List<MediaDefinition>>() {
                }.getType();
                mediaDefinitionArray = gson.fromJson(eventDefinition.getEvent_link_array(), type);
                for (int i = 0; i < mediaDefinitionArray.size(); i++) {
                    mediaDefinition = mediaDefinitionArray.get(i);
                    if (mediaDefinition.getMedia_platform().equals("PDF")) {
                        if (!mediaDefinition.getPdfFilePath().contains("parkezly-images")) {
                            if (mediaDefinition.getPdfFilePath() != null) {
                                File pdfFile = new File(mediaDefinition.getPdfFilePath());
                                Upload upload = new Upload();
                                upload.setType("pdf");
                                upload.setPos(i);
                                upload.setLink(String.valueOf(pdfFile));
                                fileUploads.add(upload);
                            }
                        }
                    } else if (mediaDefinition.getMedia_platform().equals("Powerpoint")) {
                        if (!mediaDefinition.getPptFilePath().contains("parkezly-images")) {
                            Log.d("#DEBUG", "PowerPoint: " + mediaDefinition.getPptFilePath());
                            if (mediaDefinition.getPptFilePath() != null) {
                                File pptFile = new File(mediaDefinition.getPptFilePath());
                                Upload upload = new Upload();
                                upload.setType("ppt");
                                upload.setPos(i);
                                upload.setLink(String.valueOf(pptFile));
                                fileUploads.add(upload);
                            }
                        }
                    } else if (mediaDefinition.getMedia_platform().equals("Slideshow")) {
                        if (!mediaDefinition.getFilePath().contains("parkezly-images")) {
                            Log.d("#DEBUG", "Slideshow: " + mediaDefinition.getFilePath());
                            if (mediaDefinition.getFilePath() != null) {
                                File file = new File(mediaDefinition.getFilePath());
                                Upload upload = new Upload();
                                upload.setType("file");
                                upload.setPos(i);
                                upload.setLink(String.valueOf(file));
                                fileUploads.add(upload);
                            }
                        }
                    } else if (mediaDefinition.getMedia_platform().equals("Audio/Video")) {
                        if (!mediaDefinition.getAudioVideoPath().contains("parkezly-images")) {
                            Log.d("#DEBUG", "Audio/Video: " + mediaDefinition.getAudioVideoPath());
                            if (mediaDefinition.getAudioVideoPath() != null) {
                                File file = new File(mediaDefinition.getAudioVideoPath());
                                Upload upload = new Upload();
                                upload.setType(mediaDefinition.getAudioVideoType());
                                upload.setPos(i);
                                upload.setLink(String.valueOf(file));
                                fileUploads.add(upload);
                            }
                        }
                    }
                }
                Log.d("#DEBUG", "fileUpload size: " + fileUploads.size());
                if (fileUploads.size() > 0) {
                    upload_file();
                } else {
                    Log.d("#DEBUG", "Msg3: updateEventDetails().execute()");
                    new updateEventDetails().execute();
                }
            }
        });

        binding.ivBtnAddParkingTiming.setOnClickListener(v -> {
//            showDialogAddParkingTiming();
            if (TextUtils.isEmpty(binding.etParkingBeginsTime.getText())) {
                Toast.makeText(getActivity(), "Please select event begin time first!", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(binding.etParkingEndTime.getText())) {
                Toast.makeText(getActivity(), "Please select event end time first!", Toast.LENGTH_SHORT).show();
            } else {
                multiDates.clear();
                ArrayList<String> dates = DateTimeUtils.getDates(selectedStartDateForMultiDate, selectedEndDateForMultiDate);
                for (int i = 0; i < dates.size(); i++) {
                    MultiDate multiDate = new MultiDate();
                    multiDate.setSelected(true);
                    multiDate.setDate(dates.get(i));
                    multiDate.setStartTime(selectedStartDateForMultiDate);
                    multiDate.setEndTime(selectedEndDateForMultiDate);
                    multiDates.add(multiDate);
                }
                showDialogSelectEventMultiDates();
            }
        });

        binding.etParkingTiming.setOnClickListener(v -> {
            if (TextUtils.isEmpty(binding.etParkingBeginsTime.getText())) {
                Toast.makeText(getActivity(), "Please select event begin time first!", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(binding.etParkingEndTime.getText())) {
                Toast.makeText(getActivity(), "Please select event end time first!", Toast.LENGTH_SHORT).show();
            } else {
                multiDates.clear();
                ArrayList<String> dates = DateTimeUtils.getDates(selectedStartDateForMultiDate, selectedEndDateForMultiDate);
                for (int i = 0; i < dates.size(); i++) {
                    MultiDate multiDate = new MultiDate();
                    multiDate.setSelected(true);
                    multiDate.setDate(dates.get(i));
                    multiDate.setStartTime(selectedStartDateForMultiDate);
                    multiDate.setEndTime(selectedEndDateForMultiDate);
                    multiDates.add(multiDate);
                }
                showDialogSelectEventMultiDates();
            }
        });


        binding.etParkingBeginsTime.setOnClickListener(v -> {
            showDialogParkingBeginsTime();
        });
        binding.etParkingEndTime.setOnClickListener(v -> {
            showDialogParkingEntTime();
        });

    }

    String final_Division;
    StringBuffer file_Division;

    public void upload_file() {
        progressDialog.show();
        file_Division = new StringBuffer();
        String fileUrl = "";
        fileUrl = fileUploads.get(0).getLink();
        File file = new File(fileUrl);
        String extension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
        String name = eventDefinition.getEvent_name().replace(" ", "-");
        String namegsxsax = name + "_" + System.currentTimeMillis() + extension;
        file_Division.append(namegsxsax + ",");
        namegsxsax = "/images/" + namegsxsax;
        final_Division = file_Division.substring(0, file_Division.length() - 1);
        Log.e("#DEBUG", "Filetype: " + extension);
        Log.e("#DEBUG", "FileName: " + final_Division);
        TransferObserver transferObserver = transferUtility.upload("parkezly-images", namegsxsax, file);
        transferObserverListener(transferObserver);
    }

    public void transferObserverListener(final TransferObserver transferObserver) {
        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                Log.e("#DEBUG", "    transferObserverListener:   onStateChanged:  " + state.name());
                if (state.name().equals("COMPLETED")) {
                    if (fileUploads.size() != 0) {
                        Log.e("#DEBUG", "final_DivisionImage Name: " + final_Division);
                        if (fileUploads.get(0).getType().equals("Logo")) {
                            eventDefinition.setEvent_logo("https://s3.amazonaws.com/parkezly-images//images/" + final_Division);
                        } else if (fileUploads.get(0).getType().equals("Image")) {
                            eventImageUploads.add("https://s3.amazonaws.com/parkezly-images//images/" + final_Division);
                        } else if (fileUploads.get(0).getType().equals("pdf")) {
                            mediaDefinitionArray.get(fileUploads.get(0).getPos()).setPdfFilePath("https://s3.amazonaws.com/parkezly-images//images/" + final_Division);
                        } else if (fileUploads.get(0).getType().equals("ppt")) {
                            mediaDefinitionArray.get(fileUploads.get(0).getPos()).setPptFilePath("https://s3.amazonaws.com/parkezly-images//images/" + final_Division);
                        } else if (fileUploads.get(0).getType().equals("file")) {
                            mediaDefinitionArray.get(fileUploads.get(0).getPos()).setFilePath("https://s3.amazonaws.com/parkezly-images//images/" + final_Division);
                        } else if (fileUploads.get(0).getType().equals("Audio") || fileUploads.get(0).getType().equals("Video")) {
                            mediaDefinitionArray.get(fileUploads.get(0).getPos()).setAudioVideoPath("https://s3.amazonaws.com/parkezly-images//images/" + final_Division);
                        }
                    }
                    if (fileUploads.size() > 0) {
                        fileUploads.remove(0);
                        if (fileUploads.size() == 0) {
                            eventDefinition.setEvent_link_array(new Gson().toJson(mediaDefinitionArray));
                            Log.e("#DEBUG", "get Progress: " + percentage);
                            if (percentage == 100) {
                                progressDialog.dismiss();
                                Log.e("#DEBUG", "updateEventDetails().execute() ");
                                new updateEventDetails().execute();
                            }
                        } else {
                            upload_file();
                        }
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                try {
                    percentage = (int) (bytesCurrent / bytesTotal * 100);
                    Log.e("#DEBUG ", " : " + percentage);
                    if (percentage == 100) {
                        Log.e("#DEBUG ", " Dismis Dialog : ");
                        progressDialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", "error");
            }

        });
    }

    public class updateEventDetails extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String exit_status = "_table/event_definitions";
        JSONObject json, json1;
        String re_id;
        String id;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues = new HashMap<String, Object>();

            if (isEdit && !isRepeat) {
                jsonValues.put("id", eventDefinition.getId());
                jsonValues.put("year", eventDefinition.getYear());
            } else {
                jsonValues.put("date_time", DateTimeUtils.getSqlFormatDate(Calendar.getInstance().getTime()));
                jsonValues.put("year", DateTimeUtils.getSqlFormatDate(Calendar.getInstance().getTime()));
            }

            if (eventDefinition.getEvent_parking_fee() != null) {
                jsonValues.put("event_parking_fee", eventDefinition.getEvent_parking_fee());
                jsonValues.put("event_parking_fee_duration", eventDefinition.getEvent_parking_fee_duration());

            }

            jsonValues.put("manager_type", eventDefinition.getManager_type());
            jsonValues.put("manager_type_id", eventDefinition.getManager_type_id());
            jsonValues.put("twp_id", eventDefinition.getTwp_id());
            jsonValues.put("township_code", eventDefinition.getTownship_code());
            jsonValues.put("township_name", eventDefinition.getTownship_name());
            jsonValues.put("company_id", eventDefinition.getCompany_id());
            jsonValues.put("company_code", eventDefinition.getCompany_code());
            jsonValues.put("company_name", eventDefinition.getCompany_name());
            jsonValues.put("location_address", eventDefinition.getLocation_address());
            jsonValues.put("location_lat_lng", eventDefinition.getLocation_lat_lng());
            jsonValues.put("covered_locations", eventDefinition.getCovered_locations());
            jsonValues.put("lat", eventDefinition.getLat());
            jsonValues.put("lng", eventDefinition.getLng());

            jsonValues.put("event_category_type_id", eventDefinition.getEvent_category_type_id());
            jsonValues.put("event_category_type", eventDefinition.getEvent_category_type());
            jsonValues.put("event_type", eventDefinition.getEvent_type());
            jsonValues.put("event_name", eventDefinition.getEvent_name());
            jsonValues.put("event_short_description", eventDefinition.getEvent_short_description());
            jsonValues.put("event_long_description", eventDefinition.getEvent_long_description());
            jsonValues.put("event_link_on_web", eventDefinition.getEvent_link_on_web());
            jsonValues.put("event_link_twitter", eventDefinition.getEvent_link_twitter());
            jsonValues.put("event_link_whatsapp", eventDefinition.getEvent_link_whatsapp());
            jsonValues.put("event_link_other_media", eventDefinition.getEvent_link_other_media());
            jsonValues.put("event_link_ytube", eventDefinition.getEvent_link_ytube());
            jsonValues.put("event_link_zoom", eventDefinition.getEvent_link_zoom());
            jsonValues.put("event_link_googlemeet", eventDefinition.getEvent_link_googlemeet());
            jsonValues.put("event_link_googleclassroom", eventDefinition.getEvent_link_googleclassroom());
            jsonValues.put("event_link_facebook", eventDefinition.getEvent_link_facebook());

            jsonValues.put("company_logo", eventDefinition.getCompany_logo());
            jsonValues.put("event_logo", eventDefinition.getEvent_logo());
            jsonValues.put("event_image1", eventDefinition.getEvent_image1());
            jsonValues.put("event_image2", eventDefinition.getEvent_image2());
            jsonValues.put("event_address", eventDefinition.getEvent_address());
            jsonValues.put("requirements", eventDefinition.getRequirements());
            jsonValues.put("cost", eventDefinition.getCost());
            if (eventDefinition.getEvent_begins_date_time() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_begins_date_time())) {
                jsonValues.put("event_begins_date_time", DateTimeUtils.localToGMT(eventDefinition.getEvent_begins_date_time()));
            }
            if (eventDefinition.getEvent_ends_date_time() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_ends_date_time())) {
                jsonValues.put("event_ends_date_time", DateTimeUtils.localToGMT(eventDefinition.getEvent_ends_date_time()));
                jsonValues.put("expires_by", DateTimeUtils.localToGMT(eventDefinition.getEvent_ends_date_time()));
            }
            if (eventDefinition.getEvent_parking_begins_date_time() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_parking_begins_date_time())) {
                jsonValues.put("event_parking_begins_date_time", DateTimeUtils.localToGMT(eventDefinition.getEvent_parking_begins_date_time()));
            }
            if (eventDefinition.getEvent_parking_ends_date_time() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_parking_ends_date_time())) {
                jsonValues.put("event_parking_ends_date_time", DateTimeUtils.localToGMT(eventDefinition.getEvent_parking_ends_date_time()));
            }
            jsonValues.put("event_multi_dates", eventDefinition.getEvent_multi_dates());
            if (eventDefinition.getEvent_parking_timings() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_parking_timings())) {
                jsonValues.put("event_parking_timings", eventDefinition.getEvent_parking_timings());
            }
            jsonValues.put("event_event_timings", eventDefinition.getEvent_event_timings());
            jsonValues.put("regn_reqd", eventDefinition.isRegn_reqd());
            jsonValues.put("regn_reqd_for_parking", eventDefinition.isRegn_reqd_for_parking());
            jsonValues.put("renewable", eventDefinition.isRenewable());
            jsonValues.put("active", eventDefinition.isActive());
            jsonValues.put("event_logi_type", eventDefinition.getEvent_logi_type());

            jsonValues.put("regn_user_info_reqd", eventDefinition.isRegn_user_info_reqd());
            jsonValues.put("regn_addl_user_info_reqd", eventDefinition.isRegn_addl_user_info_reqd());


            jsonValues.put("event_multi_sessions", eventDefinition.isEvent_multi_sessions() ? 1 : 0);
            jsonValues.put("event_indiv_session_regn_allowed", eventDefinition.isEvent_indiv_session_regn_allowed() ? 1 : 0);
            jsonValues.put("event_indiv_session_regn_required", eventDefinition.isEvent_indiv_session_regn_required() ? 1 : 0);
            jsonValues.put("event_full_regn_required", eventDefinition.isEvent_full_regn_required() ? 1 : 0);

            if (eventDefinition.getEvent_full_regn_fee() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_full_regn_fee())) {
                jsonValues.put("event_full_regn_fee", eventDefinition.getEvent_full_regn_fee());
            }

            if (eventDefinition.getEvent_full_youth_fee() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_full_youth_fee())) {
                jsonValues.put("event_full_youth_fee", eventDefinition.getEvent_full_youth_fee());
            }

            if (eventDefinition.getEvent_full_child_fee() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_full_child_fee())) {
                jsonValues.put("event_full_child_fee", eventDefinition.getEvent_full_child_fee());
            }

            if (eventDefinition.getEvent_full_student_fee() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_full_student_fee())) {
                jsonValues.put("event_full_student_fee", eventDefinition.getEvent_full_student_fee());
            }

            if (eventDefinition.getEvent_full_minister_fee() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_full_minister_fee())) {
                jsonValues.put("event_full_minister_fee", eventDefinition.getEvent_full_minister_fee());
            }

            if (eventDefinition.getEvent_full_clergy_fee() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_full_clergy_fee())) {
                jsonValues.put("event_full_clergy_fee", eventDefinition.getEvent_full_clergy_fee());
            }

            if (eventDefinition.getEvent_full_promo_fee() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_full_promo_fee())) {
                jsonValues.put("event_full_promo_fee", eventDefinition.getEvent_full_promo_fee());
            }

            if (eventDefinition.getEvent_full_senior_fee() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_full_senior_fee())) {
                jsonValues.put("event_full_senior_fee", eventDefinition.getEvent_full_senior_fee());
            }

            if (eventDefinition.getEvent_full_staff_fee() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_full_staff_fee())) {
                jsonValues.put("event_full_staff_fee", eventDefinition.getEvent_full_staff_fee());
            }

            jsonValues.put("free_event", eventDefinition.isFree_event() ? 1 : 0);
            jsonValues.put("free_event_parking", eventDefinition.isFree_event_parking() ? 1 : 0);

            if (eventDefinition.getEvent_regn_limit() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_regn_limit())) {
                jsonValues.put("event_regn_limit", eventDefinition.getEvent_regn_limit());
            }
            jsonValues.put("parking_reservation_limit", eventDefinition.getParking_reservation_limit());
            jsonValues.put("web_event_full_regn_fee", eventDefinition.getWeb_event_full_regn_fee());
            jsonValues.put("web_event_regn_limit", eventDefinition.getWeb_event_regn_limit());
            jsonValues.put("web_event_location_to_show", eventDefinition.isWeb_event_location_to_show() ? 1 : 0);
            jsonValues.put("free_web_event", eventDefinition.isFree_web_event());
            jsonValues.put("local_timezone", selectedTimeZone);
            jsonValues.put("local_time_event_start", eventDefinition.getEvent_begins_date_time());
            jsonValues.put("is_parking_allowed", eventDefinition.isParking_allowed() ? 1 : 0);
            jsonValues.put("event_blob_image", new Gson().toJson(eventImageUploads));
            jsonValues.put("free_local_event", eventDefinition.isFree_local_event() ? 1 : 0);
            jsonValues.put("reqd_local_event_regn", eventDefinition.isReqd_local_event_regn() ? 1 : 0);
            jsonValues.put("reqd_web_event_regn", eventDefinition.isReqd_web_event_regn() ? 1 : 0);
            jsonValues.put("event_led_by", eventDefinition.getEvent_led_by());
            jsonValues.put("event_leaders_bio", eventDefinition.getEvent_leaders_bio());

            jsonValues.put("event_public", eventDefinition.isEvent_public() ? 1 : 0);
            jsonValues.put("event_unlisted", eventDefinition.isEvent_unlisted() ? 1 : 0);
            jsonValues.put("event_tp_sharable", eventDefinition.isEvent_tp_sharable() ? 1 : 0);
            jsonValues.put("event_invite_only", eventDefinition.isEvent_invite_only() ? 1 : 0);
            jsonValues.put("event_regn_invite_only", eventDefinition.isEvent_regn_invite_only() ? 1 : 0);
            jsonValues.put("show_live_regd_only", eventDefinition.isShow_live_regd_only() ? 1 : 0);
            jsonValues.put("show_live_invite_only", eventDefinition.isShow_live_invite_only() ? 1 : 0);
            jsonValues.put("show_past_regd_only", eventDefinition.isShow_past_regd_only() ? 1 : 0);
            jsonValues.put("show_past_invite_only", eventDefinition.isShow_past_invite_only() ? 1 : 0);
            jsonValues.put("geo_restrict_countries", eventDefinition.isGeo_restrict_countries() ? 1 : 0);
            jsonValues.put("geo_allow_countries", eventDefinition.isGeo_allow_countres() ? 1 : 0);
            jsonValues.put("geo_restrict_states", eventDefinition.isGeo_restrict_states() ? 1 : 0);
            jsonValues.put("geo_allow_states", eventDefinition.isGeo_allow_states() ? 1 : 0);
            jsonValues.put("geo_restrict_lat_lng", eventDefinition.isGeo_restrict_lat_lng() ? 1 : 0);
            jsonValues.put("geo_allow_lat_lng", eventDefinition.isGeo_allow_lat_lng() ? 1 : 0);

            jsonValues.put("allow_audio_conf", eventDefinition.isAllow_audio_conf());
            jsonValues.put("allow_audio_condition_no", eventDefinition.isAllow_audio_condition_no());
            jsonValues.put("allow_audio_conf_rec", eventDefinition.isAllow_audio_conf_rec());
            jsonValues.put("allow_audio_rec_condition", eventDefinition.isAllow_audio_rec_condition());
            jsonValues.put("allow_video_conf", eventDefinition.isAllow_video_conf());
            jsonValues.put("allow_video_condition_no", eventDefinition.isAllow_video_condition_no());
            jsonValues.put("allow_video_conf_rec", eventDefinition.isAllow_video_conf_rec());
            jsonValues.put("allow_video_rec_condition", eventDefinition.isAllow_video_rec_condition());
            jsonValues.put("allow_livechat", eventDefinition.isAllow_livechat());
            jsonValues.put("allow_livechat_condition_no", eventDefinition.isAllow_livechat_condition_no());
            jsonValues.put("archive_access", eventDefinition.isArchive_access());
            if (!TextUtils.isEmpty(eventDefinition.getEvent_link_array())) {
                jsonValues.put("event_link_array", eventDefinition.getEvent_link_array());
            }

            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpResponse response = null;
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (isEdit && !isRepeat) {
                HttpPut post = new HttpPut(url);
                post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
                post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
                post.setEntity(entity);
                Log.e("#DEBUG", "  HttpPut:  updateURL:  " + url);
                Log.e("#DEBUG", "   HttpPut:  update Params: " + String.valueOf(json));
                try {
                    response = client.execute(post);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                HttpPost post = new HttpPost(url);
                post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
                post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
                post.setEntity(entity);
                Log.e("#DEBUG", "  HttpPost:  updateURL:  " + url);
                Log.e("#DEBUG", "   HttpPost:  update Params: " + String.valueOf(json));
                try {
                    response = client.execute(post);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   Add/Edit Event:  Response:  " + responseStr);
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.e("#DEBUG", "  Add/Update EventDetails: ResponseID: " + re_id);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.hide();
            if (getActivity() != null && json1 != null) {
                if (re_id != null && !re_id.equals("null")) {
                    if (isEdit) {
                        isUpdated = true;
                        if (isRepeat) {
                            new updateEventId().execute(re_id);
                        } else {
                            Toast.makeText(getActivity(), "Updated!", Toast.LENGTH_SHORT).show();
                        }
                        getActivity().onBackPressed();
                    } else {
                        new updateEventId().execute(re_id);
                    }
                }
                super.onPostExecute(s);
            }
        }
    }

    public class updateEventId extends AsyncTask<String, String, String> {
        String exit_status = "_table/event_definitions";
        JSONObject json, json1;
        String re_id;
        String id;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues = new HashMap<String, Object>();


            jsonValues.put("id", params[0]);
            jsonValues.put("event_id", params[0]);

            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpResponse response = null;
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            post.setEntity(entity);
            Log.e("#DEBUG", "  updateEventId:  updateURL:  " + url);
            Log.e("#DEBUG", "   updateEventId:  update Params: " + String.valueOf(json));
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.e("#DEBUG", "  updateEventId:  update: ResponseID: " + re_id);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.hide();
            if (getActivity() != null && json1 != null) {
                if (re_id != null && !re_id.equals("null")) {
                    isUpdated = true;
                    if (isRepeat) {
                        Toast.makeText(getActivity(), "Repeat Event Added Successfully!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "Added!", Toast.LENGTH_SHORT).show();
                    }
                    getActivity().onBackPressed();
                }
                super.onPostExecute(s);
            }
        }
    }

    private boolean isValidate() {
        if (eventDefinition.isParking_allowed()) {
            if (TextUtils.isEmpty(binding.etParkingBeginsTime.getText())) {
                Toast.makeText(getActivity(), "Please select Parking Begins Time!", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (TextUtils.isEmpty(binding.etParkingEndTime.getText())) {
                Toast.makeText(getActivity(), "Please select Parking End Time!", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (!isEdit) {
                if (TextUtils.isEmpty(binding.etParkingTiming.getText())) {
                    Toast.makeText(getActivity(), "Please select Parking Timing!", Toast.LENGTH_SHORT).show();
                    return false;
                }
            } else if (isDateChanged) {
                if (TextUtils.isEmpty(binding.etParkingTiming.getText())) {
                    Toast.makeText(getActivity(), "Please select Parking Timing!", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        }
        return true;
    }

    private void showDialogSelectEventMultiDates() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_multi_date_select, null);
        builder.setView(dialogView);
        TextView tvTitle = dialogView.findViewById(R.id.tvTitle);
        tvTitle.setText("Select Parking Dates and Timing");
        RecyclerView rvMultiDates = dialogView.findViewById(R.id.rvMultiDates);
        MultiDatesAdapter multiDatesAdapter = new MultiDatesAdapter();
        rvMultiDates.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMultiDates.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        rvMultiDates.setAdapter(multiDatesAdapter);
        Button btnDone = dialogView.findViewById(R.id.btnDone);
        AlertDialog alertDialog = builder.create();

        btnDone.setOnClickListener(v -> {
            alertDialog.dismiss();
            Log.e("#DEBUG", "   MultiDates:  " + new Gson().toJson(multiDates));
            eventDates = new StringBuilder();
            eventTiming = new StringBuilder();
            StringBuilder displayText = new StringBuilder();
            for (int i = 0; i < multiDates.size(); i++) {
                if (multiDates.get(i).isSelected()) {
                    if (!TextUtils.isEmpty(eventDates.toString())) {
                        eventDates.append(", ");
                    }
                    eventDates.append(DateTimeUtils.localToGMTYYYYMMDDHHMMSS(multiDates.get(i).getDate()));
                    displayText.append(multiDates.get(i).getDate().substring(0, 10));

                    if (!TextUtils.isEmpty(eventTiming.toString())) {
                        eventTiming.append(", ");
                    }
                    eventTiming.append(DateTimeUtils.localToGMT_DD_HH_AM_PM(multiDates.get(i).getDate().substring(0, 10) + " "
                            + multiDates.get(i).getStartHour() + ":" + multiDates.get(i).getStartMinute() + " " + multiDates.get(i).getStartAMPM()));
                    eventTiming.append(" - ");
                    eventTiming.append(DateTimeUtils.localToGMT_DD_HH_AM_PM(multiDates.get(i).getDate().substring(0, 10) + " "
                            + multiDates.get(i).getEndHour() + ":" + multiDates.get(i).getEndMinute() + " " + multiDates.get(i).getEndAMPM()));

                    displayText.append("    (");
                    displayText.append(multiDates.get(i).getStartHour() + ":" + multiDates.get(i).getStartMinute() + " " + multiDates.get(i).getStartAMPM());
                    displayText.append(" - ");
                    displayText.append(multiDates.get(i).getEndHour() + ":" + multiDates.get(i).getEndMinute() + " " + multiDates.get(i).getEndAMPM());
                    displayText.append(")");
                    if (i == multiDates.size() - 1) {

                    } else {
                        displayText.append("\n");
                    }
                }
            }
            binding.etParkingTiming.setText(displayText.toString());

            String[] strings = eventTiming.toString().split(", ");
            JSONArray jsonArray = new JSONArray();
            for (String string : strings) {
                JSONArray jsonArray1 = new JSONArray();
                jsonArray1.put(string);
                jsonArray.put(jsonArray1);
            }
            eventDefinition.setEvent_parking_timings(jsonArray.toString());

//            binding.etEventDates.setText(eventDates.toString());
        });


        alertDialog.show();
    }

    private void showDialogParkingEntTime() {
        DatePickerDialog entryDatePickerDialog = new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
            final Calendar calendar1 = Calendar.getInstance();
            calendar1.set(Calendar.YEAR, i);
            calendar1.set(Calendar.MONTH, i1);
            calendar1.set(Calendar.DAY_OF_MONTH, i2);


            new TimePickerDialog(getActivity(), (timePicker, i3, i11) -> {

                Calendar datetime = Calendar.getInstance();
                Calendar calendar = Calendar.getInstance();
                datetime.set(Calendar.YEAR, i);
                datetime.set(Calendar.MONTH, i1);
                datetime.set(Calendar.DAY_OF_MONTH, i2);
                datetime.set(Calendar.HOUR_OF_DAY, i3);
                datetime.set(Calendar.MINUTE, i11);
                if (datetime.getTimeInMillis() >= calendar.getTimeInMillis()) {
                    calendar1.set(Calendar.HOUR_OF_DAY, i3);
                    calendar1.set(Calendar.MINUTE, i11);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault());
                    binding.etParkingEndTime.setText(dateFormat1.format(new Date(calendar1.getTimeInMillis())));
                    eventDefinition.setEvent_parking_ends_date_time(dateFormat.format(new Date(calendar1.getTimeInMillis())));
                    selectedEndDateForMultiDate = dateFormat.format(new Date(calendar1.getTimeInMillis()));
                    isDateChanged = true;
                    binding.etParkingTiming.setText("");
                } else {
                    Toast.makeText(getActivity(), "Please select future date!", Toast.LENGTH_LONG).show();
                }

            }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE),
                    false).show();
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        entryDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        entryDatePickerDialog.show();
    }

    private void showDialogParkingBeginsTime() {
        DatePickerDialog entryDatePickerDialog = new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
            final Calendar calendar1 = Calendar.getInstance();
            calendar1.set(Calendar.YEAR, i);
            calendar1.set(Calendar.MONTH, i1);
            calendar1.set(Calendar.DAY_OF_MONTH, i2);


            new TimePickerDialog(getActivity(), (timePicker, i3, i11) -> {

                Calendar datetime = Calendar.getInstance();
                Calendar calendar = Calendar.getInstance();
                datetime.set(Calendar.YEAR, i);
                datetime.set(Calendar.MONTH, i1);
                datetime.set(Calendar.DAY_OF_MONTH, i2);
                datetime.set(Calendar.HOUR_OF_DAY, i3);
                datetime.set(Calendar.MINUTE, i11);
                if (datetime.getTimeInMillis() >= calendar.getTimeInMillis()) {
                    calendar1.set(Calendar.HOUR_OF_DAY, i3);
                    calendar1.set(Calendar.MINUTE, i11);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault());
                    binding.etParkingBeginsTime.setText(dateFormat1.format(new Date(calendar1.getTimeInMillis())));
                    eventDefinition.setEvent_parking_begins_date_time(dateFormat.format(new Date(calendar1.getTimeInMillis())));
                    selectedStartDateForMultiDate = dateFormat.format(new Date(calendar1.getTimeInMillis()));
                    isDateChanged = true;
                    binding.etParkingEndTime.setText("");
                    binding.etParkingTiming.setText("");
                } else {
                    Toast.makeText(getActivity(), "Please select future date!", Toast.LENGTH_LONG).show();
                }

            }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE),
                    false).show();
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        entryDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        entryDatePickerDialog.show();
    }

    @Override
    protected void initViews(View v) {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");

    }

    private class MultiDatesAdapter extends RecyclerView.Adapter<MultiDatesAdapter.DatesHolder> {
        ArrayList<String> startHours = new ArrayList<>();
        ArrayList<String> endHour = new ArrayList<>();
        ArrayList<String> startMinutes = new ArrayList<>();
        ArrayList<String> endMinutes = new ArrayList<>();
        ArrayList<String> amPm = new ArrayList<>();

        @NonNull
        @Override
        public MultiDatesAdapter.DatesHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i1) {
            startHours.clear();
            startHours.addAll(Arrays.asList(getResources().getStringArray(R.array.time_hours)));
            endHour.clear();
            endHour.addAll(Arrays.asList(getResources().getStringArray(R.array.time_hours)));
            startMinutes.clear();
            startMinutes.addAll(Arrays.asList(getResources().getStringArray(R.array.time_minutes)));
            endMinutes.clear();
            endMinutes.addAll(Arrays.asList(getResources().getStringArray(R.array.time_minutes)));

            amPm.clear();
            amPm.addAll(Arrays.asList(getResources().getStringArray(R.array.time_am_pm)));

            return new DatesHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_multi_date_time, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MultiDatesAdapter.DatesHolder holder, int i) {
            MultiDate multiDate = multiDates.get(i);
            if (multiDate != null) {
                holder.tvDate.setText(multiDate.getDate().substring(0, 10));
                holder.cbSelected.setChecked(multiDate.isSelected());

                if (startHours.indexOf(DateTimeUtils.getHH(multiDate.getStartTime())) != -1) {
                    holder.spinnerStartHour.setSelection(startHours.indexOf(DateTimeUtils.getHH(multiDate.getStartTime())));
                }

                if (startMinutes.indexOf(DateTimeUtils.getMM(multiDate.getStartTime())) != -1) {
                    holder.spinnerStartMinute.setSelection(startMinutes.indexOf(DateTimeUtils.getMM(multiDate.getStartTime())));
                }

                if (endHour.indexOf(DateTimeUtils.getHH(multiDate.getEndTime())) != -1) {
                    holder.spinnerEndHour.setSelection(endHour.indexOf(DateTimeUtils.getHH(multiDate.getEndTime())));
                }

                if (endMinutes.indexOf(DateTimeUtils.getMM(multiDate.getEndTime())) != -1) {
                    holder.spinnerEndMinute.setSelection(endMinutes.indexOf(DateTimeUtils.getMM(multiDate.getEndTime())));
                }

                if (amPm.indexOf(DateTimeUtils.getAMPM(multiDate.getStartTime())) != -1) {
                    holder.spinnerStartAMPM.setSelection(amPm.indexOf(DateTimeUtils.getAMPM(multiDate.getStartTime())));
                }

                if (amPm.indexOf(DateTimeUtils.getAMPM(multiDate.getEndTime())) != -1) {
                    holder.spinnerEndAMPM.setSelection(amPm.indexOf(DateTimeUtils.getAMPM(multiDate.getEndTime())));
                }

                holder.spinnerStartHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        multiDates.get(i).setStartHour(startHours.get(position));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                holder.spinnerEndHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        multiDates.get(i).setEndHour(endHour.get(position));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                holder.spinnerStartMinute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        multiDates.get(i).setStartMinute(startMinutes.get(position));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                holder.spinnerEndMinute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        multiDates.get(i).setEndMinute(endMinutes.get(position));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                holder.spinnerStartAMPM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        multiDates.get(i).setStartAMPM(amPm.get(position));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                holder.spinnerEndAMPM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        multiDates.get(i).setEndAMPM(amPm.get(position));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                holder.cbSelected.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    multiDates.get(i).setSelected(isChecked);
                });
            }
        }

        @Override
        public int getItemCount() {
            return multiDates.size();
        }

        class DatesHolder extends RecyclerView.ViewHolder {
            AppCompatCheckBox cbSelected;
            TextView tvDate;
            AppCompatSpinner spinnerStartHour, spinnerStartMinute, spinnerEndHour, spinnerEndMinute,
                    spinnerStartAMPM, spinnerEndAMPM;

            DatesHolder(@NonNull View itemView) {
                super(itemView);

                cbSelected = itemView.findViewById(R.id.cbSelected);
                tvDate = itemView.findViewById(R.id.tvDate);
                spinnerStartHour = itemView.findViewById(R.id.spinnerStartHour);
                spinnerEndHour = itemView.findViewById(R.id.spinnerEndHour);
                spinnerStartMinute = itemView.findViewById(R.id.spinnerStartMinute);
                spinnerEndMinute = itemView.findViewById(R.id.spinnerEndMinute);
                spinnerStartAMPM = itemView.findViewById(R.id.spinnerStartAMPM);
                spinnerEndAMPM = itemView.findViewById(R.id.spinnerEndAMPM);

                ArrayAdapter<String> timeStartHoursAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, startHours);
                spinnerStartHour.setAdapter(timeStartHoursAdapter);

                ArrayAdapter<String> timeStartMinuteAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, startMinutes);
                spinnerStartMinute.setAdapter(timeStartMinuteAdapter);

                ArrayAdapter<String> timeEndHourAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, endHour);
                spinnerEndHour.setAdapter(timeEndHourAdapter);

                ArrayAdapter<String> timeEndMinuteAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, endMinutes);
                spinnerEndMinute.setAdapter(timeEndMinuteAdapter);

                ArrayAdapter<String> amPmAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, amPm);
                spinnerStartAMPM.setAdapter(amPmAdapter);

                ArrayAdapter<String> endAmPmAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, amPm);
                spinnerEndAMPM.setAdapter(endAmPmAdapter);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("#DEBUG", "   Parking:  onResume");
        if (isUpdated) {
            if (getActivity() != null) getActivity().onBackPressed();
        }
    }
}
