package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.concurrent.NotThreadSafe;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class Fragment_permit_definitions_Three_Edit extends Fragment {

    ConnectionDetector cd;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar;
    View mView;
    String id, select_town, township_code, permit_name, permit_types, township_name, manager_type;
    ListView listofvehicleno, lv_TownshipAdd;
    ArrayList<item> township_array_list, township_array;
    RelativeLayout rl_TownshipAdd;
    CustomAdapterTwonship customAdapterTwonship;
    CustomAdapter customAdapter;

    ListView lv_Township;
    TextView txt_Add, txt_Next;
    ImageView img_Close;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_permit_definitions_three_edit, container, false);

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        id = logindeatl.getString("id", "null");

        if (getArguments() != null) {
            select_town = String.valueOf(getArguments().getString("manager_id"));
            township_code = String.valueOf(getArguments().getString("township_code"));
            permit_name = String.valueOf(getArguments().getString("permit_name"));
            township_name = String.valueOf(getArguments().getString("township_name"));
            permit_types = String.valueOf(getArguments().getString("permit_types"));
            manager_type = String.valueOf(getArguments().getString("manager_type"));
        }

        ints();

        try {
            new getPermitData().execute();
            if (manager_type.equalsIgnoreCase("Township")) {
                new getTownship_parking_partners("township_parking_partners").execute();
            } else if (manager_type.equalsIgnoreCase("Commercial")) {
                new getTownship_parking_partners("google_parking_partners").execute();
            } else if (manager_type.equalsIgnoreCase("Private")) {
                new getTownship_parking_partners("other_parking_partners").execute();
            } else {
                new getTownship_parking_partners("other_parking_partners").execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mView;
    }

    public void ints() {
        rl_TownshipAdd = (RelativeLayout) mView.findViewById(R.id.rl_TownshipAdd);
        lv_TownshipAdd = (ListView) mView.findViewById(R.id.lv_TownshipAdd);
        img_Close = (ImageView) mView.findViewById(R.id.img_Close);

        txt_Add = (TextView) mView.findViewById(R.id.txt_Add);
        txt_Next = (TextView) mView.findViewById(R.id.txt_Next);
        lv_Township = (ListView) mView.findViewById(R.id.lv_Township);
        progressBar = (ProgressBar) mView.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) mView.findViewById(R.id.rl_progressbar);
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        onClicks();
    }

    public void onClicks() {

        txt_Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lv_Township.setVisibility(View.GONE);
                rl_TownshipAdd.setVisibility(View.VISIBLE);
                lv_TownshipAdd.setVisibility(View.VISIBLE);
                if (township_array.size() > 0) {
                    customAdapter = new CustomAdapter(getActivity(), township_array, getResources());
                    lv_TownshipAdd.setAdapter(customAdapter);
                } else {
                    //txt_No_Record.setVisibility(View.VISIBLE);
                    lv_TownshipAdd.setVisibility(View.GONE);
                }
            }
        });

        img_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lv_Township.setVisibility(View.VISIBLE);
                rl_TownshipAdd.setVisibility(View.GONE);
                lv_TownshipAdd.setVisibility(View.GONE);
            }
        });

        txt_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (township_array_list.size() > 0) {
                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Fragment_permit_definitions_Four_Edit pay = new Fragment_permit_definitions_Four_Edit();
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                } else {
                    Toast.makeText(getContext(), "Please Add location", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public class getPermitData extends AsyncTask<String, String, String> {

        String vehiclenourl = "_table/locations_permit?filter=(permit_id%3D'" + URLEncoder.encode(select_town, "utf-8") + "')";

        JSONObject json = null;

        public getPermitData() throws UnsupportedEncodingException {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            township_array_list = new ArrayList<item>();
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("get_vehicle_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;

            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);
                    Log.e("rerpones", String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setId(c.getString("id"));
                        item1.setPermit_type(c.getString("permit_type"));
                        item1.setPermit_id(c.getString("permit_id"));
                        item1.setPermit_name(c.getString("permit_name"));
                        item1.setLocation_id(c.getString("location_id"));
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setLocation_name(c.getString("location_name"));
                        item1.setAddress(c.getString("address"));
                        item1.setTownship_id(c.getString("township_id"));
                        item1.setTownship_code(c.getString("township_code"));
                        item1.setTownship_name(c.getString("township_name"));
                        item1.setManager_type(c.getString("manager_type"));
                        item1.setManager_type_id(c.getString("manager_type_id"));
                        item1.setIsslected(true);
                        township_array_list.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity != null) {
                    customAdapterTwonship = new CustomAdapterTwonship(getActivity(), township_array_list, getResources());
                    lv_Township.setAdapter(customAdapterTwonship);
                }
            }
        }
    }

    public class CustomAdapterTwonship extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdapterTwonship(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txt_Location_id, txt_location_code, txt_location_name, txt_township_name,
                    txt_Remove, txt_Added, txt_townshipCode;
            ImageView img_logo, img_selected;
            RelativeLayout rl_main;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_permit_definitions_edit, null);
                holder = new ViewHolder();

                holder.txt_Location_id = (TextView) vi.findViewById(R.id.txt_Location_id);
                holder.txt_location_code = (TextView) vi.findViewById(R.id.txt_location_code);
                holder.txt_townshipCode = (TextView) vi.findViewById(R.id.txt_township_code);

                holder.txt_Remove = (TextView) vi.findViewById(R.id.txt_Remove);
                holder.txt_Added = (TextView) vi.findViewById(R.id.txt_Added);
                holder.img_logo = (ImageView) vi.findViewById(R.id.img_logo);
                holder.rl_main = (RelativeLayout) vi.findViewById(R.id.rl_main);

                vi.setTag(holder);
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Picasso.with(context).load(R.mipmap.township_icon).into(holder.img_logo);

            holder.txt_Location_id.setText(data.get(position).getLocation_code());
            holder.txt_location_code.setText(data.get(position).getLocation_name());
            holder.txt_townshipCode.setText(data.get(position).getTownship_name());

            holder.rl_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            holder.txt_Added.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (data.get(position).getIsslected()) {
                        holder.img_selected.setVisibility(View.INVISIBLE);
                        township_array_list.get(position).setIsslected(false);
                    } else {
                        township_array_list.get(position).setIsslected(true);
                        holder.img_selected.setVisibility(View.VISIBLE);
                    }
                    customAdapterTwonship.notifyDataSetChanged();
                }
            });

            holder.txt_Remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle("Confirm Delete...");
                    alertDialog.setMessage("Are you sure you want delete this?");
                    alertDialog.setIcon(R.drawable.icon);

                    alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            new deleteTownship(data.get(position).getId()).execute();
                        }
                    });
                    alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    alertDialog.show();

                }
            });

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

    public class deleteTownship extends AsyncTask<String, String, String> {
        String locationurl;
        String id;
        String gf = "null";
        JSONObject json, json1;
        JSONArray array = null;

        public deleteTownship(String id) {
            locationurl = "_table/locations_permit";
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", id);
            JSONObject json1 = new JSONObject(jsonValues);
            Log.e("json1", json1.toString());
            String url = getString(R.string.api) + getString(R.string.povlive) + locationurl;
            Log.e("delete_uel", url);

            try {
                HttpEntity entity = new StringEntity(json1.toString());
                HttpClient httpClient = new DefaultHttpClient();
                HttpDeleteWithBody httpDeleteWithBody = new HttpDeleteWithBody(url);
                httpDeleteWithBody.setHeader("X-DreamFactory-Application-Name", "parkezly");
                httpDeleteWithBody.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
                httpDeleteWithBody.setEntity(entity);

                HttpResponse response = httpClient.execute(httpDeleteWithBody);
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        gf = c.getString("id");
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json != null) {
                Log.e("Json ", " : " + json.toString());
                for (int i = 0; i < township_array_list.size(); i++) {
                    if (township_array_list.get(i).getId().equalsIgnoreCase(gf)) {
                        township_array_list.remove(i);
                        break;
                    }
                }
                customAdapterTwonship.notifyDataSetChanged();
            }
            super.onPostExecute(s);
        }
    }

    @NotThreadSafe
    class HttpDeleteWithBody extends HttpEntityEnclosingRequestBase {
        public static final String METHOD_NAME = "DELETE";

        public String getMethod() {
            return METHOD_NAME;
        }

        public HttpDeleteWithBody(final String uri) {
            super();
            setURI(URI.create(uri));
        }

        public HttpDeleteWithBody(final URI uri) {
            super();
            setURI(uri);
        }

        public HttpDeleteWithBody() {
            super();
        }
    }

    public class getTownship_parking_partners extends AsyncTask<String, String, String> {

        String vehiclenourl = "_table/township_parking_partners?filter=(township_code%3D'" + URLEncoder.encode(township_code, "utf-8") + "')";

        JSONObject json = null;

        public getTownship_parking_partners(String api) throws UnsupportedEncodingException {
            vehiclenourl = "_table/" + api + "?filter=(township_code%3D'" + URLEncoder.encode(township_code, "utf-8") + "')";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            township_array = new ArrayList<item>();
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("get_vehicle_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;

            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);
                    Log.e("rerpones", String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setId(c.getString("id"));
                        item1.setActive(c.getString("active"));
                        item1.setAddress(c.getString("address"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setIn_effect(c.getString("in_effect"));
                        item1.setLat(c.getString("lat"));
                        item1.setLng(c.getString("lng"));
                        item1.setLots_avbl(c.getString("lots_avbl"));
                        item1.setLots_total(c.getString("lots_total"));
                        item1.setMarker_type(c.getString("marker_type"));
                        item1.setNo_parking_times(c.getString("no_parking_times"));
                        item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        item1.setOff_peak_ends(c.getString("off_peak_ends"));
                        item1.setOff_peak_starts(c.getString("off_peak_starts"));
                        item1.setParking_times(c.getString("parking_times"));
                        item1.setRenewable(c.getString("renewable"));
                        item1.setTitle(c.getString("title"));
                        item1.setUser_id(c.getString("user_id"));
                        item1.setWeekend_special_diff(c.getString("weekend_special_diff"));
                        item1.setLocation_place_id(c.getString("location_place_id"));
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setTownship_code(c.getString("township_code"));
                        item1.setIsKiosk(c.getString("isKiosk"));
                        item1.setManager_id(c.getString("manager_id"));
                        item1.setIsslected(false);
                        township_array.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity != null) {

                }
            }
        }
    }

    public class CustomAdapter extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdapter(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txt_Location_id, txt_location_code, txt_location_name, txt_township_name,
                    txt_APPLY, txt_townshipCode;
            ImageView img_logo, img_selected;
            RelativeLayout rl_main;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_permit_definitions, null);
                holder = new ViewHolder();

                holder.txt_Location_id = (TextView) vi.findViewById(R.id.txt_Location_id);
                holder.txt_location_code = (TextView) vi.findViewById(R.id.txt_location_code);
                holder.txt_location_name = (TextView) vi.findViewById(R.id.txt_location_name);
                holder.txt_township_name = (TextView) vi.findViewById(R.id.txt_township_name);
                holder.txt_townshipCode = (TextView) vi.findViewById(R.id.txt_townshipCode);
                holder.txt_APPLY = (TextView) vi.findViewById(R.id.txt_APPLY);
                holder.img_logo = (ImageView) vi.findViewById(R.id.img_logo);
                holder.img_selected = (ImageView) vi.findViewById(R.id.img_selected);
                holder.rl_main = (RelativeLayout) vi.findViewById(R.id.rl_main);

                vi.setTag(holder);
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Picasso.with(context).load(R.mipmap.township_icon).into(holder.img_logo);
            holder.txt_location_name.setSelected(true);

            holder.txt_Location_id.setText(data.get(position).getTitle());
            //holder.txt_location_code.setText(data.get(position).getLocation_code());
            holder.txt_location_name.setText(data.get(position).getAddress());
            holder.txt_township_name.setText(data.get(position).getTownship_name());
            //holder.txt_townshipCode.setText(data.get(position).getTownshipcode());

            if (data.get(position).getIsslected()) {
                holder.img_selected.setVisibility(View.VISIBLE);
            } else {
                holder.img_selected.setVisibility(View.INVISIBLE);
            }

            holder.rl_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.txt_APPLY.performClick();
                }
            });

            holder.txt_APPLY.setText("Add");
            holder.txt_APPLY.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle("Confirm Add...");
                    alertDialog.setMessage("Are you sure you want Add this?");
                    alertDialog.setIcon(R.drawable.icon);

                    alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            new AddLocations_permit(data.get(position)).execute();
                            lv_Township.setVisibility(View.VISIBLE);
                            rl_TownshipAdd.setVisibility(View.GONE);
                            lv_TownshipAdd.setVisibility(View.GONE);
                        }
                    });

                    alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    alertDialog.show();
                }
            });

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

    public class AddLocations_permit extends AsyncTask<String, String, String> {
        JSONObject json;
        String responseStr = null;
        String managedlosturl = "_table/locations_permit";
        Date current;
        item mitem;

        public AddLocations_permit(item mItem) {
            mitem = mItem;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            String currentdate = sdf.format(new Date());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            try {
                current = format.parse(currentdate);
                Log.e("current_date", String.valueOf(current));
                System.out.println(current);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("date_time", current);
            jsonValues1.put("permit_type", permit_types);
            jsonValues1.put("permit_id", select_town);
            jsonValues1.put("permit_name", permit_name);
            jsonValues1.put("location_id", mitem.getId());
            jsonValues1.put("location_code", mitem.getLocation_code());
            jsonValues1.put("location_name", mitem.getAddress());
            jsonValues1.put("address", mitem.getAddress());
            jsonValues1.put("township_id", mitem.getTownship_id());
            jsonValues1.put("township_code", mitem.getTownship_code());
            jsonValues1.put("township_name", township_name);
            jsonValues1.put("manager_type", mitem.getManager_type());
            jsonValues1.put("manager_type_id", mitem.getManager_type_id());

            JSONObject json1 = new JSONObject(jsonValues1);
            Log.e("json1", " : " + json1.toString());
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("responseStr ", " : " + responseStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return responseStr;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && responseStr != null) {
                boolean errors = responseStr.indexOf("error") > 0;
                if (errors) {
                    Toast.makeText(getContext(), "Already exists...", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getContext(), "Successfully added", Toast.LENGTH_SHORT).show();
                    try {
                        new getPermitData().execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
