package com.softmeasures.eventezlyadminplus.frament;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.softmeasures.eventezlyadminplus.Modules.DirectionFinder;
import com.softmeasures.eventezlyadminplus.Modules.DirectionFinderListener;
import com.softmeasures.eventezlyadminplus.Modules.Route;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.softmeasures.eventezlyadminplus.frament.f_direction.add_more_destination_array;
import static com.softmeasures.eventezlyadminplus.frament.f_direction.is_click_atob;
import static com.softmeasures.eventezlyadminplus.frament.f_direction.is_click_close_up;
import static com.softmeasures.eventezlyadminplus.frament.f_direction.is_delays;


public class show_direction extends Fragment implements OnMapReadyCallback, DirectionFinderListener {
    double latitude = 0.0, longitude = 0.0;
    ConnectionDetector cd;
    GoogleMap googleMap;
    MarkerOptions options;
    ProgressBar progressBar;
    ArrayList<item> step = new ArrayList<>();
    TextView txt_total_dis, txt_total_dur, txt_origin_address, txt_destion_address;
    ListView list_step;
    LatLng startPosition;
    LatLng destinationPosition;
    double totaldistance = 0.0, totalduration = 0.0, avgsppedinmilesperhor = 0.0;
    static ImageView img_refresh, img_zoom;
    RelativeLayout rl_progressbar;
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    String click_on_move = "";
    static LinearLayout ll_detail;
    static RelativeLayout rl_map;
    Marker marker_temp;

    List<LatLng> lat_lang = new ArrayList<>();
    String startaddress, endaddress;
    List<Route> temp_route = new ArrayList<>();
    ArrayList<item> temp_destination_array = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_show_direction, container, false);
        SharedPreferences ss = getActivity().getSharedPreferences("show_direction", Context.MODE_PRIVATE);
        final String cu = ss.getString("current", "null");
        final String en = ss.getString("des", "null");
        final String mode = ss.getString("mode", "driving");
        final String curnnt = ss.getString("current_lat", "0.0");
        final String endloc = ss.getString("des_lat", "0.0");
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        SharedPreferences sd = getActivity().getSharedPreferences("directionback", Context.MODE_PRIVATE);
        SharedPreferences.Editor d = sd.edit();
        d.putString("directionback", "yes");
        d.commit();
        list_step = (ListView) view.findViewById(R.id.list_location);
        txt_total_dis = (TextView) view.findViewById(R.id.txt_total_distances);
        txt_total_dur = (TextView) view.findViewById(R.id.txt_total_duration1);
        txt_origin_address = (TextView) view.findViewById(R.id.txt_origin_address);
        txt_destion_address = (TextView) view.findViewById(R.id.txt_destination_address);
        img_refresh = (ImageView) view.findViewById(R.id.img_refresh);
        img_zoom = (ImageView) view.findViewById(R.id.img_zoom);
        ll_detail = (LinearLayout) view.findViewById(R.id.ll_list_detail);
        rl_map = (RelativeLayout) view.findViewById(R.id.rl_map);
        cd = new ConnectionDetector(getActivity());


        RelativeLayout rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        if (cd.isConnectingToInternet()) {
            currentlocation();
            mapFragment = ((SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map));
            LatLng position = new LatLng(latitude, longitude);
            options = new MarkerOptions();
            options.position(position);
            SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            fm.getMapAsync(googleMap1 -> {
                googleMap = googleMap1;
                //oogleMap.addMarker(options);
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                }
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.setMyLocationEnabled(false);
                map = googleMap;
            });

        }
        if (!cu.equals("null") && !en.equals("null")) {
            String lar = curnnt.substring(0, curnnt.indexOf(","));
            String lat1 = curnnt.substring(curnnt.indexOf(",") + 1);
            String endlat = endloc.substring(0, endloc.indexOf(","));
            String endlat1 = endloc.substring(endloc.indexOf(",") + 1);
            startPosition = new LatLng(Double.parseDouble(lar), Double.parseDouble(lat1));
            destinationPosition = new LatLng(Double.parseDouble(endlat), Double.parseDouble(endlat1));
            if (add_more_destination_array.size() > 0) {
                double lat = Double.parseDouble(add_more_destination_array.get(add_more_destination_array.size() - 1).getLat());
                double lang = Double.parseDouble(add_more_destination_array.get(add_more_destination_array.size() - 1).getLang());
                destinationPosition = new LatLng(lat, lang);
                String address = add_more_destination_array.get(add_more_destination_array.size() - 1).getAddress();
                StringBuffer waypoint = new StringBuffer();


                item ii = new item();
                ii.setAddress(en);
                ii.setLat(endlat);
                ii.setLang(endlat1);
                temp_destination_array.add(ii);
                temp_destination_array.addAll(add_more_destination_array);

                for (int i = 0; i < temp_destination_array.size() - 1; i++) {
                    waypoint.append(temp_destination_array.get(i).getLat() + "," + temp_destination_array.get(i).getLang() + "|");
                }

                String finalwayout = method(waypoint.toString());
                sendRequest(cu, address, mode, finalwayout);
            } else {
                destinationPosition = new LatLng(Double.parseDouble(endlat), Double.parseDouble(endlat1));
                sendRequest(cu, en, mode);
            }
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            click_on_move = bundle.getString("click");
        }

        img_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleMap.clear();
                if (!cu.equals("null") && !en.equals("null")) {
                    String lar = curnnt.substring(0, curnnt.indexOf(","));
                    String lat1 = curnnt.substring(curnnt.indexOf(",") + 1);
                    String endlat = endloc.substring(0, endloc.indexOf(","));
                    String endlat1 = endloc.substring(endloc.indexOf(",") + 1);
                    startPosition = new LatLng(Double.parseDouble(lar), Double.parseDouble(lat1));
                    destinationPosition = new LatLng(Double.parseDouble(endlat), Double.parseDouble(endlat1));
                    if (add_more_destination_array.size() > 0) {
                        double lat = Double.parseDouble(add_more_destination_array.get(add_more_destination_array.size() - 1).getLat());
                        double lang = Double.parseDouble(add_more_destination_array.get(add_more_destination_array.size() - 1).getLang());
                        destinationPosition = new LatLng(lat, lang);
                        String address = add_more_destination_array.get(add_more_destination_array.size() - 1).getAddress();
                        StringBuffer waypoint = new StringBuffer();


                        item ii = new item();
                        ii.setAddress(en);
                        ii.setLat(endlat);
                        ii.setLang(endlat1);
                        temp_destination_array.add(ii);
                        temp_destination_array.addAll(add_more_destination_array);

                        for (int i = 0; i < temp_destination_array.size() - 1; i++) {
                            waypoint.append(temp_destination_array.get(i).getLat() + "," + temp_destination_array.get(i).getLang() + "|");
                        }

                        String finalwayout = method(waypoint.toString());
                        sendRequest(cu, address, mode, finalwayout);
                    } else {
                        destinationPosition = new LatLng(Double.parseDouble(endlat), Double.parseDouble(endlat1));
                        sendRequest(cu, en, mode);
                    }
                }
            }
        });


        if (click_on_move.equals("Close up")) {
            ll_detail.setVisibility(View.GONE);
        } else if (click_on_move.equals("atob")) {

        } else if (click_on_move.equals("hybrid view")) {

        } else if (click_on_move.equals("map_detail")) {
            ll_detail.setVisibility(View.VISIBLE);
        } else if (click_on_move.equals("detail")) {
            ll_detail.setVisibility(View.VISIBLE);
            rl_map.setVisibility(View.GONE);
        }

        img_zoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                googleMap.animateCamera(CameraUpdateFactory.zoomIn());

            }
        });
        return view;
    }

    public String method(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == '|') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    public void sendRequest(String cu, String end, String mode, String waypoint) {

        try {
            new DirectionFinder(this, cu, end, mode, waypoint).execute();
            onMapReady(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void currentlocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Failed to fetch user location!");
            alertDialog.setMessage("Please enable user location for app, location is required for app to work properly");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            alertDialog.show();
        }
    }

    public void sendRequest(String cu, String end, String mode) {

        try {
            new DirectionFinder(this, cu, end, mode).execute();
            onMapReady(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map.clear();
        map = googleMap;
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);
        map.setTrafficEnabled(false);
        map.setIndoorEnabled(false);
        map.setBuildingsEnabled(true);
        LatLng hcmus = new LatLng(latitude, longitude);
        map.setTrafficEnabled(false);
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        map.setMyLocationEnabled(true);
       /* originMarkers.add(map.addMarker(new MarkerOptions()
                .title("Direction")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.pin))
                .position(hcmus)));*/
    }

    @Override
    public void onDirectionFinderStart() {
        rl_progressbar.setVisibility(View.VISIBLE);
        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }
        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }
        if (polylinePaths != null) {
            for (Polyline polyline : polylinePaths) {
                polyline.remove();
            }
        }
    }

    @Override
    public void onDirectionFondererror() {
        rl_progressbar.setVisibility(View.GONE);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage("You have exceeded your daily request quota for this API.");
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog.show();
    }


    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        rl_progressbar.setVisibility(View.GONE);
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();
        step.clear();
        temp_route.clear();
        temp_route.addAll(routes);
        // ActionStartsHere();
        map.addMarker(new MarkerOptions()
                .title(startaddress)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.origin))
                .position(startPosition));
        map.addMarker(new MarkerOptions()
                .title(endaddress)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.destination))
                .position(destinationPosition));

        if (temp_destination_array.size() > 0) {
            for (int i = 0; i < temp_destination_array.size() - 1; i++) {
                LatLng position = new LatLng(Double.parseDouble(temp_destination_array.get(i).getLat()), Double.parseDouble(temp_destination_array.get(i).getLang()));
                map.addMarker(new MarkerOptions()
                        .title(temp_destination_array.get(i).getAddress())
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.destination_red))
                        .position(position));
            }
        }
       /* for (Route route : routes) {
            lat_lang.addAll(route.points);
        }*/
        try {
            lat_lang.addAll(routes.get(0).points);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (Route route : routes) {
            if (route.status.equals("ZERO_RESULTS")) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("No routes available");
                alertDialog.setMessage("No routes available for selected mode");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
                break;
            } else if (route.status.equals("INVALID_REQUEST")) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Could not find destination");
                alertDialog.setMessage("Try including the City and State, or change mode of search");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            } else {
                try {
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 10));
                  /*  originMarkers.add(map.addMarker(new MarkerOptions()
                            .title(route.startAddress)
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.origin))
                            .position(route.startLocation)));
                    destinationMarkers.add(map.addMarker(new MarkerOptions()
                            .title(route.endAddress)
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.destination))
                            .position(route.endLocation)));*/

                    String dis = route.distance.text;
                    String dur = route.duration.text;
                    startaddress = route.startAddress;
                    endaddress = route.endAddress;
                    if (step.size() <= 0) {
                        for (int j = 0; j < route.step.size(); j++) {
                            item ii = new item();
                            ii.setDistancea(route.step.get(j).getDistancea());
                            ii.setStep(route.step.get(j).getStep());
                            ii.setDuration(route.step.get(j).getDuration());
                            String distances = route.step.get(j).getDistancea();
                            String disvalue = distances.substring(0, distances.indexOf(' '));
                            String disname = distances.substring(distances.indexOf(' ') + 1);
                            double distacesinft = Double.parseDouble(disvalue);
                            if (disname.equals("ft")) {
                                distacesinft = distacesinft * 0.000189394;
                            }
                            totaldistance = totaldistance + distacesinft;
                            String durations = route.step.get(j).getDuration();
                            String durvalue = durations.substring(0, durations.indexOf(' '));
                            double durv = Double.parseDouble(durvalue);
                            totalduration = totalduration + (durv / 60);
                            step.add(ii);
                        }
                        avgsppedinmilesperhor = totaldistance / totalduration;

                        txt_total_dur.setText("Total Duration: " + dur);
                        txt_total_dis.setText("Total Distance: " + dis);
                        txt_destion_address.setText("" +
                                " Address: " + endaddress);
                        txt_origin_address.setText("Origin Address: " + startaddress);
                        if (avgsppedinmilesperhor < 15.0) {
                            PolylineOptions polylineOptions = new PolylineOptions().
                                    geodesic(true).
                                    color(Color.parseColor("#00B3FD")).
                                    width(18);
                            for (int i = 0; i < route.points.size(); i++) {
                                polylineOptions.add(route.points.get(i));
                            }
                            polylinePaths.add(map.addPolyline(polylineOptions));
                        } else if (avgsppedinmilesperhor >= 15.0 && avgsppedinmilesperhor < 50.0) {
                            PolylineOptions polylineOptions = new PolylineOptions().
                                    geodesic(true).
                                    color(Color.parseColor("#00B3FD")).
                                    width(18);
                            for (int i = 0; i < route.points.size(); i++) {
                                polylineOptions.add(route.points.get(i));
                            }
                            polylinePaths.add(map.addPolyline(polylineOptions));

                        } else {
                            PolylineOptions polylineOptions = new PolylineOptions().
                                    geodesic(true).
                                    color(Color.parseColor("#00B3FD")).
                                    width(18);
                            for (int i = 0; i < route.points.size(); i++) {
                                polylineOptions.add(route.points.get(i));
                            }
                            polylinePaths.add(map.addPolyline(polylineOptions));
                        }
                        setAnimation(map, route.points);


                    }
                } catch (Exception e) {
                }
            }
        }
        // drawPolyLineOnMap(lat_lang);
        Resources rs;
        CustomAdaptercity adpater = new CustomAdaptercity(getActivity(), step, rs = getResources());
        list_step.setAdapter(adpater);
    }


    private void changeCamera(CameraUpdate update, GoogleMap.CancelableCallback callback, boolean instant) {
        if (instant) {
            map.animateCamera(update, 1, callback);
        } else {
            map.animateCamera(update, 1000, callback);
        }
    }

    public void ActionStartsHere() {
        againStartGPSAndSendFile();
    }

    public void ActionStartsHere1() {
        againStartGPSAndSendFile_();
    }


    public void againStartGPSAndSendFile() {
        new CountDownTimer(300000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);
                //Log.e("second", String.valueOf(seconds));
            }

            @Override
            public void onFinish() {
                Activity activty = getActivity();
                if (activty != null) {
                    googleMap.clear();
                    SharedPreferences ss = getActivity().getSharedPreferences("show_direction", Context.MODE_PRIVATE);
                    final String cu = ss.getString("current", "null");
                    final String en = ss.getString("des", "null");
                    final String mode = ss.getString("mode", "driving");
                    final String curnnt = ss.getString("current_lat", "0.0");
                    final String endloc = ss.getString("des_lat", "0.0");
                    if (!cu.equals("null") && !en.equals("null")) {
                        String lar = curnnt.substring(0, curnnt.indexOf(","));
                        String lat1 = curnnt.substring(curnnt.indexOf(",") + 1);
                        String endlat = endloc.substring(0, endloc.indexOf(","));
                        String endlat1 = endloc.substring(endloc.indexOf(",") + 1);
                        startPosition = new LatLng(Double.parseDouble(lar), Double.parseDouble(lat1));
                        destinationPosition = new LatLng(Double.parseDouble(endlat), Double.parseDouble(endlat1));
                        sendRequest(cu, en, mode);
                    }
                    //  ActionStartsHere();
                }
            }
        }.start();
    }

    public void againStartGPSAndSendFile_() {
        new CountDownTimer(3000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);
                Log.e("action", String.valueOf(seconds));
            }

            @Override
            public void onFinish() {
                cancel();
                if (googleMap != null) {
                    Log.e("actgion", "no");
                    googleMap.stopAnimation();
                    map.stopAnimation();
                }
                if (marker_temp != null) {
                    marker_temp.remove();
                    Log.e("actgion", "no1");
                    googleMap.stopAnimation();
                    map.stopAnimation();
                }
            }
        }.start();
    }

    public void setAnimation(GoogleMap myMap, final List<LatLng> directionPoint) {


        Marker marker = myMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.destination))
                .position(directionPoint.get(0))
                .flat(true));

        myMap.animateCamera(CameraUpdateFactory.newLatLngZoom(directionPoint.get(0), 10));

        animateMarker(myMap, marker, directionPoint, false);
    }

    private void animateMarker(final GoogleMap myMap, final Marker marker, final List<LatLng> directionPoint,
                               final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = myMap.getProjection();
        final long duration = 5000;
        marker_temp = marker;
        final Interpolator interpolator = new LinearInterpolator();


        handler.post(new Runnable() {
            int i = 0;

            @Override
            public void run() {

                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                if (i < directionPoint.size())
                    marker.setPosition(directionPoint.get(i));
                try {
                    myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(directionPoint.get(i), 10));
                } catch (Exception e) {
                }

                i++;
                if (i == directionPoint.size()) {
                    handler.removeCallbacks(this);
                    marker.remove();
                    marker.setVisible(false);
                    handler.removeMessages(0);
                    map.addMarker(new MarkerOptions()
                            .title(startaddress)
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.origin))
                            .position(startPosition));
                    map.addMarker(new MarkerOptions()
                            .title(endaddress)
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.destination))
                            .position(destinationPosition));

                    CameraPosition mCPFrom = new CameraPosition.Builder()
                            .target(startPosition).zoom(10.5f).bearing(0).tilt(25)
                            .build();
                    final CameraPosition mCPTo = new CameraPosition.Builder()
                            .target(destinationPosition).zoom(10.5f).bearing(0)
                            .tilt(50).build();

                    changeCamera(CameraUpdateFactory.newCameraPosition(mCPFrom),
                            new GoogleMap.CancelableCallback() {
                                @Override
                                public void onFinish() {
                                    marker.remove();
                                    changeCamera(CameraUpdateFactory
                                                    .newCameraPosition(mCPTo),
                                            new GoogleMap.CancelableCallback() {

                                                @Override
                                                public void onFinish() {
                                                    myMap.stopAnimation();
                                                    map.stopAnimation();
                                                    marker.remove();


                                                         /*   LatLngBounds bounds = new LatLngBounds.Builder()
                                                                    .include(startPosition)
                                                                    .include(destinationPosition)
                                                                    .build();*/

                                                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                                                    if (temp_destination_array.size() > 0) {
                                                        for (int i = 0; i < temp_destination_array.size(); i++) {
                                                            LatLng position = new LatLng(Double.parseDouble(temp_destination_array.get(i).getLat()), Double.parseDouble(temp_destination_array.get(i).getLang()));
                                                            builder.include(position);
                                                        }
                                                        builder.include(startPosition);
                                                        LatLngBounds bounds = builder.build();
                                                        changeCamera(
                                                                CameraUpdateFactory
                                                                        .newLatLngBounds(
                                                                                bounds, 50),
                                                                null, false);
                                                    } else {
                                                        LatLngBounds bounds = new LatLngBounds.Builder()
                                                                .include(startPosition)
                                                                .include(destinationPosition)
                                                                .build();
                                                        changeCamera(
                                                                CameraUpdateFactory
                                                                        .newLatLngBounds(
                                                                                bounds, 50),
                                                                null, false);
                                                    }


                                                    ActionStartsHere1();
                                                    //marker.setVisible(false);

                                                }


                                                @Override
                                                public void onCancel() {
                                                    marker.remove();
                                                }
                                            }, false);
                                }

                                @Override
                                public void onCancel() {
                                    marker.remove();
                                }
                            }, true);
                    Log.e("time", "off");
                } else {

                    if (t < 1.0) {
                        //marker.remove();
                        Log.e("time", "on");
                        handler.postDelayed(this, 3);
                        googleMap.stopAnimation();
                        //handler.removeCallbacks(this);
                        //  marker.remove();
                    } else {
                        Log.e("time1", "on");
                        // googleMap.stopAnimation();
                        // map.stopAnimation();
                        // marker.remove();
                        // handler.removeCallbacks(this);
                        if (hideMarker) {
                            marker.setVisible(false);
                        } else {
                            marker.setVisible(true);
                        }
                    }
                }

            }
        });
    }


    public class CustomAdaptercity extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdaptercity(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.adapetstep, null);
                holder = new ViewHolder();
                holder.txt_step_dis = (TextView) vi.findViewById(R.id.txt_step_distance);
                holder.txt_step_dur = (TextView) vi.findViewById(R.id.txt_step_duration);
                holder.txt_step_title = (TextView) vi.findViewById(R.id.txt_step_title);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String myString = tempValues.getStep();
                String trimmedString = myString.replace("<div>", "");
                holder.txt_step_title.setText(Html.fromHtml(trimmedString));
                holder.txt_step_dur.setText("Duration: " + tempValues.getDuration());
                holder.txt_step_dis.setText("Distance: " + tempValues.getDistancea());

            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txt_step_title, txt_step_dis, txt_step_dur;
        }
    }

    public void chnage_map_data_and_view(String click_on_move) {
        if (googleMap != null) {
            googleMap.stopAnimation();
        }
        if (click_on_move.equals("Close up")) {
            ll_detail.setVisibility(View.GONE);
            img_refresh.setVisibility(View.VISIBLE);
            rl_map.setVisibility(View.VISIBLE);
            if (googleMap != null) {

                if (is_delays) {
                    googleMap.setTrafficEnabled(true);
                } else {
                    googleMap.setTrafficEnabled(false);
                }

                if (is_click_close_up) {
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(startPosition, 15));
                } else {
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(destinationPosition, 15));
                }

                draw_poyline_close();
                //googleMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
            }
        } else if (click_on_move.equals("atob")) {
            ll_detail.setVisibility(View.GONE);
            img_refresh.setVisibility(View.VISIBLE);
            rl_map.setVisibility(View.VISIBLE);
            if (googleMap != null) {

                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                if (is_click_atob) {
                    PolylineOptions pp = new PolylineOptions();
                    googleMap.addPolyline(pp);
                    googleMap.clear();
                    googleMap.addMarker(new MarkerOptions()
                            .title(startaddress)
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.origin))
                            .position(startPosition));
                    googleMap.addMarker(new MarkerOptions()
                            .title(endaddress)
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.destination))
                            .position(destinationPosition));
                    if (temp_destination_array.size() > 0) {
                        for (int i = 0; i < temp_destination_array.size() - 1; i++) {
                            LatLng position = new LatLng(Double.parseDouble(temp_destination_array.get(i).getLat()), Double.parseDouble(temp_destination_array.get(i).getLang()));
                            googleMap.addMarker(new MarkerOptions()
                                    .title(temp_destination_array.get(i).getAddress())
                                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.destination_red))
                                    .position(position));
                        }
                    }
                } else {
                    draw_poyline();
                }

                if (is_delays) {
                    googleMap.setTrafficEnabled(true);
                } else {
                    googleMap.setTrafficEnabled(false);
                }

            }
        } else if (click_on_move.equals("hybrid view")) {
            rl_map.setVisibility(View.VISIBLE);
            img_refresh.setVisibility(View.VISIBLE);
            if (googleMap != null) {
                if (is_delays) {
                    googleMap.setTrafficEnabled(true);
                } else {
                    googleMap.setTrafficEnabled(false);
                }

                draw_poyline();
                //googleMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
                googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

            }
        } else if (click_on_move.equals("map_detail")) {
            ll_detail.setVisibility(View.VISIBLE);
            rl_map.setVisibility(View.VISIBLE);
            img_refresh.setVisibility(View.VISIBLE);
            if (googleMap != null) {

                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                draw_poyline();
                if (is_delays) {
                    googleMap.setTrafficEnabled(true);
                } else {
                    googleMap.setTrafficEnabled(false);
                }

                // googleMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
            }
        } else if (click_on_move.equals("detail")) {
            ll_detail.setVisibility(View.VISIBLE);
            rl_map.setVisibility(View.GONE);
            img_refresh.setVisibility(View.GONE);

            if (googleMap != null) {
                if (is_delays) {
                    googleMap.setTrafficEnabled(true);
                } else {
                    googleMap.setTrafficEnabled(false);
                }
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                draw_poyline();
                //  googleMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
            }
        }

    }

    public void draw_poyline() {
        if (temp_route.size() >= 0) {
            googleMap.clear();
            googleMap.addMarker(new MarkerOptions()
                    .title(startaddress)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.origin))
                    .position(startPosition));
            googleMap.addMarker(new MarkerOptions()
                    .title(endaddress)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.destination))
                    .position(destinationPosition));

            if (temp_destination_array.size() > 0) {
                for (int i = 0; i < temp_destination_array.size() - 1; i++) {
                    LatLng position = new LatLng(Double.parseDouble(temp_destination_array.get(i).getLat()), Double.parseDouble(temp_destination_array.get(i).getLang()));
                    googleMap.addMarker(new MarkerOptions()
                            .title(temp_destination_array.get(i).getAddress())
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.destination_red))
                            .position(position));
                }
            }
            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.parseColor("#00B3FD")).
                    width(18);
            List<LatLng> gg = temp_route.get(0).points;
            for (int i = 0; i < gg.size(); i++) {
                polylineOptions.add(gg.get(i));
                googleMap.addPolyline(polylineOptions);
            }

            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            if (temp_destination_array.size() > 0) {
                for (int i = 0; i < temp_destination_array.size(); i++) {
                    LatLng position = new LatLng(Double.parseDouble(temp_destination_array.get(i).getLat()), Double.parseDouble(temp_destination_array.get(i).getLang()));
                    builder.include(position);
                }
                builder.include(startPosition);
                LatLngBounds bounds = builder.build();
                changeCamera(
                        CameraUpdateFactory
                                .newLatLngBounds(
                                        bounds, 50),
                        null, false);
            } else {
                LatLngBounds bounds = new LatLngBounds.Builder()
                        .include(startPosition)
                        .include(destinationPosition)
                        .build();
                changeCamera(
                        CameraUpdateFactory
                                .newLatLngBounds(
                                        bounds, 50),
                        null, false);
            }
        }
    }

    public void draw_poyline_close() {
        if (temp_route.size() >= 0) {
            googleMap.clear();
            googleMap.addMarker(new MarkerOptions()
                    .title(startaddress)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.origin))
                    .position(startPosition));
            googleMap.addMarker(new MarkerOptions()
                    .title(endaddress)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.destination))
                    .position(destinationPosition));

            if (temp_destination_array.size() > 0) {
                for (int i = 0; i < temp_destination_array.size() - 1; i++) {
                    LatLng position = new LatLng(Double.parseDouble(temp_destination_array.get(i).getLat()), Double.parseDouble(temp_destination_array.get(i).getLang()));
                    googleMap.addMarker(new MarkerOptions()
                            .title(temp_destination_array.get(i).getAddress())
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.destination_red))
                            .position(position));
                }
            }
            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.parseColor("#00B3FD")).
                    width(18);
            List<LatLng> gg = temp_route.get(0).points;
            for (int i = 0; i < gg.size(); i++) {
                polylineOptions.add(gg.get(i));
                googleMap.addPolyline(polylineOptions);
            }
        }
    }


}
