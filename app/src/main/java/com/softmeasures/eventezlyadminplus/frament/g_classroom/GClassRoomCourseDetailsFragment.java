package com.softmeasures.eventezlyadminplus.frament.g_classroom;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.api.services.classroom.model.Course;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragGClassroomCourseDetailsBinding;

import org.json.JSONException;
import org.json.JSONObject;

public class GClassRoomCourseDetailsFragment extends BaseFragment {

    private FragGClassroomCourseDetailsBinding binding;
    private String courseId = "";
    private Course course;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_g_classroom_course_details, container, false);
        if (getArguments() != null) {
            courseId = getArguments().getString("courseId");
        }
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        fetchCourseDetails();
    }

    private void fetchCourseDetails() {


        binding.srl.setRefreshing(true);
        AndroidNetworking.get("https://classroom.googleapis.com/v1/courses/" + courseId)
                .addHeaders("Authorization", "Bearer " + myApp.getOAuth())
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("#DEBUG", "  fetchCourseDetails:  onResponse:  " + response);
                        course = new Course();
                        binding.srl.setRefreshing(false);
                        try {
                            JSONObject objectCourse = new JSONObject(response);

                            if (objectCourse.has("id")) {
                                course.setId(objectCourse.getString("id"));
                            }
                            if (objectCourse.has("name")) {
                                course.setName(objectCourse.getString("name"));
                            }
                            if (objectCourse.has("section")) {
                                course.setSection(objectCourse.getString("section"));
                            }
                            if (objectCourse.has("descriptionHeading")) {
                                course.setDescriptionHeading(objectCourse.getString("descriptionHeading"));
                            }
                            if (objectCourse.has("room")) {
                                course.setRoom(objectCourse.getString("room"));
                            }
                            if (objectCourse.has("ownerId")) {
                                course.setOwnerId(objectCourse.getString("ownerId"));
                            }
                            if (objectCourse.has("creationTime")) {
                                course.setCreationTime(objectCourse.getString("creationTime"));
                            }
                            if (objectCourse.has("updateTime")) {
                                course.setUpdateTime(objectCourse.getString("updateTime"));
                            }
                            if (objectCourse.has("enrollmentCode")) {
                                course.setEnrollmentCode(objectCourse.getString("enrollmentCode"));
                            }
                            if (objectCourse.has("courseState")) {
                                course.setCourseState(objectCourse.getString("courseState"));
                            }
                            if (objectCourse.has("alternateLink")) {
                                course.setAlternateLink(objectCourse.getString("alternateLink"));
                            }
                            if (objectCourse.has("teacherGroupEmail")) {
                                course.setTeacherGroupEmail(objectCourse.getString("teacherGroupEmail"));
                            }
                            if (objectCourse.has("courseGroupEmail")) {
                                course.setCourseGroupEmail(objectCourse.getString("courseGroupEmail"));
                            }
                            if (objectCourse.has("guardiansEnabled")) {
                                course.setGuardiansEnabled(objectCourse.getBoolean("guardiansEnabled"));
                            }
                            if (objectCourse.has("calendarId")) {
                                course.setCalendarId(objectCourse.getString("calendarId"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        binding.srl.setRefreshing(false);
                    }
                });

    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {

        binding.srl.setOnRefreshListener(this::fetchCourseDetails);

    }

    @Override
    protected void initViews(View v) {

    }
}
