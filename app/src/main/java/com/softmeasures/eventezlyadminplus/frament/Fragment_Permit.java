package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

/**
 * Created by Significant Info on 3/14/2018.
 */

public class Fragment_Permit extends Fragment {

    View rootView;
    Activity activity;
    Context mContext;
    ConnectionDetector cd;
    String user_id;
    ListView lv_Permit;
    ProgressBar progressbar;
    RelativeLayout rl_progressbar, rl_main;
    ArrayList<item> array_parking_permits;
    CustomAdapterTwonship adpater;
    Animation animation;
    String township_city, township_state, township_country, township_managed_id;
    String township_type[] = {"Paid", "free", "Managed Paid", "Managed Free"}, township_id;
    TextView txt_Title;

    TextView txt_No_Record;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_permit, container, false);

        activity = getActivity();
        mContext = getContext();

        cd = new ConnectionDetector(activity);
        SharedPreferences logindeatl = activity.getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = logindeatl.getString("id", "null");

        rl_main = (RelativeLayout) rootView.findViewById(R.id.rl_main);
        rl_progressbar = (RelativeLayout) rootView.findViewById(R.id.rl_progressbar);
        progressbar = (ProgressBar) rootView.findViewById(R.id.progressbar);
        lv_Permit = (ListView) rootView.findViewById(R.id.lv_Permit);
        txt_Title = (TextView) rootView.findViewById(R.id.txt_Title);
        txt_No_Record = (TextView) rootView.findViewById(R.id.txt_No_Record);
        txt_Title.setText("Select");

        if (cd.isConnectingToInternet()) {
            try {
                new gettownship_list().execute();
            } catch (Exception e) {
                e.printStackTrace();
            }

            lv_Permit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Bundle bundle = new Bundle();
                    bundle.putString("twp_id", array_parking_permits.get(position).getTwp_id());
                    bundle.putString("Official_logo", array_parking_permits.get(position).getOfficial_logo());
                    bundle.putString("Township_logo", array_parking_permits.get(position).getTownship_logo());

                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Fragment_Permit_One pay = new Fragment_Permit_One();
                    ft.add(R.id.My_Container_1_ID, pay);
                    pay.setArguments(bundle);
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                }
            });

        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");

            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            alertDialog.show();
        }
        return rootView;
    }

    public class gettownship_list extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String parkingurl = "_table/parking_permits_by_manager_type_view";


        @Override
        protected void onPreExecute() {
            array_parking_permits = new ArrayList<item>();
            rl_progressbar.setVisibility(View.VISIBLE);
            progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    Log.e("responseStr ", " : " + responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setManager_type(c.getString("manager_type"));
                        item1.setManager_type_id(c.getString("manager_type_id"));
                        item1.setActive(c.getString("active"));
                        item1.setRelatedPermitIssued(c.getString("RelatedPermits"));

                        item1.setIsslected(false);
                        array_parking_permits.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity != null) {
                    if (array_parking_permits.size() > 0) {
                        adpater = new CustomAdapterTwonship(getActivity(), array_parking_permits, getResources());
                        lv_Permit.setAdapter(adpater);
                    } else {
                        txt_No_Record.setVisibility(View.VISIBLE);
                        lv_Permit.setVisibility(View.GONE);
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class CustomAdapterTwonship extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdapterTwonship(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txt_Name;
            public ImageView img_logo;
            public RelativeLayout ll_Main;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_permit_base_1, null);
                holder = new ViewHolder();
                holder.img_logo = (ImageView) vi.findViewById(R.id.img_logo);
                holder.txt_Name = (TextView) vi.findViewById(R.id.txt_Name);
                holder.ll_Main = (RelativeLayout) vi.findViewById(R.id.rl_main);

                vi.setTag(holder);
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (data.size() <= 0) {

            } else {
                holder.ll_Main.setBackgroundColor(Color.parseColor("#FFFFFF"));
                holder.txt_Name.setText(data.get(position).getManager_type());

                if (data.get(position).getManager_type().equalsIgnoreCase("COMMERCIAL")) {
                    Picasso.with(context).load(R.mipmap.commercial_icon).into(holder.img_logo);
                } else if (data.get(position).getManager_type().equalsIgnoreCase("TOWNSHIP")) {
                    Picasso.with(context).load(R.mipmap.township_icon).into(holder.img_logo);
                } else {
                    Picasso.with(context).load(R.mipmap.local_icon).into(holder.img_logo);
                }

                holder.ll_Main.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tempValues = (item) data.get(position);
                        Bundle bundle = new Bundle();
                        bundle.putString("Manager_type", tempValues.getManager_type());
                        bundle.putString("Manager_type_id", tempValues.getManager_type_id());

                        ((vchome) getActivity()).show_back_button();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        Fragment_Permit_One_New pay = new Fragment_Permit_One_New();
                        pay.setArguments(bundle);
                        ft.add(R.id.My_Container_1_ID, pay);
                        fragmentStack.lastElement().onPause();
                        ft.hide(fragmentStack.lastElement());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                    }
                });

            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

}
