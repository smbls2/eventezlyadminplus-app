package com.softmeasures.eventezlyadminplus.frament;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;

import com.softmeasures.eventezlyadminplus.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Frg_Change_password extends Fragment {

    EditText edit_new_password, edit_retype_password;
    RelativeLayout rl_progressbar;
    Button btn_done;
    String new_password, new_Email, session_id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.frg_change_password, container, false);
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        new_Email = logindeatl.getString("email", "null");
        session_id = logindeatl.getString("session_id", "");

        edit_new_password = (EditText) view.findViewById(R.id.edit_new_password);
        edit_retype_password = (EditText) view.findViewById(R.id.edit_confirm_password);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        btn_done = (Button) view.findViewById(R.id.btn_done);

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_data()) {
                    String mPass = edit_new_password.getText().toString();
                    new ForgotPassword(new_Email, mPass).execute();
                }
                hideKeyboard(v);
            }
        });
        return view;
    }

    public boolean check_data() {
        new_password = edit_new_password.getText().toString();
        String confirm_pass = edit_retype_password.getText().toString();
        if (new_password.equals("")) {
            show_Alert("Please enter new password");
            return false;
        } else if (confirm_pass.equals("")) {
            show_Alert("Please enter Confirm password");
            return false;
        } else if (!confirm_pass.equals(new_password)) {
            show_Alert("Password does not match");
            return false;
        } else
            return true;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void hideKeyboard(View view) {
        try {
            if (getActivity() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
        }

    }

    public void show_Alert(String mess) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Error");
        alertDialog.setMessage(mess);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        if (getActivity() != null) {
            alertDialog.show();
        }
    }

    private class ForgotPassword extends AsyncTask<String, String, String> {
        String id;
        String login = "user/password?reset=true";
        String email, message = "", success = "", password = "";

        public ForgotPassword(String email, String password) {
            this.email = email;
            this.password = password;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("email", email);
            jsonValues.put("password", password);

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + login;
            Log.e("login_url", url);
            Log.e("login_param", String.valueOf(json));

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            post.setHeader("x-dreamfactory-session-token", session_id);
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject jso = new JSONObject(responseStr);
                    Log.e("jso ", " : " + jso.toString());
                    if (!jso.has("error")) {
                        success = jso.getString("success");
                    } else {
                        JSONObject ds = jso.getJSONObject("error");
                        message = ds.getString("message");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            super.onPostExecute(s);
            if (getActivity() != null && message.equals("")) {
                if (success.equals("true")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("password changed");
                    //alertDialog.setMessage("You will receive code in your registered email !");
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            edit_new_password.setText("");
                            edit_retype_password.setText("");
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Please try again");
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Error");
                alertDialog.setMessage(message);
                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        }
    }
}