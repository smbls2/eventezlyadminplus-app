package com.softmeasures.eventezlyadminplus.frament.event.AddEdit;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.compressor.Compressor;
import com.softmeasures.eventezlyadminplus.databinding.FragSessionLocationAddEditBinding;
import com.softmeasures.eventezlyadminplus.frament.event.AllEventsFragment;
import com.softmeasures.eventezlyadminplus.frament.event.CoveredLocationsFragment;
import com.softmeasures.eventezlyadminplus.frament.event.session.SessionImageAddEditFragment;
import com.softmeasures.eventezlyadminplus.frament.event.session.SessionsFragment;
import com.softmeasures.eventezlyadminplus.frament.locations.SelectAddressFragment;
import com.softmeasures.eventezlyadminplus.models.EventAddress;
import com.softmeasures.eventezlyadminplus.models.EventSession;
import com.softmeasures.eventezlyadminplus.models.MultiDate;
import com.softmeasures.eventezlyadminplus.models.Upload;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_LOCATION;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_VIRTUALLY;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_BOTH;

public class SessionLocationAddEditFragment extends BaseFragment {

//    public static FragSessionLocationAddEditBinding binding;
//
//    private ProgressDialog progressDialog;
//    private StringBuilder eventDates = new StringBuilder();
//    private StringBuilder eventTiming = new StringBuilder();
//    private String selectedEventStartTime = "", selectedEventEndTime = "";
//    public static ArrayList<String> selectedLocations = new ArrayList<>();
//    private ArrayList<MultiDate> multiDates = new ArrayList<>();
//    private String selectedStartDateForMultiDate = "", selectedEndDateForMultiDate = "";
//    public static ArrayList<EventAddress> eventAddresses = new ArrayList<>();
//    public static EventAddressAdapter eventAddressAdapter;
//    AmazonS3 s3;
//    TransferUtility transferUtility;
//    private ArrayList<Upload> imageUploads = new ArrayList<>();
//
//    private ArrayList<String> timeZones = new ArrayList<>();
//    public static String selectedTimeZone = "";
//
//    private boolean isEdit = false, isRepeat = false;
//    private EventSession eventSession;
//    private ArrayList<String> eventImageUploads = new ArrayList<>();
//    private boolean isDateChanged = false;
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        if (getArguments() != null) {
//            isEdit = getArguments().getBoolean("isEdit", false);
//            isRepeat = getArguments().getBoolean("isRepeat", false);
//            eventSession = new Gson().fromJson(getArguments().getString("eventSession"), EventSession.class);
//        }
//        binding = DataBindingUtil.inflate(inflater, R.layout.frag_session_location_add_edit, container, false);
//        return binding.getRoot();
//    }
//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        s3 = new AmazonS3Client(new BasicAWSCredentials("AKIAJNG22KFRVUAICSEA", "NSrQR/yAg52RPD3kp3FMb3NO+Vrxuty4iAwPU4th"));
//        transferUtility = new TransferUtility(s3, getActivity().getApplicationContext());
//
//        initViews(view);
//        updateViews();
//        setListeners();
//    }
//
//    @Override
//    protected void updateViews() {
//
//        if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_LOCATION) {
//            binding.llEventLocationShow.setVisibility(View.GONE);
//            binding.llSessionLocation.setVisibility(View.VISIBLE);
//
//            binding.tvBtnEventLocationNext.setText(eventSession.isIs_parking_allowed() ? "NEXT"
//                    : isEdit ? "UPDATE" : "ADD");
//
//        } else if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_VIRTUALLY) {
//            binding.llEventLocationShow.setVisibility(View.VISIBLE);
//            binding.llSessionLocation.setVisibility(View.GONE);
//
//            binding.tvBtnEventLocationNext.setText(isEdit ? "UPDATE" : "ADD");
//
//        } else if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_BOTH) {
//            binding.llEventLocationShow.setVisibility(View.VISIBLE);
//            binding.llSessionLocation.setVisibility(View.VISIBLE);
//
//            binding.tvBtnEventLocationNext.setText(eventSession.isIs_parking_allowed() ? "NEXT"
//                    : isEdit ? "UPDATE" : "ADD");
//        }
//
//        if (isEdit) {
//
//            if (eventSession != null) {
//
//                if (eventSession.getEvent_session_address() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_address())) {
//                    binding.etEventAddress.setText(eventSession.getEvent_session_address());
//                    try {
//                        JSONArray jsonArray = new JSONArray(eventSession.getEvent_session_address());
//                        JSONArray jsonArrayLatLng = new JSONArray(eventSession.getLocation_lat_lng());
//                        eventAddresses.clear();
//                        eventAddressAdapter.notifyDataSetChanged();
//                        for (int i = 0; i < jsonArray.length(); i++) {
//                            JSONArray jsonArray1 = jsonArray.getJSONArray(i);
//                            EventAddress eventAddress = new EventAddress();
//                            eventAddress.setEventAddress(jsonArray1.getString(0));
//
//                            JSONArray jsonArray2 = jsonArrayLatLng.getJSONArray(i);
//                            String[] latLng = jsonArray2.getString(0).split(",");
//                            if (latLng != null && latLng.length > 1) {
//                                eventAddress.setLat(Double.parseDouble(latLng[0]));
//                                eventAddress.setLng(Double.parseDouble(latLng[1]));
//                            }
//                            Log.e("#DEBUG", "   EventAddress:  " + new Gson().toJson(eventAddress));
//                            eventAddresses.add(eventAddress);
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                if (eventSession.getEvent_session_begins_date_time() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_begins_date_time())) {
//                    binding.etEventBeginsTime.setText(eventSession.getEvent_session_begins_date_time());
//                    selectedStartDateForMultiDate = eventSession.getEvent_session_begins_date_time();
//                }
//
//                if (eventSession.getEvent_session_ends_date_time() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_ends_date_time())) {
//                    binding.etEventEndTime.setText(eventSession.getEvent_session_ends_date_time());
//                    selectedEndDateForMultiDate = eventSession.getEvent_session_ends_date_time();
//                }
//
//                if (eventSession.getEvent_session_timings() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_timings())) {
//                    try {
//                        JSONArray jsonArrayTime = null;
//                        StringBuilder stringBuilder = new StringBuilder();
//                        jsonArrayTime = new JSONArray(eventSession.getEvent_session_timings());
//                        for (int i = 0; i < jsonArrayTime.length(); i++) {
//                            JSONArray jsonArray2 = jsonArrayTime.getJSONArray(i);
//                            if (i != 0) {
//                                stringBuilder.append("\n");
//                            }
//                            if (jsonArray2 != null && jsonArray2.length() > 0) {
////                                    stringBuilder.append(jsonArray2.get(0).toString());
//                                if (jsonArray2.get(0).toString().length() > 30) {
//                                    String[] s = jsonArray2.get(0).toString().split(" - ");
//                                    for (int j = 0; j < s.length; j++) {
//                                        if (j != 0) {
//                                            stringBuilder.append("-");
//                                        }
//                                        stringBuilder.append(DateTimeUtils.gmtToLocalDateAMPM(s[j]));
//                                    }
//                                } else {
//                                    stringBuilder.append(jsonArray2.get(0).toString());
//                                }
//                            }
//                        }
//                        binding.etEventDates.setText(stringBuilder.toString());
//
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                if (eventSession.getCovered_locations() != null
//                        && !TextUtils.isEmpty(eventSession.getCovered_locations())) {
//                    String[] locations = eventSession.getCovered_locations().split(",");
//                    selectedLocations.clear();
//                    selectedLocations.addAll(Arrays.asList(locations));
//                    binding.etCoveredLocations.setText(eventSession.getCovered_locations());
//                }
//
//                binding.switchShowEventLocation.setChecked(eventSession.isWeb_event_session_location_to_show());
//            }
//
//        } else {
//            if (eventSession != null) {
//                if (eventSession.getLocation_address() != null) {
//                    binding.etEventAddress.setText(eventSession.getLocation_address());
//                }
//
//                if (eventSession.getCovered_locations() != null) {
//                    binding.etCoveredLocations.setText(eventSession.getCovered_locations());
//                }
//
//                try {
//                    JSONArray jsonArray = new JSONArray(eventSession.getEvent_session_address());
//                    JSONArray jsonArrayLatLng = new JSONArray(eventSession.getLocation_lat_lng());
//                    eventAddresses.clear();
//                    eventAddressAdapter.notifyDataSetChanged();
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        JSONArray jsonArray1 = jsonArray.getJSONArray(i);
//                        EventAddress eventAddress = new EventAddress();
//                        eventAddress.setEventAddress(jsonArray1.getString(0));
//
//                        JSONArray jsonArray2 = jsonArrayLatLng.getJSONArray(i);
//                        String[] latLng = jsonArray2.getString(0).split(",");
//                        if (latLng != null && latLng.length > 1) {
//                            eventAddress.setLat(Double.parseDouble(latLng[0]));
//                            eventAddress.setLng(Double.parseDouble(latLng[1]));
//                        }
//                        Log.e("#DEBUG", "   EventAddress:  " + new Gson().toJson(eventAddress));
//                        eventAddresses.add(eventAddress);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//    }
//
//    @Override
//    protected void setListeners() {
//
//        binding.tvBtnEventLocationNext.setOnClickListener(v -> {
//            if (binding.tvBtnEventLocationNext.getText().toString().equals("NEXT")) {
//                //Goto Parking Location
//                if (isValidate()) {
//                    Log.e("#DEBUG", "  isValid:  eventSession:  " + new Gson().toJson(eventSession));
//                    Fragment fragment = new SessionParkingLocationAddEditFragment();
//                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
//                    Bundle bundle = new Bundle();
//                    bundle.putBoolean("isEdit", isEdit);
//                    bundle.putBoolean("isRepeat", isRepeat);
//                    bundle.putString("eventSession", new Gson().toJson(eventSession));
//                    fragment.setArguments(bundle);
//                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
//                    ft.add(R.id.My_Container_1_ID, fragment);
//                    fragmentStack.lastElement().onPause();
//                    ft.hide(fragmentStack.lastElement());
//                    fragmentStack.push(fragment);
//                    ft.commitAllowingStateLoss();
//                }
//            } else {
//                //Submit
//                if (isValidate()) {
//                    Log.e("#DEBUG", "  isValid:  eventSession:  " + new Gson().toJson(eventSession));
//                    imageUploads.clear();
//                    eventImageUploads.clear();
//                    if (SessionInfoAddEditFragment.isEventLogoChange && !TextUtils.isEmpty(eventSession.getEvent_session_logo())) {
//                        File ImageFile = new File(eventSession.getEvent_session_logo());
//                        File compressedImageFile = null;
//                        try {
//                            compressedImageFile = new Compressor(getActivity()).compressToFile(ImageFile);
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        Upload upload = new Upload();
//                        upload.setType("Logo");
//                        upload.setLink(String.valueOf(compressedImageFile));
//                        imageUploads.add(upload);
//                    }
//
//                    for (int i = 1; i < eventSession.getEventImages().size(); i++) {
//                        if (!eventSession.getEventImages().get(i).getPath().contains("parkezly-images")) {
//                            File ImageFile = new File(eventSession.getEventImages().get(i).getPath());
//                            File compressedImageFile = null;
//                            try {
//                                compressedImageFile = new Compressor(getActivity()).compressToFile(ImageFile);
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                            Upload upload = new Upload();
//                            upload.setType("Image");
//                            upload.setLink(String.valueOf(compressedImageFile));
//                            imageUploads.add(upload);
//                        } else {
//                            eventImageUploads.add(eventSession.getEventImages().get(i).getPath());
//                        }
//                    }
//
//                    if (imageUploads.size() > 0) {
//                        upload_file();
//                    } else {
//                        new updateEventDetails().execute();
//                    }
//
//                }
//            }
//        });
//
//        binding.spinnerTimeZone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                selectedTimeZone = timeZones.get(position);
//                Log.e("#DEBUG", "    selectedTimeZone:  " + selectedTimeZone);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//        binding.etEventAddress.setOnClickListener(v -> {
////            SessionAddMainViewFragment.binding.btnSelectAddress.performClick();
//        });
//
//
//        binding.ivBtnAddEventAddress.setOnClickListener(v -> {
//            openSelectAddress();
//        });
//
//        binding.etEventBeginsTime.setOnClickListener(v -> {
//            showDialogSelectEventBeginsTime();
//        });
//
//        binding.etEventEndTime.setOnClickListener(v -> {
//            showDialogSelectEventEndTime();
//        });
//
////        binding.ivBtnAddEventDates.setOnClickListener(v -> {
////            showDialogEventDates();
////        });
//
//        binding.etEventDates.setOnClickListener(v -> {
//            if (TextUtils.isEmpty(binding.etEventBeginsTime.getText())) {
//                Toast.makeText(getActivity(), "Please select event begin time first!", Toast.LENGTH_SHORT).show();
//            } else if (TextUtils.isEmpty(binding.etEventEndTime.getText())) {
//                Toast.makeText(getActivity(), "Please select event end time first!", Toast.LENGTH_SHORT).show();
//            } else {
//                multiDates.clear();
//                ArrayList<String> dates = DateTimeUtils.getDates(selectedStartDateForMultiDate, selectedEndDateForMultiDate);
//                for (int i = 0; i < dates.size(); i++) {
//                    MultiDate multiDate = new MultiDate();
//                    multiDate.setSelected(true);
//                    multiDate.setDate(dates.get(i));
//                    multiDate.setStartTime(selectedStartDateForMultiDate);
//                    multiDate.setEndTime(selectedEndDateForMultiDate);
//                    multiDates.add(multiDate);
//                }
//                showDialogSelectEventMultiDates();
//            }
//        });
//
//        binding.ivBtnAddEventTiming.setOnClickListener(v -> {
//            showDialogAddEventTiming();
//        });
//
//        binding.etEventTiming.setOnClickListener(v -> {
//            if (!TextUtils.isEmpty(binding.etEventTiming.getText())) {
//                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                builder.setMessage("Are you sure you want to clear event timing?");
//                builder.setPositiveButton("YES", (dialog, which) -> {
//                    dialog.dismiss();
//                    binding.etEventTiming.setText("");
//                    eventTiming = new StringBuilder();
//                });
//                builder.setNegativeButton("NO", (dialog, which) -> {
//                    dialog.dismiss();
//                });
//                AlertDialog alertDialog = builder.create();
//                alertDialog.show();
//
//            }
//        });
//
//        binding.etCoveredLocations.setOnClickListener(v -> {
//            openCoveredLocation();
//        });
//    }
//
//    private void openCoveredLocation() {
//        final FragmentTransaction ft = getFragmentManager().beginTransaction();
//        CoveredLocationsFragment frag = new CoveredLocationsFragment();
//        Bundle bundle = new Bundle();
//        bundle.putBoolean("isEdit", false);
//        bundle.putBoolean("isSession", true);
//        bundle.putString("selectedManager", new Gson().toJson(AllEventsFragment.selectedManager));
//        frag.setArguments(bundle);
//        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
//        ft.add(R.id.My_Container_1_ID, frag);
//        fragmentStack.lastElement().onPause();
//        ft.hide(fragmentStack.lastElement());
//        fragmentStack.push(frag);
//        ft.commitAllowingStateLoss();
//    }
//
//    public class updateEventDetails extends AsyncTask<String, String, String> {
//        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
//        String exit_status = "_table/event_session_definitions";
//        JSONObject json, json1;
//        String re_id;
//        String id;
//
//        @Override
//        protected void onPreExecute() {
//            progressDialog.show();
//            super.onPreExecute();
//        }
//
//        @TargetApi(Build.VERSION_CODES.KITKAT)
//        @Override
//        protected String doInBackground(String... params) {
//
//            Map<String, Object> jsonValues = new HashMap<String, Object>();
//
//            if (isEdit) {
//                jsonValues.put("id", eventSession.getId());
//            }
//
//            jsonValues.put("date_time", DateTimeUtils.getSqlFormatDate(Calendar.getInstance().getTime()));
//            jsonValues.put("manager_type", eventSession.getManager_type());
//            jsonValues.put("manager_type_id", eventSession.getManager_type_id());
//            jsonValues.put("twp_id", eventSession.getTwp_id());
//            jsonValues.put("township_code", eventSession.getTownship_code());
//            jsonValues.put("township_name", eventSession.getTownship_name());
//            jsonValues.put("company_id", eventSession.getCompany_id());
//            jsonValues.put("company_code", eventSession.getCompany_code());
//            jsonValues.put("company_name", eventSession.getCompany_name());
//
//            jsonValues.put("event_id", eventSession.getEvent_id());
//            jsonValues.put("event_type", eventSession.getEvent_type());
//            jsonValues.put("event_name", eventSession.getEvent_name());
//            //event_session_id
//            jsonValues.put("event_session_type", eventSession.getEvent_session_type());
//            //event_session_code
//            jsonValues.put("event_session_name", eventSession.getEvent_session_name());
//            jsonValues.put("event_session_short_description", eventSession.getEvent_session_short_description());
//            jsonValues.put("event_session_long_description", eventSession.getEvent_session_long_description());
//            jsonValues.put("event_session_link_on_web", eventSession.getEvent_session_link_on_web());
//            jsonValues.put("event_session_link_ytube", eventSession.getEvent_session_link_ytube());
//            jsonValues.put("event_session_link_zoom", eventSession.getEvent_session_link_zoom());
//            jsonValues.put("event_sessionlink_googlemeet", eventSession.getEvent_session_link_googlemeet());
//            jsonValues.put("event_session_link_googleclassroom", eventSession.getEvent_session_link_googleclassroom());
//            jsonValues.put("event_session_link_facebook", eventSession.getEvent_session_link_facebook());
//            jsonValues.put("event_session_link_twitter", eventSession.getEvent_session_link_twitter());
//            jsonValues.put("event_session_link_whatsapp", eventSession.getEvent_session_link_whatsapp());
//            jsonValues.put("event_session_link_other_media", eventSession.getEvent_session_link_other_media());
//            jsonValues.put("company_logo", eventSession.getCompany_logo());
//            jsonValues.put("event_logo", eventSession.getEvent_logo());
//            jsonValues.put("event_session_logo", eventSession.getEvent_session_logo());
//            jsonValues.put("event_session_image1", eventSession.getEvent_session_image1());
//            jsonValues.put("event_session_image2", eventSession.getEvent_session_image2());
//            jsonValues.put("event_session_blob_image", new Gson().toJson(eventImageUploads));
//            jsonValues.put("event_session_address", eventSession.getEvent_session_address());
//            jsonValues.put("covered_locations", eventSession.getCovered_locations());
//            jsonValues.put("session_regn_needed_approval", eventSession.isSession_regn_needed_approval());
//            jsonValues.put("requirements", eventSession.getRequirements());
//            //appl_req_download
//            jsonValues.put("cost", eventSession.getCost());
//            jsonValues.put("year", eventSession.getYear());
//            jsonValues.put("location_address", eventSession.getLocation_address());
//            //scheme_type
//            //event_prefix
//            //event_session_prefix
//            //event_nextnum
//            //event_session_nextnum
//            jsonValues.put("local_timezone", selectedTimeZone);
//            jsonValues.put("local_time_event_session_start", eventSession.getEvent_session_begins_date_time());
//            if (eventSession.getEvent_session_begins_date_time() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_begins_date_time())) {
//                jsonValues.put("event_session_begins_date_time", DateTimeUtils.localToGMT(eventSession.getEvent_session_begins_date_time()));
//            }
//            if (eventSession.getEvent_session_ends_date_time() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_ends_date_time())) {
//                jsonValues.put("event_session_ends_date_time", DateTimeUtils.localToGMT(eventSession.getEvent_session_ends_date_time()));
//            }
//            if (eventSession.getEvent_session_parking_begins_date_time() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_parking_begins_date_time())) {
//                jsonValues.put("event_session_parking_begins_date_time", DateTimeUtils.localToGMT(eventSession.getEvent_session_parking_begins_date_time()));
//            }
//            if (eventSession.getEvent_session_parking_ends_date_time() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_parking_ends_date_time())) {
//                jsonValues.put("event_session_parking_ends_date_time", DateTimeUtils.localToGMT(eventSession.getEvent_session_parking_ends_date_time()));
//            }
//            if (eventSession.getEvent_session_multi_dates() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_multi_dates())) {
//                jsonValues.put("event_session_multi_dates", eventSession.getEvent_session_multi_dates());
//            }
//            if (eventSession.getEvent_session_parking_timings() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_parking_timings())) {
//                jsonValues.put("event_session_parking_timings", eventSession.getEvent_session_parking_timings());
//            }
//            if (eventSession.getEvent_session_timings() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_timings())) {
//                jsonValues.put("event_session_timings", eventSession.getEvent_session_timings());
//            }
//            if (eventSession.getEvent_session_parking_ends_date_time() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_parking_ends_date_time())) {
//                jsonValues.put("expires_by", DateTimeUtils.localToGMT(eventSession.getEvent_session_parking_ends_date_time()));
//            }
//            jsonValues.put("regn_reqd", eventSession.isRegn_reqd());
//            if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_VIRTUALLY) {
//                jsonValues.put("is_parking_allowed", 0);
//            } else {
//                jsonValues.put("is_parking_allowed", eventSession.isIs_parking_allowed() ? 1 : 0);
//            }
//            jsonValues.put("event_session_logi_type", eventSession.getEvent_session_logi_type());
//            jsonValues.put("event_session_regn_allowed", eventSession.isEvent_session_regn_allowed());
//            jsonValues.put("event_session_regn_limit", eventSession.getEvent_session_regn_limit());
//            jsonValues.put("event_session_regn_fee", eventSession.getEvent_session_regn_fee());
//            if (eventSession.getEvent_session_regn_fee() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_regn_fee()))
//                jsonValues.put("event_session_regn_fee", eventSession.getEvent_session_regn_fee());
//            if (eventSession.getEvent_session_youth_fee() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_youth_fee()))
//                jsonValues.put("event_session_youth_fee", eventSession.getEvent_session_youth_fee());
//            if (eventSession.getEvent_session_child_fee() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_child_fee()))
//                jsonValues.put("event_session_child_fee", eventSession.getEvent_session_child_fee());
//            if (eventSession.getEvent_session_student_fee() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_student_fee()))
//                jsonValues.put("event_session_student_fee", eventSession.getEvent_session_student_fee());
//            if (eventSession.getEvent_session_minister_fee() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_minister_fee()))
//                jsonValues.put("event_session_minister_fee", eventSession.getEvent_session_minister_fee());
//            if (eventSession.getEvent_session_clergy_fee() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_clergy_fee()))
//                jsonValues.put("event_session_clergy_fee", eventSession.getEvent_session_clergy_fee());
//            if (eventSession.getEvent_session_promo_fee() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_promo_fee()))
//                jsonValues.put("event_session_promo_fee", eventSession.getEvent_session_promo_fee());
//            if (eventSession.getEvent_session_senior_fee() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_senior_fee()))
//                jsonValues.put("event_session_senior_fee", eventSession.getEvent_session_senior_fee());
//            //event_session_family_fee
//            if (eventSession.getEvent_session_staff_fee() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_staff_fee()))
//                jsonValues.put("event_session_staff_fee", eventSession.getEvent_session_staff_fee());
//            jsonValues.put("web_event_session_regn_fee", eventSession.getWeb_event_session_regn_fee());
//            jsonValues.put("web_event_session_regn_limit", eventSession.getWeb_event_session_regn_limit());
//            jsonValues.put("web_event_session_location_to_show", eventSession.isWeb_event_session_location_to_show() ? 1 : 0);
//            if (eventSession.getEvent_session_parking_fee() != null
//                    && !TextUtils.isEmpty(eventSession.getEvent_session_parking_fee()))
//                jsonValues.put("event_session_parking_fee", eventSession.getEvent_session_parking_fee());
//            jsonValues.put("event_session_led_by", eventSession.getEvent_session_led_by());
//            jsonValues.put("event_session_leaders_bio", eventSession.getEvent_session_leaders_bio());
//            jsonValues.put("regn_reqd_for_parking", eventSession.isRegn_reqd_for_parking());
//            jsonValues.put("renewable", eventSession.isRenewable());
//            jsonValues.put("free_session", eventSession.isFree_session() ? 1 : 0);
//            jsonValues.put("reqd_local_session_regn", eventSession.isReqd_local_session_regn() ? 1 : 0);
//            jsonValues.put("reqd_web_session_regn", eventSession.isReqd_web_session_regn() ? 1 : 0);
//            jsonValues.put("free_local_session", eventSession.isFree_local_session() ? 1 : 0);
//            jsonValues.put("free_web_session", eventSession.isFree_web_session());
//            jsonValues.put("free_session_parking", eventSession.isFree_session_parking() ? 1 : 0);
//            jsonValues.put("active", eventSession.isActive());
//            if (!TextUtils.isEmpty(eventSession.getSession_link_array())) {
//                jsonValues.put("session_link_array", eventSession.getSession_link_array());
//            }
//            //#####
//
//            JSONObject json = new JSONObject(jsonValues);
//            DefaultHttpClient client = new DefaultHttpClient();
//            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
//            HttpResponse response = null;
//            StringEntity entity = null;
//            try {
//                entity = new StringEntity(json.toString(), "UTF8");
//                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            if (isEdit) {
//                HttpPut post = new HttpPut(url);
//                post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
//                post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
//                post.setEntity(entity);
//                Log.e("#DEBUG", "  updateURL:  " + url);
//                Log.e("#DEBUG", "   update Params: " + String.valueOf(json));
//                try {
//                    response = client.execute(post);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                HttpPost post = new HttpPost(url);
//                post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
//                post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
//                post.setEntity(entity);
//                Log.e("#DEBUG", "  updateURL:  " + url);
//                Log.e("#DEBUG", "   update Params: " + String.valueOf(json));
//                try {
//                    response = client.execute(post);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            if (response != null) {
//                HttpEntity resEntity = response.getEntity();
//                String responseStr = null;
//                try {
//                    responseStr = EntityUtils.toString(resEntity).trim();
//                    Log.e("#DEBUG", "   Add/Edit Event:  Response:  " + responseStr);
//                    json1 = new JSONObject(responseStr);
//                    JSONArray array = json1.getJSONArray("resource");
//                    for (int i = 0; i < array.length(); i++) {
//
//                        JSONObject c = array.getJSONObject(i);
//                        re_id = c.getString("id");
//                        Log.e("#DEBUG", "  update: ResponseID: " + re_id);
//
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                System.out.println("Response: " + response.getStatusLine());
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            progressDialog.hide();
//            if (getActivity() != null && json1 != null) {
//                if (re_id != null && !re_id.equals("null")) {
//                    if (isEdit) {
//                        SessionsFragment.isSessionUpdated = true;
//                        Toast.makeText(getActivity(), "Updated!", Toast.LENGTH_SHORT).show();
//                        getActivity().onBackPressed();
//                    } else {
//                        new updateEventId().execute(re_id);
//                    }
//                }
//                super.onPostExecute(s);
//            }
//        }
//    }
//
//    public class updateEventId extends AsyncTask<String, String, String> {
//        String exit_status = "_table/event_session_definitions";
//        JSONObject json, json1;
//        String re_id;
//        String id;
//
//        @Override
//        protected void onPreExecute() {
//            progressDialog.show();
//            super.onPreExecute();
//        }
//
//        @TargetApi(Build.VERSION_CODES.KITKAT)
//        @Override
//        protected String doInBackground(String... params) {
//
//            Map<String, Object> jsonValues = new HashMap<String, Object>();
//
//
//            jsonValues.put("id", params[0]);
//            jsonValues.put("event_session_id", params[0]);
//
//            JSONObject json = new JSONObject(jsonValues);
//            DefaultHttpClient client = new DefaultHttpClient();
//            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
//            HttpResponse response = null;
//            StringEntity entity = null;
//            try {
//                entity = new StringEntity(json.toString(), "UTF8");
//                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            HttpPut post = new HttpPut(url);
//            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
//            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
//            post.setEntity(entity);
//            Log.e("#DEBUG", "  updateURL:  " + url);
//            Log.e("#DEBUG", "   update Params: " + String.valueOf(json));
//            try {
//                response = client.execute(post);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            if (response != null) {
//                HttpEntity resEntity = response.getEntity();
//                String responseStr = null;
//                try {
//                    responseStr = EntityUtils.toString(resEntity).trim();
//                    json1 = new JSONObject(responseStr);
//                    JSONArray array = json1.getJSONArray("resource");
//                    for (int i = 0; i < array.length(); i++) {
//
//                        JSONObject c = array.getJSONObject(i);
//                        re_id = c.getString("id");
//                        Log.e("#DEBUG", "  update: ResponseID: " + re_id);
//
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                System.out.println("Response: " + response.getStatusLine());
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            progressDialog.hide();
//            if (getActivity() != null && json1 != null) {
//                if (re_id != null && !re_id.equals("null")) {
//                    SessionsFragment.isSessionUpdated = true;
//                    Toast.makeText(getActivity(), "Added!", Toast.LENGTH_SHORT).show();
//                    getActivity().onBackPressed();
//                }
//                super.onPostExecute(s);
//            }
//        }
//    }
//
//    private boolean isValidate() {
//
//        if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_LOCATION
//                || eventSession.getEvent_session_logi_type() == EVENT_TYPE_BOTH) {
//
//            if (eventAddresses != null && eventAddresses.size() == 0) {
//                Toast.makeText(getActivity(), "Please select address!", Toast.LENGTH_SHORT).show();
//                return false;
//            } else if (eventAddresses != null) {
//                ArrayList<EventAddress> eventAddresses = new ArrayList<>(this.eventAddresses);
//                JSONArray jsonArrayAddress = new JSONArray();
//                JSONArray jsonArrayLatLang = new JSONArray();
//                for (int i = 0; i < eventAddresses.size(); i++) {
//                    JSONArray jsonArray1 = new JSONArray();
//                    jsonArray1.put(eventAddresses.get(i).getEventAddress());
//                    jsonArrayAddress.put(jsonArray1);
//
//                    JSONArray jsonArray2 = new JSONArray();
//                    jsonArray2.put(eventAddresses.get(i).getLat() + "," + eventAddresses.get(i).getLng());
////                    if (i == 0) {
////                        eventSession.setLat(String.valueOf(eventAddresses.get(i).getLat()));
////                        eventSession.setLng(String.valueOf(eventAddresses.get(i).getLng()));
////                    }
//                    jsonArrayLatLang.put(jsonArray2);
//                }
//
//                eventSession.setEvent_session_address(jsonArrayAddress.toString());
//                eventSession.setLocation_address(jsonArrayAddress.toString());
//                eventSession.setLocation_lat_lng(jsonArrayLatLang.toString());
//            }
//        }
//
//        if (!TextUtils.isEmpty(binding.etCoveredLocations.getText())) {
//            eventSession.setCovered_locations(binding.etCoveredLocations.getText().toString());
//        }
//
//        if (TextUtils.isEmpty(binding.etEventBeginsTime.getText())) {
//            Toast.makeText(getActivity(), "Please select Event Begins Time!", Toast.LENGTH_SHORT).show();
//            return false;
//        }
//        if (TextUtils.isEmpty(binding.etEventEndTime.getText())) {
//            Toast.makeText(getActivity(), "Please select Event End Time!", Toast.LENGTH_SHORT).show();
//            return false;
//        }
//
//        if (!isEdit) {
//            if (TextUtils.isEmpty(binding.etEventDates.getText())) {
//                Toast.makeText(getActivity(), "Please select Event Date and Timing!", Toast.LENGTH_SHORT).show();
//                return false;
//            }
//            if (!TextUtils.isEmpty(binding.etEventTiming.getText())) {
//                String eventMultiDay = binding.etEventTiming.getText().toString();
//                String[] strings = eventMultiDay.split(", ");
//                JSONArray jsonArray = new JSONArray();
//                for (String string : strings) {
//                    JSONArray jsonArray1 = new JSONArray();
//                    jsonArray1.put(string);
//                    jsonArray.put(jsonArray1);
//                }
//                eventSession.setEvent_session_timings(jsonArray.toString());
//            } else {
//                Toast.makeText(getActivity(), "Please select Event Timing!", Toast.LENGTH_SHORT).show();
//                return false;
//            }
//        } else if (isDateChanged) {
//            isDateChanged = false;
//            if (TextUtils.isEmpty(binding.etEventDates.getText())) {
//                Toast.makeText(getActivity(), "Please select Event Date and Timing!", Toast.LENGTH_SHORT).show();
//                return false;
//            }
//            if (!TextUtils.isEmpty(binding.etEventTiming.getText())) {
//                String eventMultiDay = binding.etEventTiming.getText().toString();
//                String[] strings = eventMultiDay.split(", ");
//                JSONArray jsonArray = new JSONArray();
//                for (String string : strings) {
//                    JSONArray jsonArray1 = new JSONArray();
//                    jsonArray1.put(string);
//                    jsonArray.put(jsonArray1);
//                }
//                eventSession.setEvent_session_timings(jsonArray.toString());
//            } else {
//                Toast.makeText(getActivity(), "Please select Event Timing!", Toast.LENGTH_SHORT).show();
//                return false;
//            }
//        }
//
//        eventSession.setWeb_event_session_location_to_show(binding.switchShowEventLocation.isChecked());
//        eventSession.setLocal_timezone(selectedTimeZone);
//        return true;
//    }
//
//    String final_DivisionImage;
//    StringBuffer file_DivisionImage;
//
//    public void upload_file() {
//        progressDialog.show();
//        file_DivisionImage = new StringBuffer();
//        String fileUrl = "";
//        fileUrl = imageUploads.get(0).getLink();
//        File file = new File(fileUrl);
//        String namegsxsax = System.currentTimeMillis() + ".jpg";
//        file_DivisionImage.append(namegsxsax + ",");
//        namegsxsax = "/images/" + namegsxsax;
//        final_DivisionImage = file_DivisionImage.substring(0, file_DivisionImage.length() - 1);
//        TransferObserver transferObserver = transferUtility.upload("parkezly-images", namegsxsax, file);
//        transferObserverListener(transferObserver);
//    }
//
//    public void transferObserverListener(final TransferObserver transferObserver) {
//        transferObserver.setTransferListener(new TransferListener() {
//            @Override
//            public void onStateChanged(int id, TransferState state) {
//                if (state.name().equals("COMPLETED")) {
//                    if (imageUploads.get(0).getType().equals("Logo")) {
//                        eventSession.setEvent_session_logo("https://s3.amazonaws.com/parkezly-images//images/" + final_DivisionImage);
//                    } else if (imageUploads.get(0).getType().equals("Image")) {
//                        eventImageUploads.add("https://s3.amazonaws.com/parkezly-images//images/" + final_DivisionImage);
//                    }
//                    if (imageUploads.size() > 0) {
//                        imageUploads.remove(0);
//                        if (imageUploads.size() == 0) {
//                            //Upload complete
//                            new updateEventDetails().execute();
//                        } else {
//                            upload_file();
//                        }
//                    }
//                }
//            }
//
//            @Override
//            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
//                try {
//                    int percentage = (int) (bytesCurrent / bytesTotal * 100);
//                    Log.e("percentage ", " : " + percentage);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onError(int id, Exception ex) {
//                Log.e("error", "error");
//            }
//
//        });
//    }
//
//    private void showDialogSelectEventMultiDates() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_multi_date_select, null);
//        builder.setView(dialogView);
//        TextView tvTitle = dialogView.findViewById(R.id.tvTitle);
//        tvTitle.setText("Select Event Dates and Timing");
//        RecyclerView rvMultiDates = dialogView.findViewById(R.id.rvMultiDates);
//        MultiDatesAdapter multiDatesAdapter = new MultiDatesAdapter();
//        rvMultiDates.setLayoutManager(new LinearLayoutManager(getActivity()));
//        rvMultiDates.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
//        rvMultiDates.setAdapter(multiDatesAdapter);
//        Button btnDone = dialogView.findViewById(R.id.btnDone);
//        AlertDialog alertDialog = builder.create();
//
//        btnDone.setOnClickListener(v -> {
//            alertDialog.dismiss();
//            Log.e("#DEBUG", "   MultiDates:  " + new Gson().toJson(multiDates));
//            eventDates = new StringBuilder();
//            eventTiming = new StringBuilder();
//            StringBuilder displayText = new StringBuilder();
//            for (int i = 0; i < multiDates.size(); i++) {
//                if (multiDates.get(i).isSelected()) {
//                    if (!TextUtils.isEmpty(eventDates.toString())) {
//                        eventDates.append(", ");
//                    }
//                    eventDates.append(DateTimeUtils.localToGMTYYYYMMDDHHMMSS(multiDates.get(i).getDate()));
//                    displayText.append(multiDates.get(i).getDate().substring(0, 10));
//
//                    if (!TextUtils.isEmpty(eventTiming.toString())) {
//                        eventTiming.append(", ");
//                    }
//                    eventTiming.append(DateTimeUtils.localToGMT_DD_HH_AM_PM(multiDates.get(i).getDate().substring(0, 10) + " "
//                            + multiDates.get(i).getStartHour() + ":" + multiDates.get(i).getStartMinute() + " " + multiDates.get(i).getStartAMPM()));
//                    eventTiming.append(" - ");
//                    eventTiming.append(DateTimeUtils.localToGMT_DD_HH_AM_PM(multiDates.get(i).getDate().substring(0, 10) + " "
//                            + multiDates.get(i).getEndHour() + ":" + multiDates.get(i).getEndMinute() + " " + multiDates.get(i).getEndAMPM()));
//
//                    displayText.append("    (");
//                    displayText.append(multiDates.get(i).getStartHour() + ":" + multiDates.get(i).getStartMinute() + " " + multiDates.get(i).getStartAMPM());
//                    displayText.append(" - ");
//                    displayText.append(multiDates.get(i).getEndHour() + ":" + multiDates.get(i).getEndMinute() + " " + multiDates.get(i).getEndAMPM());
//                    displayText.append(")");
//                    if (i == multiDates.size() - 1) {
//
//                    } else {
//                        displayText.append("\n");
//                    }
//                }
//            }
//            binding.etEventTiming.setText(eventTiming.toString());
//            binding.etEventDates.setText(displayText.toString());
//
//            String[] strings = eventDates.toString().split(",");
//            JSONArray jsonArray = new JSONArray();
//            for (String string : strings) {
//                JSONArray jsonArray1 = new JSONArray();
//                jsonArray1.put(string);
//                jsonArray.put(jsonArray1);
//            }
//            eventSession.setEvent_session_multi_dates(jsonArray.toString());
//        });
//
//
//        alertDialog.show();
//    }
//
//    private void showDialogAddEventTiming() {
//        selectedEventStartTime = "";
//        selectedEventEndTime = "";
//        ArrayList<String> timeHours = new ArrayList<>();
//        ArrayList<String> timeMinutes = new ArrayList<>();
//
//        timeHours.clear();
//        for (int i = 0; i < 2; i++) {
//            for (int j = 1; j < 13; j++) {
//                if (i == 0) {
//                    if (j == 12) {
//                        timeHours.add(j + " PM");
//                    } else {
//                        timeHours.add(j + " AM");
//                    }
//                } else {
//                    timeHours.add(j + " PM");
//                }
//            }
//        }
//        timeMinutes.clear();
//        timeMinutes.addAll(timeHours);
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setCancelable(false);
//        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_time_picker_dialog, null);
//        AppCompatSpinner spinnerHour = dialogView.findViewById(R.id.spinnerHour);
//        AppCompatSpinner spinnerMinute = dialogView.findViewById(R.id.spinnerMinute);
//
//        ArrayAdapter<String> timeHoursAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, timeHours);
//        spinnerHour.setAdapter(timeHoursAdapter);
//
//        ArrayAdapter<String> timeMinutesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, timeMinutes);
//        spinnerMinute.setAdapter(timeMinutesAdapter);
//
//        Button btnDone = dialogView.findViewById(R.id.btnDone);
//
//        builder.setView(dialogView);
//        AlertDialog alertDialog = builder.create();
//
//        spinnerHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                selectedEventStartTime = timeHours.get(position);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//        spinnerMinute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                selectedEventEndTime = timeMinutes.get(position);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//        btnDone.setOnClickListener(v -> {
//            alertDialog.dismiss();
//            if (!TextUtils.isEmpty(eventTiming.toString())) {
//                eventTiming.append(", ");
//            }
//            eventTiming.append(selectedEventStartTime + "-" + selectedEventEndTime);
//            binding.etEventTiming.setText(eventTiming.toString());
//        });
//
//
//        alertDialog.show();
//    }
//
//    private void showDialogEventDates() {
//        Calendar calendar = Calendar.getInstance();
//        new DatePickerDialog(getActivity(), (view, year, month, dayOfMonth) -> {
//            Calendar calendar1 = Calendar.getInstance();
//            calendar1.set(Calendar.YEAR, year);
//            calendar1.set(Calendar.MONTH, month);
//            calendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
//            if (!TextUtils.isEmpty(eventDates.toString())) {
//                eventDates.append(",");
//            }
//            eventDates.append(dateFormat.format(calendar1.getTime()));
//            binding.etEventDates.setText(eventDates.toString());
//        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
//
//    }
//
//    private void showDialogSelectEventEndTime() {
//        DatePickerDialog entryDatePickerDialog = new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
//            final Calendar calendar1 = Calendar.getInstance();
//            calendar1.set(Calendar.YEAR, i);
//            calendar1.set(Calendar.MONTH, i1);
//            calendar1.set(Calendar.DAY_OF_MONTH, i2);
//
//
//            new TimePickerDialog(getActivity(), (timePicker, i3, i11) -> {
//
//                Calendar datetime = Calendar.getInstance();
//                Calendar calendar = Calendar.getInstance();
//                datetime.set(Calendar.YEAR, i);
//                datetime.set(Calendar.MONTH, i1);
//                datetime.set(Calendar.DAY_OF_MONTH, i2);
//                if (datetime.getTimeInMillis() >= calendar1.getTimeInMillis()) {
//                    calendar1.set(Calendar.HOUR_OF_DAY, i3);
//                    calendar1.set(Calendar.MINUTE, i11);
//
//                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault());
//                    binding.etEventEndTime.setText(dateFormat1.format(new Date(calendar1.getTimeInMillis())));
//                    eventSession.setEvent_session_ends_date_time(dateFormat.format(new Date(calendar1.getTimeInMillis())));
//                    selectedEndDateForMultiDate = dateFormat.format(new Date(calendar1.getTimeInMillis()));
//                    isDateChanged = true;
//                    binding.etEventDates.setText("");
//                    binding.etEventTiming.setText("");
//                } else {
//                    Toast.makeText(getActivity(), "Please select future date!", Toast.LENGTH_LONG).show();
//                }
//
//            }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE),
//                    false).show();
//        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),
//                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
//        entryDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//        entryDatePickerDialog.show();
//    }
//
//    private void showDialogSelectEventBeginsTime() {
//        DatePickerDialog entryDatePickerDialog = new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
//            final Calendar calendar1 = Calendar.getInstance();
//            calendar1.set(Calendar.YEAR, i);
//            calendar1.set(Calendar.MONTH, i1);
//            calendar1.set(Calendar.DAY_OF_MONTH, i2);
//
//
//            new TimePickerDialog(getActivity(), (timePicker, i3, i11) -> {
//
//                Calendar datetime = Calendar.getInstance();
//                Calendar calendar = Calendar.getInstance();
//                datetime.set(Calendar.YEAR, i);
//                datetime.set(Calendar.MONTH, i1);
//                datetime.set(Calendar.DAY_OF_MONTH, i2);
//                if (datetime.getTimeInMillis() >= calendar1.getTimeInMillis()) {
//                    calendar1.set(Calendar.HOUR_OF_DAY, i3);
//                    calendar1.set(Calendar.MINUTE, i11);
//
//                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault());
//                    binding.etEventBeginsTime.setText(dateFormat1.format(new Date(calendar1.getTimeInMillis())));
//                    eventSession.setEvent_session_begins_date_time(dateFormat.format(new Date(calendar1.getTimeInMillis())));
//                    eventSession.setYear(dateFormat.format(new Date(calendar1.getTimeInMillis())));
//                    selectedStartDateForMultiDate = dateFormat.format(new Date(calendar1.getTimeInMillis()));
//                    isDateChanged = true;
//                    binding.etEventEndTime.setText("");
//                    binding.etEventDates.setText("");
//                    binding.etEventTiming.setText("");
//                } else {
//                    Toast.makeText(getActivity(), "Please select future date!", Toast.LENGTH_LONG).show();
//                }
//
//            }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE),
//                    false).show();
//        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),
//                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
//        entryDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//        entryDatePickerDialog.show();
//    }
//
//    private void openSelectAddress() {
//        final FragmentTransaction ft = getFragmentManager().beginTransaction();
//        SelectAddressFragment pay = new SelectAddressFragment();
//        Bundle bundle = new Bundle();
//        bundle.putBoolean("isEvent", true);
//        bundle.putBoolean("isSession", true);
//        pay.setArguments(bundle);
//        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
//        ft.add(R.id.My_Container_1_ID, pay);
//        fragmentStack.lastElement().onPause();
//        ft.hide(fragmentStack.lastElement());
//        fragmentStack.push(pay);
//        ft.commitAllowingStateLoss();
//    }
//
//    @Override
//    protected void initViews(View v) {
//
//        eventAddresses.clear();
//        eventAddressAdapter = new EventAddressAdapter();
//        binding.rvEventAddress.setLayoutManager(new LinearLayoutManager(getActivity()));
//        binding.rvEventAddress.setAdapter(eventAddressAdapter);
//
//        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setCancelable(false);
//        progressDialog.setMessage("Loading...");
//
//        timeZones.addAll(Arrays.asList(getResources().getStringArray(R.array.timezone)));
//        ArrayAdapter<String> occupationAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, timeZones);
//        binding.spinnerTimeZone.setAdapter(occupationAdapter);
//        binding.spinnerTimeZone.setSelection(timeZones.indexOf("America/New_York"));
//
//    }
//
//    private class MultiDatesAdapter extends RecyclerView.Adapter<MultiDatesAdapter.DatesHolder> {
//        ArrayList<String> startHours = new ArrayList<>();
//        ArrayList<String> endHour = new ArrayList<>();
//        ArrayList<String> startMinutes = new ArrayList<>();
//        ArrayList<String> endMinutes = new ArrayList<>();
//        ArrayList<String> amPm = new ArrayList<>();
//
//        @NonNull
//        @Override
//        public MultiDatesAdapter.DatesHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i1) {
//            startHours.clear();
//            startHours.addAll(Arrays.asList(getResources().getStringArray(R.array.time_hours)));
//            endHour.clear();
//            endHour.addAll(Arrays.asList(getResources().getStringArray(R.array.time_hours)));
//            startMinutes.clear();
//            startMinutes.addAll(Arrays.asList(getResources().getStringArray(R.array.time_minutes)));
//            endMinutes.clear();
//            endMinutes.addAll(Arrays.asList(getResources().getStringArray(R.array.time_minutes)));
//
//            amPm.clear();
//            amPm.addAll(Arrays.asList(getResources().getStringArray(R.array.time_am_pm)));
//
//            return new MultiDatesAdapter.DatesHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_multi_date_time, viewGroup, false));
//        }
//
//        @Override
//        public void onBindViewHolder(@NonNull MultiDatesAdapter.DatesHolder holder, int i) {
//            MultiDate multiDate = multiDates.get(i);
//            if (multiDate != null) {
//                holder.tvDate.setText(multiDate.getDate().substring(0, 10));
//                holder.cbSelected.setChecked(multiDate.isSelected());
//
//                if (startHours.indexOf(DateTimeUtils.getHH(multiDate.getStartTime())) != -1) {
//                    holder.spinnerStartHour.setSelection(startHours.indexOf(DateTimeUtils.getHH(multiDate.getStartTime())));
//                }
//
//                if (startMinutes.indexOf(DateTimeUtils.getMM(multiDate.getStartTime())) != -1) {
//                    holder.spinnerStartMinute.setSelection(startMinutes.indexOf(DateTimeUtils.getMM(multiDate.getStartTime())));
//                }
//
//                if (endHour.indexOf(DateTimeUtils.getHH(multiDate.getEndTime())) != -1) {
//                    holder.spinnerEndHour.setSelection(endHour.indexOf(DateTimeUtils.getHH(multiDate.getEndTime())));
//                }
//
//                if (endMinutes.indexOf(DateTimeUtils.getMM(multiDate.getEndTime())) != -1) {
//                    holder.spinnerEndMinute.setSelection(endMinutes.indexOf(DateTimeUtils.getMM(multiDate.getEndTime())));
//                }
//
//                if (amPm.indexOf(DateTimeUtils.getAMPM(multiDate.getStartTime())) != -1) {
//                    holder.spinnerStartAMPM.setSelection(amPm.indexOf(DateTimeUtils.getAMPM(multiDate.getStartTime())));
//                }
//
//                if (amPm.indexOf(DateTimeUtils.getAMPM(multiDate.getEndTime())) != -1) {
//                    holder.spinnerEndAMPM.setSelection(amPm.indexOf(DateTimeUtils.getAMPM(multiDate.getEndTime())));
//                }
//
//                holder.spinnerStartHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                    @Override
//                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                        multiDates.get(i).setStartHour(startHours.get(position));
//                    }
//
//                    @Override
//                    public void onNothingSelected(AdapterView<?> parent) {
//
//                    }
//                });
//
//                holder.spinnerEndHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                    @Override
//                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                        multiDates.get(i).setEndHour(endHour.get(position));
//                    }
//
//                    @Override
//                    public void onNothingSelected(AdapterView<?> parent) {
//
//                    }
//                });
//
//                holder.spinnerStartMinute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                    @Override
//                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                        multiDates.get(i).setStartMinute(startMinutes.get(position));
//                    }
//
//                    @Override
//                    public void onNothingSelected(AdapterView<?> parent) {
//
//                    }
//                });
//
//                holder.spinnerEndMinute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                    @Override
//                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                        multiDates.get(i).setEndMinute(endMinutes.get(position));
//                    }
//
//                    @Override
//                    public void onNothingSelected(AdapterView<?> parent) {
//
//                    }
//                });
//
//                holder.spinnerStartAMPM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                    @Override
//                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                        multiDates.get(i).setStartAMPM(amPm.get(position));
//                    }
//
//                    @Override
//                    public void onNothingSelected(AdapterView<?> parent) {
//
//                    }
//                });
//
//                holder.spinnerEndAMPM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                    @Override
//                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                        multiDates.get(i).setEndAMPM(amPm.get(position));
//                    }
//
//                    @Override
//                    public void onNothingSelected(AdapterView<?> parent) {
//
//                    }
//                });
//
//                holder.cbSelected.setOnCheckedChangeListener((buttonView, isChecked) -> {
//                    multiDates.get(i).setSelected(isChecked);
//                });
//            }
//        }
//
//        @Override
//        public int getItemCount() {
//            return multiDates.size();
//        }
//
//        class DatesHolder extends RecyclerView.ViewHolder {
//            AppCompatCheckBox cbSelected;
//            TextView tvDate;
//            AppCompatSpinner spinnerStartHour, spinnerStartMinute, spinnerEndHour, spinnerEndMinute,
//                    spinnerStartAMPM, spinnerEndAMPM;
//
//            DatesHolder(@NonNull View itemView) {
//                super(itemView);
//
//                cbSelected = itemView.findViewById(R.id.cbSelected);
//                tvDate = itemView.findViewById(R.id.tvDate);
//                spinnerStartHour = itemView.findViewById(R.id.spinnerStartHour);
//                spinnerEndHour = itemView.findViewById(R.id.spinnerEndHour);
//                spinnerStartMinute = itemView.findViewById(R.id.spinnerStartMinute);
//                spinnerEndMinute = itemView.findViewById(R.id.spinnerEndMinute);
//                spinnerStartAMPM = itemView.findViewById(R.id.spinnerStartAMPM);
//                spinnerEndAMPM = itemView.findViewById(R.id.spinnerEndAMPM);
//
//                ArrayAdapter<String> timeStartHoursAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, startHours);
//                spinnerStartHour.setAdapter(timeStartHoursAdapter);
//
//                ArrayAdapter<String> timeStartMinuteAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, startMinutes);
//                spinnerStartMinute.setAdapter(timeStartMinuteAdapter);
//
//                ArrayAdapter<String> timeEndHourAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, endHour);
//                spinnerEndHour.setAdapter(timeEndHourAdapter);
//
//                ArrayAdapter<String> timeEndMinuteAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, endMinutes);
//                spinnerEndMinute.setAdapter(timeEndMinuteAdapter);
//
//                ArrayAdapter<String> amPmAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, amPm);
//                spinnerStartAMPM.setAdapter(amPmAdapter);
//
//                ArrayAdapter<String> endAmPmAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, amPm);
//                spinnerEndAMPM.setAdapter(endAmPmAdapter);
//
//
//            }
//        }
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        if (SessionsFragment.isSessionUpdated && getActivity() != null) {
//            getActivity().onBackPressed();
//        }
//    }
//
//    public class EventAddressAdapter extends RecyclerView.Adapter<EventAddressAdapter.EventAddressHolder> {
//
//        @NonNull
//        @Override
//        public EventAddressAdapter.EventAddressHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//            return new EventAddressHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_event_address,
//                    viewGroup, false));
//        }
//
//        @Override
//        public void onBindViewHolder(@NonNull EventAddressAdapter.EventAddressHolder eventAddressHolder, int i) {
//            EventAddress eventAddress = eventAddresses.get(i);
//            if (eventAddress != null) {
//                eventAddressHolder.tvAddress.setText(eventAddress.getEventAddress());
//            }
//        }
//
//        @Override
//        public int getItemCount() {
//            return eventAddresses.size();
//        }
//
//        class EventAddressHolder extends RecyclerView.ViewHolder {
//            TextView tvAddress;
//            ImageView ivBtnRemoveEventAddress;
//
//            EventAddressHolder(@NonNull View itemView) {
//                super(itemView);
//
//                tvAddress = itemView.findViewById(R.id.tvAddress);
//                ivBtnRemoveEventAddress = itemView.findViewById(R.id.ivBtnRemoveEventAddress);
//
//                ivBtnRemoveEventAddress.setOnClickListener(v -> {
//                    final int pos = getAdapterPosition();
//                    if (pos != RecyclerView.NO_POSITION) {
//                        eventAddresses.remove(pos);
//                        notifyItemRemoved(pos);
//                    }
//                });
//            }
//        }
//    }

    public static FragSessionLocationAddEditBinding binding;

    private ProgressDialog progressDialog;
    private StringBuilder eventDates = new StringBuilder();
    private StringBuilder eventTiming = new StringBuilder();
    private String selectedEventStartTime = "", selectedEventEndTime = "";
    public static ArrayList<String> selectedLocations = new ArrayList<>();
    private ArrayList<MultiDate> multiDates = new ArrayList<>();
    private String selectedStartDateForMultiDate = "", selectedEndDateForMultiDate = "";
    public static ArrayList<EventAddress> eventAddresses = new ArrayList<>();
    public static EventAddressAdapter eventAddressAdapter;
    AmazonS3 s3;
    TransferUtility transferUtility;
    private ArrayList<Upload> imageUploads = new ArrayList<>();

    private ArrayList<String> timeZones = new ArrayList<>();
    public static String selectedTimeZone = "";

    private boolean isEdit = false, isRepeat = false;
    private EventSession eventSession;
    private ArrayList<String> eventImageUploads = new ArrayList<>();
    private boolean isDateChanged = false;
    private String TAG = "#DEBUG SessionLocation";
    private Calendar beginTime = Calendar.getInstance();
    private Calendar endTime = Calendar.getInstance();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            isEdit = getArguments().getBoolean("isEdit", false);
            isRepeat = getArguments().getBoolean("isRepeat", false);
            eventSession = new Gson().fromJson(getArguments().getString("eventSession"), EventSession.class);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_session_location_add_edit, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        s3 = new AmazonS3Client(new BasicAWSCredentials("AKIAJNG22KFRVUAICSEA", "NSrQR/yAg52RPD3kp3FMb3NO+Vrxuty4iAwPU4th"));
        transferUtility = new TransferUtility(s3, getActivity().getApplicationContext());

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {

        if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_LOCATION) {
            binding.llEventLocationShow.setVisibility(View.GONE);
            binding.llSessionLocation.setVisibility(View.VISIBLE);

            binding.tvBtnEventLocationNext.setText(eventSession.isParking_allowed() ? "NEXT"
                    : isEdit ? "UPDATE" : "ADD");

        } else if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_VIRTUALLY) {
            binding.llEventLocationShow.setVisibility(View.VISIBLE);
            binding.llSessionLocation.setVisibility(View.GONE);

            binding.tvBtnEventLocationNext.setText(isEdit ? "UPDATE" : "ADD");

        } else if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_BOTH) {
            binding.llEventLocationShow.setVisibility(View.VISIBLE);
            binding.llSessionLocation.setVisibility(View.VISIBLE);

            binding.tvBtnEventLocationNext.setText(eventSession.isParking_allowed() ? "NEXT"
                    : isEdit ? "UPDATE" : "ADD");
        }

        if (isEdit) {
            if (eventSession != null) {

                if (eventSession.getEvent_session_address() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_address())) {
                    binding.etEventAddress.setText(eventSession.getEvent_session_address());
                    try {
                        JSONArray jsonArray = new JSONArray(eventSession.getEvent_session_address());
                        JSONArray jsonArrayLatLng = new JSONArray(eventSession.getLocation_lat_lng());
                        // eventAddresses.clear();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONArray jsonArray1 = jsonArray.getJSONArray(i);
                            EventAddress eventAddress = new EventAddress();
                            eventAddress.setEventAddress(jsonArray1.getString(0));

                            JSONArray jsonArray2 = jsonArrayLatLng.getJSONArray(i);
                            String[] latLng = jsonArray2.getString(0).split(",");
                            if (latLng != null && latLng.length > 1) {
                                eventAddress.setLat(Double.parseDouble(latLng[0]));
                                eventAddress.setLng(Double.parseDouble(latLng[1]));
                            }
                            Log.e("#DEBUG", "   EventAddress:  " + new Gson().toJson(eventAddress));
                            eventAddresses.add(eventAddress);
                        }
                        eventAddressAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (eventSession.getEvent_session_begins_date_time() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_begins_date_time())) {
                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault());
                        binding.etEventBeginsTime.setText(dateFormat1.format(dateFormat.parse(eventSession.getEvent_session_begins_date_time())));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    } // binding.etEventBeginsTime.setText(eventSession.getEvent_session_begins_date_time());
                    selectedStartDateForMultiDate = eventSession.getEvent_session_begins_date_time();
                }

                if (eventSession.getEvent_session_ends_date_time() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_ends_date_time())) {
                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault());
                        binding.etEventEndTime.setText(dateFormat1.format(dateFormat.parse(eventSession.getEvent_session_ends_date_time())));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }// binding.etEventEndTime.setText(eventSession.getEvent_session_ends_date_time());
                    selectedEndDateForMultiDate = eventSession.getEvent_session_ends_date_time();
                }

                if (eventSession.getEvent_session_timings() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_timings())) {
                    try {
                        JSONArray jsonArrayTime = null;
                        StringBuilder stringBuilder = new StringBuilder();
                        jsonArrayTime = new JSONArray(eventSession.getEvent_session_timings());
                        for (int i = 0; i < jsonArrayTime.length(); i++) {
                            JSONArray jsonArray2 = jsonArrayTime.getJSONArray(i);
                            if (i != 0) {
                                stringBuilder.append("\n");
                            }
                            if (jsonArray2 != null && jsonArray2.length() > 0) {
//                                    stringBuilder.append(jsonArray2.get(0).toString());
                                if (jsonArray2.get(0).toString().length() > 30) {
                                    String[] s = jsonArray2.get(0).toString().split(" - ");
                                    for (int j = 0; j < s.length; j++) {
                                        if (j != 0) {
                                            stringBuilder.append("-");
                                        }
                                        stringBuilder.append(DateTimeUtils.gmtToLocalDateAMPM(s[j]));
                                    }
                                } else {
                                    stringBuilder.append(jsonArray2.get(0).toString());
                                }
                            }
                        }
                        binding.etEventDates.setText(stringBuilder.toString());


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (eventSession.getCovered_locations() != null
                        && !TextUtils.isEmpty(eventSession.getCovered_locations())) {
                    String[] locations = eventSession.getCovered_locations().split(",");
                    selectedLocations.clear();
                    selectedLocations.addAll(Arrays.asList(locations));
                    binding.etCoveredLocations.setText(eventSession.getCovered_locations());
                }

                binding.switchShowEventLocation.setChecked(eventSession.isWeb_event_session_location_to_show());
            }
        } else {
            if (eventSession != null) {
                if (eventSession.getLocation_address() != null) {
                    binding.etEventAddress.setText(eventSession.getLocation_address());
                }

                if (eventSession.getCovered_locations() != null) {
                    binding.etCoveredLocations.setText(eventSession.getCovered_locations());
                }

                try {
                    JSONArray jsonArray = new JSONArray(eventSession.getEvent_session_address());
                    JSONArray jsonArrayLatLng = new JSONArray(eventSession.getLocation_lat_lng());
//                    eventAddresses.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONArray jsonArray1 = jsonArray.getJSONArray(i);
                        EventAddress eventAddress = new EventAddress();
                        eventAddress.setEventAddress(jsonArray1.getString(0));

                        JSONArray jsonArray2 = jsonArrayLatLng.getJSONArray(i);
                        String[] latLng = jsonArray2.getString(0).split(",");
                        if (latLng != null && latLng.length > 1) {
                            eventAddress.setLat(Double.parseDouble(latLng[0]));
                            eventAddress.setLng(Double.parseDouble(latLng[1]));
                        }
                        Log.e("#DEBUG", "   EventAddress:  " + new Gson().toJson(eventAddress));
                        eventAddresses.add(eventAddress);
                    }
                    eventAddressAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    protected void setListeners() {

        binding.tvBtnEventLocationNext.setOnClickListener(v -> {
            if (binding.tvBtnEventLocationNext.getText().toString().equals("NEXT")) {
                //Goto Parking Location
                if (isValidate()) {
                    Log.e("#DEBUG", "  isValid:  eventSession:  " + new Gson().toJson(eventSession));
                    Fragment fragment = new SessionParkingLocationAddEditFragment();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isEdit", isEdit);
                    bundle.putBoolean("isRepeat", isRepeat);
                    bundle.putString("eventSession", new Gson().toJson(eventSession));
                    fragment.setArguments(bundle);
                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                    ft.add(R.id.My_Container_1_ID, fragment);
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    fragmentStack.push(fragment);
                    ft.commitAllowingStateLoss();
                }
            } else {
                //Submit
                if (isValidate()) {
                    Log.e("#DEBUG", "  isValid:  eventSession:  " + new Gson().toJson(eventSession));
                    imageUploads.clear();
                    eventImageUploads.clear();
                    if (SessionImageAddEditFragment.isEventLogoChange && !TextUtils.isEmpty(eventSession.getEvent_session_logo())) {
                        File ImageFile = new File(eventSession.getEvent_session_logo());
                        File compressedImageFile = null;
                        try {
                            compressedImageFile = new Compressor(getActivity()).compressToFile(ImageFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Upload upload = new Upload();
                        upload.setType("Logo");
                        upload.setLink(String.valueOf(compressedImageFile));
                        imageUploads.add(upload);
                    }

                    for (int i = 1; i < eventSession.getEventImages().size(); i++) {
                        if (!eventSession.getEventImages().get(i).getPath().contains("parkezly-images")) {
                            File ImageFile = new File(eventSession.getEventImages().get(i).getPath());
                            File compressedImageFile = null;
                            try {
                                compressedImageFile = new Compressor(getActivity()).compressToFile(ImageFile);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            Upload upload = new Upload();
                            upload.setType("Image");
                            upload.setLink(String.valueOf(compressedImageFile));
                            imageUploads.add(upload);
                        } else {
                            eventImageUploads.add(eventSession.getEventImages().get(i).getPath());
                        }
                    }

                    if (imageUploads.size() > 0) {
                        upload_file();
                    } else {
                        new updateEventDetails().execute();
                    }

                }
            }
        });

        binding.spinnerTimeZone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedTimeZone = timeZones.get(position);
                Log.e("#DEBUG", "    selectedTimeZone:  " + selectedTimeZone);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.etEventAddress.setOnClickListener(v -> {
//            SessionAddMainViewFragment.binding.btnSelectAddress.performClick();
        });


        binding.ivBtnAddEventAddress.setOnClickListener(v -> {
            openSelectAddress();
        });

        binding.etEventBeginsTime.setOnClickListener(v -> {
            showDialogSelectEventBeginsTime();
        });

        binding.etEventEndTime.setOnClickListener(v -> {
            showDialogSelectEventEndTime();
        });

//        binding.ivBtnAddEventDates.setOnClickListener(v -> {
//            showDialogEventDates();
//        });

        binding.etEventDates.setOnClickListener(v -> {
            if (TextUtils.isEmpty(binding.etEventBeginsTime.getText())) {
                Toast.makeText(getActivity(), "Please select event begin time first!", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(binding.etEventEndTime.getText())) {
                Toast.makeText(getActivity(), "Please select event end time first!", Toast.LENGTH_SHORT).show();
            } else {
                multiDates.clear();
                ArrayList<String> dates = DateTimeUtils.getDates(selectedStartDateForMultiDate, selectedEndDateForMultiDate);
                for (int i = 0; i < dates.size(); i++) {
                    MultiDate multiDate = new MultiDate();
                    multiDate.setSelected(true);
                    multiDate.setDate(dates.get(i));
                    multiDate.setStartTime(selectedStartDateForMultiDate);
                    multiDate.setEndTime(selectedEndDateForMultiDate);
                    multiDates.add(multiDate);
                }
                showDialogSelectEventMultiDates();
            }
        });

        binding.ivBtnAddEventTiming.setOnClickListener(v -> {
            showDialogAddEventTiming();
        });

        binding.etEventTiming.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(binding.etEventTiming.getText())) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Are you sure you want to clear event timing?");
                builder.setPositiveButton("YES", (dialog, which) -> {
                    dialog.dismiss();
                    binding.etEventTiming.setText("");
                    eventTiming = new StringBuilder();
                });
                builder.setNegativeButton("NO", (dialog, which) -> {
                    dialog.dismiss();
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }
        });

        binding.etCoveredLocations.setOnClickListener(v -> {
            openCoveredLocation();
        });
    }

    private void openCoveredLocation() {
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        CoveredLocationsFragment frag = new CoveredLocationsFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isEdit", false);
        bundle.putBoolean("isSession", true);
        bundle.putString("selectedManager", new Gson().toJson(myApp.selectedManager));
        frag.setArguments(bundle);
        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.add(R.id.My_Container_1_ID, frag);
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(frag);
        ft.commitAllowingStateLoss();
    }

    public class updateEventDetails extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String exit_status = "_table/event_session_definitions";
        JSONObject json, json1;
        String re_id;
        String id;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues = new HashMap<String, Object>();

            if (isEdit) {
                jsonValues.put("id", eventSession.getId());
            }

            jsonValues.put("date_time", DateTimeUtils.getSqlFormatDate(Calendar.getInstance().getTime()));
            jsonValues.put("manager_type", eventSession.getManager_type());
            jsonValues.put("manager_type_id", eventSession.getManager_type_id());
            jsonValues.put("twp_id", eventSession.getTwp_id());
            jsonValues.put("township_code", eventSession.getTownship_code());
            jsonValues.put("township_name", eventSession.getTownship_name());
            jsonValues.put("company_id", eventSession.getCompany_id());
            jsonValues.put("company_code", eventSession.getCompany_code());
            jsonValues.put("company_name", eventSession.getCompany_name());

            jsonValues.put("event_id", eventSession.getEvent_id());
            jsonValues.put("event_type", eventSession.getEvent_type());
            jsonValues.put("event_name", eventSession.getEvent_name());
            //event_session_id
            jsonValues.put("event_session_type", eventSession.getEvent_session_type());
            //event_session_code
            jsonValues.put("event_session_name", eventSession.getEvent_session_name());
            jsonValues.put("event_session_short_description", eventSession.getEvent_session_short_description());
            jsonValues.put("event_session_long_description", eventSession.getEvent_session_long_description());
            jsonValues.put("event_session_link_on_web", eventSession.getEvent_session_link_on_web());
            jsonValues.put("event_session_link_ytube", eventSession.getEvent_session_link_ytube());
            jsonValues.put("event_session_link_zoom", eventSession.getEvent_session_link_zoom());
            jsonValues.put("event_sessionlink_googlemeet", eventSession.getEvent_session_link_googlemeet());
            jsonValues.put("event_session_link_googleclassroom", eventSession.getEvent_session_link_googleclassroom());
            jsonValues.put("event_session_link_facebook", eventSession.getEvent_session_link_facebook());
            jsonValues.put("event_session_link_twitter", eventSession.getEvent_session_link_twitter());
            jsonValues.put("event_session_link_whatsapp", eventSession.getEvent_session_link_whatsapp());
            jsonValues.put("event_session_link_other_media", eventSession.getEvent_session_link_other_media());
            jsonValues.put("company_logo", eventSession.getCompany_logo());
            jsonValues.put("event_logo", eventSession.getEvent_logo());
            jsonValues.put("event_session_logo", eventSession.getEvent_session_logo());
            jsonValues.put("event_session_image1", eventSession.getEvent_session_image1());
            jsonValues.put("event_session_image2", eventSession.getEvent_session_image2());
            jsonValues.put("event_session_blob_image", new Gson().toJson(eventImageUploads));
            jsonValues.put("event_session_address", eventSession.getEvent_session_address());
            jsonValues.put("covered_locations", eventSession.getCovered_locations());
            jsonValues.put("session_regn_needed_approval", eventSession.isSession_regn_needed_approval());
            jsonValues.put("requirements", eventSession.getRequirements());
            //appl_req_download
            jsonValues.put("cost", eventSession.getCost());
            jsonValues.put("year", eventSession.getYear());
            jsonValues.put("location_address", eventSession.getLocation_address());
            //scheme_type
            //event_prefix
            //event_session_prefix
            //event_nextnum
            //event_session_nextnum
            jsonValues.put("local_timezone", selectedTimeZone);
            jsonValues.put("local_time_event_session_start", eventSession.getEvent_session_begins_date_time());
            if (eventSession.getEvent_session_begins_date_time() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_begins_date_time())) {
                jsonValues.put("event_session_begins_date_time", DateTimeUtils.localToGMT(eventSession.getEvent_session_begins_date_time()));
            }
            if (eventSession.getEvent_session_ends_date_time() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_ends_date_time())) {
                jsonValues.put("event_session_ends_date_time", DateTimeUtils.localToGMT(eventSession.getEvent_session_ends_date_time()));
            }
            if (eventSession.getEvent_session_parking_begins_date_time() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_parking_begins_date_time())) {
                jsonValues.put("event_session_parking_begins_date_time", DateTimeUtils.localToGMT(eventSession.getEvent_session_parking_begins_date_time()));
            }
            if (eventSession.getEvent_session_parking_ends_date_time() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_parking_ends_date_time())) {
                jsonValues.put("event_session_parking_ends_date_time", DateTimeUtils.localToGMT(eventSession.getEvent_session_parking_ends_date_time()));
            }
            if (eventSession.getEvent_session_multi_dates() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_multi_dates())) {
                jsonValues.put("event_session_multi_dates", eventSession.getEvent_session_multi_dates());
            }
            if (eventSession.getEvent_session_parking_timings() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_parking_timings())) {
                jsonValues.put("event_session_parking_timings", eventSession.getEvent_session_parking_timings());
            }
            if (eventSession.getEvent_session_timings() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_timings())) {
                jsonValues.put("event_session_timings", eventSession.getEvent_session_timings());
            }
            if (eventSession.getEvent_session_parking_ends_date_time() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_parking_ends_date_time())) {
                jsonValues.put("expires_by", DateTimeUtils.localToGMT(eventSession.getEvent_session_parking_ends_date_time()));
            }
            jsonValues.put("parking_reservation_limit", eventSession.getParking_reservation_limit());
            jsonValues.put("regn_reqd", eventSession.isRegn_reqd());
            if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_VIRTUALLY) {
                jsonValues.put("is_parking_allowed", 0);
            } else {
                jsonValues.put("is_parking_allowed", eventSession.isParking_allowed() ? 1 : 0);
            }
            jsonValues.put("event_session_logi_type", eventSession.getEvent_session_logi_type());
            jsonValues.put("event_session_regn_allowed", eventSession.isEvent_session_regn_allowed());
            jsonValues.put("event_session_regn_limit", eventSession.getEvent_session_regn_limit());
            jsonValues.put("event_session_regn_fee", eventSession.getEvent_session_regn_fee());
            if (eventSession.getEvent_session_regn_fee() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_regn_fee()))
                jsonValues.put("event_session_regn_fee", eventSession.getEvent_session_regn_fee());
            if (eventSession.getEvent_session_youth_fee() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_youth_fee()))
                jsonValues.put("event_session_youth_fee", eventSession.getEvent_session_youth_fee());
            if (eventSession.getEvent_session_child_fee() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_child_fee()))
                jsonValues.put("event_session_child_fee", eventSession.getEvent_session_child_fee());
            if (eventSession.getEvent_session_student_fee() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_student_fee()))
                jsonValues.put("event_session_student_fee", eventSession.getEvent_session_student_fee());
            if (eventSession.getEvent_session_minister_fee() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_minister_fee()))
                jsonValues.put("event_session_minister_fee", eventSession.getEvent_session_minister_fee());
            if (eventSession.getEvent_session_clergy_fee() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_clergy_fee()))
                jsonValues.put("event_session_clergy_fee", eventSession.getEvent_session_clergy_fee());
            if (eventSession.getEvent_session_promo_fee() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_promo_fee()))
                jsonValues.put("event_session_promo_fee", eventSession.getEvent_session_promo_fee());
            if (eventSession.getEvent_session_senior_fee() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_senior_fee()))
                jsonValues.put("event_session_senior_fee", eventSession.getEvent_session_senior_fee());
            //event_session_family_fee
            if (eventSession.getEvent_session_staff_fee() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_staff_fee()))
                jsonValues.put("event_session_staff_fee", eventSession.getEvent_session_staff_fee());
            jsonValues.put("web_event_session_regn_fee", eventSession.getWeb_event_session_regn_fee());
            jsonValues.put("web_event_session_regn_limit", eventSession.getWeb_event_session_regn_limit());
            jsonValues.put("web_event_session_location_to_show", eventSession.isWeb_event_session_location_to_show() ? 1 : 0);
            if (eventSession.getEvent_session_parking_fee() != null
                    && !TextUtils.isEmpty(eventSession.getEvent_session_parking_fee()))
                jsonValues.put("event_session_parking_fee", eventSession.getEvent_session_parking_fee());
            jsonValues.put("event_session_led_by", eventSession.getEvent_session_led_by());
            jsonValues.put("event_session_leaders_bio", eventSession.getEvent_session_leaders_bio());
            jsonValues.put("regn_reqd_for_parking", eventSession.isRegn_reqd_for_parking());
            jsonValues.put("renewable", eventSession.isRenewable());
            jsonValues.put("free_session", eventSession.isFree_session() ? 1 : 0);
            jsonValues.put("reqd_local_session_regn", eventSession.isReqd_local_session_regn() ? 1 : 0);
            jsonValues.put("reqd_web_session_regn", eventSession.isReqd_web_session_regn() ? 1 : 0);
            jsonValues.put("free_local_session", eventSession.isFree_local_session() ? 1 : 0);
            jsonValues.put("free_web_session", eventSession.isFree_web_session());
            jsonValues.put("free_session_parking", eventSession.isFree_session_parking() ? 1 : 0);
            jsonValues.put("active", eventSession.isActive());
            if (!TextUtils.isEmpty(eventSession.getSession_link_array())) {
                jsonValues.put("session_link_array", eventSession.getSession_link_array());
            }
            //#####

            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpResponse response = null;
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (isEdit) {
                HttpPut post = new HttpPut(url);
                post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
                post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
                post.setEntity(entity);
                Log.e("#DEBUG", "  updateURL:  " + url);
                Log.e("#DEBUG", "   update Params: " + String.valueOf(json));
                try {
                    response = client.execute(post);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                HttpPost post = new HttpPost(url);
                post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
                post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
                post.setEntity(entity);
                Log.e("#DEBUG", "  updateURL:  " + url);
                Log.e("#DEBUG", "   update Params: " + String.valueOf(json));
                try {
                    response = client.execute(post);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   Add/Edit Event:  Response:  " + responseStr);
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.e("#DEBUG", "  update: ResponseID: " + re_id);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.hide();
            if (getActivity() != null && json1 != null) {
                if (re_id != null && !re_id.equals("null")) {
                    if (isEdit) {
                        SessionsFragment.isSessionUpdated = true;
                        Toast.makeText(getActivity(), "Updated!", Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();
                    } else {
                        new updateEventId().execute(re_id);
                    }
                }
                super.onPostExecute(s);
            }
        }
    }

    public class updateEventId extends AsyncTask<String, String, String> {
        String exit_status = "_table/event_session_definitions";
        JSONObject json, json1;
        String re_id;
        String id;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues = new HashMap<String, Object>();


            jsonValues.put("id", params[0]);
            jsonValues.put("event_session_id", params[0]);

            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpResponse response = null;
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            post.setEntity(entity);
            Log.e("#DEBUG", "  updateURL:  " + url);
            Log.e("#DEBUG", "   update Params: " + String.valueOf(json));
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.e("#DEBUG", "  update: ResponseID: " + re_id);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.hide();
            if (getActivity() != null && json1 != null) {
                if (re_id != null && !re_id.equals("null")) {
                    SessionsFragment.isSessionUpdated = true;
                    Toast.makeText(getActivity(), "Added!", Toast.LENGTH_SHORT).show();
                    getActivity().onBackPressed();
                }
                super.onPostExecute(s);
            }
        }
    }

    private boolean isValidate() {

        if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_LOCATION
                || eventSession.getEvent_session_logi_type() == EVENT_TYPE_BOTH) {

            if (eventAddresses != null && eventAddresses.size() == 0) {
                Toast.makeText(getActivity(), "Please select address!", Toast.LENGTH_SHORT).show();
                return false;
            } else if (eventAddresses != null) {
                ArrayList<EventAddress> eventAddresses = new ArrayList<>(this.eventAddresses);
                JSONArray jsonArrayAddress = new JSONArray();
                JSONArray jsonArrayLatLang = new JSONArray();
                for (int i = 0; i < eventAddresses.size(); i++) {
                    JSONArray jsonArray1 = new JSONArray();
                    jsonArray1.put(eventAddresses.get(i).getEventAddress());
                    jsonArrayAddress.put(jsonArray1);

                    JSONArray jsonArray2 = new JSONArray();
                    jsonArray2.put(eventAddresses.get(i).getLat() + "," + eventAddresses.get(i).getLng());
//                    if (i == 0) {
//                        eventSession.setLat(String.valueOf(eventAddresses.get(i).getLat()));
//                        eventSession.setLng(String.valueOf(eventAddresses.get(i).getLng()));
//                    }
                    jsonArrayLatLang.put(jsonArray2);
                }

                eventSession.setEvent_session_address(jsonArrayAddress.toString());
                eventSession.setLocation_address(jsonArrayAddress.toString());
                eventSession.setLocation_lat_lng(jsonArrayLatLang.toString());
            }
        }

        if (!TextUtils.isEmpty(binding.etCoveredLocations.getText())) {
            eventSession.setCovered_locations(binding.etCoveredLocations.getText().toString());
        }

        if (TextUtils.isEmpty(binding.etEventBeginsTime.getText())) {
            Toast.makeText(getActivity(), "Please select Event Begins Time!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(binding.etEventEndTime.getText())) {
            Toast.makeText(getActivity(), "Please select Event End Time!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!isEdit) {
            if (TextUtils.isEmpty(binding.etEventDates.getText())) {
                Toast.makeText(getActivity(), "Please select Event Date and Timing!", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (!TextUtils.isEmpty(binding.etEventTiming.getText())) {
                String eventMultiDay = binding.etEventTiming.getText().toString();
                String[] strings = eventMultiDay.split(", ");
                JSONArray jsonArray = new JSONArray();
                for (String string : strings) {
                    JSONArray jsonArray1 = new JSONArray();
                    jsonArray1.put(string);
                    jsonArray.put(jsonArray1);
                }
                eventSession.setEvent_session_timings(jsonArray.toString());
            } else {
                Toast.makeText(getActivity(), "Please select Event Timing!", Toast.LENGTH_SHORT).show();
                return false;
            }
        } else if (isDateChanged) {
            isDateChanged = false;
            if (TextUtils.isEmpty(binding.etEventDates.getText())) {
                Toast.makeText(getActivity(), "Please select Event Date and Timing!", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (!TextUtils.isEmpty(binding.etEventTiming.getText())) {
                String eventMultiDay = binding.etEventTiming.getText().toString();
                String[] strings = eventMultiDay.split(", ");
                JSONArray jsonArray = new JSONArray();
                for (String string : strings) {
                    JSONArray jsonArray1 = new JSONArray();
                    jsonArray1.put(string);
                    jsonArray.put(jsonArray1);
                }
                eventSession.setEvent_session_timings(jsonArray.toString());
            } else {
                Toast.makeText(getActivity(), "Please select Event Timing!", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        eventSession.setWeb_event_session_location_to_show(binding.switchShowEventLocation.isChecked());
        eventSession.setLocal_timezone(selectedTimeZone);
        return true;
    }

    String final_DivisionImage;
    StringBuffer file_DivisionImage;

    public void upload_file() {
        progressDialog.show();
        file_DivisionImage = new StringBuffer();
        String fileUrl = "";
        fileUrl = imageUploads.get(0).getLink();
        File file = new File(fileUrl);
        String namegsxsax = System.currentTimeMillis() + ".jpg";
        file_DivisionImage.append(namegsxsax + ",");
        namegsxsax = "/images/" + namegsxsax;
        final_DivisionImage = file_DivisionImage.substring(0, file_DivisionImage.length() - 1);
        TransferObserver transferObserver = transferUtility.upload("parkezly-images", namegsxsax, file);
        transferObserverListener(transferObserver);
    }

    public void transferObserverListener(final TransferObserver transferObserver) {
        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.name().equals("COMPLETED")) {
                    if (imageUploads.get(0).getType().equals("Logo")) {
                        eventSession.setEvent_session_logo("https://s3.amazonaws.com/parkezly-images//images/" + final_DivisionImage);
                    } else if (imageUploads.get(0).getType().equals("Image")) {
                        eventImageUploads.add("https://s3.amazonaws.com/parkezly-images//images/" + final_DivisionImage);
                    }
                    if (imageUploads.size() > 0) {
                        imageUploads.remove(0);
                        if (imageUploads.size() == 0) {
                            //Upload complete
                            new updateEventDetails().execute();
                        } else {
                            upload_file();
                        }
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                try {
                    int percentage = (int) (bytesCurrent / bytesTotal * 100);
                    Log.e("percentage ", " : " + percentage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", "error");
            }

        });
    }

    private void showDialogSelectEventMultiDates() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_multi_date_select, null);
        builder.setView(dialogView);
        TextView tvTitle = dialogView.findViewById(R.id.tvTitle);
        tvTitle.setText("Select Event Dates and Timing");
        RecyclerView rvMultiDates = dialogView.findViewById(R.id.rvMultiDates);
        MultiDatesAdapter multiDatesAdapter = new MultiDatesAdapter();
        rvMultiDates.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMultiDates.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        rvMultiDates.setAdapter(multiDatesAdapter);
        Button btnDone = dialogView.findViewById(R.id.btnDone);
        AlertDialog alertDialog = builder.create();

        btnDone.setOnClickListener(v -> {
            alertDialog.dismiss();
            Log.e("#DEBUG", "   MultiDates:  " + new Gson().toJson(multiDates));
            eventDates = new StringBuilder();
            eventTiming = new StringBuilder();
            StringBuilder displayText = new StringBuilder();
            for (int i = 0; i < multiDates.size(); i++) {
                if (multiDates.get(i).isSelected()) {
                    if (!TextUtils.isEmpty(eventDates.toString())) {
                        eventDates.append(", ");
                    }
                    eventDates.append(DateTimeUtils.localToGMTYYYYMMDDHHMMSS(multiDates.get(i).getDate()));
                    displayText.append(multiDates.get(i).getDate().substring(0, 10));

                    if (!TextUtils.isEmpty(eventTiming.toString())) {
                        eventTiming.append(", ");
                    }
                    eventTiming.append(DateTimeUtils.localToGMT_DD_HH_AM_PM(multiDates.get(i).getDate().substring(0, 10) + " "
                            + multiDates.get(i).getStartHour() + ":" + multiDates.get(i).getStartMinute() + " " + multiDates.get(i).getStartAMPM()));
                    eventTiming.append(" - ");
                    eventTiming.append(DateTimeUtils.localToGMT_DD_HH_AM_PM(multiDates.get(i).getDate().substring(0, 10) + " "
                            + multiDates.get(i).getEndHour() + ":" + multiDates.get(i).getEndMinute() + " " + multiDates.get(i).getEndAMPM()));

                    displayText.append("    (");
                    displayText.append(multiDates.get(i).getStartHour() + ":" + multiDates.get(i).getStartMinute() + " " + multiDates.get(i).getStartAMPM());
                    displayText.append(" - ");
                    displayText.append(multiDates.get(i).getEndHour() + ":" + multiDates.get(i).getEndMinute() + " " + multiDates.get(i).getEndAMPM());
                    displayText.append(")");
                    if (i == multiDates.size() - 1) {

                    } else {
                        displayText.append("\n");
                    }
                }
            }
            binding.etEventTiming.setText(eventTiming.toString());
            binding.etEventDates.setText(displayText.toString());

            String[] strings = eventDates.toString().split(",");
            JSONArray jsonArray = new JSONArray();
            for (String string : strings) {
                JSONArray jsonArray1 = new JSONArray();
                jsonArray1.put(string);
                jsonArray.put(jsonArray1);
            }
            eventSession.setEvent_session_multi_dates(jsonArray.toString());
        });


        alertDialog.show();
    }

    private void showDialogAddEventTiming() {
        selectedEventStartTime = "";
        selectedEventEndTime = "";
        ArrayList<String> timeHours = new ArrayList<>();
        ArrayList<String> timeMinutes = new ArrayList<>();

        timeHours.clear();
        for (int i = 0; i < 2; i++) {
            for (int j = 1; j < 13; j++) {
                if (i == 0) {
                    if (j == 12) {
                        timeHours.add(j + " PM");
                    } else {
                        timeHours.add(j + " AM");
                    }
                } else {
                    timeHours.add(j + " PM");
                }
            }
        }
        timeMinutes.clear();
        timeMinutes.addAll(timeHours);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_time_picker_dialog, null);
        AppCompatSpinner spinnerHour = dialogView.findViewById(R.id.spinnerHour);
        AppCompatSpinner spinnerMinute = dialogView.findViewById(R.id.spinnerMinute);

        ArrayAdapter<String> timeHoursAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, timeHours);
        spinnerHour.setAdapter(timeHoursAdapter);

        ArrayAdapter<String> timeMinutesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, timeMinutes);
        spinnerMinute.setAdapter(timeMinutesAdapter);

        Button btnDone = dialogView.findViewById(R.id.btnDone);

        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();

        spinnerHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedEventStartTime = timeHours.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerMinute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedEventEndTime = timeMinutes.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnDone.setOnClickListener(v -> {
            alertDialog.dismiss();
            if (!TextUtils.isEmpty(eventTiming.toString())) {
                eventTiming.append(", ");
            }
            eventTiming.append(selectedEventStartTime + "-" + selectedEventEndTime);
            binding.etEventTiming.setText(eventTiming.toString());
        });


        alertDialog.show();
    }

    private void showDialogEventDates() {
        Calendar calendar = Calendar.getInstance();
        new DatePickerDialog(getActivity(), (view, year, month, dayOfMonth) -> {
            Calendar calendar1 = Calendar.getInstance();
            calendar1.set(Calendar.YEAR, year);
            calendar1.set(Calendar.MONTH, month);
            calendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            if (!TextUtils.isEmpty(eventDates.toString())) {
                eventDates.append(",");
            }
            eventDates.append(dateFormat.format(calendar1.getTime()));
            binding.etEventDates.setText(eventDates.toString());
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();

    }

    /*    private void showDialogSelectEventEndTime() {
            DatePickerDialog entryDatePickerDialog = new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
                final Calendar calendar1 = Calendar.getInstance();
                calendar1.set(Calendar.YEAR, i);
                calendar1.set(Calendar.MONTH, i1);
                calendar1.set(Calendar.DAY_OF_MONTH, i2);


                new TimePickerDialog(getActivity(), (timePicker, i3, i11) -> {

                    Calendar datetime = Calendar.getInstance();
                    Calendar calendar = Calendar.getInstance();
                    datetime.set(Calendar.YEAR, i);
                    datetime.set(Calendar.MONTH, i1);
                    datetime.set(Calendar.DAY_OF_MONTH, i2);
                    if (datetime.getTimeInMillis() >= calendar1.getTimeInMillis()) {
                        calendar1.set(Calendar.HOUR_OF_DAY, i3);
                        calendar1.set(Calendar.MINUTE, i11);
                        calendar1.set(Calendar.SECOND, 00);
                        calendar1.set(Calendar.MILLISECOND, 00);

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault());
                        binding.etEventEndTime.setText(dateFormat1.format(new Date(calendar1.getTimeInMillis())));
                        eventSession.setEvent_session_ends_date_time(dateFormat.format(new Date(calendar1.getTimeInMillis())));
                        selectedEndDateForMultiDate = dateFormat.format(new Date(calendar1.getTimeInMillis()));
                        isDateChanged = true;
                        binding.etEventDates.setText("");
                        binding.etEventTiming.setText("");
                    } else {
                        Toast.makeText(getActivity(), "Please select future date!", Toast.LENGTH_LONG).show();
                    }

                }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE),
                        false).show();
            }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
            entryDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            entryDatePickerDialog.show();
        }
        private void showDialogSelectEventBeginsTime() {
            DatePickerDialog entryDatePickerDialog = new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
                final Calendar calendar1 = Calendar.getInstance();
                calendar1.set(Calendar.YEAR, i);
                calendar1.set(Calendar.MONTH, i1);
                calendar1.set(Calendar.DAY_OF_MONTH, i2);


                new TimePickerDialog(getActivity(), (timePicker, i3, i11) -> {

                    Calendar datetime = Calendar.getInstance();
                    Calendar calendar = Calendar.getInstance();
                    datetime.set(Calendar.YEAR, i);
                    datetime.set(Calendar.MONTH, i1);
                    datetime.set(Calendar.DAY_OF_MONTH, i2);
                    if (datetime.getTimeInMillis() >= calendar1.getTimeInMillis()) {
                        calendar1.set(Calendar.HOUR_OF_DAY, i3);
                        calendar1.set(Calendar.MINUTE, i11);
                        calendar1.set(Calendar.SECOND, 00);
                        calendar1.set(Calendar.MILLISECOND, 00);

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault());
                        binding.etEventBeginsTime.setText(dateFormat1.format(new Date(calendar1.getTimeInMillis())));
                        eventSession.setEvent_session_begins_date_time(dateFormat.format(new Date(calendar1.getTimeInMillis())));
                        eventSession.setYear(dateFormat.format(new Date(calendar1.getTimeInMillis())));
                        selectedStartDateForMultiDate = dateFormat.format(new Date(calendar1.getTimeInMillis()));
                        isDateChanged = true;
                        binding.etEventEndTime.setText("");
                        binding.etEventDates.setText("");
                        binding.etEventTiming.setText("");
                    } else {
                        Toast.makeText(getActivity(), "Please select future date!", Toast.LENGTH_LONG).show();
                    }

                }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE),
                        false).show();
            }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
            entryDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            entryDatePickerDialog.show();
        }*/
    private void showDialogSelectEventEndTime() {
        DatePickerDialog entryDatePickerDialog = new DatePickerDialog(getActivity(), (datePicker, yy, mm, dd) -> {
            final Calendar calendar1 = Calendar.getInstance();
            calendar1.set(Calendar.YEAR, yy);
            calendar1.set(Calendar.MONTH, mm);
            calendar1.set(Calendar.DAY_OF_MONTH, dd);
            calendar1.set(Calendar.SECOND, 0);

            new TimePickerDialog(getActivity(), (timePicker, hour, min) -> {
                Calendar calendar = Calendar.getInstance();
                calendar1.set(Calendar.HOUR_OF_DAY, hour);
                calendar1.set(Calendar.MINUTE, min);
                calendar1.set(Calendar.MILLISECOND, 0);
                if (calendar.getTimeInMillis() < calendar1.getTimeInMillis()) {
                    if (beginTime.getTimeInMillis() < calendar1.getTimeInMillis()) {
                        endTime = calendar1;
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault());
                        binding.etEventEndTime.setText(dateFormat1.format(new Date(calendar1.getTimeInMillis())));
                        eventSession.setEvent_session_ends_date_time(dateFormat.format(new Date(calendar1.getTimeInMillis())));
                        selectedEndDateForMultiDate = dateFormat.format(new Date(calendar1.getTimeInMillis()));
                        isDateChanged = true;
                        binding.etEventDates.setText("");
                        binding.etEventTiming.setText("");
                    } else
                        Toast.makeText(getActivity(), "Please select future date from begin date!", Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(getActivity(), "Please select future date!", Toast.LENGTH_LONG).show();
            }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), false).show();
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        entryDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        entryDatePickerDialog.show();
    }
    private void showDialogSelectEventBeginsTime() {
        DatePickerDialog entryDatePickerDialog = new DatePickerDialog(getActivity(), (datePicker, yy, mm, dd) -> {
            final Calendar calendar1 = Calendar.getInstance();
            calendar1.set(Calendar.YEAR, yy);
            calendar1.set(Calendar.MONTH, mm);
            calendar1.set(Calendar.DAY_OF_MONTH, dd);
            new TimePickerDialog(getActivity(), (timePicker, hour, min) -> {
                calendar1.set(Calendar.HOUR_OF_DAY, hour);
                calendar1.set(Calendar.MINUTE, min);
                calendar1.set(Calendar.MILLISECOND, 0);
                Calendar calendar = Calendar.getInstance();
                if (calendar.getTimeInMillis() < calendar1.getTimeInMillis()) {
                    beginTime = calendar1;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault());
                    binding.etEventBeginsTime.setText(dateFormat1.format(new Date(calendar1.getTimeInMillis())));
                    eventSession.setEvent_session_begins_date_time(dateFormat.format(new Date(calendar1.getTimeInMillis())));
                    eventSession.setYear(dateFormat.format(new Date(calendar1.getTimeInMillis())));
                    selectedStartDateForMultiDate = dateFormat.format(new Date(calendar1.getTimeInMillis()));
                    isDateChanged = true;
                    binding.etEventEndTime.setText("");
                    binding.etEventDates.setText("");
                    binding.etEventTiming.setText("");
                } else
                    Toast.makeText(getActivity(), "Please select future date!", Toast.LENGTH_LONG).show();
            }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), false).show();
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        entryDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        entryDatePickerDialog.show();
    }

    private void openSelectAddress() {
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        SelectAddressFragment pay = new SelectAddressFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isEvent", true);
        bundle.putBoolean("isSession", true);
        pay.setArguments(bundle);
        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.add(R.id.My_Container_1_ID, pay);
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(pay);
        ft.commitAllowingStateLoss();
    }

    @Override
    protected void initViews(View v) {

//        eventAddresses.clear();
        eventAddressAdapter = new EventAddressAdapter();
        binding.rvEventAddress.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvEventAddress.setAdapter(eventAddressAdapter);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");

        timeZones.addAll(Arrays.asList(getResources().getStringArray(R.array.timezone)));
        ArrayAdapter<String> occupationAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, timeZones);
        binding.spinnerTimeZone.setAdapter(occupationAdapter);
        binding.spinnerTimeZone.setSelection(timeZones.indexOf("America/New_York"));

    }

    private class MultiDatesAdapter extends RecyclerView.Adapter<MultiDatesAdapter.DatesHolder> {
        ArrayList<String> startHours = new ArrayList<>();
        ArrayList<String> endHour = new ArrayList<>();
        ArrayList<String> startMinutes = new ArrayList<>();
        ArrayList<String> endMinutes = new ArrayList<>();
        ArrayList<String> amPm = new ArrayList<>();

        @NonNull
        @Override
        public MultiDatesAdapter.DatesHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i1) {
            startHours.clear();
            startHours.addAll(Arrays.asList(getResources().getStringArray(R.array.time_hours)));
            endHour.clear();
            endHour.addAll(Arrays.asList(getResources().getStringArray(R.array.time_hours)));
            startMinutes.clear();
            startMinutes.addAll(Arrays.asList(getResources().getStringArray(R.array.time_minutes)));
            endMinutes.clear();
            endMinutes.addAll(Arrays.asList(getResources().getStringArray(R.array.time_minutes)));

            amPm.clear();
            amPm.addAll(Arrays.asList(getResources().getStringArray(R.array.time_am_pm)));

            return new MultiDatesAdapter.DatesHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_multi_date_time, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MultiDatesAdapter.DatesHolder holder, int i) {
            MultiDate multiDate = multiDates.get(i);
            if (multiDate != null) {
                holder.tvDate.setText(multiDate.getDate().substring(0, 10));
                holder.cbSelected.setChecked(multiDate.isSelected());

                if (startHours.indexOf(DateTimeUtils.getHH(multiDate.getStartTime())) != -1) {
                    holder.spinnerStartHour.setSelection(startHours.indexOf(DateTimeUtils.getHH(multiDate.getStartTime())));
                }

                if (startMinutes.indexOf(DateTimeUtils.getMM(multiDate.getStartTime())) != -1) {
                    holder.spinnerStartMinute.setSelection(startMinutes.indexOf(DateTimeUtils.getMM(multiDate.getStartTime())));
                }

                if (endHour.indexOf(DateTimeUtils.getHH(multiDate.getEndTime())) != -1) {
                    holder.spinnerEndHour.setSelection(endHour.indexOf(DateTimeUtils.getHH(multiDate.getEndTime())));
                }

                if (endMinutes.indexOf(DateTimeUtils.getMM(multiDate.getEndTime())) != -1) {
                    holder.spinnerEndMinute.setSelection(endMinutes.indexOf(DateTimeUtils.getMM(multiDate.getEndTime())));
                }

                if (amPm.indexOf(DateTimeUtils.getAMPM(multiDate.getStartTime())) != -1) {
                    holder.spinnerStartAMPM.setSelection(amPm.indexOf(DateTimeUtils.getAMPM(multiDate.getStartTime())));
                }

                if (amPm.indexOf(DateTimeUtils.getAMPM(multiDate.getEndTime())) != -1) {
                    holder.spinnerEndAMPM.setSelection(amPm.indexOf(DateTimeUtils.getAMPM(multiDate.getEndTime())));
                }

                holder.spinnerStartHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        multiDates.get(i).setStartHour(startHours.get(position));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                holder.spinnerEndHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        multiDates.get(i).setEndHour(endHour.get(position));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                holder.spinnerStartMinute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        multiDates.get(i).setStartMinute(startMinutes.get(position));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                holder.spinnerEndMinute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        multiDates.get(i).setEndMinute(endMinutes.get(position));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                holder.spinnerStartAMPM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        multiDates.get(i).setStartAMPM(amPm.get(position));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                holder.spinnerEndAMPM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        multiDates.get(i).setEndAMPM(amPm.get(position));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                holder.cbSelected.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    multiDates.get(i).setSelected(isChecked);
                });
            }
        }

        @Override
        public int getItemCount() {
            return multiDates.size();
        }

        class DatesHolder extends RecyclerView.ViewHolder {
            AppCompatCheckBox cbSelected;
            TextView tvDate;
            AppCompatSpinner spinnerStartHour, spinnerStartMinute, spinnerEndHour, spinnerEndMinute,
                    spinnerStartAMPM, spinnerEndAMPM;

            DatesHolder(@NonNull View itemView) {
                super(itemView);

                cbSelected = itemView.findViewById(R.id.cbSelected);
                tvDate = itemView.findViewById(R.id.tvDate);
                spinnerStartHour = itemView.findViewById(R.id.spinnerStartHour);
                spinnerEndHour = itemView.findViewById(R.id.spinnerEndHour);
                spinnerStartMinute = itemView.findViewById(R.id.spinnerStartMinute);
                spinnerEndMinute = itemView.findViewById(R.id.spinnerEndMinute);
                spinnerStartAMPM = itemView.findViewById(R.id.spinnerStartAMPM);
                spinnerEndAMPM = itemView.findViewById(R.id.spinnerEndAMPM);

                ArrayAdapter<String> timeStartHoursAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, startHours);
                spinnerStartHour.setAdapter(timeStartHoursAdapter);

                ArrayAdapter<String> timeStartMinuteAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, startMinutes);
                spinnerStartMinute.setAdapter(timeStartMinuteAdapter);

                ArrayAdapter<String> timeEndHourAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, endHour);
                spinnerEndHour.setAdapter(timeEndHourAdapter);

                ArrayAdapter<String> timeEndMinuteAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, endMinutes);
                spinnerEndMinute.setAdapter(timeEndMinuteAdapter);

                ArrayAdapter<String> amPmAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, amPm);
                spinnerStartAMPM.setAdapter(amPmAdapter);

                ArrayAdapter<String> endAmPmAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, amPm);
                spinnerEndAMPM.setAdapter(endAmPmAdapter);


            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SessionsFragment.isSessionUpdated)
            if (getActivity() != null) getActivity().onBackPressed();
    }

    public class EventAddressAdapter extends RecyclerView.Adapter<EventAddressAdapter.EventAddressHolder> {

        @NonNull
        @Override
        public EventAddressAdapter.EventAddressHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new EventAddressHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_event_address,
                    viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull EventAddressAdapter.EventAddressHolder eventAddressHolder, int i) {
            EventAddress eventAddress = eventAddresses.get(i);
            if (eventAddress != null) {
                eventAddressHolder.tvAddress.setText(eventAddress.getEventAddress());
            }
        }

        @Override
        public int getItemCount() {
            return eventAddresses.size();
        }

        class EventAddressHolder extends RecyclerView.ViewHolder {
            TextView tvAddress;
            ImageView ivBtnRemoveEventAddress;

            EventAddressHolder(@NonNull View itemView) {
                super(itemView);

                tvAddress = itemView.findViewById(R.id.tvAddress);
                ivBtnRemoveEventAddress = itemView.findViewById(R.id.ivBtnRemoveEventAddress);

                ivBtnRemoveEventAddress.setOnClickListener(v -> {
                    final int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        eventAddresses.remove(pos);
                        notifyItemRemoved(pos);
                    }
                });
            }
        }
    }
}
