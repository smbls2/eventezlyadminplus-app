package com.softmeasures.eventezlyadminplus.frament.event.AddEdit;



import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragEventSettingsAdvancedAddEditBinding;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;


import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.frament.event.AllEventsFragment.isUpdated;


public class EventSettingsAdvancedAddEditFragment extends BaseFragment {

    public FragEventSettingsAdvancedAddEditBinding binding;

    private boolean isEdit = false, isRepeat = false;
    private EventDefinition eventDefinition;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            isEdit = getArguments().getBoolean("isEdit", false);
            isRepeat = getArguments().getBoolean("isRepeat", false);
            if (getArguments().containsKey("eventDefinition")) {
                eventDefinition = new Gson().fromJson(getArguments().getString("eventDefinition"), EventDefinition.class);
            } else {
                eventDefinition = new EventDefinition();
                eventDefinition.setIs_parking_allowed(true);
            }
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_event_settings_advanced_add_edit, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {
        if (isEdit && eventDefinition != null) {


            binding.switchRenewable.setChecked(eventDefinition.isRenewable());

            binding.switchShowPastInviteOnly.setChecked(eventDefinition.isShow_past_invite_only());
            binding.switchShowPastRegdOnly.setChecked(eventDefinition.isShow_past_regd_only());
            binding.switchShowLiveInviteOnly.setChecked(eventDefinition.isShow_live_invite_only());
            binding.switchShowLiveRegOnly.setChecked(eventDefinition.isShow_live_regd_only());
            binding.switchEventInviteOnly.setChecked(eventDefinition.isEvent_invite_only());
            binding.switchEventRegInviteOnly.setChecked(eventDefinition.isEvent_regn_invite_only());
            binding.switchGeoAllowCountries.setChecked(eventDefinition.isGeo_allow_countres());
            binding.switchGeoAllowState.setChecked(eventDefinition.isGeo_allow_states());
            binding.switchGeoAllowLatLang.setChecked(eventDefinition.isGeo_allow_lat_lng());
            binding.switchGeoRestrictCountries.setChecked(eventDefinition.isGeo_restrict_countries());
            binding.switchGeoRestrictState.setChecked(eventDefinition.isGeo_restrict_states());
            binding.switchGeoRestrictLatLang.setChecked(eventDefinition.isGeo_restrict_lat_lng());
            //Audio Video LiveChat
            binding.switchAudioConference.setChecked(eventDefinition.isAllow_audio_conf());
            binding.switchAudioConditions.setChecked(eventDefinition.isAllow_audio_condition_no());
            binding.switchAudioConfRecoding.setChecked(eventDefinition.isAllow_audio_conf_rec());
            binding.switchAudioRecConditions.setChecked(eventDefinition.isAllow_audio_rec_condition());
            binding.switchVideoConference.setChecked(eventDefinition.isAllow_video_conf());
            binding.switchVideoConditions.setChecked(eventDefinition.isAllow_video_condition_no());
            binding.switchVideoConfRecoding.setChecked(eventDefinition.isAllow_video_conf_rec());
            binding.switchVideoRecConditions.setChecked(eventDefinition.isAllow_video_rec_condition());
            binding.switchLivechat.setChecked(eventDefinition.isAllow_livechat());
            binding.switchLivechatCond.setChecked(eventDefinition.isAllow_livechat_condition_no());
        }
    }

    @Override
    protected void setListeners() {

        binding.tvBtnNext.setOnClickListener(v -> {
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            Fragment fragment = new EventInfoAddEditFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isEdit", isEdit);
            bundle.putBoolean("isRepeat", isRepeat);
            bundle.putString("eventDefinition", new Gson().toJson(eventDefinition));
            fragment.setArguments(bundle);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, fragment);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(fragment);
            ft.commitAllowingStateLoss();
        });

        //Is Renewable?
        binding.switchRenewable.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setRenewable(isChecked);
        });

        //EVENT REGISTERING RESTRICTIONS
        binding.switchEventInviteOnly.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setEvent_invite_only(isChecked);
        });
        binding.switchEventRegInviteOnly.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setEvent_regn_invite_only(isChecked);
        });
        //EVENT WATCHING RESTRICTIONS
        binding.switchShowLiveRegOnly.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setShow_live_regd_only(isChecked);
        });
        binding.switchShowLiveInviteOnly.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setShow_live_invite_only(isChecked);
        });
        binding.switchShowPastRegdOnly.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setShow_past_regd_only(isChecked);
        });
        binding.switchShowPastInviteOnly.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setShow_past_invite_only(isChecked);
        });
        //GEOGRAPHICAL RESTRICTIONS
        binding.switchGeoAllowCountries.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setGeo_allow_countres(isChecked);
        });
        binding.switchGeoRestrictCountries.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setGeo_restrict_countries(isChecked);
        });
        binding.switchGeoAllowState.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setGeo_allow_states(isChecked);
        });
        binding.switchGeoRestrictState.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setGeo_restrict_states(isChecked);
        });
        binding.switchGeoAllowLatLang.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setGeo_allow_lat_lng(isChecked);
        });
        binding.switchGeoRestrictLatLang.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setGeo_restrict_lat_lng(isChecked);
        });
        //Audio Video LiveChat
        binding.switchAudioConference.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setAllow_audio_conf(isChecked);
        });
        binding.switchAudioConditions.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setAllow_audio_condition_no(isChecked);
        });
        binding.switchAudioConfRecoding.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setAllow_audio_conf_rec(isChecked);
        });
        binding.switchAudioRecConditions.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setAllow_audio_rec_condition(isChecked);
        });
        binding.switchVideoConference.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setAllow_video_conf(isChecked);
        });
        binding.switchVideoConditions.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setAllow_video_condition_no(isChecked);
        });
        binding.switchVideoConfRecoding.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setAllow_video_conf_rec(isChecked);
        });
        binding.switchVideoRecConditions.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setAllow_video_rec_condition(isChecked);
        });
        binding.switchLivechat.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setAllow_livechat(isChecked);
        });
        binding.switchLivechatCond.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setAllow_livechat_condition_no(isChecked);
        });
    }

    @Override
    protected void initViews(View v) {
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isUpdated)
            if (getActivity() != null) getActivity().onBackPressed();
    }
}
