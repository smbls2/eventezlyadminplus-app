package com.softmeasures.eventezlyadminplus.frament.event.AddEdit;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragSessionPricingLimitBinding;
import com.softmeasures.eventezlyadminplus.frament.event.session.SessionsFragment;
import com.softmeasures.eventezlyadminplus.models.EventSession;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_LOCATION;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_VIRTUALLY;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_BOTH;

public class SessionPricingLimitFragment extends BaseFragment {

//    public FragSessionPricingLimitBinding binding;
//    private boolean isEdit = false, isRepeat = false;
//    private EventSession eventSession;
//
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        if (getArguments() != null) {
//            isEdit = getArguments().getBoolean("isEdit", false);
//            isRepeat = getArguments().getBoolean("isRepeat", false);
//            eventSession = new Gson().fromJson(getArguments().getString("eventSession"), EventSession.class);
//        }
//        binding = DataBindingUtil.inflate(inflater, R.layout.frag_session_pricing_limit, container, false);
//        return binding.getRoot();
//    }
//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        initViews(view);
//        updateViews();
//        setListeners();
//    }
//
//    @Override
//    protected void updateViews() {
//
//        if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_VIRTUALLY) {
//            binding.llWebEventPrice.setVisibility(View.VISIBLE);
//            binding.llLocationEventPrice.setVisibility(View.GONE);
//            binding.llParkingPrice.setVisibility(View.GONE);
//
//        } else if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_LOCATION) {
//            binding.llWebEventPrice.setVisibility(View.GONE);
//            if (eventSession.isFree_local_session()) {
//                binding.llLocationEventPrice.setVisibility(View.GONE);
//            } else {
//                binding.llLocationEventPrice.setVisibility(View.VISIBLE);
//            }
//            if (eventSession != null && eventSession.isIs_parking_allowed()) {
//                binding.llParkingPrice.setVisibility(View.VISIBLE);
//            } else {
//                binding.llParkingPrice.setVisibility(View.GONE);
//            }
//        } else if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_BOTH) {
//            binding.llWebEventPrice.setVisibility(View.VISIBLE);
//            if (eventSession.isFree_local_session()) {
//                binding.llLocationEventPrice.setVisibility(View.GONE);
//            } else {
//                binding.llLocationEventPrice.setVisibility(View.VISIBLE);
//            }
//            if (eventSession != null && eventSession.isIs_parking_allowed()) {
//                binding.llParkingPrice.setVisibility(View.VISIBLE);
//            } else {
//                binding.llParkingPrice.setVisibility(View.GONE);
//            }
//        }
//
//        if (eventSession.getEvent_session_parking_fee() != null
//                && !TextUtils.isEmpty(eventSession.getEvent_session_parking_fee())) {
//            binding.etParkingCost.setText(eventSession.getEvent_session_parking_fee());
//        }
//
//        if (eventSession.getEvent_session_regn_fee() != null
//                && !TextUtils.isEmpty(eventSession.getEvent_session_regn_fee())) {
//            binding.etEventFullFees.setText(eventSession.getEvent_session_regn_fee());
//        }
//
//        if (eventSession.getCost() != null
//                && !TextUtils.isEmpty(eventSession.getCost())) {
//            binding.etEventCost.setText(eventSession.getCost());
//        }
//
//        if (eventSession.getEvent_session_youth_fee() != null
//                && !TextUtils.isEmpty(eventSession.getEvent_session_youth_fee())) {
//            binding.etEventYouthFee.setText(eventSession.getEvent_session_youth_fee());
//        }
//
//        if (eventSession.getEvent_session_child_fee() != null
//                && !TextUtils.isEmpty(eventSession.getEvent_session_child_fee())) {
//            binding.etEventChildFee.setText(eventSession.getEvent_session_child_fee());
//        }
//
//        if (eventSession.getEvent_session_student_fee() != null
//                && !TextUtils.isEmpty(eventSession.getEvent_session_student_fee())) {
//            binding.etEventStudentFee.setText(eventSession.getEvent_session_student_fee());
//        }
//
//        if (eventSession.getEvent_session_minister_fee() != null
//                && !TextUtils.isEmpty(eventSession.getEvent_session_minister_fee())) {
//            binding.etEventMinisterFee.setText(eventSession.getEvent_session_minister_fee());
//        }
//
//        if (eventSession.getEvent_session_clergy_fee() != null
//                && !TextUtils.isEmpty(eventSession.getEvent_session_clergy_fee())) {
//            binding.etEventClergyFee.setText(eventSession.getEvent_session_clergy_fee());
//        }
//
//        if (eventSession.getEvent_session_promo_fee() != null
//                && !TextUtils.isEmpty(eventSession.getEvent_session_promo_fee())) {
//            binding.etEventPromoFee.setText(eventSession.getEvent_session_promo_fee());
//        }
//
//        if (eventSession.getEvent_session_senior_fee() != null
//                && !TextUtils.isEmpty(eventSession.getEvent_session_senior_fee())) {
//            binding.etEventSeniorFee.setText(eventSession.getEvent_session_senior_fee());
//        }
//
//        if (eventSession.getEvent_session_staff_fee() != null
//                && !TextUtils.isEmpty(eventSession.getEvent_session_staff_fee())) {
//            binding.etEventStaffFee.setText(eventSession.getEvent_session_staff_fee());
//        }
//
//        if (eventSession.getWeb_event_session_regn_fee() != null
//                && !TextUtils.isEmpty(eventSession.getWeb_event_session_regn_fee())) {
//            binding.etWebEventFullRegFee.setText(eventSession.getWeb_event_session_regn_fee());
//        }
//
//    }
//
//    @Override
//    protected void setListeners() {
//
//        binding.tvBtnNext.setOnClickListener(v -> {
//
//            if (validate()) {
//                Fragment fragment = null;
//                if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_LOCATION) {
//                    fragment = new SessionLocationAddEditFragment();
//                } else {
//                    fragment = new SessionMediaAddEditFragment();
//                }
//
//                final FragmentTransaction ft = getFragmentManager().beginTransaction();
//                Bundle bundle = new Bundle();
//                bundle.putBoolean("isEdit", isEdit);
//                bundle.putBoolean("isRepeat", isRepeat);
//                bundle.putString("eventSession", new Gson().toJson(eventSession));
//                fragment.setArguments(bundle);
//                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
//                ft.add(R.id.My_Container_1_ID, fragment);
//                fragmentStack.lastElement().onPause();
//                ft.hide(fragmentStack.lastElement());
//                fragmentStack.push(fragment);
//                ft.commitAllowingStateLoss();
//            }
//
//        });
//
//    }
//
//    private boolean validate() {
//        if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_VIRTUALLY) {
//            if (!eventSession.isFree_local_session()) {
//                if (!checkWebFields()) return false;
//            }
//        } else if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_LOCATION) {
//            if (!checkLocationFields()) return false;
//        } else if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_BOTH) {
//            if (!checkLocationFields()) return false;
//            if (!eventSession.isFree_local_session()) {
//                if (!checkWebFields()) return false;
//            }
//        }
//        return true;
//    }
//
//    private boolean checkWebFields() {
//        if (!TextUtils.isEmpty(binding.etWebEventFullRegFee.getText())) {
//            eventSession.setWeb_event_session_regn_fee(String.valueOf(binding.etWebEventFullRegFee.getText()));
//        } else {
//            binding.etWebEventFullRegFee.setError("Required");
//            binding.etWebEventFullRegFee.requestFocus();
//            return false;
//        }
//        return true;
//    }
//
//    private boolean checkLocationFields() {
//
//        if (eventSession.isFree_session()) {
//            eventSession.setEvent_session_regn_fee("0");
//            eventSession.setEvent_session_youth_fee("0");
//            eventSession.setEvent_session_child_fee("0");
//            eventSession.setEvent_session_student_fee("0");
//            eventSession.setEvent_session_staff_fee("0");
//            eventSession.setEvent_session_minister_fee("0");
//            eventSession.setEvent_session_clergy_fee("0");
//            eventSession.setEvent_session_promo_fee("0");
//            eventSession.setEvent_session_senior_fee("0");
//        } else {
//            if (!TextUtils.isEmpty(binding.etEventCost.getText())) {
//                eventSession.setCost(String.format("%s", binding.etEventCost.getText().toString()));
//            } else {
//                binding.etEventCost.setError("Required");
//                binding.etEventCost.requestFocus();
//                return false;
//            }
//
//            if (!TextUtils.isEmpty(binding.etEventFullFees.getText())) {
//                eventSession.setEvent_session_regn_fee(binding.etEventFullFees.getText().toString());
//            } else {
//                binding.etEventFullFees.setError("Required");
//                binding.etEventFullFees.requestFocus();
//                return false;
//            }
//
//            if (!TextUtils.isEmpty(binding.etEventYouthFee.getText())) {
//                eventSession.setEvent_session_youth_fee(binding.etEventYouthFee.getText().toString());
//            } else {
//                binding.etEventYouthFee.setError("Required");
//                binding.etEventYouthFee.requestFocus();
//                return false;
//            }
//
//            if (!TextUtils.isEmpty(binding.etEventChildFee.getText())) {
//                eventSession.setEvent_session_child_fee(binding.etEventChildFee.getText().toString());
//            } else {
//                binding.etEventChildFee.setError("Required");
//                binding.etEventChildFee.requestFocus();
//                return false;
//            }
//
//            if (!TextUtils.isEmpty(binding.etEventStudentFee.getText())) {
//                eventSession.setEvent_session_student_fee(binding.etEventStudentFee.getText().toString());
//            } else {
//                binding.etEventStudentFee.setError("Required");
//                binding.etEventStudentFee.requestFocus();
//                return false;
//            }
//
//            if (!TextUtils.isEmpty(binding.etEventStaffFee.getText())) {
//                eventSession.setEvent_session_staff_fee(binding.etEventStaffFee.getText().toString());
//            } else {
//                binding.etEventStaffFee.setError("Required");
//                binding.etEventStaffFee.requestFocus();
//                return false;
//            }
//
//            if (!TextUtils.isEmpty(binding.etEventMinisterFee.getText())) {
//                eventSession.setEvent_session_minister_fee(binding.etEventMinisterFee.getText().toString());
//            } else {
//                binding.etEventMinisterFee.setError("Required");
//                binding.etEventMinisterFee.requestFocus();
//                return false;
//            }
//
//            if (!TextUtils.isEmpty(binding.etEventClergyFee.getText())) {
//                eventSession.setEvent_session_clergy_fee(binding.etEventClergyFee.getText().toString());
//            } else {
//                binding.etEventClergyFee.setError("Required");
//                binding.etEventClergyFee.requestFocus();
//                return false;
//            }
//
//            if (!TextUtils.isEmpty(binding.etEventPromoFee.getText())) {
//                eventSession.setEvent_session_promo_fee(binding.etEventPromoFee.getText().toString());
//            } else {
//                binding.etEventPromoFee.setError("Required");
//                binding.etEventPromoFee.requestFocus();
//                return false;
//            }
//
//            if (!TextUtils.isEmpty(binding.etEventSeniorFee.getText())) {
//                eventSession.setEvent_session_senior_fee(binding.etEventSeniorFee.getText().toString());
//            } else {
//
//                binding.etEventSeniorFee.setError("Required");
//                binding.etEventSeniorFee.requestFocus();
//                return false;
//            }
//        }
//
//        if (eventSession.isIs_parking_allowed()) {
//            if (!TextUtils.isEmpty(binding.etParkingCost.getText())) {
//                eventSession.setEvent_session_parking_fee(String.valueOf(binding.etParkingCost.getText()));
//            } else {
//                binding.etParkingCost.setError("Required");
//                binding.etParkingCost.requestFocus();
//                return false;
//            }
//        }
//        return true;
//    }
//
//    @Override
//    protected void initViews(View v) {
//
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        if (SessionsFragment.isSessionUpdated && getActivity() != null) {
//            getActivity().onBackPressed();
//        }
//    }

    public FragSessionPricingLimitBinding binding;
    private boolean isEdit = false, isRepeat = false;
    private EventSession eventSession;
    private String TAG = "#DEBUG SessionPricing";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            isEdit = getArguments().getBoolean("isEdit", false);
            isRepeat = getArguments().getBoolean("isRepeat", false);
            eventSession = new Gson().fromJson(getArguments().getString("eventSession"), EventSession.class);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_session_pricing_limit, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {

        if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_VIRTUALLY) {
            binding.llWebEventPrice.setVisibility(View.VISIBLE);
            binding.llLocationEventPrice.setVisibility(View.GONE);
            binding.llParkingPrice.setVisibility(View.GONE);

        } else if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_LOCATION) {
            binding.llWebEventPrice.setVisibility(View.GONE);
            if (eventSession.isFree_local_session()) {
                binding.llLocationEventPrice.setVisibility(View.GONE);
            } else {
                binding.llLocationEventPrice.setVisibility(View.VISIBLE);
            }
            if (eventSession != null && eventSession.isParking_allowed()) {
                binding.llParkingPrice.setVisibility(View.VISIBLE);
            } else {
                binding.llParkingPrice.setVisibility(View.GONE);
            }
        } else if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_BOTH) {
            binding.llWebEventPrice.setVisibility(View.VISIBLE);
            if (eventSession.isFree_local_session()) {
                binding.llLocationEventPrice.setVisibility(View.GONE);
            } else {
                binding.llLocationEventPrice.setVisibility(View.VISIBLE);
            }
            if (eventSession != null && eventSession.isParking_allowed()) {
                binding.llParkingPrice.setVisibility(View.VISIBLE);
            } else {
                binding.llParkingPrice.setVisibility(View.GONE);
            }
        }

        if (eventSession.getEvent_session_parking_fee() != null
                && !TextUtils.isEmpty(eventSession.getEvent_session_parking_fee())) {
            binding.etParkingCost.setText(eventSession.getEvent_session_parking_fee());
        }

        if (eventSession.getEvent_session_regn_fee() != null
                && !TextUtils.isEmpty(eventSession.getEvent_session_regn_fee())) {
            binding.etEventFullFees.setText(eventSession.getEvent_session_regn_fee());
        }

        if (eventSession.getCost() != null
                && !TextUtils.isEmpty(eventSession.getCost())) {
            binding.etEventCost.setText(eventSession.getCost());
        }

        if (eventSession.getEvent_session_youth_fee() != null
                && !TextUtils.isEmpty(eventSession.getEvent_session_youth_fee())) {
            binding.etEventYouthFee.setText(eventSession.getEvent_session_youth_fee());
        }

        if (eventSession.getEvent_session_child_fee() != null
                && !TextUtils.isEmpty(eventSession.getEvent_session_child_fee())) {
            binding.etEventChildFee.setText(eventSession.getEvent_session_child_fee());
        }

        if (eventSession.getEvent_session_student_fee() != null
                && !TextUtils.isEmpty(eventSession.getEvent_session_student_fee())) {
            binding.etEventStudentFee.setText(eventSession.getEvent_session_student_fee());
        }

        if (eventSession.getEvent_session_minister_fee() != null
                && !TextUtils.isEmpty(eventSession.getEvent_session_minister_fee())) {
            binding.etEventMinisterFee.setText(eventSession.getEvent_session_minister_fee());
        }

        if (eventSession.getEvent_session_clergy_fee() != null
                && !TextUtils.isEmpty(eventSession.getEvent_session_clergy_fee())) {
            binding.etEventClergyFee.setText(eventSession.getEvent_session_clergy_fee());
        }

        if (eventSession.getEvent_session_promo_fee() != null
                && !TextUtils.isEmpty(eventSession.getEvent_session_promo_fee())) {
            binding.etEventPromoFee.setText(eventSession.getEvent_session_promo_fee());
        }

        if (eventSession.getEvent_session_senior_fee() != null
                && !TextUtils.isEmpty(eventSession.getEvent_session_senior_fee())) {
            binding.etEventSeniorFee.setText(eventSession.getEvent_session_senior_fee());
        }

        if (eventSession.getEvent_session_staff_fee() != null
                && !TextUtils.isEmpty(eventSession.getEvent_session_staff_fee())) {
            binding.etEventStaffFee.setText(eventSession.getEvent_session_staff_fee());
        }

        if (eventSession.getWeb_event_session_regn_fee() != null
                && !TextUtils.isEmpty(eventSession.getWeb_event_session_regn_fee())) {
            binding.etWebEventFullRegFee.setText(eventSession.getWeb_event_session_regn_fee());
        }

    }

    @Override
    protected void setListeners() {

        binding.etEventCost.setOnClickListener(v -> {
            binding.etEventCost.setError(null);
            Log.d("jkgg","====");
            showEnter_txt(" Session Registration Cost  ", "Enter value", binding.etEventCost, "etEventCost", false);
        });
        binding.etEventFullFees.setOnClickListener(v -> {
            binding.etEventFullFees.setError(null);
            showEnter_txt(" Full Event Registration Fee(REGULAR)  ", "Enter amount", binding.etEventFullFees, "etEventFullFees", false);
        });
        binding.etEventYouthFee.setOnClickListener(v -> {
            binding.etEventYouthFee.setError(null);
            showEnter_txt(" Registration Fee for YOUTH  ", "Enter amount", binding.etEventYouthFee, "etEventYouthFee", false);
        });
        binding.etEventChildFee.setOnClickListener(v -> {
            binding.etEventChildFee.setError(null);
            showEnter_txt(" Registration Fee for CHILD  ", "Enter amount", binding.etEventChildFee, "etEventChildFee", false);
        });
        binding.etEventStudentFee.setOnClickListener(v -> {
            binding.etEventStudentFee.setError(null);
            showEnter_txt(" Registration Fee for STUDENT  ", "Enter amount", binding.etEventStudentFee, "etEventStudentFee", false);
        });
        binding.etEventStaffFee.setOnClickListener(v -> {
            binding.etEventStaffFee.setError(null);
            showEnter_txt(" Registration Fee for STAFF  ", "Enter amount", binding.etEventStaffFee, "etEventStaffFee", false);
        });
        binding.etEventMinisterFee.setOnClickListener(v -> {
            binding.etEventMinisterFee.setError(null);
            showEnter_txt(" Registration Fee for MINISTER  ", "Enter amount", binding.etEventMinisterFee, "etEventMinisterFee", false);
        });
        binding.etEventClergyFee.setOnClickListener(v -> {
            binding.etEventClergyFee.setError(null);
            showEnter_txt(" Registration Fee for CLERGY  ", "Enter amount", binding.etEventClergyFee, "etEventClergyFee", false);
        });
        binding.etEventPromoFee.setOnClickListener(v -> {
            binding.etEventPromoFee.setError(null);
            showEnter_txt(" Registration Fee for PROMO  ", "Enter amount", binding.etEventPromoFee, "etEventPromoFee", false);
        });
        binding.etEventSeniorFee.setOnClickListener(v -> {
            binding.etEventSeniorFee.setError(null);
            showEnter_txt(" Registration Fee for SENIOR  ", "Enter amount", binding.etEventSeniorFee, "etEventSeniorFee", false);
        });

        binding.etParkingCost.setOnClickListener(v -> {
            binding.etParkingCost.setError(null);
            showEnter_txt(" Parking Rate  ", "Ex. $10/1 Hour", binding.etParkingCost, "etParkingCost", true);
        });

        binding.etWebEventFullRegFee.setOnClickListener(v -> {
            binding.etWebEventFullRegFee.setError(null);
            showEnter_txt(" Web Session Full Registration Fee  ", "Enter amount", binding.etWebEventFullRegFee, "etWebEventFullRegFee", false);
        });
        binding.etWebEventRegLimit.setOnClickListener(v -> {
            binding.etWebEventRegLimit.setError(null);
            showEnter_txt(" Web Session Registration Limit  ", "Enter amount", binding.etWebEventRegLimit, "etWebEventRegLimit", false);
        });

        binding.tvBtnNext.setOnClickListener(v -> {

            if (validate()) {
                Fragment fragment = null;
                if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_LOCATION) {
                    fragment = new SessionLocationAddEditFragment();
                } else {
                    fragment = new SessionMediaAddEditFragment();
                }

                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Bundle bundle = new Bundle();
                bundle.putBoolean("isEdit", isEdit);
                bundle.putBoolean("isRepeat", isRepeat);
                bundle.putString("eventSession", new Gson().toJson(eventSession));
                fragment.setArguments(bundle);
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.add(R.id.My_Container_1_ID, fragment);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(fragment);
                ft.commitAllowingStateLoss();
            }

        });

    }

    private boolean validate() {
        if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_VIRTUALLY) {
            if (!eventSession.isFree_local_session()) {
                if (!checkWebFields()) return false;
            }
        } else if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_LOCATION) {
            if (!checkLocationFields()) return false;
        } else if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_BOTH) {
            if (!checkLocationFields()) return false;
            if (!eventSession.isFree_local_session()) {
                if (!checkWebFields()) return false;
            }
        }
        return true;
    }

    private boolean checkWebFields() {
        if (!TextUtils.isEmpty(binding.etWebEventFullRegFee.getText())) {
            eventSession.setWeb_event_session_regn_fee(String.valueOf(binding.etWebEventFullRegFee.getText()));
        } else {
            binding.etWebEventFullRegFee.setError("Required");
            binding.etWebEventFullRegFee.requestFocus();
            return false;
        }
        return true;
    }

    private boolean checkLocationFields() {

        if (eventSession.isFree_session()) {
            eventSession.setEvent_session_regn_fee("0");
            eventSession.setEvent_session_youth_fee("0");
            eventSession.setEvent_session_child_fee("0");
            eventSession.setEvent_session_student_fee("0");
            eventSession.setEvent_session_staff_fee("0");
            eventSession.setEvent_session_minister_fee("0");
            eventSession.setEvent_session_clergy_fee("0");
            eventSession.setEvent_session_promo_fee("0");
            eventSession.setEvent_session_senior_fee("0");
        } else {
            if (!TextUtils.isEmpty(binding.etEventCost.getText())) {
                eventSession.setCost(String.format("%s", binding.etEventCost.getText().toString()));
            } else {
                binding.etEventCost.setError("Required");
                binding.etEventCost.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventFullFees.getText())) {
                eventSession.setEvent_session_regn_fee(binding.etEventFullFees.getText().toString());
            } else {
                binding.etEventFullFees.setError("Required");
                binding.etEventFullFees.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventYouthFee.getText())) {
                eventSession.setEvent_session_youth_fee(binding.etEventYouthFee.getText().toString());
            } else {
                binding.etEventYouthFee.setError("Required");
                binding.etEventYouthFee.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventChildFee.getText())) {
                eventSession.setEvent_session_child_fee(binding.etEventChildFee.getText().toString());
            } else {
                binding.etEventChildFee.setError("Required");
                binding.etEventChildFee.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventStudentFee.getText())) {
                eventSession.setEvent_session_student_fee(binding.etEventStudentFee.getText().toString());
            } else {
                binding.etEventStudentFee.setError("Required");
                binding.etEventStudentFee.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventStaffFee.getText())) {
                eventSession.setEvent_session_staff_fee(binding.etEventStaffFee.getText().toString());
            } else {
                binding.etEventStaffFee.setError("Required");
                binding.etEventStaffFee.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventMinisterFee.getText())) {
                eventSession.setEvent_session_minister_fee(binding.etEventMinisterFee.getText().toString());
            } else {
                binding.etEventMinisterFee.setError("Required");
                binding.etEventMinisterFee.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventClergyFee.getText())) {
                eventSession.setEvent_session_clergy_fee(binding.etEventClergyFee.getText().toString());
            } else {
                binding.etEventClergyFee.setError("Required");
                binding.etEventClergyFee.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventPromoFee.getText())) {
                eventSession.setEvent_session_promo_fee(binding.etEventPromoFee.getText().toString());
            } else {
                binding.etEventPromoFee.setError("Required");
                binding.etEventPromoFee.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventSeniorFee.getText())) {
                eventSession.setEvent_session_senior_fee(binding.etEventSeniorFee.getText().toString());
            } else {

                binding.etEventSeniorFee.setError("Required");
                binding.etEventSeniorFee.requestFocus();
                return false;
            }
        }

        if (eventSession.isParking_allowed()) {
            if (!TextUtils.isEmpty(binding.etParkingCost.getText())) {
                eventSession.setEvent_session_parking_fee(String.valueOf(binding.etParkingCost.getText()));
            } else {
                binding.etParkingCost.setError("Required");
                binding.etParkingCost.requestFocus();
                return false;
            }
        }
        return true;
    }

    private void showEnter_txt(String title, String hintText, TextView etTextView, String selectedEditText, boolean isParkingFees) {
        RelativeLayout viewGroup = (RelativeLayout) getActivity().findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.enter_txt, viewGroup, false);
        final PopupWindow popup = new PopupWindow(getActivity());
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(true);
        popup.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        popup.setFocusable(true);
        popup.update();
        popup.showAtLocation(layout, Gravity.TOP, 0, 0);

        final EditText editNu;
        final TextView txtTitle, tvPaste;
        final Button btnDone;
        RelativeLayout close;
        LinearLayout rlEdit, rlFees;
        ImageView ivPaste;
        SwitchCompat switchDay, switchEvent, switchHR, switchMin;

        tvPaste = (TextView) layout.findViewById(R.id.tvPaste);
        ivPaste = (ImageView) layout.findViewById(R.id.ivPaste);
        editNu = (EditText) layout.findViewById(R.id.edit_nu);
        txtTitle = (TextView) layout.findViewById(R.id.txt_title);
        switchDay = (SwitchCompat) layout.findViewById(R.id.switchDay);
        switchEvent = (SwitchCompat) layout.findViewById(R.id.switchEvent);
        switchHR = (SwitchCompat) layout.findViewById(R.id.switchHR);
        switchMin = (SwitchCompat) layout.findViewById(R.id.switchMin);
        rlFees = (LinearLayout) layout.findViewById(R.id.rl_fees);

        txtTitle.setText(title);
        editNu.setHint(hintText);
        editNu.setInputType(InputType.TYPE_CLASS_NUMBER);

        close = (RelativeLayout) layout.findViewById(R.id.close);
        btnDone = (Button) layout.findViewById(R.id.btn_done);
        rlEdit = (LinearLayout) layout.findViewById(R.id.rl_edit);

        editNu.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        if (!TextUtils.isEmpty(etTextView.getText()))
            editNu.setText(etTextView.getText());
        editNu.setSelection(editNu.getText().length());

        if (isParkingFees) {
            rlFees.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(eventSession.getSession_parking_fee_duration())) {
                String duration = eventSession.getSession_parking_fee_duration();
                if (duration.equals("Event"))
                    switchEvent.setChecked(true);
                else if (duration.equals("Day"))
                    switchDay.setChecked(true);
                else if (duration.equals("Hour"))
                    switchHR.setChecked(true);
                else if (duration.equals("Minute"))
                    switchMin.setChecked(true);
            } else {
                switchEvent.setChecked(true);
            }
            editNu.setText(eventSession.getEvent_session_parking_fee());
        } else
            rlFees.setVisibility(View.GONE);

        editNu.setFocusable(true);
        btnDone.setOnClickListener(v -> {
//            imm.hideSoftInputFromWindow(editNu.getWindowToken(), 0);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            if (isParkingFees) {
                String duration = "Event";
                if (switchEvent.isChecked())
                    duration = "Event";
                else if (switchDay.isChecked())
                    duration = "Day";
                else if (switchHR.isChecked())
                    duration = "Hour";
                else if (switchMin.isChecked())
                    duration = "Minute";
//                eventSession.setEvent_session_parking_fee_duration(duration);
                eventSession.setEvent_session_parking_fee(editNu.getText().toString());
                etTextView.setText("$" + editNu.getText().toString() + "/1 " + duration);
            } else {
                etTextView.setText(editNu.getText().toString());
            }
            popup.dismiss();
            if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_LOCATION) {
                if (selectedEditText.equals("etEventCost"))
                    binding.etEventFullFees.performClick();
                if (selectedEditText.equals("etEventFullFees"))
                    binding.etEventYouthFee.performClick();
                if (selectedEditText.equals("etEventYouthFee"))
                    binding.etEventChildFee.performClick();
                if (selectedEditText.equals("etEventChildFee"))
                    binding.etEventStudentFee.performClick();
                if (selectedEditText.equals("etEventStudentFee"))
                    binding.etEventStaffFee.performClick();
                if (selectedEditText.equals("etEventStaffFee"))
                    binding.etEventMinisterFee.performClick();
                if (selectedEditText.equals("etEventMinisterFee"))
                    binding.etEventClergyFee.performClick();
                if (selectedEditText.equals("etEventClergyFee"))
                    binding.etEventPromoFee.performClick();
                if (selectedEditText.equals("etEventPromoFee"))
                    binding.etEventSeniorFee.performClick();
                if (selectedEditText.equals("etEventSeniorFee"))
                    if (eventSession.isParking_allowed())
                        binding.etParkingCost.performClick();
            }
            if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_BOTH) {
                if (selectedEditText.equals("etEventCost"))
                    binding.etEventFullFees.performClick();
                if (selectedEditText.equals("etEventFullFees"))
                    binding.etEventYouthFee.performClick();
                if (selectedEditText.equals("etEventYouthFee"))
                    binding.etEventChildFee.performClick();
                if (selectedEditText.equals("etEventChildFee"))
                    binding.etEventStudentFee.performClick();
                if (selectedEditText.equals("etEventStudentFee"))
                    binding.etEventStaffFee.performClick();
                if (selectedEditText.equals("etEventStaffFee"))
                    binding.etEventMinisterFee.performClick();
                if (selectedEditText.equals("etEventMinisterFee"))
                    binding.etEventClergyFee.performClick();
                if (selectedEditText.equals("etEventClergyFee"))
                    binding.etEventPromoFee.performClick();
                if (selectedEditText.equals("etEventPromoFee"))
                    binding.etEventSeniorFee.performClick();

                if (selectedEditText.equals("etEventSeniorFee")) {
                    if (eventSession.isParking_allowed())
                        binding.etParkingCost.performClick();
                    else if (!eventSession.isFree_web_session())
                        binding.etWebEventFullRegFee.performClick();
                }
                if (selectedEditText.equals("etParkingCost"))
                    if (!eventSession.isFree_web_session())
                        binding.etWebEventFullRegFee.performClick();
                if (selectedEditText.equals("etWebEventFullRegFee"))
                    binding.etWebEventRegLimit.performClick();
            }
            if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_VIRTUALLY) {
                if (selectedEditText.equals("etWebEventFullRegFee"))
                    binding.etWebEventRegLimit.performClick();
            }
        });

        close.setOnClickListener(v -> {
            InputMethodManager imm1 = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm1.hideSoftInputFromWindow(editNu.getWindowToken(), 0);
            String txt = editNu.getText().toString();
            popup.dismiss();
        });
        editNu.setOnClickListener(v -> {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            tvPaste.setVisibility(View.GONE);
        });
        rlEdit.setOnClickListener(v -> tvPaste.setVisibility(View.GONE));
        editNu.setOnLongClickListener(v -> {
            tvPaste.setVisibility(View.VISIBLE);
            return true;
        });
        tvPaste.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData pData = clipboard.getPrimaryClip();
            if (pData != null) {
                ClipData.Item item = pData.getItemAt(0);
                String txtpaste = item.getText().toString();
                editNu.getText().insert(editNu.getSelectionStart(), txtpaste);
                editNu.setText(editNu.getText()+txtpaste);
            }
        });
        ivPaste.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData pData = clipboard.getPrimaryClip();
            if (pData != null) {
                ClipData.Item item = pData.getItemAt(0);
                String txtpaste = item.getText().toString();
                editNu.getText().insert(editNu.getSelectionStart(), txtpaste);
                editNu.setText(editNu.getText()+txtpaste);
            }
        });

        switchEvent.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                switchDay.setChecked(false);
                switchHR.setChecked(false);
                switchMin.setChecked(false);
            }
        });
        switchDay.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                switchEvent.setChecked(false);
                switchHR.setChecked(false);
                switchMin.setChecked(false);
            }
        });
        switchHR.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                switchDay.setChecked(false);
                switchEvent.setChecked(false);
                switchMin.setChecked(false);
            }
        });
        switchMin.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                switchDay.setChecked(false);
                switchHR.setChecked(false);
                switchEvent.setChecked(false);
            }
        });
    }

    @Override
    protected void initViews(View v) {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (SessionsFragment.isSessionUpdated)
            if (getActivity() != null) getActivity().onBackPressed();
    }
}
