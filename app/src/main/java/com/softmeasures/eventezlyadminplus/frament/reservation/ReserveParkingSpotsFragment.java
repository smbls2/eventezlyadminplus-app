package com.softmeasures.eventezlyadminplus.frament.reservation;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.frament.Other_City;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class ReserveParkingSpotsFragment extends BaseFragment implements Other_City.Other_city_listenere {


    private ImageView ivBtnFindNearBy, ivBtnOtherCity;
    private GoogleMap googleMap;
    private RelativeLayout rlProgressbar;
    private ArrayList<item> parkingSpots = new ArrayList<>();
    private SupportMapFragment mapFragment;
    private double latitude = 0.0, longitude = 0.0;
    private Handler mHandler = null;
    private Runnable mAnimation;
    private double centerlat = 0.0, centerlang = 0.0;
    private boolean mapisloadcallback = false, managedmapisloaded = false, managedpinisclick = false;
    private boolean nearbyclick = false, searchotherlocation = false;
    private ArrayList<item> nearbylist = new ArrayList<>();
    private ArrayList<item> findnearbyarray = new ArrayList<>();
    private adpaternearby adpater;
    private int c = 0;

    private RelativeLayout rl_nearbylist;
    private ListView list_nearby;
    private ImageView img_close_nearbylist;

    private RelativeLayout rl_searchinfo;
    private ArrayList<item> parking_rules = new ArrayList<>();
    String lat = "null", lang = "null";

    private String parkingType = "";

    private LinearLayout llBottomOptions;
    private RelativeLayout rlBtnTownship, rlBtnCommercial, rlBtnOther;
    private ImageView ivTownship, ivCommercial, ivOther;
    private boolean isAllOptions = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            parkingType = getArguments().getString("parkingType", "Township");
            if (parkingType.equals("All")) isAllOptions = true;
            Log.e("#DEBUG", "   ReserveParkingSpotsFragment:  parkingType:  " + parkingType);
        }
        return inflater.inflate(R.layout.frag_reserve_parking_spots, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
        getCurrentLocation();

        loadMap();

        new getparkingrules().execute();
    }

    public void getCurrentLocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {

            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();

        }
    }

    @Override
    public void OnItemFindClick(String lat, String lang) {
        latitude = Double.parseDouble(lat);
        longitude = Double.parseDouble(lang);
        centerlat = 0.0;
        centerlang = 0.0;
        new fetchReservationParkingSpots().execute();

    }

    private class fetchReservationParkingSpots extends AsyncTask<String, String, String> {
        JSONObject jsonObject;
        JSONArray jsonArray;
        String parkingSpotsUrl = "_proc/find_reserv_township_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            if (parkingType.equals("Township")) {
                parkingSpotsUrl = "_proc/find_reserv_township_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";
            } else if (parkingType.equals("Commercial")) {
                parkingSpotsUrl = "_proc/find_reserv_commercial_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";
            } else if (parkingType.equals("Other")) {
                parkingSpotsUrl = "_proc/find_reserv_other_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";
            }
            parkingSpots.clear();
            if (googleMap != null) {
                googleMap.clear();
            }
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingSpotsUrl;
            Log.e("#DEBUG", "  fetchReservationParkingSpots:  Url:  " + url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
//                    jsonObject = new JSONObject(responseStr);
                    jsonArray = new JSONArray(responseStr);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject c = jsonArray.getJSONObject(i);
                        item item1 = new item();

                        if (c.has("id")
                                && !TextUtils.isEmpty(c.getString("id"))
                                && !c.getString("id").equals("null")) {
                            item1.setId(c.getString("id"));
                        }
                        if (c.has("location_id")
                                && !TextUtils.isEmpty(c.getString("location_id"))
                                && !c.getString("location_id").equals("null")) {
                            item1.setLocation_id(c.getString("location_id"));
                        }
                        if (c.has("location_place_id")
                                && !TextUtils.isEmpty(c.getString("location_place_id"))
                                && !c.getString("location_place_id").equals("null")) {
                            item1.setLocation_place_id(c.getString("location_place_id"));
                        }
                        if (c.has("location_code")
                                && !TextUtils.isEmpty(c.getString("location_code"))
                                && !c.getString("location_code").equals("null")) {
                            item1.setLocation_code(c.getString("location_code"));
                        }
                        if (c.has("location_name")
                                && !TextUtils.isEmpty(c.getString("location_name"))
                                && !c.getString("location_name").equals("null")) {
                            item1.setLocation_name(c.getString("location_name"));
                        }
                        if (c.has("place_id")
                                && !TextUtils.isEmpty(c.getString("place_id"))
                                && !c.getString("place_id").equals("null")) {
                            item1.setPlace_id(c.getString("place_id"));
                        }
                        if (c.has("active")
                                && !TextUtils.isEmpty(c.getString("active"))
                                && !c.getString("active").equals("null")) {
                            item1.setActive(c.getString("active"));
                        }
                        if (c.has("address")
                                && !TextUtils.isEmpty(c.getString("address"))
                                && !c.getString("address").equals("null")) {
                            item1.setAddress(c.getString("address"));
                        }
                        if (c.has("custom_notice")
                                && !TextUtils.isEmpty(c.getString("custom_notice"))
                                && !c.getString("custom_notice").equals("null")) {
                            item1.setCustom_notice(c.getString("custom_notice"));
                        }
                        if (c.has("in_effect")
                                && !TextUtils.isEmpty(c.getString("in_effect"))
                                && !c.getString("in_effect").equals("null")) {
                            item1.setIn_effect(c.getString("in_effect"));
                        }
                        if (c.has("lat")
                                && !TextUtils.isEmpty(c.getString("lat"))
                                && !c.getString("lat").equals("null")) {
                            item1.setLat(c.getString("lat"));
                        }
                        if (c.has("lng")
                                && !TextUtils.isEmpty(c.getString("lng"))
                                && !c.getString("lng").equals("null")) {
                            item1.setLng(c.getString("lng"));
                        }
                        if (c.has("lots_avbl")
                                && !TextUtils.isEmpty(c.getString("lots_avbl"))
                                && !c.getString("lots_avbl").equals("null")) {
                            item1.setLots_avbl(c.getString("lots_avbl"));
                        }
                        if (c.has("lot_numbering_type")
                                && !TextUtils.isEmpty(c.getString("lot_numbering_type"))
                                && !c.getString("lot_numbering_type").equals("null")) {
                            item1.setLot_numbering_type(c.getString("lot_numbering_type"));
                        }
                        if (c.has("lot_numbering_description")
                                && !TextUtils.isEmpty(c.getString("lot_numbering_description"))
                                && !c.getString("lot_numbering_description").equals("null")) {
                            item1.setLot_numbering_description(c.getString("lot_numbering_description"));
                        }
                        if (c.has("lots_total")
                                && !TextUtils.isEmpty(c.getString("lots_total"))
                                && !c.getString("lots_total").equals("null")) {
                            item1.setLots_total(c.getString("lots_total"));
                        }
                        if (c.has("marker_type")
                                && !TextUtils.isEmpty(c.getString("marker_type"))
                                && !c.getString("marker_type").equals("null")) {
                            item1.setMarker_type(c.getString("marker_type"));
                        }
                        if (c.has("no_parking_times")
                                && !TextUtils.isEmpty(c.getString("no_parking_times"))
                                && !c.getString("no_parking_times").equals("null")) {
                            item1.setNo_parking_times(c.getString("no_parking_times"));
                        }
                        if (c.has("off_peak_discount")
                                && !TextUtils.isEmpty(c.getString("off_peak_discount"))
                                && !c.getString("off_peak_discount").equals("null")) {
                            item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        }
                        if (c.has("off_peak_ends")
                                && !TextUtils.isEmpty(c.getString("off_peak_ends"))
                                && !c.getString("off_peak_ends").equals("null")) {
                            item1.setOff_peak_ends(c.getString("off_peak_ends"));
                        }
                        if (c.has("off_peak_starts")
                                && !TextUtils.isEmpty(c.getString("off_peak_starts"))
                                && !c.getString("off_peak_starts").equals("null")) {
                            item1.setOff_peak_starts(c.getString("off_peak_starts"));
                        }
                        if (c.has("parking_times")
                                && !TextUtils.isEmpty(c.getString("parking_times"))
                                && !c.getString("parking_times").equals("null")) {
                            item1.setParking_times(c.getString("parking_times"));
                        }
                        if (c.has("renewable")
                                && !TextUtils.isEmpty(c.getString("renewable"))
                                && !c.getString("renewable").equals("null")) {
                            item1.setRenewable(c.getString("renewable"));
                        }
                        if (c.has("title")
                                && !TextUtils.isEmpty(c.getString("title"))
                                && !c.getString("title").equals("null")) {
                            item1.setTitle(c.getString("title"));
                        }
                        if (c.has("user_id")
                                && !TextUtils.isEmpty(c.getString("user_id"))
                                && !c.getString("user_id").equals("null")) {
                            item1.setUser_id(c.getString("user_id"));
                        }
                        if (c.has("weekend_special_diff")
                                && !TextUtils.isEmpty(c.getString("weekend_special_diff"))
                                && !c.getString("weekend_special_diff").equals("null")) {
                            item1.setWeekend_special_diff(c.getString("weekend_special_diff"));
                        }
                        if (c.has("twp_id")
                                && !TextUtils.isEmpty(c.getString("twp_id"))
                                && !c.getString("twp_id").equals("null")) {
                            item1.setTwp_id(c.getString("twp_id"));
                        }
                        if (c.has("township_id")
                                && !TextUtils.isEmpty(c.getString("township_id"))
                                && !c.getString("township_id").equals("null")) {
                            item1.setTownship_id(c.getString("township_id"));
                        }
                        if (c.has("township_code")
                                && !TextUtils.isEmpty(c.getString("township_code"))
                                && !c.getString("township_code").equals("null")) {
                            item1.setTownship_code(c.getString("township_code"));
                        }
                        if (c.has("manager_id")
                                && !TextUtils.isEmpty(c.getString("manager_id"))
                                && !c.getString("manager_id").equals("null")) {
                            item1.setManager_id(c.getString("manager_id"));
                        }
                        if (c.has("company_id")
                                && !TextUtils.isEmpty(c.getString("company_id"))
                                && !c.getString("company_id").equals("null")) {
                            item1.setCompany_id(c.getString("company_id"));
                        }
                        if (c.has("division_id")
                                && !TextUtils.isEmpty(c.getString("division_id"))
                                && !c.getString("division_id").equals("null")) {
                            item1.setDivision_id(c.getString("division_id"));
                        }
                        if (c.has("dept_id")
                                && !TextUtils.isEmpty(c.getString("dept_id"))
                                && !c.getString("dept_id").equals("null")) {
                            item1.setDept_id(c.getString("dept_id"));
                        }
                        if (c.has("isKiosk")
                                && !TextUtils.isEmpty(c.getString("isKiosk"))
                                && !c.getString("isKiosk").equals("null")) {
                            item1.setIsKiosk(c.getString("isKiosk"));
                        }
                        if (c.has("manager_type_id")
                                && !TextUtils.isEmpty(c.getString("manager_type_id"))
                                && !c.getString("manager_type_id").equals("null")) {
                            item1.setManager_type_id(c.getString("manager_type_id"));
                        }
                        if (c.has("company_type_id")
                                && !TextUtils.isEmpty(c.getString("company_type_id"))
                                && !c.getString("company_type_id").equals("null")) {
                            item1.setCompany_type_id(c.getString("company_type_id"));
                        }
                        if (c.has("twp_id")
                                && !TextUtils.isEmpty(c.getString("twp_id"))
                                && !c.getString("twp_id").equals("null")) {
                            item1.setTwp_id(c.getString("twp_id"));
                        }
//                        if (c.has("reservable")
//                                && !TextUtils.isEmpty(c.getString("reservable"))
//                                && !c.getString("reservable").equals("null")) {
//                            if (c.getString("reservable").equals("1")) {
//                                item1.setReservable(true);
//                            } else if (c.getString("reservable").equals("0")) {
//                                item1.setReservable(false);
//                            } else {
//                                item1.setReservable(c.getBoolean("reservable"));
//                            }
//                        }
                        if (c.has("distance")
                                && !TextUtils.isEmpty(c.getString("distance"))
                                && !c.getString("distance").equals("null")) {
                            item1.setDistances(c.getString("distance"));
                        }
                        if (c.has("day_type")
                                && !TextUtils.isEmpty(c.getString("day_type"))
                                && !c.getString("day_type").equals("null")) {
                            item1.setDay_type(c.getString("day_type"));
                        }
                        if (c.has("max_duration")
                                && !TextUtils.isEmpty(c.getString("max_duration"))
                                && !c.getString("max_duration").equals("null")) {
                            item1.setMax_duration(c.getString("max_duration"));
                        }
                        if (c.has("pricing")
                                && !TextUtils.isEmpty(c.getString("pricing"))
                                && !c.getString("pricing").equals("null")) {
                            item1.setPricing(c.getString("pricing"));
                        }
                        if (c.has("pricing_duration")
                                && !TextUtils.isEmpty(c.getString("pricing_duration"))
                                && !c.getString("pricing_duration").equals("null")) {
                            item1.setPricing_duration(c.getString("pricing_duration"));
                        }
                        if (c.has("premium_pricing")
                                && !TextUtils.isEmpty(c.getString("premium_pricing"))
                                && !c.getString("premium_pricing").equals("null")) {
                            item1.setPremium_pricing(c.getString("premium_pricing"));
                        }
                        if (c.has("duration_unit")
                                && !TextUtils.isEmpty(c.getString("duration_unit"))
                                && !c.getString("duration_unit").equals("null")) {
                            item1.setDuration_unit(c.getString("duration_unit"));
                        }
                        if (c.has("start_hour")
                                && !TextUtils.isEmpty(c.getString("start_hour"))
                                && !c.getString("start_hour").equals("null")) {
                            item1.setStart_hour(c.getString("start_hour"));
                        }
                        if (c.has("start_time")
                                && !TextUtils.isEmpty(c.getString("start_time"))
                                && !c.getString("start_time").equals("null")) {
                            item1.setStart_time(c.getString("start_time"));
                        }
                        if (c.has("end_hour")
                                && !TextUtils.isEmpty(c.getString("end_hour"))
                                && !c.getString("end_hour").equals("null")) {
                            item1.setEnd_hour(c.getString("end_hour"));
                        }
                        if (c.has("end_time")
                                && !TextUtils.isEmpty(c.getString("end_time"))
                                && !c.getString("end_time").equals("null")) {
                            item1.setEnd_time(c.getString("end_time"));
                        }
                        if (c.has("time_rule")
                                && !TextUtils.isEmpty(c.getString("time_rule"))
                                && !c.getString("time_rule").equals("null")) {
                            item1.setTime_rule(c.getString("time_rule"));
                        }
                        if (c.has("lot_numbering_type")
                                && !TextUtils.isEmpty(c.getString("lot_numbering_type"))
                                && !c.getString("lot_numbering_type").equals("null")) {
                            item1.setLot_numbering_type(c.getString("lot_numbering_type"));
                        }
                        if (c.has("lot_numbering_description")
                                && !TextUtils.isEmpty(c.getString("lot_numbering_description"))
                                && !c.getString("lot_numbering_description").equals("null")) {
                            item1.setLot_numbering_description(c.getString("lot_numbering_description"));
                        }
                        if (c.has("currency")
                                && !TextUtils.isEmpty(c.getString("currency"))
                                && !c.getString("currency").equals("null")) {
                            item1.setCurrency(c.getString("currency"));
                        }
                        if (c.has("currency_symbol")
                                && !TextUtils.isEmpty(c.getString("currency_symbol"))
                                && !c.getString("currency_symbol").equals("null")) {
                            item1.setCurrency_symbol(c.getString("currency_symbol"));
                        }
                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item1.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }
                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item1.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }
                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item1.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }
                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item1.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }
                        if (c.has("Reservation_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_Fee"))
                                && !c.getString("Reservation_PostPayment_Fee").equals("null")) {
                            item1.setReservation_PostPayment_Fee(c.getString("Reservation_PostPayment_Fee"));
                        }
                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item1.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }
                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item1.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }
                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item1.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }
                        if (c.has("ParkNow_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_Fee"))
                                && !c.getString("ParkNow_PostPayment_Fee").equals("null")) {
                            item1.setParkNow_PostPayment_Fee(c.getString("ParkNow_PostPayment_Fee"));
                        }
                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item1.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }
                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item1.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }
                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item1.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }
                        if (c.has("number_of_reservable_spots")
                                && !TextUtils.isEmpty(c.getString("number_of_reservable_spots"))
                                && !c.getString("number_of_reservable_spots").equals("null")) {
                            item1.setNumber_of_reservable_spots(c.getString("number_of_reservable_spots"));
                        }
                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item1.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }
                        if (c.has("address")
                                && !TextUtils.isEmpty(c.getString("address"))
                                && !c.getString("address").equals("null")) {
                            item1.setAddress(c.getString("address"));
                        }
                        if (c.has("no_parking_times")
                                && !TextUtils.isEmpty(c.getString("no_parking_times"))
                                && !c.getString("no_parking_times").equals("null")) {
                            item1.setNo_parking_times(c.getString("no_parking_times"));
                        }
                        if (c.has("off_peak_discount")
                                && !TextUtils.isEmpty(c.getString("off_peak_discount"))
                                && !c.getString("off_peak_discount").equals("null")) {
                            item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        }
                        if (c.has("off_peak_ends")
                                && !TextUtils.isEmpty(c.getString("off_peak_ends"))
                                && !c.getString("off_peak_ends").equals("null")) {
                            item1.setOff_peak_ends(c.getString("off_peak_ends"));
                        }
                        if (c.has("off_peak_starts")
                                && !TextUtils.isEmpty(c.getString("off_peak_starts"))
                                && !c.getString("off_peak_starts").equals("null")) {
                            item1.setOff_peak_starts(c.getString("off_peak_starts"));
                        }
                        if (c.has("parking_times")
                                && !TextUtils.isEmpty(c.getString("parking_times"))
                                && !c.getString("parking_times").equals("null")) {
                            item1.setParking_times(c.getString("parking_times"));
                        }
                        if (c.has("title")
                                && !TextUtils.isEmpty(c.getString("title"))
                                && !c.getString("title").equals("null")) {
                            item1.setTitle(c.getString("title"));
                        }

                        for (int j = 0; j < parking_rules.size(); j++) {
                            if (item1.getLocation_code().equals(parking_rules.get(j).getLocation_code())) {
                                item1.setPricing(parking_rules.get(j).getPricing());
                                item1.setPricing_duration(parking_rules.get(j).getPricing_duration());
                                item1.setDuration_unit(parking_rules.get(j).getDuration_unit());
                                item1.setMax_hours(parking_rules.get(j).getMax_hours());
                                item1.setStart_time(parking_rules.get(j).getStart_time());
                                item1.setStart_hour(parking_rules.get(j).getStart_hour());
                                item1.setEnd_time(parking_rules.get(j).getEnd_time());
                                item1.setEnd_hour(parking_rules.get(j).getEnd_hour());
                            }
                        }

                        parkingSpots.add(item1);
                    }
                } catch (IOException e) {
                    Log.e("#DEBUG", "   Error:  " + e.getMessage());
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.e("#DEBUG", "   Error:  " + e.getMessage());
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                rlProgressbar.setVisibility(View.GONE);
                if (jsonArray != null) {
                    if (parkingSpots.size() != 0) {
                        showParkingSpotsOnMap();
                    } else {
                        Toast.makeText(getActivity(), "Not found!", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        }
    }

    private void showParkingSpotsOnMap() {
        if (googleMap != null) {
            Log.e("#DEBUG", "  showParkingSpotsOnMap");
            googleMap.clear();
            MarkerOptions markerOptions;
            for (int i = 0; i < parkingSpots.size(); i++) {
                markerOptions = new MarkerOptions();
                LatLng position = new LatLng(Double.parseDouble(parkingSpots.get(i).getLat()),
                        Double.parseDouble(parkingSpots.get(i).getLng()));
                markerOptions.position(position);
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmap(i)));
                markerOptions.title("township");
                markerOptions.snippet(parkingSpots.get(i).getLat() + "-" + parkingSpots.get(i).getLng());
                googleMap.addMarker(markerOptions);

            }

            LatLng po1 = new LatLng(latitude, longitude);
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    po1, 12);
            if (centerlang == 0.0 && centerlat == 0.0) {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(po1, 12));
                googleMap.animateCamera(location);
            }
//            final long start = SystemClock.uptimeMillis();
//            final long duration = 1500L;
            // Cancels the previous animation
//            mHandler.removeCallbacks(mAnimation);
//            mAnimation = new BounceAnimation(start, duration, melbourne, mHandler);
//            mHandler.post(mAnimation);


            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    managedpinisclick = true;
                    managedmapisloaded = false;
                    openDetailView(marker);
                    return true;
                }
            });

            googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    managedmapisloaded = true;
                    managedpinisclick = false;
                }
            });

            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    mapisloadcallback = true;
                }
            });

            googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    lat = String.valueOf(cameraPosition.target.latitude);
                    lang = String.valueOf(cameraPosition.target.longitude);
                    LatLng position = googleMap.getCameraPosition().target;
                    centerlat = position.latitude;
                    centerlang = position.longitude;
                    if (managedmapisloaded) {
                        if (!managedpinisclick) {
                            latitude = cameraPosition.target.latitude;
                            longitude = cameraPosition.target.longitude;
                            managedpinisclick = true;
                            Log.e("#DEBUG", "   api_is_start   yes");
                            new fetchReservationParkingSpots().execute();
//                            new getparkingpin(lat, lang).execute();
                        }
                    }
                }
            });


        } else {
            Log.e("#DEBUG", "  showParkingSpotsOnMap:  googleMap null");
        }
    }

    private void openDetailView(Marker marker) {
        item itemLocation = new item();
        itemLocation.setLat(String.valueOf(marker.getPosition().latitude));
        itemLocation.setLng(String.valueOf(marker.getPosition().longitude));
        ((vchome) getActivity()).show_back_button();
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ReservationParkingSpotsDetailsFragment pay = new ReservationParkingSpotsDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("selectedSpot", new Gson().toJson(parkingSpots.get(parkingSpots.indexOf(itemLocation))));
        bundle.putString("parkingType", parkingType);
        bundle.putBoolean("isReserveSpot", true);
        pay.setArguments(bundle);
        ft.add(R.id.My_Container_1_ID, pay);
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(pay);
        ft.commitAllowingStateLoss();
    }

    private Bitmap getMarkerBitmap(int pos) {

        String text = parkingSpots.get(pos).getPricing() != null
                ? (!parkingSpots.get(pos).getPricing().equals("0")
                ? (!parkingSpots.get(pos).getPricing().equals("null")
                ? "$" + parkingSpots.get(pos).getPricing() + "/"
                + parkingSpots.get(pos).getPricing_duration() + " "
                + parkingSpots.get(pos).getDuration_unit() + "" : "FREE") : "FREE") : "FREE";

        LinearLayout distanceMarkerLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.marker_with_price, null);
        TextView positionDistance = distanceMarkerLayout.findViewById(R.id.tvPrice);
        TextView tvLocationName = distanceMarkerLayout.findViewById(R.id.tvLocationName);
        positionDistance.setText(text);
        if (!TextUtils.isEmpty(parkingSpots.get(pos).getLocation_name())) {
            tvLocationName.setVisibility(View.VISIBLE);
            tvLocationName.setText(parkingSpots.get(pos).getLocation_name());
        } else {
            tvLocationName.setVisibility(View.GONE);
        }
        distanceMarkerLayout.setDrawingCacheEnabled(true);
        distanceMarkerLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        distanceMarkerLayout.layout(0, 0, distanceMarkerLayout.getMeasuredWidth(), distanceMarkerLayout.getMeasuredHeight());
        distanceMarkerLayout.buildDrawingCache(true);


        Bitmap bitmap = Bitmap.createBitmap(distanceMarkerLayout.getDrawingCache());
        distanceMarkerLayout.setDrawingCacheEnabled(false);
        BitmapDescriptor flagBitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
        return bitmap;
    }

    private static class BounceAnimation implements Runnable {

        private final long mStart, mDuration;
        private final Interpolator mInterpolator;
        private final Marker mMarker;
        private final Handler mHandler;

        private BounceAnimation(long start, long duration, Marker marker, Handler handler) {
            mStart = start;
            mDuration = duration;
            mMarker = marker;
            mHandler = handler;
            mInterpolator = new BounceInterpolator();
        }

        @Override
        public void run() {
            long elapsed = SystemClock.uptimeMillis() - mStart;
            float t = Math.max(1 - mInterpolator.getInterpolation((float) elapsed / mDuration), 0f);
            mMarker.setAnchor(0.5f, 1.0f + 1.2f * t);

            if (t > 0.0) {
                // Post again 16ms later.
                mHandler.postDelayed(this, 16L);
            }
        }
    }

    private void loadMap() {
        if (getActivity() != null) {
            try {
                mapFragment = (SupportMapFragment) getChildFragmentManager()
                        .findFragmentById(R.id.fragmentMap);
                if (mapFragment != null) {
                    mapFragment.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap map) {
                            googleMap = map;
                            showParkingSpotsOnMap();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void updateViews() {
        if (isAllOptions) {
            llBottomOptions.setVisibility(View.VISIBLE);
        } else llBottomOptions.setVisibility(View.GONE);
    }

    @Override
    protected void setListeners() {

        ivBtnFindNearBy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapisloadcallback = false;
                getCurrentLocation();
                searchotherlocation = false;
//                btnsearch.setVisibility(View.GONE);
//                btnback.setVisibility(View.VISIBLE);
                nearbylist.clear();
//                rl_nearby_pin_deatil.setVisibility(View.GONE);
//                txtgo.setVisibility(View.GONE);
//                editdirection.setVisibility(View.GONE);
                new fetchNearByList().execute();
            }
        });

        ivBtnOtherCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.getString("click", "commercia");
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Other_City pay = new Other_City();
                pay.setArguments(b);
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                pay.registerForListener(ReserveParkingSpotsFragment.this);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        rlBtnTownship.setOnClickListener(v -> {
            ivTownship.setAlpha((float) 1.0);
            ivCommercial.setAlpha((float) 0.3);
            ivOther.setAlpha((float) 0.3);
            parkingType = "Township";
            new getparkingrules().execute();
        });

        rlBtnCommercial.setOnClickListener(v -> {
            ivTownship.setAlpha((float) 0.3);
            ivCommercial.setAlpha((float) 1.0);
            ivOther.setAlpha((float) 0.3);
            parkingType = "Commercial";
            new getparkingrules().execute();
        });

        rlBtnOther.setOnClickListener(v -> {
            ivTownship.setAlpha((float) 0.3);
            ivCommercial.setAlpha((float) 0.3);
            ivOther.setAlpha((float) 1.0);
            parkingType = "Other";
            new getparkingrules().execute();
        });

    }

    public class fetchNearByList extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String nearbyurl = "_proc/find_reserv_township_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            if (lat.equals("null")) {
                nearbyurl = "_proc/find_reserv_township_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";
                if (parkingType.equals("Township")) {
                    nearbyurl = "_proc/find_reserv_township_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";
                } else if (parkingType.equals("Commercial")) {
                    nearbyurl = "_proc/find_reserv_commercial_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";
                } else if (parkingType.equals("Other")) {
                    nearbyurl = "_proc/find_reserv_other_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";
                }
            } else {
                nearbyurl = "_proc/find_reserv_township_parking_partners_nearby?in_lat=" + lat + "&in_lng=" + lang + "";
                if (parkingType.equals("Township")) {
                    nearbyurl = "_proc/find_reserv_township_parking_partners_nearby?in_lat=" + lat + "&in_lng=" + lang + "";
                } else if (parkingType.equals("Commercial")) {
                    nearbyurl = "_proc/find_reserv_commercial_parking_partners_nearby?in_lat=" + lat + "&in_lng=" + lang + "";
                } else if (parkingType.equals("Other")) {
                    nearbyurl = "_proc/find_reserv_other_parking_partners_nearby?in_lat=" + lat + "&in_lng=" + lang + "";
                }
            }
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            findnearbyarray.clear();
            nearbylist.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + nearbyurl;
            Log.e("#DEBUG", "  fetchNearByList:  Url:  " + url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
//                    json = new JSONObject(responseStr);
                    json1 = new JSONArray(responseStr);

                    for (int i = 0; i < json1.length(); i++) {
                        JSONObject c = json1.getJSONObject(i);
                        item item1 = new item();

                        if (c.has("id")
                                && !TextUtils.isEmpty(c.getString("id"))
                                && !c.getString("id").equals("null")) {
                            item1.setId(c.getString("id"));
                        }
                        if (c.has("location_id")
                                && !TextUtils.isEmpty(c.getString("location_id"))
                                && !c.getString("location_id").equals("null")) {
                            item1.setLocation_id(c.getString("location_id"));
                        }
                        if (c.has("location_place_id")
                                && !TextUtils.isEmpty(c.getString("location_place_id"))
                                && !c.getString("location_place_id").equals("null")) {
                            item1.setLocation_place_id(c.getString("location_place_id"));
                        }
                        if (c.has("location_code")
                                && !TextUtils.isEmpty(c.getString("location_code"))
                                && !c.getString("location_code").equals("null")) {
                            item1.setLocation_code(c.getString("location_code"));
                        }
                        if (c.has("location_name")
                                && !TextUtils.isEmpty(c.getString("location_name"))
                                && !c.getString("location_name").equals("null")) {
                            item1.setLocation_name(c.getString("location_name"));
                        }
                        if (c.has("place_id")
                                && !TextUtils.isEmpty(c.getString("place_id"))
                                && !c.getString("place_id").equals("null")) {
                            item1.setPlace_id(c.getString("place_id"));
                        }
                        if (c.has("active")
                                && !TextUtils.isEmpty(c.getString("active"))
                                && !c.getString("active").equals("null")) {
                            item1.setActive(c.getString("active"));
                        }
                        if (c.has("address")
                                && !TextUtils.isEmpty(c.getString("address"))
                                && !c.getString("address").equals("null")) {
                            item1.setAddress(c.getString("address"));
                        }
                        if (c.has("custom_notice")
                                && !TextUtils.isEmpty(c.getString("custom_notice"))
                                && !c.getString("custom_notice").equals("null")) {
                            item1.setCustom_notice(c.getString("custom_notice"));
                        }
                        if (c.has("in_effect")
                                && !TextUtils.isEmpty(c.getString("in_effect"))
                                && !c.getString("in_effect").equals("null")) {
                            item1.setIn_effect(c.getString("in_effect"));
                        }
                        if (c.has("lat")
                                && !TextUtils.isEmpty(c.getString("lat"))
                                && !c.getString("lat").equals("null")) {
                            item1.setLat(c.getString("lat"));
                        }
                        if (c.has("lng")
                                && !TextUtils.isEmpty(c.getString("lng"))
                                && !c.getString("lng").equals("null")) {
                            item1.setLng(c.getString("lng"));
                        }
                        if (c.has("lots_avbl")
                                && !TextUtils.isEmpty(c.getString("lots_avbl"))
                                && !c.getString("lots_avbl").equals("null")) {
                            item1.setLots_avbl(c.getString("lots_avbl"));
                        }
                        if (c.has("lot_numbering_type")
                                && !TextUtils.isEmpty(c.getString("lot_numbering_type"))
                                && !c.getString("lot_numbering_type").equals("null")) {
                            item1.setLot_numbering_type(c.getString("lot_numbering_type"));
                        }
                        if (c.has("lot_numbering_description")
                                && !TextUtils.isEmpty(c.getString("lot_numbering_description"))
                                && !c.getString("lot_numbering_description").equals("null")) {
                            item1.setLot_numbering_description(c.getString("lot_numbering_description"));
                        }
                        if (c.has("lots_total")
                                && !TextUtils.isEmpty(c.getString("lots_total"))
                                && !c.getString("lots_total").equals("null")) {
                            item1.setLots_total(c.getString("lots_total"));
                        }
                        if (c.has("marker_type")
                                && !TextUtils.isEmpty(c.getString("marker_type"))
                                && !c.getString("marker_type").equals("null")) {
                            item1.setMarker_type(c.getString("marker_type"));
                        }
                        if (c.has("no_parking_times")
                                && !TextUtils.isEmpty(c.getString("no_parking_times"))
                                && !c.getString("no_parking_times").equals("null")) {
                            item1.setNo_parking_times(c.getString("no_parking_times"));
                        }
                        if (c.has("off_peak_discount")
                                && !TextUtils.isEmpty(c.getString("off_peak_discount"))
                                && !c.getString("off_peak_discount").equals("null")) {
                            item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        }
                        if (c.has("off_peak_ends")
                                && !TextUtils.isEmpty(c.getString("off_peak_ends"))
                                && !c.getString("off_peak_ends").equals("null")) {
                            item1.setOff_peak_ends(c.getString("off_peak_ends"));
                        }
                        if (c.has("off_peak_starts")
                                && !TextUtils.isEmpty(c.getString("off_peak_starts"))
                                && !c.getString("off_peak_starts").equals("null")) {
                            item1.setOff_peak_starts(c.getString("off_peak_starts"));
                        }
                        if (c.has("parking_times")
                                && !TextUtils.isEmpty(c.getString("parking_times"))
                                && !c.getString("parking_times").equals("null")) {
                            item1.setParking_times(c.getString("parking_times"));
                        }
                        if (c.has("renewable")
                                && !TextUtils.isEmpty(c.getString("renewable"))
                                && !c.getString("renewable").equals("null")) {
                            item1.setRenewable(c.getString("renewable"));
                        }
                        if (c.has("title")
                                && !TextUtils.isEmpty(c.getString("title"))
                                && !c.getString("title").equals("null")) {
                            item1.setTitle(c.getString("title"));
                        }
                        if (c.has("user_id")
                                && !TextUtils.isEmpty(c.getString("user_id"))
                                && !c.getString("user_id").equals("null")) {
                            item1.setUser_id(c.getString("user_id"));
                        }
                        if (c.has("weekend_special_diff")
                                && !TextUtils.isEmpty(c.getString("weekend_special_diff"))
                                && !c.getString("weekend_special_diff").equals("null")) {
                            item1.setWeekend_special_diff(c.getString("weekend_special_diff"));
                        }
                        if (c.has("twp_id")
                                && !TextUtils.isEmpty(c.getString("twp_id"))
                                && !c.getString("twp_id").equals("null")) {
                            item1.setTwp_id(c.getString("twp_id"));
                        }
                        if (c.has("township_id")
                                && !TextUtils.isEmpty(c.getString("township_id"))
                                && !c.getString("township_id").equals("null")) {
                            item1.setTownship_id(c.getString("township_id"));
                        }
                        if (c.has("township_code")
                                && !TextUtils.isEmpty(c.getString("township_code"))
                                && !c.getString("township_code").equals("null")) {
                            item1.setTownship_code(c.getString("township_code"));
                        }
                        if (c.has("manager_id")
                                && !TextUtils.isEmpty(c.getString("manager_id"))
                                && !c.getString("manager_id").equals("null")) {
                            item1.setManager_id(c.getString("manager_id"));
                        }
                        if (c.has("company_id")
                                && !TextUtils.isEmpty(c.getString("company_id"))
                                && !c.getString("company_id").equals("null")) {
                            item1.setCompany_id(c.getString("company_id"));
                        }
                        if (c.has("division_id")
                                && !TextUtils.isEmpty(c.getString("division_id"))
                                && !c.getString("division_id").equals("null")) {
                            item1.setDivision_id(c.getString("division_id"));
                        }
                        if (c.has("dept_id")
                                && !TextUtils.isEmpty(c.getString("dept_id"))
                                && !c.getString("dept_id").equals("null")) {
                            item1.setDept_id(c.getString("dept_id"));
                        }
                        if (c.has("isKiosk")
                                && !TextUtils.isEmpty(c.getString("isKiosk"))
                                && !c.getString("isKiosk").equals("null")) {
                            item1.setIsKiosk(c.getString("isKiosk"));
                        }
                        if (c.has("manager_type_id")
                                && !TextUtils.isEmpty(c.getString("manager_type_id"))
                                && !c.getString("manager_type_id").equals("null")) {
                            item1.setManager_type_id(c.getString("manager_type_id"));
                        }
                        if (c.has("company_type_id")
                                && !TextUtils.isEmpty(c.getString("company_type_id"))
                                && !c.getString("company_type_id").equals("null")) {
                            item1.setCompany_type_id(c.getString("company_type_id"));
                        }
                        if (c.has("twp_id")
                                && !TextUtils.isEmpty(c.getString("twp_id"))
                                && !c.getString("twp_id").equals("null")) {
                            item1.setTwp_id(c.getString("twp_id"));
                        }
                        if (c.has("reservable")
                                && !TextUtils.isEmpty(c.getString("reservable"))
                                && !c.getString("reservable").equals("null")) {
                            if (c.getString("reservable").equals("1")) {
                                item1.setReservable(true);
                            } else if (c.getString("reservable").equals("0")) {
                                item1.setReservable(false);
                            } else {
                                item1.setReservable(c.getBoolean("reservable"));
                            }
                        }
                        if (c.has("distance")
                                && !TextUtils.isEmpty(c.getString("distance"))
                                && !c.getString("distance").equals("null")) {
                            item1.setDistances(c.getString("distance"));
                        }
                        if (c.has("day_type")
                                && !TextUtils.isEmpty(c.getString("day_type"))
                                && !c.getString("day_type").equals("null")) {
                            item1.setDay_type(c.getString("day_type"));
                        }
                        if (c.has("max_duration")
                                && !TextUtils.isEmpty(c.getString("max_duration"))
                                && !c.getString("max_duration").equals("null")) {
                            item1.setMax_duration(c.getString("max_duration"));
                        }
                        if (c.has("pricing")
                                && !TextUtils.isEmpty(c.getString("pricing"))
                                && !c.getString("pricing").equals("null")) {
                            item1.setPricing(c.getString("pricing"));
                        }
                        if (c.has("pricing_duration")
                                && !TextUtils.isEmpty(c.getString("pricing_duration"))
                                && !c.getString("pricing_duration").equals("null")) {
                            item1.setPricing_duration(c.getString("pricing_duration"));
                        }
                        if (c.has("premium_pricing")
                                && !TextUtils.isEmpty(c.getString("premium_pricing"))
                                && !c.getString("premium_pricing").equals("null")) {
                            item1.setPremium_pricing(c.getString("premium_pricing"));
                        }
                        if (c.has("duration_unit")
                                && !TextUtils.isEmpty(c.getString("duration_unit"))
                                && !c.getString("duration_unit").equals("null")) {
                            item1.setDuration_unit(c.getString("duration_unit"));
                        }
                        if (c.has("start_hour")
                                && !TextUtils.isEmpty(c.getString("start_hour"))
                                && !c.getString("start_hour").equals("null")) {
                            item1.setStart_hour(c.getString("start_hour"));
                        }
                        if (c.has("start_time")
                                && !TextUtils.isEmpty(c.getString("start_time"))
                                && !c.getString("start_time").equals("null")) {
                            item1.setStart_time(c.getString("start_time"));
                        }
                        if (c.has("end_hour")
                                && !TextUtils.isEmpty(c.getString("end_hour"))
                                && !c.getString("end_hour").equals("null")) {
                            item1.setEnd_hour(c.getString("end_hour"));
                        }
                        if (c.has("end_time")
                                && !TextUtils.isEmpty(c.getString("end_time"))
                                && !c.getString("end_time").equals("null")) {
                            item1.setEnd_time(c.getString("end_time"));
                        }
                        if (c.has("time_rule")
                                && !TextUtils.isEmpty(c.getString("time_rule"))
                                && !c.getString("time_rule").equals("null")) {
                            item1.setTime_rule(c.getString("time_rule"));
                        }
                        if (c.has("lot_numbering_type")
                                && !TextUtils.isEmpty(c.getString("lot_numbering_type"))
                                && !c.getString("lot_numbering_type").equals("null")) {
                            item1.setLot_numbering_type(c.getString("lot_numbering_type"));
                        }
                        if (c.has("lot_numbering_description")
                                && !TextUtils.isEmpty(c.getString("lot_numbering_description"))
                                && !c.getString("lot_numbering_description").equals("null")) {
                            item1.setLot_numbering_description(c.getString("lot_numbering_description"));
                        }
                        if (c.has("currency")
                                && !TextUtils.isEmpty(c.getString("currency"))
                                && !c.getString("currency").equals("null")) {
                            item1.setCurrency(c.getString("currency"));
                        }
                        if (c.has("currency_symbol")
                                && !TextUtils.isEmpty(c.getString("currency_symbol"))
                                && !c.getString("currency_symbol").equals("null")) {
                            item1.setCurrency_symbol(c.getString("currency_symbol"));
                        }
                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item1.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }
                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item1.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }
                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item1.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }
                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item1.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }
                        if (c.has("Reservation_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_Fee"))
                                && !c.getString("Reservation_PostPayment_Fee").equals("null")) {
                            item1.setReservation_PostPayment_Fee(c.getString("Reservation_PostPayment_Fee"));
                        }
                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item1.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }
                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item1.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }
                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item1.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }
                        if (c.has("ParkNow_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_Fee"))
                                && !c.getString("ParkNow_PostPayment_Fee").equals("null")) {
                            item1.setParkNow_PostPayment_Fee(c.getString("ParkNow_PostPayment_Fee"));
                        }
                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item1.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }
                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item1.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }
                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item1.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }
                        if (c.has("number_of_reservable_spots")
                                && !TextUtils.isEmpty(c.getString("number_of_reservable_spots"))
                                && !c.getString("number_of_reservable_spots").equals("null")) {
                            item1.setNumber_of_reservable_spots(c.getString("number_of_reservable_spots"));
                        }
                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item1.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }
                        if (c.has("address")
                                && !TextUtils.isEmpty(c.getString("address"))
                                && !c.getString("address").equals("null")) {
                            item1.setAddress(c.getString("address"));
                        }
                        if (c.has("no_parking_times")
                                && !TextUtils.isEmpty(c.getString("no_parking_times"))
                                && !c.getString("no_parking_times").equals("null")) {
                            item1.setNo_parking_times(c.getString("no_parking_times"));
                        }
                        if (c.has("off_peak_discount")
                                && !TextUtils.isEmpty(c.getString("off_peak_discount"))
                                && !c.getString("off_peak_discount").equals("null")) {
                            item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        }
                        if (c.has("off_peak_ends")
                                && !TextUtils.isEmpty(c.getString("off_peak_ends"))
                                && !c.getString("off_peak_ends").equals("null")) {
                            item1.setOff_peak_ends(c.getString("off_peak_ends"));
                        }
                        if (c.has("off_peak_starts")
                                && !TextUtils.isEmpty(c.getString("off_peak_starts"))
                                && !c.getString("off_peak_starts").equals("null")) {
                            item1.setOff_peak_starts(c.getString("off_peak_starts"));
                        }
                        if (c.has("parking_times")
                                && !TextUtils.isEmpty(c.getString("parking_times"))
                                && !c.getString("parking_times").equals("null")) {
                            item1.setParking_times(c.getString("parking_times"));
                        }
                        if (c.has("title")
                                && !TextUtils.isEmpty(c.getString("title"))
                                && !c.getString("title").equals("null")) {
                            item1.setTitle(c.getString("title"));
                        }
                        for (int j = 0; j < parking_rules.size(); j++) {
                            if (item1.getLocation_code().equals(parking_rules.get(j).getLocation_code())) {
                                item1.setPricing(parking_rules.get(j).getPricing());
                                item1.setPricing_duration(parking_rules.get(j).getPricing_duration());
                                item1.setDuration_unit(parking_rules.get(j).getDuration_unit());
                                item1.setMax_hours(parking_rules.get(j).getMax_hours());
                                item1.setStart_time(parking_rules.get(j).getStart_time());
                                item1.setStart_hour(parking_rules.get(j).getStart_hour());
                                item1.setEnd_time(parking_rules.get(j).getEnd_time());
                                item1.setEnd_hour(parking_rules.get(j).getEnd_hour());
                                item1.setTime_rule(parking_rules.get(j).getTime_rule());
                            }
                        }
                        findnearbyarray.add(item1);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   fetchNearByList:  Error:  " + e.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   fetchNearByList:  Error:  " + e.getMessage());
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null) {
                rlProgressbar.setVisibility(View.GONE);
                Log.e("#DEBUG", "   fetchNearByList:  findnearbyarray: Size: " + findnearbyarray.size());
                //progressBar.setVisibility(View.GONE);
                Resources rs;
                mapisloadcallback = false;
                managedmapisloaded = false;
                managedpinisclick = true;
                fingnearbywithcurrent();
            }
            super.onPostExecute(s);
        }
    }

    public void fingnearbywithcurrent() {
        nearbylist.clear();
        for (int j = 0; j < findnearbyarray.size(); j++) {
            String loction_code = findnearbyarray.get(j).getLocation_code();

//                    title = findnearbyarray.get(j).getTitle();
//                    address = findnearbyarray.get(j).getAddress();
//                    html = findnearbyarray.get(j).getHtml();
            String distances = findnearbyarray.get(j).getDistancea();
            String parking_time = findnearbyarray.get(j).getParking_times();
            String no_parking_time = findnearbyarray.get(j).getNo_parking_times();
            String marker = findnearbyarray.get(j).getMarker_type();
            String lat = findnearbyarray.get(j).getLat();
            String lang = findnearbyarray.get(j).getLng();
            String addres = findnearbyarray.get(j).getAddress();
            String time_rlue = findnearbyarray.get(j).getTime_rule();
            String lots_av = findnearbyarray.get(j).getLots_aval();
            String lost_total = findnearbyarray.get(j).getLots_total();

//                    time_rules = findnearbyarray.get(j).getTime_rule();
//                    max_hours = findnearbyarray.get(j).getMax_time();

            //  locationnamne = findnearbyarray.get(j).getLoationname();
            // zip_code = findnearbyarray.get(j).getZip_code();
            // city = findnearbyarray.get(j).getCity();
            // state = findnearbyarray.get(j).getState();
            // String markertype = findnearbyarray.get(j).getMarker_type();
            if (!TextUtils.isEmpty(findnearbyarray.get(j).getStart_hour())
                    && !TextUtils.isEmpty(findnearbyarray.get(j).getEnd_hour())
//                    && !TextUtils.isEmpty(findnearbyarray.get(j).getThis_day())
            ) {
                Calendar c = Calendar.getInstance();
                int hour = c.get(Calendar.HOUR);
                SimpleDateFormat sdf = new SimpleDateFormat("EEE");
                Date d = new Date();
                String today = sdf.format(d).toLowerCase();
                int start_hours = Integer.parseInt(findnearbyarray.get(j).getStart_hour());
                int end_hours = Integer.parseInt(findnearbyarray.get(j).getEnd_hour());


                item ii = new item();
                ii.setDistancea(distances);
                ii.setPricing_duration(findnearbyarray.get(j).getPricing_duration());
                ii.setPricing(findnearbyarray.get(j).getPricing());
                ii.setNo_parking_times(no_parking_time);
                ii.setMarker(marker);
                ii.setMarker_type(marker);
                ii.setLang(lang);
                ii.setLng(lang);
                ii.setLat(lat);
                ii.setIsslected(false);
                ii.setTime_rule(time_rlue);
                ii.setMax_hours(findnearbyarray.get(j).getMax_time());
                ii.setLocation_code(loction_code);
                ii.setLocation_name(findnearbyarray.get(j).getLocation_name());
                ii.setLocation_id(findnearbyarray.get(j).getLocation_id());
                ii.setTownship_code(findnearbyarray.get(j).getTownship_code());
                ii.setManager_type_id(findnearbyarray.get(j).getManager_type_id());
                ii.setCompany_id(findnearbyarray.get(j).getCompany_id());
                ii.setTwp_id(findnearbyarray.get(j).getTwp_id());
                ii.setLoationname("");
                ii.setZip_code("");
                ii.setCity("");
                ii.setLat(lat);
                ii.setLang(lang);
                ii.setAddress(findnearbyarray.get(j).getAddress());
                ii.setHtml("");
                ii.setTitle(findnearbyarray.get(j).getTitle());
                ii.setParking_times(parking_time);
                ii.setActive(findnearbyarray.get(j).getActive());
                ii.setLots_aval(lots_av);
                ii.setTotal_lots(lost_total);
                ii.setDuration_unit(findnearbyarray.get(j).getDuration_unit());
                ii.setCustom_notice(findnearbyarray.get(j).getCustom_notice());
                ii.setMax_duration(findnearbyarray.get(j).getMax_duration());
                nearbylist.add(ii);

            }
        }
        Resources rs;
        //list_nearby.setAdapter(null);
        rl_nearbylist.setVisibility(View.VISIBLE);
        adpater = new adpaternearby(getActivity(), nearbylist, rs = getResources());
        list_nearby.setAdapter(adpater);
        list_nearby.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (nearbylist.size() != 0) {
                    item itemLocation = new item();
                    itemLocation.setLat(String.valueOf(nearbylist.get(position).getLat()));
                    itemLocation.setLng(String.valueOf(nearbylist.get(position).getLng()));
                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ReservationParkingSpotsDetailsFragment pay = new ReservationParkingSpotsDetailsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("selectedSpot", new Gson().toJson(nearbylist.get(nearbylist.indexOf(itemLocation))));
                    bundle.putString("parkingType", parkingType);
                    bundle.putBoolean("isReserveSpot", true);
                    pay.setArguments(bundle);
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                }

            }
        });
        img_close_nearbylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_nearbylist.setVisibility(View.GONE);
            }
        });
        // shownearbyparkin(vchome.this);
    }

    public double getDistance(LatLng LatLng1) {
        double distance = 0;
        Location locationA = new Location("A");
        locationA.setLatitude(LatLng1.latitude);
        locationA.setLongitude(LatLng1.longitude);
        Location locationB = new Location("B");
        locationB.setLatitude(latitude);
        locationB.setLongitude(longitude);
        distance = locationA.distanceTo(locationB);
        return distance;
    }

    public class adpaternearby extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public adpaternearby(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {


                vi = inflater.inflate(R.layout.listofnearbyparking, null);
                holder = new ViewHolder();
                holder.txtparkinname = vi.findViewById(R.id.parkingname);
                holder.txtprices = vi.findViewById(R.id.txtrate);
                holder.txtmiles = vi.findViewById(R.id.txtmile);
                holder.txtparkhere = vi.findViewById(R.id.txtlable);
                holder.img = vi.findViewById(R.id.imgpark);
                holder.txtlabel = vi.findViewById(R.id.label);
                holder.txt_new_rate = vi.findViewById(R.id.txt_rate);
                holder.txt_parking_status = vi.findViewById(R.id.txt_open);
                holder.txttime_rules = vi.findViewById(R.id.time_rule);
                holder.txtmax = vi.findViewById(R.id.max);
                holder.txtpricing = vi.findViewById(R.id.pricing);
                holder.txtpricing_duration = vi.findViewById(R.id.pricing_duration);
                holder.txtlocation_code = vi.findViewById(R.id.location_code);
                holder.txtlocationname = vi.findViewById(R.id.location_name);
                holder.txtzipcode = vi.findViewById(R.id.zip_code);
                holder.txtcity = vi.findViewById(R.id.city);
                holder.txtlat = vi.findViewById(R.id.lat);
                holder.txtlang = vi.findViewById(R.id.lang);
                holder.txt_add_code = vi.findViewById(R.id.txt_address_location_code);

                vi.setTag(holder);
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
                holder.txtparkinname.setText("No Data");
//                btnback.setVisibility(View.GONE);
//                btnsearch.setVisibility(View.VISIBLE);
//                rl_nearbylist.setVisibility(View.GONE);
//                Allparking_Nearby_click = false;
            } else {
                /***** Get each Model object from Arraylist ********/
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                /************  Set Model values in Holder elements ***********/
                String cat = tempValues.getLang();
                if (cat == null) {
                    holder.txtmiles.setVisibility(View.GONE);
                } else if (cat != null) {
                    if (tempValues.getIsslected() == false) {

                        holder.txtparkhere.setVisibility(View.GONE);
                    } else {

                        holder.txtparkhere.setVisibility(View.VISIBLE);
                    }
                    String distances = tempValues.getDistancea();
                   /* double dis = Double.parseDouble(distances);
                    double ans = dis / 5280;*/
                    /*float kk = (float) Math.round(ans * 100) / 100;*/
                    String finalans = "";
                    if (!tempValues.getLat().equals("") && !tempValues.getLang().equals("")) {
                        LatLng latLng = new LatLng(Double.parseDouble(tempValues.getLat()), Double.parseDouble(tempValues.getLang()));
                        double dis = getDistance(latLng);
                        double ff = dis * 0.000621371192;
                        finalans = String.format("%.2f", ff);

                    }

                    if (!finalans.equals("50.0")) {
                        String marker = tempValues.getMarker();
                        if (marker.equals("Free")) {
                            holder.txt_new_rate.setText("");
                            holder.txtparkinname.setText("FREE Public Parking -" + tempValues.getLocation_code());
                            if (tempValues.getParking_times() != "null") {
                                holder.txtprices.setText(tempValues.getParking_times());

                            } else {
                                holder.txtprices.setText("");
                            }

                            if (finalans.equals("1.0")) {
                                holder.txtmiles.setText(finalans + " Mile");
                            } else {
                                holder.txtmiles.setText(finalans + " Miles");
                            }

                            holder.img.setBackgroundResource(R.mipmap.park_free_circle_pin);

                        } else if (marker.equals("Managed Paid") || marker.equals("Managed Free")) {
                            holder.txtparkinname.setText(tempValues.getTitle() + " -" + tempValues.getLocation_code());

                            if (tempValues.getParking_times().equals("null")) {

                                holder.txt_new_rate.setText("$" + tempValues.getPricing());
                                holder.txtprices.setText("");
                            } else {
                                holder.txtprices.setText(tempValues.getParking_times());
                                holder.txt_new_rate.setText("$" + tempValues.getPricing());
                            }

                            if (finalans.equals("1.0")) {
                                holder.txtmiles.setText(finalans + " Mile");
                            } else {
                                holder.txtmiles.setText(finalans + " Miles");
                            }

                            holder.img.setBackgroundResource(R.mipmap.park_managed_circle);

                        } else if (marker.equals("Paid")) {
                            holder.txtparkinname.setText("Public  Parking -" + tempValues.getLocation_code());


                            if (tempValues.getParking_times().equals("null")) {
                                holder.txt_new_rate.setText("$" + tempValues.getPricing());
                                holder.txtprices.setText("");
                            } else {
                                holder.txtprices.setText(tempValues.getParking_times());
                                holder.txt_new_rate.setText("$" + tempValues.getPricing());
                            }
                            if (finalans.equals("1.0")) {
                                holder.txtmiles.setText(finalans + " Mile");
                            } else {
                                holder.txtmiles.setText(finalans + " Miles");
                            }
                            holder.img.setBackgroundResource(R.mipmap.park_paid_circle_pin);
                        }

                        if (!tempValues.getPricing().equals("")) {
                            double prices = Double.parseDouble(tempValues.getPricing());
                            String lots_aval = data.get(position).getLots_aval();
                            lots_aval = lots_aval != null ? (!lots_aval.equals("") ? (!lots_aval.equals("null") ? lots_aval : "") : "") : "";
                            if (prices != -1.0 || prices != 1) {
                                holder.txt_parking_status.setVisibility(View.VISIBLE);
                                holder.txt_parking_status.setText("OPEN");
                                holder.txt_parking_status.setBackgroundColor(Color.parseColor("#3CB385"));
                            } else if (lots_aval.equals("0")) {
                                holder.txt_parking_status.setVisibility(View.VISIBLE);
                                holder.txt_parking_status.setText("CLOSED");
                                holder.txt_parking_status.setBackgroundColor(Color.parseColor("#E9967A"));
                            } else {
                                holder.txt_parking_status.setVisibility(View.VISIBLE);
                                holder.txt_parking_status.setText("CLOSED");
                                holder.txt_parking_status.setBackgroundColor(Color.parseColor("#E9967A"));
                            }

                        } else {
                            holder.txt_parking_status.setVisibility(View.GONE);
                        }
                        String pinaddress = tempValues.getAddress();
                        String pinlocation_code = tempValues.getLocation_code();
                        holder.txt_add_code.setText(pinaddress);
                        holder.txttime_rules.setText(tempValues.getTime_rule());
                        holder.txtmax.setText(tempValues.getMax_hours());
                        holder.txtpricing.setText(tempValues.getPricing());
                        holder.txtpricing_duration.setText(tempValues.getPricing_duration());
                        holder.txtlocation_code.setText(tempValues.getLocation_code());
                        holder.txtlocationname.setText(tempValues.getLoationname());
                        holder.txtzipcode.setText(tempValues.getZip_code());
                        holder.txtcity.setText(tempValues.getCity());
                        holder.txtlat.setText(tempValues.getLat());
                        holder.txtlang.setText(tempValues.getLang());
                    }
                }
            }
            if (position == 0) {
                c = 1;
                holder.txtlabel.setVisibility(View.VISIBLE);
            } else {
                holder.txtlabel.setVisibility(View.GONE);
            }
            c = 1;
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txtparkinname, txtprices, txtmiles, txtparkhere, txtlabel, txttime_rules, txtmax, txtpricing, txtpricing_duration, txtlocation_code, txtlocationname, txtzipcode, txtcity, txtlat, txtlang, txt_parking_status, txt_new_rate;
            TextView txt_add_code;
            ImageView img;
        }
    }

    public class getparkingrules extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String parkingurl = "_table/township_parking_rules";
        ArrayList<item> temp_array = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            if (parkingType.equals("Township")) {
                parkingurl = "_table/township_parking_rules";
            } else if (parkingType.equals("Commercial")) {
                parkingurl = "_table/google_parking_rules";
            } else if (parkingType.equals("Other")) {
                parkingurl = "_table/other_parking_rules";
            }
            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            temp_array.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setId(c.getString("id"));
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setTime_rule(c.getString("time_rule"));
                        item1.setDay_rule(c.getString("day_type"));
                        item1.setMax_time(c.getString("max_duration"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setActive(c.getString("active"));
                        item1.setPricing(c.getString("pricing"));
                        item1.setPricing_duration(c.getString("pricing_duration"));
                        item1.setStart_time(c.getString("start_time"));
                        item1.setEnd_time(c.getString("end_time"));
                        item1.setMax_hours(c.getString("duration_unit"));
                        item1.setStart_hour(c.getString("start_hour"));
                        item1.setEnd_hour(c.getString("end_hour"));
                        item1.setLoationcode1(c.getString("location_code"));
                        item1.setMarker_type(c.getString("marker_type"));
                        item1.setDuration_unit(c.getString("duration_unit"));
                        item1.setMax_duration(c.getString("max_duration"));
                        item1.setLocation_name(c.getString("location_name"));

                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item1.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }

                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item1.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }

                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item1.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }

                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item1.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }

                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item1.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }

                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item1.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item1.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }

                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item1.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }

                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item1.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }

                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item1.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }

                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item1.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }

                        if (c.has("Reservation_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_Fee"))
                                && !c.getString("Reservation_PostPayment_Fee").equals("null")) {
                            item1.setReservation_PostPayment_Fee(c.getString("Reservation_PostPayment_Fee"));
                        }

                        if (c.has("ParkNow_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_Fee"))
                                && !c.getString("ParkNow_PostPayment_Fee").equals("null")) {
                            item1.setParkNow_PostPayment_Fee(c.getString("ParkNow_PostPayment_Fee"));
                        }

                        if (c.has("number_of_reservable_spots")
                                && !TextUtils.isEmpty(c.getString("number_of_reservable_spots"))
                                && !c.getString("number_of_reservable_spots").equals("null")) {
                            item1.setNumber_of_reservable_spots(c.getString("number_of_reservable_spots"));
                        }
                        temp_array.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs;
            if (getActivity() != null) {
                rlProgressbar.setVisibility(View.GONE);
                if (json != null) {
                    if (temp_array.size() > 0) {
                        parking_rules.clear();
                        parking_rules.addAll(temp_array);
                        temp_array.clear();
                        new fetchReservationParkingSpots().execute();

                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    @Override
    protected void initViews(View v) {

        ivBtnFindNearBy = v.findViewById(R.id.ivBtnFindNearBy);
        ivBtnOtherCity = v.findViewById(R.id.ivBtnOtherCity);
        rlProgressbar = v.findViewById(R.id.rlProgressbar);

        rl_nearbylist = v.findViewById(R.id.rl_nearbylist);
        list_nearby = v.findViewById(R.id.listofnearbyparking);
        img_close_nearbylist = v.findViewById(R.id.close);

        rl_searchinfo = v.findViewById(R.id.rl_searchinfo);
        rl_searchinfo.setVisibility(View.GONE);

        llBottomOptions = v.findViewById(R.id.llBottomOptions);
        rlBtnTownship = v.findViewById(R.id.rlBtnTownship);
        rlBtnCommercial = v.findViewById(R.id.rlBtnCommercial);
        rlBtnOther = v.findViewById(R.id.rlBtnOther);
        ivTownship = v.findViewById(R.id.ivTownship);
        ivCommercial = v.findViewById(R.id.ivCommercial);
        ivOther = v.findViewById(R.id.ivOther);

    }
}
