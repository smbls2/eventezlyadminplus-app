package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class Fragment_permit_definitions extends Fragment implements SearchView.OnQueryTextListener {

    ConnectionDetector cd;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar;
    View mView;
    String id, twcode, role;
    ListView listofvehicleno;
    RelativeLayout ll_ADDAPERMIT;
    ArrayList<item> array_parking_permits;
    AdapterParking_Permit adapterParking_permit;
    public static ArrayList<item> array_list;

    public static String Township_Name = "", Township_ID = "", Township_Code = "";
    public String file_download_path = "";
    File dir_File;
    AmazonS3 s3;
    TransferUtility transferUtility;
    SearchView mSearchView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_permit_definitions, container, false);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        id = logindeatl.getString("id", "null");
        role = logindeatl.getString("role", "null");
        twcode = logindeatl.getString("twcode", "null");

        file_download_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" +
                getActivity().getPackageName() + "/Images/";
        dir_File = new File(file_download_path);
        if (!dir_File.exists()) {
            if (!dir_File.mkdirs()) {
            }
        }
        s3 = new AmazonS3Client(new BasicAWSCredentials("AKIAJNG22KFRVUAICSEA", "NSrQR/yAg52RPD3kp3FMb3NO+Vrxuty4iAwPU4th"));
        transferUtility = new TransferUtility(s3, getActivity().getApplicationContext());

        ints();

        return mView;
    }

    public void ints() {
        mSearchView = (SearchView) mView.findViewById(R.id.searchView1);
        ll_ADDAPERMIT = (RelativeLayout) mView.findViewById(R.id.ll_ADDAPERMIT);
        listofvehicleno = (ListView) mView.findViewById(R.id.listofvehicle);
        progressBar = (ProgressBar) mView.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) mView.findViewById(R.id.rl_progressbar);
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(true);
        mSearchView.setQueryHint("Search Permit");

        onClicks();

        new getPermitData().execute();

    }

    public void onClicks() {
        ll_ADDAPERMIT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment_permit_definitions_One pay = new Fragment_permit_definitions_One();
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapterParking_permit.getFilter().filter(newText);
        return true;
    }

    public class getPermitData extends AsyncTask<String, String, String> {

        String vehiclenourl = "_table/parking_permits";
        JSONObject json = null;

        @Override
        protected void onPreExecute() {
            array_parking_permits = new ArrayList<item>();
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            if (role.equalsIgnoreCase("TwpAdmin")) {
                try {
                    vehiclenourl = "_table/parking_permits?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (role.equalsIgnoreCase("TwpInspector")) {
                try {
                    vehiclenourl = "_table/parking_permits?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (role.equalsIgnoreCase("TwpBursar")) {
                try {
                    vehiclenourl = "_table/parking_permits?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                vehiclenourl = "_table/parking_permits";
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("get_vehicle_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;

            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);
                    Log.e("rerpones", String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setId(c.getString("id"));
                        item1.setDate(c.getString("date_time"));
                        item1.setManager_type(c.getString("manager_type"));
                        item1.setManager_type_id(c.getString("manager_type_id"));
                        item1.setTwp_id(c.getString("twp_id"));
                        item1.setTownship_code(c.getString("township_code"));
                        item1.setTownship_name(c.getString("township_name"));
                        item1.setPermit_type(c.getString("permit_type"));
                        item1.setPermit_name(c.getString("permit_name"));
                        item1.setCovered_locations(c.getString("covered_locations"));
                        item1.setRequirements(c.getString("requirements"));
                        item1.setAppl_req_download(c.getString("appl_req_download"));
                        item1.setCost(c.getString("cost"));
                        item1.setYear(c.getString("year"));
                        item1.setLocation_address(c.getString("location_address"));
                        item1.setActive(c.getString("active"));
                        item1.setScheme_type(c.getString("scheme_type"));
                        item1.setPermit_prefix(c.getString("permit_prefix"));
                        item1.setPermit_nextnum(c.getString("permit_nextnum"));
                        item1.setExpires_by(c.getString("expires_by"));
                        item1.setRenewable(c.getString("renewable"));
                        item1.setIsslected(false);
                        array_parking_permits.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity != null) {
                    if (array_parking_permits.size() > 0) {
                        adapterParking_permit = new AdapterParking_Permit(getActivity(), array_parking_permits, getResources());
                        listofvehicleno.setAdapter(adapterParking_permit);
                    } else {
                        //txt_No_Record.setVisibility(View.VISIBLE);
                        //lv_Permit.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

    public class AdapterParking_Permit extends BaseAdapter implements View.OnClickListener, Filterable {
        private Activity activity;
        private ArrayList<item> data;
        private ArrayList<item> Olddata;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        CustomFilter filter;

        public AdapterParking_Permit(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Filter getFilter() {
            if (filter == null) {
                filter = new CustomFilter(data, AdapterParking_Permit.this);
            }
            return filter;
        }

        public class ViewHolder {
            public TextView txt_township_code, txt_permit_type, txt_cost, txt_township_name, txt_permit_name;
            TextView txt_Expired, txt_CoveredLocation, txt_edit, txt_PermitName;
            ImageView img_township, img_pdf;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_parking_permits_1, null);
                holder = new ViewHolder();

                holder.txt_township_code = (TextView) vi.findViewById(R.id.txt_township_code);
                holder.txt_permit_type = (TextView) vi.findViewById(R.id.txt_permit_type);
                holder.txt_Expired = (TextView) vi.findViewById(R.id.txt_Expired);
                holder.txt_CoveredLocation = (TextView) vi.findViewById(R.id.txt_CoveredLocation);
                holder.txt_cost = (TextView) vi.findViewById(R.id.txt_cost);
                holder.txt_township_name = (TextView) vi.findViewById(R.id.txt_township_name);
                holder.txt_permit_name = (TextView) vi.findViewById(R.id.txt_permit_name);
                holder.txt_edit = (TextView) vi.findViewById(R.id.txt_edit);
                holder.img_township = (ImageView) vi.findViewById(R.id.img_township);
                holder.img_pdf = (ImageView) vi.findViewById(R.id.img_pdf);
                holder.txt_PermitName = (TextView) vi.findViewById(R.id.txt_PermitName);

                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }

            try {
                state = data.get(position);
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.txt_township_code.setText(data.get(position).getTownship_code());
            holder.txt_permit_type.setText(data.get(position).getPermit_type());
            holder.txt_PermitName.setText(data.get(position).getPermit_name());
            holder.txt_Expired.setText(data.get(position).getExpires_by());
            holder.txt_cost.setText("Fee: " + data.get(position).getCost());
            holder.txt_CoveredLocation.setText("Covered Location: " + data.get(position).getCovered_locations());
            holder.txt_permit_name.setText(data.get(position).getRequirements());

            holder.img_pdf.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!data.get(position).getAppl_req_download().equalsIgnoreCase("") &&
                            !data.get(position).getAppl_req_download().equalsIgnoreCase("null")) {
                        getPdfFile(data.get(position).getAppl_req_download());
                    } else {
                        Toast.makeText(getActivity(), "File not added...", Toast.LENGTH_LONG).show();
                    }
                }
            });

            holder.txt_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    array_list = new ArrayList<item>();
                    Township_Name = data.get(position).getTownship_name();
                    array_list.add(data.get(position));

                    Bundle bundle = new Bundle();
                    bundle.putString("manager_id", data.get(position).getId());
                    bundle.putString("township_code", data.get(position).getTownship_code());
                    bundle.putString("permit_name", data.get(position).getPermit_name());
                    bundle.putString("township_name", data.get(position).getTownship_name());
                    bundle.putString("permit_types", data.get(position).getPermit_type());
                    bundle.putString("manager_type", data.get(position).getManager_type());

                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Fragment_permit_definitions_Three_Edit pay = new Fragment_permit_definitions_Three_Edit();
                    pay.setArguments(bundle);
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                }
            });

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

    public class CustomFilter extends Filter {

        AdapterParking_Permit adapter;
        ArrayList<item> CategoryArrayList;

        public CustomFilter(ArrayList<item> categoryArrayList, AdapterParking_Permit context) {
            adapter = context;
            CategoryArrayList = categoryArrayList;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                constraint = constraint.toString().toUpperCase();
                ArrayList<item> filteredPlayers = new ArrayList<>();
                for (int i = 0; i < CategoryArrayList.size(); i++) {
                    if (CategoryArrayList.get(i).getTownship_code().toUpperCase().contains(constraint)) {
                        filteredPlayers.add(CategoryArrayList.get(i));
                    }
                }

                results.count = filteredPlayers.size();
                results.values = filteredPlayers;
            } else {
                results.count = CategoryArrayList.size();
                results.values = CategoryArrayList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.data = (ArrayList<item>) results.values;
            adapter.notifyDataSetChanged();
        }
    }


    public void getPdfFile(String pdf_download) {
        Log.e("pdf_download ", " : " + pdf_download);
        rl_progressbar.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        try {
            final File file_exists = new File(file_download_path, pdf_download);
            if (file_exists.exists()) {
                rl_progressbar.setVisibility(View.GONE);
                MimeTypeMap myMime = MimeTypeMap.getSingleton();
                Intent newIntent = new Intent(Intent.ACTION_VIEW);
                String mimeType = myMime.getMimeTypeFromExtension(fileExt(String.valueOf(((file_exists.getAbsolutePath())))).substring(1));
                newIntent.setDataAndType(Uri.fromFile((new File(file_exists.getAbsolutePath()))), mimeType);
                newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                try {
                    getActivity().startActivity(newIntent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getActivity(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
                }
            } else {
                TransferObserver downloadObserver = transferUtility.download("parkezly-images", "/images/" + pdf_download, file_exists);
                downloadObserver.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (TransferState.COMPLETED == state) {
                            rl_progressbar.setVisibility(View.GONE);
                            MimeTypeMap myMime = MimeTypeMap.getSingleton();
                            Intent newIntent = new Intent(Intent.ACTION_VIEW);
                            String mimeType = myMime.getMimeTypeFromExtension(fileExt(String.valueOf(((file_exists.getAbsolutePath())))).substring(1));
                            newIntent.setDataAndType(Uri.fromFile((new File(file_exists.getAbsolutePath()))), mimeType);
                            newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            try {
                                getActivity().startActivity(newIntent);
                            } catch (ActivityNotFoundException e) {
                                Toast.makeText(getActivity(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                        int percentDone = (int) percentDonef;
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        ex.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();
        }
    }

}
