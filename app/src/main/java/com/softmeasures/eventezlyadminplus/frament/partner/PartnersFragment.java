package com.softmeasures.eventezlyadminplus.frament.partner;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1;
import com.softmeasures.eventezlyadminplus.frament.frg_private_selecte_location;
import com.softmeasures.eventezlyadminplus.models.Manager;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class PartnersFragment extends BaseFragment {

    private PartnerAdapter partnerAdapter;
    private ArrayList<Manager> managers = new ArrayList<>();
    private RelativeLayout rlProgressbar;
    private TextView tvTitle;
    private String parkingType = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            parkingType = getArguments().getString("parkingType");
        }
        return inflater.inflate(R.layout.frag_partners, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        new fetchManagers().execute();

    }

    public class fetchManagers extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String parkingurl = "_table/townships_manager";

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            if (parkingType.equals("Commercial")) {
                parkingurl = "_table/commercial_manager";
            } else if (parkingType.equals("Other")) {
                parkingurl = "_table/other_private_manager";
            }
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            managers.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("#DEBUG", "   fetchManagers:  URL:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        Manager manager = new Manager();
                        manager.setId(c.getInt("id"));
                        manager.setDate_time(c.getString("date_time"));
                        manager.setManager_id(c.getString("manager_id"));
                        manager.setManager_type(c.getString("manager_type"));
                        manager.setManager_type_id(c.getInt("manager_type_id"));
                        manager.setLot_manager(c.getString("lot_manager"));
                        manager.setStreet(c.getString("street"));
                        manager.setAddress(c.getString("address"));
                        manager.setState(c.getString("state"));
                        manager.setState_name(c.getString("state_name"));
                        manager.setCity(c.getString("city"));
                        manager.setCountry(c.getString("country"));
                        manager.setZip(c.getString("zip"));
                        manager.setContact_person(c.getString("contact_person"));
                        manager.setContact_title(c.getString("contact_title"));
                        manager.setContact_phone(c.getString("contact_phone"));
                        manager.setContact_email(c.getString("contact_email"));
                        manager.setTownship_logo(c.getString("township_logo"));
                        manager.setOfficial_logo(c.getString("official_logo"));
                        manager.setUser_id(c.getInt("user_id"));
                        managers.add(manager);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs = getResources();
            if (getActivity() != null) {
                rlProgressbar.setVisibility(View.GONE);
                if (managers.size() != 0) {
                    partnerAdapter.notifyDataSetChanged();
                }
            }
            super.onPostExecute(s);
        }
    }


    @Override
    protected void updateViews() {
        if (parkingType.equals("Commercial")) {
            tvTitle.setText("Select Commercial");
        } else if (parkingType.equals("Other")) {
            tvTitle.setText("Select Private");
        }
    }

    @Override
    protected void setListeners() {

    }

    @Override
    protected void initViews(View v) {
        RecyclerView rvPartners = v.findViewById(R.id.rvPartners);
        rlProgressbar = v.findViewById(R.id.rlProgressbar);
        tvTitle = v.findViewById(R.id.tvTitle);

        partnerAdapter = new PartnerAdapter();
        rvPartners.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvPartners.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        rvPartners.setAdapter(partnerAdapter);
    }

    private class PartnerAdapter extends RecyclerView.Adapter<PartnerAdapter.PartnerHolder> {

        @NonNull
        @Override
        public PartnerAdapter.PartnerHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new PartnerHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_partner_new,
                    viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull PartnerAdapter.PartnerHolder holder, int i) {
            Manager manager = managers.get(i);
            if (manager != null) {
                holder.tvName.setText(manager.getLot_manager());
                holder.tvAddress.setText(manager.getAddress());
                Picasso.with(getActivity()).load(manager.getTownship_logo()).into(holder.ivPartner);
            }
        }

        @Override
        public int getItemCount() {
            return managers.size();
        }

        class PartnerHolder extends RecyclerView.ViewHolder {
            ImageView ivPartner;
            TextView tvName, tvAddress;

            PartnerHolder(@NonNull View itemView) {
                super(itemView);

                ivPartner = itemView.findViewById(R.id.ivPartner);
                tvName = itemView.findViewById(R.id.tvName);
                tvAddress = itemView.findViewById(R.id.tvAddress);

                itemView.setOnClickListener(v -> {
                    final int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        //Do stuff here
                        myApp.setSelectedManager(managers.get(pos));
                        if (parkingType.equals("Other")) {
                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            frg_private_selecte_location pay = new frg_private_selecte_location();
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.add(R.id.My_Container_1_ID, pay);
                            fragmentStack.lastElement().onPause();
                            ft.hide(fragmentStack.lastElement());
                            fragmentStack.push(pay);
                            ft.commitAllowingStateLoss();
                        } else if (parkingType.equals("Commercial")) {
                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            frg_commercial_edit_add_step1 pay = new frg_commercial_edit_add_step1();
                            pay.setArguments(getArguments());
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.add(R.id.My_Container_1_ID, pay);
                            fragmentStack.lastElement().onPause();
                            ft.hide(fragmentStack.lastElement());
                            fragmentStack.push(pay);
                            ft.commitAllowingStateLoss();
                        }
                    }
                });

            }
        }
    }
}
