package com.softmeasures.eventezlyadminplus.frament.g_youtube;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragGYoutubeLiveStreamsBinding;
import com.softmeasures.eventezlyadminplus.models.YSnippet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GYoutubeLiveStreamsFragment extends BaseFragment {

    private FragGYoutubeLiveStreamsBinding binding;

    private StreamAdapter streamAdapter;
    private ArrayList<YSnippet> snippets = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_g_youtube_live_streams, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        fetchStreams();
    }

    private void fetchStreams() {
        binding.srl.setRefreshing(true);

        AndroidNetworking.get("https://www.googleapis.com/youtube/v3/liveBroadcasts?part=snippet&broadcastStatus=all")
                .addHeaders("Authorization", "Bearer " + myApp.getOAuth())
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("#DEBUG", "   fetchStreams: onResponse: " + response);
                        binding.tvError.setVisibility(View.GONE);
                        binding.srl.setRefreshing(false);
                        snippets.clear();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("items");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                YSnippet snippet = new YSnippet();
                                JSONObject objectMain = jsonArray.getJSONObject(i);
                                JSONObject objectSnippet = objectMain.getJSONObject("snippet");
                                snippet.setTitle(objectSnippet.getString("publishedAt"));
                                snippet.setTitle(objectSnippet.getString("channelId"));
                                snippet.setTitle(objectSnippet.getString("title"));
                                snippet.setTitle(objectSnippet.getString("description"));
                                snippet.setTitle(objectSnippet.getString("scheduledStartTime"));
                                snippet.setTitle(objectSnippet.getString("scheduledEndTime"));
                                snippet.setTitle(objectSnippet.getString("actualStartTime"));
                                snippet.setTitle(objectSnippet.getString("actualEndTime"));

                                snippets.add(snippet);
                            }
                            streamAdapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("#DEBUG", "   fetchStreams: onError: " + anError.getErrorBody());
                        binding.tvError.setVisibility(View.VISIBLE);
                        try {
                            binding.tvError.setText(new JSONObject(anError.getErrorBody()).getJSONObject("error").getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        binding.srl.setRefreshing(false);
                    }
                });
    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {
        binding.srl.setOnRefreshListener(this::fetchStreams);
    }

    @Override
    protected void initViews(View v) {

        streamAdapter = new StreamAdapter();
        binding.rvStreams.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvStreams.setAdapter(streamAdapter);

    }

    private class StreamAdapter extends RecyclerView.Adapter<StreamAdapter.StreamHolder> {

        @NonNull
        @Override
        public StreamHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new StreamHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_stream, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull StreamHolder holder, int position) {
            YSnippet snippet = snippets.get(position);
            if (snippet != null) {
                holder.tvTitle.setText(snippet.getTitle());
                holder.tvDesc.setText(snippet.getDescription());
                holder.tvStart.setText(String.format("Schedule Start Time: %s", snippet.getScheduledStartTime()));
                holder.tvEnd.setText(String.format("Schedule End Time: %s", snippet.getScheduledEndTime()));
            }
        }

        @Override
        public int getItemCount() {
            return snippets.size();
        }

        public class StreamHolder extends RecyclerView.ViewHolder {
            TextView tvTitle, tvDesc, tvStart, tvEnd;

            public StreamHolder(@NonNull View itemView) {
                super(itemView);
                tvTitle = itemView.findViewById(R.id.tvTitle);
                tvDesc = itemView.findViewById(R.id.tvDesc);
                tvStart = itemView.findViewById(R.id.tvStart);
                tvEnd = itemView.findViewById(R.id.tvEnd);
            }
        }
    }
}
