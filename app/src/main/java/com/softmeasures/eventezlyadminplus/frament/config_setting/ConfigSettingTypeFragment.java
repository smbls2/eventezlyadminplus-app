package com.softmeasures.eventezlyadminplus.frament.config_setting;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragmentConfigSettingTypeBinding;
import com.softmeasures.eventezlyadminplus.frament.event.EventPartnerTypesFragment;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class ConfigSettingTypeFragment extends BaseFragment {
    FragmentConfigSettingTypeBinding binding;
    private String menu = "";
    private String configType;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_config_setting_type, container, false);
        if (getArguments() != null) {
            menu = getArguments().getString("menu");
            configType = getArguments().getString("Config_type");
        }
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        setListeners();
        updateViews();
    }

    @Override
    protected void updateViews() {
        if (configType.equals("Call Config"))
            binding.tvTitle.setText("Config Settings");
        else if (configType.equals("Chat Config"))
            binding.tvTitle.setText("Chat Config Settings");
        if (configType.equals("Group Config"))
            binding.tvTitle.setText("Group Settings");
    }

    @Override
    protected void setListeners() {
        binding.llBtnCompanies.setOnClickListener(v -> {
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            EventPartnerTypesFragment frag = new EventPartnerTypesFragment();
            Bundle bundle = new Bundle();
            bundle.putString("Config_type", configType);
            frag.setArguments(bundle);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, frag);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(frag);
            ft.commitAllowingStateLoss();
        });

        binding.llBtnUsers.setOnClickListener(v -> {
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
//            UserInfoFragment frag = new UserInfoFragment();
            Fragment frag = new UserTypesFragment();
            Bundle bundle = new Bundle();
            bundle.putString("Config_type", configType);
            frag.setArguments(bundle);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, frag);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(frag);
            ft.commitAllowingStateLoss();
        });
    }

    @Override
    protected void initViews(View v) {

    }
}