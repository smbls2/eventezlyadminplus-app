package com.softmeasures.eventezlyadminplus.frament.g_classroom;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.api.services.classroom.model.Course;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.TransferActivity;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragGClassroomCoursesBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GClassRoomCoursesFragment extends BaseFragment {

    private FragGClassroomCoursesBinding binding;
    private ArrayList<Course> courses = new ArrayList<>();
    private CourseAdapter courseAdapter;
    public static boolean isNewAdded = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_g_classroom_courses, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        fetchCourses();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isNewAdded) {
            isNewAdded = false;
            fetchCourses();
        }
    }

    private void fetchCourses() {

        binding.srl.setRefreshing(true);
        courses.clear();
        AndroidNetworking.get("https://classroom.googleapis.com/v1/courses")
                .addHeaders("Authorization", "Bearer " + myApp.getOAuth())
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("#DEBUG", "  fetchCourses:  onResponse:  " + response);
                        binding.srl.setRefreshing(false);
                        binding.tvError.setVisibility(View.VISIBLE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has("courses")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("courses");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    Course course = new Course();
                                    JSONObject objectCourse = jsonArray.getJSONObject(i);

                                    if (objectCourse.has("id")) {
                                        course.setId(objectCourse.getString("id"));
                                    }
                                    if (objectCourse.has("name")) {
                                        course.setName(objectCourse.getString("name"));
                                    }
                                    if (objectCourse.has("section")) {
                                        course.setSection(objectCourse.getString("section"));
                                    }
                                    if (objectCourse.has("descriptionHeading")) {
                                        course.setDescriptionHeading(objectCourse.getString("descriptionHeading"));
                                    }
                                    if (objectCourse.has("room")) {
                                        course.setRoom(objectCourse.getString("room"));
                                    }
                                    if (objectCourse.has("ownerId")) {
                                        course.setOwnerId(objectCourse.getString("ownerId"));
                                    }
                                    if (objectCourse.has("creationTime")) {
                                        course.setCreationTime(objectCourse.getString("creationTime"));
                                    }
                                    if (objectCourse.has("updateTime")) {
                                        course.setUpdateTime(objectCourse.getString("updateTime"));
                                    }
                                    if (objectCourse.has("enrollmentCode")) {
                                        course.setEnrollmentCode(objectCourse.getString("enrollmentCode"));
                                    }
                                    if (objectCourse.has("courseState")) {
                                        course.setCourseState(objectCourse.getString("courseState"));
                                    }
                                    if (objectCourse.has("alternateLink")) {
                                        course.setAlternateLink(objectCourse.getString("alternateLink"));
                                    }
                                    if (objectCourse.has("teacherGroupEmail")) {
                                        course.setTeacherGroupEmail(objectCourse.getString("teacherGroupEmail"));
                                    }
                                    if (objectCourse.has("courseGroupEmail")) {
                                        course.setCourseGroupEmail(objectCourse.getString("courseGroupEmail"));
                                    }
                                    if (objectCourse.has("guardiansEnabled")) {
                                        course.setGuardiansEnabled(objectCourse.getBoolean("guardiansEnabled"));
                                    }
                                    if (objectCourse.has("calendarId")) {
                                        course.setCalendarId(objectCourse.getString("calendarId"));
                                    }
                                    courses.add(course);
                                }
                                if (courses.size() == 0) {
                                    binding.tvError.setVisibility(View.VISIBLE);
                                } else {
                                    binding.tvError.setVisibility(View.GONE);
                                    courseAdapter.notifyDataSetChanged();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("#DEBUG", "  fetchCourses:  onError: " + anError.getErrorBody());
                        binding.srl.setRefreshing(false);
                        binding.tvError.setVisibility(View.VISIBLE);
                        if (anError.getErrorCode() == 401) {
                            GClassroomActivity.authExpired = true;
                            Toast.makeText(getActivity(), "Session expired!, You need to login again!", Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();
                        }
                    }
                });

    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {

        binding.tvBtnAddCourse.setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), TransferActivity.class)
                    .putExtra("type", "addCourse"));
        });

        binding.srl.setOnRefreshListener(this::fetchCourses);

    }

    @Override
    protected void initViews(View v) {

        courseAdapter = new CourseAdapter();
        binding.rvCourses.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvCourses.setAdapter(courseAdapter);

    }

    private class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.CourseHolder> {

        @NonNull
        @Override
        public CourseAdapter.CourseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new CourseHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_g_classroom_course, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull CourseAdapter.CourseHolder holder, int position) {
            Course course = courses.get(position);
            if (course != null) {
                holder.tvCourseName.setText(course.getName());
                holder.tvCourseDescription.setText(course.getDescriptionHeading());
            }

        }

        @Override
        public int getItemCount() {
            return courses.size();
        }

        public class CourseHolder extends RecyclerView.ViewHolder {
            private TextView tvCourseName, tvCourseDescription;

            public CourseHolder(@NonNull View itemView) {
                super(itemView);

                tvCourseName = itemView.findViewById(R.id.tvCourseName);
                tvCourseDescription = itemView.findViewById(R.id.tvCourseDescription);

                itemView.setOnClickListener(view -> {
                    startActivity(new Intent(getActivity(), TransferActivity.class)
                            .putExtra("type", "courseDetails")
                            .putExtra("courseId", courses.get(getAdapterPosition()).getId()));
                });
            }
        }
    }
}
