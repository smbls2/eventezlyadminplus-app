package com.softmeasures.eventezlyadminplus.frament.reservation;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.panstreetview;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.frament.f_direction;
import com.softmeasures.eventezlyadminplus.frament.freeparkhere;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.LocationLot;
import com.softmeasures.eventezlyadminplus.models.R_Parking;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class ReservationParkingSpotsDetailsFragment extends BaseFragment {

    private item selectedSpot;
    private TextView txttitle, txttoday, txtmax, txtstreetview, txtdrections, txtaddress,
            txttime, txtmanaged, txt_main_title, txt_park_here, txt_no_parking, txt_parkin_time, txt_custom_notices;
    private String this_day, active = "null", title = "", html, address, pricing = "null",
            pricing_duration = "null", max_hours, time_rules, cuuretaddres, state_cont = "", loginid, duration_unit = "";
    private TextView txt_lost;
    private RelativeLayout rl_main, rl_progressbar;
    private ArrayList<LocationLot> locationLots = new ArrayList<>();
    //    private String latitude = "", longitude = "";
    private ArrayList<item> parking_rules = new ArrayList<>();
    private String adrreslocation;
    private String start_hour, parking_time, no_parking_time, notices;
    String end_hour, location_code, marker, locationnamne, zip_code, city, state, id, township_code;
    private item parkingRule;
    private Double latitude = 0.0, longitude = 0.0;
    private String lat1, lang1;
    private String parkingType = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            parkingType = getArguments().getString("parkingType", "Township");
            Log.e("#DEBUG", "   ReservationParkingSpotsDetailsFragment:  parkingType:  " + parkingType);
            selectedSpot = new Gson().fromJson(getArguments().getString("selectedSpot"), item.class);
            Log.e("#DEBUG", "  ReservationParkingSpotsDetailsFragment: \nselectedSpot:  "
                    + new Gson().toJson(selectedSpot));
        }
        return inflater.inflate(R.layout.frag_mark_reservable_spots_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        currentlocation();
        initViews(view);
        updateViews();
        setListeners();

        showMap();

        new fetchLatLongFromaddress(String.valueOf(latitude), String.valueOf(longitude)).execute();

    }

    public void currentlocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
        } else {
            android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Failed to fetch user location!");
            alertDialog.setMessage("Please enabl e user location for app, location is required for app to work properly");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });

            alertDialog.show();
        }
    }

    public void showMap() {
        lat1 = lat1 != null ? (!lat1.equals("") ? lat1 : "40.7326609") : "40.7326609";
        lang1 = lang1 != null ? (!lang1.equals("") ? lang1 : "-73.6948277") : "-73.6948277";
        try {


            final LatLng position = new LatLng(Double.parseDouble(lat1), Double.parseDouble(lang1));
            MarkerOptions options = new MarkerOptions();
            options.position(position);
            options.title(title);
            options.snippet(address);

            SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            fm.getMapAsync(googleMap1 -> {
                final GoogleMap googleMap = googleMap1;
                if (selectedSpot.getMarker_type().equals("free")) {
                    options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.free_star1));
                } else if (selectedSpot.getMarker_type().equals("Managed Paid") || selectedSpot.getMarker_type().equals("Managed Free")) {
                    options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.rsz_manages_star11));
                } else if (selectedSpot.getMarker_type().equals("Paid")) {
                    options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.paid_star1));
                }
                Marker melbourne = googleMap.addMarker(options);
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 14));
                final long start = SystemClock.uptimeMillis();
                final long duration = 1500L;
//            mHandler.removeCallbacks(mAnimation);
//            mAnimation = new BounceAnimation(start, duration, melbourne, mHandler);
//            mHandler.post(mAnimation);
                googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                    @Override
                    public View getInfoWindow(Marker arg0) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(Marker arg0) {
                        View v = getActivity().getLayoutInflater().inflate(R.layout.otherparkingpin, null);
                        LatLng latLng = arg0.getPosition();
                        TextView txttitle = (TextView) v.findViewById(R.id.txtvalues);
                        TextView txtcontaint = (TextView) v.findViewById(R.id.txttotle);
                        String sa = arg0.getSnippet();
                        txtcontaint.setText(arg0.getSnippet());
                        txttitle.setText(arg0.getTitle());
                        return v;
                    }
                });
            });
        } catch (Exception e) {
        }

    }

    @Override
    protected void updateViews() {
        if (selectedSpot != null) {
            lat1 = selectedSpot.getLat();
            lang1 = selectedSpot.getLng();
            pricing = selectedSpot.getPricing();
            pricing_duration = selectedSpot.getPricing_duration();


            if (!TextUtils.isEmpty(selectedSpot.getTime_rule())) {
                txttime.setText(selectedSpot.getTime_rule());
            }
            if (!TextUtils.isEmpty(selectedSpot.getMax_duration())
                    && !TextUtils.isEmpty(selectedSpot.getDuration_unit())) {
                txtmax.setText(String.format("%s %s", selectedSpot.getMax_duration(),
                        selectedSpot.getDuration_unit()));
                if (!selectedSpot.getMax_duration().equals("1")) {
                    if (selectedSpot.getDuration_unit().equals("Hour")) {
                        txtmax.setText(String.format("%s %s", selectedSpot.getMax_duration(),
                                "Hours"));
                    } else if (selectedSpot.getDuration_unit().equals("Minute")) {
                        txtmax.setText(String.format("%s %s", selectedSpot.getMax_duration(),
                                "Minutes"));
                    } else if (selectedSpot.getDuration_unit().equals("Day")) {
                        txtmax.setText(String.format("%s %s", selectedSpot.getMax_duration(),
                                "Days"));
                    } else if (selectedSpot.getDuration_unit().equals("Week")) {
                        txtmax.setText(String.format("%s %s", selectedSpot.getMax_duration(),
                                "Weeks"));
                    } else if (selectedSpot.getDuration_unit().equals("Month")) {
                        txtmax.setText(String.format("%s %s", selectedSpot.getMax_duration(),
                                "Months"));
                    }
                }

            }
            txttitle.setText(String.format("$%s/%s %s", selectedSpot.getPricing(),
                    selectedSpot.getPricing_duration(), selectedSpot.getDuration_unit()));

            if (!TextUtils.isEmpty(selectedSpot.getCustom_notice())) {
                txt_custom_notices.setText(selectedSpot.getCustom_notice());
            }

            if (selectedSpot.getMarker_type().equalsIgnoreCase("managed paid")
                    || selectedSpot.getMarker_type().equalsIgnoreCase("managed free")) {
                txt_lost.setVisibility(View.VISIBLE);
            } else {
                txt_lost.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(selectedSpot.getTitle())) {
                txt_main_title.setText(selectedSpot.getTitle());
            }
            if (!TextUtils.isEmpty(selectedSpot.getAddress())) {
                txtaddress.setText(selectedSpot.getAddress());
                address = selectedSpot.getAddress();
            }

            if (!TextUtils.isEmpty(selectedSpot.getParking_times())) {
                txt_parkin_time.setText(selectedSpot.getParking_times());
            }

            if (!TextUtils.isEmpty(selectedSpot.getNo_parking_times())) {
                txt_no_parking.setText(selectedSpot.getNo_parking_times());
            }

        }

        if (parkingRule != null) {
            pricing = parkingRule.getPricing();
            pricing_duration = parkingRule.getPricing_duration();


            if (!TextUtils.isEmpty(parkingRule.getTime_rule())) {
                txttime.setText(parkingRule.getTime_rule());
                selectedSpot.setTime_rule(parkingRule.getTime_rule());
            }
            if (!TextUtils.isEmpty(parkingRule.getMax_duration())
                    && !TextUtils.isEmpty(parkingRule.getDuration_unit())) {
                txtmax.setText(String.format("%s %s", parkingRule.getMax_duration(),
                        parkingRule.getDuration_unit()));
                if (!parkingRule.getMax_duration().equals("1")) {
                    if (parkingRule.getDuration_unit().equals("Hour")) {
                        txtmax.setText(String.format("%s %s", parkingRule.getMax_duration(),
                                "Hours"));
                    } else if (parkingRule.getDuration_unit().equals("Minute")) {
                        txtmax.setText(String.format("%s %s", parkingRule.getMax_duration(),
                                "Minutes"));
                    } else if (parkingRule.getDuration_unit().equals("Day")) {
                        txtmax.setText(String.format("%s %s", parkingRule.getMax_duration(),
                                "Days"));
                    } else if (parkingRule.getDuration_unit().equals("Week")) {
                        txtmax.setText(String.format("%s %s", parkingRule.getMax_duration(),
                                "Weeks"));
                    } else if (parkingRule.getDuration_unit().equals("Month")) {
                        txtmax.setText(String.format("%s %s", parkingRule.getMax_duration(),
                                "Months"));
                    }
                }
                selectedSpot.setMax_duration(parkingRule.getMax_duration());
                selectedSpot.setDuration_unit(parkingRule.getDuration_unit());
            }
            txttitle.setText(String.format("$%s/%s %s", parkingRule.getPricing(),
                    parkingRule.getPricing_duration(), parkingRule.getDuration_unit()));
            selectedSpot.setPricing(parkingRule.getPricing());
            selectedSpot.setPricing_duration(parkingRule.getPricing_duration());
            selectedSpot.setDuration_unit(parkingRule.getDuration_unit());

            if (!TextUtils.isEmpty(parkingRule.getCustom_notice())) {
                txt_custom_notices.setText(parkingRule.getCustom_notice());
                selectedSpot.setCustom_notice(parkingRule.getCustom_notice());
            }

//            if (parkingRule.getMarker_type().equalsIgnoreCase("managed paid")
//                    || parkingRule.getMarker_type().equalsIgnoreCase("managed free")) {
//                txt_lost.setVisibility(View.VISIBLE);
//            } else {
//                txt_lost.setVisibility(View.GONE);
//            }
        }
    }

    @Override
    protected void setListeners() {

        txtdrections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!selectedSpot.getLat().equals("0.0") || !selectedSpot.getLng().equals("0.0")) {

                    SharedPreferences sh1 = getActivity().getSharedPreferences("directiontomydirection", Context.MODE_PRIVATE);
                    SharedPreferences.Editor ed1 = sh1.edit();
                    ed1.putString("lat", selectedSpot.getLat());
                    ed1.putString("lang", selectedSpot.getLng());
                    ed1.putString("address", address);
                    ed1.commit();
                    SharedPreferences backtotimer = getActivity().getSharedPreferences("back_parkezly", Context.MODE_PRIVATE);
                    SharedPreferences.Editor eddd = backtotimer.edit();
                    eddd.putString("isback", "yes");
                    eddd.commit();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                   /* ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                    ft.add(R.id.My_Container_1_ID, new f_direction(), "NewFragmentTag");
                    ft.commit();*/
                    f_direction pay = new f_direction();
                    fragmentStack.clear();
                    // parking_first_screen.registerForListener(Vchome.this);
                    ft.add(R.id.My_Container_1_ID, pay, "home");
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                }
            }
        });

        txtstreetview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), panstreetview.class);
                i.putExtra("lat", selectedSpot.getLat());
                i.putExtra("lang", selectedSpot.getLng());
                i.putExtra("title", title);
                i.putExtra("address", address);
                getActivity().startActivity(i);
            }
        });

        txt_park_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {


                    if (txtmanaged.getVisibility() == View.VISIBLE) {
                        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                        loginid = logindeatl.getString("id", "null");
                        if (loginid.equals("null")) {
                            loginid = "0";
                        }
                        //String currentaddress = getlatandlangtoaddress(latitude, longitude);
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Confirm Parking");
                        String message = "You selected to Reserve a Spot at "
                                + " \"" + "<b>" + selectedSpot.getLocation_name() + "</b>" +
                                "\" at \"" + address + "\".\nAt this location, your actual spot# could be different from what you reserve, based on the availability.";
                        alertDialog.setMessage(Html.fromHtml(message));
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                new managedparkhere(selectedSpot.getLocation_code(), address, lat1, lang1).execute();
                            }
                        });
                        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alertDialog.show();

                    } else {
                        //String currentaddress = getlatandlangtoaddress(latitude, longitude);
                        if (!adrreslocation.equals("")) {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("Confirm Parking");
                            String message = "You selected to Reserve a Spot at "
                                    + " \"" + "<b>" + selectedSpot.getLocation_name() + "</b>" +
                                    "\" at \"" + address + "\".\nAt this location, your actual spot# could be different from what you reserve, based on the availability.";
                            alertDialog.setMessage(Html.fromHtml(message));
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                /*SharedPreferences sharedPreferences=getActivity().getSharedPreferences("back_parkezly_parking",Context.MODE_PRIVATE);
                                SharedPreferences.Editor ede=sharedPreferences.edit();
                                ede.putString("back","yes");
                                ede.commit();
*/
                                    if (pricing.equals("null") || pricing.equals("0")) {
                                        SharedPreferences s = getActivity().getSharedPreferences("paidparkhere", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor ed = s.edit();
                                        ed.putString("Time", selectedSpot.getTime_rule());
                                        ed.putString("Max", selectedSpot.getMax_duration());
                                        ed.putString("rate", selectedSpot.getPricing());
                                        ed.putString("rated", selectedSpot.getPricing_duration());
                                        ed.putString("location_code", selectedSpot.getLocation_code());
                                        ed.putString("location_name", selectedSpot.getLocation_name());
                                        ed.putString("zip_code", "");
                                        ed.putString("city", "");
                                        ed.putString("lat", lat1);
                                        ed.putString("lang", lang1);
                                        ed.putString("id", "");
                                        ed.putString("location_id", selectedSpot.getLocation_id());
                                        ed.putString("address", selectedSpot.getAddress());
                                        ed.putString("title", selectedSpot.getTitle());
                                        ed.putString("township_code", selectedSpot.getTownship_code());
                                        ed.putString("duation_unit", selectedSpot.getDuration_unit());
                                        if (txt_lost.getVisibility() == View.VISIBLE) {
                                            ed.putString("managedloick", "yes");
                                            ed.putString("mar", "managed");
                                        } else {
                                            ed.putString("managedloick", "no");
                                            ed.putString("mar", "free");
                                        }
                                        ed.putString("parkhere", "no");
                                        ed.putString("parking_for", "free");
                                        ed.putString("parking_type", selectedSpot.getMarker_type());
                                        ed.commit();
                                   /* final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                    ft.replace(R.id.My_Container_1_ID, new freeparkhere(), "NewFragmentTag");
                                    ft.commit();*/
                                        freeparkhere pay = new freeparkhere();
                                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.add(R.id.My_Container_1_ID, pay);
                                        fragmentStack.lastElement().onPause();
                                        ft.hide(fragmentStack.lastElement());
                                        fragmentStack.push(pay);
                                        ft.commitAllowingStateLoss();
                                        //free park;
                                    } else {
                                        SharedPreferences s = getActivity().getSharedPreferences("paidparkhere", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor ed = s.edit();
                                        Log.e("#DEBUG", "    selectedSpot:  " + new Gson().toJson(selectedSpot));
                                        R_Parking r_parking = new R_Parking();
                                        r_parking.setParkingAvailability(selectedSpot.getTime_rule());
                                        r_parking.setMaxTime(selectedSpot.getMax_duration());
                                        r_parking.setParkingRate(selectedSpot.getPricing());
                                        r_parking.setParkingDuration(selectedSpot.getPricing_duration());
                                        r_parking.setParkingDurationUnit(selectedSpot.getDuration_unit());
                                        r_parking.setMarkerAddress(selectedSpot.getAddress());
                                        r_parking.setMarkerCity(city);
                                        r_parking.setMarkerZip(zip_code);
                                        r_parking.setMarkerLat(lat1);
                                        r_parking.setMarkerLng(lang1);
                                        r_parking.setManagerTypeId(selectedSpot.getManager_type_id());
                                        r_parking.setManagerType(selectedSpot.getManager_type());
                                        r_parking.setTownshipCode(selectedSpot.getTownship_code());
                                        r_parking.setLocationId(selectedSpot.getLocation_id());
                                        r_parking.setLocationCode(selectedSpot.getLocation_code());
                                        r_parking.setLocationName(selectedSpot.getLocation_name());
                                        r_parking.setCompanyId(selectedSpot.getCompany_id());
                                        r_parking.setTwpId(selectedSpot.getTwp_id());
                                        r_parking.setMarkerType(selectedSpot.getMarker_type());
                                        ed.putString("Time", selectedSpot.getTime_rule());
                                        ed.putString("Max", selectedSpot.getMax_duration());
                                        ed.putString("rate", selectedSpot.getPricing());
                                        ed.putString("rated", selectedSpot.getPricing_duration());
                                        ed.putString("location_code", selectedSpot.getLocation_code());
                                        ed.putString("location_name", selectedSpot.getLocation_name());
                                        ed.putString("zip_code", zip_code);
                                        ed.putString("city", city);
                                        ed.putString("lat", lat1);
                                        ed.putString("lang", lat1);
                                        ed.putString("id", "");
                                        ed.putString("township_code", selectedSpot.getTownship_code());
                                        ed.putString("address", selectedSpot.getAddress());
                                        ed.putString("duation_unit", selectedSpot.getDuration_unit());
                                        ed.putString("title", selectedSpot.getTitle());
                                        if (txt_lost.getVisibility() == View.VISIBLE) {
                                            ed.putString("managedloick", "yes");
                                            ed.putString("mar", selectedSpot.getMarker_type());
                                        } else {
                                            ed.putString("managedloick", "no");
                                            ed.putString("mar", "paid");
                                        }
                                        ed.putString("parkhere", "no");
                                        ed.putString("location_id", selectedSpot.getLocation_id());
                                        ed.putString("parking_for", "paid");
                                        ed.putString("parking_type", selectedSpot.getMarker_type());
                                        ed.commit();

                                  /*  final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                    ft.replace(R.id.My_Container_1_ID, new paidparkhere(), "NewFragmentTag");
                                    ft.commit();*/

                                        Fragment fragment = null;
                                        Bundle bundle = new Bundle();
                                        if (getArguments() != null) {
                                            bundle = getArguments();
                                        }
                                        bundle.putString("rParking", new Gson().toJson(r_parking));
                                        fragment = new FinalReservationFragment();

                                        fragment.setArguments(bundle);

//                                        MyApplication.getInstance().paidparkhereFragment = pay;
                                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.add(R.id.My_Container_1_ID, fragment, "rParking");
                                        fragmentStack.lastElement().onPause();
                                        ft.hide(fragmentStack.lastElement());
                                        fragmentStack.push(fragment);
                                        ft.commitAllowingStateLoss();
                                        //padi park;
                                    }
                                }
                            });
                            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            alertDialog.show();
                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("Address not found!");
                            alertDialog.setMessage("Could not find location. Try including the exact location information");
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    new fetchLatLongFromaddress(String.valueOf(latitude), String.valueOf(longitude)).execute();
                                }
                            });
                        }
                    }
                } catch (Exception w) {
                }
            }
        });

        txt_lost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new fetchLocationLotOccupiedData(selectedSpot.getLocation_code()).execute();
            }
        });

    }

    public class managedparkhere extends AsyncTask<String, String, String> {
        String streetviewurl = "null";
        String locationcode4 = "", address;
        String lat, lang;
        JSONArray googleparking1;
        JSONObject json, json1;

        public managedparkhere(String locationcode3, String address, String lat, String lang) {
            this.locationcode4 = locationcode3;
            this.address = address;
            this.lat = lat;
            this.lang = lang;
            streetviewurl = "_table/subscriptions?filter=(location_code%3D'" + locationcode3 + "')%20AND%20(user_id%3D'" + loginid + "')";

        }

        @Override
        protected void onPreExecute() {


            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + streetviewurl;
            Log.e("managed_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    googleparking1 = json.getJSONArray("resource");


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json != null) {
                if (getActivity() != null) {
                    if (googleparking1.isNull(0)) {
                        for (int k = 0; k < parking_rules.size(); k++) {
                            String loccode = parking_rules.get(k).getLoationcode1();
                            if (loccode.equals(locationcode4)) {
                                start_hour = parking_rules.get(k).getStart_hour();
                                end_hour = parking_rules.get(k).getEnd_hour();
                                this_day = parking_rules.get(k).getThis_day();
                                active = parking_rules.get(k).getActive();
                                pricing = parking_rules.get(k).getPricing();
                                pricing_duration = parking_rules.get(k).getPricing_duration();
                                time_rules = parking_rules.get(k).getTime_rule();
                                max_hours = parking_rules.get(k).getMax_hours();
                                locationnamne = parking_rules.get(k).getLoationname();
                                zip_code = parking_rules.get(k).getZip_code();
                                city = parking_rules.get(k).getCity();
                                state = parking_rules.get(k).getState();
                                String finalId = parking_rules.get(k).getId();
                                if (!start_hour.equals("null") && !end_hour.equals("null") && !this_day.equals("null")) {
                                    Calendar c = Calendar.getInstance();
                                    int hour = c.get(Calendar.HOUR);
                                    SimpleDateFormat sdf = new SimpleDateFormat("EEE");
                                    Date d = new Date();
                                    String today = sdf.format(d).toLowerCase();
                                    int start_hours = Integer.parseInt(start_hour);
                                    int end_hours = Integer.parseInt(end_hour);
                                    if (hour >= start_hours && hour <= end_hours && (this_day.equals(today) || this_day.equals("everyday"))) {


                                        SharedPreferences s5 = getActivity().getSharedPreferences("paidparkhere", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor ed = s5.edit();
                                        ed.putString("Time", time_rules);
                                        ed.putString("Max", max_hours);
                                        ed.putString("rate", pricing);
                                        ed.putString("rated", pricing_duration);
                                        ed.putString("location_code", location_code);
                                        ed.putString("location_name", locationnamne);
                                        ed.putString("zip_code", zip_code);
                                        ed.putString("city", city);
                                        ed.putString("lat", lat1);
                                        ed.putString("lang", lang1);
                                        ed.putString("id", "");
                                        ed.putString("address", selectedSpot.getAddress());
                                        ed.putString("title", selectedSpot.getTitle());
                                        if (txtmanaged.getVisibility() == View.VISIBLE) {
                                            ed.putString("managedloick", "yes");
                                            ed.putString("mar", "managed");
                                        } else {
                                            ed.putString("managedloick", "no");
                                            ed.putString("mar", "free");
                                        }
                                        ed.putString("parkhere", "no");
                                        ed.putString("parking_for", "paid");
                                        ed.commit();

                                        R_Parking r_parking = new R_Parking();
                                        r_parking.setMaxTime(max_hours);
                                        r_parking.setParkingAvailability(time_rules);
                                        r_parking.setParkingRate(pricing);
                                        r_parking.setParkingDuration(pricing_duration);
                                        r_parking.setParkingDurationUnit(pricing_duration);
                                        r_parking.setParkingLat(lat);
                                        r_parking.setParkingLng(lang);
                                        r_parking.setLocationCode(location_code);
                                        r_parking.setManagerTypeId(selectedSpot.getManager_type_id());
                                        r_parking.setManagerType(selectedSpot.getManager_type());
                                        r_parking.setTownshipCode(selectedSpot.getTownship_code());
                                        r_parking.setLocationId(selectedSpot.getLocation_id());
                                        r_parking.setLocationName(selectedSpot.getLocation_name());
                                        r_parking.setCompanyId(selectedSpot.getCompany_id());
                                        r_parking.setTwpId(selectedSpot.getTwp_id());

                                        Fragment fragment = null;
                                        Bundle bundle = new Bundle();
                                        if (getArguments() != null) {
                                            bundle = getArguments();
                                        }
                                        bundle.putString("rParking", new Gson().toJson(r_parking));
                                        fragment = new FinalReservationFragment();

                                        fragment.setArguments(bundle);


                                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                        ft.replace(R.id.My_Container_1_ID, fragment, "rParking");
                                        ft.commit();


                                        break;
                                    }
                                }
                            }
                        }

                    } else {

                        for (int k = 0; k < parking_rules.size(); k++) {
                            String loccode = parking_rules.get(k).getLoationcode1();
                            if (loccode.equals(locationcode4)) {
                                start_hour = parking_rules.get(k).getStart_hour();
                                end_hour = parking_rules.get(k).getEnd_hour();
                                this_day = parking_rules.get(k).getThis_day();
                                active = parking_rules.get(k).getActive();
                                pricing = parking_rules.get(k).getPricing();
                                pricing_duration = parking_rules.get(k).getPricing_duration();
                                time_rules = parking_rules.get(k).getTime_rule();
                                max_hours = parking_rules.get(k).getMax_hours();
                                locationnamne = parking_rules.get(k).getLoationname();
                                zip_code = parking_rules.get(k).getZip_code();
                                city = parking_rules.get(k).getCity();
                                state = parking_rules.get(k).getState();
                                String finalId = parking_rules.get(k).getId();
                                if (!start_hour.equals("null") && !end_hour.equals("null") && !this_day.equals("null")) {
                                    Calendar c = Calendar.getInstance();
                                    int hour = c.get(Calendar.HOUR);
                                    SimpleDateFormat sdf = new SimpleDateFormat("EEE");
                                    Date d = new Date();
                                    String today = sdf.format(d).toLowerCase();
                                    int start_hours = Integer.parseInt(start_hour);
                                    int end_hours = Integer.parseInt(end_hour);
                                    if (hour >= start_hours && hour <= end_hours && (this_day.equals(today) || this_day.equals("everyday"))) {
                                        if (max_hours.equals("null") || max_hours.equals("")) {
                                            max_hours = "24";
                                        }

                                        SharedPreferences s5 = getActivity().getSharedPreferences("paidparkhere", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor ed = s5.edit();
                                        ed.putString("Time", time_rules);
                                        ed.putString("Max", max_hours);
                                        ed.putString("rate", pricing);
                                        ed.putString("rated", pricing_duration);
                                        ed.putString("location_code", location_code);
                                        ed.putString("location_name", locationnamne);
                                        ed.putString("zip_code", zip_code);
                                        ed.putString("city", city);
                                        ed.putString("lat", lat1);
                                        ed.putString("lang", lang1);
                                        ed.putString("id", "");
                                        ed.putString("address", address);
                                        ed.putString("title", title);
                                        if (txtmanaged.getVisibility() == View.VISIBLE) {
                                            ed.putString("managedloick", "yes");
                                            ed.putString("mar", "managed");
                                        } else {
                                            ed.putString("managedloick", "no");
                                            ed.putString("mar", "free");
                                        }
                                        ed.putString("parkhere", "no");
                                        ed.putString("parking_for", "free");
                                        ed.commit();
                                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                        ft.replace(R.id.My_Container_1_ID, new freeparkhere(), "NewFragmentTag");
                                        ft.commit();

                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class fetchLocationLotOccupiedData extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String lat, lang, location_code1, address, title, html, mark, parking_time, no_parking_time, notice;

        String managedlosturl = "_proc/managed_lot_status?&order=(lot_row%20ASC%2C%20lot_number%20ASC)";

        ArrayList<item> data = new ArrayList<>();

        public fetchLocationLotOccupiedData(String loc) {
            this.location_code1 = loc;
        }

        @Override
        protected void onPreExecute() {
            if (parkingType.equals("Township")) {
                managedlosturl = "_proc/managed_lot_status?&order=(lot_row%20ASC%2C%20lot_number%20ASC)";
            } else if (parkingType.equals("Commercial")) {
                managedlosturl = "_proc/managed_lot_status?&order=(lot_row%20ASC%2C%20lot_number%20ASC)";
            } else if (parkingType.equals("Other")) {
                managedlosturl = "_proc/managed_lot_status?&order=(lot_row%20ASC%2C%20lot_number%20ASC)";
            }
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            locationLots.clear();
            JSONObject managed = new JSONObject();


            /*  if(latitude==0.0) {*/
            try {
                managed.put("name", "in_location_code");
                managed.put("value", location_code1);

                //  student1.put("value", latitude);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            JSONArray jsonArray = new JSONArray();
            jsonArray.put(managed);

            JSONObject manageda = new JSONObject();
            try {
                manageda.put("params", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("managed_lost_url", url);
            Log.e("managed_lodt_param", String.valueOf(manageda));

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(manageda.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);

            HttpResponse response = null;

            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    TimeZone zone = TimeZone.getTimeZone("UTC");
                    sdf.setTimeZone(zone);
                    String currentdate = sdf.format(new Date());
                    Date current = null;
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        current = format.parse(currentdate);
                        System.out.println(current);
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   fetchLocationLotOccupiedData: Response: " + responseStr);

                    json1 = new JSONArray(responseStr);

                    for (int i = 0; i < json1.length(); i++) {
                        LocationLot item = new LocationLot();
                        JSONObject jsonObject = json1.getJSONObject(i);
                        if (jsonObject.has("id")
                                && !TextUtils.isEmpty(jsonObject.getString("id"))
                                && !jsonObject.getString("id").equals("null")) {
                            item.setId(jsonObject.getInt("id"));
                        }
                        if (jsonObject.has("date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("date_time"))
                                && !jsonObject.getString("date_time").equals("null")) {
                            item.setDate_time(jsonObject.getString("date_time"));
                        }

                        if (jsonObject.has("township_code")
                                && !TextUtils.isEmpty(jsonObject.getString("township_code"))
                                && !jsonObject.getString("township_code").equals("null")) {
                            item.setTownship_code(jsonObject.getString("township_code"));
                        }

                        if (jsonObject.has("location_code")
                                && !TextUtils.isEmpty(jsonObject.getString("location_code"))
                                && !jsonObject.getString("location_code").equals("null")) {
                            item.setLocation_code(jsonObject.getString("location_code"));
                        }

                        if (jsonObject.has("location_name")
                                && !TextUtils.isEmpty(jsonObject.getString("location_name"))
                                && !jsonObject.getString("location_name").equals("null")) {
                            item.setLocation_name(jsonObject.getString("location_name"));
                        }

                        if (jsonObject.has("lot_row")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_row"))
                                && !jsonObject.getString("lot_row").equals("null")) {
                            item.setLot_row(jsonObject.getString("lot_row"));
                        }

                        if (jsonObject.has("lot_number")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_number"))
                                && !jsonObject.getString("lot_number").equals("null")) {
                            item.setLot_number(jsonObject.getString("lot_number"));
                        }
                        if (jsonObject.has("lot_id")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_id"))
                                && !jsonObject.getString("lot_id").equals("null")) {
                            item.setLot_id(jsonObject.getString("lot_id"));
                        }

                        if (jsonObject.has("occupied")
                                && !TextUtils.isEmpty(jsonObject.getString("occupied"))
                                && !jsonObject.getString("occupied").equals("null")) {
                            item.setOccupied(jsonObject.getString("occupied").toLowerCase());
                        }

                        if (jsonObject.has("plate_no")
                                && !TextUtils.isEmpty(jsonObject.getString("plate_no"))
                                && !jsonObject.getString("plate_no").equals("null")) {
                            item.setPlate_no(jsonObject.getString("plate_no"));
                        }
                        if (jsonObject.has("plate_state")
                                && !TextUtils.isEmpty(jsonObject.getString("plate_state"))
                                && !jsonObject.getString("plate_state").equals("null")) {
                            item.setPlate_state(jsonObject.getString("plate_state"));
                        }

                        if (jsonObject.has("entry_date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("entry_date_time"))
                                && !jsonObject.getString("entry_date_time").equals("null")) {
                            item.setEntry_date_time(jsonObject.getString("entry_date_time"));
                        }

                        if (jsonObject.has("exit_date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("exit_date_time"))
                                && !jsonObject.getString("exit_date_time").equals("null")) {
                            item.setExit_date_time(jsonObject.getString("exit_date_time"));
                        }

                        if (jsonObject.has("expiry_time")
                                && !TextUtils.isEmpty(jsonObject.getString("expiry_time"))
                                && !jsonObject.getString("expiry_time").equals("null")) {
                            item.setExpiry_time(jsonObject.getString("expiry_time"));
                        }

                        if (jsonObject.has("parking_type")
                                && !TextUtils.isEmpty(jsonObject.getString("parking_type"))
                                && !jsonObject.getString("parking_type").equals("null")) {
                            item.setParking_type(jsonObject.getString("parking_type"));
                        }

                        if (jsonObject.has("parking_status")
                                && !TextUtils.isEmpty(jsonObject.getString("parking_status"))
                                && !jsonObject.getString("parking_status").equals("null")) {
                            item.setParking_status(jsonObject.getString("parking_status").toLowerCase());
                        }

                        if (jsonObject.has("is_reserved")
                                && !TextUtils.isEmpty(jsonObject.getString("is_reserved"))
                                && !jsonObject.getString("is_reserved").equals("null")) {
                            if (jsonObject.getString("is_reserved").equals("1")) {
                                item.setIs_reserved(true);
                            } else {
                                item.setIs_reserved(false);
                            }
                        }

                        if (jsonObject.has("lot_reservable")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_reservable"))
                                && !jsonObject.getString("lot_reservable").equals("null")) {
                            if (jsonObject.getString("lot_reservable").equals("1")) {
                                item.setLot_reservable(true);
                            } else if (jsonObject.getString("lot_reservable").equals("0")) {
                                item.setLot_reservable(false);
                            } else {
                                item.setLot_reservable(jsonObject.getBoolean("lot_reservable"));
                            }
                        }
                        if (jsonObject.has("lot_reservable_only")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_reservable_only"))
                                && !jsonObject.getString("lot_reservable_only").equals("null")) {
                            if (jsonObject.getString("lot_reservable_only").equals("1")) {
                                item.setLot_reservable_only(true);
                            } else if (jsonObject.getString("lot_reservable_only").equals("0")) {
                                item.setLot_reservable_only(false);
                            } else {
                                item.setLot_reservable_only(jsonObject.getBoolean("lot_reservable_only"));
                            }
                        }
                        if (jsonObject.has("premium_lot")
                                && !TextUtils.isEmpty(jsonObject.getString("premium_lot"))
                                && !jsonObject.getString("premium_lot").equals("null")) {
                            if (jsonObject.getString("premium_lot").equals("1")) {
                                item.setPremium_lot(true);
                            } else if (jsonObject.getString("premium_lot").equals("0")) {
                                item.setPremium_lot(false);
                            } else {
                                item.setPremium_lot(jsonObject.getBoolean("premium_lot"));
                            }
                        }
                        if (jsonObject.has("special_need_handi_lot")
                                && !TextUtils.isEmpty(jsonObject.getString("special_need_handi_lot"))
                                && !jsonObject.getString("special_need_handi_lot").equals("null")) {
                            if (jsonObject.getString("special_need_handi_lot").equals("1")) {
                                item.setSpecial_need_handi_lot(true);
                            } else if (jsonObject.getString("special_need_handi_lot").equals("0")) {
                                item.setSpecial_need_handi_lot(false);
                            } else {
                                item.setSpecial_need_handi_lot(jsonObject.getBoolean("premium_lot"));
                            }
                        }
                        if (jsonObject.has("location_id")
                                && !TextUtils.isEmpty(jsonObject.getString("location_id"))
                                && !jsonObject.getString("location_id").equals("null")) {
                            item.setLocation_id(Integer.parseInt(jsonObject.getString("location_id")));
                        }

                        if (!TextUtils.isEmpty(item.getParking_status())
                                && !TextUtils.isEmpty(item.getOccupied())
                                && item.getParking_status().equals("ENTRY")
                                && item.getOccupied().equals("yes")) {

                            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date exitre = null;
                            try {
                                exitre = format1.parse(item.getExpiry_time());

                            } catch (android.net.ParseException e) {
                                // TODO Auto-generated catch block
                                Log.e("#DEBUG", "  Error:  " + e.getMessage());
                                e.printStackTrace();
                            } catch (java.text.ParseException e) {
                                Log.e("#DEBUG", "  Error:  " + e.getMessage());
                                e.printStackTrace();
                            }

                            if (current.after(exitre)) {
                                item.setExpired(true);
                            } else {
                                item.setExpired(false);
                            }
                        }
                        if (!locationLots.contains(item) && item.isLot_reservable()) {
                            locationLots.add(item);
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json1 != null) {

//                for (int i = 0; i < managedlostpoparray.size(); i++) {
//                    String row_no = managedlostpoparray.get(i).getRow_no();
//                    if (!row_no.equals("")) {
//                        if (isInteger(row_no)) {
//                            int jj = Integer.parseInt(row_no);
//                            String row_name = managedlostpoparray.get(i).getRoe_name();
//                            for (int j = 0; j < data.size(); j++) {
//                                String row_no11 = data.get(j).getRow_no();
//                                if (!row_no11.equals("")) {
//                                    int datarow = Integer.parseInt(row_no11);
//                                    String row_name2 = data.get(j).getRoe_name();
//                                    if (jj == datarow) {
//                                        if (row_name.equals(row_name2)) {
//                                            data.get(j).setMarker(managedlostpoparray.get(i).getMarker());
//                                            data.get(j).setIsparked("t");
//                                        }
//                                    }
//                                }
//                            }
//                        } else {
//                            String row_name = managedlostpoparray.get(i).getRoe_name();
//                            for (int j = 0; j < data.size(); j++) {
//                                String row_no11 = data.get(j).getRow_no();
//                                if (!row_no11.equals("")) {
//                                    String row_name2 = data.get(j).getRoe_name();
//                                    if (row_no.equals(row_no11)) {
//                                        if (row_name.equals(row_name2)) {
//                                            data.get(j).setMarker(managedlostpoparray.get(i).getMarker());
//                                            data.get(j).setIsparked("t");
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//
//                }
            }
            if (getActivity() != null)
                showDialogLocationLots();
            super.onPostExecute(s);
        }
    }

    private void showDialogLocationLots() {
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        View dialogView = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_locatoin_lot, null);
        builder.setView(dialogView);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout rlClose = dialogView.findViewById(R.id.rlClose);
        RecyclerView rvLocationLot = dialogView.findViewById(R.id.rvLocationLot);
        LocationLotAdapter locationLotAdapter = new LocationLotAdapter();
        rvLocationLot.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        rvLocationLot.setAdapter(locationLotAdapter);
        rlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    @Override
    protected void initViews(View v) {
        txttitle = v.findViewById(R.id.txtlable1);
        txtaddress = v.findViewById(R.id.txt_address);
        txttoday = v.findViewById(R.id.txtpouptoday);
        txtmax = v.findViewById(R.id.txtpopupax);
        txtstreetview = v.findViewById(R.id.txt_street_view);
        txtdrections = v.findViewById(R.id.txt_direction);
        txttime = v.findViewById(R.id.txtpopuptime1);
        txtmanaged = v.findViewById(R.id.txtpopupmanaged);
        txt_main_title = v.findViewById(R.id.txt_main_title);
        txt_park_here = v.findViewById(R.id.txt_park_here);
        txt_no_parking = v.findViewById(R.id.txt_private_no_parking_time);
        txt_parkin_time = v.findViewById(R.id.txt_private_parking_time);
        txt_custom_notices = v.findViewById(R.id.txt_custom_notices);
        rl_progressbar = v.findViewById(R.id.rl_progressbar);
        txt_lost = v.findViewById(R.id.txt_lost1);
        rl_main = v.findViewById(R.id.rl_main);
    }

    private class LocationLotAdapter extends RecyclerView.Adapter<LocationLotAdapter.LocationLotHolder> {
        private Drawable drawableRed, drawableBlue, drawableGreen, drawableGrey;

        @NonNull
        @Override
        public LocationLotAdapter.LocationLotHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            drawableRed = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_red);
            drawableBlue = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_blue);
            drawableGreen = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_green);
            drawableGrey = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_gree_car);
            return new LocationLotHolder(LayoutInflater.from(getActivity())
                    .inflate(R.layout.item_location_lot, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull LocationLotAdapter.LocationLotHolder locationLotHolder, int i) {
            LocationLot locationLot = locationLots.get(i);
            locationLotHolder.tvReserved.setVisibility(View.GONE);
            if (locationLot != null) {
                if (!TextUtils.isEmpty(locationLot.getLot_id())) {
                    locationLotHolder.tvRow.setText(locationLot.getLot_id());
                }

                if (!TextUtils.isEmpty(locationLot.getOccupied())) {
                    if (locationLot.getOccupied().equalsIgnoreCase("yes")) {
                        //Car already parked at this location
                        if (locationLot.isExpired()) {
                            //RED, parking expired
                            locationLotHolder.ivCar.setImageDrawable(drawableRed);
                        } else {
                            //GREEN, parking valid
                            locationLotHolder.ivCar.setImageDrawable(drawableGreen);
                        }
                        if (locationLot.isLot_reservable()) {
//                            locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                        }
                    } else if (locationLot.getOccupied().equalsIgnoreCase("no")) {
                        if (locationLot.isLot_reservable()) {
                            //GREY with R, Available for reservation
                            locationLotHolder.ivCar.setImageDrawable(drawableGrey);
//                            locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                        } else {
                            //GREY, Available for parking
                            locationLotHolder.ivCar.setImageDrawable(drawableGrey);
                        }

                    } else if (locationLot.getOccupied().equalsIgnoreCase("reserved")) {
                        //BLUE with R, Reserved for parking
                        locationLotHolder.ivCar.setImageDrawable(drawableBlue);
//                        locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (locationLot.isLot_reservable()) {
                        //GREY with R, Available for reservation
                        locationLotHolder.ivCar.setImageDrawable(drawableGrey);
//                        locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                    } else {
                        //GREY, Available for parking
                        locationLotHolder.ivCar.setImageDrawable(drawableGrey);
                    }
                }

                if (locationLot.isIs_reserved()) {
                    locationLotHolder.viewIsReserved.setVisibility(View.VISIBLE);
                } else locationLotHolder.viewIsReserved.setVisibility(View.GONE);

                if (locationLot.isLot_reservable()) {
                    locationLotHolder.ivReserved.setVisibility(View.VISIBLE);
                } else {
                    locationLotHolder.ivReserved.setVisibility(View.GONE);
                }

                if (locationLot.isPremium_lot())
                    locationLotHolder.ivPremium.setVisibility(View.VISIBLE);
                else locationLotHolder.ivPremium.setVisibility(View.GONE);

                if (locationLot.isSpecial_need_handi_lot())
                    locationLotHolder.ivHandicapped.setVisibility(View.VISIBLE);
                else locationLotHolder.ivHandicapped.setVisibility(View.GONE);

                if (locationLot.isLot_reservable_only()) {
                    locationLotHolder.ivReservedOnly.setVisibility(View.VISIBLE);
                } else locationLotHolder.ivReservedOnly.setVisibility(View.GONE);

//                if (locationLot.getIsparked().equals("t")) {
//                    if (locationLot.getMarker().equals("red")) {
//                        locationLotHolder.ivCar.setBackgroundResource(R.mipmap.popup_car_red);
//                    } else if (locationLot.getMarker().equals("green")) {
//                        locationLotHolder.ivCar.setBackgroundResource(R.mipmap.popup_car_green);
//                    }
//                } else {
//                    locationLotHolder.ivCar.setBackgroundResource(R.mipmap.popup_gree_car);
//                }
            }

        }

        @Override
        public int getItemCount() {
            return locationLots.size();
        }

        class LocationLotHolder extends RecyclerView.ViewHolder {
            ImageView ivCar, ivReserved, ivPremium, ivHandicapped, ivReservedOnly;
            TextView tvRow, tvReserved;
            View viewIsReserved;

            LocationLotHolder(@NonNull View itemView) {
                super(itemView);

                ivCar = itemView.findViewById(R.id.ivCar);
                tvRow = itemView.findViewById(R.id.tvRow);
                tvReserved = itemView.findViewById(R.id.tvReserved);
                viewIsReserved = itemView.findViewById(R.id.viewIsReserved);
                ivReserved = itemView.findViewById(R.id.ivReserved);
                ivPremium = itemView.findViewById(R.id.ivPremium);
                ivHandicapped = itemView.findViewById(R.id.ivHandicapped);
                ivReservedOnly = itemView.findViewById(R.id.ivReservedOnly);

//                itemView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        final int position = getAdapterPosition();
//                        if (position != RecyclerView.NO_POSITION) {
////                            selectedLocationLot = locationLots.get(position);
//                            if (!TextUtils.isEmpty(locationLots.get(position).getOccupied())) {
//                                if (locationLots.get(position).getOccupied().equalsIgnoreCase("no")) {
//                                    locationLots.get(position).setOccupied("yes");
//                                } else {
//                                    locationLots.get(position).setOccupied("no");
//                                }
//                            } else {
//                                locationLots.get(position).setOccupied("yes");
//                            }
//
//                            notifyItemChanged(position);
//                        }
//                    }
//                });
            }
        }
    }

    public class getparkingrules extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String parkingurl = "_table/township_parking_rules";
        ArrayList<item> temp_array = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            if (parkingType.equals("Township")) {
                parkingurl = "_table/township_parking_rules";
            } else if (parkingType.equals("Commercial")) {
                parkingurl = "_table/google_parking_rules";
            } else if (parkingType.equals("Other")) {
                parkingurl = "_table/other_parking_rules";
            }
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            temp_array.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setId(c.getString("id"));
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setTime_rule(c.getString("time_rule"));
                        item1.setDay_rule(c.getString("day_type"));
                        item1.setMax_time(c.getString("max_duration"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setActive(c.getString("active"));
                        item1.setPricing(c.getString("pricing"));
                        item1.setPricing_duration(c.getString("pricing_duration"));
                        item1.setStart_time(c.getString("start_time"));
                        item1.setEnd_time(c.getString("end_time"));
                        item1.setMax_hours(c.getString("duration_unit"));
                        item1.setStart_hour(c.getString("start_hour"));
                        item1.setEnd_hour(c.getString("end_hour"));
                        item1.setLoationcode1(c.getString("location_code"));
                        item1.setMarker_type(c.getString("marker_type"));
                        item1.setDuration_unit(c.getString("duration_unit"));
                        item1.setMax_duration(c.getString("max_duration"));

                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item1.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }

                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item1.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }

                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item1.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }

                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item1.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }

                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item1.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }

                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item1.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item1.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }

                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item1.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }

                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item1.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }

                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item1.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }

                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item1.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }

                        if (c.has("Reservation_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_Fee"))
                                && !c.getString("Reservation_PostPayment_Fee").equals("null")) {
                            item1.setReservation_PostPayment_Fee(c.getString("Reservation_PostPayment_Fee"));
                        }

                        if (c.has("ParkNow_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_Fee"))
                                && !c.getString("ParkNow_PostPayment_Fee").equals("null")) {
                            item1.setParkNow_PostPayment_Fee(c.getString("ParkNow_PostPayment_Fee"));
                        }

                        if (c.has("number_of_reservable_spots")
                                && !TextUtils.isEmpty(c.getString("number_of_reservable_spots"))
                                && !c.getString("number_of_reservable_spots").equals("null")) {
                            item1.setNumber_of_reservable_spots(c.getString("number_of_reservable_spots"));
                        }
                        if (selectedSpot.getLocation_code().equals(item1.getLocation_code())) {
                            temp_array.add(item1);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs;
            if (getActivity() != null) {
                rl_progressbar.setVisibility(View.GONE);
                if (json != null) {
                    if (temp_array.size() > 0) {
                        parking_rules.clear();
                        parking_rules.addAll(temp_array);
                        parkingRule = parking_rules.get(0);
//                    Log.e("#DEBUG", "  getparkingrules:  onPost:  parking_rules: " + new Gson().toJson(parking_rules));
                        Log.e("#DEBUG", "   parking_rules: Size:  " + parking_rules.size());
                        temp_array.clear();
                        updateViews();
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class fetchLatLongFromaddress extends AsyncTask<Void, Void, StringBuilder> {

        String lat1, lang1;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        public fetchLatLongFromaddress(String lat, String lang) {
            super();
            this.lat1 = lat;
            this.lang1 = lang;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {

                //  new getsearchotherlocation().execute();
                //
                // ();

                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat1 + "," + lang1 + "&key=AIzaSyBRdCWdlUMy3X5dc-9jMe0jRXJENibjdN8";

                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
                rl_progressbar.setVisibility(View.GONE);

                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");

                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray
                            .getJSONObject(0);
                    adrreslocation = before_geometry_jsonObj.getString("formatted_address");
                    JSONObject geometry_jsonObj = before_geometry_jsonObj
                            .getJSONObject("geometry");

                    JSONObject location_jsonObj = geometry_jsonObj
                            .getJSONObject("location");

                   /* lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);

                    lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);*/


                } else {
                    android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
                new getparkingrules().execute();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
