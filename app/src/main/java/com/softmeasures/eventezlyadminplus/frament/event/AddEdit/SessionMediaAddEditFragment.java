package com.softmeasures.eventezlyadminplus.frament.event.AddEdit;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubeIntents;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softmeasures.eventezlyadminplus.MyApplication;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.YoutubeSearchVideoActivity;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.DialogAddEditMediaLinkBinding;
import com.softmeasures.eventezlyadminplus.databinding.FragSessionMediaAddEditBinding;
import com.softmeasures.eventezlyadminplus.frament.event.PDFViewFragment;
import com.softmeasures.eventezlyadminplus.frament.event.session.SessionsFragment;
import com.softmeasures.eventezlyadminplus.models.EventSession;
import com.softmeasures.eventezlyadminplus.models.MediaDefinition;
import com.softmeasures.eventezlyadminplus.services.Constants;
import com.softmeasures.eventezlyadminplus.utils.AESEncyption;
import com.softmeasures.eventezlyadminplus.utils.FilePath;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.utils.AESEncyption.encrypt;

public class SessionMediaAddEditFragment extends BaseFragment {

//    public FragSessionMediaAddEditBinding binding;
//    private boolean isEdit = false, isRepeat = false;
//    private EventSession eventSession;
//
//    private ArrayList<MediaDefinition> mediaDefinitions = new ArrayList<>();
//    private ArrayList<MediaDefinition> mediaDefinitionsMain = new ArrayList<>();
//    private MediaAdapter mediaAdapter;
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        if (getArguments() != null) {
//            isEdit = getArguments().getBoolean("isEdit", false);
//            isRepeat = getArguments().getBoolean("isRepeat", false);
//            eventSession = new Gson().fromJson(getArguments().getString("eventSession"), EventSession.class);
//            if (isEdit) {
//                mediaDefinitions.clear();
//                if (!TextUtils.isEmpty(eventSession.getSession_link_array())) {
//                    mediaDefinitions.addAll(new Gson().fromJson(eventSession.getSession_link_array(),
//                            new TypeToken<ArrayList<MediaDefinition>>() {
//                            }.getType()));
//                }
//            }
//        }
//        binding = DataBindingUtil.inflate(inflater, R.layout.frag_session_media_add_edit, container, false);
//        return binding.getRoot();
//    }
//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        initViews(view);
//        updateViews();
//        setListeners();
//
//        new fetchMediaDefinitions().execute();
//    }
//
//    private class fetchMediaDefinitions extends AsyncTask<String, String, String> {
//        JSONObject json;
//        JSONArray json1;
//        String eventDefUrl = "_table/event_media_definitions";
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            mediaDefinitionsMain.clear();
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            String url = getString(R.string.api) + getString(R.string.povlive) + eventDefUrl;
//            Log.e("#DEBUG", "      fetchEventType:  " + url);
//            DefaultHttpClient client = new DefaultHttpClient();
//            HttpGet post = new HttpGet(url);
//            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
//            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
//            HttpResponse response = null;
//            try {
//                response = client.execute(post);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            if (response != null) {
//                HttpEntity resEntity = response.getEntity();
//                String responseStr = null;
//                try {
//                    responseStr = EntityUtils.toString(resEntity).trim();
//                    json = new JSONObject(responseStr);
//                    JSONArray jsonArray = json.getJSONArray("resource");
//
//                    for (int j = 0; j < jsonArray.length(); j++) {
//                        MediaDefinition media = new MediaDefinition();
//                        JSONObject object = jsonArray.getJSONObject(j);
//                        if (object.has("id")
//                                && !TextUtils.isEmpty(object.getString("id"))
//                                && !object.getString("id").equals("null")) {
//                            media.setId(object.getInt("id"));
//                        }
//
//                        if (object.has("media_platform")
//                                && !TextUtils.isEmpty(object.getString("media_platform"))
//                                && !object.getString("media_platform").equals("null")) {
//                            media.setMedia_platform(object.getString("media_platform"));
//                        }
//                        if (object.has("media_domain_url")
//                                && !TextUtils.isEmpty(object.getString("media_domain_url"))
//                                && !object.getString("media_domain_url").equals("null")) {
//                            media.setMedia_domain_url(object.getString("media_domain_url"));
//                        }
//                        if (object.has("media_app_name")
//                                && !TextUtils.isEmpty(object.getString("media_app_name"))
//                                && !object.getString("media_app_name").equals("null")) {
//                            media.setMedia_app_name(object.getString("media_app_name"));
//                        }
//
//                        mediaDefinitionsMain.add(media);
//
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            if (getActivity() != null) {
////                if (mediaDefinitionsMain.size() != 0) {
////                    if (mediaAdapter != null) {
////                        mediaAdapter.notifyDataSetChanged();
////                    }
////                } else {
////
////                }
//            }
//        }
//    }
//
//    @Override
//    protected void updateViews() {
//
//        if (isEdit) {
//
//            if (eventSession != null) {
//                if (eventSession.getEvent_session_link_ytube() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_ytube())) {
//                    binding.etEventLinkYoutube.setText(eventSession.getEvent_session_link_ytube());
//                }
//
//                if (eventSession.getEvent_session_link_zoom() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_zoom())) {
//                    binding.etEventLinkZoom.setText(eventSession.getEvent_session_link_zoom());
//                }
//
//                if (eventSession.getEvent_session_link_googlemeet() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_googlemeet())) {
//                    binding.etEventLinkGoogleMeet.setText(eventSession.getEvent_session_link_googlemeet());
//                }
//
//                if (eventSession.getEvent_session_link_googleclassroom() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_googleclassroom())) {
//                    binding.etEventLinkGoogleClassroom.setText(eventSession.getEvent_session_link_googleclassroom());
//                }
//
//                if (eventSession.getEvent_session_link_facebook() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_facebook())) {
//                    binding.etEventLinkFacebook.setText(eventSession.getEvent_session_link_facebook());
//                }
//
//                if (eventSession.getEvent_session_link_on_web() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_on_web())) {
//                    binding.etEventLinkWeb.setText(eventSession.getEvent_session_link_on_web());
//                }
//
//                if (eventSession.getEvent_session_link_whatsapp() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_whatsapp())) {
//                    binding.etEventLinkWhatsapp.setText(eventSession.getEvent_session_link_whatsapp());
//                }
//
//                if (eventSession.getEvent_session_link_twitter() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_twitter())) {
//                    binding.etEventLinkTwitter.setText(eventSession.getEvent_session_link_twitter());
//                }
//
//                if (eventSession.getEvent_session_link_other_media() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_other_media())) {
//                    binding.etEventLinkOther.setText(eventSession.getEvent_session_link_other_media());
//                }
//
//            }
//
//        }
//
//    }
//
//    @Override
//    protected void setListeners() {
//        binding.tvBtnNext.setOnClickListener(v -> {
//            if (validate()) {
//                Fragment fragment = new SessionLocationAddEditFragment();
//                final FragmentTransaction ft = getFragmentManager().beginTransaction();
//                Bundle bundle = new Bundle();
//                bundle.putBoolean("isEdit", isEdit);
//                bundle.putBoolean("isRepeat", isRepeat);
//                bundle.putString("eventSession", new Gson().toJson(eventSession));
//                fragment.setArguments(bundle);
//                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
//                ft.add(R.id.My_Container_1_ID, fragment);
//                fragmentStack.lastElement().onPause();
//                ft.hide(fragmentStack.lastElement());
//                fragmentStack.push(fragment);
//                ft.commitAllowingStateLoss();
//            }
//
//        });
//
//        binding.ivBtnAddMedia.setOnClickListener(v -> showDialogAddMediaLink(false, -1));
//    }
//
//    private boolean validate() {
//        if (!TextUtils.isEmpty(binding.etEventLinkYoutube.getText())) {
//            eventSession.setEvent_session_link_ytube(binding.etEventLinkYoutube.getText().toString());
//        }
//
//        if (!TextUtils.isEmpty(binding.etEventLinkZoom.getText())) {
//            eventSession.setEvent_session_link_zoom(binding.etEventLinkZoom.getText().toString());
//        }
//
//        if (!TextUtils.isEmpty(binding.etEventLinkGoogleMeet.getText())) {
//            eventSession.setEvent_session_link_googlemeet(binding.etEventLinkGoogleMeet.getText().toString());
//        }
//
//        if (!TextUtils.isEmpty(binding.etEventLinkGoogleClassroom.getText())) {
//            eventSession.setEvent_session_link_googleclassroom(binding.etEventLinkGoogleClassroom.getText().toString());
//        }
//
//        if (!TextUtils.isEmpty(binding.etEventLinkFacebook.getText())) {
//            eventSession.setEvent_session_link_facebook(binding.etEventLinkFacebook.getText().toString());
//        }
//
//        if (!TextUtils.isEmpty(binding.etEventLinkWeb.getText())) {
//            eventSession.setEvent_session_link_on_web(binding.etEventLinkWeb.getText().toString());
//        }
//
//        if (!TextUtils.isEmpty(binding.etEventLinkTwitter.getText())) {
//            eventSession.setEvent_session_link_twitter(binding.etEventLinkTwitter.getText().toString());
//        }
//
//        if (!TextUtils.isEmpty(binding.etEventLinkWhatsapp.getText())) {
//            eventSession.setEvent_session_link_whatsapp(binding.etEventLinkWhatsapp.getText().toString());
//        }
//
//        if (!TextUtils.isEmpty(binding.etEventLinkOther.getText())) {
//            eventSession.setEvent_session_link_other_media(binding.etEventLinkOther.getText().toString());
//        }
//
//        if (mediaDefinitions.size() > 0) {
//            eventSession.setSession_link_array(new Gson().toJson(mediaDefinitions));
//        }
//
//        return true;
//    }
//
//    @Override
//    protected void initViews(View v) {
//
//        mediaAdapter = new MediaAdapter();
//        binding.rvMediaLinks.setLayoutManager(new LinearLayoutManager(getActivity()));
//        binding.rvMediaLinks.setAdapter(mediaAdapter);
//
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        if (SessionsFragment.isSessionUpdated && getActivity() != null) {
//            getActivity().onBackPressed();
//        }
//    }
//
//    private void showDialogAddMediaLink(boolean isEdit, int pos) {
//        MediaDefinition mediaDefinition;
//        if (pos == -1) {
//            mediaDefinition = new MediaDefinition();
//        } else {
//            mediaDefinition = mediaDefinitions.get(pos);
//        }
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        DialogAddEditMediaLinkBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
//                R.layout.dialog_add_edit_media_link, null, false);
//        builder.setView(binding.getRoot());
//
//
//        ArrayAdapter<MediaDefinition> categoryAdapter = new ArrayAdapter<>(getActivity(),
//                android.R.layout.simple_spinner_dropdown_item, mediaDefinitionsMain);
//        binding.spinnerPlatfrom.setAdapter(categoryAdapter);
//
//        if (isEdit) {
//            binding.btnAdd.setText("UPDATE");
//            binding.tvTitle.setText("Edit Event Media Link");
//            binding.btnDelete.setVisibility(View.VISIBLE);
//            binding.spinnerPlatfrom.setSelection(mediaDefinitions.indexOf(mediaDefinition));
////            binding.etEventLink.setText(mediaDefinition.getMedia_event_link());
////            binding.etApiKey.setText(TextUtils.isEmpty(mediaDefinition.getMedia_app_key()) ?
////                    "" : mediaDefinition.getMedia_app_key());
//        } else {
//            binding.btnAdd.setText("ADD");
//            binding.btnDelete.setVisibility(View.GONE);
//            binding.tvTitle.setText("Add Event Media Link");
//        }
//
//        binding.spinnerPlatfrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                binding.tvPlatfromUrl.setText(mediaDefinitionsMain.get(position).getMedia_domain_url());
//                if (isEdit) {
//                    mediaDefinitions.get(pos).setMedia_platform(mediaDefinitionsMain.get(position).getMedia_platform());
//                    mediaDefinitions.get(pos).setMedia_domain_url(mediaDefinitionsMain.get(position).getMedia_domain_url());
//                } else {
//                    mediaDefinition.setMedia_platform(mediaDefinitionsMain.get(position).getMedia_platform());
//                    mediaDefinition.setMedia_domain_url(mediaDefinitionsMain.get(position).getMedia_domain_url());
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//        AlertDialog alertDialog = builder.create();
//
//        binding.btnAdd.setOnClickListener(v -> {
////            if (TextUtils.isEmpty(binding.etEventLink.getText().toString())) {
////                Toast.makeText(getActivity(), "Please Enter URL!", Toast.LENGTH_SHORT).show();
////                return;
////            }
////            if (!TextUtils.isEmpty(binding.etApiKey.getText().toString())) {
////                if (isEdit) {
////                    mediaDefinitions.get(pos).setMedia_app_key(binding.etApiKey.getText().toString());
////                } else {
////                    mediaDefinition.setMedia_app_key(binding.etApiKey.getText().toString());
////                }
////            } else {
////                if (isEdit) {
////                    mediaDefinitions.get(pos).setMedia_app_key("");
////                } else {
////                    mediaDefinition.setMedia_app_key("");
////                }
////            }
//            if (isEdit) {
////                mediaDefinitions.get(pos).setMedia_event_link(binding.etEventLink.getText().toString());
//                mediaAdapter.notifyItemChanged(pos);
//            } else {
////                mediaDefinition.setMedia_event_link(binding.etEventLink.getText().toString());
//                mediaDefinition.setId(mediaDefinitions.size() + 1);
//                mediaDefinitions.add(mediaDefinition);
//                mediaAdapter.notifyDataSetChanged();
//            }
//            alertDialog.dismiss();
//        });
//
//        binding.btnDelete.setOnClickListener(v -> {
//            alertDialog.dismiss();
//            mediaDefinitions.remove(pos);
//            mediaAdapter.notifyItemRemoved(pos);
//        });
//
//
//        alertDialog.show();
//
//    }
//
//    private class MediaAdapter extends RecyclerView.Adapter<MediaAdapter.MediaHolder> {
//
//        @NonNull
//        @Override
//        public MediaAdapter.MediaHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//            return new MediaAdapter.MediaHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_media_link, viewGroup, false));
//        }
//
//        @Override
//        public void onBindViewHolder(@NonNull MediaAdapter.MediaHolder mediaHolder, int i) {
//
//            MediaDefinition mediaDefinition = mediaDefinitions.get(i);
//            if (mediaDefinition != null) {
//                if (!TextUtils.isEmpty(mediaDefinition.getMedia_platform())) {
//                    mediaHolder.tvPlatform.setText(mediaDefinition.getMedia_platform());
//                }
//                if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
//                    mediaHolder.tvPlatformUrl.setText(mediaDefinition.getMedia_domain_url());
//                }
//            }
//
//        }
//
//        @Override
//        public int getItemCount() {
//            return mediaDefinitions.size();
//        }
//
//        public class MediaHolder extends RecyclerView.ViewHolder {
//            TextView tvPlatform, tvPlatformUrl;
//            ImageView ivEditLink;
//
//            public MediaHolder(@NonNull View itemView) {
//                super(itemView);
//                tvPlatform = itemView.findViewById(R.id.tvPlatform);
//                tvPlatformUrl = itemView.findViewById(R.id.tvPlatformUrl);
//                ivEditLink = itemView.findViewById(R.id.ivEditLink);
//
//                ivEditLink.setOnClickListener(v -> showDialogAddMediaLink(true, getAdapterPosition()));
//            }
//        }
//    }
public FragSessionMediaAddEditBinding binding;
    private boolean isEdit = false, isMediaEdit = false, isRepeat = false, isEmpty = false;
    private EventSession eventSession;

    private ArrayList<MediaDefinition> mediaDefinitions = new ArrayList<>();
    private ArrayList<MediaDefinition> mediaDefinitionsMain = new ArrayList<>();
    private MediaAdapter mediaAdapter;

    ImageView selectedFile;
    ImageView selectedAudioVideoFile;

    String selectedFilepath;
    String selectedPPTFilepath;
    String selectedPDFFilepath;
    String selectedAudioVideoFilepath;

    private ArrayList<String> teleNumbers = new ArrayList<>();
    TeleConfNumberAdapter numberAdapter;
    private boolean flag = true;
    private boolean isEditFlag = true;
    private int pos = 0;
    private int permissionFlag;
    private int code = 0;
    private String type = "";
    private EditText etVideoLink;
    private EditText etVideoTitle;
    private ProgressDialog progressDialog;
    private MediaTypeAdapter mediaTypeAdapter;

    private static final int REQUEST_SEARCH_VIDEO = 585;
    private static final int PICK_VIDEO_REQUEST = 104;
    private static final int PICK_AUDIO_REQUEST = 105;
    private static final int VIDEO_CAPTURE = 106;
    private static final int AUDIO_CAPTURE = 107;
    public static String[] PERMISSON = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};
    public static final int REQUEST_PERMISSION_KEY = 100;
    private static final int PICK_PDF_REQUEST = 101;
    private static final int PICK_PPT_REQUEST = 102;
    private static final int PICK_FILE_REQUEST = 103;
    private int selectedMediaType;
    private Uri filePath;
    private PopupWindow popupmanaged;
    private MediaDefinition mediaDefinition;
    private String TAG = "#DEBUG SessionMedia";
    private String storageDir = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            isEdit = getArguments().getBoolean("isEdit", false);
            isRepeat = getArguments().getBoolean("isRepeat", false);
            eventSession = new Gson().fromJson(getArguments().getString("eventSession"), EventSession.class);
            if (isEdit) {
                mediaDefinitions.clear();
                if (!TextUtils.isEmpty(eventSession.getSession_link_array())) {
                    mediaDefinitions.addAll(new Gson().fromJson(eventSession.getSession_link_array(),
                            new TypeToken<ArrayList<MediaDefinition>>() {
                            }.getType()));
                }
            }
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_session_media_add_edit, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        initViews(view);
        updateViews();
        setListeners();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R)
            PERMISSON[PERMISSON.length - 1] = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        new fetchMediaDefinitions().execute();
    }

    private class fetchMediaDefinitions extends AsyncTask<String, String, String> {
        JSONObject json;
        JSONArray json1;
        String eventDefUrl = "_table/event_media_definitions";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mediaDefinitionsMain.clear();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + eventDefUrl;
            Log.e("#DEBUG", "      fetchEventType:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");

                    for (int j = 0; j < jsonArray.length(); j++) {
                        MediaDefinition media = new MediaDefinition();
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            media.setId(object.getInt("id"));
                        }

                        if (object.has("media_platform")
                                && !TextUtils.isEmpty(object.getString("media_platform"))
                                && !object.getString("media_platform").equals("null")) {
                            media.setMedia_platform(object.getString("media_platform"));
                        }
                        if (object.has("media_domain_url")
                                && !TextUtils.isEmpty(object.getString("media_domain_url"))
                                && !object.getString("media_domain_url").equals("null")) {
                            media.setMedia_domain_url(object.getString("media_domain_url"));
                        }
                        if (object.has("media_app_name")
                                && !TextUtils.isEmpty(object.getString("media_app_name"))
                                && !object.getString("media_app_name").equals("null")) {
                            media.setMedia_app_name(object.getString("media_app_name"));
                        }

                        mediaDefinitionsMain.add(media);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
//                if (mediaDefinitionsMain.size() != 0) {
//                    if (mediaAdapter != null) {
//                        mediaAdapter.notifyDataSetChanged();
//                    }
//                } else {
//
//                }
            }
        }
    }

    @Override
    protected void updateViews() {
        /*if (isEdit) {
            if (eventSession != null) {
                if (eventSession.getEvent_session_link_ytube() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_ytube())) {
                    binding.etEventLinkYoutube.setText(eventSession.getEvent_session_link_ytube());
                }

                if (eventSession.getEvent_session_link_zoom() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_zoom())) {
                    binding.etEventLinkZoom.setText(eventSession.getEvent_session_link_zoom());
                }

                if (eventSession.getEvent_session_link_googlemeet() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_googlemeet())) {
                    binding.etEventLinkGoogleMeet.setText(eventSession.getEvent_session_link_googlemeet());
                }

                if (eventSession.getEvent_session_link_googleclassroom() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_googleclassroom())) {
                    binding.etEventLinkGoogleClassroom.setText(eventSession.getEvent_session_link_googleclassroom());
                }

                if (eventSession.getEvent_session_link_facebook() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_facebook())) {
                    binding.etEventLinkFacebook.setText(eventSession.getEvent_session_link_facebook());
                }

                if (eventSession.getEvent_session_link_on_web() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_on_web())) {
                    binding.etEventLinkWeb.setText(eventSession.getEvent_session_link_on_web());
                }

                if (eventSession.getEvent_session_link_whatsapp() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_whatsapp())) {
                    binding.etEventLinkWhatsapp.setText(eventSession.getEvent_session_link_whatsapp());
                }

                if (eventSession.getEvent_session_link_twitter() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_twitter())) {
                    binding.etEventLinkTwitter.setText(eventSession.getEvent_session_link_twitter());
                }

                if (eventSession.getEvent_session_link_other_media() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_other_media())) {
                    binding.etEventLinkOther.setText(eventSession.getEvent_session_link_other_media());
                }

            }

        }*/
    }

    @Override
    protected void setListeners() {
        binding.tvBtnNext.setOnClickListener(v -> {
//            if (validate()) {
            if (mediaDefinitions.size() > 0) {
                Log.d("Event Media Defination", new Gson().toJson(mediaDefinitions));
                eventSession.setSession_link_array(new Gson().toJson(mediaDefinitions));
            }
            Fragment fragment = new SessionLocationAddEditFragment();
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isEdit", isEdit);
            bundle.putBoolean("isRepeat", isRepeat);
            bundle.putString("eventSession", new Gson().toJson(eventSession));
            fragment.setArguments(bundle);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, fragment);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(fragment);
            ft.commitAllowingStateLoss();
//            }
        });

        binding.ivBtnAddMedia.setOnClickListener(v -> {
//            showDialogAddMediaLink(false, -1);
            try {
//                isEdit = false;
                pos = -1;
                if (mediaDefinitionsMain.size() == 0)
                    isEmpty = true;
                else
                    showMediaTypePupup(false, -1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private boolean validate() {
        if (!TextUtils.isEmpty(binding.etEventLinkYoutube.getText())) {
            eventSession.setEvent_session_link_ytube(binding.etEventLinkYoutube.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLinkZoom.getText())) {
            eventSession.setEvent_session_link_zoom(binding.etEventLinkZoom.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLinkGoogleMeet.getText())) {
            eventSession.setEvent_session_link_googlemeet(binding.etEventLinkGoogleMeet.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLinkGoogleClassroom.getText())) {
            eventSession.setEvent_session_link_googleclassroom(binding.etEventLinkGoogleClassroom.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLinkFacebook.getText())) {
            eventSession.setEvent_session_link_facebook(binding.etEventLinkFacebook.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLinkWeb.getText())) {
            eventSession.setEvent_session_link_on_web(binding.etEventLinkWeb.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLinkTwitter.getText())) {
            eventSession.setEvent_session_link_twitter(binding.etEventLinkTwitter.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLinkWhatsapp.getText())) {
            eventSession.setEvent_session_link_whatsapp(binding.etEventLinkWhatsapp.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLinkOther.getText())) {
            eventSession.setEvent_session_link_other_media(binding.etEventLinkOther.getText().toString());
        }

        if (mediaDefinitions.size() > 0) {
            eventSession.setSession_link_array(new Gson().toJson(mediaDefinitions));
        }

        return true;
    }

    @Override
    protected void initViews(View v) {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");

        mediaAdapter = new MediaAdapter();
        binding.rvMediaLinks.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvMediaLinks.setAdapter(mediaAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SessionsFragment.isSessionUpdated)
            if (getActivity() != null) getActivity().onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION_KEY: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (permissionFlag != 3) {
                        showFileChooser(this.code, this.type);
                    } else if (permissionFlag != 2) {
                        recordVideo();
                    } else if (permissionFlag != 1) {
                        recordAudio();
                    }
                } else {
                    ActivityCompat.requestPermissions(getActivity(), PERMISSON, REQUEST_PERMISSION_KEY);
                    Toast.makeText(getActivity(), "You must accept permissions.", Toast.LENGTH_LONG).show();
                }
            }
            break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SEARCH_VIDEO && resultCode == RESULT_OK) {
            if (etVideoLink != null) {
                etVideoLink.setText(String.format("https://youtu.be/%s", data.getStringExtra("URL")));
                etVideoTitle.setText(data.getStringExtra("Title"));
            }
        }
        if (data != null && data.getData() != null) {
            filePath = data.getData();
            selectedFilepath = FilePath.getPath(getActivity(), filePath);
            ContentResolver cR = getActivity().getContentResolver();
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            String type = mime.getExtensionFromMimeType(cR.getType(filePath));
            Log.e("#DEBUG", "  onActivityResult: filePath: " + filePath);
            Log.e("#DEBUG", "  onActivityResult: selectedFilepath: " + selectedFilepath);
            Log.e("#DEBUG", "  onActivityResult: fileType: " + type);
            if (requestCode == PICK_PDF_REQUEST) {
                selectedFile.setVisibility(View.VISIBLE);
                mediaDefinition.setPdfFilePath(selectedFilepath);
            } else if (requestCode == PICK_PPT_REQUEST) {
                if (type.equals("ppt") || type.equals("pptx")) {
                    selectedFile.setVisibility(View.VISIBLE);
                    mediaDefinition.setPptFilePath(selectedFilepath);
                } else {
                    Toast.makeText(getActivity(), "Please select PPT or pptx file!", Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == PICK_FILE_REQUEST) {
                selectedFile.setVisibility(View.VISIBLE);
                mediaDefinition.setFilePath(selectedFilepath);
            } else if (requestCode == PICK_AUDIO_REQUEST) {
                selectedAudioVideoFile.setVisibility(View.VISIBLE);
                mediaDefinition.setAudioVideoPath(selectedFilepath);
            } else if (requestCode == PICK_VIDEO_REQUEST) {
                selectedAudioVideoFile.setVisibility(View.VISIBLE);
                mediaDefinition.setAudioVideoPath(selectedFilepath);
            } else if (requestCode == VIDEO_CAPTURE) {
                if (resultCode == RESULT_OK) {
                    selectedAudioVideoFile.setVisibility(View.VISIBLE);
                    selectedAudioVideoFilepath = FilePath.getPath(getActivity(), filePath);
                    mediaDefinition.setAudioVideoPath(selectedAudioVideoFilepath);
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(getActivity(), "Video recording cancelled.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Failed to record video", Toast.LENGTH_LONG).show();
                }
            } else if (requestCode == AUDIO_CAPTURE) {
                if (resultCode == RESULT_OK) {
                    selectedAudioVideoFile.setVisibility(View.VISIBLE);
                    selectedAudioVideoFilepath = FilePath.getPath(getActivity(), filePath);
                    mediaDefinition.setAudioVideoPath(selectedAudioVideoFilepath);
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(getActivity(), "Audio recording cancelled.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Failed to record audio", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void showMediaTypePupup(boolean isEdit, int pos) {
        LinearLayout viewGroup = (LinearLayout) getActivity().findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_event_category, viewGroup, false);
        popupmanaged = new PopupWindow(getActivity());
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        //  popupmanaged.setAnimationStyle(R.style.animationName);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);
        RecyclerView rvMediaType;
        TextView txt_title;
        ImageView ivClose;

        TextView tvNext = (TextView) layout.findViewById(R.id.tvNext);
        txt_title = (TextView) layout.findViewById(R.id.tvTitle);
        rvMediaType = (RecyclerView) layout.findViewById(R.id.rvEventTypeList);
        ivClose = (ImageView) layout.findViewById(R.id.ivClose);

        txt_title.setText("Event Media Type");
        mediaTypeAdapter = new MediaTypeAdapter();
        rvMediaType.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        rvMediaType.setAdapter(mediaTypeAdapter);
        rvMediaType.scrollToPosition(selectedMediaType);
        isMediaEdit = isEdit;
        if (isEdit)
            selectedMediaType = mediaDefinitionsMain.indexOf(mediaDefinitions.get(pos).getMedia_platform());
        tvNext.setOnClickListener(v -> {
            showDialogAddMediaLink(isMediaEdit, pos);
            popupmanaged.dismiss();
        });
        ivClose.setOnClickListener(v -> popupmanaged.dismiss());
        //        popupmanaged.dismiss();
    }

    private class MediaTypeAdapter extends RecyclerView.Adapter<MediaTypeAdapter.MediaTypeHolder> {
        @NonNull
        @Override
        public MediaTypeAdapter.MediaTypeHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new MediaTypeAdapter.MediaTypeHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_event_category, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MediaTypeAdapter.MediaTypeHolder holder, int i) {
            holder.tvCategoryTitle.setText(mediaDefinitionsMain.get(i).getMedia_platform());
            Glide.with(getActivity()).load(mediaDefinitionsMain.get(i).getIcon_media_type_link()).into(holder.ivEventCategoryImage);
            holder.llItem.setBackgroundResource(selectedMediaType == i ? R.drawable.highlight : 0);
            holder.tvCategoryTitle.setTextColor(ContextCompat.getColor(getContext(), selectedMediaType == i ? R.color.itemType : R.color.grey_700));
        }

        @Override
        public int getItemCount() {
            return mediaDefinitionsMain.size();
        }

        public class MediaTypeHolder extends RecyclerView.ViewHolder {
            ImageView ivEventCategoryImage;
            TextView tvCategoryTitle;
            LinearLayout llItem;

            public MediaTypeHolder(@NonNull View itemView) {
                super(itemView);
                ivEventCategoryImage = itemView.findViewById(R.id.ivEvent_category_image);
                tvCategoryTitle = itemView.findViewById(R.id.tvEventCategory);
                llItem = itemView.findViewById(R.id.ll_item);

                itemView.setOnClickListener(v -> {
                    selectedMediaType = getBindingAdapterPosition();
                    notifyDataSetChanged();
                });
            }
        }
    }

    private void showDialogAddMediaLink(boolean isEdit, int pos) {
        if (pos == -1) {
            mediaDefinition = new MediaDefinition();
        } else {
            mediaDefinition = mediaDefinitions.get(pos);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogAddEditMediaLinkBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_add_edit_media_link, null, false);
        builder.setView(binding.getRoot());

        etVideoLink = binding.etYoutubeStreamUrl;
        etVideoTitle = binding.etYoutubeTitle;
        /*ArrayAdapter<MediaDefinition> categoryAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, mediaDefinitionsMain);
        binding.spinnerPlatfrom.setAdapter(categoryAdapter);*/
        /* binding.etEventMediaType.setOnClickListener(v -> {
            showDialogMediaType();
        });*/

        mediaDefinition.setMedia_platform(mediaDefinitionsMain.get(selectedMediaType).getMedia_platform());
        mediaDefinition.setMedia_domain_url(mediaDefinitionsMain.get(selectedMediaType).getMedia_domain_url());

        binding.etEventMediaType.setText(mediaDefinitionsMain.get(selectedMediaType).getMedia_platform());
        binding.tvPlatfromUrl.setText(mediaDefinitionsMain.get(selectedMediaType).getMedia_domain_url());

        binding.llYoutube.setVisibility(View.GONE);
        binding.llFacebook.setVisibility(View.GONE);
        binding.llGClassroom.setVisibility(View.GONE);
        binding.llGoogleMeet.setVisibility(View.GONE);
        binding.llZoom.setVisibility(View.GONE);
        binding.llSkype.setVisibility(View.GONE);
        binding.llfile.setVisibility(View.GONE);
        binding.llTeleConference.setVisibility(View.GONE);
        binding.llMeetingRoom.setVisibility(View.GONE);

        if (mediaDefinitionsMain.get(selectedMediaType).getMedia_platform().equals("YouTube")) {
            binding.llYoutube.setVisibility(View.VISIBLE);
        } else if (mediaDefinitionsMain.get(selectedMediaType).getMedia_platform().equals("Zoom")) {
            binding.llZoom.setVisibility(View.VISIBLE);
        } else if (mediaDefinitionsMain.get(selectedMediaType).getMedia_platform().equals("Facebook")) {
            binding.llFacebook.setVisibility(View.VISIBLE);
        } else if (mediaDefinitionsMain.get(selectedMediaType).getMedia_platform().equals("Google Meet")) {
            binding.llGoogleMeet.setVisibility(View.VISIBLE);
        } else if (mediaDefinitionsMain.get(selectedMediaType).getMedia_platform().equals("Google Classroom")) {
            binding.llGClassroom.setVisibility(View.VISIBLE);
        } else if (mediaDefinitionsMain.get(selectedMediaType).getMedia_platform().equals("Skype")) {
            binding.llSkype.setVisibility(View.VISIBLE);
        } else if (mediaDefinitionsMain.get(selectedMediaType).getMedia_platform().equals("PDF")) {
            binding.llfile.setVisibility(View.VISIBLE);
            binding.llPassword.setVisibility(View.VISIBLE);
            binding.tvUploadFile.setText("Upload PDF ");
            binding.btnChooseFile.setText(" Choose PDF ");
            binding.tvPlatformUrlTitle.setVisibility(View.GONE);
            binding.tvPlatfromUrl.setVisibility(View.GONE);
        } else if (mediaDefinitionsMain.get(selectedMediaType).getMedia_platform().equals("Powerpoint")) {
            binding.llfile.setVisibility(View.VISIBLE);
            binding.tvUploadFile.setText("Upload PPT ");
            binding.btnChooseFile.setText(" Choose PPT ");
            binding.tvPlatformUrlTitle.setVisibility(View.GONE);
            binding.tvPlatfromUrl.setVisibility(View.GONE);
        } else if (mediaDefinitionsMain.get(selectedMediaType).getMedia_platform().equals("Slideshow")) {
            binding.llfile.setVisibility(View.VISIBLE);
            binding.tvUploadFile.setText("Upload File ");
            binding.btnChooseFile.setText(" Choose File ");
            binding.tvPlatformUrlTitle.setVisibility(View.GONE);
            binding.tvPlatfromUrl.setVisibility(View.GONE);
        } else if (mediaDefinitionsMain.get(selectedMediaType).getMedia_platform().equals("TeleConference")) {
            binding.llTeleConference.setVisibility(View.VISIBLE);
            numberAdapter = new TeleConfNumberAdapter();
            binding.rvTeleConfNumber.setLayoutManager(new LinearLayoutManager(getActivity()));
            binding.rvTeleConfNumber.setAdapter(numberAdapter);
            if (teleNumbers.size() == 0) {
                teleNumbers.add("");
            }
            numberAdapter.notifyDataSetChanged();
        } else if (mediaDefinitionsMain.get(selectedMediaType).getMedia_platform().equals("Audio/Video")) {
            mediaDefinition.setAudioVideoType("Audio");
            binding.llAudioVideo.setVisibility(View.VISIBLE);
            binding.tvPlatformUrlTitle.setVisibility(View.GONE);
            binding.tvPlatfromUrl.setVisibility(View.GONE);
        } else if (mediaDefinitionsMain.get(selectedMediaType).getMedia_platform().equals("Audio Meetin Room")) {
            binding.llMeetingRoom.setVisibility(View.VISIBLE);
            binding.tvPlatformUrlTitle.setVisibility(View.GONE);
            binding.tvPlatfromUrl.setVisibility(View.GONE);
        } else if (mediaDefinitionsMain.get(selectedMediaType).getMedia_platform().equals("Video Meeting Room")) {
            binding.llMeetingRoom.setVisibility(View.VISIBLE);
            binding.tvPlatformUrlTitle.setVisibility(View.GONE);
            binding.tvPlatfromUrl.setVisibility(View.GONE);
        }

        if (isEdit) {
            binding.btnAdd.setText("UPDATE");
            binding.tvTitle.setText("Edit Event Media Link");
            binding.btnDelete.setVisibility(View.VISIBLE);

            selectedMediaType = mediaDefinitionsMain.indexOf(mediaDefinition);

            if (mediaDefinition.getMedia_platform().equals("YouTube")) {
                if (mediaDefinition.getLinkType() != null
                        && !TextUtils.isEmpty(mediaDefinition.getLinkType())
                        && !mediaDefinition.getLinkType().equalsIgnoreCase("null")
                        && mediaDefinition.getLinkType().equals(Constants.LINK_TYPE_EXISTING)) {
                    binding.rgYoutubeLinkType.check(R.id.rbtnYoutubeExisting);
                } else {
                    binding.rgYoutubeLinkType.check(R.id.rbtnYoutubeScheduled);
                }
            } else if (mediaDefinition.getMedia_platform().equals("Zoom")) {
                if (mediaDefinition.getLinkType() != null && !TextUtils.isEmpty(mediaDefinition.getLinkType())
                        && !mediaDefinition.getLinkType().equalsIgnoreCase("null")
                        && mediaDefinition.getLinkType().equals(Constants.LINK_TYPE_SCHEDULED)) {
                    binding.rgZoomLinkType.check(R.id.rbtnZoomScheduled);
                } else {
                    binding.rgZoomLinkType.check(R.id.rbtnZoomExisting);
                }
            } else if (mediaDefinition.getMedia_platform().equals("Facebook")) {
                if (mediaDefinition.getLinkType() != null && !TextUtils.isEmpty(mediaDefinition.getLinkType())
                        && !mediaDefinition.getLinkType().equalsIgnoreCase("null")
                        && mediaDefinition.getLinkType().equals(Constants.LINK_TYPE_SCHEDULED)) {
                    binding.rgFacebookLinkType.check(R.id.rbtnFacebookScheduled);
                } else {
                    binding.rgFacebookLinkType.check(R.id.rbtnFacebookExisting);
                }
            } else if (mediaDefinition.getMedia_platform().equals("Google Meet")) {
                if (mediaDefinition.getLinkType() != null && !TextUtils.isEmpty(mediaDefinition.getLinkType())
                        && !mediaDefinition.getLinkType().equalsIgnoreCase("null")
                        && mediaDefinition.getLinkType().equals(Constants.LINK_TYPE_EXISTING)) {
                    binding.rgGoogleMeetLinkType.check(R.id.rbtnGoogleMeetExisting);
                } else {
                    binding.rgGoogleMeetLinkType.check(R.id.rbtnGoogleMeetScheduled);
                }
            } else if (mediaDefinition.getMedia_platform().equals("Skype")) {
                if (mediaDefinition.getLinkType() != null && !TextUtils.isEmpty(mediaDefinition.getLinkType())
                        && !mediaDefinition.getLinkType().equalsIgnoreCase("null")
                        && mediaDefinition.getLinkType().equals(Constants.LINK_TYPE_SCHEDULED)) {
                    binding.rgSkypeLinkType.check(R.id.rbtnSkypeScheduled);
                } else {
                    binding.rgSkypeLinkType.check(R.id.rbtnSkypeExisting);
                }
            } else if (mediaDefinition.getMedia_platform().equals("Audio/Video") && mediaDefinition.getAudioVideoType() != null && !TextUtils.isEmpty(mediaDefinition.getAudioVideoType())) {
                if (mediaDefinition.getAudioVideoType().equals("Audio")) {
                    binding.rgAudioVideoType.check(R.id.rbtnAudio);
                } else if (mediaDefinition.getAudioVideoType().equals("Video")) {
                    binding.rgAudioVideoType.check(R.id.rbtnVideo);
                }
            }

            if (mediaDefinition.getYoutubeStreamUrl() != null && !TextUtils.isEmpty(mediaDefinition.getYoutubeStreamUrl())) {
                binding.etYoutubeStreamUrl.setText(mediaDefinition.getYoutubeStreamUrl());
            }
            if (mediaDefinition.getYoutubeStreamTitle() != null && !TextUtils.isEmpty(mediaDefinition.getYoutubeStreamTitle())) {
                binding.etYoutubeTitle.setText(mediaDefinition.getYoutubeStreamTitle());
            }
            if (mediaDefinition.getZoomSpecialUrl() != null && !TextUtils.isEmpty(mediaDefinition.getZoomSpecialUrl())) {
                binding.etZoomSpecialUrl.setText(mediaDefinition.getZoomSpecialUrl());
            }
            if (mediaDefinition.getZoomMeetingId() != null && !TextUtils.isEmpty(mediaDefinition.getZoomMeetingId())) {
                binding.etZoomMeetingId.setText(mediaDefinition.getZoomMeetingId());
            }
            if (mediaDefinition.getZoomPassword() != null && !TextUtils.isEmpty(mediaDefinition.getZoomPassword())) {
                try {
                    binding.etZoomPassword.setText(AESEncyption.decrypt(mediaDefinition.getZoomPassword()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (mediaDefinition.getZoomPhone() != null && !TextUtils.isEmpty(mediaDefinition.getZoomPhone())) {
                binding.etZoomPhone.setText(mediaDefinition.getZoomPhone());
            }
            if (mediaDefinition.getFacebookServerUrl() != null && !TextUtils.isEmpty(mediaDefinition.getFacebookServerUrl())) {
                binding.etFacebookServerUrl.setText(mediaDefinition.getFacebookServerUrl());
                if (mediaDefinition.getLinkType().equals(Constants.LINK_TYPE_EXISTING)) {
                    binding.rgFacebookLinkType.check(R.id.rbtnFacebookExisting);
                } else {
                    binding.rgFacebookLinkType.check(R.id.rbtnFacebookScheduled);
                }
            }

            if (mediaDefinition.getFacebookStreamKey() != null && !TextUtils.isEmpty(mediaDefinition.getFacebookStreamKey())) {
                try {
                    binding.etFacebookStreamKey.setText(AESEncyption.decrypt(mediaDefinition.getFacebookStreamKey()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (mediaDefinition.getGoogleMeetUrl() != null && !TextUtils.isEmpty(mediaDefinition.getGoogleMeetUrl())) {
                binding.etGMeetURL.setText(mediaDefinition.getGoogleMeetUrl());
            }
            if (mediaDefinition.getGoogleClasstroomCode() != null && !TextUtils.isEmpty(mediaDefinition.getGoogleClasstroomCode())) {
                binding.etGClassroomCode.setText(mediaDefinition.getGoogleClasstroomCode());
            }
            if (mediaDefinition.getSkypeStreamUrl() != null && !TextUtils.isEmpty(mediaDefinition.getSkypeStreamUrl())) {
                binding.etSkypeStreamUrl.setText(mediaDefinition.getSkypeStreamUrl());
            }
            if (mediaDefinition.getPdfFilePath() != null && !TextUtils.isEmpty(mediaDefinition.getPdfFilePath())) {
                selectedPDFFilepath = mediaDefinition.getPdfFilePath();
            }
            if (mediaDefinition.getPdfPassword() != null && !TextUtils.isEmpty(mediaDefinition.getPdfPassword())) {
                try {
                    binding.etPdfPassword.setText(AESEncyption.decrypt(mediaDefinition.getPdfPassword()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                binding.etPdfPassword.setText(mediaDefinition.getPdfPassword());
            }
            if (mediaDefinition.getPptFilePath() != null && !TextUtils.isEmpty(mediaDefinition.getPptFilePath())) {
                selectedPPTFilepath = mediaDefinition.getPptFilePath();
            }
            if (mediaDefinition.getFilePath() != null && !TextUtils.isEmpty(mediaDefinition.getFilePath())) {
                selectedFilepath = mediaDefinition.getFilePath();
            }
            if (mediaDefinition.getTeleConfCountryCode() != null && !TextUtils.isEmpty(mediaDefinition.getTeleConfCountryCode())) {
                binding.etTeleConfCountryCode.setText(mediaDefinition.getTeleConfCountryCode());
            }
            if (mediaDefinition.getTeleConfNumbers() != null) {
                teleNumbers.clear();
                teleNumbers.addAll(mediaDefinition.getTeleConfNumbers());
            }
            if (mediaDefinition.getTeleConfMeetingId() != null && !TextUtils.isEmpty(mediaDefinition.getTeleConfMeetingId())) {
                binding.etTeleConfMeetingId.setText(mediaDefinition.getTeleConfMeetingId());
            }
            if (mediaDefinition.getTeleConfMeetingPassword() != null && !TextUtils.isEmpty(mediaDefinition.getTeleConfMeetingPassword())) {
                binding.etTeleConfMeetingPassword.setText(mediaDefinition.getTeleConfMeetingPassword());
            }
            if (mediaDefinition.getAudioVideoPath() != null && !TextUtils.isEmpty(mediaDefinition.getAudioVideoPath())) {
                selectedAudioVideoFilepath = mediaDefinition.getAudioVideoPath();
            }

            if (mediaDefinition.getMedia_short_description() != null && !TextUtils.isEmpty(mediaDefinition.getMedia_short_description())) {
                binding.etMediaShortDscr.setText(mediaDefinition.getMedia_short_description());
            }
            if (mediaDefinition.getMedia_page_pin() != null && !TextUtils.isEmpty(mediaDefinition.getMedia_page_pin())) {
                try {
                    binding.etMediaPagePin.setText(AESEncyption.decrypt(mediaDefinition.getMedia_page_pin()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            binding.btnAdd.setText("ADD");
            binding.btnDelete.setVisibility(View.GONE);
            binding.tvTitle.setText("Add Event Media Link");
        }

        selectedFile = binding.ivSelectedFile;
        selectedAudioVideoFile = binding.ivSelectedAudioVideoFile;

        /*binding.spinnerPlatfrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                binding.tvPlatfromUrl.setText(mediaDefinitionsMain.get(position).getMedia_domain_url());
                if (isEdit) {
                    mediaDefinitions.get(pos).setMedia_platform(mediaDefinitionsMain.get(position).getMedia_platform());
                    mediaDefinitions.get(pos).setMedia_domain_url(mediaDefinitionsMain.get(position).getMedia_domain_url());
                } else {
                    mediaDefinition.setMedia_platform(mediaDefinitionsMain.get(position).getMedia_platform());
                    mediaDefinition.setMedia_domain_url(mediaDefinitionsMain.get(position).getMedia_domain_url());
                }
                binding.llYoutube.setVisibility(View.GONE);
                binding.llFacebook.setVisibility(View.GONE);
                binding.llGClassroom.setVisibility(View.GONE);
                binding.llGoogleMeet.setVisibility(View.GONE);
                binding.llZoom.setVisibility(View.GONE);
                binding.llSkype.setVisibility(View.GONE);
                binding.llfile.setVisibility(View.GONE);
                binding.llTeleConference.setVisibility(View.GONE);

                if (mediaDefinitionsMain.get(position).getMedia_platform().equals("YouTube")) {
                    binding.llYoutube.setVisibility(View.VISIBLE);
                } else if (mediaDefinitionsMain.get(position).getMedia_platform().equals("Zoom")) {
                    binding.llZoom.setVisibility(View.VISIBLE);
                } else if (mediaDefinitionsMain.get(position).getMedia_platform().equals("Facebook")) {
                    binding.llFacebook.setVisibility(View.VISIBLE);
                } else if (mediaDefinitionsMain.get(position).getMedia_platform().equals("Google Meet")) {
                    binding.llGoogleMeet.setVisibility(View.VISIBLE);
                } else if (mediaDefinitionsMain.get(position).getMedia_platform().equals("Google Classroom")) {
                    binding.llGClassroom.setVisibility(View.VISIBLE);
                } else if (mediaDefinitionsMain.get(position).getMedia_platform().equals("Skype")) {
                    binding.llSkype.setVisibility(View.VISIBLE);
                } else if (mediaDefinitionsMain.get(position).getMedia_platform().equals("PDF")) {
                    binding.llfile.setVisibility(View.VISIBLE);
                    binding.llPassword.setVisibility(View.VISIBLE);
                    binding.tvUploadFile.setText("Upload PDF ");
                    binding.btnChooseFile.setText(" Choose PDF ");
                    binding.tvPlatformUrlTitle.setVisibility(View.GONE);
                    binding.tvPlatfromUrl.setVisibility(View.GONE);
                } else if (mediaDefinitionsMain.get(position).getMedia_platform().equals("Powerpoint")) {
                    binding.llfile.setVisibility(View.VISIBLE);
                    binding.tvUploadFile.setText("Upload PPT ");
                    binding.btnChooseFile.setText(" Choose PPT ");
                    binding.tvPlatformUrlTitle.setVisibility(View.GONE);
                    binding.tvPlatfromUrl.setVisibility(View.GONE);
                } else if (mediaDefinitionsMain.get(position).getMedia_platform().equals("Slideshow")) {
                    binding.llfile.setVisibility(View.VISIBLE);
                    binding.tvUploadFile.setText("Upload File ");
                    binding.btnChooseFile.setText(" Choose File ");
                    binding.tvPlatformUrlTitle.setVisibility(View.GONE);
                    binding.tvPlatfromUrl.setVisibility(View.GONE);
                } else if (mediaDefinitionsMain.get(position).getMedia_platform().equals("TeleConference")) {
                    binding.llTeleConference.setVisibility(View.VISIBLE);
                    numberAdapter = new TeleConfNumberAdapter();
                    binding.rvTeleConfNumber.setLayoutManager(new LinearLayoutManager(getActivity()));
                    binding.rvTeleConfNumber.setAdapter(numberAdapter);
                    if (teleNumbers.size() == 0) {
                        teleNumbers.add("");
                    }
                    numberAdapter.notifyDataSetChanged();
                } else if (mediaDefinitionsMain.get(position).getMedia_platform().equals("Audio/Video")) {
                    mediaDefinition.setAudioVideoType("Audio");
                    binding.llAudioVideo.setVisibility(View.VISIBLE);
                    binding.tvPlatformUrlTitle.setVisibility(View.GONE);
                    binding.tvPlatfromUrl.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/

        AlertDialog alertDialog = builder.create();

        binding.rgAudioVideoType.setOnCheckedChangeListener((radioGroup, i) -> {
            if (i == R.id.rbtnAudio)
                mediaDefinition.setAudioVideoType("Audio");
            else if (i == R.id.rbtnVideo)
                mediaDefinition.setAudioVideoType("Video");

            if (binding.rbtnUpload.isChecked()) {
                binding.btnChooseAudioVideoFile.setVisibility(View.VISIBLE);
            } else if (binding.rbtnRecord.isChecked()) {
                binding.btnChooseAudioVideoFile.setVisibility(View.GONE);
                if (mediaDefinition.getAudioVideoType() == "Audio") {
                    recordAudio();
                } else if (mediaDefinition.getAudioVideoType() == "Video") {
                    recordVideo();
                }
            }
        });

        binding.rgUploadType.setOnCheckedChangeListener((radioGroup, i) -> {
            if (i == R.id.rbtnUpload) {
                binding.btnChooseAudioVideoFile.setVisibility(View.VISIBLE);
            } else if (i == R.id.rbtnRecord) {
                binding.btnChooseAudioVideoFile.setVisibility(View.GONE);
                if (mediaDefinition.getAudioVideoType() == "Audio") {
                    recordAudio();
                } else if (mediaDefinition.getAudioVideoType() == "Video") {
                    recordVideo();
                }
            }
        });

        binding.btnSearchVideo.setOnClickListener(view -> startActivityForResult(new Intent(getActivity(),
                YoutubeSearchVideoActivity.class), REQUEST_SEARCH_VIDEO));

        binding.ivBtnAddTeleConfNumber.setOnClickListener(view -> {
            teleNumbers.add("");
            numberAdapter.notifyItemInserted(teleNumbers.size());
        });

        binding.btnChooseFile.setOnClickListener(v -> {
            if (mediaDefinition.getMedia_platform().equals("PDF")) {
                showFileChooser(PICK_PDF_REQUEST, "application/pdf");
            } else if (mediaDefinition.getMedia_platform().equals("Powerpoint")) {
                showFileChooser(PICK_PPT_REQUEST, "application/*");
            } else if (mediaDefinition.getMedia_platform().equals("Slideshow")) {
                showFileChooser(PICK_FILE_REQUEST, "application/vnd.openxmlformats-officedocument.presentationml.presentation");
                //"application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation"  //.ppt,.pptx
            }
        });

        binding.btnChooseAudioVideoFile.setOnClickListener(view -> {
            if (binding.rbtnUpload.isChecked()) {
                if (binding.rbtnAudio.isChecked())
                    showFileChooser(PICK_AUDIO_REQUEST, "audio/*");
                if (binding.rbtnVideo.isChecked())
                    showFileChooser(PICK_VIDEO_REQUEST, "video/*");
            }
        });

        binding.btnAdd.setOnClickListener(v -> {
            if (mediaDefinition.getMedia_platform().equals("YouTube")) {
                if (binding.rgYoutubeLinkType.getCheckedRadioButtonId() == -1) {
                    Toast.makeText(getActivity(), "Please Select Link!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(binding.etYoutubeTitle.getText().toString())) {
                    Toast.makeText(getActivity(), "Please enter Title!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    mediaDefinition.setYoutubeStreamTitle(binding.etYoutubeTitle.getText().toString());
                }
                int selectedId = binding.rgYoutubeLinkType.getCheckedRadioButtonId();
                if (selectedId == R.id.rbtnYoutubeExisting) {
                    mediaDefinition.setLinkType(Constants.LINK_TYPE_EXISTING);
                } else {
                    mediaDefinition.setLinkType(Constants.LINK_TYPE_SCHEDULED);
                }

                if (TextUtils.isEmpty(binding.etYoutubeStreamUrl.getText().toString())) {
                    Toast.makeText(getActivity(), "Please enter stream Url!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    mediaDefinition.setYoutubeStreamUrl(binding.etYoutubeStreamUrl.getText().toString());
                }
            } else if (mediaDefinition.getMedia_platform().equals("Zoom")) {
                if (TextUtils.isEmpty(binding.etZoomSpecialUrl.getText().toString())) {
                    Toast.makeText(getActivity(), "Please enter Special Url!", Toast.LENGTH_SHORT).show();
                    return;
                } else if (TextUtils.isEmpty(binding.etZoomMeetingId.getText().toString())) {
                    Toast.makeText(getActivity(), "Please enter meeting id!", Toast.LENGTH_SHORT).show();
                    return;
                }
                mediaDefinition.setZoomSpecialUrl(binding.etZoomSpecialUrl.getText().toString());
                mediaDefinition.setZoomMeetingId(binding.etZoomMeetingId.getText().toString());
                if (!TextUtils.isEmpty(binding.etZoomPassword.getText().toString())) {
                    try {
                        mediaDefinition.setZoomPassword(encrypt(binding.etZoomPassword.getText().toString()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!TextUtils.isEmpty(binding.etZoomPhone.getText().toString())) {
                    mediaDefinition.setZoomPhone(binding.etZoomPhone.getText().toString());
                }
            } else if (mediaDefinition.getMedia_platform().equals("Facebook")) {
                if (binding.rgFacebookLinkType.getCheckedRadioButtonId() == -1) {
                    Toast.makeText(getActivity(), "Please Select Link!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    int selectedId = binding.rgFacebookLinkType.getCheckedRadioButtonId();
                    if (selectedId == R.id.rbtnFacebookExisting) {
                        mediaDefinition.setLinkType(Constants.LINK_TYPE_EXISTING);
                    } else {
                        mediaDefinition.setLinkType(Constants.LINK_TYPE_SCHEDULED);
                    }
                }
                if (TextUtils.isEmpty(binding.etFacebookServerUrl.getText().toString())) {
                    Toast.makeText(getActivity(), "Please enter Server url!", Toast.LENGTH_SHORT).show();
                    return;
                } else if (TextUtils.isEmpty(binding.etFacebookStreamKey.getText().toString())) {
                    Toast.makeText(getActivity(), "Please enter Stream key!", Toast.LENGTH_SHORT).show();
                    return;
                }
                mediaDefinition.setFacebookServerUrl(binding.etFacebookServerUrl.getText().toString());
                try {
                    mediaDefinition.setFacebookStreamKey(encrypt(binding.etFacebookStreamKey.getText().toString()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (mediaDefinition.getMedia_platform().equals("Google Meet")) {
                if (TextUtils.isEmpty(binding.etGMeetURL.getText().toString())) {
                    Toast.makeText(getActivity(), "Please enter Meeting Url!", Toast.LENGTH_SHORT).show();
                    return;
                }
                mediaDefinition.setGoogleMeetUrl(binding.etGMeetURL.getText().toString());
                mediaDefinition.setGoogleMeetPhoneNumber(binding.etGMeetPhoneNumber.getText().toString());
                if (!TextUtils.isEmpty(binding.etGMeetPassword.getText().toString())) {
                    try {
                        mediaDefinition.setGoogleMeetPassword(encrypt(binding.etGMeetPassword.getText().toString()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                int selectedId = binding.rgGoogleMeetLinkType.getCheckedRadioButtonId();
                if (selectedId == R.id.rbtnGoogleMeetExisting) {
                    mediaDefinition.setLinkType(Constants.LINK_TYPE_EXISTING);
                } else {
                    mediaDefinition.setLinkType(Constants.LINK_TYPE_SCHEDULED);
                }
            } else if (mediaDefinition.getMedia_platform().equals("Google Classroom")) {
                if (TextUtils.isEmpty(binding.etGClassroomCode.getText().toString())) {
                    Toast.makeText(getActivity(), "Please enter classroom code!", Toast.LENGTH_SHORT).show();
                    return;
                }
                mediaDefinition.setGoogleClasstroomCode(binding.etGClassroomCode.getText().toString());
            } else if (mediaDefinition.getMedia_platform().equals("Skype")) {
                if (TextUtils.isEmpty(binding.etSkypeStreamUrl.getText().toString())) {
                    Toast.makeText(getActivity(), "Please enter Stream url!", Toast.LENGTH_SHORT).show();
                    return;
                }
                mediaDefinition.setSkypeStreamUrl(binding.etSkypeStreamUrl.getText().toString());
            } else if (mediaDefinition.getMedia_platform().equals("PDF")) {
                //PDF
                if (mediaDefinition.getPdfFilePath() == null) {
                    Toast.makeText(getActivity(), "Please choose your PDF File!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!TextUtils.isEmpty(binding.etPdfPassword.getText().toString())) {
                    try {
                        mediaDefinition.setPdfPassword(encrypt(binding.etPdfPassword.getText().toString()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (mediaDefinition.getMedia_platform().equals("Powerpoint")) {
                //Powerpoint
                if (mediaDefinition.getPptFilePath() == null) {
                    Toast.makeText(getActivity(), "Please choose your Powerpoint File!", Toast.LENGTH_SHORT).show();
                    return;
                }
            } else if (mediaDefinition.getMedia_platform().equals("Slideshow")) {
                //Slideshow
                if (mediaDefinition.getFilePath() == null) {
                    Toast.makeText(getActivity(), "Please choose your Slideshow File!", Toast.LENGTH_SHORT).show();
                    return;
                }
            } else if (mediaDefinition.getMedia_platform().equals("TeleConference")) {
                if (TextUtils.isEmpty(binding.etTeleConfCountryCode.getText().toString())) {
                    Toast.makeText(getActivity(), "Please enter country code!", Toast.LENGTH_SHORT).show();
                    return;
                } else if (teleNumbers.size() == 1 && TextUtils.isEmpty(teleNumbers.get(0))) {
                    Toast.makeText(getActivity(), "Conference number is required!", Toast.LENGTH_SHORT).show();
                    return;
                }
                mediaDefinition.setTeleConfCountryCode(binding.etTeleConfCountryCode.getText().toString());
                mediaDefinition.setTeleConfNumbers(teleNumbers);
                if (!TextUtils.isEmpty(binding.etTeleConfMeetingId.getText().toString())) {
                    mediaDefinition.setTeleConfMeetingId(binding.etTeleConfMeetingId.getText().toString());
                }
                if (!TextUtils.isEmpty(binding.etTeleConfMeetingPassword.getText().toString())) {
                    try {
                        mediaDefinition.setTeleConfMeetingPassword(encrypt(binding.etTeleConfMeetingPassword.getText().toString()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (mediaDefinition.getMedia_platform().equals("Audio/Video")) {
                int selectedId = binding.rgAudioVideoType.getCheckedRadioButtonId();
                if (selectedId == R.id.rbtnAudio) {
                    mediaDefinition.setAudioVideoType("Audio");
                } else {
                    mediaDefinition.setAudioVideoType("Video");
                }
                //AudioVideo
                if (mediaDefinition.getAudioVideoPath() == null) {
                    Toast.makeText(getActivity(), "Please choose your File!", Toast.LENGTH_SHORT).show();
                    return;
                }
            } else if (mediaDefinition.getMedia_platform().equals("Audio Meeting Room")) {
                int selectedId = binding.rgMeetingType.getCheckedRadioButtonId();
                if (selectedId == R.id.rbtnOneToOne) {
                    mediaDefinition.setMeeting_type("OneToOne");
                } else {
                    mediaDefinition.setMeeting_type("Group");
                }
                if (!TextUtils.isEmpty(binding.etMeetingId.getText().toString())) {
                    mediaDefinition.setMeeting_id(binding.etMeetingId.getText().toString());
                }

            } else if (mediaDefinition.getMedia_platform().equals("Video Meeting Room")) {
                int selectedId = binding.rgMeetingType.getCheckedRadioButtonId();
                if (selectedId == R.id.rbtnOneToOne) {
                    mediaDefinition.setMeeting_type("OneToOne");
                } else {
                    mediaDefinition.setMeeting_type("Group");
                }
                if (!TextUtils.isEmpty(binding.etMeetingId.getText().toString())) {
                    mediaDefinition.setMeeting_id(binding.etMeetingId.getText().toString());
                }
            }
            if (!TextUtils.isEmpty(binding.etMediaShortDscr.getText().toString())) {
                mediaDefinition.setMedia_short_description(binding.etMediaShortDscr.getText().toString());
            }
            try {
                if (!TextUtils.isEmpty(binding.etMediaPagePin.getText().toString())) {
                    mediaDefinition.setMedia_page_pin(encrypt(binding.etMediaPagePin.getText().toString()));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (isEdit) {
                mediaAdapter.notifyItemChanged(pos);
            } else {
                mediaDefinition.setId(mediaDefinitions.size() + 1);
                mediaDefinitions.add(mediaDefinition);
                mediaAdapter.notifyDataSetChanged();
            }
            alertDialog.dismiss();
        });
        binding.btnDelete.setOnClickListener(v -> {
            alertDialog.dismiss();
            mediaDefinitions.remove(pos);
            mediaAdapter.notifyItemRemoved(pos);
        });
        binding.ivBtnClose.setOnClickListener(v -> alertDialog.dismiss());
        alertDialog.show();
    }

    //method to show file chooser
    private void showFileChooser(int code, String type) {
        if (!MyApplication.hasPermissions(getActivity(), PERMISSON)) {
            permissionFlag = 3;
            this.code = code;
            this.type = type;
            ActivityCompat.requestPermissions(getActivity(), PERMISSON, REQUEST_PERMISSION_KEY);
        } else {
            Intent intent = new Intent();
            intent.setType(type); //            intent.setType("*/*"); // all File
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select File"), code);
        }
    }

    private void recordAudio() {
        if (!MyApplication.hasPermissions(getActivity(), PERMISSON)) {
            permissionFlag = 1;
            ActivityCompat.requestPermissions(getActivity(), PERMISSON, REQUEST_PERMISSION_KEY);
        } else {
            Intent intent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
            startActivityForResult(intent, AUDIO_CAPTURE);
        }
    }

    private void recordVideo() {
        if (!MyApplication.hasPermissions(getActivity(), PERMISSON)) {
            permissionFlag = 2;
            ActivityCompat.requestPermissions(getActivity(), PERMISSON, REQUEST_PERMISSION_KEY);
        } else {
            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            startActivityForResult(intent, VIDEO_CAPTURE);
        }
    }

    /*private boolean validate() {
        if (!TextUtils.isEmpty(binding.etEventLinkYoutube.getText())) {
            eventDefinition.setEvent_link_ytube(binding.etEventLinkYoutube.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLinkZoom.getText())) {
            eventDefinition.setEvent_link_zoom(binding.etEventLinkZoom.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLinkGoogleMeet.getText())) {
            eventDefinition.setEvent_link_googlemeet(binding.etEventLinkGoogleMeet.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLinkGoogleClassroom.getText())) {
            eventDefinition.setEvent_link_googleclassroom(binding.etEventLinkGoogleClassroom.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLinkFacebook.getText())) {
            eventDefinition.setEvent_link_facebook(binding.etEventLinkFacebook.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLinkWeb.getText())) {
            eventDefinition.setEvent_link_on_web(binding.etEventLinkWeb.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLinkTwitter.getText())) {
            eventDefinition.setEvent_link_twitter(binding.etEventLinkTwitter.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLinkWhatsapp.getText())) {
            eventDefinition.setEvent_link_whatsapp(binding.etEventLinkWhatsapp.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLinkOther.getText())) {
            eventDefinition.setEvent_link_other_media(binding.etEventLinkOther.getText().toString());
        }
        if (mediaDefinitions.size() > 0) {
            Log.d("EventDefination", new Gson().toJson(mediaDefinitions));
            eventDefinition.setEvent_link_array(new Gson().toJson(mediaDefinitions));
        }
        return true;
    }*/
    private class MediaAdapter extends RecyclerView.Adapter<MediaAdapter.MediaHolder> {

        @NonNull
        @Override
        public MediaAdapter.MediaHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new MediaAdapter.MediaHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_media_link, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MediaAdapter.MediaHolder mediaHolder, int i) {

            MediaDefinition mediaDefinition = mediaDefinitions.get(i);
            if (mediaDefinition != null) {
                mediaHolder.llMain.setVisibility(View.GONE);
                mediaHolder.llZoom.setVisibility(View.GONE);
                mediaHolder.llYoutube.setVisibility(View.GONE);
                mediaHolder.llFacebook.setVisibility(View.GONE);
                mediaHolder.llGoogleMeet.setVisibility(View.GONE);
                mediaHolder.llGoogleClassroom.setVisibility(View.GONE);
                mediaHolder.llSkype.setVisibility(View.GONE);
                mediaHolder.llPDF.setVisibility(View.GONE);
                mediaHolder.llPowerPoint.setVisibility(View.GONE);
                mediaHolder.llSlideshow.setVisibility(View.GONE);
                mediaHolder.llTeleConference.setVisibility(View.GONE);
                mediaHolder.llAudioVideo.setVisibility(View.GONE);
                mediaHolder.llMeetingRoom.setVisibility(View.GONE);
                mediaHolder.tvMediaPagePin.setVisibility(View.GONE);
                mediaHolder.tvMediaShortDescr.setVisibility(View.GONE);

                if (!TextUtils.isEmpty(mediaDefinition.getMedia_platform())) {
                    mediaHolder.tvPlatform.setText(mediaDefinition.getMedia_platform());
                    if (mediaDefinition.getMedia_platform().equals("Zoom")) {
                        mediaHolder.llZoom.setVisibility(View.VISIBLE);
                        if (mediaDefinition.getZoomSpecialUrl() != null
                                && !TextUtils.isEmpty(mediaDefinition.getZoomSpecialUrl())) {
                            mediaHolder.tvZoomMeetingId.setText("Meeting URL: " + mediaDefinition.getZoomSpecialUrl());
                        } else {
                            mediaHolder.tvYoutubeTitle.setText("Not Defined");
                        }
                        if (mediaDefinition.getZoomMeetingId() != null
                                && !TextUtils.isEmpty(mediaDefinition.getZoomMeetingId())) {
                            mediaHolder.tvZoomMeetingId.setText(mediaHolder.tvZoomMeetingId.getText() + "\n\nMeeting Id: " + mediaDefinition.getZoomMeetingId());
                        } else {
                            mediaHolder.tvZoomMeetingId.setText(mediaHolder.tvZoomMeetingId.getText() + "\nMeeting Id: " + "Not Defined");
                        }
                        if (mediaDefinition.getZoomPassword() != null
                                && !TextUtils.isEmpty(mediaDefinition.getZoomPassword())) {
                            try {
                                mediaHolder.tvZoomMeetingId.setText(mediaHolder.tvZoomMeetingId.getText() + "\n\nPassword: " + AESEncyption.decrypt(mediaDefinition.getZoomPassword()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            mediaHolder.tvZoomMeetingId.setText(mediaHolder.tvZoomMeetingId.getText() + "\nPassword: " + " Not Defined");
                        }
                        if (mediaDefinition.getZoomPhone() != null
                                && !TextUtils.isEmpty(mediaDefinition.getZoomPhone())) {
                            mediaHolder.tvZoomMeetingId.setText(mediaHolder.tvZoomMeetingId.getText() + "\n\nPhone Number: " + mediaDefinition.getZoomPhone());
                        } else {
                            mediaHolder.tvZoomMeetingId.setText(mediaHolder.tvZoomMeetingId.getText() + "\nPhone Number: Not Defined");
                        }

                    } else if (mediaDefinition.getMedia_platform().equals("YouTube")) {
                        mediaHolder.llYoutube.setVisibility(View.VISIBLE);
                        if (mediaDefinition.getYoutubeStreamTitle() != null
                                && !TextUtils.isEmpty(mediaDefinition.getYoutubeStreamTitle())) {
                            mediaHolder.tvYoutubeTitle.setText("Title: " + mediaDefinition.getYoutubeStreamTitle());
                        } else {
                            mediaHolder.tvYoutubeTitle.setText("Title Not Defined");
                        }
                        if (mediaDefinition.getYoutubeStreamUrl() != null
                                && !TextUtils.isEmpty(mediaDefinition.getYoutubeStreamUrl())) {
                            mediaHolder.tvYoutubeTitle.setText(mediaHolder.tvYoutubeTitle.getText() + "\n\nWeb URL: " + mediaDefinition.getYoutubeStreamUrl());
                        } else {
                            mediaHolder.tvYoutubeTitle.setText(mediaHolder.tvYoutubeTitle.getText() + "\n\nWeb Url: " + "URL Not Defined");
                        }
                    } else if (mediaDefinition.getMedia_platform().equals("Facebook")) {
                        mediaHolder.llFacebook.setVisibility(View.VISIBLE);
                        if (mediaDefinition.getFacebookServerUrl() != null
                                && !TextUtils.isEmpty(mediaDefinition.getFacebookServerUrl())) {
                            mediaHolder.tvFacebook.setText("Web URL: " + mediaDefinition.getFacebookServerUrl());
                        } else {
                            mediaHolder.tvFacebook.setText("Not Defined");
                        }
                        if (mediaDefinition.getFacebookStreamKey() != null
                                && !TextUtils.isEmpty(mediaDefinition.getFacebookStreamKey())) {
                            try {
                                mediaHolder.tvFacebook.setText(mediaHolder.tvFacebook.getText() + "\n\nStream key: " + AESEncyption.decrypt(mediaDefinition.getFacebookStreamKey()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            mediaHolder.tvFacebook.setText(mediaHolder.tvFacebook.getText() + "\n\nStream key: Not Defined");
                        }
                    } else if (mediaDefinition.getMedia_platform().equals("Google Meet")) {
                        mediaHolder.llGoogleMeet.setVisibility(View.VISIBLE);
                        if (mediaDefinition.getGoogleMeetUrl() != null
                                && !TextUtils.isEmpty(mediaDefinition.getGoogleMeetUrl())) {
                            mediaHolder.tvGoogleMeet.setText("Meeting URL: " + mediaDefinition.getGoogleMeetUrl());
                        } else {
                            mediaHolder.tvGoogleMeet.setText("Not Defined");
                        }
                        if (mediaDefinition.getGoogleMeetPassword() != null
                                && !TextUtils.isEmpty(mediaDefinition.getGoogleMeetPassword())) {
                            try {
                                mediaHolder.tvGoogleMeet.setText(mediaHolder.tvGoogleMeet.getText() + "\n\nPassword: " + AESEncyption.decrypt(mediaDefinition.getGoogleMeetPassword()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            mediaHolder.tvGoogleMeet.setText(mediaHolder.tvGoogleMeet.getText() + "\n\nPassword: Not Defined");
                        }
                        if (mediaDefinition.getGoogleMeetPhoneNumber() != null
                                && !TextUtils.isEmpty(mediaDefinition.getGoogleMeetPhoneNumber())) {
                            mediaHolder.tvGoogleMeet.setText(mediaHolder.tvGoogleMeet.getText() + "\n\nPhone Number: " + mediaDefinition.getGoogleMeetPhoneNumber());
                        } else {
                            mediaHolder.tvGoogleMeet.setText(mediaHolder.tvGoogleMeet.getText() + "\n\nPhone Number: Not Defined");
                        }
                    } else if (mediaDefinition.getMedia_platform().equals("Google Classroom")) {
                        mediaHolder.llGoogleClassroom.setVisibility(View.VISIBLE);
                        if (mediaDefinition.getGoogleClasstroomCode() != null
                                && !TextUtils.isEmpty(mediaDefinition.getGoogleClasstroomCode())) {
                            mediaHolder.tvGoogleClassroom.setText("Web URL: " + mediaDefinition.getGoogleClasstroomCode());
                        } else {
                            mediaHolder.tvGoogleClassroom.setText("Not Defined");
                        }
                    } else if (mediaDefinition.getMedia_platform().equals("Skype")) {
                        mediaHolder.llSkype.setVisibility(View.VISIBLE);
                        if (mediaDefinition.getSkypeStreamUrl() != null
                                && !TextUtils.isEmpty(mediaDefinition.getSkypeStreamUrl())) {
                            mediaHolder.tvSkype.setText("Web URL: " + mediaDefinition.getSkypeStreamUrl());
                        } else {
                            mediaHolder.tvSkype.setText("Not Defined");
                        }
                    } else if (mediaDefinition.getMedia_platform().equals("PDF")) {
                        mediaHolder.llPDF.setVisibility(View.VISIBLE);
                        if (mediaDefinition.getPdfPassword() != null
                                && !TextUtils.isEmpty(mediaDefinition.getPdfPassword())) {
                            try {
                                mediaHolder.tvPDF.setText("Password: " + AESEncyption.decrypt(mediaDefinition.getPdfPassword()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            mediaHolder.tvPDF.setText("");
                        }
                        if (mediaDefinition.getPdfFilePath() != null
                                && !TextUtils.isEmpty(mediaDefinition.getPdfFilePath())) {
                            mediaHolder.tvPDF.setText(mediaHolder.tvPDF.getText() + "\n\n" + mediaDefinition.getPdfFilePath().substring(mediaDefinition.getPdfFilePath().lastIndexOf("/") + 1));
                        } else {
                            mediaHolder.tvPDF.setText("Not Defined");
                        }
                    } else if (mediaDefinition.getMedia_platform().equals("Powerpoint")) {
                        mediaHolder.llPowerPoint.setVisibility(View.VISIBLE);
                        if (mediaDefinition.getPptFilePath() != null
                                && !TextUtils.isEmpty(mediaDefinition.getPptFilePath())) {
                            mediaHolder.tvPowerPoint.setText(mediaDefinition.getPptFilePath().substring(mediaDefinition.getPptFilePath().lastIndexOf("/") + 1));
                        } else {
                            mediaHolder.tvPowerPoint.setText("Not Defined");
                        }
                    } else if (mediaDefinition.getMedia_platform().equals("Slideshow")) {
                        mediaHolder.llSlideshow.setVisibility(View.VISIBLE);
                        if (mediaDefinition.getFilePath() != null
                                && !TextUtils.isEmpty(mediaDefinition.getFilePath())) {
                            mediaHolder.tvSlideshow.setText(mediaDefinition.getFilePath().substring(mediaDefinition.getFilePath().lastIndexOf("/") + 1));
                        } else {
                            mediaHolder.tvSlideshow.setText("Not Defined");
                        }
                    } else if (mediaDefinition.getMedia_platform().equals("TeleConference")) {
                        mediaHolder.llTeleConference.setVisibility(View.VISIBLE);
                        if (mediaDefinition.getTeleConfMeetingId() != null
                                && !TextUtils.isEmpty(mediaDefinition.getTeleConfMeetingId())) {
                            mediaHolder.tvTeleConference.setText("Meeting Id: " + mediaDefinition.getTeleConfMeetingId());
                        }
                        if (mediaDefinition.getTeleConfMeetingPassword() != null
                                && !TextUtils.isEmpty(mediaDefinition.getTeleConfMeetingPassword())) {
                            try {
                                mediaHolder.tvTeleConference.setText(mediaHolder.tvTeleConference.getText() + "\n\nMeeting Password: " + AESEncyption.decrypt(mediaDefinition.getTeleConfMeetingPassword()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (mediaDefinition.getTeleConfNumbers() != null
                                && mediaDefinition.getTeleConfNumbers().size() > 0) {
                            String str = "";
                            for (int j = 0; j < mediaDefinition.getTeleConfNumbers().size(); j++) {
                                str += "+" + mediaDefinition.getTeleConfCountryCode() + " " + mediaDefinition.getTeleConfNumbers().get(j) + "\n";
                            }
                            mediaHolder.tvTeleConference.setText(mediaHolder.tvTeleConference.getText() + "\n\nConfernece Number: \n" + str);
                        } else {
                            mediaHolder.tvTeleConference.setText("Not Defined");
                        }
                    } else if (mediaDefinition.getMedia_platform().equals("Audio/Video")) {
                        mediaHolder.llAudioVideo.setVisibility(View.VISIBLE);
                        if (mediaDefinition.getAudioVideoPath() != null
                                && !TextUtils.isEmpty(mediaDefinition.getAudioVideoPath())) {
                            mediaHolder.tvAudioVideo.setText(mediaDefinition.getAudioVideoPath().substring(mediaDefinition.getAudioVideoPath().lastIndexOf("/") + 1));
                        } else {
                            mediaHolder.tvAudioVideo.setText("Not Defined");
                        }
                    } else if (mediaDefinition.getMedia_platform().equals("Audio Meeting Room")) {
                        mediaHolder.llMeetingRoom.setVisibility(View.VISIBLE);
                        if (mediaDefinition.getMeeting_id() != null
                                && !TextUtils.isEmpty(mediaDefinition.getMeeting_id())) {
                            mediaHolder.tvMeetingId.setText("Meeting Id: " + mediaDefinition.getMeeting_id());
                        }
                    } else if (mediaDefinition.getMedia_platform().equals("Video Meeting Room")) {
                        mediaHolder.llMeetingRoom.setVisibility(View.VISIBLE);
                        if (mediaDefinition.getMeeting_id() != null
                                && !TextUtils.isEmpty(mediaDefinition.getMeeting_id())) {
                            mediaHolder.tvMeetingId.setText("Meeting Id: " + mediaDefinition.getMeeting_id());
                        }
                    } else {
                        if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                            mediaHolder.llMain.setVisibility(View.VISIBLE);
                            mediaHolder.tvPlatformUrl.setText(mediaDefinition.getMedia_domain_url());
                        }
                    }

                    if (mediaDefinition.getMedia_short_description() != null
                            && !TextUtils.isEmpty(mediaDefinition.getMedia_short_description())) {
                        mediaHolder.tvMediaShortDescr.setVisibility(View.VISIBLE);
                        mediaHolder.tvMediaShortDescr.setText("Media Description:\n" + mediaDefinition.getMedia_short_description());
                    }
                    if (mediaDefinition.getMedia_page_pin() != null
                            && !TextUtils.isEmpty(mediaDefinition.getMedia_page_pin())) {
                        mediaHolder.tvMediaPagePin.setVisibility(View.VISIBLE);
                        try {
                            mediaHolder.tvMediaPagePin.setText("Media Password:  " + AESEncyption.decrypt(mediaDefinition.getMedia_page_pin()));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        @Override
        public int getItemCount() {
            return mediaDefinitions.size();
        }

        public class MediaHolder extends RecyclerView.ViewHolder {
            TextView tvPlatform, tvPlatformUrl;
            TextView tvYoutubeTitle, tvBtnYoutubePreview, tvZoomMeetingId;
            TextView tvFacebook, tvGoogleMeet, tvGoogleClassroom, tvSkype,
                    tvSlideshow, tvPowerPoint, tvPDF, tvTeleConference,
                    tvAudioVideo, tvMeetingId;
            TextView tvBtnZoomPreview, tvBtnFacebookPreview, tvBtnGoogleMeetPreview, tvBtnGoogleClassroomPreview,
                    tvBtnSkypePreview, tvBtnSlideshowPreview, tvBtnPowerPointPreview,
                    tvBtnPDFPreview, tvBtnTeleConferencePreview, tvBtnAudioVideoPreview, tvMediaShortDescr, tvMediaPagePin;
            ImageView ivEditLink;
            CardView cvMain;
            LinearLayout llMain, llYoutube, llZoom, llFacebook, llGoogleMeet,
                    llGoogleClassroom, llSkype, llSlideshow, llPowerPoint,
                    llPDF, llTeleConference, llAudioVideo, llMeetingRoom;

            public MediaHolder(@NonNull View itemView) {
                super(itemView);
                tvPlatform = itemView.findViewById(R.id.tvPlatform);
                tvPlatformUrl = itemView.findViewById(R.id.tvPlatformUrl);
                ivEditLink = itemView.findViewById(R.id.ivEditLink);
                cvMain = itemView.findViewById(R.id.cvMain);
                tvYoutubeTitle = itemView.findViewById(R.id.tvYoutubeTitle);
                tvBtnYoutubePreview = itemView.findViewById(R.id.tvBtnYoutubePreview);
                tvZoomMeetingId = itemView.findViewById(R.id.tvZoomMeetingId);
                llMain = itemView.findViewById(R.id.llMain);
                llYoutube = itemView.findViewById(R.id.llYoutube);
                llZoom = itemView.findViewById(R.id.llZoom);
                llFacebook = itemView.findViewById(R.id.llFacebook);
                llGoogleMeet = itemView.findViewById(R.id.llGoogleMeet);
                llGoogleClassroom = itemView.findViewById(R.id.llGoogleClassroom);
                llSkype = itemView.findViewById(R.id.llSkype);
                llSlideshow = itemView.findViewById(R.id.llSlideshow);
                llPowerPoint = itemView.findViewById(R.id.llPowerPoint);
                llPDF = itemView.findViewById(R.id.llPDF);
                llTeleConference = itemView.findViewById(R.id.llTeleConference);
                llAudioVideo = itemView.findViewById(R.id.llAudioVideo);
                llMeetingRoom = itemView.findViewById(R.id.llMeetingRoom);

                tvFacebook = itemView.findViewById(R.id.tvFacebook);
                tvGoogleMeet = itemView.findViewById(R.id.tvGoogleMeet);
                tvGoogleClassroom = itemView.findViewById(R.id.tvGoogleClassroom);
                tvSkype = itemView.findViewById(R.id.tvSkype);
                tvSlideshow = itemView.findViewById(R.id.tvSlideshow);
                tvPowerPoint = itemView.findViewById(R.id.tvPowerPoint);
                tvPDF = itemView.findViewById(R.id.tvPDF);
                tvTeleConference = itemView.findViewById(R.id.tvTeleConference);
                tvAudioVideo = itemView.findViewById(R.id.tvAudioVideo);
                tvMeetingId = itemView.findViewById(R.id.tvMeetingId);
                tvBtnZoomPreview = itemView.findViewById(R.id.tvBtnZoomPreview);
                tvBtnFacebookPreview = itemView.findViewById(R.id.tvBtnFacebookPreview);
                tvBtnGoogleMeetPreview = itemView.findViewById(R.id.tvBtnGoogleMeetPreview);
                tvBtnGoogleClassroomPreview = itemView.findViewById(R.id.tvBtnGoogleClassroomPreview);
                tvBtnSkypePreview = itemView.findViewById(R.id.tvBtnSkypePreview);
                tvBtnSlideshowPreview = itemView.findViewById(R.id.tvBtnSlideshowPreview);
                tvBtnPowerPointPreview = itemView.findViewById(R.id.tvBtnPowerPointPreview);
                tvBtnPDFPreview = itemView.findViewById(R.id.tvBtnPDFPreview);
                tvBtnTeleConferencePreview = itemView.findViewById(R.id.tvBtnTeleConferencePreview);
                tvBtnAudioVideoPreview = itemView.findViewById(R.id.tvBtnAudioVideoPreview);
                tvMediaShortDescr = itemView.findViewById(R.id.tvMediaShortDscr);
                tvMediaPagePin = itemView.findViewById(R.id.tvMediaPagePin);
                ivEditLink.setOnClickListener(v -> {
                    try {
                        pos = getAdapterPosition();
                        showMediaTypePupup(true, getAdapterPosition()); //showDialogMediaType();
//                        showDialogAddMediaLink(true, getAdapterPosition());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

                tvBtnYoutubePreview.setOnClickListener(view -> {
                    if (mediaDefinitions.get(getAdapterPosition()).getYoutubeStreamUrl() != null
                            && !TextUtils.isEmpty(mediaDefinitions.get(getAdapterPosition()).getYoutubeStreamUrl())) {
                        String id, url = mediaDefinitions.get(getAdapterPosition()).getYoutubeStreamUrl();
                        if (url.contains("=")) {
                            String ary[] = url.split("=");
                            id = ary[ary.length - 1];
                        } else {
                            String ary[] = url.split("/");
                            id = ary[ary.length - 1];
                        }
                        Log.d("YouTube", "id: " + id);
                        Intent intent = YouTubeIntents.createPlayVideoIntentWithOptions(getActivity(), id, true, false);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getActivity(), "URL not found!", Toast.LENGTH_SHORT).show();
                    }
                });

                tvBtnZoomPreview.setOnClickListener(view -> {
                    openZoomUrlInBrowser(mediaDefinitions.get(getAdapterPosition()));
//                    openWebUrl(mediaDefinitions.get(getAdapterPosition()).getZoomSpecialUrl());
                });

                tvBtnFacebookPreview.setOnClickListener(view -> {
                    openWebUrl(mediaDefinitions.get(getAdapterPosition()).getFacebookServerUrl());
                });

                tvBtnGoogleMeetPreview.setOnClickListener(view -> {
                    if (mediaDefinitions.get(getAdapterPosition()).getGoogleMeetUrl() != null
                            && !TextUtils.isEmpty(mediaDefinitions.get(getAdapterPosition()).getGoogleMeetUrl())) {
                        openWebUrl(mediaDefinitions.get(getAdapterPosition()).getGoogleMeetUrl());
                    }
                });

                tvBtnGoogleClassroomPreview.setOnClickListener(view -> {
                    openWebUrl("http://www.classroom.google.com");
                });

                tvBtnSkypePreview.setOnClickListener(view -> {
                    if (mediaDefinitions.get(getAdapterPosition()).getSkypeStreamUrl() != null
                            && !TextUtils.isEmpty(mediaDefinitions.get(getAdapterPosition()).getSkypeStreamUrl())) {
                        openWebUrl(mediaDefinitions.get(getAdapterPosition()).getSkypeStreamUrl());
                    }
                });

                tvBtnSlideshowPreview.setOnClickListener(view -> {
                    if (mediaDefinitions.get(getAdapterPosition()).getFilePath() != null
                            && !TextUtils.isEmpty(mediaDefinitions.get(getAdapterPosition()).getFilePath())) {
                        downloadFile(mediaDefinitions.get(getAdapterPosition()).getFilePath());
                    }
                });
                tvBtnPowerPointPreview.setOnClickListener(view -> {
                    if (mediaDefinitions.get(getAdapterPosition()).getPptFilePath() != null
                            && !TextUtils.isEmpty(mediaDefinitions.get(getAdapterPosition()).getPptFilePath())) {
                        downloadFile(mediaDefinitions.get(getAdapterPosition()).getPptFilePath());
                    }
                });
                tvBtnPDFPreview.setOnClickListener(view -> {
                    if (mediaDefinitions.get(getAdapterPosition()).getPdfFilePath() != null
                            && !TextUtils.isEmpty(mediaDefinitions.get(getAdapterPosition()).getPdfFilePath())) {
                        PDFViewFragment pdfViewFragment = new PDFViewFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("mediaDefinition", new Gson().toJson(mediaDefinitions.get(getAdapterPosition())));
                        pdfViewFragment.setArguments(bundle);
                        replaceFragment(pdfViewFragment, "pdfView");
                    }
                });
                tvBtnAudioVideoPreview.setOnClickListener(view -> {
                    if (mediaDefinitions.get(getAdapterPosition()).getAudioVideoPath() != null
                            && !TextUtils.isEmpty(mediaDefinitions.get(getAdapterPosition()).getAudioVideoPath())) {
                        String path = mediaDefinitions.get(getAdapterPosition()).getAudioVideoPath();
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);//DO NOT FORGET THIS EVER

                        if (path.endsWith(".mp3")) {
                            intent.setDataAndType(Uri.parse(path), "audio/*");
                        } else if (path.endsWith(".mp4")) {
                            intent.setDataAndType(Uri.parse(path), "video/*");
                        }
                        startActivity(intent);
                    }
                });
            }
        }
    }

    private void downloadFile(String url) {
        progressDialog.setMessage("Downloading file...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        storageDir = Environment.getExternalStorageDirectory().getAbsolutePath();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
            storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
        File file = new File(storageDir + "/parkezly/");
        if (!file.exists()) {
            file.mkdir();
        }
        Log.e("#DEBUG", "  FileName:  " + "test." + url.substring(url.lastIndexOf(".") + 1));
        AndroidNetworking.download(url, file.getAbsolutePath(), "test." + url.substring(url.lastIndexOf(".") + 1))
                .build()
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        progressDialog.dismiss();
                        try {
                            File file = new File(storageDir + "/parkezly/test.ppt");
                            Uri uri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", file);
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "Sorry, Unable to open file!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Sorry, Unable to open file!", Toast.LENGTH_SHORT).show();
                        Log.e("#DEBUG", "  downloadFile: onError: " + anError.getMessage());
                    }
                });
    }

    private void openWebUrl(String url) {
        Intent browserIntent;
        if (url.contains("www") || url.contains("http") || url.contains("https")) {
            Log.d("#DEBUG", "   if");
            browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        } else {
            Log.d("#DEBUG", "   Else");
            browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://" + url.trim()));
        }
        try {
            browserIntent.setPackage("com.android.chrome");
            getActivity().startActivity(browserIntent);
        } catch (Exception e) {
            Toast.makeText(myApp, "There is something went to wrong", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void openZoomUrlInBrowser(MediaDefinition mediaDefinition) {
        String pass = "";
        try {
            pass = AESEncyption.decrypt(mediaDefinition.getZoomPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String url = "https://zoom.us/join?confno=" + mediaDefinition.getZoomMeetingId() + "&pwd=" + pass;
        Log.e("#DEBUG", "   openZoomUrlInBrowser:  Url: " + url);
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class TeleConfNumberAdapter extends RecyclerView.Adapter<TeleConfNumberAdapter.NumberHolder> {

        @NonNull
        @Override
        public TeleConfNumberAdapter.NumberHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new TeleConfNumberAdapter.NumberHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_tele_number, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull TeleConfNumberAdapter.NumberHolder holder, int position) {
            holder.etTeleConfNumber.setText(teleNumbers.get(position));
        }

        @Override
        public int getItemCount() {
            return teleNumbers.size();
        }

        public class NumberHolder extends RecyclerView.ViewHolder {
            EditText etTeleConfNumber;
            ImageView ivBtnRemove;

            public NumberHolder(@NonNull View itemView) {
                super(itemView);
                etTeleConfNumber = itemView.findViewById(R.id.etTeleConfNumber);
                ivBtnRemove = itemView.findViewById(R.id.ivBtnRemove);

                ivBtnRemove.setOnClickListener(view -> {
                    if (teleNumbers.size() == 1) {
                        Toast.makeText(getActivity(), "At least one number is required!", Toast.LENGTH_SHORT).show();
                    } else {
                        teleNumbers.remove(getAdapterPosition());
                        notifyItemRemoved(getAdapterPosition());
                    }
                });

                etTeleConfNumber.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if (!TextUtils.isEmpty(charSequence)) {
                            teleNumbers.set(getAdapterPosition(), charSequence.toString());
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
            }
        }
    }

    /*private void showDialogAddMediaLink(boolean isEdit, int pos) {
        MediaDefinition mediaDefinition;
        if (pos == -1) {
            mediaDefinition = new MediaDefinition();
        } else {
            mediaDefinition = mediaDefinitions.get(pos);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogAddEditMediaLinkBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_add_edit_media_link, null, false);
        builder.setView(binding.getRoot());


        *//*ArrayAdapter<MediaDefinition> categoryAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, mediaDefinitionsMain);
        binding.spinnerPlatfrom.setAdapter(categoryAdapter);*//*

        if (isEdit) {
            binding.btnAdd.setText("UPDATE");
            binding.tvTitle.setText("Edit Event Media Link");
            binding.btnDelete.setVisibility(View.VISIBLE);
//            binding.spinnerPlatfrom.setSelection(mediaDefinitions.indexOf(mediaDefinition));

//            binding.etEventLink.setText(mediaDefinition.getMedia_event_link());
//            binding.etApiKey.setText(TextUtils.isEmpty(mediaDefinition.getMedia_app_key()) ?
//                    "" : mediaDefinition.getMedia_app_key());
        } else {
            binding.btnAdd.setText("ADD");
            binding.btnDelete.setVisibility(View.GONE);
            binding.tvTitle.setText("Add Event Media Link");
        }

     *//*   binding.spinnerPlatfrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                binding.tvPlatfromUrl.setText(mediaDefinitionsMain.get(position).getMedia_domain_url());
                if (isEdit) {
                    mediaDefinitions.get(pos).setMedia_platform(mediaDefinitionsMain.get(position).getMedia_platform());
                    mediaDefinitions.get(pos).setMedia_domain_url(mediaDefinitionsMain.get(position).getMedia_domain_url());
                } else {
                    mediaDefinition.setMedia_platform(mediaDefinitionsMain.get(position).getMedia_platform());
                    mediaDefinition.setMedia_domain_url(mediaDefinitionsMain.get(position).getMedia_domain_url());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*//*

        AlertDialog alertDialog = builder.create();

        binding.btnAdd.setOnClickListener(v -> {
//            if (TextUtils.isEmpty(binding.etEventLink.getText().toString())) {
//                Toast.makeText(getActivity(), "Please Enter URL!", Toast.LENGTH_SHORT).show();
//                return;
//            }
//            if (!TextUtils.isEmpty(binding.etApiKey.getText().toString())) {
//                if (isEdit) {
//                    mediaDefinitions.get(pos).setMedia_app_key(binding.etApiKey.getText().toString());
//                } else {
//                    mediaDefinition.setMedia_app_key(binding.etApiKey.getText().toString());
//                }
//            } else {
//                if (isEdit) {
//                    mediaDefinitions.get(pos).setMedia_app_key("");
//                } else {
//                    mediaDefinition.setMedia_app_key("");
//                }
//            }
            if (isEdit) {
//                mediaDefinitions.get(pos).setMedia_event_link(binding.etEventLink.getText().toString());
                mediaAdapter.notifyItemChanged(pos);
            } else {
//                mediaDefinition.setMedia_event_link(binding.etEventLink.getText().toString());
                mediaDefinition.setId(mediaDefinitions.size() + 1);
                mediaDefinitions.add(mediaDefinition);
                mediaAdapter.notifyDataSetChanged();
            }
            alertDialog.dismiss();
        });

        binding.btnDelete.setOnClickListener(v -> {
            alertDialog.dismiss();
            mediaDefinitions.remove(pos);
            mediaAdapter.notifyItemRemoved(pos);
        });


        alertDialog.show();

    }*/
    /*private class MediaAdapter extends RecyclerView.Adapter<MediaAdapter.MediaHolder> {

        @NonNull
        @Override
        public MediaAdapter.MediaHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new MediaAdapter.MediaHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_media_link, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MediaAdapter.MediaHolder mediaHolder, int i) {

            MediaDefinition mediaDefinition = mediaDefinitions.get(i);
            if (mediaDefinition != null) {
                if (!TextUtils.isEmpty(mediaDefinition.getMedia_platform())) {
                    mediaHolder.tvPlatform.setText(mediaDefinition.getMedia_platform());
                }
                if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                    mediaHolder.tvPlatformUrl.setText(mediaDefinition.getMedia_domain_url());
                }
            }

        }

        @Override
        public int getItemCount() {
            return mediaDefinitions.size();
        }

        public class MediaHolder extends RecyclerView.ViewHolder {
            TextView tvPlatform, tvPlatformUrl;
            ImageView ivEditLink;

            public MediaHolder(@NonNull View itemView) {
                super(itemView);
                tvPlatform = itemView.findViewById(R.id.tvPlatform);
                tvPlatformUrl = itemView.findViewById(R.id.tvPlatformUrl);
                ivEditLink = itemView.findViewById(R.id.ivEditLink);

                ivEditLink.setOnClickListener(v -> showDialogAddMediaLink(true, getAdapterPosition()));
            }
        }
    }*/
}
