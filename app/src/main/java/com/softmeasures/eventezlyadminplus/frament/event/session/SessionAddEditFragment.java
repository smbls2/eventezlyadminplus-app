package com.softmeasures.eventezlyadminplus.frament.event.session;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.databinding.DataBindingUtil;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.nileshp.multiphotopicker.photopicker.activity.PickImageActivity;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.compressor.Compressor;
import com.softmeasures.eventezlyadminplus.databinding.FragSessionAddEditBinding;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.EventSession;
import com.softmeasures.eventezlyadminplus.models.Manager;
import com.softmeasures.eventezlyadminplus.models.Upload;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class SessionAddEditFragment extends BaseFragment {

    private static final int REQUEST_PICK_IMAGE = 888;
    private FragSessionAddEditBinding binding;
    private boolean isEdit = false;
    private EventDefinition eventDefinition;
    private EventSession eventSession;
    private Manager selectedManager;
    private ProgressDialog progressDialog;
    private String imageOption = "";
    private StringBuilder eventDates = new StringBuilder();
    private StringBuilder eventTiming = new StringBuilder();
    private StringBuilder parkingTiming = new StringBuilder();
    private String selectedEventStartTime = "", selectedEventEndTime = "";
    private boolean isEventLogoChange = false, isEventImage1Change = false, isEventImage2Change = false;
    AmazonS3 s3;
    TransferUtility transferUtility;
    private ArrayList<Upload> imageUploads = new ArrayList<>();
    public static ArrayList<String> selectedLocations = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            isEdit = getArguments().getBoolean("isEdit", false);
            selectedManager = new Gson().fromJson(getArguments().getString("selectedManager"), Manager.class);
            eventDefinition = new Gson().fromJson(getArguments().getString("eventDetails"), EventDefinition.class);
            eventSession = new Gson().fromJson(getArguments().getString("eventSessionDetails"), EventSession.class);
            if (eventDefinition != null) {
                Log.e("#DEBUG", "   EventSessionAddEditFragment:  eventDetails:  " + new Gson().toJson(eventDefinition));
            }
            if (eventSession != null) {
                Log.e("#DEBUG", "   EventSessionAddEditFragment:  eventSessionDetails:  " + new Gson().toJson(eventSession));
            }
            if (selectedManager != null) {
                Log.e("#DEBUG", "   EventSessionAddEditFragment:  selectedManager:  " + new Gson().toJson(selectedManager));
            }
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_session_add_edit, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        s3 = new AmazonS3Client(new BasicAWSCredentials("AKIAJNG22KFRVUAICSEA", "NSrQR/yAg52RPD3kp3FMb3NO+Vrxuty4iAwPU4th"));
        transferUtility = new TransferUtility(s3, getActivity().getApplicationContext());

        initViews(view);
        updateViews();
        setListeners();
    }

    public class updateEventDetails extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String exit_status = "_table/event_session_definitions";
        JSONObject json, json1;
        String re_id;
        String id;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues = new HashMap<String, Object>();

            if (isEdit) {
                jsonValues.put("id", eventSession.getId());
            }
            jsonValues.put("date_time", DateTimeUtils.getSqlFormatDate(Calendar.getInstance().getTime()));
            jsonValues.put("manager_type", eventSession.getManager_type());
            jsonValues.put("manager_type_id", eventSession.getManager_type_id());
            jsonValues.put("twp_id", eventSession.getTwp_id());
            jsonValues.put("township_code", eventSession.getTownship_code());
            jsonValues.put("township_name", eventSession.getTownship_name());
            jsonValues.put("company_id", eventSession.getCompany_id());
            jsonValues.put("company_code", eventSession.getCompany_code());
            jsonValues.put("company_name", eventSession.getCompany_name());
            jsonValues.put("location_address", eventSession.getLocation_address());
            jsonValues.put("covered_locations", eventSession.getCovered_locations());

            jsonValues.put("event_id", eventSession.getEvent_id());
            jsonValues.put("event_type", eventSession.getEvent_type());
            jsonValues.put("event_name", eventSession.getEvent_name());
            jsonValues.put("event_session_type", eventSession.getEvent_session_type());
            jsonValues.put("event_session_name", eventSession.getEvent_session_name());
            jsonValues.put("event_session_short_description", eventSession.getEvent_session_short_description());
            jsonValues.put("event_session_long_description", eventSession.getEvent_session_long_description());
            jsonValues.put("event_session_link_on_web", eventSession.getEvent_session_link_on_web());
            jsonValues.put("event_session_link_twitter", eventSession.getEvent_session_link_twitter());
            jsonValues.put("event_session_link_whatsapp", eventSession.getEvent_session_link_whatsapp());
            jsonValues.put("event_session_link_other_media", eventSession.getEvent_session_link_other_media());
            jsonValues.put("company_logo", eventSession.getCompany_logo());
            jsonValues.put("event_logo", eventSession.getEvent_logo());
            jsonValues.put("event_session_logo", eventSession.getEvent_session_logo());
            jsonValues.put("event_session_image1", eventSession.getEvent_session_image1());
            jsonValues.put("event_session_image2", eventSession.getEvent_session_image2());
            jsonValues.put("event_session_address", eventSession.getEvent_session_address());
            jsonValues.put("session_regn_needed_approval", eventSession.isSession_regn_needed_approval());
            jsonValues.put("requirements", eventSession.getRequirements());
            jsonValues.put("cost", eventSession.getCost());
            jsonValues.put("year", eventSession.getYear());
            jsonValues.put("event_session_begins_date_time", eventSession.getEvent_session_begins_date_time());
            jsonValues.put("event_session_ends_date_time", eventSession.getEvent_session_ends_date_time());
            jsonValues.put("event_session_parking_begins_date_time", eventSession.getEvent_session_parking_begins_date_time());
            jsonValues.put("event_session_parking_ends_date_time", eventSession.getEvent_session_parking_ends_date_time());
            jsonValues.put("event_session_multi_dates", eventSession.getEvent_session_multi_dates());
            jsonValues.put("event_session_parking_timings", eventSession.getEvent_session_parking_timings());
            jsonValues.put("event_session_timings", eventSession.getEvent_session_timings());
            jsonValues.put("expires_by", eventSession.getExpires_by());
            jsonValues.put("regn_reqd", eventSession.isRegn_reqd());
            jsonValues.put("event_session_regn_allowed", eventSession.isEvent_session_regn_allowed());
            jsonValues.put("event_session_regn_fee", eventSession.getEvent_session_regn_fee());
            jsonValues.put("event_session_led_by", eventSession.getEvent_session_led_by());
            jsonValues.put("event_session_leaders_bio", eventSession.getEvent_session_leaders_bio());
            jsonValues.put("regn_reqd_for_parking", eventSession.isRegn_reqd_for_parking());
            jsonValues.put("renewable", eventSession.isRenewable());
            jsonValues.put("active", eventSession.isActive());
            jsonValues.put("free_session", eventSession.isFree_session());
            jsonValues.put("free_session_parking", eventSession.isFree_session_parking());


            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpResponse response = null;
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (isEdit) {
                HttpPut post = new HttpPut(url);
                post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
                post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
                post.setEntity(entity);
                Log.e("#DEBUG", "  updateURL:  " + url);
                Log.e("#DEBUG", "   update Params: " + String.valueOf(json));
                try {
                    response = client.execute(post);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                HttpPost post = new HttpPost(url);
                post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
                post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
                post.setEntity(entity);
                Log.e("#DEBUG", "  updateURL:  " + url);
                Log.e("#DEBUG", "   update Params: " + String.valueOf(json));
                try {
                    response = client.execute(post);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   Add/Edit Event:  Response:  " + responseStr);
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.e("#DEBUG", "  update: ResponseID: " + re_id);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.hide();
            if (getActivity() != null && json1 != null) {
                if (re_id != null && !re_id.equals("null")) {
                    if (isEdit) {
                        SessionsFragment.isSessionUpdated = true;
                        Toast.makeText(getActivity(), "Updated!", Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();
                    } else {
                        new updateEventId().execute(re_id);
                    }
                }
                super.onPostExecute(s);
            }
        }
    }

    public class updateEventId extends AsyncTask<String, String, String> {
        String exit_status = "_table/event_session_definitions";
        JSONObject json, json1;
        String re_id;
        String id;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues = new HashMap<String, Object>();


            jsonValues.put("id", params[0]);
            jsonValues.put("event_session_id", params[0]);

            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpResponse response = null;
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            post.setEntity(entity);
            Log.e("#DEBUG", "  updateURL:  " + url);
            Log.e("#DEBUG", "   update Params: " + String.valueOf(json));
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.e("#DEBUG", "  update: ResponseID: " + re_id);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.hide();
            if (getActivity() != null && json1 != null) {
                if (re_id != null && !re_id.equals("null")) {
                    SessionsFragment.isSessionUpdated = true;
                    Toast.makeText(getActivity(), "Added!", Toast.LENGTH_SHORT).show();
                    getActivity().onBackPressed();
                }
                super.onPostExecute(s);
            }
        }
    }

    @Override
    protected void updateViews() {
        if (isEdit) {
            binding.tvTitle.setText("Edit Event Session Details");
            binding.tvBtnAddEvent.setText("UPDATE");

            if (eventSession != null) {

                if (eventSession.getEvent_session_name() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_name())) {
                    binding.etEventName.setText(eventSession.getEvent_session_name());
                }

                if (eventSession.getEvent_session_type() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_type())) {
                    binding.etEventType.setText(eventSession.getEvent_session_type());
                }

                if (eventSession.getEvent_session_short_description() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_short_description())) {
                    binding.etEventSortDesc.setText(eventSession.getEvent_session_short_description());
                }

                if (eventSession.getEvent_session_long_description() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_long_description())) {
                    binding.etEventLongDecr.setText(eventSession.getEvent_session_long_description());
                }

                if (eventSession.getEvent_session_long_description() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_long_description())) {
                    binding.etEventLongDecr.setText(eventSession.getEvent_session_long_description());
                }

                if (eventSession.getEvent_session_link_on_web() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_on_web())) {
                    binding.etEventLinkWeb.setText(eventSession.getEvent_session_link_on_web());
                }

                if (eventSession.getEvent_session_link_whatsapp() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_whatsapp())) {
                    binding.etEventLinkWhatsapp.setText(eventSession.getEvent_session_link_whatsapp());
                }

                if (eventSession.getEvent_session_link_twitter() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_twitter())) {
                    binding.etEventLinkTwitter.setText(eventSession.getEvent_session_link_twitter());
                }

                if (eventSession.getEvent_session_link_other_media() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_link_other_media())) {
                    binding.etEventLinkOther.setText(eventSession.getEvent_session_link_other_media());
                }

                if (eventSession.getEvent_session_address() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_address())) {
                    binding.etEventAddress.setText(eventSession.getEvent_session_address());
                }

                if (eventSession.getRequirements() != null
                        && !TextUtils.isEmpty(eventSession.getRequirements())) {
                    binding.etRequirements.setText(eventSession.getRequirements());
                }

                if (eventSession.getCost() != null
                        && !TextUtils.isEmpty(eventSession.getCost())) {
                    binding.etEventCost.setText(String.format("%s", eventSession.getCost()));
                }

                if (eventSession.getYear() != null
                        && !TextUtils.isEmpty(eventSession.getYear())) {
                    binding.etEventYear.setText(eventSession.getYear());
                }

                if (eventSession.getEvent_session_begins_date_time() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_begins_date_time())) {
                    binding.etEventBeginsTime.setText(eventSession.getEvent_session_begins_date_time());
                }

                if (eventSession.getEvent_session_ends_date_time() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_ends_date_time())) {
                    binding.etEventEndTime.setText(eventSession.getEvent_session_ends_date_time());
                }

                if (eventSession.getEvent_session_multi_dates() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_multi_dates())) {
                    try {
                        JSONArray jsonArray = new JSONArray(eventSession.getEvent_session_multi_dates());
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONArray jsonArray1 = jsonArray.getJSONArray(i);
                            if (jsonArray1 != null && jsonArray1.length() > 0) {
                                if (i != 0) {
                                    stringBuilder.append(",");
                                }
                                stringBuilder.append(jsonArray1.get(0).toString());
                            }
                        }
                        binding.etEventDates.setText(stringBuilder.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (eventSession.getEvent_session_timings() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_timings())) {
                    try {
                        JSONArray jsonArray = new JSONArray(eventSession.getEvent_session_timings());
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONArray jsonArray1 = jsonArray.getJSONArray(i);
                            if (jsonArray1 != null && jsonArray1.length() > 0) {
                                if (i != 0) {
                                    stringBuilder.append(",");
                                }
                                stringBuilder.append(jsonArray1.get(0).toString());
                            }
                        }
                        binding.etEventTiming.setText(stringBuilder.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (eventSession.getEvent_session_parking_begins_date_time() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_parking_begins_date_time())) {
                    binding.etParkingBeginsTime.setText(eventSession.getEvent_session_parking_begins_date_time());
                }

                if (eventSession.getEvent_session_parking_ends_date_time() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_parking_ends_date_time())) {
                    binding.etParkingEndTime.setText(eventSession.getEvent_session_parking_ends_date_time());
                }

                if (eventSession.getEvent_session_parking_timings() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_parking_timings())) {
                    try {
                        JSONArray jsonArray = new JSONArray(eventSession.getEvent_session_parking_timings());
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONArray jsonArray1 = jsonArray.getJSONArray(i);
                            if (jsonArray1 != null && jsonArray1.length() > 0) {
                                if (i != 0) {
                                    stringBuilder.append(",");
                                }
                                stringBuilder.append(jsonArray1.get(0).toString());
                            }
                        }
                        binding.etParkingTiming.setText(stringBuilder.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (eventSession.getExpires_by() != null
                        && !TextUtils.isEmpty(eventSession.getExpires_by())) {
                    binding.etExpiresBy.setText(eventSession.getExpires_by());
                }

                binding.switchRegReqEvent.setChecked(eventSession.isRegn_reqd());
                binding.switchRegReqParking.setChecked(eventSession.isRegn_reqd_for_parking());
                binding.switchRenewable.setChecked(eventSession.isRenewable());
                binding.switchActive.setChecked(eventSession.isActive());
                binding.switchEventRegNeedApproval.setChecked(eventSession.isSession_regn_needed_approval());
                binding.switchEventRegAllowed.setChecked(eventSession.isEvent_session_regn_allowed());

                if (eventSession.getEvent_session_logo() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_logo())) {
                    binding.rlBtnAddEventLogo.setVisibility(View.GONE);
                    binding.ivEventLogoClose.setVisibility(View.VISIBLE);
                    Glide.with(getActivity()).load(eventSession.getEvent_session_logo()).into(binding.ivEventLogo);
                } else {
                    binding.rlBtnAddEventLogo.setVisibility(View.VISIBLE);
                    binding.ivEventLogoClose.setVisibility(View.GONE);
                }

                if (eventSession.getEvent_session_image1() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_image1())) {
                    binding.rlBtnAddEventImage1.setVisibility(View.GONE);
                    binding.ivEventImage1Close.setVisibility(View.VISIBLE);
                    Glide.with(getActivity()).load(eventSession.getEvent_session_image1()).into(binding.ivEventImage1);
                } else {
                    binding.rlBtnAddEventImage1.setVisibility(View.VISIBLE);
                    binding.ivEventImage1Close.setVisibility(View.GONE);
                }

                if (eventSession.getEvent_session_image2() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_image2())) {
                    binding.rlBtnAddEventImage2.setVisibility(View.GONE);
                    binding.ivEventImage2Close.setVisibility(View.VISIBLE);
                    Glide.with(getActivity()).load(eventSession.getEvent_session_image2()).into(binding.ivEventImage2);
                } else {
                    binding.rlBtnAddEventImage2.setVisibility(View.VISIBLE);
                    binding.ivEventImage2Close.setVisibility(View.GONE);
                }

                if (eventSession.getCovered_locations() != null
                        && !TextUtils.isEmpty(eventSession.getCovered_locations())) {
                    String[] locations = eventSession.getCovered_locations().split(",");
                    selectedLocations.clear();
                    selectedLocations.addAll(Arrays.asList(locations));
                    binding.etCoveredLocations.setText(eventSession.getCovered_locations());
                }

                if (eventSession.getEvent_session_led_by() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_led_by())) {
                    binding.etEventLeadBy.setText(eventSession.getEvent_session_led_by());
                }

                if (eventSession.getEvent_session_leaders_bio() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_leaders_bio())) {
                    binding.etEventLeaderBio.setText(eventSession.getEvent_session_leaders_bio());
                }

                if (eventSession.getEvent_session_regn_fee() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_regn_fee())) {
                    binding.etEventSessionRegFees.setText(eventSession.getEvent_session_regn_fee());
                }

            }

        } else {
            binding.tvTitle.setText("Add Event Session");
            binding.tvBtnAddEvent.setText("ADD");
            eventSession = new EventSession();

            if (eventDefinition != null) {
                if (eventDefinition.getLocation_address() != null) {
                    binding.etEventAddress.setText(eventDefinition.getLocation_address());
                }

                if (eventDefinition.getCovered_locations() != null) {
                    binding.etCoveredLocations.setText(eventDefinition.getCovered_locations());
                }
            }
        }
    }

    @Override
    protected void setListeners() {
        binding.etEventAddress.setOnClickListener(v -> {
            openSelectAddress();
        });

        binding.etEventYear.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            new DatePickerDialog(getActivity(), (view, year, month, dayOfMonth) -> {
                Calendar calendar1 = Calendar.getInstance();
                calendar1.set(Calendar.YEAR, year);
                calendar1.set(Calendar.MONTH, month);
                calendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Date date = calendar1.getTime();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy", Locale.getDefault());
                binding.etEventYear.setText(dateFormat.format(date));

                eventSession.setYear(DateTimeUtils.getSqlFormatDate(date));
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
        });

        binding.etEventBeginsTime.setOnClickListener(v -> {
            showDialogSelectEventBeginsTime();
        });

        binding.etEventEndTime.setOnClickListener(v -> {
            showDialogSelectEventEndTime();
        });

        binding.ivBtnAddEventDates.setOnClickListener(v -> {
            showDialogEventDates();
        });

        binding.etEventDates.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(binding.etEventDates.getText())) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Are you sure you want to clear event dates?");
                builder.setPositiveButton("YES", (dialog, which) -> {
                    dialog.dismiss();
                    binding.etEventDates.setText("");
                    eventDates = new StringBuilder();
                });
                builder.setNegativeButton("NO", (dialog, which) -> {
                    dialog.dismiss();
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }
        });

        binding.ivBtnAddEventTiming.setOnClickListener(v -> {
            showDialogAddEventTiming();
        });

        binding.etEventTiming.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(binding.etEventTiming.getText())) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Are you sure you want to clear event timing?");
                builder.setPositiveButton("YES", (dialog, which) -> {
                    dialog.dismiss();
                    binding.etEventTiming.setText("");
                    eventTiming = new StringBuilder();
                });
                builder.setNegativeButton("NO", (dialog, which) -> {
                    dialog.dismiss();
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }
        });

        binding.ivBtnAddParkingTiming.setOnClickListener(v -> {
            showDialogAddParkingTiming();
        });

        binding.etParkingTiming.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(binding.etParkingTiming.getText())) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Are you sure you want to clear parking timing?");
                builder.setPositiveButton("YES", (dialog, which) -> {
                    dialog.dismiss();
                    binding.etParkingTiming.setText("");
                    parkingTiming = new StringBuilder();
                });
                builder.setNegativeButton("NO", (dialog, which) -> {
                    dialog.dismiss();
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }
        });


        binding.etParkingBeginsTime.setOnClickListener(v -> {
            showDialogParkingBeginsTime();
        });
        binding.etParkingEndTime.setOnClickListener(v -> {
            showDialogParkingEntTime();
        });

        binding.switchRegReqEvent.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setRegn_reqd(isChecked);
        });
        binding.switchRegReqParking.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setRegn_reqd_for_parking(isChecked);
        });
        binding.switchRenewable.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setRenewable(isChecked);
        });
        binding.switchActive.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setActive(isChecked);
        });

        binding.switchEventRegAllowed.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setEvent_session_regn_allowed(isChecked);
        });

        binding.switchEventRegNeedApproval.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventSession.setSession_regn_needed_approval(isChecked);
        });

        binding.rlBtnAddEventLogo.setOnClickListener(v -> {
            imageOption = "EventLogo";
            Intent mIntent = new Intent(getActivity(), PickImageActivity.class);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 1);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
            startActivityForResult(mIntent, REQUEST_PICK_IMAGE);
        });

        binding.ivEventLogoClose.setOnClickListener(v -> {
            binding.ivEventLogo.setImageDrawable(null);
            binding.rlBtnAddEventLogo.setVisibility(View.VISIBLE);
            binding.ivEventLogoClose.setVisibility(View.GONE);
        });

        binding.ivEventImage1Close.setOnClickListener(v -> {
            binding.ivEventImage1.setImageDrawable(null);
            binding.rlBtnAddEventImage1.setVisibility(View.VISIBLE);
            binding.ivEventImage1Close.setVisibility(View.GONE);
        });

        binding.ivEventImage2Close.setOnClickListener(v -> {
            binding.ivEventImage2.setImageDrawable(null);
            binding.rlBtnAddEventImage2.setVisibility(View.VISIBLE);
            binding.ivEventImage2Close.setVisibility(View.GONE);
        });

        binding.rlBtnAddEventImage1.setOnClickListener(v -> {
            imageOption = "EventImage1";

            Intent mIntent = new Intent(getActivity(), PickImageActivity.class);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 1);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
            startActivityForResult(mIntent, REQUEST_PICK_IMAGE);
//            Intent intent = new Intent();
//            intent.setType("image/*");
//            intent.setAction(Intent.ACTION_GET_CONTENT);
//            startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_PICK_IMAGE);
        });

        binding.rlBtnAddEventImage2.setOnClickListener(v -> {
            imageOption = "EventImage2";
            Intent mIntent = new Intent(getActivity(), PickImageActivity.class);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 1);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
            startActivityForResult(mIntent, REQUEST_PICK_IMAGE);
//            Intent intent = new Intent();
//            intent.setType("image/*");
//            intent.setAction(Intent.ACTION_GET_CONTENT);
//            startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_PICK_IMAGE);
        });

        binding.etCoveredLocations.setOnClickListener(v -> {
//            final FragmentTransaction ft = getFragmentManager().beginTransaction();
//            CoveredLocationsFragment frag = new CoveredLocationsFragment();
//            Bundle bundle = new Bundle();
//            bundle.putBoolean("isEdit", false);
//            bundle.putString("selectedManager", new Gson().toJson(selectedManager));
//            frag.setArguments(bundle);
//            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
//            ft.add(R.id.My_Container_1_ID, frag);
//            fragmentStack.lastElement().onPause();
//            ft.hide(fragmentStack.lastElement());
//            fragmentStack.push(frag);
//            ft.commitAllowingStateLoss();
        });

        binding.tvBtnAddEvent.setOnClickListener(v -> {
            if (isValidate()) {
                imageUploads.clear();
                Log.e("#DEBUG", "  isValid:  eventSession:  " + new Gson().toJson(eventSession));
                if (isEventLogoChange && !TextUtils.isEmpty(eventSession.getEvent_session_logo())) {
                    File ImageFile = new File(eventSession.getEvent_session_logo());
                    File compressedImageFile = null;
                    try {
                        compressedImageFile = new Compressor(getActivity()).compressToFile(ImageFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Upload upload = new Upload();
                    upload.setType("Logo");
                    upload.setLink(String.valueOf(compressedImageFile));
                    imageUploads.add(upload);
                }

                if (isEventImage1Change && !TextUtils.isEmpty(eventSession.getEvent_session_image1())) {
                    File ImageFile = new File(eventSession.getEvent_session_image1());
                    File compressedImageFile = null;
                    try {
                        compressedImageFile = new Compressor(getActivity()).compressToFile(ImageFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Upload upload = new Upload();
                    upload.setType("Image1");
                    upload.setLink(String.valueOf(compressedImageFile));
                    imageUploads.add(upload);
                }

                if (isEventImage2Change && !TextUtils.isEmpty(eventSession.getEvent_session_image2())) {
                    File ImageFile = new File(eventSession.getEvent_session_image2());
                    File compressedImageFile = null;
                    try {
                        compressedImageFile = new Compressor(getActivity()).compressToFile(ImageFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Upload upload = new Upload();
                    upload.setType("Image2");
                    upload.setLink(String.valueOf(compressedImageFile));
                    imageUploads.add(upload);
                }

                if (imageUploads.size() > 0) {
                    upload_file();
                } else {
                    new updateEventDetails().execute();
                }
            }
        });
    }

    String final_DivisionImage;
    StringBuffer file_DivisionImage;

    public void upload_file() {
        progressDialog.show();
        file_DivisionImage = new StringBuffer();
        String fileUrl = "";
        fileUrl = imageUploads.get(0).getLink();
        File file = new File(fileUrl);
        String namegsxsax = System.currentTimeMillis() + ".jpg";
        file_DivisionImage.append(namegsxsax + ",");
        namegsxsax = "/images/" + namegsxsax;
        final_DivisionImage = file_DivisionImage.substring(0, file_DivisionImage.length() - 1);
        TransferObserver transferObserver = transferUtility.upload("parkezly-images", namegsxsax, file);
        transferObserverListener(transferObserver);
    }

    public void transferObserverListener(final TransferObserver transferObserver) {
        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.name().equals("COMPLETED")) {
                    if (imageUploads.get(0).getType().equals("Logo")) {
                        eventSession.setEvent_session_logo("https://s3.amazonaws.com/parkezly-images//images/" + final_DivisionImage);
                    } else if (imageUploads.get(0).getType().equals("Image1")) {
                        eventSession.setEvent_session_image1("https://s3.amazonaws.com/parkezly-images//images/" + final_DivisionImage);
                    } else if (imageUploads.get(0).getType().equals("Image2")) {
                        eventSession.setEvent_session_image2("https://s3.amazonaws.com/parkezly-images//images/" + final_DivisionImage);
                    }
                    if (imageUploads.size() > 0) {
                        imageUploads.remove(0);
                        if (imageUploads.size() == 0) {
                            //Upload complete
                            new updateEventDetails().execute();
                        } else {
                            upload_file();
                        }
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                try {
                    int percentage = (int) (bytesCurrent / bytesTotal * 100);
                    Log.e("percentage ", " : " + percentage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", "error");
            }

        });
    }

    private void showDialogAddParkingTiming() {
        selectedEventStartTime = "";
        selectedEventEndTime = "";
        ArrayList<String> timeHours = new ArrayList<>();
        ArrayList<String> timeMinutes = new ArrayList<>();

        timeHours.clear();
        for (int i = 0; i < 2; i++) {
            for (int j = 1; j < 13; j++) {
                if (i == 0) {
                    if (j == 12) {
                        timeHours.add(j + " PM");
                    } else {
                        timeHours.add(j + " AM");
                    }
                } else {
                    timeHours.add(j + " PM");
                }
            }
        }
        timeMinutes.clear();
        timeMinutes.addAll(timeHours);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_time_picker_dialog, null);
        AppCompatSpinner spinnerHour = dialogView.findViewById(R.id.spinnerHour);
        AppCompatSpinner spinnerMinute = dialogView.findViewById(R.id.spinnerMinute);

        ArrayAdapter<String> timeHoursAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, timeHours);
        spinnerHour.setAdapter(timeHoursAdapter);

        ArrayAdapter<String> timeMinutesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, timeMinutes);
        spinnerMinute.setAdapter(timeMinutesAdapter);

        Button btnDone = dialogView.findViewById(R.id.btnDone);

        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();

        spinnerHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedEventStartTime = timeHours.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerMinute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedEventEndTime = timeMinutes.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnDone.setOnClickListener(v -> {
            alertDialog.dismiss();
            if (!TextUtils.isEmpty(parkingTiming.toString())) {
                parkingTiming.append(", ");
            }
            parkingTiming.append(selectedEventStartTime + "-" + selectedEventEndTime);
            binding.etParkingTiming.setText(parkingTiming.toString());
        });


        alertDialog.show();
    }

    private void showDialogAddEventTiming() {
        selectedEventStartTime = "";
        selectedEventEndTime = "";
        ArrayList<String> timeHours = new ArrayList<>();
        ArrayList<String> timeMinutes = new ArrayList<>();

        timeHours.clear();
        for (int i = 0; i < 2; i++) {
            for (int j = 1; j < 13; j++) {
                if (i == 0) {
                    if (j == 12) {
                        timeHours.add(j + " PM");
                    } else {
                        timeHours.add(j + " AM");
                    }
                } else {
                    timeHours.add(j + " PM");
                }
            }
        }
        timeMinutes.clear();
        timeMinutes.addAll(timeHours);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_time_picker_dialog, null);
        AppCompatSpinner spinnerHour = dialogView.findViewById(R.id.spinnerHour);
        AppCompatSpinner spinnerMinute = dialogView.findViewById(R.id.spinnerMinute);

        ArrayAdapter<String> timeHoursAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, timeHours);
        spinnerHour.setAdapter(timeHoursAdapter);

        ArrayAdapter<String> timeMinutesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, timeMinutes);
        spinnerMinute.setAdapter(timeMinutesAdapter);

        Button btnDone = dialogView.findViewById(R.id.btnDone);

        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();

        spinnerHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedEventStartTime = timeHours.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerMinute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedEventEndTime = timeMinutes.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnDone.setOnClickListener(v -> {
            alertDialog.dismiss();
            if (!TextUtils.isEmpty(eventTiming.toString())) {
                eventTiming.append(", ");
            }
            eventTiming.append(selectedEventStartTime + "-" + selectedEventEndTime);
            binding.etEventTiming.setText(eventTiming.toString());
        });


        alertDialog.show();
    }

    private void showDialogEventDates() {
        Calendar calendar = Calendar.getInstance();
        new DatePickerDialog(getActivity(), (view, year, month, dayOfMonth) -> {
            Calendar calendar1 = Calendar.getInstance();
            calendar1.set(Calendar.YEAR, year);
            calendar1.set(Calendar.MONTH, month);
            calendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            if (!TextUtils.isEmpty(eventDates.toString())) {
                eventDates.append(",");
            }
            eventDates.append(dateFormat.format(calendar1.getTime()));
            binding.etEventDates.setText(eventDates.toString());
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();

    }

    private void showDialogParkingEntTime() {
        DatePickerDialog entryDatePickerDialog = new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
            final Calendar calendar1 = Calendar.getInstance();
            calendar1.set(Calendar.YEAR, i);
            calendar1.set(Calendar.MONTH, i1);
            calendar1.set(Calendar.DAY_OF_MONTH, i2);


            new TimePickerDialog(getActivity(), (timePicker, i3, i11) -> {

                Calendar datetime = Calendar.getInstance();
                Calendar calendar = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, i3);
                datetime.set(Calendar.MINUTE, i11);
                if (datetime.getTimeInMillis() >= calendar.getTimeInMillis()) {
                    calendar1.set(Calendar.HOUR_OF_DAY, i3);
                    calendar1.set(Calendar.MINUTE, i11);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault());
                    binding.etParkingEndTime.setText(dateFormat1.format(new Date(calendar1.getTimeInMillis())));
                    eventSession.setEvent_session_parking_ends_date_time(dateFormat.format(new Date(calendar1.getTimeInMillis())));
                } else {
                    Toast.makeText(getActivity(), "Please select future date!", Toast.LENGTH_LONG).show();
                }

            }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE),
                    false).show();
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        entryDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        entryDatePickerDialog.show();
    }

    private void showDialogParkingBeginsTime() {
        DatePickerDialog entryDatePickerDialog = new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
            final Calendar calendar1 = Calendar.getInstance();
            calendar1.set(Calendar.YEAR, i);
            calendar1.set(Calendar.MONTH, i1);
            calendar1.set(Calendar.DAY_OF_MONTH, i2);


            new TimePickerDialog(getActivity(), (timePicker, i3, i11) -> {

                Calendar datetime = Calendar.getInstance();
                Calendar calendar = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, i3);
                datetime.set(Calendar.MINUTE, i11);
                if (datetime.getTimeInMillis() >= calendar.getTimeInMillis()) {
                    calendar1.set(Calendar.HOUR_OF_DAY, i3);
                    calendar1.set(Calendar.MINUTE, i11);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault());
                    binding.etParkingBeginsTime.setText(dateFormat1.format(new Date(calendar1.getTimeInMillis())));
                    eventSession.setEvent_session_parking_begins_date_time(dateFormat.format(new Date(calendar1.getTimeInMillis())));
                } else {
                    Toast.makeText(getActivity(), "Please select future date!", Toast.LENGTH_LONG).show();
                }

            }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE),
                    false).show();
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        entryDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        entryDatePickerDialog.show();
    }

    private void showDialogSelectEventEndTime() {
        DatePickerDialog entryDatePickerDialog = new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
            final Calendar calendar1 = Calendar.getInstance();
            calendar1.set(Calendar.YEAR, i);
            calendar1.set(Calendar.MONTH, i1);
            calendar1.set(Calendar.DAY_OF_MONTH, i2);


            new TimePickerDialog(getActivity(), (timePicker, i3, i11) -> {

                Calendar datetime = Calendar.getInstance();
                Calendar calendar = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, i3);
                datetime.set(Calendar.MINUTE, i11);
                if (datetime.getTimeInMillis() >= calendar.getTimeInMillis()) {
                    calendar1.set(Calendar.HOUR_OF_DAY, i3);
                    calendar1.set(Calendar.MINUTE, i11);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault());
                    binding.etEventEndTime.setText(dateFormat1.format(new Date(calendar1.getTimeInMillis())));
                    eventSession.setEvent_session_ends_date_time(dateFormat.format(new Date(calendar1.getTimeInMillis())));
                } else {
                    Toast.makeText(getActivity(), "Please select future date!", Toast.LENGTH_LONG).show();
                }

            }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE),
                    false).show();
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        entryDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        entryDatePickerDialog.show();
    }

    private void showDialogSelectEventBeginsTime() {
        DatePickerDialog entryDatePickerDialog = new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
            final Calendar calendar1 = Calendar.getInstance();
            calendar1.set(Calendar.YEAR, i);
            calendar1.set(Calendar.MONTH, i1);
            calendar1.set(Calendar.DAY_OF_MONTH, i2);


            new TimePickerDialog(getActivity(), (timePicker, i3, i11) -> {

                Calendar datetime = Calendar.getInstance();
                Calendar calendar = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, i3);
                datetime.set(Calendar.MINUTE, i11);
                if (datetime.getTimeInMillis() >= calendar.getTimeInMillis()) {
                    calendar1.set(Calendar.HOUR_OF_DAY, i3);
                    calendar1.set(Calendar.MINUTE, i11);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault());
                    binding.etEventBeginsTime.setText(dateFormat1.format(new Date(calendar1.getTimeInMillis())));
                    eventSession.setEvent_session_begins_date_time(dateFormat.format(new Date(calendar1.getTimeInMillis())));
                } else {
                    Toast.makeText(getActivity(), "Please select future date!", Toast.LENGTH_LONG).show();
                }

            }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE),
                    false).show();
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        entryDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        entryDatePickerDialog.show();
    }

    private void openSelectAddress() {
//        final FragmentTransaction ft = getFragmentManager().beginTransaction();
//        SelectAddressFragment pay = new SelectAddressFragment();
//        Bundle bundle = new Bundle();
//        bundle.putBoolean("isEvent", true);
//        pay.setArguments(bundle);
//        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
//        ft.add(R.id.My_Container_1_ID, pay);
//        fragmentStack.lastElement().onPause();
//        ft.hide(fragmentStack.lastElement());
//        fragmentStack.push(pay);
//        ft.commitAllowingStateLoss();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PICK_IMAGE && resultCode == RESULT_OK) {

            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                if (imageOption.equals("EventLogo")) {
                    isEventLogoChange = true;
                    binding.rlBtnAddEventLogo.setVisibility(View.GONE);
                    binding.ivEventLogoClose.setVisibility(View.VISIBLE);
                    eventSession.setEvent_session_logo(String.valueOf(pathList.get(0)));
                    Glide.with(getActivity()).load(eventSession.getEvent_session_logo()).into(binding.ivEventLogo);
                } else if (imageOption.equals("EventImage1")) {
                    isEventImage1Change = true;
                    binding.rlBtnAddEventImage1.setVisibility(View.GONE);
                    binding.ivEventImage1Close.setVisibility(View.VISIBLE);
                    eventSession.setEvent_session_image1(String.valueOf(pathList.get(0)));
                    Glide.with(getActivity()).load(eventSession.getEvent_session_image1()).into(binding.ivEventImage1);
                } else if (imageOption.equals("EventImage2")) {
                    isEventImage2Change = true;
                    binding.rlBtnAddEventImage2.setVisibility(View.GONE);
                    binding.ivEventImage2Close.setVisibility(View.VISIBLE);
                    eventSession.setEvent_session_image2(String.valueOf(pathList.get(0)));
                    Glide.with(getActivity()).load(eventSession.getEvent_session_image2()).into(binding.ivEventImage2);
                }
            }
        }
    }

    private boolean isValidate() {
        if (!TextUtils.isEmpty(binding.etEventName.getText())) {
            eventSession.setEvent_session_name(binding.etEventName.getText().toString());
        } else {
            binding.etEventName.setError("Required");
            binding.etEventName.requestFocus();
            return false;
        }
        if (!TextUtils.isEmpty(binding.etEventType.getText())) {
            eventSession.setEvent_session_type(binding.etEventType.getText().toString());
        } else {
            binding.etEventType.setError("Required");
            binding.etEventType.requestFocus();
            return false;
        }
        if (!TextUtils.isEmpty(binding.etEventSortDesc.getText())) {
            eventSession.setEvent_session_short_description(binding.etEventSortDesc.getText().toString());
        } else {
            binding.etEventSortDesc.setError("Required");
            binding.etEventSortDesc.requestFocus();
            return false;
        }
        if (!TextUtils.isEmpty(binding.etEventLongDecr.getText())) {
            eventSession.setEvent_session_long_description(binding.etEventLongDecr.getText().toString());
        } else {
            binding.etEventLongDecr.setError("Required");
            binding.etEventLongDecr.requestFocus();
            return false;
        }
        if (!TextUtils.isEmpty(binding.etEventAddress.getText())) {
            eventSession.setEvent_session_address(binding.etEventAddress.getText().toString());
        } else {
            Toast.makeText(getActivity(), "Please select address!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!TextUtils.isEmpty(binding.etRequirements.getText())) {
            eventSession.setRequirements(binding.etRequirements.getText().toString());
        } else {
            binding.etRequirements.setError("Required");
            binding.etRequirements.requestFocus();
            return false;
        }
        if (!TextUtils.isEmpty(binding.etEventCost.getText())) {
            eventSession.setCost(String.format("%s", binding.etEventCost.getText().toString()));
        } else {
            binding.etEventCost.setError("Required");
            binding.etEventCost.requestFocus();
            return false;
        }
        if (!TextUtils.isEmpty(binding.etEventYear.getText())) {
            eventSession.setYear(binding.etEventYear.getText().toString());
        } else {
            Toast.makeText(getActivity(), "Please select Event Year!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(binding.etEventBeginsTime.getText())) {
            Toast.makeText(getActivity(), "Please select Event Begins Time!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(binding.etEventEndTime.getText())) {
            Toast.makeText(getActivity(), "Please select Event End Time!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!TextUtils.isEmpty(binding.etEventDates.getText())) {
            String eventMultiDay = binding.etEventDates.getText().toString();
            String[] strings = eventMultiDay.split(",");
            JSONArray jsonArray = new JSONArray();
            for (String string : strings) {
                JSONArray jsonArray1 = new JSONArray();
                jsonArray1.put(string);
                jsonArray.put(jsonArray1);
            }
            eventSession.setEvent_session_multi_dates(jsonArray.toString());
        } else {
            Toast.makeText(getActivity(), "Please select Event Dates!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!TextUtils.isEmpty(binding.etEventTiming.getText())) {
            String eventMultiDay = binding.etEventTiming.getText().toString();
            String[] strings = eventMultiDay.split(", ");
            JSONArray jsonArray = new JSONArray();
            for (String string : strings) {
                JSONArray jsonArray1 = new JSONArray();
                jsonArray1.put(string);
                jsonArray.put(jsonArray1);
            }
            eventSession.setEvent_session_timings(jsonArray.toString());
        } else {
            Toast.makeText(getActivity(), "Please select Event Timing!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(binding.etParkingBeginsTime.getText())) {
            Toast.makeText(getActivity(), "Please select Parking Begins Time!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(binding.etParkingEndTime.getText())) {
            Toast.makeText(getActivity(), "Please select Parking End Time!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!TextUtils.isEmpty(binding.etParkingTiming.getText())) {
            String eventMultiDay = binding.etParkingTiming.getText().toString();
            String[] strings = eventMultiDay.split(", ");
            JSONArray jsonArray = new JSONArray();
            for (String string : strings) {
                JSONArray jsonArray1 = new JSONArray();
                jsonArray1.put(string);
                jsonArray.put(jsonArray1);
            }
            eventSession.setEvent_session_parking_timings(jsonArray.toString());
        } else {
            Toast.makeText(getActivity(), "Please select Parking Timing!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!TextUtils.isEmpty(binding.etExpiresBy.getText())) {
            eventSession.setExpires_by(binding.etExpiresBy.getText().toString());
        } else {
            binding.etExpiresBy.setError("Required");
            binding.etExpiresBy.requestFocus();
            return false;
        }

        if (binding.rlBtnAddEventLogo.getVisibility() == View.VISIBLE) {
            Toast.makeText(getActivity(), "Please add Event Logo Image!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (binding.rlBtnAddEventImage1.getVisibility() == View.VISIBLE) {
            Toast.makeText(getActivity(), "Please add Event Image 1!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (binding.rlBtnAddEventImage2.getVisibility() == View.VISIBLE) {
            Toast.makeText(getActivity(), "Please add Event Image 2!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!TextUtils.isEmpty(binding.etEventLeadBy.getText())) {
            eventSession.setEvent_session_led_by(binding.etEventLeadBy.getText().toString());
        } else {
            binding.etEventLeadBy.setError("Required");
            binding.etEventLeadBy.requestFocus();
            return false;
        }

        if (!TextUtils.isEmpty(binding.etEventLeaderBio.getText())) {
            eventSession.setEvent_session_leaders_bio(binding.etEventLeaderBio.getText().toString());
        } else {
            binding.etEventLeaderBio.setError("Required");
            binding.etEventLeaderBio.requestFocus();
            return false;
        }

        if (!TextUtils.isEmpty(binding.etEventSessionRegFees.getText())) {
            eventSession.setEvent_session_regn_fee(binding.etEventSessionRegFees.getText().toString());
        } else {
            binding.etEventSessionRegFees.setError("Required");
            binding.etEventSessionRegFees.requestFocus();
            return false;
        }

        if (!TextUtils.isEmpty(binding.etEventLinkWeb.getText())) {
            eventSession.setEvent_session_link_on_web(binding.etEventLinkWeb.getText().toString());
        }
        if (!TextUtils.isEmpty(binding.etEventLinkTwitter.getText())) {
            eventSession.setEvent_session_link_twitter(binding.etEventLinkTwitter.getText().toString());
        }
        if (!TextUtils.isEmpty(binding.etEventLinkWhatsapp.getText())) {
            eventSession.setEvent_session_link_whatsapp(binding.etEventLinkWhatsapp.getText().toString());
        }
        if (!TextUtils.isEmpty(binding.etEventLinkOther.getText())) {
            eventSession.setEvent_session_link_other_media(binding.etEventLinkOther.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etCoveredLocations.getText())) {
            eventSession.setCovered_locations(binding.etCoveredLocations.getText().toString());
        }

        eventSession.setManager_type(eventDefinition.getManager_type());
        eventSession.setManager_type_id(eventDefinition.getManager_type_id());
        eventSession.setTwp_id(eventDefinition.getTwp_id());
        eventSession.setTownship_code(eventDefinition.getTownship_code());
        eventSession.setTownship_name(eventDefinition.getTownship_name());
        eventSession.setCompany_id(eventDefinition.getCompany_id());
        eventSession.setCompany_code(eventDefinition.getCompany_code());
        eventSession.setCompany_name(eventDefinition.getCompany_name());
        eventSession.setCompany_logo(eventDefinition.getCompany_logo());
        eventSession.setLocation_address(eventDefinition.getLocation_address());
        eventSession.setEvent_id(eventDefinition.getId());
        eventSession.setEvent_type(eventDefinition.getEvent_type());
        eventSession.setEvent_name(eventDefinition.getEvent_name());
        eventSession.setEvent_logo(eventDefinition.getEvent_logo());

        return true;
    }

    @Override
    protected void initViews(View v) {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");
    }
}
