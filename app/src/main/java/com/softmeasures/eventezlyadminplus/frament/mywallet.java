package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class mywallet extends Fragment {
    ProgressDialog pdialog;
    ArrayList<item> permitsarray = new ArrayList<>();
    CustomAdaptercity adpater;
    ListView listofvehicleno;
    TextView txttitle, txtbal, txthistory, txtaddfund;
    RelativeLayout btnok;
    String cuurentamount = "0.0", currentdate, userid;
    int i = 1;
    ConnectionDetector cd;
    PopupWindow popup;
    double addamount;
    SimpleDateFormat sdf;
    RelativeLayout showsucessmesg;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar;
    Double tr_percentage1 = 0.0, tr_fee1 = 0.0;
    String township_code1;
    //////////////////paypal///////////////////
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    private static final String TAG = "paymentQlighting";
    private static final String CONFIG_CLIENT_ID = "AQXvOmlRwvO2AI6H3XzqTe14pp4x6VtP2skKfUkhgjRAoNj0NoT7l0ZGeaRAXkAtYqyPCmxKEUXkrT89";
    private static final int REQUEST_CODE_PAYMENT = 2;
    private static PayPalConfiguration config = new PayPalConfiguration().environment(CONFIG_ENVIRONMENT).acceptCreditCards(true).clientId(CONFIG_CLIENT_ID);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View view = inflater.inflate(R.layout.fragment_mywallet, container, false);
        listofvehicleno = (ListView) view.findViewById(R.id.listofvehicle);
        txttitle = (TextView) view.findViewById(R.id.txttitte);
        btnok = (RelativeLayout) view.findViewById(R.id.addfunds);
        txtbal = (TextView) view.findViewById(R.id.txtbalance);
        txthistory = (TextView) view.findViewById(R.id.history);
        txtaddfund = (TextView) view.findViewById(R.id.txtok);
        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Thin.ttf");
        txttitle.setTypeface(type);
        txtbal.setTypeface(type);
        txthistory.setTypeface(type);
        showsucessmesg = (RelativeLayout) view.findViewById(R.id.sucessfully);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        RelativeLayout rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        cd = new ConnectionDetector(getActivity());
        if (cd.isConnectingToInternet()) {
            // new servicecharge().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            new gettransaction().execute();
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            currentdate = sdf.format(new Date());
            SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
            userid = logindeatl.getString("id", "nulnew gettransaction().execute();l");
            btnok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addfunds(getActivity());
                }
            });
            txtaddfund.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addfunds(getActivity());
                }
            });
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }


        return view;
    }

    public class gettransaction extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String loginid = logindeatl.getString("id", "null");
        JSONObject json, json1;
        String transactionurl = "_table/user_wallet?order=date_time%20DESC";
        boolean f = false;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            permitsarray.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            Log.e("url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;

            try {
                response = client.execute(post);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        String user = c.getString("user_id");

                        if (user.equals(userid)) {
                            String amount = c.getString("add_amt");
                            String date = c.getString("date_time");
                            String paidamount = c.getString("last_paid_amt");

                            if (amount.equals("null")) {
                                amount = "";
                            }

                            if (f == false) {
                                cuurentamount = c.getString("new_balance");
                                f = true;
                            }
                            item.setAmount(amount);
                            item.setExdate(date);
                            item.setPaidamount(paidamount);
                            permitsarray.add(item);
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json != null) {

                adpater = new CustomAdaptercity(getActivity(), permitsarray, rs = getResources());
                listofvehicleno.setAdapter(adpater);
            }
            super.onPostExecute(s);

        }


    }

    public class CustomAdaptercity extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdaptercity(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;

            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txtdate, txtpaymenttype, txtpayamont;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {


                vi = inflater.inflate(R.layout.mywallet, null);
                holder = new ViewHolder();

                holder.txtdate = (TextView) vi.findViewById(R.id.txtwalletdate);
                holder.txtpayamont = (TextView) vi.findViewById(R.id.txtpayamount);
                holder.txtpaymenttype = (TextView) vi.findViewById(R.id.txtwalletpaymenttype);
                vi.setTag(holder);
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
                holder.txtdate.setText("No Data");
                holder.txtdate.setVisibility(View.GONE);

            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String cat = tempValues.getExdate();
                if (cat == null) {
                    holder.txtdate.setVisibility(View.GONE);
                } else if (cat != null) {
                    holder.txtdate.setVisibility(View.VISIBLE);
                    Typeface thin = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Thin.ttf");
                    holder.txtdate.setTypeface(thin);
                    holder.txtpayamont.setTypeface(thin);
                    holder.txtpaymenttype.setTypeface(thin);
                    String inputPattern = "yyyy-MM-dd HH:mm:ss";
                    SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                    SimpleDateFormat outputFormat = new SimpleDateFormat("MM-dd-yyyy");
                    Date date = null;
                    String str = null;
                    String datef = tempValues.getExdate();
                    try {
                        if (datef.equals("0000-00-00 00:00:00")) {
                            holder.txtdate.setText("");
                        } else {

                            date = inputFormat.parse(tempValues.getExdate());
                            str = outputFormat.format(date);
                            holder.txtdate.setText(str);
                            String amu = tempValues.getAmount();
                            int a = amu.length();

                            String am = tempValues.getPaidamount();
                            if (a == 0) {
                                if (am.equals("null")) {
                                    holder.txtpayamont.setText("- $0.00");
                                    holder.txtpayamont.setTextColor(Color.parseColor("#FFFFFF"));
                                    holder.txtpayamont.setTextColor(Color.parseColor("#FFFFFF"));
                                    holder.txtdate.setTextColor(Color.parseColor("#FFFFFF"));
                                } else {
                                    Double amount = Double.parseDouble(tempValues.getPaidamount());
                                    String AM = String.format("%.2f", amount);
                                    holder.txtpayamont.setText("- $" + AM);
                                    holder.txtpayamont.setTextColor(Color.parseColor("#FFFFFF"));
                                    holder.txtdate.setTextColor(Color.parseColor("#FFFFFF"));

                                }
                                vi.setBackgroundColor(Color.parseColor("#60000000"));
                                holder.txtpaymenttype.setText("Parking Payment");
                                holder.txtpaymenttype.setTextColor(Color.parseColor("#ffffff"));
                            } else {
                                vi.setBackgroundColor(Color.parseColor("#00000000"));
                                holder.txtpayamont.setTextColor(Color.parseColor("#000000"));
                                holder.txtdate.setTextColor(Color.parseColor("#000000"));
                                holder.txtpaymenttype.setTextColor(Color.parseColor("#000000"));
                                holder.txtpaymenttype.setText("Deposit");
                                if (am.equals("null")) {
                                    holder.txtpayamont.setText("+ $0.00");
                                } else {
                                    Double amount = Double.parseDouble(tempValues.getPaidamount());
                                    String AM = String.format("%.2f", amount);
                                    holder.txtpayamont.setText("+ $" + AM);
                                }
                            }
                            double gg = Double.parseDouble(cuurentamount);
                            String finallab2 = String.format("%.2f", gg);
                            txtbal.setText("$" + finallab2);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }


    private void addfunds(Activity context) {
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.addfund, null);
        popup = new PopupWindow(context);
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setContentView(layout);
        popup.setFocusable(true);
        popup.setOutsideTouchable(true);
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        popup.showAtLocation(layout, Gravity.CENTER_VERTICAL, 0, 0);
        final EditText editfund;
        Button btncancel, btnprocced;

        editfund = (EditText) layout.findViewById(R.id.editamonut);
        btncancel = (Button) layout.findViewById(R.id.cancel);
        btnprocced = (Button) layout.findViewById(R.id.proceed);
        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
        btnprocced.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                String amount = editfund.getText().toString().toString().toString();
                if (!amount.equals("") || !amount.equals("null") || amount != "") {
                    try {
                        addamount = Double.parseDouble(amount);
                        // addamount = addamount + (addamount * tr_percentage1) + tr_fee1;
                        //amount=String.valueOf(addamount);
                        if (addamount != 0.0) {

                            PaypalPaymentIntegration(amount);
                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("Payment Not Possible");
                            alertDialog.setMessage("Please Enter Amount in more the $1.0");
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialog.show();
                        }

                    } catch (Exception e) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Amount Missing");
                        alertDialog.setMessage("Please Enter Amount in order to Add Funds.");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Amount Missing");
                    alertDialog.setMessage("Please Enter Amount in order to Add Funds.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        });
    }

    private void PaypalPaymentIntegration(String amount) {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE, amount);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PayPalService.KEYGUARD_SERVICE, true);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent, String amonut) {
        return new PayPalPayment(new BigDecimal(amonut), "USD", "Deposit", paymentIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.i(TAG, confirm.toJSONObject().toString(4));
                    Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                    JSONObject jsonObj = new JSONObject(confirm.getPayment().toJSONObject().toString(4));
                    Log.e("JSON IS", "LIKE" + jsonObj);
                    Log.e("short_description", "short_description : " + jsonObj.getString("short_description"));
                    Log.e("amount", "amount : " + jsonObj.getString("amount"));
                    Log.e("intent", "intent : " + jsonObj.getString("intent"));
                    Log.e("currency_code", "currency_code : " + jsonObj.getString("currency_code"));
                    JSONObject jsonObjResponse = confirm.toJSONObject()
                            .getJSONObject("response");
                    String transection_id = jsonObjResponse.getString("id");
                    if (transection_id != null) {
                        new addamonut().execute();
                    } else {
                        transactionfailalert();
                    }
                    Log.e("transactionId", ":;;;;" + transection_id);
                } catch (JSONException e) {
                    Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                }
            }
        }
    }

    public void ActionStartsHere() {
        againStartGPSAndSendFile();
    }

    public void againStartGPSAndSendFile() {
        new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                ActionStartsHere();

                showsucessmesg.setVisibility(View.GONE);

            }

        }.start();
    }

    public void transactionfailalert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Transaction Fail try again");
        alertDialog.setMessage("Fail");
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alertDialog.show();
    }

    public class addamonut extends AsyncTask<String, String, String> {
        String id;
        JSONObject json, json1;
        String addamounturl = "_table/user_wallet";

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            currentdate = sdf.format(new Date());
            double bal = Double.parseDouble(cuurentamount);
            bal = bal + addamount;
            String finallab = String.format("%.2f", bal);
            String AM = String.format("%.2f", addamount);
            String ip = getLocalIpAddress();
            String url = getString(R.string.api) + getString(R.string.povlive) + addamounturl;
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("paid_date", currentdate);
            jsonValues.put("last_paid_amt", AM);
            jsonValues.put("ip", ip);
            jsonValues.put("ipn_status", "");
            jsonValues.put("current_balance", finallab);
            jsonValues.put("ipn_txn_id", "");
            jsonValues.put("add_amt", AM);
            jsonValues.put("ipn_custom", "");
            jsonValues.put("ipn_payment", "");
            jsonValues.put("date_time", currentdate);
            jsonValues.put("ipn_address", "");
            jsonValues.put("user_id", userid);
            jsonValues.put("new_balance", finallab);
            jsonValues.put("action", "addition");


            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        this.id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json1 != null) {
                popup.dismiss();
                ((vchome) getActivity()).showback();
                showsucessmesg.setVisibility(View.VISIBLE);
                ActionStartsHere();
                new gettransaction().execute();
            }
            super.onPostExecute(s);
        }
    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("ip", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("aax", ex.toString());
        }
        return null;
    }


    public class servicecharge extends AsyncTask<String, String, String> {


        JSONObject json, json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String tow = logindeatl.getString("twcode", "FDV");
        String servicechargeurl = "_table/service_charges?filter=manager_id%3D'" + tow + "'";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + servicechargeurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        String tr_percentage = c.getString("tr_percentage");
                        String tr_fee = c.getString("tr_fee");
                        if (!tr_fee.equals("null")) {
                            tr_fee1 = Double.parseDouble(tr_fee);
                        } else {
                            tr_fee1 = 0.0;
                        }
                        if (!tr_percentage.equals("null")) {
                            tr_percentage1 = Double.parseDouble(tr_percentage);
                        } else {
                            tr_percentage1 = 0.0;
                        }
                        Log.e("tr_percentage1", String.valueOf(tr_percentage1));
                        Log.e("tr_free", String.valueOf(tr_fee1));
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null && json != null) {
            }
            super.onPostExecute(s);
        }
    }
}
