package com.softmeasures.eventezlyadminplus.frament.event;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragEventLocationsBinding;
import com.softmeasures.eventezlyadminplus.frament.locations.ParkingRulesFragment;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class EventLocationsFragment extends BaseFragment {

    private FragEventLocationsBinding binding;
    private ArrayList<item> locations = new ArrayList<>();
    private EventDefinition eventDefinition;
    private LocationsAdapter adapter;
    private ArrayList<String> coveredLocations = new ArrayList<>();
    private String parkingType = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            eventDefinition = new Gson().fromJson(getArguments().getString("eventDefinition"), EventDefinition.class);
            parkingType = getArguments().getString("type");
            if (eventDefinition.getCovered_locations() != null
                    && !TextUtils.isEmpty(eventDefinition.getCovered_locations())
                    && !eventDefinition.getCovered_locations().equals("null")) {
                coveredLocations.clear();
                coveredLocations.addAll(Arrays.asList(eventDefinition.getCovered_locations().split(",")));
            }
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_event_locations, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        new fetchLocations().execute();
    }

    public class fetchLocations extends AsyncTask<String, String, String> {

        String locationsUrl = "";

        JSONObject json = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            locations = new ArrayList<item>();
            locations.clear();
            if (adapter != null) adapter.notifyDataSetChanged();
            binding.swipeRefreshLayout.setRefreshing(true);
            try {
                locationsUrl = "_table/township_parking_partners?filter=(township_code%3D'" + URLEncoder.encode(eventDefinition.getTownship_code(), "utf-8") + "')";
                if (parkingType.equals("Commercial")) {
                    locationsUrl = "_table/google_parking_partners?filter=(township_code%3D'" + URLEncoder.encode(eventDefinition.getTownship_code(), "utf-8") + "')";
                } else if (parkingType.equals("Other")) {
                    locationsUrl = "_table/other_parking_partners?filter=(township_code%3D'" + URLEncoder.encode(eventDefinition.getTownship_code(), "utf-8") + "')";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + locationsUrl;
            Log.e("#DEBUG", "  fetchLocations URL: " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;

            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);
                    Log.e("#DEBUG", "  fetchLocations:  Response:  " + String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setId(c.getString("id"));
                        item1.setLocation_id(c.getString("location_id"));
                        item1.setLocation_name(c.getString("location_name"));
                        item1.setActive(c.getString("active"));
                        item1.setAddress(c.getString("address"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setIn_effect(c.getString("in_effect"));
                        item1.setLat(c.getString("lat"));
                        item1.setLng(c.getString("lng"));
                        item1.setLots_avbl(c.getString("lots_avbl"));
                        item1.setLots_total(c.getString("lots_total"));
                        item1.setMarker_type(c.getString("marker_type"));
                        item1.setNo_parking_times(c.getString("no_parking_times"));
                        item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        item1.setOff_peak_ends(c.getString("off_peak_ends"));
                        item1.setOff_peak_starts(c.getString("off_peak_starts"));
                        item1.setParking_times(c.getString("parking_times"));
                        item1.setRenewable(c.getString("renewable"));
                        item1.setTitle(c.getString("title"));
                        item1.setUser_id(c.getString("user_id"));
                        item1.setWeekend_special_diff(c.getString("weekend_special_diff"));
                        item1.setLocation_place_id(c.getString("location_place_id"));
                        if (c.has("place_id")) {
                            item1.setPlace_id(c.getString("place_id"));
                        }
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setTwp_id(c.getString("twp_id"));
                        if (c.has("township_id")) {
                            item1.setTownship_id(c.getString("township_id"));
                        }
                        item1.setTownship_code(c.getString("township_code"));
                        item1.setManager_id(c.getString("manager_id"));
                        item1.setIsKiosk(c.getString("isKiosk"));
                        if (coveredLocations.contains(item1.getLocation_code())) {
                            locations.add(item1);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                binding.swipeRefreshLayout.setRefreshing(false);
            }
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity != null) {
                    if (locations.size() > 0) {
                        //Notify adapter
                        adapter.notifyDataSetChanged();
                        binding.tvError.setVisibility(View.GONE);
                    } else {
                        binding.tvError.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {

        binding.swipeRefreshLayout.setOnRefreshListener(() -> {
            new fetchLocations().execute();
        });

    }

    @Override
    protected void initViews(View v) {
        adapter = new LocationsAdapter();
        binding.rvLocations.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvLocations.setAdapter(adapter);
    }

    private class LocationsAdapter extends RecyclerView.Adapter<LocationsAdapter.LocationsHolder> {

        @NonNull
        @Override
        public LocationsAdapter.LocationsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new LocationsHolder(LayoutInflater.from(getActivity())
                    .inflate(R.layout.item_location, null));
        }

        @Override
        public void onBindViewHolder(@NonNull LocationsAdapter.LocationsHolder locationsHolder, int i) {
            item location = locations.get(i);
            if (location != null) {
                if (location.getLocation_name() != null
                        && !TextUtils.isEmpty(location.getLocation_name())
                        && !location.getLocation_name().equals("null")) {
                    locationsHolder.tvLocationName.setText(location.getLocation_name());
                } else {
                    locationsHolder.tvLocationName.setText("-");
                }
                if (location.getLocation_code() != null
                        && !TextUtils.isEmpty(location.getLocation_code())
                        && !location.getLocation_code().equals("null")) {
                    locationsHolder.tvLocationCode.setText(location.getLocation_code());
                } else {
                    locationsHolder.tvLocationCode.setText("-");
                }
                if (location.getAddress() != null
                        && !TextUtils.isEmpty(location.getAddress())
                        && !location.getAddress().equals("null")) {
                    locationsHolder.tvAddress.setText(location.getAddress());
                } else {
                    locationsHolder.tvAddress.setText("-");
                }
            }

        }

        @Override
        public int getItemCount() {
            return locations.size();
        }

        class LocationsHolder extends RecyclerView.ViewHolder {
            TextView tvLocationCode, tvLocationName, tvAddress;

            LocationsHolder(@NonNull View itemView) {
                super(itemView);
                tvLocationCode = itemView.findViewById(R.id.tvLocationCode);
                tvLocationName = itemView.findViewById(R.id.tvLocationName);
                tvAddress = itemView.findViewById(R.id.tvAddress);

                itemView.setOnClickListener(v -> {
                    final int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        Bundle bundle = getArguments();
                        bundle.putString("location", new Gson().toJson(locations.get(pos)));
                        ParkingRulesFragment pay = new ParkingRulesFragment();
                        pay.setArguments(bundle);
                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.add(R.id.My_Container_1_ID, pay);
                        fragmentStack.lastElement().onPause();
                        ft.hide(fragmentStack.lastElement());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                    }
                });
            }
        }
    }
}
