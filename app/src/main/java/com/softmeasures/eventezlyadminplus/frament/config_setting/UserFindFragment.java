package com.softmeasures.eventezlyadminplus.frament.config_setting;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragmentUserFindBinding;
import com.softmeasures.eventezlyadminplus.frament.ConfigSettingsFragment;
import com.softmeasures.eventezlyadminplus.models.users.TownshipUser;
import com.softmeasures.eventezlyadminplus.models.users.UserProfile;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class UserFindFragment extends BaseFragment {
    private static final String TAG = "#DEBUG";
    FragmentUserFindBinding binding;
    private ArrayList<UserProfile.UserProfileData> userList = new ArrayList<>();
    private ArrayList<TownshipUser> townshipUserList = new ArrayList<>();
    private UserAdapter userAdapter = null;
    private ProgressDialog progressDialog;
    SharedPreferences logindeatl;
    String name = "";
    int user_id = 0;
    public TownshipUser townshipUser = null;
    private String ConfigType = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_find, container, false);
        if (getArguments() != null) {
            ConfigType = getArguments().getString("Config_type");
        }
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        initViews(view);
        setListeners();
        updateViews();
    }

    @Override
    protected void updateViews() {
    }

    @Override
    protected void setListeners() {
        binding.autotvUserId.setOnClickListener(v ->
                showEnter_txt("User ID", "Enter User ID", binding.autotvUserId, "autotvUserId", InputType.TYPE_CLASS_NUMBER));
        binding.autotvUserName.setOnClickListener(v ->
                showEnter_txt("User Name", "Enter User Name", binding.autotvUserName, "autotvUserName", InputType.TYPE_CLASS_TEXT));
        binding.autotvUserEmail.setOnClickListener(v ->
                showEnter_txt("Email", "Enter Email", binding.autotvUserEmail, "autotvUserEmail", InputType.TYPE_CLASS_TEXT));
        binding.autotvUserPhone.setOnClickListener(v ->
                showEnter_txt("Phone Number", "Enter Phone Number", binding.autotvUserPhone, "autotvUserPhone", InputType.TYPE_CLASS_PHONE));

        binding.btnfind.setOnClickListener(v -> {
            String id = "", name = "", email = "", phone = "";
            if (TextUtils.isEmpty(binding.autotvUserId.getText().toString())) {
                if (TextUtils.isEmpty(binding.autotvUserName.getText().toString())) {
                    if (TextUtils.isEmpty(binding.autotvUserEmail.getText().toString())) {
                        if (TextUtils.isEmpty(binding.autotvUserPhone.getText().toString())) {
                            Toast.makeText(getActivity(), "Please Enter Above ID or Name Or Email Or Phone Number", Toast.LENGTH_SHORT).show();
                            return;
                        } else phone = binding.autotvUserPhone.getText().toString();
                    } else email = binding.autotvUserEmail.getText().toString();
                } else name = binding.autotvUserName.getText().toString();
            } else id = binding.autotvUserId.getText().toString();
            new fetchUsers(id, name, email, phone).execute();
        });
    }

    @Override
    protected void initViews(View v) {
        userAdapter = new UserAdapter();
        binding.rvUsers.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        binding.rvUsers.setAdapter(userAdapter);
    }

    private void showEnter_txt(String title, String hintText, TextView etTextView, String selectedEditText, int inputType) {
        RelativeLayout viewGroup = (RelativeLayout) getActivity().findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.enter_txt, viewGroup, false);
        final PopupWindow popup = new PopupWindow(getActivity());
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(true);
        popup.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        popup.setFocusable(true);
        popup.update();
        popup.showAtLocation(layout, Gravity.TOP, 0, 0);

        final EditText edit_text;
        final TextView text_title, tvPaste;
        final Button btn_done;
        RelativeLayout btn_cancel;
        LinearLayout rl_edit;
        ImageView ivPaste;

        tvPaste = (TextView) layout.findViewById(R.id.tvPaste);
        ivPaste = (ImageView) layout.findViewById(R.id.ivPaste);
        edit_text = (EditText) layout.findViewById(R.id.edit_nu);
        text_title = (TextView) layout.findViewById(R.id.txt_title);
        rl_edit = (LinearLayout) layout.findViewById(R.id.rl_edit);

        text_title.setText(title);
        edit_text.setHint(hintText);
        edit_text.setInputType(inputType);
        if (!TextUtils.isEmpty(etTextView.getText()))
            edit_text.setText(etTextView.getText());
        btn_cancel = (RelativeLayout) layout.findViewById(R.id.close);
        btn_done = (Button) layout.findViewById(R.id.btn_done);

        edit_text.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        edit_text.setFocusable(true);
        edit_text.setSelection(edit_text.getText().length());
        edit_text.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            if (imm.isAcceptingText()) {
                Log.d(TAG, "Software Keyboard was shown");
            } else {
                Log.d(TAG, "Software Keyboard was not shown");
            }
        });
        rl_edit.setOnClickListener(v -> tvPaste.setVisibility(View.GONE));
        edit_text.setOnLongClickListener(v -> {
            tvPaste.setVisibility(View.VISIBLE);
            return true;
        });
        tvPaste.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData pData = clipboard.getPrimaryClip();
            if (pData != null) {
                ClipData.Item item = pData.getItemAt(0);
                String txtpaste = item.getText().toString();
                edit_text.getText().insert(edit_text.getSelectionStart(), txtpaste);
                edit_text.setText(edit_text.getText()+txtpaste);
            }
        });
        ivPaste.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData pData = clipboard.getPrimaryClip();
            if (pData != null) {
                ClipData.Item item = pData.getItemAt(0);
                String txtpaste = item.getText().toString();
                edit_text.getText().insert(edit_text.getSelectionStart(), txtpaste);
                edit_text.setText(edit_text.getText()+txtpaste);
            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imm.isAcceptingText()) {
                    Log.d(TAG, "DONE: Software Keyboard was shown");
                } else {
                    Log.d(TAG, "DONE: Software Keyboard was not shown");
                }
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                etTextView.setText(edit_text.getText().toString());
                popup.dismiss();
            }
        });
        btn_cancel.setOnClickListener(v -> {
            InputMethodManager imm1 = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm1.hideSoftInputFromWindow(edit_text.getWindowToken(), 0);
            String txt = edit_text.getText().toString();
            popup.dismiss();
        });
    }

    private class fetchUsers extends AsyncTask<String, String, String> {
        String userUrl = "_table/user_profile";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            binding.rlProgressbar.setVisibility(View.VISIBLE);
        }

        public fetchUsers(String uID, String uName, String uEmail, String uPhone) {
            if (!TextUtils.isEmpty(uID))
                userUrl = "_table/user_profile?filter=(user_id=" + uID + ")";
            else if (!TextUtils.isEmpty(uName))
                userUrl = "_table/user_profile?filter=(user_name=" + uName + ")";
            else if (!TextUtils.isEmpty(uEmail))
                userUrl = "_table/user_profile?filter=(email=" + uEmail + ")";
            else if (!TextUtils.isEmpty(uPhone))
                userUrl = "_table/user_profile?filter=(mobile=" + uPhone + ")";
        }

        @Override
        protected String doInBackground(String... strings) {
            userList.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + userUrl;
            Log.e("#DEBUG", "      fetchGroups:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", " fetchUsers: Response: " + responseStr);
                    JSONObject json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");
                    if (jsonArray != null) {
                        for (int j = 0; j < jsonArray.length(); j++) {
                            JSONObject object = jsonArray.getJSONObject(j);
                            UserProfile.UserProfileData user = new UserProfile.UserProfileData();
                            user.setId(object.getInt("id"));
                            user.setUser_id(object.getInt("user_id"));
                            if (object.has("date_time")
                                    && !TextUtils.isEmpty(object.getString("date_time"))
                                    && !object.getString("date_time").equals("null")) {
                                user.setDate_time(object.getString("date_time"));
                            }
                            if (object.has("fname")
                                    && !TextUtils.isEmpty(object.getString("fname"))
                                    && !object.getString("fname").equals("null")) {
                                user.setFname(object.getString("fname"));
                            }
                            if (object.has("lname")
                                    && !TextUtils.isEmpty(object.getString("lname"))
                                    && !object.getString("lname").equals("null")) {
                                user.setLname(object.getString("lname"));
                            }
                            if (object.has("email")
                                    && !TextUtils.isEmpty(object.getString("email"))
                                    && !object.getString("email").equals("null")) {
                                user.setEmail(object.getString("email"));
                            }
                            if (object.has("mobile")
                                    && !TextUtils.isEmpty(object.getString("mobile"))
                                    && !object.getString("mobile").equals("null")) {
                                user.setMobile(object.getString("mobile"));
                            }
                            if (object.has("lphone")
                                    && !TextUtils.isEmpty(object.getString("lphone"))
                                    && !object.getString("lphone").equals("null")) {
                                user.setLphone(object.getString("lphone"));
                            }
                            if (object.has("address")
                                    && !TextUtils.isEmpty(object.getString("address"))
                                    && !object.getString("address").equals("null")) {
                                user.setAddress(object.getString("address"));
                            }
                            if (object.has("city")
                                    && !TextUtils.isEmpty(object.getString("city"))
                                    && !object.getString("city").equals("null")) {
                                user.setCity(object.getString("city"));
                            }
                            if (object.has("state")
                                    && !TextUtils.isEmpty(object.getString("state"))
                                    && !object.getString("state").equals("null")) {
                                user.setState(object.getString("state"));
                            }
                            if (object.has("zip")
                                    && !TextUtils.isEmpty(object.getString("zip"))
                                    && !object.getString("zip").equals("null")) {
                                user.setZip(object.getString("zip"));
                            }
                            if (object.has("country")
                                    && !TextUtils.isEmpty(object.getString("country"))
                                    && !object.getString("country").equals("null")) {
                                user.setCountry(object.getString("country"));
                            }
                            if (object.has("full_address")
                                    && !TextUtils.isEmpty(object.getString("full_address"))
                                    && !object.getString("full_address").equals("null")) {
                                user.setFull_address(object.getString("full_address"));
                            }
                            if (object.has("ad_proof")
                                    && !TextUtils.isEmpty(object.getString("ad_proof"))
                                    && !object.getString("ad_proof").equals("null")) {
                                user.setAd_proof(object.getString("ad_proof"));
                            }
                            if (object.has("user_name")
                                    && !TextUtils.isEmpty(object.getString("user_name"))
                                    && !object.getString("user_name").equals("null")) {
                                user.setUser_name(object.getString("user_name"));
                            }
                            if (object.has("gender")
                                    && !TextUtils.isEmpty(object.getString("gender"))
                                    && !object.getString("gender").equals("null")) {
                                user.setGender(object.getString("gender"));
                            }
                            if (object.has("ip")
                                    && !TextUtils.isEmpty(object.getString("ip"))
                                    && !object.getString("ip").equals("null")) {
                                user.setIp(object.getString("ip"));
                            }
                            if (object.has("township_code")
                                    && !TextUtils.isEmpty(object.getString("township_code"))
                                    && !object.getString("township_code").equals("null")) {
                                user.setTownship_code(object.getString("township_code"));
                            }
                            if (object.has("firebase_token")
                                    && !TextUtils.isEmpty(object.getString("firebase_token"))
                                    && !object.getString("firebase_token").equals("null")) {
                                user.setFirebase_token(object.getString("firebase_token"));
                            }
                            userList.add(user);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                binding.rlProgressbar.setVisibility(View.GONE);
                if (userList.size() != 0) {
                    binding.tvError.setVisibility(View.GONE);
                    userAdapter.notifyDataSetChanged();
                    for (int pos = 0; pos < userList.size(); pos++)
                        new fetchTownshipUsers(userList.get(pos).getUser_id(), pos).execute();
                } else {
                    userAdapter.notifyDataSetChanged();
                    binding.tvError.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private class fetchTownshipUsers extends AsyncTask<String, String, String> {
        String userUrl = "_table/township_users";
        int pos = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            binding.rlProgressbar.setVisibility(View.VISIBLE);
        }

        public fetchTownshipUsers(int uID, int pos) {
            this.pos = pos;
            userUrl = "_table/township_users?filter=(user_id=" + uID + ")";
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + userUrl;
            Log.e("#DEBUG", "      fetchTownshipUsers:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    try {
                        JSONObject json = new JSONObject(responseStr);
                        JSONArray jsonArray = json.getJSONArray("resource");
                        for (int j = 0; j < jsonArray.length(); j++) {
                            JSONObject object = jsonArray.getJSONObject(j);
                            townshipUser = new TownshipUser();
                            if (object.has("id")
                                    && !TextUtils.isEmpty(object.getString("id"))
                                    && !object.getString("id").equals("null")) {
                                townshipUser.setId(object.getInt("id"));
                            }
                            if (object.has("date_time")
                                    && !TextUtils.isEmpty(object.getString("date_time"))
                                    && !object.getString("date_time").equals("null")) {
                                townshipUser.setDateTime(object.getString("date_time"));
                            }
                            if (object.has("user_id")
                                    && !TextUtils.isEmpty(object.getString("user_id"))
                                    && !object.getString("user_id").equals("null")) {
                                townshipUser.setUserId(object.getInt("user_id"));
                            }
                            if (object.has("user_name")
                                    && !TextUtils.isEmpty(object.getString("user_name"))
                                    && !object.getString("user_name").equals("null")) {
                                townshipUser.setUserName(object.getString("user_name"));
                            }
                            if (object.has("twp_id")
                                    && !TextUtils.isEmpty(object.getString("twp_id"))
                                    && !object.getString("twp_id").equals("null")) {
                                townshipUser.setTwpId(object.getInt("twp_id"));
                            }
                            if (object.has("township_code")
                                    && !TextUtils.isEmpty(object.getString("township_code"))
                                    && !object.getString("township_code").equals("null")) {
                                townshipUser.setTownshipCode(object.getString("township_code"));
                            }
                            if (object.has("township_name")
                                    && !TextUtils.isEmpty(object.getString("township_name"))
                                    && !object.getString("township_name").equals("null")) {
                                townshipUser.setTownshipName(object.getString("township_name"));
                            }
                            if (object.has("profile_name")
                                    && !TextUtils.isEmpty(object.getString("profile_name"))
                                    && !object.getString("profile_name").equals("null")) {
                                townshipUser.setProfileName(object.getString("profile_name"));
                            }
                            if (object.has("status")
                                    && !TextUtils.isEmpty(object.getString("status"))
                                    && !object.getString("status").equals("null")) {
                                townshipUser.setStatus(object.getString("status"));
                            }
                            if (object.has("email")
                                    && !TextUtils.isEmpty(object.getString("email"))
                                    && !object.getString("email").equals("null")) {
                                townshipUser.setEmail(object.getString("email"));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                binding.rlProgressbar.setVisibility(View.GONE);
                if (townshipUser != null) {
                    userList.get(pos).setRole(townshipUser.getProfileName());
                    userAdapter.notifyDataSetChanged();
                } else {
//                    binding.tvError.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyUserViewHolder> {
        @NonNull
        @Override
        public UserAdapter.MyUserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new UserAdapter.MyUserViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_users, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull UserAdapter.MyUserViewHolder holder, int position) {
            holder.tvUserId.setText("" + userList.get(position).getUser_id());

            if (!TextUtils.isEmpty(userList.get(position).getFname()) || !TextUtils.isEmpty(userList.get(position).getLname()))
                holder.tvUserName.setText(userList.get(position).getFname() + " " + userList.get(position).getLname());
            else holder.tvUserName.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(userList.get(position).getMobile()))
                holder.tvUserCode.setText(userList.get(position).getMobile());
            else holder.tvUserCode.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(userList.get(position).getEmail()))
                holder.tvUserEmail.setText(userList.get(position).getEmail());
            else holder.tvUserEmail.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(userList.get(position).getRole()))
                holder.tvUserRole.setText(userList.get(position).getRole());
            else holder.tvUserRole.setVisibility(View.GONE);
        }

        @Override
        public int getItemCount() {
            return userList.size();
        }

        public class MyUserViewHolder extends RecyclerView.ViewHolder {
            TextView tvUserId, tvUserName, tvUserCode, tvUserEmail, tvUserRole;

            public MyUserViewHolder(@NonNull View itemView) {
                super(itemView);
                tvUserId = itemView.findViewById(R.id.tvUserId);
                tvUserName = itemView.findViewById(R.id.tvUserName);
                tvUserCode = itemView.findViewById(R.id.tvUserContact);
                tvUserEmail = itemView.findViewById(R.id.tvUserEmail);
                tvUserRole = itemView.findViewById(R.id.tvUserRole);

                itemView.setOnClickListener(v -> {
                    Log.d("#DEBUG", "userProfileData: " + new Gson().toJson(userList.get(getAdapterPosition())));
                    final int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        Fragment frag = null;
                        if (ConfigType.equals("Call Config"))
                            frag = new ConfigSettingsFragment();
                        else if (ConfigType.equals("Chat Config"))
                            frag = new MessagingSettingFragment();
                        else if (ConfigType.equals("Group Config"))
                            frag = new GroupSettingFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("selectedUser", new Gson().toJson(userList.get(pos)));
                        frag.setArguments(bundle);
                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.add(R.id.My_Container_1_ID, frag);
                        fragmentStack.lastElement().onPause();
                        ft.hide(fragmentStack.lastElement());
                        fragmentStack.push(frag);
                        ft.commitAllowingStateLoss();
                    }
                });
            }
        }
    }
}