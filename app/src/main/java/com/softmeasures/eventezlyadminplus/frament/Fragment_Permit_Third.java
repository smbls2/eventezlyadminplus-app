package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.bumptech.glide.Glide;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.nileshp.multiphotopicker.photopicker.activity.PickImageActivity;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.MultiSelection;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.compressor.Compressor;
import com.softmeasures.eventezlyadminplus.frament.user_profile.Frg_User_edit_profile_Permit;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

/**
 * Created by Significant Info on 3/14/2018.
 */

public class Fragment_Permit_Third extends Fragment {

    View rootView;
    Activity activity;
    Context mContext;
    ConnectionDetector cd;
    String user_id;
    TextView txt_Title;
    RelativeLayout rl_main, rl_progressbar;
    ProgressBar progressbar;

    public SignaturePad mSignaturePad;
    public Button mClearButton, mSubmitButton;
    public String township_name, township_code, user_name, date;

    private ArrayList<String> pathList = new ArrayList<>();
    HorizontalScrollView horizontalScrollView, horizontalScrollView1;
    LinearLayout layoutListItemSelect, layoutListItemSelect1;
    ArrayList<MultiSelection> listItemSelect, listItemSelect1;
    ArrayList<MultiSelection> CompressItemSelect, CompressItemSelect1;
    private int GALLERY = 1, CAMERA = 2;
    Button btn_ResidencySelectImage, btn_DriverLicenseSelectImage;
    String Location_id = "", Location_code = "", Location_name = "",
            Township_name = "", Townshipcode = "", Permit_id = "",
            Permit_type = "", Permit_name = "", Cost = "", Expires_by = "",
            Renewable = "", Permit_nextnum = "", Requirements = "",
            Appl_req_download = "", Official_logo = "", twp_id = "", Manager_type = "", Manager_type_id = "";

    public String m_Open = "Residency";
    AmazonS3 s3;
    TransferUtility transferUtility;

    StringBuffer file_Residency;
    StringBuffer file_SignaturePad;
    StringBuffer file_Driver;

    String final_Residency;
    String final_Driver;
    String final_SignaturePad;

    int current_upload = 0, totla_size = 0;
    TextView txt_township_name, txt_permit_name, txt_ProfileDetails, txt_ExpiryTime, txt_Fee,
            txt_CoveredLocation, txt_PermitNext, btn_ProfileEdit;
    TextView txt_Name, txt_Phone, txt_Mobile, txt_Email, txt_Address, txt_ValidUntil, txt_Requirements,
            txt_Download_PDF, txt_Upload_PDF, txt_pdfUpload;
    SimpleDateFormat dateFormat, dateFormat1;
    String Expires_Date = "";

    String first_name = "", last_name = "", phone = "", lphone = "",
            message = "", email = "", displayName = "", full_address = "", township_id = "";

    ImageView img_Permit_logo;
    Boolean ProfileEdit = true;
    public String file_download_path = "";
    File dir_File;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_permit_third, container, false);

        StrictMode.VmPolicy policy = new StrictMode.VmPolicy.Builder().detectAll().penaltyLog().build();
        StrictMode.setVmPolicy(policy);

        activity = getActivity();
        mContext = getContext();
        cd = new ConnectionDetector(activity);

        file_Residency = new StringBuffer();
        file_Driver = new StringBuffer();
        file_SignaturePad = new StringBuffer();

        file_download_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" +
                getActivity().getPackageName() + "/Images/";
        dir_File = new File(file_download_path);
        if (!dir_File.exists()) {
            if (!dir_File.mkdirs()) {
            }
        }
        s3 = new AmazonS3Client(new BasicAWSCredentials("AKIAJNG22KFRVUAICSEA", "NSrQR/yAg52RPD3kp3FMb3NO+Vrxuty4iAwPU4th"));
        transferUtility = new TransferUtility(s3, getActivity().getApplicationContext());

        SharedPreferences logindeatl = activity.getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = logindeatl.getString("id", "null");
        user_name = logindeatl.getString("name", "null");

        if (getArguments() != null) {
            township_id = getArguments().getString("township_id");
            Location_id = getArguments().getString("Location_id");
            Location_code = getArguments().getString("Location_code");
            Location_name = getArguments().getString("Location_name");
            Township_name = getArguments().getString("Township_name");
            Townshipcode = getArguments().getString("Township_code");
            Permit_id = getArguments().getString("Permit_id");
            Permit_type = getArguments().getString("Permit_type");
            Permit_name = getArguments().getString("Permit_name");
            Cost = getArguments().getString("cost");
            Expires_by = getArguments().getString("Expires_by");
            Renewable = getArguments().getString("Renewable");
            Permit_nextnum = getArguments().getString("Permit_nextnum");
            Requirements = getArguments().getString("requirements");
            Appl_req_download = getArguments().getString("appl_req_download");
            Official_logo = getArguments().getString("Official_logo");
            twp_id = getArguments().getString("twp_id");
            Manager_type = getArguments().getString("Manager_type");
            Manager_type_id = getArguments().getString("Manager_type_id");
        }

        //+1
        int nextnum = Integer.parseInt(Permit_nextnum);
        Permit_nextnum = Townshipcode + "-" + String.valueOf(nextnum);
        Log.e("Permit nextnum ", " : " + Permit_nextnum);

        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        Calendar getCurrent = Calendar.getInstance();
        String getDate = dateFormat.format(getCurrent.getTime());
        try {
            Date pDate = dateFormat.parse(getDate);
            if (Expires_by.equalsIgnoreCase("365 days")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);
                c.add(Calendar.DATE, 365);
                Date expDate = c.getTime();
                Expires_Date = dateFormat1.format(expDate) + " 23:59:59";
            } else if (Expires_by.equalsIgnoreCase("YearEnd")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);
                Expires_Date = String.valueOf(c.get(Calendar.YEAR)) + "-12-31 23:59:59";
            } else if (Expires_by.equalsIgnoreCase("MonthEnd")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);
                int thisMonth = c.get(Calendar.MONTH) + 1;
                Expires_Date = String.valueOf(c.get(Calendar.YEAR) + "-" + thisMonth
                        + "-" + c.getActualMaximum(Calendar.DATE)) + " 23:59:59";
            } else if (Expires_by.equalsIgnoreCase("30 Days")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);
                c.add(Calendar.DATE, 30);
                Date expDate = c.getTime();
                Expires_Date = dateFormat.format(expDate);
            } else if (Expires_by.equalsIgnoreCase("7 Days")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);
                c.add(Calendar.DATE, 7);
                Date expDate = c.getTime();
                Expires_Date = dateFormat.format(expDate);
            } else if (Expires_by.equalsIgnoreCase("Weekend")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);

                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                if (Calendar.MONDAY == dayOfWeek) {
                    c.add(Calendar.DATE, 6);
                } else if (Calendar.TUESDAY == dayOfWeek) {
                    c.add(Calendar.DATE, 5);
                } else if (Calendar.WEDNESDAY == dayOfWeek) {
                    c.add(Calendar.DATE, 4);
                } else if (Calendar.THURSDAY == dayOfWeek) {
                    c.add(Calendar.DATE, 3);
                } else if (Calendar.FRIDAY == dayOfWeek) {
                    c.add(Calendar.DATE, 2);
                } else if (Calendar.SATURDAY == dayOfWeek) {
                    c.add(Calendar.DATE, 1);
                } else if (Calendar.SUNDAY == dayOfWeek) {
                    c.add(Calendar.DATE, 0);
                }

                int thisMonth = c.get(Calendar.MONTH) + 1;
                Expires_Date = String.valueOf(c.get(Calendar.YEAR) + "-" + thisMonth
                        + "-" + c.get(Calendar.DAY_OF_MONTH)) + " 23:59:59";
            } else if (Expires_by.equalsIgnoreCase("1 Days")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);
                c.add(Calendar.DATE, 1);
                Date expDate = c.getTime();
                Expires_Date = dateFormat.format(expDate);
            } else if (Expires_by.equalsIgnoreCase("24 Hours")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);
                c.add(Calendar.DATE, 1);
                Date expDate = c.getTime();
                Expires_Date = dateFormat.format(expDate);
            } else if (Expires_by.equalsIgnoreCase("Daily")) {
                Calendar c = Calendar.getInstance();
                c.setTime(pDate);
                int thisMonth = c.get(Calendar.MONTH) + 1;
                Expires_Date = String.valueOf(c.get(Calendar.YEAR) + "-" + thisMonth
                        + "-" + c.get(Calendar.DAY_OF_MONTH)) + " 23:59:59";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        listItemSelect = new ArrayList<MultiSelection>();
        listItemSelect1 = new ArrayList<MultiSelection>();

        txt_township_name = (TextView) rootView.findViewById(R.id.txt_township_name);
        txt_permit_name = (TextView) rootView.findViewById(R.id.txt_permit_name);

        rl_main = (RelativeLayout) rootView.findViewById(R.id.rl_main);
        rl_progressbar = (RelativeLayout) rootView.findViewById(R.id.rl_progressbar);
        progressbar = (ProgressBar) rootView.findViewById(R.id.progressbar);
        txt_Title = (TextView) rootView.findViewById(R.id.txt_Title);
        txt_Title.setText("PERMIT APPLICATION");

        mSignaturePad = (SignaturePad) rootView.findViewById(R.id.signature_pad);
        mClearButton = (Button) rootView.findViewById(R.id.clear_button);
        mSubmitButton = (Button) rootView.findViewById(R.id.submit_button);

        layoutListItemSelect = (LinearLayout) rootView.findViewById(R.id.layoutListItemSelect);
        horizontalScrollView = (HorizontalScrollView) rootView.findViewById(R.id.horizontalScrollView);
        layoutListItemSelect1 = (LinearLayout) rootView.findViewById(R.id.layoutListItemSelect1);
        horizontalScrollView1 = (HorizontalScrollView) rootView.findViewById(R.id.horizontalScrollView1);
        btn_ResidencySelectImage = (Button) rootView.findViewById(R.id.btn_ResidencySelectImage);
        btn_DriverLicenseSelectImage = (Button) rootView.findViewById(R.id.btn_DriverLicenseSelectImage);
        txt_ProfileDetails = (TextView) rootView.findViewById(R.id.txt_ProfileDetails);
        txt_ExpiryTime = (TextView) rootView.findViewById(R.id.txt_ExpiryTime);
        txt_Fee = (TextView) rootView.findViewById(R.id.txt_Fee);
        txt_CoveredLocation = (TextView) rootView.findViewById(R.id.txt_CoveredLocation);

        txt_Name = (TextView) rootView.findViewById(R.id.txt_Name);
        txt_Phone = (TextView) rootView.findViewById(R.id.txt_Phone);
        txt_Mobile = (TextView) rootView.findViewById(R.id.txt_Mobile);
        txt_Email = (TextView) rootView.findViewById(R.id.txt_Email);
        txt_Address = (TextView) rootView.findViewById(R.id.txt_Address);
        txt_ValidUntil = (TextView) rootView.findViewById(R.id.txt_ValidUntil);
        txt_Requirements = (TextView) rootView.findViewById(R.id.txt_Requirements);
        txt_Download_PDF = (TextView) rootView.findViewById(R.id.txt_Download_PDF);
        txt_Upload_PDF = (TextView) rootView.findViewById(R.id.txt_Upload_PDF);
        txt_pdfUpload = (TextView) rootView.findViewById(R.id.txt_pdfUpload);
        txt_PermitNext = (TextView) rootView.findViewById(R.id.txt_PermitNext);
        btn_ProfileEdit = (TextView) rootView.findViewById(R.id.btn_ProfileEdit);
        txt_PermitNext.setText(Permit_nextnum);
        img_Permit_logo = (ImageView) rootView.findViewById(R.id.img_Permit_logo);

        Picasso.with(getContext()).load(Official_logo).into(img_Permit_logo);

        txt_Download_PDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Appl_req_download.equalsIgnoreCase("") &&
                        !Appl_req_download.equalsIgnoreCase("null")) {
                    getPdfFile(Appl_req_download);
                } else {
                    Toast.makeText(getActivity(), "File not added...", Toast.LENGTH_LONG).show();
                }
            }
        });

        txt_ExpiryTime.setText(Expires_Date);
        txt_township_name.setText("Village of Farmingdale");
        txt_permit_name.setText(Townshipcode + "-" + Permit_name);
        txt_Fee.setText(Cost);
        txt_CoveredLocation.setText(Location_name);
        txt_ValidUntil.setText(Expires_by);
        txt_Requirements.setText(Requirements);

        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                //Event triggered when the pad is touched
            }

            @Override
            public void onSigned() {
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mClearButton.setEnabled(false);
            }
        });

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });

        btn_ResidencySelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_Open = "Residency";
                showPictureDialog();
            }
        });

        btn_DriverLicenseSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_Open = "DriverLicense";
                showPictureDialog();
            }
        });

        txt_Upload_PDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_Open = "PDF";
                Intent i = new Intent(getContext(), FileActivity.class);
                i.putExtra("isdoc", true);
                startActivityForResult(i, 8);
            }
        });

        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClearButton.isEnabled()) {
                    Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                    if (addJpgSignatureToGallery(signatureBitmap)) {
                        new ImageCompress().execute();
                    } else {
                        Toast.makeText(getActivity(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    new ImageCompress().execute();
                }

            }
        });


        btn_ProfileEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileEdit = true;
                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Frg_User_edit_profile_Permit pay = new Frg_User_edit_profile_Permit();
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        return rootView;
    }

    public void getPdfFile(String pdf_download) {
        Log.e("pdf_download ", " : " + pdf_download);
        rl_progressbar.setVisibility(View.VISIBLE);
        progressbar.setVisibility(View.VISIBLE);
        try {
            final File file_exists = new File(file_download_path, pdf_download);
            if (file_exists.exists()) {
                rl_progressbar.setVisibility(View.GONE);
                MimeTypeMap myMime = MimeTypeMap.getSingleton();
                Intent newIntent = new Intent(Intent.ACTION_VIEW);
                String mimeType = myMime.getMimeTypeFromExtension(fileExt(String.valueOf(((file_exists.getAbsolutePath())))).substring(1));
                newIntent.setDataAndType(Uri.fromFile((new File(file_exists.getAbsolutePath()))), mimeType);
                newIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                newIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                try {
                    getActivity().startActivity(newIntent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getActivity(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
                }
            } else {
                TransferObserver downloadObserver = transferUtility.download("parkezly-images", "/images/" + pdf_download, file_exists);
                downloadObserver.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (TransferState.COMPLETED == state) {
                            rl_progressbar.setVisibility(View.GONE);
                            MimeTypeMap myMime = MimeTypeMap.getSingleton();
                            Intent newIntent = new Intent(Intent.ACTION_VIEW);
                            String mimeType = myMime.getMimeTypeFromExtension(fileExt(String.valueOf(((file_exists.getAbsolutePath())))).substring(1));
                            newIntent.setDataAndType(Uri.fromFile((new File(file_exists.getAbsolutePath()))), mimeType);
                            newIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            newIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            try {
                                getActivity().startActivity(newIntent);
                            } catch (ActivityNotFoundException e) {
                                Toast.makeText(getActivity(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                        int percentDone = (int) percentDonef;
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        ex.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();
        }
    }

    File photo;

    public boolean addJpgSignatureToGallery(Bitmap signature) {
        boolean result = false;
        try {
            photo = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.jpg", System.currentTimeMillis()));
            saveBitmapToJPG(signature, photo);
            scanMediaFile(photo);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public File getAlbumStorageDir(String albumName) {
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(mContext);
        String[] pictureDialogItems = {"File", "Camera", "Photo Gallery"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Intent i = new Intent(getContext(), FileActivity.class);
                                i.putExtra("isdoc", true);
                                startActivityForResult(i, 8);
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                            case 2:
                                openImagePickerIntent();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void openImagePickerIntent() {
        Intent mIntent = new Intent(mContext, PickImageActivity.class);
        mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 5);
        mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
        startActivityForResult(mIntent, PickImageActivity.PICKER_REQUEST_CODE);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    public void addItemSelect(final MultiSelection item) {
        listItemSelect.add(item);
        final View viewItemSelected = View.inflate(mContext, R.layout.piclist_item_selected_big, null);
        ImageView imageItem = (ImageView) viewItemSelected.findViewById(R.id.imageItem);
        ImageView btnDelete = (ImageView) viewItemSelected.findViewById(R.id.btnDelete);
        TextView txt_Name = (TextView) viewItemSelected.findViewById(R.id.txt_Name);
        txt_Name.setText(item.getF_Name());

        imageItem.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Glide.with(mContext).asBitmap().load(item.getF_Path()).placeholder(R.mipmap.docs).into(imageItem);

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutListItemSelect.removeView(viewItemSelected);
                listItemSelect.remove(item);
            }
        });

        layoutListItemSelect.addView(viewItemSelected);
        viewItemSelected.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.abc_fade_in));
        sendScroll();
    }

    public void addItemSelect1(final MultiSelection item) {
        listItemSelect1.add(item);
        final View viewItemSelected = View.inflate(mContext, R.layout.piclist_item_selected_big, null);
        ImageView imageItem = (ImageView) viewItemSelected.findViewById(R.id.imageItem);
        ImageView btnDelete = (ImageView) viewItemSelected.findViewById(R.id.btnDelete);
        TextView txt_Name = (TextView) viewItemSelected.findViewById(R.id.txt_Name);

        txt_Name.setText(item.getF_Name());
        imageItem.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Glide.with(mContext).asBitmap().load(item.getF_Path()).placeholder(R.mipmap.docs).into(imageItem);

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutListItemSelect1.removeView(viewItemSelected);
                listItemSelect1.remove(item);
            }
        });

        layoutListItemSelect1.addView(viewItemSelected);
        viewItemSelected.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.abc_fade_in));
        sendScroll1();
    }

    private void sendScroll() {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        horizontalScrollView.fullScroll(66);
                    }
                });
            }
        }).start();
    }

    private void sendScroll1() {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        horizontalScrollView1.fullScroll(66);
                    }
                });
            }
        }).start();
    }

    String pdfPath = "";
    Uri pfdURI = null;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            Uri tempUri = getImageUri(activity, thumbnail);
            File finalFile = new File(getRealPathFromURI(tempUri));
            if (m_Open.equalsIgnoreCase("Residency")) {
                MultiSelection multiSelection = new MultiSelection();
                multiSelection.setF_Path(String.valueOf(finalFile));
                multiSelection.setF_Name(finalFile.getName());
                multiSelection.setF_Com("yes");
                addItemSelect(multiSelection);
            } else if (m_Open.equalsIgnoreCase("DriverLicense")) {
                MultiSelection multiSelection = new MultiSelection();
                multiSelection.setF_Path(String.valueOf(finalFile));
                multiSelection.setF_Name(finalFile.getName());
                multiSelection.setF_Com("yes");
                addItemSelect1(multiSelection);
            }
        }
        if (resultCode != RESULT_OK) {
            return;
        }
        if (resultCode == -1 && requestCode == PickImageActivity.PICKER_REQUEST_CODE) {
            pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (this.pathList != null && !this.pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    if (m_Open.equalsIgnoreCase("Residency")) {
                        MultiSelection multiSelection = new MultiSelection();
                        multiSelection.setF_Path(String.valueOf(pathList.get(i)));
                        multiSelection.setF_Name(pathList.get(i).substring(pathList.get(i).lastIndexOf("/") + 1));
                        multiSelection.setF_Com("yes");
                        addItemSelect(multiSelection);
                    } else if (m_Open.equalsIgnoreCase("DriverLicense")) {
                        MultiSelection multiSelection = new MultiSelection();
                        multiSelection.setF_Path(String.valueOf(pathList.get(i)));
                        multiSelection.setF_Name(pathList.get(i).substring(pathList.get(i).lastIndexOf("/") + 1));
                        multiSelection.setF_Com("yes");
                        addItemSelect1(multiSelection);
                    }
                }
            }
        }
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 8) {

                pdfPath = data.getExtras().getString("path");
                File file = new File(pdfPath);

                long fileSizeInBytes = file.length();
                long fileSizeInKB = fileSizeInBytes / 1024;
                long fileSizeInMB = fileSizeInKB / 1024;

                if (fileSizeInKB > 2048) {
                    pfdURI = null;
                    Toast.makeText(getContext(), "Can not upload large scale document", Toast.LENGTH_LONG).show();
                } else {
                    if (m_Open.equalsIgnoreCase("Residency")) {
                        MultiSelection multiSelection = new MultiSelection();
                        multiSelection.setF_Path(pdfPath);
                        multiSelection.setF_Name(pdfPath.substring(pdfPath.lastIndexOf("/") + 1));
                        multiSelection.setF_Com("no");
                        addItemSelect(multiSelection);
                    } else if (m_Open.equalsIgnoreCase("DriverLicense")) {
                        MultiSelection multiSelection = new MultiSelection();
                        multiSelection.setF_Path(pdfPath);
                        multiSelection.setF_Name(pdfPath.substring(pdfPath.lastIndexOf("/") + 1));
                        multiSelection.setF_Com("no");
                        addItemSelect1(multiSelection);
                    } else if (m_Open.equalsIgnoreCase("PDF")) {
                        txt_pdfUpload.setVisibility(View.VISIBLE);
                        pfdURI = Uri.fromFile(new File(pdfPath));
                        txt_pdfUpload.setText(new File(pdfPath).getName());
                    }
                }
            }

        }
    }

    public Uri getImageUri(Activity inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public class ImageCompress extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CompressItemSelect = new ArrayList();
            CompressItemSelect1 = new ArrayList();
            totla_size = 0;
            rl_progressbar.setVisibility(View.VISIBLE);
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                for (int i = 0; i < listItemSelect.size(); i++) {
                    if (listItemSelect.get(i).getF_Com().equalsIgnoreCase("yes")) {
                        File ImageFile = new File(listItemSelect.get(i).getF_Path());
                        File compressedImageFile = null;
                        String strFile = "";
                        try {
                            compressedImageFile = new Compressor(mContext).compressToFile(ImageFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        strFile = String.valueOf(compressedImageFile);

                        MultiSelection multiSelection = new MultiSelection();
                        multiSelection.setF_Path(strFile);
                        multiSelection.setF_Name(strFile.substring(strFile.lastIndexOf("/") + 1));
                        multiSelection.setF_Com("yes");
                        CompressItemSelect.add(multiSelection);
                    } else {
                        MultiSelection multiSelection = new MultiSelection();
                        multiSelection.setF_Path(listItemSelect.get(i).getF_Path());
                        multiSelection.setF_Name(listItemSelect.get(i).getF_Name());
                        multiSelection.setF_Com("no");
                        CompressItemSelect.add(multiSelection);
                    }


                }
                totla_size = totla_size + CompressItemSelect.size();
                for (int i = 0; i < listItemSelect1.size(); i++) {
                    if (listItemSelect1.get(i).getF_Com().equalsIgnoreCase("yes")) {
                        File ImageFile = new File(listItemSelect1.get(i).getF_Path());
                        File compressedImageFile = null;
                        String strFile = "";
                        try {
                            compressedImageFile = new Compressor(mContext).compressToFile(ImageFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        strFile = String.valueOf(compressedImageFile);

                        MultiSelection multiSelection = new MultiSelection();
                        multiSelection.setF_Path(strFile);
                        multiSelection.setF_Name(strFile.substring(strFile.lastIndexOf("/") + 1));
                        multiSelection.setF_Com("yes");
                        CompressItemSelect1.add(multiSelection);
                    } else {
                        MultiSelection multiSelection = new MultiSelection();
                        multiSelection.setF_Path(listItemSelect1.get(i).getF_Path());
                        multiSelection.setF_Name(listItemSelect1.get(i).getF_Name());
                        multiSelection.setF_Com("no");
                        CompressItemSelect1.add(multiSelection);
                    }
                }
                totla_size = totla_size + CompressItemSelect1.size();
                if (mClearButton.isEnabled()) {
                    totla_size = totla_size + 1;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (getActivity() != null && totla_size == 0) {
                new getPermit_Subscription().execute();
            } else {
                upload_file();
            }
        }
    }

    public void upload_file() {

        for (int i = 0; i < CompressItemSelect.size(); i++) {
            File file = new File(CompressItemSelect.get(i).getF_Path());
            //String namegsxsax = System.currentTimeMillis() + ".jpg";
            String namegsxsax = CompressItemSelect.get(i).getF_Name();
            file_Residency.append(namegsxsax + ",");
            namegsxsax = "/images/" + namegsxsax;
            final_Residency = file_Residency.substring(0, file_Residency.length() - 1);
            Log.e("namegsxsax " + namegsxsax, "  : " + final_Residency);
            TransferObserver transferObserver = transferUtility.upload("parkezly-images", namegsxsax, file);
            transferObserverListener(transferObserver);
        }

        for (int i = 0; i < CompressItemSelect1.size(); i++) {
            File file = new File(CompressItemSelect1.get(i).getF_Path());
            //String namegsxsax = System.currentTimeMillis() + ".jpg";
            String namegsxsax = CompressItemSelect1.get(i).getF_Name();
            file_Driver.append(namegsxsax + ",");
            namegsxsax = "/images/" + namegsxsax;
            final_Driver = file_Driver.substring(0, file_Driver.length() - 1);
            TransferObserver transferObserver = transferUtility.upload("parkezly-images", namegsxsax, file);
            transferObserverListener(transferObserver);
        }

        if (mClearButton.isEnabled()) {
            for (int i = 0; i < 1; i++) {
                File file = photo;
                String namegsxsax = System.currentTimeMillis() + ".jpg";
                file_SignaturePad.append(namegsxsax + ",");
                namegsxsax = "/images/" + namegsxsax;
                final_SignaturePad = file_SignaturePad.substring(0, file_SignaturePad.length() - 1);
                TransferObserver transferObserver = transferUtility.upload("parkezly-images", namegsxsax, file);
                transferObserverListener(transferObserver);
            }
        }
    }

    public class getPermit_Subscription extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String responseStr = null;
        String managedlosturl = "_table/permit_subscription";
        Date current;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            String currentdate = sdf.format(new Date());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            try {
                current = format.parse(currentdate);
                Log.e("current_date", String.valueOf(current));
                System.out.println(current);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            //jsonValues1.put("id", "72");
            jsonValues1.put("user_id", user_id);
            jsonValues1.put("manager_type", Manager_type.toUpperCase());
            jsonValues1.put("manager_type_id", Manager_type_id);
            jsonValues1.put("twp_id", twp_id);
            jsonValues1.put("permit_id", township_id);
            jsonValues1.put("permit_num", Permit_nextnum);
            jsonValues1.put("full_address", full_address);
            jsonValues1.put("full_name", first_name + " " + last_name);
            jsonValues1.put("email", email);
            jsonValues1.put("phone", phone);
            jsonValues1.put("date_time", current);
            jsonValues1.put("date_action", current);
            jsonValues1.put("township_code", Townshipcode);
            jsonValues1.put("township_name", Township_name);
            jsonValues1.put("permit_type", Permit_type);
            jsonValues1.put("permit_name", Permit_name);
            if (CompressItemSelect.size() > 0) {
                jsonValues1.put("residency_proof", final_Residency);
            } else {
                jsonValues1.put("residency_proof", "");
            }
            jsonValues1.put("approved", "0");
            jsonValues1.put("status", "0");
            jsonValues1.put("paid", "0");
            jsonValues1.put("user_comments", "");
            jsonValues1.put("town_comments", "");
            if (CompressItemSelect1.size() > 0) {
                jsonValues1.put("drv_lic_proof", final_Driver);
            } else {
                jsonValues1.put("drv_lic_proof", "");
            }
            jsonValues1.put("logo", Official_logo);
            jsonValues1.put("scheme_type", "SUBSCRIPTION");
            jsonValues1.put("first_contact_date", current);
            jsonValues1.put("permit_status_image", "");
            jsonValues1.put("rate", Cost);
            jsonValues1.put("user_name", user_id);
            if (mClearButton.isEnabled()) {
                jsonValues1.put("signature", final_SignaturePad);
            } else {
                jsonValues1.put("signature", "");
            }
            jsonValues1.put("permit_validity", Expires_by);
            jsonValues1.put("permit_expires_on", Expires_Date);
            jsonValues1.put("doc_requirements", Requirements);
            jsonValues1.put("app_pdf_upload", "");
            jsonValues1.put("expired", "0");
            jsonValues1.put("renewable", Renewable);

            JSONObject json1 = new JSONObject(jsonValues1);
            Log.e("json1", " : " + json1.toString());
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("responseStr ", " : " + responseStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return responseStr;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && responseStr != null) {
                boolean errors = responseStr.indexOf("error") > 0;
                if (errors) {
                    Toast.makeText(getContext(), "Already exists...", Toast.LENGTH_LONG).show();
                    fragmentStack.clear();
                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    MyPermits_VehicleUser pay = new MyPermits_VehicleUser();
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();

                } else {
                    Toast.makeText(getContext(), "Successfully added", Toast.LENGTH_LONG).show();
                    try {
                        JSONObject jsonObject = new JSONObject(responseStr);
                        JSONArray jsonArray = jsonObject.getJSONArray("resource");
                        String jsonObject1 = jsonArray.getJSONObject(0).getString("id");
                        Log.e("jsonObject1", " : " + jsonObject1);
                        new getPermit_Subscription_Archive(jsonObject1).execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public class getPermit_Subscription_Archive extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String responseStr = null, permit_sbscription_id;
        String managedlosturl = "_table/permit_subscription_archive";
        Date current;

        public getPermit_Subscription_Archive(String jsonObject1) {
            permit_sbscription_id = jsonObject1;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            MultipartEntity reqEntity = null;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            String currentdate = sdf.format(new Date());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            try {
                current = format.parse(currentdate);
                Log.e("current_date", String.valueOf(current));
                System.out.println(current);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            //jsonValues1.put("id", "72");
            jsonValues1.put("permit_sbscription_id", permit_sbscription_id);
            jsonValues1.put("user_id", user_id);
            jsonValues1.put("manager_type", Manager_type.toUpperCase());
            jsonValues1.put("manager_type_id", Manager_type_id);
            jsonValues1.put("twp_id", twp_id);
            jsonValues1.put("permit_id", township_id);
            jsonValues1.put("permit_num", Permit_nextnum);
            jsonValues1.put("full_address", full_address);
            jsonValues1.put("full_name", first_name + " " + last_name);
            jsonValues1.put("email", email);
            jsonValues1.put("phone", phone);
            jsonValues1.put("date_time", current);
            jsonValues1.put("date_action", current);
            jsonValues1.put("township_code", Townshipcode);
            jsonValues1.put("township_name", Township_name);
            jsonValues1.put("permit_type", Permit_type);
            jsonValues1.put("permit_name", Permit_name);
            if (CompressItemSelect.size() > 0) {
                jsonValues1.put("residency_proof", final_Residency);
            } else {
                jsonValues1.put("residency_proof", "");
            }
            jsonValues1.put("approved", "0");
            jsonValues1.put("status", "0");
            jsonValues1.put("paid", "0");
            jsonValues1.put("user_comments", "");
            jsonValues1.put("town_comments", "");
            if (CompressItemSelect1.size() > 0) {
                jsonValues1.put("drv_lic_proof", final_Driver);
            } else {
                jsonValues1.put("drv_lic_proof", "");
            }
            jsonValues1.put("logo", Official_logo);
            jsonValues1.put("scheme_type", "SUBSCRIPTION");
            jsonValues1.put("first_contact_date", current);
            jsonValues1.put("permit_status_image", "");
            jsonValues1.put("rate", Cost);
            jsonValues1.put("user_name", user_id);
            if (mClearButton.isEnabled()) {
                jsonValues1.put("signature", final_SignaturePad);
            } else {
                jsonValues1.put("signature", "");
            }
            jsonValues1.put("permit_validity", Expires_by);
            jsonValues1.put("permit_expires_on", Expires_Date);
            jsonValues1.put("doc_requirements", Requirements);
            jsonValues1.put("app_pdf_upload", "");
            jsonValues1.put("expired", "0");
            jsonValues1.put("renewable", Renewable);

            JSONObject json1 = new JSONObject(jsonValues1);
            Log.e("json1", " : " + json1.toString());
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("responseStr ", " : " + responseStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return responseStr;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && responseStr != null) {
                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                MyPermits_VehicleUser pay = new MyPermits_VehicleUser();
                fragmentStack.clear();
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        }
    }

    public void transferObserverListener(final TransferObserver transferObserver) {
        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.name().equals("COMPLETED")) {
                    current_upload++;
                    if (totla_size == current_upload) {
                        new getPermit_Subscription().execute();
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                try {
                    int percentage = (int) (bytesCurrent / bytesTotal * 100);
                    Log.e("percentage ", " : " + percentage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", "error");
            }

        });
    }

    private class get_user_profile extends AsyncTask<String, String, String> {
        String login = "_table/user_profile";

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String ins_id = logindeatl.getString("id", "null");
        String session_id = logindeatl.getString("session_id", "");
        String emial_ID = logindeatl.getString("email", "");

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rl_progressbar.setVisibility(View.VISIBLE);
            progressbar.setVisibility(View.VISIBLE);
            try {
                login = "_table/user_profile?filter=(id%3D'" + URLEncoder.encode(ins_id, "utf-8") + "')";
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + login;
            Log.e("login_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            //post.setHeader("x-dreamfactory-session-token", session_id);

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject jso = new JSONObject(responseStr);
                    Log.e("jso : " + ins_id, " : " + jso.toString());
                    JSONArray jsonArray = jso.getJSONArray("resource");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        email = object.getString("email");
                        first_name = object.getString("fname");
                        last_name = object.getString("lname");
                        phone = object.getString("mobile");
                        lphone = object.getString("lphone");
                        displayName = object.getString("full_name");
                        full_address = object.getString("full_address");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && message.equals("")) {
                txt_Name.setText(first_name + " " + last_name);
                txt_Phone.setText(lphone);
                txt_Mobile.setText(phone);
                txt_Email.setText(email);
                txt_Address.setText(full_address);
            } else {
                show_Alert(message);
            }
        }
    }

    public void show_Alert(String mess) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Error");
        alertDialog.setMessage(mess);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        if (getActivity() != null) {
            alertDialog.show();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (cd.isConnectingToInternet()) {
            if (ProfileEdit) {
                try {
                    new get_user_profile().execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ProfileEdit = false;
            }

        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }
    }

}