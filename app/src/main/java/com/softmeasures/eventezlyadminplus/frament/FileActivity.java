package com.softmeasures.eventezlyadminplus.frament;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.MergeCursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.softmeasures.eventezlyadminplus.R;

import java.io.File;
import java.util.ArrayList;

public class FileActivity extends AppCompatActivity {

    private File sdcard;
    private ArrayList<String> filelist = new ArrayList<String>();

    RecyclerView rv_gallery2;
    static final int REQUEST_PERMISSION_KEY = 131;
    boolean isdoc = false;
    LinearLayout llNotFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file);

        rv_gallery2 = (RecyclerView) findViewById(R.id.rv_gallery2);
        llNotFound = (LinearLayout) findViewById(R.id.llNotFound);
        sdcard = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
        Bundle myIntent = getIntent().getExtras();
        if (myIntent != null) {
            if (myIntent.containsKey("isdoc")) {
                isdoc = myIntent.getBoolean("isdoc", false);
            }
        }

        findViewById(R.id.rl_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_PERMISSION_KEY);
        } else {
            LoadAlbum loadAlbumTask = new LoadAlbum();
            loadAlbumTask.execute();
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    class LoadAllFolder extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... args) {
            if (sdcard.isDirectory()) {
                File[] files = sdcard.listFiles();

                try {
                    for (File f : files) {
                        if (!f.isDirectory()) {
                            if (f.getName().endsWith(".doc") || f.getName().endsWith(".pdf") || f.getName().endsWith(".docx") || f.getName().endsWith(".jpg") || f.getName().endsWith(".png") || f.getName().endsWith(".jpeg")) {
                                filelist.add(f.getAbsolutePath());
                            }
                        } else {
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String xml) {

        }
    }

    public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.MyviewHolder> {

        private Context activity;
        private ArrayList<String> data;
        int lastPosition = -1;

        public AlbumAdapter(Context a, ArrayList<String> d) {
            activity = a;
            data = d;
        }

        public class MyviewHolder extends RecyclerView.ViewHolder {

            ImageView galleryImage;
            TextView tv_filenm;

            public MyviewHolder(View view) {
                super(view);
                galleryImage = (ImageView) view.findViewById(R.id.galleryImage);
                tv_filenm = (TextView) view.findViewById(R.id.tv_filenm);
            }
        }

        @Override
        public MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_album_row, parent, false);
            return new MyviewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyviewHolder holder, final int position) {
            holder.galleryImage.setId(position);
            holder.tv_filenm.setId(position);
            holder.tv_filenm.setSelected(true);

            try {
                if (isdoc) {
                    if (data.get(position).endsWith(".pdf")) {
                        Glide.with(activity).asBitmap()
                                .load(R.mipmap.pdf)
                                .into(holder.galleryImage);
                    } else if (data.get(position).endsWith(".doc") || data.get(position).endsWith(".docx")) {
                        Glide.with(activity).asBitmap()
                                .load(R.mipmap.docs)
                                .into(holder.galleryImage);
                    }
                }

                holder.tv_filenm.setText(new File(data.get(position)).getName());

            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.galleryImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent();
                    i.putExtra("path", data.get(position));
                    setResult(RESULT_OK, i);
                    finish();

                }
            });

            holder.tv_filenm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent();
                    i.putExtra("path", data.get(position));
                    setResult(RESULT_OK, i);
                    finish();

                }
            });

        }

        @Override
        public int getItemCount() {
            return data.size();
        }
    }

    class LoadAlbum extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(String... args) {
            String xml = "";

            String path = null;
            String videopath = null;
            String videoname = null;
            String album = null;
            String timestamp = null;
            String countPhoto = null;
            Uri uriExternal = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            Uri uriInternal = MediaStore.Images.Media.INTERNAL_CONTENT_URI;
            Uri uriInternalFile = MediaStore.Files.getContentUri("internal");
            Uri uriExternalFile = MediaStore.Files.getContentUri("external");

            String[] projection = {MediaStore.MediaColumns.DATA,
                    MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.MediaColumns.DATE_MODIFIED};

            String selection = MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_NONE;

            Cursor cursorExternal = getContentResolver().query(uriExternal, projection, "_data IS NOT NULL) GROUP BY (bucket_display_name",
                    null, null);
            Cursor cursorInternal = getContentResolver().query(uriInternal, projection, "_data IS NOT NULL) GROUP BY (bucket_display_name",
                    null, null);

            Cursor cursorInternalFile = getContentResolver().query(uriInternalFile, null, selection,
                    null, null);

            Cursor cursorExtFile = getContentResolver().query(uriExternalFile, null, selection,
                    null, null);

            Cursor cursor = new MergeCursor(new Cursor[]{cursorExternal, cursorInternal, cursorInternalFile, cursorExtFile});

            while (cursor.moveToNext()) {

                path = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA));

                if (isdoc) {
                    if (path.endsWith(".pdf") || path.endsWith(".doc") || path.endsWith(".docx")) {
                        filelist.add(path);
                        Log.e("extensionoffile", path);
                    }
                } else if (!isdoc) {
                    if (path.endsWith(".png") || path.endsWith(".jpg") || path.endsWith(".jpeg")) {
                        filelist.add(path);
                        Log.e("extensionoffile", path);
                    }
                }


            }
            cursor.close();

            return xml;
        }

        @Override
        protected void onPostExecute(String xml) {
            if (filelist.size() > 0) {
                GridLayoutManager gridLayoutManager = new GridLayoutManager(FileActivity.this, 1);
                AlbumAdapter adapter2 = new AlbumAdapter(FileActivity.this, filelist);
                rv_gallery2.setAdapter(adapter2);
                rv_gallery2.setLayoutManager(gridLayoutManager);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
                rv_gallery2.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION_KEY: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    LoadAlbum loadAlbumTask = new LoadAlbum();
                    loadAlbumTask.execute();
                } else {
                    Toast.makeText(FileActivity.this, "You must accept permissions.", Toast.LENGTH_LONG).show();
                }
            }
        }

    }


}
