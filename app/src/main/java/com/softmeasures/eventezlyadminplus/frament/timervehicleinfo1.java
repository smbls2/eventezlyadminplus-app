package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;
import com.google.android.gms.maps.model.StreetViewPanoramaLocation;
import com.google.android.gms.maps.model.StreetViewPanoramaOrientation;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.BounceAnimation;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.services.NotificationPublisher;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import static android.content.Context.ALARM_SERVICE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;


public class timervehicleinfo1 extends Fragment implements StreetViewPanorama.OnStreetViewPanoramaChangeListener, StreetViewPanorama.OnStreetViewPanoramaCameraChangeListener,
        StreetViewPanorama.OnStreetViewPanoramaClickListener, StreetViewPanorama.OnStreetViewPanoramaLongClickListener {

    public StreetViewPanorama mStreetViewPanorama;
    ProgressDialog pdialog;
    TextView txtplantno, txttimerrgiserexpiresdate, txtaprkat, txttimermaxtime, txttimer,
            txtaddress, txt_SelectedAddreess, txtlocationname;
    String platno, statename, lat, lang, currentdate, max_time, exit_date_time, hr, id, min, user_id, token, markertype, parking_free = "",
            fullsatename, locationname, address, exit_time, country, city, title, parkehere,
            parked_add, parking_type, lot_no, lot_row;
    RelativeLayout rltimer, rl_done, rl_deatil, rl_layout_map_view;
    String parked_time;
    CounterClass timer1;
    ConnectionDetector cd;
    double latitude = 0.0, longitude = 0.0;
    Double lat1 = 0.0, lang1 = 0.0;
    RelativeLayout rl_progessbar;

    SupportStreetViewPanoramaFragment streetViewPanoramaFragment;
    LatLng SYDNEY;
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;
    boolean isFinishing = false;
    ArrayList<item> parking_rules = new ArrayList<>();
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private Handler mHandler = new Handler();
    private long startTime = 0L;
    private Handler customHandler = new Handler();
    private StreetViewPanoramaView mStreetViewPanoramaView;
    private float x1, x2;
    private Handler mHandler1 = null;
    private Runnable mAnimation;
    String timeStamp;
    ArrayList<item> township_parking = new ArrayList<>();
    MyCountDownTimer mycount;
    GoogleMap googleMap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_timervehicle_pre, container, false);

        mHandler1 = new Handler();
        SupportMapFragment ffm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        ffm.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap1) {
                googleMap = googleMap1;
            }
        });

        rltimer = (RelativeLayout) view.findViewById(R.id.opnetimer);
        txttimer = (TextView) view.findViewById(R.id.time);

        rl_deatil = (RelativeLayout) view.findViewById(R.id.deatil);
        txtplantno = (TextView) view.findViewById(R.id.txtplate);
        txttimerrgiserexpiresdate = (TextView) view.findViewById(R.id.txtexpires);
        txtaprkat = (TextView) view.findViewById(R.id.txtparkedat);
        txtlocationname = (TextView) view.findViewById(R.id.txt_locationname);
        txtaddress = (TextView) view.findViewById(R.id.txt_addreess);
        txt_SelectedAddreess = (TextView) view.findViewById(R.id.txt_SelectedAddreess);
        txttimermaxtime = (TextView) view.findViewById(R.id.txtmaxtime);
        rl_done = (RelativeLayout) view.findViewById(R.id.rl_done);
        rl_layout_map_view = (RelativeLayout) view.findViewById(R.id.layoutmap1);
        rl_progessbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);

        rl_deatil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        rl_progessbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        rl_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentStack.clear();
                getActivity().onBackPressed();
            }
        });


        new getparkingrules().execute();

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = logindeatl.getString("id", "null");

        cd = new ConnectionDetector(getActivity());
        if (cd.isConnectingToInternet()) {
            final SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
            platno = sh.getString("platno", "");
            statename = sh.getString("state", "");
            currentdate = sh.getString("currentdate", "");
            exit_time = sh.getString("exit_date_time", "null");

            hr = sh.getString("hr", "");
            lat = sh.getString("lat", "40.7326");
            lang = sh.getString("log", "-73.4454");
            id = sh.getString("id", "null");
            min = sh.getString("min", "null");
            address = sh.getString("address", "null");
            locationname = sh.getString("locationname", "null");
            markertype = sh.getString("markertye", "null");
            max_time = sh.getString("max", "10");

            city = sh.getString("city", "city");
            country = sh.getString("country", "country");
            title = sh.getString("title", "null");
            parked_add = sh.getString("parked_address", "");
            parkehere = sh.getString("parkhere", "no");
            lot_no = sh.getString("lot_no", "");
            lot_row = sh.getString("lot_row", "");
            parking_type = sh.getString("parking_type", "null");
            parking_free = sh.getString("parkhere", "");
            parked_time = sh.getString("parked_time", "0");

            txtplantno.setText(platno + " " + statename);
            txttimerrgiserexpiresdate.setText("" + exit_time);
            txtaprkat.setText("" + currentdate);
            txtlocationname.setText(locationname + " " + lot_row + "" + lot_no);
            txtaddress.setText(address);
            txt_SelectedAddreess.setText(parked_add);

            if (max_time != null) {
                txttimermaxtime.setText(max_time + " Hours");
            }

            if (lat.equals("null") || lat1.equals("")) {
                lat1 = 0.0;
            } else {
                lat1 = Double.parseDouble(lat);
            }
            if (lang.equals("null") || lang.equals("")) {
                lang1 = 0.0;
            } else {
                lang1 = Double.parseDouble(lang);
            }
            showmap();

            SharedPreferences isexieornot = getActivity().getSharedPreferences("isexitornot", Context.MODE_PRIVATE);
            final SharedPreferences.Editor is = isexieornot.edit();
            is.putString("paltno", platno);
            is.putString("id", id);
            is.commit();

            SharedPreferences sh1 = getActivity().getSharedPreferences("token", Context.MODE_PRIVATE);
            token = sh1.getString("token", "null");

            new gettownship_parking().execute();

            if (!platno.equals("")) {
                //timer();
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Date dCurrentDate = new Date();
                    Date dExpDate = formatter.parse(exit_time);

                    long diff = dExpDate.getTime() - dCurrentDate.getTime();

                    new CountDownTimer(diff, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            String hms1 = String.format("%02d : %02d : %02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                            txttimer.setText(hms1);
                        }

                        @Override
                        public void onFinish() {
                            txttimer.setText("00 : 00 : 00");
                        }
                    }.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();

        }
        return view;
    }


    private Runnable updateTimerThread1 = new Runnable() {

        public void run() {

            if (isFinishing == false) {
                long tim2e = 0;
                Date date1 = null;
                TimeZone zone12 = TimeZone.getTimeZone("UTC");
                try {
                    String inputPattern1 = "yyyy-MM-dd HH:mm:ss";
                    SimpleDateFormat inputFormat1 = new SimpleDateFormat(inputPattern1);
                    SimpleDateFormat outputFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    inputFormat1.setTimeZone(zone12);
                    outputFormat1.setTimeZone(zone12);

                    String str1 = null;
                    String datef1 = currentdate;

                    date1 = inputFormat1.parse(currentdate);
                    tim2e = date1.getTime();
                    str1 = outputFormat1.format(date1);

                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Date value = formatter.parse(currentdate);

                    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //this format changeable
                    dateFormatter.setTimeZone(TimeZone.getDefault());
                    String localtime1 = dateFormatter.format(value);

                    //txtaprkat.setText(localtime1);
                    //txtaprkat.setText(str1);

                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }


                SimpleDateFormat df;
                df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                df.setTimeZone(zone12);
                Date date2 = null;
                try {

                    Calendar cal = Calendar.getInstance();
                    Date currentLocalTime = cal.getTime();

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    TimeZone zone = TimeZone.getTimeZone("UTC");
                    sdf.setTimeZone(zone12);
                    String currentdate = sdf.format(new Date());
                    //  String sss = txtaprkat.getText().toString();
                    String inputPattern1 = "yyyy-MM-dd HH:mm:ss";
                    SimpleDateFormat inputFormat1 = new SimpleDateFormat(inputPattern1);
                    SimpleDateFormat outputFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    inputFormat1.setTimeZone(zone12);
                    outputFormat1.setTimeZone(zone12);

                    String str1 = null;
                    String datef1 = currentdate;
                    date2 = inputFormat1.parse(currentdate);
                    String str2 = outputFormat1.format(date2);
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df11 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String formattedDate = df11.format(c.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                timeInMilliseconds = date2.getTime() - date1.getTime();

                long millis = timeInMilliseconds;
                String hms1 = String.format("%02d : %02d : %02d", TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                        TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                txttimer.setText(hms1);
                customHandler.postDelayed(this, 1);
            } else {
                onDestroy();
                customHandler.removeCallbacks(this);
            }


        }

    };
    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            if (isFinishing == false) {

                timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

                updatedTime = timeSwapBuff + timeInMilliseconds;

                int secs = (int) (updatedTime / 1000);
                int mins = secs / 60;
                int hor = mins / 60;
                secs = secs % 60;
                int milliseconds = (int) (updatedTime % 1000);
                String hms = String.format("%01d : %02d : %02d", hor, mins, secs);
                txttimer.setText(hms);
                customHandler.postDelayed(this, 1);
            } else {
                onDestroy();
                customHandler.removeCallbacks(this);
            }

        }

    };


    public void timer() {
        try {
            customHandler.removeCallbacks(updateTimerThread);
            customHandler.removeCallbacks(updateTimerThread1);

            int ii = Integer.parseInt(max_time);
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone zone12 = TimeZone.getTimeZone("UTC");
            final String currentDateandTime = currentdate;
            Date date11 = null;
            try {
                date11 = sdf.parse(currentDateandTime);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date11);
            calendar.add(Calendar.HOUR, ii);

            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();
            String expridate = sdf.format(calendar.getTime());
            Log.d("Time zone: ", tz.getDisplayName());
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Date date = null;
            String str = null;
            String datef = expridate;
            try {

                date = inputFormat.parse(expridate);
                str = outputFormat.format(date);

                SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                formatter1.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date value1 = formatter1.parse(expridate);
                SimpleDateFormat dateFormatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //this format changeable
                dateFormatter1.setTimeZone(TimeZone.getDefault());
                String localtime2 = dateFormatter1.format(value1);


                String inputPattern1 = "yyyy-MM-dd HH:mm:ss";
                SimpleDateFormat inputFormat1 = new SimpleDateFormat(inputPattern1);
                SimpleDateFormat outputFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                inputFormat1.setTimeZone(zone12);
                outputFormat1.setTimeZone(zone12);
                Date date1 = null;
                String str1 = null;
                String datef1 = currentdate;

                date1 = inputFormat1.parse(currentdate);
                str1 = outputFormat1.format(date1);


                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date value = formatter.parse(currentdate);

                SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //this format changeable
                dateFormatter.setTimeZone(TimeZone.getDefault());
                String localtime1 = dateFormatter.format(value);

                //txtaprkat.setText(localtime1);

                //  txtaprkat.setText(str1);

            } catch (ParseException e) {
                e.printStackTrace();
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }


            SimpleDateFormat df;
            df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            df.setTimeZone(zone12);
            Date date111 = null;
            Date date22 = null;
            try {
                String ss = expridate;
                date111 = df.parse(ss);
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            Date date2 = null;
            try {

                Calendar cal1 = Calendar.getInstance();
                Date currentLocalTime = cal1.getTime();

                try {
                    String sss = currentdate;
                    date2 = df.parse(sss);
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
            try {
                long diff = date111.getTime() - date2.getTime();
                long timeInSeconds = diff / 1000;
                long hours, minutes, seconds;
                hours = timeInSeconds / 3600;
                timeInSeconds = timeInSeconds - (hours * 3600);
                minutes = timeInSeconds / 60;
                timeInSeconds = timeInSeconds - (minutes * 60);
                seconds = timeInSeconds;
                int day = (int) (hours / 24);
                long date_in_notification = 0;
                SimpleDateFormat df1;
                df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                int parking_id = Integer.parseInt(id);
                if (hours > 0) {

                    if (diff > 2700000) {
                        date_in_notification = diff - 2700000;
                        Calendar timeout = Calendar.getInstance();
                        int gg = (int) date_in_notification;
                        String dddq = df1.format(timeout.getTime());
                        timeout.add(Calendar.MILLISECOND, gg);
                        String ddd = df1.format(timeout.getTime());
                        Log.e("date1", ddd);
                        //timeout.setTimeInMillis(date_in_notification);
                        long ff = timeout.getTimeInMillis();
                        scheduleNotification(getNotification("Alert Parkezly! Parking is going to expire in 45 mints.", parking_id + 1), ff, parking_id + 1);
                    }
                    if (diff > 1800000) {
                        date_in_notification = diff - 1800000;
                        Calendar timeout = Calendar.getInstance();
                        int gg = (int) date_in_notification;
                        String dddq = df1.format(timeout.getTime());
                        timeout.add(Calendar.MILLISECOND, gg);
                        String ddd = df1.format(timeout.getTime());
                        Log.e("date2", ddd);
                        //timeout.setTimeInMillis(date_in_notification);
                        long ff = timeout.getTimeInMillis();
                        scheduleNotification(getNotification("Alert Parkezly! Parking is going to expire in 30 mints.", parking_id + 2), ff, parking_id + 2);
                    }
                    if (diff > 900000) {
                        date_in_notification = diff - 900000;
                        Calendar timeout = Calendar.getInstance();
                        int gg = (int) date_in_notification;
                        String dddq = df1.format(timeout.getTime());
                        timeout.add(Calendar.MILLISECOND, gg);
                        String ddd = df1.format(timeout.getTime());
                        Log.e("date3", ddd);
                        //timeout.setTimeInMillis(date_in_notification);
                        long ff = timeout.getTimeInMillis();
                        scheduleNotification(getNotification("Alert Parkezly! Parking is going to expire in 15 mints.", parking_id + 3), ff, parking_id + 3);
                        //scheduleNotification(getNotification("15 second delay"), date_in_notification,3);
                    }

                    // timer = new vchome.CounterClass(diff, 1000, id);
                    timer1 = new CounterClass(diff, 1000);
                    //timer.start();
                    timer1.start();
                    MyCountDownTimer ss = new MyCountDownTimer(3000, 1000);
                    // ss.start();
                } else if (minutes > 0) {
                    if (diff > 2700000) {
                        date_in_notification = diff - 2700000;
                        Calendar timeout = Calendar.getInstance();
                        int gg = (int) date_in_notification;
                        String dddq = df1.format(timeout.getTime());
                        timeout.add(Calendar.MILLISECOND, gg);
                        String ddd = df1.format(timeout.getTime());
                        Log.e("date1", ddd);
                        //timeout.setTimeInMillis(date_in_notification);
                        long ff = timeout.getTimeInMillis();
                        scheduleNotification(getNotification("Alert Parkezly! Parking is going to expire in 45 mints.", parking_id + 1), ff, parking_id + 1);
                    }
                    if (diff > 1800000) {
                        date_in_notification = diff - 1800000;
                        Calendar timeout = Calendar.getInstance();
                        int gg = (int) date_in_notification;
                        String dddq = df1.format(timeout.getTime());
                        timeout.add(Calendar.MILLISECOND, gg);
                        String ddd = df1.format(timeout.getTime());
                        Log.e("date2", ddd);
                        //timeout.setTimeInMillis(date_in_notification);
                        long ff = timeout.getTimeInMillis();
                        scheduleNotification(getNotification("Alert Parkezly! Parking is going to expire in 30 mints.", parking_id + 1), ff, parking_id + 2);
                    }
                    if (diff > 900000) {
                        date_in_notification = diff - 900000;
                        Calendar timeout = Calendar.getInstance();
                        int gg = (int) date_in_notification;
                        String dddq = df1.format(timeout.getTime());
                        timeout.add(Calendar.MILLISECOND, gg);
                        String ddd = df1.format(timeout.getTime());
                        Log.e("date3", ddd);
                        //timeout.setTimeInMillis(date_in_notification);
                        long ff = timeout.getTimeInMillis();
                        scheduleNotification(getNotification("Alert Parkezly! Parking is going to expire in 15 mints.", parking_id + 3), ff, parking_id + 3);
                        //scheduleNotification(getNotification("15 second delay"), date_in_notification,3);
                    }
                    //timer = new vchome.CounterClass(diff, 1000, id);
                    timer1 = new CounterClass(diff, 1000);
                    //timer.start();
                    timer1.start();
                    mycount = new MyCountDownTimer(3000, 1000);
                    // mycount.start();
                } else if (seconds > 0) {
                    // timer = new vchome.CounterClass(diff, 1000, id);
                    timer1 = new CounterClass(diff, 1000);
                    //timer.start();
                    timer1.start();

                    mycount = new MyCountDownTimer(3000, 1000);
                    // mycount.start();
                } else {
                    mycount = new MyCountDownTimer(3000, 1000);
                    //  mycount.start();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception E) {
            E.printStackTrace();
        }
    }

    @Override
    public void onResume() {

        super.onResume();
    }

    @Override
    public void onPause() {

        super.onPause();
    }

    public void showmap() {
        SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        // final GoogleMap googleMap = fm.getMap();
        fm.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap1) {
                googleMap = googleMap1;

                /*Iterator myVeryOwnIterator = statefullname.keySet().iterator();
                while (myVeryOwnIterator.hasNext()) {
                    String key = (String) myVeryOwnIterator.next();
                    if (key.equals(statename)) {
                        fullsatename = (String) statefullname.get(key);
                        Log.e("statename", fullsatename);
                        break;
                    }
                }*/


                final LatLng position = new LatLng(lat1, lang1);

                MarkerOptions options = new MarkerOptions();
                Marker melbourne = null;
                // Setting position for the MarkerOptions
                options.position(position);
                options.title(platno);
                options.snippet(fullsatename);
                if (markertype.equals("free") || markertype.equals("anywhere") || parking_type.equals("anywhere") || parking_type.equals("random") || parking_type.equals("otherPartner") || parking_type.equals("google") || parking_type.equals("googlePartner") || markertype.equals("managed_free")) {
                    melbourne = googleMap.addMarker(new MarkerOptions()
                            .title(platno)
                            .snippet(fullsatename)
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.free_star))
                            .position(position));
                } else if (markertype.contains("managed")) {
                    melbourne = googleMap.addMarker(new MarkerOptions()
                            .title(platno)
                            .snippet(fullsatename)
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.rsz_1manages_star))
                            .position(position));
                    options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.rsz_1manages_star));
                } else if (markertype.equals("Paid")) {
                    melbourne = googleMap.addMarker(new MarkerOptions()
                            .title(platno)
                            .snippet(fullsatename)
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.paid_star))
                            .position(position));
                } else if (markertype.equals("guest")) {
                    melbourne = googleMap.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.rsz_1manages_star))
                            .position(position));
                }
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 18));
                final long start = SystemClock.uptimeMillis();
                final long duration = 1500L;

                // Cancels the previous animation
                mHandler1.removeCallbacks(mAnimation);
                if (melbourne != null) {
                    mAnimation = new BounceAnimation(start, duration, melbourne, mHandler);
                    mHandler1.post(mAnimation);
                }

                googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(Marker arg0) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(Marker arg0) {
                        View v = getActivity().getLayoutInflater().inflate(R.layout.otherparkingpin, null);
                        LatLng latLng = arg0.getPosition();
                        TextView txttitle = (TextView) v.findViewById(R.id.txtvalues);
                        TextView txtcontaint = (TextView) v.findViewById(R.id.txttotle);
                        String sa = arg0.getSnippet();
                        txtcontaint.setText(arg0.getSnippet());
                        txttitle.setText(arg0.getTitle());
                        return v;
                    }
                });
            }
        });
    }

    public void ActionStartsHere() {
        againStartGPSAndSendFile();
    }

    public void againStartGPSAndSendFile() {
        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.e("countdown", "Start:-" + millisUntilFinished);
            }

            @Override
            public void onFinish() {
                //ActionStartsHere();
                showstreetmap();

            }

        }.start();
    }

    @Override
    public void onStreetViewPanoramaCameraChange(StreetViewPanoramaCamera streetViewPanoramaCamera) {

    }

    @Override
    public void onStreetViewPanoramaChange(StreetViewPanoramaLocation streetViewPanoramaLocation) {
        if (streetViewPanoramaLocation != null && streetViewPanoramaLocation.links != null) {
            // location is present
            Log.e("park for free", "location  changed");
        } else {
            // location not available
            Log.e("park for free", "location  not chmage");
        }
    }

    @Override
    public void onStreetViewPanoramaClick(StreetViewPanoramaOrientation orientation) {
        try {
            Point point = mStreetViewPanorama.orientationToPoint(orientation);

            if (point != null) {
                try {

                    if (mStreetViewPanorama.getLocation() != null) {
                        showstreetmap();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                showstreetmap();
            }
        } catch (Exception E) {
            showstreetmap();
        }
    }

    @Override
    public void onStreetViewPanoramaLongClick(StreetViewPanoramaOrientation streetViewPanoramaOrientation) {

    }

    public void showstreetmap() {
        try {
            streetViewPanoramaFragment =
                    (SupportStreetViewPanoramaFragment)
                            getChildFragmentManager().findFragmentById(R.id.streetviewpanorama);
            SYDNEY = new LatLng(lat1, lang1);
            mStreetViewPanorama = null;
            mStreetViewPanoramaView = new StreetViewPanoramaView(getActivity());
            streetViewPanoramaFragment.getStreetViewPanoramaAsync(
                    new OnStreetViewPanoramaReadyCallback() {
                        @Override
                        public void onStreetViewPanoramaReady(final StreetViewPanorama panorama) {
                            mStreetViewPanorama = null;
                            try {
                                mStreetViewPanorama = panorama;

                                mStreetViewPanorama.setOnStreetViewPanoramaChangeListener(
                                        timervehicleinfo1.this);
                                mStreetViewPanorama.setOnStreetViewPanoramaCameraChangeListener(
                                        timervehicleinfo1.this);
                                mStreetViewPanorama.setOnStreetViewPanoramaClickListener(
                                        timervehicleinfo1.this);
                                mStreetViewPanorama.setOnStreetViewPanoramaLongClickListener(
                                        timervehicleinfo1.this);

                                mStreetViewPanorama.setPosition(SYDNEY);
                                Log.e("data", "secondtimelod");
                            } catch (Exception e) {
                            }
                        }

                    });

        } catch (Exception e) {
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (isFinishing) {
            customHandler.removeCallbacks(updateTimerThread);
            customHandler.removeCallbacks(updateTimerThread1);
        }
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @SuppressLint("NewApi")
    public class CounterClass extends CountDownTimer {

        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            txttimer.setText("00 : 00 : 00");
        }

        @SuppressLint("NewApi")
        @TargetApi(Build.VERSION_CODES.GINGERBREAD)
        @Override
        public void onTick(long millisUntilFinished) {

            long millis = millisUntilFinished;
            String hms = String.format("%02d : %02d : %02d", TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
            Log.e("hms ", " : " + hms);
            long h = TimeUnit.MILLISECONDS.toHours(millis);
            long min = TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis));

            long s = TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis));
            txttimer.setText(hms);
        }
    }


    public class getparkingrules extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String parkingurl = "_table/township_parking_rules";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            parking_rules.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        String locationcode1 = c.getString("location_code");
                        String id = c.getString("id");
                        // String date_time = c.getString("date_time");
                        // String location_name = c.getString("location_name");
                        String time_rule = c.getString("time_rule");
                        String day_rule = c.getString("day_type");
                        String max_time = c.getString("max_duration");
                        // String enforced = c.getString("enforced");
                        //String date_special_enforce = c.getString("date_special_enforce");
                        String custom_notice = c.getString("custom_notice");
                        String active = c.getString("active");
                        // String date = c.getString("date");
                        // String city = c.getString("city");
                        // String state = c.getString("state");
                        String pricing = c.getString("pricing");
                        String pricing_duration = c.getString("pricing_duration");
                        //String zip_code = c.getString("zip_code");
                        String start_time = c.getString("start_time");
                        String end_time = c.getString("end_time");
                        String this_day = c.getString("day_type").toLowerCase();
                        // String parking_times = c.getString("parking_times");
                        //String no_parking_times = c.getString("no_parking_times");
                        //String start_end_rule = c.getString("start_end_rule");
                        // String this_hour = c.getString("this_hour");
                        String max_hours = c.getString("duration_unit");
                        String start_hour = c.getString("start_hour");
                        String end_hour = c.getString("end_hour");
                        item1.setId(id);
                        item1.setLocation_code(locationcode1);
                        item1.setTime_rule(time_rule);
                        item1.setDay_rule(day_rule);
                        item1.setMax_time(max_time);
                        item1.setCustom_notice(custom_notice);
                        item1.setActive(active);
                        item1.setPricing(pricing);
                        item1.setPricing_duration(pricing_duration);
                        item1.setStart_time(start_time);
                        item1.setEnd_time(end_time);
                        item1.setThis_day(this_day);
                        item1.setMax_hours(max_hours);
                        item1.setStart_hour(start_hour);
                        item1.setEnd_hour(end_hour);
                        item1.setLoationcode1(locationcode1);
                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item1.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }

                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item1.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }

                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item1.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }

                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item1.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }

                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item1.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }

                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item1.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item1.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }

                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item1.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }

                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item1.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }

                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item1.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }

                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item1.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }
                        parking_rules.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs;
            if (getActivity() != null && json != null) {
            }
            super.onPostExecute(s);
        }
    }

    public class gettownship_parking extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String managedlosturl = "_table/township_parking_partners";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("find_parking_nearby", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));


            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);

                    json1 = json.getJSONArray("resource");

                    for (int i = 0; i < json1.length(); i++) {
                        item item = new item();
                        JSONObject josnget = json1.getJSONObject(i);
                        item.setRenew(josnget.getString("renewable"));
                        item.setLocation_code(josnget.getString("location_code"));
                        item.setTownshipcode(josnget.getString("township_id"));
                        township_parking.add(item);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null && township_parking.size() > 0) {
                for (int i = 0; i < township_parking.size(); i++) {
                    String loc_code = township_parking.get(i).getLocation_code();
                    if (loc_code.equals(locationname)) {
                        String renew = township_parking.get(i).getRenew();
                        renew = renew != null ? (!renew.equals("null") ? (!renew.equals("") ? renew : "") : "") : "";
                        if (renew.equals("1")) {

                        } else {

                        }
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    private void scheduleNotification(Notification notification, long delay, int postion) {

        Intent notificationIntent = new Intent(getActivity(), NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, postion);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), postion, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        GregorianCalendar hbhbh = new GregorianCalendar();
        hbhbh.setTimeInMillis(delay);
        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, hbhbh.getTimeInMillis(), pendingIntent);
    }

    private Notification getNotification(String content, int postion) {
        long[] vibrate = {0, 100, 200, 300};
        String strtitle = getActivity().getString(R.string.customnotificationtitle);
        // Set Notification Text
        String strtext = "Alert Parkezly! Parking is almost expired.";
        Intent intent = new Intent(getActivity().getApplicationContext(), vchome.class);
        intent.putExtra("title", strtitle);
        intent.putExtra("text", strtext);
        intent.putExtra("id", id);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pIntent = PendingIntent.getActivity(getActivity().getApplicationContext(), postion, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        androidx.core.app.NotificationCompat.Builder builder = new NotificationCompat.Builder(getActivity())
                .setSmallIcon(R.mipmap.icon)
                .setTicker(getActivity().getString(R.string.customnotificationticker))
                .setVibrate(vibrate)
                .setContentIntent(pIntent)
                .setContentTitle(strtitle)
                .setContentText(content)
                .setAutoCancel(true);
        return builder.build();
    }

    public void canclealarm(int id) {
        Intent notificationIntent = new Intent(getActivity(), NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, id);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(getActivity(), id + 1, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent alarmIntent1 = PendingIntent.getBroadcast(getActivity(), id + 2, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent alarmIntent3 = PendingIntent.getBroadcast(getActivity(), id + 3, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
        alarmManager.cancel(alarmIntent);
        alarmIntent.cancel();
        alarmManager.cancel(alarmIntent1);
        alarmIntent1.cancel();
        alarmManager.cancel(alarmIntent3);
        alarmIntent3.cancel();
    }

    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long startTime, long interval) {

            super(startTime, interval);

        }

        @Override
        public void onFinish() {
            if (getActivity() != null) {
            }
            Log.e("time", "off");

        }

        @Override
        public void onTick(long millisUntilFinished) {
            Log.e("timer", "on");

        }

    }


}
