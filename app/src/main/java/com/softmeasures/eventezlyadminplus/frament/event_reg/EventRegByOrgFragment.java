package com.softmeasures.eventezlyadminplus.frament.event_reg;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragEventRegByOrgBinding;

public class EventRegByOrgFragment extends BaseFragment {

    private FragEventRegByOrgBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_event_reg_by_org, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {

        binding.llBtnCommercial.setOnClickListener(v -> {
            openEventsScreen("Commercial");
        });
        binding.llBtnTownship.setOnClickListener(v -> {
            openEventsScreen("Township");
        });
        binding.llBtnOthers.setOnClickListener(v -> {
            openEventsScreen("Other");
        });

    }

    private void openEventsScreen(String type) {

        EventRegByOrgEventsFragment frag = new EventRegByOrgEventsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        frag.setArguments(bundle);
        replaceFragment(frag, "eventRegByOrgEvents");
    }

    @Override
    protected void initViews(View v) {

    }
}
