package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class traffic extends Fragment {
    ProgressDialog pdialog;
    ArrayList<item> permitsarray = new ArrayList<>();
    CustomAdaptercity adpater;
    ListView listofvehicleno;
    TextView txttitle, txtbal;
    String cuurentamount;
    int i = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View view = inflater.inflate(R.layout.fragment_traffic, container, false);
        listofvehicleno = (ListView) view.findViewById(R.id.listofvehicle);
        txttitle = (TextView) view.findViewById(R.id.txttitte);

        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Thin.ttf");
        txttitle.setTypeface(type);
        return view;
    }

    public class gettransaction extends AsyncTask<String, String, String> {

        JSONObject json, json1;

        @Override
        protected void onPreExecute() {

            pdialog = new ProgressDialog(getActivity());
            pdialog.setMessage("Loading.....");
            pdialog.setIndeterminate(false);
            pdialog.setCancelable(false);
            pdialog.show();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            String url = "http://100.12.26.176:8006/api/v2/pzly01live7/_table/user_wallet?order=date_time%20DESC";

            DefaultHttpClient client = new DefaultHttpClient();


            HttpGet post = new HttpGet(url);

            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", "dbed451c5e4e1518d301c118ffe078ca16a2c287d5efff98515b938538abb5b5");

//this is your response:
            HttpResponse response = null;

            try {
                response = client.execute(post);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);


                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);

                        String amount = c.getString("add_amt");
                        String date = c.getString("date_time");
                        String paidamount = c.getString("last_paid_amt");
                        cuurentamount = c.getString("current_balance");

                        item.setAmount(amount);
                        item.setExdate(date);
                        item.setPaidamount(paidamount);

                        permitsarray.add(item);

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (pdialog != null)
                pdialog.dismiss();
            Resources rs;
            if (getActivity() != null && json != null) {

                adpater = new CustomAdaptercity(getActivity(), permitsarray, rs = getResources());
                listofvehicleno.setAdapter(adpater);
            }
            super.onPostExecute(s);

        }


    }

    public class CustomAdaptercity extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdaptercity(Activity a, ArrayList<item> d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;
            data = d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;


            if (convertView == null) {

                /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
                vi = inflater.inflate(R.layout.mywallet, null);
                /****** View Holder Object to contain tabitem.xml file elements ******/

                holder = new ViewHolder();

                holder.txtdate = (TextView) vi.findViewById(R.id.txtwalletdate);
                holder.txtpayamont = (TextView) vi.findViewById(R.id.txtpayamount);
                holder.txtpaymenttype = (TextView) vi.findViewById(R.id.txtwalletpaymenttype);

                /************  Set holder with LayoutInflater ************/
                vi.setTag(holder);
                vi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       /* platno=holder.txtplatno.getText().toString();
                        statename=holder.state.getText().toString();
                        id=holder.txtid.getText().toString();
                        editetxtplatno.setText(platno);
                        addvehicle.setVisibility(View.VISIBLE);*/

                    }
                });
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
                holder.txtdate.setText("No Data");

            } else {
                /***** Get each Model object from Arraylist ********/
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                /************  Set Model values in Holder elements ***********/
                String cat = tempValues.getExdate();
                if (cat == null) {
                    holder.txtdate.setVisibility(View.GONE);


                } else if (cat != null) {

                    Typeface thin = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Thin.ttf");
                    holder.txtdate.setTypeface(thin);
                    holder.txtpayamont.setTypeface(thin);
                    holder.txtpaymenttype.setTypeface(thin);
                    String inputPattern = "yyyy-MM-dd HH:mm:ss";
                    SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                    SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date date = null;
                    String str = null;
                    String datef = tempValues.getExdate();
                    try {
                        if (datef.equals("0000-00-00 00:00:00")) {
                            holder.txtdate.setText("");
                        } else {

                            date = inputFormat.parse(tempValues.getExdate());
                            str = outputFormat.format(date);
                            holder.txtdate.setText(str);
                            String amu = tempValues.getAmount();
                            txtbal.setText("$" + cuurentamount);
                            if (amu.equals("")) {
                                holder.txtpaymenttype.setText("Parking Payment");
                            } else {
                                holder.txtpaymenttype.setText("Deposit");
                            }
                            String am = tempValues.getPaidamount();
                            if (am.equals("null")) {
                                holder.txtpayamont.setText("$0.00");
                            } else {
                                holder.txtpayamont.setText("$" + tempValues.getPaidamount());
                            }

                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }


                }


            }

            return vi;


        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {


            //  public ImageView image;
            public TextView txtdate, txtpaymenttype, txtpayamont;
            RelativeLayout rldata;


        }
    }
}
