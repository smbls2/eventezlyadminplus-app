package com.softmeasures.eventezlyadminplus.frament.event;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragEventAddMainViewBinding;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.EventInfoAddEditFragment;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.EventLocationAddEditFragment;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.EventMediaAddEditFragment;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.EventParkingLocationAddEditFragment;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.EventPricingLimitFragment;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.EventSettingsAddEditFragment;
import com.softmeasures.eventezlyadminplus.frament.locations.SelectAddressFragment;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.Manager;

import java.util.ArrayList;
import java.util.List;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_LOCATION;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_VIRTUALLY;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_BOTH;

public class EventAddMainViewFragment extends BaseFragment {

    public static FragEventAddMainViewBinding binding;
    public static TabAdapter adapter;
    public static EventInfoAddEditFragment infoAddEditFragment;
    public static EventLocationAddEditFragment locationAddEditFragment;
    public static EventParkingLocationAddEditFragment parkingLocationAddEditFragment;
    public static EventSettingsAddEditFragment settingsAddEditFragment;
    public static EventMediaAddEditFragment eventMediaAddEditFragment;
    public static EventPricingLimitFragment pricingLimitFragment;


    public static boolean isEdit = false;
    public static EventDefinition eventDefinition;
    public static Manager selectedManager;
    public static boolean isRepeat = false;
    public static int eventLogiType = EVENT_TYPE_AT_LOCATION;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            eventLogiType = getArguments().getInt("eventLogiType", EVENT_TYPE_AT_LOCATION);
            isEdit = getArguments().getBoolean("isEdit", false);
            isRepeat = getArguments().getBoolean("isRepeat", false);
            selectedManager = new Gson().fromJson(getArguments().getString("selectedManager"), Manager.class);
            eventDefinition = new Gson().fromJson(getArguments().getString("eventDetails"), EventDefinition.class);
            if (eventDefinition != null) {
                Log.e("#DEBUG", "   EventAddEditFragment:  eventDetails:  " + new Gson().toJson(eventDefinition));
                eventLogiType = eventDefinition.getEvent_logi_type();
            }
            if (selectedManager != null) {
                Log.e("#DEBUG", "   EventAddEditFragment:  selectedManager:  " + new Gson().toJson(selectedManager));
            }
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_event_add_main_view, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    private void openCoveredLocation() {
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        CoveredLocationsFragment frag = new CoveredLocationsFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isEdit", false);
        bundle.putString("selectedManager", new Gson().toJson(EventAddMainViewFragment.selectedManager));
        frag.setArguments(bundle);
        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.add(R.id.My_Container_1_ID, frag);
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(frag);
        ft.commitAllowingStateLoss();
    }

    private void openSelectAddress() {
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        SelectAddressFragment pay = new SelectAddressFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isEvent", true);
        pay.setArguments(bundle);
        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.add(R.id.My_Container_1_ID, pay);
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(pay);
        ft.commitAllowingStateLoss();
    }

    @Override
    protected void updateViews() {

        if (isEdit) {
            if (isRepeat) binding.tvTitle.setText("Repeat Event Details");
            else binding.tvTitle.setText("Edit Event Details");
        } else {
            binding.tvTitle.setText("Add Event Details");
            eventDefinition = new EventDefinition();
            eventDefinition.setIs_parking_allowed(true);
        }

    }

    @Override
    protected void setListeners() {

        binding.btnSelectAddress.setOnClickListener(v -> {
            openSelectAddress();
        });

        binding.btnCoveredAddress.setOnClickListener(v -> {
            openCoveredLocation();
        });


    }

    @Override
    protected void initViews(View v) {

        infoAddEditFragment = new EventInfoAddEditFragment();
        infoAddEditFragment.setArguments(getArguments());
        pricingLimitFragment = new EventPricingLimitFragment();
        pricingLimitFragment.setArguments(getArguments());
        locationAddEditFragment = new EventLocationAddEditFragment();
        locationAddEditFragment.setArguments(getArguments());
        parkingLocationAddEditFragment = new EventParkingLocationAddEditFragment();
        parkingLocationAddEditFragment.setArguments(getArguments());
        settingsAddEditFragment = new EventSettingsAddEditFragment();
        settingsAddEditFragment.setArguments(getArguments());

        eventMediaAddEditFragment = new EventMediaAddEditFragment();
        eventMediaAddEditFragment.setArguments(getArguments());

        adapter = new TabAdapter(getChildFragmentManager());
        adapter.addFragment(settingsAddEditFragment, "SETTINGS"); //0
        adapter.addFragment(infoAddEditFragment, "EVENT INFO"); //1
        adapter.addFragment(pricingLimitFragment, "PRICING & LIMIT"); //2
        if (eventLogiType == EVENT_TYPE_AT_VIRTUALLY) {
            adapter.addFragment(eventMediaAddEditFragment, "EVENT MEDIA LINKS"); //3
            adapter.addFragment(locationAddEditFragment, "EVENT TIMES"); //4
            binding.viewPager.setAdapter(adapter);
            binding.viewPager.setOffscreenPageLimit(4);
        }
        if (eventLogiType == EVENT_TYPE_AT_LOCATION) {
            adapter.addFragment(locationAddEditFragment, "EVENT LOCATION & TIMES");  //3
            adapter.addFragment(parkingLocationAddEditFragment, "PARKING LOCATION & TIMES"); //4
            binding.viewPager.setAdapter(adapter);
            binding.viewPager.setOffscreenPageLimit(5);
        }
        if (eventLogiType == EVENT_TYPE_BOTH) {
            adapter.addFragment(eventMediaAddEditFragment, "EVENT MEDIA LINKS"); //3
            adapter.addFragment(locationAddEditFragment, "EVENT LOCATION & TIMES"); //4
            adapter.addFragment(parkingLocationAddEditFragment, "PARKING LOCATION & TIMES");  //5
            binding.viewPager.setAdapter(adapter);
            binding.viewPager.setOffscreenPageLimit(6);
        }
        binding.tabLayout.setupWithViewPager(binding.viewPager);


    }

    public class TabAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        TabAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getItemPosition(Object object) {
            int index = mFragmentList.indexOf(object);

            if (index == -1)
                return POSITION_NONE;
            else
                return index;
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public void removeFragment(Fragment fragment, String title, int pos) {
            mFragmentList.remove(pos);
            mFragmentTitleList.remove(pos);
        }

        public void addFragmentPosition(Fragment fragment, String title, int pos) {
            mFragmentList.add(pos, fragment);
            mFragmentTitleList.add(pos, title);
        }

        public void addFragmentPosition(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
