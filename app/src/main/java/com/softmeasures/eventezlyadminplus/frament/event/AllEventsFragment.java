package com.softmeasures.eventezlyadminplus.frament.event;

import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragAllEventsBinding;
import com.softmeasures.eventezlyadminplus.databinding.PopupEventOptionTypeBinding;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.EventOptionTypeDialog;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.EventSettingsAddEditFragment;
import com.softmeasures.eventezlyadminplus.interfaces.EventTypeListener;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.Manager;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_LOCATION;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_VIRTUALLY;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_BOTH;

public class AllEventsFragment extends BaseFragment implements EventTypeListener {

    public final String TAG = "#DEBUG ALLEvent";
    public static Manager selectedManager;
    private FragAllEventsBinding binding;
    private ArrayList<EventDefinition> eventDefinitions = new ArrayList<>();
    private EventsAdapter eventsAdapter;
    private String parkingType = "";
    public static boolean isUpdated = false;
    private boolean isCheckIn = false;
    private int selectedItemPosition = -1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            parkingType = getArguments().getString("parkingType");
            isCheckIn = getArguments().getBoolean("isCheckIn", false);
            selectedManager = new Gson().fromJson(getArguments().getString("selectedManager"), Manager.class);
            if (selectedManager != null) {
                Log.e("#DEBUG", "   AllEventsFragment:  selectedManager:  " + new Gson().toJson(selectedManager));
            }
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_all_events, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
        fetchEvents();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isUpdated) {
            isUpdated = false;
            fetchEvents();
        }
    }

    @Override
    public void onNext(int type, boolean isEdit) {
        if (isEdit) {

            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            EventSettingsAddEditFragment frag = new EventSettingsAddEditFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isEdit", true);
            bundle.putString("selectedManager", new Gson().toJson(selectedManager));
            EventDefinition eventDefinition = eventDefinitions.get(selectedItemPosition);
            eventDefinition.setEvent_logi_type(type);
            bundle.putString("eventDetails", new Gson().toJson(eventDefinition));
            bundle.putInt("eventLogiType", type);
            frag.setArguments(bundle);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, frag);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(frag);
            ft.commitAllowingStateLoss();

        } else {
            openAddEditEvent(type);
        }
    }

//    private class fetchEvents extends AsyncTask<String, String, String> {
//        JSONObject json;
//        JSONArray json1;
//        String eventDefUrl = "_table/event_definitions";
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            binding.swipeRefreshLayout.setRefreshing(true);
//            eventDefinitions.clear();
//            if (selectedManager != null) {
////                eventDefUrl = "_table/event_definitions?filter=company_id=" + selectedManager.getId();
//                eventDefUrl = "_table/event_definitions?filter=(company_id%3D" + selectedManager.getId()
//                        + ")%20AND%20(manager_type_id%3D" + selectedManager.getManager_type_id() + ")";
//            }
////            if (selectedManager != null && selectedManager.getTownship_code() != null
////                    && !TextUtils.isEmpty(selectedManager.getTownship_code())
////                    && !selectedManager.getTownship_code().equals("null")) {
////                eventDefUrl = "_table/event_definitions?filter=township_code=" + selectedSpot.getTownship_code();
////            }
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            String url = getString(R.string.api) + getString(R.string.povlive) + eventDefUrl;
//            Log.e("#DEBUG", "      fetchEvents:  " + url);
//            DefaultHttpClient client = new DefaultHttpClient();
//            HttpGet post = new HttpGet(url);
//            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
//            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
//            HttpResponse response = null;
//            try {
//                response = client.execute(post);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            if (response != null) {
//                HttpEntity resEntity = response.getEntity();
//                String responseStr = null;
//                try {
//                    responseStr = EntityUtils.toString(resEntity).trim();
//                    json = new JSONObject(responseStr);
//                    JSONArray jsonArray = json.getJSONArray("resource");
//
//                    for (int j = 0; j < jsonArray.length(); j++) {
//                        EventDefinition eventDefinition = new EventDefinition();
//                        JSONObject object = jsonArray.getJSONObject(j);
//                        if (object.has("id")
//                                && !TextUtils.isEmpty(object.getString("id"))
//                                && !object.getString("id").equals("null")) {
//                            eventDefinition.setId(object.getInt("id"));
//                        }
//                        if (object.has("date_time")
//                                && !TextUtils.isEmpty(object.getString("date_time"))
//                                && !object.getString("date_time").equals("null")) {
//                            eventDefinition.setDate_time(object.getString("date_time"));
//                        }
//                        if (object.has("manager_type")
//                                && !TextUtils.isEmpty(object.getString("manager_type"))
//                                && !object.getString("manager_type").equals("null")) {
//                            eventDefinition.setManager_type(object.getString("manager_type"));
//                        }
//                        if (object.has("manager_type_id")
//                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
//                                && !object.getString("manager_type_id").equals("null")) {
//                            eventDefinition.setManager_type_id(object.getInt("manager_type_id"));
//                        }
//                        if (object.has("twp_id")
//                                && !TextUtils.isEmpty(object.getString("twp_id"))
//                                && !object.getString("twp_id").equals("null")) {
//                            eventDefinition.setTwp_id(object.getInt("twp_id"));
//                        }
//                        if (object.has("township_code")
//                                && !TextUtils.isEmpty(object.getString("township_code"))
//                                && !object.getString("township_code").equals("null")) {
//                            eventDefinition.setTownship_code(object.getString("township_code"));
//                        }
//                        if (object.has("township_name")
//                                && !TextUtils.isEmpty(object.getString("township_name"))
//                                && !object.getString("township_name").equals("null")) {
//                            eventDefinition.setTownship_name(object.getString("township_name"));
//                        }
//                        if (object.has("company_id")
//                                && !TextUtils.isEmpty(object.getString("company_id"))
//                                && !object.getString("company_id").equals("null")) {
//                            eventDefinition.setCompany_id(object.getInt("company_id"));
//                        }
//                        if (object.has("company_code")
//                                && !TextUtils.isEmpty(object.getString("company_code"))
//                                && !object.getString("company_code").equals("null")) {
//                            eventDefinition.setCompany_code(object.getString("company_code"));
//                        }
//                        if (object.has("company_name")
//                                && !TextUtils.isEmpty(object.getString("company_name"))
//                                && !object.getString("company_name").equals("null")) {
//                            eventDefinition.setCompany_name(object.getString("company_name"));
//                        }
//                        if (object.has("event_type")
//                                && !TextUtils.isEmpty(object.getString("event_type"))
//                                && !object.getString("event_type").equals("null")) {
//                            eventDefinition.setEvent_type(object.getString("event_type"));
//                        }
//                        if (object.has("event_name")
//                                && !TextUtils.isEmpty(object.getString("event_name"))
//                                && !object.getString("event_name").equals("null")) {
//                            eventDefinition.setEvent_name(object.getString("event_name"));
//                        }
//                        if (object.has("event_short_description")
//                                && !TextUtils.isEmpty(object.getString("event_short_description"))
//                                && !object.getString("event_short_description").equals("null")) {
//                            eventDefinition.setEvent_short_description(object.getString("event_short_description"));
//                        }
//                        if (object.has("event_long_description")
//                                && !TextUtils.isEmpty(object.getString("event_long_description"))
//                                && !object.getString("event_long_description").equals("null")) {
//                            eventDefinition.setEvent_long_description(object.getString("event_long_description"));
//                        }
//                        if (object.has("event_link_on_web")
//                                && !TextUtils.isEmpty(object.getString("event_link_on_web"))
//                                && !object.getString("event_link_on_web").equals("null")) {
//                            eventDefinition.setEvent_link_on_web(object.getString("event_link_on_web"));
//                        }
//                        if (object.has("event_link_twitter")
//                                && !TextUtils.isEmpty(object.getString("event_link_twitter"))
//                                && !object.getString("event_link_twitter").equals("null")) {
//                            eventDefinition.setEvent_link_twitter(object.getString("event_link_twitter"));
//                        }
//                        if (object.has("event_link_whatsapp")
//                                && !TextUtils.isEmpty(object.getString("event_link_whatsapp"))
//                                && !object.getString("event_link_whatsapp").equals("null")) {
//                            eventDefinition.setEvent_link_whatsapp(object.getString("event_link_whatsapp"));
//                        }
//                        if (object.has("event_link_other_media")
//                                && !TextUtils.isEmpty(object.getString("event_link_other_media"))
//                                && !object.getString("event_link_other_media").equals("null")) {
//                            eventDefinition.setEvent_link_other_media(object.getString("event_link_other_media"));
//                        }
//                        if (object.has("event_link_ytube")
//                                && !TextUtils.isEmpty(object.getString("event_link_ytube"))
//                                && !object.getString("event_link_ytube").equals("null")) {
//                            eventDefinition.setEvent_link_ytube(object.getString("event_link_ytube"));
//                        }
//                        if (object.has("event_link_zoom")
//                                && !TextUtils.isEmpty(object.getString("event_link_zoom"))
//                                && !object.getString("event_link_zoom").equals("null")) {
//                            eventDefinition.setEvent_link_zoom(object.getString("event_link_zoom"));
//                        }
//                        if (object.has("event_link_googlemeet")
//                                && !TextUtils.isEmpty(object.getString("event_link_googlemeet"))
//                                && !object.getString("event_link_googlemeet").equals("null")) {
//                            eventDefinition.setEvent_link_googlemeet(object.getString("event_link_googlemeet"));
//                        }
//                        if (object.has("event_link_googleclassroom")
//                                && !TextUtils.isEmpty(object.getString("event_link_googleclassroom"))
//                                && !object.getString("event_link_googleclassroom").equals("null")) {
//                            eventDefinition.setEvent_link_googleclassroom(object.getString("event_link_googleclassroom"));
//                        }
//                        if (object.has("event_logi_type")
//                                && !TextUtils.isEmpty(object.getString("event_logi_type"))
//                                && !object.getString("event_logi_type").equals("null")) {
//                            eventDefinition.setEvent_logi_type(object.getInt("event_logi_type"));
//                        } else {
//                            eventDefinition.setEvent_logi_type(1);
//                        }
//                        if (object.has("event_regn_limit")
//                                && !TextUtils.isEmpty(object.getString("event_regn_limit"))
//                                && !object.getString("event_regn_limit").equals("null")) {
//                            eventDefinition.setEvent_regn_limit(object.getString("event_regn_limit"));
//                        }
//                        if (object.has("web_event_full_regn_fee")
//                                && !TextUtils.isEmpty(object.getString("web_event_full_regn_fee"))
//                                && !object.getString("web_event_full_regn_fee").equals("null")) {
//                            eventDefinition.setWeb_event_full_regn_fee(object.getString("web_event_full_regn_fee"));
//                        }
//                        if (object.has("web_event_regn_limit")
//                                && !TextUtils.isEmpty(object.getString("web_event_regn_limit"))
//                                && !object.getString("web_event_regn_limit").equals("null")) {
//                            eventDefinition.setWeb_event_regn_limit(object.getString("web_event_regn_limit"));
//                        }
//                        if (object.has("web_event_location_to_show")
//                                && !TextUtils.isEmpty(object.getString("web_event_location_to_show"))
//                                && !object.getString("web_event_location_to_show").equals("null")) {
//                            eventDefinition.setWeb_event_location_to_show(object.getInt("web_event_location_to_show") == 1);
//                        }
//                        if (object.has("free_web_event")
//                                && !TextUtils.isEmpty(object.getString("free_web_event"))
//                                && !object.getString("free_web_event").equals("null")) {
//                            eventDefinition.setFree_web_event(object.getBoolean("free_web_event"));
//                        }
//                        if (object.has("company_logo")
//                                && !TextUtils.isEmpty(object.getString("company_logo"))
//                                && !object.getString("company_logo").equals("null")) {
//                            eventDefinition.setCompany_logo(object.getString("company_logo"));
//                        }
//                        if (object.has("event_logo")
//                                && !TextUtils.isEmpty(object.getString("event_logo"))
//                                && !object.getString("event_logo").equals("null")) {
//                            eventDefinition.setEvent_logo(object.getString("event_logo"));
//                        }
//                        if (object.has("event_image1")
//                                && !TextUtils.isEmpty(object.getString("event_image1"))
//                                && !object.getString("event_image1").equals("null")) {
//                            eventDefinition.setEvent_image1(object.getString("event_image1"));
//                        }
//                        if (object.has("event_image2")
//                                && !TextUtils.isEmpty(object.getString("event_image2"))
//                                && !object.getString("event_image2").equals("null")) {
//                            eventDefinition.setEvent_image2(object.getString("event_image2"));
//                        }
//                        if (object.has("event_blob_image")
//                                && !TextUtils.isEmpty(object.getString("event_blob_image"))
//                                && !object.getString("event_blob_image").equals("null")) {
//                            eventDefinition.setEvent_blob_image(object.getString("event_blob_image"));
//                        }
//                        if (object.has("event_address")
//                                && !TextUtils.isEmpty(object.getString("event_address"))
//                                && !object.getString("event_address").equals("null")) {
//                            eventDefinition.setEvent_address(object.getString("event_address"));
//                        }
//                        if (object.has("covered_locations")
//                                && !TextUtils.isEmpty(object.getString("covered_locations"))
//                                && !object.getString("covered_locations").equals("null")) {
//                            eventDefinition.setCovered_locations(object.getString("covered_locations"));
//                        }
//                        if (object.has("requirements")
//                                && !TextUtils.isEmpty(object.getString("requirements"))
//                                && !object.getString("requirements").equals("null")) {
//                            eventDefinition.setRequirements(object.getString("requirements"));
//                        }
//                        if (object.has("appl_req_download")
//                                && !TextUtils.isEmpty(object.getString("appl_req_download"))
//                                && !object.getString("appl_req_download").equals("null")) {
//                            eventDefinition.setAppl_req_download(object.getString("appl_req_download"));
//                        }
//                        if (object.has("cost")
//                                && !TextUtils.isEmpty(object.getString("cost"))
//                                && !object.getString("cost").equals("null")) {
//                            eventDefinition.setCost(object.getString("cost"));
//                        }
//                        if (object.has("year")
//                                && !TextUtils.isEmpty(object.getString("year"))
//                                && !object.getString("year").equals("null")) {
//                            eventDefinition.setYear(object.getString("year"));
//                        }
//                        if (object.has("location_address")
//                                && !TextUtils.isEmpty(object.getString("location_address"))
//                                && !object.getString("location_address").equals("null")) {
//                            eventDefinition.setLocation_address(object.getString("location_address"));
//                        }
//                        if (object.has("scheme_type")
//                                && !TextUtils.isEmpty(object.getString("scheme_type"))
//                                && !object.getString("scheme_type").equals("null")) {
//                            eventDefinition.setScheme_type(object.getString("scheme_type"));
//                        }
//                        if (object.has("event_prefix")
//                                && !TextUtils.isEmpty(object.getString("event_prefix"))
//                                && !object.getString("event_prefix").equals("null")) {
//                            eventDefinition.setEvent_prefix(object.getString("event_prefix"));
//                        }
//                        if (object.has("event_nextnum")
//                                && !TextUtils.isEmpty(object.getString("event_nextnum"))
//                                && !object.getString("event_nextnum").equals("null")) {
//                            eventDefinition.setEvent_nextnum(object.getString("event_nextnum"));
//                        }
//                        if (object.has("event_begins_date_time")
//                                && !TextUtils.isEmpty(object.getString("event_begins_date_time"))
//                                && !object.getString("event_begins_date_time").equals("null")) {
//                            eventDefinition.setEvent_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_begins_date_time")));
//                        }
//                        if (object.has("event_ends_date_time")
//                                && !TextUtils.isEmpty(object.getString("event_ends_date_time"))
//                                && !object.getString("event_ends_date_time").equals("null")) {
//                            eventDefinition.setEvent_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_ends_date_time")));
//                        }
//                        if (object.has("event_parking_begins_date_time")
//                                && !TextUtils.isEmpty(object.getString("event_parking_begins_date_time"))
//                                && !object.getString("event_parking_begins_date_time").equals("null")) {
//                            eventDefinition.setEvent_parking_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_parking_begins_date_time")));
//                        }
//                        if (object.has("event_parking_ends_date_time")
//                                && !TextUtils.isEmpty(object.getString("event_parking_ends_date_time"))
//                                && !object.getString("event_parking_ends_date_time").equals("null")) {
//                            eventDefinition.setEvent_parking_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_parking_ends_date_time")));
//                        }
//
//                        if (isCheckIn) {
//                            if (object.has("event_multi_dates")
//                                    && !TextUtils.isEmpty(object.getString("event_multi_dates"))
//                                    && !object.getString("event_multi_dates").equals("null")) {
//                                eventDefinition.setEvent_multi_dates(object.getString("event_multi_dates"));
//                                String currentDate = "";
//                                currentDate = DateTimeUtils.getCurrentDateYYYYMMDD();
//                                long currentDateMs = DateTimeUtils.getMilliseconds(currentDate);
//                                Log.e("#DEBUG", "   fetchEvent: currentDate:  " + currentDate);
//                                Log.e("#DEBUG", "   fetchEvent: currentDateMS:  " + currentDateMs);
//                                ArrayList<String> dates = new ArrayList<>();
//                                try {
//                                    JSONArray jsonArray2 = new JSONArray(eventDefinition.getEvent_multi_dates());
//                                    for (int i = 0; i < jsonArray2.length(); i++) {
//                                        JSONArray jsonArray1 = jsonArray2.getJSONArray(i);
//                                        if (jsonArray1 != null && jsonArray1.length() > 0) {
//                                            dates.add(jsonArray1.get(0).toString());
//                                            Log.e("#DEBUG", "   fetchEvent:  eventDate:  " + jsonArray1.get(0).toString());
//                                            Log.e("#DEBUG", "  fetchEvent:  eventDateMS: " + DateTimeUtils.getMilliseconds(jsonArray1.get(0).toString()));
//                                            if (currentDateMs == DateTimeUtils.getMilliseconds(jsonArray1.get(0).toString())) {
//                                                eventDefinition.setEventCategory("current");
//                                            } else if (currentDateMs > DateTimeUtils.getMilliseconds(jsonArray1.get(0).toString())) {
//                                                eventDefinition.setEventCategory("past");
//                                            } else if (currentDateMs < DateTimeUtils.getMilliseconds(jsonArray1.get(0).toString())) {
//                                                eventDefinition.setEventCategory("upcoming");
//                                            } else {
//                                                eventDefinition.setEventCategory("");
//                                            }
//                                        }
//                                    }
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        } else {
//                            if (object.has("event_multi_dates")
//                                    && !TextUtils.isEmpty(object.getString("event_multi_dates"))
//                                    && !object.getString("event_multi_dates").equals("null")) {
//                                eventDefinition.setEvent_multi_dates(object.getString("event_multi_dates"));
//                                ArrayList<String> dates = new ArrayList<>();
//                                ArrayList<String> eventDates = new ArrayList<>();
//                                JSONArray jsonArray2 = new JSONArray(eventDefinition.getEvent_multi_dates());
//                                for (int i = 0; i < jsonArray2.length(); i++) {
//                                    JSONArray jsonArray1 = jsonArray2.getJSONArray(i);
//                                    if (jsonArray1 != null && jsonArray1.length() > 0) {
//                                        eventDates.add(jsonArray1.get(0).toString());
//                                        dates.add(DateTimeUtils.gmtToLocalDateYYYYMMDD(jsonArray1.get(0).toString()));
//                                    }
//                                }
////                                Log.e("#DEBUG", "     Position: " + j + "   eventDates:   " + new Gson().toJson(eventDates));
////                                Log.e("#DEBUG", "     Position: " + j + "   dates:   " + new Gson().toJson(dates));
//                            }
//                        }
//
//
//                        if (object.has("event_parking_timings")
//                                && !TextUtils.isEmpty(object.getString("event_parking_timings"))
//                                && !object.getString("event_parking_timings").equals("null")) {
//                            eventDefinition.setEvent_parking_timings(object.getString("event_parking_timings"));
//                        }
//
//                        if (object.has("event_event_timings")
//                                && !TextUtils.isEmpty(object.getString("event_event_timings"))
//                                && !object.getString("event_event_timings").equals("null")) {
//
//
//                            eventDefinition.setEvent_event_timings(object.getString("event_event_timings"));
//                        }
//
//                        if (object.has("expires_by")
//                                && !TextUtils.isEmpty(object.getString("expires_by"))
//                                && !object.getString("expires_by").equals("null")) {
//                            eventDefinition.setExpires_by(object.getString("expires_by"));
//                        }
//                        if (object.has("regn_reqd")
//                                && !TextUtils.isEmpty(object.getString("regn_reqd"))
//                                && !object.getString("regn_reqd").equals("null")) {
//                            eventDefinition.setRegn_reqd(object.getBoolean("regn_reqd"));
//                        }
//                        if (object.has("regn_reqd_for_parking")
//                                && !TextUtils.isEmpty(object.getString("regn_reqd_for_parking"))
//                                && !object.getString("regn_reqd_for_parking").equals("null")) {
//                            eventDefinition.setRegn_reqd_for_parking(object.getBoolean("regn_reqd_for_parking"));
//                        }
//                        if (object.has("renewable")
//                                && !TextUtils.isEmpty(object.getString("renewable"))
//                                && !object.getString("renewable").equals("null")) {
//                            eventDefinition.setRenewable(object.getBoolean("renewable"));
//                        }
//                        if (object.has("active")
//                                && !TextUtils.isEmpty(object.getString("active"))
//                                && !object.getString("active").equals("null")) {
//                            eventDefinition.setActive(object.getBoolean("active"));
//                        }
//
//                        if (object.has("event_multi_sessions")
//                                && !TextUtils.isEmpty(object.getString("event_multi_sessions"))
//                                && !object.getString("event_multi_sessions").equals("null")) {
//                            eventDefinition.setEvent_multi_sessions(object.getBoolean("event_multi_sessions"));
//                        }
//
//                        if (object.has("event_indiv_session_regn_allowed")
//                                && !TextUtils.isEmpty(object.getString("event_indiv_session_regn_allowed"))
//                                && !object.getString("event_indiv_session_regn_allowed").equals("null")) {
//                            eventDefinition.setEvent_indiv_session_regn_allowed(object.getBoolean("event_indiv_session_regn_allowed"));
//                        }
//
//                        if (object.has("event_indiv_session_regn_required")
//                                && !TextUtils.isEmpty(object.getString("event_indiv_session_regn_required"))
//                                && !object.getString("event_indiv_session_regn_required").equals("null")) {
//                            eventDefinition.setEvent_indiv_session_regn_required(object.getBoolean("event_indiv_session_regn_required"));
//                        }
//
//                        if (object.has("event_full_regn_required")
//                                && !TextUtils.isEmpty(object.getString("event_full_regn_required"))
//                                && !object.getString("event_full_regn_required").equals("null")) {
//                            eventDefinition.setEvent_full_regn_required(object.getBoolean("event_full_regn_required"));
//                        }
//
//                        if (object.has("event_full_regn_fee")
//                                && !TextUtils.isEmpty(object.getString("event_full_regn_fee"))
//                                && !object.getString("event_full_regn_fee").equals("null")) {
//                            eventDefinition.setEvent_full_regn_fee(object.getString("event_full_regn_fee"));
//                        }
//
//                        if (object.has("event_parking_fee")
//                                && !TextUtils.isEmpty(object.getString("event_parking_fee"))
//                                && !object.getString("event_parking_fee").equals("null")) {
//                            eventDefinition.setEvent_parking_fee(object.getString("event_parking_fee"));
//                        }
//
//                        if (object.has("location_lat_lng")
//                                && !TextUtils.isEmpty(object.getString("location_lat_lng"))
//                                && !object.getString("location_lat_lng").equals("null")) {
//                            eventDefinition.setLocation_lat_lng(object.getString("location_lat_lng"));
//                        }
//
//                        if (object.has("regn_user_info_reqd")
//                                && !TextUtils.isEmpty(object.getString("regn_user_info_reqd"))
//                                && !object.getString("regn_user_info_reqd").equals("null")) {
//                            eventDefinition.setRegn_user_info_reqd(object.getBoolean("regn_user_info_reqd"));
//                        }
//
//                        if (object.has("regn_addl_user_info_reqd")
//                                && !TextUtils.isEmpty(object.getString("regn_addl_user_info_reqd"))
//                                && !object.getString("regn_addl_user_info_reqd").equals("null")) {
//                            eventDefinition.setRegn_addl_user_info_reqd(object.getBoolean("regn_addl_user_info_reqd"));
//                        }
//
//                        if (object.has("event_full_youth_fee")
//                                && !TextUtils.isEmpty(object.getString("event_full_youth_fee"))
//                                && !object.getString("event_full_youth_fee").equals("null")) {
//                            eventDefinition.setEvent_full_youth_fee(object.getString("event_full_youth_fee"));
//                        }
//
//                        if (object.has("event_full_child_fee")
//                                && !TextUtils.isEmpty(object.getString("event_full_child_fee"))
//                                && !object.getString("event_full_child_fee").equals("null")) {
//                            eventDefinition.setEvent_full_child_fee(object.getString("event_full_child_fee"));
//                        }
//
//                        if (object.has("event_full_student_fee")
//                                && !TextUtils.isEmpty(object.getString("event_full_student_fee"))
//                                && !object.getString("event_full_student_fee").equals("null")) {
//                            eventDefinition.setEvent_full_student_fee(object.getString("event_full_student_fee"));
//                        }
//
//                        if (object.has("event_full_minister_fee")
//                                && !TextUtils.isEmpty(object.getString("event_full_minister_fee"))
//                                && !object.getString("event_full_minister_fee").equals("null")) {
//                            eventDefinition.setEvent_full_minister_fee(object.getString("event_full_minister_fee"));
//                        }
//
//                        if (object.has("event_full_clergy_fee")
//                                && !TextUtils.isEmpty(object.getString("event_full_clergy_fee"))
//                                && !object.getString("event_full_clergy_fee").equals("null")) {
//                            eventDefinition.setEvent_full_clergy_fee(object.getString("event_full_clergy_fee"));
//                        }
//
//                        if (object.has("event_full_promo_fee")
//                                && !TextUtils.isEmpty(object.getString("event_full_promo_fee"))
//                                && !object.getString("event_full_promo_fee").equals("null")) {
//                            eventDefinition.setEvent_full_promo_fee(object.getString("event_full_promo_fee"));
//                        }
//
//                        if (object.has("event_full_senior_fee")
//                                && !TextUtils.isEmpty(object.getString("event_full_senior_fee"))
//                                && !object.getString("event_full_senior_fee").equals("null")) {
//                            eventDefinition.setEvent_full_senior_fee(object.getString("event_full_senior_fee"));
//                        }
//
//                        if (object.has("event_full_staff_fee")
//                                && !TextUtils.isEmpty(object.getString("event_full_staff_fee"))
//                                && !object.getString("event_full_staff_fee").equals("null")) {
//                            eventDefinition.setEvent_full_staff_fee(object.getString("event_full_staff_fee"));
//                        }
//
//                        if (object.has("free_event")
//                                && !TextUtils.isEmpty(object.getString("free_event"))
//                                && !object.getString("free_event").equals("null")) {
//                            eventDefinition.setFree_event(object.getBoolean("free_event"));
//                        } else {
//                            eventDefinition.setFree_event(false);
//                        }
//
//                        if (object.has("free_event_parking")
//                                && !TextUtils.isEmpty(object.getString("free_event_parking"))
//                                && !object.getString("free_event_parking").equals("null")) {
//                            eventDefinition.setFree_event_parking(object.getBoolean("free_event_parking"));
//                        } else {
//                            eventDefinition.setFree_event(false);
//                        }
//
//                        if (isCheckIn) {
//                            if (eventDefinition.getEventCategory() != null
//                                    && !TextUtils.isEmpty(eventDefinition.getEventCategory())
//                                    && !eventDefinition.getEventCategory().equals("past")) {
//                                eventDefinitions.add(eventDefinition);
//                            }
//                        } else {
//                            eventDefinitions.add(eventDefinition);
//                        }
//
//                        if (object.has("is_parking_allowed")
//                                && !TextUtils.isEmpty(object.getString("is_parking_allowed"))
//                                && !object.getString("is_parking_allowed").equals("null")) {
//                            eventDefinition.setIs_parking_allowed(object.getBoolean("is_parking_allowed"));
//                        }
//
//                        if (object.has("local_timezone")
//                                && !TextUtils.isEmpty(object.getString("local_timezone"))
//                                && !object.getString("local_timezone").equals("null")) {
//                            eventDefinition.setLocal_timezone(object.getString("local_timezone"));
//                        }
//
//                        if (object.has("local_time_event_start")
//                                && !TextUtils.isEmpty(object.getString("local_time_event_start"))
//                                && !object.getString("local_time_event_start").equals("null")) {
//                            eventDefinition.setLocal_time_event_start(object.getString("local_time_event_start"));
//                        }
//
//                        if (object.has("reqd_local_event_regn")
//                                && !TextUtils.isEmpty(object.getString("reqd_local_event_regn"))
//                                && !object.getString("reqd_local_event_regn").equals("null")) {
//                            eventDefinition.setReqd_local_event_regn(object.getBoolean("reqd_local_event_regn"));
//                        } else eventDefinition.setReqd_local_event_regn(false);
//
//                        if (object.has("reqd_web_event_regn")
//                                && !TextUtils.isEmpty(object.getString("reqd_web_event_regn"))
//                                && !object.getString("reqd_web_event_regn").equals("null")) {
//                            eventDefinition.setReqd_web_event_regn(object.getBoolean("reqd_web_event_regn"));
//                        } else eventDefinition.setReqd_web_event_regn(false);
//
//                        if (object.has("free_local_event")
//                                && !TextUtils.isEmpty(object.getString("free_local_event"))
//                                && !object.getString("free_local_event").equals("null")) {
//                            eventDefinition.setFree_local_event(object.getBoolean("free_local_event"));
//                        } else eventDefinition.setFree_local_event(false);
//
//                        if (object.has("free_web_event")
//                                && !TextUtils.isEmpty(object.getString("free_web_event"))
//                                && !object.getString("free_web_event").equals("null")) {
//                            eventDefinition.setFree_web_event(object.getBoolean("free_web_event"));
//                        } else eventDefinition.setFree_web_event(false);
//
//                        if (object.has("event_link_array")
//                                && !TextUtils.isEmpty(object.getString("event_link_array"))
//                                && !object.getString("event_link_array").equals("null")) {
//                            eventDefinition.setEvent_link_array(object.getString("event_link_array"));
//                        }
//
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            if (getActivity() != null) {
//                binding.swipeRefreshLayout.setRefreshing(false);
//                if (eventDefinitions.size() != 0) {
//                    binding.tvError.setVisibility(View.GONE);
//                    Collections.sort(eventDefinitions, (eventDefinition, t1) -> t1.getId() - eventDefinition.getId());
//                    eventsAdapter.notifyDataSetChanged();
//                } else {
//                    binding.tvError.setVisibility(View.VISIBLE);
//                }
//            }
//        }
//    }

    private class fetchEvents extends AsyncTask<String, String, String> {
        JSONObject json;
        JSONArray json1;
        String eventDefUrl = "_table/event_definitions";
        String url = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            binding.swipeRefreshLayout.setRefreshing(true);
            eventDefinitions.clear();
            if (eventsAdapter != null) eventsAdapter.notifyDataSetChanged();
            if (myApp.selectedManager != null) {
//                eventDefUrl = "_table/event_definitions?filter=company_id=" + selectedManager.getId();
                eventDefUrl = "_table/event_definitions?filter=(company_id%3D" + myApp.selectedManager.getId()
                        + ")%20AND%20(manager_type_id%3D" + myApp.selectedManager.getManager_type_id() + ")&&order=id%20DESC";
            }
//            if (selectedManager != null && selectedManager.getTownship_code() != null
//                    && !TextUtils.isEmpty(selectedManager.getTownship_code())
//                    && !selectedManager.getTownship_code().equals("null")) {
//                eventDefUrl = "_table/event_definitions?filter=township_code=" + selectedSpot.getTownship_code();
//            }
            url = getString(R.string.api) + getString(R.string.povlive) + eventDefUrl;
        }

        @Override
        protected String doInBackground(String... strings) {
            Log.e(TAG, "      fetchEvents:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getActivity().getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");

                    for (int j = 0; j < jsonArray.length(); j++) {
                        EventDefinition eventDefinition = new EventDefinition();
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            eventDefinition.setId(object.getInt("id"));
                        }
                        if (object.has("date_time")
                                && !TextUtils.isEmpty(object.getString("date_time"))
                                && !object.getString("date_time").equals("null")) {
                            eventDefinition.setDate_time(object.getString("date_time"));
                        }
                        if (object.has("manager_type")
                                && !TextUtils.isEmpty(object.getString("manager_type"))
                                && !object.getString("manager_type").equals("null")) {
                            eventDefinition.setManager_type(object.getString("manager_type"));
                        }
                        if (object.has("manager_type_id")
                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
                                && !object.getString("manager_type_id").equals("null")) {
                            eventDefinition.setManager_type_id(object.getInt("manager_type_id"));
                        }
                        if (object.has("twp_id")
                                && !TextUtils.isEmpty(object.getString("twp_id"))
                                && !object.getString("twp_id").equals("null")) {
                            eventDefinition.setTwp_id(object.getInt("twp_id"));
                        }
                        if (object.has("township_code")
                                && !TextUtils.isEmpty(object.getString("township_code"))
                                && !object.getString("township_code").equals("null")) {
                            eventDefinition.setTownship_code(object.getString("township_code"));
                        }
                        if (object.has("township_name")
                                && !TextUtils.isEmpty(object.getString("township_name"))
                                && !object.getString("township_name").equals("null")) {
                            eventDefinition.setTownship_name(object.getString("township_name"));
                        }
                        if (object.has("company_id")
                                && !TextUtils.isEmpty(object.getString("company_id"))
                                && !object.getString("company_id").equals("null")) {
                            eventDefinition.setCompany_id(object.getInt("company_id"));
                        }
                        if (object.has("company_code")
                                && !TextUtils.isEmpty(object.getString("company_code"))
                                && !object.getString("company_code").equals("null")) {
                            eventDefinition.setCompany_code(object.getString("company_code"));
                        }
                        if (object.has("company_name")
                                && !TextUtils.isEmpty(object.getString("company_name"))
                                && !object.getString("company_name").equals("null")) {
                            eventDefinition.setCompany_name(object.getString("company_name"));
                        }
                        if (object.has("event_category_type_id")
                                && !TextUtils.isEmpty(object.getString("event_category_type_id"))
                                && !object.getString("event_category_type_id").equals("null")) {
                            eventDefinition.setEvent_category_type_id(object.getInt("event_category_type_id"));
                        }
                        if (object.has("event_category_type")
                                && !TextUtils.isEmpty(object.getString("event_category_type"))
                                && !object.getString("event_category_type").equals("null")) {
                            eventDefinition.setEvent_category_type(object.getString("event_category_type"));
                        }
                        if (object.has("event_type")
                                && !TextUtils.isEmpty(object.getString("event_type"))
                                && !object.getString("event_type").equals("null")) {
                            eventDefinition.setEvent_type(object.getString("event_type"));
                        }
                        if (object.has("event_name")
                                && !TextUtils.isEmpty(object.getString("event_name"))
                                && !object.getString("event_name").equals("null")) {
                            eventDefinition.setEvent_name(object.getString("event_name"));
                        }
                        if (object.has("event_short_description")
                                && !TextUtils.isEmpty(object.getString("event_short_description"))
                                && !object.getString("event_short_description").equals("null")) {
                            eventDefinition.setEvent_short_description(object.getString("event_short_description"));
                        }
                        if (object.has("event_long_description")
                                && !TextUtils.isEmpty(object.getString("event_long_description"))
                                && !object.getString("event_long_description").equals("null")) {
                            eventDefinition.setEvent_long_description(object.getString("event_long_description"));
                        }
                        if (object.has("event_led_by")
                                && !TextUtils.isEmpty(object.getString("event_led_by"))
                                && !object.getString("event_led_by").equals("null")) {
                            eventDefinition.setEvent_led_by(object.getString("event_led_by"));
                        }
                        if (object.has("event_leaders_bio")
                                && !TextUtils.isEmpty(object.getString("event_leaders_bio"))
                                && !object.getString("event_leaders_bio").equals("null")) {
                            eventDefinition.setEvent_leaders_bio(object.getString("event_leaders_bio"));
                        }
                        if (object.has("event_link_on_web")
                                && !TextUtils.isEmpty(object.getString("event_link_on_web"))
                                && !object.getString("event_link_on_web").equals("null")) {
                            eventDefinition.setEvent_link_on_web(object.getString("event_link_on_web"));
                        }
                        if (object.has("event_link_twitter")
                                && !TextUtils.isEmpty(object.getString("event_link_twitter"))
                                && !object.getString("event_link_twitter").equals("null")) {
                            eventDefinition.setEvent_link_twitter(object.getString("event_link_twitter"));
                        }
                        if (object.has("event_link_whatsapp")
                                && !TextUtils.isEmpty(object.getString("event_link_whatsapp"))
                                && !object.getString("event_link_whatsapp").equals("null")) {
                            eventDefinition.setEvent_link_whatsapp(object.getString("event_link_whatsapp"));
                        }
                        if (object.has("event_link_other_media")
                                && !TextUtils.isEmpty(object.getString("event_link_other_media"))
                                && !object.getString("event_link_other_media").equals("null")) {
                            eventDefinition.setEvent_link_other_media(object.getString("event_link_other_media"));
                        }
                        if (object.has("event_link_ytube")
                                && !TextUtils.isEmpty(object.getString("event_link_ytube"))
                                && !object.getString("event_link_ytube").equals("null")) {
                            eventDefinition.setEvent_link_ytube(object.getString("event_link_ytube"));
                        }
                        if (object.has("event_link_zoom")
                                && !TextUtils.isEmpty(object.getString("event_link_zoom"))
                                && !object.getString("event_link_zoom").equals("null")) {
                            eventDefinition.setEvent_link_zoom(object.getString("event_link_zoom"));
                        }
                        if (object.has("event_link_googlemeet")
                                && !TextUtils.isEmpty(object.getString("event_link_googlemeet"))
                                && !object.getString("event_link_googlemeet").equals("null")) {
                            eventDefinition.setEvent_link_googlemeet(object.getString("event_link_googlemeet"));
                        }
                        if (object.has("event_link_googleclassroom")
                                && !TextUtils.isEmpty(object.getString("event_link_googleclassroom"))
                                && !object.getString("event_link_googleclassroom").equals("null")) {
                            eventDefinition.setEvent_link_googleclassroom(object.getString("event_link_googleclassroom"));
                        }
                        if (object.has("event_logi_type")
                                && !TextUtils.isEmpty(object.getString("event_logi_type"))
                                && !object.getString("event_logi_type").equals("null")) {
                            eventDefinition.setEvent_logi_type(object.getInt("event_logi_type"));
                        } else {
                            eventDefinition.setEvent_logi_type(1);
                        }
                        if (object.has("event_regn_limit")
                                && !TextUtils.isEmpty(object.getString("event_regn_limit"))
                                && !object.getString("event_regn_limit").equals("null")) {
                            eventDefinition.setEvent_regn_limit(object.getString("event_regn_limit"));
                        }
                        if (object.has("web_event_full_regn_fee")
                                && !TextUtils.isEmpty(object.getString("web_event_full_regn_fee"))
                                && !object.getString("web_event_full_regn_fee").equals("null")) {
                            eventDefinition.setWeb_event_full_regn_fee(object.getString("web_event_full_regn_fee"));
                        }
                        if (object.has("web_event_regn_limit")
                                && !TextUtils.isEmpty(object.getString("web_event_regn_limit"))
                                && !object.getString("web_event_regn_limit").equals("null")) {
                            eventDefinition.setWeb_event_regn_limit(object.getString("web_event_regn_limit"));
                        }
                        if (object.has("web_event_location_to_show")
                                && !TextUtils.isEmpty(object.getString("web_event_location_to_show"))
                                && !object.getString("web_event_location_to_show").equals("null")) {
                            eventDefinition.setWeb_event_location_to_show(object.getInt("web_event_location_to_show") == 1);
                        }
                        if (object.has("free_web_event")
                                && !TextUtils.isEmpty(object.getString("free_web_event"))
                                && !object.getString("free_web_event").equals("null")) {
                            eventDefinition.setFree_web_event(object.getBoolean("free_web_event"));
                        }
                        if (object.has("company_logo")
                                && !TextUtils.isEmpty(object.getString("company_logo"))
                                && !object.getString("company_logo").equals("null")) {
                            eventDefinition.setCompany_logo(object.getString("company_logo"));
                        }
                        if (object.has("event_logo")
                                && !TextUtils.isEmpty(object.getString("event_logo"))
                                && !object.getString("event_logo").equals("null")) {
                            eventDefinition.setEvent_logo(object.getString("event_logo"));
                        }
                        if (object.has("event_image1")
                                && !TextUtils.isEmpty(object.getString("event_image1"))
                                && !object.getString("event_image1").equals("null")) {
                            eventDefinition.setEvent_image1(object.getString("event_image1"));
                        }
                        if (object.has("event_image2")
                                && !TextUtils.isEmpty(object.getString("event_image2"))
                                && !object.getString("event_image2").equals("null")) {
                            eventDefinition.setEvent_image2(object.getString("event_image2"));
                        }
                        if (object.has("event_blob_image")
                                && !TextUtils.isEmpty(object.getString("event_blob_image"))
                                && !object.getString("event_blob_image").equals("null")) {
                            eventDefinition.setEvent_blob_image(object.getString("event_blob_image"));
                        }
                        if (object.has("event_address")
                                && !TextUtils.isEmpty(object.getString("event_address"))
                                && !object.getString("event_address").equals("null")) {
                            eventDefinition.setEvent_address(object.getString("event_address"));
                        }
                        if (object.has("covered_locations")
                                && !TextUtils.isEmpty(object.getString("covered_locations"))
                                && !object.getString("covered_locations").equals("null")) {
                            eventDefinition.setCovered_locations(object.getString("covered_locations"));
                        }
                        if (object.has("requirements")
                                && !TextUtils.isEmpty(object.getString("requirements"))
                                && !object.getString("requirements").equals("null")) {
                            eventDefinition.setRequirements(object.getString("requirements"));
                        }
                        if (object.has("appl_req_download")
                                && !TextUtils.isEmpty(object.getString("appl_req_download"))
                                && !object.getString("appl_req_download").equals("null")) {
                            eventDefinition.setAppl_req_download(object.getString("appl_req_download"));
                        }
                        if (object.has("cost")
                                && !TextUtils.isEmpty(object.getString("cost"))
                                && !object.getString("cost").equals("null")) {
                            eventDefinition.setCost(object.getString("cost"));
                        }
                        if (object.has("year")
                                && !TextUtils.isEmpty(object.getString("year"))
                                && !object.getString("year").equals("null")) {
                            eventDefinition.setYear(object.getString("year"));
                        }
                        if (object.has("location_address")
                                && !TextUtils.isEmpty(object.getString("location_address"))
                                && !object.getString("location_address").equals("null")) {
                            eventDefinition.setLocation_address(object.getString("location_address"));
                        }
                        if (object.has("scheme_type")
                                && !TextUtils.isEmpty(object.getString("scheme_type"))
                                && !object.getString("scheme_type").equals("null")) {
                            eventDefinition.setScheme_type(object.getString("scheme_type"));
                        }
                        if (object.has("event_prefix")
                                && !TextUtils.isEmpty(object.getString("event_prefix"))
                                && !object.getString("event_prefix").equals("null")) {
                            eventDefinition.setEvent_prefix(object.getString("event_prefix"));
                        }
                        if (object.has("event_nextnum")
                                && !TextUtils.isEmpty(object.getString("event_nextnum"))
                                && !object.getString("event_nextnum").equals("null")) {
                            eventDefinition.setEvent_nextnum(object.getString("event_nextnum"));
                        }
                        if (object.has("event_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_begins_date_time"))
                                && !object.getString("event_begins_date_time").equals("null")) {
                            eventDefinition.setEvent_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_begins_date_time")));
                        }
                        if (object.has("event_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_ends_date_time"))
                                && !object.getString("event_ends_date_time").equals("null")) {
                            eventDefinition.setEvent_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_ends_date_time")));
                        }
                        if (object.has("parking_reservation_limit")
                                && !TextUtils.isEmpty(object.getString("parking_reservation_limit"))
                                && !object.getString("parking_reservation_limit").equals("null")) {
                            eventDefinition.setParking_reservation_limit(object.getString("parking_reservation_limit"));
                        }
                        if (object.has("event_parking_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_parking_begins_date_time"))
                                && !object.getString("event_parking_begins_date_time").equals("null")) {
                            eventDefinition.setEvent_parking_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_parking_begins_date_time")));
                        }
                        if (object.has("event_parking_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_parking_ends_date_time"))
                                && !object.getString("event_parking_ends_date_time").equals("null")) {
                            eventDefinition.setEvent_parking_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_parking_ends_date_time")));
                        }

                        if (isCheckIn) {
                            if (object.has("event_multi_dates")
                                    && !TextUtils.isEmpty(object.getString("event_multi_dates"))
                                    && !object.getString("event_multi_dates").equals("null")) {
                                eventDefinition.setEvent_multi_dates(object.getString("event_multi_dates"));
                                String currentDate = "";
                                currentDate = DateTimeUtils.getCurrentDateYYYYMMDD();
                                long currentDateMs = DateTimeUtils.getMilliseconds(currentDate);
                                Log.e(TAG, "   fetchEvent: currentDate:  " + currentDate);
                                Log.e(TAG, "   fetchEvent: currentDateMS:  " + currentDateMs);
                                ArrayList<String> dates = new ArrayList<>();
                                try {
                                    JSONArray jsonArray2 = new JSONArray(eventDefinition.getEvent_multi_dates());
                                    for (int i = 0; i < jsonArray2.length(); i++) {
                                        JSONArray jsonArray1 = jsonArray2.getJSONArray(i);
                                        if (jsonArray1 != null && jsonArray1.length() > 0) {
                                            dates.add(jsonArray1.get(0).toString());
                                            Log.e(TAG, "   fetchEvent:  eventDate:  " + jsonArray1.get(0).toString());
                                            Log.e(TAG, "  fetchEvent:  eventDateMS: " + DateTimeUtils.getMilliseconds(jsonArray1.get(0).toString()));
                                            if (currentDateMs == DateTimeUtils.getMilliseconds(jsonArray1.get(0).toString())) {
                                                eventDefinition.setEventCategory("current");
                                            } else if (currentDateMs > DateTimeUtils.getMilliseconds(jsonArray1.get(0).toString())) {
                                                eventDefinition.setEventCategory("past");
                                            } else if (currentDateMs < DateTimeUtils.getMilliseconds(jsonArray1.get(0).toString())) {
                                                eventDefinition.setEventCategory("upcoming");
                                            } else {
                                                eventDefinition.setEventCategory("");
                                            }
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            if (object.has("event_multi_dates")
                                    && !TextUtils.isEmpty(object.getString("event_multi_dates"))
                                    && !object.getString("event_multi_dates").equals("null")) {
                                eventDefinition.setEvent_multi_dates(object.getString("event_multi_dates"));
                                ArrayList<String> dates = new ArrayList<>();
                                ArrayList<String> eventDates = new ArrayList<>();
                                JSONArray jsonArray2 = new JSONArray(eventDefinition.getEvent_multi_dates());
                                for (int i = 0; i < jsonArray2.length(); i++) {
                                    JSONArray jsonArray1 = jsonArray2.getJSONArray(i);
                                    if (jsonArray1 != null && jsonArray1.length() > 0) {
                                        eventDates.add(jsonArray1.get(0).toString());
                                        dates.add(DateTimeUtils.gmtToLocalDateYYYYMMDD(jsonArray1.get(0).toString()));
                                    }
                                }
//                                Log.e(TAG, "     Position: " + j + "   eventDates:   " + new Gson().toJson(eventDates));
//                                Log.e(TAG, "     Position: " + j + "   dates:   " + new Gson().toJson(dates));
                            }
                        }

                        if (object.has("event_parking_timings")
                                && !TextUtils.isEmpty(object.getString("event_parking_timings"))
                                && !object.getString("event_parking_timings").equals("null")) {
                            eventDefinition.setEvent_parking_timings(object.getString("event_parking_timings"));
                        }

                        if (object.has("event_event_timings")
                                && !TextUtils.isEmpty(object.getString("event_event_timings"))
                                && !object.getString("event_event_timings").equals("null")) {


                            eventDefinition.setEvent_event_timings(object.getString("event_event_timings"));
                        }

                        if (object.has("expires_by")
                                && !TextUtils.isEmpty(object.getString("expires_by"))
                                && !object.getString("expires_by").equals("null")) {
                            eventDefinition.setExpires_by(object.getString("expires_by"));
                        }
                        if (object.has("regn_reqd")
                                && !TextUtils.isEmpty(object.getString("regn_reqd"))
                                && !object.getString("regn_reqd").equals("null")) {
                            eventDefinition.setRegn_reqd(object.getBoolean("regn_reqd"));
                        }
                        if (object.has("regn_reqd_for_parking")
                                && !TextUtils.isEmpty(object.getString("regn_reqd_for_parking"))
                                && !object.getString("regn_reqd_for_parking").equals("null")) {
                            eventDefinition.setRegn_reqd_for_parking(object.getBoolean("regn_reqd_for_parking"));
                        }
                        if (object.has("renewable")
                                && !TextUtils.isEmpty(object.getString("renewable"))
                                && !object.getString("renewable").equals("null")) {
                            eventDefinition.setRenewable(object.getBoolean("renewable"));
                        }
                        if (object.has("active")
                                && !TextUtils.isEmpty(object.getString("active"))
                                && !object.getString("active").equals("null")) {
                            eventDefinition.setActive(object.getBoolean("active"));
                        }

                        if (object.has("event_multi_sessions")
                                && !TextUtils.isEmpty(object.getString("event_multi_sessions"))
                                && !object.getString("event_multi_sessions").equals("null")) {
                            eventDefinition.setEvent_multi_sessions(object.getBoolean("event_multi_sessions"));
                        }

                        if (object.has("event_indiv_session_regn_allowed")
                                && !TextUtils.isEmpty(object.getString("event_indiv_session_regn_allowed"))
                                && !object.getString("event_indiv_session_regn_allowed").equals("null")) {
                            eventDefinition.setEvent_indiv_session_regn_allowed(object.getBoolean("event_indiv_session_regn_allowed"));
                        }

                        if (object.has("event_indiv_session_regn_required")
                                && !TextUtils.isEmpty(object.getString("event_indiv_session_regn_required"))
                                && !object.getString("event_indiv_session_regn_required").equals("null")) {
                            eventDefinition.setEvent_indiv_session_regn_required(object.getBoolean("event_indiv_session_regn_required"));
                        }

                        if (object.has("event_full_regn_required")
                                && !TextUtils.isEmpty(object.getString("event_full_regn_required"))
                                && !object.getString("event_full_regn_required").equals("null")) {
                            eventDefinition.setEvent_full_regn_required(object.getBoolean("event_full_regn_required"));
                        }

                        if (object.has("event_full_regn_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_regn_fee"))
                                && !object.getString("event_full_regn_fee").equals("null")) {
                            eventDefinition.setEvent_full_regn_fee(object.getString("event_full_regn_fee"));
                        }

                        if (object.has("event_parking_fee")
                                && !TextUtils.isEmpty(object.getString("event_parking_fee"))
                                && !object.getString("event_parking_fee").equals("null")) {
                            eventDefinition.setEvent_parking_fee(object.getString("event_parking_fee"));
                        }
                        if (object.has("event_parking_fee_duration")
                                && !TextUtils.isEmpty(object.getString("event_parking_fee_duration"))
                                && !object.getString("event_parking_fee_duration").equals("null")) {
                            eventDefinition.setEvent_parking_fee_duration(object.getString("event_parking_fee_duration"));
                        } else {
                            eventDefinition.setEvent_parking_fee_duration("Event");
                        }
                        if (object.has("location_lat_lng")
                                && !TextUtils.isEmpty(object.getString("location_lat_lng"))
                                && !object.getString("location_lat_lng").equals("null")) {
                            eventDefinition.setLocation_lat_lng(object.getString("location_lat_lng"));
                        }

                        if (object.has("regn_user_info_reqd")
                                && !TextUtils.isEmpty(object.getString("regn_user_info_reqd"))
                                && !object.getString("regn_user_info_reqd").equals("null")) {
                            eventDefinition.setRegn_user_info_reqd(object.getBoolean("regn_user_info_reqd"));
                        }

                        if (object.has("regn_addl_user_info_reqd")
                                && !TextUtils.isEmpty(object.getString("regn_addl_user_info_reqd"))
                                && !object.getString("regn_addl_user_info_reqd").equals("null")) {
                            eventDefinition.setRegn_addl_user_info_reqd(object.getBoolean("regn_addl_user_info_reqd"));
                        }

                        if (object.has("event_full_youth_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_youth_fee"))
                                && !object.getString("event_full_youth_fee").equals("null")) {
                            eventDefinition.setEvent_full_youth_fee(object.getString("event_full_youth_fee"));
                        }

                        if (object.has("event_full_child_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_child_fee"))
                                && !object.getString("event_full_child_fee").equals("null")) {
                            eventDefinition.setEvent_full_child_fee(object.getString("event_full_child_fee"));
                        }

                        if (object.has("event_full_student_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_student_fee"))
                                && !object.getString("event_full_student_fee").equals("null")) {
                            eventDefinition.setEvent_full_student_fee(object.getString("event_full_student_fee"));
                        }

                        if (object.has("event_full_minister_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_minister_fee"))
                                && !object.getString("event_full_minister_fee").equals("null")) {
                            eventDefinition.setEvent_full_minister_fee(object.getString("event_full_minister_fee"));
                        }

                        if (object.has("event_full_clergy_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_clergy_fee"))
                                && !object.getString("event_full_clergy_fee").equals("null")) {
                            eventDefinition.setEvent_full_clergy_fee(object.getString("event_full_clergy_fee"));
                        }

                        if (object.has("event_full_promo_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_promo_fee"))
                                && !object.getString("event_full_promo_fee").equals("null")) {
                            eventDefinition.setEvent_full_promo_fee(object.getString("event_full_promo_fee"));
                        }

                        if (object.has("event_full_senior_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_senior_fee"))
                                && !object.getString("event_full_senior_fee").equals("null")) {
                            eventDefinition.setEvent_full_senior_fee(object.getString("event_full_senior_fee"));
                        }

                        if (object.has("event_full_staff_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_staff_fee"))
                                && !object.getString("event_full_staff_fee").equals("null")) {
                            eventDefinition.setEvent_full_staff_fee(object.getString("event_full_staff_fee"));
                        }

                        if (object.has("free_event")
                                && !TextUtils.isEmpty(object.getString("free_event"))
                                && !object.getString("free_event").equals("null")) {
                            eventDefinition.setFree_event(object.getBoolean("free_event"));
                        } else {
                            eventDefinition.setFree_event(false);
                        }

                        if (object.has("free_event_parking")
                                && !TextUtils.isEmpty(object.getString("free_event_parking"))
                                && !object.getString("free_event_parking").equals("null")) {
                            eventDefinition.setFree_event_parking(object.getBoolean("free_event_parking"));
                        } else {
                            eventDefinition.setFree_event(false);
                        }

                        if (object.has("event_public")
                                && !TextUtils.isEmpty(object.getString("event_public"))
                                && !object.getString("event_public").equals("null")) {
                            eventDefinition.setEvent_public(object.getBoolean("event_public"));
                        } else {
                            eventDefinition.setEvent_public(false);
                        }
                        if (object.has("event_unlisted")
                                && !TextUtils.isEmpty(object.getString("event_unlisted"))
                                && !object.getString("event_unlisted").equals("null")) {
                            eventDefinition.setEvent_unlisted(object.getBoolean("event_unlisted"));
                        } else {
                            eventDefinition.setEvent_unlisted(false);
                        }
                        if (object.has("event_tp_sharable")
                                && !TextUtils.isEmpty(object.getString("event_tp_sharable"))
                                && !object.getString("event_tp_sharable").equals("null")) {
                            eventDefinition.setEvent_tp_sharable(object.getBoolean("event_tp_sharable"));
                        } else {
                            eventDefinition.setEvent_tp_sharable(false);
                        }
                        if (object.has("event_invite_only")
                                && !TextUtils.isEmpty(object.getString("event_invite_only"))
                                && !object.getString("event_invite_only").equals("null")) {
                            eventDefinition.setEvent_invite_only(object.getBoolean("event_invite_only"));
                        } else {
                            eventDefinition.setEvent_invite_only(false);
                        }
                        if (object.has("event_regn_invite_only")
                                && !TextUtils.isEmpty(object.getString("event_regn_invite_only"))
                                && !object.getString("event_regn_invite_only").equals("null")) {
                            eventDefinition.setEvent_regn_invite_only(object.getBoolean("event_regn_invite_only"));
                        } else {
                            eventDefinition.setEvent_regn_invite_only(false);
                        }
                        if (object.has("show_live_regd_only")
                                && !TextUtils.isEmpty(object.getString("show_live_regd_only"))
                                && !object.getString("show_live_regd_only").equals("null")) {
                            eventDefinition.setShow_live_regd_only(object.getBoolean("show_live_regd_only"));
                        } else {
                            eventDefinition.setShow_live_regd_only(false);
                        }
                        if (object.has("show_live_invite_only")
                                && !TextUtils.isEmpty(object.getString("show_live_invite_only"))
                                && !object.getString("show_live_invite_only").equals("null")) {
                            eventDefinition.setShow_live_invite_only(object.getBoolean("show_live_invite_only"));
                        } else {
                            eventDefinition.setShow_live_invite_only(false);
                        }
                        if (object.has("show_past_regd_only")
                                && !TextUtils.isEmpty(object.getString("show_past_regd_only"))
                                && !object.getString("show_past_regd_only").equals("null")) {
                            eventDefinition.setShow_past_regd_only(object.getBoolean("show_past_regd_only"));
                        } else {
                            eventDefinition.setShow_past_regd_only(false);
                        }
                        if (object.has("show_past_invite_only")
                                && !TextUtils.isEmpty(object.getString("show_past_invite_only"))
                                && !object.getString("show_past_invite_only").equals("null")) {
                            eventDefinition.setShow_past_invite_only(object.getBoolean("show_past_invite_only"));
                        } else {
                            eventDefinition.setShow_past_invite_only(false);
                        }
                        if (object.has("geo_restrict_countries")
                                && !TextUtils.isEmpty(object.getString("geo_restrict_countries"))
                                && !object.getString("geo_restrict_countries").equals("null")) {
                            eventDefinition.setGeo_restrict_countries(object.getBoolean("geo_restrict_countries"));
                        } else {
                            eventDefinition.setGeo_restrict_countries(false);
                        }
                        if (object.has("geo_allow_countries")
                                && !TextUtils.isEmpty(object.getString("geo_allow_countries"))
                                && !object.getString("geo_allow_countries").equals("null")) {
                            eventDefinition.setGeo_allow_countres(object.getBoolean("geo_allow_countries"));
                        } else {
                            eventDefinition.setGeo_allow_countres(false);
                        }
                        if (object.has("geo_restrict_states")
                                && !TextUtils.isEmpty(object.getString("geo_restrict_states"))
                                && !object.getString("geo_restrict_states").equals("null")) {
                            eventDefinition.setGeo_restrict_states(object.getBoolean("geo_restrict_states"));
                        } else {
                            eventDefinition.setGeo_restrict_states(false);
                        }
                        if (object.has("geo_allow_states")
                                && !TextUtils.isEmpty(object.getString("geo_allow_states"))
                                && !object.getString("geo_allow_states").equals("null")) {
                            eventDefinition.setGeo_allow_states(object.getBoolean("geo_allow_states"));
                        } else {
                            eventDefinition.setGeo_allow_states(false);
                        }
                        if (object.has("geo_restrict_lat_lng")
                                && !TextUtils.isEmpty(object.getString("geo_restrict_lat_lng"))
                                && !object.getString("geo_restrict_lat_lng").equals("null")) {
                            eventDefinition.setGeo_restrict_lat_lng(object.getBoolean("geo_restrict_lat_lng"));
                        } else {
                            eventDefinition.setGeo_restrict_lat_lng(false);
                        }
                        if (object.has("geo_allow_lat_lng")
                                && !TextUtils.isEmpty(object.getString("geo_allow_lat_lng"))
                                && !object.getString("geo_allow_lat_lng").equals("null")) {
                            eventDefinition.setGeo_allow_lat_lng(object.getBoolean("geo_allow_lat_lng"));
                        } else {
                            eventDefinition.setGeo_allow_lat_lng(false);
                        }
                        if (object.has("allow_audio_conf")
                                && !TextUtils.isEmpty(object.getString("allow_audio_conf"))
                                && !object.getString("allow_audio_conf").equals("null")) {
                            eventDefinition.setAllow_audio_conf(object.getBoolean("allow_audio_conf"));
                        } else {
                            eventDefinition.setAllow_audio_conf(false);
                        }
                        if (object.has("allow_audio_condition_no")
                                && !TextUtils.isEmpty(object.getString("allow_audio_condition_no"))
                                && !object.getString("allow_audio_condition_no").equals("null")) {
                            eventDefinition.setAllow_audio_condition_no(object.getBoolean("allow_audio_condition_no"));
                        } else {
                            eventDefinition.setAllow_audio_condition_no(false);
                        }
                        if (object.has("allow_audio_conf_rec")
                                && !TextUtils.isEmpty(object.getString("allow_audio_conf_rec"))
                                && !object.getString("allow_audio_conf_rec").equals("null")) {
                            eventDefinition.setAllow_audio_conf_rec(object.getBoolean("allow_audio_conf_rec"));
                        } else {
                            eventDefinition.setAllow_audio_conf_rec(false);
                        }
                        if (object.has("allow_audio_rec_condition")
                                && !TextUtils.isEmpty(object.getString("allow_audio_rec_condition"))
                                && !object.getString("allow_audio_rec_condition").equals("null")) {
                            eventDefinition.setAllow_audio_rec_condition(object.getBoolean("allow_audio_rec_condition"));
                        } else {
                            eventDefinition.setAllow_audio_rec_condition(false);
                        }
                        if (object.has("allow_video_conf")
                                && !TextUtils.isEmpty(object.getString("allow_video_conf"))
                                && !object.getString("allow_video_conf").equals("null")) {
                            eventDefinition.setAllow_video_conf(object.getBoolean("allow_video_conf"));
                        } else {
                            eventDefinition.setAllow_video_conf(false);
                        }
                        if (object.has("allow_video_condition_no")
                                && !TextUtils.isEmpty(object.getString("allow_video_condition_no"))
                                && !object.getString("allow_video_condition_no").equals("null")) {
                            eventDefinition.setAllow_video_condition_no(object.getBoolean("allow_video_condition_no"));
                        } else {
                            eventDefinition.setAllow_video_condition_no(false);
                        }
                        if (object.has("allow_video_conf_rec")
                                && !TextUtils.isEmpty(object.getString("allow_video_conf_rec"))
                                && !object.getString("allow_video_conf_rec").equals("null")) {
                            eventDefinition.setAllow_video_conf_rec(object.getBoolean("allow_video_conf_rec"));
                        } else {
                            eventDefinition.setAllow_video_conf_rec(false);
                        }
                        if (object.has("allow_video_rec_condition")
                                && !TextUtils.isEmpty(object.getString("allow_video_rec_condition"))
                                && !object.getString("allow_video_rec_condition").equals("null")) {
                            eventDefinition.setAllow_video_rec_condition(object.getBoolean("allow_video_rec_condition"));
                        } else {
                            eventDefinition.setAllow_video_rec_condition(false);
                        }
                        if (object.has("allow_livechat")
                                && !TextUtils.isEmpty(object.getString("allow_livechat"))
                                && !object.getString("allow_livechat").equals("null")) {
                            eventDefinition.setAllow_livechat(object.getBoolean("allow_livechat"));
                        } else {
                            eventDefinition.setAllow_livechat(false);
                        }
                        if (object.has("allow_livechat_condition_no")
                                && !TextUtils.isEmpty(object.getString("allow_livechat_condition_no"))
                                && !object.getString("allow_livechat_condition_no").equals("null")) {
                            eventDefinition.setAllow_livechat_condition_no(object.getBoolean("allow_livechat_condition_no"));
                        } else {
                            eventDefinition.setAllow_livechat_condition_no(false);
                        }

                        if (object.has("is_parking_allowed")
                                && !TextUtils.isEmpty(object.getString("is_parking_allowed"))
                                && !object.getString("is_parking_allowed").equals("null")) {
                            eventDefinition.setIs_parking_allowed(object.getBoolean("is_parking_allowed"));
                        }

                        if (object.has("local_timezone")
                                && !TextUtils.isEmpty(object.getString("local_timezone"))
                                && !object.getString("local_timezone").equals("null")) {
                            eventDefinition.setLocal_timezone(object.getString("local_timezone"));
                        }

                        if (object.has("local_time_event_start")
                                && !TextUtils.isEmpty(object.getString("local_time_event_start"))
                                && !object.getString("local_time_event_start").equals("null")) {
                            eventDefinition.setLocal_time_event_start(object.getString("local_time_event_start"));
                        }

                        if (object.has("reqd_local_event_regn")
                                && !TextUtils.isEmpty(object.getString("reqd_local_event_regn"))
                                && !object.getString("reqd_local_event_regn").equals("null")) {
                            eventDefinition.setReqd_local_event_regn(object.getBoolean("reqd_local_event_regn"));
                        } else eventDefinition.setReqd_local_event_regn(false);

                        if (object.has("reqd_web_event_regn")
                                && !TextUtils.isEmpty(object.getString("reqd_web_event_regn"))
                                && !object.getString("reqd_web_event_regn").equals("null")) {
                            eventDefinition.setReqd_web_event_regn(object.getBoolean("reqd_web_event_regn"));
                        } else eventDefinition.setReqd_web_event_regn(false);

                        if (object.has("free_local_event")
                                && !TextUtils.isEmpty(object.getString("free_local_event"))
                                && !object.getString("free_local_event").equals("null")) {
                            eventDefinition.setFree_local_event(object.getBoolean("free_local_event"));
                        } else eventDefinition.setFree_local_event(false);

                        if (object.has("free_web_event")
                                && !TextUtils.isEmpty(object.getString("free_web_event"))
                                && !object.getString("free_web_event").equals("null")) {
                            eventDefinition.setFree_web_event(object.getBoolean("free_web_event"));
                        } else eventDefinition.setFree_web_event(false);

                        if (object.has("event_link_array")
                                && !TextUtils.isEmpty(object.getString("event_link_array"))
                                && !object.getString("event_link_array").equals("null")) {
                            eventDefinition.setEvent_link_array(object.getString("event_link_array"));
                        }

                        if (isCheckIn) {
                            if (eventDefinition.getEventCategory() != null
                                    && !TextUtils.isEmpty(eventDefinition.getEventCategory())
                                    && !eventDefinition.getEventCategory().equals("past")) {
                                eventDefinitions.add(eventDefinition);
                            }
                        } else {
                            eventDefinitions.add(eventDefinition);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                binding.swipeRefreshLayout.setRefreshing(false);
                if (eventDefinitions.size() != 0) {
                    binding.tvError.setVisibility(View.GONE);
                    Collections.sort(eventDefinitions, (eventDefinition, t1) -> t1.getId() - eventDefinition.getId());
                    eventsAdapter.notifyDataSetChanged();
                } else {
                    binding.tvError.setVisibility(View.VISIBLE);
                }
            }
        }
    }
    @Override
    protected void updateViews() {

        if (isCheckIn) {
            binding.tvBtnAddEvent.setVisibility(View.GONE);
        } else {
            binding.tvBtnAddEvent.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void setListeners() {

        binding.swipeRefreshLayout.setOnRefreshListener(this::fetchEvents);

        binding.tvBtnAddEvent.setOnClickListener(v -> {
 //          EventOptionTypeDialog dialog = EventOptionTypeDialog.newInstance();
//            Bundle bundle = new Bundle();
//            bundle.putBoolean("isSession", false);
//            bundle.putInt("eventLogiType", EVENT_TYPE_AT_LOCATION);
//            dialog.setArguments(bundle);
//            dialog.show(getChildFragmentManager(), "payment");


            showAddEditEventOptionTypePupup(false, EVENT_TYPE_AT_LOCATION); //new ui
        });

    }

    int selectedEventType;
    private void showAddEditEventOptionTypePupup(boolean isEdit, int eventLogiType) {
        PopupEventOptionTypeBinding binding;
        PopupWindow popupmanaged;

        LinearLayout viewGroup = (LinearLayout) getActivity().findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.popup_event_option_type, viewGroup, false);
        View layout = binding.getRoot();

        popupmanaged = new PopupWindow(getActivity());
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);

        selectedEventType = eventLogiType;
        Typeface typeface = binding.tvLocal.getTypeface();
        if (eventLogiType == EVENT_TYPE_AT_LOCATION) {
            binding.tvLocal.setTypeface(typeface, Typeface.BOLD);
            binding.llLocal.setBackgroundResource(R.drawable.highlight);
            binding.tvLocal.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_700));
        } else if (eventLogiType == EVENT_TYPE_AT_VIRTUALLY) {
            binding.llOnline.setBackgroundResource(R.drawable.highlight);
            binding.tvOnline.setTypeface(typeface, Typeface.BOLD);
            binding.tvOnline.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_700));
        } else if (eventLogiType == EVENT_TYPE_BOTH) {
            binding.llBoth.setBackgroundResource(R.drawable.highlight);
            binding.tvBoth.setTypeface(typeface, Typeface.BOLD);
            binding.tvBoth.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_700));
        }

        binding.llLocal.setOnClickListener(v -> {
            selectedEventType = EVENT_TYPE_AT_LOCATION;
            binding.tvLocal.setTypeface(typeface, Typeface.BOLD);
            binding.tvOnline.setTypeface(typeface, Typeface.NORMAL);
            binding.tvBoth.setTypeface(typeface, Typeface.NORMAL);
            binding.tvLocal.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_700));
            binding.tvOnline.setTextColor(ContextCompat.getColor(getContext(), R.color.itemType));
            binding.tvBoth.setTextColor(ContextCompat.getColor(getContext(), R.color.itemType));
            binding.llLocal.setBackgroundResource(R.drawable.highlight);
            binding.llOnline.setBackgroundResource(0);
            binding.llBoth.setBackgroundResource(0);
        });

        binding.llOnline.setOnClickListener(v -> {
            selectedEventType = EVENT_TYPE_AT_VIRTUALLY;
            binding.tvLocal.setTypeface(typeface, Typeface.NORMAL);
            binding.tvOnline.setTypeface(typeface, Typeface.BOLD);
            binding.tvBoth.setTypeface(typeface, Typeface.NORMAL);
            binding.tvLocal.setTextColor(ContextCompat.getColor(getContext(), R.color.itemType));
            binding.tvOnline.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_700));
            binding.tvBoth.setTextColor(ContextCompat.getColor(getContext(), R.color.itemType));
            binding.llLocal.setBackgroundResource(0);
            binding.llOnline.setBackgroundResource(R.drawable.highlight);
            binding.llBoth.setBackgroundResource(0);
        });

        binding.llBoth.setOnClickListener(v -> {
            selectedEventType = EVENT_TYPE_BOTH;
            binding.tvLocal.setTypeface(typeface, Typeface.NORMAL);
            binding.tvOnline.setTypeface(typeface, Typeface.NORMAL);
            binding.tvBoth.setTypeface(typeface, Typeface.BOLD);
            binding.tvLocal.setTextColor(ContextCompat.getColor(getContext(), R.color.itemType));
            binding.tvOnline.setTextColor(ContextCompat.getColor(getContext(), R.color.itemType));
            binding.tvBoth.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_700));
            binding.llLocal.setBackgroundResource(0);
            binding.llOnline.setBackgroundResource(0);
            binding.llBoth.setBackgroundResource(R.drawable.highlight);
        });

        binding.ivRight.setOnClickListener(v -> {
            //Log.d(TAG, "AllEventsFrag " + selectedEventType);
            popupmanaged.dismiss();
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            EventSettingsAddEditFragment frag = new EventSettingsAddEditFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isSession", false);
            bundle.putBoolean("isEdit", isEdit);
            bundle.putString("selectedManager", new Gson().toJson(selectedManager));
            bundle.putInt("eventLogiType", selectedEventType);
            if (isEdit) {
                EventDefinition eventDefinition = eventDefinitions.get(selectedItemPosition);
                eventDefinition.setEvent_logi_type(selectedEventType);
                bundle.putString("eventDetails", new Gson().toJson(eventDefinition));
            }
            frag.setArguments(bundle);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, frag);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(frag);
            ft.commitAllowingStateLoss();
        });

        binding.ivClose.setOnClickListener(v -> popupmanaged.dismiss());
    }

    private void openAddEditEvent(int type) {
        Fragment fragment = null;
//        if (type == EVENT_TYPE_AT_LOCATION) {
//            fragment = new LEMainFragment();
//        } else if (type == EVENT_TYPE_AT_VIRTUALLY) {
//            fragment = new WEMainFragment();
//        } else if (type == EVENT_TYPE_BOTH) {
//            fragment = new LWEMainFragment();
//        }

        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        fragment = new EventSettingsAddEditFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isEdit", false);
        bundle.putInt("eventLogiType", type);
        bundle.putString("selectedManager", new Gson().toJson(selectedManager));
        fragment.setArguments(bundle);
        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.add(R.id.My_Container_1_ID, fragment);
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(fragment);
        ft.commitAllowingStateLoss();
    }

    private void fetchEvents() {
        new fetchEvents().execute();
    }

    @Override
    protected void initViews(View v) {

        eventsAdapter = new EventsAdapter();
        binding.rvEvents.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        binding.rvEvents.setAdapter(eventsAdapter);

    }

    private class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventHolder> {

        @NonNull
        @Override
        public EventsAdapter.EventHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//            return new EventHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_event_definition, viewGroup, false));
            return new EventHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_event_definition_new, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull EventHolder eventHolder, int i) {
            EventDefinition eventDefinition = eventDefinitions.get(i);
            if (eventDefinition != null) {
                if (eventDefinition.getEvent_name() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_name())) {
                    eventHolder.tvEventTitle.setText(eventDefinition.getEvent_name());
                }
                if (eventDefinition.getEvent_type() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_type())) {
                    eventHolder.tvEventType.setText(eventDefinition.getEvent_type());
                }
                Glide.with(getActivity()).load(eventDefinition.getEvent_logo()).placeholder(R.drawable.place_holder)
                        .error(R.drawable.place_holder).into(eventHolder.ivLogo);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    eventHolder.ivLogo.setClipToOutline(true);
                }

                if (eventDefinition.getEvent_address() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_address())
                        && !eventDefinition.getEvent_address().equals("null")) {
                    eventHolder.tvAddress.setText(eventDefinition.getEvent_address());
                    try {
                        JSONArray jsonArray = new JSONArray(eventDefinition.getEvent_address());
                        if (jsonArray.length() > 0) {
                            JSONArray jsonArray1 = jsonArray.getJSONArray(0);
                            if (jsonArray1.length() > 0) {
                                eventHolder.tvAddress.setText(jsonArray1.getString(0));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    eventHolder.tvAddress.setText("-");
                }
                if (eventDefinition.getCost() != null
                        && !TextUtils.isEmpty(eventDefinition.getCost())) {
                    if (eventDefinition.getCost().equals("0")) {
                        eventHolder.tvCost.setText("FREE");
                    } else {
                        eventHolder.tvCost.setText(String.format("$%s", eventDefinition.getCost()));
                    }
                } else {
                    eventHolder.tvCost.setText("FREE");
                }
                if (eventDefinition.getEvent_begins_date_time() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_begins_date_time())) {
                    eventHolder.tvEventDate.setText(DateTimeUtils.getDDMMM(eventDefinition.getEvent_begins_date_time()));
                } else {
                    eventHolder.tvEventDate.setText("-");
                }
                if (eventDefinition.getEvent_event_timings() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_event_timings())) {
                    try {
                        JSONArray jsonArrayTime = new JSONArray(eventDefinition.getEvent_event_timings());
                        StringBuilder stringBuilder = new StringBuilder();
                        if (jsonArrayTime != null && jsonArrayTime.length() > 0) {
                            JSONArray jsonArray2 = jsonArrayTime.getJSONArray(0);
                            if (jsonArray2 != null && jsonArray2.length() > 0) {
                                if (jsonArray2.get(0).toString().length() > 30) {
                                    String[] s = jsonArray2.get(0).toString().split(" - ");
                                    for (int j = 0; j < s.length; j++) {
                                        if (j != 0) {
                                            stringBuilder.append("-\n");
                                        }
                                        stringBuilder.append(DateTimeUtils.gmtToLocalDateAMPM(s[j]).substring(11));
                                    }
                                } else {
                                    stringBuilder.append(jsonArray2.get(0).toString());
                                }
                            }
                        }
                        eventHolder.tvEventTime.setText(stringBuilder.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    eventHolder.tvEventTime.setText("-");
                }
            }
            if (isCheckIn) {
                eventHolder.tvBtnEditEvent.setVisibility(View.GONE);
            } else {
                eventHolder.tvBtnEditEvent.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public int getItemCount() {
            return eventDefinitions.size();
        }

        class EventHolder extends RecyclerView.ViewHolder {
            TextView tvEventTitle, tvEventType, tvAddress, tvCost, tvEventDate, tvEventTime;
            TextView tvBtnEditEvent, tvBtnRepeatEvent;
            ImageView ivLogo;

            EventHolder(@NonNull View itemView) {
                super(itemView);
                ivLogo = itemView.findViewById(R.id.ivLogo);
                tvEventTitle = itemView.findViewById(R.id.tvEventTitle);
                tvEventType = itemView.findViewById(R.id.tvEventType);
                tvAddress = itemView.findViewById(R.id.tvAddress);
                tvCost = itemView.findViewById(R.id.tvCost);
                tvEventDate = itemView.findViewById(R.id.tvEventDate);
                tvEventTime = itemView.findViewById(R.id.tvEventTime);
                tvBtnEditEvent = itemView.findViewById(R.id.tvBtnEditEvent);
                tvBtnRepeatEvent = itemView.findViewById(R.id.tvBtnRepeatEvent);
                if (isCheckIn) {
                    tvBtnRepeatEvent.setVisibility(View.GONE);
                }

                itemView.setOnClickListener(v -> {
                    final int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        EventInfoViewFragment frag = new EventInfoViewFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("parkingType", parkingType);
                        bundle.putBoolean("isCheckIn", isCheckIn);
                        bundle.putInt("eventId", eventDefinitions.get(pos).getId());
                        bundle.putString("selectedManager", new Gson().toJson(selectedManager));
                        frag.setArguments(bundle);
                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.add(R.id.My_Container_1_ID, frag);
                        fragmentStack.lastElement().onPause();
                        ft.hide(fragmentStack.lastElement());
                        fragmentStack.push(frag);
                        ft.commitAllowingStateLoss();
                    }
                });

                tvBtnEditEvent.setOnClickListener(v -> {
                    final int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {

//                        selectedItemPosition = getAdapterPosition();
//                        EventOptionTypeDialog dialog = EventOptionTypeDialog.newInstance();
//                        Bundle bundle = new Bundle();
//                        bundle.putInt("eventLogiType", eventDefinitions.get(pos).getEvent_logi_type());
//                        bundle.putBoolean("isEdit", true);
//                        bundle.putBoolean("isSession", false);
//                        dialog.setArguments(bundle);
//                        dialog.show(getChildFragmentManager(), "payment");

                        selectedItemPosition = getAdapterPosition();
                        showAddEditEventOptionTypePupup(true, eventDefinitions.get(selectedItemPosition).getEvent_logi_type());

                    }
                });

                tvBtnRepeatEvent.setOnClickListener(v -> {
                    final int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        new AlertDialog.Builder(getActivity())
                                .setMessage("Are you sure, you want to repeat this Event?")
                                .setPositiveButton("YES", (dialog, which) -> {
                                    dialog.dismiss();
                                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    EventSettingsAddEditFragment frag = new EventSettingsAddEditFragment();
                                    Bundle bundle = new Bundle();
                                    bundle.putBoolean("isEdit", true);
                                    bundle.putBoolean("isRepeat", true);
                                    bundle.putString("selectedManager", new Gson().toJson(selectedManager));
                                    bundle.putString("eventDetails", new Gson().toJson(eventDefinitions.get(pos)));
                                    frag.setArguments(bundle);
                                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                    ft.add(R.id.My_Container_1_ID, frag);
                                    fragmentStack.lastElement().onPause();
                                    ft.hide(fragmentStack.lastElement());
                                    fragmentStack.push(frag);
                                    ft.commitAllowingStateLoss();
                                })
                                .setNegativeButton("NO", (dialog, which) -> dialog.dismiss())
                                .create().show();
                    }
                });
            }
        }
    }
}
