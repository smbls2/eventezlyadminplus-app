package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.TimePickerDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragmentConfigSettingsBinding;
import com.softmeasures.eventezlyadminplus.models.Manager;
import com.softmeasures.eventezlyadminplus.models.call_chat.CallsAVWebConfSettings;
import com.softmeasures.eventezlyadminplus.models.users.UserProfile;
import com.softmeasures.eventezlyadminplus.services.Constants;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ConfigSettingsFragment extends BaseFragment {
    FragmentConfigSettingsBinding binding;
    Manager selectedManger = null;
    UserProfile.UserProfileData selectedUser = null;
    ArrayList<Manager> selectedMangers = new ArrayList<>();
    ArrayList<UserProfile.UserProfileData> selectedUsers = new ArrayList<>();
    ArrayList<CallsAVWebConfSettings> tempCallConfSetting = new ArrayList<>();
    CallsAVWebConfSettings callConfSetting;
    private String TAG = "#DEBUG ConfigSettingsFragment";
    SharedPreferences logindeatl;
    String user_id, role;
    boolean isEdit = false, isMultiple = false, isCompany = false;
    private int pos = 0;
    TimePickerDialog picker;
    private PopupWindow popupmanaged;
    private PartnerAdapter partnerAdapter;
    private UserAdapter userAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_config_settings, container, false);
        if (getArguments() != null) {
            isMultiple = getArguments().getBoolean("isMultiple", false);
            selectedManger = new Gson().fromJson(getArguments().getString("selectedManager", null), Manager.class);
            selectedUser = new Gson().fromJson(getArguments().getString("selectedUser", null), UserProfile.UserProfileData.class);
            selectedMangers = new Gson().fromJson(getArguments().getString("selectedManagers", null), new TypeToken<ArrayList<Manager>>() {
            }.getType());
            selectedUsers = new Gson().fromJson(getArguments().getString("selectedUsers", null), new TypeToken<ArrayList<UserProfile.UserProfileData>>() {
            }.getType());
        }
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        initViews(view);
        setListeners();
        updateViews();
    }

    @Override
    protected void initViews(View v) {
        logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = logindeatl.getString("id", "null");
        role = logindeatl.getString("role", "");
        if (selectedManger != null || selectedUser != null)
            new fetchConfigSettings().execute();
        else if (isMultiple) {
            if (selectedMangers != null) {
                //if select multiple partner(company name) then concat selected.
                StringBuilder total = new StringBuilder();
                StringBuilder totalManagerid = new StringBuilder();
                for (int i=0;i<selectedMangers.size();i++)
                {
                    total.append(selectedMangers.get(i).getLot_manager()+" , ");
                    totalManagerid.append(selectedMangers.get(i).getManager_id()+" , ");
                }
                String completeText = total.toString();
                completeText = completeText.substring(0, completeText.length() - 2);
                String completeManagerid = totalManagerid.toString();
                completeManagerid = completeManagerid.substring(0,completeManagerid.length() - 2);
                //end multiple partner
                binding.etCompanyName.setText(completeText);
                binding.etCompanyCode.setText(completeManagerid);
//                binding.etCompanyName.setText(selectedMangers.get(selectedMangers.size() - 1).getLot_manager());
//                binding.etCompanyCode.setText(selectedMangers.get(selectedMangers.size() - 1).getManager_id());
                callConfSetting = new CallsAVWebConfSettings();
                for (int i = 0; i < selectedMangers.size(); i++)
                    new fetchConfigSettings(i).execute();
            }
            if (selectedUsers != null) {
                //if select multiple partner(company name) then concat selected.
                StringBuilder totalUsername = new StringBuilder();
                StringBuilder totalEmail = new StringBuilder();
                StringBuilder totalRole = new StringBuilder();
                for (int i=0;i<selectedUsers.size();i++)
                {
                    if (!TextUtils.isEmpty(selectedUsers.get(i).getUser_name()))
                    {
                        totalUsername.append(selectedUsers.get(i).getUser_name()+" , ");
                    }
                    else {
                        totalEmail.append(selectedUsers.get(i).getEmail()+" , ");
                    }
                    totalRole.append(selectedUsers.get(i).getRole()+" , ");
                }
                String completeUsername = null;
                if (!TextUtils.isEmpty(totalUsername.toString()))
                {

                    completeUsername = totalUsername.toString();
                    completeUsername = completeUsername.substring(0, completeUsername.length() - 2);    
                }
                String completeEmail = null;
                if (!TextUtils.isEmpty(totalEmail.toString()))
                {

                    completeEmail = totalEmail.toString();
                    completeEmail = completeEmail.substring(0,completeEmail.length() - 2);
                }

                String completeRole = totalRole.toString();
                completeRole = completeRole.substring(0,completeRole.length() - 2);
                //end multiple partner

                Log.d("checkusername","=username="+completeUsername);
                Log.d("checkusername","=email="+completeEmail);

                String combine_username_email =null;
                if (completeUsername != null && !completeUsername.isEmpty() && !completeUsername.equals("null") && completeEmail != null && !completeEmail.isEmpty() && !completeEmail.equals("null") )
                {
                    combine_username_email = completeUsername+" , "+completeEmail;
                }
                else if(completeUsername != null && !completeUsername.isEmpty() && !completeUsername.equals("null") )
                {
                    combine_username_email = completeUsername;
                }
                else if (completeEmail != null && !completeEmail.isEmpty() && !completeEmail.equals("null"))
                {
                    combine_username_email = completeEmail;
                }

                binding.etCompanyName.setText(combine_username_email);
                binding.etCompanyCode.setText(completeRole);

//                if (!TextUtils.isEmpty(selectedUsers.get(selectedUsers.size() - 1).getUser_name()))
//                    binding.etCompanyName.setText(selectedUsers.get(selectedUsers.size() - 1).getUser_name());
//                else
//                    binding.etCompanyName.setText(selectedUsers.get(selectedUsers.size() - 1).getEmail());
//                binding.etCompanyCode.setText(selectedUsers.get(selectedUsers.size() - 1).getRole());
                callConfSetting = new CallsAVWebConfSettings();
                for (int i = 0; i < selectedUsers.size(); i++)
                    new fetchConfigSettings(i).execute();
            }
        }
    }

    @Override
    protected void setListeners() {
        binding.switchTotalTimeUnlimited.setOnCheckedChangeListener((buttonView, isChecked) -> binding.llTotalTimeLimited.setVisibility(isChecked ? View.GONE : View.VISIBLE));
        binding.switchSessionTimeUnlimited.setOnCheckedChangeListener((buttonView, isChecked) -> binding.llSessionTimeLimited.setVisibility(isChecked ? View.GONE : View.VISIBLE));
        binding.etMaxParticipent.setOnClickListener(v ->
                showEnter_txt("Maximum Participant", "Enter Maximum Participant Limit", binding.etMaxParticipent, "etMaxParticipent", InputType.TYPE_CLASS_NUMBER, false));
      /*  binding.etRegionName.setOnClickListener(v ->
                showEnter_txt("Region Name", "Enter Region Name ", binding.etRegionName, "etRegionName", InputType.TYPE_CLASS_TEXT));
        binding.etBucketName.setOnClickListener(v ->
                showEnter_txt("Bucket Name", "Enter Bucket Name", binding.etBucketName, "etBucketName", InputType.TYPE_CLASS_TEXT));
        binding.etLocalRecDir.setOnClickListener(v ->
                showEnter_txt("Local Recording Directory", "Enter Local Recording Directory", binding.etLocalRecDir, "etLocalRecDir", InputType.TYPE_CLASS_TEXT));
        binding.etCloudRecDir.setOnClickListener(v ->
                showEnter_txt("Cloud Recording Directory", "Enter Cloud Recording Directory", binding.etCloudRecDir, "etCloudRecDir", InputType.TYPE_CLASS_TEXT));
      */
        binding.etCloudRecSizeSessionLimit.setOnClickListener(v ->
                showEnter_txt("Cloud Rec Size Session Limit", "Enter Cloud Recording Size Session Limit", binding.etCloudRecSizeSessionLimit, "etCloudRecSizeSessionLimit", InputType.TYPE_CLASS_NUMBER, false));
        binding.etSessionTimeLimit.setOnClickListener(v ->
                showEnter_txt("Session Time Limit", "Enter Session Time Limit", binding.etSessionTimeLimit, "etSessionTimeLimit", InputType.TYPE_CLASS_NUMBER, false));
        binding.etCloudRecSizeTotalLimit.setOnClickListener(v ->
                showEnter_txt("Cloud Rec Size Total Limit", "Enter Cloud Rec Size session  Limit", binding.etCloudRecSizeTotalLimit, "etCloudRecSizeTotalLimit", InputType.TYPE_CLASS_NUMBER, false));
        binding.etTotalTimeLimit.setOnClickListener(v -> {
            showEnter_txt("Total Time Limit", "Enter Total Time Limit", binding.etTotalTimeLimit, "etTotalTimeLimit", InputType.TYPE_CLASS_NUMBER, false);
            /*final Calendar cldr = Calendar.getInstance();
            int hour = cldr.get(Calendar.HOUR_OF_DAY);
            int minutes = cldr.get(Calendar.MINUTE);
            // time picker dialog
            picker = new TimePickerDialog(getActivity(),
                    new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker tp, int sHour, int sMinute) {
                            binding.etTotalTimeLimit.setText(sHour + ":" + sMinute);
                        }
                    }, hour, minutes, true);
            picker.show();*/
        });
        binding.etCompanyName.setOnClickListener(v -> {
            if (isMultiple) selectedCompanyiesUsers();
        });
        binding.etCompanyCode.setOnClickListener(v -> {
            if (isMultiple) selectedCompanyiesUsers();
        });
        binding.btnSaveConfig.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(binding.etMaxParticipent.getText()))
                callConfSetting.setMaximumUsers(Integer.parseInt(binding.etMaxParticipent.getText().toString()));
            else {
                Toast.makeText(myApp, "Please Enter Maximum Users", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!TextUtils.isEmpty(binding.etCloudRecSizeSessionLimit.getText()))
                callConfSetting.setCloudRecSizeSessionLimit(Integer.parseInt(binding.etCloudRecSizeSessionLimit.getText().toString()));
            else {
                Toast.makeText(myApp, "Please Enter Cloud Rec Size Session Limit", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!TextUtils.isEmpty(binding.etCloudRecSizeTotalLimit.getText()))
                callConfSetting.setCloudRecSizeTotalLimit(Integer.parseInt(binding.etCloudRecSizeTotalLimit.getText().toString()));
            else {
                Toast.makeText(myApp, "Please Enter Cloud Rec Size Total Limit", Toast.LENGTH_SHORT).show();
                return;
            }

            callConfSetting.setSessionTimeUnlimited(binding.switchSessionTimeUnlimited.isChecked());
            if (!callConfSetting.isSessionTimeUnlimited()) {
                if (!TextUtils.isEmpty(binding.etSessionTimeLimit.getText()))
                    callConfSetting.setSessionTimeLimit(Integer.parseInt(binding.etSessionTimeLimit.getText().toString()));
                else {
                    if (!callConfSetting.isSessionTimeUnlimited()) {
                        Toast.makeText(myApp, "Please Enter Session Time Limit", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }
            callConfSetting.setTotalTimeUnlimited(binding.switchTotalTimeUnlimited.isChecked());
            if (!callConfSetting.isTotalTimeUnlimited()) {
                if (!TextUtils.isEmpty(binding.etTotalTimeLimit.getText()))
                    callConfSetting.setTotalTimeLimit(Integer.parseInt(binding.etTotalTimeLimit.getText().toString()));
                else {
                    if (!callConfSetting.isTotalTimeUnlimited()) {
                        Toast.makeText(myApp, "Please Enter Total Time Limit", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }
          /*  if (!TextUtils.isEmpty(binding.etRegionName.getText()))
                callConfSetting.setRegionName(binding.etRegionName.getText().toString());
            else {
                Toast.makeText(myApp, "Please Enter Region Name ", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!TextUtils.isEmpty(binding.etBucketName.getText()))
                callConfSetting.setBucketName(binding.etBucketName.getText().toString());
            else {
                Toast.makeText(myApp, "Please Enter Bucket Name", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!TextUtils.isEmpty(binding.etCloudRecDir.getText()))
                callConfSetting.setCloudRecDir(binding.etCloudRecDir.getText().toString());
            else {
                Toast.makeText(myApp, "Please Enter Cloud Rec Directory", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!TextUtils.isEmpty(binding.etLocalRecDir.getText()))
                callConfSetting.setLocalRecDir(binding.etLocalRecDir.getText().toString());
            else {
                Toast.makeText(myApp, "Please Enter Local Rec Directory ", Toast.LENGTH_SHORT).show();
                return;
            }*/
            callConfSetting.setAttachLiveCamera(binding.switchAttachLiveCamera.isChecked());
            callConfSetting.setAttachLiveMic(binding.switchAttachLiveMic.isChecked());
            callConfSetting.setAttachDoc(binding.switchAttachDoc.isChecked());
            callConfSetting.setAttachImage(binding.switchAttachImage.isChecked());
            callConfSetting.setAttachAudio(binding.switchAttachAudio.isChecked());
            callConfSetting.setAttachVideo(binding.switchAttachVideo.isChecked());
            callConfSetting.setAttachZip(binding.switchAttachZip.isChecked());
            /*callConfSetting.setDisplayAttachDoc(binding.switchDisplayAttachDoc.isChecked());
            callConfSetting.setDisplayAttachImage(binding.switchDisplayAttachImage.isChecked());
            callConfSetting.setDisplayAttachAudio(binding.switchDisplayAttachAudio.isChecked());
            callConfSetting.setDisplayAttachVideo(binding.switchDisplayAttachVideo.isChecked());*/
            callConfSetting.setRecAllowedHost(binding.switchRecHostAllowed.isChecked());
            callConfSetting.setRecAllowedGuest(binding.switchRecGuestAllowed.isChecked());
            callConfSetting.setEventAudioRecordingLocal(binding.switchEventAudioRecordingLocal.isChecked());
            callConfSetting.setEventAudioRecordingCloud(binding.switchEventAudioRecordingCloud.isChecked());
            callConfSetting.setEventVideoRecordingLocal(binding.switchEventVideoRecordingLocal.isChecked());
            callConfSetting.setEventVideoRecordingCloud(binding.switchEventVideoRecordingCloud.isChecked());
            callConfSetting.setEventWebinarRecordingLocal(binding.switchEventWebinarRecordingLocal.isChecked());
            callConfSetting.setEventWebinarRecordingCloud(binding.switchEventWebinarRecordingCloud.isChecked());
            callConfSetting.setInstantAudioRecordingLocal(binding.switchInstantAudioRecordingLocal.isChecked());
            callConfSetting.setInstantVideoRecordingCloud(binding.switchInstantAudioRecordingLocal.isChecked());

            /*callConfSetting.setCloudRecSizeSessionLimit(Integer.parseInt(binding.etCloudRecSizeSessionLimit.getText().toString()));
            callConfSetting.setCloudRecSizeTotalLimit(Integer.parseInt(binding.etCloudRecSizeSessionLimit.getText().toString()));
            callConfSetting.setSessionTimeLimit(Integer.parseInt(binding.etCloudRecSizeSessionLimit.getText().toString()));
            callConfSetting.setTotalTimeLimit(Integer.parseInt(binding.etCloudRecSizeSessionLimit.getText().toString()));*/
            if (isMultiple) {
                if (selectedMangers != null) {
                    Gson gson = new Gson();
                    if (tempCallConfSetting != null) tempCallConfSetting.clear();
                    for (int i = 0; i < selectedMangers.size(); i++) {
                        callConfSetting.setCompanyId(selectedMangers.get(i).getId());
                        callConfSetting.setCompanyTypeId(selectedMangers.get(i).getManager_type_id());
                        callConfSetting.setCompanyName(selectedMangers.get(i).getLot_manager());
                        callConfSetting.setCompanyCode(selectedMangers.get(i).getManager_id());
                        callConfSetting.setId(selectedMangers.get(i).getSelected_id());
                        tempCallConfSetting.add(gson.fromJson(new Gson().toJson(callConfSetting), CallsAVWebConfSettings.class));
                    }
                    pos = 0;
                    new addEditCallConfigSettings(tempCallConfSetting.get(0), tempCallConfSetting.get(0).getId() != 0).execute();
                } else if (selectedUsers != null) {
                    Gson gson = new Gson();
                    if (tempCallConfSetting != null) tempCallConfSetting.clear();
                    for (int i = 0; i < selectedUsers.size(); i++) {
                        callConfSetting.setUserId(selectedUsers.get(i).getUser_id());
                        callConfSetting.setUserProfileType(selectedUsers.get(i).getRole());
                        callConfSetting.setId(selectedUsers.get(i).getSelected_id());
                        tempCallConfSetting.add(gson.fromJson(new Gson().toJson(callConfSetting), CallsAVWebConfSettings.class));
                    }
                    pos = 0;
                    new addEditCallConfigSettings(tempCallConfSetting.get(0), tempCallConfSetting.get(0).getId() != 0).execute();
                }
            } else new addEditCallConfigSettings(callConfSetting, isEdit).execute();
        });
    }

    @Override
    protected void updateViews() {
        if (selectedManger != null) {
            binding.tvTitleName.setText("Company Name");
            binding.tvTitleCode.setText("Company Code");
            binding.etCompanyName.setText(selectedManger.getLot_manager());
            binding.etCompanyCode.setText(selectedManger.getManager_id());
        } else if (selectedUser != null) {
            binding.tvTitleName.setText("User Name");
            binding.tvTitleCode.setText("User Role");
            if (!TextUtils.isEmpty(selectedUser.getUser_name()))
                binding.etCompanyName.setText(selectedUser.getUser_name());
            else binding.etCompanyName.setText(selectedUser.getEmail());
            binding.etCompanyCode.setText(selectedUser.getRole());
        }
        if (selectedUsers !=null)
        {
            binding.tvTitleName.setText("User Name");
            binding.tvTitleCode.setText("User Role");
        }else if(selectedMangers != null)
        {
            binding.tvTitleName.setText("Company Name");
            binding.tvTitleCode.setText("Company Code");
        }
    }

    public void selectedCompanyiesUsers() {
        if (selectedMangers != null || selectedUsers != null) {
            LinearLayout viewGroup = (LinearLayout) getActivity().findViewById(R.id.popup);
            LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View layout = layoutInflater.inflate(R.layout.popup_selected_company_user_list, viewGroup, false);
            popupmanaged = new PopupWindow(getActivity());
            popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
            popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
            popupmanaged.setContentView(layout);
            popupmanaged.setBackgroundDrawable(null);
            popupmanaged.setFocusable(true);
            popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);
            RecyclerView rvMeetingId;
            TextView txt_title;
            ImageView ivClose;

            txt_title = (TextView) layout.findViewById(R.id.tvTitle);
            rvMeetingId = (RecyclerView) layout.findViewById(R.id.rvMeetingId);
            ivClose = (ImageView) layout.findViewById(R.id.ivClose);

            rvMeetingId.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            if (selectedMangers != null) {
                txt_title.setText("Selected Companies");
                partnerAdapter = new PartnerAdapter();
                rvMeetingId.setAdapter(partnerAdapter);
            } else if (selectedUsers != null) {
                txt_title.setText("Selected Users");
                userAdapter = new UserAdapter();
                rvMeetingId.setAdapter(userAdapter);
            }
            ivClose.setOnClickListener(v1 -> popupmanaged.dismiss());
        } else {
            Toast.makeText(myApp, "You don't have any previous meeting.", Toast.LENGTH_SHORT).show();
        }
    }

    private void showEnter_txt(String title, String hintText, TextView etTextView, String selectedEditText, int inputType, boolean isParkingFees) {
        RelativeLayout viewGroup = (RelativeLayout) getActivity().findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.enter_txt, viewGroup, false);
        final PopupWindow popup = new PopupWindow(getActivity());
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(true);
        popup.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        popup.setFocusable(true);
        popup.update();
        popup.showAtLocation(layout, Gravity.TOP, 0, 0);

        final EditText edit_text;
        final TextView text_title, tvPaste;
        final Button btn_done;
        RelativeLayout btn_cancel;
        LinearLayout rl_edit, rlFees;
        ImageView ivPaste;
        SwitchCompat switchDay, switchEvent, switchHR, switchMin;

        tvPaste = (TextView) layout.findViewById(R.id.tvPaste);
        ivPaste = (ImageView) layout.findViewById(R.id.ivPaste);
        edit_text = (EditText) layout.findViewById(R.id.edit_nu);
        text_title = (TextView) layout.findViewById(R.id.txt_title);
        rl_edit = (LinearLayout) layout.findViewById(R.id.rl_edit);

        switchDay = (SwitchCompat) layout.findViewById(R.id.switchDay);
        switchEvent = (SwitchCompat) layout.findViewById(R.id.switchEvent);
        switchHR = (SwitchCompat) layout.findViewById(R.id.switchHR);
        switchMin = (SwitchCompat) layout.findViewById(R.id.switchMin);
        rlFees = (LinearLayout) layout.findViewById(R.id.rl_fees);

        text_title.setText(title);
        edit_text.setHint(hintText);
        edit_text.setInputType(inputType);
        if (!TextUtils.isEmpty(etTextView.getText()))
            edit_text.setText(etTextView.getText());
        btn_cancel = (RelativeLayout) layout.findViewById(R.id.close);
        btn_done = (Button) layout.findViewById(R.id.btn_done);

        if (isParkingFees) {
            rlFees.setVisibility(View.VISIBLE);
            if (true) {
                String duration = "Hour";
                if (duration.equals("Event"))
                    switchEvent.setChecked(true);
                else if (duration.equals("Day"))
                    switchDay.setChecked(true);
                else if (duration.equals("Hour"))
                    switchHR.setChecked(true);
                else if (duration.equals("Minute"))
                    switchMin.setChecked(true);
            } else {
                switchEvent.setChecked(true);
            }
        } else
            rlFees.setVisibility(View.GONE);

        edit_text.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        edit_text.setFocusable(true);
        edit_text.setSelection(edit_text.getText().length());
        edit_text.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            if (imm.isAcceptingText()) {
                Log.d(TAG, "Software Keyboard was shown");
            } else {
                Log.d(TAG, "Software Keyboard was not shown");
            }
        });
        rl_edit.setOnClickListener(v -> tvPaste.setVisibility(View.GONE));
        edit_text.setOnLongClickListener(v -> {
            tvPaste.setVisibility(View.VISIBLE);
            return true;
        });
        tvPaste.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData pData = clipboard.getPrimaryClip();
            if (pData != null) {
                ClipData.Item item = pData.getItemAt(0);
                String txtpaste = item.getText().toString();
                edit_text.getText().insert(edit_text.getSelectionStart(), txtpaste);
                edit_text.setText(edit_text.getText()+txtpaste);
            }
        });
        ivPaste.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData pData = clipboard.getPrimaryClip();
            if (pData != null) {
                ClipData.Item item = pData.getItemAt(0);
                String txtpaste = item.getText().toString();
                edit_text.getText().insert(edit_text.getSelectionStart(), txtpaste);
                edit_text.setText(edit_text.getText()+txtpaste);

            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imm.isAcceptingText()) {
                    Log.d(TAG, "DONE: Software Keyboard was shown");
                } else {
                    Log.d(TAG, "DONE: Software Keyboard was not shown");
                }
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                if (isParkingFees) {
                    String duration = "Event";
                    if (switchEvent.isChecked())
                        duration = "Event";
                    else if (switchDay.isChecked())
                        duration = "Day";
                    else if (switchHR.isChecked())
                        duration = "Hour";
                    else if (switchMin.isChecked())
                        duration = "Minute";
                    etTextView.setText("$" + edit_text.getText().toString() + "/1 " + duration);
                } else {
                    etTextView.setText(edit_text.getText().toString());

                }
                popup.dismiss();
                /*if (selectedEditText.equals("etRegionName")) binding.etBucketName.performClick();
                if (selectedEditText.equals("etBucketName")) binding.etCloudRecDir.performClick();
                if (selectedEditText.equals("etCloudRecDir")) binding.etLocalRecDir.performClick();
                if (selectedEditText.equals("etLocalRecDir"))
                    binding.etCloudRecSizeSessionLimit.performClick();*/
                if (selectedEditText.equals("etCloudRecSizeSessionLimit"))
                    if (!binding.switchSessionTimeUnlimited.isChecked())
                        binding.etSessionTimeLimit.performClick();
                    else
                        binding.etCloudRecSizeTotalLimit.performClick();
                if (selectedEditText.equals("etSessionTimeLimit"))
                    binding.etCloudRecSizeTotalLimit.performClick();
                if (selectedEditText.equals("etCloudRecSizeTotalLimit"))
                    if (!binding.switchTotalTimeUnlimited.isChecked())
                        binding.etTotalTimeLimit.performClick();

            }
        });
        btn_cancel.setOnClickListener(v -> {
            InputMethodManager imm1 = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm1.hideSoftInputFromWindow(edit_text.getWindowToken(), 0);
            String txt = edit_text.getText().toString();
            popup.dismiss();
        });
    }

    public class addEditCallConfigSettings extends AsyncTask<String, String, String> {
        String exit_status = "_table/calls_av_web_conf_settings";
        JSONObject json1;
        int gid = 0, i = 0;
        boolean isUserEdit = false;
        CallsAVWebConfSettings callConfSetting;

        public addEditCallConfigSettings(CallsAVWebConfSettings callConfSetting, boolean isEdit) {
            isUserEdit = isEdit;
            this.callConfSetting = callConfSetting;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            binding.rlProgressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            if (isUserEdit) {
                jsonValues.put("id", callConfSetting.getId());
                jsonValues.put("user_access_level", Constants.COMPANY_USER);
                jsonValues.put("event_audio_recording_local", callConfSetting.isEventAudioRecordingLocal());
                jsonValues.put("event_audio_recording_cloud", callConfSetting.isEventAudioRecordingCloud());
                jsonValues.put("event_video_recording_local", callConfSetting.isEventVideoRecordingLocal());
                jsonValues.put("event_video_recording_cloud", callConfSetting.isEventVideoRecordingLocal());
                jsonValues.put("instant_audio_recording_local", callConfSetting.isInstantAudioRecordingLocal());
                jsonValues.put("instant_video_recording_cloud", callConfSetting.isInstantVideoRecordingCloud());
                jsonValues.put("event_webinar_recording_local", callConfSetting.isEventWebinarRecordingLocal());
                jsonValues.put("event_webinar_recording_cloud", callConfSetting.isEventWebinarRecordingCloud());
                jsonValues.put("region_name", callConfSetting.getRegionName());
                jsonValues.put("bucket_name", callConfSetting.getBucketName());
                jsonValues.put("cloud_rec_dir", callConfSetting.getCloudRecDir());
                jsonValues.put("local_rec_dir", callConfSetting.getLocalRecDir());
                jsonValues.put("rec_allowed_host", callConfSetting.isRecAllowedHost());
                jsonValues.put("rec_allowed_guest", callConfSetting.isRecAllowedGuest());

                jsonValues.put("attach_live_camera", callConfSetting.isAttachLiveCamera());
                jsonValues.put("attach_live_mic", callConfSetting.isAttachLiveMic());
                jsonValues.put("display_attach_doc", callConfSetting.isDisplayAttachDoc());
                jsonValues.put("display_attach_video", callConfSetting.isDisplayAttachVideo());
                jsonValues.put("display_attach_audio", callConfSetting.isDisplayAttachAudio());
                jsonValues.put("display_attach_image", callConfSetting.isDisplayAttachImage());
                jsonValues.put("attach_zip", callConfSetting.isAttachZip());
                jsonValues.put("attach_doc", callConfSetting.isAttachDoc());
                jsonValues.put("attach_video", callConfSetting.isAttachVideo());
                jsonValues.put("attach_audio", callConfSetting.isAttachAudio());
                jsonValues.put("attach_image", callConfSetting.isAttachImage());
                jsonValues.put("cloud_rec_size_session_limit", callConfSetting.getCloudRecSizeSessionLimit());
                jsonValues.put("cloud_rec_size_total_limit", callConfSetting.getCloudRecSizeTotalLimit());
                jsonValues.put("maximum_users", callConfSetting.getMaximumUsers());
                jsonValues.put("meeting_id_sourcetype", "");

                jsonValues.put("total_time_unlimited", callConfSetting.isTotalTimeUnlimited());
                jsonValues.put("total_time_limit", callConfSetting.getTotalTimeLimit());
                jsonValues.put("session_time_unlimited", callConfSetting.isSessionTimeUnlimited());
                jsonValues.put("session_time_limit", callConfSetting.getSessionTimeLimit());
            } else {
                jsonValues.put("datetime", "");

                jsonValues.put("company_type_id", callConfSetting.getCompanyTypeId());
                jsonValues.put("company_id", callConfSetting.getCompanyId());
                jsonValues.put("company_code", callConfSetting.getCompanyCode());
                jsonValues.put("company_name", callConfSetting.getCompanyName());
                jsonValues.put("user_profile_type", callConfSetting.getUserProfileType());
                jsonValues.put("user_id", callConfSetting.getUserId());
                jsonValues.put("app_name", "EventEzlyAdmin Plus");
                jsonValues.put("user_access_level", Constants.COMPANY_USER);
                jsonValues.put("app_id", 23);
                jsonValues.put("event_audio_recording_local", callConfSetting.isEventAudioRecordingLocal());
                jsonValues.put("event_audio_recording_cloud", callConfSetting.isEventAudioRecordingCloud());
                jsonValues.put("event_video_recording_local", callConfSetting.isEventVideoRecordingLocal());
                jsonValues.put("event_video_recording_cloud", callConfSetting.isEventVideoRecordingLocal());
                jsonValues.put("instant_audio_recording_local", callConfSetting.isInstantAudioRecordingLocal());
                jsonValues.put("instant_video_recording_cloud", callConfSetting.isInstantVideoRecordingCloud());
                jsonValues.put("event_webinar_recording_local", callConfSetting.isEventWebinarRecordingLocal());
                jsonValues.put("event_webinar_recording_cloud", callConfSetting.isEventWebinarRecordingCloud());
                jsonValues.put("region_name", callConfSetting.getRegionName());
                jsonValues.put("bucket_name", callConfSetting.getBucketName());
                jsonValues.put("cloud_rec_dir", callConfSetting.getCloudRecDir());
                jsonValues.put("local_rec_dir", callConfSetting.getLocalRecDir());
                jsonValues.put("rec_allowed_host", callConfSetting.isRecAllowedHost());
                jsonValues.put("rec_allowed_guest", callConfSetting.isRecAllowedGuest());

                jsonValues.put("attach_live_camera", callConfSetting.isAttachLiveCamera());
                jsonValues.put("attach_live_mic", callConfSetting.isAttachLiveMic());
                jsonValues.put("display_attach_doc", callConfSetting.isDisplayAttachDoc());
                jsonValues.put("display_attach_video", callConfSetting.isDisplayAttachVideo());
                jsonValues.put("display_attach_audio", callConfSetting.isDisplayAttachAudio());
                jsonValues.put("display_attach_image", callConfSetting.isDisplayAttachImage());
                jsonValues.put("attach_zip", callConfSetting.isAttachZip());
                jsonValues.put("attach_doc", callConfSetting.isAttachDoc());
                jsonValues.put("attach_video", callConfSetting.isAttachVideo());
                jsonValues.put("attach_audio", callConfSetting.isAttachAudio());
                jsonValues.put("attach_image", callConfSetting.isAttachImage());
                jsonValues.put("cloud_rec_size_session_limit", callConfSetting.getCloudRecSizeSessionLimit());
                jsonValues.put("cloud_rec_size_total_limit", callConfSetting.getCloudRecSizeTotalLimit());
                jsonValues.put("maximum_users", callConfSetting.getMaximumUsers());
                jsonValues.put("meeting_id_sourcetype", "");
                jsonValues.put("total_time_unlimited", callConfSetting.isTotalTimeUnlimited());
                jsonValues.put("total_time_limit", callConfSetting.getTotalTimeLimit());
                jsonValues.put("session_time_unlimited", callConfSetting.isSessionTimeUnlimited());
                jsonValues.put("session_time_limit", callConfSetting.getSessionTimeLimit());
            }
            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpResponse response = null;
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            if (isUserEdit) {
                HttpPut post = new HttpPut(url);
                post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
                post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
                post.setEntity(entity);
                Log.d(TAG, "HttpPut:  updateURL:  " + url);
                Log.d(TAG, "HttpPut:  update Params: " + String.valueOf(json));
                try {
                    response = client.execute(post);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                HttpPost post = new HttpPost(url);
                post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
                post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
                post.setEntity(entity);
                Log.d(TAG, "  HttpPost:  addURL:  " + url);
                Log.d(TAG, "   HttpPost:  add Params: " + String.valueOf(json));
                try {
                    response = client.execute(post);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.d(TAG, "   Add/Edit Instant Meeting Detail:  Response:  " + responseStr);
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        gid = c.getInt("id");
//                        callConfSetting.setId(gid);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                if (isMultiple) {
                    if (tempCallConfSetting.size() > 0) {
                        if (selectedMangers != null)
                            selectedMangers.get(pos).setSelected_id(gid);
                        if (selectedUsers != null)
                            selectedUsers.get(pos).setSelected_id(gid);
                        pos += 1;
                        tempCallConfSetting.remove(0);
                        if (tempCallConfSetting.size() == 0) {
                            binding.rlProgressbar.setVisibility(View.GONE);
                            Toast.makeText(myApp, "Configuration Save", Toast.LENGTH_LONG).show();
                            Log.d("here","==1");
                        } else {
                            Log.e(TAG, "Congif: " + tempCallConfSetting.get(0).getId());
                            new addEditCallConfigSettings(tempCallConfSetting.get(0), tempCallConfSetting.get(0).getId() != 0).execute();
                        }
                    }
                } else {
                    if (!isEdit) {
                        callConfSetting.setId(gid);
                        isEdit = true;
                    }
                    binding.rlProgressbar.setVisibility(View.GONE);
                    Toast.makeText(myApp, "Configuration Save", Toast.LENGTH_LONG).show();
                    Log.d("here","==2");
                }
            }
        }
    }

    public class fetchConfigSettings extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String parkingurl = "_table/calls_av_web_conf_settings";

        public fetchConfigSettings() {
        }

        public fetchConfigSettings(int pos) {
            i = pos;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            binding.rlProgressbar.setVisibility(View.VISIBLE);
            if (isMultiple) {
                if (selectedMangers != null)
                    parkingurl = "_table/calls_av_web_conf_settings?filter=((company_id=" + selectedMangers.get(i).getId() + ")AND(company_type_id=" + selectedMangers.get(i).getManager_type_id() + "))";
                if (selectedUsers != null)
                    parkingurl = "_table/calls_av_web_conf_settings?filter=(user_id=" + selectedUsers.get(i).getUser_id() + ")";
            } else {
                if (selectedManger != null)
                    parkingurl = "_table/calls_av_web_conf_settings?filter=((company_id=" + selectedManger.getId() + ")AND(company_type_id=" + selectedManger.getManager_type_id() + "))";
               // Log.d("manager_id", "==" + selectedManger.getId());
                if (selectedUser != null)
                    parkingurl = "_table/calls_av_web_conf_settings?filter=(user_id=" + selectedUser.getUser_id() + ")";
                //Log.d("user_id","=="+selectedUser.getUser_id());

            }
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            try {
                String url = getActivity().getString(R.string.api) + getActivity().getString(R.string.povlive) + parkingurl;
                Log.d("#DEBUG", "   fetchConfigSettings:  URL:  " + url);
                DefaultHttpClient client = new DefaultHttpClient();
                HttpGet post = new HttpGet(url);
                post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
                post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
                HttpResponse response = null;
                try {
                    response = client.execute(post);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (response != null) {
                    HttpEntity resEntity = response.getEntity();
                    String responseStr = null;
                    try {
                        responseStr = EntityUtils.toString(resEntity).trim();
                        Log.d("#DEBUG", "   fetchConfigSettings:  Response:  " + responseStr);
                        json = new JSONObject(responseStr);
                        JSONArray array = json.getJSONArray("resource");
                        if (isMultiple) {
                            Log.e(TAG, " isMultiple: " + i);
                            if (array.length() > 0) {
                                for (int j = 0; j < array.length(); j++) {
                                    JSONObject object = array.getJSONObject(j);
                                    if (object.has("id")
                                            && !TextUtils.isEmpty(object.getString("id"))
                                            && !object.getString("id").equals("null")) {
                                        if (selectedMangers != null)
                                            selectedMangers.get(i).setSelected_id(object.getInt("id"));
                                        else if (selectedUsers != null)
                                            selectedUsers.get(i).setSelected_id(object.getInt("id"));
                                    }
                                }
                            } else {
                                if (selectedMangers != null)
                                    selectedMangers.get(i).setSelected_id(0);
                                else if (selectedUsers != null)
                                    selectedUsers.get(i).setSelected_id(0);
                            }
                        } else {
                            for (int j = 0; j < array.length(); j++) {
                                JSONObject object = array.getJSONObject(j);
                                callConfSetting = new CallsAVWebConfSettings();
                                if (object.has("id")
                                        && !TextUtils.isEmpty(object.getString("id"))
                                        && !object.getString("id").equals("null")) {
                                    callConfSetting.setId(object.getInt("id"));
                                }

                                if (object.has("datetime")
                                        && !TextUtils.isEmpty(object.getString("datetime"))
                                        && !object.getString("datetime").equals("null")) {
                                    callConfSetting.setDatetime(object.getString("datetime"));
                                }
                                if (object.has("company_type_id")
                                        && !TextUtils.isEmpty(object.getString("company_type_id"))
                                        && !object.getString("company_type_id").equals("null")) {
                                    callConfSetting.setCompanyTypeId(object.getInt("company_type_id"));
                                }
                                if (object.has("company_id")
                                        && !TextUtils.isEmpty(object.getString("company_id"))
                                        && !object.getString("company_id").equals("null")) {
                                    callConfSetting.setCompanyId(object.getInt("company_id"));
                                }
                                if (object.has("company_code")
                                        && !TextUtils.isEmpty(object.getString("company_code"))
                                        && !object.getString("company_code").equals("null")) {
                                    callConfSetting.setCompanyCode(object.getString("company_code"));
                                }
                                if (object.has("company_name")
                                        && !TextUtils.isEmpty(object.getString("company_name"))
                                        && !object.getString("company_name").equals("null")) {
                                    callConfSetting.setCompanyName(object.getString("company_name"));
                                }
                                if (object.has("user_profile_type")
                                        && !TextUtils.isEmpty(object.getString("user_profile_type"))
                                        && !object.getString("user_profile_type").equals("null")) {
                                    callConfSetting.setUserProfileType(object.getString("user_profile_type"));
                                }
                                if (object.has("user_id")
                                        && !TextUtils.isEmpty(object.getString("user_id"))
                                        && !object.getString("user_id").equals("null")) {
                                    callConfSetting.setUserId(object.getInt("user_id"));
                                }
                                if (object.has("app_name")
                                        && !TextUtils.isEmpty(object.getString("app_name"))
                                        && !object.getString("app_name").equals("null")) {
                                    callConfSetting.setAppName(object.getString("app_name"));
                                }
                                if (object.has("app_id")
                                        && !TextUtils.isEmpty(object.getString("app_id"))
                                        && !object.getString("app_id").equals("null")) {
                                    callConfSetting.setAppId(object.getInt("app_id"));
                                }
                                if (object.has("event_audio_recording_local")
                                        && !TextUtils.isEmpty(object.getString("event_audio_recording_local"))
                                        && !object.getString("event_audio_recording_local").equals("null")) {
                                    callConfSetting.setEventAudioRecordingLocal(object.getBoolean("event_audio_recording_local"));
                                }
                                if (object.has("event_audio_recording_cloud")
                                        && !TextUtils.isEmpty(object.getString("event_audio_recording_cloud"))
                                        && !object.getString("event_audio_recording_cloud").equals("null")) {
                                    callConfSetting.setEventAudioRecordingCloud(object.getBoolean("event_audio_recording_cloud"));
                                }
                                if (object.has("event_video_recording_local")
                                        && !TextUtils.isEmpty(object.getString("event_video_recording_local"))
                                        && !object.getString("event_video_recording_local").equals("null")) {
                                    callConfSetting.setEventVideoRecordingLocal(object.getBoolean("event_video_recording_local"));
                                }
                                if (object.has("event_video_recording_cloud")
                                        && !TextUtils.isEmpty(object.getString("event_video_recording_cloud"))
                                        && !object.getString("event_video_recording_cloud").equals("null")) {
                                    callConfSetting.setEventVideoRecordingCloud(object.getBoolean("event_video_recording_cloud"));
                                }
                                if (object.has("instant_audio_recording_local")
                                        && !TextUtils.isEmpty(object.getString("instant_audio_recording_local"))
                                        && !object.getString("instant_audio_recording_local").equals("null")) {
                                    callConfSetting.setInstantAudioRecordingLocal(object.getBoolean("instant_audio_recording_local"));
                                }
                                if (object.has("instant_video_recording_cloud")
                                        && !TextUtils.isEmpty(object.getString("instant_video_recording_cloud"))
                                        && !object.getString("instant_video_recording_cloud").equals("null")) {
                                    callConfSetting.setInstantVideoRecordingCloud(object.getBoolean("instant_video_recording_cloud"));
                                }
                                if (object.has("event_webinar_recording_local")
                                        && !TextUtils.isEmpty(object.getString("event_webinar_recording_local"))
                                        && !object.getString("event_webinar_recording_local").equals("null")) {
                                    callConfSetting.setEventWebinarRecordingLocal(object.getBoolean("event_webinar_recording_local"));
                                }
                                if (object.has("event_webinar_recording_cloud")
                                        && !TextUtils.isEmpty(object.getString("event_webinar_recording_cloud"))
                                        && !object.getString("event_webinar_recording_cloud").equals("null")) {
                                    callConfSetting.setEventWebinarRecordingCloud(object.getBoolean("event_webinar_recording_cloud"));
                                }
                                if (object.has("region_name")
                                        && !TextUtils.isEmpty(object.getString("region_name"))
                                        && !object.getString("region_name").equals("null")) {
                                    callConfSetting.setRegionName(object.getString("region_name"));
                                }
                                if (object.has("bucket_name")
                                        && !TextUtils.isEmpty(object.getString("bucket_name"))
                                        && !object.getString("bucket_name").equals("null")) {
                                    callConfSetting.setBucketName(object.getString("bucket_name"));
                                }
                                if (object.has("cloud_rec_dir")
                                        && !TextUtils.isEmpty(object.getString("cloud_rec_dir"))
                                        && !object.getString("cloud_rec_dir").equals("null")) {
                                    callConfSetting.setCloudRecDir(object.getString("cloud_rec_dir"));
                                }
                                if (object.has("local_rec_dir")
                                        && !TextUtils.isEmpty(object.getString("local_rec_dir"))
                                        && !object.getString("local_rec_dir").equals("null")) {
                                    callConfSetting.setLocalRecDir(object.getString("local_rec_dir"));
                                }

                                if (object.has("rec_allowed_host")
                                        && !TextUtils.isEmpty(object.getString("rec_allowed_host"))
                                        && !object.getString("rec_allowed_host").equals("null")) {
                                    callConfSetting.setRecAllowedHost(object.getBoolean("rec_allowed_host"));
                                }
                                if (object.has("rec_allowed_guest")
                                        && !TextUtils.isEmpty(object.getString("rec_allowed_guest"))
                                        && !object.getString("rec_allowed_guest").equals("null")) {
                                    callConfSetting.setRecAllowedGuest(object.getBoolean("rec_allowed_guest"));
                                }

                                if (object.has("attach_live_camera")
                                        && !TextUtils.isEmpty(object.getString("attach_live_camera"))
                                        && !object.getString("attach_live_camera").equals("null")) {
                                    callConfSetting.setAttachLiveCamera(object.getBoolean("attach_live_camera"));
                                }
                                if (object.has("attach_live_mic")
                                        && !TextUtils.isEmpty(object.getString("attach_live_mic"))
                                        && !object.getString("attach_live_mic").equals("null")) {
                                    callConfSetting.setAttachLiveMic(object.getBoolean("attach_live_mic"));
                                }
                                if (object.has("display_attach_doc")
                                        && !TextUtils.isEmpty(object.getString("display_attach_doc"))
                                        && !object.getString("display_attach_doc").equals("null")) {
                                    callConfSetting.setDisplayAttachDoc(object.getBoolean("display_attach_doc"));
                                }
                                if (object.has("display_attach_image")
                                        && !TextUtils.isEmpty(object.getString("display_attach_image"))
                                        && !object.getString("display_attach_image").equals("null")) {
                                    callConfSetting.setDisplayAttachImage(object.getBoolean("display_attach_image"));
                                }
                                if (object.has("display_attach_video")
                                        && !TextUtils.isEmpty(object.getString("display_attach_video"))
                                        && !object.getString("display_attach_video").equals("null")) {
                                    callConfSetting.setDisplayAttachVideo(object.getBoolean("display_attach_video"));
                                }
                                if (object.has("display_attach_audio")
                                        && !TextUtils.isEmpty(object.getString("display_attach_audio"))
                                        && !object.getString("display_attach_audio").equals("null")) {
                                    callConfSetting.setDisplayAttachAudio(object.getBoolean("display_attach_audio"));
                                }
                                if (object.has("attach_zip")
                                        && !TextUtils.isEmpty(object.getString("attach_zip"))
                                        && !object.getString("attach_zip").equals("null")) {
                                    callConfSetting.setAttachZip(object.getBoolean("attach_zip"));
                                }
                                if (object.has("attach_doc")
                                        && !TextUtils.isEmpty(object.getString("attach_doc"))
                                        && !object.getString("attach_doc").equals("null")) {
                                    callConfSetting.setAttachDoc(object.getBoolean("attach_doc"));
                                }
                                if (object.has("attach_video")
                                        && !TextUtils.isEmpty(object.getString("attach_video"))
                                        && !object.getString("attach_video").equals("null")) {
                                    callConfSetting.setAttachVideo(object.getBoolean("attach_video"));
                                }
                                if (object.has("attach_audio")
                                        && !TextUtils.isEmpty(object.getString("attach_audio"))
                                        && !object.getString("attach_audio").equals("null")) {
                                    callConfSetting.setAttachAudio(object.getBoolean("attach_audio"));
                                }
                                if (object.has("attach_image")
                                        && !TextUtils.isEmpty(object.getString("attach_image"))
                                        && !object.getString("attach_image").equals("null")) {
                                    callConfSetting.setAttachImage(object.getBoolean("attach_image"));
                                }
                                if (object.has("cloud_rec_size_session_limit")
                                        && !TextUtils.isEmpty(object.getString("cloud_rec_size_session_limit"))
                                        && !object.getString("cloud_rec_size_session_limit").equals("null")) {
                                    callConfSetting.setCloudRecSizeSessionLimit(object.getInt("cloud_rec_size_session_limit"));
                                }
                                if (object.has("cloud_rec_size_total_limit")
                                        && !TextUtils.isEmpty(object.getString("cloud_rec_size_total_limit"))
                                        && !object.getString("cloud_rec_size_total_limit").equals("null")) {
                                    callConfSetting.setCloudRecSizeTotalLimit(object.getInt("cloud_rec_size_total_limit"));
                                }
                                if (object.has("maximum_users")
                                        && !TextUtils.isEmpty(object.getString("maximum_users"))
                                        && !object.getString("maximum_users").equals("null")) {
                                    callConfSetting.setMaximumUsers(object.getInt("maximum_users"));
                                }

                                if (object.has("meeting_id_sourcetype")
                                        && !TextUtils.isEmpty(object.getString("meeting_id_sourcetype"))
                                        && !object.getString("meeting_id_sourcetype").equals("null")) {
                                    callConfSetting.setMeetingIdSourcetype(object.getString("meeting_id_sourcetype"));
                                }

                                if (object.has("total_time_unlimited")
                                        && !TextUtils.isEmpty(object.getString("total_time_unlimited"))
                                        && !object.getString("total_time_unlimited").equals("null")) {
                                    callConfSetting.setTotalTimeUnlimited(object.getBoolean("total_time_unlimited"));
                                }
                                if (object.has("total_time_limit")
                                        && !TextUtils.isEmpty(object.getString("total_time_limit"))
                                        && !object.getString("total_time_limit").equals("null")) {
                                    callConfSetting.setTotalTimeLimit(object.getInt("total_time_limit"));
                                }
                                if (object.has("session_time_unlimited")
                                        && !TextUtils.isEmpty(object.getString("session_time_unlimited"))
                                        && !object.getString("session_time_unlimited").equals("null")) {
                                    callConfSetting.setSessionTimeUnlimited(object.getBoolean("session_time_unlimited"));
                                }
                                if (object.has("session_time_limit")
                                        && !TextUtils.isEmpty(object.getString("session_time_limit"))
                                        && !object.getString("session_time_limit").equals("null")) {
                                    callConfSetting.setSessionTimeLimit(object.getInt("session_time_limit"));
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }catch (Exception e)
            {
                Log.d("ghghghg","====="+e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null) {
//                Resources rs = getResources();   //not used
                if (isMultiple) {
                    if (selectedUsers != null) {
                        if (i == selectedUsers.size() - 1)
                            binding.rlProgressbar.setVisibility(View.GONE);
                    } else if (selectedMangers != null) {
                        if (i == selectedMangers.size() - 1)
                            binding.rlProgressbar.setVisibility(View.GONE);
                    }
                } else {
                    binding.rlProgressbar.setVisibility(View.GONE);
                    Log.d("#DEBUG", " CallConfSetting " + new Gson().toJson(callConfSetting));
                    if (callConfSetting == null) {
                        isEdit = false;
                        callConfSetting = new CallsAVWebConfSettings();
                        if (selectedManger != null) {
                            callConfSetting.setCompanyId(selectedManger.getId());
                            callConfSetting.setCompanyTypeId(selectedManger.getManager_type_id());
                            callConfSetting.setCompanyName(selectedManger.getLot_manager());
                            callConfSetting.setCompanyCode(selectedManger.getManager_id());

                            binding.etCompanyName.setText(selectedManger.getLot_manager());
                            binding.etCompanyCode.setText(selectedManger.getManager_id());
                        }
                        if (selectedUser != null) {
                            callConfSetting.setUserId(selectedUser.getUser_id());
                            callConfSetting.setUserProfileType(selectedUser.getRole());

                            if (!TextUtils.isEmpty(selectedUser.getUser_name()))
                                binding.etCompanyName.setText(selectedUser.getUser_name());
                            else binding.etCompanyName.setText(selectedUser.getEmail());
                            binding.etCompanyCode.setText(selectedUser.getRole());
                        }
                    } else {
                        isEdit = true;
                        if (selectedManger != null) {
                            if (!TextUtils.isEmpty(callConfSetting.getCompanyCode())) {
                                binding.etCompanyName.setText(callConfSetting.getCompanyName());
                                binding.etCompanyCode.setText(callConfSetting.getCompanyCode());
                            }
                        }
                        if (selectedUser != null) {
                            if (callConfSetting.getUserId() == selectedUser.getUser_id()) {
                                if (!TextUtils.isEmpty(selectedUser.getUser_name()))
                                    binding.etCompanyName.setText(selectedUser.getUser_name());
                                else binding.etCompanyName.setText(selectedUser.getEmail());
                                binding.etCompanyCode.setText(selectedUser.getRole());
                            }
                        }
                        binding.etMaxParticipent.setText(String.valueOf(callConfSetting.getMaximumUsers()));
                        //Call chat config
                        binding.switchAttachDoc.setChecked(callConfSetting.isAttachDoc());
                        binding.switchAttachImage.setChecked(callConfSetting.isAttachImage());
                        binding.switchAttachAudio.setChecked(callConfSetting.isAttachAudio());
                        binding.switchAttachVideo.setChecked(callConfSetting.isAttachVideo());
                        binding.switchAttachLiveCamera.setChecked(callConfSetting.isAttachLiveCamera());
                        binding.switchAttachLiveMic.setChecked(callConfSetting.isAttachLiveMic());
                        binding.switchAttachZip.setChecked(callConfSetting.isAttachZip());
                   /* binding.switchDisplayAttachDoc.setChecked(callConfSetting.isRecAllowedGuest());
                    binding.switchDisplayAttachImage.setChecked(callConfSetting.isRecAllowedGuest());
                    binding.switchDisplayAttachAudio.setChecked(callConfSetting.isRecAllowedGuest());
                    binding.switchDisplayAttachVideo.setChecked(callConfSetting.isRecAllowedGuest());*/
                        //Call recording Config
                        binding.switchRecHostAllowed.setChecked(callConfSetting.isRecAllowedHost());
                        binding.switchRecGuestAllowed.setChecked(callConfSetting.isRecAllowedGuest());
                        binding.switchEventAudioRecordingCloud.setChecked(callConfSetting.isEventAudioRecordingCloud());
                        binding.switchEventAudioRecordingLocal.setChecked(callConfSetting.isEventAudioRecordingLocal());
                        binding.switchEventVideoRecordingCloud.setChecked(callConfSetting.isEventVideoRecordingCloud());
                        binding.switchEventVideoRecordingLocal.setChecked(callConfSetting.isEventVideoRecordingLocal());
                        binding.switchEventWebinarRecordingCloud.setChecked(callConfSetting.isEventWebinarRecordingCloud());
                        binding.switchEventWebinarRecordingLocal.setChecked(callConfSetting.isEventWebinarRecordingLocal());
                        binding.switchInstantAudioRecordingLocal.setChecked(callConfSetting.isInstantAudioRecordingLocal());
                        binding.switchInstantVideoRecordingCloud.setChecked(callConfSetting.isInstantVideoRecordingCloud());

                        binding.switchTotalTimeUnlimited.setChecked(callConfSetting.isTotalTimeUnlimited());
                        binding.switchSessionTimeUnlimited.setChecked(callConfSetting.isSessionTimeUnlimited());

                        binding.etCloudRecSizeSessionLimit.setText(String.valueOf(callConfSetting.getCloudRecSizeSessionLimit()));
                        binding.etCloudRecSizeTotalLimit.setText(String.valueOf(callConfSetting.getCloudRecSizeTotalLimit()));
                        binding.etSessionTimeLimit.setText(String.valueOf(callConfSetting.getSessionTimeLimit()));
                        binding.etTotalTimeLimit.setText(String.valueOf(callConfSetting.getTotalTimeLimit()));
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    private class PartnerAdapter extends RecyclerView.Adapter<PartnerAdapter.PartnerHolder> {

        @NonNull
        @Override
        public PartnerAdapter.PartnerHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new PartnerAdapter.PartnerHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_partner_new_manager,
                    viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull PartnerAdapter.PartnerHolder holder, int i) {
            Manager manager = selectedMangers.get(i);
            if (manager != null) {
                holder.ivSelectCompany.setVisibility(View.GONE);
                holder.tvName.setText(manager.getLot_manager());
                holder.tvAddress.setText(manager.getAddress());
                Glide.with(getActivity()).load(manager.getTownship_logo()).into(holder.ivPartner);
            }
        }

        @Override
        public int getItemCount() {
            return selectedMangers.size();
        }

        class PartnerHolder extends RecyclerView.ViewHolder {
            CircleImageView ivPartner;
            ImageView ivSelectCompany;
            TextView tvName, tvAddress;

            PartnerHolder(@NonNull View itemView) {
                super(itemView);
                ivSelectCompany = itemView.findViewById(R.id.ivSelectedCompany);
                ivPartner = itemView.findViewById(R.id.ivPartner);
                tvName = itemView.findViewById(R.id.tvName);
                tvAddress = itemView.findViewById(R.id.tvAddress);
            }
        }
    }

    public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyUserViewHolder> {
        @NonNull
        @Override
        public UserAdapter.MyUserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new UserAdapter.MyUserViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_users, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull UserAdapter.MyUserViewHolder holder, int position) {
            holder.tvUserId.setText("" + selectedUsers.get(position).getUser_id());

            if (!TextUtils.isEmpty(selectedUsers.get(position).getFname()) || !TextUtils.isEmpty(selectedUsers.get(position).getLname()))
                holder.tvUserName.setText(selectedUsers.get(position).getFname() + " " + selectedUsers.get(position).getLname());
            else holder.tvUserName.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(selectedUsers.get(position).getMobile()))
                holder.tvUserCode.setText(selectedUsers.get(position).getMobile());
            else holder.tvUserCode.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(selectedUsers.get(position).getEmail()))
                holder.tvUserEmail.setText(selectedUsers.get(position).getEmail());
            else holder.tvUserEmail.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(selectedUsers.get(position).getRole()))
                holder.tvUserRole.setText(selectedUsers.get(position).getRole());
            else holder.tvUserRole.setVisibility(View.GONE);
        }

        @Override
        public int getItemCount() {
            return selectedUsers.size();
        }

        public class MyUserViewHolder extends RecyclerView.ViewHolder {
            TextView tvUserId, tvUserName, tvUserCode, tvUserEmail, tvUserRole;

            public MyUserViewHolder(@NonNull View itemView) {
                super(itemView);
                tvUserId = itemView.findViewById(R.id.tvUserId);
                tvUserName = itemView.findViewById(R.id.tvUserName);
                tvUserCode = itemView.findViewById(R.id.tvUserContact);
                tvUserEmail = itemView.findViewById(R.id.tvUserEmail);
                tvUserRole = itemView.findViewById(R.id.tvUserRole);
            }
        }
    }

}