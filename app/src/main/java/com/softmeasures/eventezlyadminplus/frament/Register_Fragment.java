package com.softmeasures.eventezlyadminplus.frament;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static android.content.Context.MODE_PRIVATE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

/**
 * Created by Significant Info on 1/31/2018.
 */

public class Register_Fragment extends Fragment {

    View rootView;
    ConnectionDetector cd;
    RelativeLayout rl_progressbar;

    EditText edt_first, edt_Last, edt_email, edt_password, edt_Phone;
    Button btn_register;
    String first_Name = "", Last_Name = "", Email_id = "", Password = "", Phone;
    String Expn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
            + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
            + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

    public static boolean Doyouwatregister = false;
    private Login_listener listener;

    public interface Login_listener {
        public void onLogin_success(String role);

        public void onRegister_success();
    }

    TextView txtloginskip;

    double latitude, longitude;
    Geocoder geocoder;
    List<Address> addresses;
    String statearray[] = {"Enter State", "All states", "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"};
    String mCity, mZipcode, mAddress, mState, mCountry;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frg_register, container, false);

        cd = new ConnectionDetector(getActivity());

        rl_progressbar = (RelativeLayout) rootView.findViewById(R.id.rl_progressbar1);

        edt_first = (EditText) rootView.findViewById(R.id.edt_first);
        edt_Last = (EditText) rootView.findViewById(R.id.edt_Last);
        edt_email = (EditText) rootView.findViewById(R.id.edt_email);
        edt_password = (EditText) rootView.findViewById(R.id.edt_password);
        edt_Phone = (EditText) rootView.findViewById(R.id.edt_Phone);
        btn_register = (Button) rootView.findViewById(R.id.btn_register);
        txtloginskip = (TextView) rootView.findViewById(R.id.txtloginskip);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cd.isConnectingToInternet()) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Internet Connection Required");
                    alertDialog.setMessage("Please connect to working Internet connection");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    alertDialog.show();
                } else {
                    if (check_valid_data()) {
                        new register().execute();
                    }
                }
            }
        });

        txtloginskip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        getAddress();

        return rootView;
    }

    public void registerForListener(Login_listener listener) {
        this.listener = listener;
    }

    public boolean check_valid_data() {
        boolean isresult = false;
        first_Name = edt_first.getText().toString().trim();
        Last_Name = edt_Last.getText().toString().trim();
        Email_id = edt_email.getText().toString().trim();
        Password = edt_password.getText().toString().trim();
        Phone = edt_Phone.getText().toString().trim();

        if (first_Name.equals("")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Information Missing");
            alertDialog.setMessage("Please enter first name");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
            isresult = false;
        } else if (Last_Name.equals("")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Information Missing");
            alertDialog.setMessage("Please enter last name");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
            isresult = false;
        } else if (Email_id.equals("")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Information Missing");
            alertDialog.setMessage("Please enter email id");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
            isresult = false;
        } else if (!Email_id.matches(Expn)) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Invalid Email");
            alertDialog.setMessage("Please enter a valid Email ID.");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
            isresult = false;
        } else if (Email_id.length() < 5) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Invalid");
            alertDialog.setMessage("Username must be greater than 5 characters");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
            isresult = false;
        } else if (Password.equals("")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Information Missing");
            alertDialog.setMessage("Please enter the password for login.");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
            isresult = false;
        } else if (Phone.equals("")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Information Missing");
            alertDialog.setMessage("Please enter the Phone Number");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
            isresult = false;
        } else if (Password.length() < 5) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Invalid");
            alertDialog.setMessage("Password must be greater than 4 characters");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();

            isresult = false;
        } else {
            isresult = true;
        }
        return isresult;
    }

    private class register extends AsyncTask<String, String, String> {
        String mess, res, res_id;
        String login = "user/register?login=true";
        String sss = "";

        public register() {

        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("email", Email_id);
            jsonValues.put("first_ name", first_Name);
            jsonValues.put("last_name", Last_Name);
            jsonValues.put("phone", Phone);
            jsonValues.put("new_password", Password);

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + login;
            Log.e("regi_url", url);
            Log.e("regi_param", String.valueOf(json));
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            //setting json object to post request.
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            //this is your response:
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {

                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    res = String.valueOf(response.getStatusLine());

                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject jso = new JSONObject(responseStr);
                    Log.e("jso ", " : " + jso.toString());
                    sss = jso.getString("error");
                    if (!sss.equals("")) {
                        JSONObject hh = new JSONObject(sss);
                        JSONObject error = hh.getJSONObject("context");
                        JSONArray jj = error.getJSONArray("email");
                        mess = jj.get(0).toString();
                    } else {
                        if (res.equals("HTTP/1.1 500 Internal Server Error") || res.equals("HTTP/1.1 400 Bad Request")) {
                            JSONObject towers = jso.getJSONObject("error");
                            mess = towers.getString("message");
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null)
                if (res.equals("HTTP/1.1 500 Internal Server Error") || res.equals("HTTP/1.1 400 Bad Request")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage(mess);
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                } else if (res.equals("HTTP/1.1 200 OK")) {
                    new login(Email_id, Password).execute();
                }
        }
    }

    private class login extends AsyncTask<String, String, String> {
        String id;
        String login = "user/session";
        String email, passwrod, name;

        public login(String email, String pass) {
            this.email = email;
            this.passwrod = pass;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("email", email);
            jsonValues.put("remember_me", "true");
            jsonValues.put("password", passwrod);

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + login;
            Log.e("login_url", url);
            Log.e("login_param", String.valueOf(json));

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject jso = new JSONObject(responseStr);
                    String email = jso.getString("email");
                    id = jso.getString("id");
                    String session_id = jso.getString("session_token");
                    name = jso.getString("name");
                    String is_sys_admin = jso.getString("is_sys_admin");
                    String user_role = jso.getString("role");
                    SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = logindeatl.edit();
                    edit.putString("name", name);
                    edit.putString("email", email);
                    edit.putString("is_sys_admin", is_sys_admin);
                    edit.putString("session_id", session_id);
                    edit.putString("id", id);
                    edit.putString("user_role", user_role);
                    edit.commit();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (getActivity() != null && id != null) {
                new getuserprofile().execute();
            } else {
                rl_progressbar.setVisibility(View.GONE);
                TextView tvTitle = new TextView(getActivity());
                tvTitle.setText("Login Failed!");
                tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                tvTitle.setPadding(0, 10, 0, 0);
                tvTitle.setTextColor(Color.parseColor("#000000"));
                tvTitle.setTextSize(20);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setCustomTitle(tvTitle);
                alertDialog.setMessage("Invalid credentials / Invalid credentials supplied. \n Please try again.");
                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                final AlertDialog alertd = alertDialog.create();
                alertd.show();
                TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                messageText.setGravity(Gravity.CENTER_HORIZONTAL);
            }

        }

    }

    private class getuserprofile extends AsyncTask<String, String, String> {
        String mess, res, res_id;
        String login = "_table/user_profile";
        String email;

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String ins_id = logindeatl.getString("id", "null");
        String session_id = logindeatl.getString("session_id", "");
        String emial_ID = logindeatl.getString("email", "");

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            String currentdate = sdf.format(new Date());
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", ins_id);
            jsonValues.put("date_time", currentdate);
            jsonValues.put("user_id", ins_id);
            jsonValues.put("email", emial_ID);
            jsonValues.put("user_name", emial_ID);
            jsonValues.put("fname", first_Name);
            jsonValues.put("lname", Last_Name);
            jsonValues.put("full_name", first_Name + " " + Last_Name);
            jsonValues.put("phone", Phone);
            jsonValues.put("mobile", Phone);
            jsonValues.put("lphone", "");
            jsonValues.put("address", mAddress);
            jsonValues.put("city", mCity);
            jsonValues.put("state", mState);
            jsonValues.put("zip", mZipcode);
            jsonValues.put("country", mCountry);
            jsonValues.put("full_address", mAddress + ", " + mCity + ", " + mState
                    + ", " + mZipcode + ", " + mCountry);
            jsonValues.put("ip", "100.24.234.123");


            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + login;
            Log.e("get_user_url", url);
            Log.e("get_user", String.valueOf(json));
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {

                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    res = String.valueOf(response.getStatusLine());
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject jso = new JSONObject(responseStr);
                    Log.e("res ", " : " + res.toString());
                    if (res.equals("HTTP/1.1 500 Internal Server Error") || res.equals("HTTP/1.1 400 Bad Request")) {
                        JSONObject towers = jso.getJSONObject("error");
                        mess = towers.getString("message");
                    } else if (res.equals("HTTP/1.1 200 OK")) {
                        JSONArray array = jso.getJSONArray("resource");
                        Log.e("array ", " : " + array);
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject c = array.getJSONObject(i);
                            res_id = c.getString("id");
                        }
                        /*SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                        SharedPreferences.Editor edit = logindeatl.edit();
                        edit.putString("id", res_id);
                        edit.commit();*/
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null) {
                if (res.equals("HTTP/1.1 500 Internal Server Error") || res.equals("HTTP/1.1 400 Bad Request")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage(mess);
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                } else if (res.equals("HTTP/1.1 200 OK")) {
                    if (Doyouwatregister != true) {
                        //listener.onRegister_success();
                        Bundle bundle = new Bundle();
                        bundle.putString("Email", Email_id);

                        frg_contact pay = new frg_contact();
                        ((vchome) getActivity()).show_back_button();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.add(R.id.My_Container_1_ID, pay);
                        pay.setArguments(bundle);
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();

                    } else {
                        Doyouwatregister = false;
                        //listener.onRegister_success();
                        getActivity().onBackPressed();
                        ((vchome) getActivity()).show_back_button();
//                        ((vchome) getActivity()).Show_vehicle_status(getActivity());
                    }
                }
            }
        }
    }


    public void getAddress() {

        if (cd.isConnectingToInternet()) {
            GPSTracker tracker = new GPSTracker(getActivity().getApplicationContext());
            if (tracker.canGetLocation()) {
                try {
                    latitude = tracker.getLatitude();
                    longitude = tracker.getLongitude();

                    geocoder = new Geocoder(getContext(), Locale.getDefault());
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);

                    mCountry = addresses.get(0).getCountryName();
                    mCity = (addresses.get(0).getLocality());
                    mZipcode = (addresses.get(0).getPostalCode());
                    mAddress = (addresses.get(0).getFeatureName());
                    String country_Short = mCountry.substring(0, 2);
                    for (int i = 0; i < statearray.length; i++) {
                        if (country_Short.equalsIgnoreCase(statearray[i])) {
                            mState = statearray[i];
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Failed to fetch user location!");
                alertDialog.setMessage("Please enable user location for app, location is required for app to work properly");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });
                alertDialog.show();
            }

        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();

        }
    }

    private class getuserprofile1 extends AsyncTask<String, String, String> {
        String mess, res, res_id;
        String login = "_table/user_profile";
        String email;

        public getuserprofile1(String emai) {
            this.email = emai;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            String currentdate = sdf.format(new Date());
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("user_id", email);
            jsonValues.put("email", Email_id);
            jsonValues.put("first_name", first_Name);
            jsonValues.put("last_name", Last_Name);
            jsonValues.put("new_password", Password);
            jsonValues.put("phone", "1234567890");

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + login;
            Log.e("get_user_url", url);
            Log.e("get_user", String.valueOf(json));
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {

                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    res = String.valueOf(response.getStatusLine());
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject jso = new JSONObject(responseStr);
                    Log.e("res ", " : " + res.toString());
                    if (res.equals("HTTP/1.1 500 Internal Server Error") || res.equals("HTTP/1.1 400 Bad Request")) {
                        JSONObject towers = jso.getJSONObject("error");
                        mess = towers.getString("message");
                    } else if (res.equals("HTTP/1.1 200 OK")) {
                        JSONArray array = jso.getJSONArray("resource");
                        Log.e("array ", " : " + array);
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject c = array.getJSONObject(i);
                            res_id = c.getString("id");
                        }
                        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                        SharedPreferences.Editor edit = logindeatl.edit();
                        edit.putString("id", res_id);
                        edit.commit();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null)
                if (res.equals("HTTP/1.1 500 Internal Server Error") || res.equals("HTTP/1.1 400 Bad Request")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage(mess);
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                } else if (res.equals("HTTP/1.1 200 OK")) {

                    if (Doyouwatregister != true) {
                        //listener.onRegister_success();
                        Bundle bundle = new Bundle();
                        bundle.putString("Email", email);

                        frg_contact pay = new frg_contact();
                        ((vchome) getActivity()).show_back_button();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.add(R.id.My_Container_1_ID, pay);
                        pay.setArguments(bundle);
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();

                    } else {
                        Doyouwatregister = false;
                        //listener.onRegister_success();
                        getActivity().onBackPressed();
                        ((vchome) getActivity()).show_back_button();
//                        ((vchome) getActivity()).Show_vehicle_status(getActivity());
                    }
                }
        }
    }
}
