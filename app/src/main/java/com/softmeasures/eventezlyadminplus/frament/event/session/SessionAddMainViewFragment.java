package com.softmeasures.eventezlyadminplus.frament.event.session;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragSessionAddMainViewBinding;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.SessionInfoAddEditFragment;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.SessionLocationAddEditFragment;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.SessionMediaAddEditFragment;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.SessionParkingLocationAddEditFragment;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.SessionPricingLimitFragment;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.SessionSettingsAddEditFragment;
import com.softmeasures.eventezlyadminplus.frament.event.CoveredLocationsFragment;
import com.softmeasures.eventezlyadminplus.frament.event.EventAddMainViewFragment;
import com.softmeasures.eventezlyadminplus.frament.locations.SelectAddressFragment;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.EventSession;
import com.softmeasures.eventezlyadminplus.models.Manager;

import java.util.ArrayList;
import java.util.List;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_LOCATION;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_VIRTUALLY;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_BOTH;

public class SessionAddMainViewFragment extends BaseFragment {

    public static FragSessionAddMainViewBinding binding;
    private TabAdapter adapter;

    public static SessionInfoAddEditFragment infoAddEditFragment;
    public static SessionLocationAddEditFragment locationAddEditFragment;
    public static SessionParkingLocationAddEditFragment parkingLocationAddEditFragment;
    public static SessionSettingsAddEditFragment settingsAddEditFragment;
    public static SessionMediaAddEditFragment mediaAddEditFragment;
    public static SessionPricingLimitFragment pricingLimitFragment;

    public static boolean isEdit = false;
    public static EventDefinition eventDefinition;
    public static EventSession eventSession;
    public static Manager selectedManager;
    public static int eventSessionLogiType = EVENT_TYPE_AT_LOCATION;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            eventSessionLogiType = getArguments().getInt("eventSessionLogiType", EVENT_TYPE_AT_LOCATION);
            isEdit = getArguments().getBoolean("isEdit", false);
            selectedManager = new Gson().fromJson(getArguments().getString("selectedManager"), Manager.class);
            eventDefinition = new Gson().fromJson(getArguments().getString("eventDetails"), EventDefinition.class);
            eventSession = new Gson().fromJson(getArguments().getString("eventSessionDetails"), EventSession.class);
            if (eventDefinition != null) {
                Log.e("#DEBUG", "   EventSessionAddEditFragment:  eventDetails:  " + new Gson().toJson(eventDefinition));
            }
            if (eventSession != null) {
                Log.e("#DEBUG", "   EventSessionAddEditFragment:  eventSessionDetails:  " + new Gson().toJson(eventSession));
                eventSessionLogiType = eventSession.getEvent_session_logi_type();
            }
            if (selectedManager != null) {
                Log.e("#DEBUG", "   EventSessionAddEditFragment:  selectedManager:  " + new Gson().toJson(selectedManager));
            }
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_session_add_main_view, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    private void openCoveredLocation() {
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        CoveredLocationsFragment frag = new CoveredLocationsFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isEdit", false);
        bundle.putString("selectedManager", new Gson().toJson(EventAddMainViewFragment.selectedManager));
        frag.setArguments(bundle);
        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.add(R.id.My_Container_1_ID, frag);
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(frag);
        ft.commitAllowingStateLoss();
    }

    private void openSelectAddress() {
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        SelectAddressFragment pay = new SelectAddressFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isEvent", true);
        bundle.putBoolean("isSession", true);
        pay.setArguments(bundle);
        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.add(R.id.My_Container_1_ID, pay);
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(pay);
        ft.commitAllowingStateLoss();
    }

    @Override
    protected void updateViews() {
        if (isEdit) {
            binding.tvTitle.setText("Edit Session Details");
        } else {
            if (eventDefinition != null && eventDefinition.getEvent_name() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_name())) {
                binding.tvTitle.setText("Add Session for " + eventDefinition.getEvent_name());
            } else {
                binding.tvTitle.setText("Add Session Details");
            }
            eventSession = new EventSession();
        }
    }

    @Override
    protected void setListeners() {
        binding.btnSelectAddress.setOnClickListener(v -> {
            openSelectAddress();
        });

        binding.btnCoveredAddress.setOnClickListener(v -> {
            openCoveredLocation();
        });
    }

    @Override
    protected void initViews(View v) {

        infoAddEditFragment = new SessionInfoAddEditFragment();
        infoAddEditFragment.setArguments(getArguments());
        locationAddEditFragment = new SessionLocationAddEditFragment();
        locationAddEditFragment.setArguments(getArguments());
        parkingLocationAddEditFragment = new SessionParkingLocationAddEditFragment();
        parkingLocationAddEditFragment.setArguments(getArguments());
        settingsAddEditFragment = new SessionSettingsAddEditFragment();
        settingsAddEditFragment.setArguments(getArguments());

        mediaAddEditFragment = new SessionMediaAddEditFragment();
        mediaAddEditFragment.setArguments(getArguments());

        pricingLimitFragment = new SessionPricingLimitFragment();
        pricingLimitFragment.setArguments(getArguments());

        adapter = new TabAdapter(getChildFragmentManager());
        adapter.addFragment(settingsAddEditFragment, "SETTINGS");
        adapter.addFragment(infoAddEditFragment, "SESSION INFO");
        adapter.addFragment(pricingLimitFragment, "PRICING & LIMIT");
        if (eventSessionLogiType == EVENT_TYPE_AT_VIRTUALLY) {
            adapter.addFragment(mediaAddEditFragment, "SESSION MEDIA LINKS");
            adapter.addFragment(locationAddEditFragment, "SESSION TIMES");
            binding.viewPager.setAdapter(adapter);
            binding.viewPager.setOffscreenPageLimit(4);
        } else if (eventSessionLogiType == EVENT_TYPE_AT_LOCATION) {
            adapter.addFragment(locationAddEditFragment, "SESSION LOCATION & TIMES");
            adapter.addFragment(parkingLocationAddEditFragment, "PARKING LOCATION & TIMES");
            binding.viewPager.setAdapter(adapter);
            binding.viewPager.setOffscreenPageLimit(5);
        } else if (eventSessionLogiType == EVENT_TYPE_BOTH) {
            adapter.addFragment(mediaAddEditFragment, "SESSION MEDIA LINKS");
            adapter.addFragment(locationAddEditFragment, "SESSION LOCATION & TIMES");
            adapter.addFragment(parkingLocationAddEditFragment, "PARKING LOCATION & TIMES");
            binding.viewPager.setAdapter(adapter);
            binding.viewPager.setOffscreenPageLimit(6);
        }

        binding.tabLayout.setupWithViewPager(binding.viewPager);

    }

    private class TabAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        TabAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
