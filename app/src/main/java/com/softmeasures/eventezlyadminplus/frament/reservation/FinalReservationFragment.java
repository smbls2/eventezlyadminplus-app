package com.softmeasures.eventezlyadminplus.frament.reservation;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.Splashscreen;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.dialogs.PaymentOptionDialog;
import com.softmeasures.eventezlyadminplus.dialogs.PaymentOptionListener;
import com.softmeasures.eventezlyadminplus.frament.vehicle_add;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.LocationLot;
import com.softmeasures.eventezlyadminplus.models.R_Parking;
import com.softmeasures.eventezlyadminplus.models.ReserveParking;
import com.softmeasures.eventezlyadminplus.services.Constants;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;
import com.softmeasures.eventezlyadminplus.services.GeocodeAddressIntentService;
import com.softmeasures.eventezlyadminplus.services.NotificationPublisher;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class FinalReservationFragment extends Fragment implements vehicle_add.vehivcle_add_Click_listener,
        PaymentOptionListener {

    private static String TAG = FinalReservationFragment.class.getSimpleName();
    private static final int REQUEST_CODE_PAYMENT = 2;

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    private static final String CONFIG_CLIENT_ID = "AQXvOmlRwvO2AI6H3XzqTe14pp4x6VtP2skKfUkhgjRAoNj0NoT7l0ZGeaRAXkAtYqyPCmxKEUXkrT89";
    private static PayPalConfiguration config = new PayPalConfiguration().environment(CONFIG_ENVIRONMENT).clientId(CONFIG_CLIENT_ID);

    private TextView tvParkingAvailability, tvParkingMaxHour, tvParkingRate, tvEntryTime,
            tvExitTime, tvParkingDuration, tvParkingLot, tvPlatNo, tvBtnReserveNow;
    private RelativeLayout rlBtnEntryTime, rlBtnExitTime, rlBtnParkingDuration, rlBtnParkingLot,
            rlBtnPlatNo, rlProgressbar, rlTitle, rlPaymentSuccess;
    private LinearLayout llParkOptions, llMain;

    private R_Parking rParking;
    private ArrayList<item> other_parking_rule = new ArrayList<item>();

    private double mTrFee = 0.0, mTrPercent = 0.0, mParkingSubTotal = 0.0, mParkingTotal = 0.0,
            mParkNowPostPayFees = 0.0, mReserveNowPostPayFees = 0.0, mCancelationCharges = 0.0,
            mParkNowPostPayLateFees = 0.0, mReserveNowPostPayLateFees = 0.0;

    private boolean mIsCanCancelReservation = false;
    private boolean isPrePaymentRequiredForReservation = true;
    private String reservationPostPayTerms = "";
    public static boolean isParkNowPostPayAllowed = false;
    private String parkNowPostPayTerms = "";

    private String companyId = "", twpId = "";

    //For new changes
    public static boolean mIsParking = false; // parking = true, reserve = false
    private boolean mIsPayNow = false; //payNow = true, payLater = false
    private boolean mIsWalletPayment = true; //pay with wallet = true, make payment = false

    private int DURATION_UNIT = 0;

    private ArrayList<String> values = new ArrayList<>();
    private String shoues = "";
    final ArrayList<item> durations = new ArrayList<item>();

    private ArrayList<LocationLot> locationLots = new ArrayList<>();
    private LocationLot selectedLocationLot;
    private AlertDialog dialogLocationLot;

    private String reserveEntryTime = "";
    private String reserveExpiryTime = "";
    private String reserveCurrentTime = "";
    private int intHour = 0;

    public PopupWindow popup_state;
    private ArrayList<item> vehicles = new ArrayList<>();

    private String mSelectedPlatNo = "";
    private String mSelectedPlatState = "";

    private String min, finallab, nbal, cbal = "0", ip, currentdate, lat, lang;
    private double finalpayamount, newbal, subTotal;
    Double tr_percentage1 = 0.0, tr_percentage_for_parked_cars = 0.0, tr_fee1 = 0.0, latitude = 0.0, longitude = 0.0;
    private int hours;
    private int parkingQty;
    private double parkingUnits;
    private double parkingRate;
    private ArrayList<item> wallbalarray = new ArrayList<>();
    private SimpleDateFormat sdf;
    private String paymentmethod = "", transection_id, wallet_id = "";
    private boolean isPrePaid = false;

    private String mAddress1 = "", mAddress2 = "", mDistrict = "", mCity = "", mStateCode = "", mCountryCode = "", mZipCode = "";
    private String mMarkerAddress2 = "", mMarkerDistrict = "", mMarkerCity = "",
            mMarkerStateCode = "", mMarkerCountryCode = "", mMarkerZipCode = "";
    private AddressResultReceiver mResultReceiver;
    private int token = 0;

    private long selectedEntryTime, selectedExitTime;
    private ArrayList<ReserveParking> reserveParkings = new ArrayList<>();
    private String parkingType = "";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            parkingType = getArguments().getString("parkingType", "Township");
            Log.e("#DEBUG", "   FinalReservationFragment:  parkingType:  " + parkingType);
            rParking = new Gson().fromJson(getArguments().getString("rParking"), R_Parking.class);
            Log.e(TAG, "  rParking:  " + new Gson().toJson(rParking));
        }

        return inflater.inflate(R.layout.frag_final_reservation, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
        updateViews();
        setListeners();

        mResultReceiver = new AddressResultReceiver(null);
        currentlocation();

        new getParkingRules().execute();
        new fetchLatLongFromaddress(String.valueOf(latitude), String.valueOf(longitude)).execute();

        new fetchServiceCharges().execute();
    }

    private class fetchReservedParking extends AsyncTask<String, String, String> {
        JSONObject json;
        JSONArray json1;
        String reservedParkingUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rlProgressbar.setVisibility(View.VISIBLE);
            reservedParkingUrl = "_table/reserved_parkings?filter=(location_code%3D" + rParking.getLocationCode()
                    + ")%20AND%20(reservation_status%3DACTIVE)";
            reserveParkings.clear();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + reservedParkingUrl;
            Log.e("#DEBUG", "      fetchReserveParking:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");

                    for (int j = 0; j < jsonArray.length(); j++) {
                        ReserveParking parkingUpdate = new ReserveParking();
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            parkingUpdate.setId(object.getString("id"));
                        }

                        if (object.has("lot_row")
                                && !TextUtils.isEmpty(object.getString("lot_row"))
                                && !object.getString("lot_row").equals("null")) {
                            parkingUpdate.setLot_row(object.getString("lot_row"));
                        }
                        if (object.has("lot_number")
                                && !TextUtils.isEmpty(object.getString("lot_number"))
                                && !object.getString("lot_number").equals("null")) {
                            parkingUpdate.setLot_number(object.getString("lot_number"));
                        }
                        if (object.has("lot_id")
                                && !TextUtils.isEmpty(object.getString("lot_id"))
                                && !object.getString("lot_id").equals("null")) {
                            parkingUpdate.setLot_id(object.getString("lot_id"));
                        }
                        if (object.has("reservation_date_time")
                                && !TextUtils.isEmpty(object.getString("reservation_date_time"))
                                && !object.getString("reservation_date_time").equals("null")) {
                            parkingUpdate.setReservation_date_time(object.getString("reservation_date_time"));
                        }
                        if (object.has("reserve_entry_time")
                                && !TextUtils.isEmpty(object.getString("reserve_entry_time"))
                                && !object.getString("reserve_entry_time").equals("null")) {
                            parkingUpdate.setReserve_entry_time(object.getString("reserve_entry_time"));
                        }
                        if (object.has("reserve_expiry_time")
                                && !TextUtils.isEmpty(object.getString("reserve_expiry_time"))
                                && !object.getString("reserve_expiry_time").equals("null")) {
                            parkingUpdate.setReserve_expiry_time(object.getString("reserve_expiry_time"));
                        }
                        if (object.has("reserve_exit_date_time")
                                && !TextUtils.isEmpty(object.getString("reserve_exit_date_time"))
                                && !object.getString("reserve_exit_date_time").equals("null")) {
                            parkingUpdate.setReserve_exit_date_time(object.getString("reserve_exit_date_time"));
                        }

                        if (object.has("reservation_status")
                                && !TextUtils.isEmpty(object.getString("reservation_status"))
                                && !object.getString("reservation_status").equals("null")) {
                            parkingUpdate.setReservation_status(object.getString("reservation_status"));
                        }
                        if (object.has("user_id")
                                && !TextUtils.isEmpty(object.getString("user_id"))
                                && !object.getString("user_id").equals("null")) {
                            parkingUpdate.setUser_id(object.getString("user_id"));
                        }
                        if (object.has("plate_no")
                                && !TextUtils.isEmpty(object.getString("plate_no"))
                                && !object.getString("plate_no").equals("null")) {
                            parkingUpdate.setPlate_no(object.getString("plate_no"));
                        }
                        if (object.has("pl_state")
                                && !TextUtils.isEmpty(object.getString("pl_state"))
                                && !object.getString("pl_state").equals("null")) {
                            parkingUpdate.setPl_state(object.getString("pl_state"));
                        }
                        if (object.has("pl_country")
                                && !TextUtils.isEmpty(object.getString("pl_country"))
                                && !object.getString("pl_country").equals("null")) {
                            parkingUpdate.setPl_country(object.getString("pl_country"));
                        }


                        if (object.has("parking_status")
                                && !TextUtils.isEmpty(object.getString("parking_status"))
                                && !object.getString("parking_status").equals("null")) {
                            parkingUpdate.setParking_status(object.getString("parking_status"));
                        }
                        if (object.has("location_lot_id")
                                && !TextUtils.isEmpty(object.getString("location_lot_id"))
                                && !object.getString("location_lot_id").equals("null")) {
                            parkingUpdate.setLocation_lot_id(object.getString("location_lot_id"));
                        }

                        reserveParkings.add(parkingUpdate);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            rlProgressbar.setVisibility(View.GONE);
            Log.e("#DEBUG", "   fetchReservedParking:  onPost:  Size: " + reserveParkings.size());
            if (reserveParkings.size() != 0) {

            } else {

            }
            new fetchVehicles().execute();
        }
    }

    private void updateManagedLocations(LocationLotAdapter locationLotAdapter) {

        for (int i = 0; i < locationLots.size(); i++) {
            locationLots.get(i).setIs_reserved(false);
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        for (int i = 0; i < reserveParkings.size(); i++) {
            try {
                long entryTime = simpleDateFormat.parse(reserveParkings.get(i).getReserve_entry_time()).getTime();
                long exitTime = simpleDateFormat.parse(reserveParkings.get(i).getReserve_expiry_time()).getTime();
                for (int j = 0; j < locationLots.size(); j++) {
                    if (reserveParkings.get(i).getLocation_lot_id() != null
                            && !reserveParkings.get(i).getLocation_lot_id().equals("null")
                            && Integer.valueOf(reserveParkings.get(i).getLocation_lot_id()) == locationLots.get(j).getId()) {
                        if (selectedEntryTime > entryTime && selectedExitTime < exitTime) {
                            // 2PM               1PM           3PM               4PM
                            locationLots.get(j).setIs_reserved(true);
                        } else if (selectedEntryTime >= entryTime && selectedEntryTime <= exitTime) {
                            // 2PM                      1PM         2PM                 4PM
                            locationLots.get(j).setIs_reserved(true);
                        } else if (selectedExitTime >= entryTime && selectedExitTime <= exitTime) {
                            // 2PM                      1PM         2PM                 4PM
                            locationLots.get(j).setIs_reserved(true);
                        }
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e("#DEBUG", "    updateManagedLocations:  Error:  " + e.getMessage());
            }

        }

        locationLotAdapter.notifyDataSetChanged();

    }

    public void currentlocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {

            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
            Intent intent = new Intent(getActivity(), GeocodeAddressIntentService.class);
            intent.putExtra(Constants.RECEIVER, mResultReceiver);
            intent.putExtra(Constants.FETCH_TYPE_EXTRA, Constants.USE_ADDRESS_LOCATION);
            intent.putExtra(Constants.LOCATION_LATITUDE_DATA_EXTRA, latitude);
            intent.putExtra(Constants.LOCATION_LONGITUDE_DATA_EXTRA, longitude);
            getActivity().startService(intent);
        } else {

            android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Failed to fetch user location!");
            alertDialog.setMessage("Please enable user location for app, location is required for app to work properly");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            alertDialog.show();
        }
    }

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, final Bundle resultData) {
            if (resultCode == Constants.SUCCESS_RESULT) {
                final Address address = resultData.getParcelable(Constants.RESULT_ADDRESS);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

//                        lat = String.valueOf(address.getLatitude());
//                        lang = String.valueOf(address.getLongitude());

                    }
                });
            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rlProgressbar.setVisibility(View.GONE);
                       /* infoText.setVisibility(View.VISIBLE);
                        infoText.setText(resultData.getString(Constants.RESULT_DATA_KEY));*/
                    }
                });
            }
        }
    }

    public class fetchLatLongFromaddress extends AsyncTask<Void, Void, StringBuilder> {

        String lat1, lang1;

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        public fetchLatLongFromaddress(String lat, String lang) {
            super();
            this.lat1 = lat;
            this.lang1 = lang;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {

                //  new getsearchotherlocation().execute();
                //
                // ();

                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat1 + "," + lang1 + "&key=AIzaSyBRdCWdlUMy3X5dc-9jMe0jRXJENibjdN8";
                Log.e("#DEBUG", "    currentAddress:  URL:  " + googleMapUrl);

                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
//                rlProgressbar.setVisibility(View.GONE);

                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");

                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray
                            .getJSONObject(0);
                    mAddress1 = before_geometry_jsonObj.getString("formatted_address");
                    JSONObject geometry_jsonObj = before_geometry_jsonObj
                            .getJSONObject("geometry");

                    JSONObject location_jsonObj = geometry_jsonObj
                            .getJSONObject("location");

                    lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);

                    lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);

                    if (before_geometry_jsonObj.has("address_components")) {
                        JSONArray jsonArrayComponents = before_geometry_jsonObj.getJSONArray("address_components");
                        Log.e("#DEBUG", "   address_components:  Size: " + jsonArrayComponents.length());
                        try {
                            JSONObject objectAddress2 = jsonArrayComponents.getJSONObject(0);
                            mAddress2 = objectAddress2.getString("long_name");

                            JSONObject objectCity = jsonArrayComponents.getJSONObject(2);
                            mCity = objectCity.getString("long_name");

                            JSONObject objectDistrict = jsonArrayComponents.getJSONObject(4);
                            mDistrict = objectDistrict.getString("long_name");
                            mCity = mDistrict;

                            JSONObject objectState = jsonArrayComponents.getJSONObject(5);
                            mStateCode = objectState.getString("short_name");

                            JSONObject objectCountry = jsonArrayComponents.getJSONObject(6);
                            mCountryCode = objectCountry.getString("short_name");

                            JSONObject objectZip = jsonArrayComponents.getJSONObject(7);
                            mZipCode = objectZip.getString("long_name");

                            Log.e("#DEBUG", "\nmAddress2:  " + mAddress2
                                    + "\nmCity:  " + mCity
                                    + "\nmDistrict" + mDistrict
                                    + "\nmStateCode: " + mStateCode
                                    + "\nmCountryCode:  " + mCountryCode
                                    + "\nmZipCode: " + mZipCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("#DEBUG", "  fetchLatLng  Error:  " + e.getMessage());
                        }
                    }


                } else {
                    android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public class fetchAddressFromLatLng extends AsyncTask<Void, Void, StringBuilder> {

        String lat1, lang1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        public fetchAddressFromLatLng(String lat, String lang) {
            super();
            this.lat1 = lat;
            this.lang1 = lang;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {

                //  new getsearchotherlocation().execute();
                //
                // ();

                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat1 + "," + lang1 + "&key=AIzaSyBRdCWdlUMy3X5dc-9jMe0jRXJENibjdN8";
//                Log.e("#DEBUG", "    markerAddress:  URL:  " + googleMapUrl);

                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
//                rlProgressbar.setVisibility(View.GONE);

                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");

                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray
                            .getJSONObject(0);
//                    mAddress1 = before_geometry_jsonObj.getString("formatted_address");
                    JSONObject geometry_jsonObj = before_geometry_jsonObj
                            .getJSONObject("geometry");

                    JSONObject location_jsonObj = geometry_jsonObj
                            .getJSONObject("location");

                    lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);

                    lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);

                    if (before_geometry_jsonObj.has("address_components")) {
                        JSONArray jsonArrayComponents = before_geometry_jsonObj.getJSONArray("address_components");
                        Log.e("#DEBUG", "   address_components:  Size: " + jsonArrayComponents.length());
                        try {
                            JSONObject objectAddress2 = jsonArrayComponents.getJSONObject(0);
                            mMarkerAddress2 = objectAddress2.getString("long_name");

                            JSONObject objectCity = jsonArrayComponents.getJSONObject(3);
                            mMarkerCity = objectCity.getString("long_name");

                            JSONObject objectDistrict = jsonArrayComponents.getJSONObject(4);
                            mMarkerDistrict = objectDistrict.getString("long_name");
//                            mMarkerCity = mMarkerDistrict;

                            JSONObject objectState = jsonArrayComponents.getJSONObject(5);
                            mMarkerStateCode = objectState.getString("short_name");

                            JSONObject objectCountry = jsonArrayComponents.getJSONObject(6);
                            mMarkerCountryCode = objectCountry.getString("short_name");

                            JSONObject objectZip = jsonArrayComponents.getJSONObject(7);
                            mMarkerZipCode = objectZip.getString("long_name");

                            Log.e("#DEBUG", "\nmMarkerAddress2:  " + mMarkerAddress2
                                    + "\nmMarkerCity:  " + mMarkerCity
                                    + "\nmMarkerDistrict" + mMarkerDistrict
                                    + "\nmMarkerStateCode: " + mMarkerStateCode
                                    + "\nmMarkerCountryCode:  " + mMarkerCountryCode
                                    + "\nmMarkerZipCode: " + mMarkerZipCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("#DEBUG", "  fetchLatLng  Error:  " + e.getMessage());
                        }
                    }


                } else {
                    android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Override
    public void Onitem_save() {
        getActivity().onBackPressed();
        new fetchVehicles().execute();
    }

    @Override
    public void Onitem_delete() {
        getActivity().onBackPressed();
        new fetchVehicles().execute();
    }

    @Override
    public void onWalletClick() {
        String message = "Your Cancellation Fee is $" + mCancelationCharges + "\n\nWant to Reserve Now?";
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setMessage(message);
        builder.setPositiveButton("RESERVE NOW", (dialogInterface, i) -> {
            dialogInterface.dismiss();
            mIsPayNow = true;
            mIsWalletPayment = true;
            calculateFees();
        });
        builder.setNegativeButton("CANCEL", (dialogInterface, i) -> dialogInterface.dismiss());
        androidx.appcompat.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void calculateFees() {
        min = shoues.substring(shoues.indexOf(' ') + 1);
        String hr = shoues.substring(0, shoues.indexOf(' '));
        Double d = new Double(Double.parseDouble(hr));
        hours = d.intValue();
        parkingQty = hours;
        float minutes;
        String h;

        double hrr = Double.parseDouble(rParking.getParkingDuration());
        parkingUnits = hrr;
        double prices = Double.parseDouble(rParking.getParkingRate());
        parkingRate = prices;
        double perhrrate = hrr / prices;
        finalpayamount = hours / perhrrate;
        subTotal = hours / perhrrate;
        tr_percentage_for_parked_cars = finalpayamount * tr_percentage1;
        finalpayamount = finalpayamount + tr_percentage_for_parked_cars + tr_fee1;
        finallab = String.format("%.2f", finalpayamount);

        mParkingSubTotal = subTotal;
        mTrFee = tr_fee1;
        mTrPercent = tr_percentage_for_parked_cars;
        mParkingTotal = Double.parseDouble(finallab);

        Log.e("#DEBUG", "  calculateFees:  mTrFee:  " + mTrFee + "  mTrPercent: "
                + mTrPercent + "   mParkingSubTotal:  " + mParkingSubTotal + "  mParkingTotal: " + mParkingTotal);

        if (mIsPayNow) {
            //calculate fees for reservation
            if (mIsWalletPayment) {
                //pay with wallet balance
//                printTestData();
                new fetchWalletBalance().execute();
            } else {
                //pay with paypal
//                printTestData();
                PaypalPaymentIntegration(String.valueOf(mParkingTotal));
            }

        } else {
            // calculate fees for reservation with pay later charges
            mParkingSubTotal = subTotal + mReserveNowPostPayFees;
            mParkingTotal = mParkingTotal + mReserveNowPostPayFees;
            new reserveNow().execute();
//            Log.e("#DEBUG", "   payLater:  true");
//            printTestData();

        }
    }

    private void printTestData() {
        //Test data

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String dueBy = "";

        if (!isPrePaid) {
            if (!TextUtils.isEmpty(reservationPostPayTerms)) {
                switch (reservationPostPayTerms) {
                    case "ENTRY":
                        dueBy = reserveEntryTime;
                        break;
                    case "EXIT":
                        dueBy = reserveExpiryTime;
                        break;
                    case "1 Day":
                        calendar = Calendar.getInstance();
                        calendar.add(Calendar.DAY_OF_MONTH, 1);
                        dueBy = sdf.format(new Date(calendar.getTimeInMillis()));
                        break;
                    case "7 Days":
                        calendar = Calendar.getInstance();
                        calendar.add(Calendar.DAY_OF_MONTH, 7);
                        dueBy = sdf.format(new Date(calendar.getTimeInMillis()));
                        break;
                    case "15 Days":
                        calendar = Calendar.getInstance();
                        calendar.add(Calendar.DAY_OF_MONTH, 15);
                        dueBy = sdf.format(new Date(calendar.getTimeInMillis()));
                        break;
                    case "30 Days":
                        calendar = Calendar.getInstance();
                        calendar.add(Calendar.DAY_OF_MONTH, 30);
                        dueBy = sdf.format(new Date(calendar.getTimeInMillis()));
                        break;
                }
            }
        }
        Map<String, Object> jsonValues = new HashMap<String, Object>();
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        currentdate = sdf.format(new Date());
        reserveCurrentTime = sdf.format(new Date());
        final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//            TimeZone zone12 = TimeZone.getTimeZone("UTC");
//            sdf1.setTimeZone(zone12);
        final String currentDateandTime = currentdate;
        Date date11 = null;
        try {
            date11 = sdf1.parse(currentDateandTime);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        final Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date11);
        if (min.equals("Hrs") || min.equals("Hr")) {
            calendar1.add(Calendar.HOUR, hours);
        } else if (min.equals("Months") || min.equals("Month")) {
            calendar1.add(Calendar.MONTH, hours);
            calendar1.add(Calendar.DATE, -1);
        } else if (min.equals("Days") || min.equals("Day")) {
            calendar1.add(Calendar.DATE, hours);
        } else if (min.equals("Weeks") || min.equals("Week")) {
            calendar1.add(Calendar.DAY_OF_WEEK_IN_MONTH, hours);
        } else {
            calendar1.add(Calendar.MINUTE, hours);
        }

        String expridate = sdf1.format(calendar1.getTime());
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        Date date = null;
        Date Current = null;
        try {
            date = inputFormat.parse(expridate);
            Current = inputFormat.parse(currentdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int h1 = 0;
//            if (user_id.equals("null")) {
//                h = 0;
//            } else {
//                h = Integer.parseInt(user_id);
//            }

        jsonValues.put("CanCancel_Reservation", mIsCanCancelReservation);
        jsonValues.put("Cancellation_Charge", mCancelationCharges);
        jsonValues.put("Reservation_PostPayment_term", reservationPostPayTerms);
        jsonValues.put("Reservation_PostPayment_LateFee", mReserveNowPostPayLateFees);

        jsonValues.put("reserve_entry_time", reserveEntryTime);
        jsonValues.put("reserve_expiry_time", reserveExpiryTime);
        jsonValues.put("reserve_exit_date_time", reserveExpiryTime);
        jsonValues.put("reservation_date_time", reserveCurrentTime);

        Log.e("#DEBUG", "   reserveEntryTime:  " + reserveEntryTime
                + "   reserveExpiryTime:  " + reserveExpiryTime
                + "   reserveCurrentTime:  " + reserveCurrentTime
                + "    dueBy:  " + dueBy);

        Log.e("#DEBUG", "    reservationPostPayTerms:  " + reservationPostPayTerms);

        jsonValues.put("company_type_id", "");
        jsonValues.put("company_type", "");
        jsonValues.put("manager_type_id", "");
        jsonValues.put("manager_type", "");
        try {
            if (!TextUtils.isEmpty(rParking.getCompanyId())) {
                jsonValues.put("company_id", Integer.valueOf(rParking.getCompanyId()));
            } else {
                jsonValues.put("company_id", "");
            }

            if (!TextUtils.isEmpty(twpId)) {
                jsonValues.put("twp_id", Integer.valueOf(twpId));
            } else {
                jsonValues.put("twp_id", "");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("#DEBUG", "   reserveNow:  Error:  " + e.getMessage());
        }

        if (mIsPayNow) {
            jsonValues.put("reserve_payment_condition", "PRE-PYMT");
            jsonValues.put("reserve_payment_due_by", reserveCurrentTime);
            jsonValues.put("reserve_payment_paid", "PAID");
        } else {
            jsonValues.put("reserve_payment_condition", reservationPostPayTerms);
            jsonValues.put("reserve_payment_due_by", dueBy);
            jsonValues.put("reserve_payment_paid", "UNPAID");
        }

        jsonValues.put("reservation_status", "ACTIVE");

        jsonValues.put("max_duration", rParking.getMaxTime());
        jsonValues.put("payment_method", paymentmethod);
        jsonValues.put("payment_choice", paymentmethod);
        jsonValues.put("parking_rate", parkingRate);

        jsonValues.put("parking_subtotal", mParkingSubTotal);
        jsonValues.put("tr_percent", mTrPercent);
        jsonValues.put("tr_fee", mTrFee);
        jsonValues.put("parking_total", mParkingTotal);
        jsonValues.put("wallet_trx_id", wallet_id);

        if (paymentmethod.equals("Paypal")) {
            jsonValues.put("ipn_txn_id", transection_id);
            jsonValues.put("ipn_payment", "Paypal");
            jsonValues.put("ipn_status", "success");
            jsonValues.put("ipn_address", "");
        } else {
            jsonValues.put("ipn_txn_id", "");
            jsonValues.put("ipn_payment", "");
            jsonValues.put("ipn_status", "");
            jsonValues.put("ipn_address", "");
        }
        String plate = mSelectedPlatNo;
        plate = plate.replaceAll("\\s+", "");
        jsonValues.put("parking_type", rParking.getMarkerType());
        jsonValues.put("township_code", rParking.getTownshipCode());
        jsonValues.put("location_code", rParking.getLocationCode());

        jsonValues.put("user_id", h1);
        jsonValues.put("permit_id", 0);
        jsonValues.put("subscription_id", 0);
        jsonValues.put("plate_no", plate);
        jsonValues.put("pl_state", mSelectedPlatState);
        jsonValues.put("lat", latitude);
        jsonValues.put("lng", longitude);
        jsonValues.put("address1", mAddress1);
        jsonValues.put("address2", "");
        if (!TextUtils.isEmpty(mCity)) {
            jsonValues.put("city", mCity);
        } else {
            jsonValues.put("city", "");
        }
        if (!TextUtils.isEmpty(mStateCode)) {
            jsonValues.put("state", mStateCode);
        } else {
            jsonValues.put("state", "");
        }
        if (!TextUtils.isEmpty(mZipCode)) {
            jsonValues.put("zip", mZipCode);
        } else jsonValues.put("zip", "");
        if (!TextUtils.isEmpty(mCountryCode)) {
            jsonValues.put("country", mCountryCode);
        } else jsonValues.put("country", "USA");
        if (selectedLocationLot != null) {
            if (!TextUtils.isEmpty(selectedLocationLot.getLot_id())) {
                jsonValues.put("lot_id", selectedLocationLot.getLot_id());
            }
            if (!TextUtils.isEmpty(selectedLocationLot.getLot_number())) {
                jsonValues.put("lot_number", selectedLocationLot.getLot_number());
            }
            if (!TextUtils.isEmpty(selectedLocationLot.getLot_row())) {
                jsonValues.put("lot_row", selectedLocationLot.getLot_row());
            }
            jsonValues.put("location_lot_id", selectedLocationLot.getId());
        }
        jsonValues.put("ip", getLocalIpAddress());
        jsonValues.put("parking_token", token);
        jsonValues.put("parking_status", "RESERVED");
        jsonValues.put("platform", "Android");

        jsonValues.put("parking_units", String.valueOf(parkingUnits));
        jsonValues.put("parking_qty", parkingQty);

        jsonValues.put("ipn_custom", "");

        jsonValues.put("location_id", rParking.getLocationId());

        String mismatch = "0";
        float dis = getdistances(rParking.getMarkerAddress(), mAddress1);
        float ff = dis / 1609;
        if (ff > 0.5) {
            mismatch = "1";
        }
        jsonValues.put("distance_to_marker", getdistances(rParking.getMarkerAddress(), mAddress1));
        jsonValues.put("marker_address1", rParking.getMarkerAddress());
        jsonValues.put("marker_address2", "");
        jsonValues.put("marker_city", rParking.getMarkerCity());
        jsonValues.put("marker_state", rParking.getMarkerState());
        jsonValues.put("marker_zip", "");
        jsonValues.put("marker_country", "USA");
        jsonValues.put("token", 0);
        jsonValues.put("mismatch", mismatch);
        jsonValues.put("marker_lng", rParking.getMarkerLat());
        jsonValues.put("marker_lat", rParking.getMarkerLng());
        jsonValues.put("ticket_status", "");
        jsonValues.put("date_time", "");
        jsonValues.put("duration_unit", min);
        jsonValues.put("unit_pricing", rParking.getParkingRate());
        jsonValues.put("selected_duration", hours);
        jsonValues.put("pl_country", "");
        JSONObject json = new JSONObject(jsonValues);

        Log.e("#DEBUG", "   test data:  " + json.toString());

        //Test data Over
    }

    private void PaypalPaymentIntegration(String amount) {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE, amount);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent, String payamount) {
        return new PayPalPayment(new BigDecimal(payamount), "USD", "Parking Payment", paymentIntent);
    }

    public class reserveNow extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String rid = logindeatl.getString("id", "null");
        String id1 = "null";
        JSONObject json, json1;
        String pov2 = "_table/reserved_parkings";
        //        String pov2 = "_table/sdafga";
        boolean isPayLater = false;
        String paramOption = "";
        private String dueBy = "";
        SharedPreferences logindeatl1 = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl1.getString("id", "null");

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//            TimeZone zonen = TimeZone.getTimeZone("UTC");
//            sdf.setTimeZone(zonen);

            if (!isPrePaid) {
                if (!TextUtils.isEmpty(reservationPostPayTerms)) {
                    switch (reservationPostPayTerms) {
                        case "ENTRY":
                            dueBy = reserveEntryTime;
                            break;
                        case "EXIT":
                            dueBy = reserveExpiryTime;
                            break;
                        case "1 Day":
                            calendar = Calendar.getInstance();
                            calendar.add(Calendar.DAY_OF_MONTH, 1);
                            dueBy = sdf.format(new Date(calendar.getTimeInMillis()));
                            break;
                        case "7 Days":
                            calendar = Calendar.getInstance();
                            calendar.add(Calendar.DAY_OF_MONTH, 7);
                            dueBy = sdf.format(new Date(calendar.getTimeInMillis()));
                            break;
                        case "15 Days":
                            calendar = Calendar.getInstance();
                            calendar.add(Calendar.DAY_OF_MONTH, 15);
                            dueBy = sdf.format(new Date(calendar.getTimeInMillis()));
                            break;
                        case "30 Days":
                            calendar = Calendar.getInstance();
                            calendar.add(Calendar.DAY_OF_MONTH, 30);
                            dueBy = sdf.format(new Date(calendar.getTimeInMillis()));
                            break;
                    }
                }
            }

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            if (params.length != 0) {
                paramOption = params[0];
            }
            isPayLater = !TextUtils.isEmpty(paramOption) && paramOption.equals("payLater");
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            currentdate = sdf.format(new Date());
            reserveCurrentTime = sdf.format(new Date());
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//            TimeZone zone12 = TimeZone.getTimeZone("UTC");
//            sdf.setTimeZone(zone12);
            final String currentDateandTime = currentdate;
            Date date11 = null;
            try {
                date11 = sdf.parse(currentDateandTime);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date11);
            if (min.equals("Hrs")) {
                calendar.add(Calendar.HOUR, hours);
            } else if (min.equals("Months")) {
                calendar.add(Calendar.MONTH, hours);
                calendar.add(Calendar.DATE, -1);
            } else if (min.equals("Days")) {
                calendar.add(Calendar.DATE, hours);
            } else if (min.equals("Weeks")) {
                calendar.add(Calendar.DAY_OF_WEEK_IN_MONTH, hours);
            } else {
                calendar.add(Calendar.MINUTE, hours);
            }

            String expridate = sdf.format(calendar.getTime());
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
            SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

            Date date = null;
            Date Current = null;
            try {
                date = inputFormat.parse(expridate);
                Current = inputFormat.parse(currentdate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            int h = 0;
            if (user_id.equals("null")) {
                h = 0;
            } else {
                h = Integer.parseInt(user_id);
            }

            jsonValues.put("CanCancel_Reservation", mIsCanCancelReservation);
            jsonValues.put("Cancellation_Charge", mCancelationCharges);
            jsonValues.put("Reservation_PostPayment_term", reservationPostPayTerms);
            jsonValues.put("Reservation_PostPayment_LateFee", mReserveNowPostPayLateFees);

            jsonValues.put("reserve_entry_time", reserveEntryTime);
            jsonValues.put("reserve_expiry_time", reserveExpiryTime);
            jsonValues.put("reserve_exit_date_time", reserveExpiryTime);
            jsonValues.put("reservation_date_time", reserveCurrentTime);

            Log.e("#DEBUG", "   reserveEntryTime:  " + reserveEntryTime
                    + "   reserveExpiryTime:  " + reserveExpiryTime
                    + "   reserveCurrentTime:  " + reserveCurrentTime
                    + "    dueBy:  " + dueBy);

            Log.e("#DEBUG", "    reservationPostPayTerms:  " + reservationPostPayTerms);

            jsonValues.put("company_type_id", "");
            jsonValues.put("company_type", "");
            jsonValues.put("manager_type_id", rParking.getManagerTypeId());
            jsonValues.put("manager_type", rParking.getManagerType());
            try {
                if (!TextUtils.isEmpty(rParking.getCompanyId())) {
                    jsonValues.put("company_id", Integer.valueOf(rParking.getCompanyId()));
                } else {
                    jsonValues.put("company_id", "");
                }

                if (!TextUtils.isEmpty(twpId)) {
                    jsonValues.put("twp_id", Integer.valueOf(twpId));
                } else {
                    jsonValues.put("twp_id", "");
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("#DEBUG", "   reserveNow:  Error:  " + e.getMessage());
            }

            if (mIsPayNow) {
                jsonValues.put("reserve_payment_condition", "PRE-PYMT");
                jsonValues.put("reserve_payment_due_by", reserveCurrentTime);
                jsonValues.put("reserve_payment_paid", "PAID");
            } else {
                jsonValues.put("reserve_payment_condition", reservationPostPayTerms);
                jsonValues.put("reserve_payment_due_by", dueBy);
                jsonValues.put("reserve_payment_paid", "UNPAID");
            }

            jsonValues.put("reservation_status", "ACTIVE");

            jsonValues.put("max_duration", rParking.getMaxTime());
            jsonValues.put("payment_method", paymentmethod);
            jsonValues.put("payment_choice", paymentmethod);
            jsonValues.put("parking_rate", parkingRate);

            jsonValues.put("parking_subtotal", mParkingSubTotal);
            jsonValues.put("tr_percent", mTrPercent);
            jsonValues.put("tr_fee", mTrFee);
            jsonValues.put("parking_total", mParkingTotal);
            jsonValues.put("wallet_trx_id", wallet_id);

            if (paymentmethod.equals("Paypal")) {
                jsonValues.put("ipn_txn_id", transection_id);
                jsonValues.put("ipn_payment", "Paypal");
                jsonValues.put("ipn_status", "success");
                jsonValues.put("ipn_address", "");
            } else {
                jsonValues.put("ipn_txn_id", "");
                jsonValues.put("ipn_payment", "");
                jsonValues.put("ipn_status", "");
                jsonValues.put("ipn_address", "");
            }
            String plate = mSelectedPlatNo;
            plate = plate.replaceAll("\\s+", "");
            jsonValues.put("parking_type", rParking.getMarkerType());
            jsonValues.put("township_code", rParking.getTownshipCode());
            jsonValues.put("location_code", rParking.getLocationCode());
            jsonValues.put("location_name", rParking.getLocationName());

            jsonValues.put("user_id", h);
            jsonValues.put("permit_id", 0);
            jsonValues.put("subscription_id", 0);
            jsonValues.put("plate_no", plate);
            jsonValues.put("pl_state", mSelectedPlatState);
            jsonValues.put("lat", latitude);
            jsonValues.put("lng", longitude);
            jsonValues.put("address1", mAddress1);
            jsonValues.put("address2", mAddress2);
            if (!TextUtils.isEmpty(mCity)) {
                jsonValues.put("city", mCity);
            } else {
                jsonValues.put("city", "");
            }
            if (!TextUtils.isEmpty(mDistrict)) {
                jsonValues.put("district", mDistrict);
            } else {
                jsonValues.put("district", "");
            }
            if (!TextUtils.isEmpty(mStateCode)) {
                jsonValues.put("state", mStateCode);
            } else {
                jsonValues.put("state", "");
            }
            if (!TextUtils.isEmpty(mZipCode)) {
                jsonValues.put("zip", mZipCode);
            } else jsonValues.put("zip", "");
            if (!TextUtils.isEmpty(mCountryCode)) {
                jsonValues.put("country", mCountryCode);
            } else jsonValues.put("country", "USA");
            if (selectedLocationLot != null) {
                if (!TextUtils.isEmpty(selectedLocationLot.getLot_id())) {
                    jsonValues.put("lot_id", selectedLocationLot.getLot_id());
                }
                if (!TextUtils.isEmpty(selectedLocationLot.getLot_number())) {
                    jsonValues.put("lot_number", selectedLocationLot.getLot_number());
                }
                if (!TextUtils.isEmpty(selectedLocationLot.getLot_row())) {
                    jsonValues.put("lot_row", selectedLocationLot.getLot_row());
                }
                jsonValues.put("location_lot_id", selectedLocationLot.getId());
            }
            jsonValues.put("ip", getLocalIpAddress());
            jsonValues.put("parking_token", token);
            jsonValues.put("parking_status", "RESERVED");
            jsonValues.put("platform", "Android");

            jsonValues.put("parking_units", String.valueOf(parkingUnits));
            jsonValues.put("parking_qty", parkingQty);

            jsonValues.put("ipn_custom", "");

            jsonValues.put("location_id", rParking.getLocationId());

            String mismatch = "0";
            float dis = getdistances(rParking.getMarkerAddress(), mAddress1);
            float ff = dis / 1609;
            if (ff > 0.5) {
                mismatch = "1";
            }
            jsonValues.put("distance_to_marker", getdistances(rParking.getMarkerAddress(), mAddress1));
            jsonValues.put("marker_address1", rParking.getMarkerAddress());
            jsonValues.put("marker_lng", rParking.getMarkerLng());
            jsonValues.put("marker_lat", rParking.getMarkerLat());

            jsonValues.put("marker_address2", mMarkerAddress2);
            jsonValues.put("marker_city", mMarkerCity);
            jsonValues.put("marker_district", mMarkerDistrict);
            jsonValues.put("marker_state", mMarkerStateCode);
            jsonValues.put("marker_zip", mMarkerZipCode);
            jsonValues.put("marker_country", mMarkerCountryCode);
            jsonValues.put("token", 0);
            jsonValues.put("mismatch", mismatch);
            jsonValues.put("ticket_status", "");
            jsonValues.put("date_time", "");
            jsonValues.put("duration_unit", min);
            jsonValues.put("unit_pricing", rParking.getParkingRate());
            jsonValues.put("selected_duration", hours);
            jsonValues.put("pl_country", "");
            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + pov2;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "    reserveNow:  Response:  " + responseStr);
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id1 = c.getString("id");

                    }
                } catch (IOException e) {
                    Log.e("#DEBUG", "    reserveNow:  Error:  " + e.getMessage());
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.e("#DEBUG", "    reserveNow:  Error:  " + e.getMessage());
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rlProgressbar != null)
                rlProgressbar.setVisibility(View.GONE);
            //progressBar.setVisibility(View.GONE);
            if (getActivity() != null && json1 != null) {
                if (!id1.equals("null")) {
                    new updateLocationLot(id1).execute();

                    Toast.makeText(getActivity(), "Parking Reserved Successfully!", Toast.LENGTH_SHORT).show();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Bundle bundle = new Bundle();
                    bundle.putString("reservationId", id1);
                    ReservationParkingDetailsFragment pay = new ReservationParkingDetailsFragment();
                    pay.setArguments(bundle);
                    fragmentStack.lastElement().onPause();
                    ft.add(R.id.My_Container_1_ID, pay, "home");
                    fragmentStack.lastElement().onPause();
                    ft.remove(fragmentStack.pop());
                    fragmentStack.lastElement().onPause();
                    ft.remove(fragmentStack.pop());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                    sendNotificationReservationConfirm(id1);

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    final String currentDateandTime = reserveEntryTime;
                    Date date11 = null;
                    try {
                        date11 = sdf.parse(currentDateandTime);
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    final Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date11);
                    calendar.add(Calendar.MINUTE, -5);
                    scheduleNotification(getNotification("Your reservation starts in 5 Min", 0),
                            calendar.getTimeInMillis(), 0);

                    if (!mIsPayNow) {
                        String dueDate = dueBy;
                        try {
                            date11 = sdf.parse(dueDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        calendar.setTime(date11);
                        calendar.add(Calendar.MINUTE, -5);
                        scheduleNotification(getNotification("Your reservation payment due in 5 Min", 0),
                                calendar.getTimeInMillis(), 0);
                    }

                }
            }
            super.onPostExecute(s);
        }
    }

    public float getdistances(String address, String address1) {
        GPSTracker tracker = new GPSTracker(getActivity());

        try {
            Location loc1 = new Location(address1);
            loc1.setLatitude(latitude);
            loc1.setLongitude(longitude);
            Location loc2 = new Location(address);
            loc2.setLatitude(Double.parseDouble(lat));
            loc2.setLongitude(Double.parseDouble(lang));
            float distanceInMeters = loc1.distanceTo(loc2);
            return distanceInMeters;
        } catch (Exception e) {
            return 0;
        }

    }

    private void sendNotificationReservationConfirm(String id1) {
        int notificationId = (int) Calendar.getInstance().getTimeInMillis();
        if (!TextUtils.isEmpty(id1)) {
            try {
                notificationId = Integer.parseInt(id1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Intent intent = new Intent(getActivity(), Splashscreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 0, intent, 0);

        NotificationChannel channel = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            channel = new NotificationChannel("reservation", "reservation", importance);
            channel.setDescription("Notification for reservation");
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getActivity(), "reservation")
                .setSmallIcon(R.mipmap.icon)
                .setContentTitle("ParkEzly")
                .setContentText("Thank you, Your reservation is confirmed!")
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getActivity());
        notificationManager.notify(notificationId, builder.build());
        if (channel != null) {
            NotificationManager notificationManager1 = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                notificationManager1 = getActivity().getSystemService(NotificationManager.class);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationManager1.createNotificationChannel(channel);
            }
            notificationManager1.notify(notificationId, builder.build());
        }

    }

    private Notification getNotification(String content, int postion) {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        long[] vibrate = {0, 100, 200, 300};
        String strtitle = getActivity().getString(R.string.customnotificationtitle);
        // Set Notification Text
        String strtext = "Alert Parkezly!";
        Intent intent = new Intent(getActivity().getApplicationContext(), vchome.class);
        intent.putExtra("title", strtitle);
        intent.putExtra("text", strtext);
        intent.putExtra("id", id);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pIntent = PendingIntent.getActivity(getActivity().getApplicationContext(), postion, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        androidx.core.app.NotificationCompat.Builder builder = new NotificationCompat.Builder(getActivity())
                .setSmallIcon(R.mipmap.icon)
                .setTicker(getActivity().getString(R.string.customnotificationticker))
                .setVibrate(vibrate)
                .setContentIntent(pIntent)
                .setContentTitle(strtitle)
                .setContentText(content)
                .setAutoCancel(true);
        return builder.build();
    }

    private void scheduleNotification(Notification notification, long delay, int postion) {

        Intent notificationIntent = new Intent(getActivity(), NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, postion);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), postion, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        GregorianCalendar hbhbh = new GregorianCalendar();
        hbhbh.setTimeInMillis(delay);
        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, hbhbh.getTimeInMillis(), pendingIntent);
    }

    public class fetchWalletBalance extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String userid = logindeatl.getString("id", "null");
        JSONObject json, json1;
        String wallbalurl = "_table/user_wallet?order=date_time%20DESC";
        //String wallbalurl = "_table/user_wallet?filter=(user_id%3D'"+userid+"')";
        String w_id;

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + wallbalurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            HttpEntity resEntity = response.getEntity();
            String responseStr = null;
            if (response != null) {
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        String user = c.getString("user_id");

                        if (user.equals(userid)) {
                            nbal = c.getString("new_balance");
                            if (!nbal.equals("null")) {
                                cbal = c.getString("current_balance");
                                item.setAmount(nbal);
                                item.setPaidamount(cbal);
                                wallbalarray.add(item);
                                break;
                            }
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rlProgressbar != null)
                rlProgressbar.setVisibility(View.GONE);
            // progressBar.setVisibility(View.GONE);

            if (getActivity() != null && json != null) {
                if (wallbalarray.size() > 0) {
                    double bal = Double.parseDouble(cbal);
                    if (bal > 0) {
                        finalpayamount = 0.0;
                        min = shoues.substring(shoues.indexOf(' ') + 1);
                        String hr = shoues.substring(0, shoues.indexOf(' '));
                        Double d = new Double(Double.parseDouble(hr));
                        hours = d.intValue();
                        parkingQty = hours;
                        float minutes;
                        String h;

                        double hrr = Double.parseDouble(rParking.getParkingDuration());
                        parkingUnits = hrr;
                        double prices = Double.parseDouble(rParking.getParkingRate());
                        parkingRate = prices;
                        double perhrrate = hrr / prices;
                        finalpayamount = hours / perhrrate;
                        subTotal = hours / perhrrate;
                        tr_percentage_for_parked_cars = finalpayamount * tr_percentage1;
                        finalpayamount = finalpayamount + tr_percentage_for_parked_cars + tr_fee1;
                        newbal = bal - mParkingTotal;
                        String finallab2 = String.format("%.2f", bal);
                        finallab = String.format("%.2f", finalpayamount);
                        boolean is_parked_time = true;
                        int remain_time = 0;
//                        Renew_parked_time = Renew_parked_time != null ? (!Renew_parked_time.equals("") ? (!Renew_parked_time.equals("null") ? Renew_parked_time : "") : "") : "";
//                        if (!Renew_parked_time.equals("")) {
//                            int parked_hours = Integer.parseInt(Renew_parked_time);
//                            final_count = parked_hours + hours;
//                            int max = Integer.parseInt(max_time);
//                            remain_time = max - parked_hours;
//                            if (final_count <= max) {
//                                is_parked_time = true;
//                            } else {
//                                is_parked_time = false;
//                            }
//                        } else {
//                            final_count = hours;
//                        }
                        Calendar calendar = Calendar.getInstance();
                        int day = calendar.get(Calendar.DAY_OF_WEEK);

                        if (newbal < 0) {
                            android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("alert");
                            alertDialog.setMessage("You don't have enough funds in your wallet.");
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialog.show();

                        } else {
                            android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("Wallet Balance: $" + finallab2);
                            alertDialog.setMessage("Would you like to pay from your ParkEZly wallet ? With service and transaction fee your total will be " + "$" + finallab + "");
                            alertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    ip = getLocalIpAddress();
                                    new updateWalletBalance().execute();
                                }
                            });
                            alertDialog.setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialog.show();
                        }
                    } else {
                        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("alert");
                        alertDialog.setMessage("You don't have enough funds in your wallet.");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }

                } else {
                    android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("alert");
                    alertDialog.setMessage("You don't have enough funds in your wallet.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
            super.onPostExecute(s);

        }
    }

    public class updateWalletBalance extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONObject json12;
        String walleturl = "_table/user_wallet";
        String w_id = "";

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            currentdate = sdf.format(new Date());
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("paid_date", currentdate);
            jsonValues1.put("last_paid_amt", mParkingTotal);
            jsonValues1.put("ip", ip);
            jsonValues1.put("remember_me", "");
            jsonValues1.put("current_balance", newbal);
            jsonValues1.put("ipn_txn_id", "");
            jsonValues1.put("ipn_custom", "");
            jsonValues1.put("ipn_payment", "");
            jsonValues1.put("date_time", currentdate);
            jsonValues1.put("ipn_address", "");
            jsonValues1.put("user_id", id);
            jsonValues1.put("new_balance", newbal);
            jsonValues1.put("action", "deduction");

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        wallet_id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            rlProgressbar.setVisibility(View.VISIBLE);
            //progressBar.setVisibility(View.GONE);
            if (getActivity() != null && json12 != null) {
                isPrePaid = true;

                if (!wallet_id.equals("")) {
                    new reserveNow().execute();
//                    printTestData();
                }

            }
            super.onPostExecute(s);
        }
    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("ip", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("aax", ex.toString());
        }
        return null;
    }


    @Override
    public void onPaymentClick() {
        // payment option= paypal, wallet = false, paynow= true
        mIsPayNow = true;
        mIsWalletPayment = false;
        if (!mIsParking) {
            String message = "Your Cancellation Fee is $" + mCancelationCharges + "\n\nWant to Reserve Now?";
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
            builder.setMessage(message);
            builder.setPositiveButton("RESERVE NOW", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    calculateFees();
                }
            });
            builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            androidx.appcompat.app.AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            calculateFees();
        }
    }

    @Override
    public void onPayLaterClick() {
        Log.e(TAG, "    onPayLaterClick");
        String message = "";
        String txtOK = "";
        message = "Your Cancellation Fee is $" + mCancelationCharges + "\n\nWant to Reserve Now and Pay Later?";
        txtOK = "RESERVE NOW";
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setMessage(message);
        builder.setPositiveButton(txtOK, (dialogInterface, i) -> {
            dialogInterface.dismiss();
            mIsPayNow = false;
            calculateFees();
        });
        builder.setNegativeButton("CANCEL", (dialogInterface, i) -> dialogInterface.dismiss());
        androidx.appcompat.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    public class getParkingRules extends AsyncTask<String, String, String> {

        JSONObject json;
        String manageslocation = "";

        @Override
        protected void onPreExecute() {
            manageslocation = "_table/township_parking_rules?filter=location_code%3D'" + rParking.getLocationCode() + "'";
            rlProgressbar.setVisibility(View.VISIBLE);
            if (parkingType.equals("Township")) {
                manageslocation = "_table/township_parking_rules?filter=location_code%3D'" + rParking.getLocationCode() + "'";
            } else if (parkingType.equals("Commercial")) {
                manageslocation = "_table/google_parking_rules?filter=location_code%3D'" + rParking.getLocationCode() + "'";
            } else if (parkingType.equals("Other")) {
                manageslocation = "_table/other_parking_rules?filter=location_code%3D'" + rParking.getLocationCode() + "'";
            }
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            other_parking_rule.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + manageslocation;
            Log.e("#DEBUG", "   other_parking_rule:  URL:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   other_parking_rule:  response:  " + responseStr);
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        item.setPricing_duration(c.getString("pricing_duration"));
                        item.setMax_duration(c.getString("max_duration"));
                        item.setDuration_unit(c.getString("duration_unit"));
                        item.setPricing(c.getString("pricing"));
                        if (c.has("twp_id")
                                && !TextUtils.isEmpty(c.getString("twp_id"))
                                && !c.getString("twp_id").equals("null")) {
                            item.setTwp_id(c.getString("twp_id"));
                            item.setCompany_id(c.getString("twp_id"));
                        }

                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }

                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }

                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }

                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }

                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }

                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }

                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }

                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }

                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }

                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }

                        if (c.has("Reservation_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_Fee"))
                                && !c.getString("Reservation_PostPayment_Fee").equals("null")) {
                            item.setReservation_PostPayment_Fee(c.getString("Reservation_PostPayment_Fee"));
                        }

                        if (c.has("ParkNow_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_Fee"))
                                && !c.getString("ParkNow_PostPayment_Fee").equals("null")) {
                            item.setParkNow_PostPayment_Fee(c.getString("ParkNow_PostPayment_Fee"));
                        }

                        if (c.has("number_of_reservable_spots")
                                && !TextUtils.isEmpty(c.getString("number_of_reservable_spots"))
                                && !c.getString("number_of_reservable_spots").equals("null")) {
                            item.setNumber_of_reservable_spots(c.getString("number_of_reservable_spots"));
                        }
                        other_parking_rule.add(item);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   other_parking_rule:  Error:  " + e.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   other_parking_rule:  Error:  " + e.getMessage());
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.e("#DEBUG", "  other_parking_rules Size: " + String.valueOf(other_parking_rule.size()));
            if (getActivity() != null && json != null) {
//                rlProgressbar.setVisibility(View.GONE);
                for (int j = 0; j < other_parking_rule.size(); j++) {
                    isPrePaymentRequiredForReservation = other_parking_rule.get(j).isPrePymntReqd_for_Reservation();
                    reservationPostPayTerms = other_parking_rule.get(j).getReservation_PostPayment_term();
                    isParkNowPostPayAllowed = other_parking_rule.get(j).isParkNow_PostPaymentAllowed();
                    parkNowPostPayTerms = other_parking_rule.get(j).getParkNow_PostPayment_term();
                    companyId = other_parking_rule.get(j).getCompany_id();
                    twpId = other_parking_rule.get(j).getTwp_id();
                    mIsCanCancelReservation = other_parking_rule.get(j).isCanCancel_Reservation();
                    if (!TextUtils.isEmpty(other_parking_rule.get(j).getCancellation_Charge())) {
                        mCancelationCharges = Double.parseDouble(other_parking_rule.get(j).getCancellation_Charge());
                    }

                    if (!TextUtils.isEmpty(other_parking_rule.get(j).getParkNow_PostPayment_Fee()))
                        mParkNowPostPayFees = Double.parseDouble(other_parking_rule.get(j).getParkNow_PostPayment_Fee());
                    if (!TextUtils.isEmpty(other_parking_rule.get(j).getParkNow_PostPayment_LateFee()))
                        mParkNowPostPayLateFees = Double.parseDouble(other_parking_rule.get(j).getParkNow_PostPayment_LateFee());

                    if (!TextUtils.isEmpty(other_parking_rule.get(j).getReservation_PostPayment_Fee()))
                        mReserveNowPostPayFees = Double.parseDouble(other_parking_rule.get(j).getReservation_PostPayment_Fee());
                    if (!TextUtils.isEmpty(other_parking_rule.get(j).getReservation_PostPayment_LateFee()))
                        mReserveNowPostPayLateFees = Double.parseDouble(other_parking_rule.get(j).getReservation_PostPayment_LateFee());

                    Log.e("#DEBUG", "   mCancelationCharges:  " + mCancelationCharges
                            + "\nmParkNowPostPayFees:  " + mParkNowPostPayFees
                            + "\nmParkNowPostPayLateFees:  " + mParkNowPostPayLateFees
                            + "\nmReserveNowPostPayFees:  " + mReserveNowPostPayFees
                            + "\nmReserveNowPostPayLateFees:  " + mReserveNowPostPayLateFees
                            + "\nmIsCanCancelReservation:  " + mIsCanCancelReservation);
                }

                new fetchLocationLotOccupiedData().execute();
            }
            super.onPostExecute(s);
        }
    }

    public class fetchLocationLotOccupiedData extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String lat, lang, address, title, html, mark, parking_time, no_parking_time, notice;

        String managedlosturl = "_proc/managed_lot_status?&order=(lot_row%20ASC%2C%20lot_number%20ASC)";

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            if (parkingType.equals("Township")) {
                managedlosturl = "_proc/managed_lot_status?&order=(lot_row%20ASC%2C%20lot_number%20ASC)";
            } else if (parkingType.equals("Commercial")) {
                managedlosturl = "_proc/managed_lot_status?&order=(lot_row%20ASC%2C%20lot_number%20ASC)";
            } else if (parkingType.equals("Other")) {
                managedlosturl = "_proc/managed_lot_status?&order=(lot_row%20ASC%2C%20lot_number%20ASC)";
            }
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            locationLots.clear();
            JSONObject managed = new JSONObject();


            /*  if(latitude==0.0) {*/
            try {
                managed.put("name", "in_location_code");
                managed.put("value", rParking.getLocationCode());

                //  student1.put("value", latitude);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            JSONArray jsonArray = new JSONArray();
            jsonArray.put(managed);

            JSONObject manageda = new JSONObject();
            try {
                manageda.put("params", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("managed_lost_url", url);
            Log.e("managed_lodt_param", String.valueOf(manageda));

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(manageda.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);

            HttpResponse response = null;

            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    TimeZone zone = TimeZone.getTimeZone("UTC");
                    sdf.setTimeZone(zone);
                    String currentdate = sdf.format(new Date());
                    Date current = null;
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        current = format.parse(currentdate);
                        System.out.println(current);
                    } catch (android.net.ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json1 = new JSONArray(responseStr);

                    for (int i = 0; i < json1.length(); i++) {
                        LocationLot item = new LocationLot();
                        JSONObject jsonObject = json1.getJSONObject(i);
                        if (jsonObject.has("id")
                                && !TextUtils.isEmpty(jsonObject.getString("id"))
                                && !jsonObject.getString("id").equals("null")) {
                            item.setId(jsonObject.getInt("id"));
                        }
                        if (jsonObject.has("date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("date_time"))
                                && !jsonObject.getString("date_time").equals("null")) {
                            item.setDate_time(jsonObject.getString("date_time"));
                        }

                        if (jsonObject.has("township_code")
                                && !TextUtils.isEmpty(jsonObject.getString("township_code"))
                                && !jsonObject.getString("township_code").equals("null")) {
                            item.setTownship_code(jsonObject.getString("township_code"));
                        }

                        if (jsonObject.has("location_code")
                                && !TextUtils.isEmpty(jsonObject.getString("location_code"))
                                && !jsonObject.getString("location_code").equals("null")) {
                            item.setLocation_code(jsonObject.getString("location_code"));
                        }

                        if (jsonObject.has("location_name")
                                && !TextUtils.isEmpty(jsonObject.getString("location_name"))
                                && !jsonObject.getString("location_name").equals("null")) {
                            item.setLocation_name(jsonObject.getString("location_name"));
                        }

                        if (jsonObject.has("lot_row")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_row"))
                                && !jsonObject.getString("lot_row").equals("null")) {
                            item.setLot_row(jsonObject.getString("lot_row"));
                        }

                        if (jsonObject.has("lot_number")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_number"))
                                && !jsonObject.getString("lot_number").equals("null")) {
                            item.setLot_number(jsonObject.getString("lot_number"));
                        }
                        if (jsonObject.has("lot_id")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_id"))
                                && !jsonObject.getString("lot_id").equals("null")) {
                            item.setLot_id(jsonObject.getString("lot_id"));
                        }

                        if (jsonObject.has("occupied")
                                && !TextUtils.isEmpty(jsonObject.getString("occupied"))
                                && !jsonObject.getString("occupied").equals("null")) {
                            item.setOccupied(jsonObject.getString("occupied").toLowerCase());
                        }

                        if (jsonObject.has("plate_no")
                                && !TextUtils.isEmpty(jsonObject.getString("plate_no"))
                                && !jsonObject.getString("plate_no").equals("null")) {
                            item.setPlate_no(jsonObject.getString("plate_no"));
                        }
                        if (jsonObject.has("plate_state")
                                && !TextUtils.isEmpty(jsonObject.getString("plate_state"))
                                && !jsonObject.getString("plate_state").equals("null")) {
                            item.setPlate_state(jsonObject.getString("plate_state"));
                        }

                        if (jsonObject.has("entry_date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("entry_date_time"))
                                && !jsonObject.getString("entry_date_time").equals("null")) {
                            item.setEntry_date_time(jsonObject.getString("entry_date_time"));
                        }

                        if (jsonObject.has("exit_date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("exit_date_time"))
                                && !jsonObject.getString("exit_date_time").equals("null")) {
                            item.setExit_date_time(jsonObject.getString("exit_date_time"));
                        }

                        if (jsonObject.has("expiry_time")
                                && !TextUtils.isEmpty(jsonObject.getString("expiry_time"))
                                && !jsonObject.getString("expiry_time").equals("null")) {
                            item.setExpiry_time(jsonObject.getString("expiry_time"));
                        }

                        if (jsonObject.has("parking_type")
                                && !TextUtils.isEmpty(jsonObject.getString("parking_type"))
                                && !jsonObject.getString("parking_type").equals("null")) {
                            item.setParking_type(jsonObject.getString("parking_type"));
                        }

                        if (jsonObject.has("parking_status")
                                && !TextUtils.isEmpty(jsonObject.getString("parking_status"))
                                && !jsonObject.getString("parking_status").equals("null")) {
                            item.setParking_status(jsonObject.getString("parking_status").toLowerCase());
                        }

                        if (jsonObject.has("lot_reservable")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_reservable"))
                                && !jsonObject.getString("lot_reservable").equals("null")) {
                            if (jsonObject.getString("lot_reservable").equals("1")) {
                                item.setLot_reservable(true);
                            } else if (jsonObject.getString("lot_reservable").equals("0")) {
                                item.setLot_reservable(false);
                            } else {
                                item.setLot_reservable(jsonObject.getBoolean("lot_reservable"));
                            }
                        }
                        if (jsonObject.has("lot_reservable_only")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_reservable_only"))
                                && !jsonObject.getString("lot_reservable_only").equals("null")) {
                            if (jsonObject.getString("lot_reservable_only").equals("1")) {
                                item.setLot_reservable_only(true);
                            } else if (jsonObject.getString("lot_reservable_only").equals("0")) {
                                item.setLot_reservable_only(false);
                            } else {
                                item.setLot_reservable_only(jsonObject.getBoolean("lot_reservable_only"));
                            }
                        }
                        if (jsonObject.has("premium_lot")
                                && !TextUtils.isEmpty(jsonObject.getString("premium_lot"))
                                && !jsonObject.getString("premium_lot").equals("null")) {
                            if (jsonObject.getString("premium_lot").equals("1")) {
                                item.setPremium_lot(true);
                            } else if (jsonObject.getString("premium_lot").equals("0")) {
                                item.setPremium_lot(false);
                            } else {
                                item.setPremium_lot(jsonObject.getBoolean("premium_lot"));
                            }
                        }
                        if (jsonObject.has("special_need_handi_lot")
                                && !TextUtils.isEmpty(jsonObject.getString("special_need_handi_lot"))
                                && !jsonObject.getString("special_need_handi_lot").equals("null")) {
                            if (jsonObject.getString("special_need_handi_lot").equals("1")) {
                                item.setSpecial_need_handi_lot(true);
                            } else if (jsonObject.getString("special_need_handi_lot").equals("0")) {
                                item.setSpecial_need_handi_lot(false);
                            } else {
                                item.setSpecial_need_handi_lot(jsonObject.getBoolean("premium_lot"));
                            }
                        }
                        if (jsonObject.has("location_id")
                                && !TextUtils.isEmpty(jsonObject.getString("location_id"))
                                && !jsonObject.getString("location_id").equals("null")) {
                            item.setLocation_id(Integer.parseInt(jsonObject.getString("location_id")));
                        }

                        if (jsonObject.has("is_reserved")
                                && !TextUtils.isEmpty(jsonObject.getString("is_reserved"))
                                && !jsonObject.getString("is_reserved").equals("null")) {
                            if (jsonObject.getString("is_reserved").equals("1")) {
                                item.setIs_reserved(true);
                            } else {
                                item.setIs_reserved(false);
                            }
                        }
                        item.setIs_reserved(false);
                        if (!TextUtils.isEmpty(item.getParking_status())
                                && !TextUtils.isEmpty(item.getOccupied())
                                && item.getParking_status().equals("ENTRY")
                                && item.getOccupied().equals("yes")) {

                            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date exitre = null;
                            try {
                                exitre = format1.parse(item.getExpiry_time());

                            } catch (android.net.ParseException e) {
                                // TODO Auto-generated catch block
                                Log.e("#DEBUG", "  Error:  " + e.getMessage());
                                e.printStackTrace();
                            } catch (java.text.ParseException e) {
                                Log.e("#DEBUG", "  Error:  " + e.getMessage());
                                e.printStackTrace();
                            }

                            if (current.after(exitre)) {
                                item.setExpired(true);
                            } else {
                                item.setExpired(false);
                            }
                        }
                        if (!locationLots.contains(item))
                            if (item.isLot_reservable()) {
                                locationLots.add(item);
                            }

                    }


                } catch (JSONException e) {
                    Log.e("#DEBUG", "  Error:  " + e.getMessage());
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.e("#DEBUG", "  Error:  " + e.getMessage());
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rlProgressbar != null)
//                rlProgressbar.setVisibility(View.GONE);
                //progressBar.setVisibility(View.GONE);
                if (getActivity() != null) {
                    new fetchReservedParking().execute();
                }

            super.onPostExecute(s);
        }
    }

    private void updateViews() {

        if (rParking != null) {

            new fetchAddressFromLatLng(rParking.getMarkerLat(), rParking.getMarkerLng()).execute();

            if (!rParking.getMaxTime().equals("") && !rParking.getParkingDuration().equals("")) {
                try {

                    values.clear();
                    values.add(rParking.getParkingDurationUnit() + "s");
                    double limt = Double.parseDouble(rParking.getMaxTime());
                    double pricing_dur = Double.parseDouble(rParking.getParkingDuration());
                    int ssw = (int) (limt / pricing_dur);
                    for (int y = 1; y <= ssw; y++) {
                        if (rParking.getParkingDurationUnit().equals("Minute")) {
                            int min = (int) (y * pricing_dur);
                            if (min == 1) {
                                values.add(String.valueOf(min) + " Min");
                            } else {
                                values.add(String.valueOf(min) + " Mins");
                            }
                        } else if (rParking.getParkingDurationUnit().equals("Month")) {
                            int min = (int) (y * pricing_dur);
                            if (min == 1) {
                                values.add(String.valueOf(min) + " Month");
                            } else values.add(String.valueOf(min) + " Months");
                        } else if (rParking.getParkingDurationUnit().equals("Hour")) {
                            int min = (int) (y * pricing_dur);
                            if (min == 1) {
                                values.add(String.valueOf(min) + " Hr");
                            } else values.add(String.valueOf(min) + " Hrs");
                        } else if (rParking.getParkingDurationUnit().equals("Day")) {
                            int min = (int) (y * pricing_dur);
                            if (min == 1) {
                                values.add(String.valueOf(min) + " Day");
                            } else values.add(String.valueOf(min) + " Days");
                        } else if (rParking.getParkingDurationUnit().equals("Week")) {
                            int min = (int) (y * pricing_dur);
                            if (min == 1) {
                                values.add(String.valueOf(min) + " Week");
                            } else values.add(String.valueOf(min) + " Weeks");
                        }
                    }
                } catch (ArithmeticException a) {
                } catch (Exception E) {
                }
            }

//            if (rParking.getParkingDurationUnit() != null
//                    && !TextUtils.isEmpty(rParking.getParkingDurationUnit())
//                    && !rParking.getParkingDurationUnit().equals("null")) {
//                if (rParking.getParkingDurationUnit().equals("Minute")) {
//                    DURATION_UNIT = 0;
//                } else if (rParking.getParkingDurationUnit().equals("Hour")) {
//                    DURATION_UNIT = 1;
//                } else if (rParking.getParkingDurationUnit().equals("Day")) {
//                    DURATION_UNIT = 2;
//                } else if (rParking.getParkingDurationUnit().equals("Week")) {
//                    DURATION_UNIT = 3;
//                } else if (rParking.getParkingDurationUnit().equals("Month")) {
//                    DURATION_UNIT = 4;
//                }
//            }

            tvParkingAvailability.setText(rParking.getParkingAvailability());
            if (rParking.getParkingDuration().equals("1")) {
                tvParkingRate.setText("Rate: $" + rParking.getParkingRate() + "/" + rParking.getParkingDuration()
                        + " " + rParking.getParkingDurationUnit());
            } else {
                tvParkingRate.setText("Rate: $" + rParking.getParkingRate() + "/" + rParking.getParkingDuration()
                        + "s " + rParking.getParkingDurationUnit());
            }

            if (rParking.getMaxTime().equals("1")) {
                tvParkingMaxHour.setText("Max " + rParking.getMaxTime() + " " + rParking.getParkingDurationUnit());
            } else {
                tvParkingMaxHour.setText("Max " + rParking.getMaxTime() + " " + rParking.getParkingDurationUnit() + "s");
            }

            //Duration

            durations.clear();
            for (int i = 1; i < values.size(); i++) {
                item ii = new item();
                ii.setState(values.get(i).toString());
                ii.setSelected_parking(false);
                durations.add(ii);
            }

            if (durations.size() >= 4) {
                durations.get(3).setSelected_parking(true);
                shoues = durations.get(3).getState();
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    tvParkingDuration.setText(hr + " Hrs");
                } else {
                    tvParkingDuration.setText(shoues);
                }

            } else if (durations.size() >= 3) {
                durations.get(2).setSelected_parking(true);
                shoues = durations.get(2).getState();
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    tvParkingDuration.setText(hr + " Hrs");
                } else {
                    tvParkingDuration.setText(shoues);
                }
            } else if (durations.size() >= 2) {
                durations.get(1).setSelected_parking(true);
                shoues = durations.get(1).getState();
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    tvParkingDuration.setText(hr + " Hrs");
                } else {
                    tvParkingDuration.setText(shoues);
                }
            }
        }

    }

    public class fetchVehicles extends AsyncTask<String, String, String> {
        JSONObject json = null;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "0");
        String vehiclenourl = "_table/user_vehicles?filter=user_id%3D" + user_id + "";

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            vehicles.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("get_vehicle_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);
                    Log.e("rerpones", String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        String plate_no = c.getString("plate_no");
                        String state = c.getString("registered_state");
                        String id = c.getString("id");
                        item.setState(state);
                        item.setPlateno(plate_no);
                        item.setId(id);
                        item.setVehicle_country(c.getString("vehicle_country"));
                        item.setIsselect(false);
                        item.setCheckboxselected(false);
                        vehicles.add(item);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            //progressBar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null) {
                rlProgressbar.setVisibility(View.GONE);
                rlBtnParkingDuration.performClick();
            }
            super.onPostExecute(s);
        }
    }

    private void setListeners() {

        rlBtnEntryTime.setOnClickListener(v -> showDialogSelectEntryTime());

        rlBtnParkingDuration.setOnClickListener(v -> showDialogParkingDuration(durations));

        rlBtnParkingLot.setOnClickListener(v -> showDialogParkingLots());

        rlBtnPlatNo.setOnClickListener(v -> showDialogSelectPlat());

        tvBtnReserveNow.setOnClickListener(v -> {
            String parkingDuration = tvParkingDuration.getText().toString();
            String entryTime = tvEntryTime.getText().toString();
            String parkingSpace = tvParkingLot.getText().toString();
            if (TextUtils.isEmpty(parkingDuration)) {
                Toast.makeText(getActivity(), "Please select parking duration!", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(entryTime)) {
                Toast.makeText(getActivity(), "Please select entry time!", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(parkingSpace)) {
                Toast.makeText(getActivity(), "Please select parking space!", Toast.LENGTH_SHORT).show();
            } else {
                showPaymentOptionReservation();
            }
        });

    }

    private void showDialogSelectPlat() {
        RelativeLayout viewGroup = (RelativeLayout) getActivity().findViewById(R.id.popupfree);

        LayoutInflater layoutInflater = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.parkherepopup, null);
        if (popup_state != null && popup_state.isShowing()) {
            popup_state.dismiss();
            popup_state = null;
        }
        popup_state = new PopupWindow(getActivity());
        popup_state.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup_state.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup_state.setContentView(layout);
        popup_state.setFocusable(false);
        popup_state.setAnimationStyle(R.style.animationName);
        popup_state.setBackgroundDrawable(null);
        ListView lvVehicles = (ListView) layout.findViewById(R.id.listselectvehicl);
        TextView txt_add_vi = (TextView) layout.findViewById(R.id.txtaddvehicle);
        txt_add_vi.setVisibility(View.VISIBLE);
        Resources rs;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int yyy = llMain.getHeight();
            int yyy1 = rlTitle.getHeight();
            int final_position = yyy1 + 245;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                popup_state.showAtLocation(layout, Gravity.NO_GRAVITY, 0, final_position);
            } else {
                popup_state.showAsDropDown(rlTitle, 0, 0);
            }
        } else {
            popup_state.showAsDropDown(rlTitle, 0, 0);
        }
        if (vehicles.size() > 0) {
            Activity activty = getActivity();
            if (activty == null) {
                if (popup_state != null && popup_state.isShowing()) {
                    popup_state.dismiss();
                    popup_state = null;
                }
            } else {
                VehiclesAdapter adpater = new VehiclesAdapter(getActivity(), vehicles, rs = getResources());
                lvVehicles.setAdapter(adpater);
            }
        }

        txt_add_vi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                is_add_back = true;
                SharedPreferences add1 = getActivity().getSharedPreferences("addvehicle", Context.MODE_PRIVATE);
                SharedPreferences.Editor ed = add1.edit();
                ed.putString("addvehicle", "yes");
                ed.commit();
                Bundle b = new Bundle();
                b.putString("direct_add", "yes");
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                vehicle_add pay = new vehicle_add();
                pay.registerForListener(FinalReservationFragment.this);
                pay.setArguments(b);
                ft.add(R.id.My_Container_1_ID, pay, "my_vehicle_add");
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
                if (popup_state != null && popup_state.isShowing()) {
                    popup_state.dismiss();
                    popup_state = null;
                }
            }
        });

        RelativeLayout imageclose;
        imageclose = (RelativeLayout) layout.findViewById(R.id.close);
        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.popup_off);
                    layout.startAnimation(animation);
                    if (popup_state != null && popup_state.isShowing()) {
                        popup_state.dismiss();
                        popup_state = null;
                    }
                } catch (Exception e) {
                }
            }
        });
        rlProgressbar.setVisibility(View.GONE);
    }

    private void showDialogParkingLots() {

        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        View dialogView = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_select_locatoin_lot, null);
        builder.setView(dialogView);
        dialogLocationLot = builder.create();
        dialogLocationLot.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView tvLocationTitle = dialogView.findViewById(R.id.tvLocationTitle);
        tvLocationTitle.setVisibility(View.VISIBLE);
        tvLocationTitle.setText(rParking.getLocationName());
        RelativeLayout rlClose = dialogView.findViewById(R.id.rlClose);
        RelativeLayout rlDone = dialogView.findViewById(R.id.rlDone);
        RelativeLayout rlSelectedLot = dialogView.findViewById(R.id.rlSelectedLot);
        RecyclerView rvLocationLot = dialogView.findViewById(R.id.rvLocationLot);
        TextView tvSelectedLot = dialogView.findViewById(R.id.tvSelectedLot);

        if (selectedLocationLot != null) {
            rlSelectedLot.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(selectedLocationLot.getLot_id())) {
                tvSelectedLot.setText(selectedLocationLot.getLot_id());
            }
        } else {
            rlSelectedLot.setVisibility(View.GONE);
        }

        LocationLotAdapter locationLotAdapter = new LocationLotAdapter();
        rvLocationLot.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        rvLocationLot.setAdapter(locationLotAdapter);

        rlDone.setOnClickListener(view -> {
            dialogLocationLot.dismiss();
            if (TextUtils.isEmpty(tvPlatNo.getText().toString())) {
                rlBtnPlatNo.performClick();
            }
        });

        rlClose.setOnClickListener(view -> dialogLocationLot.dismiss());

        updateManagedLocations(locationLotAdapter);

        dialogLocationLot.show();
    }

    private void showDialogParkingDuration(ArrayList<item> data1) {

        Context context = getActivity();
//            RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.hourpopup_p, null, false);
        final PopupWindow popupmanaged = new PopupWindow(context);
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        //  popupmanaged.setAnimationStyle(R.style.animationName);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);
        Button btnparkhere;
        ImageView imageclose;
        GridView grid_managed;
        RelativeLayout rl_popclose;
        ImageView img_state;
        TextView txt_state_;
        btnparkhere = (Button) layout.findViewById(R.id.btn_managed_park);
        grid_managed = (GridView) layout.findViewById(R.id.grid_managed);
        imageclose = (ImageView) layout.findViewById(R.id.img_managed_close);
        RelativeLayout rl_done = (RelativeLayout) layout.findViewById(R.id.rl_done);
        rl_popclose = (RelativeLayout) layout.findViewById(R.id.rl_pop);
        rl_popclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });
        img_state = (ImageView) layout.findViewById(R.id.img_state);
        txt_state_ = (TextView) layout.findViewById(R.id.txt_state_name);

        Resources rs;
        final hour_adapter adpter = new hour_adapter(getActivity(), data1, rs = getResources());
        grid_managed.setAdapter(adpter);


        imageclose.setOnClickListener(v -> {
          /*  for(int i=0;i<data.size();i++){
                data.get(i).setSelected_parking(false);
            }
            adpter.notifyDataSetChanged();*/
            popupmanaged.dismiss();
            // txt_hours.setText("Select Hour");

        });

        for (int k = 0; k < data1.size(); k++) {
            boolean selected_ore = data1.get(k).isSelected_parking();

            if (selected_ore) {
                shoues = data1.get(k).getState();
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    txt_state_.setText(hr + " Hrs");
                } else {
                    txt_state_.setText(shoues);
                }
                img_state.setImageResource(R.mipmap.new_cook_hr);
                break;
            }
        }

        rl_done.setOnClickListener(v -> {
            popupmanaged.dismiss();

            for (int k = 0; k < data1.size(); k++) {
                boolean selected_ore = data1.get(k).isSelected_parking();

                if (selected_ore) {
                    shoues = data1.get(k).getState();
                    if (shoues.contains("Hour")) {
                        String hr = shoues.substring(0, shoues.indexOf(' '));
                        tvParkingDuration.setText(hr + " Hrs");
                    } else {
                        tvParkingDuration.setText(shoues);
                    }
                    break;
                }
            }

        });
        grid_managed.setOnItemClickListener((parent, view, position, id) -> {

            for (int i = 0; i < data1.size(); i++) {
                data1.get(i).setSelected_parking(false);
            }
            data1.get(position).setSelected_parking(true);
            shoues = data1.get(position).getState();
            if (shoues.contains("Hour")) {
                String hr = shoues.substring(0, shoues.indexOf(' '));
                tvParkingDuration.setText(hr + " Hrs");
            } else {
                tvParkingDuration.setText(shoues);
            }
            adpter.notifyDataSetChanged();
            popupmanaged.dismiss();
            tvEntryTime.setText("");
            tvExitTime.setText("");
            selectedLocationLot = null;
            tvParkingLot.setText("");
            if (TextUtils.isEmpty(tvEntryTime.getText().toString())) {
                rlBtnEntryTime.performClick();
            }
//                if (rl_sp_row.getVisibility() == View.VISIBLE) {
////                    Showmanaedpopup(getActivity(), data);
//                    showDialogLocationLots();
//                    show_marke_payment_button();
//                } else {
//                    show_marke_payment_button();
//                }
        });
    }

    private void showDialogSelectEntryTime() {
        intHour = Integer.valueOf(tvParkingDuration.getText().toString().substring(0, tvParkingDuration.getText().toString().indexOf(" ")));
        DatePickerDialog entryDatePickerDialog = new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
            final Calendar calendar1 = Calendar.getInstance();
            calendar1.set(Calendar.YEAR, i);
            calendar1.set(Calendar.MONTH, i1);
            calendar1.set(Calendar.DAY_OF_MONTH, i2);


            new TimePickerDialog(getActivity(), (timePicker, i3, i11) -> {

                Calendar datetime = Calendar.getInstance();
                Calendar calendar = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, i3);
                datetime.set(Calendar.MINUTE, i11);
                if (datetime.getTimeInMillis() >= calendar.getTimeInMillis()) {
                    calendar1.set(Calendar.HOUR_OF_DAY, i3);
                    calendar1.set(Calendar.MINUTE, i11);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault());
                    tvEntryTime.setText(dateFormat1.format(new Date(calendar1.getTimeInMillis())));
                    reserveEntryTime = dateFormat.format(new Date(calendar1.getTimeInMillis()));
                    if (rParking.getParkingDurationUnit().equals("Hour")) {
                        calendar1.add(Calendar.HOUR, intHour);
                    } else if (rParking.getParkingDurationUnit().equals("Minute")) {
                        calendar1.add(Calendar.MINUTE, intHour);
                    } else if (rParking.getParkingDurationUnit().equals("Day")) {
                        calendar1.add(Calendar.DAY_OF_MONTH, intHour);
                    } else if (rParking.getParkingDurationUnit().equals("Week")) {
                        calendar1.add(Calendar.WEEK_OF_MONTH, intHour);
                    } else if (rParking.getParkingDurationUnit().equals("Month")) {
                        calendar1.add(Calendar.MONTH, intHour);
                    }
                    tvExitTime.setText(dateFormat1.format(new Date(calendar1.getTimeInMillis())));
                    reserveExpiryTime = dateFormat.format(new Date(calendar1.getTimeInMillis()));
                    try {
                        selectedEntryTime = dateFormat.parse(reserveEntryTime).getTime();
                        selectedExitTime = dateFormat.parse(reserveExpiryTime).getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    tvParkingLot.setText("");
                    if (TextUtils.isEmpty(tvParkingLot.getText().toString())) {
                        rlBtnParkingLot.performClick();
                    }
                } else {
                    Toast.makeText(getActivity(), "Please select future date!", Toast.LENGTH_LONG).show();
                }

            }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE),
                    false).show();
        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        entryDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        entryDatePickerDialog.show();
    }

    public class updateLocationLot extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String exit_status = "_table/location_lot";
        JSONObject json, json1;
        String re_id;
        String id;

        public updateLocationLot(String idd) {
            this.id = idd;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (parkingType.equals("Township")) {
                exit_status = "_table/location_lot";
            } else if (parkingType.equals("Commercial")) {
                exit_status = "_table/location_lot";
            } else if (parkingType.equals("Other")) {
                exit_status = "_table/location_lot";
            }
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", selectedLocationLot.getId());
            jsonValues.put("is_reserved", 1);
            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            Log.e("managed_url", url);
            Log.e("parama", String.valueOf(json));

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.e("#DEBUG", "  updateManageLot: ResponseID: " + re_id);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null && json1 != null) {
                if (!re_id.equals("null")) {
                }
                super.onPostExecute(s);
            }
        }
    }

    private void initView(View view) {
        tvParkingAvailability = view.findViewById(R.id.tvParkingAvailability);
        tvParkingMaxHour = view.findViewById(R.id.tvParkingMaxHour);
        tvParkingRate = view.findViewById(R.id.tvParkingRate);
        tvEntryTime = view.findViewById(R.id.tvEntryTime);
        tvExitTime = view.findViewById(R.id.tvExitTime);
        tvParkingDuration = view.findViewById(R.id.tvParkingDuration);
        tvParkingLot = view.findViewById(R.id.tvParkingLot);
        tvPlatNo = view.findViewById(R.id.tvPlatNo);

        tvBtnReserveNow = view.findViewById(R.id.tvBtnReserveNow);

        rlBtnEntryTime = view.findViewById(R.id.rlBtnEntryTime);
        rlBtnExitTime = view.findViewById(R.id.rlBtnExitTime);
        rlBtnParkingDuration = view.findViewById(R.id.rlBtnParkingDuration);
        rlBtnParkingLot = view.findViewById(R.id.rlBtnParkingLot);
        rlBtnPlatNo = view.findViewById(R.id.rlBtnPlatNo);
        rlPaymentSuccess = view.findViewById(R.id.rlPaymentSuccess);

        llParkOptions = view.findViewById(R.id.llParkOptions);
        llMain = view.findViewById(R.id.llMain);
        rlTitle = view.findViewById(R.id.rlTitle);

        rlProgressbar = view.findViewById(R.id.rlProgressbar);

    }

    public class hour_adapter extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        boolean isselect = false;
        int a = 0;
        private Activity activity;
        private ArrayList<item> data;
        private SparseBooleanArray mCheckStates;
        private LayoutInflater inflater = null;

        /*************
         * CustomAdapter Constructor
         *****************/
        public hour_adapter(Activity a, ArrayList<item> d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;
            data = d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        /********
         * What is the size of Passed Arraylist Size
         ************/
        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        /******
         * Depends upon data size called for each row , Create each ListView row
         *****/
        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;


            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_state, null);
                holder = new ViewHolder();

                holder.txt_state = (TextView) vi.findViewById(R.id.txt_state);
                holder.img = (ImageView) vi.findViewById(R.id.img_state);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {

            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String cat = tempValues.getState();
                if (cat == null) {
                } else if (cat != null) {

                    if (tempValues.isSelected_parking()) {
                        holder.img.setImageResource(R.mipmap.new_cook_hr);
                        holder.txt_state.setTextColor(getResources().getColor(R.color.selected_text));
                    } else {
                        holder.img.setImageResource(R.mipmap.icon_cook_un);
                        holder.txt_state.setTextColor(Color.parseColor("#000000"));
                    }

                    holder.txt_state.setText(tempValues.getState());
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }


        public class ViewHolder {

            public TextView txt_state;
            ImageView img;
        }
    }

    private void showPaymentOptionReservation() {
//        isClickParking = false;
        PaymentOptionDialog dialog = PaymentOptionDialog.newInstance();
        Bundle bundle = new Bundle();
        bundle.putBoolean("mIsPayLater", !isPrePaymentRequiredForReservation);
        dialog.setArguments(bundle);
        dialog.show(getChildFragmentManager(), "payment");
    }

    private class LocationLotAdapter extends RecyclerView.Adapter<LocationLotAdapter.LocationLotHolder> {
        private Drawable drawableRed, drawableBlue, drawableGreen, drawableGrey;

        @NonNull
        @Override
        public LocationLotAdapter.LocationLotHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            drawableRed = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_red);
            drawableBlue = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_blue);
            drawableGreen = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_green);
            drawableGrey = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_gree_car);
            return new LocationLotHolder(LayoutInflater.from(getActivity())
                    .inflate(R.layout.item_location_lot, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull LocationLotAdapter.LocationLotHolder locationLotHolder, int i) {
            LocationLot locationLot = locationLots.get(i);
            Log.e("#DEBUG", "    onBindViewHolder:  Is Reserved???  " + locationLot.isIs_reserved());
            locationLotHolder.tvReserved.setVisibility(View.GONE);
            locationLotHolder.ivReserved.setVisibility(View.GONE);
            locationLotHolder.ivReservedOnly.setVisibility(View.GONE);
            locationLotHolder.ivPremium.setVisibility(View.GONE);
            locationLotHolder.ivHandicapped.setVisibility(View.GONE);
            if (locationLot != null) {
                if (!TextUtils.isEmpty(locationLot.getLot_id())) {
                    locationLotHolder.tvRow.setText(locationLot.getLot_id());
                }

                if (!TextUtils.isEmpty(locationLot.getOccupied())) {
                    if (locationLot.getOccupied().equalsIgnoreCase("yes")) {
                        //Car already parked at this location
                        if (locationLot.isExpired()) {
                            //RED, parking expired
                            locationLotHolder.ivCar.setImageDrawable(drawableRed);
                        } else {
                            //GREEN, parking valid
                            locationLotHolder.ivCar.setImageDrawable(drawableGreen);
                        }
                        if (locationLot.isLot_reservable()) {
//                            locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                        }
                    } else if (locationLot.getOccupied().equalsIgnoreCase("no")) {
                        if (locationLot.isLot_reservable()) {
                            //GREY with R, Available for reservation
                            locationLotHolder.ivCar.setImageDrawable(drawableGrey);
//                            locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                        } else {
                            //GREY, Available for parking
                            locationLotHolder.ivCar.setImageDrawable(drawableGrey);
                        }

                    } else if (locationLot.getOccupied().equalsIgnoreCase("reserved")) {
                        //BLUE with R, Reserved for parking
                        locationLotHolder.ivCar.setImageDrawable(drawableBlue);
//                        locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (locationLot.isLot_reservable()) {
                        //GREY with R, Available for reservation
                        locationLotHolder.ivCar.setImageDrawable(drawableGrey);
//                        locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                    } else {
                        //GREY, Available for parking
                        locationLotHolder.ivCar.setImageDrawable(drawableGrey);
                    }
                }

                if (locationLot.isIs_reserved()) {
                    locationLotHolder.ivCar.setImageDrawable(drawableBlue);
                    locationLotHolder.viewIsReserved.setVisibility(View.VISIBLE);
                } else {
                    locationLotHolder.viewIsReserved.setVisibility(View.GONE);
//                    locationLotHolder.ivCar.setImageDrawable(drawableGrey);
                }

                if (locationLot.isLot_reservable()) {
                    locationLotHolder.ivReserved.setVisibility(View.VISIBLE);
                } else {
                    locationLotHolder.ivReserved.setVisibility(View.GONE);
                }

                if (locationLot.isPremium_lot())
                    locationLotHolder.ivPremium.setVisibility(View.VISIBLE);
                else locationLotHolder.ivPremium.setVisibility(View.GONE);

                if (locationLot.isSpecial_need_handi_lot())
                    locationLotHolder.ivHandicapped.setVisibility(View.VISIBLE);
                else locationLotHolder.ivHandicapped.setVisibility(View.GONE);

                if (locationLot.isLot_reservable_only()) {
                    locationLotHolder.ivReservedOnly.setVisibility(View.VISIBLE);
                } else locationLotHolder.ivReservedOnly.setVisibility(View.GONE);
            }

        }

        @Override
        public int getItemCount() {
            return locationLots.size();
        }

        class LocationLotHolder extends RecyclerView.ViewHolder {
            ImageView ivCar, ivReserved, ivPremium, ivHandicapped, ivReservedOnly;
            TextView tvRow, tvReserved;
            View viewIsReserved;

            LocationLotHolder(@NonNull View itemView) {
                super(itemView);

                ivCar = itemView.findViewById(R.id.ivCar);
                tvRow = itemView.findViewById(R.id.tvRow);
                tvReserved = itemView.findViewById(R.id.tvReserved);
                viewIsReserved = itemView.findViewById(R.id.viewIsReserved);
                ivReserved = itemView.findViewById(R.id.ivReserved);
                ivPremium = itemView.findViewById(R.id.ivPremium);
                ivHandicapped = itemView.findViewById(R.id.ivHandicapped);
                ivReservedOnly = itemView.findViewById(R.id.ivReservedOnly);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
//                            selectedLocationLot = locationLots.get(position);
                            if (locationLots.get(position).isIs_reserved()) {
                                Toast.makeText(getActivity(), "Space already reserved!", Toast.LENGTH_SHORT).show();
                            } else {
                                if (selectedLocationLot != null) {
                                    locationLots.get(locationLots.indexOf(
                                            new LocationLot(selectedLocationLot.getId()))).setOccupied("no");
                                    selectedLocationLot = null;
                                    tvParkingLot.setText("Parking Space");
                                }
                                if (!TextUtils.isEmpty(locationLots.get(position).getOccupied())) {
                                    if (locationLots.get(position).getOccupied().equalsIgnoreCase("no")) {
                                        locationLots.get(position).setOccupied("yes");
                                        selectedLocationLot = locationLots.get(position);
                                        tvParkingLot.setText(selectedLocationLot.getLot_id());
                                        notifyItemChanged(position);
                                        if (dialogLocationLot != null && dialogLocationLot.isShowing()) {
                                            dialogLocationLot.dismiss();
                                            if (TextUtils.isEmpty(tvPlatNo.getText().toString())) {
                                                rlBtnPlatNo.performClick();
                                            }
                                        }
                                    } else {
                                        if (selectedLocationLot != null &&
                                                selectedLocationLot.getId() == locationLots.get(position).getId()) {
                                            selectedLocationLot = null;
                                            locationLots.get(position).setOccupied("no");
                                            if (dialogLocationLot != null && dialogLocationLot.isShowing()) {
                                                dialogLocationLot.dismiss();
                                            }
                                        } else {
                                            Toast.makeText(getActivity(), "Already occupied!", Toast.LENGTH_SHORT).show();
                                        }
//                                    locationLots.get(position).setOccupied("no");
                                    }
                                } else {
                                    locationLots.get(position).setOccupied("yes");
                                    selectedLocationLot = locationLots.get(position);
                                    tvParkingLot.setText(selectedLocationLot.getLot_id());
                                    notifyItemChanged(position);
                                    if (dialogLocationLot != null && dialogLocationLot.isShowing()) {
                                        dialogLocationLot.dismiss();
                                    }
                                }
                                notifyItemChanged(position);
                            }
                        }
                    }
                });
            }
        }
    }

    public class VehiclesAdapter extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public VehiclesAdapter(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txtplateno, txtstate;
            public RelativeLayout rl_main;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {
                vi = inflater.inflate(R.layout.selectvehicle, null);
                holder = new ViewHolder();

                holder.txtplateno = vi.findViewById(R.id.txtlicenseplate);
                holder.txtstate = vi.findViewById(R.id.txtstate);
                holder.rl_main = vi.findViewById(R.id.rl_main);
                vi.setTag(holder);
                vi.setOnClickListener(
                        v -> {
                            mSelectedPlatNo = holder.txtplateno.getText().toString().toUpperCase();
                            mSelectedPlatState = holder.txtstate.getText().toString().toUpperCase();
                            tvPlatNo.setText(mSelectedPlatNo + " (" + mSelectedPlatState + ")");
                            popup_state.dismiss();
                        });
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }
            if (data.size() <= 0) {
                holder.txtplateno.setText("No Data");
            } else {
                /***** Get each Model object from Arraylist ********/
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                /************  Set Model values in Holder elements ***********/
                String cat = tempValues.getPlantno();
                if (cat == null) {
                    holder.txtplateno.setVisibility(View.GONE);
                } else if (cat != null) {
                    Typeface light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Light.otf");
                    holder.txtplateno.setTypeface(light);
                    holder.txtstate.setTypeface(light);
                    holder.txtplateno.setText(tempValues.getPlantno());
                    holder.txtstate.setText(tempValues.getState());
//                    if (!rate.equals("") && !rate1.equals("")) {
//                        Double du = Double.parseDouble(rate1) / 60.0;
//                        txtrate.setText(String.format("Rate: $%s@%s%s", rate, rate1, duration_unit));
//                        if (!rate1.equals("1")) {
//                            if (duration_unit.equals("Hour")) {
//                                txtrate.setText(String.format("Rate: $%s@%s Hours", rate, rate1));
//                            } else if (duration_unit.equals("Minute")) {
//                                txtrate.setText(String.format("Rate: $%s@%s Minutes", rate, rate1));
//                            } else if (duration_unit.equals("Day")) {
//                                txtrate.setText(String.format("Rate: $%s@%s Days", rate, rate1));
//                            } else if (duration_unit.equals("Week")) {
//                                txtrate.setText(String.format("Rate: $%s@%s Weeks", rate, rate1));
//                            } else if (duration_unit.equals("Month")) {
//                                txtrate.setText(String.format("Rate: $%s@%s Months", rate, rate1));
//                            }
//                        } else {
//                            txtrate.setText(String.format("Rate: $%s@%s%s", rate, rate1, duration_unit));
//                        }
//                    }

                    if (position % 2 == 0) {
                        holder.rl_main.setBackgroundColor(Color.parseColor("#ffffff"));
                    } else {
                        holder.rl_main.setBackgroundColor(Color.parseColor("#e0e0e0"));
                    }
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.i(TAG, confirm.toJSONObject().toString(4));
                    Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                    JSONObject jsonObj = new JSONObject(confirm.getPayment().toJSONObject().toString(4));
                    Log.e("JSON IS", "LIKE" + jsonObj);
                    Log.e("short_description", "short_description : " + jsonObj.getString("short_description"));
                    Log.e("amount", "amount : " + jsonObj.getString("amount"));
                    Log.e("intent", "intent : " + jsonObj.getString("intent"));
                    Log.e("currency_code", "currency_code : " + jsonObj.getString("currency_code"));
                    JSONObject jsonObjResponse = confirm.toJSONObject().getJSONObject("response");
                    transection_id = jsonObjResponse.getString("id");
                    if (transection_id != null) {
                        isPrePaid = true;
                        rlPaymentSuccess.setVisibility(View.VISIBLE);
                        ActionStartsHere();
                        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                        String id = logindeatl.getString("id", "null");
                        paymentmethod = "Paypal";

                        new reserveNow().execute();
                    } else {
                        transactionFailedAlert();
                    }
                    Log.e("transactionId", ":;;;;" + transection_id);

                } catch (JSONException e) {
                    Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                }
            }
        } else {

        }
    }

    public void transactionFailedAlert() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Transaction Fail try again");
        alertDialog.setMessage("Fail");
        alertDialog.setPositiveButton("OK", (dialog, which) -> {
        });

        alertDialog.show();
    }

    public void ActionStartsHere() {
        againStartGPSAndSendFile();
    }

    public void againStartGPSAndSendFile() {
        new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                ActionStartsHere();
                rlPaymentSuccess.setVisibility(View.GONE);

            }

        }.start();
    }

    public class fetchServiceCharges extends AsyncTask<String, String, String> {

        JSONObject json, json1;
        String servicechargeurl = "_table/service_charges?filter=manager_id%3D'" + rParking.getTownshipCode() + "'";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + servicechargeurl;
            DefaultHttpClient client = new DefaultHttpClient();
            Log.e("#DEBUG", "   fetchServiceCharges:  URL:  " + url);
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        String tr_percentage = c.getString("tr_percentage");
                        String tr_fee = c.getString("tr_fee");
                        if (!tr_fee.equals("null")) {
                            tr_fee1 = Double.parseDouble(tr_fee);
                        } else {
                            tr_fee1 = 0.0;
                        }
                        if (!tr_percentage.equals("null")) {
                            tr_percentage1 = Double.parseDouble(tr_percentage);
                        } else {
                            tr_percentage1 = 0.0;
                        }
                        Log.e("#DEBUG", "    tr_percentage1:  " + String.valueOf(tr_percentage1));
                        Log.e("#DEBUG", "    tr_free: " + String.valueOf(tr_fee1));
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null && json != null) {
            }
            super.onPostExecute(s);
        }
    }
}
