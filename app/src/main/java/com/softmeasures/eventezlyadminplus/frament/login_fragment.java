package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class login_fragment extends Fragment {

    ConnectionDetector cd;
    RelativeLayout rl_progressbar;
    String id;
    EditText edit_email;
    EditText edit_password;
    String email, password;
    String Expn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
            + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
            + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
    private Login_listener listener;
    public static boolean Doyouwatregister = false;
    private boolean isBursor = false;
    String user_role1, session_id, is_sys_admin, towcode;
    String forgot_email = "";

    public interface Login_listener {
        public void onLogin_success(String role);

        public void onRegister_success();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_login, container, false);

        cd = new ConnectionDetector(getActivity());
        edit_email = (EditText) view.findViewById(R.id.edittext_email);
        edit_password = (EditText) view.findViewById(R.id.password);
        Button btn_login = (Button) view.findViewById(R.id.btnlogin);
        Button btn_resgister = (Button) view.findViewById(R.id.register);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar1);
        RelativeLayout rl_login = (RelativeLayout) view.findViewById(R.id.layoutlogin);
        TextView txt_skip = (TextView) view.findViewById(R.id.txtloginskip);
        TextView txt_forgot = (TextView) view.findViewById(R.id.txt_forgot_password);
        rl_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        if (!cd.isConnectingToInternet()) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }
        boolean ss = Doyouwatregister;
        boolean sss = Doyouwatregister;

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cd.isConnectingToInternet()) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Internet Connection Required");
                    alertDialog.setMessage("Please connect to working Internet connection");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    alertDialog.show();
                } else {
                    if (check_valid_data()) {
                        new login(email, password).execute();
                    }
                }
            }
        });

        btn_resgister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Register_Fragment pay = new Register_Fragment();
                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.add(R.id.My_Container_1_ID, pay);

                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
                /*if (!cd.isConnectingToInternet()) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Internet Connection Required");
                    alertDialog.setMessage("Please connect to working Internet connection");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    alertDialog.show();
                } else {
                    if (check_valid_data()) {
                        new register(email, password).execute();
                    }
                }*/
            }
        });

        txt_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                getActivity().onBackPressed();
            }
        });

        txt_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ForgotPassword(getActivity());
                /*Constants.mForgot = "Forgot";
                Forgot_Password pay = new Forgot_Password();
                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();*/
            }
        });

        return view;
    }

    private void showLoginFaildDialog() {
        rl_progressbar.setVisibility(View.GONE);
        TextView tvTitle = new TextView(getActivity());
        tvTitle.setText("Login Failed!");
        tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
        tvTitle.setPadding(0, 10, 0, 0);
        tvTitle.setTextColor(Color.parseColor("#000000"));
        tvTitle.setTextSize(20);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCustomTitle(tvTitle);
        alertDialog.setMessage("Invalid credentials / Invalid credentials supplied. \n Please try again.");
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertd = alertDialog.create();
        alertd.show();
        TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    public boolean check_valid_data() {
        boolean isresult = false;
        email = edit_email.getText().toString();
        password = edit_password.getText().toString();
        if (email.equals("")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Information Missing");
            alertDialog.setMessage("Please enter the Email for login.");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
            isresult = false;
        } else if (!email.matches(Expn)) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Invalid Email");
            alertDialog.setMessage("Please enter a valid Email ID.");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
            isresult = false;
        } else if (email.length() < 5) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Invalid");
            alertDialog.setMessage("Username must be greater than 5 characters");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
            isresult = false;
        } else if (password.equals("")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Information Missing");
            alertDialog.setMessage("Please enter the password for login.");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
            isresult = false;
        } else if (password.length() < 5) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Invalid");
            alertDialog.setMessage("Password must be greater than 4 characters");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();

            isresult = false;
        } else {
            isresult = true;
        }
        return isresult;
    }

    private class login extends AsyncTask<String, String, String> {
        String id;
        String login = "user/session";
        String email, passwrod, name;

        public login(String email, String pass) {
            this.email = email;
            this.passwrod = pass;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("email", email);
            jsonValues.put("remember_me", "true");
            jsonValues.put("password", passwrod);

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + login;
            Log.e("login_url", url);
            Log.e("login_param", String.valueOf(json));

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   login Response:  " + responseStr);
                    JSONObject jso = new JSONObject(responseStr);
                    String email = jso.getString("email");
                    id = jso.getString("id");
                    session_id = jso.getString("session_token");
                    name = jso.getString("name");
                    is_sys_admin = jso.getString("is_sys_admin");
                    user_role1 = jso.getString("role");
                    SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = logindeatl.edit();
                    edit.putString("name", name);
                    edit.putString("first_name", jso.getString("first_name"));
                    edit.putString("last_name", jso.getString("last_name"));
                    edit.putString("email", email);
                    edit.putString("is_sys_admin", is_sys_admin);
                    edit.putString("session_id", session_id);
                    edit.putString("id", id);
                    edit.putString("user_role", user_role1);
                    if (user_role1.equalsIgnoreCase("Registered"))
                        isBursor = true;
                    else isBursor = false;
                    edit.commit();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (getActivity() != null && id != null) {
                edit_email.setText("");
                edit_password.setText("");
                new checklocation(id, name).execute();
            } else {
                rl_progressbar.setVisibility(View.GONE);
                TextView tvTitle = new TextView(getActivity());
                tvTitle.setText("Login Failed!");
                tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                tvTitle.setPadding(0, 10, 0, 0);
                tvTitle.setTextColor(Color.parseColor("#000000"));
                tvTitle.setTextSize(20);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setCustomTitle(tvTitle);
                alertDialog.setMessage("Invalid credentials / Invalid credentials supplied. \n Please try again.");
                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                final AlertDialog alertd = alertDialog.create();
                alertd.show();
                TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                messageText.setGravity(Gravity.CENTER_HORIZONTAL);
            }

        }


    }

    public class checklocation extends AsyncTask<String, String, String> {
        String vechiclenourl;
        String res_id, name;
        JSONObject json = new JSONObject();
        JSONArray array;
        String towcode;

        public checklocation(String id, String name) {
            this.res_id = id;
            this.name = name;
            vechiclenourl = "_table/user_locations?filter=user_id%3D" + id + "";
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + vechiclenourl;
            Log.e("check_location", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   checkLocation:  " + responseStr);
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        towcode = c.getString("township_code");
                        //username=c.getString("profile_name");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                new userrole(res_id, name).execute();
            }
            super.onPostExecute(s);

        }
    }

    public class userrole extends AsyncTask<String, String, String> {
        String vechiclenourl;
        String res_id, name, user_role = "null";
        JSONObject json = new JSONObject();
        JSONArray array;

        public userrole(String id, String name) {
            this.res_id = id;
            this.name = name;
            vechiclenourl = "_table/township_users?filter=user_id%3D" + id + "";
        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + vechiclenourl;
            Log.e("user_role_url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   userrole:  Response:  " + responseStr);
                    json = new JSONObject(responseStr);

                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        user_role = c.getString("profile_name");
                        towcode = c.getString("township_code");
                        Log.e("role", user_role);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (user_role.equals("null")) {
                    showLoginFaildDialog();
                } else {
                    SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = logindeatl.edit();
                    edit.putString("id", res_id);
                    edit.putString("name", name);
                    edit.putString("twcode", towcode);
                    edit.putString("is_sys_admin", is_sys_admin);
                    edit.putString("session_id", session_id);
                    if (isBursor) {
                        edit.putString("role", "Bursar");
                        user_role = "Bursar";
                    } else edit.putString("role", user_role);
                    edit.commit();

                    if (user_role.equals("TwpAdmin")) {
                        new getCompanyUsers(res_id, name).execute();
                    } else {
                        if (listener != null)
                            listener.onLogin_success(user_role);
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public void registerForListener(Login_listener listener) {
        this.listener = listener;
    }

    private class register extends AsyncTask<String, String, String> {
        String mess, res, res_id;
        String login = "user/register?login=true";
        String password, email;
        String sss = "";

        public register(String email, String password) {
            this.email = email;
            this.password = password;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("email", email);
            jsonValues.put("first_name", "");
            jsonValues.put("last_name", "");
            jsonValues.put("password", password);

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + login;
            Log.e("regi_url", url);
            Log.e("regi_param", String.valueOf(json));
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            //setting json object to post request.
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            //this is your response:
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {

                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    res = String.valueOf(response.getStatusLine());

                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject jso = new JSONObject(responseStr);
                    sss = jso.getString("error");
                    if (!sss.equals("")) {
                        JSONObject hh = new JSONObject(sss);
                        JSONObject error = hh.getJSONObject("context");
                        JSONArray jj = error.getJSONArray("email");
                        mess = jj.get(0).toString();

                    } else {
                        if (res.equals("HTTP/1.1 500 Internal Server Error") || res.equals("HTTP/1.1 400 Bad Request")) {
                            JSONObject towers = jso.getJSONObject("error");
                            mess = towers.getString("message");
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            rl_progressbar.setVisibility(View.GONE);
            if (res.equals("HTTP/1.1 500 Internal Server Error") || res.equals("HTTP/1.1 400 Bad Request")) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Error");
                alertDialog.setMessage(mess);
                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            } else if (res.equals("HTTP/1.1 200 OK")) {
                new getuserprofile(email).execute();
            }
        }
    }

    private class getuserprofile extends AsyncTask<String, String, String> {
        String mess, res, res_id;
        String login = "_table/user_profile";
        String password, email;

        public getuserprofile(String emai) {
            this.email = emai;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            String currentdate = sdf.format(new Date());
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("email", email);
            jsonValues.put("user_name", email);
            jsonValues.put("date_time", currentdate);
            jsonValues.put("password", "");

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + login;
            Log.e("get_user_url", url);
            Log.e("get_user", String.valueOf(json));
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
//setting json object to post request.
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
//this is your response:
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {

                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    res = String.valueOf(response.getStatusLine());
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject jso = new JSONObject(responseStr);
                    if (res.equals("HTTP/1.1 500 Internal Server Error") || res.equals("HTTP/1.1 400 Bad Request")) {
                        JSONObject towers = jso.getJSONObject("error");
                        mess = towers.getString("message");
                    } else if (res.equals("HTTP/1.1 200 OK")) {
                        JSONArray array = jso.getJSONArray("resource");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject c = array.getJSONObject(i);
                            res_id = c.getString("id");
                        }
                        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                        SharedPreferences.Editor edit = logindeatl.edit();
                        edit.putString("id", res_id);
                        edit.commit();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (res.equals("HTTP/1.1 500 Internal Server Error") || res.equals("HTTP/1.1 400 Bad Request")) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Error");
                alertDialog.setMessage(mess);
                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            } else if (res.equals("HTTP/1.1 200 OK")) {

                if (Doyouwatregister != true) {
                    listener.onRegister_success();
                    frg_contact pay = new frg_contact();
                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.lastElement().onPause();
                    ft.remove(fragmentStack.pop());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                } else {
                    Doyouwatregister = false;
                    listener.onRegister_success();
                    getActivity().onBackPressed();
                    ((vchome) getActivity()).show_back_button();
//                    ((vchome) getActivity()).Show_vehicle_status(getActivity());
                }
            }
        }
    }

    private class getCompanyUsers extends AsyncTask<String, String, String> {
        String name, res_id;
        String login = "_table/company_users";
        String user_name, company_id, company_code, company_name, user_role;

        public getCompanyUsers(String id, String name) {
            this.res_id = id;
            this.name = name;
            login = "_table/company_users?filter=user_id%3D" + id + "";
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + login;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {

                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "  CompanyUsers Response:  " + responseStr);
                    JSONObject json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        user_name = c.getString("user_name");
                        email = c.getString("email");
                        company_id = c.getString("company_id");
                        company_code = c.getString("company_code");
                        company_name = c.getString("company_name");
                        user_role = c.getString("profile_name");
                        //status = c.getString("status");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                rl_progressbar.setVisibility(View.GONE);
                SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = logindeatl.edit();
                edit.putString("id", res_id);
                edit.putString("name", user_name);
                edit.putString("twcode", towcode);
                edit.putString("role", user_role);
                edit.putString("email", email);
                edit.putString("is_sys_admin", is_sys_admin);
                edit.putString("session_id", session_id);
                edit.putString("user_role", user_role);
                edit.putString("deptnum", "");
                edit.putString("deptcode", "");
                edit.putString("company_id", company_id);
                edit.putString("company_code", company_code);
                edit.putString("company_name", company_name);
                edit.apply();
                if (listener != null) {
                    listener.onLogin_success(user_role);
                }
            }
        }
    }

    private class ForgotPassword extends AsyncTask<String, String, String> {
        String id;
        String login = "user/password?reset=true";
        String email, message = "", success = "";

        public ForgotPassword(String email) {
            this.email = email;

        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("email", email);

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + login;
            Log.e("login_url", url);
            Log.e("login_param", String.valueOf(json));

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject jso = new JSONObject(responseStr);
                    if (!jso.has("error")) {
                        success = jso.getString("success");
                    } else {
                        JSONObject ds = jso.getJSONObject("error");
                        message = ds.getString("message");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null) {
                rl_progressbar.setVisibility(View.GONE);
                Log.e("Response ", "  : " + s);
            }
            if (getActivity() != null)
                if (message.equals("")) {
                    if (success.equals("true")) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setMessage("You will receive code in your registered email !");
                        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                ForgotPassword_next(getActivity());

                            }
                        });
                        alertDialog.show();
                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Error");
                        alertDialog.setMessage("Please try again");
                        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alertDialog.show();
                    }
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage(message);
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }

        }


    }

    @TargetApi(Build.VERSION_CODES.M)
    private void ForgotPassword(Activity context) {
        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.popup_forgot_password, viewGroup, false);
        final PopupWindow popup = new PopupWindow(context);
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(false);
        popup.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        popup.setFocusable(true);
        popup.setAnimationStyle(R.style.animationName);
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);
        final EditText edit_email = (EditText) layout.findViewById(R.id.edit_email);

        RelativeLayout rl_ok = (RelativeLayout) layout.findViewById(R.id.rl_ok);
        RelativeLayout rl_cancel = (RelativeLayout) layout.findViewById(R.id.rl_cancel);
        rl_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                forgot_email = edit_email.getText().toString();
                if (!forgot_email.equals("")) {
                    new ForgotPassword(forgot_email).execute();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Alert!!");
                    alertDialog.setMessage("Please fill in all required fields.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                }
            }
        });

        rl_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void ForgotPassword_next(Activity context) {
        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.popup_new_code_password, viewGroup, false);
        final PopupWindow popup = new PopupWindow(context);
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(false);
        popup.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        popup.setFocusable(true);
        popup.setAnimationStyle(R.style.animationName);
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);
        final EditText edit_code = (EditText) layout.findViewById(R.id.edit_code);
        final EditText edit_new_password = (EditText) layout.findViewById(R.id.edit_code);
        final EditText edit_retype = (EditText) layout.findViewById(R.id.edit_code);
        edit_code.setText("JDJ5JDEwJDhkejZlN2VUanlCM2t5dUZhdHAyck84VWcz");
        RelativeLayout rl_ok = (RelativeLayout) layout.findViewById(R.id.rl_ok);
        RelativeLayout rl_cancel = (RelativeLayout) layout.findViewById(R.id.rl_cancel);
        rl_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                String code = edit_code.getText().toString();
                String new_password = edit_new_password.getText().toString();
                String retype_password = edit_retype.getText().toString();

                if (code.equals("")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Alert!!");
                    alertDialog.setMessage("Please enter code");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                } else if (new_password.equals("")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Alert!!");
                    alertDialog.setMessage("Please enter New password");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                } else if (retype_password.equals("")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Alert!!");
                    alertDialog.setMessage("Please enter confirm password");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                } else if (!new_password.equals(retype_password)) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Alert!!");
                    alertDialog.setMessage("Password does not match");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                } else {
                    new ForgotPassword_final_setp(code, new_password).execute();
                }
            }
        });

        rl_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    private class ForgotPassword_final_setp extends AsyncTask<String, String, String> {
        String id;
        String login = "user/password";
        String code, message = "", success = "", password;

        public ForgotPassword_final_setp(String code, String password) {
            this.code = code;
            this.password = password;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("email", forgot_email);
            jsonValues.put("code", code);
            jsonValues.put("new_password", password);

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + login;
            Log.e("login_url", url);
            Log.e("login_param", String.valueOf(json));

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject jso = new JSONObject(responseStr);
                    if (!jso.has("error")) {
                        success = jso.getString("success");
                    } else {
                        JSONObject ds = jso.getJSONObject("error");
                        message = ds.getString("message");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            super.onPostExecute(s);
            if (getActivity() != null)
                if (message.equals("")) {
                    if (success.equals("true")) {

                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Error");
                        alertDialog.setMessage("Please try again");
                        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                            }
                        });
                        alertDialog.show();
                    }
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage(message);
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });
                    alertDialog.show();
                }

        }


    }
}
