package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.bumptech.glide.Glide;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.nileshp.multiphotopicker.photopicker.activity.PickImageActivity;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.MultiSelection;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.compressor.Compressor;
import com.softmeasures.eventezlyadminplus.frament.user_profile.Frg_User_edit_profile_Permit;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

/**
 * Created by Significant Info on 3/20/2018.
 */

public class MyPermit_VehicleUser_Details_ReApply extends Fragment {

    View rootView;
    ConnectionDetector cd;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar;
    ArrayList<item> permitsarray;
    Context mContext;
    TextView txt_Print, txt_PERMIT, txt_ISSUED, txt_FEE, txt_ProfileDetails,
            txt_ExpiryTime, btn_ProfileEdit;
    TextView txt_User_comments, txt_Town_comments,
            txt_User_comments1, txt_Town_comments1, txt_ValidUntil, txt_Requirements;
    ImageView img_PrintLogo, img_SignatureProof;

    String id, ListDriver, ListResidency, SignaturePath, date_time, user_id, township_code, township_name,
            permit_type, permit_name, residency_proof, permit_id, twp_id, manager_type, manager_type_id,
            approved, status, paid, scheme_type, rate, user_name, signature, driver_proof,
            First_contact_date, permit_expires_on, Permit_nextnum, Full_name, Full_address, email, phone,
            User_comments, Town_comments, Logo, Expires_by, Requirements, Renewable, Expires_Date;
    SimpleDateFormat dateFormat, dateFormat1;
    AmazonS3 s3;
    TransferUtility transferUtility;
    ArrayList<String> arrayListResidency, arrayListDriver;
    RecyclerView rv_horizontal, rv_DriverProof;
    CustomAdapter horizontalAdapter;
    DriverAdapter driverAdapter;
    int s_pos = 0, d_pos = 0;
    ArrayList<String> listResidency, listDriver;
    Double finalpay = 0.0, payball = 0.0, tr_percentage1 = 0.0, tr_fee1 = 0.0;
    //////////////////paypal///////////////////
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    private static final String TAG = "paymentQlighting";
    private static final String CONFIG_CLIENT_ID = "Aft0eWMQqeJfDnHj6pmSY_8Ta5kARfjF-BEfAFFzIenRq28s6HDlMCENCyUnmp9EvRBuI3P90XG4heGC";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static PayPalConfiguration config =
            new PayPalConfiguration().environment(CONFIG_ENVIRONMENT).clientId(CONFIG_CLIENT_ID);

    RelativeLayout showsucessmesg;
    String address, newbal = "0", finallab = "", payb = "";
    String timeStamp;
    Bitmap mSignature;
    TextView txt_Cancel, txt_Save;

    String pdfPath = "";
    Uri pfdURI = null;
    private ArrayList<String> pathList = new ArrayList<>();
    HorizontalScrollView horizontalScrollView, horizontalScrollView1;
    LinearLayout layoutListItemSelect, layoutListItemSelect1;
    ArrayList<MultiSelection> listItemSelect, listItemSelect1;
    ArrayList<MultiSelection> CompressItemSelect, CompressItemSelect1;
    int current_upload = 0, totla_size = 0;
    private int GALLERY = 1, CAMERA = 2;
    Button btn_ResidencySelectImage, btn_DriverLicenseSelectImage;
    public String m_Open = "Residency";

    public String file_download_path = "";
    File dir_File;
    String str_SignaturePath = "";
    Button btn_EditSignature, mClearButton;
    public SignaturePad mSignaturePad;

    StringBuffer file_Residency, file_SignaturePad, file_Driver;
    String final_Residency, final_Driver, final_SignaturePad;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_managepermit_vehicleuser_details_edit, container, false);
        mContext = getContext();
        cd = new ConnectionDetector(mContext);
        s3 = new AmazonS3Client(new BasicAWSCredentials("AKIAJNG22KFRVUAICSEA", "NSrQR/yAg52RPD3kp3FMb3NO+Vrxuty4iAwPU4th"));
        transferUtility = new TransferUtility(s3, getActivity().getApplicationContext());
        arrayListResidency = new ArrayList<String>();
        arrayListDriver = new ArrayList<String>();

        init();

        file_download_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" +
                getActivity().getPackageName() + "/Images/";
        dir_File = new File(file_download_path);
        if (!dir_File.exists()) {
            if (!dir_File.mkdirs()) {
            }
        }

        if (getArguments() != null) {
            id = getArguments().getString("id");
            ListDriver = getArguments().getString("ListDriver");
            ListResidency = getArguments().getString("ListResidency");
            SignaturePath = getArguments().getString("SignaturePath");
        }

        try {
            new getSubscription().execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

        onClicks();

        return rootView;
    }

    public void init() {
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) rootView.findViewById(R.id.rl_progressbar);
        showsucessmesg = (RelativeLayout) rootView.findViewById(R.id.sucessfully);
        txt_Print = (TextView) rootView.findViewById(R.id.txt_Print);
        txt_PERMIT = (TextView) rootView.findViewById(R.id.txt_PERMIT);
        txt_ISSUED = (TextView) rootView.findViewById(R.id.txt_ISSUED);
        txt_FEE = (TextView) rootView.findViewById(R.id.txt_FEE);
        txt_ProfileDetails = (TextView) rootView.findViewById(R.id.txt_ProfileDetails);
        txt_ExpiryTime = (TextView) rootView.findViewById(R.id.txt_ExpiryTime);
        img_SignatureProof = (ImageView) rootView.findViewById(R.id.img_SignatureProof);

        img_PrintLogo = (ImageView) rootView.findViewById(R.id.img_PrintLogo);

        rv_horizontal = (RecyclerView) rootView.findViewById(R.id.rv_horizontal);
        horizontalAdapter = new CustomAdapter(getContext(), arrayListResidency);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rv_horizontal.setLayoutManager(horizontalLayoutManagaer);
        rv_horizontal.setAdapter(horizontalAdapter);

        rv_DriverProof = (RecyclerView) rootView.findViewById(R.id.rv_DriverProof);
        driverAdapter = new DriverAdapter(getContext(), arrayListDriver);
        LinearLayoutManager DriverProofLayoutManagaer = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rv_DriverProof.setLayoutManager(DriverProofLayoutManagaer);
        rv_DriverProof.setAdapter(driverAdapter);

        txt_User_comments = (TextView) rootView.findViewById(R.id.txt_User_comments);
        txt_Town_comments = (TextView) rootView.findViewById(R.id.txt_Town_comments);
        txt_User_comments1 = (TextView) rootView.findViewById(R.id.txt_User_comments1);
        txt_Town_comments1 = (TextView) rootView.findViewById(R.id.txt_Town_comments1);
        txt_Cancel = (TextView) rootView.findViewById(R.id.txt_Cancel);
        txt_Save = (TextView) rootView.findViewById(R.id.txt_Save);

        listItemSelect = new ArrayList<MultiSelection>();
        listItemSelect1 = new ArrayList<MultiSelection>();

        file_Residency = new StringBuffer();
        file_Driver = new StringBuffer();
        file_SignaturePad = new StringBuffer();
        layoutListItemSelect = (LinearLayout) rootView.findViewById(R.id.layoutListItemSelect);
        horizontalScrollView = (HorizontalScrollView) rootView.findViewById(R.id.horizontalScrollView);
        layoutListItemSelect1 = (LinearLayout) rootView.findViewById(R.id.layoutListItemSelect1);
        horizontalScrollView1 = (HorizontalScrollView) rootView.findViewById(R.id.horizontalScrollView1);
        btn_ResidencySelectImage = (Button) rootView.findViewById(R.id.btn_ResidencySelectImage);
        btn_DriverLicenseSelectImage = (Button) rootView.findViewById(R.id.btn_DriverLicenseSelectImage);

        btn_ProfileEdit = (TextView) rootView.findViewById(R.id.btn_ProfileEdit);
        txt_ValidUntil = (TextView) rootView.findViewById(R.id.txt_ValidUntil);
        txt_Requirements = (TextView) rootView.findViewById(R.id.txt_Requirements);
        btn_EditSignature = (Button) rootView.findViewById(R.id.btn_EditSignature);
        mSignaturePad = (SignaturePad) rootView.findViewById(R.id.signature_pad);
        mClearButton = (Button) rootView.findViewById(R.id.clear_button);

    }

    public void onClicks() {

        txt_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                MyPermits_VehicleUser pay = new MyPermits_VehicleUser();
                ft.add(R.id.My_Container_1_ID, pay);
                ft.commitAllowingStateLoss();
            }
        });

        txt_Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btn_EditSignature.getText().toString().equalsIgnoreCase("Edit")) {
                    new ImageCompress().execute();
                } else if (btn_EditSignature.getText().toString().equalsIgnoreCase("Cancel")) {
                    if (mClearButton.isEnabled()) {
                        Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                        if (addJpgSignatureToGallery(signatureBitmap)) {
                            new ImageCompress().execute();
                        } else {
                            Toast.makeText(getActivity(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        new ImageCompress().execute();
                    }
                }
            }
        });

        btn_ResidencySelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_Open = "Residency";
                showPictureDialog();
            }
        });

        btn_DriverLicenseSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_Open = "DriverLicense";
                showPictureDialog();
            }
        });

        btn_ProfileEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ProfileEdit = true;
                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Frg_User_edit_profile_Permit pay = new Frg_User_edit_profile_Permit();
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        btn_EditSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btn_EditSignature.getText().toString().equalsIgnoreCase("Edit")) {
                    mSignaturePad.setVisibility(View.VISIBLE);
                    mClearButton.setVisibility(View.VISIBLE);
                    img_SignatureProof.setVisibility(View.GONE);
                    btn_EditSignature.setText("Cancel");
                } else if (btn_EditSignature.getText().toString().equalsIgnoreCase("Cancel")) {
                    mSignaturePad.setVisibility(View.GONE);
                    mClearButton.setVisibility(View.GONE);
                    img_SignatureProof.setVisibility(View.VISIBLE);
                    btn_EditSignature.setText("Edit");
                }
            }
        });

        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                //Event triggered when the pad is touched
            }

            @Override
            public void onSigned() {
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mClearButton.setEnabled(false);
            }
        });

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });

    }

    File photo;

    public boolean addJpgSignatureToGallery(Bitmap signature) {
        boolean result = false;
        try {
            photo = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.jpg", System.currentTimeMillis()));
            saveBitmapToJPG(signature, photo);
            scanMediaFile(photo);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public File getAlbumStorageDir(String albumName) {
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public class getSubscription extends AsyncTask<String, String, String> {

        JSONObject json;
        String subcribe = "_table/permit_subscription?filter=(id%3D'" + URLEncoder.encode(id, "utf-8") + "')";

        public getSubscription() throws UnsupportedEncodingException {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            permitsarray = new ArrayList<item>();
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + subcribe;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    Log.e("json ", " : " + json.toString());
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id = (c.getString("id"));
                        date_time = (c.getString("date_time"));
                        user_id = (c.getString("user_id"));
                        township_code = (c.getString("township_code"));
                        township_name = (c.getString("township_name"));
                        manager_type = (c.getString("manager_type"));
                        manager_type_id = (c.getString("manager_type_id"));
                        twp_id = (c.getString("twp_id"));
                        permit_id = (c.getString("permit_id"));
                        permit_type = (c.getString("permit_type"));
                        permit_name = (c.getString("permit_name"));
                        approved = (c.getString("approved"));
                        status = (c.getString("status"));
                        paid = (c.getString("paid"));
                        driver_proof = (c.getString("drv_lic_proof"));
                        residency_proof = (c.getString("residency_proof"));
                        signature = (c.getString("signature"));
                        User_comments = (c.getString("user_comments"));
                        Town_comments = (c.getString("town_comments"));
                        Logo = (c.getString("logo"));
                        First_contact_date = (c.getString("first_contact_date"));
                        rate = (c.getString("rate"));
                        Permit_nextnum = (c.getString("permit_num"));
                        Full_address = (c.getString("full_address"));
                        Full_name = (c.getString("full_name"));
                        email = (c.getString("email"));
                        phone = (c.getString("phone"));

                        permit_expires_on = (c.getString("permit_expires_on"));
                        //item.setExpired(c.getString("expired"));
                        Expires_by = (c.getString("permit_validity"));
                        Requirements = (c.getString("doc_requirements"));
                        Renewable = (c.getString("renewable"));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            if (getActivity() != null && json != null) {

                Picasso.with(getContext()).load(Logo).into(img_PrintLogo);

                if (!Full_name.equalsIgnoreCase("")) {
                    txt_ProfileDetails.setText("Name: " + Full_name + "\nPhone : " + phone + " Email : " + email
                            + "\nAddress : " + Full_address);
                }

                if (!User_comments.equalsIgnoreCase("")) {
                    txt_User_comments.setText(User_comments);
                    txt_User_comments1.setPaintFlags(txt_User_comments1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                }

                if (!Town_comments.equalsIgnoreCase("")) {
                    txt_Town_comments.setText(Town_comments);
                    txt_Town_comments1.setPaintFlags(txt_Town_comments1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                }

                txt_PERMIT.setText(township_code + "-" + permit_name);
                txt_ISSUED.setText(date_time);
                if (rate.equalsIgnoreCase("") || rate.equalsIgnoreCase("null") || rate.isEmpty()) {
                    txt_FEE.setText("$0");
                } else {
                    txt_FEE.setText("" + rate);
                }


                txt_ValidUntil.setText(Expires_by);
                txt_Requirements.setText(Requirements);

                if (!signature.equalsIgnoreCase("")) {
                    img_SignatureProof.setVisibility(View.VISIBLE);
                    Signature();
                }

                if (!residency_proof.equalsIgnoreCase("")) {
                    listResidency = new ArrayList<String>(Arrays.asList(residency_proof.split(",")));
                    Residence(0);
                }

                if (!driver_proof.equalsIgnoreCase("")) {
                    listDriver = new ArrayList<String>(Arrays.asList(driver_proof.split(",")));
                    Driver(0);
                }

                try {
                    String str = Permit_nextnum.substring(Permit_nextnum.lastIndexOf("-") + 1);
                    int Permitnextnum = Integer.parseInt(str) + 1;
                    Permit_nextnum = township_code + "-" + Permitnextnum;
                    Log.e("Permit nextnum : " + Permitnextnum, " : " + Permit_nextnum);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                Calendar getCurrent = Calendar.getInstance();
                String getDate = dateFormat.format(getCurrent.getTime());
                try {
                    Date pDate = dateFormat.parse(getDate);
                    if (Expires_by.equalsIgnoreCase("365 days")) {
                        Calendar c = Calendar.getInstance();
                        c.setTime(pDate);
                        c.add(Calendar.DATE, 365);
                        Date expDate = c.getTime();
                        Expires_Date = dateFormat1.format(expDate) + " 23:59:59";
                    } else if (Expires_by.equalsIgnoreCase("YearEnd")) {
                        Calendar c = Calendar.getInstance();
                        c.setTime(pDate);
                        Expires_Date = String.valueOf(c.get(Calendar.YEAR)) + "-12-31 23:59:59";
                    } else if (Expires_by.equalsIgnoreCase("MonthEnd")) {
                        Calendar c = Calendar.getInstance();
                        c.setTime(pDate);
                        int thisMonth = c.get(Calendar.MONTH) + 1;
                        Expires_Date = String.valueOf(c.get(Calendar.YEAR) + "-" + thisMonth
                                + "-" + c.getActualMaximum(Calendar.DATE)) + " 23:59:59";
                    } else if (Expires_by.equalsIgnoreCase("30 Days")) {
                        Calendar c = Calendar.getInstance();
                        c.setTime(pDate);
                        c.add(Calendar.DATE, 30);
                        Date expDate = c.getTime();
                        Expires_Date = dateFormat.format(expDate);
                    } else if (Expires_by.equalsIgnoreCase("7 Days")) {
                        Calendar c = Calendar.getInstance();
                        c.setTime(pDate);
                        c.add(Calendar.DATE, 7);
                        Date expDate = c.getTime();
                        Expires_Date = dateFormat.format(expDate);
                    } else if (Expires_by.equalsIgnoreCase("Weekend")) {
                        Calendar c = Calendar.getInstance();
                        c.setTime(pDate);

                        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                        if (Calendar.MONDAY == dayOfWeek) {
                            c.add(Calendar.DATE, 6);
                        } else if (Calendar.TUESDAY == dayOfWeek) {
                            c.add(Calendar.DATE, 5);
                        } else if (Calendar.WEDNESDAY == dayOfWeek) {
                            c.add(Calendar.DATE, 4);
                        } else if (Calendar.THURSDAY == dayOfWeek) {
                            c.add(Calendar.DATE, 3);
                        } else if (Calendar.FRIDAY == dayOfWeek) {
                            c.add(Calendar.DATE, 2);
                        } else if (Calendar.SATURDAY == dayOfWeek) {
                            c.add(Calendar.DATE, 1);
                        } else if (Calendar.SUNDAY == dayOfWeek) {
                            c.add(Calendar.DATE, 0);
                        }

                        int thisMonth = c.get(Calendar.MONTH) + 1;
                        Expires_Date = String.valueOf(c.get(Calendar.YEAR) + "-" + thisMonth
                                + "-" + c.get(Calendar.DAY_OF_MONTH)) + " 23:59:59";
                    } else if (Expires_by.equalsIgnoreCase("1 Days")) {
                        Calendar c = Calendar.getInstance();
                        c.setTime(pDate);
                        c.add(Calendar.DATE, 1);
                        Date expDate = c.getTime();
                        Expires_Date = dateFormat.format(expDate);
                    } else if (Expires_by.equalsIgnoreCase("24 Hours")) {
                        Calendar c = Calendar.getInstance();
                        c.setTime(pDate);
                        c.add(Calendar.DATE, 1);
                        Date expDate = c.getTime();
                        Expires_Date = dateFormat.format(expDate);
                    } else if (Expires_by.equalsIgnoreCase("Daily")) {
                        Calendar c = Calendar.getInstance();
                        c.setTime(pDate);
                        int thisMonth = c.get(Calendar.MONTH) + 1;
                        Expires_Date = String.valueOf(c.get(Calendar.YEAR) + "-" + thisMonth
                                + "-" + c.get(Calendar.DAY_OF_MONTH)) + " 23:59:59";
                    }
                    txt_ExpiryTime.setText(Expires_Date);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(mContext);
        String[] pictureDialogItems = {"File", "Camera", "Photo Gallery"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Intent i = new Intent(getContext(), FileActivity.class);
                                i.putExtra("isdoc", true);
                                startActivityForResult(i, 8);
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                            case 2:
                                openImagePickerIntent();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void openImagePickerIntent() {
        Intent mIntent = new Intent(mContext, PickImageActivity.class);
        mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 30);
        mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
        startActivityForResult(mIntent, PickImageActivity.PICKER_REQUEST_CODE);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    public void addItemSelect(final MultiSelection item) {
        listItemSelect.add(item);
        final View viewItemSelected = View.inflate(mContext, R.layout.piclist_item_selected_big, null);
        ImageView imageItem = (ImageView) viewItemSelected.findViewById(R.id.imageItem);
        ImageView btnDelete = (ImageView) viewItemSelected.findViewById(R.id.btnDelete);
        TextView txt_Name = (TextView) viewItemSelected.findViewById(R.id.txt_Name);
        txt_Name.setText(item.getF_Name());

        imageItem.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Glide.with(mContext).asBitmap().load(item.getF_Path()).placeholder(R.mipmap.docs).into(imageItem);

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutListItemSelect.removeView(viewItemSelected);
                listItemSelect.remove(item);
            }
        });

        layoutListItemSelect.addView(viewItemSelected);
        viewItemSelected.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.abc_fade_in));
        sendScroll();
    }

    public void addItemSelect1(final MultiSelection item) {
        listItemSelect1.add(item);
        final View viewItemSelected = View.inflate(mContext, R.layout.piclist_item_selected_big, null);
        ImageView imageItem = (ImageView) viewItemSelected.findViewById(R.id.imageItem);
        ImageView btnDelete = (ImageView) viewItemSelected.findViewById(R.id.btnDelete);
        TextView txt_Name = (TextView) viewItemSelected.findViewById(R.id.txt_Name);

        txt_Name.setText(item.getF_Name());
        imageItem.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Glide.with(mContext).asBitmap().load(item.getF_Path()).placeholder(R.mipmap.docs).into(imageItem);

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutListItemSelect1.removeView(viewItemSelected);
                listItemSelect1.remove(item);
            }
        });

        layoutListItemSelect1.addView(viewItemSelected);
        viewItemSelected.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.abc_fade_in));
        sendScroll1();
    }

    private void sendScroll() {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        horizontalScrollView.fullScroll(66);
                    }
                });
            }
        }).start();
    }

    private void sendScroll1() {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        horizontalScrollView1.fullScroll(66);
                    }
                });
            }
        }).start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            Uri tempUri = getImageUri(getActivity(), thumbnail);
            File finalFile = new File(getRealPathFromURI(tempUri));
            if (m_Open.equalsIgnoreCase("Residency")) {
                MultiSelection multiSelection = new MultiSelection();
                multiSelection.setF_Path(String.valueOf(finalFile));
                multiSelection.setF_Name(finalFile.getName());
                multiSelection.setF_Com("yes");
                addItemSelect(multiSelection);
            } else if (m_Open.equalsIgnoreCase("DriverLicense")) {
                MultiSelection multiSelection = new MultiSelection();
                multiSelection.setF_Path(String.valueOf(finalFile));
                multiSelection.setF_Name(finalFile.getName());
                multiSelection.setF_Com("yes");
                addItemSelect1(multiSelection);
            }
        }
        if (resultCode != RESULT_OK) {
            return;
        }
        if (resultCode == -1 && requestCode == PickImageActivity.PICKER_REQUEST_CODE) {
            pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (this.pathList != null && !this.pathList.isEmpty()) {
                for (int i = 0; i < pathList.size(); i++) {
                    if (m_Open.equalsIgnoreCase("Residency")) {
                        MultiSelection multiSelection = new MultiSelection();
                        multiSelection.setF_Path(String.valueOf(pathList.get(i)));
                        multiSelection.setF_Name(pathList.get(i).substring(pathList.get(i).lastIndexOf("/") + 1));
                        multiSelection.setF_Com("yes");
                        addItemSelect(multiSelection);
                    } else if (m_Open.equalsIgnoreCase("DriverLicense")) {
                        MultiSelection multiSelection = new MultiSelection();
                        multiSelection.setF_Path(String.valueOf(pathList.get(i)));
                        multiSelection.setF_Name(pathList.get(i).substring(pathList.get(i).lastIndexOf("/") + 1));
                        multiSelection.setF_Com("yes");
                        addItemSelect1(multiSelection);
                    }
                }
            }
        }
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 8) {

                pdfPath = data.getExtras().getString("path");
                File file = new File(pdfPath);

                long fileSizeInBytes = file.length();
                long fileSizeInKB = fileSizeInBytes / 1024;
                long fileSizeInMB = fileSizeInKB / 1024;

                if (fileSizeInKB > 2048) {
                    pfdURI = null;
                    Toast.makeText(getContext(), "Can not upload large scale document", Toast.LENGTH_LONG).show();
                } else {
                    if (m_Open.equalsIgnoreCase("Residency")) {
                        MultiSelection multiSelection = new MultiSelection();
                        multiSelection.setF_Path(pdfPath);
                        multiSelection.setF_Name(pdfPath.substring(pdfPath.lastIndexOf("/") + 1));
                        multiSelection.setF_Com("no");
                        addItemSelect(multiSelection);
                    } else if (m_Open.equalsIgnoreCase("DriverLicense")) {
                        MultiSelection multiSelection = new MultiSelection();
                        multiSelection.setF_Path(pdfPath);
                        multiSelection.setF_Name(pdfPath.substring(pdfPath.lastIndexOf("/") + 1));
                        multiSelection.setF_Com("no");
                        addItemSelect1(multiSelection);
                    } else if (m_Open.equalsIgnoreCase("PDF")) {
                        /*txt_pdfUpload.setVisibility(View.VISIBLE);
                        pfdURI = Uri.fromFile(new File(pdfPath));
                        txt_pdfUpload.setText(new File(pdfPath).getName());*/
                    }
                }
            }

        }

    }

    public Uri getImageUri(Activity inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public void Residence(int current_pos) {
        s_pos = current_pos;
        try {
            final File file_exists = new File(file_download_path, listResidency.get(s_pos));
            if (file_exists.exists()) {

                MultiSelection multiSelection = new MultiSelection();
                String extension = file_exists.getAbsolutePath().substring(file_exists.getAbsolutePath().lastIndexOf("."));
                if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg")
                        || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".bmp")) {
                    multiSelection.setF_Path(String.valueOf(file_exists.getAbsolutePath()));
                    multiSelection.setF_Name(file_exists.getName());
                    multiSelection.setF_Com("yes");
                } else {
                    multiSelection.setF_Path(String.valueOf(file_exists.getAbsolutePath()));
                    multiSelection.setF_Name(file_exists.getName());
                    multiSelection.setF_Com("no");
                }
                addItemSelect(multiSelection);

                arrayListResidency.add(file_exists.getAbsolutePath());
                horizontalAdapter.notifyDataSetChanged();
                if (listResidency.size() == s_pos + 1) {

                } else {
                    s_pos++;
                    Residence(s_pos);
                }
            } else {
                TransferObserver downloadObserver = transferUtility.download("parkezly-images", "/images/" + listResidency.get(s_pos), file_exists);
                downloadObserver.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (TransferState.COMPLETED == state) {
                            MultiSelection multiSelection = new MultiSelection();
                            String extension = file_exists.getAbsolutePath().substring(file_exists.getAbsolutePath().lastIndexOf("."));
                            if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg")
                                    || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".bmp")) {
                                multiSelection.setF_Path(String.valueOf(file_exists.getAbsolutePath()));
                                multiSelection.setF_Name(file_exists.getName());
                                multiSelection.setF_Com("yes");
                            } else {
                                multiSelection.setF_Path(String.valueOf(file_exists.getAbsolutePath()));
                                multiSelection.setF_Name(file_exists.getName());
                                multiSelection.setF_Com("no");
                            }
                            addItemSelect(multiSelection);
                            arrayListResidency.add(file_exists.getAbsolutePath());
                            horizontalAdapter.notifyDataSetChanged();
                            if (listResidency.size() == s_pos + 1) {

                            } else {
                                s_pos++;
                                Residence(s_pos);
                            }
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                        int percentDone = (int) percentDonef;
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        ex.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Driver(int current_pos) {
        d_pos = current_pos;
        try {
            final File file_exists = new File(file_download_path, listDriver.get(d_pos));
            if (file_exists.exists()) {
                MultiSelection multiSelection = new MultiSelection();
                String extension = file_exists.getAbsolutePath().substring(file_exists.getAbsolutePath().lastIndexOf("."));
                if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg")
                        || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".bmp")) {
                    multiSelection.setF_Path(String.valueOf(file_exists.getAbsolutePath()));
                    multiSelection.setF_Name(file_exists.getName());
                    multiSelection.setF_Com("yes");
                } else {
                    multiSelection.setF_Path(String.valueOf(file_exists.getAbsolutePath()));
                    multiSelection.setF_Name(file_exists.getName());
                    multiSelection.setF_Com("no");
                }
                addItemSelect1(multiSelection);
                arrayListDriver.add(file_exists.getAbsolutePath());
                driverAdapter.notifyDataSetChanged();
                if (listDriver.size() == d_pos + 1) {

                } else {
                    d_pos++;
                    Driver(d_pos);
                }
            } else {
                TransferObserver downloadObserver = transferUtility.download("parkezly-images", "/images/" + listDriver.get(d_pos), file_exists);

                downloadObserver.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (TransferState.COMPLETED == state) {
                            MultiSelection multiSelection = new MultiSelection();
                            String extension = file_exists.getAbsolutePath().substring(file_exists.getAbsolutePath().lastIndexOf("."));
                            if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg")
                                    || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".bmp")) {
                                multiSelection.setF_Path(String.valueOf(file_exists.getAbsolutePath()));
                                multiSelection.setF_Name(file_exists.getName());
                                multiSelection.setF_Com("yes");
                            } else {
                                multiSelection.setF_Path(String.valueOf(file_exists.getAbsolutePath()));
                                multiSelection.setF_Name(file_exists.getName());
                                multiSelection.setF_Com("no");
                            }
                            addItemSelect1(multiSelection);
                            arrayListDriver.add(file_exists.getAbsolutePath());
                            driverAdapter.notifyDataSetChanged();
                            if (listDriver.size() == d_pos + 1) {

                            } else {
                                d_pos++;
                                Driver(d_pos);
                            }
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                        int percentDone = (int) percentDonef;
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        ex.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Signature() {
        try {
            final File file_exists = new File(file_download_path, signature);
            if (file_exists.exists()) {
                Bitmap bmp = BitmapFactory.decodeFile(file_exists.getAbsolutePath());
                str_SignaturePath = file_exists.getAbsolutePath();
                mSignature = bmp;
                img_SignatureProof.setImageBitmap(bmp);
            } else {
                TransferObserver downloadObserver = transferUtility.download("parkezly-images",
                        "/images/" + signature, file_exists);

                downloadObserver.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (TransferState.COMPLETED == state) {
                            Bitmap bmp = BitmapFactory.decodeFile(file_exists.getAbsolutePath());
                            str_SignaturePath = file_exists.getAbsolutePath();
                            mSignature = bmp;
                            img_SignatureProof.setImageBitmap(bmp);
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                        int percentDone = (int) percentDonef;
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        ex.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {
        ArrayList<String> dataSet;
        Context mContext;

        public CustomAdapter(Context context, ArrayList<String> arrayListSignature) {
            mContext = context;
            dataSet = arrayListSignature;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView imageViewIcon;

            public MyViewHolder(View itemView) {
                super(itemView);
                this.imageViewIcon = (ImageView) itemView.findViewById(R.id.image);
            }
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapater_managepermit_items, parent, false);

            MyViewHolder myViewHolder = new MyViewHolder(view);
            return myViewHolder;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
            String extension = dataSet.get(listPosition).toString().substring(dataSet.get(listPosition).toString().lastIndexOf("."));
            if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg")
                    || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".bmp")) {
                Bitmap bmp = BitmapFactory.decodeFile(dataSet.get(listPosition));
                holder.imageViewIcon.setImageBitmap(bmp);
            } else {
                Bitmap bmp = BitmapFactory.decodeFile(dataSet.get(listPosition));
                holder.imageViewIcon.setImageDrawable(getResources().getDrawable(R.mipmap.docs));
            }
        }

        @Override
        public int getItemCount() {
            return dataSet.size();
        }
    }

    public class DriverAdapter extends RecyclerView.Adapter<DriverAdapter.MyViewHolder> {
        ArrayList<String> dataSet;
        Context mContext;

        public DriverAdapter(Context context, ArrayList<String> arrayListSignature) {
            mContext = context;
            dataSet = arrayListSignature;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView imageViewIcon;

            public MyViewHolder(View itemView) {
                super(itemView);
                this.imageViewIcon = (ImageView) itemView.findViewById(R.id.image);
            }
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapater_managepermit_items, parent, false);

            MyViewHolder myViewHolder = new MyViewHolder(view);
            return myViewHolder;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
            String extension = dataSet.get(listPosition).toString().substring(dataSet.get(listPosition).toString().lastIndexOf("."));
            if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg")
                    || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".bmp")) {
                Bitmap bmp = BitmapFactory.decodeFile(dataSet.get(listPosition));
                holder.imageViewIcon.setImageBitmap(bmp);
            } else {
                Bitmap bmp = BitmapFactory.decodeFile(dataSet.get(listPosition));
                holder.imageViewIcon.setImageDrawable(getResources().getDrawable(R.mipmap.docs));
            }
        }

        @Override
        public int getItemCount() {
            return dataSet.size();
        }
    }

    public class ImageCompress extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CompressItemSelect = new ArrayList();
            CompressItemSelect1 = new ArrayList();
            totla_size = 0;
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                for (int i = 0; i < listItemSelect.size(); i++) {
                    if (listItemSelect.get(i).getF_Com().equalsIgnoreCase("yes")) {
                        File ImageFile = new File(listItemSelect.get(i).getF_Path());
                        File compressedImageFile = null;
                        String strFile = "";
                        try {
                            compressedImageFile = new Compressor(mContext).compressToFile(ImageFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        strFile = String.valueOf(compressedImageFile);

                        MultiSelection multiSelection = new MultiSelection();
                        multiSelection.setF_Path(strFile);
                        multiSelection.setF_Name(strFile.substring(strFile.lastIndexOf("/") + 1));
                        multiSelection.setF_Com("yes");
                        CompressItemSelect.add(multiSelection);
                    } else {
                        MultiSelection multiSelection = new MultiSelection();
                        multiSelection.setF_Path(listItemSelect.get(i).getF_Path());
                        multiSelection.setF_Name(listItemSelect.get(i).getF_Name());
                        multiSelection.setF_Com("no");
                        CompressItemSelect.add(multiSelection);
                    }

                }
                totla_size = totla_size + CompressItemSelect.size();
                for (int i = 0; i < listItemSelect1.size(); i++) {
                    if (listItemSelect1.get(i).getF_Com().equalsIgnoreCase("yes")) {
                        File ImageFile = new File(listItemSelect1.get(i).getF_Path());
                        File compressedImageFile = null;
                        String strFile = "";
                        try {
                            compressedImageFile = new Compressor(mContext).compressToFile(ImageFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        strFile = String.valueOf(compressedImageFile);

                        MultiSelection multiSelection = new MultiSelection();
                        multiSelection.setF_Path(strFile);
                        multiSelection.setF_Name(strFile.substring(strFile.lastIndexOf("/") + 1));
                        multiSelection.setF_Com("yes");
                        CompressItemSelect1.add(multiSelection);
                    } else {
                        MultiSelection multiSelection = new MultiSelection();
                        multiSelection.setF_Path(listItemSelect1.get(i).getF_Path());
                        multiSelection.setF_Name(listItemSelect1.get(i).getF_Name());
                        multiSelection.setF_Com("no");
                        CompressItemSelect1.add(multiSelection);
                    }
                }
                totla_size = totla_size + CompressItemSelect1.size();
                if (btn_EditSignature.getText().toString().equalsIgnoreCase("Edit")) {

                } else if (btn_EditSignature.getText().toString().equalsIgnoreCase("Cancel")) {
                    if (mClearButton.isEnabled()) {
                        totla_size = totla_size + 1;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (getActivity() != null && totla_size == 0) {
                new getPermit_Subscription().execute();
            } else {
                upload_file();
            }
        }
    }

    public void upload_file() {
        for (int i = 0; i < CompressItemSelect.size(); i++) {
            File file = new File(CompressItemSelect.get(i).getF_Path());
            String namegsxsax = CompressItemSelect.get(i).getF_Name();
            file_Residency.append(namegsxsax + ",");
            namegsxsax = "/images/" + namegsxsax;
            final_Residency = file_Residency.substring(0, file_Residency.length() - 1);
            Log.e("namegsxsax " + namegsxsax, "  : " + final_Residency);
            TransferObserver transferObserver = transferUtility.upload("parkezly-images", namegsxsax, file);
            transferObserverListener(transferObserver);
        }

        for (int i = 0; i < CompressItemSelect1.size(); i++) {
            File file = new File(CompressItemSelect1.get(i).getF_Path());
            String namegsxsax = CompressItemSelect1.get(i).getF_Name();
            file_Driver.append(namegsxsax + ",");
            namegsxsax = "/images/" + namegsxsax;
            final_Driver = file_Driver.substring(0, file_Driver.length() - 1);
            TransferObserver transferObserver = transferUtility.upload("parkezly-images", namegsxsax, file);
            transferObserverListener(transferObserver);
        }

        if (btn_EditSignature.getText().toString().equalsIgnoreCase("Edit")) {
            final_SignaturePad = signature;
        } else if (btn_EditSignature.getText().toString().equalsIgnoreCase("Cancel")) {
            if (mClearButton.isEnabled()) {
                for (int i = 0; i < 1; i++) {
                    File file = photo;
                    String namegsxsax = System.currentTimeMillis() + ".jpg";
                    file_SignaturePad.append(namegsxsax + ",");
                    namegsxsax = "/images/" + namegsxsax;
                    final_SignaturePad = file_SignaturePad.substring(0, file_SignaturePad.length() - 1);
                    TransferObserver transferObserver = transferUtility.upload("parkezly-images", namegsxsax, file);
                    transferObserverListener(transferObserver);
                }
            }
        }

    }

    public void transferObserverListener(final TransferObserver transferObserver) {
        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.name().equals("COMPLETED")) {
                    current_upload++;
                    if (totla_size == current_upload) {
                        new getPermit_Subscription().execute();
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                try {
                    int percentage = (int) (bytesCurrent / bytesTotal * 100);
                    Log.e("percentage ", " : " + percentage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", "error");
            }

        });
    }

    public class getPermit_Subscription extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String responseStr = null;
        String managedlosturl = "_table/permit_subscription";
        Date current;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            //


            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            String currentdate = sdf.format(new Date());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            try {
                current = format.parse(currentdate);
                Log.e("current_date", String.valueOf(current));
                System.out.println(current);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("id", id);
            jsonValues1.put("user_id", user_id);
            jsonValues1.put("manager_type", manager_type);
            jsonValues1.put("manager_type_id", manager_type_id);
            jsonValues1.put("twp_id", twp_id);
            jsonValues1.put("permit_id", permit_id);
            jsonValues1.put("permit_num", Permit_nextnum);
            jsonValues1.put("full_address", Full_address);
            jsonValues1.put("full_name", Full_name);
            jsonValues1.put("email", email);
            jsonValues1.put("phone", phone);
            jsonValues1.put("date_time", current);
            jsonValues1.put("date_action", current);
            jsonValues1.put("township_code", township_code);
            jsonValues1.put("township_name", township_name);
            jsonValues1.put("permit_type", permit_type);
            jsonValues1.put("permit_name", permit_name);
            if (CompressItemSelect.size() > 0) {
                jsonValues1.put("residency_proof", final_Residency);
            } else {
                jsonValues1.put("residency_proof", "");
            }
            jsonValues1.put("approved", "0");
            jsonValues1.put("status", "0");
            jsonValues1.put("paid", "0");
            jsonValues1.put("user_comments", "");
            jsonValues1.put("town_comments", "");
            if (CompressItemSelect1.size() > 0) {
                jsonValues1.put("drv_lic_proof", final_Driver);
            } else {
                jsonValues1.put("drv_lic_proof", "");
            }
            jsonValues1.put("logo", Logo);
            jsonValues1.put("scheme_type", "SUBSCRIPTION");
            jsonValues1.put("first_contact_date", current);
            jsonValues1.put("permit_status_image", "");
            jsonValues1.put("rate", rate);
            jsonValues1.put("user_name", user_id);
            if (mClearButton.isEnabled()) {
                jsonValues1.put("signature", final_SignaturePad);
            } else {
                jsonValues1.put("signature", "");
            }
            jsonValues1.put("permit_validity", Expires_by);
            jsonValues1.put("permit_expires_on", Expires_Date);
            jsonValues1.put("doc_requirements", Requirements);
            jsonValues1.put("app_pdf_upload", "");
            jsonValues1.put("expired", "0");
            jsonValues1.put("renewable", Renewable);


            JSONObject json1 = new JSONObject(jsonValues1);
            Log.e("json1", " : " + json1.toString());
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("responseStr ", " : " + responseStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return responseStr;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && responseStr != null) {
                boolean errors = responseStr.indexOf("error") > 0;
                if (errors) {
                    Toast.makeText(getContext(), "Already exists...", Toast.LENGTH_LONG).show();
                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    MyPermits_VehicleUser pay = new MyPermits_VehicleUser();
                    ft.add(R.id.My_Container_1_ID, pay);
                    ft.commitAllowingStateLoss();

                } else {
                    Toast.makeText(getContext(), "Successfully added", Toast.LENGTH_LONG).show();
                    try {
                        JSONObject jsonObject = new JSONObject(responseStr);
                        JSONArray jsonArray = jsonObject.getJSONArray("resource");
                        String jsonObject1 = jsonArray.getJSONObject(0).getString("id");
                        Log.e("jsonObject1", " : " + jsonObject1);
                        new getPermit_Subscription_Archive(jsonObject1).execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public class getPermit_Subscription_Archive extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String responseStr = null, permit_sbscription_id;
        String managedlosturl = "_table/permit_subscription_archive";
        Date current;

        public getPermit_Subscription_Archive(String jsonObject1) {
            permit_sbscription_id = jsonObject1;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            String currentdate = sdf.format(new Date());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            try {
                current = format.parse(currentdate);
                Log.e("current_date", String.valueOf(current));
                System.out.println(current);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("id", id);
            jsonValues1.put("user_id", user_id);
            jsonValues1.put("manager_type", manager_type);
            jsonValues1.put("twp_id", twp_id);
            jsonValues1.put("permit_id", permit_id);
            jsonValues1.put("permit_num", Permit_nextnum);
            jsonValues1.put("full_address", Full_address);
            jsonValues1.put("full_name", Full_name);
            jsonValues1.put("email", email);
            jsonValues1.put("phone", phone);
            jsonValues1.put("date_time", current);
            jsonValues1.put("date_action", current);
            jsonValues1.put("township_code", township_code);
            jsonValues1.put("township_name", township_name);
            jsonValues1.put("permit_type", permit_type);
            jsonValues1.put("permit_name", permit_name);
            if (CompressItemSelect.size() > 0) {
                jsonValues1.put("residency_proof", final_Residency);
            } else {
                jsonValues1.put("residency_proof", "");
            }
            jsonValues1.put("approved", "0");
            jsonValues1.put("status", "0");
            jsonValues1.put("paid", "0");
            jsonValues1.put("user_comments", "");
            jsonValues1.put("town_comments", "");
            if (CompressItemSelect1.size() > 0) {
                jsonValues1.put("drv_lic_proof", final_Driver);
            } else {
                jsonValues1.put("drv_lic_proof", "");
            }
            jsonValues1.put("logo", Logo);
            jsonValues1.put("scheme_type", "SUBSCRIPTION");
            jsonValues1.put("first_contact_date", current);
            jsonValues1.put("permit_status_image", "");
            jsonValues1.put("rate", rate);
            jsonValues1.put("user_name", user_id);
            if (mClearButton.isEnabled()) {
                jsonValues1.put("signature", final_SignaturePad);
            } else {
                jsonValues1.put("signature", "");
            }
            jsonValues1.put("permit_validity", Expires_by);
            jsonValues1.put("permit_expires_on", Expires_Date);
            jsonValues1.put("doc_requirements", Requirements);
            jsonValues1.put("app_pdf_upload", "");
            jsonValues1.put("expired", "0");
            jsonValues1.put("renewable", Renewable);

            JSONObject json1 = new JSONObject(jsonValues1);
            Log.e("json1", " : " + json1.toString());
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("responseStr ", " : " + responseStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return responseStr;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && responseStr != null) {
                boolean errors = responseStr.indexOf("error") > 0;
                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                MyPermits_VehicleUser pay = new MyPermits_VehicleUser();
                ft.add(R.id.My_Container_1_ID, pay);
                ft.commitAllowingStateLoss();
            }
        }
    }

    public void backticket() {
        getActivity().onBackPressed();
    }

}
