package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.softmeasures.eventezlyadminplus.ItemAdapter;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.Main_Activity;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.CustomDateTimePicker;
import com.softmeasures.eventezlyadminplus.java.item;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static android.content.Context.MODE_PRIVATE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class create_ticket_main extends Fragment implements frg_violation_list.list_item_click_viloation, frg_hearing_list.list_item_click_hearing, ItemAdapter.ItemListener
        , frg_proof_add.click_done_and_back {
    TextView txt_violation_code, txt_violation_fess, txt_violation_address, txt_violation_type, txt_hearinf_type, textty1, txt_crweateticket1, txt_court_id, txt_hearing_date, txt_finalticket, txt_final_create_ticket;
    ListView list_of_violation_type, list_of_hearing_location;
    adpterviolationtype ad;
    RelativeLayout rl_progressbar, rl_hearing_date, rl_priviewticket;
    String marker = "", platno, statename, lat, lang, row = "", row_no = "", township_code, violation_code, violation_desc, hearing_address, violation_fee, hearing_location, court_id, user_id, status, heraing_date, parking_type1;
    ArrayList<item> violationtype = new ArrayList<item>();
    ArrayList<item> hearingtype = new ArrayList<item>();
    EditText txt_hearing_address, txt_hearing_location;
    ArrayList<item> date_list = new ArrayList<>();
    TextView txt_hearing_mutiple;
    private ItemAdapter mAdapter;
    BottomSheetBehavior behavior;
    CoordinatorLayout CoordinatorLayout;
    private BottomSheetDialog mBottomSheetDialog;
    CustomDateTimePicker custom;
    String expire, entry, datew;
    ImageView imgmarker;
    TextView txtplantno, txtstatename, txt_row, txt_proof_detail;
    ArrayList<item> township_array_list = new ArrayList<>();
    TextView txt_preview;
    String township_logo_url = "";
    String currentdate = "", expridate = "", ticket_id = "", create_ticket_date;
    String lot_row, expiry_time, marker_state, subscription_id, ip, ipn_status, lot_number, ipn_txn_id, pl_state, parking_token, ipn_custom, parking_units, ipn_payment, tr_percent, zip, parking_rate, tr_fee, ipn_address, marker_address1, parking_qty, city, parking_type, wallet_trx_id, country, plate_no, marker_zip, location_code, exit_date_time, parking_total, payment_method, marker_address2, address2, ticket_status, max_time, permit_id, marker_city, parking_status, marker_country, entry_date_time, parking_subtotal, address1, state, lng1, lat1, user_id1 = "", Officer_id = "", email = "", selected_address, finalDate = "", parking_id = "0", final_township_code = "", location_id, platform, pl_country, parked_address;
    TextView txt_township_name;
    ImageView img_logo, img_qrcode, img_main_logo;
    int distance_to_marker, id1;
    StringBuffer file_name = new StringBuffer();
    int totla_size_of_body = 0, current_file_upload_position = 0;
    String recording_name = "", comment = "", final_final_name;
    AmazonS3 s3;
    TransferUtility transferUtility;
    String timeStamp = "";
    RelativeLayout rl_main_view;
    TextView txt_address;
    boolean vehicle_found = false;
    private String townshipCode = "";
    private double tr_fee1, tr_percentage1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_create_ticket, container, false);
        txt_violation_type = (TextView) view.findViewById(R.id.txt_type);
        list_of_hearing_location = (ListView) view.findViewById(R.id.list_hearing);
        list_of_violation_type = (ListView) view.findViewById(R.id.list_violationtype);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        txt_violation_code = (TextView) view.findViewById(R.id.txt_violation_code);
        txt_violation_fess = (TextView) view.findViewById(R.id.txt_violation_free);
        txt_violation_address = (TextView) view.findViewById(R.id.txt_description);
        txt_hearing_address = (EditText) view.findViewById(R.id.txt_hearing_address1);
        txt_hearing_location = (EditText) view.findViewById(R.id.txt_hearinglocation);
        txt_court_id = (TextView) view.findViewById(R.id.txt_couty_id);
        txt_violation_type = (TextView) view.findViewById(R.id.txt_type);
        txt_hearinf_type = (TextView) view.findViewById(R.id.txt_location);
        txt_hearing_mutiple = (TextView) view.findViewById(R.id.txt_hearing_location_mutiple);
        rl_hearing_date = (RelativeLayout) view.findViewById(R.id.rl_hearing_date);
        txt_hearing_date = (TextView) view.findViewById(R.id.txt_date);
        imgmarker = (ImageView) view.findViewById(R.id.imgmarker);
        textty1 = (TextView) view.findViewById(R.id.textty1);
        txt_row = (TextView) view.findViewById(R.id.txtrow);
        txtplantno = (TextView) view.findViewById(R.id.txtplatno);
        txtstatename = (TextView) view.findViewById(R.id.txtstatename);
        txt_proof_detail = (TextView) view.findViewById(R.id.txt_proof_deatil);
        txt_preview = (TextView) view.findViewById(R.id.txt_crweateticket1);
        RelativeLayout rl_show_violation_code = (RelativeLayout) view.findViewById(R.id.rl_violationshowdata);
        RelativeLayout rl_add_proof = (RelativeLayout) view.findViewById(R.id.rl_add_proof);
        txt_township_name = (TextView) view.findViewById(R.id.txt_title_of_print);
        txt_finalticket = (TextView) view.findViewById(R.id.text);
        txt_final_create_ticket = (TextView) view.findViewById(R.id.txt_crweateticket2);
        img_logo = (ImageView) view.findViewById(R.id.sd_image);
        img_qrcode = (ImageView) view.findViewById(R.id.img_qrcode);
        img_main_logo = (ImageView) view.findViewById(R.id.img_logo);
        rl_priviewticket = (RelativeLayout) view.findViewById(R.id.rl_priviewticket);
        rl_main_view = (RelativeLayout) view.findViewById(R.id.tt);
        txt_address = (TextView) view.findViewById(R.id.txtaddress);
        s3 = new AmazonS3Client(new BasicAWSCredentials("AKIAJNG22KFRVUAICSEA", "NSrQR/yAg52RPD3kp3FMb3NO+Vrxuty4iAwPU4th"));
        transferUtility = new TransferUtility(s3, getActivity().getApplicationContext());
        txt_violation_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                frg_violation_list pay = new frg_violation_list();
                pay.registerForListener(create_ticket_main.this);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        final ImageView img_sd = (ImageView) view.findViewById(R.id.sd_image);

        rl_show_violation_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(view);
                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                frg_violation_list pay = new frg_violation_list();
                pay.registerForListener(create_ticket_main.this);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        rl_add_proof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(view);
                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                frg_proof_add pay = new frg_proof_add();
                pay.registerForListener(create_ticket_main.this);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            townshipCode = bundle.getString("townshipCode", "");
            txtplantno.setText(bundle.getString("plate"));
            txtstatename.setText(bundle.getString("state"));
            String row = bundle.getString("row");
            String row_no = bundle.getString("row_no");
            String is_direct = bundle.getString("is_direct");
            String parked_ad = bundle.getString("parked_car_address");
            if (row.equals("null") || row.equals("")) {
                txt_row.setVisibility(View.GONE);
            } else {
                txt_row.setVisibility(View.VISIBLE);
                txt_row.setText("Row - " + row + row_no);
            }

            marker = bundle.getString("marker");

            if (is_direct.equals("no")) {
                vehicle_found = true;
                rl_main_view.setBackgroundColor(Color.parseColor("#ffffff"));
                txt_address.setVisibility(View.GONE);
                img_sd.setVisibility(View.VISIBLE);
                txt_township_name.setVisibility(View.VISIBLE);
                SharedPreferences sh = getActivity().getSharedPreferences("streetview", MODE_PRIVATE);
                platno = sh.getString("title", "null");
                statename = sh.getString("statename", "null");
                marker = sh.getString("marker", "null");
                lat = sh.getString("la", "null");
                lang = sh.getString("lang", "null");
                row = sh.getString("lot_row", "null");
                row_no = sh.getString("lot_row_no", "null");
                expire = sh.getString("exiry", "null");
                entry = sh.getString("entry", "null");
                status = sh.getString("status", "null");
                parking_type1 = sh.getString("parking_type", "null");
                String max = sh.getString("max_time", "0");
                parking_id = sh.getString("id", "");
                final_township_code = sh.getString("township_code", "");
                selected_address = sh.getString("selected_address", "null");
                parked_address = sh.getString("parked_address", "null");
            } else {
                vehicle_found = false;
                rl_main_view.setBackgroundColor(Color.parseColor("#c2c2c2"));
                txt_address.setVisibility(View.VISIBLE);
                txt_address.setText(parked_ad);
                address1 = "N/A";
                selected_address = "N/A";
                img_sd.setVisibility(View.GONE);
                txt_township_name.setVisibility(View.GONE);
                platno = bundle.getString("plat1");
                statename = bundle.getString("state1");
                parked_address = parked_ad;
                marker_address1 = parked_ad;
            }
        }

        parked_address = parked_address != null ? (!parked_address.equals("null") ? (!parked_address.equals("") ? parked_address : "") : "") : "";
        if (marker.equals("status-green")) {
            imgmarker.setBackgroundResource(R.mipmap.popup_car_green);
        } else if (marker.equals("status-red")) {
            imgmarker.setBackgroundResource(R.mipmap.popup_car_red);

        } else if (marker.equals("status-yellow")) {
            imgmarker.setBackgroundResource(R.mipmap.car_yellow_pin);
        }

        View bottomSheet = view.findViewById(R.id.ll_hearing_datetime);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });
        CoordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinatorLayout);

        txt_hearing_mutiple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                frg_hearing_list pay = new frg_hearing_list();
                pay.registerForListener(create_ticket_main.this);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        rl_hearing_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // custom.showDialog();

                showBottomSheetDialog();

            }
        });

        new gettownship_list().execute();


        final TimeZone zone12 = TimeZone.getTimeZone("UTC");


        txt_preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(view);
                if (txt_violation_address.getText().toString().equals("") || txt_violation_code.getText().toString().equals("") || txt_violation_fess.getText().toString().equals("") ||
                        txt_hearing_address.getText().toString().equals("") || txt_hearing_location.getText().toString().equals("") || txt_hearing_date.getText().toString().equals("")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Parkezly Alert!");
                    alertDialog.setMessage("Please select Violation Type and Hearing Location both, in order to Create Ticket.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                } else {

                    rl_priviewticket.setVisibility(View.VISIBLE);
                    violation_code = txt_violation_code.getText().toString();
                    violation_desc = txt_violation_address.getText().toString();
                    String violation_fee1 = txt_violation_fess.getText().toString();
                    violation_fee = violation_fee1.substring(violation_fee1.indexOf(' ') + 1);
                    hearing_address = txt_hearing_address.getText().toString();
                    hearing_location = txt_hearing_location.getText().toString();
                    heraing_date = txt_hearing_date.getText().toString();
                    court_id = txt_court_id.getText().toString();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                    // TimeZone zone = TimeZone.getTimeZone("UTC");
                    //sdf.setTimeZone(zone);
                    currentdate = sdf.format(new Date());
                    final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

                    final String currentDateandTime = currentdate;
                    Date date11 = null;
                    try {
                        date11 = sdf1.parse(currentDateandTime);
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    final Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date11);
                    calendar.add(Calendar.MONTH, 1);
                    expridate = sdf.format(calendar.getTime());
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                    Date myDate = null;
                    try {
                        try {
                            myDate = dateFormat.parse(heraing_date);
                        } catch (java.text.ParseException e) {
                            e.printStackTrace();
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (township_array_list.size() > 0) {
                        for (int i = 0; i < township_array_list.size(); i++) {
                            String township_code = township_array_list.get(i).getTitle();
                            if (township_code.equals(final_township_code)) {
                                township_logo_url = township_array_list.get(i).getOfficial_logo();
                                String township_name = township_array_list.get(i).getLot_manager();
                                txt_township_name.setText(township_name);
                                break;
                            }
                        }
                        if (!township_logo_url.equals("")) {
                            Picasso.with(getActivity()).load(township_logo_url).resize(100, 100).centerCrop().into(img_sd, new Callback() {
                                @Override
                                public void onSuccess() {
                                    Log.e("township_logo", "yes");
                                }

                                @Override
                                public void onError() {
                                    Log.e("township_logo", "no");
                                }
                            });
                            //  Glide.with(getActivity()).load(township_logo_url).into(img_sd);
                        }
                    }
                    create_ticket_date = currentdate;
                    SimpleDateFormat timeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
                    finalDate = timeFormat.format(myDate);


                    String ticketprive = "Ticket Date: " + create_ticket_date + "\n\nTicket Number: " + ticket_id + "\n\nViolation Fee: " + violation_fee + "\n\nViolation Description: " + violation_desc + "\n\nViolation Details: " + violation_desc + "\n\nRespond By Date: " + expridate + "\n\nHearing Location: " + hearing_location + "\n\nHearing Date: " + finalDate + "\n\nPlate # :" + platno + "\n\nState: " + statename + "\n\nParked Address: " + parked_address + "\n\nSelected Address: " + selected_address + "\n\nLocation Name: ";
                    txt_finalticket.setText(ticketprive);
                }
            }
        });

        txt_final_create_ticket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(view);
                show_officer_id(getActivity());
            }
        });

        new fetchServideChargesByTownship().execute();

        return view;
    }

    @Override
    public void on_violation(String code, String fess, String des) {
        txt_violation_code.setText(code);
        txt_violation_fess.setText(fess);
        txt_violation_address.setText(des);
        new getviolationtype().execute();
    }


    @Override
    public void onItemClick(item item) {
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public void on_hearing(String code, String fess, String des, String date, boolean is_multiple, ArrayList<item> data) {

    }

    @Override
    public void onclick_done(String show_data) {
        txt_proof_detail.setText(show_data);
        getActivity().onBackPressed();
    }

    public class adpterviolationtype extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public adpterviolationtype(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;


            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {

                vi = inflater.inflate(R.layout.adapaterviolationtype, null);
                holder = new ViewHolder();

                holder.txtvilationcode = (TextView) vi.findViewById(R.id.txt_violation_code);
                holder.txtviocationdescription = (TextView) vi.findViewById(R.id.txt_violation_descripetion);
                holder.txtfess = (TextView) vi.findViewById(R.id.txt_violation_fees);

                vi.setTag(holder);
                vi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences gg = getActivity().getSharedPreferences("violation", MODE_PRIVATE);
                        SharedPreferences.Editor d = gg.edit();
                        d.clear();
                        d.commit();
                        String code = holder.txtvilationcode.getText().toString();
                        String fess = holder.txtfess.getText().toString();
                        String des = holder.txtviocationdescription.getText().toString();
                        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.sidepannelleft);
                       /* rl_list_violation_type.startAnimation(animation);
                        rl_list_violation_type.setVisibility(View.GONE);
                        rl_violation.setVisibility(View.VISIBLE);
                        txt_violation_code.setText(code);
                        txt_violation_fess.setText(fess);
                        txt_violation_address.setText(des);
                        rl_hearing.setVisibility(View.GONE);
                        txt_hearing_address.setText("");
                        txt_hearing_location.setText("");
                        new gethearing().execute();*/
                    }
                });
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {

            } else {

                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;

                Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvetica-normal.ttf");
                Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvetica-neue-medium.ttf");

                holder.txtvilationcode.setTypeface(medium);
                holder.txtfess.setTypeface(medium);

                if (tempValues.getVioloation_code().equals("null")) {
                    holder.txtvilationcode.setText("Code: ");
                }
                if (tempValues.getViolation_fess().equals("null")) {
                    holder.txtfess.setText("Fee: ");

                }
                if (tempValues.getViolation_desciprtion().equals("null")) {

                    holder.txtviocationdescription.setText("");
                }

                if (!tempValues.getVioloation_code().equals("null") && !tempValues.getViolation_desciprtion().equals("null") && !tempValues.getViolation_fess().equals("null")) {
                    holder.txtvilationcode.setText("Code: " + tempValues.getVioloation_code());
                    holder.txtfess.setText("Fee: " + tempValues.getViolation_fess());
                    holder.txtviocationdescription.setText(tempValues.getViolation_desciprtion());
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txtvilationcode, txtviocationdescription, txtfess;
        }

    }


    public class getviolationtype extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONObject json, json1;
        String transactionurl = "_table/township_users?filter=user_id%3D" + id + "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            Log.e("vilation_type", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray googleparking = json.getJSONArray("resource");

                    for (int j = 0; j < googleparking.length(); j++) {
                        JSONObject c = googleparking.getJSONObject(j);
                        township_code = c.getString("township_code");
                        String email = c.getString("email");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs;
            if (getActivity() != null && json != null) {

                new gethearing().execute();

            }

        }
    }


    public class gethearing extends AsyncTask<String, String, String> {

        JSONObject json, json1;
        String transactionurl = "_table/hearing_place_info?filter=township_code%3D" + township_code + "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            hearingtype.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            DefaultHttpClient client = new DefaultHttpClient();
            Log.e("hearing_place", url);
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray googleparking = json.getJSONArray("resource");

                    for (int j = 0; j < googleparking.length(); j++) {
                        item item = new item();

                        JSONObject c = googleparking.getJSONObject(j);
                        String location = c.getString("hearing_location");
                        String address = c.getString("hearing_address");
                        String id = c.getString("court_id");
                        String date = c.getString("first_court_date_time");
                        item.setHearing_address(address);
                        item.setHearing_location(location);
                        item.setCourt_id(id);
                        item.setDate(date);
                        item.setSecond_court_date_time(c.getString("second_court_date_time"));
                        item.setThird_court_date_time(c.getString("third_court_date_time"));
                        item.setFourth_court_date_time(c.getString("fourth_court_date_time"));
                        item.setFifth_court_date_time(c.getString("fifth_court_date_time"));
                        item.setSixth_court_date_time(c.getString("sixth_court_date_time"));
                        item.setSeventh_court_date_time(c.getString("seventh_court_date_time"));
                        item.setEighth_court_date_time(c.getString("eighth_court_date_time"));
                        item.setNinth_court_date_time(c.getString("ninth_court_date_time"));
                        item.setTenth_court_date_time(c.getString("tenth_court_date_time"));
                        item.setEleventh_court_date_time(c.getString("eleventh_court_date_time"));
                        item.setTwelfth_court_date_time(c.getString("twelfth_court_date_time"));
                        hearingtype.add(item);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity != null) {
                    Log.e("hearing", "yes");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                    String currentDateandTime = sdf.format(new Date());

                    if (hearingtype.size() > 0) {
                        if (hearingtype.size() == 1) {
                            SharedPreferences gg = getActivity().getSharedPreferences("vehicleinfo", MODE_PRIVATE);
                            SharedPreferences.Editor d = gg.edit();
                            d.clear();
                            d.commit();
                            String address = hearingtype.get(0).getHearing_address();
                            String location = hearingtype.get(0).getHearing_location();
                            String des = hearingtype.get(0).getCourt_id();
                            String date = hearingtype.get(0).getDate();
                            date_list.clear();
                            date_list.add(hearingtype.get(0));
                            txt_hearing_date.setText(date);
                            setdata(date_list);
                           /* setdata(date_list);
                            Animation animation = AnimationUtils.loadAnimation(getActivity(),
                                    R.anim.sidepannelleft);
                            rl_list_hearing.startAnimation(animation);
                            rl_list_violation_type.setVisibility(View.GONE);
                            rl_list_hearing.setVisibility(View.GONE);
                            rl_hearing.setVisibility(View.VISIBLE);
                            txt_hearing_location.setText(location);
                            txt_hearing_address.setText(address);
                            txt_court_id.setText(des);
                            txt_hearing_mutiple.setVisibility(View.GONE);*/

                            txt_hearing_location.setText(location);
                            txt_hearing_address.setText(address);
                            txt_court_id.setText(des);
                            txt_hearing_mutiple.setVisibility(View.GONE);
                        } else {
                            txt_hearing_mutiple.setVisibility(View.VISIBLE);
                           /* rl_hearing.setVisibility(View.VISIBLE);
                            txt_hearing_mutiple.setVisibility(View.VISIBLE);
                            txt_hearing_mutiple.setText("Please select");*/
                        }
                    }
                }
            }
        }


    }

    public void setdata(ArrayList<item> dfaya) {
        mAdapter = new ItemAdapter(dfaya, this);
//        recyclerView.setAdapter(mAdapter);
    }


    private void showBottomSheetDialog() {

        CoordinatorLayout.setVisibility(View.VISIBLE);
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        ArrayList<String> hh = new ArrayList<>();
        for (int h = 0; h < date_list.size(); h++) {
            String date1 = date_list.get(h).getDate();
            String date2 = date_list.get(h).getSecond_court_date_time();
            String date3 = date_list.get(h).getThird_court_date_time();
            String date4 = date_list.get(h).getFourth_court_date_time();
            String date5 = date_list.get(h).getFifth_court_date_time();
            String date6 = date_list.get(h).getSixth_court_date_time();
            String date7 = date_list.get(h).getSeventh_court_date_time();
            String date8 = date_list.get(h).getEighth_court_date_time();
            String date9 = date_list.get(h).getNinth_court_date_time();
            String date10 = date_list.get(h).getTenth_court_date_time();
            String date11 = date_list.get(h).getEleventh_court_date_time();
            String date12 = date_list.get(h).getTwelfth_court_date_time();


            date1 = date1 != null ? (!date1.equals("null") ? (!date1.equals("") ? date1 : "") : "") : "";
            date2 = date2 != null ? (!date2.equals("null") ? (!date2.equals("") ? date2 : "") : "") : "";
            date3 = date3 != null ? (!date3.equals("null") ? (!date3.equals("") ? date3 : "") : "") : "";
            date4 = date4 != null ? (!date4.equals("null") ? (!date4.equals("") ? date4 : "") : "") : "";
            date5 = date5 != null ? (!date5.equals("null") ? (!date5.equals("") ? date5 : "") : "") : "";
            date6 = date6 != null ? (!date6.equals("null") ? (!date6.equals("") ? date6 : "") : "") : "";
            date7 = date7 != null ? (!date7.equals("null") ? (!date7.equals("") ? date7 : "") : "") : "";
            date8 = date8 != null ? (!date8.equals("null") ? (!date8.equals("") ? date8 : "") : "") : "";
            date9 = date9 != null ? (!date9.equals("null") ? (!date9.equals("") ? date9 : "") : "") : "";
            date10 = date10 != null ? (!date10.equals("null") ? (!date10.equals("") ? date10 : "") : "") : "";
            date11 = date11 != null ? (!date11.equals("null") ? (!date11.equals("") ? date11 : "") : "") : "";
            date12 = date12 != null ? (!date12.equals("null") ? (!date12.equals("") ? date12 : "") : "") : "";


            if (!date1.equals("")) {
                hh.add(date1);
            }
            if (!date2.equals("")) {
                hh.add(date2);
            }

            if (!date3.equals("")) {
                hh.add(date3);
            }

            if (!date4.equals("")) {
                hh.add(date4);
            }

            if (!date5.equals("")) {
                hh.add(date5);
            }
            if (!date6.equals("")) {
                hh.add(date6);
            }

            if (!date7.equals("")) {
                hh.add(date7);
            }

            if (!date8.equals("")) {
                hh.add(date8);
            }

            if (!date9.equals("")) {
                hh.add(date9);
            }

            if (!date10.equals("")) {
                hh.add(date10);
            }

            if (!date11.equals("")) {
                hh.add(date11);
            }

            if (!date12.equals("")) {
                hh.add(date12);
            }
        }
        ArrayList<item> data = new ArrayList<>();
        for (int i = 0; i < hh.size(); i++) {
            item ii = new item();
            ii.setDate(hh.get(i).toString());
            data.add(ii);
        }
        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.sheet, null);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new ItemAdapter(data, new ItemAdapter.ItemListener() {
            @Override
            public void onItemClick(item item) {
                if (mBottomSheetDialog != null) {
                    mBottomSheetDialog.dismiss();
                    txt_hearing_date.setText(item.getDate());
                    //Toast.makeText(getActivity(),item.getDate(),Toast.LENGTH_LONG).show();
                }
            }
        }));

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                CoordinatorLayout.setVisibility(View.GONE);
                mBottomSheetDialog = null;
            }
        });
    }


    public class gettownship_list extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String parkingurl = "_table/townships_manager";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            township_array_list.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setTitle(c.getString("manager_id"));
                        item1.setManager_type(c.getString("manager_type"));
                        item1.setLot_manager(c.getString("lot_manager"));
                        item1.setAddress(c.getString("address"));
                        item1.setState(c.getString("state"));
                        item1.setCity(c.getString("city"));
                        item1.setCountry(c.getString("country"));
                        item1.setZip_code(c.getString("zip"));
                        item1.setTownship_logo(c.getString("township_logo"));
                        item1.setOfficial_logo(c.getString("official_logo"));
                        item1.setIsslected(false);
                        township_array_list.add(item1);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null) {
                Resources rs = getResources();
                if (json != null) {

                }
            }
            super.onPostExecute(s);
        }
    }


    private void show_officer_id(Activity context) {
        context = getActivity();
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.create_ticket_popup, null);
        final PopupWindow popup = new PopupWindow(context);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setFocusable(true);
        popup.setAnimationStyle(R.style.animationName);
        popup.showAtLocation(layout, Gravity.CENTER_VERTICAL, 0, 0);
        popup.setBackgroundDrawable(null);
        final EditText edit_officer = (EditText) layout.findViewById(R.id.edit_officer);
        Button btn_enter = (Button) layout.findViewById(R.id.btn_enter);
        Button btn_can = (Button) layout.findViewById(R.id.btn_cancel);

        btn_enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Officer_id = edit_officer.getText().toString();

                if (!Officer_id.equals("")) {
                    popup.dismiss();
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(edit_officer.getWindowToken(), 0);
                    new check_ticket_is_create().execute();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Invalid Code");
                    alertDialog.setMessage("Please enter a valid officer code to proceed.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }

            }
        });
        btn_can.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

    }

    public class check_ticket_is_create extends AsyncTask<String, String, String> {

        JSONObject json, json1;
        String transactionurl;

        String ticket_status = "null";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String query = null;
            try {
                transactionurl = "_table/parked_cars?filter=(plate_no%3D'" + URLEncoder.encode(platno, "utf-8") + "')%20AND%20(parking_status%3D'ENTRY')";
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                query = URLEncoder.encode(platno, "utf-8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray googleparking = json.getJSONArray("resource");
                    if (googleparking.length() > 0) {
                        JSONObject c = googleparking.getJSONObject(0);
                        ticket_status = c.getString("ticket_status");
                        lot_row = c.getString("lot_row");
                        expiry_time = c.getString("expiry_time");
                        marker_state = c.getString("marker_state");
                        subscription_id = c.getString("subscription_id");
                        ip = c.getString("ip");
                        ipn_status = c.getString("ipn_status");
                        lot_number = c.getString("lot_number");
                        ipn_txn_id = c.getString("ipn_txn_id");
                        pl_state = c.getString("pl_state");

                        parking_token = c.getString("parking_token");
                        ipn_custom = c.getString("ipn_custom");
                        parking_units = c.getString("parking_units");
                        ipn_payment = c.getString("ipn_payment");
                        tr_percent = c.getString("tr_percent");
                        zip = c.getString("zip");
                        parking_rate = c.getString("parking_rate");
                        tr_fee = c.getString("tr_fee");
                        ipn_address = c.getString("ipn_address");
                        marker_address1 = c.getString("marker_address1");

                        parking_qty = c.getString("parking_qty");
                        city = c.getString("city");
                        parking_type = c.getString("parking_type");
                        wallet_trx_id = c.getString("wallet_trx_id");
                        country = c.getString("country");
                        plate_no = c.getString("plate_no");
                        marker_zip = c.getString("marker_zip");
                        location_code = c.getString("location_code");
                        location_id = c.getString("location_id");
                        platform = c.getString("platform");
                        pl_country = c.getString("pl_country");
                        exit_date_time = c.getString("exit_date_time");
                        parking_total = c.getString("parking_total");

                        payment_method = c.getString("payment_method");
                        marker_address2 = c.getString("marker_address2");
                        address2 = c.getString("address2");
                        ticket_status = c.getString("ticket_status");
                        max_time = c.getString("max_duration");
                        permit_id = c.getString("permit_id");
                        marker_city = c.getString("marker_city");
                        String dis = c.getString("distance_to_marker");
                        if (dis.equals("null")) {
                            distance_to_marker = 0;
                        } else {
                            distance_to_marker = Integer.parseInt(c.getString("distance_to_marker"));
                        }
                        parking_status = c.getString("parking_status");
                        marker_country = c.getString("marker_country");
                        final_township_code = c.getString("township_code");
                        entry_date_time = c.getString("entry_date_time");
                        parking_subtotal = c.getString("parking_subtotal");
                        if (parking_subtotal.equals("null") || parking_subtotal.equals("")) {
                            parking_subtotal = "0";
                        }
                        address1 = c.getString("address1");
                        state = c.getString("state");
                        lng1 = c.getString("lng");
                        lat1 = c.getString("lat");
                        user_id1 = c.getString("user_id");
                        id1 = c.getInt("id");
                        townshipCode = c.getString("township_code");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            Resources rs;
            if (getActivity() != null && json != null) {
                if (ticket_status.equals("TICKETED")) {
                    rl_progressbar.setVisibility(View.GONE);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Already Ticketed");
                    alertDialog.setMessage("This vehicle has already been ticketed today at this location.");
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getActivity().onBackPressed();
                        }
                    });
                    alertDialog.show();
                } else {
                    upload_file();
                }
            }

            super.onPostExecute(s);
        }
    }

    public void upload_file() {

        file_name = new StringBuffer();
        totla_size_of_body = frg_proof_add.file_body.size();
        File file1 = null;
        SharedPreferences getdatatt = getActivity().getSharedPreferences("file_url", MODE_PRIVATE);
        String filepath = getdatatt.getString("file_path", "null");
        if (!filepath.equals("null")) {
            file1 = new File(filepath);
            totla_size_of_body = totla_size_of_body + 1;
        }

        if (totla_size_of_body == 0 && file1 == null) {
            if (vehicle_found) {
                new finalupdate().execute();
            } else {
                new createticket().execute();
            }
        } else {
            for (int i = 0; i < frg_proof_add.file_body.size(); i++) {
                File file = frg_proof_add.file_body.get(i).getFile_body();
                String name = file.getAbsolutePath();
                String nameg2 = name.substring(name.lastIndexOf("/"));
                String namegsxsax = nameg2.replace("/", "");
                file_name.append(namegsxsax + ", ");
                namegsxsax = "/images/" + namegsxsax;
                final_final_name = file_name.substring(0, file_name.length() - 2);
                TransferObserver transferObserver = transferUtility.upload("parkezly-images", namegsxsax, frg_proof_add.file_body.get(i).getFile_body());
                transferObserverListener(transferObserver);
            }

            if (file1 != null) {
                String name = file1.getAbsolutePath();
                String nameg2 = name.substring(name.lastIndexOf("/"));
                String namegsxsax = nameg2.replace("/", "");
                recording_name = namegsxsax;
                namegsxsax = "/audio-rec/" + namegsxsax;
                TransferObserver transferObserver = transferUtility.upload("parkezly-images", namegsxsax, file1);
                transferObserverListener(transferObserver);
            }
        }
    }


    public void transferObserverListener(TransferObserver transferObserver) {

        transferObserver.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
                Log.e("statechange", state + "");
                if (state.name().equals("COMPLETED")) {
                    current_file_upload_position++;

                    if (totla_size_of_body == current_file_upload_position) {
                        current_file_upload_position = 0;
                        Log.e("data", "yes");
                        if (vehicle_found) {
                            new finalupdate().execute();
                        } else {
                            new createticket().execute();
                        }
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                try {
                    int percentage = (int) (bytesCurrent / bytesTotal * 100);
                    Log.e("percentage", percentage + "");
                } catch (Exception E) {
                }

            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", "error");
            }

        });
    }

    public class finalupdate extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String rid = logindeatl.getString("id", "null");
        String id11 = "null";
        JSONObject json, json1;
        String pov2 = "_table/parked_cars";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            // jsonValues.put("id", 0);
            int pato = parking_token != null ? (!parking_token.equals("") ? (!parking_token.equals("null") ? Integer.parseInt(parking_token) : 0) : 0) : 0;
            lot_row = lot_row != null ? (!lot_row.equals("") ? (!lot_row.equals("null") ? lot_row : "") : "") : "";
            lot_number = lot_number != null ? (!lot_number.equals("") ? (!lot_number.equals("null") ? lot_number : "") : "") : "";
            int h = user_id1 != null ? (!user_id1.equals("") ? (!user_id1.equals("null") ? Integer.parseInt(user_id1) : 0) : 0) : 0;
            permit_id = permit_id != null ? (!permit_id.equals("") ? (!permit_id.equals("null") ? permit_id : "0") : "0") : "0";
            subscription_id = subscription_id != null ? (!subscription_id.equals("") ? (!subscription_id.equals("null") ? subscription_id : "0") : "0") : "0";
            wallet_trx_id = wallet_trx_id != null ? (!wallet_trx_id.equals("") ? (!wallet_trx_id.equals("null") ? wallet_trx_id : "0") : "0") : "0";
            ipn_txn_id = ipn_txn_id != null ? (!ipn_txn_id.equals("") ? (!ipn_txn_id.equals("null") ? ipn_txn_id : "0") : "0") : "0";

            jsonValues.put("parking_type", parking_type);
            jsonValues.put("township_code", township_code);
            jsonValues.put("location_code", location_code);
            jsonValues.put("entry_date_time", entry_date_time);
            jsonValues.put("exit_date_time", exit_date_time);
            jsonValues.put("expiry_time", expiry_time);
            jsonValues.put("max_duration", max_time);
            jsonValues.put("user_id", h);
            jsonValues.put("permit_id", Integer.parseInt(permit_id));
            jsonValues.put("subscription_id", subscription_id);
            jsonValues.put("plate_no", plate_no);
            jsonValues.put("pl_state", pl_state);
            jsonValues.put("lat", lat1);
            jsonValues.put("lng", lng1);
            jsonValues.put("address1", address1);
            jsonValues.put("address2", address2);
            jsonValues.put("city", city);
            jsonValues.put("state", state);
            jsonValues.put("zip", zip);
            jsonValues.put("country", country);
            jsonValues.put("lot_row", lot_row);
            jsonValues.put("lot_number", lot_number);
            jsonValues.put("ip", ip);
            jsonValues.put("parking_token", pato);
            jsonValues.put("parking_status", parking_status);
            jsonValues.put("payment_method", payment_method);
            jsonValues.put("parking_rate", parking_rate);
            jsonValues.put("parking_units", parking_units);
            jsonValues.put("parking_qty", parking_qty);
            jsonValues.put("parking_subtotal", parking_subtotal);
            jsonValues.put("wallet_trx_id", wallet_trx_id);
            jsonValues.put("tr_percent", tr_percent);
            jsonValues.put("tr_fee", tr_fee);
            jsonValues.put("parking_total", parking_total);
            jsonValues.put("ipn_custom", ipn_custom);
            jsonValues.put("ipn_txn_id", ipn_txn_id);
            jsonValues.put("ipn_payment", ipn_payment);
            jsonValues.put("ipn_status", ipn_status);
            jsonValues.put("ipn_address", ipn_address);
            jsonValues.put("distance_to_marker", distance_to_marker);
            jsonValues.put("marker_address1", marker_address1);
            jsonValues.put("marker_address2", marker_address2);
            jsonValues.put("marker_city", marker_city);
            jsonValues.put("marker_state", marker_state);
            jsonValues.put("marker_zip", marker_zip);
            jsonValues.put("marker_country", marker_country);
            jsonValues.put("ticket_status", "TICKETED");
            jsonValues.put("id", id1);
            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + pov2;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id11 = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json1 != null) {
                if (!id11.equals("null")) {
                    new createticket().execute();
                }
            }
            super.onPostExecute(s);
        }
    }

    public class createticket extends AsyncTask<String, String, String> {
        String idr = "null";
        JSONObject json, json1;
        String transactionurl = "_table/parking_violations";
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String ins_id = logindeatl.getString("id", "null");

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            comment = frg_proof_add.comment;
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
            String email = logindeatl.getString("email", "null");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            String currentdate = sdf.format(new Date());

            final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            TimeZone zone12 = TimeZone.getTimeZone("UTC");
            sdf1.setTimeZone(zone12);
            final String currentDateandTime = currentdate;
            Date date11 = null;
            try {
                date11 = sdf1.parse(currentDateandTime);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date11);
            calendar.add(Calendar.MONTH, 1);
            String expridate = sdf.format(calendar.getTime());
            int h = user_id1 != null ? (!user_id1.equals("") ? (!user_id1.equals("null") ? Integer.parseInt(user_id1) : 0) : 0) : 0;

            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("town_logo", township_logo_url);
            jsonValues.put("address", address1);
            jsonValues.put("violation_description", violation_desc);
            jsonValues.put("plate_no", platno);
            jsonValues.put("pl_state", statename.toUpperCase());
            jsonValues.put("date_ticket", currentdate);
            jsonValues.put("hearing_date", finalDate);
            jsonValues.put("location_code", location_code);
            jsonValues.put("violation_detail", violation_desc);
            jsonValues.put("respond_date", expridate);
            jsonValues.put("violation_fee", violation_fee);
            jsonValues.put("hearing_location", hearing_location);
            jsonValues.put("court_id", court_id);
            jsonValues.put("hearing_address", hearing_address);
            jsonValues.put("tkt_status", "OPEN");
            jsonValues.put("v_user_name", "");
            jsonValues.put("user_id", ins_id);
            jsonValues.put("v_user_id", h);
            jsonValues.put("plead_guilty_no_guilty", "");
            jsonValues.put("violation_code", violation_code);
            jsonValues.put("township_code", township_code);
            jsonValues.put("parked_location_address", marker_address1);
            jsonValues.put("officer_id", Officer_id);
            jsonValues.put("email", email);
            jsonValues.put("state", statename);
            jsonValues.put("text_comments", comment);
            jsonValues.put("photo_comments", final_final_name);
            jsonValues.put("av_comments", recording_name);
            jsonValues.put("location_id", location_id);
            pl_country = pl_country != null ? (!pl_country.equals("null") ? (!pl_country.equals("") ? pl_country : "") : "") : "";
            country = country != null ? (!country.equals("null") ? (!country.equals("") ? country : "") : "") : "";
            jsonValues.put("pl_country", country);
            jsonValues.put("platform", platform);
            jsonValues.put("tr_fee", tr_fee);
            double dTrPercent = tr_percentage1 * Double.parseDouble(violation_fee.substring(1));
            jsonValues.put("tr_percent", dTrPercent);
            jsonValues.put("i_platform", "Android");
            jsonValues.put("insptr_ip", getLocalIpAddress());
            jsonValues.put("ip", getLocalIpAddress());
            jsonValues.put("ticket_subtotal", violation_fee.substring(1));
            double dTicketTotal = Double.parseDouble(violation_fee.substring(1)) + dTrPercent + Double.parseDouble(tr_fee);
            jsonValues.put("ticket_total", dTicketTotal);

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);

                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        idr = c.getString("id");
                        ticket_id = idr;
                        create_ticket_date = currentdate;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json1 != null) {
                if (!idr.equals("null")) {
                    TextView tvTitle = new TextView(getActivity());
                    tvTitle.setText("Done ✓");
                    tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                    tvTitle.setTextColor(Color.BLACK);
                    tvTitle.setTextSize(20);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setCustomTitle(tvTitle);
                    alertDialog.setMessage("Ticket Created Successfully,  Would you Like to Print the Ticket");
                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ShowPrinterOption(getActivity());
                        }
                    });

                    alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            getActivity().onBackPressed();
                        }
                    });
                    alertDialog.show();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Oops, Please try again.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                }
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Error");
                alertDialog.setMessage("Oops, Please try again.");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alertDialog.show();
            }
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    public void ShowPrinterOption(Activity context) {

        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View layout = layoutInflater.inflate(R.layout.select_printer_option, viewGroup, false);
        final PopupWindow popup = new PopupWindow(context);
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(false);
        popup.setFocusable(false);
        popup.setAnimationStyle(R.style.animationName);
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

        ImageView img_close = (ImageView) layout.findViewById(R.id.img_close);
        TextView txt_pdf = (TextView) layout.findViewById(R.id.txt_pdf);
        TextView txt_bluetooth = (TextView) layout.findViewById(R.id.txt_bluetooth);


        txt_bluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                Intent i = new Intent(getActivity(), Main_Activity.class);
                i.putExtra("title", txt_township_name.getText().toString());
                i.putExtra("township_logo", township_logo_url);
                i.putExtra("date", "Ticket Date: " + create_ticket_date + "\n\nTicket Number: " + ticket_id + "\n\nViolation Fee: " + violation_fee + "\n\nViolation Description: " + violation_desc + "\n\nViolation Details: " + violation_desc + "\n\nRespond By Date: " + expridate + "\n\nHearing Location: " + hearing_location + "\n\nHearing Date: " + finalDate + "\n\nPlate # :" + platno + "\n\nState: " + statename + "\n\nParked Address: " + parked_address + "\n\nSelected Address: " + selected_address + "\n\nLocation Name: ");
                getActivity().startActivity(i);
            }
        });

        txt_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                createandDisplayPdf();
            }
        });

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

    }

    public void manipulatePdf(String src) throws IOException, DocumentException {

        try {

            PdfReader reader = new PdfReader(src);
            int n = reader.getNumberOfPages();
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ParkEZly";
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();

            File file = new File(dir, "parking_ticket" + "entry_ticket" + platno + timeStamp + "1" + ".pdf");
            FileOutputStream fOut = new FileOutputStream(file);
            PdfStamper stamper = new PdfStamper(reader, fOut);
            stamper.setRotateContents(false);
            // text watermark
           /* Font f = new Font(Font.FontFamily.HELVETICA, 30);
            Phrase p = new Phrase("My watermark (text)", f);*/
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File file1 = new File(path, "logo.PNG");
            FileOutputStream outStream = null;
            try {
                ImageView imgq = new ImageView(getActivity());
                imgq.setImageResource(R.drawable.rsz_1rsz_splashscreen);

                Bitmap bitmap = ((BitmapDrawable) imgq.getDrawable()).getBitmap();

                outStream = new FileOutputStream(file1);
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, outStream);
                outStream.flush();
                outStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String getpath = Environment.getExternalStorageDirectory() + "/" + "ParkEZly" + "/" + "logo.PNG";
            // image watermark
            Image img = Image.getInstance(getpath);
            float w = img.getScaledWidth();
            float h = img.getScaledHeight();

            // transparency
            PdfGState gs1 = new PdfGState();
            gs1.setFillOpacity(0.2f);
            // properties
            PdfContentByte over;
            Rectangle pagesize;
            float x, y;

            // loop over every page
            for (int i = 1; i <= n; i++) {
                pagesize = reader.getPageSize(i);

                x = (pagesize.getLeft() + pagesize.getRight()) / 2;
                y = (pagesize.getTop() + pagesize.getBottom()) / 2;
                over = stamper.getOverContent(i);
                over.saveState();
                over.setGState(gs1);
                //over.addImage(img,true);
                float gg = x - (w / 2);
                float hh = y - (h / 2);
                Log.e("dcd", String.valueOf(gg));

                Log.e("dcd", String.valueOf(hh));
                over.addImage(img, w, 0, 0, h, x - (w / 2), y - (h / 2));
                //  over.addImage(img, 200,0,200,0,0,0);
                over.restoreState();
            }
            stamper.close();
            reader.close();
            // viewPdf("parking_ticket" + platno +timeStamp+"1" + ".pdf", "parkEZly");
            onther_view(file);
            File file11 = new File(dir, "entry_ticket" + platno + timeStamp + ".pdf");
            file11.delete();
        } catch (IOException os) {
            Log.e("vb", String.valueOf(os));
        }
    }

    public void createandDisplayPdf() {
        Document doc = new Document();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        timeStamp = dateFormat.format(new Date());
        try {


            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ParkEZly";
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();

            File file = new File(dir, "entry_ticket" + platno + timeStamp + ".pdf");
            FileOutputStream fOut = new FileOutputStream(file);
            Font paraFont = new Font(Font.FontFamily.TIMES_ROMAN, 10);
            paraFont.setStyle(Font.BOLD);
            PdfWriter.getInstance(doc, fOut);

            //open the document
            doc.open();

            Paragraph pp1 = new Paragraph();
            Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 22, Font.BOLD);
            pp1.setFont(smallBold);
            pp1.add(txt_township_name.getText().toString());
            pp1.setAlignment(Paragraph.ALIGN_CENTER);
            doc.add(pp1);

            if (!township_logo_url.equals("")) {
                Bitmap bitmap = ((BitmapDrawable) img_logo.getDrawable()).getBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                Image companyLogo = Image.getInstance(stream.toByteArray());
                companyLogo.setAlignment(Image.MIDDLE | Image.TEXTWRAP);
                companyLogo.setWidthPercentage(50);
                doc.add(companyLogo);
            }

            String ad = "Violation Ticket\n\n\n";
            Paragraph pp = new Paragraph();

            pp.setFont(smallBold);
            pp.add(ad);
            pp.setAlignment(Paragraph.ALIGN_LEFT);
            doc.add(pp);
            String dd = "Ticket Date: " + create_ticket_date + "\n\nTicket Number: " + ticket_id + "\n\nViolation Fee: " + violation_fee + "\n\nViolation Description: " + violation_desc + "\n\nViolation Details: " + violation_desc + "\n\nRespond By Date: " + expridate + "\n\nHearing Location: " + hearing_location + "\n\nHearing Date: " + finalDate + "\n\nPlate # :" + platno + "\n\nState: " + statename + "\n\nParked Address: " + parked_address + "\n\nSelected Address: " + selected_address + "\n\nLocation Name: \n\n";
            Font paraFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 20);
            Paragraph p1 = new Paragraph();
            p1.setFont(paraFont1);
            p1.add(dd);
            p1.setAlignment(Paragraph.ALIGN_LEFT);
            doc.add(p1);


            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(50);

            Bitmap bitmap1 = ((BitmapDrawable) img_main_logo.getDrawable()).getBitmap();
            bitmap1 = getResizedBitmap(bitmap1, 80, 80);
            table.addCell(createImageCell(bitmap1));

          /*  ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
            bitmap1.compress(Bitmap.CompressFormat.PNG, 100, stream1);
            Image companyLogo1 = Image.getInstance(stream1.toByteArray());
            companyLogo1.setAlignment(Image.MIDDLE | Image.TEXTWRAP);
            companyLogo1.setWidthPercentage(30);
            doc.add(companyLogo1);*/

            Bitmap bitmap11 = ((BitmapDrawable) img_qrcode.getDrawable()).getBitmap();
            bitmap11 = getResizedBitmap(bitmap11, 80, 80);
            table.addCell(createImageCell(bitmap11));
            doc.add(table);
            doc.close();
           /* ByteArrayOutputStream stream11 = new ByteArrayOutputStream();
            bitmap11.compress(Bitmap.CompressFormat.PNG, 100, stream11);
            Image companyLogo11 = Image.getInstance(stream11.toByteArray());
            companyLogo11.setAlignment(Image.MIDDLE | Image.TEXTWRAP);
            companyLogo11.setWidthPercentage(30);
            doc.add(companyLogo11);*/


        } catch (DocumentException de) {
            Log.e("PDFCreator", "DocumentException:" + de);
        } catch (IOException e) {
            Log.e("PDFCreator", "ioException:" + e);
        } finally {
            doc.close();
        }

        String getpath = Environment.getExternalStorageDirectory() + "/" + "parkEZly" + "/" + "entry_ticket" + platno + timeStamp + ".pdf";

        try {
            manipulatePdf(getpath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        //viewPdf("entry_ticket" + platno + timeStamp + ".pdf", "parkEZly");
    }

    public PdfPCell createImageCell(Bitmap path) throws DocumentException, IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        path.compress(Bitmap.CompressFormat.PNG, 100, stream);
        Image image = Image.getInstance(stream.toByteArray());
        PdfPCell cell = new PdfPCell(image, true);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        // bm.recycle();
        return resizedBitmap;
    }

    private void viewPdf(String file, String directory) {

        String getpath = Environment.getExternalStorageDirectory() + "/" + directory + "/" + file;

        /*try {
            manipulatePdf(getpath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }*/
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/" + directory + "/" + file);
        Uri path = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", pdfFile);

        // Setting the intent for pdf reader
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        // pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getActivity(), "Can't read pdf file", Toast.LENGTH_SHORT).show();
        }
    }

    public void onther_view(File file) {
        File pdfFile = file;
        try {
            if (pdfFile.exists()) //Checking for the file is exist or not
            {
                // Uri path1 = Uri.fromFile(pdfFile);
                Uri path1 = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", pdfFile);
                Intent objIntent = new Intent(Intent.ACTION_VIEW);
                objIntent.setDataAndType(path1, "application/pdf");
                //  objIntent.setFlags(Intent. FLAG_ACTIVITY_CLEAR_TOP);
                objIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                // objIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(objIntent);//Staring the pdf viewer
            } else {
                Toast.makeText(getActivity(), "The file not exists! ", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
        }
    }

    public void hideKeyboard(View view) {
        Log.e("outtouch1", "yes");
        try {
            if (getActivity() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
        }

    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("ip", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("aax", ex.toString());
        }
        return null;
    }

    public class fetchServideChargesByTownship extends AsyncTask<String, String, String> {

        JSONObject json, json1;
        String servicechargeurl = "_table/service_charges?filter=manager_id%3D'" + townshipCode + "'";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + servicechargeurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        String tr_percentage = c.getString("tr_percentage");
                        String tr_fee = c.getString("tr_fee");
                        if (!tr_fee.equals("null")) {
                            tr_fee1 = Double.parseDouble(tr_fee);
                        } else {
                            tr_fee1 = 0.0;
                        }
                        if (!tr_percentage.equals("null")) {
                            tr_percentage1 = Double.parseDouble(tr_percentage);
                        } else {
                            tr_percentage1 = 0.0;
                        }
                        Log.e("tr_percentage1", String.valueOf(tr_percentage1));
                        Log.e("tr_free", String.valueOf(tr_fee1));
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null && json != null) {
            }
            super.onPostExecute(s);
        }
    }
}
