package com.softmeasures.eventezlyadminplus.frament;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.softmeasures.eventezlyadminplus.MyApplication;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.panstreetview;
import com.softmeasures.eventezlyadminplus.frament.partner.PartnersFragment;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.activity.vchome.latitude;
import static com.softmeasures.eventezlyadminplus.activity.vchome.longitude;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1.commercail_no_parking_time;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1.commercail_parking_time;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1.commercial_address;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step2.commecial_lots_avali;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step2.commercial_lost_total;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step2.commercial_off_end;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step2.commercial_off_peak_discount;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step2.commercial_off_peak_weekend;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step2.commercial_off_start;

public class commercial_parking_main extends Fragment implements Other_City.Other_city_listenere {
    ArrayList<item> googlepartnerarray = new ArrayList<>();
    ArrayList<item> commercial_parking_rules = new ArrayList<>();
    ArrayList<item> googleparkingarray = new ArrayList<>();
    ArrayList<item> commercial_array = new ArrayList<>();
    String commericalmap = "commercial";
    Double centerlat = 0.0, centerlang = 0.0;
    boolean CommercialMapLoaded = false, CommercialMarkerClick = false, CommercialPopup = false, CommercialBack = false, commercial_add_effect = true, commercial_add_active = true, commercial_renew = true;
    double commercial_lat = 0.0, commercial_lang = 0.0;
    String lat = "null", lang = "null";
    public static String commercial_loc_code, commercial_id;
    RelativeLayout rl_progressbar;
    RelativeLayout rl_nearby_commercial_list;
    ImageView img_commercial_nearby, img_nearby_close, img_other_city_of_commercial;
    ListView list_nearby_commercial;
    Animation animation;
    public static boolean commercial_edit_parking = false;
    private boolean isNewLocation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.commercial_map, container, false);

        if (getArguments() != null) {
            isNewLocation = getArguments().getBoolean("isNewLocation");
        }

        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        img_commercial_nearby = (ImageView) view.findViewById(R.id.findnearby_commerical);
        rl_nearby_commercial_list = (RelativeLayout) view.findViewById(R.id.rl_commercial_nearby);
        list_nearby_commercial = (ListView) view.findViewById(R.id.list_nearby_commercal);
        img_nearby_close = (ImageView) view.findViewById(R.id.img_commercial_map_close);
        img_other_city_of_commercial = (ImageView) view.findViewById(R.id.searchotherlocationcommercial);
        new getgooglepartner().execute();
        new getall_commercialrulesparking().execute();
        new getcommericalparking(latitude, longitude).execute();

        img_commercial_nearby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animation = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.sidepannelright);
                rl_nearby_commercial_list.setVisibility(View.VISIBLE);
                rl_nearby_commercial_list.startAnimation(animation);
                Resources rs = getResources();
                list_nearby_commercial.setAdapter(new CustomAdaptercommercial_direct(getActivity(), googleparkingarray, rs));

                list_nearby_commercial.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        rl_nearby_commercial_list.setVisibility(View.GONE);
                        new getplacedeatilCommercial(googleparkingarray.get(position).getPlace_id(), googleparkingarray.get(position).getMarker()).execute();
                    }
                });
            }
        });

        img_nearby_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animation = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.sidepannelleft);
                rl_nearby_commercial_list.setAnimation(animation);
                rl_nearby_commercial_list.setVisibility(View.GONE);
            }
        });


        img_other_city_of_commercial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.getString("click", "commercia");
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Other_City pay = new Other_City();
                pay.setArguments(b);
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                pay.registerForListener(commercial_parking_main.this);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        if (MyApplication.getInstance().isLocationAdded) {
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
        } else {
            getView().setFocusableInTouchMode(true);
            getView().requestFocus();
            getView().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {

                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                        if (rl_nearby_commercial_list.getVisibility() == View.VISIBLE) {
                            animation = AnimationUtils.loadAnimation(getActivity(),
                                    R.anim.sidepannelleft);
                            rl_nearby_commercial_list.setAnimation(animation);
                            rl_nearby_commercial_list.setVisibility(View.GONE);
                        } else {
                            getActivity().onBackPressed();
                        }
                        return true;
                    }
                    return false;
                }
            });
        }
        super.onResume();
    }

    @Override
    public void OnItemFindClick(String lat, String lang) {
        this.lat = lat;
        this.lang = lang;

        new getcommericalparking2(Double.parseDouble(lat), Double.parseDouble(lang)).execute();
    }

    // get all google parking data
    public class getgooglepartner extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json = new JSONObject();
        JSONArray json1;
        String parkingurl = "_table/google_parking_partners";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            googlepartnerarray.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setPlace_id(c.getString("place_id"));
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setTitle(c.getString("title"));
                        item1.setAddress(c.getString("address"));
                        item1.setEffect(c.getString("in_effect"));
                        item1.setActive(c.getString("active"));
                        item1.setRenew(c.getString("renewable"));
                        item1.setParking_times(c.getString("parking_times"));
                        item1.setNo_parking_times(c.getString("no_parking_times"));
                        item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        item1.setWeek_ebd_discount(c.getString("weekend_special_diff"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setTotal_lots(c.getString("lots_total"));
                        item1.setLots_aval(c.getString("lots_avbl"));
                        item1.setOff_peak_start(c.getString("off_peak_starts"));
                        item1.setOff_peak_end(c.getString("off_peak_ends"));
                        item1.setId(c.getString("id"));
                        googlepartnerarray.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs;
            if (getActivity() != null && json != null) {
                Log.e("googlepartnerarray", String.valueOf(googlepartnerarray.size()));
            }
            super.onPostExecute(s);
        }
    }

    //get commercial rules..............
    public class getall_commercialrulesparking extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        // http://108.30.248.212:8006/api/v2/pzly01live7/_table/user_locations?filter=user_id%3D5%20AND%20location_code%3DNY-NHP-03
        String id = logindeatl.getString("id", "null");
        String parkingurl = "_table/google_parking_rules";


        @Override
        protected void onPreExecute() {
            // rl_progressbar.setVisibility(View.VISIBLE);
            // progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            commercial_parking_rules.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setLoationname("");
                        item1.setTime_rule(c.getString("time_rule"));
                        item1.setPricing_duration(c.getString("pricing_duration"));
                        item1.setDuration_unit(c.getString("duration_unit"));
                        item1.setMax_time(c.getString("max_duration"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        String time_rules = c.getString("time_rule");
                        item1.setPricing(c.getString("pricing"));
                        item1.setId(c.getString("id"));
                        item1.setEffect(c.getString("in_effect"));
                        item1.setThis_day(c.getString("day_type"));
                        // item1.setMarker_type(c.getString("marker_type"));
                        item1.setLocation_code(c.getString("location_code"));
                        if (time_rules.contains("-")) {
                            String start_time = time_rules.substring(0, time_rules.indexOf("-"));
                            String end_time = time_rules.substring(time_rules.indexOf("-") + 1);
                            item1.setStart_time(start_time);
                            item1.setEnd_time(end_time);
                        } else {
                            item1.setStart_time("null");
                            item1.setEnd_time("null");
                        }
                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item1.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }

                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item1.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }

                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item1.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }

                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item1.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }

                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item1.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }

                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item1.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item1.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }

                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item1.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }

                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item1.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }

                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item1.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }

                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item1.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }
                        commercial_parking_rules.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs = getResources();
            if (getActivity() != null && json != null) {
            }
            super.onPostExecute(s);
        }
    }

    public class getcommericalparking extends AsyncTask<String, String, String> {
        boolean places_id_match = false;
        JSONObject json, json1, json_township;
        double lat = 0.0, lang = 0.0;

        public getcommericalparking(double lat, double lang) {
            this.lat = lat;
            this.lang = lang;
        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            googleparkingarray.clear();

            JSONObject inlat = new JSONObject();
            JSONObject inlang = new JSONObject();
            try {
                inlat.put("name", "in_lat");
                inlat.put("value", this.lat);
                // inlat.put("value", "40.7333");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
                inlang.put("name", "in_lng");
                inlang.put("value", this.lang);
                // inlang.put("value", "-73.445");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            JSONArray jsonArray = new JSONArray();

            jsonArray.put(inlat);
            jsonArray.put(inlang);

            JSONObject studentsObj = new JSONObject();
            try {
                studentsObj.put("params", jsonArray);
                Log.e("managed_params", String.valueOf(studentsObj));
            } catch (JSONException e) {
                e.printStackTrace();
            }


            String url1;
            if (lat == 0.0 || lang == 0.0) {
                url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=40.74599864,-73.9825673&rankby=distance&distance=50000&sensor=false&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&types=parking";
            } else {
                url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + lat + "," + lang + "&rankby=distance&distance=50000&sensor=false&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&types=parking";
            }


            DefaultHttpClient client1 = new DefaultHttpClient();
            HttpGet post1 = new HttpGet(url1);
            StringEntity entity = null;
            try {
                entity = new StringEntity(studentsObj.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

//this is your response:

            HttpResponse response1 = null;

            try {
                response1 = client1.execute(post1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response1 != null) {
                try {


                    HttpEntity resEntity1 = response1.getEntity();
                    String responseStr1 = null;


                    responseStr1 = EntityUtils.toString(resEntity1).trim();
                    //json = new JSONObject(responseStr);
                    json1 = new JSONObject(responseStr1);


                    JSONArray googleparking = json1.getJSONArray("results");

                    for (int j = 0; j < googleparking.length(); j++) {
                        item item = new item();
                        JSONObject google = googleparking.getJSONObject(j);
                        boolean pid = google.has("place_id");
                        if (pid) {
                            String place_id = google.getString("place_id");
                            for (int i = 0; i < googlepartnerarray.size(); i++) {
                                String plac_id = googlepartnerarray.get(i).getPlace_id();
                                if (place_id.equals(plac_id)) {
                                    String loccode = googlepartnerarray.get(i).getLocation_code();
                                    for (int k = 0; k < commercial_parking_rules.size(); k++) {
                                        String locationcode = commercial_parking_rules.get(k).getLocation_code();
                                        if (loccode.equals(locationcode)) {
                                            places_id_match = true;
                                            item.setLocation_code(googlepartnerarray.get(i).getLocation_code());
                                            item.setId(googlepartnerarray.get(i).getId());
                                            item.setWeekday_12hr_price(commercial_parking_rules.get(k).getPricing());
                                            Log.e("location_code pr", googlepartnerarray.get(i).getLocation_code() + " " + commercial_parking_rules.get(k).getPricing());
                                            break;
                                        }
                                    }

                                }
                            }


                            item.setPlace_id(place_id);


                        } else {
                            places_id_match = false;
                            //   item.setMarker("google");
                            item.setPlace_id("null");
                        }
                        if (places_id_match) {
                            places_id_match = false;
                            item.setMarker("partner");
                        } else {
                            item.setMarker("google");
                        }
                        String geo = google.getString("geometry");
                        JSONObject geometry = new JSONObject(geo);
                        String location = geometry.getString("location");
                        JSONObject lant = new JSONObject(location);
                        String lat = lant.getString("lat");
                        String lang = lant.getString("lng");
                        String name = google.getString("name");
                        if (google.has("vicinity")) {
                            String vicinity = google.getString("vicinity");
                            item.setVicinity(vicinity);
                        } else {
                            item.setVicinity("");
                        }
                        boolean openh = google.has("opening_hours");
                        item.setGooglelat(lat);
                        item.setGooglelan(lang);
                        item.setName(name);


                        if (openh) {
                            String opening_hours = google.getString("opening_hours");
                            JSONObject openig_h = new JSONObject(opening_hours);

                            String open = openig_h.getString("open_now");
                            item.setOpen_now(open);
                        } else {
                            item.setOpen_now("null");
                            //  Log.e("dataother", vicinity);
                        }
                        googleparkingarray.add(item);

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json1 != null) {
                if (googleparkingarray.size() >= 0) {
                    commericalmap = "commercial";
                    centerlang = 0.0;
                    centerlat = 0.0;
                    ShowCommercialMap();
                }
            }
            super.onPostExecute(s);

        }
    }

    //show map from commercial parking..............
    public void ShowCommercialMap() {

        SupportMapFragment fm1 = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map2);
        fm1.getMapAsync(googleMap -> {
            GoogleMap map = googleMap;
            MarkerOptions options = null;
            options = new MarkerOptions();
            if (commericalmap.equals("commercial")) {
                for (int i = 0; i < googleparkingarray.size(); i++) {
                    LatLng position = new LatLng(Double.parseDouble(googleparkingarray.get(i).getGooglelat()), Double.parseDouble(googleparkingarray.get(i).getGooglelan()));
                    options.position(position);
                    View marker = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
                    TextView numTxt = (TextView) marker.findViewById(R.id.text);
                    ImageView img = (ImageView) marker.findViewById(R.id.img_other_pin);
                    numTxt.setText("$10");
                    String markesr = googleparkingarray.get(i).getMarker();
                    if (markesr.equals("google")) {
                        img.setBackgroundResource(R.mipmap.commercial_icon);
                        options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.commercial_icon));
                        options.title("commercial");
                        options.snippet(googleparkingarray.get(i).getGooglelat() + "-" + googleparkingarray.get(i).getGooglelan());
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                                && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        map.setMyLocationEnabled(true);
                        map.addMarker(options);
                        Log.e("google", "yes");
                    } else if (markesr.equals("partner")) {
                        img.setBackgroundResource(R.mipmap.partner_pin_empt);
                        numTxt.setVisibility(View.VISIBLE);
                        numTxt.setText("$" + googleparkingarray.get(i).getWeekday_12hr_price());
                        options.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), marker)));
                        options.title("commercial");
                        options.snippet(googleparkingarray.get(i).getGooglelat() + "-" + googleparkingarray.get(i).getGooglelan());
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        map.setMyLocationEnabled(false);
                        map.addMarker(options);
                        Log.e("google", "no");
                    }
                    View locationButton = ((View) fm1.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                    RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                    rlp.setMargins(0, 0, 30, 20);
                }

                LatLng po1 = new LatLng(latitude, longitude);
                if (centerlang == 0.0 && centerlat == 0.0) {
                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(po1, 12);
                    map.animateCamera(location);
                }

                map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        if (commericalmap.equals("commercial")) {
                            Log.e("commercial_map_loaded", "yes");
                            CommercialMapLoaded = true;
                        }
                    }
                });


                final GoogleMap finalMap1 = map;
                map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        commercial_lat = cameraPosition.target.latitude;
                        commercial_lang = cameraPosition.target.longitude;
                        if (CommercialMapLoaded) {
                            if (CommercialMarkerClick == false) {
                                centerlat = cameraPosition.target.latitude;
                                centerlang = cameraPosition.target.longitude;
                                System.out.print("location chamges");
                                Log.e("Commercial_", "yessss");
                                new getcommericalparking1(centerlat, centerlang).execute();
                            }
                        }
                    }
                });

                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker arg0) {
                        String title = arg0.getTitle();
                        String lang = arg0.getSnippet();
                        String marker_lat = null, marker_lang = null;
                        CommercialMarkerClick = true;
                        CommercialMapLoaded = false;
                        commercial_loc_code = "";
                        if (lang.contains("-")) {
                            marker_lat = lang.substring(0, lang.indexOf("-"));
                            marker_lang = lang.substring(lang.indexOf("-") + 1);
                        }
                        if (title.equals("commercial")) {
                            for (int i = 0; i < googleparkingarray.size(); i++) {
                                String goolat = googleparkingarray.get(i).getGooglelat();
                                String glang = googleparkingarray.get(i).getGooglelan();
                                if (marker_lat.equals(goolat) && marker_lang.equals(glang)) {
                                    if (CommercialPopup == false) {
                                        if (commericalmap.equals("commercial")) {
                                            CommercialPopup = true;
                                            new getplacedeatilCommercial(googleparkingarray.get(i).getPlace_id(), googleparkingarray.get(i).getMarker()).execute();
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                        return true;
                    }
                });
            } else if (commericalmap.equals("CommericalChange")) {
                map.clear();
                for (int i = 0; i < googleparkingarray.size(); i++) {
                    if (!googleparkingarray.get(i).getGooglelat().equals("") && !googleparkingarray.get(i).getGooglelan().equals("")) {
                        LatLng position = new LatLng(Double.parseDouble(googleparkingarray.get(i).getGooglelat()), Double.parseDouble(googleparkingarray.get(i).getGooglelan()));
                        options.position(position);
                        View marker = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
                        TextView numTxt = (TextView) marker.findViewById(R.id.text);
                        ImageView img = (ImageView) marker.findViewById(R.id.img_other_pin);
                        String markesr = googleparkingarray.get(i).getMarker();
                        if (markesr.equals("google")) {
                            numTxt.setVisibility(View.GONE);
                            img.setBackgroundResource(R.mipmap.commercial_icon);
                            options.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), marker)));
                            options.title("commercial");
                            options.snippet(googleparkingarray.get(i).getGooglelat() + "-" + googleparkingarray.get(i).getGooglelan());
                            map.setMyLocationEnabled(false);
                            map.addMarker(options);
                            Log.e("google", "yes");
                        }
                        if (markesr.equals("partner")) {
                            img.setBackgroundResource(R.mipmap.partner_pin_empt);
                            numTxt.setVisibility(View.VISIBLE);
                            numTxt.setText("$" + googleparkingarray.get(i).getWeekday_12hr_price());
                            options.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), marker)));
                            options.title("commercial");
                            options.snippet(googleparkingarray.get(i).getGooglelat() + "-" + googleparkingarray.get(i).getGooglelan());
                            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                return;
                            }
                            map.setMyLocationEnabled(true);
                            map.addMarker(options);
                            Log.e("google", "no");
                        }
                        View locationButton = ((View) fm1.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                        rlp.setMargins(0, 0, 30, 20);
                    }
                }

                map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        if (commericalmap.equals("CommericalChange")) {
                            Log.e("commercial_map_loaded", "yes");
                            CommercialMapLoaded = true;
                        }

                    }
                });

                final GoogleMap finalMap = map;
                map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        commercial_lat = cameraPosition.target.latitude;
                        commercial_lang = cameraPosition.target.longitude;
                        if (CommercialMapLoaded) {
                            if (CommercialMarkerClick == false) {
                                centerlat = cameraPosition.target.latitude;
                                centerlang = cameraPosition.target.longitude;
                                System.out.print("location chamges");
                                Log.e("Commercial_", "yessss");
                                new getcommericalparking1(commercial_lat, commercial_lang).execute();
                            }
                        }
                    }
                });

                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker arg0) {
                        String title = arg0.getTitle();
                        String lang = arg0.getSnippet();
                        String marker_lat = null, marker_lang = null;
                        CommercialMarkerClick = true;
                        CommercialMapLoaded = false;
                        commercial_loc_code = "";
                        if (lang.contains("-")) {
                            marker_lat = lang.substring(0, lang.indexOf("-"));
                            marker_lang = lang.substring(lang.indexOf("-") + 1);
                        }
                        if (title.equals("commercial")) {
                            for (int i = 0; i < googleparkingarray.size(); i++) {
                                String goolat = googleparkingarray.get(i).getGooglelat();
                                String glang = googleparkingarray.get(i).getGooglelan();
                                if (marker_lat.equals(goolat) && marker_lang.equals(glang)) {
                                    if (CommercialPopup == false) {
                                        if (commericalmap.equals("CommericalChange")) {
                                            CommercialPopup = true;
                                            new getplacedeatilCommercial(googleparkingarray.get(i).getPlace_id(), googleparkingarray.get(i).getMarker()).execute();
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                        return true;
                    }
                });
            } else if (commericalmap.equals("CommericalChange1")) {
                for (int i = 0; i < googleparkingarray.size(); i++) {
                    LatLng position = new LatLng(Double.parseDouble(googleparkingarray.get(i).getGooglelat()), Double.parseDouble(googleparkingarray.get(i).getGooglelan()));
                    options.position(position);

                    View marker = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
                    TextView numTxt = (TextView) marker.findViewById(R.id.text);
                    ImageView img = (ImageView) marker.findViewById(R.id.img_other_pin);
                    numTxt.setText("$10");
                    String markesr = googleparkingarray.get(i).getMarker();
                    if (markesr.equals("google")) {
                        img.setBackgroundResource(R.mipmap.partner_pin_empt);
                        options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.commercial_icon));
                        options.title("commercial");
                        options.snippet(googleparkingarray.get(i).getGooglelat() + "-" + googleparkingarray.get(i).getGooglelan());
                        map.setMyLocationEnabled(false);
                        map.addMarker(options);
                        Log.e("google", "yes");
                    }
                    if (markesr.equals("partner")) {
                        img.setBackgroundResource(R.mipmap.partner_pin_empt);
                        numTxt.setVisibility(View.VISIBLE);
                        numTxt.setText("$" + googleparkingarray.get(i).getWeekday_12hr_price());
                        options.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), marker)));
                        options.title("commercial");
                        options.snippet(googleparkingarray.get(i).getGooglelat() + "-" + googleparkingarray.get(i).getGooglelan());
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        map.setMyLocationEnabled(true);
                        map.addMarker(options);
                        Log.e("google", "no");
                    }

                    View locationButton = ((View) fm1.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                    RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                    rlp.setMargins(0, 0, 30, 20);

                }

                LatLng po1 = new LatLng(Double.parseDouble(lat), Double.parseDouble(lang));
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                        po1, 12);
                map.animateCamera(location);
                map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        if (commericalmap.equals("commercial")) {
                            Log.e("commercial_map_loaded", "yes");
                            CommercialMapLoaded = true;
                        }

                    }
                });


                final GoogleMap finalMap1 = map;
                map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        commercial_lat = cameraPosition.target.latitude;
                        commercial_lang = cameraPosition.target.longitude;
                        if (CommercialMapLoaded) {
                            if (CommercialMarkerClick == false) {
                                centerlat = cameraPosition.target.latitude;
                                centerlang = cameraPosition.target.longitude;
                                System.out.print("location chamges");
                                Log.e("Commercial_", "yessss");
                                new getcommericalparking1(commercial_lat, commercial_lang).execute();
                            }
                        }
                    }
                });

                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker arg0) {
                        String title = arg0.getTitle();
                        String lang = arg0.getSnippet();
                        String marker_lat = null, marker_lang = null;
                        CommercialMarkerClick = true;
                        CommercialMapLoaded = false;
                        commercial_loc_code = "";
                        if (lang.contains("-")) {
                            marker_lat = lang.substring(0, lang.indexOf("-"));
                            marker_lang = lang.substring(lang.indexOf("-") + 1);
                        }
                        if (title.equals("commercial")) {
                            for (int i = 0; i < googleparkingarray.size(); i++) {
                                String goolat = googleparkingarray.get(i).getGooglelat();
                                String glang = googleparkingarray.get(i).getGooglelan();

                                if (marker_lat.equals(goolat) && marker_lang.equals(glang)) {

                                    if (CommercialPopup == false) {
                                        if (commericalmap.equals("CommericalChange1")) {
                                            CommercialPopup = true;
                                            new getplacedeatilCommercial(googleparkingarray.get(i).getPlace_id(), googleparkingarray.get(i).getMarker()).execute();
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                        return true;
                    }
                });
            }
        });
    }


    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    //get place deatil from marker click on commercial map
    public class getplacedeatilCommercial extends AsyncTask<String, String, String> {

        JSONObject json, json1;
        String pid, marker_type;
        String glat = "null", glang = "null", address = "null", gname = "null", open_ho = "null", places_id = "null";

        public getplacedeatilCommercial(String places_id, String marker_type) {
            this.pid = places_id;
            this.marker_type = marker_type;
        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            String url1 = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + pid + "&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA";

            Log.e("placeddeatil", url1);
            DefaultHttpClient client1 = new DefaultHttpClient();
            HttpGet post1 = new HttpGet(url1);
            post1.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post1.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

//this is your response:

            HttpResponse response1 = null;
            try {

                response1 = client1.execute(post1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response1 != null) {
                try {

                    HttpEntity resEntity1 = response1.getEntity();
                    String responseStr1 = null;


                    responseStr1 = EntityUtils.toString(resEntity1).trim();

                    json1 = new JSONObject(responseStr1);

                    JSONObject google = json1.getJSONObject("result");
                    String geo = google.getString("geometry");
                    JSONObject geometry = new JSONObject(geo);
                    String location = geometry.getString("location");
                    JSONObject lant = new JSONObject(location);
                    glat = lant.getString("lat");
                    glang = lant.getString("lng");
                    gname = google.getString("name");


                    boolean openh = google.has("opening_hours");


                    if (openh) {
                        String opening_hours = google.getString("opening_hours");
                        JSONObject openig_h = new JSONObject(opening_hours);
                        open_ho = openig_h.getString("open_now");

                    }

                    boolean pid = google.has("place_id");
                    if (pid) {
                        places_id = google.getString("place_id");

                    }
                    address = google.getString("formatted_address");
                    //  googleparkingarray.add(item);


                    // }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json1 != null) {

                ShoWCommercialParking(getActivity(), this.glat, glang, address, gname, open_ho, places_id, marker_type);
            } else {
                CommercialMapLoaded = true;
                CommercialMarkerClick = false;
                CommercialPopup = false;
            }
            super.onPostExecute(s);

        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void ShoWCommercialParking(Activity context, final String lat1, final String lang1, final String address, final String title, String open_h, final String place_id, final String marker_type) {
        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View layout = layoutInflater.inflate(R.layout.commercial, viewGroup, false);
        final PopupWindow popup = new PopupWindow(context);
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(false);
        popup.setFocusable(false);
        popup.setAnimationStyle(R.style.animationName);
        final TextView txttitle, txtstreetview, txtdrections, txt_parking_time, txt_no_parking_time, txt_off_peak, txt_weekend_discount, txt_custom, txt_if_effect, txt_day_rules, txt_time, txt_time_lable, txt_rate, txt_rate_lable, txt_max, txt_max_lable, txt_totla_lots, txt_aval_lots, txt_off_peak_start, txt_off_peak_end;
        Button btnadd, btnedit;
        ImageView imageclose;
        RelativeLayout rl_popupclose;
        String id = null;
        txttitle = (TextView) layout.findViewById(R.id.txtlable);
        txt_parking_time = (TextView) layout.findViewById(R.id.txt_parking_time);
        txt_no_parking_time = (TextView) layout.findViewById(R.id.txt_no_parking_time);
        txt_off_peak = (TextView) layout.findViewById(R.id.txt_off_peak);
        txt_weekend_discount = (TextView) layout.findViewById(R.id.txt_weekend_discount);
        txt_custom = (TextView) layout.findViewById(R.id.txt_custome_notice);
        txt_if_effect = (TextView) layout.findViewById(R.id.txt_effect);
        txt_day_rules = (TextView) layout.findViewById(R.id.txtpouptoday1);
        txt_time_lable = (TextView) layout.findViewById(R.id.txtpouptoday);
        txt_time = (TextView) layout.findViewById(R.id.txtpopupaddress);
        txt_rate_lable = (TextView) layout.findViewById(R.id.txtpopuptime1);
        txt_rate = (TextView) layout.findViewById(R.id.txtpopuptime);
        txt_max = (TextView) layout.findViewById(R.id.txtpopupax);
        txt_max_lable = (TextView) layout.findViewById(R.id.txtpopupax1);
        txtstreetview = (TextView) layout.findViewById(R.id.txtpopupstreetview);
        txtdrections = (TextView) layout.findViewById(R.id.txtpopupdirection);
        txt_totla_lots = (TextView) layout.findViewById(R.id.txt_totla_lots);
        txt_aval_lots = (TextView) layout.findViewById(R.id.txt_totla_aval);
        txt_off_peak_start = (TextView) layout.findViewById(R.id.txt_off_prak_start);
        txt_off_peak_end = (TextView) layout.findViewById(R.id.txt_off_peak_end);
        btnadd = (Button) layout.findViewById(R.id.btn_add);
        btnedit = (Button) layout.findViewById(R.id.btn_edit);
        rl_popupclose = (RelativeLayout) layout.findViewById(R.id.rl_pop);
        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Light.otf");
        txttitle.setTypeface(type);
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);
        txttitle.setText(title + "\n" + address);
        String final_loc_code = "";
        try {
            String loca_code1 = address.substring(0, address.indexOf(","));
            String loc = address.substring(address.indexOf(",") + 1);
            loca_code1 = loca_code1.replace(" ", "");
            loc = loc.replace(" ", "");
            String upToNCharacters = loc.substring(0, Math.min(loc.length(), 2));
            final_loc_code = loca_code1 + upToNCharacters;
        } catch (Exception e) {
            final_loc_code = title;

        }

        imageclose = (ImageView) layout.findViewById(R.id.popupclose);

        if (marker_type.equals("partner")) {
            for (int k = 0; k < googlepartnerarray.size(); k++) {
                String paces_id = googlepartnerarray.get(k).getPlace_id();
                if (paces_id.equals(place_id)) {
                    String location_code = googlepartnerarray.get(k).getLocation_code();
                    for (int j = 0; j < commercial_parking_rules.size(); j++) {
                        String loc_code = commercial_parking_rules.get(j).getLocation_code();
                        if (location_code.equals(loc_code)) {
                            if (googlepartnerarray.get(k).getNo_parking_times() != null) {
                                txt_no_parking_time.setText("No Parking Times: " + googlepartnerarray.get(k).getNo_parking_times());
                                commercail_no_parking_time = googlepartnerarray.get(k).getNo_parking_times();
                            }
                            String vjhdv = googlepartnerarray.get(k).getWeek_ebd_discount();
                            if (vjhdv != null) {
                                double weeke = Double.parseDouble(googlepartnerarray.get(k).getWeek_ebd_discount());
                                String week = String.format("%.1f", weeke);
                                txt_weekend_discount.setText("Weekend Special Difference: " + week + "%");
                                commercial_off_peak_weekend = week;
                            } else {
                                txt_weekend_discount.setText("Weekend Special Difference: ");
                                commercial_off_peak_weekend = "";
                            }

                            if (googlepartnerarray.get(k).getTotal_lots() != null) {
                                if (!googlepartnerarray.get(k).getTotal_lots().equals("") || !googlepartnerarray.get(k).getTotal_lots().equals("null")) {
                                    txt_totla_lots.setText("Total Lots: " + googlepartnerarray.get(k).getTotal_lots());
                                    commercial_lost_total = googlepartnerarray.get(k).getTotal_lots();
                                } else {
                                    txt_totla_lots.setText("Total Lots: ");
                                    commercial_lost_total = "";
                                }

                            } else {
                                txt_totla_lots.setText("Total Lots: ");
                                commercial_lost_total = "";
                            }

                            if (googlepartnerarray.get(k).getParking_times() != null) {
                                if (!googlepartnerarray.get(k).getParking_times().equals("") || !googlepartnerarray.get(k).getParking_times().equals("null")) {
                                    txt_parking_time.setText("Parking Times: " + googlepartnerarray.get(k).getParking_times());
                                    commercail_parking_time = googlepartnerarray.get(k).getParking_times();
                                } else {
                                    txt_parking_time.setText("Parking Times: ");
                                    commercail_parking_time = "";
                                }

                            } else {
                                txt_parking_time.setText("Parking Times: ");
                            }

                            if (googlepartnerarray.get(k).getLots_aval() != null) {
                                if (!googlepartnerarray.get(k).getLots_aval().equals("") || !googlepartnerarray.get(k).getLots_aval().equals("null")) {
                                    txt_aval_lots.setText("Available Lots: " + googlepartnerarray.get(k).getLots_aval());
                                    commecial_lots_avali = googlepartnerarray.get(k).getLots_aval();
                                } else {
                                    txt_aval_lots.setText("Available Lots: ");
                                    commecial_lots_avali = "";
                                }

                            } else {
                                txt_aval_lots.setText("Available Lots: ");
                                commecial_lots_avali = "";
                            }

                            if (googlepartnerarray.get(k).getOff_peak_start() != null) {
                                if (!googlepartnerarray.get(k).getOff_peak_start().equals("") || !googlepartnerarray.get(k).getOff_peak_start().equals("null")) {
                                    txt_off_peak_start.setText("Off Peak Start Hour: " + googlepartnerarray.get(k).getOff_peak_start());
                                    commercial_off_start = googlepartnerarray.get(k).getOff_peak_start();

                                } else {
                                    txt_off_peak_start.setText("Off Peak Start Hour: ");
                                    commercial_off_start = "";
                                }

                            } else {
                                txt_off_peak_start.setText("Off Peak Start Hour: ");
                                commercial_off_start = "";
                            }

                            if (googlepartnerarray.get(k).getOff_peak_end() != null) {
                                if (!googlepartnerarray.get(k).getOff_peak_end().equals("") || !googlepartnerarray.get(k).getOff_peak_end().equals("null")) {
                                    txt_off_peak_end.setText("Off Peak End Hour: " + googlepartnerarray.get(k).getOff_peak_end());
                                    commercial_off_end = googlepartnerarray.get(k).getOff_peak_end();
                                } else {

                                    txt_off_peak_end.setText("Off Peak End Hour: ");
                                    commercial_off_end = "";
                                }

                            } else {
                                txt_off_peak_end.setText("Off Peak End Hour: ");
                                commercial_off_end = "";
                            }

                            if (googlepartnerarray.get(k).getOff_peak_discount() != null) {
                                double off = Double.parseDouble(googlepartnerarray.get(k).getOff_peak_discount());
                                String oddpeak = String.format("%.1f", off);
                                txt_off_peak.setText("Off Peak Discount: " + oddpeak + "%");
                                commercial_off_peak_discount = oddpeak;
                            } else {
                                txt_off_peak.setText("Off Peak Discount:");
                                commercial_off_peak_discount = "";
                            }
                            if (googlepartnerarray.get(k).getCustom_notice() != null) {
                                txt_custom.setText("Custom Notices: " + googlepartnerarray.get(k).getCustom_notice());
                            } else {
                                txt_custom.setText("Custom Notices:");
                            }
                            commercial_id = googlepartnerarray.get(k).getId();
                            commercial_loc_code = googlepartnerarray.get(k).getLocation_code();
                            commercial_address = googlepartnerarray.get(k).getAddress();
                            txt_if_effect.setText("In Effect: " + googlepartnerarray.get(k).getEffect());
                            txt_day_rules.setText(commercial_parking_rules.get(j).getThis_day());
                            txt_time.setText(commercial_parking_rules.get(j).getTime_rule());
                            txt_rate.setText("$" + commercial_parking_rules.get(j).getPricing() + " @ " + commercial_parking_rules.get(j).getPricing_duration() + commercial_parking_rules.get(j).getDuration_unit());
                            txt_max.setText(commercial_parking_rules.get(j).getMax_time() + commercial_parking_rules.get(j).getDuration_unit());
                            txt_parking_time.setVisibility(View.VISIBLE);
                            txt_no_parking_time.setVisibility(View.VISIBLE);
                            txt_weekend_discount.setVisibility(View.VISIBLE);
                            txt_off_peak.setVisibility(View.VISIBLE);
                            txt_custom.setVisibility(View.VISIBLE);
                            txt_if_effect.setVisibility(View.VISIBLE);
                            txt_day_rules.setVisibility(View.VISIBLE);
                            txt_time_lable.setVisibility(View.VISIBLE);
                            txt_time.setVisibility(View.VISIBLE);
                            txt_rate_lable.setVisibility(View.VISIBLE);
                            txt_rate.setVisibility(View.VISIBLE);
                            txt_max.setVisibility(View.VISIBLE);
                            txt_max_lable.setVisibility(View.VISIBLE);
                            txt_totla_lots.setVisibility(View.VISIBLE);
                            txt_aval_lots.setVisibility(View.VISIBLE);
                            txt_off_peak_end.setVisibility(View.VISIBLE);
                            txt_off_peak_start.setVisibility(View.VISIBLE);
                            btnedit.setVisibility(View.VISIBLE);
                            btnadd.setVisibility(View.GONE);
                            break;
                        }
                    }

                }

            }
        }
        final String final_loc_code1 = final_loc_code;
        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommercialMapLoaded = true;
                CommercialMarkerClick = false;
                CommercialPopup = false;
                commercial_loc_code = final_loc_code1;
                commercial_edit_parking = false;
                for (int i = 0; i < googleparkingarray.size(); i++) {
                    String paces_id = googleparkingarray.get(i).getPlace_id();
                    if (paces_id.equals(place_id)) {
                        Bundle b = new Bundle();
                        b.putString("place_id", googleparkingarray.get(i).getPlace_id());
                        b.putString("title", title);
                        b.putString("address", title);
                        b.putString("code", commercial_loc_code);
                        b.putString("lat", googleparkingarray.get(i).getGooglelat());
                        b.putString("lng", googleparkingarray.get(i).getGooglelan());
                        b.putString("parking_time", "");
                        b.putString("parking_non_time", "");
                        b.putString("is_add", "yes");
                        b.putString("parkingType", "Commercial");

                        if (isNewLocation) {
                            b.putBoolean("isNewLocation", isNewLocation);
                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            frg_commercial_edit_add_step1 pay = new frg_commercial_edit_add_step1();
                            pay.setArguments(b);
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.add(R.id.My_Container_1_ID, pay);
                            fragmentStack.lastElement().onPause();
                            ft.hide(fragmentStack.lastElement());
                            fragmentStack.push(pay);
                            ft.commitAllowingStateLoss();

                        } else {
                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            PartnersFragment pay = new PartnersFragment();
                            pay.setArguments(b);
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.add(R.id.My_Container_1_ID, pay);
                            fragmentStack.lastElement().onPause();
                            ft.hide(fragmentStack.lastElement());
                            fragmentStack.push(pay);
                            ft.commitAllowingStateLoss();
                            break;
                        }

                    }
                }
                popup.dismiss();
            }
        });

        btnedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommercialMapLoaded = true;
                CommercialMarkerClick = false;
                CommercialPopup = false;
                commercial_edit_parking = true;
                commercial_loc_code = final_loc_code1;
                for (int i = 0; i < googleparkingarray.size(); i++) {
                    String paces_id = googleparkingarray.get(i).getPlace_id();
                    if (paces_id.equals(place_id)) {
                        Bundle b = new Bundle();
                        b.putString("place_id", googleparkingarray.get(i).getPlace_id());
                        b.putString("title", title);
                        b.putString("address", title);
                        b.putString("code", commercial_loc_code);
                        b.putString("lat", googleparkingarray.get(i).getGooglelat());
                        b.putString("lng", googleparkingarray.get(i).getGooglelan());
                        b.putString("parking_time", "");
                        b.putString("parking_non_time", "");
                        b.putString("is_add", "no");

                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        frg_commercial_edit_add_step1 pay = new frg_commercial_edit_add_step1();
                        pay.setArguments(b);
                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.add(R.id.My_Container_1_ID, pay);
                        fragmentStack.lastElement().onPause();
                        ft.hide(fragmentStack.lastElement());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                        break;
                    }
                }
                popup.dismiss();
            }
        });

        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             /*   rl_nearby_pin_deatil.setVisibility(View.GONE);
                animation = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.popup_off);
                layout.startAnimation(animation);*/
                CommercialMapLoaded = true;
                CommercialMarkerClick = false;
                CommercialPopup = false;
                commercial_loc_code = "";
                popup.dismiss();
            }
        });
        rl_popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //otherparkingoptionmarker = false;
                CommercialMapLoaded = true;
                CommercialMarkerClick = false;
                CommercialPopup = false;
                commercial_loc_code = "";
                // rl_nearby_pin_deatil.setVisibility(View.GONE);
              /*  animation = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.popup_off);
                layout.startAnimation(animation);*/
                popup.dismiss();
            }
        });
        txtdrections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommercialBack = true;
                commercial_loc_code = "";
                CommercialMapLoaded = true;
                CommercialPopup = false;
                CommercialMarkerClick = false;
                popup.dismiss();
                SharedPreferences sh1 = getActivity().getSharedPreferences("directiontomydirection", Context.MODE_PRIVATE);
                SharedPreferences.Editor ed1 = sh1.edit();
                ed1.putString("lat", lat1);
                ed1.putString("lang", lang1);
                ed1.putString("address", address);
                ed1.commit();

                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                f_direction pay = new f_direction();
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        txtstreetview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommercialMapLoaded = true;
                commercial_loc_code = "";
                CommercialMarkerClick = false;
                CommercialPopup = false;
                Intent i = new Intent(getActivity(), panstreetview.class);
                i.putExtra("lat", lat1);
                i.putExtra("lang", lang1);
                i.putExtra("title", title);
                i.putExtra("address", address);
                startActivity(i);
            }
        });

        // markerclick=false;
    }


    //show data when map postion is change
    public class getcommericalparking1 extends AsyncTask<String, String, String> {
        boolean places_id_match = false;
        JSONObject json, json1, json_township;
        double lat = 0.0, lang = 0.0;

        public getcommericalparking1(double lat, double lang) {
            this.lat = lat;
            this.lang = lang;
        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.GONE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            commercial_array.clear();
            JSONObject inlat = new JSONObject();
            JSONObject inlang = new JSONObject();
            try {
                inlat.put("name", "in_lat");
                inlat.put("value", this.lat);
                // inlat.put("value", "40.7333");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
                inlang.put("name", "in_lng");
                inlang.put("value", this.lang);
                // inlang.put("value", "-73.445");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            JSONArray jsonArray = new JSONArray();

            jsonArray.put(inlat);
            jsonArray.put(inlang);

            JSONObject studentsObj = new JSONObject();
            try {
                studentsObj.put("params", jsonArray);
                Log.e("managed_params", String.valueOf(studentsObj));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //    String url = "http://api.parkwhiz.com/p/new-york-parking/?key=0fe91971e1fa4e1031bca1011b7f383c";
            //  String url = getString(R.string.api) + getString(R.string.povlive) + "_table/other_parking_partners";

            String url1;
            if (lat == 0.0 || lang == 0.0) {
                url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=40.74599864,-73.9825673&rankby=distance&distance=50000&sensor=false&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&types=parking";
            } else {
                url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + lat + "," + lang + "&rankby=distance&distance=50000&sensor=false&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&types=parking";
            }


            DefaultHttpClient client1 = new DefaultHttpClient();
            HttpGet post1 = new HttpGet(url1);
            StringEntity entity = null;
            try {
                entity = new StringEntity(studentsObj.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

//this is your response:

            HttpResponse response1 = null;

            try {
                response1 = client1.execute(post1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response1 != null) {
                try {


                    HttpEntity resEntity1 = response1.getEntity();
                    String responseStr1 = null;


                    responseStr1 = EntityUtils.toString(resEntity1).trim();
                    //json = new JSONObject(responseStr);
                    json1 = new JSONObject(responseStr1);


                    JSONArray googleparking = json1.getJSONArray("results");

                    for (int j = 0; j < googleparking.length(); j++) {
                        item item = new item();
                        JSONObject google = googleparking.getJSONObject(j);
                        boolean pid = google.has("place_id");
                        if (pid) {
                            String place_id = google.getString("place_id");
                            for (int i = 0; i < googlepartnerarray.size(); i++) {
                                String plac_id = googlepartnerarray.get(i).getPlace_id();
                                if (place_id.equals(plac_id)) {
                                    String loccode = googlepartnerarray.get(i).getLocation_code();
                                    for (int k = 0; k < commercial_parking_rules.size(); k++) {
                                        String locationcode = commercial_parking_rules.get(k).getLocation_code();
                                        if (loccode.equals(locationcode)) {
                                            places_id_match = true;
                                            item.setLocation_code(googlepartnerarray.get(i).getLocation_code());
                                            item.setId(googlepartnerarray.get(i).getId());
                                            item.setWeekday_12hr_price(commercial_parking_rules.get(k).getPricing());
                                            break;
                                        }
                                    }
                                }
                            }
                            item.setPlace_id(place_id);


                        } else {
                            places_id_match = false;
                            //   item.setMarker("google");
                            item.setPlace_id("null");
                        }
                        if (places_id_match) {
                            places_id_match = false;
                            item.setMarker("partner");
                        } else {
                            item.setMarker("google");
                        }
                        String geo = google.getString("geometry");
                        JSONObject geometry = new JSONObject(geo);
                        String location = geometry.getString("location");
                        JSONObject lant = new JSONObject(location);
                        String lat = lant.getString("lat");
                        String lang = lant.getString("lng");
                        String name = google.getString("name");
                        if (google.has("vicinity")) {
                            String vicinity = google.getString("vicinity");
                            item.setVicinity(vicinity);
                        } else {
                            item.setVicinity("");
                        }
                        boolean openh = google.has("opening_hours");
                        item.setGooglelat(lat);
                        item.setGooglelan(lang);
                        item.setName(name);


                        if (openh) {
                            String opening_hours = google.getString("opening_hours");
                            JSONObject openig_h = new JSONObject(opening_hours);

                            String open = openig_h.getString("open_now");
                            item.setOpen_now(open);
                        } else {
                            item.setOpen_now("null");
                            //  Log.e("dataother", vicinity);
                        }
                        commercial_array.add(item);

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json1 != null) {
                if (commercial_array.size() >= 0) {
                    commericalmap = "CommericalChange";
                    googleparkingarray.clear();
                    googleparkingarray.addAll(commercial_array);

                    ShowCommercialMap();
                }
            }
            super.onPostExecute(s);

        }
    }


    public class CustomAdaptercommercial_direct extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdaptercommercial_direct(Activity a, ArrayList<item> d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;
            data = d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.adapter_commerical_parking, null);
                /****** View Holder Object to contain tabitem.xml file elements ******/
                holder = new ViewHolder();

                holder.tvLocationName = vi.findViewById(R.id.tvLocationName);
                holder.tvPrice = vi.findViewById(R.id.tvPrice);
                holder.tvAddress = vi.findViewById(R.id.tvAddress);
                holder.tvDistance = vi.findViewById(R.id.tvDistance);
                holder.tvTimingRules = vi.findViewById(R.id.tvTimingRules);
                holder.tvStatus = vi.findViewById(R.id.tvStatus);

                holder.txt_commerical_name = (TextView) vi.findViewById(R.id.txt_comm_name);
                holder.txt_deatil = (TextView) vi.findViewById(R.id.txt_comm_add);
                holder.rldata = (RelativeLayout) vi.findViewById(R.id.rl_main);
                holder.txt_rate = (TextView) vi.findViewById(R.id.txt_pricing);
                holder.txt_mile = (TextView) vi.findViewById(R.id.txt_mile);
                holder.txt_parking_status = (TextView) vi.findViewById(R.id.txt_commercial_view);
                holder.txt_la = (TextView) vi.findViewById(R.id.txt_lat);
                holder.txt_lang = (TextView) vi.findViewById(R.id.txt_lang);
                holder.txt_location_code = (TextView) vi.findViewById(R.id.txt_location_code);
                holder.txt_place_id = (TextView) vi.findViewById(R.id.txt_place_id);
                holder.txt_marker_type = (TextView) vi.findViewById(R.id.txt_marker_type);
                /************  Set holder with LayoutInflater ************/
                holder.txt_parking_status.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String status = holder.txt_parking_status.getText().toString();
                        if (status.equals("OPEN")) {
                            rl_nearby_commercial_list.setVisibility(View.GONE);
                            new getplacedeatilCommercial(holder.txt_place_id.getText().toString(), holder.txt_marker_type.getText().toString()).execute();


                        }
                    }
                });
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }
            if (data.size() <= 0) {
                rl_nearby_commercial_list.setVisibility(View.GONE);
            } else {

                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String cat = tempValues.getName();
                if (cat == null) {

                } else if (cat != null) {

                    holder.txt_commerical_name.setText(tempValues.getName());
                    holder.tvLocationName.setText(tempValues.getName());
                    holder.tvAddress.setText(tempValues.getVicinity());
                    holder.txt_deatil.setText(tempValues.getVicinity());
                    if (position % 2 == 0) {
                        holder.rldata.setBackgroundColor(Color.parseColor("#949494"));
                    } else {
                        holder.rldata.setBackgroundColor(Color.parseColor("#434343"));
                    }

                    String markesr = tempValues.getMarker();
                    Log.e("markget", markesr);
                    if (markesr.equals("partner")) {
                        Log.e("markget", "tes");
                        String data1 = tempValues.getWeekday_12hr_price();
                        String placeid = tempValues.getPlace_id();
                        if (!data1.equals("")) {


                            for (int y = 0; y < googlepartnerarray.size(); y++) {
                                String pid = googlepartnerarray.get(y).getPlace_id();
                                if (placeid.equals(pid)) {
                                    holder.txt_rate.setVisibility(View.VISIBLE);
                                    holder.txt_rate.setText("$" + data1);
                                    holder.tvPrice.setText("$" + data1);
                                    double prices = Double.parseDouble(data1);
                                    String effect = googlepartnerarray.get(y).getEffect();
                                    String lots_aval = googlepartnerarray.get(y).getLots_aval();
                                    if (effect.equals("0")) {
                                        holder.txt_parking_status.setVisibility(View.VISIBLE);
                                        holder.txt_parking_status.setText("CLOSED");
                                        holder.tvStatus.setText("CLOSED");
                                        holder.txt_parking_status.setBackgroundColor(Color.parseColor("#E9967A"));
                                    } else if (lots_aval.equals("0")) {
                                        holder.txt_parking_status.setVisibility(View.VISIBLE);
                                        holder.txt_parking_status.setText("CLOSED");
                                        holder.tvStatus.setText("CLOSED");
                                        holder.txt_parking_status.setBackgroundColor(Color.parseColor("#E9967A"));
                                    } else if (prices != -1.0 || prices != 1) {
                                        holder.txt_parking_status.setVisibility(View.VISIBLE);
                                        holder.txt_parking_status.setText("OPEN");
                                        holder.tvStatus.setText("OPEN");
                                        holder.txt_parking_status.setBackgroundColor(Color.parseColor("#3CB385"));
                                    } else {
                                        holder.txt_parking_status.setVisibility(View.VISIBLE);
                                        holder.txt_parking_status.setText("CLOSED");
                                        holder.tvStatus.setText("CLOSED");
                                        holder.txt_parking_status.setBackgroundColor(Color.parseColor("#E9967A"));
                                    }
                                    break;
                                }
                            }

                        } else {
                            holder.txt_parking_status.setVisibility(View.GONE);
                            holder.txt_rate.setVisibility(View.GONE);
                        }
                    } else {
                        holder.txt_parking_status.setVisibility(View.GONE);
                        holder.txt_rate.setVisibility(View.GONE);
                    }
                    if (!tempValues.getGooglelat().equals("") && !tempValues.getGooglelan().equals("")) {
                        holder.txt_mile.setVisibility(View.VISIBLE);
                        LatLng kk = new LatLng(Double.parseDouble(tempValues.getGooglelat()), Double.parseDouble(tempValues.getGooglelan()));
                        double dist = getDistance(kk);
                        double ff = dist * 0.000621371192;
                        String finallab2 = String.format("%.2f", ff);
                        holder.txt_mile.setText(finallab2 + " Mile");
                        holder.tvDistance.setText(finallab2 + " Mile");
                    } else {
                        holder.txt_mile.setVisibility(View.GONE);
                    }
                    holder.txt_la.setText(tempValues.getGooglelat());
                    holder.txt_lang.setText(tempValues.getGooglelan());
                    holder.txt_location_code.setText(tempValues.getLocation_code());
                    holder.txt_place_id.setText(tempValues.getPlace_id());
                    holder.txt_marker_type.setText(tempValues.getMarker_type());
                }
            }

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {

            public TextView txt_commerical_name, txt_deatil, txt_rate, txt_mile, txt_parking_status, txt_la, txt_lang, txt_location_code, txt_place_id, txt_marker_type;
            RelativeLayout rldata;
            TextView tvLocationName, tvPrice, tvAddress, tvDistance, tvTimingRules, tvStatus;
        }
    }

    public double getDistance(LatLng LatLng1) {
        double distance = 0;
        Location locationA = new Location("A");
        locationA.setLatitude(LatLng1.latitude);
        locationA.setLongitude(LatLng1.longitude);
        Location locationB = new Location("B");
        locationB.setLatitude(latitude);
        locationB.setLongitude(longitude);
        distance = locationA.distanceTo(locationB);
        return distance;
    }

    //show data from commercial map with search other city
    public class getcommericalparking2 extends AsyncTask<String, String, String> {
        boolean places_id_match = false;
        JSONObject json, json1, json_township;
        double lat = 0.0, lang = 0.0;

        public getcommericalparking2(double lat, double lang) {
            this.lat = lat;
            this.lang = lang;
        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.GONE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            googleparkingarray.clear();
            JSONObject inlat = new JSONObject();
            JSONObject inlang = new JSONObject();
            try {
                inlat.put("name", "in_lat");
                inlat.put("value", this.lat);
                // inlat.put("value", "40.7333");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
                inlang.put("name", "in_lng");
                inlang.put("value", this.lang);
                // inlang.put("value", "-73.445");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            JSONArray jsonArray = new JSONArray();

            jsonArray.put(inlat);
            jsonArray.put(inlang);

            JSONObject studentsObj = new JSONObject();
            try {
                studentsObj.put("params", jsonArray);
                Log.e("managed_params", String.valueOf(studentsObj));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //    String url = "http://api.parkwhiz.com/p/new-york-parking/?key=0fe91971e1fa4e1031bca1011b7f383c";
            //  String url = getString(R.string.api) + getString(R.string.povlive) + "_table/other_parking_partners";
            String url1;
            if (lat == 0.0 || lang == 0.0) {
                url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=40.74599864,-73.9825673&rankby=distance&distance=50000&sensor=false&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&types=parking";
            } else {
                url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + lat + "," + lang + "&rankby=distance&distance=50000&sensor=false&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&types=parking";
            }


            DefaultHttpClient client1 = new DefaultHttpClient();
            HttpGet post1 = new HttpGet(url1);
            StringEntity entity = null;
            try {
                entity = new StringEntity(studentsObj.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            HttpResponse response1 = null;

            try {
                response1 = client1.execute(post1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response1 != null) {
                try {


                    HttpEntity resEntity1 = response1.getEntity();
                    String responseStr1 = null;

                    responseStr1 = EntityUtils.toString(resEntity1).trim();
                    json1 = new JSONObject(responseStr1);


                    JSONArray googleparking = json1.getJSONArray("results");

                    for (int j = 0; j < googleparking.length(); j++) {
                        item item = new item();
                        JSONObject google = googleparking.getJSONObject(j);
                        boolean pid = google.has("place_id");
                        if (pid) {
                            String place_id = google.getString("place_id");
                            for (int i = 0; i < googlepartnerarray.size(); i++) {
                                String plac_id = googlepartnerarray.get(i).getPlace_id();
                                if (place_id.equals(plac_id)) {
                                    String loccode = googlepartnerarray.get(i).getLocation_code();
                                    for (int k = 0; k < commercial_parking_rules.size(); k++) {
                                        String locationcode = commercial_parking_rules.get(k).getLocation_code();
                                        if (loccode.equals(locationcode)) {
                                            places_id_match = true;
                                            item.setLocation_code(googlepartnerarray.get(i).getLocation_code());
                                            item.setId(googlepartnerarray.get(i).getId());
                                            item.setWeekday_12hr_price(commercial_parking_rules.get(k).getPricing());
                                            Log.e("location_code pr", googlepartnerarray.get(i).getLocation_code() + " " + commercial_parking_rules.get(k).getPricing());
                                            break;
                                        }
                                    }
                                }
                            }
                            item.setPlace_id(place_id);

                        } else {
                            places_id_match = false;
                            item.setPlace_id("null");
                        }
                        if (places_id_match) {
                            places_id_match = false;
                            item.setMarker("partner");
                        } else {
                            item.setMarker("google");
                        }
                        String geo = google.getString("geometry");
                        JSONObject geometry = new JSONObject(geo);
                        String location = geometry.getString("location");
                        JSONObject lant = new JSONObject(location);
                        String lat = lant.getString("lat");
                        String lang = lant.getString("lng");
                        String name = google.getString("name");
                        if (google.has("vicinity")) {
                            String vicinity = google.getString("vicinity");
                            item.setVicinity(vicinity);
                        } else {
                            item.setVicinity("");
                        }
                        boolean openh = google.has("opening_hours");
                        item.setGooglelat(lat);
                        item.setGooglelan(lang);
                        item.setName(name);


                        if (openh) {
                            String opening_hours = google.getString("opening_hours");
                            JSONObject openig_h = new JSONObject(opening_hours);

                            String open = openig_h.getString("open_now");
                            item.setOpen_now(open);
                        } else {
                            item.setOpen_now("null");
                        }
                        googleparkingarray.add(item);

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json1 != null) {
                if (googleparkingarray.size() >= 0) {
                    commericalmap = "CommericalChange1";

                    ShowCommercialMap();
                }
            }
            super.onPostExecute(s);

        }
    }
}
