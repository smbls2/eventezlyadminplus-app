package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import static com.softmeasures.eventezlyadminplus.activity.vchome.adrreslocation;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class frg_create_direct_menu extends Fragment {
    EditText edit_plate_no;
    Button btn_find, btn_create_ticket;
    private list_item_click_hearing listener;
    RelativeLayout rl_progressbar;
    Spinner sp_state;
    ArrayList<item> search_plate_no_list = new ArrayList<>();
    String plate_no;
    String state_name = "Enter State", timeStamp;
    String statearray[] = {"Enter State", "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"};

    public interface list_item_click_hearing {
        public void on_hearing(String code, String fess, String des, String date, boolean is_multiple, ArrayList<item> data);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.frg_create_ticket_direct, container, false);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        edit_plate_no = (EditText) view.findViewById(R.id.edit_plate_number);
        btn_create_ticket = (Button) view.findViewById(R.id.btn_create_ticket);
        sp_state = (Spinner) view.findViewById(R.id.sp_state_plate_search);
        ArrayAdapter<String> sp_state_adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner, R.id.textcity, statearray);
        sp_state.setAdapter(sp_state_adapter);

        sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                state_name = (String) sp_state.getSelectedItem();
                hideKeyboard(view);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_create_ticket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
                plate_no = edit_plate_no.getText().toString();
                if (!plate_no.equals("") && !state_name.equals("Enter State")) {
                    new getplatenowisesearch(plate_no, state_name).execute();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Parkezly Alert!");
                    alertDialog.setMessage("Please enter plate no and select state");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }


            }
        });

        edit_plate_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String ss = s.toString();
                if (!ss.equals(ss.toUpperCase())) {

                    edit_plate_no.setText(ss.toUpperCase());
                    edit_plate_no.setSelection(ss.length());
                }
            }
        });
        return view;
    }

    public void hideKeyboard(View view) {
        Log.e("outtouch1", "yes");
        try {
            if (getActivity() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(edit_plate_no.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class getplatenowisesearch extends AsyncTask<String, String, String> {

        JSONObject json, json1;
        String res_id = "null";
        String streetviewurl;
        String pno, pstate, max_time, entry_time, lat11, long1, exiprytime;
        Date current = null;

        public getplatenowisesearch(String platno, String state) {

            String pla = URLEncoder.encode(platno);
            this.streetviewurl = "_table/parked_cars?filter=(plate_no%20like%20%25" + pla + "%25)%20AND%20(pl_state%3D" + state + ")%20AND%20(parking_status%3D'ENTRY')";
        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            HttpResponse response = null;
            search_plate_no_list.clear();
            try {
                String url = getString(R.string.api) + getString(R.string.povlive) + streetviewurl;
                Log.e("urlsearch_plate_no", url);
                DefaultHttpClient client = new DefaultHttpClient();
                HttpGet post = new HttpGet(url);
                post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
                post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception E) {
                E.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    TimeZone zone = TimeZone.getTimeZone("UTC");
                    sdf.setTimeZone(zone);
                    String currentdate = sdf.format(new Date());
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        current = format.parse(currentdate);
                        Log.e("current_date", String.valueOf(current));
                        System.out.println(current);
                    } catch (android.net.ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray googleparking = json.getJSONArray("resource");
                    for (int j = 0; j < googleparking.length(); j++) {
                        item item = new item();
                        JSONObject c = googleparking.getJSONObject(j);
                        item.setPlate(c.getString("plate_no"));
                        String state = c.getString("pl_state");
                        if (state.equals("") || state.equals("null")) {
                            item.setState(c.getString("state"));
                        } else {
                            item.setState(c.getString("pl_state"));
                        }
                        item.setId(c.getString("id"));
                        item.setLocation_code(c.getString("location_code"));
                        String type = c.getString("parking_type");
                        String lat1 = null, lang = null;

                        String lat2 = c.getString("lat");
                        String lang2 = c.getString("lng");
                        boolean hr = lat2.contains("Optional");
                        if (hr) {
                            Log.e("Optional", "yes");
                            String hr1 = lat2.substring(lat2.indexOf('(') + 1);
                            String hr2 = hr1.substring(0, hr1.indexOf(')'));
                            String hr3 = lang2.substring(lang2.indexOf('(') + 1);
                            String hr4 = hr3.substring(0, hr3.indexOf(')'));
                            lat1 = hr2;
                            lang = hr4;

                        } else {
                            lat1 = lat2;
                            lang = lang2;
                        }

                        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date exitre = null;
                        try {
                            exitre = format1.parse(c.getString("expiry_time"));

                        } catch (android.net.ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (java.text.ParseException e) {
                            e.printStackTrace();
                        }
                        String status = c.getString("ticket_status");
                        String carcolor;
                        if (!type.equals("anywhere")) {
                            if (current.after(exitre)) {
                                if (type.equals("anywhere")) {
                                    carcolor = "status-anywhere";
                                } else {
                                    if (status.equals("TICKETED")) {
                                        carcolor = "status-red_tic";
                                    } else {
                                        carcolor = "status-red";
                                    }
                                }
                            } else {

                                carcolor = "status-green";

                                Log.d("current", String.valueOf(current));
                                Log.d("ex", String.valueOf(exitre));
                                System.out.println(current);
                                System.out.println(exitre);
                            }
                        } else {
                            carcolor = "status-anywhere";
                        }
                        item.setAddress(c.getString("marker_address1"));
                        item.setAddesss1(c.getString("address1"));
                        item.setCity(c.getString("city"));
                        item.setCourt_id(c.getString("country"));
                        item.setDate(c.getString("entry_date_time"));
                        item.setExdate(c.getString("expiry_time"));
                        item.setExit_time(c.getString("exit_date_time"));
                        item.setMismatch(c.getString("mismatch"));
                        item.setLat(lat1);
                        item.setLang(lang);
                        item.setCategory(carcolor);
                        item.setTicket_satus(c.getString("ticket_status"));
                        item.setMarker(c.getString("parking_type"));
                        item.setMax_time(c.getString("max_duration"));
                        item.setMarker_type("4");
                        item.setParking_type(type);
                        item.setLot_row(c.getString("lot_row"));
                        item.setLot_no(c.getString("lot_number"));
                        search_plate_no_list.add(item);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);

            Resources rs;

            if (getActivity() != null && json != null) {

                if (search_plate_no_list.size() > 0) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Vehicle Parked using app");
                    alertDialog.setMessage("This vehicle is parked using app please locate and create ticket after that");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                } else {
                    Bundle b = new Bundle();
                    b.putString("plate", "Plate Number - " + plate_no);
                    b.putString("state", "State - " + state_name);
                    b.putString("plat1", plate_no);
                    b.putString("state1", state_name);
                    b.putString("row", "");
                    b.putString("row_no", "");
                    b.putString("marker", "status-red");
                    b.putString("is_direct", "yes");
                    b.putString("parked_car_address", "Parked Address:- " + adrreslocation);
                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    create_ticket_main pay = new create_ticket_main();
                    pay.setArguments(b);
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();

                }
            }
            super.onPostExecute(s);
        }
    }


}
