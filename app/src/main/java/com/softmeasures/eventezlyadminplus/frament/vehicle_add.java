package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.concurrent.NotThreadSafe;

//import org.apache.http.annotation.NotThreadSafe;

public class vehicle_add extends Fragment {
    ProgressDialog pdialog;
    ArrayList<item> vehiclearry = new ArrayList<>();
    CustomAdaptercity adpater;
    ListView listofvehicleno;
    TextView txttitle, btnaddvehicle, txtshkip;
    static RelativeLayout addvehicle;
    Spinner spsate;
    EditText editetxtplatno;
    String statename, platno, id, CountryName = "";
    Button btnsave;
    int i = 1;
    ConnectionDetector cd;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar, rl_list;
    Animation animation;
    ImageView btn_delete;
    Boolean getvehicle = false;
    private vehivcle_add_Click_listener listener;
    String is_direct_add = "no";

    ArrayList<item> country_array = new ArrayList<>();
    ArrayList<item> State_array = new ArrayList<>();

    Spinner spinnerCountry;
    TextView edt_Make, edt_Model, edt_Year, edt_Color;
    CustomStateAdapter customStateAdapter;
    CustomCountryAdapter customCountryAdapter;
    Double latitude = 0.0, longitude = 0.0;
    Geocoder geocoder;
    List<Address> addresses;
    String c_address, c_city, c_state, c_country, c_postalCode, c_knownName;
    LinearLayout llMore, llMoreView;
    TextView txt_More;

    ArrayList<item> Array_make = new ArrayList<>();
    ArrayList<item> Array_model = new ArrayList<>();
    ArrayList<item> Array_year = new ArrayList<>();
    ArrayList<item> Array_body_style = new ArrayList<>();
    ArrayList<item> Array_color = new ArrayList<>();

    public static String make = "", model = "", year = "", color = "", body_type = "";
    StringBuffer str_mak_model_year = new StringBuffer();
    StringBuffer year_color = new StringBuffer();
    PopupWindow popupcolor;
    ArrayList<item> popup_window_show = new ArrayList<>();

    String v_country = "", v_state = "", v_color = "", v_make = "", v_model = "", v_year = "";

    public Boolean isShowPopup = true;

    public interface vehivcle_add_Click_listener {
        public void Onitem_save();

        public void Onitem_delete();
    }

    public void registerForListener(vehivcle_add_Click_listener listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_vehicle, container, false);

        listofvehicleno = (ListView) view.findViewById(R.id.listofvehicle);
        txttitle = (TextView) view.findViewById(R.id.txttitte);
        addvehicle = (RelativeLayout) view.findViewById(R.id.addvehicle);
        btnaddvehicle = (TextView) view.findViewById(R.id.txtaddvehicle);
        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Thin.ttf");
        txttitle.setTypeface(type);
        spsate = (Spinner) view.findViewById(R.id.spinnerstate);
        editetxtplatno = (EditText) view.findViewById(R.id.edittext_vehicle);
        btnsave = (Button) view.findViewById(R.id.btnsave);
        txtshkip = (TextView) view.findViewById(R.id.textskip);
        txttitle.setText("My Vehicles");
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        rl_list = (RelativeLayout) view.findViewById(R.id.rl_list);
        btn_delete = (ImageView) view.findViewById(R.id.btn_delete);
        cd = new ConnectionDetector(getActivity());
        RelativeLayout main = (RelativeLayout) view.findViewById(R.id.main);

        spinnerCountry = (Spinner) view.findViewById(R.id.spinnerCountry);
        edt_Make = (TextView) view.findViewById(R.id.edt_Make);
        edt_Model = (TextView) view.findViewById(R.id.edt_Model);
        edt_Year = (TextView) view.findViewById(R.id.edt_Year);
        edt_Color = (TextView) view.findViewById(R.id.edt_Color);
        llMore = (LinearLayout) view.findViewById(R.id.llMore);
        txt_More = (TextView) view.findViewById(R.id.txt_More);
        llMoreView = (LinearLayout) view.findViewById(R.id.llMoreView);

        llMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txt_More.getText().toString().equalsIgnoreCase("More Options..")) {
                    txt_More.setText("Hide Options..");
                    llMoreView.setVisibility(View.VISIBLE);

                    if (isShowPopup) {
                        isShowPopup = false;

                        str_mak_model_year = new StringBuffer();
                        if (Array_make.size() <= 0) {
                            new getModel().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            ShowMakePupup(getActivity(), Array_make);
                        }
                    }
                } else {
                    txt_More.setText("More Options..");
                    llMoreView.setVisibility(View.GONE);
                }
            }
        });


        edt_Make.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_mak_model_year = new StringBuffer();
                if (Array_make.size() <= 0) {
                    new getModel().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    ShowMakePupup(getActivity(), Array_make);
                }
            }
        });

        edt_Model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Array_make.size() <= 0) {
                    new getModel().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    ArrayList<item> temp_model = new ArrayList<item>();
                    for (int i = 0; i < Array_model.size(); i++) {
                        String make_ = Array_model.get(i).getPlace_id();
                        if (make_.equals(make)) {
                            temp_model.add(Array_model.get(i));
                        }
                    }

                    ArrayList<item> Temp_array_model = new ArrayList<>();
                    boolean is_found_model = false;
                    for (int i = 0; i < temp_model.size(); i++) {
                        String title = temp_model.get(i).getState();
                        for (int k = 0; k < Temp_array_model.size(); k++) {
                            String titleq = Temp_array_model.get(k).getState();
                            if (title.equals(titleq)) {
                                is_found_model = true;
                            }
                        }
                        if (!is_found_model) {
                            Temp_array_model.add(temp_model.get(i));
                        } else {
                            is_found_model = false;
                        }
                    }

                    if (Temp_array_model.size() > 0) {
                        temp_model.clear();
                        temp_model.addAll(Temp_array_model);
                    }

                    if (temp_model.size() > 0) {
                        ShowModelPupup(getActivity(), temp_model);
                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Parkezly Alert!");
                        alertDialog.setMessage("No Model available for this " + make + ",choose another Make");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }
                }
            }
        });

        edt_Year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Array_make.size() <= 0) {
                    new getModel().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    year_color = new StringBuffer();
                    ShowyearPupup(getActivity(), Array_year);
                }
            }
        });

        edt_Color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Array_make.size() <= 0) {
                    new getModel().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    ShowcolorPupup(getActivity(), Array_color);
                }
            }
        });


        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        addvehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
            }
        });

        SharedPreferences add1 = getActivity().getSharedPreferences("addvehicle", Context.MODE_PRIVATE);
        final String addve = add1.getString("addvehicle", "no");

        txttitle.setText("My Vehicle");

        if (cd.isConnectingToInternet()) {
            spsate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    statename = State_array.get(position).getCountry_name();
                    Log.e("statename", " : " + statename);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    CountryName = country_array.get(position).getCountry_name();
                    if (!CountryName.equals("Select Country")) {
                        try {
                            new get_country_state(CountryName).execute();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (customStateAdapter != null) {
                            State_array.clear();
                            item mo1 = new item();
                            mo1.setCountry_code("State");
                            mo1.setCountry_name("State");
                            State_array.add(mo1);
                            customStateAdapter.notifyDataSetChanged();
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            btnsave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    platno = editetxtplatno.getText().toString();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    if (platno.equals("")) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Information Missing");
                        alertDialog.setMessage("Please enter the Plate No");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    } else if (statename.equals("State")) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Information Missing");
                        alertDialog.setMessage("Please Select State");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    } else {
                        if (cd.isConnectingToInternet()) {
                            new checkvehicle(platno, statename).execute();
                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("Internet Connection Required");
                            alertDialog.setMessage("Please connect to working Internet connection");
                            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            alertDialog.show();
                        }
                    }
                }
            });

            final Bundle bundle = this.getArguments();
            if (bundle != null) {
                v_country = bundle.getString("v_country", "");
                v_state = bundle.getString("v_state", "");
                v_color = bundle.getString("v_color", "");
                v_make = bundle.getString("v_make", "");
                v_model = bundle.getString("v_model", "");
                v_year = bundle.getString("v_year", "");

                statename = v_state;

                make = v_make;
                edt_Make.setText("" + make);
                model = v_model;
                edt_Model.setText("" + model);
                year = v_year;
                edt_Year.setText("" + year);
                color = v_color;
                edt_Color.setText("" + color);

                llMoreView.setVisibility(View.VISIBLE);

                String plate_no = bundle.getString("plate_no");
                editetxtplatno.setText(plate_no);
                id = bundle.getString("id");
                is_direct_add = bundle.getString("direct_add", "no");
            }

            txtshkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bundle != null) {
                        String pno = bundle.getString("plate_no");
                        new checkvehicleparked(pno, statename, id).execute();
                    }

                }
            });

            if (v_color.equalsIgnoreCase("") || v_color.isEmpty()) {
                String[] colorNames = getResources().getStringArray(R.array.colorNames);
                for (int i = 0; i < colorNames.length; i++) {
                    TypedArray ta = getResources().obtainTypedArray(R.array.colors);
                    int colorToUse = ta.getResourceId(i, 0);

                    item i1 = new item();
                    i1.setState(colorNames[i]);
                    i1.setSelected_parking(false);
                    i1.setImg_id(colorToUse);
                    Array_color.add(i1);
                }
            } else {
                String[] colorNames = getResources().getStringArray(R.array.colorNames);
                for (int i = 0; i < colorNames.length; i++) {
                    if (v_color.equalsIgnoreCase(colorNames[i])) {
                        TypedArray ta = getResources().obtainTypedArray(R.array.colors);
                        int colorToUse = ta.getResourceId(i, 0);

                        item i1 = new item();
                        i1.setState(colorNames[i]);
                        i1.setSelected_parking(true);
                        i1.setImg_id(colorToUse);
                        Array_color.add(i1);
                    } else {
                        TypedArray ta = getResources().obtainTypedArray(R.array.colors);
                        int colorToUse = ta.getResourceId(i, 0);

                        item i1 = new item();
                        i1.setState(colorNames[i]);
                        i1.setSelected_parking(false);
                        i1.setImg_id(colorToUse);
                        Array_color.add(i1);
                    }
                }
            }


            if (is_direct_add.equals("yes")) {
                txtshkip.setEnabled(false);
                txtshkip.setAlpha((float) 0.3);
            } else {
                txtshkip.setEnabled(true);
                txtshkip.setAlpha((float) 1.0);
            }
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }

        return view;
    }

    public class deletevehicle extends AsyncTask<String, String, String> {
        String locationurl;
        String id;

        public deletevehicle(String id) {
            locationurl = "_table/user_vehicles";
            this.id = id;
        }

        String gf = "null";
        JSONObject json, json1;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        JSONArray array = null;

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", id);
            JSONObject json1 = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + locationurl;
            Log.e("delete_uel", url);

            try {
                HttpEntity entity = new StringEntity(json1.toString());
                HttpClient httpClient = new DefaultHttpClient();
                HttpDeleteWithBody httpDeleteWithBody = new HttpDeleteWithBody(url);
                httpDeleteWithBody.setHeader("X-DreamFactory-Application-Name", "parkezly");
                httpDeleteWithBody.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
                httpDeleteWithBody.setEntity(entity);

                HttpResponse response = httpClient.execute(httpDeleteWithBody);
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        gf = c.getString("id");
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json != null) {
                for (int i = 0; i < vehiclearry.size(); i++) {
                    if (vehiclearry.get(i).isCheckboxselected()) {
                        getvehicle = true;
                        delete();
                        break;
                    }
                }
                if (getvehicle != true) {
                    new getvehiclenumber().execute();
                }
            }
            super.onPostExecute(s);
        }
    }

    @NotThreadSafe
    class HttpDeleteWithBody extends HttpEntityEnclosingRequestBase {
        public static final String METHOD_NAME = "DELETE";

        public String getMethod() {
            return METHOD_NAME;
        }

        public HttpDeleteWithBody(final String uri) {
            super();
            setURI(URI.create(uri));
        }

        public HttpDeleteWithBody(final URI uri) {
            super();
            setURI(uri);
        }

        public HttpDeleteWithBody() {
            super();
        }
    }

    public class deletevehicle1 extends AsyncTask<String, String, String> {
        String locationurl;
        String id;

        public deletevehicle1(String id) {
            locationurl = "_table/user_vehicles";
            this.id = id;
        }

        String gf = "null";
        //String locationurl="_table/user_locations";
        JSONObject json, json1;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        JSONArray array = null;

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", id);
            JSONObject json1 = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + locationurl;
            Log.e("delete_uel", url);

            try {
                HttpEntity entity = new StringEntity(json1.toString());
                HttpClient httpClient = new DefaultHttpClient();
                HttpDeleteWithBody httpDeleteWithBody = new HttpDeleteWithBody(url);
                httpDeleteWithBody.setHeader("X-DreamFactory-Application-Name", "parkezly");
                httpDeleteWithBody.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
                httpDeleteWithBody.setEntity(entity);

                HttpResponse response = httpClient.execute(httpDeleteWithBody);
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        gf = c.getString("id");
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);

            Resources rs;
            if (getActivity() != null && json != null) {
                listener.Onitem_delete();
                // new getvehiclenumber().execute();
            }
            super.onPostExecute(s);
        }
    }

    public class getvehiclenumber extends AsyncTask<String, String, String> {
        JSONObject json = null;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        String vehiclenourl = "_table/user_vehicles?filter=user_id%3D" + user_id + "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            vehiclearry.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("get_vehicle_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);
                    Log.e("rerpones", String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        String plate_no = c.getString("plate_no");
                        String state = c.getString("pl_state");
                        String id = c.getString("id");
                        /*Iterator myVeryOwnIterator = statefullname.keySet().iterator();
                        while (myVeryOwnIterator.hasNext()) {
                            String key = (String) myVeryOwnIterator.next();
                            if (key.equals(state)) {
                                String value = (String) statefullname.get(key);
                                item.setState(value);
                                break;
                            }
                        }*/
                        item.setState(state);
                        item.setPlateno(plate_no);
                        item.setId(id);
                        item.setIsselect(false);
                        item.setCheckboxselected(false);
                        vehiclearry.add(item);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity == null) {

                } else {
                    adpater = new CustomAdaptercity(getActivity(), vehiclearry, rs = getResources());
                    listofvehicleno.setAdapter(adpater);
                }
            }
            super.onPostExecute(s);
        }
    }

    public class CustomAdaptercity extends BaseAdapter {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdaptercity(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txtplatno, state, txtid;
            public RelativeLayout rl;
            CheckBox checkBox;

        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.listvehicle, null);
                holder = new ViewHolder();
                holder.txtplatno = (TextView) vi.findViewById(R.id.txtnoplatno);
                holder.state = (TextView) vi.findViewById(R.id.txtstate);
                holder.txtid = (TextView) vi.findViewById(R.id.id);
                holder.rl = (RelativeLayout) vi.findViewById(R.id.rl_m);
                holder.checkBox = (CheckBox) vi.findViewById(R.id.chk_id);
                /************  Set holder with LayoutInflater ************/
                listofvehicleno.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id1) {
                        SharedPreferences add = getActivity().getSharedPreferences("addvehicleback", Context.MODE_PRIVATE);
                        SharedPreferences.Editor ed = add.edit();
                        ed.putString("addvehicleback", "addvehicleback");
                        ed.commit();
                        txttitle.setText("My Vehicle");

                        String platno = vehiclearry.get(position).getPlantno();
                        statename = vehiclearry.get(position).getState();
                        id = vehiclearry.get(position).getId();
                        editetxtplatno.setText(platno);
                        addvehicle.setVisibility(View.VISIBLE);
                        animation = AnimationUtils.loadAnimation(getActivity(),
                                R.anim.sidepannelright);
                        addvehicle.startAnimation(animation);
                        /*Iterator myVeryOwnIterator = statefullname.keySet().iterator();
                        while (myVeryOwnIterator.hasNext()) {
                            String key = (String) myVeryOwnIterator.next();
                            String value = (String) statefullname.get(key);
                            if (value.equals(statename)) {
                                for (int i = 0; i <= statearray.length; i++) {

                                    String state = statearray[i];
                                    if (state == key) {
                                        spsate.setSelection(i);
                                        statename = state;
                                        break;
                                    }
                                }
                            }
                        }*/
                    }
                });
                vi.setTag(holder);
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
                holder.txtplatno.setText("No Data");
                holder.rl.setVisibility(View.GONE);
            } else {
                /***** Get each Model object from Arraylist ********/
                tempValues = null;
                tempValues = (item) vehiclearry.get(position);
                int k = 0;
                /************  Set Model values in Holder elements ***********/
                String cat = tempValues.getPlantno();
                if (cat == null) {
                    holder.rl.setVisibility(View.GONE);
                    holder.state.setVisibility(View.GONE);
                } else if (cat != null) {
                    holder.rl.setVisibility(View.VISIBLE);
                    Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvetica-neue-medium.ttf");
                    holder.txtplatno.setTypeface(type);
                    holder.state.setTypeface(type);
                    holder.txtplatno.setText(tempValues.getPlantno().toUpperCase());
                    holder.state.setText(tempValues.getState());
                    holder.txtid.setText(tempValues.getId());
                    if (tempValues.Getisselect() == false) {
                        holder.checkBox.setVisibility(View.GONE);
                    } else {
                        holder.checkBox.setVisibility(View.VISIBLE);
                    }
                    if (tempValues.isCheckboxselected()) {
                        holder.checkBox.setChecked(true);
                    } else {
                        holder.checkBox.setChecked(false);
                    }
                    holder.checkBox.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (vehiclearry.get(position).isCheckboxselected()) {
                                vehiclearry.get(position).setCheckboxselected(false);
                                holder.checkBox.setChecked(false);
                            } else {
                                vehiclearry.get(position).setCheckboxselected(true);
                                holder.checkBox.setChecked(true);
                                btn_delete.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
            }
            return vi;
        }
    }

    public class checkvehicle extends AsyncTask<String, String, String> {
        JSONObject json = null;
        String vehiclenourl;
        JSONArray array;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");

        public checkvehicle(String pno, String state) {
            try {
                vehiclenourl = "_table/user_vehicles?filter=(plate_no%3D" + URLEncoder.encode(pno, "utf-8") + ")%20AND%20(pl_state%3D" + URLEncoder.encode(state, "utf-8") + ")";
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            vehiclearry.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json != null) {
                if (!array.isNull(0) || array.length() > 0) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Duplicate plate number");
                    alertDialog.setMessage("Plate number that you have entered already exist in record for your state.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                } else {
                    new addvehicle().execute();
                }
            }
            super.onPostExecute(s);
        }
    }

    public class addvehicle extends AsyncTask<String, String, String> {
        String ss = "null";
        JSONObject json;
        String addvehicleurl = "_table/user_vehicles";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
            String user_id = logindeatl.getString("id", "null");
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("plate_no", platno);
            if (id == null) {
                jsonValues.put("id", 0);
            } else {
                jsonValues.put("id", Integer.parseInt(id));
            }

            jsonValues.put("registered_state", statename);
            jsonValues.put("pl_state", statename);
            jsonValues.put("user_id", Integer.parseInt(user_id));

            jsonValues.put("vehicle_color", edt_Color.getText().toString());
            jsonValues.put("vehicle_country", CountryName);
            jsonValues.put("vehicle_make", edt_Make.getText().toString());
            jsonValues.put("vehicle_model", edt_Model.getText().toString());
            jsonValues.put("vehicle_year", edt_Year.getText().toString());

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + addvehicleurl;
            Log.e("add_vehicle", url);
            Log.e("json value", json.toString());
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = null;
            HttpPut put = null;
            if (id == null) {
                post = new HttpPost(url);
                Log.d("add_vehicle_param", json.toString());
                post.setHeader("X-DreamFactory-Application-Name", "parkezly");
                post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            } else {
                put = new HttpPut(url);
                Log.d("ccc", json.toString());
                put.setHeader("X-DreamFactory-Application-Name", "parkezly");
                put.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            }
            //setting json object to post request.
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            if (id == null) {
                post.setEntity(entity);
            } else {
                put.setEntity(entity);
            }

            HttpResponse response = null;
            try {
                if (id == null) {
                    response = client.execute(post);
                } else {
                    response = client.execute(put);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                ss = String.valueOf(response.getStatusLine());
                try {

                    responseStr = EntityUtils.toString(resEntity).trim();
                    if (ss.equals("HTTP/1.1 400 Bad Request")) {
                    } else {
                        JSONObject jso = new JSONObject(responseStr);
                        //String email = jso.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null) {
                SharedPreferences add = getActivity().getSharedPreferences("addvehicleback", Context.MODE_PRIVATE);
                add.edit().clear().commit();
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                txttitle.setText("My Vehicles");
                Resources rs;

                if (ss.equals("HTTP/1.1 400 Bad Request")) {

                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    alertDialogBuilder.setTitle("Sorry");
                    alertDialogBuilder.setMessage("Something wrong");
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            new getvehiclenumber().execute();
                        }
                    });

                    AlertDialog alert = alertDialogBuilder.create();
                    alert.show();

                    animation = AnimationUtils.loadAnimation(getActivity(), R.anim.sidepannelleft);
                    addvehicle.startAnimation(animation);
                    addvehicle.setVisibility(View.GONE);
                    vehiclearry.clear();
                    //listener.Onitem_save();
                } else {
                    if (is_direct_add.equals("yes")) {
                        Bundle bundle = getArguments();
                        if (bundle != null) {
                            String ss = bundle.getString("direct_add");
                            if (ss.equals("yes")) {
                                getActivity().onBackPressed();
                                SharedPreferences add11 = getActivity().getSharedPreferences("addvehicle", Context.MODE_PRIVATE);
                                add11.edit().clear().commit();
                            }
                            return;
                        } else {
                            listener.Onitem_save();
                        }
                    } else {
                        listener.Onitem_save();
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public void delete() {
        getvehicle = false;
        for (int i = 0; i < vehiclearry.size(); i++) {
            if (vehiclearry.get(i).isCheckboxselected()) {
                String id = vehiclearry.get(i).getId();
                String pno = vehiclearry.get(i).getPlantno();
                String state = vehiclearry.get(i).getState();
                vehiclearry.get(i).setCheckboxselected(false);
                String key = state;
               /* Iterator myVeryOwnIterator = statefullname.keySet().iterator();
                while (myVeryOwnIterator.hasNext()) {
                    key = (String) myVeryOwnIterator.next();
                    String value = (String) statefullname.get(key);
                    if (value.equals(state)) {
                        break;
                    }
                }*/
                new checkcardparkedornotwallet(pno, key, id).execute();
                break;
            }
        }

    }

    public class checkcardparkedornotwallet extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONArray array;
        String plno, state, pid;

        public checkcardparkedornotwallet(String plno, String state, String id) {
            this.plno = plno;
            this.state = state;
            this.pid = id;

        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String vechiclenourl = null;
            try {
                vechiclenourl = "_table/parked_cars?filter=(plate_no%3D" + URLEncoder.encode(plno, "utf-8") + ")%20AND%20(pl_state%3D" + state + ")%20AND%20(parking_status%3D'ENTRY')";
            } catch (Exception e) {
                e.printStackTrace();
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + vechiclenourl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (array.length() > 0) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Vehicle already parked!");
                    alertDialog.setMessage("Vehicle that you want to delete is currently parked, please exit from parking before deleting.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            delete();
                        }
                    });
                    alertDialog.show();
                } else {
                    new deletevehicle(pid).execute();
                }

            }
            super.onPostExecute(s);

        }


    }

    public class checkvehicleparked extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONArray array;
        String plno, state, pid;

        public checkvehicleparked(String plno, String state, String id) {
            this.plno = plno;
            this.state = state;
            this.pid = id;

        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String vechiclenourl = null;
            try {
                vechiclenourl = "_table/parked_cars?filter=(plate_no%3D" + URLEncoder.encode(plno, "utf-8") + ")%20AND%20(pl_state%3D" + state + ")%20AND%20(parking_status%3D'ENTRY')";
            } catch (Exception e) {
                e.printStackTrace();
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + vechiclenourl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (array.length() > 0) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Vehicle already parked!");
                    alertDialog.setMessage("Vehicle that you want to delete is currently parked, please exit from parking before deleting.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getActivity().onBackPressed();
                        }
                    });
                    alertDialog.show();
                } else {
                    new deletevehicle1(pid).execute();
                }
            }
            super.onPostExecute(s);
        }
    }

    @Override
    public void onResume() {
        currentlocation();
        super.onResume();
    }

    public class get_country extends AsyncTask<String, String, String> {
        JSONObject json = null;
        String vehiclenourl = "_table/countries";
        JSONArray array;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            country_array.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            item mo1 = new item();
            mo1.setCountry_code("");
            mo1.setCountry_name("Select Country");
            country_array.add(mo1);
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item mo = new item();
                        mo.setCountry_code(c.getString("country_code"));
                        mo.setCountry_name(c.getString("country_name"));
                        country_array.add(mo);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json != null) {
                customCountryAdapter = new CustomCountryAdapter(getActivity(),
                        R.layout.listitems_state, R.id.txt_Name, country_array);
                spinnerCountry.setAdapter(customCountryAdapter);

                if (v_country.equalsIgnoreCase("")) {
                    for (int i = 0; i < country_array.size(); i++) {
                        if (country_array.get(i).getCountry_name().equalsIgnoreCase(c_country)) {
                            spinnerCountry.setSelection(i);
                            break;
                        }
                    }
                } else {
                    for (int i = 0; i < country_array.size(); i++) {
                        if (country_array.get(i).getCountry_name().equalsIgnoreCase(v_country)) {
                            spinnerCountry.setSelection(i);
                            break;
                        }
                    }
                }

            }
            super.onPostExecute(s);
        }
    }

    public class CustomCountryAdapter extends ArrayAdapter<item> {

        LayoutInflater flater;

        public CustomCountryAdapter(Activity context, int resouceId, int textviewId, List<item> list) {
            super(context, resouceId, textviewId, list);
            flater = context.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return rowview(position, convertView, parent);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return rowview(position, convertView, parent);
        }

        private View rowview(int position, View convertView, ViewGroup parent) {
            item rowItem = getItem(position);

            viewHolder holder;
            View rowview = convertView;
            if (rowview == null) {

                holder = new viewHolder();
                flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowview = flater.inflate(R.layout.listitems_state, parent, false);

                holder.txtTitle = (TextView) rowview.findViewById(R.id.txt_Name);
                rowview.setTag(holder);
            } else {
                holder = (viewHolder) rowview.getTag();
            }
            holder.txtTitle.setText(rowItem.getCountry_name());

            return rowview;
        }

        private class viewHolder {
            TextView txtTitle;
        }

    }

    public class get_country_state extends AsyncTask<String, String, String> {
        JSONObject json = null;
        String vehiclenourl = "";
        JSONArray array;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");

        public get_country_state(String Country_name) throws UnsupportedEncodingException {
            vehiclenourl = "_table/country_states?filter=country_name%3D" + URLEncoder.encode(Country_name, "utf-8");
        }

        public get_country_state() throws UnsupportedEncodingException {
            vehiclenourl = "_table/country_states";
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            State_array.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            Log.e("country_state_url", url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            item mo1 = new item();
            mo1.setCountry_code("State");
            mo1.setCountry_name("State");
            State_array.add(mo1);
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item mo = new item();
                        mo.setCountry_code(c.getString("state_2_code"));
                        mo.setCountry_name(c.getString("state_name"));
                        State_array.add(mo);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json != null) {
                customStateAdapter = new CustomStateAdapter(getActivity(),
                        R.layout.listitems_state, R.id.txt_Name, State_array);
                spsate.setAdapter(customStateAdapter);


                if (v_state.equalsIgnoreCase("")) {
                    for (int i = 0; i < State_array.size(); i++) {
                        if (State_array.get(i).getCountry_name().equalsIgnoreCase(c_state)) {
                            spsate.setSelection(i);
                            break;
                        }
                    }
                } else {
                    for (int i = 0; i < State_array.size(); i++) {
                        if (State_array.get(i).getCountry_name().equalsIgnoreCase(v_state)) {
                            spsate.setSelection(i);
                            break;
                        }
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class CustomStateAdapter extends ArrayAdapter<item> {

        LayoutInflater flater;

        public CustomStateAdapter(Activity context, int resouceId, int textviewId, List<item> list) {
            super(context, resouceId, textviewId, list);
            flater = context.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return rowview(position, convertView, parent);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return rowview(position, convertView, parent);
        }

        private View rowview(int position, View convertView, ViewGroup parent) {
            item rowItem = getItem(position);

            viewHolder holder;
            View rowview = convertView;
            if (rowview == null) {

                holder = new viewHolder();
                flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowview = flater.inflate(R.layout.listitems_state, parent, false);

                holder.txtTitle = (TextView) rowview.findViewById(R.id.txt_Name);
                rowview.setTag(holder);
            } else {
                holder = (viewHolder) rowview.getTag();
            }
            holder.txtTitle.setText(rowItem.getCountry_name());

            return rowview;
        }

        private class viewHolder {
            TextView txtTitle;
        }

    }

    public void currentlocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        try {
            if (tracker.canGetLocation()) {
                latitude = tracker.getLatitude();
                longitude = tracker.getLongitude();

                geocoder = new Geocoder(getActivity(), Locale.getDefault());

                addresses = geocoder.getFromLocation(latitude, longitude, 1);

                c_address = addresses.get(0).getAddressLine(0);
                c_city = addresses.get(0).getLocality();
                c_state = addresses.get(0).getAdminArea();
                c_country = addresses.get(0).getCountryName();
                c_postalCode = addresses.get(0).getPostalCode();
                c_knownName = addresses.get(0).getFeatureName();
                new get_country().execute();
            } else {
                new get_country().execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
            new get_country().execute();
        }

    }

    public class getModel extends AsyncTask<String, String, String> {
        JSONObject json = null;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "0");
        String vehiclenourl = "_table/car_models";
        String res_id;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item i_year = new item();
                        item i_make = new item();
                        item i_model = new item();
                        item i_body = new item();

                        JSONObject c = array.getJSONObject(i);
                        String make = c.getString("make");
                        make = make != null ? (!make.equals("") ? (!make.equals("null") ? make : "") : "") : "";
                        if (!make.equals("")) {
                            if (v_make.equalsIgnoreCase("")) {
                                i_make.setState(make);
                                i_make.setSelected_parking(false);
                                Array_make.add(i_make);
                            } else {
                                if (v_make.equalsIgnoreCase(make)) {
                                    i_make.setState(make);
                                    i_make.setSelected_parking(true);
                                    Array_make.add(i_make);
                                } else {
                                    i_make.setState(make);
                                    i_make.setSelected_parking(false);
                                    Array_make.add(i_make);
                                }
                            }
                        }

                        String model = c.getString("model");
                        model = model != null ? (!model.equals("") ? (!model.equals("null") ? model : "") : "") : "";
                        if (!model.equals("")) {
                            if (v_model.equalsIgnoreCase("")) {
                                i_model.setState(model);
                                i_model.setPlace_id(make);
                                i_model.setSelected_parking(false);
                                Array_model.add(i_model);
                            } else {
                                if (v_model.equalsIgnoreCase(model)) {
                                    i_model.setState(model);
                                    i_model.setPlace_id(make);
                                    i_model.setSelected_parking(true);
                                    Array_model.add(i_model);
                                } else {
                                    i_model.setState(model);
                                    i_model.setPlace_id(make);
                                    i_model.setSelected_parking(false);
                                    Array_model.add(i_model);
                                }
                            }
                        }

                        String boday_style = c.getString("body_style");
                        boday_style = boday_style != null ? (!boday_style.equals("") ? (!boday_style.equals("null") ? boday_style : "") : "") : "";
                        if (!boday_style.equals("")) {
                            i_body.setState(boday_style);
                            i_body.setPlace_id(make);
                            i_body.setSelected_parking(false);
                            Array_body_style.add(i_body);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs;
            rl_progressbar.setVisibility(View.GONE);
            if (json != null) {
                Activity activity = getActivity();
                if (activity == null) {
                } else {
                    if (json != null) {
                        int year = Calendar.getInstance().get(Calendar.YEAR) + 1;

                        if (v_year.equalsIgnoreCase("")) {
                            for (int i = 1950; i <= year; i++) {
                                item i_year = new item();
                                i_year.setState(String.valueOf(i));
                                i_year.setSelected_parking(false);
                                Array_year.add(i_year);
                            }
                        } else {
                            for (int i = 1950; i <= year; i++) {
                                if (v_year.equalsIgnoreCase(String.valueOf(i))) {
                                    item i_year = new item();
                                    i_year.setState(String.valueOf(i));
                                    i_year.setSelected_parking(true);
                                    Array_year.add(i_year);
                                } else {
                                    item i_year = new item();
                                    i_year.setState(String.valueOf(i));
                                    i_year.setSelected_parking(false);
                                    Array_year.add(i_year);
                                }
                            }
                        }

                        ArrayList<item> Temp_array_make = new ArrayList<>();
                        boolean is_found_make = false;
                        for (int i = 0; i < Array_make.size(); i++) {
                            String title = Array_make.get(i).getState();
                            for (int k = 0; k < Temp_array_make.size(); k++) {
                                String titleq = Temp_array_make.get(k).getState();
                                if (title.equals(titleq)) {
                                    is_found_make = true;
                                }
                            }
                            if (!is_found_make) {
                                Temp_array_make.add(Array_make.get(i));
                            } else {
                                is_found_make = false;
                            }
                        }

                        if (Temp_array_make.size() > 0) {
                            Array_make.clear();
                            Array_make.addAll(Temp_array_make);
                        }
                        ShowMakePupup(getActivity(), Array_make);
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    private void ShowMakePupup(Activity context, final ArrayList<item> data) {

        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);
        RelativeLayout rl_view1 = (RelativeLayout) context.findViewById(R.id.title);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.mek_model_popup_pre, viewGroup, false);
        final PopupWindow popupmanaged = new PopupWindow(context);
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);
        Button btnparkhere;
        ImageView imageclose;
        final GridView grid_managed;
        RelativeLayout rl_popclose, rl_done;
        TextView txt_title;
        ImageView img_state;
        TextView txt_state_;
        txt_title = (TextView) layout.findViewById(R.id.txt_title);
        btnparkhere = (Button) layout.findViewById(R.id.btn_managed_park);
        grid_managed = (GridView) layout.findViewById(R.id.grid_managed);
        imageclose = (ImageView) layout.findViewById(R.id.img_managed_close);
        rl_done = (RelativeLayout) layout.findViewById(R.id.rl_done);
        rl_popclose = (RelativeLayout) layout.findViewById(R.id.rl_pop);
        rl_popclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });
        img_state = (ImageView) layout.findViewById(R.id.img_managed_car);
        txt_state_ = (TextView) layout.findViewById(R.id.txt_row_no1);
        Resources rs;
        final make_adapter adpter = new make_adapter(getActivity(), data, rs = getResources());
        grid_managed.setAdapter(adpter);
        for (int k = 0; k < data.size(); k++) {
            boolean selected_ore = data.get(k).isSelected_parking();

            if (selected_ore) {
                String statename = data.get(k).getState();
                img_state.setImageResource(R.mipmap.popup_car_green);
                txt_state_.setText(statename);
                break;
            }
        }

        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });

        rl_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                grid_managed.performClick();
            }
        });

        grid_managed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < data.size(); i++) {
                    data.get(i).setSelected_parking(false);
                }
                data.get(position).setSelected_parking(true);
                adpter.notifyDataSetChanged();
                popupmanaged.dismiss();
                make = data.get(position).getState();
                edt_Make.setText("" + make);

                str_mak_model_year.append(data.get(position).getState() + ",");
                ArrayList<item> temp_model = new ArrayList<item>();
                rl_progressbar.setVisibility(View.VISIBLE);
                for (int i = 0; i < Array_model.size(); i++) {
                    String make_ = Array_model.get(i).getPlace_id();
                    if (make_.equals(make)) {
                        temp_model.add(Array_model.get(i));

                    }
                }

                ArrayList<item> Temp_array_model = new ArrayList<>();
                boolean is_found_model = false;
                for (int i = 0; i < temp_model.size(); i++) {
                    String title = temp_model.get(i).getState();
                    for (int k = 0; k < Temp_array_model.size(); k++) {
                        String titleq = Temp_array_model.get(k).getState();
                        if (title.equals(titleq)) {
                            is_found_model = true;
                        }
                    }
                    if (!is_found_model) {

                        Temp_array_model.add(temp_model.get(i));
                    } else {
                        is_found_model = false;
                    }

                }

                if (Temp_array_model.size() > 0) {
                    temp_model.clear();
                    temp_model.addAll(Temp_array_model);
                }
                if (temp_model.size() > 0) {
                    ShowModelPupup(getActivity(), temp_model);
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Parkezly Alert!");
                    alertDialog.setMessage("No Model available for this " + make + ",choose another Make");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
        });
        txt_title.setText("Select Make");
    }

    public class make_adapter extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        boolean isselect = false;
        int a = 0;
        private Activity activity;
        private ArrayList<item> data;
        private SparseBooleanArray mCheckStates;
        private LayoutInflater inflater = null;

        public make_adapter(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_make_popup, null);
                holder = new ViewHolder();
                holder.txt_state = (TextView) vi.findViewById(R.id.txt_state);
                holder.img = (ImageView) vi.findViewById(R.id.img_state);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {

            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String cat = tempValues.getState();
                if (cat == null) {
                } else if (cat != null) {

                    if (tempValues.isSelected_parking()) {
                        holder.img.setImageResource(R.mipmap.popup_car_green);
                        holder.txt_state.setTextColor(getResources().getColor(R.color.selected_text));
                    } else {
                        holder.img.setImageResource(R.mipmap.popup_gree_car);
                        holder.txt_state.setTextColor(Color.parseColor("#000000"));
                    }

                    holder.txt_state.setText(tempValues.getState());
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txt_state;
            ImageView img;
        }
    }

    private void ShowModelPupup(Activity context, final ArrayList<item> data) {
        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);
        RelativeLayout rl_view1 = (RelativeLayout) context.findViewById(R.id.title);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.mek_model_popup_pre, viewGroup, false);
        final PopupWindow popupmanaged = new PopupWindow(context);
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);
        Button btnparkhere;
        ImageView imageclose;
        final GridView grid_managed;
        RelativeLayout rl_popclose, rl_done;
        TextView txt_title;
        ImageView img_state;
        TextView txt_state_;
        txt_title = (TextView) layout.findViewById(R.id.txt_title);
        btnparkhere = (Button) layout.findViewById(R.id.btn_managed_park);
        grid_managed = (GridView) layout.findViewById(R.id.grid_managed);
        imageclose = (ImageView) layout.findViewById(R.id.img_managed_close);
        img_state = (ImageView) layout.findViewById(R.id.img_managed_car);
        txt_state_ = (TextView) layout.findViewById(R.id.txt_row_no1);
        rl_popclose = (RelativeLayout) layout.findViewById(R.id.rl_pop);
        rl_done = (RelativeLayout) layout.findViewById(R.id.rl_done);
        rl_popclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });
        txt_title.setText("Select Model");
        Resources rs;
        rl_progressbar.setVisibility(View.GONE);
        final make_adapter adpter = new make_adapter(getActivity(), data, rs = getResources());
        grid_managed.setAdapter(adpter);

        for (int k = 0; k < data.size(); k++) {
            boolean selected_ore = data.get(k).isSelected_parking();

            if (selected_ore) {
                String statename = data.get(k).getState();
                img_state.setImageResource(R.mipmap.popup_car_green);
                txt_state_.setText(statename);
                break;
            }
        }
        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });

        rl_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                grid_managed.performClick();
            }
        });

        grid_managed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < data.size(); i++) {
                    data.get(i).setSelected_parking(false);
                }
                data.get(position).setSelected_parking(true);
                adpter.notifyDataSetChanged();
                popupmanaged.dismiss();
                str_mak_model_year.append(data.get(position).getState() + ",");
                model = data.get(position).getState();
                edt_Model.setText("" + model);

                ArrayList<item> temp_boday = new ArrayList<item>();
                for (int i = 0; i < Array_body_style.size(); i++) {
                    String make_ = Array_body_style.get(i).getPlace_id();
                    if (make_.equals(make)) {
                        temp_boday.add(Array_body_style.get(i));

                    }
                }

                ArrayList<item> Temp_array_boday = new ArrayList<>();
                boolean is_found_body = false;
                for (int i = 0; i < temp_boday.size(); i++) {
                    String title = temp_boday.get(i).getState();
                    for (int kk = 0; kk < Temp_array_boday.size(); kk++) {
                        String titleq = Temp_array_boday.get(kk).getState();
                        if (title.equals(titleq)) {
                            is_found_body = true;
                        }
                    }
                    if (!is_found_body) {

                        Temp_array_boday.add(temp_boday.get(i));
                    } else {
                        is_found_body = false;
                    }

                }

                if (Temp_array_boday.size() > 0) {
                    temp_boday.clear();
                    temp_boday.addAll(Temp_array_boday);
                }

                year_color = new StringBuffer();
                ShowyearPupup(getActivity(), Array_year);

                /*if (temp_boday.size() > 0) {
                    ShowBodyPupup(getActivity(), temp_boday);
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Parkezly Alert!");
                    alertDialog.setMessage("No Body Style available for this " + make + ",choose another Make");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }*/
            }
        });

    }

    private void ShowBodyPupup(Activity context, final ArrayList<item> data) {

        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);
        RelativeLayout rl_view1 = (RelativeLayout) context.findViewById(R.id.title);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.mek_model_popup_pre, viewGroup, false);
        final PopupWindow popupmanaged = new PopupWindow(context);
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);
        Button btnparkhere;
        ImageView imageclose;
        GridView grid_managed;
        RelativeLayout rl_popclose, rl_done;
        TextView txt_title;
        ImageView img_state;
        TextView txt_state_;
        txt_title = (TextView) layout.findViewById(R.id.txt_title);
        grid_managed = (GridView) layout.findViewById(R.id.grid_managed);
        imageclose = (ImageView) layout.findViewById(R.id.img_managed_close);
        img_state = (ImageView) layout.findViewById(R.id.img_managed_car);
        txt_state_ = (TextView) layout.findViewById(R.id.txt_row_no1);
        rl_popclose = (RelativeLayout) layout.findViewById(R.id.rl_pop);
        rl_popclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });
        rl_done = (RelativeLayout) layout.findViewById(R.id.rl_done);
        Resources rs;
        final make_adapter adpter = new make_adapter(getActivity(), data, rs = getResources());
        grid_managed.setAdapter(adpter);

        txt_title.setText("Select Body Style");
        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });

        for (int k = 0; k < data.size(); k++) {
            boolean selected_ore = data.get(k).isSelected_parking();

            if (selected_ore) {
                String statename = data.get(k).getState();
                img_state.setImageResource(R.mipmap.popup_car_green);
                txt_state_.setText(statename);
                break;
            }
        }

        rl_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int k = 0; k < data.size(); k++) {
                    boolean selected_ore = data.get(k).isSelected_parking();
                    if (selected_ore) {
                        int position = k;

                        str_mak_model_year.append(data.get(position).getState());
                        body_type = data.get(position).getState();
                        popupmanaged.dismiss();
                        Log.e("make", str_mak_model_year.toString());

                        for (int i = 0; i < popup_window_show.size(); i++) {
                            boolean is_open = popup_window_show.get(i).isSelected_parking();
                            if (!is_open) {
                                String name = popup_window_show.get(i).getName();
                                popup_window_show.get(i).setSelected_parking(true);
                                //ShowPopup(name);
                                break;
                            }
                        }
                    }
                }
            }
        });

        grid_managed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < data.size(); i++) {
                    data.get(i).setSelected_parking(false);
                }
                data.get(position).setSelected_parking(true);
                adpter.notifyDataSetChanged();
                str_mak_model_year.append(data.get(position).getState());
                body_type = data.get(position).getState();
                popupmanaged.dismiss();
                Log.e("make", str_mak_model_year.toString());

                for (int i = 0; i < popup_window_show.size(); i++) {
                    boolean is_open = popup_window_show.get(i).isSelected_parking();
                    if (!is_open) {
                        String name = popup_window_show.get(i).getName();
                        popup_window_show.get(i).setSelected_parking(true);
                        //ShowPopup(name);
                        break;
                    }
                }

            }
        });

    }

    private void ShowyearPupup(Activity context, final ArrayList<item> data) {

        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);
        RelativeLayout rl_view1 = (RelativeLayout) context.findViewById(R.id.title);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.statepopup_pre, viewGroup, false);
        final PopupWindow popupmanaged = new PopupWindow(context);
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);
        Button btnparkhere;
        ImageView imageclose;
        GridView grid_managed;
        RelativeLayout rl_popclose, rl_done;
        TextView txt_title;
        ImageView img_state;
        TextView txt_state_;
        txt_title = (TextView) layout.findViewById(R.id.txt_title);
        grid_managed = (GridView) layout.findViewById(R.id.grid_managed);
        imageclose = (ImageView) layout.findViewById(R.id.img_managed_close);
        img_state = (ImageView) layout.findViewById(R.id.img_state);
        txt_state_ = (TextView) layout.findViewById(R.id.txt_state_name);
        rl_popclose = (RelativeLayout) layout.findViewById(R.id.rl_pop);
        rl_popclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });
        rl_done = (RelativeLayout) layout.findViewById(R.id.rl_done);
        Resources rs;
        final year_adapter adpter = new year_adapter(getActivity(), data, rs = getResources());
        grid_managed.setAdapter(adpter);

        txt_title.setText("Select Year");
        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });

        for (int k = 0; k < data.size(); k++) {
            boolean selected_ore = data.get(k).isSelected_parking();

            if (selected_ore) {
                String statename = data.get(k).getState();
                img_state.setImageResource(R.mipmap.icon_year);
                txt_state_.setText(statename);
                break;
            }
        }

        rl_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int k = 0; k < data.size(); k++) {
                    boolean selected_ore = data.get(k).isSelected_parking();
                    if (selected_ore) {
                        popupmanaged.dismiss();
                        int position = k;
                        year = data.get(position).getState();
                        edt_Year.setText("" + year);
                        year_color.append(data.get(position).getState() + ",");
                        ShowcolorPupup(getActivity(), Array_color);
                    }
                }
            }
        });

        grid_managed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < data.size(); i++) {
                    data.get(i).setSelected_parking(false);
                }
                data.get(position).setSelected_parking(true);
                adpter.notifyDataSetChanged();
                popupmanaged.dismiss();
                year = data.get(position).getState();
                edt_Year.setText("" + year);
                year_color.append(data.get(position).getState() + ",");
                ShowcolorPupup(getActivity(), Array_color);
            }
        });

    }

    public class year_adapter extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        boolean isselect = false;
        int a = 0;
        private Activity activity;
        private ArrayList<item> data;
        private SparseBooleanArray mCheckStates;
        private LayoutInflater inflater = null;

        public year_adapter(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_state_popup, null);
                holder = new ViewHolder();

                holder.txt_state = (TextView) vi.findViewById(R.id.txt_state);
                holder.img = (ImageView) vi.findViewById(R.id.img_state);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (data.size() <= 0) {

            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String cat = tempValues.getState();
                if (cat == null) {
                } else if (cat != null) {

                    if (tempValues.isSelected_parking()) {
                        holder.img.setImageResource(R.mipmap.icon_year);
                        holder.txt_state.setTextColor(getResources().getColor(R.color.selected_text));
                    } else {
                        holder.img.setImageResource(R.mipmap.icon_year_unselcted);
                        holder.txt_state.setTextColor(Color.parseColor("#000000"));
                    }
                    //holder.img.setImageResource(R.mipmap.icon_year);
                    holder.txt_state.setText(tempValues.getState());
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txt_state;
            ImageView img;
        }
    }

    private void ShowcolorPupup(Activity context, final ArrayList<item> data) {
        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);
        RelativeLayout rl_view1 = (RelativeLayout) context.findViewById(R.id.title);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.popup_pre_color, viewGroup, false);
        popupcolor = new PopupWindow(context);
        popupcolor.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupcolor.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupcolor.setContentView(layout);
        popupcolor.setBackgroundDrawable(null);
        popupcolor.setFocusable(true);
        popupcolor.showAtLocation(layout, Gravity.CENTER, 0, 0);
        Button btnparkhere;
        ImageView imageclose;
        RecyclerView grid_managed;
        RelativeLayout rl_popclose, rl_selected_view, rl_color, rl_done;
        TextView txt_title, txt_color_name;

        txt_title = (TextView) layout.findViewById(R.id.txt_title);
        grid_managed = (RecyclerView) layout.findViewById(R.id.list_more_destination);
        imageclose = (ImageView) layout.findViewById(R.id.img_managed_close);
        rl_selected_view = (RelativeLayout) layout.findViewById(R.id.rl_selected_color);
        rl_color = (RelativeLayout) layout.findViewById(R.id.rl_color);
        txt_color_name = (TextView) layout.findViewById(R.id.txt_color_name);
        rl_popclose = (RelativeLayout) layout.findViewById(R.id.rl_pop);
        rl_done = (RelativeLayout) layout.findViewById(R.id.rl_done);
        rl_popclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupcolor.dismiss();
            }
        });

        Resources rs;
        boolean is_selected = false;
        final CustomAdapter_color adpter = new CustomAdapter_color(data);
        for (int k = 0; k < data.size(); k++) {
            boolean selected_ore = data.get(k).isSelected_parking();

            if (selected_ore) {
                is_selected = true;
                String statename = data.get(k).getState();
                rl_color.setBackgroundResource(data.get(k).getImg_id());
                txt_color_name.setText(statename);
                break;
            }
        }

        if (is_selected) {
            rl_selected_view.setVisibility(View.VISIBLE);
        } else {
            rl_selected_view.setVisibility(View.GONE);
        }

        rl_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int k = 0; k < data.size(); k++) {
                    boolean selected_ore = data.get(k).isSelected_parking();
                    if (selected_ore) {
                        int position = k;
                        popupcolor.dismiss();
                        color = data.get(position).getState();
                        year_color.append(color);
                        edt_Color.setText("" + color);

                        for (int i = 0; i < popup_window_show.size(); i++) {
                            boolean is_open = popup_window_show.get(i).isSelected_parking();
                            if (!is_open) {
                                String name = popup_window_show.get(i).getName();
                                popup_window_show.get(i).setSelected_parking(true);
                                break;
                            }
                        }
                    }
                }
            }
        });
        grid_managed.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
        grid_managed.setLayoutManager(layoutManager);
        grid_managed.setAdapter(adpter);


        txt_title.setText("Select Color");
        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupcolor.dismiss();
            }
        });
    }

    public class CustomAdapter_color extends RecyclerView.Adapter<CustomAdapter_color.MyViewHolder> {

        private ArrayList<item> dataSet;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            RelativeLayout rl_view_click;
            public TextView txt_state;
            ImageView img;
            RelativeLayout rl_color, rl_selected;

            public MyViewHolder(View itemView) {
                super(itemView);
                txt_state = (TextView) itemView.findViewById(R.id.txt_state);
                rl_color = (RelativeLayout) itemView.findViewById(R.id.rl_color);
                rl_view_click = (RelativeLayout) itemView.findViewById(R.id.rl_view_clikc);
                rl_selected = (RelativeLayout) itemView.findViewById(R.id.dd);
            }
        }

        public CustomAdapter_color(ArrayList<item> data) {
            this.dataSet = data;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_color, parent, false);

            //view.setOnClickListener(MainActivity.myOnClickListener);

            MyViewHolder myViewHolder = new MyViewHolder(view);

            return myViewHolder;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {

            item tempValues = dataSet.get(position);
            String d = tempValues.getTitle_address();

            holder.rl_color.setBackgroundResource(tempValues.getImg_id());
            holder.txt_state.setText(tempValues.getState());

            if (tempValues.isSelected_parking()) {

                holder.rl_selected.setBackgroundResource(R.drawable.backboder_with_color);
                holder.txt_state.setTextColor(getResources().getColor(R.color.selected_text));
            } else {
                holder.rl_selected.setBackgroundResource(R.drawable.backboder_with_tran);
                holder.txt_state.setTextColor(Color.parseColor("#000000"));
            }

            holder.rl_view_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < dataSet.size(); i++) {
                        dataSet.get(i).setSelected_parking(false);
                    }
                    dataSet.get(position).setSelected_parking(true);
                    notifyDataSetChanged();
                    popupcolor.dismiss();
                    color = dataSet.get(position).getState();
                    edt_Color.setText("" + color);
                    year_color.append(color);

                    for (int i = 0; i < popup_window_show.size(); i++) {
                        boolean is_open = popup_window_show.get(i).isSelected_parking();
                        if (!is_open) {
                            String name = popup_window_show.get(i).getName();
                            popup_window_show.get(i).setSelected_parking(true);
                            //ShowPopup(name);
                            break;
                        }
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return dataSet.size();
        }
    }


}