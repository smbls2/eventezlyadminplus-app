package com.softmeasures.eventezlyadminplus.frament;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.item;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class frg_proof_add extends Fragment implements frg_voices_add.click_save_cancel {

    TextView txt_next_photo, txt_next_voices;
    RelativeLayout rl_add_audio;
    static TextView txt_cancel, txt_save_audio, txt_proof_detail;
    static EditText edit_comment;
    static ImageView img_1, img_2, img_3, img_close_1, img_close_2, img_close_3;
    static RelativeLayout rl_recoding_view, rl_delete, rl_refresh, rl_img_1, rl_img_2, rl_img_3;
    LinearLayout ll_photo, ll_voices;
    boolean img_pick1 = false, img_pick2 = false, img_pick3 = false, complted_recording_upload = false;
    private static final int CAMERA_REQUEST = 100;
    File file_image1, file_image2, file_image3;
    public static ArrayList<item> file_body = new ArrayList<>();
    String img_url1 = "", img_url2 = "", img_url3 = "";
    SharedPreferences.Editor ed;
    SharedPreferences saveproof;
    public static String comment;
    private click_done_and_back listener;

    public interface click_done_and_back {
        public void onclick_done(String show_data);
    }

    public void registerForListener(click_done_and_back listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        file_body.clear();
        View view = inflater.inflate(R.layout.frg_add_prof_layout, container, false);
        edit_comment = (EditText) view.findViewById(R.id.edit_comment);
        txt_next_photo = (TextView) view.findViewById(R.id.txt_next_photo);
        txt_next_voices = (TextView) view.findViewById(R.id.txt_next_voice);

        rl_add_audio = (RelativeLayout) view.findViewById(R.id.rl_add_voice);
        rl_delete = (RelativeLayout) view.findViewById(R.id.rl_rl_delete);
        rl_refresh = (RelativeLayout) view.findViewById(R.id.rl_rl_refresh);

        rl_img_1 = (RelativeLayout) view.findViewById(R.id.rl_img_1);
        rl_img_2 = (RelativeLayout) view.findViewById(R.id.rl_img_2);
        rl_img_3 = (RelativeLayout) view.findViewById(R.id.rl_img_3);

        img_1 = (ImageView) view.findViewById(R.id.img_pic1);
        img_2 = (ImageView) view.findViewById(R.id.img_pic2);
        img_3 = (ImageView) view.findViewById(R.id.img_pic3);

        img_close_1 = (ImageView) view.findViewById(R.id.img_close1);
        img_close_2 = (ImageView) view.findViewById(R.id.img_close2);
        img_close_3 = (ImageView) view.findViewById(R.id.img_close3);

        TextView txt_done = (TextView) view.findViewById(R.id.txt_done);
        ll_photo = (LinearLayout) view.findViewById(R.id.ll_photo);
        ll_voices = (LinearLayout) view.findViewById(R.id.ll_voice);
        txt_done = (TextView) view.findViewById(R.id.txt_done);
        txt_next_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_next_photo.setVisibility(View.GONE);
                ll_photo.setVisibility(View.VISIBLE);
                txt_next_voices.setVisibility(View.VISIBLE);
            }
        });

        txt_next_voices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_next_voices.setVisibility(View.GONE);
                ll_voices.setVisibility(View.VISIBLE);
            }
        });

        rl_img_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_pick1 = true;
                img_pick2 = false;
                img_pick3 = false;
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);

            }
        });

        rl_img_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_pick2 = true;
                img_pick1 = false;
                img_pick3 = false;
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        rl_img_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_pick3 = true;
                img_pick2 = false;
                img_pick1 = false;
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        saveproof = getActivity().getSharedPreferences("save_proof", Context.MODE_PRIVATE);
        ed = saveproof.edit();

        rl_add_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("is_new", "yes");
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                frg_voices_add pay = new frg_voices_add();
                pay.setArguments(b);
                pay.registerForListener(frg_proof_add.this);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        rl_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("is_new", "no");
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                frg_voices_add pay = new frg_voices_add();
                pay.setArguments(b);
                pay.registerForListener(frg_proof_add.this);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });


        img_close_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("Are you sure you want to delete voice recording?");
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                                R.anim.side_down);
                        rl_img_1.startAnimation(bottomUp);
                        rl_img_1.setVisibility(View.GONE);
                        img_1.setImageResource(0);
                        rl_img_1.setVisibility(View.VISIBLE);
                        img_close_1.setVisibility(View.GONE);
                        File file = new File(img_url1);
                        file.delete();
                        img_url1 = "";
                        boolean img_one = hasImage(img_1);
                        boolean img_two = hasImage(img_2);
                        boolean img_three = hasImage(img_3);

                        if (!img_one) {
                            ed.putString("img", "no");
                            ed.commit();
                            if (!img_two) {
                                ed.putString("img", "no");
                                ed.commit();
                                if (!img_three) {
                                    ed.putString("img", "no");
                                    ed.commit();
                                    rl_img_2.setVisibility(View.GONE);
                                    rl_img_3.setVisibility(View.GONE);
                                } else {
                                    ed.putString("img", "3 Image");
                                    ed.commit();
                                }

                            } else {
                                ed.putString("img", "2 Image");
                                ed.commit();
                            }
                        } else {
                            ed.putString("img", "1 Image");
                            ed.commit();
                        }

                        for (int i = 0; i < file_body.size(); i++) {
                            String file_name = file_body.get(i).getFile_name();
                            if (file_name.equals("file1")) {
                                file_body.remove(i);
                                break;
                            }
                        }
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();


            }
        });
        img_close_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("Are you sure you want to delete voice recording?");
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                                R.anim.side_down);
                        rl_img_2.startAnimation(bottomUp);
                        rl_img_2.setVisibility(View.GONE);
                        img_2.setImageResource(0);
                        rl_img_2.setVisibility(View.VISIBLE);
                        img_close_2.setVisibility(View.GONE);
                        boolean img_one = hasImage(img_1);
                        boolean img_two = hasImage(img_2);
                        boolean img_three = hasImage(img_3);
                        File file = new File(img_url2);
                        file.delete();
                        img_url2 = "";
                        if (!img_one) {
                            ed.putString("img", "no");
                            ed.commit();
                            if (!img_two) {
                                ed.putString("img", "no");
                                ed.commit();
                                if (!img_three) {
                                    ed.putString("img", "no");
                                    ed.commit();
                                    rl_img_2.setVisibility(View.GONE);
                                    rl_img_3.setVisibility(View.GONE);
                                } else {
                                    ed.putString("img", "3 Image");
                                    ed.commit();
                                }

                            } else {
                                ed.putString("img", "2 Image");
                                ed.commit();
                            }

                        } else {
                            ed.putString("img", "1 Image");
                            ed.commit();
                        }

                        for (int i = 0; i < file_body.size(); i++) {
                            String file_name = file_body.get(i).getFile_name();
                            if (file_name.equals("file2")) {
                                file_body.remove(i);
                                break;
                            }
                        }
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();

            }
        });

        img_close_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("Are you sure you want to delete voice recording?");
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                                R.anim.side_down);
                        rl_img_3.startAnimation(bottomUp);
                        rl_img_3.setVisibility(View.GONE);
                        img_3.setImageResource(0);
                        rl_img_3.setVisibility(View.VISIBLE);
                        img_close_3.setVisibility(View.GONE);
                        boolean img_one = hasImage(img_1);
                        boolean img_two = hasImage(img_2);
                        boolean img_three = hasImage(img_3);
                        File file = new File(img_url3);
                        file.delete();
                        img_url3 = "";
                        if (!img_one) {
                            ed.putString("img", "no");
                            ed.commit();
                            if (!img_two) {
                                ed.putString("img", "no");
                                ed.commit();
                                if (!img_three) {
                                    ed.putString("img", "no");
                                    ed.commit();
                                    rl_img_2.setVisibility(View.GONE);
                                    rl_img_3.setVisibility(View.GONE);
                                } else {
                                    ed.putString("img", "3 Image");
                                    ed.commit();
                                }

                            } else {
                                ed.putString("img", "2 Image");
                                ed.commit();
                            }
                        } else {
                            ed.putString("img", "1 Image");
                            ed.commit();
                        }
                        for (int i = 0; i < file_body.size(); i++) {
                            String file_name = file_body.get(i).getFile_name();
                            if (file_name.equals("file3")) {
                                file_body.remove(i);
                                break;
                            }
                        }

                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();


            }
        });

        txt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ticket_back();
            }
        });

        return view;
    }


    @Override
    public void onResume() {
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    ticket_back();
                    return true;
                }
                return false;
            }
        });
        super.onResume();
    }

    private boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);

        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable) drawable).getBitmap() != null;
        }

        return hasImage;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            final Bitmap photo = (Bitmap) data.getExtras().get("data");
            // Uri tempUri = getImageUri(getActivity(), photo);
            //SaveImage(photo);

            // CALL THIS METHOD TO GET THE ACTUAL PATH

            int size_file_body = file_body.size();
            if (size_file_body == 3) {
                file_body.clear();
            }

            item item = new item();
            if (img_pick1) {
                file_image1 = SaveImage(photo);
                img_url1 = file_image1.getAbsolutePath();
                item.setFile_body(file_image1);
                item.setFile_name("file1");
                file_body.add(item);
                Log.e("url1", img_url1);
                Picasso.with(getContext()).load(file_image1).into(img_1, new Callback() {
                    @Override
                    public void onSuccess() {
                        img_1.setImageResource(0);
                        img_1.setImageBitmap(photo);
                        rl_img_2.setVisibility(View.VISIBLE);
                        img_close_1.setVisibility(View.VISIBLE);
                        // moveBackground();
                    }

                    @Override
                    public void onError() {
                    }
                });

            } else if (img_pick2) {
                file_image2 = SaveImage(photo);
                img_url2 = file_image2.getAbsolutePath();
                item.setFile_body(file_image2);
                item.setFile_name("file2");
                file_body.add(item);
                Log.e("url2", img_url2);
                Picasso.with(getContext()).load(file_image2).into(img_2, new Callback() {
                    @Override
                    public void onSuccess() {
                        rl_img_3.setVisibility(View.VISIBLE);
                        img_close_2.setVisibility(View.VISIBLE);
                        // moveBackground();
                    }

                    @Override
                    public void onError() {
                    }
                });

            } else if (img_pick3) {
                file_image3 = SaveImage(photo);
                img_url3 = file_image3.getAbsolutePath();
                item.setFile_body(file_image3);
                item.setFile_name("file3");
                file_body.add(item);
                Log.e("url3", img_url3);
                Picasso.with(getContext()).load(file_image3).into(img_3, new Callback() {
                    @Override
                    public void onSuccess() {
                        img_close_3.setVisibility(View.VISIBLE);
                        // moveBackground();
                    }

                    @Override
                    public void onError() {
                    }
                });
            }
        }
    }

    private File SaveImage(Bitmap finalBitmap) {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String rid = logindeatl.getString("id", "null");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        String timeStamp = dateFormat.format(new Date());
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ParkEZly";
        File dir = new File(path);
        if (!dir.exists())
            dir.mkdirs();

        File file = new File(dir, rid + timeStamp + ".jpeg");
        if (file.exists()) file.delete();
        try {
            OutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
            return file;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public void onclick_save() {
        ed.putString("Audio", "1 Recording");
        ed.commit();
        rl_delete.setVisibility(View.VISIBLE);
        rl_refresh.setVisibility(View.VISIBLE);
        rl_add_audio.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onlick_cancel() {

    }


    public void ticket_back() {
        comment = edit_comment.getText().toString();
        String proof = "";
        String commerbt_final = "";
        String audiofinal = "";

        boolean img1 = hasImage(img_1);
        boolean img2 = hasImage(img_2);
        boolean img3 = hasImage(img_3);

        int i = 0;
        if (img1) {
            i++;
        }
        if (img2) {
            i++;
        }

        if (img3) {
            i++;
        }

        if (i > 1) {
            proof = i + " Images";
        } else if (i == 1) {
            proof = i + " Image";
        } else {
            proof = "";
        }


        if (!comment.equals("")) {
            commerbt_final = "1 Comment";
        }

        if (rl_delete.getVisibility() == View.VISIBLE) {
            audiofinal = "1 Recording";
        }

        StringBuffer str_final = new StringBuffer();
        if (!commerbt_final.equals("")) {
            str_final.append("1 Comment , ");
        }

        if (!proof.equals("")) {
            str_final.append(proof + " , ");
        }

        if (!audiofinal.equals("")) {
            str_final.append(audiofinal + " , ");
        }

        String finaldata = str_final.toString();
        if (finaldata.length() > 0) {
            finaldata = finaldata.substring(0, finaldata.length() - 2);
            listener.onclick_done(finaldata);
            //txt_proof_detail.setText(finaldata);
        } else {
            listener.onclick_done("");
        }
    }
}
