package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class frg_hearing_list extends Fragment {
    TextView txt_violation_code, txt_violation_fess, txt_violation_address, txt_violation_type, txt_hearinf_type, textty1, txt_crweateticket1, txt_court_id, txt_hearing_date, txt_finalticket, txt_final_create_ticket, txt_address;
    ListView list_of_violation_type, list_of_hearing_location;
    RelativeLayout rl_progressbar;
    String marker, platno, statename, lat, lang, row, row_no, township_code, violation_code, violation_desc, hearing_address, violation_fee, hearing_location, court_id, user_id, status, heraing_date, parking_type1;
    ArrayList<item> violationtype = new ArrayList<item>();
    ArrayList<item> hearingtype = new ArrayList<item>();
    ArrayList<item> date_list = new ArrayList<>();
    private list_item_click_hearing listener;

    public interface list_item_click_hearing {
        public void on_hearing(String code, String fess, String des, String date, boolean is_multiple, ArrayList<item> data);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_hearing_list, container, false);
        list_of_hearing_location = (ListView) view.findViewById(R.id.list_hearing);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        new getviolationtype().execute();
        return view;
    }


    public class getviolationtype extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONObject json, json1;
        String transactionurl = "_table/township_users?filter=user_id%3D" + id + "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            Log.e("vilation_type", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray googleparking = json.getJSONArray("resource");

                    for (int j = 0; j < googleparking.length(); j++) {
                        JSONObject c = googleparking.getJSONObject(j);
                        township_code = c.getString("township_code");
                        String email = c.getString("email");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs;
            if (getActivity() != null && json != null) {

                new gethearing().execute();

            }

        }
    }

    public class gethearing extends AsyncTask<String, String, String> {

        JSONObject json, json1;
        String transactionurl = "_table/hearing_place_info?filter=township_code%3D" + township_code + "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            hearingtype.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            DefaultHttpClient client = new DefaultHttpClient();
            Log.e("hearing_place", url);
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray googleparking = json.getJSONArray("resource");

                    for (int j = 0; j < googleparking.length(); j++) {
                        item item = new item();

                        JSONObject c = googleparking.getJSONObject(j);
                        String location = c.getString("hearing_location");
                        String address = c.getString("hearing_address");
                        String id = c.getString("court_id");
                        String date = c.getString("first_court_date_time");
                        item.setHearing_address(address);
                        item.setHearing_location(location);
                        item.setCourt_id(id);
                        item.setDate(date);
                        item.setSecond_court_date_time(c.getString("second_court_date_time"));
                        item.setThird_court_date_time(c.getString("third_court_date_time"));
                        item.setFourth_court_date_time(c.getString("fourth_court_date_time"));
                        item.setFifth_court_date_time(c.getString("fifth_court_date_time"));
                        item.setSixth_court_date_time(c.getString("sixth_court_date_time"));
                        item.setSeventh_court_date_time(c.getString("seventh_court_date_time"));
                        item.setEighth_court_date_time(c.getString("eighth_court_date_time"));
                        item.setNinth_court_date_time(c.getString("ninth_court_date_time"));
                        item.setTenth_court_date_time(c.getString("tenth_court_date_time"));
                        item.setEleventh_court_date_time(c.getString("eleventh_court_date_time"));
                        item.setTwelfth_court_date_time(c.getString("twelfth_court_date_time"));
                        hearingtype.add(item);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity != null) {
                    Log.e("hearing", "yes");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                    String currentDateandTime = sdf.format(new Date());

                    if (hearingtype.size() > 0) {
                        if (hearingtype.size() == 1) {
                            SharedPreferences gg = getActivity().getSharedPreferences("vehicleinfo", MODE_PRIVATE);
                            SharedPreferences.Editor d = gg.edit();
                            d.clear();
                            d.commit();
                            String address = hearingtype.get(0).getHearing_address();
                            String location = hearingtype.get(0).getHearing_location();
                            String des = hearingtype.get(0).getCourt_id();
                            String date = hearingtype.get(0).getDate();
                            date_list.clear();
                            date_list.add(hearingtype.get(0));
                            txt_hearing_date.setText(date);
                            listener.on_hearing(address, location, des, date, false, date_list);

                        } else {
                            list_of_hearing_location.setAdapter(new adpterhearing(getActivity(), hearingtype, getResources()));
                        }
                    }
                }
            }
        }


    }

    public void registerForListener(list_item_click_hearing listener) {
        this.listener = listener;
    }

    public class adpterhearing extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public adpterhearing(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;


            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {


                vi = inflater.inflate(R.layout.adapterhearing, null);
                holder = new ViewHolder();

                holder.txt_hearingaddress = (TextView) vi.findViewById(R.id.txt_hearing_address);
                holder.txt_hearing_locatiobn = (TextView) vi.findViewById(R.id.txt_hearing_location);
                holder.txt_court_id = (TextView) vi.findViewById(R.id.text_courtid);
                holder.txt_date = (TextView) vi.findViewById(R.id.text_date);

                vi.setTag(holder);
               /* vi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });*/

                list_of_hearing_location.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        SharedPreferences gg = getActivity().getSharedPreferences("vehicleinfo", MODE_PRIVATE);
                        SharedPreferences.Editor d = gg.edit();
                        d.clear();
                        d.commit();
                        textty1.setText("Hearing Location");
                        String address = holder.txt_hearingaddress.getText().toString();
                        String location = holder.txt_hearing_locatiobn.getText().toString();
                        String des = holder.txt_court_id.getText().toString();
                        Animation animation = AnimationUtils.loadAnimation(getActivity(),
                                R.anim.sidepannelleft);
                        String date = holder.txt_date.getText().toString();
                        date_list.clear();
                        date_list.add(hearingtype.get(position));
                        listener.on_hearing(address, location, des, date, true, date_list);
                        getActivity().onBackPressed();
                    }
                });
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
                //holder.txtdate.setText("No Data");

            } else {
                /***** Get each Model object from Arraylist ********/
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                /************  Set Model values in Holder elements ***********/


                Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvetica-normal.ttf");
                Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvetica-neue-medium.ttf");

                holder.txt_hearing_locatiobn.setTypeface(medium);
                holder.txt_court_id.setText(tempValues.getCourt_id());
                if (tempValues.getHearing_address().equals("null")) {
                    holder.txt_hearingaddress.setText("");
                } else {
                    holder.txt_hearingaddress.setText(tempValues.getHearing_address());
                }
                holder.txt_hearing_locatiobn.setText(tempValues.getHearing_location());
                holder.txt_date.setText(tempValues.getDate());
            }

            return vi;


        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txt_hearingaddress, txt_hearing_locatiobn, txt_court_id, txt_date;
        }

    }

}
