package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class mypermits extends Fragment {

    ProgressDialog pdialog;
    ArrayList<item> permitsarray, permitsarray_sort;
    CustomAdaptercity adpater;
    ListView listofvehicleno;
    TextView txttitle, btnaddvehicle;
    TextView txtok;
    RelativeLayout btnok;
    int i = 1;
    ConnectionDetector cd;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar;
    String id, twcode, role;
    TextView txt_Pending, txt_DUE, txt_ACTIVE;
    String type_approved = "0", type_DUE = "0", type_ACTIVE = "0";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mypermits, container, false);
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        id = logindeatl.getString("id", "null");
        role = logindeatl.getString("role", "null");
        twcode = logindeatl.getString("twcode", "null");
        Log.e("twcode ", " : " + twcode);

        txt_Pending = (TextView) view.findViewById(R.id.txt_Pending);
        txt_DUE = (TextView) view.findViewById(R.id.txt_DUE);
        txt_ACTIVE = (TextView) view.findViewById(R.id.txt_ACTIVE);

        listofvehicleno = (ListView) view.findViewById(R.id.listofvehicle);
        txttitle = (TextView) view.findViewById(R.id.txttitte);
        btnok = (RelativeLayout) view.findViewById(R.id.ok);
        btnaddvehicle = (TextView) view.findViewById(R.id.txtaddvehicle);
        txtok = (TextView) view.findViewById(R.id.txtok);
        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Thin.ttf");
        txttitle.setTypeface(type);
        RelativeLayout rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        cd = new ConnectionDetector(getActivity());
        if (cd.isConnectingToInternet()) {

            //new getSubscription().execute();

        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((vchome) getActivity()).hide();
            }
        });

        txt_Pending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permitsarray_sort.clear();
                if (type_approved.equals("0")) {
                    for (int i = 0; i < permitsarray.size(); i++) {
                        String approved = permitsarray.get(i).getApproved();
                        if (approved.equals("0")) {
                            permitsarray_sort.add(permitsarray.get(i));
                        }
                    }
                    txt_Pending.setText("PENDING");
                    type_approved = "1";
                } else if (type_approved.equals("1")) {
                    for (int i = 0; i < permitsarray.size(); i++) {
                        String approved = permitsarray.get(i).getApproved();
                        if (approved.equals("1")) {
                            permitsarray_sort.add(permitsarray.get(i));
                        }
                    }
                    txt_Pending.setText("ACCEPTED");
                    type_approved = "2";
                } else if (type_approved.equals("2")) {
                    for (int i = 0; i < permitsarray.size(); i++) {
                        String approved = permitsarray.get(i).getApproved();
                        if (approved.equals("2")) {
                            permitsarray_sort.add(permitsarray.get(i));
                        }
                    }
                    txt_Pending.setText("REJECTED");
                    type_approved = "0";
                }
                adpater = new CustomAdaptercity(getActivity(), permitsarray_sort);
                listofvehicleno.setAdapter(adpater);
                adpater.notifyDataSetChanged();
                txt_Pending.setTypeface(txt_Pending.getTypeface(), Typeface.BOLD_ITALIC);
                txt_DUE.setTypeface(txt_DUE.getTypeface(), Typeface.NORMAL);
                txt_ACTIVE.setTypeface(txt_ACTIVE.getTypeface(), Typeface.NORMAL);
            }
        });

        txt_DUE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permitsarray_sort.clear();
                if (type_DUE.equals("0")) {
                    for (int i = 0; i < permitsarray.size(); i++) {
                        String paid = permitsarray.get(i).getPaid();
                        if (paid.equals("0")) {
                            permitsarray_sort.add(permitsarray.get(i));
                        }
                    }
                    txt_DUE.setText("UNPAID");
                    type_DUE = "1";
                } else if (type_DUE.equals("1")) {
                    for (int i = 0; i < permitsarray.size(); i++) {
                        String paid = permitsarray.get(i).getPaid();
                        if (paid.equals("1")) {
                            permitsarray_sort.add(permitsarray.get(i));
                        }
                    }
                    txt_DUE.setText("PYMT DUE");
                    type_DUE = "0";
                }
                adpater = new CustomAdaptercity(getActivity(), permitsarray_sort);
                listofvehicleno.setAdapter(adpater);
                adpater.notifyDataSetChanged();
                txt_Pending.setTypeface(txt_Pending.getTypeface(), Typeface.NORMAL);
                txt_DUE.setTypeface(txt_DUE.getTypeface(), Typeface.BOLD_ITALIC);
                txt_ACTIVE.setTypeface(txt_ACTIVE.getTypeface(), Typeface.NORMAL);
            }
        });

        txt_ACTIVE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permitsarray_sort.clear();
                if (type_ACTIVE.equals("0")) {
                    for (int i = 0; i < permitsarray.size(); i++) {
                        String paid = permitsarray.get(i).getStatus();
                        if (paid.equals("0")) {
                            permitsarray_sort.add(permitsarray.get(i));
                        }
                    }
                    txt_ACTIVE.setText("INACTIVE");
                    type_ACTIVE = "1";
                } else if (type_ACTIVE.equals("1")) {
                    for (int i = 0; i < permitsarray.size(); i++) {
                        String paid = permitsarray.get(i).getStatus();
                        if (paid.equals("1")) {
                            permitsarray_sort.add(permitsarray.get(i));
                        }
                    }
                    txt_ACTIVE.setText("ACTIVE");
                    type_ACTIVE = "0";
                }
                adpater = new CustomAdaptercity(getActivity(), permitsarray_sort);
                listofvehicleno.setAdapter(adpater);
                adpater.notifyDataSetChanged();
                txt_Pending.setTypeface(txt_Pending.getTypeface(), Typeface.NORMAL);
                txt_DUE.setTypeface(txt_DUE.getTypeface(), Typeface.NORMAL);
                txt_ACTIVE.setTypeface(txt_ACTIVE.getTypeface(), Typeface.BOLD_ITALIC);
            }
        });

        return view;
    }

    public class getSubscription extends AsyncTask<String, String, String> {
        JSONObject json;
        String subcribe = "_table/permit_subscription";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            permitsarray = new ArrayList<>();
            permitsarray_sort = new ArrayList<item>();
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            if (role.equalsIgnoreCase("TwpAdmin")) {
                try {
                    subcribe = "_table/permit_subscription?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (role.equalsIgnoreCase("TwpInspector")) {
                try {
                    subcribe = "_table/permit_subscription?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (role.equalsIgnoreCase("TwpBursar")) {
                try {
                    subcribe = "_table/permit_subscription?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                subcribe = "_table/permit_subscription";
            }
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + subcribe;
            Log.e("URL ", " : " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    Log.e("json ", " : " + json.toString());
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        item.setId(c.getString("id"));
                        item.setDate_time(c.getString("date_time"));
                        item.setUser_id(c.getString("user_id"));
                        item.setPermit_id(c.getString("permit_id"));
                        item.setTownship_code(c.getString("township_code"));
                        item.setTownship_name(c.getString("township_name"));
                        item.setPermit_type(c.getString("permit_type"));
                        item.setPermit_name(c.getString("permit_name"));
                        item.setPermit_validity(c.getString("permit_validity"));
                        item.setPermit_expires_on(c.getString("permit_expires_on"));
                        item.setExpired(c.getString("expired"));
                        item.setDrv_lic_proof(c.getString("drv_lic_proof"));
                        item.setResidency_proof(c.getString("residency_proof"));
                        item.setApproved(c.getString("approved"));
                        item.setStatus(c.getString("status"));
                        item.setPaid(c.getString("paid"));
                        item.setUser_comments(c.getString("user_comments"));
                        item.setTown_comments(c.getString("town_comments"));
                        item.setLogo(c.getString("logo"));
                        item.setScheme_type(c.getString("scheme_type"));
                        item.setFirst_contact_date(c.getString("first_contact_date"));
                        item.setPermit_status_image(c.getString("permit_status_image"));
                        item.setRate(c.getString("rate"));
                        item.setUser_name(c.getString("user_name"));
                        item.setSignature(c.getString("signature"));
                        item.setPermit_num(c.getString("permit_num"));
                        item.setFull_address(c.getString("full_address"));
                        item.setFull_name(c.getString("full_name"));
                        item.setEmail(c.getString("email"));
                        item.setPhone(c.getString("phone"));
                        item.setDoc_requirements(c.getString("doc_requirements"));

                        if (c.getString("status").equals("1")) {
                            item.setBgColor("#E6BFB6");
                        } else {
                            item.setBgColor("#FDD835");
                        }
                        permitsarray.add(item);
                    }
                    permitsarray_sort.addAll(permitsarray);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            if (getActivity() != null && json != null) {
                adpater = new CustomAdaptercity(getActivity(), permitsarray_sort);
                listofvehicleno.setAdapter(adpater);
            }
            super.onPostExecute(s);
        }
    }


    /*APPROVED=PENDING, YES,NO  (or 0,1,2)
      STATUS : INACTIVE, ACTIVE, EXPIRED  ( 0,1,2 )
      Pymt Due, Paid, Not Paid ( 0,1,2)
      RENEWABLE ( Yes or No ) 1 or 0
      EXPIRED  ( Yes or No ) 1 or 0 */

    public class CustomAdaptercity extends BaseAdapter {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        item tempValues = null;
        Context context;

        public CustomAdaptercity(Activity a, ArrayList<item> d) {
            activity = a;
            context = a;
            data = d;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        public class ViewHolder {
            TextView txtTownship_code, txtTownship_name, txtRate;
            TextView txt_Pending, txt_DUE, txt_ACTIVE;
            RelativeLayout rl_main;

            public ViewHolder(View convertView) {
                rl_main = (RelativeLayout) convertView.findViewById(R.id.rl_main);
                txtTownship_code = (TextView) convertView.findViewById(R.id.txtTownship_code);
                txtTownship_name = (TextView) convertView.findViewById(R.id.txtTownship_name);
                txt_Pending = (TextView) convertView.findViewById(R.id.txt_Pending);
                txt_DUE = (TextView) convertView.findViewById(R.id.txt_DUE);
                txt_ACTIVE = (TextView) convertView.findViewById(R.id.txt_ACTIVE);
                txtRate = (TextView) convertView.findViewById(R.id.txtRate);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.mypermits, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvetica-neue-medium.ttf");
            Typeface light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Light.otf");

            if (data.size() <= 0) {
                holder.rl_main.setVisibility(View.GONE);
            } else {
                tempValues = (item) data.get(position);
                holder.txtTownship_code.setText(tempValues.getTownship_code() + " - " + tempValues.getPermit_name());

                holder.txtTownship_name.setText("ExpiryDate : " + tempValues.getPermit_expires_on());

                if (tempValues.getRate().equalsIgnoreCase("null") ||
                        tempValues.getRate().equalsIgnoreCase("")) {
                    holder.txtRate.setText("Permit Fee : $0");
                } else {
                    holder.txtRate.setText("Permit Fee : " + tempValues.getRate());
                }

                if (tempValues.getPaid().equalsIgnoreCase("0")) {
                    holder.txt_DUE.setText("PYMT DUE");
                    holder.txt_DUE.setBackground(getResources().getDrawable(R.drawable.button5));
                } else if (tempValues.getPaid().equalsIgnoreCase("1")) {
                    holder.txt_DUE.setText("PAID");
                    holder.txt_DUE.setBackground(getResources().getDrawable(R.drawable.button3));
                } else if (tempValues.getPaid().equalsIgnoreCase("2")) {
                    holder.txt_DUE.setText("UNPAID");
                    holder.txt_DUE.setBackground(getResources().getDrawable(R.drawable.button5));
                }

                if (tempValues.getStatus().equalsIgnoreCase("0")) {
                    holder.txt_ACTIVE.setText("INACTIVE");
                } else if (tempValues.getStatus().equalsIgnoreCase("1")) {
                    holder.txt_ACTIVE.setText("ACTIVE");
                } else if (tempValues.getStatus().equalsIgnoreCase("2")) {
                    holder.txt_ACTIVE.setText("EXPIRED");
                }

                if (tempValues.getApproved().equalsIgnoreCase("0")) {
                    holder.txt_Pending.setText("PENDING");
                } else if (tempValues.getApproved().equalsIgnoreCase("1")) {
                    holder.txt_Pending.setText("ACCEPTED");
                } else if (tempValues.getApproved().equalsIgnoreCase("2")) {
                    holder.txt_Pending.setText("REJECTED");
                }

                holder.rl_main.setBackgroundColor(Color.parseColor(tempValues.getBgColor()));


                holder.rl_main.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tempValues = (item) data.get(position);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", tempValues.getId());
                        bundle.putString("date_time", tempValues.getDate_time());
                        bundle.putString("user_id", tempValues.getUser_id());
                        bundle.putString("permit_id", tempValues.getPermit_id());
                        bundle.putString("township_code", tempValues.getTownship_code());
                        bundle.putString("township_name", tempValues.getTownship_name());
                        bundle.putString("permit_type", tempValues.getPermit_type());
                        bundle.putString("permit_name", tempValues.getPermit_name());
                        bundle.putString("driver_proof", tempValues.getDrv_lic_proof());
                        bundle.putString("residency_proof", tempValues.getResidency_proof());
                        bundle.putString("approved", tempValues.getApproved());
                        bundle.putString("status", tempValues.getStatus());
                        bundle.putString("paid", tempValues.getPaid());
                        bundle.putString("scheme_type", tempValues.getScheme_type());
                        bundle.putString("rate", tempValues.getRate());
                        bundle.putString("user_name", tempValues.getUser_name());
                        bundle.putString("signature", tempValues.getSignature());
                        bundle.putString("Logo", tempValues.getLogo());

                        bundle.putString("First_contact_date", tempValues.getFirst_contact_date());
                        bundle.putString("Permit_nextnum", tempValues.getPermit_num());
                        bundle.putString("Full_name", tempValues.getFull_name());
                        bundle.putString("Full_address", tempValues.getFull_address());
                        bundle.putString("email", tempValues.getEmail());
                        bundle.putString("phone", tempValues.getPhone());
                        bundle.putString("User_comments", tempValues.getUser_comments());
                        bundle.putString("Town_comments", tempValues.getTown_comments());

                        bundle.putString("permit_validity", tempValues.getPermit_validity());
                        bundle.putString("permit_expires_on", tempValues.getPermit_expires_on());
                        bundle.putString("expired", tempValues.getExpired());
                        bundle.putString("Doc_requirements", tempValues.getDoc_requirements());

                        ((vchome) getActivity()).show_back_button();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        Fragment_ManagePermit_Details pay = new Fragment_ManagePermit_Details();
                        pay.setArguments(bundle);
                        ft.add(R.id.My_Container_1_ID, pay);
                        fragmentStack.lastElement().onPause();
                        ft.hide(fragmentStack.lastElement());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                    }
                });
            }
            return convertView;
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        if (cd.isConnectingToInternet()) {
            try {
                new getSubscription().execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }
    }
}
