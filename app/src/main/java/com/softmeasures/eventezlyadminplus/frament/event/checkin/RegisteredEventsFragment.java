package com.softmeasures.eventezlyadminplus.frament.event.checkin;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragRegisteredEventsBinding;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.EventRegistration;
import com.softmeasures.eventezlyadminplus.models.User;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class RegisteredEventsFragment extends BaseFragment {

    private FragRegisteredEventsBinding binding;
    private EventDefinition eventDefinition;
    private ArrayList<EventRegistration> eventRegistrations = new ArrayList<>();
    private ArrayList<EventRegistration> eventRegistrationsFiltered = new ArrayList<>();

    private EventRegAdapter eventRegAdapter;
    private ArrayList<User> users = new ArrayList<>();
    private ProgressDialog progressDialog;
    private String type = "";
    private String user_id = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            eventDefinition = new Gson().fromJson(getArguments().getString("eventDefinition"), EventDefinition.class);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_registered_events, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            type = getArguments().getString("type");
        }

        initViews(view);
        updateViews();
        setListeners();

        new fetchRegisteredEvents().execute();
    }

    private class fetchRegisteredEvents extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json = new JSONObject();
        JSONArray json1 = new JSONArray();
        String regUrl = "";

        @Override
        protected void onPreExecute() {
            if (type != null && !TextUtils.isEmpty(type) && type.equalsIgnoreCase("User")) {
                regUrl = "_table/event_registration?filter=user_id=" + user_id;
            } else {
                regUrl = "_table/event_registration?filter=event_id=" + eventDefinition.getId();
            }
            binding.rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            eventRegistrations.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + regUrl;
            Log.e("#DEBUG", "  fetchRegisteredEvents: " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    json1 = json.getJSONArray("resource");

                    for (int i = 0; i < json1.length(); i++) {
                        JSONObject object = json1.getJSONObject(i);
                        EventRegistration eventRegistration = new EventRegistration();
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            eventRegistration.setId(object.getInt("id"));
                        }
                        if (object.has("manager_type_id")
                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
                                && !object.getString("manager_type_id").equals("null")) {
                            eventRegistration.setManager_type_id(object.getInt("manager_type_id"));
                        }
                        if (object.has("twp_id")
                                && !TextUtils.isEmpty(object.getString("twp_id"))
                                && !object.getString("twp_id").equals("null")) {
                            eventRegistration.setTwp_id(object.getInt("twp_id"));
                        }
                        if (object.has("company_id")
                                && !TextUtils.isEmpty(object.getString("company_id"))
                                && !object.getString("company_id").equals("null")) {
                            eventRegistration.setCompany_id(object.getInt("company_id"));
                        }
                        if (object.has("event_id")
                                && !TextUtils.isEmpty(object.getString("event_id"))
                                && !object.getString("event_id").equals("null")) {
                            eventRegistration.setEvent_id(object.getInt("event_id"));
                        }
                        if (object.has("event_session_id")
                                && !TextUtils.isEmpty(object.getString("event_session_id"))
                                && !object.getString("event_session_id").equals("null")) {
                            eventRegistration.setEvent_session_id(object.getInt("event_session_id"));
                        }
                        if (object.has("user_id")
                                && !TextUtils.isEmpty(object.getString("user_id"))
                                && !object.getString("user_id").equals("null")) {
                            eventRegistration.setUser_id(object.getInt("user_id"));
                        }
                        if (object.has("event_regn_fee_category_id")
                                && !TextUtils.isEmpty(object.getString("event_regn_fee_category_id"))
                                && !object.getString("event_regn_fee_category_id").equals("null")) {
                            eventRegistration.setEvent_regn_fee_category_id(object.getInt("event_regn_fee_category_id"));
                        }
                        if (object.has("regd_for_people_number")
                                && !TextUtils.isEmpty(object.getString("regd_for_people_number"))
                                && !object.getString("regd_for_people_number").equals("null")) {
                            eventRegistration.setRegd_for_people_number(object.getInt("regd_for_people_number"));
                        }
                        if (object.has("date_time")
                                && !TextUtils.isEmpty(object.getString("date_time"))
                                && !object.getString("date_time").equals("null")) {
                            eventRegistration.setDate_time(object.getString("date_time"));
                        }
                        if (object.has("manager_type")
                                && !TextUtils.isEmpty(object.getString("manager_type"))
                                && !object.getString("manager_type").equals("null")) {
                            eventRegistration.setManager_type(object.getString("manager_type"));
                        }

                        if (object.has("township_code")
                                && !TextUtils.isEmpty(object.getString("township_code"))
                                && !object.getString("township_code").equals("null")) {
                            eventRegistration.setTownship_code(object.getString("township_code"));
                        }
                        if (object.has("township_name")
                                && !TextUtils.isEmpty(object.getString("township_name"))
                                && !object.getString("township_name").equals("null")) {
                            eventRegistration.setTownship_name(object.getString("township_name"));
                        }

                        if (object.has("company_code")
                                && !TextUtils.isEmpty(object.getString("company_code"))
                                && !object.getString("company_code").equals("null")) {
                            eventRegistration.setCompany_code(object.getString("company_code"));
                        }
                        if (object.has("company_name")
                                && !TextUtils.isEmpty(object.getString("company_name"))
                                && !object.getString("company_name").equals("null")) {
                            eventRegistration.setCompany_name(object.getString("company_name"));
                        }
                        if (object.has("event_type")
                                && !TextUtils.isEmpty(object.getString("event_type"))
                                && !object.getString("event_type").equals("null")) {
                            eventRegistration.setEvent_type(object.getString("event_type"));
                        }
                        if (object.has("event_code")
                                && !TextUtils.isEmpty(object.getString("event_code"))
                                && !object.getString("event_code").equals("null")) {
                            eventRegistration.setEvent_code(object.getString("event_code"));
                        }
                        if (object.has("event_name")
                                && !TextUtils.isEmpty(object.getString("event_name"))
                                && !object.getString("event_name").equals("null")) {
                            eventRegistration.setEvent_name(object.getString("event_name"));
                        }
                        if (object.has("event_session_type")
                                && !TextUtils.isEmpty(object.getString("event_session_type"))
                                && !object.getString("event_session_type").equals("null")) {
                            eventRegistration.setEvent_session_type(object.getString("event_session_type"));
                        }
                        if (object.has("event_session_code")
                                && !TextUtils.isEmpty(object.getString("event_session_code"))
                                && !object.getString("event_session_code").equals("null")) {
                            eventRegistration.setEvent_session_code(object.getString("event_session_code"));
                        }
                        if (object.has("event_session_name")
                                && !TextUtils.isEmpty(object.getString("event_session_name"))
                                && !object.getString("event_session_name").equals("null")) {
                            eventRegistration.setEvent_session_name(object.getString("event_session_name"));
                        }
                        if (object.has("user_phone_num")
                                && !TextUtils.isEmpty(object.getString("user_phone_num"))
                                && !object.getString("user_phone_num").equals("null")) {
                            eventRegistration.setUser_phone_num(object.getString("user_phone_num"));
                        }
                        if (object.has("user_device_num")
                                && !TextUtils.isEmpty(object.getString("user_device_num"))
                                && !object.getString("user_device_num").equals("null")) {
                            eventRegistration.setUser_device_num(object.getString("user_device_num"));
                        }
                        if (object.has("user_email")
                                && !TextUtils.isEmpty(object.getString("user_email"))
                                && !object.getString("user_email").equals("null")) {
                            eventRegistration.setUser_email(object.getString("user_email"));
                        }
                        if (object.has("username")
                                && !TextUtils.isEmpty(object.getString("username"))
                                && !object.getString("username").equals("null")) {
                            eventRegistration.setUsername(object.getString("username"));
                        }
                        if (object.has("event_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_begins_date_time"))
                                && !object.getString("event_begins_date_time").equals("null")) {
                            eventRegistration.setEvent_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_begins_date_time")));
                        }
                        if (object.has("event_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_ends_date_time"))
                                && !object.getString("event_ends_date_time").equals("null")) {
                            eventRegistration.setEvent_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_ends_date_time")));
                        }
                        if (object.has("event_session_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_session_begins_date_time"))
                                && !object.getString("event_session_begins_date_time").equals("null")) {
                            eventRegistration.setEvent_session_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_begins_date_time")));
                        }
                        if (object.has("event_session_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_session_ends_date_time"))
                                && !object.getString("event_session_ends_date_time").equals("null")) {
                            eventRegistration.setEvent_session_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_ends_date_time")));
                        }
                        if (object.has("lat")
                                && !TextUtils.isEmpty(object.getString("lat"))
                                && !object.getString("lat").equals("null")) {
                            eventRegistration.setLat(object.getString("lat"));
                        }
                        if (object.has("lng")
                                && !TextUtils.isEmpty(object.getString("lng"))
                                && !object.getString("lng").equals("null")) {
                            eventRegistration.setLng(object.getString("lng"));
                        }
                        if (object.has("address1")
                                && !TextUtils.isEmpty(object.getString("address1"))
                                && !object.getString("address1").equals("null")) {
                            eventRegistration.setAddress1(object.getString("address1"));
                        }
                        if (object.has("event_regn_status")
                                && !TextUtils.isEmpty(object.getString("event_regn_status"))
                                && !object.getString("event_regn_status").equals("null")) {
                            eventRegistration.setEvent_regn_status(object.getString("event_regn_status"));
                        }
                        if (object.has("payment_method")
                                && !TextUtils.isEmpty(object.getString("payment_method"))
                                && !object.getString("payment_method").equals("null")) {
                            eventRegistration.setPayment_method(object.getString("payment_method"));
                        }
                        if (object.has("event_rate")
                                && !TextUtils.isEmpty(object.getString("event_rate"))
                                && !object.getString("event_rate").equals("null")) {
                            eventRegistration.setEvent_rate(object.getString("event_rate"));
                        }
                        if (object.has("event_price_total")
                                && !TextUtils.isEmpty(object.getString("event_price_total"))
                                && !object.getString("event_price_total").equals("null")) {
                            eventRegistration.setEvent_price_total(object.getString("event_price_total"));
                        }
                        if (object.has("event_regn_fee_category")
                                && !TextUtils.isEmpty(object.getString("event_regn_fee_category"))
                                && !object.getString("event_regn_fee_category").equals("null")) {
                            eventRegistration.setEvent_regn_fee_category(object.getString("event_regn_fee_category"));
                        }
                        if (object.has("regd_fee_combined")
                                && !TextUtils.isEmpty(object.getString("regd_fee_combined"))
                                && !object.getString("regd_fee_combined").equals("null")) {
                            eventRegistration.setRegd_fee_combined(object.getString("regd_fee_combined"));
                        }

                        if (object.has("full_event_regn")
                                && !TextUtils.isEmpty(object.getString("full_event_regn"))
                                && !object.getString("full_event_regn").equals("null")) {
                            eventRegistration.setFull_event_regn(object.getBoolean("full_event_regn"));
                        }
                        if (object.has("session_regn")
                                && !TextUtils.isEmpty(object.getString("session_regn"))
                                && !object.getString("session_regn").equals("null")) {
                            eventRegistration.setSession_regn(object.getBoolean("session_regn"));
                        }
                        if (object.has("event_parking_reserved")
                                && !TextUtils.isEmpty(object.getString("event_parking_reserved"))
                                && !object.getString("event_parking_reserved").equals("null")) {
                            eventRegistration.setEvent_parking_reserved(object.getBoolean("event_parking_reserved"));
                        }
                        if (object.has("session_parking_reserved")
                                && !TextUtils.isEmpty(object.getString("session_parking_reserved"))
                                && !object.getString("session_parking_reserved").equals("null")) {
                            eventRegistration.setSession_parking_reserved(object.getBoolean("session_parking_reserved"));
                        }
                        if (object.has("event_regn_approval_reqd")
                                && !TextUtils.isEmpty(object.getString("event_regn_approval_reqd"))
                                && !object.getString("event_regn_approval_reqd").equals("null")) {
                            eventRegistration.setEvent_regn_approval_reqd(object.getBoolean("event_regn_approval_reqd"));
                        }

                        eventRegistrations.add(eventRegistration);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null) {
                binding.rlProgressbar.setVisibility(View.GONE);
//                if (json != null) {
//                    if (eventRegistrations != null) {
//                        if (eventRegistrations.size() > 0) {
//                            binding.tvError.setVisibility(View.GONE);
//                            eventRegistrationsFiltered.clear();
//                            eventRegistrationsFiltered.addAll(eventRegistrations);
//                            eventRegAdapter.notifyDataSetChanged();
//                        } else {
//                            binding.tvError.setVisibility(View.VISIBLE);
//                        }
//                    }
//                }

                if (type != null && !TextUtils.isEmpty(type) && type.equalsIgnoreCase("User")) {
                    if (getActivity() != null) {
                        binding.rlProgressbar.setVisibility(View.GONE);

                        if (eventRegistrations != null) {
                            if (eventRegistrations.size() > 0) {
                                binding.tvError.setVisibility(View.GONE);
                                eventRegistrationsFiltered.clear();
                                eventRegistrationsFiltered.addAll(eventRegistrations);
                                eventRegAdapter.notifyDataSetChanged();
                            } else {
                                binding.tvError.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                } else {
                    new fetchRegUsers().execute();
                }
            }
            super.onPostExecute(s);
        }
    }

    private class fetchRegUsers extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json = new JSONObject();
        JSONArray json1 = new JSONArray();
        String regUrl = "";

        @Override
        protected void onPreExecute() {
            regUrl = "_table/event_regn_user_profile?filter=event_id=" + eventDefinition.getId();
            binding.rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            users.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + regUrl;
            Log.e("#DEBUG", "  fetchRegUsers: " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    json1 = json.getJSONArray("resource");

                    for (int i = 0; i < json1.length(); i++) {
                        JSONObject object = json1.getJSONObject(i);
                        User user = new User();
                        if (object.has("fname")
                                && !TextUtils.isEmpty(object.getString("fname"))
                                && !object.getString("fname").equals("null")) {
                            user.setFirstName(object.getString("fname"));
                        }
                        if (object.has("lname")
                                && !TextUtils.isEmpty(object.getString("lname"))
                                && !object.getString("lname").equals("null")) {
                            user.setLastName(object.getString("lname"));
                        }
                        if (object.has("email")
                                && !TextUtils.isEmpty(object.getString("email"))
                                && !object.getString("email").equals("null")) {
                            user.setEmail(object.getString("email"));
                        }
                        if (object.has("mobile")
                                && !TextUtils.isEmpty(object.getString("mobile"))
                                && !object.getString("mobile").equals("null")) {
                            user.setMobile(object.getString("mobile"));
                        }
                        if (object.has("gender")
                                && !TextUtils.isEmpty(object.getString("gender"))
                                && !object.getString("gender").equals("null")) {
                            user.setGender(object.getString("gender"));
                        }
                        if (object.has("age")
                                && !TextUtils.isEmpty(object.getString("age"))
                                && !object.getString("age").equals("null")) {
                            user.setAge(object.getString("age"));
                        }
                        if (object.has("event_fee_selected_type")
                                && !TextUtils.isEmpty(object.getString("event_fee_selected_type"))
                                && !object.getString("event_fee_selected_type").equals("null")) {
                            user.setEvent_fee_selected_type(object.getString("event_fee_selected_type"));
                        }
                        if (object.has("event_fee_selected")
                                && !TextUtils.isEmpty(object.getString("event_fee_selected"))
                                && !object.getString("event_fee_selected").equals("null")) {
                            user.setEvent_fee_selected(object.getString("event_fee_selected"));
                        }
                        if (object.has("event_registration_id")
                                && !TextUtils.isEmpty(object.getString("event_registration_id"))
                                && !object.getString("event_registration_id").equals("null")) {
                            user.setEvent_registration_id(object.getInt("event_registration_id"));
                        }

                        users.add(user);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null) {
                binding.rlProgressbar.setVisibility(View.GONE);

                if (eventRegistrations != null) {
                    if (eventRegistrations.size() > 0) {
                        binding.tvError.setVisibility(View.GONE);
                        eventRegistrationsFiltered.clear();
                        eventRegistrationsFiltered.addAll(eventRegistrations);
                        eventRegAdapter.notifyDataSetChanged();
                    } else {
                        binding.tvError.setVisibility(View.VISIBLE);
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {

        binding.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                eventRegAdapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                eventRegAdapter.getFilter().filter(s);
                return false;
            }
        });

    }

    @Override
    protected void initViews(View v) {

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = logindeatl.getString("id", "0");

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);

        eventRegAdapter = new EventRegAdapter();
        binding.rvRegistrations.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvRegistrations.setAdapter(eventRegAdapter);

    }

    public class addEventAttendance extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String urlReg = "_table/event_attendance";
        JSONObject json1;
        String re_id;
        ArrayList<User> users = new ArrayList<>();

        public addEventAttendance(ArrayList<User> users) {
            this.users.clear();
            this.users.addAll(users);
        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues = new HashMap<String, Object>();

            jsonValues.put("date_time", DateTimeUtils.getSqlFormatDate(Calendar.getInstance().getTime()));
            if (this.users.size() > 0) {
                jsonValues.put("fname", this.users.get(0).getFirstName());
                jsonValues.put("lname", this.users.get(0).getLastName());
                jsonValues.put("full_name", this.users.get(0).getFirstName() + " " + this.users.get(0).getLastName());
                jsonValues.put("email", this.users.get(0).getEmail());
                jsonValues.put("mobile", this.users.get(0).getMobile());
                jsonValues.put("age", this.users.get(0).getAge());
                jsonValues.put("gender", this.users.get(0).getGender());
                jsonValues.put("event_fee_selected_type", this.users.get(0).getEvent_fee_selected_type());
                jsonValues.put("event_fee_selected", this.users.get(0).getEvent_fee_selected());
                jsonValues.put("event_registration_id", this.users.get(0).getEvent_registration_id());
            }
            jsonValues.put("event_id", eventDefinition.getEvent_id());
            jsonValues.put("company_id", eventDefinition.getCompany_id());
            jsonValues.put("event_date", eventDefinition.getEvent_begins_date_time());
            jsonValues.put("ip", getLocalIpAddress());

            jsonValues.put("user_id", logindeatl.getString("id", ""));
            jsonValues.put("username", logindeatl.getString("name", ""));

            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + urlReg;
            HttpResponse response = null;
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            post.setEntity(entity);
            Log.e("#DEBUG", "  addEventAttendance:  " + url);
            Log.e("#DEBUG", "   addEventAttendance Params: " + String.valueOf(json));
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   addEventAttendance:  Response:  " + responseStr);
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.e("#DEBUG", "  addEventAttendance: ResponseID: " + re_id);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.hide();
            if (getActivity() != null && json1 != null) {
                if (re_id != null && !re_id.equals("null")) {
                    Toast.makeText(getActivity(), "Attendance Added!", Toast.LENGTH_SHORT).show();
                }
            }
            super.onPostExecute(s);
        }
    }

    private class EventRegAdapter extends RecyclerView.Adapter<EventRegAdapter.PartnerHolder> implements Filterable {

        @NonNull
        @Override
        public EventRegAdapter.PartnerHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new PartnerHolder(LayoutInflater.from(getActivity())
                    .inflate(R.layout.item_event_registered, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull EventRegAdapter.PartnerHolder partnerHolder, int i) {

            EventRegistration eventRegistration = eventRegistrationsFiltered.get(i);
            if (eventRegistration != null) {
                partnerHolder.tvUserName.setText(eventRegistration.getUsername());
                partnerHolder.tvEventName.setText(eventRegistration.getEvent_name());
                partnerHolder.tvStatus.setText(eventRegistration.getEvent_regn_status());
                partnerHolder.tvAmount.setText(String.format("$%s", eventRegistration.getEvent_price_total()));
                partnerHolder.tvDate.setText(DateTimeUtils.getMMMDDYYYY(eventRegistration.getDate_time()));

                for (int j = 0; j < users.size(); j++) {
                    if (eventRegistration.getId() == users.get(j).getEvent_registration_id()) {
                        partnerHolder.tempUsers.add(users.get(j));
                    }
                }
                EventUserAdapter eventUserAdapter = new EventUserAdapter(partnerHolder.tempUsers);
                partnerHolder.rvEventUsers.setLayoutManager(new LinearLayoutManager(getActivity()));
                partnerHolder.rvEventUsers.setAdapter(eventUserAdapter);

                if (partnerHolder.tempUsers.size() == 0) {
                    partnerHolder.llUsers.setVisibility(View.GONE);
                } else partnerHolder.llUsers.setVisibility(View.VISIBLE);

            }
        }

        @Override
        public int getItemCount() {
            return eventRegistrationsFiltered.size();
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {
                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        eventRegistrationsFiltered = eventRegistrations;
                    } else {
                        ArrayList<EventRegistration> filteredList = new ArrayList<>();
                        for (EventRegistration row : eventRegistrations) {

                            // name match condition. this might differ depending on your requirement
                            // here we are looking for name or phone number match
                            if (row.getUser_email().toLowerCase().contains(charString.toLowerCase())) {
                                filteredList.add(row);
                            }
                        }

                        eventRegistrationsFiltered = filteredList;
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = eventRegistrationsFiltered;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    eventRegistrationsFiltered = (ArrayList<EventRegistration>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }

        class PartnerHolder extends RecyclerView.ViewHolder {
            TextView tvUserName, tvDate, tvStatus, tvAmount, tvEventName;
            ImageView ivLogo;
            RecyclerView rvEventUsers;
            Button btnCheckIn;
            LinearLayout llUsers;
            ArrayList<User> tempUsers = new ArrayList<>();

            PartnerHolder(@NonNull View itemView) {
                super(itemView);
                ivLogo = itemView.findViewById(R.id.ivLogo);
                tvEventName = itemView.findViewById(R.id.tvEventName);
                tvUserName = itemView.findViewById(R.id.tvUserName);
                tvDate = itemView.findViewById(R.id.tvDate);
                tvStatus = itemView.findViewById(R.id.tvStatus);
                tvAmount = itemView.findViewById(R.id.tvAmount);
                btnCheckIn = itemView.findViewById(R.id.btnCheckIn);
                rvEventUsers = itemView.findViewById(R.id.rvEventUsers);
                llUsers = itemView.findViewById(R.id.llUsers);

                itemView.setOnClickListener(view -> {

                });

                btnCheckIn.setOnClickListener(v -> {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Are you sure, You want to Check-In?");
                    builder.setPositiveButton("Yes", (dialog, which) -> {
                        dialog.dismiss();
                        new addEventAttendance(tempUsers).execute();
                    });
                    builder.setNegativeButton("No", (dialog, which) -> {
                        dialog.dismiss();
                    });

                    builder.create().show();

                });
            }
        }
    }

    private class EventUserAdapter extends RecyclerView.Adapter<EventUserAdapter.EventHolder> {
        private ArrayList<User> users = new ArrayList<>();

        public EventUserAdapter(ArrayList<User> users) {
            this.users.clear();
            this.users.addAll(users);
        }

        @NonNull
        @Override
        public EventUserAdapter.EventHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new EventHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_user_display, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull EventUserAdapter.EventHolder holder, int i) {
            User user = users.get(i);
            if (user != null) {
                holder.tvName.setText(String.format("%s %s", user.getFirstName(), user.getLastName()));
                holder.tvGenderAge.setText(String.format("%s(%s)", user.getGender().toString().substring(0, 1), user.getAge()));
                holder.tvCategory.setText(String.format("%s($%s)", user.getEvent_fee_selected_type(), user.getEvent_fee_selected()));
            }
        }

        @Override
        public int getItemCount() {
            return users.size();
        }

        class EventHolder extends RecyclerView.ViewHolder {
            TextView tvName, tvGenderAge, tvCategory;

            EventHolder(@NonNull View itemView) {
                super(itemView);
                tvName = itemView.findViewById(R.id.tvName);
                tvGenderAge = itemView.findViewById(R.id.tvGenderAge);
                tvCategory = itemView.findViewById(R.id.tvCategory);
            }
        }
    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("ip", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("aax", ex.toString());
        }
        return null;
    }
}
