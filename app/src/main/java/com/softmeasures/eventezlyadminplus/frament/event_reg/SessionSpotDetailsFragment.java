package com.softmeasures.eventezlyadminplus.frament.event_reg;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragSessionSpotDetailsBinding;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.EventSession;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class SessionSpotDetailsFragment extends BaseFragment {

    private FragSessionSpotDetailsBinding binding;
    private int eventSessionId;
    private ProgressDialog progressDialog;
    private EventSession eventDefinition;
    private SimpleDateFormat dateFormat;
    private SimpleDateFormat dateFormatInput;
    private item parkingRule;

    private ArrayList<String> eventImages = new ArrayList<>();
    private EventImageAdapter eventImageAdapter;
    SimpleDateFormat dateFormatMain = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd MMM, hh:mm a", Locale.getDefault());

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            eventSessionId = getArguments().getInt("eventSessionId");
            parkingRule = new Gson().fromJson(getArguments().getString("parkingRule"), item.class);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_session_spot_details, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        new fetchSessionEventDetails().execute();
    }

    private class fetchSessionEventDetails extends AsyncTask<String, String, String> {
        JSONObject json;
        JSONArray json1;
        String eventDefUrl = "_table/event_session_definitions";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            eventDefUrl = "_table/event_session_definitions?filter=id=" + eventSessionId;
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + eventDefUrl;
            Log.e("#DEBUG", "      fetchSessionEventDetails:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");

                    for (int j = 0; j < jsonArray.length(); j++) {
                        eventDefinition = new EventSession();
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            eventDefinition.setId(object.getInt("id"));
                        }
                        if (object.has("date_time")
                                && !TextUtils.isEmpty(object.getString("date_time"))
                                && !object.getString("date_time").equals("null")) {
                            eventDefinition.setDate_time(object.getString("date_time"));
                        }
                        if (object.has("manager_type")
                                && !TextUtils.isEmpty(object.getString("manager_type"))
                                && !object.getString("manager_type").equals("null")) {
                            eventDefinition.setManager_type(object.getString("manager_type"));
                        }
                        if (object.has("manager_type_id")
                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
                                && !object.getString("manager_type_id").equals("null")) {
                            eventDefinition.setManager_type_id(object.getInt("manager_type_id"));
                        }
                        if (object.has("twp_id")
                                && !TextUtils.isEmpty(object.getString("twp_id"))
                                && !object.getString("twp_id").equals("null")) {
                            eventDefinition.setTwp_id(object.getInt("twp_id"));
                        }
                        if (object.has("township_code")
                                && !TextUtils.isEmpty(object.getString("township_code"))
                                && !object.getString("township_code").equals("null")) {
                            eventDefinition.setTownship_code(object.getString("township_code"));
                        }
                        if (object.has("township_name")
                                && !TextUtils.isEmpty(object.getString("township_name"))
                                && !object.getString("township_name").equals("null")) {
                            eventDefinition.setTownship_name(object.getString("township_name"));
                        }
                        if (object.has("company_id")
                                && !TextUtils.isEmpty(object.getString("company_id"))
                                && !object.getString("company_id").equals("null")) {
                            eventDefinition.setCompany_id(object.getInt("company_id"));
                        }
                        if (object.has("company_code")
                                && !TextUtils.isEmpty(object.getString("company_code"))
                                && !object.getString("company_code").equals("null")) {
                            eventDefinition.setCompany_code(object.getString("company_code"));
                        }
                        if (object.has("company_name")
                                && !TextUtils.isEmpty(object.getString("company_name"))
                                && !object.getString("company_name").equals("null")) {
                            eventDefinition.setCompany_name(object.getString("company_name"));
                        }
                        if (object.has("event_id")
                                && !TextUtils.isEmpty(object.getString("event_id"))
                                && !object.getString("event_id").equals("null")) {
                            eventDefinition.setEvent_id(object.getInt("event_id"));
                        }
                        if (object.has("event_name")
                                && !TextUtils.isEmpty(object.getString("event_name"))
                                && !object.getString("event_name").equals("null")) {
                            eventDefinition.setEvent_name(object.getString("event_name"));
                        }
                        if (object.has("event_type")
                                && !TextUtils.isEmpty(object.getString("event_type"))
                                && !object.getString("event_type").equals("null")) {
                            eventDefinition.setEvent_type(object.getString("event_type"));
                        }
                        if (object.has("event_code")
                                && !TextUtils.isEmpty(object.getString("event_code"))
                                && !object.getString("event_code").equals("null")) {
                            eventDefinition.setEvent_code(object.getString("event_code"));
                        }
                        if (object.has("event_session_id")
                                && !TextUtils.isEmpty(object.getString("event_session_id"))
                                && !object.getString("event_session_id").equals("null")) {
                            eventDefinition.setEvent_session_id(object.getInt("event_session_id"));
                        }
                        if (object.has("event_session_type")
                                && !TextUtils.isEmpty(object.getString("event_session_type"))
                                && !object.getString("event_session_type").equals("null")) {
                            eventDefinition.setEvent_session_type(object.getString("event_session_type"));
                        }
                        if (object.has("event_session_code")
                                && !TextUtils.isEmpty(object.getString("event_session_code"))
                                && !object.getString("event_session_code").equals("null")) {
                            eventDefinition.setEvent_session_code(object.getString("event_session_code"));
                        }
                        if (object.has("event_session_name")
                                && !TextUtils.isEmpty(object.getString("event_session_name"))
                                && !object.getString("event_session_name").equals("null")) {
                            eventDefinition.setEvent_session_name(object.getString("event_session_name"));
                        }
                        if (object.has("event_session_short_description")
                                && !TextUtils.isEmpty(object.getString("event_session_short_description"))
                                && !object.getString("event_session_short_description").equals("null")) {
                            eventDefinition.setEvent_session_short_description(object.getString("event_session_short_description"));
                        }
                        if (object.has("event_session_long_description")
                                && !TextUtils.isEmpty(object.getString("event_session_long_description"))
                                && !object.getString("event_session_long_description").equals("null")) {
                            eventDefinition.setEvent_session_long_description(object.getString("event_session_long_description"));
                        }
                        if (object.has("event_session_link_on_web")
                                && !TextUtils.isEmpty(object.getString("event_session_link_on_web"))
                                && !object.getString("event_session_link_on_web").equals("null")) {
                            eventDefinition.setEvent_session_link_on_web(object.getString("event_session_link_on_web"));
                        }
                        if (object.has("event_session_link_twitter")
                                && !TextUtils.isEmpty(object.getString("event_session_link_twitter"))
                                && !object.getString("event_session_link_twitter").equals("null")) {
                            eventDefinition.setEvent_session_link_twitter(object.getString("event_session_link_twitter"));
                        }
                        if (object.has("event_session_link_whatsapp")
                                && !TextUtils.isEmpty(object.getString("event_session_link_whatsapp"))
                                && !object.getString("event_session_link_whatsapp").equals("null")) {
                            eventDefinition.setEvent_session_link_whatsapp(object.getString("event_session_link_whatsapp"));
                        }
                        if (object.has("event_session_link_other_media")
                                && !TextUtils.isEmpty(object.getString("event_session_link_other_media"))
                                && !object.getString("event_session_link_other_media").equals("null")) {
                            eventDefinition.setEvent_session_link_other_media(object.getString("event_session_link_other_media"));
                        }
                        if (object.has("event_session_link_other_media")
                                && !TextUtils.isEmpty(object.getString("event_session_link_other_media"))
                                && !object.getString("event_session_link_other_media").equals("null")) {
                            eventDefinition.setEvent_session_link_other_media(object.getString("event_session_link_other_media"));
                        }
                        if (object.has("company_logo")
                                && !TextUtils.isEmpty(object.getString("company_logo"))
                                && !object.getString("company_logo").equals("null")) {
                            eventDefinition.setCompany_logo(object.getString("company_logo"));
                        }
                        if (object.has("event_logo")
                                && !TextUtils.isEmpty(object.getString("event_logo"))
                                && !object.getString("event_logo").equals("null")) {
                            eventDefinition.setEvent_logo(object.getString("event_logo"));
                        }
                        if (object.has("event_session_logo")
                                && !TextUtils.isEmpty(object.getString("event_session_logo"))
                                && !object.getString("event_session_logo").equals("null")) {
                            eventDefinition.setEvent_session_logo(object.getString("event_session_logo"));
                        }
                        if (object.has("event_session_image1")
                                && !TextUtils.isEmpty(object.getString("event_session_image1"))
                                && !object.getString("event_session_image1").equals("null")) {
                            eventDefinition.setEvent_session_image1(object.getString("event_session_image1"));
                        }
                        if (object.has("event_session_image2")
                                && !TextUtils.isEmpty(object.getString("event_session_image2"))
                                && !object.getString("event_session_image2").equals("null")) {
                            eventDefinition.setEvent_session_image2(object.getString("event_session_image2"));
                        }
                        if (object.has("event_session_blob_image")
                                && !TextUtils.isEmpty(object.getString("event_session_blob_image"))
                                && !object.getString("event_session_blob_image").equals("null")) {
                            eventDefinition.setEvent_session_blob_image(object.getString("event_session_blob_image"));
                        }
                        if (object.has("event_session_address")
                                && !TextUtils.isEmpty(object.getString("event_session_address"))
                                && !object.getString("event_session_address").equals("null")) {
                            eventDefinition.setEvent_session_address(object.getString("event_session_address"));
                        }
                        if (object.has("covered_locations")
                                && !TextUtils.isEmpty(object.getString("covered_locations"))
                                && !object.getString("covered_locations").equals("null")) {
                            eventDefinition.setCovered_locations(object.getString("covered_locations"));
                        }
                        if (object.has("session_regn_needed_approval")
                                && !TextUtils.isEmpty(object.getString("session_regn_needed_approval"))
                                && !object.getString("session_regn_needed_approval").equals("null")) {
                            eventDefinition.setSession_regn_needed_approval(object.getBoolean("session_regn_needed_approval"));
                        }
                        if (object.has("requirements")
                                && !TextUtils.isEmpty(object.getString("requirements"))
                                && !object.getString("requirements").equals("null")) {
                            eventDefinition.setRequirements(object.getString("requirements"));
                        }
                        if (object.has("appl_req_download")
                                && !TextUtils.isEmpty(object.getString("appl_req_download"))
                                && !object.getString("appl_req_download").equals("null")) {
                            eventDefinition.setAppl_req_download(object.getString("appl_req_download"));
                        }
                        if (object.has("cost")
                                && !TextUtils.isEmpty(object.getString("cost"))
                                && !object.getString("cost").equals("null")) {
                            eventDefinition.setCost(object.getString("cost"));
                        }
                        if (object.has("year")
                                && !TextUtils.isEmpty(object.getString("year"))
                                && !object.getString("year").equals("null")) {
                            eventDefinition.setYear(object.getString("year"));
                        }
                        if (object.has("location_address")
                                && !TextUtils.isEmpty(object.getString("location_address"))
                                && !object.getString("location_address").equals("null")) {
                            eventDefinition.setLocation_address(object.getString("location_address"));
                        }
                        if (object.has("scheme_type")
                                && !TextUtils.isEmpty(object.getString("scheme_type"))
                                && !object.getString("scheme_type").equals("null")) {
                            eventDefinition.setScheme_type(object.getString("scheme_type"));
                        }
                        if (object.has("event_prefix")
                                && !TextUtils.isEmpty(object.getString("event_prefix"))
                                && !object.getString("event_prefix").equals("null")) {
                            eventDefinition.setEvent_prefix(object.getString("event_prefix"));
                        }

                        if (object.has("event_session_prefix")
                                && !TextUtils.isEmpty(object.getString("event_session_prefix"))
                                && !object.getString("event_session_prefix").equals("null")) {
                            eventDefinition.setEvent_session_prefix(object.getString("event_session_prefix"));
                        }
                        if (object.has("event_nextnum")
                                && !TextUtils.isEmpty(object.getString("event_nextnum"))
                                && !object.getString("event_nextnum").equals("null")) {
                            eventDefinition.setEvent_nextnum(object.getInt("event_nextnum"));
                        }
                        if (object.has("event_session_nextnum")
                                && !TextUtils.isEmpty(object.getString("event_session_nextnum"))
                                && !object.getString("event_session_nextnum").equals("null")) {
                            eventDefinition.setEvent_session_nextnum(object.getInt("event_session_nextnum"));
                        }
                        if (object.has("event_session_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_session_begins_date_time"))
                                && !object.getString("event_session_begins_date_time").equals("null")) {
                            eventDefinition.setEvent_session_begins_date_time(object.getString("event_session_begins_date_time"));
                        }
                        if (object.has("event_session_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_session_ends_date_time"))
                                && !object.getString("event_session_ends_date_time").equals("null")) {
                            eventDefinition.setEvent_session_ends_date_time(object.getString("event_session_ends_date_time"));
                        }
                        if (object.has("event_session_parking_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_session_parking_begins_date_time"))
                                && !object.getString("event_session_parking_begins_date_time").equals("null")) {
                            eventDefinition.setEvent_session_parking_begins_date_time(object.getString("event_session_parking_begins_date_time"));
                        }
                        if (object.has("event_session_parking_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_session_parking_ends_date_time"))
                                && !object.getString("event_session_parking_ends_date_time").equals("null")) {
                            eventDefinition.setEvent_session_parking_ends_date_time(object.getString("event_session_parking_ends_date_time"));
                        }

                        if (object.has("event_session_multi_dates")
                                && !TextUtils.isEmpty(object.getString("event_session_multi_dates"))
                                && !object.getString("event_session_multi_dates").equals("null")) {
                            eventDefinition.setEvent_session_multi_dates(object.getString("event_session_multi_dates"));
                        }

                        if (object.has("event_session_parking_timings")
                                && !TextUtils.isEmpty(object.getString("event_session_parking_timings"))
                                && !object.getString("event_session_parking_timings").equals("null")) {
                            eventDefinition.setEvent_session_parking_timings(object.getString("event_session_parking_timings"));
                        }

                        if (object.has("event_session_timings")
                                && !TextUtils.isEmpty(object.getString("event_session_timings"))
                                && !object.getString("event_session_timings").equals("null")) {
                            eventDefinition.setEvent_session_timings(object.getString("event_session_timings"));
                        }

                        if (object.has("expires_by")
                                && !TextUtils.isEmpty(object.getString("expires_by"))
                                && !object.getString("expires_by").equals("null")) {
                            eventDefinition.setExpires_by(object.getString("expires_by"));
                        }
                        if (object.has("regn_reqd")
                                && !TextUtils.isEmpty(object.getString("regn_reqd"))
                                && !object.getString("regn_reqd").equals("null")) {
                            eventDefinition.setRegn_reqd(object.getBoolean("regn_reqd"));
                        }
                        if (object.has("event_session_regn_allowed")
                                && !TextUtils.isEmpty(object.getString("event_session_regn_allowed"))
                                && !object.getString("event_session_regn_allowed").equals("null")) {
                            eventDefinition.setEvent_session_regn_allowed(object.getBoolean("event_session_regn_allowed"));
                        }
                        if (object.has("event_session_regn_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_regn_fee"))
                                && !object.getString("event_session_regn_fee").equals("null")) {
                            eventDefinition.setEvent_session_regn_fee(object.getString("event_session_regn_fee"));
                        }
                        if (object.has("event_session_led_by")
                                && !TextUtils.isEmpty(object.getString("event_session_led_by"))
                                && !object.getString("event_session_led_by").equals("null")) {
                            eventDefinition.setEvent_session_led_by(object.getString("event_session_led_by"));
                        }
                        if (object.has("event_session_leaders_bio")
                                && !TextUtils.isEmpty(object.getString("event_session_leaders_bio"))
                                && !object.getString("event_session_leaders_bio").equals("null")) {
                            eventDefinition.setEvent_session_leaders_bio(object.getString("event_session_leaders_bio"));
                        }
                        if (object.has("regn_reqd_for_parking")
                                && !TextUtils.isEmpty(object.getString("regn_reqd_for_parking"))
                                && !object.getString("regn_reqd_for_parking").equals("null")) {
                            eventDefinition.setRegn_reqd_for_parking(object.getBoolean("regn_reqd_for_parking"));
                        }
                        if (object.has("renewable")
                                && !TextUtils.isEmpty(object.getString("renewable"))
                                && !object.getString("renewable").equals("null")) {
                            eventDefinition.setRenewable(object.getBoolean("renewable"));
                        }
                        if (object.has("active")
                                && !TextUtils.isEmpty(object.getString("active"))
                                && !object.getString("active").equals("null")) {
                            eventDefinition.setActive(object.getBoolean("active"));
                        }
                        if (object.has("event_session_parking_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_parking_fee"))
                                && !object.getString("event_session_parking_fee").equals("null")) {
                            eventDefinition.setEvent_session_parking_fee(object.getString("event_session_parking_fee"));
                        }

                        if (object.has("free_session")
                                && !TextUtils.isEmpty(object.getString("free_session"))
                                && !object.getString("free_session").equals("null")) {
                            eventDefinition.setFree_session(object.getBoolean("free_session"));
                        }

                        if (object.has("free_session_parking")
                                && !TextUtils.isEmpty(object.getString("free_session_parking"))
                                && !object.getString("free_session_parking").equals("null")) {
                            eventDefinition.setFree_session_parking(object.getBoolean("free_session_parking"));
                        }

                        if (object.has("event_session_regn_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_regn_fee"))
                                && !object.getString("event_session_regn_fee").equals("null")) {
                            eventDefinition.setEvent_session_regn_fee(object.getString("event_session_regn_fee"));
                        }

                        if (object.has("event_session_youth_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_youth_fee"))
                                && !object.getString("event_session_youth_fee").equals("null")) {
                            eventDefinition.setEvent_session_youth_fee(object.getString("event_session_youth_fee"));
                        }

                        if (object.has("event_session_child_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_child_fee"))
                                && !object.getString("event_session_child_fee").equals("null")) {
                            eventDefinition.setEvent_session_child_fee(object.getString("event_session_child_fee"));
                        }

                        if (object.has("event_session_student_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_student_fee"))
                                && !object.getString("event_session_student_fee").equals("null")) {
                            eventDefinition.setEvent_session_student_fee(object.getString("event_session_student_fee"));
                        }

                        if (object.has("event_session_minister_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_minister_fee"))
                                && !object.getString("event_session_minister_fee").equals("null")) {
                            eventDefinition.setEvent_session_minister_fee(object.getString("event_session_minister_fee"));
                        }

                        if (object.has("event_session_clergy_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_clergy_fee"))
                                && !object.getString("event_session_clergy_fee").equals("null")) {
                            eventDefinition.setEvent_session_clergy_fee(object.getString("event_session_clergy_fee"));
                        }

                        if (object.has("event_session_promo_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_promo_fee"))
                                && !object.getString("event_session_promo_fee").equals("null")) {
                            eventDefinition.setEvent_session_promo_fee(object.getString("event_session_promo_fee"));
                        }

                        if (object.has("event_session_senior_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_senior_fee"))
                                && !object.getString("event_session_senior_fee").equals("null")) {
                            eventDefinition.setEvent_session_senior_fee(object.getString("event_session_senior_fee"));
                        }

                        if (object.has("event_session_staff_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_staff_fee"))
                                && !object.getString("event_session_staff_fee").equals("null")) {
                            eventDefinition.setEvent_session_staff_fee(object.getString("event_session_staff_fee"));
                        }

                        if (object.has("session_link_array")
                                && !TextUtils.isEmpty(object.getString("session_link_array"))
                                && !object.getString("session_link_array").equals("null")) {
                            eventDefinition.setSession_link_array(object.getString("session_link_array"));
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                progressDialog.dismiss();
                updateViews();
            }
        }
    }

    @Override
    protected void updateViews() {

        if (eventDefinition != null) {

            if (eventDefinition.getEventCategory() != null
                    && !TextUtils.isEmpty(eventDefinition.getEventCategory())
                    && !eventDefinition.getEventCategory().equals("null")) {
                if (eventDefinition.getEventCategory().equals("past")) {
                    binding.llBottomOptions.setVisibility(View.GONE);
                } else {
                    binding.llBottomOptions.setVisibility(View.VISIBLE);
                }
            }


            if (eventDefinition.getEvent_session_image1() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_session_image1())
                    && !eventDefinition.getEvent_session_image1().equals("null")) {
                eventImages.add(eventDefinition.getEvent_session_image1());
            }
            if (eventDefinition.getEvent_session_image2() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_session_image2())
                    && !eventDefinition.getEvent_session_image2().equals("null")) {
                eventImages.add(eventDefinition.getEvent_session_image2());
            }
            eventImageAdapter.notifyDataSetChanged();

            if (eventDefinition.getCompany_logo() != null
                    && !TextUtils.isEmpty(eventDefinition.getCompany_logo())) {
                Glide.with(getActivity()).load(eventDefinition.getCompany_logo())
                        .into(binding.ivEventCompanyLogo);
            }
            if (!TextUtils.isEmpty(eventDefinition.getEvent_name())) {
                binding.tvEventTitle.setText(eventDefinition.getEvent_name());
            }
            if (!TextUtils.isEmpty(eventDefinition.getEvent_type())) {
                binding.tvEventType.setText(eventDefinition.getEvent_type());
            }
            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_short_description())) {
                binding.tvEventShortDesc.setText(eventDefinition.getEvent_session_short_description());
            }
            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_long_description())) {
                binding.tvEventLongDesc.setText(eventDefinition.getEvent_session_long_description());
            }
            if (eventDefinition.getEvent_session_begins_date_time() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_session_begins_date_time())) {
                try {
                    binding.tvEventStartAt.setText(dateFormat.format(dateFormatInput.parse(eventDefinition.getEvent_session_begins_date_time())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (eventDefinition.getEvent_session_ends_date_time() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_session_ends_date_time())) {
                try {
                    binding.tvEventEndAt.setText(dateFormat.format(dateFormatInput.parse(eventDefinition.getEvent_session_ends_date_time())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_parking_begins_date_time())
                    && !TextUtils.isEmpty(eventDefinition.getEvent_session_parking_ends_date_time())) {
                try {
                    binding.tvParkingTime.setText(String.format("%s to %s",
                            dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_session_parking_begins_date_time())),
                            dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_session_parking_ends_date_time()))));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_regn_fee())) {
                binding.tvEventRate.setText(String.format("$%s", eventDefinition.getEvent_session_regn_fee()));
            } else {
                binding.tvEventRate.setVisibility(View.GONE);
                binding.tvLabelCost.setVisibility(View.VISIBLE);
            }

            if (eventDefinition.getEvent_session_multi_dates() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_session_multi_dates())) {
                try {
                    JSONArray jsonArray = new JSONArray(eventDefinition.getEvent_session_multi_dates());
                    JSONArray jsonArrayTime = new JSONArray(eventDefinition.getEvent_session_timings());
                    JSONArray jsonArrayParking = new JSONArray(eventDefinition.getEvent_session_parking_timings());
                    StringBuilder stringBuilder = new StringBuilder();
                    StringBuilder stringBuilderParking = new StringBuilder();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONArray jsonArray1 = jsonArray.getJSONArray(i);
                        JSONArray jsonArray2 = jsonArrayTime.getJSONArray(i);
                        JSONArray jsonArray3 = jsonArrayParking.getJSONArray(i);
                        if (jsonArray1 != null && jsonArray1.length() > 0) {
                            if (i != 0) {
                                stringBuilder.append("\n");
                                stringBuilderParking.append("\n");
                            }
                            stringBuilder.append(DateTimeUtils.getMMMDDYYYY(jsonArray1.get(0).toString()));
                            stringBuilderParking.append(DateTimeUtils.getMMMDDYYYY(jsonArray1.get(0).toString()));
                            stringBuilder.append("      ");
                            stringBuilderParking.append("      ");
                            if (jsonArray2 != null && jsonArray2.length() > 0) {
                                stringBuilder.append(jsonArray2.get(0).toString());
                            }
                            if (jsonArray3 != null && jsonArray3.length() > 0) {
                                stringBuilderParking.append(jsonArray3.get(0).toString());
                            }
                        }
                    }
                    binding.tvParkingTime.setText(stringBuilderParking.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(eventDefinition.getLocation_address());
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONArray jsonArray1 = jsonArray.getJSONArray(i);
                    Log.e("#DEBUG", "    Address at POS: " + i + "  :" + jsonArray1.getString(0));
                    binding.tvLocationAddress.setText(jsonArray1.getString(0));

                    JSONArray jsonArray11 = new JSONArray(eventDefinition.getEvent_session_address());
                    JSONArray jsonArray22 = jsonArray11.getJSONArray(i);
                    binding.tvEventAddress.setText(jsonArray22.getString(0));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (parkingRule != null) {
            binding.tvParkingRate.setText(String.format("$%s/%s %s", parkingRule.getPricing(),
                    parkingRule.getPricing_duration(), parkingRule.getDuration_unit()));
        }

    }

    @Override
    protected void setListeners() {

        binding.tvBtnRegisterNow.setOnClickListener(v -> {
            ((vchome) getActivity()).show_back_button();
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            EventRegistrationFragment pay = new EventRegistrationFragment();
            Bundle bundle = new Bundle();
            if (getArguments() != null) {
                bundle = getArguments();
            }
            bundle.putBoolean("isSessionRegistration", true);
            bundle.putString("sessionDefinition", new Gson().toJson(eventDefinition));
            bundle.putString("parkingRule", new Gson().toJson(parkingRule));
//            bundle.putString("rParking", new Gson().toJson(rParking));
            pay.setArguments(bundle);
            ft.add(R.id.My_Container_1_ID, pay);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(pay);
            ft.commitAllowingStateLoss();
        });

    }

    @Override
    protected void initViews(View v) {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Registering for event...");
        progressDialog.setCancelable(false);

        dateFormat = new SimpleDateFormat("dd MMM, hh:mm a", Locale.getDefault());
        dateFormatInput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        eventImageAdapter = new EventImageAdapter();
        binding.vpEventImage.setAdapter(eventImageAdapter);

    }

    private class EventImageAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return eventImages.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == ((LinearLayout) o);
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View itemView = LayoutInflater.from(getActivity()).inflate(R.layout.item_event_image, container, false);

            ImageView imageView = itemView.findViewById(R.id.ivEventImage);
            Glide.with(getActivity()).load(eventImages.get(position)).into(imageView);

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((LinearLayout) object);
        }
    }
}
