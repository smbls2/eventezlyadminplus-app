package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.frament.Fragment_permit_definitions.array_list;

public class Fragment_permit_definitions_Four_Edit extends Fragment {

    ConnectionDetector cd;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar;
    View mView;
    String id, select_valid = "YearEnd";
    ListView listofvehicleno;

    EditText edt_PermitName, edt_Fee, edt_Requirement;
    Button btn_upload, btn_Done;
    Spinner sp_Valid;
    public ArrayList<String> arrayList_valid;
    TextView txt_File;
    AmazonS3 s3;
    TransferUtility transferUtility;
    StringBuffer file_uploadPdf;
    String final_file_uploadPdf = "";
    Address address;
    String get_Address;
    int i = 0, P = 0;
    String Permit_Ids = "";
    public String file_download_path = "";
    File dir_File;
    RadioGroup rg_PermitType, rg_Renewable;
    RadioButton rb_PermitType, rb_Renewable, rb_Yes, rb_No, rb_RESIDENT, rb_NON_RESIDENT;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_permit_definitions_four, container, false);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        file_download_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" +
                getActivity().getPackageName() + "/Images/";
        dir_File = new File(file_download_path);
        if (!dir_File.exists()) {
            if (!dir_File.mkdirs()) {
            }
        }
        s3 = new AmazonS3Client(new BasicAWSCredentials("AKIAJNG22KFRVUAICSEA", "NSrQR/yAg52RPD3kp3FMb3NO+Vrxuty4iAwPU4th"));
        transferUtility = new TransferUtility(s3, getActivity().getApplicationContext());
        file_uploadPdf = new StringBuffer();

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        id = logindeatl.getString("id", "null");
        arrayList_valid = new ArrayList<String>();
        arrayList_valid.add("365 days");
        arrayList_valid.add("YearEnd");
        arrayList_valid.add("MonthEnd");
        arrayList_valid.add("30 Days");
        arrayList_valid.add("7 Days");
        arrayList_valid.add("Weekend");
        arrayList_valid.add("24 Hours");
        arrayList_valid.add("Daily");

        ints();

        return mView;
    }

    public void ints() {

        rg_PermitType = (RadioGroup) mView.findViewById(R.id.rg_PermitType);
        rg_Renewable = (RadioGroup) mView.findViewById(R.id.rg_Renewable);
        rb_RESIDENT = (RadioButton) mView.findViewById(R.id.rb_RESIDENT);
        rb_NON_RESIDENT = (RadioButton) mView.findViewById(R.id.rb_NON_RESIDENT);
        rb_Yes = (RadioButton) mView.findViewById(R.id.rb_Yes);
        rb_No = (RadioButton) mView.findViewById(R.id.rb_No);
        edt_PermitName = (EditText) mView.findViewById(R.id.edt_PermitName);
        edt_Fee = (EditText) mView.findViewById(R.id.edt_Fee);
        edt_Requirement = (EditText) mView.findViewById(R.id.edt_Requirement);
        btn_upload = (Button) mView.findViewById(R.id.btn_upload);
        btn_Done = (Button) mView.findViewById(R.id.btn_Done);
        sp_Valid = (Spinner) mView.findViewById(R.id.sp_Valid);
        txt_File = (TextView) mView.findViewById(R.id.txt_File);

        progressBar = (ProgressBar) mView.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) mView.findViewById(R.id.rl_progressbar);
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ArrayAdapter aa = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, arrayList_valid);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_Valid.setAdapter(aa);
        sp_Valid.setSelection(1);

        onClicks();
        currentlocation1();

    }

    public void onClicks() {

        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), FileActivity.class);
                i.putExtra("isdoc", true);
                startActivityForResult(i, 8);
            }
        });

        btn_Done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = rg_PermitType.getCheckedRadioButtonId();
                rb_PermitType = (RadioButton) mView.findViewById(selectedId);
                int selectedYes = rg_Renewable.getCheckedRadioButtonId();
                rb_Renewable = (RadioButton) mView.findViewById(selectedYes);
                if (edt_PermitName.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getContext(), "Please Enter Permit Name", Toast.LENGTH_LONG).show();
                } else if (edt_Fee.getText().toString().trim().equalsIgnoreCase("$")) {
                    Toast.makeText(getContext(), "Please Enter Permit Fee", Toast.LENGTH_LONG).show();
                } else if (edt_Requirement.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getContext(), "Please Enter Requirement", Toast.LENGTH_LONG).show();
                } else {
                    if (pdfPath.equalsIgnoreCase("")) {
                        new postParking_permits().execute();
                    } else {
                        rl_progressbar.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.VISIBLE);
                        fileUpload();
                    }
                }
            }
        });

        sp_Valid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                select_valid = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        edt_Requirement.setText(array_list.get(0).getRequirements());
        edt_PermitName.setText(array_list.get(0).getPermit_name());
        edt_Fee.setText(array_list.get(0).getCost());
        for (int i = 0; i < arrayList_valid.size(); i++) {
            if (array_list.get(0).getExpires_by().trim().equals(arrayList_valid.get(i).toString())) {
                sp_Valid.setSelection(i);
                select_valid = array_list.get(0).getExpires_by();
                break;
            }
        }

        if (!array_list.get(0).getAppl_req_download().equalsIgnoreCase("null")) {
            txt_File.setText("" + array_list.get(0).getAppl_req_download());
            final_file_uploadPdf = array_list.get(0).getAppl_req_download();
        }

        txt_File.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPdfFile(txt_File.getText().toString().trim());
            }
        });

        if (array_list.get(0).getRenewable().equalsIgnoreCase("Yes")) {
            rb_Yes.setChecked(true);
        } else {
            rb_No.setChecked(true);
        }

        if (array_list.get(0).getPermit_type().equalsIgnoreCase("RESIDENT")) {
            rb_RESIDENT.setChecked(true);
        } else {
            rb_NON_RESIDENT.setChecked(true);
        }

    }

    public void getPdfFile(String pdf_download) {
        rl_progressbar.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        try {
            final File file_exists = new File(file_download_path, pdf_download);
            if (file_exists.exists()) {
                rl_progressbar.setVisibility(View.GONE);
                MimeTypeMap myMime = MimeTypeMap.getSingleton();
                Intent newIntent = new Intent(Intent.ACTION_VIEW);
                String mimeType = myMime.getMimeTypeFromExtension(fileExt(String.valueOf(((file_exists.getAbsolutePath())))).substring(1));
                newIntent.setDataAndType(Uri.fromFile((new File(file_exists.getAbsolutePath()))), mimeType);
                newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                try {
                    getActivity().startActivity(newIntent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getActivity(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
                }
            } else {
                TransferObserver downloadObserver = transferUtility.download("parkezly-images", "/images/" + pdf_download, file_exists);
                downloadObserver.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (TransferState.COMPLETED == state) {
                            rl_progressbar.setVisibility(View.GONE);
                            MimeTypeMap myMime = MimeTypeMap.getSingleton();
                            Intent newIntent = new Intent(Intent.ACTION_VIEW);
                            String mimeType = myMime.getMimeTypeFromExtension(fileExt(String.valueOf(((file_exists.getAbsolutePath())))).substring(1));
                            newIntent.setDataAndType(Uri.fromFile((new File(file_exists.getAbsolutePath()))), mimeType);
                            newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            try {
                                getActivity().startActivity(newIntent);
                            } catch (ActivityNotFoundException e) {
                                Toast.makeText(getActivity(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                        int percentDone = (int) percentDonef;
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        ex.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();
        }
    }

    String pdfPath = "";
    Uri pfdURI = null;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 8) {

                pdfPath = data.getExtras().getString("path");
                File file = new File(pdfPath);

                long fileSizeInBytes = file.length();
                long fileSizeInKB = fileSizeInBytes / 1024;
                long fileSizeInMB = fileSizeInKB / 1024;

                if (fileSizeInKB > 2048) {
                    pfdURI = null;
                    Toast.makeText(getContext(), "Can not upload large scale document", Toast.LENGTH_LONG).show();
                } else {
                    pfdURI = Uri.fromFile(new File(pdfPath));
                    txt_File.setText("Form to be Filled: " + new File(pdfPath).getName());
                }
            }
        }
    }

    public void fileUpload() {
        File file = new File(pdfPath);
        String namegsxsax = file.getAbsoluteFile().getName();
        file_uploadPdf.append(namegsxsax + ",");
        namegsxsax = "/images/" + namegsxsax;
        final_file_uploadPdf = file_uploadPdf.substring(0, file_uploadPdf.length() - 1);
        TransferObserver transferObserver = transferUtility.upload("parkezly-images", namegsxsax, file);
        transferObserverListener(transferObserver);
    }

    public void transferObserverListener(final TransferObserver transferObserver) {
        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.name().equals("COMPLETED")) {
                    rl_progressbar.setVisibility(View.GONE);
                    new postParking_permits().execute();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                try {
                    int percentage = (int) (bytesCurrent / bytesTotal * 100);
                    Log.e("percentage ", " : " + percentage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", "error");
            }
        });
    }


    public class postParking_permits extends AsyncTask<String, String, String> {
        JSONObject json;
        String responseStr = null;
        String managedlosturl = "_table/parking_permits";
        Date current;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            String currentdate = sdf.format(new Date());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            try {
                current = format.parse(currentdate);
                Log.e("current_date", String.valueOf(current));
                System.out.println(current);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("id", array_list.get(i).getId());
            jsonValues1.put("date_time", current);
            Log.e("Manager Type ", " : " + array_list.get(i).getManager_type());
            if (array_list.get(i).getManager_type().equalsIgnoreCase("") ||
                    array_list.get(i).getManager_type().equalsIgnoreCase("null")) {
                jsonValues1.put("manager_type", "TWNP");
            } else {
                jsonValues1.put("manager_type", array_list.get(i).getManager_type());
            }


            if (array_list.get(i).getManager_type_id().equalsIgnoreCase("") ||
                    array_list.get(i).getManager_type_id().equalsIgnoreCase("null")) {
                if (array_list.get(i).getManager_type().equalsIgnoreCase("TOWNSHIP")) {
                    jsonValues1.put("manager_type_id", "1");
                } else if (array_list.get(i).getManager_type().equalsIgnoreCase("COMMERCIAL")) {
                    jsonValues1.put("manager_type_id", "2");
                } else {
                    jsonValues1.put("manager_type_id", "3");
                }
            } else {
                jsonValues1.put("manager_type_id", array_list.get(i).getManager_type_id());
            }


            jsonValues1.put("township_code", array_list.get(i).getTownship_code());
            jsonValues1.put("township_name", array_list.get(i).getTownship_name());
            jsonValues1.put("permit_type", rb_PermitType.getText());
            jsonValues1.put("permit_name", edt_PermitName.getText().toString().trim());
            jsonValues1.put("covered_locations", array_list.get(i).getCovered_locations());
            jsonValues1.put("requirements", edt_Requirement.getText().toString().trim());
            jsonValues1.put("appl_req_download", final_file_uploadPdf);
            jsonValues1.put("cost", edt_Fee.getText().toString().trim());
            jsonValues1.put("year", "2013-07-25 09:44:00");
            jsonValues1.put("location_address", array_list.get(i).getLocation_address());
            jsonValues1.put("active", "YES");
            jsonValues1.put("scheme_type", array_list.get(i).getScheme_type());
            jsonValues1.put("permit_prefix", array_list.get(i).getPermit_prefix());
            jsonValues1.put("permit_nextnum", Integer.parseInt(array_list.get(i).getPermit_nextnum()));
            jsonValues1.put("expires_by", select_valid);
            jsonValues1.put("renewable", rb_Renewable.getText());
            jsonValues1.put("twp_id", array_list.get(i).getTwp_id());

            JSONObject json1 = new JSONObject(jsonValues1);
            Log.e("json1", " : " + json1.toString());
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("responseStr ", " : " + responseStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return responseStr;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && responseStr != null) {
                boolean errors = responseStr.indexOf("error") > 0;
                if (errors) {
                    Toast.makeText(getContext(), "Already exists...", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getContext(), "Successfully added", Toast.LENGTH_LONG).show();
                    fragmentStack.clear();
                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Fragment_permit_definitions pay = new Fragment_permit_definitions();
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                }
            }
        }
    }

    public void currentlocation1() {
        List<Address> addresses;
        GPSTracker tracker = new GPSTracker(getActivity());
        try {
            if (tracker.canGetLocation()) {
                double latitude = tracker.getLatitude();
                double longitude = tracker.getLongitude();
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());

                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                get_Address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
