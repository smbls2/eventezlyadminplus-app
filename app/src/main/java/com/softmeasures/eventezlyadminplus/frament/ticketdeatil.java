package com.softmeasures.eventezlyadminplus.frament;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.melnykov.fab.FloatingActionButton;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.Main_Activity;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class ticketdeatil extends Fragment {

    //////////////////paypal///////////////////
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    private static final String TAG = "paymentQlighting";
    private static final String CONFIG_CLIENT_ID = "Aft0eWMQqeJfDnHj6pmSY_8Ta5kARfjF-BEfAFFzIenRq28s6HDlMCENCyUnmp9EvRBuI3P90XG4heGC";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static PayPalConfiguration config = new PayPalConfiguration().environment(CONFIG_ENVIRONMENT).clientId(CONFIG_CLIENT_ID);
    ProgressDialog pdialog;
    TextView txttitle, txtticketdeatilplateno, txtticketdeatildateticket, txtticketdeatilrespdate, txtticketdeatilviolationfess, txtticketdeatilhearingdate, txt_id;
    TextView txtticketdeatilhearingloction, txtticketdeatilhearingtime, txtticketdeatilemail, txtticketdeatilofficename, txtticketdeatilofficeid, txtticketdeatiladdess, txtticketdeatilpaid;
    TextView txtticketdeatilviolationdeatil, txtticketdeatilviolationdscription, txtticketdeatiladdress1, txtticketdeatilpaid1, txtticketdeatilviolationdeatil1, txtticketdeatilviolationdscription1, txt_print;
    TextView txt_pay_wallet, txt_pay_card, txt_Plead_Not_Guilty, txt_Plead_Not;
    String plate_no, violation_detail, hearing_date = "", hearing_location, email, string, id, date, respdate;
    String officer_name, officer_id, violation_description, hearingtime;
    String paid_date, address, newbal = "0", fee = "null", finallab = "", payb = "", status = "null", townshipcode, state = "", parked_address, selected_address, finalDate, v_user_id, v_platform;
    Double finalpay = 0.0, payball = 0.0, tr_percentage1 = 0.0, tr_fee1 = 0.0;
    int postion;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar;
    RelativeLayout showsucessmesg;
    String timeStamp;
    ConnectionDetector cd;
    RelativeLayout rl_main;

    //show proof
    RelativeLayout rl_show_proof, rl_img1, rl_img2, rl_img3, rl_show_recoding;
    TextView txt_comment, txt_show_proof;
    ImageView img1, img2, img3, img_recoding;
    String photo_name, audio_name = "null", comment, township_logo_url = "";
    ArrayList<String> myList_photo = new ArrayList<>();
    ArrayList<item> township_array_list = new ArrayList<>();
    ArrayList<item> my_photot;
    int totla_size_of_body = 0, current_file_upload_position = 0;
    RelativeLayout rl_pr1, rl_pr2, rl_pr3;
    long timeWhenPaused = 0;
    MediaPlayer mMediaPlayer;
    private AmazonS3Client s3;
    private ArrayList<HashMap<String, Object>> transferRecordMaps;
    private TransferUtility transferUtility;
    private FloatingActionButton btn_play = null;
    private Chronometer mChronometer = null;
    private Handler mHandler = new Handler();

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (mMediaPlayer != null) {
                int mCurrentPosition = mMediaPlayer.getCurrentPosition();
                long minutes = TimeUnit.MILLISECONDS.toMinutes(mCurrentPosition);
                long seconds = TimeUnit.MILLISECONDS.toSeconds(mCurrentPosition)
                        - TimeUnit.MINUTES.toSeconds(minutes);
                mChronometer.setText(String.format("%02d:%02d", minutes, seconds));
                updateSeekBar();
            }
        }
    };

    ImageView img_main_logo, img_qrcode, img_top_logo;
    TextView txt_print_title;
    String transection_id, wallet_id = "", ip;
    boolean is_wallet_payment = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View view = inflater.inflate(R.layout.fragment_myticketsdetail, container, false);

        if ((ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {

            if ((ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)) && (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE))) {
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }
        SharedPreferences ticket = getActivity().getSharedPreferences("ticket", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = ticket.edit();
        ed.putString("ticket", "ticket");
        ed.commit();
        Typeface light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Light.otf");
        txttitle = (TextView) view.findViewById(R.id.txttitte);
        img_top_logo = (ImageView) view.findViewById(R.id.sd);
        img_main_logo = (ImageView) view.findViewById(R.id.img_logo);
        img_qrcode = (ImageView) view.findViewById(R.id.img_qrcode);
        txt_print_title = (TextView) view.findViewById(R.id.txt_title_of_print);
        txt_Plead_Not_Guilty = (TextView) view.findViewById(R.id.txt_plead_not_guilty);
        txt_Plead_Not = (TextView) view.findViewById(R.id.txt_plead_not);
        txtticketdeatiladdess = (TextView) view.findViewById(R.id.txtticketdeatiladdress);
        txtticketdeatiladdess.setTypeface(light);
        txtticketdeatilplateno = (TextView) view.findViewById(R.id.txtticketdeatilplateno);
        txtticketdeatilplateno.setTypeface(light);
        txtticketdeatildateticket = (TextView) view.findViewById(R.id.txtticketdeatildateticket);
        txtticketdeatilplateno.setTypeface(light);
        txtticketdeatilrespdate = (TextView) view.findViewById(R.id.txtticketdeatilrespdate);
        txtticketdeatilrespdate.setTypeface(light);
        txtticketdeatilviolationfess = (TextView) view.findViewById(R.id.txtticketdeatilfee);
        txtticketdeatilviolationfess.setTypeface(light);
        txtticketdeatilhearingdate = (TextView) view.findViewById(R.id.txtticketdeatilhearingdate);
        txtticketdeatilhearingdate.setTypeface(light);
        txtticketdeatilhearingloction = (TextView) view.findViewById(R.id.txtticketdeatilhearinglocation);
        txtticketdeatilhearingloction.setTypeface(light);
        txtticketdeatilhearingtime = (TextView) view.findViewById(R.id.txtticketdeatilhearingtime);
        txtticketdeatilhearingtime.setTypeface(light);
        txtticketdeatilemail = (TextView) view.findViewById(R.id.txtticketdeatilemail);
        txtticketdeatilemail.setTypeface(light);
        txtticketdeatilofficename = (TextView) view.findViewById(R.id.txtticketdeatilofficesname);
        txtticketdeatilofficename.setTypeface(light);
        txtticketdeatilofficeid = (TextView) view.findViewById(R.id.txtticketdeatilofficesid);
        txtticketdeatilofficeid.setTypeface(light);
        txtticketdeatilpaid = (TextView) view.findViewById(R.id.txtticketdeatilpaiddate);
        txtticketdeatilpaid.setTypeface(light);
        txtticketdeatilviolationdeatil = (TextView) view.findViewById(R.id.txtticketdeatilviolationdetails);
        txtticketdeatilviolationdeatil.setTypeface(light);
        txtticketdeatilviolationdscription = (TextView) view.findViewById(R.id.txtticketdeatilviolationdescription);
        txtticketdeatilviolationdscription.setTypeface(light);
        txtticketdeatiladdress1 = (TextView) view.findViewById(R.id.txtticketdeatilvadress1);
        txtticketdeatiladdress1.setTypeface(light);
        txtticketdeatilpaid1 = (TextView) view.findViewById(R.id.txtticketdeatilpaiddate1);
        txtticketdeatilpaid1.setTypeface(light);
        txtticketdeatilviolationdeatil1 = (TextView) view.findViewById(R.id.txtticketdeatilviolationdetails1);
        txtticketdeatilviolationdeatil1.setTypeface(light);
        txtticketdeatilviolationdscription1 = (TextView) view.findViewById(R.id.txtticketdeatilviolationdescription1);
        txtticketdeatilviolationdscription1.setTypeface(light);
        txt_id = (TextView) view.findViewById(R.id.txt_id);
        txt_print = (TextView) view.findViewById(R.id.txt_print);

        rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);

        txt_pay_card = (TextView) view.findViewById(R.id.txt_pay_card);
        txt_pay_wallet = (TextView) view.findViewById(R.id.txt_pay_wallet);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        showsucessmesg = (RelativeLayout) view.findViewById(R.id.sucessfully);

        //show proof

        txt_show_proof = (TextView) view.findViewById(R.id.txt_showproof);
        txt_comment = (TextView) view.findViewById(R.id.edit_comment);

        rl_img1 = (RelativeLayout) view.findViewById(R.id.rl_img_1);
        rl_img2 = (RelativeLayout) view.findViewById(R.id.rl_img_2);
        rl_img3 = (RelativeLayout) view.findViewById(R.id.rl_img_3);

        img1 = (ImageView) view.findViewById(R.id.img_pic1);
        img2 = (ImageView) view.findViewById(R.id.img_pic2);
        img3 = (ImageView) view.findViewById(R.id.img_pic3);

        rl_pr1 = (RelativeLayout) view.findViewById(R.id.rl_progressbar1);
        rl_pr2 = (RelativeLayout) view.findViewById(R.id.rl_progressbar2);
        rl_pr3 = (RelativeLayout) view.findViewById(R.id.rl_progressbar3);


        rl_show_proof = (RelativeLayout) view.findViewById(R.id.rl_proof_layout1);
        rl_show_recoding = (RelativeLayout) view.findViewById(R.id.rl_rl_refresh);

        mChronometer = (Chronometer) view.findViewById(R.id.chronometer);
        btn_play = (FloatingActionButton) view.findViewById(R.id.btnplay);
        SharedPreferences pr = getActivity().getSharedPreferences("postion", Context.MODE_APPEND);
        postion = pr.getInt("postion", 0);

        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Thin.ttf");
        txttitle.setTypeface(type);
        SharedPreferences prefs = getActivity().getSharedPreferences("get", Context.MODE_PRIVATE);
        String oldlist = prefs.getString("MyObject", "");
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        rl_img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File file = my_photot.get(0).getFile_body();
                showimage(getActivity(), file);
            }
        });

        rl_img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File file = my_photot.get(1).getFile_body();
                showimage(getActivity(), file);
            }
        });
        cd = new ConnectionDetector(getActivity());
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_roe = logindeatl.getString("role", "");
        new gettownship_list().execute();
        txt_pay_wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (cd.isConnectingToInternet()) {
                    String fd = fee;
                    String ba = fee.substring(fee.indexOf('$') + 1);
                    payball = Double.parseDouble(ba);
                    is_wallet_payment = true;
                    new getwalletbal().execute();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Internet Connection Required");
                    alertDialog.setMessage("Please connect to working Internet connection");
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
        });

        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        txt_pay_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    is_wallet_payment = false;
                    String fd = fee;
                    String ba = fee.substring(fee.indexOf('$') + 1);
                    payball = Double.parseDouble(ba);
                    payball = payball + (payball * tr_percentage1) + tr_fee1;
                    PaypalPaymentIntegration();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Internet Connection Required");
                    alertDialog.setMessage("Please connect to working Internet connection");
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                }
            }
        });


        txt_show_proof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                my_photot = new ArrayList<item>();
                SharedPreferences ff = getActivity().getSharedPreferences("show_proof_layout", Context.MODE_PRIVATE);
                SharedPreferences.Editor ed = ff.edit();
                ed.putString("back_ticketdeatil", "yes");
                ed.commit();
                rl_show_proof.setVisibility(View.VISIBLE);
                initData();
                if (!comment.equals("null")) {
                    txt_comment.setText(comment);
                }
                if (photo_name.equals("null") && audio_name.equals("null") && photo_name.equals("null")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("ParkEZly");
                    alertDialog.setMessage("Sorry No Proof added");
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                    rl_show_proof.setVisibility(View.GONE);
                    ff.edit().clear().commit();
                } else {
                    Animation animation = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.sidepannelright);
                    rl_show_proof.startAnimation(animation);
                    if (photo_name.equals("null") && audio_name.equals("null")) {
                        rl_pr1.setVisibility(View.GONE);
                    } else {
                        if (photo_name.equals("null")) {
                            rl_pr1.setVisibility(View.GONE);
                        }
                        totla_size_of_body = myList_photo.size();
                        if (!audio_name.equals("null")) {
                            totla_size_of_body = totla_size_of_body + 1;
                        }

                        for (int i = 0; i < myList_photo.size(); i++) {
                            String kry = myList_photo.get(i).toString();
                            kry = kry.replace(" ", "");
                            String finalkey = "/images/" + kry;
                            File file = new File(Environment.getExternalStorageDirectory().toString() + "/" + finalkey);
                            TransferObserver observer = transferUtility.download("parkezly-images", finalkey, file);
                            transferObserverListener(observer);
                            item ii = new item();
                            ii.setFile_body(file);
                            ii.setName("img");
                            my_photot.add(ii);
                        }

                        if (!audio_name.equals("null")) {
                            String kry = "/audio-rec/" + audio_name;
                            File file = new File(Environment.getExternalStorageDirectory().toString() + "/" + kry);
                            TransferObserver observer = transferUtility.download("parkezly-images", kry, file);
                            transferObserverListener(observer);
                            item ii = new item();
                            ii.setFile_body(file);
                            ii.setName("audio");
                            my_photot.add(ii);

                        }
                    }
                }

            }
        });

        rl_show_proof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        if (oldlist != "") {

            try {
                JSONArray jsonArray1 = new JSONArray(oldlist);
                JSONObject c = jsonArray1.getJSONObject(postion);
                fee = c.getString("fees");
                respdate = c.getString("respdate");
                date = c.getString("exdate");
                plate_no = c.getString("plate_no");
                violation_detail = c.getString("violation_detail");
                hearing_date = c.getString("hearing_date");
                hearing_location = c.getString("hearing_location");
                address = c.getString("address");
                email = c.getString("email");
                officer_name = c.getString("officer_name");
                officer_id = c.getString("officer_id");
                violation_description = c.getString("violationdescription");
                hearingtime = c.getString("hearingtime");
                paid_date = c.getString("paid_date");
                status = c.getString("ticket_satus");
                id = c.getString("id");
                townshipcode = c.getString("townshipcode");
                String quild = c.getString("Plead_Not_Guilty");
                selected_address = c.getString("address");
                parked_address = c.getString("addesss1");
                photo_name = c.getString("photo_comments");
                audio_name = c.getString("av_comments");
                comment = c.getString("text_comments");
                state = c.getString("state");
                v_user_id = c.getString("v_user_id");
                v_platform = c.getString("v_platform");
                audio_name = audio_name != null ? (!audio_name.equals("") ? (!audio_name.equals("null") ? audio_name : "null") : "null") : "null";
                photo_name = photo_name != null ? (!photo_name.equals("") ? (!photo_name.equals("null") ? photo_name : "null") : "null") : "null";
                comment = comment != null ? (!comment.equals("") ? (!comment.equals("null") ? comment : "null") : "null") : "null";
                if (!photo_name.equals("null")) {
                    if (photo_name.contains(",")) {
                        myList_photo = new ArrayList<String>(Arrays.asList(photo_name.split(",")));
                    } else {
                        myList_photo = new ArrayList<>();
                        myList_photo.add(photo_name);
                    }
                }
                if (quild.equals("0")) {
                    txt_Plead_Not.setVisibility(View.GONE);
                    txt_Plead_Not_Guilty.setVisibility(View.VISIBLE);
                } else {
                    txt_Plead_Not.setVisibility(View.VISIBLE);
                    txt_Plead_Not.setText("Plead Not Guilty: YES");
                    txt_Plead_Not_Guilty.setVisibility(View.GONE);
                }
                address = address != null ? (!address.equals("") ? (!address.equals("null") ? address : "") : "") : "";
                parked_address = parked_address != null ? (!parked_address.equals("") ? (!parked_address.equals("null") ? parked_address : "") : "") : "";
                plate_no = plate_no != null ? (!plate_no.equals("") ? (!plate_no.equals("null") ? plate_no : "") : "") : "";
                fee = fee != null ? (!fee.equals("") ? (!fee.equals("null") ? fee : "") : "") : "";
                hearing_location = hearing_location != null ? (!hearing_location.equals("") ? (!hearing_location.equals("null") ? hearing_location : "") : "") : "";
                hearingtime = hearingtime != null ? (!hearingtime.equals("") ? (!hearingtime.equals("null") ? hearingtime : "") : "") : "";
                email = email != null ? (!email.equals("") ? (!email.equals("null") ? email : "") : "") : "";
                officer_name = officer_name != null ? (!officer_name.equals("") ? (!officer_name.equals("null") ? officer_name : "") : "") : "";
                officer_id = officer_id != null ? (!officer_id.equals("") ? (!officer_id.equals("null") ? officer_id : "") : "") : "";
                paid_date = paid_date != null ? (!paid_date.equals("") ? (!paid_date.equals("null") ? paid_date : "") : "") : "";
                violation_detail = violation_detail != null ? (!violation_detail.equals("") ? (!violation_detail.equals("null") ? violation_detail : "") : "") : "";
                violation_description = violation_description != null ? (!violation_description.equals("") ? (!violation_description.equals("null") ? violation_description : "") : "") : "";
                address = address != null ? (!address.equals("") ? (!address.equals("null") ? address : "") : "") : "";
                txt_id.setText(id);
                if (user_roe.equals("TwpInspector") || user_roe.equals("SuperAdmin") || user_roe.equals("TwpAdmin") || user_roe.equals("TwpBursar")) {
                    txt_pay_card.setVisibility(View.GONE);
                    txt_pay_wallet.setVisibility(View.GONE);
                    txt_Plead_Not_Guilty.setVisibility(View.GONE);
                } else {
                    if (status.equals("OPEN")) {
                        txt_pay_card.setVisibility(View.VISIBLE);
                        txt_pay_wallet.setVisibility(View.VISIBLE);
                    } else {
                        txt_pay_card.setVisibility(View.GONE);
                        txt_pay_wallet.setVisibility(View.GONE);
                    }
                }
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "yyyy-MM-dd HH:mm:ss", Locale.US);
                Date myDate = null;
                try {
                    myDate = dateFormat.parse(hearing_date);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat timeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
                finalDate = timeFormat.format(myDate);
                System.out.println(finalDate);
                txtticketdeatilrespdate.setText("Violation Ticket" + "\n\nTicket Date: " + date + "\n\nTicket Number : " + id + "\n\nViolation Fee: " + fee + "\n\nViolation Description: " + violation_description + "\n\nViolation Details: " + violation_detail + "\n\nRespond By Date: " + respdate + "\n\nHearing Location: " + hearing_location + "\n\nHearing Date: " + finalDate + "\n\nPlate # :" + plate_no + "\n\nState: " + state + "\n\nParked Address: " + parked_address + "\n\nSelected Address: " + selected_address + "\n\nLocation Name: ");
            } catch (Exception e) {
                e.printStackTrace();
            }
            txt_print.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPrintOptions(getActivity());
                }
            });

            txt_Plead_Not_Guilty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new updateguild().execute();
                }
            });
        }

        try {
            new servicecharge(townshipcode).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (photo_name.equals("null") && audio_name.equals("null") && photo_name.equals("null")) {
            rl_show_proof.setVisibility(View.GONE);
        }
        return view;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1) {
            if (permissions.length == 1 && permissions[0] == Manifest.permission.WRITE_EXTERNAL_STORAGE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("pe", "yes");
            } else {
                Log.e("pe", "no");

            }
        } else {
            Log.e("pe1", "no1");
        }
    }

    public void manipulatePdf1(String src) throws IOException, DocumentException {
        try {
            PdfReader reader = new PdfReader(src);
            int n = reader.getNumberOfPages();
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ParkEZly";
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();

            File file = new File(dir, "view_ticket" + plate_no + timeStamp + ".pdf");
            FileOutputStream fOut = new FileOutputStream(file);
            PdfStamper stamper = new PdfStamper(reader, fOut);
            stamper.setRotateContents(false);


            File file1 = new File(path, "logo.PNG");
            FileOutputStream outStream = null;
            try {
                ImageView imgq = new ImageView(getActivity());
                imgq.setImageResource(R.drawable.rsz_1rsz_splashscreen);

                Bitmap bitmap = ((BitmapDrawable) imgq.getDrawable()).getBitmap();

                outStream = new FileOutputStream(file1);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
                outStream.flush();
                outStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String getpath = Environment.getExternalStorageDirectory() + "/" + "ParkEZly" + "/" + "logo.PNG";
            Image img = Image.getInstance(getpath);
            float w = img.getScaledWidth();
            float h = img.getScaledHeight();
            PdfGState gs1 = new PdfGState();
            gs1.setFillOpacity(0.2f);
            PdfContentByte over;
            Rectangle pagesize;
            float x, y;

            for (int i = 1; i <= n; i++) {
                pagesize = reader.getPageSize(i);

                x = (pagesize.getLeft() + pagesize.getRight()) / 2;
                y = (pagesize.getTop() + pagesize.getBottom()) / 2;
                over = stamper.getOverContent(i);
                over.saveState();
                over.setGState(gs1);
                //over.addImage(img,true);
                float gg = x - (w / 2);
                float hh = y - (h / 2);
                Log.e("dcd", String.valueOf(gg));

                Log.e("dcd", String.valueOf(hh));

                over.addImage(img, w, 0, 0, h, gg, hh);
                //  over.addImage(img, 200,0,200,0,0,0);
                over.restoreState();
            }
            stamper.close();
            reader.close();
            Intent i = new Intent(getActivity(), Main_Activity.class);
            i.putExtra("file_path", src);
            i.putExtra("date", "Violation Fee: " + fee + "\n\nViolation Description: " + violation_description + "\n\nViolation Details: " + violation_detail + "\n\nRespond By Date: " + respdate + "\n\nHearing Location: " + hearing_location + "\n\nHearing Date: " + finalDate + "\n\nPlate # :" + plate_no + "\n\nState: " + state + "\n\nParked Address: " + parked_address + "\n\nSelected Address: " + selected_address + "\n\nLocation Name: " + "\n\nTicket Date: " + date + "\n\nTicket# : " + id);
            getActivity().startActivity(i);
        } catch (IOException os) {
            Log.e("vb", String.valueOf(os));
        }
    }

    public void manipulatePdf(String src) throws IOException, DocumentException {

        try {

            PdfReader reader = new PdfReader(src);
            int n = reader.getNumberOfPages();
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ParkEZly";
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();

            File file = new File(dir, "parking_ticket" + "view_ticket" + plate_no + timeStamp + "1" + ".pdf");
            FileOutputStream fOut = new FileOutputStream(file);
            PdfStamper stamper = new PdfStamper(reader, fOut);
            stamper.setRotateContents(false);
            // text watermark
           /* Font f = new Font(Font.FontFamily.HELVETICA, 30);
            Phrase p = new Phrase("My watermark (text)", f);*/
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File file1 = new File(path, "logo.PNG");
            FileOutputStream outStream = null;
            try {
                ImageView imgq = new ImageView(getActivity());
                imgq.setImageResource(R.drawable.rsz_1rsz_splashscreen);

                Bitmap bitmap = ((BitmapDrawable) imgq.getDrawable()).getBitmap();

                outStream = new FileOutputStream(file1);
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, outStream);
                outStream.flush();
                outStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String getpath = Environment.getExternalStorageDirectory() + "/" + "ParkEZly" + "/" + "logo.PNG";
            // image watermark
            Image img = Image.getInstance(getpath);
            float w = img.getScaledWidth();
            float h = img.getScaledHeight();

            // transparency
            PdfGState gs1 = new PdfGState();
            gs1.setFillOpacity(0.2f);
            // properties
            PdfContentByte over;
            Rectangle pagesize;
            float x, y;

            // loop over every page
            for (int i = 1; i <= n; i++) {
                pagesize = reader.getPageSize(i);

                x = (pagesize.getLeft() + pagesize.getRight()) / 2;
                y = (pagesize.getTop() + pagesize.getBottom()) / 2;
                over = stamper.getOverContent(i);
                over.saveState();
                over.setGState(gs1);
                //over.addImage(img,true);
                float gg = x - (w / 2);
                float hh = y - (h / 2);
                Log.e("dcd", String.valueOf(gg));

                Log.e("dcd", String.valueOf(hh));
                over.addImage(img, w, 0, 0, h, x - (w / 2), y - (h / 2));
                //  over.addImage(img, 200,0,200,0,0,0);
                over.restoreState();
            }
            stamper.close();
            reader.close();

            // viewPdf("parking_ticket" + plate_no +timeStamp+"1" + ".pdf", "parkEZly");
            onther_view(file);
            File file11 = new File(dir, "parking_ticket" + "view_ticket" + plate_no + timeStamp + ".pdf");
            file11.delete();
        } catch (IOException os) {
            Log.e("vb", String.valueOf(os));
        }
    }

    public void createandDisplayPdf() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        timeStamp = dateFormat.format(new Date());
        com.itextpdf.text.Document doc = new com.itextpdf.text.Document();

        try {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ParkEZly";
            File file1 = new File(path, "logo1.PNG");
            FileOutputStream outStream = null;
            try {

                Bitmap bitmap = ((BitmapDrawable) img_top_logo.getDrawable()).getBitmap();
                outStream = new FileOutputStream(file1);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
                outStream.flush();
                outStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();

            File file = new File(dir, "view_ticket" + plate_no + timeStamp + ".pdf");
            FileOutputStream fOut = new FileOutputStream(file);
            Font paraFont = new Font(Font.FontFamily.COURIER, 20);

            PdfWriter.getInstance(doc, fOut);

            //open the document
            doc.open();

            String ad = "Village Of Farmingdale\n\n";
            Paragraph pp = new Paragraph();
            Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 22, Font.BOLD);
            pp.setFont(smallBold);
            pp.add(ad);
            pp.setAlignment(Paragraph.ALIGN_CENTER);
            pp.setPaddingTop(30);
            doc.add(pp);

            String getpath = Environment.getExternalStorageDirectory() + "/" + "ParkEZly" + "/" + "logo1.PNG";
            Image image = Image.getInstance(getpath);
            image.setAlignment(Image.MIDDLE | Image.TEXTWRAP);
            image.setWidthPercentage(50);
            Log.v("Stage 7", "Image Alignments");


            doc.add(image);

            String ad1 = "Violation Ticket\n";
            Paragraph pp1 = new Paragraph();
            Font smallBold1 = new Font(Font.FontFamily.TIMES_ROMAN, 22, Font.BOLD);
            pp1.setFont(smallBold1);
            pp1.add(ad1);
            pp1.setAlignment(Paragraph.ALIGN_LEFT);
            pp1.setPaddingTop(30);
            doc.add(pp1);

            Paragraph p1 = new Paragraph("Ticket Date: " + date + "\nTicket Number :" + id + "\nViolation Fee: " + fee + "\nViolation Description: " + violation_description + "\nViolation Details: " + violation_detail + "\nRespond By Date: " + respdate + "\nHearing Location: " + hearing_location + "\nHearing Date: " + finalDate + "\nPlate #: " + plate_no + "\nState: " + state + "\nParked Address: " + parked_address + "\nSelected Address: " + selected_address + "\nLocation Name: ");
            p1.setFont(paraFont);
            p1.setAlignment(Paragraph.ALIGN_LEFT);
            p1.setPaddingTop(10);
            doc.add(p1);

            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(50);

            Bitmap bitmap1 = ((BitmapDrawable) img_main_logo.getDrawable()).getBitmap();
            bitmap1 = getResizedBitmap(bitmap1, 80, 80);
            table.addCell(createImageCell(bitmap1));

            Bitmap bitmap11 = ((BitmapDrawable) img_qrcode.getDrawable()).getBitmap();
            bitmap11 = getResizedBitmap(bitmap11, 80, 80);
            table.addCell(createImageCell(bitmap11));
            doc.add(table);

        } catch (DocumentException de) {
            Log.e("PDFCreator", "DocumentException:" + de);
        } catch (IOException e) {
            Log.e("PDFCreator", "ioException:" + e);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            doc.close();
        }
        String getpath = Environment.getExternalStorageDirectory() + "/" + "parkEZly" + "/" + "view_ticket" + plate_no + timeStamp + ".pdf";
        //viewPdf("view_ticket" + plate_no + timeStamp + ".pdf", "parkEZly");
        try {

            manipulatePdf(getpath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    public PdfPCell createImageCell(Bitmap path) throws DocumentException, IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        path.compress(Bitmap.CompressFormat.PNG, 100, stream);
        Image image = Image.getInstance(stream.toByteArray());
        PdfPCell cell = new PdfPCell(image, true);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        // bm.recycle();
        return resizedBitmap;
    }

    public void createandDisplayPdf1() {


        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        timeStamp = dateFormat.format(new Date());
        Document doc = new Document();

        try {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ParkEZly";
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();
            File file1 = new File(path, "logo1.PNG");
            FileOutputStream outStream = null;
            try {
                ImageView imgq = new ImageView(getActivity());
                imgq.setImageResource(R.mipmap.rsz_1vof_logo);
                Bitmap bitmap = ((BitmapDrawable) imgq.getDrawable()).getBitmap();
                outStream = new FileOutputStream(file1);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
                outStream.flush();
                outStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            File file = new File(dir, "view_ticket" + plate_no + timeStamp + ".pdf");
            FileOutputStream fOut = new FileOutputStream(file);
            Font paraFont = new Font(Font.FontFamily.COURIER, 20);

            PdfWriter.getInstance(doc, fOut);

            //open the document
            doc.open();

            String ad = "Village Of Farmingdale\n\n";
            Paragraph pp = new Paragraph();
            Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 22, Font.BOLD);
            pp.setFont(smallBold);
            pp.add(ad);
            pp.setAlignment(Paragraph.ALIGN_CENTER);
            pp.setPaddingTop(30);
            doc.add(pp);

            String getpath = Environment.getExternalStorageDirectory() + "/" + "ParkEZly" + "/" + "logo1.PNG";
            Image image = Image.getInstance(getpath);
            image.setAlignment(Image.MIDDLE | Image.TEXTWRAP);
            image.setWidthPercentage(50);
            Log.v("Stage 7", "Image Alignments");


            doc.add(image);

            String ad1 = "Violation Ticket\n";
            Paragraph pp1 = new Paragraph();
            Font smallBold1 = new Font(Font.FontFamily.TIMES_ROMAN, 22, Font.BOLD);
            pp1.setFont(smallBold1);
            pp1.add(ad1);
            pp1.setAlignment(Paragraph.ALIGN_LEFT);
            pp1.setPaddingTop(30);
            doc.add(pp1);

            Paragraph p1 = new Paragraph("Violation Fee: " + fee + "\nViolation Description: " + violation_description + "\nViolation Details: " + violation_detail + "\nRespond By Date: " + respdate + "\nHearing Location: " + hearing_location + "\nHearing Date: " + finalDate + "\nPlate #: " + plate_no + "\nState: " + state + "\nParked Address: " + parked_address + "\nSelected Address: " + selected_address + "\nLocation Name: " + "\nTicket Date: " + date + "\nTicket# :" + id);
            p1.setFont(paraFont);
            p1.setAlignment(Paragraph.ALIGN_LEFT);
            p1.setPaddingTop(10);
            doc.add(p1);
            Bitmap map = TextToImageEncode("parkezly");
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            map.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            Image image1 = Image.getInstance(byteArray);
            image1.setAlignment(Image.MIDDLE | Image.TEXTWRAP);
            image1.setWidthPercentage(50);
            doc.add(image1);


        } catch (DocumentException de) {
            Log.e("PDFCreator", "DocumentException:" + de);
        } catch (IOException e) {
            Log.e("PDFCreator", "ioException:" + e);
        } catch (WriterException e) {
            e.printStackTrace();
        } finally {
            doc.close();
        }
        String getpath = Environment.getExternalStorageDirectory() + "/" + "parkEZly" + "/" + "view_ticket" + plate_no + timeStamp + ".pdf";
        try {
            manipulatePdf1(getpath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    Bitmap TextToImageEncode(String Value) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    200, 200, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ?
                        getResources().getColor(R.color.QRCodeBlackColor) : getResources().getColor(R.color.QRCodeWhiteColor);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 200, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }

    // Method for opening a pdf file
    private void viewPdf(String file, String directory) {

        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/" + directory + "/" + file);
        /* Uri path = Uri.fromFile(pdfFile);*/
        Uri path = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", pdfFile);
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            startActivity(pdfIntent);
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Can't read pdf file", Toast.LENGTH_SHORT).show();
        }
    }

    public void onther_view(File file) {
        File pdfFile = file;
        try {
            if (pdfFile.exists()) //Checking for the file is exist or not
            {
                // Uri path1 = Uri.fromFile(pdfFile);
                Uri path1 = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", pdfFile);
                Intent objIntent = new Intent(Intent.ACTION_VIEW);
                objIntent.setDataAndType(path1, "application/pdf");
                //  objIntent.setFlags(Intent. FLAG_ACTIVITY_CLEAR_TOP);
                objIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                // objIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(objIntent);//Staring the pdf viewer
            } else {
                Toast.makeText(getActivity(), "The file not exists! ", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
        }
    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("ip", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("aax", ex.toString());
        }
        return null;
    }

    public void ActionStartsHere() {
        againStartGPSAndSendFile();
    }

    public void againStartGPSAndSendFile() {
        new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {

                showsucessmesg.setVisibility(View.GONE);
                if (getActivity() != null) {
                    TextView tvTitle = new TextView(getActivity());
                    tvTitle.setText("Done ✓");
                    tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                    tvTitle.setTextColor(Color.BLACK);
                    tvTitle.setTextSize(20);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setCustomTitle(tvTitle);
                    alertDialog.setMessage("Ticket Update Successfully");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            backticket();
                        }
                    });
                    alertDialog.show();
                }
            }

        }.start();
    }

    public void backticket() {
        SharedPreferences ticket = getActivity().getSharedPreferences("ticket", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = ticket.edit();
        ed.clear().commit();
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.sidepannelleft, R.anim.sidepannelleft);
        ft.replace(R.id.My_Container_1_ID, new mytickets(), "NewFragmentTag");
        ft.commit();
    }

    private void PaypalPaymentIntegration() {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal(payball), "USD", "Parking Payment", paymentIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.i(TAG, confirm.toJSONObject().toString(4));
                    Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                    JSONObject jsonObj = new JSONObject(confirm.getPayment().toJSONObject().toString(4));
                    Log.e("JSON IS", "LIKE" + jsonObj);
                    Log.e("short_description", "short_description : " + jsonObj.getString("short_description"));
                    Log.e("amount", "amount : " + jsonObj.getString("amount"));
                    Log.e("intent", "intent : " + jsonObj.getString("intent"));
                    Log.e("currency_code", "currency_code : " + jsonObj.getString("currency_code"));

                    JSONObject jsonObjResponse = confirm.toJSONObject()
                            .getJSONObject("response");
                    transection_id = jsonObjResponse.getString("id");

                    if (transection_id != null) {

                        new updatevilotion().execute();
                    } else {
                        transactionfailalert();
                    }
                    Log.e("transactionId", ":;;;;" + transection_id);

                } catch (JSONException e) {
                    Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                }
            }
        }
    }

    public void transactionfailalert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Transaction Fail try again");
        alertDialog.setMessage("Fail");
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alertDialog.show();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void showPrintOptions(Activity context) {

        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View layout = layoutInflater.inflate(R.layout.select_printer_option, viewGroup, false);
        final PopupWindow popup = new PopupWindow(context);
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(false);
        popup.setFocusable(false);
        popup.setAnimationStyle(R.style.animationName);
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

        ImageView img_close = (ImageView) layout.findViewById(R.id.img_close);
        TextView txt_pdf = (TextView) layout.findViewById(R.id.txt_pdf);
        TextView txt_bluetooth = (TextView) layout.findViewById(R.id.txt_bluetooth);


        txt_bluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                Intent i = new Intent(getActivity(), Main_Activity.class);
                i.putExtra("title", txt_print_title.getText().toString());
                i.putExtra("township_logo", township_logo_url);
                i.putExtra("date", "Ticket Date: " + date + "\n\nTicket Number :" + id + "\n\nViolation Fee: " + fee + "\n\nViolation Description: " + violation_description + "\n\nViolation Details: " + violation_detail + "\n\nRespond By Date: " + respdate + "\n\nHearing Location: " + hearing_location + "\n\nHearing Date: " + finalDate + "\n\nPlate #: " + plate_no + "\n\nState: " + state + "\n\nParked Address: " + parked_address + "\n\nSelected Address: " + selected_address + "\n\nLocation Name: ");
                getActivity().startActivity(i);
                //   createandDisplayPdf1();
            }
        });

        txt_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                createandDisplayPdf();
            }
        });

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

    }

    private void initData() {

        s3 = new AmazonS3Client(new BasicAWSCredentials("AKIAJNG22KFRVUAICSEA", "NSrQR/yAg52RPD3kp3FMb3NO+Vrxuty4iAwPU4th"));
        transferUtility = new TransferUtility(s3, getActivity().getApplicationContext());
        transferRecordMaps = new ArrayList<HashMap<String, Object>>();
    }

    public void transferObserverListener(TransferObserver transferObserver) {

        transferObserver.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
                Log.e("statechange", state + "");
                if (state.name().equals("COMPLETED")) {
                    current_file_upload_position++;

                    if (totla_size_of_body == current_file_upload_position) {
                        current_file_upload_position = 0;
                        Log.e("data", "yes");
                        for (int i = 0; i < my_photot.size(); i++) {
                            if (my_photot.get(i).getName().equals("img")) {
                                showdata(my_photot.get(i).getFile_body(), i + 1);
                            } else if (my_photot.get(i).getName().equals("audio")) {
                                show_audio(my_photot.get(i).getFile_body());
                            }
                        }

                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                try {
                    int percentage = (int) (bytesCurrent / bytesTotal * 100);
                    Log.e("percentage", percentage + "");
                } catch (Exception e) {

                }

            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", "error");
            }

        });
    }

    public void showdata(File file, int i) {
        if (i == 1) {
            rl_img1.setVisibility(View.VISIBLE);
            Picasso.with(getActivity()).load(file).into(img1, new Callback() {
                @Override
                public void onSuccess() {
                    rl_pr1.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    rl_pr1.setVisibility(View.GONE);
                    Log.e("error", "refre");
                }

            });
        } else if (i == 2) {
            rl_img2.setVisibility(View.VISIBLE);
            Picasso.with(getActivity()).load(file).into(img2, new Callback() {
                @Override
                public void onSuccess() {
                    rl_pr2.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    rl_pr2.setVisibility(View.GONE);
                    Log.e("Error", "refre");
                }
            });
        } else if (i == 3) {
            rl_img3.setVisibility(View.VISIBLE);
            Picasso.with(getActivity()).load(file).into(img3, new Callback() {
                @Override
                public void onSuccess() {
                    rl_pr3.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    rl_pr2.setVisibility(View.GONE);
                    Log.e("error", "refre");
                }
            });
        }


    }

    public void show_audio(final File file) {

        rl_show_recoding.setVisibility(View.VISIBLE);
        rl_pr1.setVisibility(View.GONE);
        rl_pr2.setVisibility(View.GONE);
        rl_pr3.setVisibility(View.GONE);
        btn_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMediaPlayer = new MediaPlayer();
                if (mMediaPlayer.isPlaying()) {
                    btn_play.setImageResource(R.mipmap.ic_media_play);
                    mHandler.removeCallbacks(mRunnable);
                    mMediaPlayer.stop();
                    mMediaPlayer.reset();
                    mMediaPlayer.release();
                    mMediaPlayer = null;
                } else {
                    btn_play.setImageResource(R.mipmap.ic_media_pause);
                    try {
                        mMediaPlayer.setDataSource(file.getAbsolutePath());
                        mMediaPlayer.prepare();
                        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {

                                Log.e("start", "mp is start");
                                mMediaPlayer.start();
                                updateSeekBar();
                            }
                        });
                    } catch (IOException e) {
                        Log.e("", "prepare() failed");
                    }

                    mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            btn_play.setImageResource(R.mipmap.ic_media_play);
                            mHandler.removeCallbacks(mRunnable);
                            mMediaPlayer.pause();
                        }
                    });

                }
            }
        });
    }

    private void updateSeekBar() {
        mHandler.postDelayed(mRunnable, 1000);
    }

    public class getwalletbal extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONObject json, json1;
        String wallbalurl = "_table/user_wallet?order=date_time%20DESC";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + wallbalurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            HttpEntity resEntity = response.getEntity();
            String responseStr = null;

            try {
                responseStr = EntityUtils.toString(resEntity).trim();

                json = new JSONObject(responseStr);

                JSONArray array = json.getJSONArray("resource");
                for (int i = 0; i < array.length(); i++) {
                    item item = new item();
                    JSONObject c = array.getJSONObject(i);
                    String uid = c.getString("user_id");
                    if (uid.equals(id)) {
                        newbal = c.getString("new_balance");
                        break;
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }

            if (getActivity() != null && json != null) {
                double pay = Double.parseDouble(newbal);
                if (pay > 0) {
                    finalpay = 0.0;
                    payball = payball + (payball * tr_percentage1) + tr_fee1;
                    finalpay = Double.parseDouble(newbal) - payball;
                    finallab = String.format("%.2f", finalpay);
                    payb = (String.format("%.2f", payball));
                    String wallbal = (String.format("%.2f", Double.parseDouble(newbal)));
                    if (finalpay < 0) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setMessage("You don't have enough funds in your wallet.");
                        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alertDialog.show();
                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Wallet Balance: $" + wallbal);
                        alertDialog.setMessage("Would you like to pay $" + payb + " from your ParkEZly wallet?");
                        alertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                new updatewallet().execute();
                            }
                        });
                        alertDialog.setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }
                } else {
                    android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("alert");
                    alertDialog.setMessage("You don't have enough funds in your wallet.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
            super.onPostExecute(s);
        }


    }

    public class updatewallet extends AsyncTask<String, String, String> {
        String idr = "null";
        JSONObject json, json1;
        String transactionurl = "_table/user_wallet";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
            String id = logindeatl.getString("id", "");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            String currentdate = sdf.format(new Date());
            String ip = getLocalIpAddress();
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("paid_date", currentdate);
            jsonValues.put("last_paid_amt", payb);
            jsonValues.put("ip", ip);
            jsonValues.put("ipn_status", "");
            jsonValues.put("current_balance", finallab);
            jsonValues.put("ipn_txn_id", "");
            jsonValues.put("add_amt", "");
            jsonValues.put("ipn_custom", "");
            jsonValues.put("ipn_payment", "");
            jsonValues.put("date_time", currentdate);
            jsonValues.put("ipn_address", "");
            jsonValues.put("user_id", id);
            jsonValues.put("new_balance", finallab);
            jsonValues.put("action", "deduction");
            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);

                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);

                        wallet_id = c.getString("id");

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json1 != null) {
                if (!wallet_id.equals("null")) {
                    new updatevilotion().execute();
                } else {
                }
            }
        }
    }

    public class updatevilotion extends AsyncTask<String, String, String> {
        String res_id = "";
        JSONObject json, json1;
        String pov2 = "_table/parking_violations";

        @Override
        protected void onPreExecute() {


            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", id);
            jsonValues.put("ip", getLocalIpAddress());
            jsonValues.put("tkt_status", "CLOSED");
            jsonValues.put("v_platform", v_platform);
            jsonValues.put("v_user_id", v_user_id);
            if (is_wallet_payment) {
                jsonValues.put("pay_method", "wallet");
                jsonValues.put("wallet_trx_id", wallet_id);
            } else {
                jsonValues.put("ipn_txn_id", transection_id);
                jsonValues.put("ipn_payment", "Paypal");
                jsonValues.put("ipn_status", "success");
                jsonValues.put("ipn_address", "");
            }

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + pov2;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        res_id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null && json1 != null) {
                if (!res_id.equals("null")) {
                    showsucessmesg.setVisibility(View.VISIBLE);
                    ActionStartsHere();
                }
            }
            super.onPostExecute(s);
        }
    }

    public class updateguild extends AsyncTask<String, String, String> {
        String res_id = "";
        JSONObject json, json1;
        String pov2 = "_table/parking_violations";

        @Override
        protected void onPreExecute() {

            pdialog = new ProgressDialog(getActivity());
            pdialog.setMessage("Loading.....");
            pdialog.setIndeterminate(false);
            pdialog.setCancelable(false);
            // pdialog.show();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", id);
            jsonValues.put("plead_not_guilty", "1");

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + pov2;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);

                        res_id = c.getString("id");

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (pdialog != null)
                pdialog.dismiss();

            if (getActivity() != null && json1 != null) {
                if (!res_id.equals("null")) {
                    txt_Plead_Not.setText("Plead Not Guilty: YES");
                    txt_Plead_Not_Guilty.setVisibility(View.GONE);
                    showsucessmesg.setVisibility(View.VISIBLE);
                    ActionStartsHere();
                }
            }
            super.onPostExecute(s);
        }
    }

    public class servicecharge extends AsyncTask<String, String, String> {
        String township_code1;
        String servicechargeurl;
        JSONObject json, json1;

        public servicecharge(String township_code) throws UnsupportedEncodingException {
            this.township_code1 = township_code;
            servicechargeurl = "_table/service_charges?filter=manager_id%3D'" + URLEncoder.encode(township_code, "utf8") + "'";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + servicechargeurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        String tr_percentage = c.getString("tr_percentage");
                        String tr_fee = c.getString("tr_fee");

                        if (!tr_fee.equals("null")) {
                            tr_fee1 = Double.parseDouble(tr_fee);
                        } else {
                            tr_fee1 = 0.0;
                        }
                        if (!tr_percentage.equals("null")) {
                            tr_percentage1 = Double.parseDouble(tr_percentage);
                        } else {
                            tr_percentage1 = 0.0;
                        }
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null && json != null) {
            }
            super.onPostExecute(s);
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    public void showimage(Activity context, File file) {

        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.imageshow, viewGroup, false);
        final PopupWindow popup = new PopupWindow(context);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(false);
        popup.setFocusable(false);
        popup.setAnimationStyle(R.style.animationName);
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

        final ImageView img_close = (ImageView) layout.findViewById(R.id.close);

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
        Picasso.with(context).load(file).into((ImageView) layout.findViewById(R.id.img_show), new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {
            }
        });
    }

    public class gettownship_list extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String parkingurl = "_table/townships_manager";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            township_array_list.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setTitle(c.getString("manager_id"));
                        item1.setManager_type(c.getString("manager_type"));
                        item1.setLot_manager(c.getString("lot_manager"));
                        item1.setAddress(c.getString("address"));
                        item1.setState(c.getString("state"));
                        item1.setCity(c.getString("city"));
                        item1.setCountry(c.getString("country"));
                        item1.setZip_code(c.getString("zip"));
                        item1.setTownship_logo(c.getString("township_logo"));
                        item1.setOfficial_logo(c.getString("official_logo"));
                        item1.setIsslected(false);
                        township_array_list.add(item1);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs = getResources();
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            if (getActivity() != null && json != null) {
                if (township_array_list.size() > 0) {
                    for (int i = 0; i < township_array_list.size(); i++) {
                        String township_code = township_array_list.get(i).getTitle();
                        if (township_code.equals(townshipcode)) {
                            township_logo_url = township_array_list.get(i).getOfficial_logo();
                            String township_name = township_array_list.get(i).getLot_manager();
                            txt_print_title.setText(township_name);
                            break;
                        }
                    }

                    if (!township_logo_url.equals("")) {
                        Picasso.with(getActivity()).load(township_logo_url).resize(100, 100).centerCrop().into(img_top_logo, new Callback() {
                            @Override
                            public void onSuccess() {
                                Log.e("township_logo", "yes");
                            }

                            @Override
                            public void onError() {
                                Log.e("township_logo", "no");
                            }
                        });
                    }
                }
            }
            super.onPostExecute(s);
        }
    }
}
