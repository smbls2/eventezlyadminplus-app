package com.softmeasures.eventezlyadminplus.frament.event;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragPdfViewBinding;
import com.softmeasures.eventezlyadminplus.models.MediaDefinition;
import com.softmeasures.eventezlyadminplus.utils.AESEncyption;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class PDFViewFragment extends BaseFragment {

    private FragPdfViewBinding binding;
    private MediaDefinition mediaDefinition;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_pdf_view, container, false);
        if (getArguments() != null) {
            mediaDefinition = new Gson().fromJson(getArguments().getString("mediaDefinition"), MediaDefinition.class);
        }
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();


    }


    private class RetrivePDFStream extends AsyncTask<String, Void, InputStream> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            binding.progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected InputStream doInBackground(String... strings) {
            InputStream inputStream = null;
            try {
                URL uri = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) uri.openConnection();
                if (urlConnection.getResponseCode() == 200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                }
            } catch (IOException e) {
                return null;
            }
            return inputStream;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            super.onPostExecute(inputStream);

            String pdfPassword = "";
            if (mediaDefinition.getPdfPassword() != null
                    && !TextUtils.isEmpty(mediaDefinition.getPdfPassword())
                    && !mediaDefinition.getPdfPassword().equals("null")) {
                try {
                    pdfPassword = AESEncyption.decrypt(mediaDefinition.getPdfPassword());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            binding.pdfView.fromStream(inputStream)
                    .password(pdfPassword)
                    .enableDoubletap(true)
                    .onLoad(nbPages -> {
                        binding.progressBar.setVisibility(View.GONE);
                    })
                    .onError(t -> {
                        Toast.makeText(getActivity(), "Sorry, Not able to open pdf!", Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();
                    })
                    .load();

        }
    }

    @Override
    protected void updateViews() {

        if (mediaDefinition != null) {
            if (mediaDefinition.getPdfFilePath() != null
                    && !TextUtils.isEmpty(mediaDefinition.getPdfFilePath())
                    && !mediaDefinition.getPdfFilePath().equals("null")) {
                new RetrivePDFStream().execute(mediaDefinition.getPdfFilePath());
            }
        }

    }

    @Override
    protected void setListeners() {

    }

    @Override
    protected void initViews(View v) {

    }
}
