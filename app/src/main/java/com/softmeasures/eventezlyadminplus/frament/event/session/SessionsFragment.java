package com.softmeasures.eventezlyadminplus.frament.event.session;

import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragSessionsBinding;
import com.softmeasures.eventezlyadminplus.databinding.PopupEventOptionTypeBinding;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.EventOptionTypeDialog;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.SessionSettingsAddEditFragment;
import com.softmeasures.eventezlyadminplus.interfaces.EventTypeListener;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.EventSession;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_LOCATION;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_VIRTUALLY;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_BOTH;
public class SessionsFragment extends BaseFragment implements EventTypeListener {
//
//    private FragSessionsBinding binding;
//    private ArrayList<EventSession> eventSessions = new ArrayList<>();
//    private EventSessionAdapter sessionAdapter;
//    public static boolean isSessionUpdated = false;
//    private EventDefinition eventDefinition;
//    private int selectedItemPosition = -1;
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        if (getArguments() != null) {
//            eventDefinition = new Gson().fromJson(getArguments().getString("eventDefinition"), EventDefinition.class);
//        }
//        binding = DataBindingUtil.inflate(inflater, R.layout.frag_sessions, container, false);
//        return binding.getRoot();
//    }
//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        initViews(view);
//        updateViews();
//        setListeners();
//
//        new fetchSessions().execute();
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        if (isSessionUpdated) {
//            isSessionUpdated = false;
//            new fetchSessions().execute();
//        }
//    }
//
//    @Override
//    public void onNext(int type, boolean isEdit) {
//        if (isEdit) {
//            final FragmentTransaction ft = getFragmentManager().beginTransaction();
//            SessionSettingsAddEditFragment frag = new SessionSettingsAddEditFragment();
//            Bundle bundle = new Bundle();
//            bundle.putBoolean("isEdit", true);
//            bundle.putInt("eventLogiType", type);
//            EventSession eventSession = eventSessions.get(selectedItemPosition);
//            eventSession.setEvent_session_logi_type(type);
//            bundle.putString("eventSessionDetails", new Gson().toJson(eventSession));
//            frag.setArguments(bundle);
//            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
//            ft.add(R.id.My_Container_1_ID, frag);
//            fragmentStack.lastElement().onPause();
//            ft.hide(fragmentStack.lastElement());
//            fragmentStack.push(frag);
//            ft.commitAllowingStateLoss();
//        } else {
//            final FragmentTransaction ft = getFragmentManager().beginTransaction();
//            SessionSettingsAddEditFragment frag = new SessionSettingsAddEditFragment();
//            Bundle bundle = new Bundle();
//            bundle.putInt("eventSessionLogiType", type);
//            bundle.putString("eventDetails", new Gson().toJson(eventDefinition));
//            bundle.putBoolean("isEdit", false);
//            frag.setArguments(bundle);
//            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
//            ft.add(R.id.My_Container_1_ID, frag);
//            fragmentStack.lastElement().onPause();
//            ft.hide(fragmentStack.lastElement());
//            fragmentStack.push(frag);
//            ft.commitAllowingStateLoss();
//        }
//    }
//
//    private class fetchSessions extends AsyncTask<String, String, String> {
//        JSONObject json;
//        JSONArray json1;
//        String eventDefUrl = "_table/event_session_definitions";
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            binding.swipeRefreshLayout.setRefreshing(true);
//            eventSessions.clear();
//            sessionAdapter.notifyDataSetChanged();
//            if (eventDefinition != null) {
//                eventDefUrl = "_table/event_session_definitions?filter=event_id=" + eventDefinition.getId();
//            }
////            if (selectedManager != null) {
////                eventDefUrl = "_table/event_definitions?filter=company_id=" + selectedManager.getId();
////            }
////            if (selectedManager != null && selectedManager.getTownship_code() != null
////                    && !TextUtils.isEmpty(selectedManager.getTownship_code())
////                    && !selectedManager.getTownship_code().equals("null")) {
////                eventDefUrl = "_table/event_definitions?filter=township_code=" + selectedSpot.getTownship_code();
////            }
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            String url = getString(R.string.api) + getString(R.string.povlive) + eventDefUrl;
//            Log.e("#DEBUG", "      fetchEventSessions:  " + url);
//            DefaultHttpClient client = new DefaultHttpClient();
//            HttpGet post = new HttpGet(url);
//            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
//            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
//            HttpResponse response = null;
//            try {
//                response = client.execute(post);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            if (response != null) {
//                HttpEntity resEntity = response.getEntity();
//                String responseStr = null;
//                try {
//                    responseStr = EntityUtils.toString(resEntity).trim();
//                    Log.e("#DEBUG", "   fetchEventSessions  Response:  " + responseStr);
//                    json = new JSONObject(responseStr);
//                    JSONArray jsonArray = json.getJSONArray("resource");
//
//                    for (int j = 0; j < jsonArray.length(); j++) {
//                        EventSession eventDefinition = new EventSession();
//                        JSONObject object = jsonArray.getJSONObject(j);
//                        if (object.has("id")
//                                && !TextUtils.isEmpty(object.getString("id"))
//                                && !object.getString("id").equals("null")) {
//                            eventDefinition.setId(object.getInt("id"));
//                        }
//                        if (object.has("date_time")
//                                && !TextUtils.isEmpty(object.getString("date_time"))
//                                && !object.getString("date_time").equals("null")) {
//                            eventDefinition.setDate_time(object.getString("date_time"));
//                        }
//                        if (object.has("manager_type")
//                                && !TextUtils.isEmpty(object.getString("manager_type"))
//                                && !object.getString("manager_type").equals("null")) {
//                            eventDefinition.setManager_type(object.getString("manager_type"));
//                        }
//                        if (object.has("manager_type_id")
//                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
//                                && !object.getString("manager_type_id").equals("null")) {
//                            eventDefinition.setManager_type_id(object.getInt("manager_type_id"));
//                        }
//                        if (object.has("twp_id")
//                                && !TextUtils.isEmpty(object.getString("twp_id"))
//                                && !object.getString("twp_id").equals("null")) {
//                            eventDefinition.setTwp_id(object.getInt("twp_id"));
//                        }
//                        if (object.has("township_code")
//                                && !TextUtils.isEmpty(object.getString("township_code"))
//                                && !object.getString("township_code").equals("null")) {
//                            eventDefinition.setTownship_code(object.getString("township_code"));
//                        }
//                        if (object.has("township_name")
//                                && !TextUtils.isEmpty(object.getString("township_name"))
//                                && !object.getString("township_name").equals("null")) {
//                            eventDefinition.setTownship_name(object.getString("township_name"));
//                        }
//                        if (object.has("company_id")
//                                && !TextUtils.isEmpty(object.getString("company_id"))
//                                && !object.getString("company_id").equals("null")) {
//                            eventDefinition.setCompany_id(object.getInt("company_id"));
//                        }
//                        if (object.has("company_code")
//                                && !TextUtils.isEmpty(object.getString("company_code"))
//                                && !object.getString("company_code").equals("null")) {
//                            eventDefinition.setCompany_code(object.getString("company_code"));
//                        }
//                        if (object.has("company_name")
//                                && !TextUtils.isEmpty(object.getString("company_name"))
//                                && !object.getString("company_name").equals("null")) {
//                            eventDefinition.setCompany_name(object.getString("company_name"));
//                        }
//                        if (object.has("event_id")
//                                && !TextUtils.isEmpty(object.getString("event_id"))
//                                && !object.getString("event_id").equals("null")) {
//                            eventDefinition.setEvent_id(object.getInt("event_id"));
//                        }
//                        if (object.has("event_type")
//                                && !TextUtils.isEmpty(object.getString("event_type"))
//                                && !object.getString("event_type").equals("null")) {
//                            eventDefinition.setEvent_type(object.getString("event_type"));
//                        }
//                        if (object.has("event_code")
//                                && !TextUtils.isEmpty(object.getString("event_code"))
//                                && !object.getString("event_code").equals("null")) {
//                            eventDefinition.setEvent_code(object.getString("event_code"));
//                        }
//                        if (object.has("event_name")
//                                && !TextUtils.isEmpty(object.getString("event_name"))
//                                && !object.getString("event_name").equals("null")) {
//                            eventDefinition.setEvent_name(object.getString("event_name"));
//                        }
//                        if (object.has("event_session_id")
//                                && !TextUtils.isEmpty(object.getString("event_session_id"))
//                                && !object.getString("event_session_id").equals("null")) {
//                            eventDefinition.setEvent_session_id(object.getInt("event_session_id"));
//                        }
//                        if (object.has("event_session_type")
//                                && !TextUtils.isEmpty(object.getString("event_session_type"))
//                                && !object.getString("event_session_type").equals("null")) {
//                            eventDefinition.setEvent_session_type(object.getString("event_session_type"));
//                        }
//                        if (object.has("event_session_code")
//                                && !TextUtils.isEmpty(object.getString("event_session_code"))
//                                && !object.getString("event_session_code").equals("null")) {
//                            eventDefinition.setEvent_session_code(object.getString("event_session_code"));
//                        }
//                        if (object.has("event_session_name")
//                                && !TextUtils.isEmpty(object.getString("event_session_name"))
//                                && !object.getString("event_session_name").equals("null")) {
//                            eventDefinition.setEvent_session_name(object.getString("event_session_name"));
//                        }
//                        if (object.has("event_session_short_description")
//                                && !TextUtils.isEmpty(object.getString("event_session_short_description"))
//                                && !object.getString("event_session_short_description").equals("null")) {
//                            eventDefinition.setEvent_session_short_description(object.getString("event_session_short_description"));
//                        }
//                        if (object.has("event_session_long_description")
//                                && !TextUtils.isEmpty(object.getString("event_session_long_description"))
//                                && !object.getString("event_session_long_description").equals("null")) {
//                            eventDefinition.setEvent_session_long_description(object.getString("event_session_long_description"));
//                        }
//                        if (object.has("event_session_link_on_web")
//                                && !TextUtils.isEmpty(object.getString("event_session_link_on_web"))
//                                && !object.getString("event_session_link_on_web").equals("null")) {
//                            eventDefinition.setEvent_session_link_on_web(object.getString("event_session_link_on_web"));
//                        }
//                        if (object.has("event_session_link_twitter")
//                                && !TextUtils.isEmpty(object.getString("event_session_link_twitter"))
//                                && !object.getString("event_session_link_twitter").equals("null")) {
//                            eventDefinition.setEvent_session_link_twitter(object.getString("event_session_link_twitter"));
//                        }
//                        if (object.has("event_session_link_whatsapp")
//                                && !TextUtils.isEmpty(object.getString("event_session_link_whatsapp"))
//                                && !object.getString("event_session_link_whatsapp").equals("null")) {
//                            eventDefinition.setEvent_session_link_whatsapp(object.getString("event_session_link_whatsapp"));
//                        }
//                        if (object.has("event_session_link_facebook")
//                                && !TextUtils.isEmpty(object.getString("event_session_link_facebook"))
//                                && !object.getString("event_session_link_facebook").equals("null")) {
//                            eventDefinition.setEvent_session_link_facebook(object.getString("event_session_link_facebook"));
//                        }
//                        if (object.has("event_session_link_other_media")
//                                && !TextUtils.isEmpty(object.getString("event_session_link_other_media"))
//                                && !object.getString("event_session_link_other_media").equals("null")) {
//                            eventDefinition.setEvent_session_link_other_media(object.getString("event_session_link_other_media"));
//                        }
//                        if (object.has("company_logo")
//                                && !TextUtils.isEmpty(object.getString("company_logo"))
//                                && !object.getString("company_logo").equals("null")) {
//                            eventDefinition.setCompany_logo(object.getString("company_logo"));
//                        }
//                        if (object.has("event_logo")
//                                && !TextUtils.isEmpty(object.getString("event_logo"))
//                                && !object.getString("event_logo").equals("null")) {
//                            eventDefinition.setEvent_logo(object.getString("event_logo"));
//                        }
//                        if (object.has("event_session_logo")
//                                && !TextUtils.isEmpty(object.getString("event_session_logo"))
//                                && !object.getString("event_session_logo").equals("null")) {
//                            eventDefinition.setEvent_session_logo(object.getString("event_session_logo"));
//                        }
//                        if (object.has("event_session_image1")
//                                && !TextUtils.isEmpty(object.getString("event_session_image1"))
//                                && !object.getString("event_session_image1").equals("null")) {
//                            eventDefinition.setEvent_session_image1(object.getString("event_session_image1"));
//                        }
//                        if (object.has("event_session_image2")
//                                && !TextUtils.isEmpty(object.getString("event_session_image2"))
//                                && !object.getString("event_session_image2").equals("null")) {
//                            eventDefinition.setEvent_session_image2(object.getString("event_session_image2"));
//                        }
//                        if (object.has("event_session_blob_image")
//                                && !TextUtils.isEmpty(object.getString("event_session_blob_image"))
//                                && !object.getString("event_session_blob_image").equals("null")) {
//                            eventDefinition.setEvent_session_blob_image(object.getString("event_session_blob_image"));
//                        }
//                        if (object.has("event_session_address")
//                                && !TextUtils.isEmpty(object.getString("event_session_address"))
//                                && !object.getString("event_session_address").equals("null")) {
//                            eventDefinition.setEvent_session_address(object.getString("event_session_address"));
//                        }
//                        if (object.has("covered_locations")
//                                && !TextUtils.isEmpty(object.getString("covered_locations"))
//                                && !object.getString("covered_locations").equals("null")) {
//                            eventDefinition.setCovered_locations(object.getString("covered_locations"));
//                        }
//                        if (object.has("session_regn_needed_approval")
//                                && !TextUtils.isEmpty(object.getString("session_regn_needed_approval"))
//                                && !object.getString("session_regn_needed_approval").equals("null")) {
//                            eventDefinition.setSession_regn_needed_approval(object.getBoolean("session_regn_needed_approval"));
//                        }
//                        if (object.has("requirements")
//                                && !TextUtils.isEmpty(object.getString("requirements"))
//                                && !object.getString("requirements").equals("null")) {
//                            eventDefinition.setRequirements(object.getString("requirements"));
//                        }
//                        if (object.has("appl_req_download")
//                                && !TextUtils.isEmpty(object.getString("appl_req_download"))
//                                && !object.getString("appl_req_download").equals("null")) {
//                            eventDefinition.setAppl_req_download(object.getString("appl_req_download"));
//                        }
//                        if (object.has("cost")
//                                && !TextUtils.isEmpty(object.getString("cost"))
//                                && !object.getString("cost").equals("null")) {
//                            eventDefinition.setCost(object.getString("cost"));
//                        }
//                        if (object.has("year")
//                                && !TextUtils.isEmpty(object.getString("year"))
//                                && !object.getString("year").equals("null")) {
//                            eventDefinition.setYear(object.getString("year"));
//                        }
//                        if (object.has("location_address")
//                                && !TextUtils.isEmpty(object.getString("location_address"))
//                                && !object.getString("location_address").equals("null")) {
//                            eventDefinition.setLocation_address(object.getString("location_address"));
//                        }
//                        if (object.has("scheme_type")
//                                && !TextUtils.isEmpty(object.getString("scheme_type"))
//                                && !object.getString("scheme_type").equals("null")) {
//                            eventDefinition.setScheme_type(object.getString("scheme_type"));
//                        }
//                        if (object.has("event_prefix")
//                                && !TextUtils.isEmpty(object.getString("event_prefix"))
//                                && !object.getString("event_prefix").equals("null")) {
//                            eventDefinition.setEvent_prefix(object.getString("event_prefix"));
//                        }
//
//                        if (object.has("event_session_prefix")
//                                && !TextUtils.isEmpty(object.getString("event_session_prefix"))
//                                && !object.getString("event_session_prefix").equals("null")) {
//                            eventDefinition.setEvent_session_prefix(object.getString("event_session_prefix"));
//                        }
//                        if (object.has("event_nextnum")
//                                && !TextUtils.isEmpty(object.getString("event_nextnum"))
//                                && !object.getString("event_nextnum").equals("null")) {
//                            eventDefinition.setEvent_nextnum(object.getInt("event_nextnum"));
//                        }
//                        if (object.has("event_session_nextnum")
//                                && !TextUtils.isEmpty(object.getString("event_session_nextnum"))
//                                && !object.getString("event_session_nextnum").equals("null")) {
//                            eventDefinition.setEvent_session_nextnum(object.getInt("event_session_nextnum"));
//                        }
//                        if (object.has("local_time_event_session_start")
//                                && !TextUtils.isEmpty(object.getString("local_time_event_session_start"))
//                                && !object.getString("local_time_event_session_start").equals("null")) {
//                            eventDefinition.setLocal_time_event_session_start(object.getString("local_time_event_session_start"));
//                        }
//                        if (object.has("local_timezone")
//                                && !TextUtils.isEmpty(object.getString("local_timezone"))
//                                && !object.getString("local_timezone").equals("null")) {
//                            eventDefinition.setLocal_timezone(object.getString("local_timezone"));
//                        }
//                        if (object.has("event_session_begins_date_time")
//                                && !TextUtils.isEmpty(object.getString("event_session_begins_date_time"))
//                                && !object.getString("event_session_begins_date_time").equals("null")) {
//                            eventDefinition.setEvent_session_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_begins_date_time")));
//                        }
//                        if (object.has("event_session_ends_date_time")
//                                && !TextUtils.isEmpty(object.getString("event_session_ends_date_time"))
//                                && !object.getString("event_session_ends_date_time").equals("null")) {
//                            eventDefinition.setEvent_session_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_ends_date_time")));
//                        }
//                        if (object.has("event_session_parking_begins_date_time")
//                                && !TextUtils.isEmpty(object.getString("event_session_parking_begins_date_time"))
//                                && !object.getString("event_session_parking_begins_date_time").equals("null")) {
//                            eventDefinition.setEvent_session_parking_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_parking_begins_date_time")));
//                        }
//                        if (object.has("event_session_parking_ends_date_time")
//                                && !TextUtils.isEmpty(object.getString("event_session_parking_ends_date_time"))
//                                && !object.getString("event_session_parking_ends_date_time").equals("null")) {
//                            eventDefinition.setEvent_session_parking_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_parking_ends_date_time")));
//                        }
//
//                        if (object.has("event_session_multi_dates")
//                                && !TextUtils.isEmpty(object.getString("event_session_multi_dates"))
//                                && !object.getString("event_session_multi_dates").equals("null")) {
//                            eventDefinition.setEvent_session_multi_dates(object.getString("event_session_multi_dates"));
//                        }
//
//                        if (object.has("event_session_parking_timings")
//                                && !TextUtils.isEmpty(object.getString("event_session_parking_timings"))
//                                && !object.getString("event_session_parking_timings").equals("null")) {
//                            eventDefinition.setEvent_session_parking_timings(object.getString("event_session_parking_timings"));
//                        }
//
//                        if (object.has("event_session_timings")
//                                && !TextUtils.isEmpty(object.getString("event_session_timings"))
//                                && !object.getString("event_session_timings").equals("null")) {
//                            eventDefinition.setEvent_session_timings(object.getString("event_session_timings"));
//                        }
//
//                        if (object.has("expires_by")
//                                && !TextUtils.isEmpty(object.getString("expires_by"))
//                                && !object.getString("expires_by").equals("null")) {
//                            eventDefinition.setExpires_by(object.getString("expires_by"));
//                        }
//                        if (object.has("regn_reqd")
//                                && !TextUtils.isEmpty(object.getString("regn_reqd"))
//                                && !object.getString("regn_reqd").equals("null")) {
//                            eventDefinition.setRegn_reqd(object.getBoolean("regn_reqd"));
//                        }
//                        if (object.has("event_session_regn_allowed")
//                                && !TextUtils.isEmpty(object.getString("event_session_regn_allowed"))
//                                && !object.getString("event_session_regn_allowed").equals("null")) {
//                            eventDefinition.setEvent_session_regn_allowed(object.getBoolean("event_session_regn_allowed"));
//                        }
//                        if (object.has("event_session_regn_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_regn_fee"))
//                                && !object.getString("event_session_regn_fee").equals("null")) {
//                            eventDefinition.setEvent_session_regn_fee(object.getString("event_session_regn_fee"));
//                        }
//                        if (object.has("event_session_led_by")
//                                && !TextUtils.isEmpty(object.getString("event_session_led_by"))
//                                && !object.getString("event_session_led_by").equals("null")) {
//                            eventDefinition.setEvent_session_led_by(object.getString("event_session_led_by"));
//                        }
//                        if (object.has("event_session_leaders_bio")
//                                && !TextUtils.isEmpty(object.getString("event_session_leaders_bio"))
//                                && !object.getString("event_session_leaders_bio").equals("null")) {
//                            eventDefinition.setEvent_session_leaders_bio(object.getString("event_session_leaders_bio"));
//                        }
//                        if (object.has("regn_reqd_for_parking")
//                                && !TextUtils.isEmpty(object.getString("regn_reqd_for_parking"))
//                                && !object.getString("regn_reqd_for_parking").equals("null")) {
//                            eventDefinition.setRegn_reqd_for_parking(object.getBoolean("regn_reqd_for_parking"));
//                        }
//                        if (object.has("renewable")
//                                && !TextUtils.isEmpty(object.getString("renewable"))
//                                && !object.getString("renewable").equals("null")) {
//                            eventDefinition.setRenewable(object.getBoolean("renewable"));
//                        }
//                        if (object.has("active")
//                                && !TextUtils.isEmpty(object.getString("active"))
//                                && !object.getString("active").equals("null")) {
//                            eventDefinition.setActive(object.getBoolean("active"));
//                        }
//
//                        if (object.has("event_session_parking_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_parking_fee"))
//                                && !object.getString("event_session_parking_fee").equals("null")) {
//                            eventDefinition.setEvent_session_parking_fee(object.getString("event_session_parking_fee"));
//                        }
//
//                        if (object.has("free_session")
//                                && !TextUtils.isEmpty(object.getString("free_session"))
//                                && !object.getString("free_session").equals("null")) {
//                            eventDefinition.setFree_session(object.getBoolean("free_session"));
//                        }
//
//                        if (object.has("free_session_parking")
//                                && !TextUtils.isEmpty(object.getString("free_session_parking"))
//                                && !object.getString("free_session_parking").equals("null")) {
//                            eventDefinition.setFree_session_parking(object.getBoolean("free_session_parking"));
//                        }
//
//                        if (object.has("event_session_regn_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_regn_fee"))
//                                && !object.getString("event_session_regn_fee").equals("null")) {
//                            eventDefinition.setEvent_session_regn_fee(object.getString("event_session_regn_fee"));
//                        }
//
//                        if (object.has("event_session_youth_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_youth_fee"))
//                                && !object.getString("event_session_youth_fee").equals("null")) {
//                            eventDefinition.setEvent_session_youth_fee(object.getString("event_session_youth_fee"));
//                        }
//
//                        if (object.has("event_session_child_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_child_fee"))
//                                && !object.getString("event_session_child_fee").equals("null")) {
//                            eventDefinition.setEvent_session_child_fee(object.getString("event_session_child_fee"));
//                        }
//
//                        if (object.has("event_session_student_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_student_fee"))
//                                && !object.getString("event_session_student_fee").equals("null")) {
//                            eventDefinition.setEvent_session_student_fee(object.getString("event_session_student_fee"));
//                        }
//
//                        if (object.has("event_session_minister_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_minister_fee"))
//                                && !object.getString("event_session_minister_fee").equals("null")) {
//                            eventDefinition.setEvent_session_minister_fee(object.getString("event_session_minister_fee"));
//                        }
//
//                        if (object.has("event_session_clergy_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_clergy_fee"))
//                                && !object.getString("event_session_clergy_fee").equals("null")) {
//                            eventDefinition.setEvent_session_clergy_fee(object.getString("event_session_clergy_fee"));
//                        }
//
//                        if (object.has("event_session_promo_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_promo_fee"))
//                                && !object.getString("event_session_promo_fee").equals("null")) {
//                            eventDefinition.setEvent_session_promo_fee(object.getString("event_session_promo_fee"));
//                        }
//
//                        if (object.has("event_session_senior_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_senior_fee"))
//                                && !object.getString("event_session_senior_fee").equals("null")) {
//                            eventDefinition.setEvent_session_senior_fee(object.getString("event_session_senior_fee"));
//                        }
//
//                        if (object.has("event_session_staff_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_staff_fee"))
//                                && !object.getString("event_session_staff_fee").equals("null")) {
//                            eventDefinition.setEvent_session_staff_fee(object.getString("event_session_staff_fee"));
//                        }
//
//                        if (object.has("event_session_family_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_family_fee"))
//                                && !object.getString("event_session_family_fee").equals("null")) {
//                            eventDefinition.setEvent_session_family_fee(object.getString("event_session_family_fee"));
//                        }
//
//                        if (object.has("event_session_link_ytube")
//                                && !TextUtils.isEmpty(object.getString("event_session_link_ytube"))
//                                && !object.getString("event_session_link_ytube").equals("null")) {
//                            eventDefinition.setEvent_session_link_ytube(object.getString("event_session_link_ytube"));
//                        }
//                        if (object.has("event_session_link_zoom")
//                                && !TextUtils.isEmpty(object.getString("event_session_link_zoom"))
//                                && !object.getString("event_session_link_zoom").equals("null")) {
//                            eventDefinition.setEvent_session_link_zoom(object.getString("event_session_link_zoom"));
//                        }
//                        if (object.has("event_session_link_googlemeet")
//                                && !TextUtils.isEmpty(object.getString("event_session_link_googlemeet"))
//                                && !object.getString("event_session_link_googlemeet").equals("null")) {
//                            eventDefinition.setEvent_session_link_googlemeet(object.getString("event_session_link_googlemeet"));
//                        }
//                        if (object.has("event_session_link_googleclassroom")
//                                && !TextUtils.isEmpty(object.getString("event_session_link_googleclassroom"))
//                                && !object.getString("event_session_link_googleclassroom").equals("null")) {
//                            eventDefinition.setEvent_session_link_googleclassroom(object.getString("event_session_link_googleclassroom"));
//                        }
//                        if (object.has("event_session_regn_limit")
//                                && !TextUtils.isEmpty(object.getString("event_session_regn_limit"))
//                                && !object.getString("event_session_regn_limit").equals("null")) {
//                            eventDefinition.setEvent_session_regn_limit(object.getString("event_session_regn_limit"));
//                        }
//                        if (object.has("web_event_session_regn_fee")
//                                && !TextUtils.isEmpty(object.getString("web_event_session_regn_fee"))
//                                && !object.getString("web_event_session_regn_fee").equals("null")) {
//                            eventDefinition.setWeb_event_session_regn_fee(object.getString("web_event_session_regn_fee"));
//                        }
//                        if (object.has("web_event_session_regn_limit")
//                                && !TextUtils.isEmpty(object.getString("web_event_session_regn_limit"))
//                                && !object.getString("web_event_session_regn_limit").equals("null")) {
//                            eventDefinition.setWeb_event_session_regn_limit(object.getString("web_event_session_regn_limit"));
//                        }
//                        if (object.has("parking_reservation_limit")
//                                && !TextUtils.isEmpty(object.getString("parking_reservation_limit"))
//                                && !object.getString("parking_reservation_limit").equals("null")) {
//                            eventDefinition.setEvent_session_parking_reserv_limit(object.getString("parking_reservation_limit"));
//                        }
//                        if (object.has("web_event_session_location_to_show")
//                                && !TextUtils.isEmpty(object.getString("web_event_session_location_to_show"))
//                                && !object.getString("web_event_session_location_to_show").equals("null")) {
//                            eventDefinition.setWeb_event_session_location_to_show(object.getBoolean("web_event_session_location_to_show"));
//                        }
//                        if (object.has("free_web_session")
//                                && !TextUtils.isEmpty(object.getString("free_web_session"))
//                                && !object.getString("free_web_session").equals("null")) {
//                            eventDefinition.setFree_web_session(object.getBoolean("free_web_session"));
//                        }
//                        if (object.has("event_session_logi_type")
//                                && !TextUtils.isEmpty(object.getString("event_session_logi_type"))
//                                && !object.getString("event_session_logi_type").equals("null")) {
//                            eventDefinition.setEvent_session_logi_type(object.getInt("event_session_logi_type"));
//                        }
//
//                        if (object.has("free_local_session")
//                                && !TextUtils.isEmpty(object.getString("free_local_session"))
//                                && !object.getString("free_local_session").equals("null")) {
//                            eventDefinition.setFree_local_session(object.getBoolean("free_local_session"));
//                        }
//
//                        if (object.has("reqd_web_session_regn")
//                                && !TextUtils.isEmpty(object.getString("reqd_web_session_regn"))
//                                && !object.getString("reqd_web_session_regn").equals("null")) {
//                            eventDefinition.setReqd_web_session_regn(object.getBoolean("reqd_web_session_regn"));
//                        }
//
//                        if (object.has("reqd_local_session_regn")
//                                && !TextUtils.isEmpty(object.getString("reqd_local_session_regn"))
//                                && !object.getString("reqd_local_session_regn").equals("null")) {
//                            eventDefinition.setReqd_local_session_regn(object.getBoolean("reqd_local_session_regn"));
//                        }
//
//                        if (object.has("is_parking_allowed")
//                                && !TextUtils.isEmpty(object.getString("is_parking_allowed"))
//                                && !object.getString("is_parking_allowed").equals("null")) {
//                            eventDefinition.setIs_parking_allowed(object.getBoolean("is_parking_allowed"));
//                        }
//
//                        if (object.has("session_link_array")
//                                && !TextUtils.isEmpty(object.getString("session_link_array"))
//                                && !object.getString("session_link_array").equals("null")) {
//                            eventDefinition.setSession_link_array(object.getString("session_link_array"));
//                        }
//
//                        eventSessions.add(eventDefinition);
//
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            if (getActivity() != null) {
//                binding.swipeRefreshLayout.setRefreshing(false);
//                if (eventSessions.size() != 0) {
//                    binding.tvError.setVisibility(View.GONE);
//                    sessionAdapter.notifyDataSetChanged();
//                } else {
//                    binding.tvError.setVisibility(View.VISIBLE);
//                }
//            }
//        }
//    }
//
//    @Override
//    protected void updateViews() {
//
//    }
//
//    @Override
//    protected void setListeners() {
//
//        binding.swipeRefreshLayout.setOnRefreshListener(() -> {
//            new fetchSessions().execute();
//        });
//
//        binding.tvBtnAddEventSession.setOnClickListener(v -> {
//
//            EventOptionTypeDialog dialog = EventOptionTypeDialog.newInstance();
//            Bundle bundle = new Bundle();
//            bundle.putBoolean("isSession", true);
//            bundle.putInt("eventLogiType", EVENT_TYPE_AT_LOCATION);
//            dialog.setArguments(bundle);
//            dialog.show(getChildFragmentManager(), "payment");
//
//        });
//    }
//
//    @Override
//    protected void initViews(View v) {
//
//        sessionAdapter = new EventSessionAdapter();
//        binding.rvSession.setLayoutManager(new LinearLayoutManager(getActivity()));
//        binding.rvSession.setAdapter(sessionAdapter);
//
//
//    }
//
//    private class EventSessionAdapter extends RecyclerView.Adapter<EventSessionAdapter.EventSessionHolder> {
//
//        @NonNull
//        @Override
//        public EventSessionAdapter.EventSessionHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//            return new EventSessionHolder(LayoutInflater.from(getActivity())
//                    .inflate(R.layout.item_event_session_new, viewGroup, false));
//        }
//
//        @Override
//        public void onBindViewHolder(@NonNull EventSessionAdapter.EventSessionHolder eventSessionHolder, int i) {
//            EventSession eventSession = eventSessions.get(i);
//            if (eventSession != null) {
//                Glide.with(getActivity()).load(eventSession.getEvent_session_logo()).placeholder(R.drawable.place_holder)
//                        .error(R.drawable.place_holder).into(eventSessionHolder.ivLogo);
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    eventSessionHolder.ivLogo.setClipToOutline(true);
//                }
//
//                if (eventSession.getEvent_session_name() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_name())) {
//                    eventSessionHolder.tvEventTitle.setText(eventSession.getEvent_session_name());
//                }
//                if (eventSession.getEvent_session_type() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_type())) {
//                    eventSessionHolder.tvEventType.setText(eventSession.getEvent_session_type());
//                }
//                if (eventSession.getCost() != null
//                        && !TextUtils.isEmpty(eventSession.getCost())) {
//                    if (eventSession.getCost().equals("0")) {
//                        eventSessionHolder.tvCost.setText("FREE");
//                    } else {
//                        eventSessionHolder.tvCost.setText(String.format("$%s", eventSession.getCost()));
//                    }
//                } else {
//                    eventSessionHolder.tvCost.setText("FREE");
//                }
//
//                if (!TextUtils.isEmpty(eventSession.getEvent_session_address())) {
//                    eventSessionHolder.tvAddress.setText(eventSession.getEvent_session_address());
//                    try {
//                        JSONArray jsonArray = new JSONArray(eventSession.getEvent_session_address());
//                        if (jsonArray.length() > 0) {
//                            JSONArray jsonArray1 = jsonArray.getJSONArray(0);
//                            if (jsonArray1.length() > 0) {
//                                eventSessionHolder.tvAddress.setText(jsonArray1.getString(0));
//                            }
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//                if (eventSession.getEvent_session_begins_date_time() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_begins_date_time())) {
//                    eventSessionHolder.tvEventDate.setText(DateTimeUtils.getDDMMM(eventSession.getEvent_session_begins_date_time()));
//                }
//                if (eventSession.getEvent_session_led_by() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_led_by())) {
//                    eventSessionHolder.llLeadBy.setVisibility(View.VISIBLE);
//                    eventSessionHolder.tvLeadBy.setText(eventSession.getEvent_session_led_by());
//                } else {
//                    eventSessionHolder.llLeadBy.setVisibility(View.GONE);
//                }
//
//                if (eventSession.getEvent_session_timings() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_timings())) {
//                    try {
//                        JSONArray jsonArrayTime = new JSONArray(eventSession.getEvent_session_timings());
//                        StringBuilder stringBuilder = new StringBuilder();
//                        if (jsonArrayTime != null && jsonArrayTime.length() > 0) {
//                            JSONArray jsonArray2 = jsonArrayTime.getJSONArray(0);
//                            if (jsonArray2 != null && jsonArray2.length() > 0) {
//                                if (jsonArray2.get(0).toString().length() > 30) {
//                                    String[] s = jsonArray2.get(0).toString().split(" - ");
//                                    for (int j = 0; j < s.length; j++) {
//                                        if (j != 0) {
//                                            stringBuilder.append("-\n");
//                                        }
//                                        stringBuilder.append(DateTimeUtils.gmtToLocalDateAMPM(s[j]).substring(11));
//                                    }
//                                } else {
//                                    stringBuilder.append(jsonArray2.get(0).toString());
//                                }
//                            }
//                        }
//                        eventSessionHolder.tvSessionTime.setText(stringBuilder.toString());
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }
//
//        @Override
//        public int getItemCount() {
//            return eventSessions.size();
//        }
//
//        class EventSessionHolder extends RecyclerView.ViewHolder {
//            TextView tvEventTitle, tvEventType;
//            TextView tvBtnEditEvent, tvAddress, tvCost, tvEventDate, tvSessionTime, tvLeadBy;
//            ImageView ivLogo;
//            LinearLayout llLeadBy;
//
//            EventSessionHolder(@NonNull View itemView) {
//                super(itemView);
//                tvEventTitle = itemView.findViewById(R.id.tvEventTitle);
//                tvEventType = itemView.findViewById(R.id.tvEventType);
//                tvBtnEditEvent = itemView.findViewById(R.id.tvBtnEditEvent);
//                tvAddress = itemView.findViewById(R.id.tvAddress);
//                tvCost = itemView.findViewById(R.id.tvCost);
//                tvEventDate = itemView.findViewById(R.id.tvEventDate);
//                tvSessionTime = itemView.findViewById(R.id.tvSessionTime);
//                tvLeadBy = itemView.findViewById(R.id.tvLeadBy);
//                ivLogo = itemView.findViewById(R.id.ivLogo);
//                llLeadBy = itemView.findViewById(R.id.llLeadBy);
//
//                tvBtnEditEvent.setOnClickListener(v -> {
//                    final int pos = getAdapterPosition();
//                    if (pos != RecyclerView.NO_POSITION) {
//
//                        selectedItemPosition = getAdapterPosition();
//                        EventOptionTypeDialog dialog = EventOptionTypeDialog.newInstance();
//                        Bundle bundle = new Bundle();
//                        bundle.putInt("eventLogiType", eventSessions.get(pos).getEvent_session_logi_type());
//                        bundle.putBoolean("isEdit", true);
//                        bundle.putBoolean("isSession", true);
//                        dialog.setArguments(bundle);
//                        dialog.show(getChildFragmentManager(), "payment");
//                    }
//                });
//
//                itemView.setOnClickListener(v -> {
//                    final int pos = getAdapterPosition();
//                    if (pos != RecyclerView.NO_POSITION) {
//                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
//                        EventSessionInfoViewFragment frag = new EventSessionInfoViewFragment();
//                        Bundle bundle = new Bundle();
//                        bundle.putInt("eventSessionId", eventSessions.get(pos).getId());
//                        frag.setArguments(bundle);
//                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
//                        ft.add(R.id.My_Container_1_ID, frag);
//                        fragmentStack.lastElement().onPause();
//                        ft.hide(fragmentStack.lastElement());
//                        fragmentStack.push(frag);
//                        ft.commitAllowingStateLoss();
//                    }
//                });
//            }
//        }
//    }


    private FragSessionsBinding binding;
    private ArrayList<EventSession> eventSessions = new ArrayList<>();
    private EventSessionAdapter sessionAdapter;
    public static boolean isSessionUpdated = false;
    private EventDefinition eventDefinition;
    private int selectedItemPosition = -1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            eventDefinition = new Gson().fromJson(getArguments().getString("eventDefinition"), EventDefinition.class);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_sessions, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        new fetchSessions().execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isSessionUpdated) {
            isSessionUpdated = false;
            eventSessions.clear();
            sessionAdapter.notifyDataSetChanged();
            new fetchSessions().execute();
        }
    }

    @Override
    public void onNext(int type, boolean isEdit) {
        if (isEdit) {
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            SessionSettingsAddEditFragment frag = new SessionSettingsAddEditFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isEdit", true);
            bundle.putInt("eventLogiType", type);
            EventSession eventSession = eventSessions.get(selectedItemPosition);
            eventSession.setEvent_session_logi_type(type);
            bundle.putString("eventSessionDetails", new Gson().toJson(eventSession));
            frag.setArguments(bundle);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, frag);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(frag);
            ft.commitAllowingStateLoss();
        } else {
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            SessionSettingsAddEditFragment frag = new SessionSettingsAddEditFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("eventSessionLogiType", type);
            bundle.putString("eventDetails", new Gson().toJson(eventDefinition));
            bundle.putBoolean("isEdit", false);
            frag.setArguments(bundle);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, frag);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(frag);
            ft.commitAllowingStateLoss();
        }
    }

    private class fetchSessions extends AsyncTask<String, String, String> {
        JSONObject json;
        JSONArray json1;
        String eventDefUrl = "_table/event_session_definitions";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            binding.swipeRefreshLayout.setRefreshing(true);
            binding.tvError.setVisibility(View.GONE);
            eventSessions.clear();
            if (eventDefinition != null)
                eventDefUrl = "_table/event_session_definitions?filter=event_id=" + eventDefinition.getId();
//            if (selectedManager != null) {
//                eventDefUrl = "_table/event_definitions?filter=company_id=" + selectedManager.getId();
//            }
//            if (selectedManager != null && selectedManager.getTownship_code() != null
//                    && !TextUtils.isEmpty(selectedManager.getTownship_code())
//                    && !selectedManager.getTownship_code().equals("null")) {
//                eventDefUrl = "_table/event_definitions?filter=township_code=" + selectedSpot.getTownship_code();
//            }
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + eventDefUrl;
            Log.e("#DEBUG", "      fetchEventSessions:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   fetchEventSessions  Response:  " + responseStr);
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");

                    for (int j = 0; j < jsonArray.length(); j++) {
                        EventSession eventDefinition = new EventSession();
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            eventDefinition.setId(object.getInt("id"));
                        }
                        if (object.has("date_time")
                                && !TextUtils.isEmpty(object.getString("date_time"))
                                && !object.getString("date_time").equals("null")) {
                            eventDefinition.setDate_time(object.getString("date_time"));
                        }
                        if (object.has("manager_type")
                                && !TextUtils.isEmpty(object.getString("manager_type"))
                                && !object.getString("manager_type").equals("null")) {
                            eventDefinition.setManager_type(object.getString("manager_type"));
                        }
                        if (object.has("manager_type_id")
                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
                                && !object.getString("manager_type_id").equals("null")) {
                            eventDefinition.setManager_type_id(object.getInt("manager_type_id"));
                        }
                        if (object.has("twp_id")
                                && !TextUtils.isEmpty(object.getString("twp_id"))
                                && !object.getString("twp_id").equals("null")) {
                            eventDefinition.setTwp_id(object.getInt("twp_id"));
                        }
                        if (object.has("township_code")
                                && !TextUtils.isEmpty(object.getString("township_code"))
                                && !object.getString("township_code").equals("null")) {
                            eventDefinition.setTownship_code(object.getString("township_code"));
                        }
                        if (object.has("township_name")
                                && !TextUtils.isEmpty(object.getString("township_name"))
                                && !object.getString("township_name").equals("null")) {
                            eventDefinition.setTownship_name(object.getString("township_name"));
                        }
                        if (object.has("company_id")
                                && !TextUtils.isEmpty(object.getString("company_id"))
                                && !object.getString("company_id").equals("null")) {
                            eventDefinition.setCompany_id(object.getInt("company_id"));
                        }
                        if (object.has("company_code")
                                && !TextUtils.isEmpty(object.getString("company_code"))
                                && !object.getString("company_code").equals("null")) {
                            eventDefinition.setCompany_code(object.getString("company_code"));
                        }
                        if (object.has("company_name")
                                && !TextUtils.isEmpty(object.getString("company_name"))
                                && !object.getString("company_name").equals("null")) {
                            eventDefinition.setCompany_name(object.getString("company_name"));
                        }
                        if (object.has("event_id")
                                && !TextUtils.isEmpty(object.getString("event_id"))
                                && !object.getString("event_id").equals("null")) {
                            eventDefinition.setEvent_id(object.getInt("event_id"));
                        }
                        if (object.has("event_type")
                                && !TextUtils.isEmpty(object.getString("event_type"))
                                && !object.getString("event_type").equals("null")) {
                            eventDefinition.setEvent_type(object.getString("event_type"));
                        }
                        if (object.has("event_code")
                                && !TextUtils.isEmpty(object.getString("event_code"))
                                && !object.getString("event_code").equals("null")) {
                            eventDefinition.setEvent_code(object.getString("event_code"));
                        }
                        if (object.has("event_name")
                                && !TextUtils.isEmpty(object.getString("event_name"))
                                && !object.getString("event_name").equals("null")) {
                            eventDefinition.setEvent_name(object.getString("event_name"));
                        }
                        if (object.has("event_session_id")
                                && !TextUtils.isEmpty(object.getString("event_session_id"))
                                && !object.getString("event_session_id").equals("null")) {
                            eventDefinition.setEvent_session_id(object.getInt("event_session_id"));
                        }
                        if (object.has("event_session_type")
                                && !TextUtils.isEmpty(object.getString("event_session_type"))
                                && !object.getString("event_session_type").equals("null")) {
                            eventDefinition.setEvent_session_type(object.getString("event_session_type"));
                        }
                        if (object.has("event_session_code")
                                && !TextUtils.isEmpty(object.getString("event_session_code"))
                                && !object.getString("event_session_code").equals("null")) {
                            eventDefinition.setEvent_session_code(object.getString("event_session_code"));
                        }
                        if (object.has("event_session_name")
                                && !TextUtils.isEmpty(object.getString("event_session_name"))
                                && !object.getString("event_session_name").equals("null")) {
                            eventDefinition.setEvent_session_name(object.getString("event_session_name"));
                        }
                        if (object.has("event_session_short_description")
                                && !TextUtils.isEmpty(object.getString("event_session_short_description"))
                                && !object.getString("event_session_short_description").equals("null")) {
                            eventDefinition.setEvent_session_short_description(object.getString("event_session_short_description"));
                        }
                        if (object.has("event_session_long_description")
                                && !TextUtils.isEmpty(object.getString("event_session_long_description"))
                                && !object.getString("event_session_long_description").equals("null")) {
                            eventDefinition.setEvent_session_long_description(object.getString("event_session_long_description"));
                        }
                        if (object.has("event_session_link_on_web")
                                && !TextUtils.isEmpty(object.getString("event_session_link_on_web"))
                                && !object.getString("event_session_link_on_web").equals("null")) {
                            eventDefinition.setEvent_session_link_on_web(object.getString("event_session_link_on_web"));
                        }
                        if (object.has("event_session_link_twitter")
                                && !TextUtils.isEmpty(object.getString("event_session_link_twitter"))
                                && !object.getString("event_session_link_twitter").equals("null")) {
                            eventDefinition.setEvent_session_link_twitter(object.getString("event_session_link_twitter"));
                        }
                        if (object.has("event_session_link_whatsapp")
                                && !TextUtils.isEmpty(object.getString("event_session_link_whatsapp"))
                                && !object.getString("event_session_link_whatsapp").equals("null")) {
                            eventDefinition.setEvent_session_link_whatsapp(object.getString("event_session_link_whatsapp"));
                        }
                        if (object.has("event_session_link_facebook")
                                && !TextUtils.isEmpty(object.getString("event_session_link_facebook"))
                                && !object.getString("event_session_link_facebook").equals("null")) {
                            eventDefinition.setEvent_session_link_facebook(object.getString("event_session_link_facebook"));
                        }
                        if (object.has("event_session_link_other_media")
                                && !TextUtils.isEmpty(object.getString("event_session_link_other_media"))
                                && !object.getString("event_session_link_other_media").equals("null")) {
                            eventDefinition.setEvent_session_link_other_media(object.getString("event_session_link_other_media"));
                        }
                        if (object.has("company_logo")
                                && !TextUtils.isEmpty(object.getString("company_logo"))
                                && !object.getString("company_logo").equals("null")) {
                            eventDefinition.setCompany_logo(object.getString("company_logo"));
                        }
                        if (object.has("event_logo")
                                && !TextUtils.isEmpty(object.getString("event_logo"))
                                && !object.getString("event_logo").equals("null")) {
                            eventDefinition.setEvent_logo(object.getString("event_logo"));
                        }
                        if (object.has("event_session_logo")
                                && !TextUtils.isEmpty(object.getString("event_session_logo"))
                                && !object.getString("event_session_logo").equals("null")) {
                            eventDefinition.setEvent_session_logo(object.getString("event_session_logo"));
                        }
                        if (object.has("event_session_image1")
                                && !TextUtils.isEmpty(object.getString("event_session_image1"))
                                && !object.getString("event_session_image1").equals("null")) {
                            eventDefinition.setEvent_session_image1(object.getString("event_session_image1"));
                        }
                        if (object.has("event_session_image2")
                                && !TextUtils.isEmpty(object.getString("event_session_image2"))
                                && !object.getString("event_session_image2").equals("null")) {
                            eventDefinition.setEvent_session_image2(object.getString("event_session_image2"));
                        }
                        if (object.has("event_session_blob_image")
                                && !TextUtils.isEmpty(object.getString("event_session_blob_image"))
                                && !object.getString("event_session_blob_image").equals("null")) {
                            eventDefinition.setEvent_session_blob_image(object.getString("event_session_blob_image"));
                        }
                        if (object.has("event_session_address")
                                && !TextUtils.isEmpty(object.getString("event_session_address"))
                                && !object.getString("event_session_address").equals("null")) {
                            eventDefinition.setEvent_session_address(object.getString("event_session_address"));
                        }
                        if (object.has("covered_locations")
                                && !TextUtils.isEmpty(object.getString("covered_locations"))
                                && !object.getString("covered_locations").equals("null")) {
                            eventDefinition.setCovered_locations(object.getString("covered_locations"));
                        }
                        if (object.has("session_regn_needed_approval")
                                && !TextUtils.isEmpty(object.getString("session_regn_needed_approval"))
                                && !object.getString("session_regn_needed_approval").equals("null")) {
                            eventDefinition.setSession_regn_needed_approval(object.getBoolean("session_regn_needed_approval"));
                        }
                        if (object.has("requirements")
                                && !TextUtils.isEmpty(object.getString("requirements"))
                                && !object.getString("requirements").equals("null")) {
                            eventDefinition.setRequirements(object.getString("requirements"));
                        }
                        if (object.has("appl_req_download")
                                && !TextUtils.isEmpty(object.getString("appl_req_download"))
                                && !object.getString("appl_req_download").equals("null")) {
                            eventDefinition.setAppl_req_download(object.getString("appl_req_download"));
                        }
                        if (object.has("cost")
                                && !TextUtils.isEmpty(object.getString("cost"))
                                && !object.getString("cost").equals("null")) {
                            eventDefinition.setCost(object.getString("cost"));
                        }
                        if (object.has("year")
                                && !TextUtils.isEmpty(object.getString("year"))
                                && !object.getString("year").equals("null")) {
                            eventDefinition.setYear(object.getString("year"));
                        }
                        if (object.has("location_address")
                                && !TextUtils.isEmpty(object.getString("location_address"))
                                && !object.getString("location_address").equals("null")) {
                            eventDefinition.setLocation_address(object.getString("location_address"));
                        }
                        if (object.has("scheme_type")
                                && !TextUtils.isEmpty(object.getString("scheme_type"))
                                && !object.getString("scheme_type").equals("null")) {
                            eventDefinition.setScheme_type(object.getString("scheme_type"));
                        }
                        if (object.has("event_prefix")
                                && !TextUtils.isEmpty(object.getString("event_prefix"))
                                && !object.getString("event_prefix").equals("null")) {
                            eventDefinition.setEvent_prefix(object.getString("event_prefix"));
                        }

                        if (object.has("event_session_prefix")
                                && !TextUtils.isEmpty(object.getString("event_session_prefix"))
                                && !object.getString("event_session_prefix").equals("null")) {
                            eventDefinition.setEvent_session_prefix(object.getString("event_session_prefix"));
                        }
                        if (object.has("event_nextnum")
                                && !TextUtils.isEmpty(object.getString("event_nextnum"))
                                && !object.getString("event_nextnum").equals("null")) {
                            eventDefinition.setEvent_nextnum(object.getInt("event_nextnum"));
                        }
                        if (object.has("event_session_nextnum")
                                && !TextUtils.isEmpty(object.getString("event_session_nextnum"))
                                && !object.getString("event_session_nextnum").equals("null")) {
                            eventDefinition.setEvent_session_nextnum(object.getInt("event_session_nextnum"));
                        }
                        if (object.has("local_time_event_session_start")
                                && !TextUtils.isEmpty(object.getString("local_time_event_session_start"))
                                && !object.getString("local_time_event_session_start").equals("null")) {
                            eventDefinition.setLocal_time_event_session_start(object.getString("local_time_event_session_start"));
                        }
                        if (object.has("local_timezone")
                                && !TextUtils.isEmpty(object.getString("local_timezone"))
                                && !object.getString("local_timezone").equals("null")) {
                            eventDefinition.setLocal_timezone(object.getString("local_timezone"));
                        }
                        if (object.has("event_session_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_session_begins_date_time"))
                                && !object.getString("event_session_begins_date_time").equals("null")) {
                            eventDefinition.setEvent_session_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_begins_date_time")));
                        }
                        if (object.has("event_session_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_session_ends_date_time"))
                                && !object.getString("event_session_ends_date_time").equals("null")) {
                            eventDefinition.setEvent_session_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_ends_date_time")));
                        }
                        if (object.has("event_session_parking_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_session_parking_begins_date_time"))
                                && !object.getString("event_session_parking_begins_date_time").equals("null")) {
                            eventDefinition.setEvent_session_parking_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_parking_begins_date_time")));
                        }
                        if (object.has("event_session_parking_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_session_parking_ends_date_time"))
                                && !object.getString("event_session_parking_ends_date_time").equals("null")) {
                            eventDefinition.setEvent_session_parking_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_parking_ends_date_time")));
                        }

                        if (object.has("event_session_multi_dates")
                                && !TextUtils.isEmpty(object.getString("event_session_multi_dates"))
                                && !object.getString("event_session_multi_dates").equals("null")) {
                            eventDefinition.setEvent_session_multi_dates(object.getString("event_session_multi_dates"));
                        }

                        if (object.has("event_session_parking_timings")
                                && !TextUtils.isEmpty(object.getString("event_session_parking_timings"))
                                && !object.getString("event_session_parking_timings").equals("null")) {
                            eventDefinition.setEvent_session_parking_timings(object.getString("event_session_parking_timings"));
                        }

                        if (object.has("event_session_timings")
                                && !TextUtils.isEmpty(object.getString("event_session_timings"))
                                && !object.getString("event_session_timings").equals("null")) {
                            eventDefinition.setEvent_session_timings(object.getString("event_session_timings"));
                        }

                        if (object.has("expires_by")
                                && !TextUtils.isEmpty(object.getString("expires_by"))
                                && !object.getString("expires_by").equals("null")) {
                            eventDefinition.setExpires_by(object.getString("expires_by"));
                        }
                        if (object.has("regn_reqd")
                                && !TextUtils.isEmpty(object.getString("regn_reqd"))
                                && !object.getString("regn_reqd").equals("null")) {
                            eventDefinition.setRegn_reqd(object.getBoolean("regn_reqd"));
                        }
                        if (object.has("event_session_regn_allowed")
                                && !TextUtils.isEmpty(object.getString("event_session_regn_allowed"))
                                && !object.getString("event_session_regn_allowed").equals("null")) {
                            eventDefinition.setEvent_session_regn_allowed(object.getBoolean("event_session_regn_allowed"));
                        }
                        if (object.has("event_session_regn_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_regn_fee"))
                                && !object.getString("event_session_regn_fee").equals("null")) {
                            eventDefinition.setEvent_session_regn_fee(object.getString("event_session_regn_fee"));
                        }
                        if (object.has("event_session_led_by")
                                && !TextUtils.isEmpty(object.getString("event_session_led_by"))
                                && !object.getString("event_session_led_by").equals("null")) {
                            eventDefinition.setEvent_session_led_by(object.getString("event_session_led_by"));
                        }
                        if (object.has("event_session_leaders_bio")
                                && !TextUtils.isEmpty(object.getString("event_session_leaders_bio"))
                                && !object.getString("event_session_leaders_bio").equals("null")) {
                            eventDefinition.setEvent_session_leaders_bio(object.getString("event_session_leaders_bio"));
                        }
                        if (object.has("regn_reqd_for_parking")
                                && !TextUtils.isEmpty(object.getString("regn_reqd_for_parking"))
                                && !object.getString("regn_reqd_for_parking").equals("null")) {
                            eventDefinition.setRegn_reqd_for_parking(object.getBoolean("regn_reqd_for_parking"));
                        }
                        if (object.has("renewable")
                                && !TextUtils.isEmpty(object.getString("renewable"))
                                && !object.getString("renewable").equals("null")) {
                            eventDefinition.setRenewable(object.getBoolean("renewable"));
                        }
                        if (object.has("active")
                                && !TextUtils.isEmpty(object.getString("active"))
                                && !object.getString("active").equals("null")) {
                            eventDefinition.setActive(object.getBoolean("active"));
                        }

                        if (object.has("event_session_parking_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_parking_fee"))
                                && !object.getString("event_session_parking_fee").equals("null")) {
                            eventDefinition.setEvent_session_parking_fee(object.getString("event_session_parking_fee"));
                        }

                        if (object.has("free_session")
                                && !TextUtils.isEmpty(object.getString("free_session"))
                                && !object.getString("free_session").equals("null")) {
                            eventDefinition.setFree_session(object.getBoolean("free_session"));
                        }

                        if (object.has("free_session_parking")
                                && !TextUtils.isEmpty(object.getString("free_session_parking"))
                                && !object.getString("free_session_parking").equals("null")) {
                            eventDefinition.setFree_session_parking(object.getBoolean("free_session_parking"));
                        }

                        if (object.has("event_session_regn_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_regn_fee"))
                                && !object.getString("event_session_regn_fee").equals("null")) {
                            eventDefinition.setEvent_session_regn_fee(object.getString("event_session_regn_fee"));
                        }

                        if (object.has("event_session_youth_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_youth_fee"))
                                && !object.getString("event_session_youth_fee").equals("null")) {
                            eventDefinition.setEvent_session_youth_fee(object.getString("event_session_youth_fee"));
                        }

                        if (object.has("event_session_child_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_child_fee"))
                                && !object.getString("event_session_child_fee").equals("null")) {
                            eventDefinition.setEvent_session_child_fee(object.getString("event_session_child_fee"));
                        }

                        if (object.has("event_session_student_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_student_fee"))
                                && !object.getString("event_session_student_fee").equals("null")) {
                            eventDefinition.setEvent_session_student_fee(object.getString("event_session_student_fee"));
                        }

                        if (object.has("event_session_minister_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_minister_fee"))
                                && !object.getString("event_session_minister_fee").equals("null")) {
                            eventDefinition.setEvent_session_minister_fee(object.getString("event_session_minister_fee"));
                        }

                        if (object.has("event_session_clergy_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_clergy_fee"))
                                && !object.getString("event_session_clergy_fee").equals("null")) {
                            eventDefinition.setEvent_session_clergy_fee(object.getString("event_session_clergy_fee"));
                        }

                        if (object.has("event_session_promo_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_promo_fee"))
                                && !object.getString("event_session_promo_fee").equals("null")) {
                            eventDefinition.setEvent_session_promo_fee(object.getString("event_session_promo_fee"));
                        }

                        if (object.has("event_session_senior_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_senior_fee"))
                                && !object.getString("event_session_senior_fee").equals("null")) {
                            eventDefinition.setEvent_session_senior_fee(object.getString("event_session_senior_fee"));
                        }

                        if (object.has("event_session_staff_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_staff_fee"))
                                && !object.getString("event_session_staff_fee").equals("null")) {
                            eventDefinition.setEvent_session_staff_fee(object.getString("event_session_staff_fee"));
                        }

                        if (object.has("event_session_family_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_family_fee"))
                                && !object.getString("event_session_family_fee").equals("null")) {
                            eventDefinition.setEvent_session_family_fee(object.getString("event_session_family_fee"));
                        }

                        if (object.has("event_session_link_ytube")
                                && !TextUtils.isEmpty(object.getString("event_session_link_ytube"))
                                && !object.getString("event_session_link_ytube").equals("null")) {
                            eventDefinition.setEvent_session_link_ytube(object.getString("event_session_link_ytube"));
                        }
                        if (object.has("event_session_link_zoom")
                                && !TextUtils.isEmpty(object.getString("event_session_link_zoom"))
                                && !object.getString("event_session_link_zoom").equals("null")) {
                            eventDefinition.setEvent_session_link_zoom(object.getString("event_session_link_zoom"));
                        }
                        if (object.has("event_session_link_googlemeet")
                                && !TextUtils.isEmpty(object.getString("event_session_link_googlemeet"))
                                && !object.getString("event_session_link_googlemeet").equals("null")) {
                            eventDefinition.setEvent_session_link_googlemeet(object.getString("event_session_link_googlemeet"));
                        }
                        if (object.has("event_session_link_googleclassroom")
                                && !TextUtils.isEmpty(object.getString("event_session_link_googleclassroom"))
                                && !object.getString("event_session_link_googleclassroom").equals("null")) {
                            eventDefinition.setEvent_session_link_googleclassroom(object.getString("event_session_link_googleclassroom"));
                        }
                        if (object.has("event_session_regn_limit")
                                && !TextUtils.isEmpty(object.getString("event_session_regn_limit"))
                                && !object.getString("event_session_regn_limit").equals("null")) {
                            eventDefinition.setEvent_session_regn_limit(object.getString("event_session_regn_limit"));
                        }
                        if (object.has("web_event_session_regn_fee")
                                && !TextUtils.isEmpty(object.getString("web_event_session_regn_fee"))
                                && !object.getString("web_event_session_regn_fee").equals("null")) {
                            eventDefinition.setWeb_event_session_regn_fee(object.getString("web_event_session_regn_fee"));
                        }
                        if (object.has("web_event_session_regn_limit")
                                && !TextUtils.isEmpty(object.getString("web_event_session_regn_limit"))
                                && !object.getString("web_event_session_regn_limit").equals("null")) {
                            eventDefinition.setWeb_event_session_regn_limit(object.getString("web_event_session_regn_limit"));
                        }
                        if (object.has("web_event_session_location_to_show")
                                && !TextUtils.isEmpty(object.getString("web_event_session_location_to_show"))
                                && !object.getString("web_event_session_location_to_show").equals("null")) {
                            eventDefinition.setWeb_event_session_location_to_show(object.getBoolean("web_event_session_location_to_show"));
                        }
                        if (object.has("free_web_session")
                                && !TextUtils.isEmpty(object.getString("free_web_session"))
                                && !object.getString("free_web_session").equals("null")) {
                            eventDefinition.setFree_web_session(object.getBoolean("free_web_session"));
                        }
                        if (object.has("event_session_logi_type")
                                && !TextUtils.isEmpty(object.getString("event_session_logi_type"))
                                && !object.getString("event_session_logi_type").equals("null")) {
                            eventDefinition.setEvent_session_logi_type(object.getInt("event_session_logi_type"));
                        }

                        if (object.has("free_local_session")
                                && !TextUtils.isEmpty(object.getString("free_local_session"))
                                && !object.getString("free_local_session").equals("null")) {
                            eventDefinition.setFree_local_session(object.getBoolean("free_local_session"));
                        }

                        if (object.has("reqd_web_session_regn")
                                && !TextUtils.isEmpty(object.getString("reqd_web_session_regn"))
                                && !object.getString("reqd_web_session_regn").equals("null")) {
                            eventDefinition.setReqd_web_session_regn(object.getBoolean("reqd_web_session_regn"));
                        }

                        if (object.has("reqd_local_session_regn")
                                && !TextUtils.isEmpty(object.getString("reqd_local_session_regn"))
                                && !object.getString("reqd_local_session_regn").equals("null")) {
                            eventDefinition.setReqd_local_session_regn(object.getBoolean("reqd_local_session_regn"));
                        }

                        if (object.has("is_parking_allowed")
                                && !TextUtils.isEmpty(object.getString("is_parking_allowed"))
                                && !object.getString("is_parking_allowed").equals("null")) {
                            eventDefinition.setIs_parking_allowed(object.getBoolean("is_parking_allowed"));
                        }

                        if (object.has("session_link_array")
                                && !TextUtils.isEmpty(object.getString("session_link_array"))
                                && !object.getString("session_link_array").equals("null")) {
                            eventDefinition.setSession_link_array(object.getString("session_link_array"));
                        }

                        if (object.has("parking_reservation_limit")
                                && !TextUtils.isEmpty(object.getString("parking_reservation_limit"))
                                && !object.getString("parking_reservation_limit").equals("null")) {
                            eventDefinition.setParking_reservation_limit(Integer.parseInt(object.getString("parking_reservation_limit")));
                        }

                        eventSessions.add(eventDefinition);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                binding.swipeRefreshLayout.setRefreshing(false);
                if (eventSessions.size() != 0) {
                    binding.tvError.setVisibility(View.GONE);
                    sessionAdapter.notifyDataSetChanged();
                } else {
                    binding.tvError.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {

        binding.swipeRefreshLayout.setOnRefreshListener(() -> {
            sessionAdapter.notifyDataSetChanged();
            eventSessions.clear();
            new fetchSessions().execute();
        });

        binding.tvBtnAddEventSession.setOnClickListener(v -> {
            showAddEditEventOptionTypePupup(false, EVENT_TYPE_AT_LOCATION);
            /*EventOptionTypeDialog dialog = EventOptionTypeDialog.newInstance();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isSession", true);
            bundle.putInt("eventLogiType", EVENT_TYPE_AT_LOCATION);
            dialog.setArguments(bundle);
            dialog.show(getChildFragmentManager(), "payment");*/
           /* final FragmentTransaction ft = getFragmentManager().beginTransaction();
            EventOptionTypeDialog frag = new EventOptionTypeDialog();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isEdit", false);
            bundle.putBoolean("isSession", true);
            bundle.putInt("eventLogiType", EVENT_TYPE_AT_LOCATION);
            bundle.putString("eventDetails", new Gson().toJson(eventDefinition));
            frag.setArguments(bundle);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, frag);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(frag);
            ft.commitAllowingStateLoss();*/
        });
    }

    @Override
    protected void initViews(View v) {

        sessionAdapter = new EventSessionAdapter();
        binding.rvSession.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvSession.setAdapter(sessionAdapter);


    }

    int selectedEventSessionType;

    private void showAddEditEventOptionTypePupup(boolean isEdit, int eventLogiType) {
        PopupEventOptionTypeBinding binding;
        PopupWindow popupmanaged;

        LinearLayout viewGroup = (LinearLayout) getActivity().findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.popup_event_option_type, viewGroup, false);
        View layout = binding.getRoot();

        popupmanaged = new PopupWindow(getActivity());
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);

        selectedEventSessionType = eventLogiType;
        Typeface typeface = binding.tvLocal.getTypeface();
        if (eventLogiType == EVENT_TYPE_AT_LOCATION) {
            binding.tvLocal.setTypeface(typeface, Typeface.BOLD);
            binding.llLocal.setBackgroundResource(R.drawable.highlight);
            binding.tvLocal.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_700));
        } else if (eventLogiType == EVENT_TYPE_AT_VIRTUALLY) {
            binding.llOnline.setBackgroundResource(R.drawable.highlight);
            binding.tvOnline.setTypeface(typeface, Typeface.BOLD);
            binding.tvOnline.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_700));
        } else if (eventLogiType == EVENT_TYPE_BOTH) {
            binding.llBoth.setBackgroundResource(R.drawable.highlight);
            binding.tvBoth.setTypeface(typeface, Typeface.BOLD);
            binding.tvBoth.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_700));
        }

        binding.llLocal.setOnClickListener(v -> {
            selectedEventSessionType = EVENT_TYPE_AT_LOCATION;
            binding.tvLocal.setTypeface(typeface, Typeface.BOLD);
            binding.tvOnline.setTypeface(typeface, Typeface.NORMAL);
            binding.tvBoth.setTypeface(typeface, Typeface.NORMAL);
            binding.tvLocal.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_700));
            binding.tvOnline.setTextColor(ContextCompat.getColor(getContext(), R.color.itemType));
            binding.tvBoth.setTextColor(ContextCompat.getColor(getContext(), R.color.itemType));
            binding.llLocal.setBackgroundResource(R.drawable.highlight);
            binding.llOnline.setBackgroundResource(0);
            binding.llBoth.setBackgroundResource(0);
        });

        binding.llOnline.setOnClickListener(v -> {
            selectedEventSessionType = EVENT_TYPE_AT_VIRTUALLY;
            binding.tvLocal.setTypeface(typeface, Typeface.NORMAL);
            binding.tvOnline.setTypeface(typeface, Typeface.BOLD);
            binding.tvBoth.setTypeface(typeface, Typeface.NORMAL);
            binding.tvLocal.setTextColor(ContextCompat.getColor(getContext(), R.color.itemType));
            binding.tvOnline.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_700));
            binding.tvBoth.setTextColor(ContextCompat.getColor(getContext(), R.color.itemType));
            binding.llLocal.setBackgroundResource(0);
            binding.llOnline.setBackgroundResource(R.drawable.highlight);
            binding.llBoth.setBackgroundResource(0);
        });

        binding.llBoth.setOnClickListener(v -> {
            selectedEventSessionType = EVENT_TYPE_BOTH;
            binding.tvLocal.setTypeface(typeface, Typeface.NORMAL);
            binding.tvOnline.setTypeface(typeface, Typeface.NORMAL);
            binding.tvBoth.setTypeface(typeface, Typeface.BOLD);
            binding.tvLocal.setTextColor(ContextCompat.getColor(getContext(), R.color.itemType));
            binding.tvOnline.setTextColor(ContextCompat.getColor(getContext(), R.color.itemType));
            binding.tvBoth.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_700));
            binding.llLocal.setBackgroundResource(0);
            binding.llOnline.setBackgroundResource(0);
            binding.llBoth.setBackgroundResource(R.drawable.highlight);
        });

        binding.ivRight.setOnClickListener(v -> {
            Log.d("#DEBUG", "AllEventsFrag " + selectedEventSessionType);
            popupmanaged.dismiss();
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            SessionSettingsAddEditFragment frag = new SessionSettingsAddEditFragment();
            Bundle bundle = new Bundle();

            bundle.putBoolean("isSession", true);
            bundle.putBoolean("isEdit", isEdit);
            bundle.putInt("eventLogiType", selectedEventSessionType);
            if (isEdit) {
                EventSession eventSession = eventSessions.get(selectedItemPosition);
                eventSession.setEvent_session_logi_type(selectedEventSessionType);
                bundle.putString("eventSessionDetails", new Gson().toJson(eventSession));
            } else
                bundle.putString("eventDetails", new Gson().toJson(eventDefinition));
            frag.setArguments(bundle);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, frag);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(frag);
            ft.commitAllowingStateLoss();
        });

        binding.ivClose.setOnClickListener(v -> popupmanaged.dismiss());
    }

    private class EventSessionAdapter extends RecyclerView.Adapter<EventSessionAdapter.EventSessionHolder> {

        @NonNull
        @Override
        public EventSessionAdapter.EventSessionHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new EventSessionHolder(LayoutInflater.from(getActivity())
                    .inflate(R.layout.item_event_session_new, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull EventSessionAdapter.EventSessionHolder eventSessionHolder, int i) {
            EventSession eventSession = eventSessions.get(i);
            if (eventSession != null) {
                Glide.with(getActivity()).load(eventSession.getEvent_session_logo()).placeholder(R.drawable.place_holder)
                        .error(R.drawable.place_holder).into(eventSessionHolder.ivLogo);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    eventSessionHolder.ivLogo.setClipToOutline(true);
                }

                if (eventSession.getEvent_session_name() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_name())) {
                    eventSessionHolder.tvEventTitle.setText(eventSession.getEvent_session_name());
                }
                if (eventSession.getEvent_session_type() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_type())) {
                    eventSessionHolder.tvEventType.setText(eventSession.getEvent_session_type());
                }
                if (eventSession.getCost() != null
                        && !TextUtils.isEmpty(eventSession.getCost())) {
                    if (eventSession.getCost().equals("0")) {
                        eventSessionHolder.tvCost.setText("FREE");
                    } else {
                        eventSessionHolder.tvCost.setText(String.format("$%s", eventSession.getCost()));
                    }
                } else {
                    eventSessionHolder.tvCost.setText("FREE");
                }

                if (!TextUtils.isEmpty(eventSession.getEvent_session_address())) {
                    eventSessionHolder.tvAddress.setText(eventSession.getEvent_session_address());
                    try {
                        JSONArray jsonArray = new JSONArray(eventSession.getEvent_session_address());
                        if (jsonArray.length() > 0) {
                            JSONArray jsonArray1 = jsonArray.getJSONArray(0);
                            if (jsonArray1.length() > 0) {
                                eventSessionHolder.tvAddress.setText(jsonArray1.getString(0));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (eventSession.getEvent_session_begins_date_time() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_begins_date_time())) {
                    eventSessionHolder.tvEventDate.setText(DateTimeUtils.getDDMMM(eventSession.getEvent_session_begins_date_time()));
                }
                if (eventSession.getEvent_session_led_by() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_led_by())) {
                    eventSessionHolder.llLeadBy.setVisibility(View.VISIBLE);
                    eventSessionHolder.tvLeadBy.setText(eventSession.getEvent_session_led_by());
                } else {
                    eventSessionHolder.llLeadBy.setVisibility(View.GONE);
                }

                if (eventSession.getEvent_session_timings() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_timings())) {
                    try {
                        JSONArray jsonArrayTime = new JSONArray(eventSession.getEvent_session_timings());
                        StringBuilder stringBuilder = new StringBuilder();
                        if (jsonArrayTime != null && jsonArrayTime.length() > 0) {
                            JSONArray jsonArray2 = jsonArrayTime.getJSONArray(0);
                            if (jsonArray2 != null && jsonArray2.length() > 0) {
                                if (jsonArray2.get(0).toString().length() > 30) {
                                    String[] s = jsonArray2.get(0).toString().split(" - ");
                                    for (int j = 0; j < s.length; j++) {
                                        if (j != 0) {
                                            stringBuilder.append("-\n");
                                        }
                                        stringBuilder.append(DateTimeUtils.gmtToLocalDateAMPM(s[j]).substring(11));
                                    }
                                } else {
                                    stringBuilder.append(jsonArray2.get(0).toString());
                                }
                            }
                        }
                        eventSessionHolder.tvSessionTime.setText(stringBuilder.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        @Override
        public int getItemCount() {
            return eventSessions.size();
        }

        class EventSessionHolder extends RecyclerView.ViewHolder {
            TextView tvEventTitle, tvEventType;
            TextView tvBtnEditEvent, tvAddress, tvCost, tvEventDate, tvSessionTime, tvLeadBy;
            ImageView ivLogo;
            LinearLayout llLeadBy;

            EventSessionHolder(@NonNull View itemView) {
                super(itemView);
                tvEventTitle = itemView.findViewById(R.id.tvEventTitle);
                tvEventType = itemView.findViewById(R.id.tvEventType);
                tvBtnEditEvent = itemView.findViewById(R.id.tvBtnEditEvent);
                tvAddress = itemView.findViewById(R.id.tvAddress);
                tvCost = itemView.findViewById(R.id.tvCost);
                tvEventDate = itemView.findViewById(R.id.tvEventDate);
                tvSessionTime = itemView.findViewById(R.id.tvSessionTime);
                tvLeadBy = itemView.findViewById(R.id.tvLeadBy);
                ivLogo = itemView.findViewById(R.id.ivLogo);
                llLeadBy = itemView.findViewById(R.id.llLeadBy);

                tvBtnEditEvent.setOnClickListener(v -> {
                    final int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        selectedItemPosition = getAdapterPosition();
                        showAddEditEventOptionTypePupup(true, eventSessions.get(selectedItemPosition).getEvent_session_logi_type());
/*
                        EventOptionTypeDialog dialog = EventOptionTypeDialog.newInstance();
                        Bundle bundle = new Bundle();
                        bundle.putInt("eventLogiType", eventSessions.get(pos).getEvent_session_logi_type());
                        bundle.putBoolean("isEdit", true);
                        bundle.putBoolean("isSession", true);
                        dialog.setArguments(bundle);
                        dialog.show(getChildFragmentManager(), "payment");
*/
                        /*final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        EventOptionTypeDialog frag = new EventOptionTypeDialog();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("isEdit", true);
                        bundle.putBoolean("isSession", true);
                        bundle.putInt("eventLogiType", eventSessions.get(pos).getEvent_session_logi_type());
                        EventSession eventSession = eventSessions.get(pos);
                        bundle.putString("eventSessionDetails", new Gson().toJson(eventSession));
                        frag.setArguments(bundle);
                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.add(R.id.My_Container_1_ID, frag);
                        fragmentStack.lastElement().onPause();
                        ft.hide(fragmentStack.lastElement());
                        fragmentStack.push(frag);
                        ft.commitAllowingStateLoss();*/
                    }
                });

                itemView.setOnClickListener(v -> {
                    final int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        EventSessionInfoViewFragment frag = new EventSessionInfoViewFragment();
                        Bundle bundle = new Bundle();
                        bundle.putInt("eventSessionId", eventSessions.get(pos).getId());
                        frag.setArguments(bundle);
                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.add(R.id.My_Container_1_ID, frag);
                        fragmentStack.lastElement().onPause();
                        ft.hide(fragmentStack.lastElement());
                        fragmentStack.push(frag);
                        ft.commitAllowingStateLoss();
                    }
                });
            }
        }
    }
}
