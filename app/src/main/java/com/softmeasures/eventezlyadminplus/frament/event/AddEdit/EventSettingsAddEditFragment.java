package com.softmeasures.eventezlyadminplus.frament.event.AddEdit;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragEventSettingsAddEditBinding;
import com.softmeasures.eventezlyadminplus.models.APPConfigAnnounce;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.Manager;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.frament.event.AllEventsFragment.isUpdated;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_LOCATION;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_VIRTUALLY;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_BOTH;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class EventSettingsAddEditFragment extends BaseFragment {

    public FragEventSettingsAddEditBinding binding;
    public final String TAG = "#DEBUG EventSett";

    private boolean isEdit = false, isRepeat = false;
    private int eventLogiType = EVENT_TYPE_AT_LOCATION;
    //    private Manager selectedManager;
    private EventDefinition eventDefinition;
    private String freeLocalEventText, freeLocalEventLink, freeOnlineEventText, freeOnlineEventLink, freeParkingText, freeParkingLink;
    private int freeLocalEvent, freeOnlineEvent, freeParking;
    public boolean isFree = false;
    private ArrayList<APPConfigAnnounce> appConfigAnnouncesList = new ArrayList<>();
    public static final String MIME_TYPE_CONTACT = "vnd.android.cursor.item/vnd.example.contact"; //copy Paste EditText

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            isEdit = getArguments().getBoolean("isEdit", false);
            isRepeat = getArguments().getBoolean("isRepeat", false);
            eventLogiType = getArguments().getInt("eventLogiType", EVENT_TYPE_AT_LOCATION);
//            selectedManager = new Gson().fromJson(getArguments().getString("selectedManager"), Manager.class);
            if (getArguments().containsKey("eventDetails")) {
                eventDefinition = new Gson().fromJson(getArguments().getString("eventDetails"), EventDefinition.class);
                eventLogiType = eventDefinition.getEvent_logi_type();
            } else {
                eventDefinition = new EventDefinition();
                eventDefinition.setEvent_logi_type(eventLogiType);
                eventDefinition.setIs_parking_allowed(true);
                eventDefinition.setActive(true);
                eventDefinition.setFree_event(true);
                eventDefinition.setFree_local_event(true);
                eventDefinition.setFree_web_event(true);
                eventDefinition.setFree_event_parking(true);
                eventDefinition.setEvent_public(true);
            }
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_event_settings_add_edit, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void initViews(View v) {
        new fetchFreeEventAlertTaxt().execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isUpdated)
            if (getActivity() != null) getActivity().onBackPressed();
    }

    @Override
    protected void updateViews() {
        if (eventLogiType == EVENT_TYPE_AT_LOCATION) {
            //Web
            binding.llWebRegReq.setVisibility(View.GONE);
            binding.llWebEventFree.setVisibility(View.GONE);
            binding.llVirtualRegLimit.setVisibility(View.GONE);

            //Local
            binding.llParkingAllowed.setVisibility(View.VISIBLE);
            binding.llLocalRegReq.setVisibility(View.VISIBLE);
            binding.llParkingRegistration.setVisibility(View.VISIBLE);
            binding.llLocalEventFree.setVisibility(View.VISIBLE);
            binding.llParkingFree.setVisibility(View.VISIBLE);
            binding.llLocalRegLimit.setVisibility(View.VISIBLE);
            binding.llParkingLimit.setVisibility(View.VISIBLE);

        } else if (eventLogiType == EVENT_TYPE_AT_VIRTUALLY) {
            //Web
            binding.llWebRegReq.setVisibility(View.VISIBLE);
            binding.llWebEventFree.setVisibility(View.VISIBLE);
            binding.llVirtualRegLimit.setVisibility(View.VISIBLE);

            //Local
            binding.llParkingAllowed.setVisibility(View.GONE);
            binding.llLocalRegReq.setVisibility(View.GONE);
            binding.llParkingRegistration.setVisibility(View.GONE);
            binding.llLocalEventFree.setVisibility(View.GONE);
            binding.llParkingFree.setVisibility(View.GONE);
            binding.llLocalRegLimit.setVisibility(View.GONE);
            binding.llParkingLimit.setVisibility(View.GONE);

        } else if (eventLogiType == EVENT_TYPE_BOTH) {
            //Web
            binding.llWebRegReq.setVisibility(View.VISIBLE);
            binding.llWebEventFree.setVisibility(View.VISIBLE);
            binding.llVirtualRegLimit.setVisibility(View.VISIBLE);

            //Local
            binding.llParkingAllowed.setVisibility(View.VISIBLE);
            binding.llLocalRegReq.setVisibility(View.VISIBLE);
            binding.llParkingRegistration.setVisibility(View.VISIBLE);
            binding.llLocalEventFree.setVisibility(View.VISIBLE);
            binding.llParkingFree.setVisibility(View.VISIBLE);
            binding.llLocalRegLimit.setVisibility(View.VISIBLE);
            binding.llParkingLimit.setVisibility(View.VISIBLE);
        }

        if (isEdit && eventDefinition != null) {
            binding.switchParkingAllowed.setChecked(eventDefinition.isParking_allowed());
            binding.switchLocalEventRegReq.setChecked(eventDefinition.isReqd_local_event_regn());
            binding.switchWebEventRegReq.setChecked(eventDefinition.isReqd_web_event_regn());
            binding.switchRegReqParking.setChecked(eventDefinition.isRegn_reqd_for_parking());
//            binding.switchRenewable.setChecked(eventDefinition.isRenewable());
            binding.switchActive.setChecked(eventDefinition.isActive());
            binding.switchRegUserInfoReq.setChecked(eventDefinition.isRegn_user_info_reqd());
            binding.switchRegAdditionalUserInfoReq.setChecked(eventDefinition.isRegn_addl_user_info_reqd());
            binding.switchEventMultiSession.setChecked(eventDefinition.isEvent_multi_sessions());
            binding.switchEventIndivSessionRegAllowed.setChecked(eventDefinition.isEvent_indiv_session_regn_allowed());
            binding.switchEventIndivSessionRegRequired.setChecked(eventDefinition.isEvent_indiv_session_regn_required());
            binding.switchEventFullRegRequired.setChecked(eventDefinition.isEvent_full_regn_required());
            binding.switchFreeLocalEvent.setChecked(eventDefinition.isFree_local_event());
            binding.switchFreeWebEvent.setChecked(eventDefinition.isFree_web_event());
            binding.switchFreeEventParking.setChecked(eventDefinition.isFree_event_parking());

            binding.switchEventPublic.setChecked(eventDefinition.isEvent_public());
            binding.switchEventUnlisted.setChecked(eventDefinition.isEvent_unlisted());
            binding.switchEventShare.setChecked(eventDefinition.isEvent_tp_sharable());

            if (!TextUtils.isEmpty(eventDefinition.getEvent_regn_limit()))
                binding.etEventRegLimit.setText(eventDefinition.getEvent_regn_limit());
            if (!TextUtils.isEmpty(eventDefinition.getWeb_event_regn_limit()))
                binding.etWebEventRegLimit.setText(eventDefinition.getWeb_event_regn_limit());
            if (!eventDefinition.isParking_allowed())
                binding.llParkingLimit.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(eventDefinition.getParking_reservation_limit()))
                binding.etParkingReservationLimit.setText(eventDefinition.getParking_reservation_limit());
        }
    }

    @Override
    protected void setListeners() {
        binding.tvBtnNext.setOnClickListener(v -> {
            if (validate()) {
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment fragment = new EventSettingsAdvancedAddEditFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("isEdit", isEdit);
                bundle.putBoolean("isRepeat", isRepeat);
                bundle.putString("eventDefinition", new Gson().toJson(eventDefinition));
                Log.d("parkingreserved",""+eventDefinition.getParking_reservation_limit());
                Log.d("parkingreserved",""+eventDefinition.getWeb_event_regn_limit());

                fragment.setArguments(bundle);
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.add(R.id.My_Container_1_ID, fragment);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(fragment);
                ft.commitAllowingStateLoss();
            }
        });

        //Allowed Parking?
        binding.switchParkingAllowed.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setIs_parking_allowed(isChecked);
            if (isChecked) {
                binding.llParkingRegistration.setVisibility(View.VISIBLE);
                binding.llParkingFree.setVisibility(View.VISIBLE);
                binding.llParkingLimit.setVisibility(View.VISIBLE);
            } else {
                binding.llParkingRegistration.setVisibility(View.GONE);
                binding.llParkingFree.setVisibility(View.GONE);
                binding.llParkingLimit.setVisibility(View.GONE);
            }
        });

        //Registration Required for Local Event?
        binding.switchLocalEventRegReq.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setReqd_local_event_regn(isChecked);
        });

        //Registration Required for Web Event?
        binding.switchWebEventRegReq.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setReqd_web_event_regn(isChecked);
        });

        //Registration Required for Parking?
        binding.switchRegReqParking.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setRegn_reqd_for_parking(isChecked);
        });

        /*//Is Renewable?
        binding.switchRenewable.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setRenewable(isChecked);
        });*/

        //Is Active?
        binding.switchActive.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setActive(isChecked);
        });

        //Required User Info?
        binding.switchRegUserInfoReq.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setRegn_user_info_reqd(isChecked);
        });

        //Required Additional User Info
        binding.switchRegAdditionalUserInfoReq.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setRegn_addl_user_info_reqd(isChecked);
        });

        //Is Multi Session?
        binding.switchEventMultiSession.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                eventDefinition.setEvent_multi_sessions(true);
                binding.llMultiSessionOptions.setVisibility(View.VISIBLE);
            } else {
                eventDefinition.setEvent_multi_sessions(false);
                binding.llMultiSessionOptions.setVisibility(View.GONE);
            }
        });

        //Registration Allowed for Individual Session?
        binding.switchEventIndivSessionRegAllowed.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setEvent_indiv_session_regn_allowed(isChecked);
        });

        //Full Registration Required for Event?
        binding.switchEventFullRegRequired.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setEvent_full_regn_required(isChecked);
        });

        //Is Local Event Free?
        binding.switchFreeLocalEvent.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setFree_local_event(isChecked);
        });

        //Is Web Event Free?
        binding.switchFreeWebEvent.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setFree_web_event(isChecked);
        });

        //Is Parking Free?
        binding.switchFreeEventParking.setOnCheckedChangeListener((buttonView, isChecked) -> {
            eventDefinition.setFree_event_parking(isChecked);
        });

        binding.etEventRegLimit.setOnClickListener(v -> {
            binding.etEventRegLimit.setError(null);
            showEnter_txt("Local Event Registration Limit", "Enter Value", binding.etEventRegLimit, "etEventRegLimit");
        });
        binding.etWebEventRegLimit.setOnClickListener(v -> {
            binding.etWebEventRegLimit.setError(null);
            showEnter_txt("Online Event Registration Limit", "Enter Value", binding.etWebEventRegLimit, "etWebEventRegLimit");
        });
        binding.etParkingReservationLimit.setOnClickListener(v -> {
            binding.etParkingReservationLimit.setError(null);
            showEnter_txt("Parking Reservation Limit", "Enter Value", binding.etParkingReservationLimit, "etParkingReservationLimit");
        });
    }

    private boolean validate() {
      /*  if(!isFree)
            showDialog(freeLocalEventText, freeLocalEventLink);*/
        if (eventLogiType == EVENT_TYPE_AT_LOCATION) {
            eventDefinition.setFree_event(eventDefinition.isFree_local_event());
            if (TextUtils.isEmpty(binding.etEventRegLimit.getText())) {
                binding.etEventRegLimit.setError("Required!");
                binding.etEventRegLimit.requestFocus();
                return false;
            } else
                eventDefinition.setEvent_regn_limit(binding.etEventRegLimit.getText().toString());

            if (eventDefinition.isParking_allowed()) {
                if (TextUtils.isEmpty(binding.etParkingReservationLimit.getText())) {
                    binding.etParkingReservationLimit.setError("Required!");
                    binding.etParkingReservationLimit.requestFocus();
                    return false;
                } else
                    eventDefinition.setParking_reservation_limit(binding.etParkingReservationLimit.getText().toString());
            }
        } else if (eventLogiType == EVENT_TYPE_AT_VIRTUALLY) {
            eventDefinition.setFree_event(eventDefinition.isFree_web_event());
            if (TextUtils.isEmpty(binding.etWebEventRegLimit.getText())) {
                binding.etWebEventRegLimit.setError("Required!");
                binding.etWebEventRegLimit.requestFocus();
                return false;
            } else
                eventDefinition.setWeb_event_regn_limit(binding.etWebEventRegLimit.getText().toString());
        } else if (eventLogiType == EVENT_TYPE_BOTH) {
            eventDefinition.setFree_event(eventDefinition.isFree_web_event() && eventDefinition.isFree_local_event());
            if (TextUtils.isEmpty(binding.etEventRegLimit.getText())) {
                binding.etEventRegLimit.setError("Required!");
                binding.etEventRegLimit.requestFocus();
                return false;
            } else eventDefinition.setEvent_regn_limit(binding.etEventRegLimit.getText().toString());
            if (TextUtils.isEmpty(binding.etWebEventRegLimit.getText())) {
                binding.etWebEventRegLimit.setError("Required!");
                binding.etWebEventRegLimit.requestFocus();
                return false;
            } else eventDefinition.setWeb_event_regn_limit(binding.etWebEventRegLimit.getText().toString());
            if (eventDefinition.isParking_allowed()) {
                if (TextUtils.isEmpty(binding.etParkingReservationLimit.getText())) {
                    binding.etParkingReservationLimit.setError("Required!");
                    binding.etParkingReservationLimit.requestFocus();
                    return false;
                } else eventDefinition.setParking_reservation_limit(binding.etParkingReservationLimit.getText().toString());
            }
        }

        if (!isEdit) {
            eventDefinition.setManager_type(myApp.selectedManager.getManager_type());
            eventDefinition.setManager_type_id(myApp.selectedManager.getManager_type_id());
            eventDefinition.setTwp_id(myApp.selectedManager.getId());
            eventDefinition.setTownship_code(myApp.selectedManager.getManager_id());
            eventDefinition.setTownship_name(myApp.selectedManager.getLot_manager());
            eventDefinition.setCompany_id(myApp.selectedManager.getId());
            eventDefinition.setCompany_code(myApp.selectedManager.getManager_id());
            eventDefinition.setCompany_name(myApp.selectedManager.getLot_manager());
            eventDefinition.setCompany_logo(myApp.selectedManager.getTownship_logo());
        }
        eventDefinition.setEvent_logi_type(eventLogiType);
        return true;
    }

    private void showEnter_txt(String title, String hintText, TextView etTextView, String selectedEditText) {
        RelativeLayout viewGroup = (RelativeLayout) getActivity().findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.enter_txt, viewGroup, false);
        final PopupWindow popup = new PopupWindow(getActivity());
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(true);
        popup.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        popup.setFocusable(true);
        popup.update();
        popup.showAtLocation(layout, Gravity.TOP, 0, 0);

        final EditText edit_text;
        final TextView text_title, tvPaste;
        final Button btn_done;
        RelativeLayout btn_cancel;
        LinearLayout rl_edit;
        ImageView ivPaste;

        tvPaste = (TextView) layout.findViewById(R.id.tvPaste);
        ivPaste = (ImageView) layout.findViewById(R.id.ivPaste);
        edit_text = (EditText) layout.findViewById(R.id.edit_nu);
        text_title = (TextView) layout.findViewById(R.id.txt_title);
        text_title.setText(title);
        edit_text.setInputType(InputType.TYPE_CLASS_NUMBER);
        edit_text.setHint(hintText);
        edit_text.setSelection(edit_text.getText().length());

        if (!TextUtils.isEmpty(etTextView.getText()))
            edit_text.setText(etTextView.getText());
        edit_text.setSelection(edit_text.getText().length());

        edit_text.setFocusable(true);
        btn_cancel = (RelativeLayout) layout.findViewById(R.id.close);
        btn_done = (Button) layout.findViewById(R.id.btn_done);
        rl_edit = (LinearLayout) layout.findViewById(R.id.rl_edit);

        edit_text.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        edit_text.setSelection(edit_text.getText().length());
        edit_text.setOnClickListener(v -> {
//            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            tvPaste.setVisibility(View.GONE);
        });
        rl_edit.setOnClickListener(v -> tvPaste.setVisibility(View.GONE));
        edit_text.setOnLongClickListener(v -> {
            tvPaste.setVisibility(View.VISIBLE);
            return true;
        });
        tvPaste.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData pData = clipboard.getPrimaryClip();
            if (pData != null) {
                ClipData.Item item = pData.getItemAt(0);
                String txtpaste = item.getText().toString();
                edit_text.getText().insert(edit_text.getSelectionStart(), txtpaste);
                edit_text.setText(edit_text.getText()+txtpaste);
            }
        });
        ivPaste.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData pData = clipboard.getPrimaryClip();
            try {
                ClipData.Item item = pData.getItemAt(0);
                int a =item.getText().length();
                String txtpaste = item.getText().toString();
                Log.d("check_paste","=="+txtpaste + "size"+a);
                edit_text.getText().insert(edit_text.getSelectionStart(), txtpaste);
                edit_text.setText(edit_text.getText()+txtpaste);
            }catch (Exception e)
            {
                Log.d("errdddd","===="+e.toString());
                //Toast.makeText(myApp, "Clipdata is empty", Toast.LENGTH_SHORT).show();
            }

        });
        btn_done.setOnClickListener(v -> {
            imm.hideSoftInputFromWindow(edit_text.getWindowToken(), 0);
            etTextView.setText(edit_text.getText().toString());
            popup.dismiss();
            if (eventLogiType == EVENT_TYPE_AT_LOCATION) {
                if (selectedEditText.equals("etEventRegLimit"))
                    showEnter_txt("Parking Reservation Limit", "Enter Value", binding.etParkingReservationLimit, "etParkingReservationLimit");
            }
            if (eventLogiType == EVENT_TYPE_BOTH) {
                if (selectedEditText.equals("etEventRegLimit"))
                    showEnter_txt("Online Event Registration Limit", "Enter Value", binding.etWebEventRegLimit, "etWebEventRegLimit");
                if (selectedEditText.equals("etWebEventRegLimit"))
                    showEnter_txt("Parking Reservation Limit", "Enter Value", binding.etParkingReservationLimit, "etParkingReservationLimit");
            }
        });
        btn_cancel.setOnClickListener(v -> {
            InputMethodManager imm1 = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm1.hideSoftInputFromWindow(edit_text.getWindowToken(), 0);
            String txt = edit_text.getText().toString();
            popup.dismiss();
        });
    }

    private class fetchFreeEventAlertTaxt extends AsyncTask<String, String, String> {
        JSONObject json;
        String apiUrl = "_table/app_config_announce";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            binding.rlProgressbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + apiUrl;
            Log.e(TAG, "      fetchFreeEventTaxt:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");
                    for (int j = 0; j < jsonArray.length(); j++) {
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("app_id")
                                && !TextUtils.isEmpty(object.getString("app_id"))
                                && !object.getString("app_id").equals("null") && object.getInt("app_id") == 23) {
                            APPConfigAnnounce announce = new APPConfigAnnounce();
                            announce.setAppId(object.getInt("app_id"));
                            if (object.has("id")
                                    && !TextUtils.isEmpty(object.getString("id"))
                                    && !object.getString("id").equals("null")) {
                                announce.setId(object.getInt("id"));
                            }
                            if (object.has("app_name")
                                    && !TextUtils.isEmpty(object.getString("app_name"))
                                    && !object.getString("app_name").equals("null")) {
                                announce.setAppName(object.getString("app_name"));
                            }
                            if (object.has("app_table")
                                    && !TextUtils.isEmpty(object.getString("app_table"))
                                    && !object.getString("app_table").equals("null")) {
                                announce.setAppTable(object.getString("app_table"));
                            }
                            if (object.has("app_field_1")
                                    && !TextUtils.isEmpty(object.getString("app_field_1"))
                                    && !object.getString("app_field_1").equals("null")) {
                                announce.setAppField1(object.getString("app_field_1"));
                            }
                            if (object.has("app_field_1_value")
                                    && !TextUtils.isEmpty(object.getString("app_field_1_value"))
                                    && !object.getString("app_field_1_value").equals("null")) {
                                announce.setAppField1Value(object.getBoolean("app_field_1_value"));
                            }
                            if (object.has("app_field_2")
                                    && !TextUtils.isEmpty(object.getString("app_field_2"))
                                    && !object.getString("app_field_2").equals("null")) {
                                announce.setAppField2(object.getString("app_field_2"));
                            }
                            if (object.has("app_field_2_value")
                                    && !TextUtils.isEmpty(object.getString("app_field_2_value"))
                                    && !object.getString("app_field_2_value").equals("null")) {
                                announce.setAppField2Value(object.getBoolean("app_field_2_value"));
                            }
                            if (object.has("app_field_alert_message")
                                    && !TextUtils.isEmpty(object.getString("app_field_alert_message"))
                                    && !object.getString("app_field_alert_message").equals("null")) {
                                announce.setAppFieldAlertMessage(object.getString("app_field_alert_message"));
                            }

                            if (object.has("app_field_alert_proceed_link")
                                    && !TextUtils.isEmpty(object.getString("app_field_alert_proceed_link"))
                                    && !object.getString("app_field_alert_proceed_link").equals("null")) {
                                announce.setAppFieldAlertProceedLink(object.getString("app_field_alert_proceed_link"));
                            }

                            if (announce.getAppField1().equals("local_free_only") && announce.getAppField2().equals("local_paid")) {
                                if (announce.getAppField1Value() && !announce.getAppField2Value()) {
                                    freeLocalEventText = announce.getAppFieldAlertMessage();
                                    freeLocalEventLink = announce.getAppFieldAlertProceedLink();
                                }
                            }

                            if (announce.getAppField1().equals("web_free_only") && announce.getAppField2().equals("web_paid")) {
                                if (announce.getAppField1Value() && !announce.getAppField2Value()) {
                                    freeOnlineEventText = announce.getAppFieldAlertMessage();
                                    freeOnlineEventLink = announce.getAppFieldAlertProceedLink();
                                }
                            }

                            if (announce.getAppField1().equals("parking_free_only") && announce.getAppField2().equals("parking_paid")) {
                                if (announce.getAppField1Value() && !announce.getAppField2Value()) {
                                    freeParkingText = announce.getAppFieldAlertMessage();
                                    freeParkingLink = announce.getAppFieldAlertProceedLink();
                                }
                            }
                            appConfigAnnouncesList.add(announce);
                        }
                    }
                    Log.e("#DEBUG", "fetchFreeEventAlertTaxt " + jsonArray.length());
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            binding.rlProgressbar.setVisibility(View.GONE);
            if (getActivity() != null) {
               /* if (appConfigAnnouncesList.size() != 0) {
                    for (int i = 0; i < appConfigAnnouncesList.size(); i++) {
                        if (appConfigAnnouncesList.get(i).getAppField1().equals("local_free_only") && appConfigAnnouncesList.get(i).getAppField2().equals("local_paid")) {
                            if (appConfigAnnouncesList.get(i).getAppField1Value() && !appConfigAnnouncesList.get(i).getAppField2Value()) {
                                freeLocalEvent = i;
                            }
                        }

                        if (appConfigAnnouncesList.get(i).getAppField1().equals("web_free_only") && appConfigAnnouncesList.get(i).getAppField2().equals("web_paid")) {
                            if (appConfigAnnouncesList.get(i).getAppField1Value() && !appConfigAnnouncesList.get(i).getAppField2Value()) {
                                freeOnlineEvent = i;
                            }
                        }

                        if (appConfigAnnouncesList.get(i).getAppField1().equals("parking_free_only") && appConfigAnnouncesList.get(i).getAppField2().equals("parking_paid")) {
                            if (appConfigAnnouncesList.get(i).getAppField1Value() && !appConfigAnnouncesList.get(i).getAppField2Value()) {
                                freeParking = i;
                            }
                        }
                    }
                }*/
            }
        }
    }

    private void showDialog(String title, String link, SwitchCompat switchFreeEvent) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(Html.fromHtml("<p>" + title + ".<br/><a href='" + link + "'>" + link + "</a></p>"));
        builder1.setCancelable(false);
        builder1.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                switchFreeEvent.setChecked(true);
            }
        });
        AlertDialog Alert1 = builder1.create();
        Alert1.show();
        ((TextView) Alert1.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }}
