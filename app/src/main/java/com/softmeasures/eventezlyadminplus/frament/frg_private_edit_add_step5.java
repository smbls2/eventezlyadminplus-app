package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.MyApplication;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.models.LocationLot;
import com.softmeasures.eventezlyadminplus.models.Manager;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.frament.Private_parking_main.active;
import static com.softmeasures.eventezlyadminplus.frament.Private_parking_main.clickonedit;
import static com.softmeasures.eventezlyadminplus.frament.Private_parking_main.effect;
import static com.softmeasures.eventezlyadminplus.frament.Private_parking_main.lang;
import static com.softmeasures.eventezlyadminplus.frament.Private_parking_main.lat;
import static com.softmeasures.eventezlyadminplus.frament.Private_parking_main.private_id;
import static com.softmeasures.eventezlyadminplus.frament.Private_parking_main.renew;
import static com.softmeasures.eventezlyadminplus.frament.Private_parking_main.title;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1.commercial_address;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step1.Private_location_code;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step1.private_address;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step1.private_lang;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step1.private_lat;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step1.private_no_parking;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step1.private_parking_time;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step1.private_title;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step2.private_lost_avali;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step2.private_lot_total;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step2.private_off_end;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step2.private_off_peak_discount;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step2.private_off_start;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step2.private_rows_total;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step2.private_space_total;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step2.str_pluse_sign;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step2.township_labeling;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.private_place_id;

public class frg_private_edit_add_step5 extends BaseFragment {
    TextView txt_private_title_sp3, txt_save;
    Switch sw_private_in_effect, sw_private_active, sw_private_renew;
    boolean private_in_active = true, private_in_effect = true, private_renew = true;
    EditText txt_private_add_custome_notices;
    String private_custome_notice;
    RelativeLayout rl_progressbar;
    String locationId;
    private RelativeLayout rl_main;
    private String markerType = "";

    private ArrayList<LocationLot> locationLots = new ArrayList<>();
    private boolean isNewLocation = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getArguments() != null) {
            isNewLocation = getArguments().getBoolean("isNewLocation", false);
            markerType = getArguments().getString("township_type_sp");
            Log.e("#DEBUG", "   frg_commercial_edit_add_step5:  markerType:  " + markerType);
        }
        return inflater.inflate(R.layout.private_edit_step_3, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {
        txt_private_title_sp3.setText(private_title);

        if (effect.equals("1")) {
            sw_private_in_effect.setChecked(true);
        } else {
            sw_private_in_effect.setChecked(false);
        }
        if (active.equals("1")) {
            sw_private_active.setChecked(true);
        } else {
            sw_private_active.setChecked(false);
        }
        if (renew.equals("1")) {
            sw_private_renew.setChecked(true);
        } else {
            sw_private_renew.setChecked(false);
        }
    }

    @Override
    protected void setListeners() {
        sw_private_active.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                private_in_active = isChecked;
            }
        });

        sw_private_in_effect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                private_in_effect = isChecked;
            }
        });

        sw_private_renew.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                private_renew = isChecked;
            }
        });

        txt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                private_custome_notice = txt_private_add_custome_notices.getText().toString();
                if (clickonedit) {
                    clickonedit = false;
                    new updateprivate_parking().execute();
                } else {
                    new addprivate_praking().execute();
                }

            }
        });

        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    protected void initViews(View view) {
        txt_private_title_sp3 = (TextView) view.findViewById(R.id.txt_title_step3);
        sw_private_active = (Switch) view.findViewById(R.id.sw_private_active);
        sw_private_in_effect = (Switch) view.findViewById(R.id.sw_private_effect);
        sw_private_renew = (Switch) view.findViewById(R.id.sw_private_renew);
        txt_save = (TextView) view.findViewById(R.id.txt_save);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        txt_private_add_custome_notices = (EditText) view.findViewById(R.id.txt_private_custom_notices);
        rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
    }

    //update private parking................
    public class updateprivate_parking extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        JSONObject json12 = null;
        String id = "";
        String walleturl = "_table/other_parking_partners";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("address", private_address);
            jsonValues1.put("id", private_id);
            if (private_in_active) {
                jsonValues1.put("active", 1);
            } else {
                jsonValues1.put("active", 0);
            }

            if (private_in_effect) {
                jsonValues1.put("in_effect", 1);
            } else {
                jsonValues1.put("in_effect", 0);
            }
            if (private_renew) {
                jsonValues1.put("renewable", 1);
            } else {
                jsonValues1.put("renewable", 0);
            }
            jsonValues1.put("parking_times", private_parking_time);
            jsonValues1.put("no_parking_times", private_no_parking);
            jsonValues1.put("off_peak_starts", private_off_start);
            jsonValues1.put("off_peak_discount", private_off_peak_discount);
            jsonValues1.put("lots_avbl", private_lost_avali);
            jsonValues1.put("lots_total", private_lot_total);
            jsonValues1.put("weekend_special_diff", str_pluse_sign + private_off_peak_discount);
            jsonValues1.put("off_peak_ends", private_off_end);
            jsonValues1.put("custom_notice", private_custome_notice);
            jsonValues1.put("lat", lat);
            jsonValues1.put("lng", lang);
            jsonValues1.put("location_code", Private_location_code);
            jsonValues1.put("marker_type", markerType);
            jsonValues1.put("title", private_title);
            jsonValues1.put("user_id", user_id);
            jsonValues1.put("location_place_id", Private_location_code);

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json12 != null) {
                if (json12.has("error")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("Error");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                } else {
                    if (!id.equals("")) {
                        fragmentStack.clear();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        Private_parking_main pay = new Private_parking_main();
                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.add(R.id.My_Container_1_ID, pay);
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                    }
                }
            }
            super.onPostExecute(s);
        }
    }


    //add parivte parking............................................
    public class addprivate_praking extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        JSONObject json12 = null;
        String id = "";
        String walleturl = "_table/other_parking_partners";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            if (title.equals("")) {
                title = private_title;
            }
            jsonValues1.put("location_code", Private_location_code);
            jsonValues1.put("location_name", private_title);
            jsonValues1.put("location_place_id", private_place_id);
            jsonValues1.put("title", title);
            jsonValues1.put("lat", lat);
            jsonValues1.put("lng", lang);
            jsonValues1.put("user_id", user_id);
            jsonValues1.put("address", private_address);
            if (private_in_active) {
                jsonValues1.put("active", 1);
            } else {
                jsonValues1.put("active", 0);
            }
            if (private_in_effect) {
                jsonValues1.put("in_effect", 1);
            } else {
                jsonValues1.put("in_effect", 0);
            }
            jsonValues1.put("custom_notice", private_custome_notice);
            jsonValues1.put("marker_type", markerType);
            jsonValues1.put("no_parking_times", private_no_parking);
            jsonValues1.put("parking_times", private_parking_time);
            jsonValues1.put("off_peak_discount", private_off_peak_discount);
            if (private_renew) {
                jsonValues1.put("renewable", 1);
            } else {
                jsonValues1.put("renewable", 0);
            }
            jsonValues1.put("weekend_special_diff", str_pluse_sign + private_off_peak_discount);
            jsonValues1.put("off_peak_starts", private_off_start);
            jsonValues1.put("off_peak_ends", private_off_end);
            jsonValues1.put("lots_total", private_lot_total);
            jsonValues1.put("lots_avbl", private_lost_avali);
            jsonValues1.put("lot_numbering_type", township_labeling);
            jsonValues1.put("lot_numbering_description", township_labeling);
            Manager manager = myApp.getSelectedManager();
            if (manager != null) {
                jsonValues1.put("twp_id", manager.getId());
                jsonValues1.put("township_code", manager.getManager_id());
                jsonValues1.put("manager_id", manager.getId());
                jsonValues1.put("company_id", manager.getId());
                jsonValues1.put("manager_type_id", manager.getManager_type_id());
                jsonValues1.put("company_type_id", manager.getManager_type_id());
            }
            //TODO events
//            jsonValues1.put("event_on_location", "");
//            jsonValues1.put("event_dates", "");


            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json12 != null) {
                if (json12.has("error")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("Error");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                } else {
                    if (!id.equals("")) {
                        locationId = id;
                        new updateprivate_parking_location_id().execute();
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class updateprivate_parking_location_id extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        JSONObject json12 = null;
        String id = "";
        String walleturl = "_table/other_parking_partners";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("id", locationId);
            jsonValues1.put("location_id", locationId);

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json12 != null) {
                if (json12.has("error")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("Error");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                } else {
                    if (!id.equals("")) {
                        if (MyApplication.getInstance().privateRuleIds.size() != 0) {
                            new update_parking_rules().execute(MyApplication.getInstance().privateRuleIds.get(0),
                                    locationId, private_title);
                        } else {

                            if (isNewLocation) {
                                myApp.isLocationAdded = true;
                                if (getActivity() != null) {
                                    getActivity().onBackPressed();
                                }
                            } else {
                                fragmentStack.clear();
                                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                Private_parking_main pay = new Private_parking_main();
                                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                ft.add(R.id.My_Container_1_ID, pay);
                                fragmentStack.push(pay);
                                ft.commitAllowingStateLoss();
                            }
                        }
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    //update private parking rules.....................
    public class update_parking_rules extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        JSONObject json12 = null;
        String walleturl = "_table/other_parking_rules";
        String id = "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("id", params[0]);
            jsonValues1.put("location_id", params[1]);
            jsonValues1.put("location_name", params[2]);
            jsonValues1.put("twp_id", 1000 + (Integer.valueOf(params[0]) - 141));

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (getActivity() != null && json12 != null) {
                if (json12.has("error")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("Error");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                } else {
                    if (!id.equals("")) {
                        if (markerType.contains("Managed")) {
                            new addTownship_Managed_location().execute();
                        } else {
                            rl_progressbar.setVisibility(View.GONE);
                            if (isNewLocation) {
                                myApp.isLocationAdded = true;
                                getActivity().onBackPressed();
                            } else {
                                fragmentStack.clear();
                                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                Private_parking_main pay = new Private_parking_main();
                                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                ft.add(R.id.My_Container_1_ID, pay);
                                fragmentStack.push(pay);
                                ft.commitAllowingStateLoss();
                            }
                        }
                    }
                }

            }
            super.onPostExecute(s);
        }
    }

    public class addTownship_Managed_location extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        JSONObject json12 = null;
        String id = "";
        String walleturl = "_table/manage_other_locations";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();

            jsonValues1.put("date_time", DateTimeUtils.getSqlFormatDate(new Date()));
            jsonValues1.put("location_name", private_title);
            jsonValues1.put("location_type", "");
            jsonValues1.put("full_address", commercial_address);
            jsonValues1.put("show_location", "(" + private_lat + "," + private_lang + "):15");
            jsonValues1.put("location_code", Private_location_code);
            jsonValues1.put("marker_type", markerType);
            jsonValues1.put("total_rows", private_rows_total);
            jsonValues1.put("total_spaces", private_space_total);
            jsonValues1.put("spaces_mark_reqd", true);
            jsonValues1.put("labeling", township_labeling);
            jsonValues1.put("location_id", locationId);
            if (!private_place_id.equals("")) {
                jsonValues1.put("location_place_id", private_place_id);

            }

            Manager manager = myApp.getSelectedManager();
            if (manager != null) {
                jsonValues1.put("twp_id", manager.getId());
                jsonValues1.put("township_code", manager.getManager_id());
                jsonValues1.put("manager_id", manager.getId());
                jsonValues1.put("company_id", manager.getId());
                jsonValues1.put("manager_type_id", manager.getManager_type_id());
                jsonValues1.put("company_type_id", manager.getManager_type_id());
            }

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            //progressBar.setVisibility(View.GONE);
            if (json12 != null) {
                if (json12.has("error")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("Error");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                } else {
                    if (!id.equals("")) {

                        //Calculate and add location_lots
                        LocationLot locationLot = new LocationLot();
                        if (!TextUtils.isEmpty(locationId))
                            locationLot.setLocation_id(Integer.valueOf(locationId));
                        locationLot.setLocation_code(Private_location_code);
                        locationLot.setLocation_name(private_title);
                        locationLot.setOccupied("NO");
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                        locationLot.setDate_time(sdf.format(new Date(Calendar.getInstance().getTimeInMillis())));
                        calculateLocationLots(private_rows_total, private_space_total, township_labeling, locationLot);
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    //TODO calculate location lots and prepare list of location lots
    private void calculateLocationLots(String rowsCount, String spaceCount, String township_labeling,
                                       LocationLot locationLotData) {
        locationLots.clear();
        int spaces = Integer.parseInt(spaceCount);
        int rows = Integer.parseInt(rowsCount);
        String[] alpha = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
                "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

        if (township_labeling.equals("Alpha")) {
            if (spaces < alpha.length) {
                for (int j = 0; j < spaces; j++) {
                    LocationLot locationLot = new LocationLot();
                    locationLot.setDate_time(locationLotData.getDate_time());
                    locationLot.setTownship_code(locationLotData.getTownship_code());
                    locationLot.setLocation_id(locationLotData.getLocation_id());
                    locationLot.setLocation_code(locationLotData.getLocation_code());
                    locationLot.setLocation_name(locationLotData.getLocation_name());
                    locationLot.setLot_number(alpha[j]);
                    locationLot.setOccupied("NO");
                    locationLot.setLot_id(alpha[j]);
                    locationLots.add(locationLot);

                }
                Log.e("#DEBUG", "   locationLotsSize:  " + locationLots.size());
            }
        } else if (township_labeling.equals("Numeric")) {
            for (int j = 0; j < spaces; j++) {
                LocationLot locationLot = new LocationLot();
                locationLot.setDate_time(locationLotData.getDate_time());
                locationLot.setTownship_code(locationLotData.getTownship_code());
                locationLot.setLocation_id(locationLotData.getLocation_id());
                locationLot.setLocation_code(locationLotData.getLocation_code());
                locationLot.setLocation_name(locationLotData.getLocation_name());
                locationLot.setLot_number(String.valueOf(j + 1));
                locationLot.setOccupied("NO");
                locationLot.setLot_id(String.valueOf(j + 1));
                locationLots.add(locationLot);

            }
            Log.e("#DEBUG", "   locationLotsSize:  " + locationLots.size());
        } else if (township_labeling.equals("AlphaNumeric")) {
            int inEachRowSpaces = spaces / rows;
            if (rows < alpha.length) {
                for (int j = 0; j < rows; j++) {
                    for (int k = 0; k < inEachRowSpaces; k++) {
                        LocationLot locationLot = new LocationLot();
                        locationLot.setDate_time(locationLotData.getDate_time());
                        locationLot.setTownship_code(locationLotData.getTownship_code());
                        locationLot.setLocation_id(locationLotData.getLocation_id());
                        locationLot.setLocation_code(locationLotData.getLocation_code());
                        locationLot.setLocation_name(locationLotData.getLocation_name());
                        locationLot.setLot_row(alpha[j]);
                        locationLot.setLot_number(String.valueOf(k + 1));
                        locationLot.setLot_id(alpha[j] + ":" + (k + 1));
                        locationLot.setOccupied("NO");
                        locationLots.add(locationLot);
                    }
                }
            }
        } else if (township_labeling.equals("AlphaAlpha")) {
            int inEachRowSpacesAlpha = spaces / rows;
            if (rows < alpha.length + 1) {
                for (int j = 0; j < rows; j++) {
                    for (int k = 0; k < inEachRowSpacesAlpha; k++) {
                        LocationLot locationLot = new LocationLot();
                        locationLot.setDate_time(locationLotData.getDate_time());
                        locationLot.setTownship_code(locationLotData.getTownship_code());
                        locationLot.setLocation_id(locationLotData.getLocation_id());
                        locationLot.setLocation_code(locationLotData.getLocation_code());
                        locationLot.setLocation_name(locationLotData.getLocation_name());
                        locationLot.setLot_row(alpha[j]);
                        locationLot.setLot_number(alpha[k]);
                        locationLot.setLot_id(String.format("%s:%s", alpha[j], alpha[k]));
                        locationLots.add(locationLot);
                    }
                }
            }
        } else if (township_labeling.equals("NumericNumeric")) {
            int inEachRowSpacesNumeric = spaces / rows;
            if (rows < alpha.length + 1) {
                for (int j = 0; j < rows; j++) {
                    for (int k = 0; k < inEachRowSpacesNumeric; k++) {
                        LocationLot locationLot = new LocationLot();
                        locationLot.setDate_time(locationLotData.getDate_time());
                        locationLot.setTownship_code(locationLotData.getTownship_code());
                        locationLot.setLocation_id(locationLotData.getLocation_id());
                        locationLot.setLocation_code(locationLotData.getLocation_code());
                        locationLot.setLocation_name(locationLotData.getLocation_name());
                        locationLot.setLot_row(String.valueOf(j + 1));
                        locationLot.setLot_number(String.valueOf(k + 1));
                        locationLot.setLot_id(String.format("%s:%s", String.valueOf(j + 1), String.valueOf(k + 1)));
                        locationLots.add(locationLot);
                    }
                }
            }
        }

        if (locationLots.size() != 0) {
            new addLocationLots().execute();
        }

    }

    //TODO add location lots
    private class addLocationLots extends AsyncTask<String, String, String> {

        String locationLotUrl = "_table/location_lot_other";
        JSONObject json, json1;
        String re_id;

        @Override
        protected void onPreExecute() {
            Log.e("#DEBUG", "  addLocationLots:  onPre: Size: " + locationLots.size());
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            String url = getString(R.string.api) + getString(R.string.povlive) + locationLotUrl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                Log.e("#DEBUG", "   body:  location_lots: " + new Gson().toJson(locationLots.get(0)));
                entity = new StringEntity(new Gson().toJson(locationLots.get(0)), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("#DEBUG", "  addLocationLots:  Error:  " + e.getMessage());
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("#DEBUG", "  addLocationLots:  Error:  " + e.getMessage());
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   addLocationLot:  Response:    " + responseStr);
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "  addLocationLots:  Error:  " + e.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "  addLocationLots:  Error:  " + e.getMessage());
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("#DEBUG", "   onPost: locationLots: size" + locationLots.size());
            if (locationLots.size() != 0) {
                locationLots.remove(0);
                if (locationLots.size() != 0) {
                    new addLocationLots().execute();
                } else {
                    rl_progressbar.setVisibility(View.GONE);
                    if (isNewLocation) {
                        myApp.isLocationAdded = true;
                        if (getActivity() != null) {
                            getActivity().onBackPressed();
                        }
                    } else {
                        fragmentStack.clear();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        Private_parking_main pay = new Private_parking_main();
                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.add(R.id.My_Container_1_ID, pay);
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                        Log.e("#DEBUG", "   onPost addLocationCompleted! ");
                    }
                }
            } else {
                rl_progressbar.setVisibility(View.GONE);
                if (isNewLocation) {
                    myApp.isLocationAdded = true;
                    if (getActivity() != null) {
                        getActivity().onBackPressed();
                    }
                } else {
                    fragmentStack.clear();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Private_parking_main pay = new Private_parking_main();
                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                    Log.e("#DEBUG", "   onPost addLocationCompleted! ");
                }
            }
        }
    }

}
