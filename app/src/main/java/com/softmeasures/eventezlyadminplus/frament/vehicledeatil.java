package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.services.NotificationPublisher;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.MODE_PRIVATE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.activity.vchome.markerclick;

public class vehicledeatil extends Fragment {


    TextView txtplantno, txtstatename, txttimerplantno, txttimerrgiserexpiresdate, txtaprkat, txttimermaxtime, txttimer, txt_crate_ticket, txt_findmyvehicle, txt_row, txt_view_ticket, txt_parked_address;
    ImageView imgmarker;
    String marker, platno, statename, lat, lang, row, row_no, status, parking_type1, parking_id = "0", final_township_code = "", selected_address;
    RelativeLayout rl_main;
    ConnectionDetector cd;
    ProgressBar progressBar;
    TextView txt_violation_address, txt_address;
    TextView txt_force_exit;
    RelativeLayout rl_progressbar;
    long hours, minutes, seconds;
    String expire, entry;
    long timeInMilliseconds = 0L;
    long updatedTime = 0L;
    boolean isFinishing = false;
    private Handler customHandler = new Handler();
    ArrayList<item> township_array_list = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_vehicledeatil, container, false);
        txt_force_exit = (TextView) view.findViewById(R.id.txt_force_exit);
        txt_address = (TextView) view.findViewById(R.id.txt_add);
        txt_parked_address = (TextView) view.findViewById(R.id.txt_parked);
        txtaprkat = (TextView) view.findViewById(R.id.txtparkedat);
        txttimerrgiserexpiresdate = (TextView) view.findViewById(R.id.txtexpires);
        txttimermaxtime = (TextView) view.findViewById(R.id.txtmaxtime);
        txt_view_ticket = (TextView) view.findViewById(R.id.txt_view_ticket);
        txt_crate_ticket = (TextView) view.findViewById(R.id.txt_crweateticket);
        txt_findmyvehicle = (TextView) view.findViewById(R.id.txtfindvehicle);
        txtplantno = (TextView) view.findViewById(R.id.txtplatno);
        txtstatename = (TextView) view.findViewById(R.id.txtstatename);
        imgmarker = (ImageView) view.findViewById(R.id.imgmarker);
        Typeface light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Light.otf");
        txttimer = (TextView) view.findViewById(R.id.time);
        txt_row = (TextView) view.findViewById(R.id.txtrow);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        rl_main = (RelativeLayout) view.findViewById(R.id.main);
        txttimerplantno = (TextView) view.findViewById(R.id.txtplate);
        markerclick = false;
        //create ticket
        txt_violation_address = (TextView) view.findViewById(R.id.txt_description);


        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        txtplantno.setTypeface(light);
        txtstatename.setTypeface(light);
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        SharedPreferences sh = getActivity().getSharedPreferences("streetview", MODE_PRIVATE);
        platno = sh.getString("title", "null");
        statename = sh.getString("statename", "null");
        marker = sh.getString("marker", "null");
        lat = sh.getString("la", "null");
        lang = sh.getString("lang", "null");
        row = sh.getString("lot_row", "null");
        row_no = sh.getString("lot_row_no", "null");
        expire = sh.getString("exiry", "null");
        entry = sh.getString("entry", "null");
        status = sh.getString("status", "null");
        parking_type1 = sh.getString("parking_type", "null");
        String max = sh.getString("max_time", "0");
        parking_id = sh.getString("id", "");
        final_township_code = sh.getString("township_code", "");
        cd = new ConnectionDetector(getActivity());
        final String lat1 = sh.getString("lat1", "null");
        final String lang1 = sh.getString("lang1", "null");
        if (max != null) {
            if (max.contains(".")) {
                String mmm = max.substring(0, max.indexOf("."));
                txttimermaxtime.setText(mmm + " Hours");
            } else {
                txttimermaxtime.setText(max + " Hours");
            }
        }
        selected_address = sh.getString("selected_address", "null");
        String parked_address = sh.getString("parked_address", "null");

        parked_address = parked_address != null ? (!parked_address.equals("null") ? (!parked_address.equals("") ? parked_address : "") : "") : "";
        txt_parked_address.setText(parked_address);
        if (status.equals("TICKETED")) {
            txt_view_ticket.setVisibility(View.VISIBLE);
        } else {
            txt_view_ticket.setVisibility(View.GONE);
        }
        txt_findmyvehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sh1 = getActivity().getSharedPreferences("directiontomydirection", MODE_PRIVATE);
                SharedPreferences.Editor ed1 = sh1.edit();
                ed1.putString("lat", lat1);
                ed1.putString("lang", lang1);
                ed1.commit();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                f_direction pay = new f_direction();
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        txt_force_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Exit Parking");
                alertDialog.setMessage("Are you sure you want to exit vehicle from this parking ?");
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        new exitsparking().execute();
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        if (cd.isConnectingToInternet()) {
            new gettownship_list().execute();
            if (!platno.equals("null")) {
                txtplantno.setText("Plate Number - " + platno);
                txtstatename.setText("State - " + statename);

                if (row.equals("null") || row.equals("")) {
                    txt_row.setVisibility(View.GONE);
                    txt_row.setText("Row -");
                } else {
                    txt_row.setVisibility(View.VISIBLE);
                    txt_row.setText("Row - " + row + row_no);
                }

                if (parking_type1.equals("anywhere") || parking_type1.equals("random")) {
                    if (status.equals("TICKETED")) {
                        imgmarker.setBackgroundResource(R.mipmap.car_red_ticketed1);
                    } else {
                        imgmarker.setBackgroundResource(R.mipmap.popup_car_blue);
                    }
                } else if (marker.equals("status-green")) {
                    if (status.equals("TICKETED")) {
                        imgmarker.setBackgroundResource(R.mipmap.car_red_ticketed1);
                    } else {
                        imgmarker.setBackgroundResource(R.mipmap.popup_car_green);
                    }
                } else if (marker.equals("status-red")) {
                    if (status.equals("TICKETED")) {
                        imgmarker.setBackgroundResource(R.mipmap.car_red_ticketed1);
                    } else {
                        imgmarker.setBackgroundResource(R.mipmap.popup_car_red);
                    }

                } else if (marker.equals("status-yellow")) {
                    if (status.equals("TICKETED")) {
                        imgmarker.setBackgroundResource(R.mipmap.car_red_ticketed1);
                    } else {
                        imgmarker.setBackgroundResource(R.mipmap.car_yellow_pin);
                    }
                }
            }

            txt_crate_ticket.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle b = new Bundle();
                    b.putString("plate", "Plate Number - " + platno);
                    b.putString("state", "State - " + statename);
                    b.putString("row", row);
                    b.putString("row_no", row_no);
                    b.putString("marker", marker);
                    b.putString("is_direct", "no");
                    b.putString("parked_car_address", "");
                    b.putString("townshipCode", final_township_code);
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    create_ticket_main pay = new create_ticket_main();
                    pay.setArguments(b);
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                }
            });

            txt_view_ticket.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences pla = getActivity().getSharedPreferences("view_ticket", MODE_PRIVATE);
                    SharedPreferences.Editor ed = pla.edit();
                    ed.putString("view_ticket", "yes");
                    ed.putString("noplate", platno);
                    ed.putString("state_name", statename);
                    ed.commit();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    mytickets pay = new mytickets();
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                }
            });

            txttimerplantno.setText(platno + "  " + statename);
            final TimeZone zone12 = TimeZone.getTimeZone("UTC");
            if (parking_type1.equals("anywhere")) {
                if (parking_type1.equals("anywhere")) {
                    txttimermaxtime.setText("N/A");
                    txttimerrgiserexpiresdate.setText("N/A");
                    txt_address.setText("N/A");

                }
                customHandler.postDelayed(updateTimerThread1, 0);
            } else {
                if (!selected_address.equals("null")) {
                    txt_address.setText(selected_address);
                }
                String inputPattern = "yyyy-MM-dd HH:mm:ss";
                SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.US);
                SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMM, yyyy hh:mm:ss a", Locale.US);
                Date date = null;
                Date dateentryy1;
                Date currentdate1 = null;
                String str = null;
                String datef = expire;
                try {
                    if (datef.equals("0000-00-00 00:00:00")) {
                        String inputPattern1 = "yyyy-MM-dd HH:mm:ss";
                        SimpleDateFormat inputFormat1 = new SimpleDateFormat(inputPattern1, Locale.US);
                        SimpleDateFormat outputFormat1 = new SimpleDateFormat("dd MMM, yyyy hh:mm:ss a", Locale.US);
                        Date date1 = null;
                        Date dateentryy;
                        String str1 = null;
                        String datef1 = expire;
                        String dateentry = entry;
                        try {
                            if (datef1.equals("0000-00-00 00:00:00")) {

                            } else {
                                date1 = inputFormat1.parse(expire);
                                str1 = outputFormat1.format(date1);
                                dateentryy = inputFormat1.parse(entry);
                                String entrydate = outputFormat1.format(dateentryy);
                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                                Date value = formatter.parse(entry);

                                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM, yyyy hh:mm:ss a", Locale.US); //this format changeable
                                dateFormatter.setTimeZone(TimeZone.getDefault());
                                String localtime1 = dateFormatter.format(value);

                                txtaprkat.setText(localtime1);
                                //  txtaprkat.setText(entrydate);
                                if (parking_type1.equals("anywhere")) {
                                    txttimerrgiserexpiresdate.setText("N/A");
                                } else {

                                    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    formatter1.setTimeZone(TimeZone.getTimeZone("UTC"));
                                    Date value1 = formatter1.parse(expire);
                                    SimpleDateFormat dateFormatter1 = new SimpleDateFormat("dd MMM, yyyy hh:mm:ss a", Locale.US); //this format changeable
                                    dateFormatter1.setTimeZone(TimeZone.getDefault());
                                    String localtime2 = dateFormatter1.format(value1);
                                    txttimerrgiserexpiresdate.setText(localtime2);
                                }

                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        } catch (java.text.ParseException e) {
                            e.printStackTrace();
                        }
                    } else {
                        date = inputFormat.parse(expire);
                        str = outputFormat.format(date);
                        if (parking_type1.equals("anywhere")) {

                            txttimerrgiserexpiresdate.setText("N/A");
                        } else {

                            SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            formatter1.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date value1 = formatter1.parse(expire);
                            SimpleDateFormat dateFormatter1 = new SimpleDateFormat("dd MMM, yyyy hh:mm:ss a", Locale.US); //this format changeable
                            dateFormatter1.setTimeZone(TimeZone.getDefault());
                            String localtime2 = dateFormatter1.format(value1);
                            txttimerrgiserexpiresdate.setText(localtime2);
                        }
                        dateentryy1 = inputFormat.parse(entry);
                        String entrydate = outputFormat.format(dateentryy1);

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                        Date value = formatter.parse(entry);

                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM, yyyy hh:mm:ss a", Locale.US); //this format changeable
                        dateFormatter.setTimeZone(TimeZone.getDefault());
                        String localtime1 = dateFormatter.format(value);
                        txtaprkat.setText(localtime1);
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }

                SimpleDateFormat df;
                df = new SimpleDateFormat("dd MMM, yyyy hh:mm:ss a", Locale.US);
                df.setTimeZone(zone12);
                Date date11 = null;
                try {
                    inputFormat.setTimeZone(zone12);
                    outputFormat.setTimeZone(zone12);
                    date = inputFormat.parse(expire);
                    str = outputFormat.format(date);
                    String ss = str;
                    date11 = df.parse(ss);
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                Date date2 = null;
                try {
                    SimpleDateFormat current = new SimpleDateFormat("dd MMM, yyyy hh:mm:ss a", Locale.US);
                    current.setTimeZone(zone12);
                    String currentdate = current.format(new Date());
                    try {
                        date2 = current.parse(currentdate);
                        ;
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long diff = date11.getTime() - date2.getTime();
                long timeInSeconds = diff / 1000;
                hours = timeInSeconds / 3600;
                timeInSeconds = timeInSeconds - (hours * 3600);
                minutes = timeInSeconds / 60;
                timeInSeconds = timeInSeconds - (minutes * 60);
                seconds = timeInSeconds;
                int day = (int) (hours / 24);
                if (hours > 0) {
                    if (parking_type1.equals("anywhere")) {
                        txttimermaxtime.setText("N/A");
                    }
                    txttimer.setText(hours + "  : " + minutes + "  : " + seconds);
                    final CounterClass timer = new CounterClass(diff, 1000);
                    timer.start();
                    imgmarker.setBackgroundResource(R.mipmap.popup_car_green);
                } else if (minutes > 0) {
                    final CounterClass timer = new CounterClass(diff, 1000);
                    timer.start();
                    imgmarker.setBackgroundResource(R.mipmap.popup_car_green);
                } else if (seconds > 0) {
                    final CounterClass timer = new CounterClass(diff, 1000);
                    timer.start();
                    imgmarker.setBackgroundResource(R.mipmap.popup_car_green);
                } else {
                    if (parking_type1.equals("anywhere")) {
                        txttimermaxtime.setText("N/A");
                    } else if (status.equals("TICKETED")) {
                        imgmarker.setBackgroundResource(R.mipmap.car_red_ticketed1);
                    } else {
                        imgmarker.setBackgroundResource(R.mipmap.popup_car_red);
                    }
                }
            }
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();
        }
        return view;
    }


    private Runnable updateTimerThread1 = new Runnable() {

        public void run() {

            if (isFinishing == false) {
                long tim2e = 0;
                Date date1 = null;
                TimeZone zone12 = TimeZone.getTimeZone("UTC");

                try {

                    String inputPattern1 = "yyyy-MM-dd HH:mm:ss";
                    SimpleDateFormat inputFormat1 = new SimpleDateFormat(inputPattern1);
                    SimpleDateFormat outputFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a", Locale.US);
                    inputFormat1.setTimeZone(zone12);
                    outputFormat1.setTimeZone(zone12);

                    String str1 = null;
                    String datef1 = entry;

                    date1 = inputFormat1.parse(entry);

                    tim2e = date1.getTime();
                    Log.e("time", String.valueOf(tim2e));
                    str1 = outputFormat1.format(date1);
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Date value = formatter.parse(entry);

                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM, yyyy hh:mm:ss a", Locale.US); //this format changeable
                    dateFormatter.setTimeZone(TimeZone.getDefault());
                    String localtime1 = dateFormatter.format(value);

                    txtaprkat.setText(localtime1);

                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat df;
                df = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a", Locale.US);
                df.setTimeZone(zone12);
                Date date2 = null;
                try {

                    Calendar cal = Calendar.getInstance();

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                    sdf.setTimeZone(zone12);
                    String currentdate = sdf.format(new Date());
                    String inputPattern1 = "yyyy-MM-dd HH:mm:ss";
                    SimpleDateFormat inputFormat1 = new SimpleDateFormat(inputPattern1);
                    SimpleDateFormat outputFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a", Locale.US);
                    inputFormat1.setTimeZone(zone12);
                    outputFormat1.setTimeZone(zone12);

                    String str1 = null;
                    String datef1 = currentdate;

                    date2 = inputFormat1.parse(currentdate);
                    String str2 = outputFormat1.format(date2);
                    Calendar c = Calendar.getInstance();

                    SimpleDateFormat df11 = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a", Locale.US);
                    String formattedDate = df11.format(c.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                timeInMilliseconds = date2.getTime() - date1.getTime();
                int secs = (int) (timeInMilliseconds / 1000);
                Log.e("sscs", String.valueOf(secs));
                // int secs = (int) (timeInMilliseconds / 1000) ;
                int mins = secs / 60;
                int hor = mins / 60;
                secs = secs % 60;
                int milliseconds = (int) (updatedTime % 1000);

                String hms = String.format("%02d : %02d : %02d", hor, mins, secs);
                long millis = timeInMilliseconds;
                String hms1 = String.format("%02d : %02d : %02d", TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                        TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                txttimer.setText(hms1);
                customHandler.postDelayed(this, 1);
            } else {
                onDestroy();
                customHandler.removeCallbacks(this);
            }
        }
    };

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @SuppressLint("NewApi")
    public class CounterClass extends CountDownTimer {

        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            txttimer.setText("0 : 00 : 00");
        }

        @SuppressLint("NewApi")
        @TargetApi(Build.VERSION_CODES.GINGERBREAD)
        @Override
        public void onTick(long millisUntilFinished) {

            long millis = millisUntilFinished;
            String hms = String.format("%02d : %02d : %02d", TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
            System.out.println(hms);

            long h = TimeUnit.MILLISECONDS.toHours(millis);
            long min = TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis));

            long s = TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis));

           /* if(h==00 && min==6 && s==0)
            {
               imgmarker.setBackgroundResource(R.mipmap.car_yellow);
            }
            else if(h==0 && min==0&& s==0)
            {
               if(status.equals("TICKETED"))
               {
                   imgmarker.setBackgroundResource(R.mipmap.car_red_ticketed);
               }
               else
               {
                   imgmarker.setBackgroundResource(R.mipmap.car_red);
               }
            }
            else
            {
                imgmarker.setBackgroundResource(R.mipmap.car_green);
            }*/
            txttimer.setText(hms);
        }
    }

    public class exitsparking extends AsyncTask<String, String, String> {
        String exit_status = "_table/parked_cars";
        JSONObject json, json1;
        String re_id = "null";

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", parking_id);
            jsonValues.put("parking_status", "EXIT");
            JSONObject json = new JSONObject(jsonValues);
            //  String url = "http://100.12.26.176:8006/api/v2/pzly01live7/_table/poiv2";
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
//setting json object to post request.
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
//this is your response:
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.d("re_id", re_id);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);

            if (getActivity() != null && json1 != null) {
                if (!re_id.equals("null")) {
                    canclealarm(Integer.parseInt(parking_id));
                    SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                    String id1 = logindeatl.getString("id", "null");
                    if (id1.equals("null")) {
                        ArrayList<item> arraylist = new ArrayList<>();
                        SharedPreferences sharedPrefs = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
                        Gson gson = new Gson();
                        String json = sharedPrefs.getString("car", null);
                        Type type = new TypeToken<ArrayList<item>>() {
                        }.getType();
                        arraylist = gson.fromJson(json, type);
                        if (arraylist.size() > 0) {
                            for (int j = 0; j < arraylist.size(); j++) {
                                String id = arraylist.get(j).getId();
                                if (id.equals(re_id)) {
                                    arraylist.remove(j);
                                }
                            }
                        }
                        SharedPreferences sharedPrefs1 = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPrefs1.edit();
                        Gson gson1 = new Gson();
                        String json1 = gson1.toJson(arraylist);
                        editor.putString("car", json1);
                        editor.commit();
                    }
                    getActivity().onBackPressed();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Something went wrong");
                    alertDialog.setMessage("Please try again");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
            super.onPostExecute(s);
        }
    }

    public class gettownship_list extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String parkingurl = "_table/townships_manager";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            township_array_list.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setTitle(c.getString("manager_id"));
                        item1.setManager_type(c.getString("manager_type"));
                        item1.setLot_manager(c.getString("lot_manager"));
                        item1.setAddress(c.getString("address"));
                        item1.setState(c.getString("state"));
                        item1.setCity(c.getString("city"));
                        item1.setCountry(c.getString("country"));
                        item1.setZip_code(c.getString("zip"));
                        item1.setTownship_logo(c.getString("township_logo"));
                        item1.setOfficial_logo(c.getString("official_logo"));
                        item1.setIsslected(false);
                        township_array_list.add(item1);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null) {
                Resources rs = getResources();
                if (json != null) {

                }
            }
            super.onPostExecute(s);
        }
    }

    public void canclealarm(int id) {
        Intent notificationIntent = new Intent(getActivity(), NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, id);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(getActivity(), id + 1, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent alarmIntent1 = PendingIntent.getBroadcast(getActivity(), id + 2, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent alarmIntent3 = PendingIntent.getBroadcast(getActivity(), id + 3, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
        alarmManager.cancel(alarmIntent);
        alarmIntent.cancel();
        alarmManager.cancel(alarmIntent1);
        alarmIntent1.cancel();
        alarmManager.cancel(alarmIntent3);
        alarmIntent3.cancel();
    }
}
