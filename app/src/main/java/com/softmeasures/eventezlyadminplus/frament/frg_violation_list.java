package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class frg_violation_list extends Fragment {
    TextView txt_violation_code, txt_violation_fess, txt_violation_address, txt_violation_type, txt_hearinf_type, textty1, txt_crweateticket1, txt_court_id, txt_hearing_date, txt_finalticket, txt_final_create_ticket, txt_address;
    ListView list_of_violation_type, list_of_hearing_location;
    adpterviolationtype ad;
    RelativeLayout rl_progressbar;
    String marker, platno, statename, lat, lang, row, row_no, township_code, violation_code, violation_desc, hearing_address, violation_fee, hearing_location, court_id, user_id, status, heraing_date, parking_type1;
    ArrayList<item> violationtype = new ArrayList<item>();
    ArrayList<item> hearingtype = new ArrayList<item>();

    private list_item_click_viloation listener;

    public interface list_item_click_viloation {
        public void on_violation(String code, String fess, String des);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_violation_list, container, false);
        list_of_violation_type = (ListView) view.findViewById(R.id.list_violationtype);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        new getviolationtype().execute();
        return view;
    }

    public class adpterviolationtype extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public adpterviolationtype(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;


            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {

                vi = inflater.inflate(R.layout.adapaterviolationtype, null);
                holder = new ViewHolder();

                holder.txtvilationcode = (TextView) vi.findViewById(R.id.txt_violation_code);
                holder.txtviocationdescription = (TextView) vi.findViewById(R.id.txt_violation_descripetion);
                holder.txtfess = (TextView) vi.findViewById(R.id.txt_violation_fees);

                vi.setTag(holder);
                vi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences gg = getActivity().getSharedPreferences("violation", MODE_PRIVATE);
                        SharedPreferences.Editor d = gg.edit();
                        d.clear();
                        d.commit();
                        String code = holder.txtvilationcode.getText().toString();
                        String fess = holder.txtfess.getText().toString();
                        String des = holder.txtviocationdescription.getText().toString();
                        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.sidepannelleft);
                       /* rl_list_violation_type.startAnimation(animation);
                        rl_list_violation_type.setVisibility(View.GONE);
                        rl_violation.setVisibility(View.VISIBLE);
                        txt_violation_code.setText(code);
                        txt_violation_fess.setText(fess);
                        txt_violation_address.setText(des);
                        rl_hearing.setVisibility(View.GONE);
                        txt_hearing_address.setText("");
                        txt_hearing_location.setText("");
                        new gethearing().execute();*/

                        listener.on_violation(code, fess, des);
                        getActivity().onBackPressed();
                    }
                });
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {

            } else {

                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;

                Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvetica-normal.ttf");
                Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvetica-neue-medium.ttf");

                holder.txtvilationcode.setTypeface(medium);
                holder.txtfess.setTypeface(medium);

                if (tempValues.getVioloation_code().equals("null")) {
                    holder.txtvilationcode.setText("Code: ");
                }
                if (tempValues.getViolation_fess().equals("null")) {
                    holder.txtfess.setText("Fee: ");

                }
                if (tempValues.getViolation_desciprtion().equals("null")) {

                    holder.txtviocationdescription.setText("");
                }

                if (!tempValues.getVioloation_code().equals("null") && !tempValues.getViolation_desciprtion().equals("null") && !tempValues.getViolation_fess().equals("null")) {
                    holder.txtvilationcode.setText("Code: " + tempValues.getVioloation_code());
                    holder.txtfess.setText("Fee: " + tempValues.getViolation_fess());
                    holder.txtviocationdescription.setText(tempValues.getViolation_desciprtion());
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txtvilationcode, txtviocationdescription, txtfess;
        }

    }

    public class adpterhearing extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public adpterhearing(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;


            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {


                vi = inflater.inflate(R.layout.adapterhearing, null);
                holder = new ViewHolder();

                holder.txt_hearingaddress = (TextView) vi.findViewById(R.id.txt_hearing_address);
                holder.txt_hearing_locatiobn = (TextView) vi.findViewById(R.id.txt_hearing_location);
                holder.txt_court_id = (TextView) vi.findViewById(R.id.text_courtid);
                holder.txt_date = (TextView) vi.findViewById(R.id.text_date);

                vi.setTag(holder);


                list_of_hearing_location.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        SharedPreferences gg = getActivity().getSharedPreferences("vehicleinfo", MODE_PRIVATE);
                        SharedPreferences.Editor d = gg.edit();
                        d.clear();
                        d.commit();
                        textty1.setText("Hearing Location");
                        String address = holder.txt_hearingaddress.getText().toString();
                        String location = holder.txt_hearing_locatiobn.getText().toString();
                        String des = holder.txt_court_id.getText().toString();
                        /*Animation animation = AnimationUtils.loadAnimation(getActivity(),
                                R.anim.sidepannelleft);
                        String date = holder.txt_date.getText().toString();
                        rl_list_hearing.startAnimation(animation);
                        rl_list_violation_type.setVisibility(View.GONE);
                        rl_list_hearing.setVisibility(View.GONE);
                        rl_hearing.setVisibility(View.VISIBLE);
                        txt_hearing_location.setText(location);
                        txt_hearing_address.setText(address);
                        txt_court_id.setText(des);
                        txt_hearing_mutiple.setVisibility(View.GONE);
                        txt_hearing_date.setText(date);

                        date_list.clear();
                        date_list.add(hearingtype.get(position));
                        setdata(date_list);*/
                    }
                });
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
                //holder.txtdate.setText("No Data");

            } else {
                /***** Get each Model object from Arraylist ********/
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                /************  Set Model values in Holder elements ***********/


                Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvetica-normal.ttf");
                Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvetica-neue-medium.ttf");

                holder.txt_hearing_locatiobn.setTypeface(medium);
                holder.txt_court_id.setText(tempValues.getCourt_id());
                if (tempValues.getHearing_address().equals("null")) {
                    holder.txt_hearingaddress.setText("");
                } else {
                    holder.txt_hearingaddress.setText(tempValues.getHearing_address());
                }
                holder.txt_hearing_locatiobn.setText(tempValues.getHearing_location());
                holder.txt_date.setText(tempValues.getDate());
            }

            return vi;


        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txt_hearingaddress, txt_hearing_locatiobn, txt_court_id, txt_date;
        }

    }

    public class getviolationtype extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONObject json, json1;
        String transactionurl = "_table/township_users?filter=user_id%3D" + id + "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            Log.e("vilation_type", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray googleparking = json.getJSONArray("resource");

                    for (int j = 0; j < googleparking.length(); j++) {
                        JSONObject c = googleparking.getJSONObject(j);
                        township_code = c.getString("township_code");
                        String email = c.getString("email");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs;
            if (getActivity() != null && json != null) {
                if (ad != null) {
                    ad.data.clear();
                    violationtype.clear();
                    ad.notifyDataSetChanged();
                }
                new getdetailviolation().execute();


            }

        }
    }

    public class getdetailviolation extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        String transactionurl = "_table/violation_code?userid=" + township_code + "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            violationtype.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray googleparking = json.getJSONArray("resource");
                    for (int j = 0; j < googleparking.length(); j++) {
                        item item = new item();
                        JSONObject c = googleparking.getJSONObject(j);
                        String code = c.getString("violation_code");
                        String fees = c.getString("violation_fee");
                        String description = c.getString("violation_description");
                        item.setVioloation_code(code);
                        item.setViolation_fess(fees);
                        item.setViolation_desciprtion(description);
                        violationtype.add(item);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity != null) {
                    if (violationtype.size() > 0) {
                        ad = new adpterviolationtype(getActivity(), violationtype, rs = getResources());
                        list_of_violation_type.setAdapter(ad);
                    }
                }
            }
        }
    }

    public void registerForListener(list_item_click_viloation listener) {
        this.listener = listener;
    }

}
