package com.softmeasures.eventezlyadminplus.frament;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.location.Address;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsoluteLayout;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.softmeasures.eventezlyadminplus.Custom_contorl.MapWrapperLayout;
import com.softmeasures.eventezlyadminplus.Custom_contorl.OnInfoWindowElemTouchListener;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.services.Constants;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;
import com.softmeasures.eventezlyadminplus.services.GeocodeAddressIntentService;
import com.squareup.picasso.Callback;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;


public class f_search_places extends Fragment {
    private static final String LOG_TAG = "ExampleApp";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyC6UabVHdti4zlU6E1iajVuNi6ZdKhDK5g";
    TextView txttitle;
    GoogleMap googleMap;
    MarkerOptions options;
    double latitude, longitude;
    String filterAddress = "", categoriy;
    ConnectionDetector cd;
    RelativeLayout rl_editname;
    TextView btn_destination, txt_get_cat;
    EditText edit_destition_address, txtaddress;
    TextView txt_search;
    boolean opendes = false;
    String adrreslocation, addressti, addressin;
    String addrsstitle, addresssub;
    ArrayList<item> searcharray = new ArrayList<>();
    ArrayList<item> oldarraylist = new ArrayList<>();
    ArrayList<item> otherparking = new ArrayList<>();
    ArrayList<item> directionarray = new ArrayList<>();
    String categories[] = {"All Categories", "Airport", "Amusement Park", "Aquarium", "ATM", "Bakery", "Bank",
            "Bicycle Store", "Book Store", "Bus Station", "Cafe",
            "Camp Ground", "Car Rental", "Car Repair",
            "Car Wash", "Church", "City Hall", "Convenience Store",
            "Dentist", "Doctor", "Fire Station", "Florist", "Gas Station",
            "Home Goods Store", "Hospital", "Library", "Locksmith",
            "Lodging", "Meal Delivery", "Meal Takeaway", "Museum",
            "Park", "Pharmacy", "Police", "Post Office", "Restaurant",
            "RV Park", "School", "Shopping Mall", "Stadium", "Subway Station", "Taxi Stand",
            "Train Station", "Transit Station", "University", "Veterinary Care", "Zoo"};
    String lat = "null", lang = "null";
    ProgressBar progressBar;
    RelativeLayout rl_progressbar;
    Bitmap mypic;
    ArrayList<item> cat = new ArrayList<>();
    boolean not_first_time_showing_info_window = false, cast_open = false, windowopen = false, mapisloaded = false;
    ArrayList<item> searchplace = new ArrayList<>();
    AutoCompleteTextView autoCompView_des, autoCompView_sor;
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private ViewGroup infoWindow;
    MapWrapperLayout mapWrapperLayout;
    Button txt_direction, txt_maps;
    TextView txt_title, txt_lat, txt_lang;
    ImageView img_photo;
    String click_on_move;
    private OnInfoWindowElemTouchListener infoButtonListener;
    private OnInfoWindowElemTouchListener infoButtonListener_map;

    private static final int POPUP_POSITION_REFRESH_INTERVAL = 16;
    //длительность анимации перемещения карты
    private static final int ANIMATION_DURATION = 500;
    private int markerHeight = 40;
    private AbsoluteLayout.LayoutParams overlayLayoutParams;

    //слушатель, который будет обновлять смещения
    private ViewTreeObserver.OnGlobalLayoutListener infoWindowLayoutListener;

    private Handler handler;

    //Runnable, который обновляет положение окна
    private Runnable positionUpdaterRunnable;

    private LatLng trackedPosition;
    private View infoWindowContainer;
    private int popupXOffset;
    private int popupYOffset;

    AddressResultReceiver mResultReceiver;
    String address_;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.place_search, container, false);
        autoCompView_des = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteTextView);
        autoCompView_des.setAdapter(new CustomAutoplace(getActivity(), getResources()));
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        mapWrapperLayout = (MapWrapperLayout) view.findViewById(R.id.map_relative_layout);

        GPSTracker tracker = new GPSTracker(getActivity().getApplicationContext());
        mResultReceiver = new AddressResultReceiver(null);
        if (tracker.canGetLocation()) {

            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
            String d = String.valueOf(tracker.getLocation());
            showmap();
        }


        final RelativeLayout rl_go = (RelativeLayout) view.findViewById(R.id.rl_go);


        infoWindowContainer = view.findViewById(R.id.container_popup);
        infoWindowLayoutListener = new InfoWindowLayoutListener();
        infoWindowContainer.getViewTreeObserver().addOnGlobalLayoutListener(infoWindowLayoutListener);
        overlayLayoutParams = (AbsoluteLayout.LayoutParams) infoWindowContainer.getLayoutParams();

        txt_direction = (Button) infoWindowContainer.findViewById(R.id.txt_get_directions);
        txt_title = (TextView) infoWindowContainer.findViewById(R.id.text_nearby_name);
        txt_lat = (TextView) infoWindowContainer.findViewById(R.id.txt_lat);
        txt_lang = (TextView) infoWindowContainer.findViewById(R.id.txt_lang);
        txt_maps = (Button) infoWindowContainer.findViewById(R.id.txt_map_apps);
        img_photo = (ImageView) infoWindowContainer.findViewById(R.id.img_photos);
        autoCompView_des.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rl_go.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {
                String aa = autoCompView_des.getText().toString();
                if (aa.equals("")) {
                    rl_go.setVisibility(View.GONE);
                } else {
                    rl_go.setVisibility(VISIBLE);
                }
            }
        });


        rl_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String aa = autoCompView_des.getText().toString();
                if (!aa.equals("")) {
                    categoriy = autoCompView_des.getText().toString();
                    categoriy = categoriy.replace(" ", "_");
                    View viewa = getActivity().getCurrentFocus();
                    if (viewa != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(viewa.getWindowToken(), 0);
                    }
                    new getotherparkinglist().execute();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("Please enter place name");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                }
            }
        });

        autoCompView_des.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    //do something
                    String aa = autoCompView_des.getText().toString();
                    if (!aa.equals("")) {
                        categoriy = autoCompView_des.getText().toString();
                        categoriy = categoriy.replace(" ", "_");
                        View viewa = getActivity().getCurrentFocus();
                        if (viewa != null) {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(viewa.getWindowToken(), 0);
                        }
                        // new getotherparkinglist().execute();
                        Intent intent = new Intent(getActivity(), GeocodeAddressIntentService.class);
                        intent.putExtra(Constants.RECEIVER, mResultReceiver);
                        intent.putExtra(Constants.FETCH_TYPE_EXTRA, Constants.USE_ADDRESS_NAME);
                        intent.putExtra(Constants.LOCATION_NAME_DATA_EXTRA, aa);
                        getActivity().startService(intent);
                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setMessage("Please enter place name");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alertDialog.show();
                    }
                }
                return false;
            }
        });
        autoCompView_des.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String str1 = searchplace.get(position).getLoationname();
                String str = searchplace.get(position).getAddress();
                categoriy = str1;
                autoCompView_des.setText(categoriy);
                categoriy = categoriy.replace(" ", "_");

             /*   if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }*/

                View viewa = getActivity().getCurrentFocus();
                if (viewa != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(viewa.getWindowToken(), 0);
                }

//                new getotherparkinglist().execute();

                Intent intent = new Intent(getActivity(), GeocodeAddressIntentService.class);
                intent.putExtra(Constants.RECEIVER, mResultReceiver);
                intent.putExtra(Constants.FETCH_TYPE_EXTRA, Constants.USE_ADDRESS_NAME);
                intent.putExtra(Constants.LOCATION_NAME_DATA_EXTRA, str1 + "," + str);
                getActivity().startService(intent);

            }
        });

        txt_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Toast.makeText(getActivity(), txt_title.getText().toString(), Toast.LENGTH_SHORT).show();
                String lat = txt_lat.getText().toString();
                String lang = txt_lang.getText().toString();
                f_direction mParentFragment = (f_direction) getParentFragment();
                mParentFragment.show_end_address(String.valueOf(lat), String.valueOf(lang));
                infoWindowContainer.setVisibility(INVISIBLE);
            }
        });
        txt_maps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String lat = txt_lat.getText().toString();
                String lang = txt_lang.getText().toString();
                Uri gmmIntentUri = Uri.parse("geo:" + lat + "," + lang + "");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });
        return view;
    }


    public static int getPixelsFromDp(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    @Override
    public void onResume() {
        GPSTracker tracker = new GPSTracker(getActivity().getApplicationContext());
        if (tracker.canGetLocation()) {

            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
        }
        super.onResume();
    }

    public class CustomAutoplace extends BaseAdapter implements View.OnClickListener, Filterable {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAutoplace(Activity a, Resources resLocal) {

            activity = a;
            context = a;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.layout, null);
                holder = new ViewHolder();

                holder.txt_commerical_name = (TextView) vi.findViewById(R.id.txt_name);
                holder.txt_deatil = (TextView) vi.findViewById(R.id.address);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }
            if (data.size() <= 0) {


            } else {
                try {
                    tempValues = null;
                    tempValues = (item) data.get(position);
                    holder.txt_commerical_name.setText(tempValues.getLoationname());
                    holder.txt_deatil.setText(tempValues.getAddress());
                    int k = 0;
                } catch (IndexOutOfBoundsException e) {

                } catch (Exception e) {
                }
            }

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        data = autocomplete(constraint.toString());
                        filterResults.values = data;
                        filterResults.count = data.size();
                        searchplace.clear();
                        searchplace.addAll(data);
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;

        }


        public class ViewHolder {

            public TextView txt_commerical_name, txt_deatil;
        }
    }

    public class getotherparkinglist extends AsyncTask<String, String, String> {
        JSONObject json, json1;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            otherparking.clear();

            String url1;
            if (latitude == 0.0) {
                url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=40.74599864,-73.9825673&&rankby=distance&distance=40000.0&sensor=true&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&name=" + categoriy + "";
            } else {
                if (lat.equals("null") || lang.equals("null")) {
                    url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&rankby=distance&distance=40000.0&sensor=true&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&name=" + categoriy + "";
                } else {
                    url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + latitude + "&rankby=distance&distance=40000.0&sensor=true&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&name=" + categoriy + "";
                    Log.e("url search__location", url1);
                }
            }
            Log.e("url", url1);
            DefaultHttpClient client1 = new DefaultHttpClient();
            HttpGet post1 = new HttpGet(url1);


            HttpResponse response1 = null;
            try {
                response1 = client1.execute(post1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response1 != null) {

                HttpEntity resEntity1 = response1.getEntity();
                String responseStr1 = null;
                try {
                    responseStr1 = EntityUtils.toString(resEntity1).trim();
                    json1 = new JSONObject(responseStr1);
                    JSONArray googleparking = json1.getJSONArray("results");

                    for (int j = 0; j < googleparking.length(); j++) {
                        item item = new item();
                        JSONObject google = googleparking.getJSONObject(j);
                        String icon = google.getString("icon");
                        String photore = null;
                        if (google.has("photos")) {
                            String photo = google.getString("photos");
                            JSONArray photor = new JSONArray(photo);
                            JSONObject google1 = photor.getJSONObject(0);
                            photore = google1.getString("photo_reference");
                            Log.e("php", photore);
                            Log.e("icon", icon);
                        }


                        String geo = google.getString("geometry");
                        JSONObject geometry = new JSONObject(geo);
                        String location = geometry.getString("location");
                        JSONObject lant = new JSONObject(location);
                        String lat = lant.getString("lat");
                        String lang = lant.getString("lng");
                        String name = google.getString("name");
                        String vicinity = google.getString("vicinity");
                        Log.e("VICIONIITY", name);
                        item.setGooglelat(lat);
                        item.setGooglelan(lang);
                        item.setName(name);
                        item.setVicinity(vicinity);
                        item.setIcon(icon);
                        item.setPhoto_reference(photore);
                        otherparking.add(item);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            Resources rs;
            if (getActivity() != null && json1 != null) {
                if (otherparking.size() > 0) {
                    googleMap.clear();
                    new gtbitmmp().execute();
                } else {
                    rl_progressbar.setVisibility(View.GONE);
                }
            } else {
                rl_progressbar.setVisibility(View.GONE);
            }
            super.onPostExecute(s);
        }
    }

    private void showmap() {
        final List<Integer> skipIds = new ArrayList<>();
        SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        /*for (int i = 0; i < otherparking.size(); i++) {
            LatLng position = new LatLng(Double.parseDouble(otherparking.get(i).getGooglelat()), Double.parseDouble(otherparking.get(i).getGooglelan()));
            final MarkerOptions options = new MarkerOptions();
            options.position(position);
            options.icon(BitmapDescriptorFactory.fromBitmap(oldarraylist.get(i).getBitmap()));
            options.title(otherparking.get(i).getName());
            options.snippet(otherparking.get(i).getPhoto_reference());
            googleMap = fm.getMap();
            googleMap.setMyLocationEnabled(true);
            googleMap.addMarker(options);
        }*/

        fm.getMapAsync(googleMap1 -> {
            googleMap = googleMap1;
            LatLng position = null;
            if (lang.equals("null") && lat.equals("null")) {
                position = new LatLng(latitude, longitude);
            } else {
                position = new LatLng(Double.parseDouble(lat), Double.parseDouble(lang));
                final MarkerOptions options = new MarkerOptions();
                options.position(position);
                //options.icon(BitmapDescriptorFactory.fromBitmap(oldarraylist.get(i).getBitmap()));
                options.title(address_);
                options.snippet("");
                googleMap.addMarker(options);
            }
            options = new MarkerOptions();
            options.position(position);
            mapWrapperLayout.init(googleMap, getPixelsFromDp(getActivity(), 39 + 20));
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
            }
            googleMap.setMyLocationEnabled(true);
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(position));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(12));
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            View locationButton = ((View) fm.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            rlp.setMargins(0, 0, 30, 50);
            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    trackedPosition = marker.getPosition();
                    Projection projection = googleMap.getProjection();
                    Point trackedPoint = projection.toScreenLocation(trackedPosition);
                    trackedPoint.y -= popupYOffset / 2;
                    LatLng newCameraLocation = projection.fromScreenLocation(trackedPoint);
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(newCameraLocation), ANIMATION_DURATION, null);
                    txt_title.setText(marker.getTitle());
                    double lat = marker.getPosition().latitude;
                    double lang = marker.getPosition().longitude;
                    txt_lat.setText(String.valueOf(lat));
                    txt_lang.setText(String.valueOf(lang));
                    String url = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=100&photoreference=" + marker.getSnippet() + "&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA";
                    Glide.with(getActivity()).load(url).into(img_photo);
                    infoWindowContainer.setVisibility(VISIBLE);
                    return true;
                }
            });

            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    infoWindowContainer.setVisibility(INVISIBLE);
                }
            });
        });

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //очистка
        handler = new Handler(Looper.getMainLooper());
        positionUpdaterRunnable = new PositionUpdaterRunnable();
        handler.post(positionUpdaterRunnable);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        infoWindowContainer.getViewTreeObserver().removeGlobalOnLayoutListener(infoWindowLayoutListener);
        handler.removeCallbacks(positionUpdaterRunnable);
        handler = null;
    }

    private class InfoWindowRefresher implements Callback {
        private Marker markerToRefresh;

        private InfoWindowRefresher(Marker markerToRefresh) {
            this.markerToRefresh = markerToRefresh;
        }

        @Override
        public void onSuccess() {
            markerToRefresh.showInfoWindow();

        }

        @Override
        public void onError() {
        }
    }

    public class gtbitmmp extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(Bitmap... params) {
            try {
                oldarraylist.clear();
                for (int i = 0; i < otherparking.size(); i++) {
                    URL url = new URL(otherparking.get(i).getIcon());
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap myBitmap = BitmapFactory.decodeStream(input);
                    mypic = myBitmap;
                    item ii = new item();
                    Bitmap mybit = getResizedBitmap(myBitmap, 40, 40);
                    Log.e("bitmap", String.valueOf(mybit));
                    ii.setBitmap(mybit);
                    oldarraylist.add(ii);
                }
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getArguments() != null && oldarraylist.size() > 0) {
                Activity activity = getActivity();
                if (activity == null) {
                } else {
                    showmap();
                }
            }
            super.onPostExecute(bitmap);
        }
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public ArrayList<item> autocomplete(String input) {
        ArrayList<item> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?location=" + latitude + "," + longitude);
            sb.append("&radius=50000.0");
            sb.append("&key=" + API_KEY);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
            resultList = new ArrayList<item>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                String cghcbdc = predsJsonArray.getJSONObject(i).getString("description");
                if (cghcbdc.contains(",")) {
                    String street = cghcbdc.substring(0, cghcbdc.indexOf(","));
                    String addrress = cghcbdc.substring(cghcbdc.indexOf(",") + 1);
                    item ii = new item();
                    ii.setLoationname(street);
                    ii.setAddress(addrress);
                    resultList.add(ii);
                }
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }


    private class InfoWindowLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {
        @Override
        public void onGlobalLayout() {
            //размеры окна изменились, обновляем смещения
            popupXOffset = infoWindowContainer.getWidth() / 2;
            popupYOffset = infoWindowContainer.getHeight();
        }
    }

    private class PositionUpdaterRunnable implements Runnable {
        private int lastXPosition = Integer.MIN_VALUE;
        private int lastYPosition = Integer.MIN_VALUE;

        @Override
        public void run() {
            SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            //помещаем в очередь следующий цикл обновления
            handler.postDelayed(this, POPUP_POSITION_REFRESH_INTERVAL);

            fm.getMapAsync(googleMap1 -> {
                //если всплывающее окно скрыто, ничего не делаем
                if (trackedPosition != null && infoWindowContainer.getVisibility() == VISIBLE) {
                    Point targetPosition = googleMap1.getProjection().toScreenLocation(trackedPosition);

                    //если положение окна не изменилось, ничего не делаем
                    if (lastXPosition != targetPosition.x || lastYPosition != targetPosition.y) {
                        //обновляем положение
                        overlayLayoutParams.x = targetPosition.x - popupXOffset;
                        overlayLayoutParams.y = targetPosition.y - popupYOffset - markerHeight - 30;
                        infoWindowContainer.setLayoutParams(overlayLayoutParams);

                        //запоминаем текущие координаты
                        lastXPosition = targetPosition.x;
                        lastYPosition = targetPosition.y;
                    }
                }
            });

        }
    }

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, final Bundle resultData) {
            if (resultCode == Constants.SUCCESS_RESULT) {
                final Address address = resultData.getParcelable(Constants.RESULT_ADDRESS);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rl_progressbar.setVisibility(View.GONE);
                        /*infoText.setVisibility(View.VISIBLE);

                        infoText.setText("Latitude: " + address.getLatitude() + "\n" +
                                "Longitude: " + address.getLongitude() + "\n" +
                                "Address: " + resultData.getString(Constants.RESULT_DATA_KEY));*/

                        lat = String.valueOf(address.getLatitude());
                        lang = String.valueOf(address.getLongitude());
                        address_ = resultData.getString(Constants.RESULT_DATA_KEY);
                        showmap();

                    }
                });
            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rl_progressbar.setVisibility(View.GONE);
                       /* infoText.setVisibility(View.VISIBLE);
                        infoText.setText(resultData.getString(Constants.RESULT_DATA_KEY));*/
                    }
                });
            }
        }
    }
}