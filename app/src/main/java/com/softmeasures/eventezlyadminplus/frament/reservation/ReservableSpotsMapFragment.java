package com.softmeasures.eventezlyadminplus.frament.reservation;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class ReservableSpotsMapFragment extends BaseFragment {

    private GoogleMap googleMap;
    private RelativeLayout rlProgressbar;
    private ArrayList<item> townshipPartners = new ArrayList<>();
    private ArrayList<item> townshipPartnersFiltered = new ArrayList<>();
    private ArrayList<item> townshipParkingRules = new ArrayList<>();

    private Double latitude = 0.0, longitude = 0.0;
    private item updateParkingRule;
    private String locationId = "";

    private RecyclerView rvPartners;
    private PartnerAdapter partnerAdapter;
    private String type = "";
    private SearchView searchView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            type = getArguments().getString("type");
        }
        return inflater.inflate(R.layout.frag_reservable_spots_map, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        prepareMap();

        new fetchTownshipManagedPartners().execute();

        getCurrentLocation();
    }

    public void getCurrentLocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Failed to fetch user location!");
            alertDialog.setMessage("Please enabl e user location for app, location is required for app to work properly");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });

            alertDialog.show();
        }
    }

    private class fetchTownshipManagedPartners extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json = new JSONObject();
        JSONArray json1 = new JSONArray();
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String tcode = logindeatl.getString("twcode", "FDV");
        String role = logindeatl.getString("role", "");

        String managedlosturl = "_table/township_parking_partners?filter=(township_id%3D" + tcode + ")%20AND%20((marker_type%3DManaged%20Paid)%20OR%20(marker_type%3DManaged%20Free))";

        @Override
        protected void onPreExecute() {
            String typeUrl = "";
            if (type.equals("Commercial")) {
                typeUrl = "_table/google_parking_partners";
            } else if (type.equals("Township")) {
                typeUrl = "_table/township_parking_partners";
            } else if (type.equals("Other")) {
                typeUrl = "_table/other_parking_partners";
            }
            role = logindeatl.getString("role", "");
            if (!TextUtils.isEmpty(role) && role.equals("SuperAdmin")) {
                managedlosturl = typeUrl + "?filter=(marker_type%3DManaged%20Paid)%20OR%20(marker_type%3DManaged%20Free)";
            } else {
                managedlosturl = typeUrl + "?filter=(township_id%3D" + tcode + ")%20AND%20((marker_type%3DManaged%20Paid)%20OR%20(marker_type%3DManaged%20Free))";
            }
            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            townshipPartners.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("#DEBUG", "  fetchTownshipManagedPartners: " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    json1 = json.getJSONArray("resource");

                    for (int i = 0; i < json1.length(); i++) {
                        item item = new item();
                        JSONObject josnget = json1.getJSONObject(i);
                        String lat = josnget.getString("lat");
                        String lag = josnget.getString("lng");
                        String id = josnget.getString("id");
                        String title = josnget.getString("title");
                        String address = josnget.getString("address");
                        String html = "";
                        String category = "";
                        String marker = josnget.getString("marker_type");
                        String location_code = josnget.getString("location_code");
                        String location_id = josnget.getString("location_id");
                        String location_name = josnget.getString("location_name");
                        if (marker.equals("Managed Paid") || marker.equals("Managed Free")) {
                            item.setId(id);
                            item.setLots_aval(josnget.getString("lots_avbl"));
                            item.setTotal_lots(josnget.getString("lots_total"));
                            item.setNo_parking_times(josnget.getString("no_parking_times"));
                            item.setParking_times(josnget.getString("parking_times"));
                            item.setRenew(josnget.getString("renewable"));
                            item.setWeek_ebd_discount(josnget.getString("weekend_special_diff"));
                            item.setOff_peak_discount(josnget.getString("off_peak_discount"));
                            item.setOff_peak_start(josnget.getString("off_peak_starts"));
                            item.setOff_peak_end(josnget.getString("off_peak_ends"));
                            item.setCustom_notice(josnget.getString("custom_notice"));
                            item.setEffect(josnget.getString("in_effect"));
                            item.setLocation_name(location_name);
                            if (josnget.has("location_place_id")
                                    && !TextUtils.isEmpty(josnget.getString("location_place_id"))
                                    && !josnget.getString("location_place_id").equals("null"))
                                item.setLocation_place_id(josnget.getString("location_place_id"));
                            else item.setLocation_place_id("");
                            item.setLang(lag);
                            item.setLat(lat);
                            item.setTitle(title);
                            item.setAddress(address);
                            item.setUrl("");
                            item.setHtml(html);
                            item.setCategory(category);
                            item.setMarker(marker);
                            item.setLocation_code(location_code);
                            item.setLocation_id(location_id);
                            townshipPartners.add(item);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null) {
                rlProgressbar.setVisibility(View.GONE);
                //progressBar.setVisibility(View.GONE);
                if (json != null) {
                    if (townshipPartners != null) {
                        if (townshipPartners.size() > 0) {
//                            showMapTownshipPartners();
                            townshipPartnersFiltered.clear();
                            townshipPartnersFiltered.addAll(townshipPartners);
                            partnerAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    private void showMapTownshipPartners() {
        if (googleMap != null) {

            final LatLng currentPosition = new LatLng(latitude, longitude);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentPosition, 14));

            MarkerOptions options = new MarkerOptions();
            for (int i = 0; i < townshipPartners.size(); i++) {
                final LatLng position = new LatLng(Double.parseDouble(townshipPartners.get(i).getLat()),
                        Double.parseDouble(townshipPartners.get(i).getLang()));
                options.position(position);

                options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.park_managed_circle));
                options.title("township");
                options.snippet(townshipPartners.get(i).getLat() + "-" + townshipPartners.get(i).getLang());
                googleMap.addMarker(options);
            }

            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {

                    if (townshipPartners.size() > 0) {
                        String location_code = "";
                        LatLng latLng = marker.getPosition();
                        String title = marker.getTitle();
                        String lang = marker.getSnippet();
                        String marker_lat = null, marker_lang = null;
                        if (lang.contains("-")) {
                            marker_lat = lang.substring(0, lang.indexOf("-"));
                            marker_lang = lang.substring(lang.indexOf("-") + 1);
                        }
                        for (int i = 0; i < townshipPartners.size(); i++) {
                            String mp_lat = townshipPartners.get(i).getLat();
                            String map_lang = townshipPartners.get(i).getLang();
                            if (map_lang.equals(marker_lang) && marker_lat.equals(mp_lat)) {

                                Log.e("#DEBUG", "  onClickMarker:  " + new Gson().toJson(townshipPartners.get(i)));
                                locationId = townshipPartners.get(i).getLocation_id();
                                new fetchTownshipParkingRules(townshipPartners.get(i).getLocation_place_id()).execute();

//                                if (mak.equals("Managed Paid") || mak.equals("Managed Free")) {
//                                    new getmanaged_location(location_code, mp_lat, marker_lang, address, title, html, mak, no_parking_time, parking_time, notices, towship_code, location_id).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                                } else {
//                                    showtitlePopup(vchome.this, location_code, title, address, html, mp_lat, marker_lang, mak, parking_time, no_parking_time, notices, towship_code, location_id);
//                                }
                                break;
                            }
                        }

                    }

                    return true;
                }
            });
        }
    }

    public class fetchTownshipParkingRules extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        String parkingurl = "";

        public fetchTownshipParkingRules(String loc) {
            this.location = loc;
            try {
                this.parkingurl = "_table/township_parking_rules?filter=location_place_id%3D" + URLEncoder.encode(String.valueOf(loc), "utf-8") + "";
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            townshipParkingRules.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
//                        item1.setLoationname(title);
                        item1.setTime_rule(c.getString("time_rule"));
                        item1.setPricing_duration(c.getString("pricing_duration"));
                        item1.setDuration_unit(c.getString("duration_unit"));
                        item1.setMax_time(c.getString("max_duration"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        String time_rules = c.getString("time_rule");
                        item1.setPricing(c.getString("pricing"));
                        item1.setEffect(c.getString("in_effect"));
                        item1.setStart_hour(c.getString("start_time"));
                        item1.setEnd_hour(c.getString("end_time"));
                        item1.setId(c.getString("id"));
                        item1.setActive(c.getString("active"));
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setLocation_place_id(c.getString("location_place_id"));
                        if (time_rules.contains("-")) {
                            String start_time = time_rules.substring(0, time_rules.indexOf("-"));
                            String end_time = time_rules.substring(time_rules.indexOf("-") + 1);
                            item1.setStart_time(start_time);
                            item1.setEnd_time(end_time);
                        } else {
                            item1.setStart_time("null");
                            item1.setEnd_time("null");
                        }

                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item1.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }

                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item1.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }

                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item1.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }

                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item1.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }

                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item1.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }

                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item1.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item1.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }

                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item1.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }

                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item1.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }

                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item1.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }

                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item1.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }

                        if (c.has("number_of_reservable_spots")
                                && !TextUtils.isEmpty(c.getString("number_of_reservable_spots"))
                                && !c.getString("number_of_reservable_spots").equals("null")) {
                            item1.setNumber_of_reservable_spots(c.getString("number_of_reservable_spots"));
                        }

                        townshipParkingRules.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null) {
                rlProgressbar.setVisibility(View.GONE);
                if (json != null) {
                    if (townshipParkingRules != null) {
                        if (townshipParkingRules.size() > 0) {
                            // display or edit number_of_reservable_spots
                            showDialogUpdateNoOfReservableSpots();
                        }
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    private void showDialogUpdateNoOfReservableSpots() {
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        View dialogView = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_update_no_of_reservable_spots, null);
        builder.setView(dialogView);
        final EditText etNoOfReservableSpots = dialogView.findViewById(R.id.etNoOfReservableSpots);
        if (!TextUtils.isEmpty(townshipParkingRules.get(0).getNumber_of_reservable_spots())) {
            etNoOfReservableSpots.setText(townshipParkingRules.get(0).getNumber_of_reservable_spots());
            etNoOfReservableSpots.setSelection(townshipParkingRules.get(0).getNumber_of_reservable_spots().length());
        }
        Button btnMarkReservableSpot = dialogView.findViewById(R.id.btnMarkReservableSpot);
        Button btnUpdateReservableSpots = dialogView.findViewById(R.id.btnUpdateReservableSpots);
        final ImageView ivBtnClose = dialogView.findViewById(R.id.ivBtnClose);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();

        ivBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        btnMarkReservableSpot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                showDialogLocationLots(etNoOfReservableSpots.getText().toString());
            }
        });

        btnUpdateReservableSpots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String noOfReservableSpots = etNoOfReservableSpots.getText().toString();
                if (TextUtils.isEmpty(noOfReservableSpots)) {
                    Toast.makeText(getActivity(), "Please enter number of reservable spots!", Toast.LENGTH_SHORT).show();
                } else {
                    updateParkingRule = new item();
                    updateParkingRule.setId(townshipParkingRules.get(0).getId());
                    updateParkingRule.setNumber_of_reservable_spots(noOfReservableSpots);
                    showUpdateConfirmationDialog();
                }
            }
        });

        alertDialog.show();
    }

    private void showUpdateConfirmationDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure, you want to update number of reservable spots?");
        builder.setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                new updateTownshipParkingRule().execute();
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public class updateTownshipParkingRule extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        JSONObject json12 = null;
        String walleturl = "_table/township_parking_rules";
        String id = "";
        String dat_type = "";

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();


            jsonValues1.put("id", updateParkingRule.getId());
            jsonValues1.put("number_of_reservable_spots", updateParkingRule.getNumber_of_reservable_spots());

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   updateTownshipParkingRule:  Response:  " + responseStr);
                    json12 = new JSONObject(responseStr);
                    if (json12.has("resource")) {
                        JSONArray array = json12.getJSONArray("resource");
                        for (int i = 0; i < array.length(); i++) {

                            JSONObject c = array.getJSONObject(i);
                            id = c.getString("id");
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            //progressBar.setVisibility(View.GONE);
            if (getActivity() != null) {
                rlProgressbar.setVisibility(View.GONE);
                if (json12 != null) {
                    if (json12.has("error")) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setMessage("Error");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alertDialog.show();
                    } else {
                        if (!id.equals("")) {
                            Toast.makeText(getActivity(), "Updated Successfully!", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            }

            super.onPostExecute(s);
        }
    }

    //TODO display location lots
    private void showDialogLocationLots(String noOfReservableSpots) {

        ((vchome) getActivity()).show_back_button();
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        MarkReservableSpotsFragment pay = new MarkReservableSpotsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("locationId", locationId);
        bundle.putString("noOfReservableSpots", noOfReservableSpots);
        pay.setArguments(bundle);
        ft.add(R.id.My_Container_1_ID, pay);
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(pay);
        ft.commitAllowingStateLoss();


    }

    private void prepareMap() {
        final SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (fm != null) {
            fm.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap map) {
                    googleMap = map;
                    View locationButton = ((View) fm.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                    RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                    rlp.setMargins(0, 0, 30, 200);

                    displayMapDetails();
                }
            });
        }
    }

    private void displayMapDetails() {

    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                partnerAdapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                partnerAdapter.getFilter().filter(s);
                return false;
            }
        });

    }

    @Override
    protected void initViews(View v) {
        rlProgressbar = v.findViewById(R.id.rlProgressbar);
        rvPartners = v.findViewById(R.id.rvPartners);
        searchView = v.findViewById(R.id.searchView);

        partnerAdapter = new PartnerAdapter();
        rvPartners.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvPartners.setAdapter(partnerAdapter);

    }

    private class PartnerAdapter extends RecyclerView.Adapter<PartnerAdapter.PartnerHolder> implements Filterable {

        @NonNull
        @Override
        public PartnerAdapter.PartnerHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new PartnerHolder(LayoutInflater.from(getActivity())
                    .inflate(R.layout.item_partner, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull PartnerAdapter.PartnerHolder partnerHolder, int i) {
            item partner = townshipPartnersFiltered.get(i);
            if (partner != null) {
                if (!TextUtils.isEmpty(partner.getLocation_code())) {
                    partnerHolder.tvLocationName.setText(partner.getLocation_code());
                    if (!TextUtils.isEmpty(partner.getLocation_id())) {
                        partnerHolder.tvLocationName.setText(String.format("%s (%s)",
                                partner.getLocation_code(), partner.getLocation_id()));
                    }
                }
                if (!TextUtils.isEmpty(partner.getAddress())) {
                    partnerHolder.tvAddress.setText(partner.getAddress());

                }

                if (!TextUtils.isEmpty(partner.getLocation_name())
                        && !partner.getLocation_name().equals("null")) {
                    partnerHolder.tvLocationId.setVisibility(View.VISIBLE);
                    partnerHolder.tvLocationId.setText(partner.getLocation_name());
                } else {
                    partnerHolder.tvLocationId.setVisibility(View.GONE);
                }
            }

            if (i % 2 == 0) {
                partnerHolder.llParent.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white_1000));
            } else {
                partnerHolder.llParent.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.grey_300));
            }
        }

        @Override
        public int getItemCount() {
            return townshipPartnersFiltered.size();
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {
                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        townshipPartnersFiltered = townshipPartners;
                    } else {
                        ArrayList<item> filteredList = new ArrayList<>();
                        for (item row : townshipPartners) {

                            // name match condition. this might differ depending on your requirement
                            // here we are looking for name or phone number match
                            if (row.getLocation_name().toLowerCase().contains(charString.toLowerCase())) {
                                filteredList.add(row);
                            }
                        }

                        townshipPartnersFiltered = filteredList;
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = townshipPartnersFiltered;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    townshipPartnersFiltered = (ArrayList<item>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }

        class PartnerHolder extends RecyclerView.ViewHolder {
            TextView tvLocationName, tvAddress, tvLocationId;
            LinearLayout llParent;

            PartnerHolder(@NonNull View itemView) {
                super(itemView);
                llParent = itemView.findViewById(R.id.llParent);
                tvLocationName = itemView.findViewById(R.id.tvLocationName);
                tvLocationId = itemView.findViewById(R.id.tvLocationId);
                tvAddress = itemView.findViewById(R.id.tvAddress);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        locationId = townshipPartnersFiltered.get(getAdapterPosition()).getLocation_id();
                        ((vchome) getActivity()).show_back_button();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        MarkReservableSpotsFragment pay = new MarkReservableSpotsFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("locationId", locationId);
                        bundle.putString("type", type);
                        bundle.putString("noOfReservableSpots", "0");
                        pay.setArguments(bundle);
                        ft.add(R.id.My_Container_1_ID, pay);
                        fragmentStack.lastElement().onPause();
                        ft.hide(fragmentStack.lastElement());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                    }
                });
            }
        }


    }
}
