package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.dialogs.PaymentOptionDialog;
import com.softmeasures.eventezlyadminplus.dialogs.PaymentOptionListener;
import com.softmeasures.eventezlyadminplus.frament.reservation.ReservedParkingFragment;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;
import com.softmeasures.eventezlyadminplus.services.NotificationPublisher;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static android.content.Context.ALARM_SERVICE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.adrreslocation;
import static com.softmeasures.eventezlyadminplus.activity.vchome.all_state;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class Google_Paid_Parking extends Fragment implements vehicle_add.vehivcle_add_Click_listener, PaymentOptionListener {

    ProgressDialog pdialog;
    ArrayList<item> listofvehicle = new ArrayList<>();
    ArrayList<item> wallbalarray = new ArrayList<>();
    ListView listofvehicleno;
    TextView txttime, txtmaxhores, txtrate, txt_parking_location_lable, txt_title;
    EditText editno;
    Spinner spsate;
    int i = 1;
    View layout;
    PopupWindow popup;
    String rate1;
    Map<String, Object> statefullname;
    ConnectionDetector cd;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar, rl_sp_row, rl_sp_space;
    ArrayList<String> values = new ArrayList<>();
    double finalpayamount, newbal, subTotal;
    Double tr_percentage1 = 0.0, tr_percentage_for_parked_cars = 0.0, tr_fee1 = 0.0, latitude = 0.0, longitude = 0.0;
    int hours;
    RelativeLayout showsucessmesg;
    SimpleDateFormat sdf;
    Spinner sphoures, sp_row, sp_space;
    RelativeLayout rl_main;
    Animation animation;
    String statename = "", shoues, rate, adrreslocation1 = "null", countryname, nbal, cbal = "0", location_code, township_code = "null", ip, currentdate, finallab, id, platno, state, city, zipcode, locationname, max_time, lat, lang, zip_code, user_id, managedpin, token1, locationaddress, pid, marker, title, p_country;
    String lost_row, lost_no, parkingtype, township_code_final = "", lost_row1, lost_no1, min, total_hours, renew, duration_unit, week_end_discount, off_peak_discount;
    int token = 0;
    RelativeLayout rl_title;
    String Renew_date, ReNew_id, Renew_parked_time, Renew_is_valid = "", expiretime;
    int final_count = 0;
    // String statearray[]={ "State","AK","AL","AR","AZ","CA","CO","CT","DC","DE","FL", "GA", "HI" ,"IA" ,"ID"  ,"IL"  , "IN" , "KS" , "KY", "LA","ME", "MD", "MA","MI","MN" ,"MS"   ,"MO", "MT", "NE","NV", "NH", "NJ", "NM", "NY","NC","ND","OH","OK","OR",  "PA", "RI", "SC",  "SD","TN", "TX" ,  "UT", "VT","VA","WA" ,"WV", "WI","WY"};
    String row[] = {"ROW", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    ArrayList<String> space = new ArrayList<>();
    //////////////////paypal///////////////////
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    private static final String TAG = "paymentQlighting";
    private static final String CONFIG_CLIENT_ID = "AQXvOmlRwvO2AI6H3XzqTe14pp4x6VtP2skKfUkhgjRAoNj0NoT7l0ZGeaRAXkAtYqyPCmxKEUXkrT89";
    private static final int REQUEST_CODE_PAYMENT = 2;
    private static PayPalConfiguration config = new PayPalConfiguration().environment(CONFIG_ENVIRONMENT).clientId(CONFIG_CLIENT_ID);
    boolean isclickonwallet = false;
    boolean isClickPayPal = false;
    boolean isClickReserved = false;
    String paymentmethod = "", transection_id, wallet_id, location_id;
    ArrayList<item> other_parking_rule = new ArrayList<item>();
    TextView txt_state, txt_hours, txt_row, txt_space;
    TextView txt_plate_no;
    final ArrayList<item> state_array = new ArrayList<item>();
    ArrayList<item> stateq = new ArrayList<item>();
    public PopupWindow popup_state;
    private int parkingQty;
    private double parkingUnits;
    private double parkingRate;
    private TextView tvBtnParkNow, tvBtnReserveNow;
    private LinearLayout llParkOptions;
    private boolean isPayLaterClick = false;
    private boolean isReserved = false;
    private String reserveEntryTime = "";
    private String reserveExpiryTime = "";
    private String reserveCurrentTime = "";
    private int intHour = 0;

    private boolean isReservationAllowed = false;
    private boolean isPrePaymentRequiredForReservation = true;
    private String reservationPostPayTerms = "";

    private boolean isParkNowPostPayAllowed = false;
    private String parkNowPostPayTerms = "";

    private String parkingType = "";

    private boolean isPrePaid = false;

    private String companyId = "", twpId = "";
    private String marker_type = "";

    //For new changes
    public static boolean mIsParking = false; // parking = true, reserve = false
    private boolean mIsPayNow = false; //payNow = true, payLater = false
    private boolean mIsWalletPayment = true; //pay with wallet = true, make payment = false

    private double mTrFee = 0.0, mTrPercent = 0.0, mParkingSubTotal = 0.0, mParkingTotal = 0.0,
            mParkNowPostPayFees = 0.0, mReserveNowPostPayFees = 0.0;
    private String plateState = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        if (getArguments() != null) {
            parkingType = getArguments().getString("parkingType");
            Log.e("#DEBUG", "   paidparkhere:  parkingType:  " + parkingType);
        }

        View view = inflater.inflate(R.layout.fragment_google_paidparkhere, container, false);

        llParkOptions = view.findViewById(R.id.llParkOptions);
        tvBtnParkNow = view.findViewById(R.id.tvBtnParkNow);
        tvBtnReserveNow = view.findViewById(R.id.tvBtnReserveNow);

        txt_plate_no = (TextView) view.findViewById(R.id.txt_plate);
        rl_title = (RelativeLayout) view.findViewById(R.id.title);
        txttime = (TextView) view.findViewById(R.id.parkheretime);
        txt_title = (TextView) view.findViewById(R.id.txttitte);
        txtmaxhores = (TextView) view.findViewById(R.id.parkheremaxhours);
        txtrate = (TextView) view.findViewById(R.id.rate);
        rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        // txtpaypark = (TextView) view.findViewById(R.id.paypark);
        editno = (EditText) view.findViewById(R.id.editparkherenoplantno);
        spsate = (Spinner) view.findViewById(R.id.spsatename);
        //sp_row = (Spinner) view.findViewById(R.id.sp_row);
        //sp_space = (Spinner) view.findViewById(R.id.sp_space);
        showsucessmesg = (RelativeLayout) view.findViewById(R.id.sucessfully);
        rl_sp_row = (RelativeLayout) view.findViewById(R.id.rl_sp_row);
        rl_sp_space = (RelativeLayout) view.findViewById(R.id.rl_sp_space);
        txt_parking_location_lable = (TextView) view.findViewById(R.id.txt_parkinglocation);

        txt_state = (TextView) view.findViewById(R.id.txt_state);
        txt_hours = (TextView) view.findViewById(R.id.txt_hours);

        SharedPreferences logindeatl11 = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = logindeatl11.getString("id", "null");


        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Thin.ttf");
        SharedPreferences s = getActivity().getSharedPreferences("paidparkhere", Context.MODE_PRIVATE);
        String time = s.getString("Time", "");
        final String max = s.getString("Max", "");
        max_time = s.getString("Max", "");
        rate = s.getString("rate", "");
        lat = s.getString("lat", "");
        lang = s.getString("lang", "");
        zip_code = s.getString("zip_code", "");
        rate1 = s.getString("rated", "");
        location_code = s.getString("location_code", "");
        locationname = s.getString("location_name", "");
        zipcode = s.getString("zip_code", "");
        city = s.getString("city", "");
        pid = s.getString("id", "null");
        managedpin = s.getString("managedloick", "null");
        locationaddress = s.getString("address", "null");
        marker = s.getString("mar", "null");
        Log.e("#DEBUG", "   paidparkhere:  marker:  " + marker);
        title = s.getString("title", "null");
        duration_unit = s.getString("duation_unit", "Hours");
        marker_type = s.getString("parking_type", "");
        week_end_discount = s.getString("weekend_discount", "0");
        location_id = s.getString("location_id", "0");
        off_peak_discount = s.getString("off_peack", "");

        cd = new ConnectionDetector(getActivity());
        txt_title.setText(title);
        //txttime.setText(locationaddress);
        if (!time.equals("")) {
            txttime.setText(time);
            if (!max.equals("")) {
                txtmaxhores.setText(String.format("Max: %s %s", max, duration_unit));
                if (!max.equals("1")) {
                    if (duration_unit.equals("Hour")) {
                        txtmaxhores.setText(String.format("Max: %s Hours", max));
                    } else if (duration_unit.equals("Minute")) {
                        txtmaxhores.setText(String.format("Max: %s Minutes", max));
                    } else if (duration_unit.equals("Day")) {
                        txtmaxhores.setText(String.format("Max: %s Days", max));
                    } else if (duration_unit.equals("Week")) {
                        txtmaxhores.setText(String.format("Max: %s Weeks", max));
                    } else if (duration_unit.equals("Month")) {
                        txtmaxhores.setText(String.format("Max: %s Months", max));
                    }
                }
            }
        }
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        sphoures = (Spinner) view.findViewById(R.id.sphours);
        values.add("hours");
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        id = logindeatl.getString("id", "null");
        RelativeLayout rl = (RelativeLayout) view.findViewById(R.id.rlrl);
        llParkOptions.setVisibility(View.GONE);
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        space.add(("SPACE#"));
        for (int i = 1; i <= 100; i++) {
            space.add(String.valueOf(i));
        }
        ;
        if (id.equals("null")) {

//            txtwalletpayment.setVisibility(View.GONE);
        } else {
//            txtwalletpayment.setVisibility(View.VISIBLE);
        }
        if (cd.isConnectingToInternet()) {
            currentlocation();
            getaddress1(latitude, longitude);
            new other_parking_rule().execute();

            String ff = max;
            String dd = rate1;
            double limt = Double.parseDouble(ff);
            double pricing_dur = Double.parseDouble(dd);
            int ssw = (int) (limt / pricing_dur);
            for (int y = 1; y <= ssw; y++) {
                if (duration_unit.equals("Minute")) {
                    int min = (int) (y * pricing_dur);
                    values.add(String.valueOf(min) + " Mins");
                } else if (duration_unit.equals("Month")) {
                    int min = (int) (y * pricing_dur);
                    values.add(String.valueOf(min) + " Months");
                } else if (duration_unit.equals("Hour")) {
                    int min = (int) (y * pricing_dur);
                    values.add(String.valueOf(min) + " Hrs");
                } else if (duration_unit.equals("Day")) {
                    int min = (int) (y * pricing_dur);
                    values.add(String.valueOf(min) + " Days");
                } else if (duration_unit.equals("Week")) {
                    int min = (int) (y * pricing_dur);
                    values.add(String.valueOf(min) + " Weeks");
                }
            }


            for (int i = 1; i < values.size(); i++) {
                item ii = new item();
                ii.setState(values.get(i).toString());
                ii.setSelected_parking(false);
                stateq.add(ii);
            }

            if (stateq.size() >= 4) {
                stateq.get(3).setSelected_parking(true);
                shoues = stateq.get(3).getState();
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    txt_hours.setText(hr + " Hrs");
                } else {
                    txt_hours.setText(shoues);
                }

            } else if (stateq.size() >= 3) {
                stateq.get(2).setSelected_parking(true);
                shoues = stateq.get(2).getState();
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    txt_hours.setText(hr + " Hrs");
                } else {
                    txt_hours.setText(shoues);
                }
            } else if (stateq.size() >= 2) {
                stateq.get(1).setSelected_parking(true);
                shoues = stateq.get(1).getState();
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    txt_hours.setText(hr + " Hrs");
                } else {
                    txt_hours.setText(shoues);
                }
            }

            txt_hours.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShowHours(getActivity(), stateq);
                }
            });

            Activity activity = getActivity();
            if (activity != null) {
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, values);
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                sphoures.setAdapter(spinnerArrayAdapter);
            }

            if (!rate.equals("") && !rate1.equals("")) {
                txtrate.setText(String.format("Rate: $%s@%s%s", rate, rate1, duration_unit));
                Double du = Double.parseDouble(rate1) / 60.0;
                if (rate1 != null && !TextUtils.isEmpty(rate1) && !rate1.equals("1")) {
                    if (duration_unit.equals("Hour")) {
                        txtrate.setText(String.format("Rate: $%s@%s Hours", rate, rate1));
                    } else if (duration_unit.equals("Minute")) {
                        txtrate.setText(String.format("Rate: $%s@%s Minutes", rate, rate1));
                    } else if (duration_unit.equals("Day")) {
                        txtrate.setText(String.format("Rate: $%s@%s Days", rate, rate1));
                    } else if (duration_unit.equals("Week")) {
                        txtrate.setText(String.format("Rate: $%s@%s Weeks", rate, rate1));
                    } else if (duration_unit.equals("Month")) {
                        txtrate.setText(String.format("Rate: $%s@%s Months", rate, rate1));
                    }
                }
            }

            if (managedpin.equals("yes")) {
                txt_parking_location_lable.setVisibility(View.VISIBLE);
                rl_sp_row.setVisibility(View.VISIBLE);
                rl_sp_space.setVisibility(View.VISIBLE);
            } else {
                txt_parking_location_lable.setVisibility(View.GONE);
                rl_sp_row.setVisibility(View.GONE);
                rl_sp_space.setVisibility(View.GONE);
            }

            new calculate().execute();
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

            currentdate = sdf.format(new Date());


            SharedPreferences d = getActivity().getSharedPreferences("renew", Context.MODE_PRIVATE);
            renew = d.getString("renew", "no");
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, values);
            ArrayAdapter<String> sp_row_array = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, row);
            ArrayAdapter<String> sp_space_array = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, space);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
           /* sp_row.setAdapter(sp_row_array);
            sp_space.setAdapter(sp_space_array);*/
            sphoures.setAdapter(spinnerArrayAdapter);
            sphoures.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    shoues = (String) sphoures.getSelectedItem();
                }

                @Override
                public void onNothingSelected(AdapterView<?> paren1t) {
                }
            });
          /*  sp_row.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                {
                    lost_row = (String) sp_row.getSelectedItem();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent)
                {

                }
            });

            sp_space.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                {
                    lost_no = (String) sp_space.getSelectedItem();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent)
                {

                }
            });*/
           /* ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, statearray);
            spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spsate.setAdapter(spinnerArrayAdapter1);
            spsate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    statename = (String) spsate.getSelectedItem();
                    ((TextView) view).setTextColor(Color.BLACK);
                    if(!marker.equals("google") && !marker.equals("other parking")&& !marker.equals("partner"))
                    {
                        new  get_total_hours_parked_today(lat,lang,platno,statename).execute();
                    }

                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });*/

            for (int i = 1; i < all_state.size(); i++) {
                String statename = all_state.get(i).getState2Code();
                item ii = new item();
                ii.setState(statename);
                if (statename.equals("NY")) {
                    this.statename = statename;
                    ii.setSelected_parking(true);
                    txt_state.setText("NY");
                } else {
                    ii.setSelected_parking(false);
                }
                state_array.add(ii);
            }

            txt_state.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShowStatePupup(getActivity(), state_array);
                }
            });
            if (renew.equals("yes")) {
                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                String platno2 = sh.getString("platno", "");
                String statename2 = sh.getString("state", "");
                String lotrow_ = sh.getString("lot_row", "a");
                String lot_no_ = sh.getString("lot_no", "1");
                editno.setText(platno2);
                Renew_date = d.getString("cureent_date", "0");
                ReNew_id = d.getString("renew_id", "0");
                marker = d.getString("marker", "0");
                Renew_parked_time = d.getString("parked_time", "0");
                Renew_is_valid = d.getString("car_is_valid", "no");
                String marker = d.getString("marker", "null");
                if (marker.equals("guest")) {
                    txt_parking_location_lable.setVisibility(View.VISIBLE);
                    rl_sp_row.setVisibility(View.VISIBLE);
                    rl_sp_space.setVisibility(View.VISIBLE);
                }
                for (int i = 0; i < all_state.size(); i++) {

                    String ss = all_state.get(i).getState2Code();
                    if (ss.equals(statename2)) {
                        spsate.setSelection(i);
                        break;
                    }
                }

                txt_state.setText(statename2);
                state_array.clear();
                for (int i = 1; i < all_state.size(); i++) {
                    String statename = all_state.get(i).getState2Code();
                    item ii = new item();
                    ii.setState(statename);
                    if (statename.equals(statename2)) {
                        this.statename = statename;
                        ii.setSelected_parking(true);
                        txt_state.setText(statename2);
                    } else {
                        ii.setSelected_parking(false);
                    }
                    state_array.add(ii);
                }
                for (int k = 0; k < row.length; k++) {
                    if (lotrow_.equals("")) {
                        lotrow_ = "a";
                    }
                    String row1 = row[k].toString();

                    if (row1.equals(lotrow_.toUpperCase())) {
                        //sp_row.setSelection(k);
                        break;
                    }
                }
               /* array_row.clear();
                for(int i=0;i<row_array.size();i++){
                    item m=new item();
                    if(row_array.get(i).toString().equals(lotrow_))
                    {
                        m.setDisplay(row_array.get(i).toString());
                        m.setSelected_parking(true);
                        txt_row.setText(lotrow_);
                        lost_row=lotrow_;
                        m.setIsparked("f");
                        array_row.add(m);
                    }
                    else
                    {
                        m.setDisplay(row_array.get(i).toString());
                        m.setSelected_parking(false);
                        m.setIsparked("f");
                        array_row.add(m);
                    }
                }
                array_space.clear();
                for(int i=0;i<row_number_array.size();i++){
                    item m=new item();
                    if(row_number_array.get(i).toString().equals(lot_no_))
                    {
                        m.setDisplay(row_number_array.get(i).toString());
                        m.setSelected_parking(true);
                        lost_no=lot_no_;
                        txt_space.setText(lot_no_);
                        m.setIsparked("f");
                        array_space.add(m);
                    }
                    else
                    {
                        m.setDisplay(row_number_array.get(i).toString());
                        m.setSelected_parking(false);
                        m.setIsparked("f");
                        array_space.add(m);
                    }
                }*/


                for (int j = 0; j < space.size(); j++) {
                    String row1 = space.get(j).toString();
                    if (row1 == lot_no_) {
                        // sp_space.setSelection(j);
                        break;
                    }
                }
                SharedPreferences sh2 = getActivity().getSharedPreferences("renew", Context.MODE_PRIVATE);
                sh2.edit().clear().commit();
                SharedPreferences back_select2 = getActivity().getSharedPreferences("back_paid_parking", Context.MODE_PRIVATE);
                back_select2.edit().clear().commit();
            } else {
                /*if(!id.equals("null"))
                {
                    new getvehiclenumber().execute();
                }*/

                if (!id.equals("null")) {
                    SharedPreferences dd11 = getActivity().getSharedPreferences("select_vehicle", Context.MODE_PRIVATE);
                    String select_plate_no = dd11.getString("plate_no", "");
                    String select_state = dd11.getString("state", "");
                    p_country = dd11.getString("p_country", "");
                    if (renew.equals("yes")) {
                        SharedPreferences back_select2 = getActivity().getSharedPreferences("back_paid_parking", Context.MODE_PRIVATE);
                        back_select2.edit().clear().commit();
                    }
                    /*editno.setText(select_plate_no.toUpperCase());
                    for (int i = 0; i < statearray.length; i++)
                    {
                        String state = statearray[i];
                        if (state.equals(select_state))
                        {
                            spsate.setSelection(i);
                            statename=select_state;
                            break;
                        }
                    }*/

                    state_array.clear();
                    for (int i = 1; i < all_state.size(); i++) {
                        String statename = all_state.get(i).getState2Code();
                        item ii = new item();
                        ii.setState(statename);
                        if (statename.equals(select_state)) {
                            ii.setSelected_parking(true);
                            this.statename = statename;
                            txt_state.setText(select_state);
                        } else {
                            ii.setSelected_parking(false);
                        }
                        state_array.add(ii);
                    }
                    txt_plate_no.setVisibility(View.VISIBLE);
                    txt_state.setEnabled(false);
                    editno.setVisibility(View.GONE);
                    //new getvehiclenumber().execute();
                } else {
                    txt_plate_no.setVisibility(View.GONE);
                    txt_state.setEnabled(true);
                    editno.setVisibility(View.VISIBLE);
                    SharedPreferences dd1 = getActivity().getSharedPreferences("select_vehicle", Context.MODE_PRIVATE);
                    String select_plate_no = dd1.getString("plate_no", "");
                    String select_state = dd1.getString("state", "");
                    p_country = dd1.getString("p_country", "");
                   /* editno.setText(select_plate_no.toUpperCase());
                    for (int i = 0; i < statearray.length; i++)
                    {
                        String state = statearray[i];
                        if (state.equals(select_state))
                        {
                            spsate.setSelection(i);
                            break;
                        }
                    }

                    state_array.clear();*/
                  /*  for (int i = 1; i < statearray.length; i++) {
                        String statename = statearray[i].toString();
                        item ii = new item();
                        ii.setState(statearray[i].toString());
                        if (statename.equals(select_state)) {
                            ii.setSelected_parking(true);
                            this.statename=statename;
                            txt_state.setText(select_state);
                        } else {
                            ii.setSelected_parking(false);
                        }
                        state_array.add(ii);
                    }*/

                    SharedPreferences back_select2 = getActivity().getSharedPreferences("back_paid_parking", Context.MODE_PRIVATE);
                    back_select2.edit().clear().commit();
                }

                p_country = p_country != null ? (!p_country.equals("null") ? (!p_country.equals("") ? p_country : "") : "") : "";

            }

            String user_id = logindeatl.getString("id", "");
            if (!user_id.equals("")) {
                if (!renew.equals("yes")) {
                    new getvehiclenumber().execute();
                }
            } else {
                showenter_vehicle(getActivity());
            }

            txt_plate_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShowState(getActivity());
                }
            });
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connetrction Error");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();
        }
        statefullname = new HashMap<String, Object>();
        statefullname.put("AL", "Alabama");
        statefullname.put("AK", "Alaska");
        statefullname.put("AZ", "Arizona");
        statefullname.put("AR", "Arkansas");
        statefullname.put("CA", "California");
        statefullname.put("CO", "Colorado");
        statefullname.put("CT", "Connecticut");
        statefullname.put("DE", "Delaware");
        statefullname.put("DC", "District of Columbia");
        statefullname.put("FL", "Florida");
        statefullname.put("GA", "Georgia");
        statefullname.put("HI", "Hawaii");
        statefullname.put("ID", "Idaho");
        statefullname.put("IL", "Illinois");
        statefullname.put("IN", "Indiana");
        statefullname.put("IA", "Iowa");
        statefullname.put("KS", "Kansas");
        statefullname.put("KY", "Kentucky");
        statefullname.put("LA", "Louisiana");
        statefullname.put("ME", "Maine");
        statefullname.put("MD", "Maryland");
        statefullname.put("MA", "Massachusetts");
        statefullname.put("MI", "Michigan");
        statefullname.put("MN", "Minnesota");
        statefullname.put("MS", "Mississippi");
        statefullname.put("MO", "Missouri");
        statefullname.put("MT", "Montana");
        statefullname.put("NE", "Nebraska");
        statefullname.put("NV", "Nevada");
        statefullname.put("NH", "New Hampshire");
        statefullname.put("NJ", "New Jersey");
        statefullname.put("NM", "New Mexico");
        statefullname.put("NY", "New York");
        statefullname.put("NC", "North Carolina");
        statefullname.put("ND", "North Dakota");
        statefullname.put("OH", "Ohio");
        statefullname.put("OK", "Oklahoma");
        statefullname.put("OR", "Oregon");
        statefullname.put("PA", "Pennsylvani");
        statefullname.put("RI", "Rhode Island");
        statefullname.put("SC", "South Carolina");
        statefullname.put("SD", "South Dakota");
        statefullname.put("TN", "Tennessee");
        statefullname.put("TX", "Texas");
        statefullname.put("UT", "Utah");
        statefullname.put("VT", "Vermont");
        statefullname.put("VA", "Virginia");
        statefullname.put("WA", "Washington");
        statefullname.put("WV", "West virginia");
        statefullname.put("WI", "Wisconsin");
        statefullname.put("WY", "Wyoming");

        setListeners();

        return view;
    }

    private void setListeners() {

        tvBtnParkNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isReserved = false;
                isPayLaterClick = false;

                mIsParking = true;
                PaymentOptionDialog dialog = PaymentOptionDialog.newInstance();
                Bundle bundle = new Bundle();
                bundle.putBoolean("mIsPayLater", isParkNowPostPayAllowed);
                dialog.setArguments(bundle);
                dialog.show(getChildFragmentManager(), "payment");
            }
        });

        tvBtnReserveNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String hour = txt_hours.getText().toString();
                intHour = Integer.valueOf(hour.substring(0, hour.indexOf(" ")));
                isPayLaterClick = false;

                mIsParking = false;
                showReservationDialog(true);
            }
        });

//        txtwalletpayment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (cd.isConnectingToInternet()) {
//                    isclickonwallet = true;
//                    isClickReserved = false;
//                    isClickPayPal = false;
//                    SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
//                    user_id = logindeatl.getString("id", "null");
//                    if (!user_id.equals("null")) {
//                        if (marker.equals("google")) {
//                            if (marker.equals("other parking") || marker.equals("partner")) {
//                                parkingtype = "otherPartner";
//                                township_code_final = "PRIV";
//                            } else {
//                                parkingtype = "googlePartner";
//                                township_code_final = "COMM";
//                            }
//
//                        } else {
//
//                            if (marker.equals("other parking") || marker.equals("partner")) {
//                                township_code_final = "PRIV";
//                                parkingtype = "otherPartner";
//                            }
//                        }
//                    }
//                    if (!rate.equals("")) {
//                        String no = editno.getText().toString();
//                        if (!no.equals("") && !statename.equals("State")) {
//                            if (!shoues.equals("hours") && !shoues.equals("")) {
//                                platno = editno.getText().toString();
//                                if (!user_id.equals("null")) {
//                                    token = 0;
//                                    if (rl_sp_row.getVisibility() == View.VISIBLE) {
//                                        if (!lost_no.equals("SPACE#") && !lost_row.equals("ROW")) {
//                                            parkingtype = "guest";
//                                            lost_row1 = lost_row;
//                                            lost_no1 = lost_no;
//                                            check_click_wallet();
//                                            //new getwalletbal().execute();
//                                        } else {
//                                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                                            alertDialog.setTitle("Alert!!");
//                                            alertDialog.setMessage("Please fill in all required fields.");
//                                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int which) {
//
//                                                }
//                                            });
//                                            alertDialog.show();
//                                        }
//                                    } else {
//
//                                        lost_row1 = "";
//                                        lost_no1 = "";
//                                        check_click_wallet();
//                                    }
//                                }
//                            } else {
//
//                                TextView tvTitle = new TextView(getActivity());
//                                tvTitle.setText("Alert!!!");
//                                tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
//                                tvTitle.setPadding(0, 10, 0, 0);
//                                // tvTitle.setPadding(5, 25, 5, 25);
//                                tvTitle.setTextColor(Color.parseColor("#000000"));
//                                tvTitle.setTextSize(20);
//                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                                alertDialog.setCustomTitle(tvTitle);
//                                alertDialog.setMessage("Select parking hours before proceeding.");
//                                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//
//                                    }
//                                });
//                                final AlertDialog alertd = alertDialog.create();
//                                alertd.show();
//                                TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
//                                messageText.setGravity(Gravity.CENTER_HORIZONTAL);
//
//
//                            }
//                        } else {
//                            TextView tvTitle = new TextView(getActivity());
//                            tvTitle.setText("Alert!!!");
//                            tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
//                            tvTitle.setPadding(0, 10, 0, 0);
//                            // tvTitle.setPadding(5, 25, 5, 25);
//                            tvTitle.setTextColor(Color.parseColor("#000000"));
//                            tvTitle.setTextSize(20);
//                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                            alertDialog.setCustomTitle(tvTitle);
//                            alertDialog.setMessage("Fill the all fields, in order to Pay & Parking.");
//                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
//
//                                }
//                            });
//
//                            final AlertDialog alertd = alertDialog.create();
//                            alertd.show();
//                            TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
//                            messageText.setGravity(Gravity.CENTER_HORIZONTAL);
//                        }
//                    }
//                } else {
//                    showAlertDialog(getActivity(), "No Internet Connection",
//                            "You don't have internet connection.", false);
//                }
//            }
//        });
//        txtmakpayment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (cd.isConnectingToInternet()) {
//                    isclickonwallet = false;
//                    isClickPayPal = true;
//                    isClickReserved = false;
//                    SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
//                    user_id = logindeatl.getString("id", "null");
//                    platno = editno.getText().toString();
//                    SharedPreferences payment = getActivity().getSharedPreferences("paypalpaymentback", Context.MODE_PRIVATE);
//                    SharedPreferences.Editor edi = payment.edit();
//                    edi.putString("paypalback", "true");
//                    edi.commit();
//                    platno = editno.getText().toString();
//                    if (marker.equals("google")) {
//                        if (marker.equals("other parking") || marker.equals("partner")) {
//                            township_code_final = "PRIV";
//                            parkingtype = "otherPartner";
//
//                        } else {
//                            township_code_final = "COMM";
//                            parkingtype = "googlePartner";
//                        }
//                    } else {
//
//                        if (marker.equals("other parking") || marker.equals("partner")) {
//                            township_code_final = "PRIV";
//                            parkingtype = "otherPartner";
//                        }
//                    }
//                    if (!platno.equals("") && !statename.equals("State") && !shoues.equals("hours")) {
//                        if (rl_sp_row.getVisibility() == View.VISIBLE) {
//                            if (!lost_no.equals("SPACE#") & !lost_row.equals("ROW")) {
//                                parkingtype = "guest";
//                                lost_row1 = lost_row;
//                                lost_no1 = lost_no;
//                                check_makepayment();
//                            } else {
//                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                                alertDialog.setTitle("Alert!!");
//                                alertDialog.setMessage("Please fill in all required fields.");
//                                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//
//                                    }
//                                });
//                                alertDialog.show();
//                            }
//                        } else {
//
//                            lost_row1 = "";
//                            lost_no1 = "";
//                            token = 0;
//                            check_makepayment();
//                        }
//                    } else {
//                        TextView tvTitle = new TextView(getActivity());
//                        tvTitle.setText("Alert!!!");
//                        tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
//                        tvTitle.setPadding(0, 10, 0, 0);
//                        tvTitle.setTextColor(Color.parseColor("#000000"));
//                        tvTitle.setTextSize(20);
//                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                        alertDialog.setCustomTitle(tvTitle);
//                        alertDialog.setMessage("Fill the all fields, in order to Pay & Parking.");
//                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//
//                            }
//                        });
//                        final AlertDialog alertd = alertDialog.create();
//                        alertd.show();
//                        TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
//                        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
//                    }
//                } else {
//                    showAlertDialog(getActivity(), "No Internet Connection",
//                            "You don't have internet connection.", false);
//                }
//            }
//        });

    }

    private void showReservationDialog(final boolean isPrePayment) {
        // reservation dialog
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.date_time_picker_dialog, null);
        final TextView tvSelectedTime = dialogView.findViewById(R.id.tvSelectedTime);
        final TextView tvExitTime = dialogView.findViewById(R.id.tvExitTime);
        TextView tvBtnCancel = dialogView.findViewById(R.id.tvBtnCancel);
        TextView tvBtnConfirm = dialogView.findViewById(R.id.tvBtnConfirm);
        builder.setView(dialogView);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        final String stringTime = "";
        final Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        reserveCurrentTime = dateFormat1.format(new Date(calendar.getTimeInMillis()));
        reserveEntryTime = dateFormat1.format(new Date(calendar.getTimeInMillis()));
        tvSelectedTime.setText(dateFormat1.format(new Date(calendar.getTimeInMillis())));
        calendar.add(Calendar.HOUR, intHour);
        tvExitTime.setText(dateFormat1.format(new Date(calendar.getTimeInMillis())));
        reserveExpiryTime = dateFormat1.format(new Date(calendar.getTimeInMillis()));

        tvSelectedTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        final Calendar calendar1 = Calendar.getInstance();
                        calendar1.set(Calendar.YEAR, i);
                        calendar1.set(Calendar.MONTH, i1);
                        calendar1.set(Calendar.DAY_OF_MONTH, i2);


                        new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                                calendar1.set(Calendar.HOUR_OF_DAY, i);
                                calendar1.set(Calendar.MINUTE, i1);

                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//                                stringTime = dateFormat.format(new Date(calendar1.getTimeInMillis()));
                                tvSelectedTime.setText(dateFormat.format(new Date(calendar1.getTimeInMillis())));
                                reserveEntryTime = dateFormat.format(new Date(calendar1.getTimeInMillis()));
                                calendar1.add(Calendar.HOUR, intHour);
                                tvExitTime.setText(dateFormat.format(new Date(calendar1.getTimeInMillis())));
                                reserveExpiryTime = dateFormat.format(new Date(calendar1.getTimeInMillis()));

                            }
                        }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE),
                                false).show();
                    }
                }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),
                        Calendar.getInstance().get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        tvBtnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                Log.e("#DEBUG", "   reserveEntryTime:  " + reserveEntryTime + " " +
                        "reserveExpiryTime:  " + reserveExpiryTime);
                Log.e("#DEBUG", "   reserveCurrentTime: " + reserveCurrentTime);
//                if (isPrePayment) {
////                    llPaymentOptions.setVisibility(View.VISIBLE);
//                    llParkOptions.setVisibility(View.GONE);
//                    isReserved = true;
//                    isPayLaterClick = false;
//                } else {
//                    isReserved = true;
//                    getTotalAmountReserved();
//                }

                PaymentOptionDialog dialog = PaymentOptionDialog.newInstance();
                Bundle bundle = new Bundle();
                bundle.putBoolean("mIsPayLater", !isPrePaymentRequiredForReservation);
                dialog.setArguments(bundle);
                dialog.show(getChildFragmentManager(), "payment");

            }
        });

        tvBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();

    }

    private void showParkNowPayLaterConfirmationDialog() {
        // park now, pay later
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure, you want to Pay Later and Park Now?");
        builder.setPositiveButton("PARK NOW", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                new finalupdate().execute("payLater");
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    @Override
    public void onWalletClick() {
        mIsPayNow = true;
        mIsWalletPayment = true;
        calculateFees();
    }

    private void getWalletBalance() {
        //
        new getwalletbal().execute();
    }

    private void showDialogDebitFromWallet() {
        // if yes
        new updatewallet().execute();
    }

    private void updateWalletBalance() {
        if (mIsParking) {
            //add to parked_cars
            new finalupdate().execute();
        } else {
            //add to reserve_parking
            new reserveNow().execute();
        }
    }

    private void startPayPalPaymentRequest() {

        PaypalPaymentIntegration(String.valueOf(mParkingTotal));
    }

    private void calculateFees() {

        min = shoues.substring(shoues.indexOf(' ') + 1);
        String hr = shoues.substring(0, shoues.indexOf(' '));
        Double d = new Double(Double.parseDouble(hr));
        hours = d.intValue();
        parkingQty = hours;
        txtrate.setText("Rate: $" + rate + "@" + rate1 + duration_unit);
        if (!rate.equals("") && !rate1.equals("null")) {
            double hrr = Double.parseDouble(rate1);
            parkingUnits = hrr;
            double prices = Double.parseDouble(rate);
            parkingRate = prices;
            double perhrrate = hrr / prices;
            finalpayamount = hours / perhrrate;
            subTotal = hours / perhrrate;
            tr_percentage_for_parked_cars = finalpayamount * tr_percentage1;
            finalpayamount = finalpayamount + tr_percentage_for_parked_cars + tr_fee1;

            mParkingSubTotal = subTotal;
            mTrFee = tr_fee1;
            mTrPercent = tr_percentage_for_parked_cars;
            mParkingTotal = finalpayamount;


//            String finallab2 = String.format("%.2f", bal);
//            String finallab4 = String.format("%.2f", finalpayamount);
//
//            Calendar calendar = Calendar.getInstance();
//            int day = calendar.get(Calendar.DAY_OF_WEEK);

//            switch (day) {
//                case Calendar.SUNDAY:
//                    try {
//                        if ((!week_end_discount.equals("") || !week_end_discount.equals("null"))) {
//                            double week = Double.parseDouble(week_end_discount);
//                            if (week > 0.0) {
//                                finalpayamount = finalpayamount - (finalpayamount * (week / 100));
//                            }
//                        }
//                    } catch (Exception e) {
//
//                    }
//
//                    // Current day is Sunday
//
//                case Calendar.MONDAY:
//                    try {
//                        if ((!week_end_discount.equals("") || !week_end_discount.equals("null"))) {
//                            double week = Double.parseDouble(week_end_discount);
//                            if (week > 0.0) {
//                                finalpayamount = finalpayamount - (finalpayamount * (week / 100));
//                            }
//                        }
//                    } catch (Exception e) {
//
//                    }
//
//                    // Current day is Monday
//
//                case Calendar.TUESDAY:
//                    // etc.
//            }
            int remain_time = 0;
            boolean is_parked_time = true;
            Renew_parked_time = Renew_parked_time != null ? (!Renew_parked_time.equals("") ? (!Renew_parked_time.equals("null") ? Renew_parked_time : "") : "") : "";
            if (!Renew_parked_time.equals("")) {
                int parked_hours = Integer.parseInt(Renew_parked_time);
                final_count = parked_hours + hours;
                int max = Integer.parseInt(max_time);
                remain_time = max - parked_hours;
                if (final_count <= max) {
                    is_parked_time = true;
                } else {
                    is_parked_time = false;
                }
            } else {
                final_count = hours;
            }

            if (is_parked_time) {

                Log.e("#DEBUG", "  calculateFees:  mTrFee:  " + mTrFee + "  mTrPercent: "
                        + mTrPercent + "   mParkingSubTotal:  " + mParkingSubTotal + "  mParkingTotal: " + mParkingTotal);

                if (mIsParking) {
                    // follow parking steps
                    if (mIsPayNow) {
                        //calculate fees
                        checkIsCarAlreadyParked();
                    } else {
                        //calculate fees with pay later charges
                        mParkingSubTotal = subTotal + mParkNowPostPayFees;
                        mParkingTotal = mParkingTotal + mParkNowPostPayFees;
                        checkIsCarAlreadyParked();
                    }
                } else {
                    //follow reservation steps
                    if (mIsPayNow) {
                        //calculate fees for reservation
                        if (mIsWalletPayment) {
                            //pay with wallet balance
                            getWalletBalance();
                        } else {
                            //pay with paypal
                            startPayPalPaymentRequest();
                        }

                    } else {
                        // calculate fees for reservation with pay later charges
                        mParkingSubTotal = subTotal + mReserveNowPostPayFees;
                        mParkingTotal = mParkingTotal + mReserveNowPostPayFees;
                        addReserveParking();

                    }
                }


            } else {
                TextView tvTitle = new TextView(getActivity());
                tvTitle.setText("ParkEZly");
                tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                tvTitle.setPadding(0, 10, 0, 0);
                tvTitle.setTextColor(Color.parseColor("#000000"));
                tvTitle.setTextSize(20);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setCustomTitle(tvTitle);
                alertDialog.setMessage(Html.fromHtml("Duration must be less than<b> maximum park " + max_time + " " + duration_unit + "s</b> and you already parked <b>" + Renew_parked_time + " " + duration_unit + "s</b> so you can park upto <b>" + remain_time + " " + duration_unit + "s</b> more or less!"));
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                final AlertDialog alertd = alertDialog.create();
                alertd.show();
                TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                messageText.setGravity(Gravity.CENTER_HORIZONTAL);
            }

        }

//        min = shoues.substring(shoues.indexOf(' ') + 1);
//        String hr = shoues.substring(0, shoues.indexOf(' '));
//        Double d = new Double(Double.parseDouble(hr));
//        hours = d.intValue();
//        parkingQty = hours;
//        float minutes;
//        String h;
//
//        double hrr = Double.parseDouble(rate1);
//        parkingUnits = hrr;
//        double prices = Double.parseDouble(rate);
//        parkingRate = prices;
//        double perhrrate = hrr / prices;
//        finalpayamount = hours / perhrrate;
//        subTotal = hours / perhrrate;
//        tr_percentage_for_parked_cars = finalpayamount * tr_percentage1;
//        finalpayamount = finalpayamount + tr_percentage_for_parked_cars + tr_fee1;
//        finallab = String.format("%.2f", finalpayamount);
//
//        mParkingSubTotal = subTotal;
//        mTrFee = tr_fee1;
//        mTrPercent = tr_percentage_for_parked_cars;
//        mParkingTotal = Double.parseDouble(finallab);
//
//        Log.e("#DEBUG", "  calculateFees:  mTrFee:  " + mTrFee + "  mTrPercent: "
//                + mTrPercent + "   mParkingSubTotal:  " + mParkingSubTotal + "  mParkingTotal: " + mParkingTotal);
//
//
//        if (mIsParking) {
//            // follow parking steps
//            if (mIsPayNow) {
//                //calculate fees
//                checkIsCarAlreadyParked();
//            } else {
//                //calculate fees with pay later charges
//                mParkingSubTotal = subTotal + mParkNowPostPayFees;
//                mParkingTotal = mParkingTotal + mParkNowPostPayFees;
//                checkIsCarAlreadyParked();
//            }
//        } else {
//            //follow reservation steps
//            if (mIsPayNow) {
//                //calculate fees for reservation
//                if (mIsWalletPayment) {
//                    //pay with wallet balance
//                    getWalletBalance();
//                } else {
//                    //pay with paypal
//                    startPayPalPaymentRequest();
//                }
//
//            } else {
//                // calculate fees for reservation with pay later charges
//                mParkingSubTotal = subTotal + mReserveNowPostPayFees;
//                mParkingTotal = mParkingTotal + mReserveNowPostPayFees;
//                addReserveParking();
//
//            }
//        }
    }

    @Override
    public void onPaymentClick() {
        mIsPayNow = true;
        mIsWalletPayment = false;
        calculateFees();
    }

    @Override
    public void onPayLaterClick() {
        Log.e(TAG, "    onPayLaterClick");
        mIsPayNow = false;
        calculateFees();
    }

    private void checkIsCarAlreadyParked() {
        new checkParkedCarsReserved().execute();
    }

    private void addParkedCar() {
        new finalupdate().execute();
    }

    private void addReserveParking() {
        new reserveNow().execute();
    }

    public class checkParkedCarsReserved extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONArray array = new JSONArray();
        String parked_id = "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String vechiclenourl = null;
            try {
                String plate = platno;
                plate = plate.replaceAll("\\s+", "");
                vechiclenourl = "_table/parked_cars?filter=(plate_no%3D" + URLEncoder.encode(plate, "utf-8") + ")%20AND%20(pl_state%3D" + statename + ")%20AND%20(parking_status%3D'ENTRY')";
            } catch (Exception e) {
                e.printStackTrace();
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + vechiclenourl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject v = array.getJSONObject(i);
                        parked_id = v.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (!array.isNull(0)) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Vehicle already parked!");
                    alertDialog.setMessage("This Vehicle is already parked, Do you want to exit it from previous parking and park here ?");

                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            new exitsparking(parked_id).execute();
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });
                    alertDialog.show();
                } else {
                    // post values into reserved_parkings
//                    getTotalAmountReserved();

                    //not parked, park car -> add to parked_cars
                    if (mIsPayNow) {
                        //start payment for parking
                        if (mIsWalletPayment) {
                            //pay with wallet balance
                            getWalletBalance();
                        } else {
                            //pay with paypal
                            startPayPalPaymentRequest();
                        }
                    } else {
                        //add to parked_car with pay later
                        addParkedCar();
                    }

                }
            }
            super.onPostExecute(s);

        }
    }

    public void check_click_wallet() {
        if (!user_id.equals("null")) {
            if (renew.equals("yes")) {
                if (Renew_is_valid.equals("yes")) {
                    new getwalletbal().execute();
                } else {
                    new exitsparking(ReNew_id).execute();
                }
            } else {
                if (isReserved)
                    new checkParkedCarsReserved().execute();
                else new checkcardparkedornotwallet().execute();
            }

        } else {
            if (isReserved)
                new checkParkedCarsReserved().execute();
            else
                new checkcardparkedornotwallet().execute();
            //  showpaypalrandomnumber(getActivity());
        }
    }

    public void check_makepayment() {
       /* if (!user_id.equals("null"))
        {
            new checkcardparkedornotpaidt().execute();
        }
        else
        {
            showpaypalrandomnumber(getActivity());
        }*/
        if (!user_id.equals("null")) {
            if (renew.equals("yes")) {
                if (Renew_is_valid.equals("yes")) {
                    gettotalamount();
                } else {
                    new exitsparking(ReNew_id).execute();
                }
            } else {
                if (isReserved)
                    new checkParkedCarsReserved().execute();
                else new checkcardparkedornotpaidt().execute();
            }
        } else {
            if (renew.equals("yes")) {
                if (Renew_is_valid.equals("yes")) {
                    gettotalamount();
                } else {
                    new exitsparking(ReNew_id).execute();
                }
            } else {
                showpaypalrandomnumber(getActivity());
            }

        }
    }


    private void showenter_vehicle(Activity context) {

        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.enter_tvehicle, viewGroup, false);
        final PopupWindow popup = new PopupWindow(context);
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(true);
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        popup.setFocusable(true);
        popup.update();
        popup.showAtLocation(layout, Gravity.TOP, 0, 0);

        final EditText edit_number;
        final Button btn_park_now;
        RelativeLayout btn_cancel;

        edit_number = (EditText) layout.findViewById(R.id.edit_nu);
        edit_number.setFocusable(true);
        btn_cancel = (RelativeLayout) layout.findViewById(R.id.close);
        btn_park_now = (Button) layout.findViewById(R.id.btn_done);
        btn_park_now.setAlpha((float) 0.3);
        btn_park_now.setEnabled(false);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        btn_park_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edit_number.getWindowToken(), 0);
                String hgv = edit_number.getText().toString();
                editno.setText(hgv);
                popup.dismiss();
                ShowStatePupup(getActivity(), state_array);

            }
        });

        edit_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String ss = s.toString();
                if (!ss.equals(ss.toUpperCase())) {
                    edit_number.setText(ss.toUpperCase());
                    edit_number.setSelection(ss.length());
                }

                if (s.length() > 0) {
                    btn_park_now.setAlpha((float) 1.0);
                    btn_park_now.setEnabled(true);
                } else {
                    btn_park_now.setAlpha((float) 0.3);
                    btn_park_now.setEnabled(false);
                }
            }
        });


        edit_number.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String hgv = edit_number.getText().toString();
                    editno.setText(hgv);
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edit_number.getWindowToken(), 0);
                    ShowStatePupup(getActivity(), state_array);
                    return true;
                }
                return false;
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edit_number.getWindowToken(), 0);
                String hgv = edit_number.getText().toString();
                editno.setText(hgv);
                popup.dismiss();
            }
        });


    }

    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();

    }

    @Override
    public void Onitem_save() {
        getActivity().onBackPressed();
        new getvehiclenumber().execute();
    }

    @Override
    public void Onitem_delete() {
        getActivity().onBackPressed();
        new getvehiclenumber().execute();
    }

    public class getwalletbal extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String userid = logindeatl.getString("id", "null");
        JSONObject json, json1;
        String wallbalurl = "_table/user_wallet?order=date_time%20DESC";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + wallbalurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            HttpEntity resEntity = response.getEntity();
            String responseStr = null;
            if (response != null) {
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        String user = c.getString("user_id");

                        if (user.equals(userid)) {
                            nbal = c.getString("new_balance");
                            if (!nbal.equals("null")) {
                                cbal = c.getString("current_balance");
                                item.setAmount(nbal);
                                item.setPaidamount(cbal);
                                wallbalarray.add(item);
                                break;
                            }
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }

            if (getActivity() != null && json != null) {
                if (wallbalarray.size() > 0) {
                    double bal = Double.parseDouble(cbal);
                    if (bal > 0) {
                        min = shoues.substring(shoues.indexOf(' ') + 1);
                        String hr = shoues.substring(0, shoues.indexOf(' '));
                        Double d = new Double(Double.parseDouble(hr));
                        hours = d.intValue();
                        txtrate.setText(String.format("Rate: $%s@%s%s", rate, rate1, duration_unit));
                        if (rate1 != null && !TextUtils.isEmpty(rate1) && !rate1.equals("1")) {
                            if (duration_unit.equals("Hour")) {
                                txtrate.setText(String.format("Rate: $%s@%s Hours", rate, rate1));
                            } else if (duration_unit.equals("Minute")) {
                                txtrate.setText(String.format("Rate: $%s@%s Minutes", rate, rate1));
                            } else if (duration_unit.equals("Day")) {
                                txtrate.setText(String.format("Rate: $%s@%s Days", rate, rate1));
                            } else if (duration_unit.equals("Week")) {
                                txtrate.setText(String.format("Rate: $%s@%s Weeks", rate, rate1));
                            } else if (duration_unit.equals("Month")) {
                                txtrate.setText(String.format("Rate: $%s@%s Months", rate, rate1));
                            }
                        } else {
                            txtrate.setText(String.format("Rate: $%s@%s%s", rate, rate1, duration_unit));
                        }
                        if (!rate.equals("") && !rate1.equals("null")) {
                            double hrr = Double.parseDouble(rate1);
                            double prices = Double.parseDouble(rate);
                            double perhrrate = hrr / prices;
                            finalpayamount = hours / perhrrate;
                            finalpayamount = finalpayamount + (finalpayamount * tr_percentage1) + tr_fee1;
                            newbal = bal - mParkingTotal;

                            if (newbal > 0) {
                                String finallab2 = String.format("%.2f", bal);
                                String finallab4 = String.format("%.2f", mParkingTotal);

                                Calendar calendar = Calendar.getInstance();
                                int day = calendar.get(Calendar.DAY_OF_WEEK);

                                switch (day) {
                                    case Calendar.SUNDAY:
                                        try {
                                            if ((!week_end_discount.equals("") || !week_end_discount.equals("null"))) {
                                                double week = Double.parseDouble(week_end_discount);
                                                if (week > 0.0) {
                                                    finalpayamount = finalpayamount - (finalpayamount * (week / 100));
                                                }
                                            }
                                        } catch (Exception e) {

                                        }

                                        // Current day is Sunday

                                    case Calendar.MONDAY:
                                        try {
                                            if ((!week_end_discount.equals("") || !week_end_discount.equals("null"))) {
                                                double week = Double.parseDouble(week_end_discount);
                                                if (week > 0.0) {
                                                    finalpayamount = finalpayamount - (finalpayamount * (week / 100));
                                                }
                                            }
                                        } catch (Exception e) {

                                        }

                                        // Current day is Monday

                                    case Calendar.TUESDAY:
                                        // etc.
                                }
                                int remain_time = 0;
                                boolean is_parked_time = true;
                                Renew_parked_time = Renew_parked_time != null ? (!Renew_parked_time.equals("") ? (!Renew_parked_time.equals("null") ? Renew_parked_time : "") : "") : "";
                                if (!Renew_parked_time.equals("")) {
                                    int parked_hours = Integer.parseInt(Renew_parked_time);
                                    final_count = parked_hours + hours;
                                    int max = Integer.parseInt(max_time);
                                    remain_time = max - parked_hours;
                                    if (final_count <= max) {
                                        is_parked_time = true;
                                    } else {
                                        is_parked_time = false;
                                    }
                                } else {
                                    final_count = hours;
                                }

                                if (is_parked_time) {

                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                    alertDialog.setTitle("Wallet Balance: $" + finallab2);
                                    alertDialog.setMessage("Would you like to pay from your ParkEZly wallet ? With service and transaction fee your total will be " + "$" + mParkingTotal + "");
                                    alertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            ip = getLocalIpAddress();
                                            showDialogDebitFromWallet();
                                        }
                                    });
                                    alertDialog.setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });
                                    alertDialog.show();
                                } else {
                                    TextView tvTitle = new TextView(getActivity());
                                    tvTitle.setText("ParkEZly");
                                    tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                                    tvTitle.setPadding(0, 10, 0, 0);
                                    tvTitle.setTextColor(Color.parseColor("#000000"));
                                    tvTitle.setTextSize(20);
                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                    alertDialog.setCustomTitle(tvTitle);
                                    alertDialog.setMessage(Html.fromHtml("Duration must be less than<b> maximum park " + max_time + " " + duration_unit + "s</b> and you already parked <b>" + Renew_parked_time + " " + duration_unit + "s</b> so you can park upto <b>" + remain_time + " " + duration_unit + "s</b> more or less!"));
                                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                                    final AlertDialog alertd = alertDialog.create();
                                    alertd.show();
                                    TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                                    messageText.setGravity(Gravity.CENTER_HORIZONTAL);
                                }
                            } else {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                alertDialog.setTitle("alert");
                                alertDialog.setMessage("You don't have enough funds in your wallet.");
                                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                                alertDialog.show();
                            }

                        }
                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("alert");
                        alertDialog.setMessage("You don't have enough funds in your wallet.");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }

                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("alert");
                    alertDialog.setMessage("You don't have enough funds in your wallet.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }

            }
            super.onPostExecute(s);

        }
    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("ip", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("aax", ex.toString());
        }
        return null;
    }

    public class calculate extends AsyncTask<String, String, String> {

        JSONObject json, json1;
        boolean user_selects_parking_location;
        String manageslocation = "_table/manage_locations?filter=location_code%3D'" + location_code + "'";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + manageslocation;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        township_code = c.getString("township_code");
                        user_selects_parking_location = c.getBoolean("user_selects_parking_location");
                        break;

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null && json != null) {
                if (!township_code.equals("null")) {
                    if (user_selects_parking_location == false) {
                        txt_parking_location_lable.setVisibility(View.GONE);
                        rl_sp_row.setVisibility(View.GONE);
                        rl_sp_space.setVisibility(View.GONE);
                    }

                    new servicecharge().execute();
                }
            }
            super.onPostExecute(s);

        }
    }

    public class other_parking_rule extends AsyncTask<String, String, String> {

        JSONObject json, json1;
        boolean user_selects_parking_location;
        String manageslocation = "_table/other_parking_rules?filter=location_code%3D'" + location_code + "'";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            other_parking_rule.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + manageslocation;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        item.setPricing_duration(c.getString("pricing_duration"));
                        item.setMax_duration(c.getString("max_duration"));
                        item.setDuration_unit(c.getString("duration_unit"));
                        item.setPricing(c.getString("pricing"));
                        if (c.has("twp_id")
                                && !TextUtils.isEmpty(c.getString("twp_id"))
                                && !c.getString("twp_id").equals("null")) {
                            item.setTwp_id(c.getString("twp_id"));
                            item.setCompany_id(c.getString("twp_id"));
                        }

                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }

                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }

                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }

                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }

                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }

                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }

                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }

                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }

                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }

                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }

                        if (c.has("Reservation_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_Fee"))
                                && !c.getString("Reservation_PostPayment_Fee").equals("null")) {
                            item.setReservation_PostPayment_Fee(c.getString("Reservation_PostPayment_Fee"));
                        }

                        if (c.has("ParkNow_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_Fee"))
                                && !c.getString("ParkNow_PostPayment_Fee").equals("null")) {
                            item.setParkNow_PostPayment_Fee(c.getString("ParkNow_PostPayment_Fee"));
                        }

                        if (c.has("number_of_reservable_spots")
                                && !TextUtils.isEmpty(c.getString("number_of_reservable_spots"))
                                && !c.getString("number_of_reservable_spots").equals("null")) {
                            item.setNumber_of_reservable_spots(c.getString("number_of_reservable_spots"));
                        }
                        other_parking_rule.add(item);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   other_parking_rule:  Error:  " + e.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   other_parking_rule:  Error:  " + e.getMessage());
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.e("#DEBUG", "  other_parking_rules Size: " + String.valueOf(other_parking_rule.size()));
            if (json != null) {
                if (marker.equals("other parking") || marker.equals("partner")) {
                    values.clear();
                    values.add("hours");
                    for (int k = 0; k < other_parking_rule.size(); k++) {
                        isReservationAllowed = other_parking_rule.get(k).isReservationAllowed();
                        isPrePaymentRequiredForReservation = other_parking_rule.get(k).isPrePymntReqd_for_Reservation();
                        reservationPostPayTerms = other_parking_rule.get(k).getReservation_PostPayment_term();
                        isParkNowPostPayAllowed = other_parking_rule.get(k).isParkNow_PostPaymentAllowed();
                        parkNowPostPayTerms = other_parking_rule.get(k).getParkNow_PostPayment_term();
                        companyId = other_parking_rule.get(k).getCompany_id();
                        twpId = other_parking_rule.get(k).getTwp_id();

                        if (!TextUtils.isEmpty(other_parking_rule.get(k).getParkNow_PostPayment_Fee()))
                            mParkNowPostPayFees = Double.parseDouble(other_parking_rule.get(k).getParkNow_PostPayment_Fee());
                        if (!TextUtils.isEmpty(other_parking_rule.get(k).getReservation_PostPayment_Fee()))
                            mReserveNowPostPayFees = Double.parseDouble(other_parking_rule.get(k).getReservation_PostPayment_Fee());

                        Log.e("#DEBUG", "  mReserveNowPostPayFees: " + mParkNowPostPayFees
                                + "   mReserveNowPostPayFees: " + mReserveNowPostPayFees + "   id: " + other_parking_rule.get(k).getId());

                        String ff = other_parking_rule.get(k).getMax_duration();
                        String dd = other_parking_rule.get(k).getPricing_duration();
                        double limt = Double.parseDouble(ff);
                        double pricing_dur = Double.parseDouble(dd);
                        int ss = (int) (limt / pricing_dur);
                        for (int y = 1; y <= ss; y++) {
                            if (other_parking_rule.get(k).getDuration_unit().contains("Minute")) {
                                int min = (int) (y * pricing_dur);
                                values.add(String.valueOf(min) + " Mins");
                            } else if (other_parking_rule.get(k).getDuration_unit().contains("Month")) {
                                int min = (int) (y * pricing_dur);
                                values.add(String.valueOf(min) + " Months");
                            } else if (other_parking_rule.get(k).getDuration_unit().contains("Hour")) {
                                int min = (int) (y * pricing_dur);
                                values.add(String.valueOf(min) + " Hrs");
                            } else if (other_parking_rule.get(k).getDuration_unit().contains("Day")) {
                                int min = (int) (y * pricing_dur);
                                values.add(String.valueOf(min) + " Days");
                            } else if (other_parking_rule.get(k).getDuration_unit().contains("Week")) {
                                int min = (int) (y * pricing_dur);
                                values.add(String.valueOf(min) + " Weeks");
                            }
                        }
                    }
                    Activity activity = getActivity();
                    if (activity != null) {
                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, values);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        sphoures.setAdapter(spinnerArrayAdapter);
                    }
                } else {
                    for (int j = 0; j < other_parking_rule.size(); j++) {
                        isReservationAllowed = other_parking_rule.get(j).isReservationAllowed();
                        isPrePaymentRequiredForReservation = other_parking_rule.get(j).isPrePymntReqd_for_Reservation();
                        reservationPostPayTerms = other_parking_rule.get(j).getReservation_PostPayment_term();
                        isParkNowPostPayAllowed = other_parking_rule.get(j).isParkNow_PostPaymentAllowed();
                        parkNowPostPayTerms = other_parking_rule.get(j).getParkNow_PostPayment_term();
                        companyId = other_parking_rule.get(j).getCompany_id();
                        twpId = other_parking_rule.get(j).getTwp_id();

                        if (!TextUtils.isEmpty(other_parking_rule.get(j).getParkNow_PostPayment_Fee()))
                            mParkNowPostPayFees = Double.parseDouble(other_parking_rule.get(j).getParkNow_PostPayment_Fee());
                        if (!TextUtils.isEmpty(other_parking_rule.get(j).getReservation_PostPayment_Fee()))
                            mReserveNowPostPayFees = Double.parseDouble(other_parking_rule.get(j).getReservation_PostPayment_Fee());

                        Log.e("#DEBUG", "  mReserveNowPostPayFees: " + mParkNowPostPayFees
                                + "   mReserveNowPostPayFees: " + mReserveNowPostPayFees + "   id: " + other_parking_rule.get(j).getId());
                    }
                }

                if (isReservationAllowed) {
                    tvBtnReserveNow.setVisibility(View.VISIBLE);
                    if (isPrePaymentRequiredForReservation) {
//                        tvBtnReserveNowPayLater.setVisibility(View.GONE);
                    } else {
//                        tvBtnReserveNowPayLater.setVisibility(View.VISIBLE);
                    }
                } else {
                    tvBtnReserveNow.setVisibility(View.GONE);
                }

                if (isParkNowPostPayAllowed) {
//                    tvBtnParkNowPayLater.setVisibility(View.VISIBLE);
                } else {
//                    tvBtnParkNowPayLater.setVisibility(View.GONE);

                }
            }
            super.onPostExecute(s);
        }
    }

    public class servicecharge extends AsyncTask<String, String, String> {


        JSONObject json, json1;
        String servicechargeurl = "_table/service_charges?filter=manager_id%3D'" + township_code + "'";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + servicechargeurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        String tr_percentage = c.getString("tr_percentage");
                        String tr_fee = c.getString("tr_fee");
                        if (!tr_fee.equals("null")) {
                            tr_fee1 = Double.parseDouble(tr_fee);
                        } else {
                            tr_fee1 = 0.0;
                        }
                        if (!tr_percentage.equals("null")) {
                            tr_percentage1 = Double.parseDouble(tr_percentage);
                        } else {
                            tr_percentage1 = 0.0;
                        }
                        Log.e("tr_percentage1", String.valueOf(tr_percentage1));
                        Log.e("tr_free", String.valueOf(tr_fee1));
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null && json != null) {
            }
            super.onPostExecute(s);
        }
    }

    public void currentlocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {

            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
        } else {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Failed to fetch user location!");
            alertDialog.setMessage("Please enable user location for app, location is required for app to work properly");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });

            alertDialog.show();

        }
    }

    public void getTotalAmountReserved() {

        String no = editno.getText().toString();
        if (!no.equals("") && !statename.equals("State")) {

            if (!shoues.equals("hours")) {
                min = shoues.substring(shoues.indexOf(' ') + 1);
                String hr = shoues.substring(0, shoues.indexOf(' '));
                Double d = new Double(Double.parseDouble(hr));
                hours = d.intValue();
                float minutes;
                String h = "";
                if (!rate.equals("") && !rate1.equals("null")) {
                    double hrr = Double.parseDouble(rate1);
                    double prices = Double.parseDouble(rate);
                    double perhrrate = hrr / prices;
                    finalpayamount = hours / perhrrate;
                    Calendar calendar = Calendar.getInstance();
                    int day = calendar.get(Calendar.DAY_OF_WEEK);

                    switch (day) {
                        case Calendar.SUNDAY:
                            try {
                                if ((!week_end_discount.equals("") || !week_end_discount.equals("null"))) {
                                    double week = Double.parseDouble(week_end_discount);
                                    if (week > 0.0) {
                                        finalpayamount = finalpayamount - (finalpayamount * (week / 100));
                                    }
                                }
                            } catch (Exception e) {

                            }
                        case Calendar.MONDAY:
                            try {
                                if ((!week_end_discount.equals("") || !week_end_discount.equals("null"))) {
                                    double week = Double.parseDouble(week_end_discount);
                                    if (week > 0.0) {
                                        finalpayamount = finalpayamount - (finalpayamount * (week / 100));
                                    }
                                }
                            } catch (Exception e) {

                            }
                        case Calendar.TUESDAY:
                    }

                    boolean is_parked_time = true;
                    Renew_parked_time = Renew_parked_time != null ? (!Renew_parked_time.equals("") ? (!Renew_parked_time.equals("null") ? Renew_parked_time : "") : "") : "";
                    int remain_time = 0;
                    if (!Renew_parked_time.equals("")) {
                        int parked_hours = Integer.parseInt(Renew_parked_time);
                        final_count = parked_hours + hours;
                        int max = Integer.parseInt(max_time);
                        remain_time = max - parked_hours;
                        if (final_count <= max) {
                            is_parked_time = true;
                        } else {
                            is_parked_time = false;
                        }
                    } else {
                        final_count = hours;
                    }


                    if (is_parked_time) {
                        new reserveNow().execute();
                    } else {
                        TextView tvTitle = new TextView(getActivity());
                        tvTitle.setText("ParkEZly");
                        tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                        tvTitle.setPadding(0, 10, 0, 0);
                        tvTitle.setTextColor(Color.parseColor("#000000"));
                        tvTitle.setTextSize(20);
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setCustomTitle(tvTitle);
                        alertDialog.setMessage(Html.fromHtml("Duration must be less than<b> maximum park " + max_time + " " + duration_unit + "s</b> and you already parked <b>" + Renew_parked_time + " " + duration_unit + "s</b> so you can park upto <b>" + remain_time + " " + duration_unit + "s</b> more or less!"));
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        final AlertDialog alertd = alertDialog.create();
                        alertd.show();
                        TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
                    }
                } else {
                    TextView tvTitle = new TextView(getActivity());
                    tvTitle.setText("Payment not processable");
                    tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                    tvTitle.setPadding(0, 10, 0, 0);
                    tvTitle.setTextColor(Color.parseColor("#000000"));
                    tvTitle.setTextSize(20);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setCustomTitle(tvTitle);
                    alertDialog.setMessage("Amount value is $0.00");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    final AlertDialog alertd = alertDialog.create();
                    alertd.show();
                    TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                    messageText.setGravity(Gravity.CENTER_HORIZONTAL);
                }
            } else {
                TextView tvTitle = new TextView(getActivity());
                tvTitle.setText("Alert!!!");
                tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                tvTitle.setPadding(0, 10, 0, 0);
                tvTitle.setTextColor(Color.parseColor("#000000"));
                tvTitle.setTextSize(20);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setCustomTitle(tvTitle);
                alertDialog.setMessage("Select parking hours before proceeding.");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                final AlertDialog alertd = alertDialog.create();
                alertd.show();
                TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                messageText.setGravity(Gravity.CENTER_HORIZONTAL);
            }
        } else {
            TextView tvTitle = new TextView(getActivity());
            tvTitle.setText("Alert!!!");
            tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
            tvTitle.setPadding(0, 10, 0, 0);
            tvTitle.setTextColor(Color.parseColor("#000000"));
            tvTitle.setTextSize(20);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setCustomTitle(tvTitle);
            alertDialog.setMessage("Fill the all fields, in order to Pay & Parking.");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            final AlertDialog alertd = alertDialog.create();
            alertd.show();
            TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
            messageText.setGravity(Gravity.CENTER_HORIZONTAL);
        }
    }

    public void gettotalamount() {

        String no = editno.getText().toString();
        if (!no.equals("") && !statename.equals("State")) {

            if (!shoues.equals("hours")) {
                min = shoues.substring(shoues.indexOf(' ') + 1);
                String hr = shoues.substring(0, shoues.indexOf(' '));
                Double d = new Double(Double.parseDouble(hr));
                hours = d.intValue();
                float minutes;
                String h = "";
                if (!rate.equals("") && !rate1.equals("null")) {
                    double hrr = Double.parseDouble(rate1);
                    double prices = Double.parseDouble(rate);
                    double perhrrate = hrr / prices;
                    finalpayamount = hours / perhrrate;
                    Calendar calendar = Calendar.getInstance();
                    int day = calendar.get(Calendar.DAY_OF_WEEK);

                    switch (day) {
                        case Calendar.SUNDAY:
                            try {
                                if ((!week_end_discount.equals("") || !week_end_discount.equals("null"))) {
                                    double week = Double.parseDouble(week_end_discount);
                                    if (week > 0.0) {
                                        finalpayamount = finalpayamount - (finalpayamount * (week / 100));
                                    }
                                }
                            } catch (Exception e) {

                            }
                        case Calendar.MONDAY:
                            try {
                                if ((!week_end_discount.equals("") || !week_end_discount.equals("null"))) {
                                    double week = Double.parseDouble(week_end_discount);
                                    if (week > 0.0) {
                                        finalpayamount = finalpayamount - (finalpayamount * (week / 100));
                                    }
                                }
                            } catch (Exception e) {

                            }
                        case Calendar.TUESDAY:
                    }

                    boolean is_parked_time = true;
                    Renew_parked_time = Renew_parked_time != null ? (!Renew_parked_time.equals("") ? (!Renew_parked_time.equals("null") ? Renew_parked_time : "") : "") : "";
                    int remain_time = 0;
                    if (!Renew_parked_time.equals("")) {
                        int parked_hours = Integer.parseInt(Renew_parked_time);
                        final_count = parked_hours + hours;
                        int max = Integer.parseInt(max_time);
                        remain_time = max - parked_hours;
                        if (final_count <= max) {
                            is_parked_time = true;
                        } else {
                            is_parked_time = false;
                        }
                    } else {
                        final_count = hours;
                    }


                    if (is_parked_time) {
                        PaypalPaymentIntegration(String.valueOf(finalpayamount));
                    } else {
                        TextView tvTitle = new TextView(getActivity());
                        tvTitle.setText("ParkEZly");
                        tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                        tvTitle.setPadding(0, 10, 0, 0);
                        tvTitle.setTextColor(Color.parseColor("#000000"));
                        tvTitle.setTextSize(20);
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setCustomTitle(tvTitle);
                        alertDialog.setMessage(Html.fromHtml("Duration must be less than<b> maximum park " + max_time + " " + duration_unit + "s</b> and you already parked <b>" + Renew_parked_time + " " + duration_unit + "s</b> so you can park upto <b>" + remain_time + " " + duration_unit + "s</b> more or less!"));
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        final AlertDialog alertd = alertDialog.create();
                        alertd.show();
                        TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
                    }
                } else {
                    TextView tvTitle = new TextView(getActivity());
                    tvTitle.setText("Payment not processable");
                    tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                    tvTitle.setPadding(0, 10, 0, 0);
                    tvTitle.setTextColor(Color.parseColor("#000000"));
                    tvTitle.setTextSize(20);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setCustomTitle(tvTitle);
                    alertDialog.setMessage("Amount value is $0.00");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    final AlertDialog alertd = alertDialog.create();
                    alertd.show();
                    TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                    messageText.setGravity(Gravity.CENTER_HORIZONTAL);
                }
            } else {
                TextView tvTitle = new TextView(getActivity());
                tvTitle.setText("Alert!!!");
                tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                tvTitle.setPadding(0, 10, 0, 0);
                tvTitle.setTextColor(Color.parseColor("#000000"));
                tvTitle.setTextSize(20);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setCustomTitle(tvTitle);
                alertDialog.setMessage("Select parking hours before proceeding.");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                final AlertDialog alertd = alertDialog.create();
                alertd.show();
                TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                messageText.setGravity(Gravity.CENTER_HORIZONTAL);
            }
        } else {
            TextView tvTitle = new TextView(getActivity());
            tvTitle.setText("Alert!!!");
            tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
            tvTitle.setPadding(0, 10, 0, 0);
            tvTitle.setTextColor(Color.parseColor("#000000"));
            tvTitle.setTextSize(20);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setCustomTitle(tvTitle);
            alertDialog.setMessage("Fill the all fields, in order to Pay & Parking.");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            final AlertDialog alertd = alertDialog.create();
            alertd.show();
            TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
            messageText.setGravity(Gravity.CENTER_HORIZONTAL);
        }
    }

    public class updatewallet extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONObject json12;
        String walleturl = "_table/user_wallet";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            currentdate = sdf.format(new Date());
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("paid_date", currentdate);
            jsonValues1.put("last_paid_amt", mParkingTotal);
            jsonValues1.put("ip", ip);
            jsonValues1.put("remember_me", "");
            jsonValues1.put("current_balance", newbal);
            jsonValues1.put("ipn_txn_id", "");
            jsonValues1.put("ipn_custom", "");
            jsonValues1.put("ipn_payment", "");
            jsonValues1.put("date_time", currentdate);
            jsonValues1.put("ipn_address", "");
            jsonValues1.put("user_id", id);
            jsonValues1.put("new_balance", newbal);
            jsonValues1.put("action", "deduction");

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        wallet_id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
            if (getActivity() != null && json12 != null) {
                if (!id.equals("")) {
                    updateWalletBalance();
//                    paymentmethod = "wallet";
//                    showsucessmesg.setVisibility(View.VISIBLE);
//                    ActionStartsHere();
//                    //  new finalupdate().execute();
//                    if (Renew_is_valid.equals("yes")) {
//                        new Update_Parking().execute();
//                    } else {
//                        new finalupdate().execute();
//                    }
                }

            }
            super.onPostExecute(s);
        }
    }

    public class Update_Parking extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String rid = logindeatl.getString("id", "null");
        String id1 = "null";
        JSONObject json, json1;
        String pov2 = "_table/parked_cars";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);

            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            TimeZone zone12 = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone12);
            final String currentDateandTime = Renew_date;
            Date date11 = null;
            try {
                date11 = sdf.parse(currentDateandTime);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date11);
            if (min.equals("Hrs") || min.equals("Hour") || min.equals("Hr")) {
                calendar.add(Calendar.HOUR, hours);
            } else if (min.equals("Months") || min.equals("Month")) {
                calendar.add(Calendar.MONTH, hours);
                calendar.add(Calendar.DATE, -1);
            } else if (min.equals("Days") || min.equals("Day")) {
                calendar.add(Calendar.DATE, hours);
            } else if (min.equals("Weeks") || min.equals("Week")) {
                calendar.add(Calendar.DAY_OF_WEEK_IN_MONTH, hours);
            } else {
                calendar.add(Calendar.MINUTE, hours);
            }

            String expridate = sdf.format(calendar.getTime());
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.US);
            SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            outputFormat.setTimeZone(zone12);
            inputFormat.setTimeZone(zone12);

            Date date = null;
            Date Current = null;
            expiretime = null;
            String final_current_date = null;
            try {
                date = inputFormat.parse(expridate);
                Current = inputFormat.parse(Renew_date);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

            expiretime = outputFormat.format(date);
            final_current_date = outputFormat.format(Current);
            int h = 0;
            if (user_id.equals("null")) {
                h = 0;
            } else {
                h = Integer.parseInt(user_id);
            }

            jsonValues.put("exit_date_time", expiretime);
            jsonValues.put("expiry_time", expiretime);
            jsonValues.put("selected_duration", final_count);
            jsonValues.put("id", ReNew_id);
            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + pov2;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id1 = c.getString("id");

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);

            if (getActivity() != null && json1 != null) {
                if (!id1.equals("null")) {
                    new getparked_car_renew(id1).execute();

                }
            }
            super.onPostExecute(s);
        }
    }

    public class getparked_car_renew extends AsyncTask<String, String, String> {
        String park_id;
        JSONObject json;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String login_id = logindeatl.getString("id", "0");
        String transactionurl = "_table/parked_cars?filter=(user_id%3D" + login_id + ")%20AND%20(parking_status%3D'ENTRY')";
        String id = "null";

        public getparked_car_renew(String id) {
            this.park_id = id;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                        String plnp = c.getString("plate_no");
                        String state = c.getString("pl_state");
                        String state1 = c.getString("state");

                        if (!park_id.equals("null")) {
                            if (id.equals(park_id)) {

                                String city = c.getString("city");
                                String country = c.getString("country");
                                String exit = c.getString("exit_date_time");
                                String extime = c.getString("expiry_time");
                                String max = c.getString("max_duration");
                                String locationcode = c.getString("location_code");
                                String entrytime = c.getString("entry_date_time");
                                String addres = c.getString("address1");
                                String parked_adress = c.getString("marker_address1");
                                String lat = c.getString("lat");
                                String lang = c.getString("lng");
                                String lot = c.getString("lot_number");
                                String lot_row = c.getString("lot_row");
                                String parking_type = c.getString("parking_type");
                                String duration_unit = c.getString("duration_unit");
                                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sh.edit();
                                editor.clear().commit();
                                editor.putString("platno", plnp);
                                editor.putString("state", state);
                                editor.putString("currentdate", entrytime);
                                editor.putString("exip", expiretime);
                                editor.putString("exip1", expiretime);
                                editor.putString("hr", "0");
                                editor.putString("max", max);
                                editor.putString("lat", lat);
                                editor.putString("log", lang);
                                editor.putString("min", duration_unit);
                                editor.putString("id", id);
                                editor.putString("token", "");
                                editor.putString("country", country);
                                editor.putString("city", city);
                                editor.putString("exit_date_time", exit);
                                editor.putString("address", parked_adress);
                                editor.putString("parked_address", addres);
                                editor.putString("locationname", locationcode);
                                editor.putString("markertye", marker);
                                editor.putString("parking_type", parking_type);
                                editor.putString("lot_row", lot_row);
                                editor.putString("lot_no", lot);
                                editor.putString("parkhere", "no");
                                editor.putString("parking_free", "no");
                                editor.putString("parked_time", c.getString("selected_duration"));
                                editor.putString("entryTime", c.getString("entry_date_time"));
                                editor.putString("exitTime", c.getString("exit_date_time"));
                                editor.putString("locationName", c.getString("location_name"));
                                editor.putString("locationCode", c.getString("location_code"));
                                editor.putString("lotId", c.getString("lot_id"));
                                editor.commit();
                                editor.apply();
                                Log.e("cecc", "ecnejnce");
                                break;
                            }
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json != null) {

                if (!id.equals("null")) {
                    if (renew.equals("yes")) {
                        open_timer();
                    } else {
                        direct_open_timer(park_id);
                    }
                }
            }
            super.onPostExecute(s);

        }
    }


    public void open_timer() {
        SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = time.edit();
        ed.putString("result", "true");
        ed.commit();
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.replace(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
        ft.commit();
    }


    public void direct_open_timer(final String park_id) {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String login_id = logindeatl.getString("id", "0");
        if (!login_id.equals("0")) {

            if (!park_id.equals("null")) {
                            /*final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.replace(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                            ft.commitAllowingStateLoss();*/
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                timervehicleinfo pay = new timervehicleinfo();
                fragmentStack.lastElement().onPause();
                ft.add(R.id.My_Container_1_ID, pay, "timer");
                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            } else {
                SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
                SharedPreferences.Editor ed = time.edit();
                ed.putString("result", "true");
                ed.commit();
                           /* final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.add(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                            ft.commitAllowingStateLoss();*/
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                timervehicleinfo pay = new timervehicleinfo();
                fragmentStack.lastElement().onPause();
                ft.add(R.id.My_Container_1_ID, pay, "timer");
                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();

            }
        } else {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Parking Notifications");
            alertDialog.setMessage("Do you want to register for parking notifications");
            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ((vchome) getActivity()).open_login_screen();

                }
            });
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (!park_id.equals("null")) {
                                   /* final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                    ft.replace(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                                    ft.commitAllowingStateLoss();*/

                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        timervehicleinfo pay = new timervehicleinfo();
                        fragmentStack.lastElement().onPause();
                        ft.add(R.id.My_Container_1_ID, pay, "timer");
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                    } else {
                        SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
                        SharedPreferences.Editor ed = time.edit();
                        ed.putString("result", "true");
                        ed.commit();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        timervehicleinfo pay = new timervehicleinfo();
                        fragmentStack.lastElement().onPause();
                        ft.add(R.id.My_Container_1_ID, pay, "timer");
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                                     /*  final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                    ft.add(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                                    ft.commitAllowingStateLoss();*/

                    }
                }
            });
            alertDialog.show();
        }

    }

    public void ActionStartsHere() {
        againStartGPSAndSendFile();
    }

    public void againStartGPSAndSendFile() {
        new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                ActionStartsHere();
                showsucessmesg.setVisibility(View.GONE);
            }

        }.start();
    }

    public class getvehiclenumber extends AsyncTask<String, String, String> {
        JSONObject json = null;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "0");
        String vehiclenourl = "_table/user_vehicles?filter=user_id%3D" + user_id + "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            listofvehicle.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("get_vehicle_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);
                    Log.e("rerpones", String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        String plate_no = c.getString("plate_no");
                        String state = c.getString("registered_state");
                        String id = c.getString("id");
                        Iterator myVeryOwnIterator = statefullname.keySet().iterator();
                       /* while (myVeryOwnIterator.hasNext()) {
                            String key = (String) myVeryOwnIterator.next();
                            if (key.equals(state)) {
                                String value = (String) statefullname.get(key);
                                item.setState(value);
                                break;
                            }
                        }*/
//                        for (int k = 0; k < all_state.size(); k++) {
//                            String state_all = all_state.get(k).getState2Code();
//                            if (state_all.equals(state)) {
//                                item.setState(all_state.get(k).getStateName());
//                            }
//                        }
                        item.setState(state);
                        item.setPlateno(plate_no);
                        item.setId(id);
                        item.setVehicle_country(c.getString("vehicle_country"));
                        item.setIsselect(false);
                        item.setCheckboxselected(false);
                        listofvehicle.add(item);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            //progressBar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity == null) {
                } else {
                    ShowState(getActivity());
                }
            } else {
                rl_progressbar.setVisibility(View.GONE);
            }
            super.onPostExecute(s);
        }
    }

    private void ShowState(Activity context) {
        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        layout = layoutInflater.inflate(R.layout.parkherepopup, null);
        if (popup_state != null && popup_state.isShowing()) {
            popup_state.dismiss();
            popup_state = null;
        }
        popup_state = new PopupWindow(context);
        popup_state.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup_state.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup_state.setContentView(layout);
        popup_state.setFocusable(false);
        popup_state.setAnimationStyle(R.style.animationName);
        popup_state.setBackgroundDrawable(null);
        listofvehicleno = (ListView) layout.findViewById(R.id.listselectvehicl);
        TextView txt_add_vi = (TextView) layout.findViewById(R.id.txtaddvehicle);
        txt_add_vi.setVisibility(View.VISIBLE);
        Resources rs;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int yyy = rl_main.getHeight();
            int yyy1 = rl_title.getHeight();
            int final_position = yyy1 + yyy + 245;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                popup_state.showAtLocation(layout, Gravity.NO_GRAVITY, 0, final_position);
            } else {
                popup_state.showAsDropDown(rl_main, 0, 0);
            }


        } else {
            popup_state.showAsDropDown(rl_main, 0, 0);
        }
        if (listofvehicle.size() > 0) {
            Activity activty = getActivity();
            if (activty == null) {
                if (popup_state != null && popup_state.isShowing()) {
                    popup_state.dismiss();
                    popup_state = null;
                }
            } else {
                CustomAdapter_state adpater = new CustomAdapter_state(getActivity(), listofvehicle, rs = getResources());
                listofvehicleno.setAdapter(adpater);
            }
        }

        txt_add_vi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences add1 = getActivity().getSharedPreferences("addvehicle", Context.MODE_PRIVATE);
                SharedPreferences.Editor ed = add1.edit();
                ed.putString("addvehicle", "yes");
                ed.commit();
                Bundle b = new Bundle();
                b.putString("direct_add", "yes");
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                vehicle_add pay = new vehicle_add();
                pay.registerForListener(Google_Paid_Parking.this);
                pay.setArguments(b);
                ft.add(R.id.My_Container_1_ID, pay, "my_vehicle_add");
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
                if (popup_state != null && popup_state.isShowing()) {
                    popup_state.dismiss();
                    popup_state = null;
                }
            }
        });

        RelativeLayout imageclose;
        imageclose = (RelativeLayout) layout.findViewById(R.id.close);
        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animation = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.popup_off);
                layout.startAnimation(animation);
                if (popup_state != null && popup_state.isShowing()) {
                    popup_state.dismiss();
                    popup_state = null;
                }
            }
        });
        rl_progressbar.setVisibility(View.GONE);
    }

    private void PaypalPaymentIntegration(String amount) {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE, amount);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent, String payamount) {
        return new PayPalPayment(new BigDecimal(payamount), "USD", "Parking Payment", paymentIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.i(TAG, confirm.toJSONObject().toString(4));
                    Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                    JSONObject jsonObj = new JSONObject(confirm.getPayment().toJSONObject().toString(4));
                    Log.e("JSON IS", "LIKE" + jsonObj);
                    Log.e("short_description", "short_description : " + jsonObj.getString("short_description"));
                    Log.e("amount", "amount : " + jsonObj.getString("amount"));
                    Log.e("intent", "intent : " + jsonObj.getString("intent"));
                    Log.e("currency_code", "currency_code : " + jsonObj.getString("currency_code"));
                    JSONObject jsonObjResponse = confirm.toJSONObject().getJSONObject("response");
                    transection_id = jsonObjResponse.getString("id");
                    if (transection_id != null) {
                        isPrePaid = true;
                        paymentmethod = "Paypal";
                        showsucessmesg.setVisibility(View.VISIBLE);
                        ActionStartsHere();
                        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                        String id = logindeatl.getString("id", "null");
                        if (mIsParking) {
                            //add to parked_cars
                            addParkedCar();
                        } else {
                            //add to reserved_parking
                            addReserveParking();
                        }
                    } else {
                        transactionfailalert();
                    }
                    Log.e("transactionId", ":;;;;" + transection_id);

                } catch (JSONException e) {
                    Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                }
            }
        } else {

        }
    }

    public void transactionfailalert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Transaction Fail try again");
        alertDialog.setMessage("Fail");
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alertDialog.show();
    }


    public class CustomAdapter_state extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdapter_state(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txtplateno, txtstate;
            public RelativeLayout rl_main;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {
                vi = inflater.inflate(R.layout.selectvehicle, null);
                holder = new ViewHolder();

                holder.txtplateno = (TextView) vi.findViewById(R.id.txtlicenseplate);
                holder.txtstate = (TextView) vi.findViewById(R.id.txtstate);
                holder.rl_main = (RelativeLayout) vi.findViewById(R.id.rl_main);
                vi.setTag(holder);
                vi.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String platno = holder.txtplateno.getText().toString().toUpperCase();
                                String statename11 = holder.txtstate.getText().toString();
                                editno.setText(platno.toUpperCase());
                                editno.setVisibility(View.GONE);
                                txt_plate_no.setText(platno.toUpperCase());
                                txt_state.setText(statename11.toUpperCase());
                                plateState = statename11.toUpperCase();
                              /*  Iterator myVeryOwnIterator = statefullname.keySet().iterator();
                                while(myVeryOwnIterator.hasNext())
                                {
                                    String key = (String) myVeryOwnIterator.next();
                                    String value = (String) statefullname.get(key);
                                    if (value.equals(statename11))
                                    {
                                        for (int i = 1; i < statearray.length; i++) {
                                            String statename1 = statearray[i].toString();
                                            item ii = new item();
                                            ii.setState(statearray[i].toString());
                                            if (statename1.equals(key)) {
                                                ii.setSelected_parking(true);
                                                statename=statename1;
                                                txt_state.setText(statename);
                                            } else {
                                                ii.setSelected_parking(false);
                                            }
                                            state_array.add(ii);

                                        }
                                        popup_state.dismiss();
                                        ShowHours(getActivity(),stateq);

                                    }
                                }*/

                                for (int k = 0; k < all_state.size(); k++) {
                                    String state_all = all_state.get(k).getStateName();
                                    if (state_all.equals(statename11)) {
                                        String state_code = all_state.get(k).getState2Code();
                                        for (int i = 1; i < all_state.size(); i++) {
                                            String statename1 = all_state.get(i).getState2Code();
                                            item ii = new item();
                                            ii.setState(all_state.get(i).getState2Code());
                                            if (statename1.equals(state_code)) {
                                                ii.setSelected_parking(true);
                                                statename = statename1;
                                                txt_state.setText(statename);
                                            } else {
                                                ii.setSelected_parking(false);
                                            }
                                            state_array.add(ii);

                                        }

                                        popup_state.dismiss();
                                        ShowHours(getActivity(), stateq);
                                    }
                                }
                                show_marke_payment_button();
                            }
                        });
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }
            if (data.size() <= 0) {
                holder.txtplateno.setText("No Data");
            } else {
                /***** Get each Model object from Arraylist ********/
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                /************  Set Model values in Holder elements ***********/
                String cat = tempValues.getPlantno();
                if (cat == null) {
                    holder.txtplateno.setVisibility(View.GONE);
                } else if (cat != null) {
                    Typeface light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Light.otf");
                    holder.txtplateno.setTypeface(light);
                    holder.txtstate.setTypeface(light);
                    holder.txtplateno.setText(tempValues.getPlantno());
                    holder.txtstate.setText(tempValues.getState());
                    if (!rate.equals("") && !rate1.equals("")) {
                        Double du = Double.parseDouble(rate1) / 60.0;
                        txtrate.setText("Rate: $" + rate + "@" + rate1 + duration_unit);
                    }

                    if (position % 2 == 0) {
                        holder.rl_main.setBackgroundColor(Color.parseColor("#ffffff"));
                    } else {
                        holder.rl_main.setBackgroundColor(Color.parseColor("#e0e0e0"));
                    }
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

    public void show_marke_payment_button() {
        platno = editno.getText().toString();
        if (!TextUtils.isEmpty(platno)
                && !TextUtils.isEmpty(statename)
                && !TextUtils.isEmpty(shoues)
                && !platno.equals("") && !statename.equals("State") && !shoues.equals("hours") && !shoues.equals("")) {
            if (rl_sp_row.getVisibility() == View.VISIBLE) {
//                txtmakpayment.setVisibility(View.VISIBLE);
                llParkOptions.setVisibility(View.VISIBLE);
               /* InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);*/
            } else {
//                txtmakpayment.setVisibility(View.VISIBLE);
                llParkOptions.setVisibility(View.VISIBLE);

                /*InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);*/
            }
        } else {
            llParkOptions.setVisibility(View.GONE);
        }
        if (popup_state != null && popup_state.isShowing()) {
            popup_state.dismiss();
            popup_state = null;
        }
    }

    public class finalupdate extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        // http://108.30.248.212:8006/api/v2/pzly01live7/_table/user_locations?filter=user_id%3D5%20AND%20location_code%3DNY-NHP-03
        String rid = logindeatl.getString("id", "null");
        String id1 = "null";
        JSONObject json, json1;
        String pov2 = "_table/parked_cars";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            // jsonValues.put("id", 0);
            // min="Weeks";
            currentdate = sdf.format(new Date());
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            final String currentDateandTime = currentdate;
            Date date11 = null;
            try {
                date11 = sdf.parse(currentDateandTime);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date11);
            if (min.equals("Hrs") || min.equals("Hr")) {
                calendar.add(Calendar.HOUR, hours);
            } else if (min.equals("Months") || min.equals("Month")) {
                calendar.add(Calendar.MONTH, hours);
                calendar.add(Calendar.DATE, -1);
            } else if (min.equals("Days") || min.equals("Day")) {
                calendar.add(Calendar.DATE, hours);
            } else if (min.equals("Weeks") || min.equals("Week")) {
                calendar.add(Calendar.DAY_OF_WEEK_IN_MONTH, hours);
            } else {
                calendar.add(Calendar.MINUTE, hours);
            }

            String expridate = sdf.format(calendar.getTime());
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
            SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

            Date date = null;
            Date Current = null;
            expiretime = null;
            String final_current_date = null;
            try {
                date = inputFormat.parse(expridate);
                Current = inputFormat.parse(currentdate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            expiretime = outputFormat.format(date);
            final_current_date = outputFormat.format(Current);
            int h = 0;
            if (user_id.equals("null")) {
                h = 0;
            } else {
                h = Integer.parseInt(user_id);
            }
            jsonValues.put("parking_type", parkingtype);
            jsonValues.put("township_code", township_code_final);
            jsonValues.put("location_id", location_id);
            jsonValues.put("location_code", location_code);
            jsonValues.put("location_name", locationname);
            jsonValues.put("entry_date_time", currentdate);
            jsonValues.put("exit_date_time", expiretime);
            jsonValues.put("expiry_time", expiretime);
            jsonValues.put("max_duration", max_time);
            jsonValues.put("user_id", h);
            jsonValues.put("permit_id", 0);
            jsonValues.put("subscription_id", 0);
            jsonValues.put("plate_no", platno);
            jsonValues.put("pl_state", plateState);
            jsonValues.put("lat", latitude);
            jsonValues.put("lng", longitude);
            jsonValues.put("address1", adrreslocation);
            jsonValues.put("address2", "");
            jsonValues.put("city", locationname + lost_row1 + lost_no1);
            jsonValues.put("state", statename);
            jsonValues.put("zip", zip_code);
            jsonValues.put("country", "USA");
            jsonValues.put("lot_row", lost_row1);
            jsonValues.put("lot_number", lost_no1);
            jsonValues.put("ip", getLocalIpAddress());
            jsonValues.put("parking_token", token);
            jsonValues.put("parking_status", "ENTRY");
            jsonValues.put("payment_method", paymentmethod);
            jsonValues.put("payment_choice", paymentmethod);
            jsonValues.put("platform", "Android");

            jsonValues.put("parking_rate", parkingRate);

            jsonValues.put("parking_units", String.valueOf(parkingUnits));
            jsonValues.put("parking_qty", parkingQty);

            jsonValues.put("parking_subtotal", mParkingSubTotal);
            jsonValues.put("tr_percent", mTrPercent);
            jsonValues.put("tr_fee", mTrFee);
            jsonValues.put("parking_total", mParkingTotal);
            jsonValues.put("wallet_trx_id", wallet_id);

            if (paymentmethod.equals("Paypal")) {
                jsonValues.put("ipn_txn_id", transection_id);
                jsonValues.put("ipn_payment", "Paypal");
                jsonValues.put("ipn_status", "success");
                jsonValues.put("ipn_address", "");
            } else {
                jsonValues.put("ipn_txn_id", "");
                jsonValues.put("ipn_payment", "");
                jsonValues.put("ipn_status", "");
                jsonValues.put("ipn_address", "");
            }
            jsonValues.put("distance_to_marker", getdistances(locationaddress, adrreslocation));
            jsonValues.put("marker_address1", locationaddress);
            jsonValues.put("marker_address2", "");
            jsonValues.put("marker_city", city);
            jsonValues.put("marker_state", state);
            jsonValues.put("marker_zip", "");
            jsonValues.put("marker_country", "USA");


            //  jsonValues.put("location_name",locationname);
            //   jsonValues.put("name",0);
            jsonValues.put("token", 0);
            String mismatch = "0";
            float dis = getdistances(locationaddress, adrreslocation);
            float ff = dis / 1609;
            if (ff > 0.5) {
                mismatch = "1";
            }
            jsonValues.put("mismatch", mismatch);
            jsonValues.put("marker_lng", lang);
            jsonValues.put("parking_subtotal", mParkingSubTotal);
            // jsonValues.put("userName", "");
            jsonValues.put("marker_lat", lat);
            jsonValues.put("ticket_status", "");
            jsonValues.put("date_time", "");
            jsonValues.put("duration_unit", min);
            jsonValues.put("unit_pricing", rate);
            if (renew.equals("yes")) {
                jsonValues.put("selected_duration", final_count);
            } else {
                jsonValues.put("selected_duration", hours);
            }
            jsonValues.put("pl_country", p_country);
            JSONObject json = new JSONObject(jsonValues);
            Log.e("paramOption", String.valueOf(json));
            String url = getString(R.string.api) + getString(R.string.povlive) + pov2;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {

                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id1 = c.getString("id");

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            if (getActivity() != null && json1 != null) {
                Log.e("#DEBUG", "   finalupdate:  parking_id:  " + id1);
                if (!id1.equals("null")) {


                    if (rl_sp_row.getVisibility() != View.VISIBLE) {

                        new getparked_car(id1).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new updatemanaledlost(id1).execute();
                    }
                }

            }
            super.onPostExecute(s);

        }


    }

    private String getaddress1(Double lat, Double lon) {
        Geocoder gc = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = gc.getFromLocation(lat, lon, 1);
            StringBuilder sb = new StringBuilder();
            StringBuilder addressl = new StringBuilder();
            StringBuilder state_and_ciry = new StringBuilder();
            StringBuilder country = new StringBuilder();
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getLatitude()).append("\n");
                    sb.append(address.getLongitude());
                    addressl.append(address.getAddressLine(0)).append(", ");
                    addressl.append(address.getAddressLine(1));
                    state_and_ciry.append(address.getAddressLine(2));
                    country.append(address.getAddressLine(3));
                    break;
                }
                String loc = addressl.toString();
                String statename = state_and_ciry.toString();
                countryname = country.toString();
                String addressin1 = statename.substring(statename.indexOf(",") + 1);
                this.adrreslocation1 = loc;
                this.state = addressin1;
                return adrreslocation1;
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Address not found!");
                alertDialog.setMessage("Could not find location. Try including the exact location information");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alertDialog.show();
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public class getparked_car extends AsyncTask<String, String, String> {
        String park_id;
        JSONObject json;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String login_id = logindeatl.getString("id", "0");
        String transactionurl = "_table/parked_cars?filter=(user_id%3D" + login_id + ")%20AND%20(parking_status%3D'ENTRY')";
        String id = "null";

        public getparked_car(String id) {
            this.park_id = id;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                        String plnp = c.getString("plate_no");
                        String state = c.getString("pl_state");
                        String state1 = c.getString("state");
                        if (!park_id.equals("null")) {
                            if (id.equals(park_id)) {

                                String city = c.getString("city");
                                String country = c.getString("country");
                                String exit = c.getString("exit_date_time");

                                SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
                                SharedPreferences.Editor ed = time.edit();
                                ed.clear();
                                ed.commit();
                                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sh.edit();
                                editor.putString("platno", platno);
                                editor.putString("state", state);
                                editor.putString("currentdate", currentdate);
                                editor.putString("hr", String.valueOf(hours));
                                editor.putString("max", max_time);
                                editor.putString("id", id);
                                editor.putString("min", min);
                                editor.putString("token", token1);
                                editor.putString("exip", expiretime);
                                editor.putString("lat", String.valueOf(latitude));
                                editor.putString("markertye", marker);
                                editor.putString("log", String.valueOf(longitude));
                                editor.putString("country", country);
                                editor.putString("city", city);
                                editor.putString("exit_date_time", exit);
                                editor.putString("address", locationaddress);
                                editor.putString("parked_address", adrreslocation);
                                editor.putString("locationname", location_code);
                                editor.putString("lot_row", lost_row1);
                                editor.putString("lot_no", lost_no1);
                                editor.putString("title", title);
                                editor.putString("parking_type", parkingtype);
                                editor.putString("parkhere", "no");
                                editor.putString("parking_free", "no");
                                editor.putString("parked_time", c.getString("selected_duration"));
                                editor.putString("entryTime", c.getString("entry_date_time"));
                                editor.putString("exitTime", c.getString("exit_date_time"));
                                editor.putString("locationName", c.getString("location_name"));
                                editor.putString("locationCode", c.getString("location_code"));
                                editor.putString("lotId", c.getString("lot_id"));
                                editor.commit();
                                SharedPreferences sh1 = getActivity().getSharedPreferences("token", Context.MODE_PRIVATE);
                                SharedPreferences.Editor ed1 = sh1.edit();
                                ed1.putString("token", token1);
                                ed1.commit();
                                break;
                            }
                        } else {
                            if (state.equals("null")) {
                                state = state1;
                            }
                            if (plnp.equals(platno) && state.equals(statename)) {
                                String city = c.getString("city");
                                String country = c.getString("country");
                                String exit = c.getString("exit_date_time");
                                String extime = c.getString("expiry_time");
                                String max = c.getString("max_duration");
                                String locationcode = c.getString("location_code");
                                String entrytime = c.getString("entry_date_time");
                                String addres = c.getString("address1");
                                String parked_adress = c.getString("marker_address1");
                                String lat = c.getString("lat");
                                String lang = c.getString("lng");
                                String lot = c.getString("lot_number");
                                String lot_row = c.getString("lot_row");
                                String location_lot_id = c.getString("location_lot_id");
                                String parking_type = c.getString("parking_type");
                                String duration_unit = c.getString("duration_unit");
                                String locationCode = c.getString("location_code");
                                String locationName = c.getString("location_name");
                                String entryDateTime = c.getString("entry_date_time");
                                String exitDateTime = c.getString("exit_date_time");
                                String lotId = c.getString("lot_id");
                                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sh.edit();
                                editor.putString("platno", platno);
                                editor.putString("location_lot_id", location_lot_id);
                                editor.putString("state", statename);
                                editor.putString("currentdate", entrytime);
                                editor.putString("exip", extime);
                                editor.putString("hr", "0");
                                editor.putString("max", max);
                                editor.putString("lat", lat);
                                editor.putString("log", lang);
                                editor.putString("min", duration_unit);
                                editor.putString("id", id);
                                editor.putString("token", "");
                                editor.putString("parked_address", parked_adress);
                                editor.putString("country", country);
                                editor.putString("city", city);
                                editor.putString("exit_date_time", exit);
                                editor.putString("address", addres);
                                editor.putString("locationname", locationcode);
                                editor.putString("markertye", marker);
                                editor.putString("title", title);
                                editor.putString("lot_row", lot);
                                editor.putString("lot_no", lot_row);
                                editor.putString("parking_type", parking_type);
                                editor.putString("parkhere", "no");
                                editor.putString("parking_free", "no");
                                editor.putString("parked_time", c.getString("selected_duration"));
                                editor.putString("entryTime", entryDateTime);
                                editor.putString("exitTime", exitDateTime);
                                editor.putString("locationName", locationName);
                                editor.putString("locationCode", locationCode);
                                editor.putString("lotId", lotId);
                                editor.commit();
                            }
                        }

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            //progressBar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json != null) {

                if (!id.equals("null")) {
                    if (renew.equals("yes")) {
                        open_timer();
                    } else {
                        direct_open_timer(park_id);
                    }
                }
            }
            super.onPostExecute(s);

        }
    }

    public class updatemanaledlost extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String rid = logindeatl.getString("id", "null");
        String exit_status = "_table/location_lot";
        JSONObject json, json1;
        String re_id;
        String id;

        public updatemanaledlost(String idd) {
            this.id = idd;
        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("lot_row", lost_row1);
            jsonValues.put("lot_number", lost_no1);
            jsonValues.put("occupied", "YES");
            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.d("re_id", re_id);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);

            if (getActivity() != null && json1 != null) {
                if (!re_id.equals("null")) {
                    new getparked_car(id).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                super.onPostExecute(s);

            }

        }
    }

    private void showpaypalrandomnumber(Activity context) {

        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View layout = layoutInflater.inflate(R.layout.randomnumber, viewGroup, false);

        popup = new PopupWindow(context);
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(true);
        popup.setFocusable(false);
        popup.setAnimationStyle(R.style.animationName);
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

        final EditText edit_code;
        Button btn_cancel, btn_park_now;

        edit_code = (EditText) layout.findViewById(R.id.edit_randomno);
        btn_cancel = (Button) layout.findViewById(R.id.btn_cancel);
        btn_park_now = (Button) layout.findViewById(R.id.btn_popup_park_now);
        int randomPIN = (int) (Math.random() * 9000) + 1000;
        String val = "" + randomPIN;
        edit_code.setText(val);
        btn_park_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String editno = edit_code.getText().toString();

                if (!edit_code.equals("")) {
                    popup.dismiss();
                    token = Integer.parseInt(editno);
                    token1 = editno;
                    gettotalamount();
                } else {
                    popup.dismiss();
                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });


    }

    public class get_total_hours_parked_today extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String managedlosturl = "_proc/get_total_hours_parked_today";
        String re_id, lat, lang, platno1, state1;

        public get_total_hours_parked_today(String lat, String lang, String platno, String state) {
            this.lat = lat;
            this.lang = lang;
            this.platno1 = platno;
            this.state1 = state;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            JSONObject inlat = new JSONObject();
            JSONObject inlang = new JSONObject();

            JSONObject platno = new JSONObject();
            JSONObject state = new JSONObject();

            try {
                inlat.put("name", "lat");
                inlat.put("value", latitude);
                // inlat.put("value","40.7127243");

                inlang.put("name", "lng");
                inlang.put("value", longitude);
                // inlang.put("value","-74.0065046");

                platno.put("name", "plate_no");
                platno.put("value", platno1);

                state.put("name", "pl_state");
                state.put("value", state1);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            JSONArray jsonArray = new JSONArray();

            jsonArray.put(inlat);
            jsonArray.put(inlang);
            jsonArray.put(platno);
            jsonArray.put(state);

            JSONObject studentsObj = new JSONObject();
            try {
                studentsObj.put("params", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(studentsObj.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);

            HttpResponse response = null;

            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json1 = new JSONArray(responseStr);

                    for (int i = 0; i < json1.length(); i++) {
                        JSONObject josnget = json1.getJSONObject(i);

                        total_hours = josnget.getString("totalhours");
                        Log.e("hors", total_hours);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            rl_progressbar.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            Resources rs;
            if (json1 != null) {
                if (!total_hours.equals("null")) {
                    double max_min = Double.parseDouble(max_time) * 60.0;
                    double min = Double.parseDouble(total_hours) * 60.0;
                    int pri = Integer.parseInt(rate1);

                    if (min > (max_min - pri)) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Limit Exceeded!");
                        alertDialog.setMessage("You have already exceeded the daily limit for parking in this location");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alertDialog.show();
                        int count = 1;
                        int rem = (int) (max_min - min);
                        int rem1 = (int) (max_min - min);
                        values.clear();
                        values.add("hours");
                        int reee = rem / 60;
                        for (int i = 1; i <= reee; i++) {
                            double tempduration = Integer.parseInt(rate1) * i;
                            if (tempduration >= 60) {

                                values.add(String.valueOf(count) + " Hrs");
                                count++;
                            } else {
                                values.add(String.valueOf(tempduration) + " Mins");
                            }
                            values.add(String.valueOf(count) + " Hrs");
                        }

                    } else {
                        int count = 1;
                        int rem = (int) (max_min - min);
                        int rem1 = (int) (max_min - min);
                        values.clear();
                        values.add("hours");

                        int reee = rem / 60;
                        for (int i = 1; i <= reee; i++) {
                            double tempduration = Integer.parseInt(rate1) * i;
                            if (tempduration >= 60) {
                                tempduration = tempduration / 60;
                                values.add(String.valueOf(count) + " Hrs");
                                count++;
                            } else {
                                values.add(String.valueOf(tempduration) + " Mins");
                            }
                        }
                        values.add(String.valueOf(count) + " Hrs");
                    }

                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, values);
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    sphoures.setAdapter(spinnerArrayAdapter);


                } else {
                    double max_min = Double.parseDouble(max_time) * 60.0;
                    double min = 0;
                    int pri = Integer.parseInt(rate1);

                    if (min > (max_min - pri)) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Limit Exceeded!");
                        alertDialog.setMessage("You have already exceeded the daily limit for parking in this location");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alertDialog.show();
                        int rem = (int) (max_min - min);
                        int rem1 = (int) (max_min - min);
                        values.clear();
                        values.add("hours");
                        int reee = rem / 60;
                        for (int i = 1; i <= reee; i++) {
                            double tempduration = Integer.parseInt(rate1) * i;
                            if (tempduration >= 60) {

                                tempduration = tempduration / 60;
                                values.add(String.valueOf(tempduration) + " Hrs");
                            } else {
                                values.add(String.valueOf(tempduration) + " Mins");
                            }
                        }

                    } else {
                        int count = 1;
                        int rem = (int) (max_min - min);
                        int rem1 = (int) (max_min - min);
                        values.clear();
                        values.add("hours");

                        int reee = rem / 60;
                        for (int i = 1; i <= reee; i++) {
                            double tempduration = Integer.parseInt(rate1) * i;
                            if (tempduration >= 60) {

                                tempduration = tempduration / 60;
                                values.add(String.valueOf(count) + " Hrs");
                                count++;
                            } else {
                                values.add(String.valueOf(tempduration) + " Mins");
                            }
                        }
                        values.add(String.valueOf(count) + " Hrs");
                    }

                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, values);
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    sphoures.setAdapter(spinnerArrayAdapter);
                }
            }
            super.onPostExecute(s);
        }
    }


    public class checkcardparkedornotwallet extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONArray array = new JSONArray();
        String parked_id = "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String vechiclenourl = null;
            try {
                String plate = platno;
                plate = plate.replaceAll("\\s+", "");
                vechiclenourl = "_table/parked_cars?filter=(plate_no%3D" + URLEncoder.encode(plate, "utf-8") + ")%20AND%20(pl_state%3D" + statename + ")%20AND%20(parking_status%3D'ENTRY')";
            } catch (Exception e) {
                e.printStackTrace();
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + vechiclenourl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject v = array.getJSONObject(i);
                        parked_id = v.getString("id");
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (!array.isNull(0)) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Vehicle already parked!");
                    alertDialog.setMessage("This Vehicle is already parked, Do you want to exit it from previous parking and park here ?");

                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            new exitsparking(parked_id).execute();
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });
                    alertDialog.show();
                } else {
                    if (id.equals("null")) {
                        new getwalletbal().execute();
                    } else {
                        new getwalletbal().execute();
                    }
                }

            }
            super.onPostExecute(s);

        }
    }

    public class checkcardparkedornotpaidt extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONArray array;
        String tcode = logindeatl.getString("tcode", "null"), username = logindeatl.getString("name", "null");
        String parked_id = "null";

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String vechiclenourl = null;
            try {
                vechiclenourl = "_table/parked_cars?filter=(plate_no%3D" + URLEncoder.encode(platno, "utf-8") + ")%20AND%20(pl_state%3D" + statename + ")%20AND%20(parking_status%3D'ENTRY')";
            } catch (Exception e) {
                e.printStackTrace();
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + vechiclenourl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject v = array.getJSONObject(i);
                        parked_id = v.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (array.length() > 0) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Vehicle already parked!");
                    alertDialog.setMessage("This Vehicle is already parked, Do you want to exit it from previous parking and park here ?");

                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            new exitsparking(parked_id).execute();
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });
                    alertDialog.show();
                } else {
                    gettotalamount();
                }
            }
            super.onPostExecute(s);
        }
    }

    public class exitsparking extends AsyncTask<String, String, String> {
        String exit_status = "_table/parked_cars";
        JSONObject json, json1;
        String re_id = "null";
        String parked_id;

        public exitsparking(String pid) {
            this.parked_id = pid;
        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            currentdate = sdf.format(new Date());
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", parked_id);
            jsonValues.put("parking_status", "EXIT");
            jsonValues.put("exit_date_time", currentdate);
            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.d("re_id", re_id);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);

            if (getActivity() != null && json1 != null) {
                if (!re_id.equals("null")) {
                    canclealarm(Integer.parseInt(parked_id));
                    if (mIsPayNow) {
                        //start payment for parking
                        if (mIsWalletPayment) {
                            //pay with wallet balance
                            getWalletBalance();
                        } else {
                            //pay with paypal
                            startPayPalPaymentRequest();
                        }
                    } else {
                        //add to parked_car with pay later
                        addParkedCar();
                    }
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Something went wrong");
                    alertDialog.setMessage("Please try again");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
            super.onPostExecute(s);
        }
    }

    public float getdistances(String address, String address1) {
        GPSTracker tracker = new GPSTracker(getActivity());

        try {
            Location loc1 = new Location(address1);
            loc1.setLatitude(latitude);
            loc1.setLongitude(longitude);

            Location loc2 = new Location(address);
            loc2.setLatitude(Double.parseDouble(lat));
            loc2.setLongitude(Double.parseDouble(lang));
            float distanceInMeters = loc1.distanceTo(loc2);
            return distanceInMeters;
        } catch (Exception e) {
            return 0;
        }

    }

    public void canclealarm(int id) {
        Intent notificationIntent = new Intent(getActivity(), NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, id);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(getActivity(), id + 1, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent alarmIntent1 = PendingIntent.getBroadcast(getActivity(), id + 2, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent alarmIntent3 = PendingIntent.getBroadcast(getActivity(), id + 3, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
        alarmManager.cancel(alarmIntent);
        alarmIntent.cancel();
        alarmManager.cancel(alarmIntent1);
        alarmIntent1.cancel();
        alarmManager.cancel(alarmIntent3);
        alarmIntent3.cancel();
    }

    private void ShowStatePupup(Activity context, final ArrayList<item> data) {

        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.statepopup_pre, viewGroup, false);
        final PopupWindow popupmanaged = new PopupWindow(context);
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        //  popupmanaged.setAnimationStyle(R.style.animationName);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);
        Button btnparkhere;
        ImageView imageclose;
        GridView grid_managed;
        RelativeLayout rl_popclose;
        ImageView img_state;
        TextView txt_state_;


        btnparkhere = (Button) layout.findViewById(R.id.btn_managed_park);
        grid_managed = (GridView) layout.findViewById(R.id.grid_managed);
        imageclose = (ImageView) layout.findViewById(R.id.img_managed_close);
        RelativeLayout rl_done = (RelativeLayout) layout.findViewById(R.id.rl_done);
        rl_popclose = (RelativeLayout) layout.findViewById(R.id.rl_pop);
        rl_popclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });
        img_state = (ImageView) layout.findViewById(R.id.img_state);
        txt_state_ = (TextView) layout.findViewById(R.id.txt_state_name);

        Resources rs;
        final state_adapter adpter = new state_adapter(getActivity(), data, rs = getResources());
        grid_managed.setAdapter(adpter);


        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  for(int i=0;i<data.size();i++){
                    data.get(i).setSelected_parking(false);
                }*/
                //adpter.notifyDataSetChanged();
                popupmanaged.dismiss();
                // txt_state.setText("State");

            }
        });

        for (int k = 0; k < data.size(); k++) {
            boolean selected_ore = data.get(k).isSelected_parking();

            if (selected_ore) {
                statename = data.get(k).getState();
                img_state.setImageResource(R.mipmap.new_state_icon);
                txt_state_.setText(statename);
                break;
            }
        }

        rl_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();

                for (int k = 0; k < data.size(); k++) {
                    boolean selected_ore = data.get(k).isSelected_parking();

                    if (selected_ore) {
                        statename = data.get(k).getState();
                        txt_state.setText(statename);
                        break;
                    }
                }
                ShowHours(getActivity(), stateq);

                adpter.notifyDataSetChanged();

            }
        });
        grid_managed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                for (int i = 0; i < data.size(); i++) {
                    data.get(i).setSelected_parking(false);
                }
                data.get(position).setSelected_parking(true);
                statename = data.get(position).getState();
                txt_state.setText(statename);
                adpter.notifyDataSetChanged();
                popupmanaged.dismiss();
                ShowHours(getActivity(), stateq);
            }
        });


    }

    public class state_adapter extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        boolean isselect = false;
        int a = 0;
        private Activity activity;
        private ArrayList<item> data;
        private SparseBooleanArray mCheckStates;
        private LayoutInflater inflater = null;

        /*************
         * CustomAdapter Constructor
         *****************/
        public state_adapter(Activity a, ArrayList<item> d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;
            data = d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        /********
         * What is the size of Passed Arraylist Size
         ************/
        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        /******
         * Depends upon data size called for each row , Create each ListView row
         *****/
        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;


            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_state, null);
                holder = new ViewHolder();

                holder.txt_state = (TextView) vi.findViewById(R.id.txt_state);
                holder.img = (ImageView) vi.findViewById(R.id.img_state);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {

            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String cat = tempValues.getState();
                if (cat == null) {
                } else if (cat != null) {

                    if (tempValues.isSelected_parking()) {
                        holder.img.setImageResource(R.mipmap.new_state_icon);
                        holder.txt_state.setTextColor(getResources().getColor(R.color.selected_text));
                    } else {
                        holder.img.setImageResource(R.mipmap.icon_new_state_selected);
                        holder.txt_state.setTextColor(Color.parseColor("#000000"));
                    }

                    holder.txt_state.setText(tempValues.getState());
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }


        public class ViewHolder {

            public TextView txt_state;
            ImageView img;
        }
    }

    private void ShowHours(Activity context, final ArrayList<item> data1) {

        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.hourpopup_p, viewGroup, false);
        final PopupWindow popupmanaged = new PopupWindow(context);
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        //  popupmanaged.setAnimationStyle(R.style.animationName);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);
        Button btnparkhere;
        ImageView imageclose;
        GridView grid_managed;
        RelativeLayout rl_popclose;
        ImageView img_state;
        TextView txt_state_;
        btnparkhere = (Button) layout.findViewById(R.id.btn_managed_park);
        grid_managed = (GridView) layout.findViewById(R.id.grid_managed);
        imageclose = (ImageView) layout.findViewById(R.id.img_managed_close);
        RelativeLayout rl_done = (RelativeLayout) layout.findViewById(R.id.rl_done);
        rl_popclose = (RelativeLayout) layout.findViewById(R.id.rl_pop);
        rl_popclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });
        img_state = (ImageView) layout.findViewById(R.id.img_state);
        txt_state_ = (TextView) layout.findViewById(R.id.txt_state_name);

        Resources rs;
        final hour_adapter adpter = new hour_adapter(getActivity(), data1, rs = getResources());
        grid_managed.setAdapter(adpter);


        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  for(int i=0;i<data.size();i++){
                    data.get(i).setSelected_parking(false);
                }
                adpter.notifyDataSetChanged();*/
                popupmanaged.dismiss();
                // txt_hours.setText("Select Hour");

            }
        });

        for (int k = 0; k < data1.size(); k++) {
            boolean selected_ore = data1.get(k).isSelected_parking();

            if (selected_ore) {
                shoues = data1.get(k).getState();
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    txt_state_.setText(hr + " Hrs");
                } else {
                    txt_state_.setText(shoues);
                }
                img_state.setImageResource(R.mipmap.new_cook_hr);
                break;
            }
        }

        rl_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();

                for (int k = 0; k < data1.size(); k++) {
                    boolean selected_ore = data1.get(k).isSelected_parking();

                    if (selected_ore) {
                        shoues = data1.get(k).getState();
                        if (shoues.contains("Hour")) {
                            String hr = shoues.substring(0, shoues.indexOf(' '));
                            txt_hours.setText(hr + " Hrs");
                        } else {
                            txt_hours.setText(shoues);
                        }
                        break;
                    }
                }


            }
        });
        grid_managed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                for (int i = 0; i < data1.size(); i++) {
                    data1.get(i).setSelected_parking(false);
                }
                data1.get(position).setSelected_parking(true);
                shoues = data1.get(position).getState();
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    txt_hours.setText(hr + " Hrs");
                } else {
                    txt_hours.setText(shoues);
                }
                adpter.notifyDataSetChanged();
                popupmanaged.dismiss();
            }
        });


    }

    public class hour_adapter extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        boolean isselect = false;
        int a = 0;
        private Activity activity;
        private ArrayList<item> data;
        private SparseBooleanArray mCheckStates;
        private LayoutInflater inflater = null;

        /*************
         * CustomAdapter Constructor
         *****************/
        public hour_adapter(Activity a, ArrayList<item> d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;
            data = d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        /********
         * What is the size of Passed Arraylist Size
         ************/
        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        /******
         * Depends upon data size called for each row , Create each ListView row
         *****/
        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;


            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_state, null);
                holder = new ViewHolder();

                holder.txt_state = (TextView) vi.findViewById(R.id.txt_state);
                holder.img = (ImageView) vi.findViewById(R.id.img_state);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {

            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String cat = tempValues.getState();
                if (cat == null) {
                } else if (cat != null) {

                    if (tempValues.isSelected_parking()) {
                        holder.img.setImageResource(R.mipmap.new_cook_hr);
                        holder.txt_state.setTextColor(getResources().getColor(R.color.selected_text));
                    } else {
                        holder.img.setImageResource(R.mipmap.icon_cook_un);
                        holder.txt_state.setTextColor(Color.parseColor("#000000"));
                    }

                    holder.txt_state.setText(tempValues.getState());
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }


        public class ViewHolder {

            public TextView txt_state;
            ImageView img;
        }
    }

    public class reserveNow extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String rid = logindeatl.getString("id", "null");
        String id1 = "null";
        JSONObject json, json1;
        String pov2 = "_table/reserved_parkings";
        //        String pov2 = "_table/sdafga";
        boolean isPayLater = false;
        String paramOption = "";
        private String dueBy = "";
        SharedPreferences logindeatl1 = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl1.getString("id", "null");

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//            TimeZone zonen = TimeZone.getTimeZone("UTC");
//            sdf.setTimeZone(zonen);

            if (!isPrePaid) {
                if (!TextUtils.isEmpty(reservationPostPayTerms)) {
                    switch (reservationPostPayTerms) {
                        case "ENTRY":
                            dueBy = reserveEntryTime;
                            break;
                        case "EXIT":
                            dueBy = reserveExpiryTime;
                            break;
                        case "1 Day":
                            calendar = Calendar.getInstance();
                            calendar.add(Calendar.DAY_OF_MONTH, 1);
                            dueBy = sdf.format(new Date(calendar.getTimeInMillis()));
                            break;
                        case "7 Days":
                            calendar = Calendar.getInstance();
                            calendar.add(Calendar.DAY_OF_MONTH, 7);
                            dueBy = sdf.format(new Date(calendar.getTimeInMillis()));
                            break;
                        case "15 Days":
                            calendar = Calendar.getInstance();
                            calendar.add(Calendar.DAY_OF_MONTH, 15);
                            dueBy = sdf.format(new Date(calendar.getTimeInMillis()));
                            break;
                        case "30 Days":
                            calendar = Calendar.getInstance();
                            calendar.add(Calendar.DAY_OF_MONTH, 30);
                            dueBy = sdf.format(new Date(calendar.getTimeInMillis()));
                            break;
                    }
                }
            }

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            if (params.length != 0) {
                paramOption = params[0];
            }
            isPayLater = !TextUtils.isEmpty(paramOption) && paramOption.equals("payLater");
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            currentdate = sdf.format(new Date());
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//            TimeZone zone12 = TimeZone.getTimeZone("UTC");
//            sdf.setTimeZone(zone12);
            final String currentDateandTime = currentdate;
            Date date11 = null;
            try {
                date11 = sdf.parse(currentDateandTime);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date11);
            if (min.equals("Hrs") || min.equals("Hr")) {
                calendar.add(Calendar.HOUR, hours);
            } else if (min.equals("Months") || min.equals("Month")) {
                calendar.add(Calendar.MONTH, hours);
                calendar.add(Calendar.DATE, -1);
            } else if (min.equals("Days") || min.equals("Day")) {
                calendar.add(Calendar.DATE, hours);
            } else if (min.equals("Weeks") || min.equals("Week")) {
                calendar.add(Calendar.DAY_OF_WEEK_IN_MONTH, hours);
            } else {
                calendar.add(Calendar.MINUTE, hours);
            }

            String expridate = sdf.format(calendar.getTime());
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
            SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//            outputFormat.setTimeZone(zone12);
//            inputFormat.setTimeZone(zone12);

            Date date = null;
            Date Current = null;
            expiretime = null;
            String final_current_date = null;
            try {
                date = inputFormat.parse(expridate);
                Current = inputFormat.parse(currentdate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            expiretime = outputFormat.format(date);
            final_current_date = outputFormat.format(Current);
            int h = 0;
            if (user_id.equals("null")) {
                h = 0;
            } else {
                h = Integer.parseInt(user_id);
            }
            parkingtype = marker_type;
            String plate = platno;

            jsonValues.put("reserve_entry_time", reserveEntryTime);
            jsonValues.put("reserve_expiry_time", reserveExpiryTime);
            jsonValues.put("reserve_exit_date_time", reserveExpiryTime);
            jsonValues.put("reservation_date_time", reserveCurrentTime);

            Log.e("#DEBUG", "   reserveEntryTime:  " + reserveEntryTime
                    + "   reserveExpiryTime:  " + reserveExpiryTime
                    + "   reserveCurrentTime:  " + reserveCurrentTime
                    + "    dueBy:  " + dueBy);

            Log.e("#DEBUG", "    reservationPostPayTerms:  " + reservationPostPayTerms);

            jsonValues.put("company_type_id", "");
            jsonValues.put("company_type", "");
            jsonValues.put("manager_type_id", "");
            jsonValues.put("manager_type", "");
            try {
                if (!TextUtils.isEmpty(companyId)) {
                    jsonValues.put("company_id", Integer.valueOf(companyId));
                } else {
                    jsonValues.put("company_id", "");
                }

                if (!TextUtils.isEmpty(twpId)) {
                    jsonValues.put("twp_id", Integer.valueOf(twpId));
                } else {
                    jsonValues.put("twp_id", "");
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("#DEBUG", "   reserveNow:  Error:  " + e.getMessage());
            }

            if (isPrePaid) {
                jsonValues.put("reserve_payment_condition", "PRE-PYMT");
                jsonValues.put("reserve_payment_due_by", reserveCurrentTime);
                jsonValues.put("reserve_payment_paid", "PAID");
            } else {
                jsonValues.put("reserve_payment_condition", reservationPostPayTerms);
                jsonValues.put("reserve_payment_due_by", dueBy);
                jsonValues.put("reserve_payment_paid", "UNPAID");
            }

            jsonValues.put("reservation_status", "ACTIVE");

//            jsonValues.put("entry_date_time", "");
//            jsonValues.put("exit_date_time", "");
//            jsonValues.put("expiry_time", "");
            jsonValues.put("max_duration", max_time);
            jsonValues.put("payment_method", paymentmethod);
            jsonValues.put("payment_choice", paymentmethod);

            jsonValues.put("tr_percent", tr_percentage1);
            jsonValues.put("tr_fee", tr_fee1);
            jsonValues.put("parking_total", finallab);
            jsonValues.put("parking_rate", "0");
            jsonValues.put("wallet_trx_id", wallet_id);

            if (paymentmethod.equals("Paypal")) {
                jsonValues.put("ipn_txn_id", transection_id);
                jsonValues.put("ipn_payment", "Paypal");
                jsonValues.put("ipn_status", "success");
                jsonValues.put("ipn_address", "");
            } else {
                jsonValues.put("ipn_txn_id", "");
                jsonValues.put("ipn_payment", "");
                jsonValues.put("ipn_status", "");
                jsonValues.put("ipn_address", "");
            }

            plate = plate.replaceAll("\\s+", "");
            jsonValues.put("parking_type", parkingtype);
            jsonValues.put("township_code", township_code_final);
            jsonValues.put("location_code", location_code);

            jsonValues.put("user_id", h);
            jsonValues.put("permit_id", 0);
            jsonValues.put("subscription_id", 0);
            jsonValues.put("plate_no", plate);
            jsonValues.put("pl_state", statename);
            jsonValues.put("lat", latitude);
            jsonValues.put("lng", longitude);
            jsonValues.put("address1", adrreslocation);
            jsonValues.put("address2", "");
            jsonValues.put("city", locationname + lost_row1 + lost_no1);
            jsonValues.put("state", statename);
            jsonValues.put("zip", zip_code);
            jsonValues.put("country", "USA");
            jsonValues.put("lot_row", lost_row1);
            jsonValues.put("lot_number", lost_no1);
            jsonValues.put("lot_id", lost_row1 + ":" + lost_no1);
            jsonValues.put("ip", getLocalIpAddress());
            jsonValues.put("parking_token", token);
            jsonValues.put("parking_status", "RESERVED");
            jsonValues.put("platform", "Android");

            //TODO need to check
            jsonValues.put("parking_units", "ENTRY");
            jsonValues.put("parking_qty", 1);

            jsonValues.put("ipn_custom", "");

            jsonValues.put("location_id", location_id);

            String mismatch = "0";
            float dis = getdistances(locationaddress, adrreslocation);
            float ff = dis / 1609;
            if (ff > 0.5) {
                mismatch = "1";
            }
            jsonValues.put("distance_to_marker", getdistances(locationaddress, adrreslocation));
            jsonValues.put("marker_address1", locationaddress);
            jsonValues.put("marker_address2", "");
            jsonValues.put("marker_city", city);
            jsonValues.put("marker_state", state);
            jsonValues.put("marker_zip", "");
            jsonValues.put("marker_country", "USA");
            jsonValues.put("token", 0);
            jsonValues.put("mismatch", mismatch);
            jsonValues.put("marker_lng", lang);
            jsonValues.put("marker_lat", lat);
            jsonValues.put("ticket_status", "");
            jsonValues.put("date_time", "");
            jsonValues.put("duration_unit", min);
            jsonValues.put("unit_pricing", rate);
            if (renew.equals("yes")) {
                jsonValues.put("selected_duration", final_count);
            } else {
                jsonValues.put("selected_duration", hours);
            }
            jsonValues.put("pl_country", p_country);
            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + pov2;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "    reserveNow:  Response:  " + responseStr);
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id1 = c.getString("id");

                    }
                } catch (IOException e) {
                    Log.e("#DEBUG", "    reserveNow:  Error:  " + e.getMessage());
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.e("#DEBUG", "    reserveNow:  Error:  " + e.getMessage());
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            //progressBar.setVisibility(View.GONE);
            if (getActivity() != null && json1 != null) {
                if (!id1.equals("null")) {
                    Toast.makeText(getActivity(), "Parking Reserved Successfully!", Toast.LENGTH_SHORT).show();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ReservedParkingFragment pay = new ReservedParkingFragment();
                    fragmentStack.lastElement().onPause();
                    ft.add(R.id.My_Container_1_ID, pay, "home");
                    fragmentStack.lastElement().onPause();
                    ft.remove(fragmentStack.pop());
                    fragmentStack.lastElement().onPause();
                    ft.remove(fragmentStack.pop());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                }
            }
            super.onPostExecute(s);
        }
    }
}
