package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

/**
 * Created by Significant Info on 3/14/2018.
 */

public class Fragment_Permit_One extends Fragment {

    View rootView;
    Activity activity;
    Context mContext;
    ConnectionDetector cd;
    String user_id;
    ListView lv_Permit;
    ProgressBar progressbar;
    RelativeLayout rl_progressbar, rl_main;
    ArrayList<item> township_array_list, array_parking_permits;
    CustomAdapterTwonship customAdapterTwonship;
    AdapterParking_Permit adapterParking_permit;
    Animation animation;
    String twp_id, Official_logo, Township_logo, Manager_type, Manager_type_id;
    String township_type[] = {"Paid", "free", "Managed Paid", "Managed Free"}, township_id;
    TextView txt_Title;

    TextView txt_No_Record;
    String parkingurl = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_permit, container, false);

        activity = getActivity();
        mContext = getContext();
        township_array_list = new ArrayList<>();

        cd = new ConnectionDetector(activity);
        SharedPreferences logindeatl = activity.getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = logindeatl.getString("id", "null");

        if (getArguments() != null) {
            twp_id = getArguments().getString("twp_id");
            Official_logo = getArguments().getString("Official_logo");
            Township_logo = getArguments().getString("Township_logo");
            Manager_type = getArguments().getString("Manager_type");
            Manager_type_id = getArguments().getString("Manager_type_id");

            if (Manager_type.equalsIgnoreCase("TOWNSHIP")) {
                parkingurl = "_table/parking_permits?filter=(manager_type%3D" + Manager_type + ")%20AND%20(active%3D'YES')%20AND%20(twp_id%3D" + twp_id + ")";
            } else if (Manager_type.equalsIgnoreCase("COMMERCIAL")) {
                parkingurl = "_table/parking_permits?filter=(manager_type%3D" + Manager_type + ")%20AND%20(active%3D'YES')%20AND%20(twp_id%3D" + twp_id + ")";
            } else {
                parkingurl = "_table/parking_permits?filter=(manager_type%3D" + Manager_type + ")%20AND%20(active%3D'YES')%20AND%20(twp_id%3D" + twp_id + ")";
            }
        }

        rl_main = (RelativeLayout) rootView.findViewById(R.id.rl_main);
        rl_progressbar = (RelativeLayout) rootView.findViewById(R.id.rl_progressbar);
        progressbar = (ProgressBar) rootView.findViewById(R.id.progressbar);
        lv_Permit = (ListView) rootView.findViewById(R.id.lv_Permit);
        txt_Title = (TextView) rootView.findViewById(R.id.txt_Title);
        txt_No_Record = (TextView) rootView.findViewById(R.id.txt_No_Record);
        txt_Title.setText("Select Permit");

        if (cd.isConnectingToInternet()) {
            try {
                new getPermitData().execute();
            } catch (Exception e) {
                e.printStackTrace();
            }

            lv_Permit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Bundle bundle = new Bundle();
                    bundle.putString("township_id", array_parking_permits.get(position).getId());
                    bundle.putString("Townshipcode", array_parking_permits.get(position).getTownshipcode());
                    bundle.putString("Permit_name", array_parking_permits.get(position).getPermit_name());
                    bundle.putString("Cost", array_parking_permits.get(position).getCost());
                    bundle.putString("Expires_by", array_parking_permits.get(position).getExpires_by());
                    bundle.putString("Renewable", array_parking_permits.get(position).getRenewable());
                    bundle.putString("permit_nextnum", array_parking_permits.get(position).getPermit_nextnum());
                    bundle.putString("requirements", array_parking_permits.get(position).getRequirements());
                    bundle.putString("appl_req_download", array_parking_permits.get(position).getAppl_req_download());
                    bundle.putString("Official_logo", Official_logo);
                    bundle.putString("twp_id", twp_id);
                    bundle.putString("Manager_type", Manager_type);
                    bundle.putString("Manager_type_id", Manager_type_id);

                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Fragment_Permit_Second pay = new Fragment_Permit_Second();
                    ft.add(R.id.My_Container_1_ID, pay);
                    pay.setArguments(bundle);
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                }
            });

        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");

            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            alertDialog.show();
        }
        return rootView;
    }

    public class getPermitData extends AsyncTask<String, String, String> {
        //String vehiclenourl = "_table/parking_permits?filter=twp_id%3D'" + URLEncoder.encode(twp_id, "utf8") + "'";

        JSONObject json = null;

        @Override
        protected void onPreExecute() {
            array_parking_permits = new ArrayList<item>();
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("get_vehicle_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;

            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);
                    Log.e("rerpones", String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setId(c.getString("id"));
                        item1.setDate(c.getString("date_time"));
                        item1.setTownshipcode(c.getString("township_code"));
                        item1.setTownship_name(c.getString("township_name"));
                        item1.setPermit_type(c.getString("permit_type"));
                        item1.setPermit_name(c.getString("permit_name"));
                        item1.setCovered_locations(c.getString("covered_locations"));
                        item1.setCost(c.getString("cost"));
                        item1.setYear(c.getString("year"));
                        item1.setLocation_address(c.getString("location_address"));
                        item1.setActive(c.getString("active"));
                        item1.setScheme_type(c.getString("scheme_type"));
                        item1.setPermit_prefix(c.getString("permit_prefix"));
                        item1.setPermit_nextnum(c.getString("permit_nextnum"));
                        item1.setExpires_by(c.getString("expires_by"));
                        item1.setRenewable(c.getString("renewable"));
                        item1.setRequirements(c.getString("requirements"));
                        item1.setAppl_req_download(c.getString("appl_req_download"));
                        item1.setIsslected(false);
                        array_parking_permits.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity != null) {
                    if (array_parking_permits.size() > 0) {
                        adapterParking_permit = new AdapterParking_Permit(getActivity(), array_parking_permits, getResources());
                        lv_Permit.setAdapter(adapterParking_permit);
                    } else {
                        txt_No_Record.setVisibility(View.VISIBLE);
                        lv_Permit.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

    public class AdapterParking_Permit extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public AdapterParking_Permit(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txt_township_code, txt_permit_type, txt_cost,
                    txt_township_name, txt_permit_name, txt_townshipCode;
            ImageView img_township;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_parking_permits, null);
                holder = new ViewHolder();

                holder.txt_township_code = (TextView) vi.findViewById(R.id.txt_township_code);
                holder.txt_permit_type = (TextView) vi.findViewById(R.id.txt_permit_type);
                holder.txt_cost = (TextView) vi.findViewById(R.id.txt_cost);
                holder.txt_township_name = (TextView) vi.findViewById(R.id.txt_township_name);
                holder.txt_permit_name = (TextView) vi.findViewById(R.id.txt_permit_name);
                holder.txt_townshipCode = (TextView) vi.findViewById(R.id.txt_townshipCode);
                holder.img_township = (ImageView) vi.findViewById(R.id.img_township);

                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }

            try {
                state = data.get(position);
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.txt_township_code.setText(data.get(position).getTownshipcode());
            holder.txt_permit_type.setText(data.get(position).getPermit_type());
            holder.txt_cost.setText(data.get(position).getCost());
            holder.txt_township_name.setText(data.get(position).getTownship_name());
            holder.txt_permit_name.setText(data.get(position).getPermit_name());
            holder.txt_townshipCode.setVisibility(View.VISIBLE);
            holder.txt_townshipCode.setText(data.get(position).getExpires_by());

            Picasso.with(context).load(Township_logo).into(holder.img_township);


            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }


    public class gettownship_list extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String parkingurl = "_table/townships_manager";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            township_array_list.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    Log.e("responseStr ", " : " + responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setId(c.getString("id"));
                        item1.setDate(c.getString("date_time"));
                        item1.setTitle(c.getString("manager_id"));
                        item1.setManager_type(c.getString("manager_type"));
                        item1.setLot_manager(c.getString("lot_manager"));
                        item1.setAddress(c.getString("address"));
                        item1.setState(c.getString("state"));
                        item1.setCity(c.getString("city"));
                        item1.setCountry(c.getString("country"));
                        item1.setZip_code(c.getString("zip"));
                        item1.setTownship_logo(c.getString("township_logo"));
                        item1.setOfficial_logo(c.getString("official_logo"));
                        item1.setV_user_id(c.getString("user_id"));
                        item1.setIsslected(false);
                        township_array_list.add(item1);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            super.onPostExecute(s);
        }
    }

    public class CustomAdapterTwonship extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdapterTwonship(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txt_title, txt_address;
            ImageView img_check, img_township;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final CustomAdapterTwonship.ViewHolder holder;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_twonship_list, null);
                holder = new CustomAdapterTwonship.ViewHolder();

                holder.txt_title = (TextView) vi.findViewById(R.id.txt_code);
                holder.txt_address = (TextView) vi.findViewById(R.id.txt_address);
                holder.img_check = (ImageView) vi.findViewById(R.id.img_check);
                holder.img_township = (ImageView) vi.findViewById(R.id.img_township);

                vi.setTag(holder);
            } else
                holder = (CustomAdapterTwonship.ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (data.size() <= 0) {


            } else {
                holder.txt_title.setText(data.get(position).getTitle());
                holder.txt_address.setText(data.get(position).getAddress() + " " + data.get(position).getCountry());
                Picasso.with(context).load(data.get(position).getTownship_logo()).into(holder.img_township);

                if (data.get(position).getIsslected()) {
                    holder.img_check.setVisibility(View.VISIBLE);
                } else {
                    holder.img_check.setVisibility(View.GONE);
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

}
