package com.softmeasures.eventezlyadminplus.frament.event;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.frament.commercial_parking_main;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.EventLocationAddEditFragment;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.SessionLocationAddEditFragment;
import com.softmeasures.eventezlyadminplus.frament.frg_private_selecte_location;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.EventAddress;
import com.softmeasures.eventezlyadminplus.models.Manager;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class CoveredLocationsFragment extends BaseFragment {

    ConnectionDetector cd;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar;
    String id, select_town, Township_name;
    ListView listofvehicleno;
    ArrayList<item> township_array_list;

    CustomAdapterTwonship customAdapterTwonship;
    CustomAdapter customAdapter;
    CustomAdapterPrivate customAdapterPrivate;
    ListView lv_Township;
    TextView txt_SelectAll, txt_DeSelectAll, txt_Next;
    private Manager selectedManager;
    private boolean isEdit = false, isSession = false;
    public static ArrayList<item> selectedLocations;
    private TextView tvBtnAddNewLocation;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            selectedManager = new Gson().fromJson(getArguments().getString("selectedManager"), Manager.class);
            isEdit = getArguments().getBoolean("isEdit", false);
            isSession = getArguments().getBoolean("isSession", false);
        }
        return inflater.inflate(R.layout.frag_covered_locations, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        id = logindeatl.getString("id", "null");

        if (selectedManager != null) {
            select_town = selectedManager.getManager_id();
            Township_name = selectedManager.getManager_type();
        }

        try {
            if (Township_name.equalsIgnoreCase("Township")) {
                new getPermitData().execute();
            } else if (Township_name.equalsIgnoreCase("Commercial")) {
                new getGoogle_list().execute();
            } else if (Township_name.equalsIgnoreCase("Private")) {
                new getOther_list().execute();
            } else {
                new getOther_list().execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (myApp.isLocationAdded) {
            myApp.isLocationAdded = false;
            try {
                if (Township_name.equalsIgnoreCase("Township")) {
                    new getPermitData().execute();
                } else if (Township_name.equalsIgnoreCase("Commercial")) {
                    new getGoogle_list().execute();
                } else if (Township_name.equalsIgnoreCase("Private")) {
                    new getOther_list().execute();
                } else {
                    new getOther_list().execute();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {

        tvBtnAddNewLocation.setOnClickListener(v -> {
            openAddNewLocation();
        });

        txt_SelectAll.setOnClickListener(v -> {
            if (township_array_list.size() > 0) {
                for (int i = 0; i < township_array_list.size(); i++) {
                    township_array_list.get(i).setIsslected(true);
                }
                if (select_town.equalsIgnoreCase("Commercial")) {
                    customAdapter.notifyDataSetChanged();
                } else if (select_town.equalsIgnoreCase("Private")) {
                    customAdapterPrivate.notifyDataSetChanged();
                } else {
                    customAdapterTwonship.notifyDataSetChanged();
                }

            }
        });

        txt_DeSelectAll.setOnClickListener(v -> {
            if (township_array_list.size() > 0) {
                for (int i = 0; i < township_array_list.size(); i++) {
                    township_array_list.get(i).setIsslected(false);
                }
                if (select_town.equalsIgnoreCase("Commercial")) {
                    customAdapter.notifyDataSetChanged();
                } else if (select_town.equalsIgnoreCase("Private")) {
                    customAdapterPrivate.notifyDataSetChanged();
                } else {
                    customAdapterTwonship.notifyDataSetChanged();
                }
            }
        });

        txt_Next.setOnClickListener(v -> {
            selectedLocations = new ArrayList<item>();
            if (township_array_list.size() > 0) {
                for (int i = 0; i < township_array_list.size(); i++) {
                    if (township_array_list.get(i).getIsslected()) {
                        selectedLocations.add(township_array_list.get(i));
                    }
                }
            }
            if (selectedLocations.size() > 0) {
                EventLocationAddEditFragment.selectedLocations.clear();
                EventLocationAddEditFragment.eventAddresses.clear();
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < selectedLocations.size(); i++) {

                    EventAddress eventAddress = new EventAddress();
                    eventAddress.setEventAddress(selectedLocations.get(i).getAddress());
                    eventAddress.setLat(Double.parseDouble(selectedLocations.get(i).getLat()));
                    eventAddress.setLng(Double.parseDouble(selectedLocations.get(i).getLng()));
                    if (isSession) SessionLocationAddEditFragment.eventAddresses.add(eventAddress);
                    else EventLocationAddEditFragment.eventAddresses.add(eventAddress);

                    if (i != 0) {
                        stringBuilder.append(",");
                    }
                    stringBuilder.append(selectedLocations.get(i).getLocation_code());
                    if (isSession) {
                        SessionLocationAddEditFragment.selectedLocations.add(selectedLocations.get(i).getLocation_code());
                    } else
                        EventLocationAddEditFragment.selectedLocations.add(selectedLocations.get(i).getLocation_code());

                }
                if (isSession) {
                    if (SessionLocationAddEditFragment.binding != null) {
                        SessionLocationAddEditFragment.binding.etCoveredLocations.setText(stringBuilder.toString());
                    }
                    if (SessionLocationAddEditFragment.eventAddressAdapter != null) {
                        (SessionLocationAddEditFragment.eventAddressAdapter).notifyDataSetChanged();
                    }
                } else {
                    if (EventLocationAddEditFragment.binding != null) {
                        EventLocationAddEditFragment.binding.etCoveredLocations.setText(stringBuilder.toString());
                    }
                    if (EventLocationAddEditFragment.eventAddressAdapter != null) {
                        (EventLocationAddEditFragment.eventAddressAdapter).notifyDataSetChanged();
                    }
                }
                getActivity().onBackPressed();
            } else {
                Toast.makeText(getContext(), "Please select location", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void openAddNewLocation() {
        if (selectedManager != null && selectedManager.getManager_type() != null
                && !TextUtils.isEmpty(selectedManager.getManager_type())) {
            if (selectedManager.getManager_type().equals("TOWNSHIP")) {
//                                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
//                                    SelectAddressFragment pay = new SelectAddressFragment();
//                                    Bundle bundle = new Bundle();
//                                    bundle.putBoolean("isNewLocation", true);
//                                    bundle.putString("selectedManager", new Gson().toJson(selectedManager));
//                                    pay.setArguments(bundle);
//                                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
//                                    ft.add(R.id.My_Container_1_ID, pay);
//                                    fragmentStack.lastElement().onPause();
//                                    ft.hide(fragmentStack.lastElement());
//                                    fragmentStack.push(pay);
//                                    ft.commitAllowingStateLoss();
            } else if (selectedManager.getManager_type().equals("COMMERCIAL")) {
                myApp.setSelectedManager(selectedManager);
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                commercial_parking_main pay = new commercial_parking_main();
                Bundle bundle = new Bundle();
                bundle.putBoolean("isNewLocation", true);
                bundle.putString("selectedManager", new Gson().toJson(selectedManager));
                pay.setArguments(bundle);
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            } else if (selectedManager.getManager_type().equals("OTHER")) {
                myApp.setSelectedManager(selectedManager);
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                frg_private_selecte_location pay = new frg_private_selecte_location();
                Bundle bundle = new Bundle();
                bundle.putBoolean("isNewLocation", true);
                bundle.putString("selectedManager", new Gson().toJson(selectedManager));
                pay.setArguments(bundle);
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        }
    }

    @Override
    protected void initViews(View v) {

        tvBtnAddNewLocation = v.findViewById(R.id.tvBtnAddNewLocation);
        txt_SelectAll = v.findViewById(R.id.txt_SelectAll);
        txt_DeSelectAll = v.findViewById(R.id.txt_DeSelectAll);
        txt_Next = v.findViewById(R.id.txt_Next);
        lv_Township = v.findViewById(R.id.lv_Township);
        progressBar = v.findViewById(R.id.progressbar);
        rl_progressbar = v.findViewById(R.id.rl_progressbar);
        rl_progressbar.setOnClickListener(v1 -> {

        });

    }

    public class getPermitData extends AsyncTask<String, String, String> {

        String vehiclenourl = "_table/township_parking_partners?filter=(township_code%3D'" + URLEncoder.encode(select_town, "utf-8") + "')";

        JSONObject json = null;

        public getPermitData() throws UnsupportedEncodingException {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            township_array_list = new ArrayList<item>();
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("get_vehicle_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;

            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);
                    Log.e("rerpones", String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setId(c.getString("id"));
                        item1.setLocation_id(c.getString("location_id"));
                        item1.setActive(c.getString("active"));
                        item1.setAddress(c.getString("address"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setIn_effect(c.getString("in_effect"));
                        item1.setLat(c.getString("lat"));
                        item1.setLng(c.getString("lng"));
                        item1.setLots_avbl(c.getString("lots_avbl"));
                        item1.setLots_total(c.getString("lots_total"));
                        item1.setMarker_type(c.getString("marker_type"));
                        item1.setNo_parking_times(c.getString("no_parking_times"));
                        item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        item1.setOff_peak_ends(c.getString("off_peak_ends"));
                        item1.setOff_peak_starts(c.getString("off_peak_starts"));
                        item1.setParking_times(c.getString("parking_times"));
                        item1.setRenewable(c.getString("renewable"));
                        item1.setTitle(c.getString("title"));
                        item1.setUser_id(c.getString("user_id"));
                        item1.setWeekend_special_diff(c.getString("weekend_special_diff"));
                        item1.setLocation_place_id(c.getString("location_place_id"));
                        item1.setPlace_id(c.getString("place_id"));
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setTwp_id(c.getString("twp_id"));
                        item1.setTownship_id(c.getString("township_id"));
                        item1.setTownship_code(c.getString("township_code"));
                        item1.setManager_id(c.getString("manager_id"));
                        item1.setIsKiosk(c.getString("isKiosk"));
                        item1.setIsslected(false);
                        if (EventLocationAddEditFragment.selectedLocations.contains(item1.getLocation_code())) {
                            item1.setIsslected(true);
                        }
                        township_array_list.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity != null) {
                    if (township_array_list.size() > 0) {
                        customAdapterTwonship = new CustomAdapterTwonship(getActivity(), township_array_list, getResources());
                        lv_Township.setAdapter(customAdapterTwonship);
                    } else {
                        //txt_No_Record.setVisibility(View.VISIBLE);
                        lv_Township.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

    public class getGoogle_list extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String parkingurl;

        {
            try {
                parkingurl = "_table/google_parking_partners?filter=(township_code%3D'" + URLEncoder.encode(select_town, "utf-8") + "')";
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            township_array_list = new ArrayList<item>();
            township_array_list.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    Log.e("responseStr ", " : " + responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setId(c.getString("id"));
                        item1.setLocation_id(c.getString("location_id"));
                        item1.setPlace_id(c.getString("place_id"));
                        item1.setTitle(c.getString("title"));
                        item1.setAddress(c.getString("address"));
                        item1.setUser_id(c.getString("user_id"));
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setParking_times(c.getString("parking_times"));
                        item1.setNo_parking_times(c.getString("no_parking_times"));
                        item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setIn_effect(c.getString("in_effect"));
                        item1.setMarker_type(c.getString("marker_type"));
                        item1.setActive(c.getString("active"));
                        item1.setRenewable(c.getString("renewable"));
                        item1.setWeekend_special_diff(c.getString("weekend_special_diff"));
                        item1.setOff_peak_ends(c.getString("off_peak_ends"));
                        item1.setOff_peak_starts(c.getString("off_peak_starts"));
                        item1.setLots_avbl(c.getString("lots_avbl"));
                        item1.setLots_total(c.getString("lots_total"));
                        item1.setLat(c.getString("lat"));
                        item1.setLng(c.getString("lng"));
                        item1.setLocation_place_id(c.getString("location_place_id"));
                        item1.setTwp_id(c.getString("twp_id"));
                        item1.setTownship_code(c.getString("township_code"));
                        item1.setManager_id(c.getString("manager_id"));
                        item1.setIsKiosk(c.getString("isKiosk"));
                        item1.setIsslected(false);
                        if (EventLocationAddEditFragment.selectedLocations.contains(item1.getLocation_code())) {
                            item1.setIsslected(true);
                        }
                        township_array_list.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (township_array_list.size() > 0) {
                    customAdapter = new CustomAdapter(getActivity(), township_array_list, getResources());
                    lv_Township.setAdapter(customAdapter);
                } else {
                    //txt_No_Record.setVisibility(View.VISIBLE);
                    lv_Township.setVisibility(View.GONE);
                }
            }
            super.onPostExecute(s);
        }
    }

    public class getOther_list extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String parkingurl;

        {
            try {
                parkingurl = "_table/other_parking_partners?filter=(township_code%3D'" + URLEncoder.encode(select_town, "utf-8") + "')";
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            township_array_list = new ArrayList<item>();
            township_array_list.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    Log.e("responseStr ", " : " + responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setId(c.getString("id"));
                        item1.setTitle(c.getString("title"));
                        item1.setAddress(c.getString("address"));
                        item1.setLat(c.getString("lat"));
                        item1.setLng(c.getString("lng"));
                        item1.setUser_id(c.getString("user_id"));
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setActive(c.getString("active"));
                        item1.setIn_effect(c.getString("in_effect"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setMarker_type(c.getString("marker_type"));
                        item1.setNo_parking_times(c.getString("no_parking_times"));
                        item1.setParking_times(c.getString("parking_times"));
                        item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        item1.setRenewable(c.getString("renewable"));
                        item1.setWeekend_special_diff(c.getString("weekend_special_diff"));
                        item1.setOff_peak_ends(c.getString("off_peak_ends"));
                        item1.setOff_peak_starts(c.getString("off_peak_starts"));
                        item1.setLots_avbl(c.getString("lots_avbl"));
                        item1.setLots_total(c.getString("lots_total"));
                        item1.setLocation_place_id(c.getString("location_place_id"));
                        item1.setIsKiosk(c.getString("isKiosk"));
                        item1.setIsslected(false);
                        if (EventLocationAddEditFragment.selectedLocations.contains(item1.getLocation_code())) {
                            item1.setIsslected(true);
                        }
                        township_array_list.add(item1);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (township_array_list.size() > 0) {
                    customAdapterPrivate = new CustomAdapterPrivate(getActivity(), township_array_list, getResources());
                    lv_Township.setAdapter(customAdapterPrivate);
                } else {
                    //txt_No_Record.setVisibility(View.VISIBLE);
                    lv_Township.setVisibility(View.GONE);
                }
            }
            super.onPostExecute(s);
        }
    }

    public class CustomAdapterTwonship extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdapterTwonship(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txt_Location_id, txt_location_code, txt_location_name, txt_township_name,
                    txt_APPLY, txt_townshipCode;
            ImageView img_logo, img_selected;
            RelativeLayout rl_main;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_permit_definitions, null);
                holder = new ViewHolder();

                holder.txt_Location_id = vi.findViewById(R.id.txt_Location_id);
                holder.txt_location_code = vi.findViewById(R.id.txt_location_code);
                holder.txt_location_name = vi.findViewById(R.id.txt_location_name);
                holder.txt_township_name = vi.findViewById(R.id.txt_township_name);
                holder.txt_townshipCode = vi.findViewById(R.id.txt_townshipCode);
                holder.txt_APPLY = vi.findViewById(R.id.txt_APPLY);
                holder.img_logo = (ImageView) vi.findViewById(R.id.img_logo);
                holder.img_selected = (ImageView) vi.findViewById(R.id.img_selected);
                holder.rl_main = vi.findViewById(R.id.rl_main);

                vi.setTag(holder);
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Picasso.with(context).load(R.mipmap.township_icon).into(holder.img_logo);
            holder.txt_location_name.setSelected(true);

            holder.txt_Location_id.setText(data.get(position).getTownship_code() + ":"
                    + data.get(position).getLocation_code());
            holder.txt_location_name.setText(data.get(position).getTitle());
            holder.txt_township_name.setText(data.get(position).getAddress());

            if (data.get(position).getIsslected()) {
                holder.img_selected.setVisibility(View.VISIBLE);
            } else {
                holder.img_selected.setVisibility(View.INVISIBLE);
            }

            holder.rl_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.txt_APPLY.performClick();
                }
            });

            holder.txt_APPLY.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (data.get(position).getIsslected()) {
                        holder.img_selected.setVisibility(View.INVISIBLE);
                        township_array_list.get(position).setIsslected(false);
                    } else {
                        township_array_list.get(position).setIsslected(true);
                        holder.img_selected.setVisibility(View.VISIBLE);
                    }
                    customAdapterTwonship.notifyDataSetChanged();
                }
            });

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

    public class CustomAdapter extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdapter(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txt_Location_id, txt_location_code, txt_location_name, txt_township_name,
                    txt_APPLY, txt_townshipCode;
            ImageView img_logo, img_selected;
            RelativeLayout rl_main;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_permit_definitions, null);
                holder = new ViewHolder();

                holder.txt_Location_id = vi.findViewById(R.id.txt_Location_id);
                holder.txt_location_code = vi.findViewById(R.id.txt_location_code);
                holder.txt_location_name = vi.findViewById(R.id.txt_location_name);
                holder.txt_township_name = vi.findViewById(R.id.txt_township_name);
                holder.txt_townshipCode = vi.findViewById(R.id.txt_townshipCode);
                holder.txt_APPLY = vi.findViewById(R.id.txt_APPLY);
                holder.img_logo = (ImageView) vi.findViewById(R.id.img_logo);
                holder.img_selected = (ImageView) vi.findViewById(R.id.img_selected);
                holder.rl_main = vi.findViewById(R.id.rl_main);

                vi.setTag(holder);
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Picasso.with(context).load(R.mipmap.commercial_icon).into(holder.img_logo);

            holder.txt_location_name.setSelected(true);

            holder.txt_Location_id.setText(data.get(position).getTitle());
            //holder.txt_location_code.setText(data.get(position).getLocation_code());
            holder.txt_location_name.setText(data.get(position).getAddress());
            holder.txt_township_name.setText(data.get(position).getTownship_name());
            //holder.txt_townshipCode.setText(data.get(position).getTownshipcode());

            if (data.get(position).getIsslected()) {
                holder.img_selected.setVisibility(View.VISIBLE);
            } else {
                holder.img_selected.setVisibility(View.INVISIBLE);
            }

            holder.rl_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.txt_APPLY.performClick();
                }
            });

            holder.txt_APPLY.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (data.get(position).getIsslected()) {
                        holder.img_selected.setVisibility(View.INVISIBLE);
                        township_array_list.get(position).setIsslected(false);
                    } else {
                        township_array_list.get(position).setIsslected(true);
                        holder.img_selected.setVisibility(View.VISIBLE);
                    }
                    customAdapter.notifyDataSetChanged();
                }
            });

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

    public class CustomAdapterPrivate extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdapterPrivate(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txt_Location_id, txt_location_code, txt_location_name, txt_township_name,
                    txt_APPLY, txt_townshipCode;
            ImageView img_logo, img_selected;
            RelativeLayout rl_main;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_permit_definitions, null);
                holder = new ViewHolder();

                holder.txt_Location_id = vi.findViewById(R.id.txt_Location_id);
                holder.txt_location_code = vi.findViewById(R.id.txt_location_code);
                holder.txt_location_name = vi.findViewById(R.id.txt_location_name);
                holder.txt_township_name = vi.findViewById(R.id.txt_township_name);
                holder.txt_townshipCode = vi.findViewById(R.id.txt_townshipCode);
                holder.txt_APPLY = vi.findViewById(R.id.txt_APPLY);
                holder.img_logo = (ImageView) vi.findViewById(R.id.img_logo);
                holder.img_selected = (ImageView) vi.findViewById(R.id.img_selected);
                holder.rl_main = vi.findViewById(R.id.rl_main);

                vi.setTag(holder);
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Picasso.with(context).load(R.mipmap.local_icon).into(holder.img_logo);

            holder.txt_location_name.setSelected(true);

            holder.txt_Location_id.setText(data.get(position).getTitle());
            //holder.txt_location_code.setText(data.get(position).getLocation_code());
            holder.txt_location_name.setText(data.get(position).getAddress());
            holder.txt_township_name.setText(data.get(position).getTownship_name());
            //holder.txt_townshipCode.setText(data.get(position).getTownshipcode());

            if (data.get(position).getIsslected()) {
                holder.img_selected.setVisibility(View.VISIBLE);
            } else {
                holder.img_selected.setVisibility(View.INVISIBLE);
            }

            holder.rl_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.txt_APPLY.performClick();
                }
            });

            holder.txt_APPLY.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (data.get(position).getIsslected()) {
                        holder.img_selected.setVisibility(View.INVISIBLE);
                        township_array_list.get(position).setIsslected(false);
                    } else {
                        township_array_list.get(position).setIsslected(true);
                        holder.img_selected.setVisibility(View.VISIBLE);
                    }
                    customAdapterPrivate.notifyDataSetChanged();
                }
            });

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }
}
