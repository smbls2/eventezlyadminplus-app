package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class Select_vehicle extends Fragment {
    ArrayList<item> vehiclearry = new ArrayList<>();
    ListView list_vehicle;
    RelativeLayout rl_progreesbar;
    Map<String, Object> statefullname;
    String statearray[] = {"State", "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"};
    String key;
    String plate_no, p_Country;
    String parking_for, parkhere;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.select_vehicle, container, false);

        SharedPreferences back_select = getActivity().getSharedPreferences("back_parking_info", Context.MODE_PRIVATE);
        SharedPreferences.Editor edd = back_select.edit();
        edd.putString("back", "yes");
        edd.commit();

        list_vehicle = (ListView) view.findViewById(R.id.listofvehicle);
        SharedPreferences s = getActivity().getSharedPreferences("paidparkhere", Context.MODE_PRIVATE);
        rl_progreesbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        statefullname = new HashMap<String, Object>();
        statefullname.put("AL", "Alabama");
        statefullname.put("AK", "Alaska");
        statefullname.put("AZ", "Arizona");
        statefullname.put("AR", "Arkansas");
        statefullname.put("CA", "California");
        statefullname.put("CO", "Colorado");
        statefullname.put("CT", "Connecticut");
        statefullname.put("DE", "Delaware");
        statefullname.put("DC", "District of Columbia");
        statefullname.put("FL", "Florida");
        statefullname.put("GA", "Georgia");
        statefullname.put("HI", "Hawaii");
        statefullname.put("ID", "Idaho");
        statefullname.put("IL", "Illinois");
        statefullname.put("IN", "Indiana");
        statefullname.put("IA", "Iowa");
        statefullname.put("KS", "Kansas");
        statefullname.put("KY", "Kentucky");
        statefullname.put("LA", "Louisiana");
        statefullname.put("ME", "Maine");
        statefullname.put("MD", "Maryland");
        statefullname.put("MA", "Massachusetts");
        statefullname.put("MI", "Michigan");
        statefullname.put("MN", "Minnesota");
        statefullname.put("MS", "Mississippi");
        statefullname.put("MO", "Missouri");
        statefullname.put("MT", "Montana");
        statefullname.put("NE", "Nebraska");
        statefullname.put("NV", "Nevada");
        statefullname.put("NH", "New Hampshire");
        statefullname.put("NJ", "New Jersey");
        statefullname.put("NM", "New Mexico");
        statefullname.put("NY", "New York");
        statefullname.put("NC", "North Carolina");
        statefullname.put("ND", "North Dakota");
        statefullname.put("OH", "Ohio");
        statefullname.put("OK", "Oklahoma");
        statefullname.put("OR", "Oregon");
        statefullname.put("PA", "Pennsylvani");
        statefullname.put("RI", "Rhode Island");
        statefullname.put("SC", "South Carolina");
        statefullname.put("SD", "South Dakota");
        statefullname.put("TN", "Tennessee");
        statefullname.put("TX", "Texas");
        statefullname.put("UT", "Utah");
        statefullname.put("VT", "Vermont");
        statefullname.put("VA", "Virginia");
        statefullname.put("WA", "Washington");
        statefullname.put("WV", "West virginia");
        statefullname.put("WI", "Wisconsin");
        statefullname.put("WY", "Wyoming");

        parking_for = s.getString("parking_for", "null");

        RelativeLayout rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        if (!user_id.equals("null")) {
            parkhere = s.getString("parkhere", "no");
            if (!parkhere.equals("yes")) {
                new getvehiclenumber().execute();
            } else {
                SharedPreferences dd = getActivity().getSharedPreferences("select_vehicle", Context.MODE_PRIVATE);
                SharedPreferences.Editor ddd = dd.edit();
                ddd.putString("plate_no", plate_no);
                ddd.putString("state", key);
                ddd.putString("p_country", p_Country);
                ddd.commit();
                if (parking_for.equals("paid")) {
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                    ft.replace(R.id.My_Container_1_ID, new paidparkhere(), "NewFragmentTag");
                    ft.commit();
                } else if (parking_for.equals("free")) {
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                    ft.replace(R.id.My_Container_1_ID, new freeparkhere(), "NewFragmentTag");
                    ft.commit();
                }
            }
        } else {
           /* SharedPreferences sdg = getActivity().getSharedPreferences("back_parking_info", Context.MODE_PRIVATE);
            sdg.edit().clear().commit();*/
            SharedPreferences dd = getActivity().getSharedPreferences("select_vehicle", Context.MODE_PRIVATE);
            SharedPreferences.Editor ddd = dd.edit();
            ddd.putString("plate_no", plate_no);
            ddd.putString("state", key);
            ddd.putString("p_country", p_Country);
            ddd.commit();
            if (parking_for.equals("paid")) {
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.replace(R.id.My_Container_1_ID, new paidparkhere(), "NewFragmentTag");
                ft.commit();
            } else if (parking_for.equals("free")) {
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.replace(R.id.My_Container_1_ID, new freeparkhere(), "NewFragmentTag");
                ft.commit();
            }
        }


        list_vehicle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /*SharedPreferences sdg = getActivity().getSharedPreferences("back_parking_info", Context.MODE_PRIVATE);
                sdg.edit().clear().commit();*/
                if (vehiclearry.size() > 0) {
                    SharedPreferences s = getActivity().getSharedPreferences("paidparkhere", Context.MODE_PRIVATE);
                    String parkhere = s.getString("parkhere", "no");
                    if (parkhere.equals("yes")) {
                        SharedPreferences sdg = getActivity().getSharedPreferences("back_parking_info", Context.MODE_PRIVATE);
                        sdg.edit().clear().commit();
                    }
                    String statename = vehiclearry.get(position).getState();
                    plate_no = vehiclearry.get(position).getPlantno().toUpperCase();
                    p_Country = vehiclearry.get(position).getVehicle_country();
                    Iterator myVeryOwnIterator = statefullname.keySet().iterator();
                    while (myVeryOwnIterator.hasNext()) {
                        key = (String) myVeryOwnIterator.next();
                        String value = (String) statefullname.get(key);
                        if (value.equals(statename)) {
                            break;
                        }
                    }

                    SharedPreferences dd = getActivity().getSharedPreferences("select_vehicle", Context.MODE_PRIVATE);
                    SharedPreferences.Editor ddd = dd.edit();
                    ddd.putString("plate_no", plate_no);
                    ddd.putString("state", key);
                    ddd.putString("p_country", p_Country);
                    ddd.commit();
                    if (parking_for.equals("paid")) {

                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.replace(R.id.My_Container_1_ID, new paidparkhere(), "NewFragmentTag");
                        ft.commit();
                    } else if (parking_for.equals("free")) {
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.replace(R.id.My_Container_1_ID, new freeparkhere(), "NewFragmentTag");
                        ft.commit();
                    }
                }
            }
        });

        return view;
    }

    public class getvehiclenumber extends AsyncTask<String, String, String> {
        JSONObject json = null;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "0");
        String vehiclenourl = "_table/user_vehicles?filter=user_id%3D" + user_id + "";

        @Override
        protected void onPreExecute() {
            rl_progreesbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            vehiclearry.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("get_vehicle_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);
                    Log.e("rerpones", String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        String plate_no = c.getString("plate_no");
                        String state = c.getString("registered_state");
                        String id = c.getString("id");
                        /*Iterator myVeryOwnIterator = statefullname.keySet().iterator();
                        while (myVeryOwnIterator.hasNext()) {
                            String key = (String) myVeryOwnIterator.next();
                            if (key.equals(state)) {
                                String value = (String) statefullname.get(key);
                                item.setState(value);
                                break;
                            }
                        }*/
                        item.setState(state);
                        item.setPlateno(plate_no);
                        item.setId(id);
                        item.setVehicle_country(c.getString("vehicle_country"));
                        item.setIsselect(false);
                        item.setCheckboxselected(false);
                        vehiclearry.add(item);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progreesbar != null)
                rl_progreesbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity == null) {
                } else {
                    CustomAdaptercity adpater = new CustomAdaptercity(getActivity(), vehiclearry, rs = getResources());
                    list_vehicle.setAdapter(adpater);
                }
            }
            super.onPostExecute(s);
        }
    }

    public class CustomAdaptercity extends BaseAdapter {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdaptercity(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.selectvehicle, null);
                holder = new ViewHolder();
                holder.txtplatno = (TextView) vi.findViewById(R.id.txtlicenseplate);
                holder.state = (TextView) vi.findViewById(R.id.txtstate);
                holder.txtid = (TextView) vi.findViewById(R.id.txtid);
                vi.setTag(holder);
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
                holder.txtplatno.setText("No Data");
            } else {
                tempValues = null;
                tempValues = (item) vehiclearry.get(position);
                int k = 0;
                String cat = tempValues.getPlantno();
                if (cat == null) {
                    holder.state.setVisibility(View.GONE);
                } else if (cat != null) {
                    Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvetica-neue-medium.ttf");
                    holder.txtplatno.setTypeface(type);
                    holder.state.setTypeface(type);
                    holder.txtplatno.setText(tempValues.getPlantno().toUpperCase());
                    holder.state.setText(tempValues.getState());
                    holder.txtid.setText(tempValues.getId());
                }
            }
            return vi;
        }

        public class ViewHolder {
            public TextView txtplatno, state, txtid;
            public RelativeLayout rl;
            CheckBox checkBox;

        }
    }
}