package com.softmeasures.eventezlyadminplus.frament;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.softmeasures.eventezlyadminplus.Modules.DirectionFinder;
import com.softmeasures.eventezlyadminplus.Modules.DirectionFinderListener;
import com.softmeasures.eventezlyadminplus.Modules.Route;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class show_transit extends Fragment implements OnMapReadyCallback, DirectionFinderListener {
    double latitude = 0.0, longitude = 0.0;
    ConnectionDetector cd;
    GoogleMap googleMap;
    MarkerOptions options;
    RelativeLayout rl_progressbar;
    ProgressBar progressBar;
    ArrayList<item> step = new ArrayList<>();
    String cu, en;
    TextView txt_origin_address, txt_destion_address, txt_total_dis, txt_total_dur;
    double totaldistance = 0.0, totalduration = 0.0, avgsppedinmilesperhor = 0.0;
    ImageView img_refresh, img_zoom;
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    private ProgressDialog progressDialog;
    RelativeLayout rl_listview, rl_map;
    ListView list_step;
    String click_on_move = "";
    List<LatLng> lat_lang = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_show_transit, container, false);
        SharedPreferences ss = getActivity().getSharedPreferences("show_direction", Context.MODE_PRIVATE);
        cu = ss.getString("current", "null");
        en = ss.getString("des", "null");
        final String curnnt = ss.getString("current_lat", "0.0");
        final String endloc = ss.getString("des_lat", "0.0");
        final String mode = ss.getString("mode", "driving");
        SharedPreferences sd = getActivity().getSharedPreferences("transitback", Context.MODE_PRIVATE);
        SharedPreferences.Editor d = sd.edit();
        d.putString("transitback", "yes");
        d.commit();
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        txt_origin_address = (TextView) view.findViewById(R.id.txt_origin_address);
        txt_destion_address = (TextView) view.findViewById(R.id.txt_destination_address);

        rl_listview = (RelativeLayout) view.findViewById(R.id.rl_listview);
        rl_map = (RelativeLayout) view.findViewById(R.id.rl_map);
        list_step = (ListView) view.findViewById(R.id.list_location);
        txt_total_dis = (TextView) view.findViewById(R.id.txt_total_distances);
        txt_total_dur = (TextView) view.findViewById(R.id.txt_total_duration1);

        cd = new ConnectionDetector(getActivity());
        img_refresh = (ImageView) view.findViewById(R.id.img_refresh);
        img_zoom = (ImageView) view.findViewById(R.id.img_zoom);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            click_on_move = bundle.getString("click");
        }

        RelativeLayout rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        if (cd.isConnectingToInternet()) {
            currentlocation();
            String lang = curnnt.substring(0, curnnt.indexOf(","));
            String lat = curnnt.substring(curnnt.indexOf(",") + 1);
            mapFragment = ((SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map));
            LatLng position = new LatLng(Double.parseDouble(lat), Double.parseDouble(lang));
            options = new MarkerOptions();
            options.position(position);
            SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            fm.getMapAsync(googleMap1 -> {
                googleMap = googleMap1;
                googleMap.addMarker(options);
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                }
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.setMyLocationEnabled(false);
                googleMap.getUiSettings().setZoomControlsEnabled(false);
                googleMap.setTrafficEnabled(true);
                map = googleMap;
            });
        }
        if (!cu.equals("null") && !en.equals("null")) {
            sendRequest(cu, en, mode);
        }
        img_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleMap.clear();
                if (!cu.equals("null") && !en.equals("null")) {
                    sendRequest(cu, en, mode);
                }
            }
        });
        img_zoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                googleMap.animateCamera(CameraUpdateFactory.zoomIn());

            }
        });


        if (click_on_move.equals("delay")) {
            rl_listview.setVisibility(View.VISIBLE);
            rl_map.setVisibility(View.VISIBLE);
        } else if (click_on_move.equals("hybrid view")) {
            rl_map.setVisibility(View.VISIBLE);
            rl_listview.setVisibility(View.GONE);
            googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        } else if (click_on_move.equals("Close up")) {
            rl_map.setVisibility(View.VISIBLE);
            rl_listview.setVisibility(View.GONE);
        } else if (click_on_move.equals("atob")) {
            rl_map.setVisibility(View.VISIBLE);
            rl_listview.setVisibility(View.GONE);
        }
        return view;
    }

    public void currentlocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Failed to fetch user location!");
            alertDialog.setMessage("Please enable user location for app, location is required for app to work properly");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            alertDialog.show();
        }
    }

    public void sendRequest(String cu, String end, String mode) {

        try {
            new DirectionFinder(this, cu, end, mode).execute();
            onMapReady(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map.clear();
        map = googleMap;
        LatLng hcmus = new LatLng(latitude, longitude);

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hcmus, 12));
        map.setTrafficEnabled(true);

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        map.setMyLocationEnabled(false);

        originMarkers.add(map.addMarker(new MarkerOptions()
                .title("Direction")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.pin))
                .position(hcmus)));

        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition position) {
                float maxZoom = 17.0f;
                float minZoom = 11.0f;

                String pp = String.valueOf(position.zoom);
                Log.e("pos", pp);

                if (position.zoom < minZoom) {
                    Log.e("pos3", pp);
                    map.animateCamera(CameraUpdateFactory.zoomTo(minZoom));
                }
            }
        });

        if (click_on_move.equals("hybrid view")) {
            map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        }


    }

    @Override
    public void onDirectionFinderStart() {
        rl_progressbar.setVisibility(View.VISIBLE);

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }
        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }
        if (polylinePaths != null) {
            for (Polyline polyline : polylinePaths) {
                polyline.remove();
            }
        }
    }

    @Override
    public void onDirectionFondererror() {
        rl_progressbar.setVisibility(View.GONE);
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        rl_progressbar.setVisibility(View.GONE);
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();
        step.clear();
        ActionStartsHere();

        for (Route route : routes) {
            if (route.status.equals("ZERO_RESULTS")) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("No routes available");
                alertDialog.setMessage("No routes available for selected mode");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
                break;
            } else if (route.status.equals("INVALID_REQUEST")) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Could not find destination");
                alertDialog.setMessage("Try including the City and State, or change mode of search");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            } else {
                try {


                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 12));
                    originMarkers.add(map.addMarker(new MarkerOptions()
                            .title(route.startAddress)
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.origin))
                            .position(route.startLocation)));
                    destinationMarkers.add(map.addMarker(new MarkerOptions()
                            .title(route.endAddress)
                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.destination))
                            .position(route.endLocation)));
                    String startaddress = route.startAddress;
                    String endaddress = route.endAddress;
                    String dis = route.distance.text;
                    String dur = route.duration.text;
                    txt_destion_address.setText("Destination Address: " + endaddress);
                    txt_origin_address.setText("Origin Address: " + startaddress);
                    if (step.size() <= 0) {
                        for (int j = 0; j < route.step.size(); j++) {
                            item ii = new item();
                            ii.setDistancea(route.step.get(j).getDistancea());
                            ii.setStep(route.step.get(j).getStep());
                            ii.setDuration(route.step.get(j).getDuration());
                            String distances = route.step.get(j).getDistancea();
                            String disvalue = distances.substring(0, distances.indexOf(' '));
                            String disname = distances.substring(distances.indexOf(' ') + 1);
                            double distacesinft = Double.parseDouble(disvalue);
                            if (disname.equals("ft")) {
                                distacesinft = distacesinft * 0.000189394;
                            }
                            totaldistance = totaldistance + distacesinft;
                            String durations = route.step.get(j).getDuration();
                            String durvalue = durations.substring(0, durations.indexOf(' '));
                            double durv = Double.parseDouble(durvalue);
                            totalduration = totalduration + (durv / 60);
                            step.add(ii);
                        }
                        avgsppedinmilesperhor = totaldistance / totalduration;
                        txt_total_dur.setText("Total Duration: " + dur);
                        txt_total_dis.setText("Total Distance: " + dis);
                        if (avgsppedinmilesperhor < 15.0) {
                            PolylineOptions polylineOptions = new PolylineOptions().
                                    geodesic(true).
                                    color(Color.RED).
                                    width(5);
                            for (int i = 0; i < route.points.size(); i++) {
                                polylineOptions.add(route.points.get(i));
                            }
                        } else if (avgsppedinmilesperhor >= 15.0 && avgsppedinmilesperhor < 50.0) {
                            PolylineOptions polylineOptions = new PolylineOptions().
                                    geodesic(true).
                                    color(Color.parseColor("#912E03")).
                                    width(5);
                            for (int i = 0; i < route.points.size(); i++) {
                                polylineOptions.add(route.points.get(i));
                            }
                        } else {
                            PolylineOptions polylineOptions = new PolylineOptions().
                                    geodesic(true).
                                    color(Color.GREEN).
                                    width(5);
                            for (int i = 0; i < route.points.size(); i++) {
                                polylineOptions.add(route.points.get(i));
                            }
                            polylinePaths.add(map.addPolyline(polylineOptions));

                        }
                    }
                } catch (Exception e) {
                }

            }
        }
        Resources rs;
        CustomAdaptercity adpater = new CustomAdaptercity(getActivity(), step, rs = getResources());
        list_step.setAdapter(adpater);
    }

    public void ActionStartsHere() {
        againStartGPSAndSendFile();
    }

    public void againStartGPSAndSendFile() {
        new CountDownTimer(300000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);
                long minutes = (millisUntilFinished / 1000) / 60;
                int seconds1 = (int) ((millisUntilFinished / 1000) % 60);
                Log.e("minutes", String.valueOf(minutes));
                Log.e("second1", String.valueOf(seconds1));
            }

            @Override
            public void onFinish() {
                Activity activty = getActivity();
                if (activty != null) {
                    googleMap.clear();
                    SharedPreferences ss = getActivity().getSharedPreferences("show_direction", Context.MODE_PRIVATE);
                    final String mode = ss.getString("mode", "driving");
                    if (!cu.equals("null") && !en.equals("null")) {
                        sendRequest(cu, en, mode);
                    }
                    ActionStartsHere();
                }
            }

        }.start();
    }

    public class CustomAdaptercity extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdaptercity(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.adapetstep, null);
                holder = new ViewHolder();
                holder.txt_step_dis = (TextView) vi.findViewById(R.id.txt_step_distance);
                holder.txt_step_dur = (TextView) vi.findViewById(R.id.txt_step_duration);
                holder.txt_step_title = (TextView) vi.findViewById(R.id.txt_step_title);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String myString = tempValues.getStep();
                String trimmedString = myString.replace("<div>", "");
                holder.txt_step_title.setText(Html.fromHtml(trimmedString));
                holder.txt_step_dur.setText("Duration: " + tempValues.getDuration());
                holder.txt_step_dis.setText("Distance: " + tempValues.getDistancea());

            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txt_step_title, txt_step_dis, txt_step_dur;
        }
    }
}
