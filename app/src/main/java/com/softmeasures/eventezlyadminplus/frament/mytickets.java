package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class mytickets extends Fragment {
    ProgressDialog pdialog;
    ArrayList<item> permitsarray = new ArrayList<>();
    CustomAdaptercity adpater;
    ListView listofvehicleno;
    TextView txttitle;
    int i = 1;
    ConnectionDetector cd;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar;
    String id;
    String viewticket = "null", view_plate_no = "null", view_state_name = "null";
    RelativeLayout rl_main;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mytickets, container, false);
        listofvehicleno = (ListView) view.findViewById(R.id.listofvehicle);
        txttitle = (TextView) view.findViewById(R.id.txttitte);
        rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        id = logindeatl.getString("id", "null");
        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Thin.ttf");
        txttitle.setTypeface(type);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        SharedPreferences pla = getActivity().getSharedPreferences("view_ticket", Context.MODE_PRIVATE);
        viewticket = pla.getString("view_ticket", "no");
        view_plate_no = pla.getString("noplate", "null");
        view_state_name = pla.getString("state_name", "null");

        cd = new ConnectionDetector(getActivity());
        if (cd.isConnectingToInternet()) {
            if (viewticket.equals("yes")) {
                pla.edit().clear().commit();
                try {
                    new gettransaction().execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                new getmanagedlist().execute();
            }

            listofvehicleno.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    int p = position;
                    int j = position;
                    SharedPreferences pr = getActivity().getSharedPreferences("postion", Context.MODE_APPEND);
                    SharedPreferences.Editor ed = pr.edit();
                    ed.putInt("postion", position);
                    ed.commit();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                    ft.replace(R.id.My_Container_1_ID, new ticketdeatil(), "NewFragmentTag");
                    ft.commit();
                }
            });
            String role = logindeatl.getString("role", "");
            if (role.equals("SuperAdmin") || role.equals("TwpAdmin") || role.equals("TwpBursar") || role.equals("TwpInspector")) {
                txttitle.setText("Issued Tickets");
            } else {
                txttitle.setText("My Tickets");
            }
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }
        return view;
    }

    public class getmanagedlist extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);

        String plate_no, violation_detail, hearing_date, hearing_location, email;
        String officer_name, officer_id, violation_description, hearingtime, hearing_address;
        String paid_date, address;
        String managedlosturl = "_proc/user_parking_violations";
        String id = logindeatl.getString("id", "null");

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String role = logindeatl.getString("role", "");
            String twcode = logindeatl.getString("twcode", "FDV");
            if (role.equals("SuperAdmin") || role.equals("TwpAdmin") || role.equals("TwpBursar")) {
                managedlosturl = "_proc/township_parking_violations";
            } else if (role.equals("TwpInspector")) {
                managedlosturl = "_proc/inspector_parking_violations";
            } else {
                managedlosturl = "_proc/user_parking_violations";
            }


            JSONObject inlat = new JSONObject();
            try {
                inlat.put("name", "in_user_id");
                inlat.put("value", id);
                // inlat.put("value", "40.7333");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            JSONArray jsonArray = new JSONArray();

            jsonArray.put(inlat);

            JSONObject studentsObj = new JSONObject();
            try {
                if (role.equals("TwpInspector")) {
                    studentsObj.put("params", id);
                } else if (role.equals("SuperAdmin") || role.equals("TwpAdmin") || role.equals("TwpBursar")) {
                    studentsObj.put("params", twcode);
                } else {
                    studentsObj.put("params", id);
                }
                Log.e("managed_params", String.valueOf(studentsObj));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("find_parking_nearby", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(studentsObj.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json1 = new JSONArray(responseStr);
                    for (int i = 0; i < json1.length(); i++) {
                        JSONObject c = json1.getJSONObject(i);
                        item item = new item();
                        plate_no = c.getString("plate_no");

                        if (viewticket.equals("yes")) {

                            if (plate_no.toLowerCase().equals(view_plate_no.toLowerCase())) {
                                String fee = c.getString("violation_fee");
                                String respdate = c.getString("respond_date");
                                String date = c.getString("date_ticket");

                                violation_detail = c.getString("violation_detail");
                                hearing_date = c.getString("hearing_date");
                                hearing_location = c.getString("hearing_location");
                                address = c.getString("address");
                                email = c.getString("email");
                                officer_name = c.getString("officer_name");
                                officer_id = c.getString("officer_id");
                                violation_description = c.getString("violation_description");
                                hearingtime = c.getString("hearing_time");
                                hearing_address = c.getString("hearing_address");
                                paid_date = c.getString("paid_date");
                                String id = c.getString("id");
                                String ticket_status = c.getString("tkt_status");
                                String townshipcode = c.getString("township_code");

                                item.setFees(fee);
                                item.setRespdate(respdate);
                                item.setExdate(date);
                                item.setPlate_no(plate_no);
                                item.setViolation_detail(violation_detail);
                                item.setHearing_date(hearing_date);
                                item.setHearing_location(hearing_location);
                                item.setHearingtime(hearingtime);
                                item.setEmail(email);
                                item.setOfficer_id(officer_id);
                                item.setOfficer_name(officer_name);
                                item.setAddress(address);
                                item.setPaid_date(paid_date);
                                item.setViolationdescription(violation_description);
                                item.setId(id);
                                item.setTicket_satus(ticket_status);
                                item.setTownshipcode(townshipcode);
                                item.setTr_free(c.getString("tr_fee"));
                                item.setTr_percentage(c.getString("tr_percentage"));
                                item.setPlead_Not_Guilty(c.getString("plead_not_guilty"));
                                item.setAddress(c.getString("address"));
                                item.setAddesss1(c.getString("parked_location_address"));
                                item.setState(c.getString("pl_state"));
                                item.setAv_comments(c.getString("av_comments"));
                                item.setPhoto_comments(c.getString("photo_comments"));
                                item.setText_comments(c.getString("text_comments"));
                                item.setTicket_no(c.getString("ticket_no"));
                                item.setVioloation_code(c.getString("violation_code"));
                                if (c.has("v_user_id")) {
                                    item.setV_user_id(c.getString("v_user_id"));
                                } else {
                                    item.setV_user_id("");
                                }

                                if (c.has("v_platform")) {
                                    item.setV_platform(c.getString("v_platform"));
                                } else {
                                    item.setV_platform("");
                                }
                                permitsarray.add(item);
                            }
                        } else {
                            String fee = c.getString("violation_fee");
                            String respdate = c.getString("respond_date");
                            String date = c.getString("date_ticket");

                            violation_detail = c.getString("violation_detail");
                            hearing_date = c.getString("hearing_date");
                            hearing_location = c.getString("hearing_location");
                            address = c.getString("address");
                            email = c.getString("email");
                            officer_name = c.getString("officer_name");
                            officer_id = c.getString("officer_id");
                            violation_description = c.getString("violation_description");
                            hearingtime = c.getString("hearing_time");
                            hearing_address = c.getString("hearing_address");
                            paid_date = c.getString("paid_date");
                            String id = c.getString("id");
                            String ticket_status = c.getString("tkt_status");
                            String townshipcode = c.getString("township_code");

                            item.setFees(fee);
                            item.setRespdate(respdate);
                            item.setExdate(date);
                            item.setPlate_no(plate_no);
                            item.setViolation_detail(violation_detail);
                            item.setHearing_date(hearing_date);
                            item.setHearing_location(hearing_location);
                            item.setHearingtime(hearingtime);
                            item.setEmail(email);
                            item.setOfficer_id(officer_id);
                            item.setOfficer_name(officer_name);
                            item.setAddress(address);
                            item.setPaid_date(paid_date);
                            item.setViolationdescription(violation_description);
                            item.setId(id);
                            item.setTicket_satus(ticket_status);
                            item.setTownshipcode(townshipcode);
                            item.setTr_free(c.getString("tr_fee"));
                            item.setTr_percentage(c.getString("tr_percentage"));
                            item.setPlead_Not_Guilty(c.getString("plead_not_guilty"));
                            item.setAddress(c.getString("address"));
                            item.setAddesss1(c.getString("parked_location_address"));
                            item.setState(c.getString("pl_state"));
                            item.setAv_comments(c.getString("av_comments"));
                            item.setPhoto_comments(c.getString("photo_comments"));
                            item.setText_comments(c.getString("text_comments"));
                            item.setTicket_no(c.getString("ticket_no"));
                            item.setVioloation_code(c.getString("violation_code"));
                            if (c.has("v_user_id")) {
                                item.setV_user_id(c.getString("v_user_id"));
                            } else {
                                item.setV_user_id("");
                            }

                            if (c.has("v_platform")) {
                                item.setV_platform(c.getString("v_platform"));
                            } else {
                                item.setV_platform("");
                            }
                            permitsarray.add(item);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json1 != null) {
                if (getActivity() != null) {
                    Collections.reverse(permitsarray);
                    Collections.sort(permitsarray, new Comparator<item>() {


                        @Override
                        public int compare(item lhs, item rhs) {
                            return lhs.getExdate().compareToIgnoreCase(rhs.getExdate());//A
                        }


                    });
                    Collections.sort(permitsarray, new Comparator<item>() {
                        @Override
                        public int compare(item lhs, item rhs) {
                            return lhs.getTicket_satus().compareToIgnoreCase(rhs.getTicket_satus());//A
                        }
                    });
                    Collections.reverse(permitsarray);

                    Gson gson = new Gson();
                    SharedPreferences myPrefs = getActivity().getSharedPreferences("get", Context.MODE_PRIVATE);
                    SharedPreferences.Editor prefsEditor = myPrefs.edit();
                    String json = gson.toJson(permitsarray);
                    prefsEditor.putString("MyObject", json);
                    prefsEditor.commit();
                    adpater = new CustomAdaptercity(getActivity(), permitsarray, rs = getResources());
                    listofvehicleno.setAdapter(adpater);
                }
            }

            super.onPostExecute(s);
        }
    }


    public class gettransaction extends AsyncTask<String, String, String> {

        String plate_no, violation_detail, hearing_date, hearing_location, email;
        String officer_name, officer_id, violation_description, hearingtime, hearing_address;
        String paid_date, address;
        JSONObject json, json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");

        String transactionurl = "_table/parking_violations?filter=(plate_no%3D" + URLEncoder.encode(view_plate_no, "utf-8") + ")%20AND%20(pl_state%3D" + view_state_name + ")";

        public gettransaction() throws UnsupportedEncodingException {
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();


                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item = new item();
                        plate_no = c.getString("plate_no");
                        String fee = c.getString("violation_fee");
                        String respdate = c.getString("respond_date");
                        String date = c.getString("date_ticket");
                        violation_detail = c.getString("violation_detail");
                        hearing_date = c.getString("hearing_date");
                        hearing_location = c.getString("hearing_location");
                        address = c.getString("address");
                        email = c.getString("email");
                        officer_name = c.getString("officer_name");
                        officer_id = c.getString("officer_id");
                        violation_description = c.getString("violation_description");
                        hearingtime = c.getString("hearing_time");
                        hearing_address = c.getString("hearing_address");
                        paid_date = c.getString("paid_date");
                        String id = c.getString("id");
                        String ticket_status = c.getString("tkt_status");
                        String townshipcode = c.getString("township_code");

                        item.setFees(fee);
                        item.setRespdate(respdate);
                        item.setExdate(date);
                        item.setPlate_no(plate_no);
                        item.setViolation_detail(violation_detail);
                        item.setHearing_date(hearing_date);
                        item.setHearing_location(hearing_location);
                        item.setHearingtime(hearingtime);
                        item.setEmail(email);
                        item.setOfficer_id(officer_id);
                        item.setOfficer_name(officer_name);
                        item.setAddress(address);
                        item.setPaid_date(paid_date);
                        item.setViolationdescription(violation_description);
                        item.setId(id);
                        item.setTicket_satus(ticket_status);
                        item.setTownshipcode(townshipcode);
                        item.setTr_free("0");
                        item.setTr_percentage("0");
                        item.setPlead_Not_Guilty(c.getString("plead_not_guilty"));
                        item.setAddress(c.getString("address"));
                        item.setAddesss1(c.getString("parked_location_address"));
                        item.setState(c.getString("pl_state"));
                        item.setAv_comments(c.getString("av_comments"));
                        item.setPhoto_comments(c.getString("photo_comments"));
                        item.setText_comments(c.getString("text_comments"));
                        item.setTicket_no(c.getString("ticket_no"));
                        item.setVioloation_code(c.getString("violation_code"));
                        if (c.has("v_user_id")) {
                            item.setV_user_id(c.getString("v_user_id"));
                        } else {
                            item.setV_user_id("");
                        }

                        if (c.has("v_platform")) {
                            item.setV_platform(c.getString("v_platform"));
                        } else {
                            item.setV_platform("");
                        }
                        permitsarray.add(item);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json != null) {
                Collections.reverse(permitsarray);
                Collections.sort(permitsarray, new Comparator<item>() {


                    @Override
                    public int compare(item lhs, item rhs) {
                        return lhs.getExdate().compareToIgnoreCase(rhs.getExdate());//A
                    }


                });
                Collections.sort(permitsarray, new Comparator<item>() {
                    @Override
                    public int compare(item lhs, item rhs) {
                        return lhs.getTicket_satus().compareToIgnoreCase(rhs.getTicket_satus());//A
                    }
                });
                Collections.reverse(permitsarray);
                Gson gson = new Gson();
                SharedPreferences myPrefs = getActivity().getSharedPreferences("get", Context.MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = myPrefs.edit();
                String json = gson.toJson(permitsarray);
                prefsEditor.putString("MyObject", json);
                prefsEditor.commit();
                adpater = new CustomAdaptercity(getActivity(), permitsarray, rs = getResources());
                listofvehicleno.setAdapter(adpater);
            }
            super.onPostExecute(s);
        }
    }

    public class CustomAdaptercity extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdaptercity(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txtdate, txtrespdate, txtfees, txtresdatetitle, txtlabe, txt_service_free, txt_plate, txt_state, txt_v_code, txt_v_description, txt_id;
            public RelativeLayout rl_main;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {

                vi = inflater.inflate(R.layout.mytickets, null);
                holder = new ViewHolder();

                holder.txtdate = (TextView) vi.findViewById(R.id.txtticketdate);
                holder.txtrespdate = (TextView) vi.findViewById(R.id.txtmyticketresdate);
                holder.txtfees = (TextView) vi.findViewById(R.id.txtmyticketfees);
                holder.txtresdatetitle = (TextView) vi.findViewById(R.id.txtticketres1);
                holder.rl_main = (RelativeLayout) vi.findViewById(R.id.rl_main);
                holder.txtlabe = (TextView) vi.findViewById(R.id.txtlabe);
                holder.txt_service_free = (TextView) vi.findViewById(R.id.txt_services);

                holder.txt_plate = (TextView) vi.findViewById(R.id.txt_plate);
                holder.txt_state = (TextView) vi.findViewById(R.id.txt_state);
                holder.txt_v_code = (TextView) vi.findViewById(R.id.txt_violation_code);
                holder.txt_v_description = (TextView) vi.findViewById(R.id.txt_vd);
                holder.txt_id = (TextView) vi.findViewById(R.id.txtticketid);
                vi.setTag(holder);
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
                holder.txtdate.setText("No Data");
                holder.rl_main.setVisibility(View.GONE);

            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                String cat = tempValues.getExdate();
                if (cat == null) {
                    holder.txtdate.setVisibility(View.GONE);
                } else if (cat != null) {
                    Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvetica-normal.ttf");
                    Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvetica-neue-medium.ttf");
                    holder.txtdate.setTypeface(type);
                    holder.txtfees.setTypeface(type);
                    holder.txtrespdate.setTypeface(medium);
                    holder.txtresdatetitle.setTypeface(medium);
                    String inputPattern = "yyyy-MM-dd HH:mm:ss";
                    SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                    SimpleDateFormat outputFormat = new SimpleDateFormat("MM/dd/yyyy");
                    Date date = null;
                    String str = null;
                    String datef = tempValues.getExdate();
                    try {
                        if (datef.equals("0000-00-00 00:00:00")) {
                            holder.txtdate.setText("");
                        } else {
                            date = inputFormat.parse(tempValues.getExdate());
                            str = outputFormat.format(date);
                            holder.txtdate.setText(str);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    String inputPattern1 = "yyyy-MM-dd HH:mm:ss";
                    SimpleDateFormat inputFormat1 = new SimpleDateFormat(inputPattern1);
                    SimpleDateFormat outputFormat1 = new SimpleDateFormat("MM/dd/yyyy");
                    Date date1 = null;
                    Date date2 = null;
                    String str1 = null;
                    String datef1 = tempValues.getRespdate();
                    try {
                        if (datef1.equals("0000-00-00 00:00:00")) {
                            holder.txtrespdate.setText("");
                        } else {

                            date1 = inputFormat1.parse(tempValues.getRespdate());
                            String currentdate = inputFormat1.format(new Date());
                            date2 = inputFormat1.parse(currentdate);
                            str1 = outputFormat1.format(date1);
                            holder.txtrespdate.setText(str1);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    String status = tempValues.getTicket_satus();
                    if (status.equals("OPEN")) {

                        if (date1 != null || date2 != null) {
                            if (date2.after(date1)) {
                                holder.txtfees.setText(tempValues.getFees());
                                holder.txtlabe.setText("Over Due");
                                holder.rl_main.setBackgroundColor(Color.parseColor("#E6BFB6"));
                            } else if (tempValues.getPlead_Not_Guilty().equals("1")) {

                                holder.txtfees.setText(tempValues.getFees());
                                holder.txtlabe.setText("Court Due");
                                holder.rl_main.setBackgroundColor(Color.parseColor("#fdd835"));
                            } else {
                                holder.txtfees.setText(tempValues.getFees());
                                holder.txtlabe.setText("Payment Due");
                                holder.rl_main.setBackgroundColor(Color.parseColor("#E6BFB6"));
                            }
                        } else {
                            if (tempValues.getPlead_Not_Guilty().equals("1")) {

                                holder.txtfees.setText(tempValues.getFees());
                                holder.txtlabe.setText("Pleaded Not Guilty");
                                holder.rl_main.setBackgroundColor(Color.parseColor("#fdd835"));
                            } else {
                                holder.txtfees.setText(tempValues.getFees());
                                holder.txtlabe.setText("Payment Due");
                                holder.rl_main.setBackgroundColor(Color.parseColor("#E6BFB6"));
                            }
                        }

                    } else {
                        holder.txtlabe.setText("     Paid     ");
                        holder.txtfees.setText(tempValues.getFees());
                        holder.rl_main.setBackgroundColor(Color.parseColor("#D4E3CC"));
                    }
                    if (tempValues.getTr_free() != null) {
                        if (tempValues.getTr_percentage() != null) {
                            if (!tempValues.getTr_free().equals("") && !tempValues.getTr_percentage().equals("")) {

                                if (!tempValues.getTr_free().equals("null") && !tempValues.getTr_percentage().equals("null")) {

                                    double fee = Double.parseDouble(tempValues.getTr_free());
                                    double per = Double.parseDouble(tempValues.getTr_percentage());
                                    String cover = tempValues.getFees();
                                    String fff = "1";
                                    if (cover.contains("@")) {
                                        fff = cover.substring(cover.indexOf("@") + 1);
                                    } else if (cover.contains("$")) {
                                        fff = cover.substring(cover.indexOf("$") + 1);
                                    }
                                    double rate = Double.parseDouble(fff);
                                    double totla_services = (rate * per) + fee;
                                    String finallab2 = String.format("%.2f", totla_services);
                                    holder.txt_service_free.setText("Service fee: $" + finallab2);
                                } else {
                                    holder.txt_service_free.setText("Service fee: $0.0");
                                }

                            } else {
                                holder.txt_service_free.setText("Service fee: $0.0");
                            }
                        } else {
                            holder.txt_service_free.setText("Service fee: $0.0");
                        }
                    } else {
                        holder.txt_service_free.setText("Service fee: $0.0");
                    }

                    holder.txt_plate.setText(tempValues.getPlate_no());
                    holder.txt_state.setText(tempValues.getState());
                    holder.txt_v_code.setText(tempValues.getVioloation_code());
                    holder.txt_v_description.setText(tempValues.getViolationdescription());
                    String ticket_no = tempValues.getTicket_no();
                    ticket_no = ticket_no != null ? (!ticket_no.equals("") ? (!ticket_no.equals("null") ? ticket_no : "") : "") : "";
                    holder.txt_id.setText(ticket_no);
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }
}
