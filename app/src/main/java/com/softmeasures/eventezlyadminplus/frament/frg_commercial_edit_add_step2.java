package com.softmeasures.eventezlyadminplus.frament;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.java.Array_class_static;
import com.softmeasures.eventezlyadminplus.models.Alphabets;
import com.softmeasures.eventezlyadminplus.models.LocationLot;

import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.frament.commercial_parking_main.commercial_edit_parking;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1.commercial_title;

public class frg_commercial_edit_add_step2 extends BaseFragment {
    TextView txt_commercial_title_2;
    EditText edit_commercial_off_peak, edit_commercial_weekend_special_discount, edit_commercial_lots_totla, edit_commercial_lots_aval;
    public static String commercial_off_peak_discount, commercial_off_start, commercial_off_end, str_pluse = "",
            commercial_off_peak_weekend, commercial_lost_total, commecial_lots_avali,
            commercial_space_total = "", commercial_rows_total = "", township_labeling = "";
    TextView txt_commercaial_off_peak_start, txt_commercial_off_peak_end, txt_commercial_pluse, txt_commercial_sp2_next;
    Spinner sp_commercial_off_peak_end, sp_commercial_off_park_start;
    boolean click_comercial_pluse = false;
    private RelativeLayout rl_main;

    private LinearLayout ll_township_managed_space_total, ll_township_rows_total, ll_township_manged_space,
            ll_labeling_township_code;
    private EditText edit_township_space_total, edit_township_rows_total;
    private Spinner sp_township_labeling;
    private Button btnPreviewLotRow;
    private String township_type_sp = "";
    private ArrayList<LocationLot> locationLots = new ArrayList<>();
    private ArrayList<String> selectedAlpha = new ArrayList<>();
    private ArrayList<Alphabets> alphabets = new ArrayList<>();
    private AlphaAdapter alphaAdapter;
    private androidx.appcompat.app.AlertDialog dialogChangeAlpha;
    String township_labeling_array[] = {"Alpha", "Numeric", "AlphaNumeric", "AlphaAlpha", "NumericNumeric"};
    private String[] alphaArray = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
            "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    private boolean isNewLocation = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getArguments() != null) {
            isNewLocation = getArguments().getBoolean("isNewLocation");
            township_type_sp = getArguments().getString("township_type_sp");
        }
        return inflater.inflate(R.layout.commercial_edit_step_2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        prepareAlpha();

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (myApp.isLocationAdded) {
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
        }
    }

    @Override
    protected void updateViews() {

        txt_commercial_title_2.setText(commercial_title);

        if (commercial_edit_parking) {
            txt_commercial_off_peak_end.setVisibility(View.VISIBLE);
            txt_commercial_off_peak_end.setText(commercial_off_end);
            sp_commercial_off_peak_end.setSelection(0);

            txt_commercaial_off_peak_start.setVisibility(View.VISIBLE);
            txt_commercaial_off_peak_start.setText(commercial_off_start);
            sp_commercial_off_park_start.setSelection(0);

            edit_commercial_weekend_special_discount.setText(commercial_off_peak_weekend);
            edit_commercial_lots_totla.setText(commercial_lost_total);
            edit_commercial_lots_aval.setText(commecial_lots_avali);
            edit_commercial_off_peak.setText(commercial_off_peak_discount);
        }

        if (township_type_sp.equals("Managed Paid")) {
            btnPreviewLotRow.setVisibility(View.VISIBLE);
            ll_township_managed_space_total.setVisibility(View.VISIBLE);
            ll_township_rows_total.setVisibility(View.VISIBLE);
            ll_township_manged_space.setVisibility(View.VISIBLE);
            ll_labeling_township_code.setVisibility(View.VISIBLE);
//            ll_off_peak_township.setVisibility(View.VISIBLE);
//            ll_township_off_peak_start.setVisibility(View.VISIBLE);
//            ll_township_off_end.setVisibility(View.VISIBLE);
//            ll_township_week_end.setVisibility(View.VISIBLE);
            edit_commercial_lots_totla.setEnabled(false);
            edit_commercial_lots_aval.setEnabled(false);
//            if (!township_parking_edit) {
//                edit_township_lots_total.setText("");
//                edit_township_lots_aval.setText("");
//                edit_township_rows_total.setText("");
//                edit_township_space_total.setText("");
//            }
//
//            ll_township_pricing_duration.setVisibility(View.VISIBLE);
//            ll_township_pricing.setVisibility(View.VISIBLE);
        } else if (township_type_sp.equals("Managed Free")) {
            btnPreviewLotRow.setVisibility(View.VISIBLE);
            ll_township_managed_space_total.setVisibility(View.VISIBLE);
            ll_township_rows_total.setVisibility(View.VISIBLE);
            ll_township_manged_space.setVisibility(View.VISIBLE);
            ll_labeling_township_code.setVisibility(View.VISIBLE);
//            ll_off_peak_township.setVisibility(View.GONE);
//            ll_township_off_peak_start.setVisibility(View.GONE);
//            ll_township_off_end.setVisibility(View.GONE);
//            ll_township_week_end.setVisibility(View.GONE);
//
            edit_commercial_lots_totla.setEnabled(false);
            edit_commercial_lots_aval.setEnabled(false);
//            if (!township_parking_edit) {
//                edit_township_lots_total.setText("");
//                edit_township_lots_aval.setText("");
//                edit_township_rows_total.setText("");
//                edit_township_space_total.setText("");
//            }
//            ll_township_pricing_duration.setVisibility(View.GONE);
//            ll_township_pricing.setVisibility(View.GONE);
        } else if (township_type_sp.equals("Paid")) {
            btnPreviewLotRow.setVisibility(View.GONE);
            ll_township_managed_space_total.setVisibility(View.GONE);
            ll_township_rows_total.setVisibility(View.GONE);
            ll_township_manged_space.setVisibility(View.GONE);
            ll_labeling_township_code.setVisibility(View.GONE);
//            ll_off_peak_township.setVisibility(View.VISIBLE);
//            ll_township_off_peak_start.setVisibility(View.VISIBLE);
//            ll_township_off_end.setVisibility(View.VISIBLE);
//            ll_township_week_end.setVisibility(View.VISIBLE);
//            if (!township_parking_edit) {
//                edit_township_lots_total.setText("");
//                edit_township_lots_aval.setText("");
//                edit_township_rows_total.setText("");
//                edit_township_space_total.setText("");
//            }
            edit_commercial_lots_totla.setEnabled(true);
            edit_commercial_lots_aval.setEnabled(true);
//            ll_township_pricing_duration.setVisibility(View.VISIBLE);
//            ll_township_pricing.setVisibility(View.VISIBLE);
        } else {
            btnPreviewLotRow.setVisibility(View.GONE);
        }

    }

    @Override
    protected void setListeners() {
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        edit_township_space_total.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.equals("")) {

                } else {
                    edit_commercial_lots_totla.setText(s);
                    edit_commercial_lots_aval.setText(s);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnPreviewLotRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                township_labeling = (String) sp_township_labeling.getSelectedItem();
                String totalRows = edit_township_rows_total.getText().toString();
                String totalSpaces = edit_township_space_total.getText().toString();

                if (!TextUtils.isEmpty(totalRows) && !TextUtils.isEmpty(totalSpaces)) {
                    int spaces = Integer.parseInt(totalSpaces);
                    int rows = Integer.parseInt(totalRows);
                    if (spaces >= rows) {
                        switch (township_labeling) {
                            case "Alpha":
                                if (spaces > 26 && rows > 26) {
                                    Toast.makeText(getActivity(), "Total Spaces not more than 26 for Alpha Series!", Toast.LENGTH_SHORT).show();
                                } else {
                                    prepareLocationLotRowsData(rows, spaces, township_labeling);
                                }
                                break;
                            case "Numeric":
                                prepareLocationLotRowsData(rows, spaces, township_labeling);
                                break;
                            case "AlphaNumeric":
                                if (rows > 26) {
                                    Toast.makeText(getActivity(), "Total Rows not more than 26 for Alpha Series!", Toast.LENGTH_SHORT).show();
                                } else {
                                    prepareLocationLotRowsData(rows, spaces, township_labeling);
                                }
                                break;
                            case "AlphaAlpha":
                                if (spaces > 26 && rows > 26) {
                                    Toast.makeText(getActivity(), "Total Rows not more than 26 for Alpha Series!", Toast.LENGTH_SHORT).show();
                                } else if (spaces > 26) {
                                    Toast.makeText(getActivity(), "Total Spaces not more than 26 for Alpha Series!", Toast.LENGTH_SHORT).show();
                                } else {
                                    prepareLocationLotRowsData(rows, spaces, township_labeling);
                                }
                                break;
                            case "NumericNumeric":
                                prepareLocationLotRowsData(rows, spaces, township_labeling);
                                break;
                        }
                    } else {
                        Toast.makeText(getActivity(), "Total Rows not more than Total Spaces", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        sp_commercial_off_park_start.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    commercial_off_start = (String) sp_commercial_off_park_start.getSelectedItem();
                    txt_commercaial_off_peak_start.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_commercial_off_peak_end.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    commercial_off_end = (String) sp_commercial_off_peak_end.getSelectedItem();
                    txt_commercial_off_peak_end.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        txt_commercial_pluse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (click_comercial_pluse) {
                    click_comercial_pluse = false;
                    txt_commercial_pluse.setText("+");
                } else {
                    click_comercial_pluse = true;
                    txt_commercial_pluse.setText("-");
                }
            }
        });

        sp_township_labeling.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                township_labeling = (String) sp_township_labeling.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> paren1t) {
            }
        });

        txt_commercial_sp2_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commercial_space_total = edit_township_space_total.getText().toString();
                commercial_rows_total = edit_township_rows_total.getText().toString();
                commercial_lost_total = edit_commercial_lots_totla.getText().toString();
                commecial_lots_avali = edit_commercial_lots_aval.getText().toString();
                str_pluse = txt_commercial_pluse.getText().toString();
                commercial_off_peak_discount = edit_commercial_off_peak.getText().toString();
                commercial_off_peak_weekend = edit_commercial_weekend_special_discount.getText().toString();
                if (v != null) {

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                if (township_type_sp.contains("Managed")) {

                    if (!commercial_space_total.equals("") && !commercial_rows_total.equals("")) {
                        if (!commercial_lost_total.equals("")) {
                            if (!commecial_lots_avali.equals("")) {
                                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                frg_commercial_edit_add_step3 pay = new frg_commercial_edit_add_step3();
                                Bundle bundle = getArguments();
                                bundle.putString("lot_numbering_type", township_labeling);
                                bundle.putBoolean("isNewLocation", isNewLocation);
                                pay.setArguments(bundle);
                                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                ft.add(R.id.My_Container_1_ID, pay);
                                fragmentStack.lastElement().onPause();
                                ft.hide(fragmentStack.lastElement());
                                fragmentStack.push(pay);
                                ft.commitAllowingStateLoss();
                            } else {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                alertDialog.setTitle("Available lots Required");
                                alertDialog.setMessage("Please enter value for available lots");
                                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                                alertDialog.show();
                            }

                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("Total lots Required");
                            alertDialog.setMessage("Please enter value for total lots");
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialog.show();
                        }
                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Spaces Required");
                        alertDialog.setMessage("Please enter value for available lots");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }
                } else {
                    if (!commercial_lost_total.equals("")) {
                        if (!commecial_lots_avali.equals("")) {
                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            frg_commercial_edit_add_step3 pay = new frg_commercial_edit_add_step3();
                            Bundle bundle = getArguments();
                            bundle.putString("lot_numbering_type", township_labeling);
                            pay.setArguments(bundle);
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.add(R.id.My_Container_1_ID, pay);
                            fragmentStack.lastElement().onPause();
                            ft.hide(fragmentStack.lastElement());
                            fragmentStack.push(pay);
                            ft.commitAllowingStateLoss();
                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("Available lots Required");
                            alertDialog.setMessage("Please enter value for available lots");
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialog.show();
                        }

                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Total lots Required");
                        alertDialog.setMessage("Please enter value for total lots");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }
                }

            }
        });


    }

    private void prepareLocationLotRowsData(int rows, int spaces, String labeling) {
        locationLots.clear();
        String[] alpha = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
                "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

        Log.e("#DEBUG", "  prepareLocationLotRowsData:  spaces: " + spaces + "  rows: " + rows
                + "labeling: " + labeling + "  alpha: " + alpha.length);
        selectedAlpha.clear();
        switch (labeling) {
            case "Alpha":
                if (spaces < alpha.length + 1) {
                    for (int j = 0; j < spaces; j++) {
                        LocationLot locationLot = new LocationLot();
                        locationLot.setLot_number(alpha[j]);
                        locationLot.setLot_id(alpha[j]);
                        locationLots.add(locationLot);
                        selectedAlpha.add(alpha[j]);
                    }
                }
                break;
            case "Numeric":
                for (int j = 0; j < spaces; j++) {
                    LocationLot locationLot = new LocationLot();
                    locationLot.setLot_number(String.valueOf(j + 1));
                    locationLot.setLot_id(String.valueOf(j + 1));
                    locationLots.add(locationLot);

                }
                break;
            case "AlphaNumeric":
                int inEachRowSpaces = spaces / rows;
                if (rows < alpha.length + 1) {
                    for (int j = 0; j < rows; j++) {
                        selectedAlpha.add(alpha[j]);
                        for (int k = 0; k < inEachRowSpaces; k++) {
                            LocationLot locationLot = new LocationLot();
                            locationLot.setLot_row(alpha[j]);
                            locationLot.setLot_number(String.valueOf(k + 1));
                            locationLot.setLot_id(alpha[j] + ":" + (k + 1));
                            locationLots.add(locationLot);
                        }
                    }
                }
                break;
            case "AlphaAlpha":
                int inEachRowSpacesAlpha = spaces / rows;
                if (rows < alpha.length + 1) {
                    for (int j = 0; j < rows; j++) {
                        selectedAlpha.add(alpha[j]);
                        for (int k = 0; k < inEachRowSpacesAlpha; k++) {
                            LocationLot locationLot = new LocationLot();
                            locationLot.setLot_row(alpha[j]);
                            locationLot.setLot_number(alpha[k]);
                            locationLot.setLot_id(String.format("%s:%s", alpha[j], alpha[k]));
                            locationLots.add(locationLot);
                        }
                    }
                }
                break;
            case "NumericNumeric":
                int inEachRowSpacesNumeric = spaces / rows;
                if (rows < alpha.length + 1) {
                    for (int j = 0; j < rows; j++) {
                        for (int k = 0; k < inEachRowSpacesNumeric; k++) {
                            LocationLot locationLot = new LocationLot();
                            locationLot.setLot_row(String.valueOf(j + 1));
                            locationLot.setLot_number(String.valueOf(k + 1));
                            locationLot.setLot_id(String.format("%s:%s", String.valueOf(j + 1), String.valueOf(k + 1)));
                            locationLots.add(locationLot);
                        }
                    }
                }
                break;
        }

        showDialogPreviewLotRow();
    }

    private void showDialogPreviewLotRow() {
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        builder.setCancelable(false);
        View dialogView = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_preview_lot_row, null);
        builder.setView(dialogView);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        RelativeLayout rlClose = dialogView.findViewById(R.id.rlClose);
        RecyclerView rvLocationLot = dialogView.findViewById(R.id.rvLocationLot);
        Button btnChangeAlpha = dialogView.findViewById(R.id.btnChangeAlpha);
        Button btnChangeNumberRange = dialogView.findViewById(R.id.btnChangeNumberRange);
        LocationLotAdapter locationLotAdapter = new LocationLotAdapter();
        rvLocationLot.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        rvLocationLot.setAdapter(locationLotAdapter);
        rlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        for (int j = 0; j < alphabets.size(); j++) {
            alphabets.get(j).setSelected(false);
        }
        for (int j = 0; j < selectedAlpha.size(); j++) {
            alphabets.get(alphabets.indexOf(new Alphabets(selectedAlpha.get(j)))).setSelected(true);
        }
        btnChangeAlpha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogChangeAlpha();
            }
        });
        btnChangeNumberRange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.show();

    }

    private void prepareAlpha() {
        alphabets.clear();
        for (int j = 0; j < alphaArray.length; j++) {
            Alphabets alphabet = new Alphabets();
            alphabet.setName(alphaArray[j]);
            alphabet.setSelected(false);
            alphabets.add(alphabet);
        }
    }

    private void showDialogChangeAlpha() {
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        View dialogView = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_change_alpha, null);
        RecyclerView rvAlpha = dialogView.findViewById(R.id.rvAlpha);
        TextView tvBtnOK = dialogView.findViewById(R.id.tvBtnOK);
        builder.setView(dialogView);
        dialogChangeAlpha = builder.create();
        tvBtnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogChangeAlpha.dismiss();
            }
        });
        alphaAdapter = new AlphaAdapter();
        rvAlpha.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvAlpha.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        rvAlpha.setAdapter(alphaAdapter);
        dialogChangeAlpha.show();
    }

    private class AlphaAdapter extends RecyclerView.Adapter<AlphaAdapter.AlphaHolder> {
        @NonNull
        @Override
        public AlphaAdapter.AlphaHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new AlphaHolder(LayoutInflater.from(getActivity())
                    .inflate(R.layout.item_alpha_multi_select, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull AlphaAdapter.AlphaHolder alphaHolder, int i) {
            Alphabets alphabet = alphabets.get(i);
            if (alphabet != null) {
                alphaHolder.tvAlpha.setText(alphabet.getName());
                if (alphabet.isSelected()) {
                    alphaHolder.cbAlpha.setChecked(true);
                } else alphaHolder.cbAlpha.setChecked(false);
            }
        }

        @Override
        public int getItemCount() {
            return alphabets.size();
        }

        class AlphaHolder extends RecyclerView.ViewHolder {
            TextView tvAlpha;
            CheckBox cbAlpha;

            AlphaHolder(@NonNull View itemView) {
                super(itemView);
                tvAlpha = itemView.findViewById(R.id.tvAlpha);
                cbAlpha = itemView.findViewById(R.id.cbAlpha);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            if (alphabets.get(position).isSelected()) {
                                alphabets.get(position).setSelected(false);
                            } else {
                                alphabets.get(position).setSelected(true);
                            }
                            notifyItemChanged(position);
                        }
                    }
                });
            }
        }
    }

    @Override
    protected void initViews(View view) {

        ll_township_managed_space_total = view.findViewById(R.id.ll_township_managed_space_total);
        ll_township_rows_total = view.findViewById(R.id.ll_township_rows_total);
        ll_township_manged_space = view.findViewById(R.id.ll_township_manged_space);
        ll_labeling_township_code = view.findViewById(R.id.ll_labeling_township_code);
        edit_township_space_total = view.findViewById(R.id.edit_township_space_total);
        edit_township_rows_total = view.findViewById(R.id.edit_township_rows_total);
        sp_township_labeling = view.findViewById(R.id.sp_township_labeling);
        btnPreviewLotRow = view.findViewById(R.id.btnPreviewLotRow);

        txt_commercial_title_2 = (TextView) view.findViewById(R.id.txt_step2_commercial_title);
        edit_commercial_off_peak = (EditText) view.findViewById(R.id.edit_commercial_off_peak);
        txt_commercial_off_peak_end = (TextView) view.findViewById(R.id.txt_commercial_off_peak_end);
        txt_commercaial_off_peak_start = (TextView) view.findViewById(R.id.txt_commercaial_off_peak_start);
        sp_commercial_off_park_start = (Spinner) view.findViewById(R.id.sp_commercial_off_peak_start);
        sp_commercial_off_peak_end = (Spinner) view.findViewById(R.id.sp_commercial_off_peak_end);
        txt_commercial_pluse = (TextView) view.findViewById(R.id.txt_commercial_plue);
        edit_commercial_weekend_special_discount = (EditText) view.findViewById(R.id.edit_commercial_weekend_special_diff);
        edit_commercial_lots_aval = (EditText) view.findViewById(R.id.edit_commercial_lots_aval);
        edit_commercial_lots_totla = (EditText) view.findViewById(R.id.edit_commercial_lots_total);
        txt_commercial_sp2_next = (TextView) view.findViewById(R.id.txt_commercial_sp2);
        rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);

        ArrayAdapter<String> sp_private1_start_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.start_hour_array);
        ArrayAdapter<String> sp_private1_end_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.end_hour_array);
        sp_commercial_off_park_start.setAdapter(sp_private1_start_adapter);
        sp_commercial_off_park_start.setSelection(2);
        sp_commercial_off_peak_end.setAdapter(sp_private1_end_adapter);
        sp_commercial_off_peak_end.setSelection(24);

        ArrayAdapter<String> sp_labeling_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, township_labeling_array);
        sp_township_labeling.setAdapter(sp_labeling_adapter);
        sp_township_labeling.setSelection(2);
    }

    private class LocationLotAdapter extends RecyclerView.Adapter<LocationLotAdapter.LocationLotHolder> {

        @NonNull
        @Override
        public LocationLotAdapter.LocationLotHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new LocationLotHolder(LayoutInflater.from(getActivity())
                    .inflate(R.layout.item_location_lot, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull LocationLotAdapter.LocationLotHolder locationLotHolder, int i) {
            LocationLot locationLot = locationLots.get(i);
            if (locationLot != null) {
                if (!TextUtils.isEmpty(locationLot.getLot_id())) {
                    locationLotHolder.tvRow.setText(locationLot.getLot_id());
                }
            }

        }

        @Override
        public int getItemCount() {
            return locationLots.size();
        }

        class LocationLotHolder extends RecyclerView.ViewHolder {
            ImageView ivCar;
            TextView tvRow;

            LocationLotHolder(@NonNull View itemView) {
                super(itemView);

                ivCar = itemView.findViewById(R.id.ivCar);
                tvRow = itemView.findViewById(R.id.tvRow);
            }
        }
    }
}
