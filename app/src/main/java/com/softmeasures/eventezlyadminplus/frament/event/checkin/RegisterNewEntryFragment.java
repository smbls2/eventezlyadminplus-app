package com.softmeasures.eventezlyadminplus.frament.event.checkin;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragRegisterNewEntryBinding;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.User;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class RegisterNewEntryFragment extends BaseFragment {

    private FragRegisterNewEntryBinding binding;
    private ProgressDialog progressDialog;

    private User user;
    private EventDefinition eventDefinition;
    private ArrayList<String> occupations = new ArrayList<>();
    private ArrayList<String> categories = new ArrayList<>();
    private String selectedOccupation = "", selectedCategory = "", paidUsing = "Cash",
            selectedFees = "", selectedContribute = "";
    private boolean isFree = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            eventDefinition = new Gson().fromJson(getArguments().getString("eventDefinition"), EventDefinition.class);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_register_new_entry, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {

        if (eventDefinition != null) {
            if (eventDefinition.getEvent_full_regn_fee() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_full_regn_fee())
                    && !eventDefinition.getEvent_full_regn_fee().equals("null")) {
                binding.tvFees.setText(String.format("$%s", eventDefinition.getEvent_full_regn_fee()));
                binding.tvContribution.setText(String.format("$%s", eventDefinition.getEvent_full_regn_fee()));
            }
        }

    }

    @Override
    protected void setListeners() {

        binding.spinnerOccupation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    selectedOccupation = occupations.get(position);
                    Log.e("#DEBUG", "    selectedOccupation:  " + selectedOccupation);
                } else selectedOccupation = "";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    selectedCategory = categories.get(position);
                    Log.e("#DEBUG", "    selectedCategory:  " + selectedCategory);
                } else selectedCategory = "";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.tvBtnFeePlus.setOnClickListener(v -> {
            int fees = Integer.parseInt(binding.tvFees.getText().toString().substring(1));
            fees = fees + 1;
            binding.tvFees.setText(String.format(Locale.getDefault(), "$%d", fees));
        });

        binding.tvBtnFeeMinus.setOnClickListener(v -> {
            int fees = Integer.parseInt(binding.tvFees.getText().toString().substring(1));
            if (fees != 0) fees = fees - 1;
            binding.tvFees.setText(String.format(Locale.getDefault(), "$%d", fees));
        });

        binding.tvBtnContributionPlus.setOnClickListener(v -> {
            int fees = Integer.parseInt(binding.tvContribution.getText().toString().substring(1));
            fees = fees + 1;
            binding.tvContribution.setText(String.format(Locale.getDefault(), "$%d", fees));
        });

        binding.tvBtnContributionMenus.setOnClickListener(v -> {
            int fees = Integer.parseInt(binding.tvContribution.getText().toString().substring(1));
            if (fees != 0) fees = fees - 1;
            binding.tvContribution.setText(String.format(Locale.getDefault(), "$%d", fees));
        });

        binding.rgPaidUsing.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.rbCash:
                    paidUsing = "Cash";
                    break;
                case R.id.rbCheck:
                    paidUsing = "Check";
                    break;
                case R.id.rbCard:
                    paidUsing = "Card";
                    break;
                case R.id.rbPledge:
                    paidUsing = "Pledge";
                    break;
            }
        });

        binding.btnCheckIn.setOnClickListener(v -> {
            if (TextUtils.isEmpty(binding.etFirstName.getText().toString())) {
                binding.etFirstName.setError("Required!");
                binding.etFirstName.requestFocus();
            } else if (TextUtils.isEmpty(binding.etLastName.getText().toString())) {
                binding.etLastName.setError("Required!");
                binding.etLastName.requestFocus();
            } else if (TextUtils.isEmpty(binding.etEmailAddress.getText().toString())) {
                binding.etEmailAddress.setError("Required!");
                binding.etEmailAddress.requestFocus();
            } else if (TextUtils.isEmpty(binding.etPhoneNumber.getText().toString())) {
                binding.etPhoneNumber.setError("Required!");
                binding.etPhoneNumber.requestFocus();
            } else if (TextUtils.isEmpty(binding.etAge.getText().toString())) {
                binding.etAge.setError("Required!");
                binding.etAge.requestFocus();
            } else if (!isFree && TextUtils.isEmpty(selectedOccupation)) {
                Toast.makeText(getActivity(), "Please Select Occupation!", Toast.LENGTH_SHORT).show();
            } else if (!isFree && TextUtils.isEmpty(selectedCategory)) {
                Toast.makeText(getActivity(), "Please Select Category!", Toast.LENGTH_SHORT).show();
            } else {
                user = new User();
                user.setFirstName(binding.etFirstName.getText().toString());
                user.setLastName(binding.etLastName.getText().toString());
                user.setEmail(binding.etEmailAddress.getText().toString());
                user.setMobile(binding.etPhoneNumber.getText().toString());
                user.setAge(binding.etAge.getText().toString());
                user.setGender(binding.rgGender.getCheckedRadioButtonId() == R.id.rbMale ? "Male" : "Female");

                if (!TextUtils.isEmpty(binding.tvFees.getText().toString())) {
                    selectedFees = binding.tvFees.getText().toString();
                }

                if (!TextUtils.isEmpty(binding.tvContribution.getText().toString())) {
                    selectedContribute = binding.tvContribution.getText().toString();
                }

                new addEventAttendance().execute();
            }
        });

    }

    @Override
    protected void initViews(View v) {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");

        occupations.addAll(Arrays.asList(getResources().getStringArray(R.array.occupations)));
        categories.addAll(Arrays.asList(getResources().getStringArray(R.array.categories)));

        ArrayAdapter<String> occupationAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, occupations);
        binding.spinnerOccupation.setAdapter(occupationAdapter);

        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, categories);
        binding.spinnerCategory.setAdapter(categoryAdapter);

    }

    public class addEventAttendance extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String urlReg = "_table/event_attendance";
        JSONObject json1;
        String re_id;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues = new HashMap<String, Object>();

            jsonValues.put("date_time", DateTimeUtils.getSqlFormatDate(Calendar.getInstance().getTime()));
            jsonValues.put("fname", user.getFirstName());
            jsonValues.put("lname", user.getLastName());
            jsonValues.put("full_name", user.getFirstName() + " " + user.getLastName());
            jsonValues.put("email", user.getEmail());
            jsonValues.put("mobile", user.getMobile());
            jsonValues.put("age", user.getAge());
            jsonValues.put("gender", user.getGender());
            jsonValues.put("event_fee_selected_type", user.getEvent_fee_selected_type());
            jsonValues.put("event_fee_selected", user.getEvent_fee_selected());
            jsonValues.put("event_registration_id", user.getEvent_registration_id());
            jsonValues.put("event_id", eventDefinition.getEvent_id());
            jsonValues.put("company_id", eventDefinition.getCompany_id());
            jsonValues.put("event_date", eventDefinition.getEvent_begins_date_time());
            jsonValues.put("ip", getLocalIpAddress());

            jsonValues.put("opt1_occupation", selectedOccupation);
            jsonValues.put("opt2_occupation_title", selectedCategory);
            jsonValues.put("opt3_fee", selectedFees);
            jsonValues.put("opt4_contribution", selectedContribute);
            jsonValues.put("paid_using", paidUsing);

            jsonValues.put("user_id", logindeatl.getString("id", ""));
            jsonValues.put("username", logindeatl.getString("name", ""));

            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + urlReg;
            HttpResponse response = null;
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            post.setEntity(entity);
            Log.e("#DEBUG", "  addEventAttendance:  " + url);
            Log.e("#DEBUG", "   addEventAttendance Params: " + String.valueOf(json));
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   addEventAttendance:  Response:  " + responseStr);
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.e("#DEBUG", "  addEventAttendance: ResponseID: " + re_id);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.hide();
            if (getActivity() != null && json1 != null) {
                if (re_id != null && !re_id.equals("null")) {
                    Toast.makeText(getActivity(), "Attendance Added!", Toast.LENGTH_SHORT).show();
                    getActivity().onBackPressed();
                }
            }
            super.onPostExecute(s);
        }
    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("ip", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("aax", ex.toString());
        }
        return null;
    }
}
