package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.Settings;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.LocationLot;
import com.softmeasures.eventezlyadminplus.services.Constants;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;
import com.softmeasures.eventezlyadminplus.services.GeocodeAddressIntentService;
import com.softmeasures.eventezlyadminplus.services.NotificationPublisher;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static android.content.Context.ALARM_SERVICE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.all_state;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class freeparkhere extends Fragment implements vehicle_add.vehivcle_add_Click_listener {

    ProgressDialog pdialog;
    TextView txttime, txtmaxhores, txtparknow, txt_parking_location_lable;
    EditText editno;
    Spinner spsate, sp_row, sp_space, sp_hours;
    int i = 1, hours = 0;
    View layout;
    PopupWindow popup;
    String statename = "State", rate, rate1, zip_code, city, lat = "0.0", lang = "0.0", platno, adrreslocation = "null", countryname, p_country, max_time, expiretime = "", entry_time = "", location_code, location_name, managedpin, user_id, lost_row, lost_no, lost_row1, lost_no1, parkingtype = "", token1, pid, locationaddress, parkhere = "", marker, state, title, renew, min, shoues = "", duration_unit, marker_type = "", location_id;
    //Map<String, Object> statefullname;
    ConnectionDetector cd;
    double latitude = 0.0, longitude = 0.0;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar, rl_main, rl_main1, rl_sp_row, rl_sp_space, rl_hours;
    String statearray[] = {"State", "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"};
    String row[] = {"ROW", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    ArrayList<String> space = new ArrayList<>();
    int token = 0;
    ArrayList<item> filtermanaged = new ArrayList<>();
    String township_row, township_spaces, township_labeling, township_sapce_marked, township_code;
    int total_row = 0;
    ArrayList<String> row_array = new ArrayList<>();
    ArrayList<String> row_number_array = new ArrayList<>();
    ArrayList<String> Duration_array = new ArrayList<>();
    TextView txt_state, txt_hours, txt_row, txt_space;
    String township_location_name;
    ArrayList<item> managedlostpoparray = new ArrayList<>();
    ArrayList<item> data = new ArrayList<>();
    RelativeLayout rl_title;
    final ArrayList<item> stateq = new ArrayList<item>();
    final ArrayList<item> state_array = new ArrayList<item>();
    ImageView txt_plate_no;
    ArrayList<item> listofvehicle = new ArrayList<>();
    public PopupWindow popup_state;
    AddressResultReceiver mResultReceiver;
    static boolean is_add_back = false;
    String Renew_date, ReNew_id, Renew_parked_time, Renew_is_valid = "";
    int final_count = 0;

    private ArrayList<LocationLot> locationLots = new ArrayList<>();
    private LocationLot selectedLocationLot;
    private androidx.appcompat.app.AlertDialog dialogLocationLot;
    private String currentdate = "";
    private SimpleDateFormat sdf;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View view = inflater.inflate(R.layout.fragment_freeparkhere, container, false);
        SharedPreferences back_select = getActivity().getSharedPreferences("back_free_parking", Context.MODE_PRIVATE);
        SharedPreferences.Editor edd = back_select.edit();
        edd.putString("back", "yes");
        edd.commit();

        mResultReceiver = new AddressResultReceiver(null);
        txttime = (TextView) view.findViewById(R.id.parkheretime);
        txtmaxhores = (TextView) view.findViewById(R.id.parkheremaxhours);
        rl_title = (RelativeLayout) view.findViewById(R.id.title);
        txt_plate_no = (ImageView) view.findViewById(R.id.txt_plate);
        txtparknow = (TextView) view.findViewById(R.id.parknow);
        editno = (EditText) view.findViewById(R.id.editparkherenoplantno);
        spsate = (Spinner) view.findViewById(R.id.spsatename);
        sp_row = (Spinner) view.findViewById(R.id.sp_row1);
        sp_space = (Spinner) view.findViewById(R.id.sp_space1);
        sp_hours = (Spinner) view.findViewById(R.id.sphours);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        rl_main1 = (RelativeLayout) view.findViewById(R.id.rl_main1);
        rl_sp_row = (RelativeLayout) view.findViewById(R.id.rl_sp_row);
        rl_sp_space = (RelativeLayout) view.findViewById(R.id.rl_sp_space);
        rl_hours = (RelativeLayout) view.findViewById(R.id.sp_houes);
        txt_parking_location_lable = (TextView) view.findViewById(R.id.txt_parkinglocation);

        txt_state = (TextView) view.findViewById(R.id.txt_state);
        txt_hours = (TextView) view.findViewById(R.id.txt_hours);
        txt_row = (TextView) view.findViewById(R.id.txt_row);
        txt_space = (TextView) view.findViewById(R.id.txt_space);
        cd = new ConnectionDetector(getActivity());
        ((vchome) getActivity()).popupopen();
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = logindeatl.getString("id", "null");
        rl_main1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        SharedPreferences d = getActivity().getSharedPreferences("renew", Context.MODE_PRIVATE);
        renew = d.getString("renew", "no");
        space.add(("SPACE#"));
        for (int i = 1; i <= 100; i++) {
            space.add(String.valueOf(i));
        }

        if (cd.isConnectingToInternet()) {
            currentlocation();
            SharedPreferences s = getActivity().getSharedPreferences("paidparkhere", Context.MODE_PRIVATE);
            String time = s.getString("Time", "");
            max_time = s.getString("Max", "");
            rate = s.getString("rate", "");
            rate1 = s.getString("rated", "");
            zip_code = s.getString("zip_code", "");
            city = s.getString("city", "");
            lat = s.getString("lat", "");
            lang = s.getString("lang", "");
            pid = s.getString("id", "null");
            location_code = s.getString("location_code", "");
            location_name = s.getString("location_name", "");
            managedpin = s.getString("managedloick", "null");
            locationaddress = s.getString("address", "null");
            parkhere = s.getString("parkhere", "no");
            marker = s.getString("mar", "marker");
            title = s.getString("title", "null");
            marker_type = s.getString("parking_type", "");
            township_code = s.getString("township_code", "");
            location_id = s.getString("location_id", "");
            duration_unit = s.getString("duation_unit", "Hours");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            entry_time = sdf.format(new Date());

            SharedPreferences appSharedPrefs = getActivity().getSharedPreferences("parking_rules", Context.MODE_PRIVATE);
            Gson gson = new Gson();
            String json1 = appSharedPrefs.getString("managed", "");
            Type type1 = new TypeToken<ArrayList<item>>() {
            }.getType();
            filtermanaged = gson.fromJson(json1, type1);
            final ArrayList<item> array_space = new ArrayList<>();
            final ArrayList<item> array_row = new ArrayList<>();
            ArrayList<item> array = new ArrayList<item>();
            if (managedpin.equals("yes")) {
                txt_parking_location_lable.setVisibility(View.GONE);
                rl_sp_row.setVisibility(View.VISIBLE);

                if (filtermanaged.size() > 0) {
                    for (int i = 0; i < filtermanaged.size(); i++) {
                        township_labeling = filtermanaged.get(i).getLabeling();
                        township_row = filtermanaged.get(i).getTotal_rows();
                        township_spaces = filtermanaged.get(i).getTotal_spaces();
                        township_sapce_marked = "true";
                        township_location_name = filtermanaged.get(i).getLoationname();
                        break;
                    }
                    if (isInteger(township_spaces) && isInteger(township_row)) {
                        int total_spces = Integer.parseInt(township_spaces);
                        total_row = Integer.parseInt(township_row);
                        int rowcount = 1;
                        if (total_row > 0 && total_spces > 0) {
                            if (total_spces >= total_row) {
                                rowcount = Integer.parseInt(township_spaces) / Integer.parseInt(township_row);
                                if ((total_spces % total_row) > 0) {
                                }
                            } else {
                                rowcount = 1;
                            }
                        }

                        int curren_row = 0;
                        int first_row = 1;
                        String row[] = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

                        int array_count = 0;
                        if (township_sapce_marked.equals("false")) {
                            array_count = total_row;
                        } else {
                            array_count = total_spces;
                        }
                        //township_labeling="AlphaNumeric";
                        if (township_labeling.equals("Alpha")) {

                            for (int i = 0; i < array_count; i++) {

                                item ii = new item();
                                curren_row++;
                                if (curren_row == rowcount) {
                                    if (township_sapce_marked.equals("false")) {
                                        ii.setDisplay(row[first_row - 1].toString());
                                    } else if (total_row < 2) {
                                        ii.setDisplay(row[first_row - 1].toString());
                                    } else {
                                        ii.setDisplay(row[first_row - 1].toString() + " : " + row[curren_row - 1].toString());
                                    }
                                    ii.setIsparked("f");
                                    ii.setSelected_parking(false);
                                    ii.setSpaces_mark_reqd(township_sapce_marked);
                                    ii.setRow_no(row[curren_row - 1].toString());
                                    ii.setRoe_name(row[first_row - 1].toString());
                                    //ii.setDisplay(row[first_row - 1].toString() + " : " + row[curren_row - 1].toString());
                                    // ii.setDisplay(row[first_row - 1].toString());
                                    ii.setMenu_img_id(total_row);
                                    ii.setMarker("red");
                                    array.add(ii);
                                    first_row++;
                                    curren_row = 0;
                                } else {
                                    ii.setSpaces_mark_reqd(township_sapce_marked);
                                    ii.setIsparked("f");
                                    ii.setRow_no(row[curren_row - 1].toString());
                                    ii.setRoe_name(row[first_row - 1].toString());
                                    ii.setMenu_img_id(total_row);
                                    ii.setMarker("red");
                                    ii.setSelected_parking(false);
                                    if (township_sapce_marked.equals("false")) {
                                        ii.setDisplay(row[first_row - 1].toString());
                                    } else if (total_row < 2) {
                                        ii.setDisplay(row[first_row - 1].toString());
                                    } else {
                                        ii.setDisplay(row[first_row - 1].toString() + " : " + row[curren_row - 1].toString());
                                    }
                                    //ii.setDisplay(row[first_row - 1].toString() + " : " + row[curren_row - 1].toString());
                                    array.add(ii);
                                }
                            }

                            for (int j = 0; j < array.size(); j++) {
                                Log.e("row", array.get(j).toString());
                            }
                        } else if (township_labeling.equals("Numeric")) {


                            for (int i = 0; i < array_count; i++) {
                                item ii = new item();
                                curren_row++;
                                if (curren_row == rowcount) {
                                    if (township_sapce_marked.equals("false")) {
                                        ii.setDisplay(String.valueOf(i + 1));
                                    } else if (total_row < 2) {
                                        ii.setDisplay(String.valueOf(first_row));
                                    } else {
                                        ii.setDisplay(String.valueOf(first_row) + " : " + String.valueOf(curren_row));
                                    }
                                    ii.setSpaces_mark_reqd(township_sapce_marked);
                                    ii.setIsparked("f");
                                    ii.setRow_no(String.valueOf(i + 1));
                                    ii.setRoe_name(String.valueOf(first_row));
                                    ii.setMenu_img_id(total_row);
                                    ii.setMarker("red");
                                    ii.setSelected_parking(false);
                                    //  ii.setDisplay(String.valueOf(first_row) + " : " + String.valueOf(curren_row));
                                    // ii.setDisplay(String.valueOf(first_row));
                                    array.add(ii);
                                    first_row++;
                                    curren_row = 0;
                                } else {
                                    if (township_sapce_marked.equals("false")) {
                                        ii.setDisplay(String.valueOf(i + 1));
                                    } else if (total_row < 2) {
                                        ii.setDisplay(String.valueOf(first_row));
                                    } else {
                                        ii.setDisplay(String.valueOf(first_row) + " : " + String.valueOf(curren_row));
                                    }
                                    ii.setSpaces_mark_reqd(township_sapce_marked);
                                    ii.setIsparked("f");
                                    ii.setSelected_parking(false);
                                    ii.setRow_no(String.valueOf(i + 1));
                                    ii.setRoe_name(String.valueOf(first_row));
                                    ii.setMenu_img_id(total_row);
                                    ii.setMarker("red");
                                    // ii.setDisplay(String.valueOf(first_row) + " : " + String.valueOf(curren_row));
                                    array.add(ii);
                                }
                            }

                            for (int j = 0; j < array.size(); j++) {
                                Log.e("row", array.get(j).toString());
                            }
                        } else if (township_labeling.equals("AlphaNumeric")) {

                            for (int i = 0; i < array_count; i++) {
                                item ii = new item();
                                curren_row++;
                                if (curren_row == rowcount) {
                                    ii.setSpaces_mark_reqd(township_sapce_marked);
                                    ii.setIsparked("f");
                                    ii.setSelected_parking(false);
                                    ii.setRow_no(String.valueOf(curren_row));
                                    ii.setRoe_name(row[first_row - 1].toString());
                                    ii.setMenu_img_id(total_row);
                                    ii.setMarker("red");
                                    ii.setDisplay(row[first_row - 1].toString() + " : " + String.valueOf(curren_row));
                                    array.add(ii);
                                    first_row++;
                                    curren_row = 0;
                                } else {
                                    ii.setSpaces_mark_reqd(township_sapce_marked);
                                    ii.setIsparked("f");
                                    ii.setSelected_parking(false);
                                    ii.setMarker("red");
                                    ii.setRow_no(String.valueOf(curren_row));
                                    ii.setMenu_img_id(total_row);
                                    ii.setRoe_name(row[first_row - 1].toString());
                                    ii.setDisplay(row[first_row - 1].toString() + " : " + String.valueOf(curren_row));
                                    array.add(ii);
                                }
                            }
                            for (int j = 0; j < array.size(); j++) {
                                Log.e("row", array.get(j).getDisplay());
                            }
                        }
                        new getlistofmanagedloatpopup(location_code, array).execute();
                    }

                    rl_sp_row.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            Showmanaedpopup(getActivity(), data);
                            showDialogLocationLots();
                        }
                    });
                }
                new fetchLocationLotOccupiedData().execute();

            } else {
                txt_parking_location_lable.setVisibility(View.GONE);
                rl_sp_row.setVisibility(View.GONE);
                rl_sp_space.setVisibility(View.GONE);
            }

            if (!time.equals("")) {
                txttime.setText(time);
                if (!max_time.equals("")) {
                    int max = Integer.parseInt(max_time);

                    if (max > 0) {

                        Duration_array.add(duration_unit + "s");
                        for (int i = 0; i < max; i++) {

                            if (duration_unit.equals("Minute")) {
                                Duration_array.add(String.valueOf(i + 1) + " Mins");
                            } else if (duration_unit.equals("Month")) {
                                Duration_array.add(String.valueOf(i + 1) + " Months");
                            } else if (duration_unit.equals("Hour")) {
                                Duration_array.add(String.valueOf(i + 1) + " Hrs");
                            } else if (duration_unit.equals("Day")) {
                                Duration_array.add(String.valueOf(i + 1) + " Days");
                            } else if (duration_unit.equals("Week")) {
                                Duration_array.add(String.valueOf(i + 1) + " Weeks");
                            }
                        }
                    }
                    ArrayAdapter<String> sp_diration_array = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Duration_array);
                    sp_hours.setAdapter(sp_diration_array);
                    txtmaxhores.setText(String.format("Max: %s %s", max_time, duration_unit));
                    if (max_time != null && !TextUtils.isEmpty(max_time)
                            && !max_time.equals("1")) {
                        if (duration_unit.equals("Hour")) {
                            txtmaxhores.setText(String.format("Max: %s Hours", max_time));
                        } else if (duration_unit.equals("Minute")) {
                            txtmaxhores.setText(String.format("Max: %s Minutes", max_time));
                        } else if (duration_unit.equals("Day")) {
                            txtmaxhores.setText(String.format("Max: %s Days", max_time));
                        } else if (duration_unit.equals("Week")) {
                            txtmaxhores.setText(String.format("Max: %s Weeks", max_time));
                        } else if (duration_unit.equals("Month")) {
                            txtmaxhores.setText(String.format("Max: %s Months", max_time));
                        }
                    }
                }

                if (max_time.equals("0")) {
                    txtmaxhores.setVisibility(View.GONE);
                    txttime.setVisibility(View.GONE);
                }
            }

            for (int i = 1; i < Duration_array.size(); i++) {
                item ii = new item();
                ii.setState(Duration_array.get(i).toString());
                ii.setSelected_parking(false);
                stateq.add(ii);
            }

            if (stateq.size() >= 4) {
                stateq.get(3).setSelected_parking(true);
                shoues = stateq.get(3).getState();
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    txt_hours.setText(hr + " Hrs");
                } else {
                    txt_hours.setText(shoues);
                }

            } else if (stateq.size() >= 3) {
                stateq.get(2).setSelected_parking(true);
                shoues = stateq.get(2).getState();
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    txt_hours.setText(hr + " Hrs");
                } else {
                    txt_hours.setText(shoues);
                }
            } else if (stateq.size() >= 2) {
                stateq.get(1).setSelected_parking(true);
                shoues = stateq.get(1).getState();
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    txt_hours.setText(hr + " Hrs");
                } else {
                    txt_hours.setText(shoues);
                }
            }

            txt_hours.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShowHours(getActivity(), stateq);
                }
            });

            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, statearray);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spsate.setAdapter(spinnerArrayAdapter);
            spsate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    statename = (String) spsate.getSelectedItem();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            sp_hours.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    shoues = (String) sp_hours.getSelectedItem();
                }

                @Override
                public void onNothingSelected(AdapterView<?> paren1t) {
                }
            });


            editno.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        show_marke_payment_button();
                        ShowStatePupup(getActivity(), state_array);
                        return true;
                    }
                    return false;
                }
            });


            txt_state.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    state_array.clear();
                    Log.e("all_state ", " : " + all_state.size());
                    for (int i = 1; i < all_state.size(); i++) {
                        String statename = all_state.get(i).getState2Code();
                        item ii = new item();
                        ii.setState(statename);
                        if (statename.equals(txt_state.getText().toString())) {
                            ii.setSelected_parking(true);
                            //txt_state.setText("NY");
                        } else {
                            ii.setSelected_parking(false);
                        }
                        state_array.add(ii);
                    }
                    Log.e("state_array ", " : " + state_array);

                    ShowStatePupup(getActivity(), state_array);
                }
            });

            if (renew.equals("yes")) {
                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                String platno2 = sh.getString("platno", "");
                String statename2 = sh.getString("state", "");
                String lotrow_ = sh.getString("lot_row", "a");
                String lot_no_ = sh.getString("lot_no", "1");
                Renew_date = d.getString("cureent_date", "0");
                ReNew_id = d.getString("renew_id", "0");
                marker = d.getString("marker", "0");
                Renew_parked_time = d.getString("parked_time", "0");
                Renew_is_valid = d.getString("car_is_valid", "no");
                editno.setText(platno2);
                String marker = d.getString("marker", "free");
                lost_no = lot_no_;
                lotrow_ = lotrow_;
                txt_row.setText(lotrow_ + " : " + lot_no_);
                if (marker.equals("managed")) {
                    //txt_parking_location_lable.setVisibility(View.VISIBLE);
                    rl_sp_row.setVisibility(View.VISIBLE);
                    //rl_sp_space.setVisibility(View.VISIBLE);
                }
                if (marker.contains("managed")) {
                    rl_sp_row.setVisibility(View.VISIBLE);
                    txt_row.setText(lot_no_ + " : " + lotrow_);
                } else {
                    rl_sp_row.setVisibility(View.GONE);
                }
                for (int i = 0; i < statearray.length; i++) {
                    String ss = statearray[i].toString();
                    if (ss.equals(statename2)) {
                        spsate.setSelection(i);
                        break;
                    }
                }


                txt_state.setText(statename2);
                state_array.clear();
                for (int i = 1; i < all_state.size(); i++) {
                    String statename = all_state.get(i).getState2Code();
                    item ii = new item();
                    ii.setState(statename);
                    if (statename.equals(statename2)) {
                        this.statename = statename;
                        ii.setSelected_parking(true);
                        txt_state.setText(statename2);
                    } else {
                        ii.setSelected_parking(false);
                    }
                    state_array.add(ii);
                }


                for (int k = 0; k < row.length; k++) {
                    if (lotrow_.equals("")) {
                        lotrow_ = "a";
                    }
                    String row1 = row[k].toString();
                    if (row1.equals(lotrow_.toUpperCase())) {
                        sp_row.setSelection(i);
                        break;
                    }
                }
                for (int j = 0; j < space.size(); j++) {
                    String row1 = space.get(j).toString();
                    if (row1 == lot_no_) {
                        sp_space.setSelection(i);
                        break;
                    }
                }
                SharedPreferences sh2 = getActivity().getSharedPreferences("renew", Context.MODE_PRIVATE);
                sh2.edit().clear().commit();
                SharedPreferences back_select2 = getActivity().getSharedPreferences("back_free_parking", Context.MODE_PRIVATE);
                back_select2.edit().clear().commit();

                show_marke_payment_button();
            } else {
                if (!user_id.equals("null")) {
                    SharedPreferences dd = getActivity().getSharedPreferences("select_vehicle", Context.MODE_PRIVATE);
                    String select_plate_no = dd.getString("plate_no", "");
                    String select_state = dd.getString("state", "");
                    p_country = dd.getString("p_country", "");
                    dd.edit().clear().commit();
                    if (parkhere.equals("yes")) {
                        SharedPreferences back_select2 = getActivity().getSharedPreferences("back_free_parking", Context.MODE_PRIVATE);
                        back_select2.edit().clear().commit();
                        // editno.setText(select_plate_no.toUpperCase());
                       /* for (int i = 0; i < statearray.length; i++) {
                            String state = statearray[i];
                            if (state.equals(select_state)) {
                                spsate.setSelection(i);
                                break;
                            }
                        }
                        state_array.clear();
                        for (int i = 1; i < statearray.length; i++) {
                            String statename = statearray[i].toString();
                            item ii = new item();
                            ii.setState(statearray[i].toString());
                            if (statename.equals(select_state)) {
                                ii.setSelected_parking(true);
                                this.statename=statename;
                                txt_state.setText(select_state);
                            } else {
                                ii.setSelected_parking(false);
                            }
                            state_array.add(ii);
                        }*/

                        rl_hours.setVisibility(View.GONE);

                    } else {
                        rl_hours.setVisibility(View.VISIBLE);
                        editno.setText(select_plate_no.toUpperCase());
                        for (int i = 0; i < statearray.length; i++) {
                            String state = statearray[i];
                            if (state.equals(select_state)) {
                                spsate.setSelection(i);
                                break;
                            }
                        }
                        state_array.clear();

                        for (int i = 1; i < all_state.size(); i++) {
                            String statename = all_state.get(i).getState2Code();
                            item ii = new item();
                            ii.setState(statename);
                            if (statename.equals(select_state)) {
                                this.statename = select_state;
                                ii.setSelected_parking(true);
                                txt_state.setText(select_state);
                            } else {
                                ii.setSelected_parking(false);
                            }
                            state_array.add(ii);
                        }

                    }

                    txt_plate_no.setVisibility(View.VISIBLE);
                    txt_state.setEnabled(true);
                    editno.setVisibility(View.VISIBLE);
                    // new getvehiclenumber().execute();
                } else {
                    txt_plate_no.setVisibility(View.VISIBLE);
                    txt_state.setEnabled(true);
                    editno.setVisibility(View.VISIBLE);
                    rl_hours.setVisibility(View.GONE);
                    SharedPreferences dd = getActivity().getSharedPreferences("select_vehicle", Context.MODE_PRIVATE);
                    String select_plate_no = dd.getString("plate_no", "");
                    String select_state = dd.getString("state", "");
                    p_country = dd.getString("p_country", "");
                    dd.edit().clear().apply();
                    editno.setText(select_plate_no.toUpperCase());
                    for (int i = 0; i < statearray.length; i++) {
                        String state = statearray[i];
                        if (state.equals(select_state)) {
                            spsate.setSelection(i);
                            break;
                        }
                    }

                    state_array.clear();
                   /* for (int i = 1; i < statearray.length; i++) {
                        String statename = statearray[i].toString();
                        item ii = new item();
                        ii.setState(statearray[i].toString());
                        if (statename.equals(select_state)) {
                            ii.setSelected_parking(true);
                            this.statename=statename;
                            txt_state.setText(select_state);
                        } else {
                            ii.setSelected_parking(false);
                        }
                        state_array.add(ii);
                    }*/
                    state_array.clear();
                    for (int i = 1; i < all_state.size(); i++) {
                        String statename = all_state.get(i).getState2Code();
                        item ii = new item();
                        ii.setState(statename);
                        if (statename.equals(select_state)) {
                            this.statename = select_state;
                            ii.setSelected_parking(true);
                            txt_state.setText(select_state);
                        } else {
                            ii.setSelected_parking(false);
                        }
                        state_array.add(ii);
                    }
                }

                p_country = p_country != null ? (!p_country.equals("null") ? (!p_country.equals("") ? p_country : "") : "") : "";
            }

            if (parkhere.equals("yes")) {
                SharedPreferences back_select2 = getActivity().getSharedPreferences("back_free_parking", Context.MODE_PRIVATE);
                back_select2.edit().clear().commit();
                rl_hours.setVisibility(View.GONE);
            } else {
                rl_hours.setVisibility(View.VISIBLE);
            }
            sp_row.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    lost_row = (String) sp_row.getSelectedItem();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
            sp_space.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    lost_no = (String) sp_space.getSelectedItem();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }

            });

            editno.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    show_marke_payment_button();
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });


            txtparknow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                    user_id = logindeatl.getString("id", "null");
                    platno = editno.getText().toString();
                    statename = txt_state.getText().toString();
                    boolean is_parked_time = true;
                    int remain_time = 0;
                    if (!platno.equals("") && !statename.equals("State")) {
                        if (rl_hours.getVisibility() == View.VISIBLE) {
                            if (!parkhere.equals("yes")) {
                                if (!shoues.equals(duration_unit + "s") && !shoues.equals("")) {
                                    if (!shoues.equals("Hours")) {
                                        min = shoues.substring(shoues.indexOf(' ') + 1);
                                        String hr = shoues.substring(0, shoues.indexOf(' '));
                                        Double d = new Double(Double.parseDouble(hr));
                                        hours = d.intValue();
                                    } else if (!shoues.equals("Months")) {
                                        min = shoues.substring(shoues.indexOf(' ') + 1);
                                        String hr = shoues.substring(0, shoues.indexOf(' '));
                                        Double d = new Double(Double.parseDouble(hr));
                                        hours = d.intValue();
                                    } else if (!shoues.equals("Days")) {
                                        min = shoues.substring(shoues.indexOf(' ') + 1);
                                        String hr = shoues.substring(0, shoues.indexOf(' '));
                                        Double d = new Double(Double.parseDouble(hr));
                                        hours = d.intValue();
                                    } else if (!shoues.equals("Weeks")) {
                                        min = shoues.substring(shoues.indexOf(' ') + 1);
                                        String hr = shoues.substring(0, shoues.indexOf(' '));
                                        Double d = new Double(Double.parseDouble(hr));
                                        hours = d.intValue();
                                    }

                                } else {
                                    TextView tvTitle = new TextView(getActivity());
                                    tvTitle.setText("Alert!!!");
                                    tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                                    tvTitle.setPadding(0, 10, 0, 0);
                                    tvTitle.setTextColor(Color.parseColor("#000000"));
                                    tvTitle.setTextSize(20);
                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                    alertDialog.setCustomTitle(tvTitle);
                                    alertDialog.setMessage("Select parking hours before proceeding.");
                                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });
                                    final AlertDialog alertd = alertDialog.create();
                                    alertd.show();
                                    TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                                    messageText.setGravity(Gravity.CENTER_HORIZONTAL);
                                    return;
                                }
                            }

                            Renew_parked_time = Renew_parked_time != null ? (!Renew_parked_time.equals("") ? (!Renew_parked_time.equals("null") ? Renew_parked_time : "") : "") : "";

                            if (!Renew_parked_time.equals("")) {
                                int parked_hours = Integer.parseInt(Renew_parked_time);
                                final_count = parked_hours + hours;
                                int max = Integer.parseInt(max_time);
                                remain_time = max - parked_hours;
                                if (final_count <= max) {
                                    is_parked_time = true;
                                } else {
                                    is_parked_time = false;
                                }
                            } else {
                                final_count = hours;
                            }
                        }

                        if (!is_parked_time) {
                            TextView tvTitle = new TextView(getActivity());
                            tvTitle.setText("ParkEZly");
                            tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                            tvTitle.setPadding(0, 10, 0, 0);
                            tvTitle.setTextColor(Color.parseColor("#000000"));
                            tvTitle.setTextSize(20);
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setCustomTitle(tvTitle);
                            alertDialog.setMessage(Html.fromHtml("Duration must be less than<b> maximum park " + max_time + " " + duration_unit + "s</b> and you already parked <b>" + Renew_parked_time + " " + duration_unit + "s</b> so you can park upto <b>" + remain_time + " " + duration_unit + "s</b> more or less!"));
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            final AlertDialog alertd = alertDialog.create();
                            alertd.show();
                            TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                            messageText.setGravity(Gravity.CENTER_HORIZONTAL);
                            return;
                        }
                        if (!user_id.equals("null")) {
                            if (marker.contains("Managed") || marker.contains("managed ")) {
                                parkingtype = "managed";
                            } else {
                                parkingtype = "free";
                            }
                        } else {
                            if (marker.contains("Managed") || marker.contains("managed")) {
                                parkingtype = "managed_guest";
                            } else {
                                parkingtype = "free";
                            }
                        }
                        if (rl_sp_row.getVisibility() == View.VISIBLE) {
                            if (township_sapce_marked.equals("false")) {
                                if (!lost_no.equals("SPACE #")) {
                                    //parkingtype = "guest";
                                    lost_row1 = "";
                                    lost_no1 = lost_no;
                                    check_data();
                                } else {
                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                    alertDialog.setTitle("Alert!!");
                                    alertDialog.setMessage("Please fill in all required fields.");
                                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });
                                    alertDialog.show();

                                }
                            } else if (total_row < 2) {

                                if (!lost_row.equals("ROW")) {
                                    //parkingtype = "guest";
                                    lost_row1 = lost_row;
                                    lost_no1 = "";
                                    check_data();
                                } else {
                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                    alertDialog.setTitle("Alert!!");
                                    alertDialog.setMessage("Please fill in all required fields.");
                                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });
                                    alertDialog.show();

                                }
                            } else {
                                if (!lost_no.equals("SPACE #") || !lost_row.equals("ROW")) {
                                    //parkingtype = "guest";
                                    lost_row1 = lost_row;
                                    lost_no1 = lost_no;
                                    check_data();
                                } else {
                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                    alertDialog.setTitle("Alert!!");
                                    alertDialog.setMessage("Please fill in all required fields.");
                                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });
                                    alertDialog.show();
                                }
                            }
                        } else {
                            check_data();
                        }
                    } else {
                        TextView tvTitle = new TextView(getActivity());
                        tvTitle.setText("Alert!!!");
                        tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                        tvTitle.setPadding(0, 10, 0, 0);
                        tvTitle.setTextColor(Color.parseColor("#000000"));
                        tvTitle.setTextSize(20);
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setCustomTitle(tvTitle);
                        alertDialog.setMessage("Fill the all fields, in order to Pay & Parking.");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                        final AlertDialog alertd = alertDialog.create();
                        alertd.show();
                        TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
                    }
                }
            });

            txt_plate_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShowState(getActivity());
                }
            });

            String user_id = logindeatl.getString("id", "");
            if (!user_id.equals("")) {
                if (!renew.equals("yes")) {
                    new getvehiclenumber().execute();
                }
            } else {
                showenter_vehicle(getActivity());
            }

        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }


        return view;
    }

    private void showDialogLocationLots() {
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        View dialogView = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_select_locatoin_lot, null);
        builder.setView(dialogView);
        dialogLocationLot = builder.create();
        dialogLocationLot.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        RelativeLayout rlClose = dialogView.findViewById(R.id.rlClose);
        RelativeLayout rlDone = dialogView.findViewById(R.id.rlDone);
        RelativeLayout rlSelectedLot = dialogView.findViewById(R.id.rlSelectedLot);
        RecyclerView rvLocationLot = dialogView.findViewById(R.id.rvLocationLot);
        TextView tvSelectedLot = dialogView.findViewById(R.id.tvSelectedLot);

        if (selectedLocationLot != null) {
            rlSelectedLot.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(selectedLocationLot.getLot_id())) {
                tvSelectedLot.setText(selectedLocationLot.getLot_id());
            }
        } else {
            rlSelectedLot.setVisibility(View.GONE);
        }

        LocationLotAdapter locationLotAdapter = new LocationLotAdapter();
        rvLocationLot.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        rvLocationLot.setAdapter(locationLotAdapter);

        rlDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLocationLot.dismiss();
            }
        });

        rlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLocationLot.dismiss();
            }
        });

        dialogLocationLot.show();
    }

    public void check_data() {
        if (!user_id.equals("null")) {
            if (renew.equals("yes")) {
                if (Renew_is_valid.equals("yes")) {
                    new Update_Parking().execute();
                } else {
                    new exitsparking(pid).execute();
                }
            } else {
                new checkcardparkedornot().execute();
            }

        } else {
            showrandomnumber(getActivity());
        }
    }

    public void currentlocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
            new fetchLatLongFromaddress(String.valueOf(latitude), String.valueOf(longitude)).execute();

            Intent intent = new Intent(getActivity(), GeocodeAddressIntentService.class);
            intent.putExtra(Constants.RECEIVER, mResultReceiver);
            intent.putExtra(Constants.FETCH_TYPE_EXTRA, Constants.USE_ADDRESS_LOCATION);
            intent.putExtra(Constants.LOCATION_LATITUDE_DATA_EXTRA, latitude);
            intent.putExtra(Constants.LOCATION_LONGITUDE_DATA_EXTRA, longitude);
            getActivity().startService(intent);
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Failed to fetch user location!");
            alertDialog.setMessage("Please enable user location for app, location is required for app to work properly");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            alertDialog.show();
        }
    }

    private String getaddress1(Double lat, Double lon) {
        Geocoder gc = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = gc.getFromLocation(lat, lon, 1);

            if (addresses.size() > 0) {
                adrreslocation = addresses.get(0).getAddressLine(0);
                city = addresses.get(0).getLocality();
                state = addresses.get(0).getAdminArea();
                statename = state;
                countryname = addresses.get(0).getCountryName();
                return adrreslocation;
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Address not found!");
                alertDialog.setMessage("Could not find location. Try including the exact location information");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new fetchLatLongFromaddress(String.valueOf(latitude), String.valueOf(longitude)).execute();
                    }
                });
                alertDialog.show();
                return null;
            }
        } catch (Exception e) {
            new fetchLatLongFromaddress(String.valueOf(latitude), String.valueOf(longitude)).execute();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void Onitem_save() {
        getActivity().onBackPressed();
        new getvehiclenumber().execute();
    }

    @Override
    public void Onitem_delete() {
        getActivity().onBackPressed();
        new getvehiclenumber().execute();
    }

    public class fetchLatLongFromaddress extends AsyncTask<Void, Void, StringBuilder> {

        String lat1, lang1;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        public fetchLatLongFromaddress(String lat, String lang) {
            super();
            this.lat1 = lat;
            this.lang1 = lang;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {

                //  new getsearchotherlocation().execute();
                //
                // ();

                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat1 + "," + lang1 + "&sensor=true";

                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
                rl_progressbar.setVisibility(View.GONE);

                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");

                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray
                            .getJSONObject(0);
                    adrreslocation = before_geometry_jsonObj.getString("formatted_address");
                    JSONObject geometry_jsonObj = before_geometry_jsonObj
                            .getJSONObject("geometry");

                    JSONObject location_jsonObj = geometry_jsonObj
                            .getJSONObject("location");

                    lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);

                    lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);
                } else {
                    getaddress1(latitude, longitude);
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void showrandomnumber(Activity context) {

        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.randomnumber, viewGroup, false);
        popup = new PopupWindow(context);
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(true);
        popup.setFocusable(false);
        popup.setAnimationStyle(R.style.animationName);
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

        final EditText edit_code;
        Button btn_cancel, btn_park_now;

        edit_code = (EditText) layout.findViewById(R.id.edit_randomno);
        btn_cancel = (Button) layout.findViewById(R.id.btn_cancel);
        btn_park_now = (Button) layout.findViewById(R.id.btn_popup_park_now);
        int randomPIN = (int) (Math.random() * 9000) + 1000;
        String val = "" + randomPIN;
        edit_code.append(val);
        btn_park_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String editno = edit_code.getText().toString();

                if (!edit_code.equals("")) {
                    if (rl_sp_row.getVisibility() == View.VISIBLE) {
                        //parkingtype = "managed";
                        lost_row1 = lost_row;
                        lost_no1 = lost_no;
                    } else {
                        //parkingtype = "free";
                        lost_no1 = "";
                        lost_row1 = "";
                    }
                    popup.dismiss();
                    token = Integer.parseInt(editno);
                    token1 = editno;
                    if (renew.equals("yes")) {
                        if (Renew_is_valid.equals("yes")) {
                            new Update_Parking().execute();
                        } else {
                            new exitsparking(pid).execute();
                        }
                    } else {
                        new checkcardparkedornot().execute();
                    }
                } else {
                    popup.dismiss();
                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    public float getdistances(String address, String address1) {
        try {
            Location loc1 = new Location(address1);
            loc1.setLatitude(latitude);
            loc1.setLongitude(longitude);

            Location loc2 = new Location(address);
            loc2.setLatitude(Double.parseDouble(lat));
            loc2.setLongitude(Double.parseDouble(lang));
            float distanceInMeters = loc1.distanceTo(loc2);
            return distanceInMeters;
        } catch (Exception e) {
            return 0;
        }
    }

    public class checkcardparkedornot extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        String pid;
        JSONArray array = new JSONArray();

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String vechiclenourl = null;
            try {
                String plate = platno;
                plate = plate.replaceAll("\\s+", "");
                vechiclenourl = "_table/parked_cars?filter=(plate_no%3D" + URLEncoder.encode(platno, "utf-8") + ")%20AND%20(pl_state%3D" + statename + ")%20AND%20(parking_status%3D'ENTRY')";
            } catch (Exception e) {
                e.printStackTrace();
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + vechiclenourl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        pid = c.getString("id");
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (!array.isNull(0)) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Vehicle already parked!");
                    alertDialog.setMessage("This Vehicle is already parked, Do you want to exit it from previous parking and park here ?");
                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            new exitsparking(pid).execute();
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                } else {
                    if (!id.equals("null")) {
                        if (parkhere.equals("yes")) {
                            new finalupdate().execute();
                        } else {
                            new finalupdate().execute();
                        }
                    } else {
                        new finalupdate().execute();
                    }
                }

            }
            super.onPostExecute(s);
        }


    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("ip", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("aax", ex.toString());
        }
        return null;
    }

    public class finalupdate extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = "null";
        JSONObject json, json1;
        String pov2 = "_table/parked_cars";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            TimeZone zone12 = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone12);
            entry_time = sdf.format(new Date());
            expiretime = "";
            if (parkhere.equals("yes")) {
                location_id = "0";
            } else {

                final String currentDateandTime = entry_time;
                Date date11 = null;
                try {
                    date11 = sdf.parse(currentDateandTime);
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                final Calendar calendar = Calendar.getInstance();
                calendar.setTime(date11);
                shoues = shoues != null ? (!shoues.equals("null") ? (!shoues.equals("") ? shoues : "null") : "null") : "null";
                if (!shoues.equals("null")) {
                    if (min.equals("Hrs") || min.equals("Hr")) {
                        calendar.add(Calendar.HOUR, hours);
                    } else if (min.equals("Months") || min.equals("Month")) {
                        calendar.add(Calendar.MONTH, hours);
                        calendar.add(Calendar.DATE, -1);
                    } else if (min.equals("Days") || min.equals("Day")) {
                        calendar.add(Calendar.DATE, hours);
                    } else if (min.equals("Weeks") || min.equals("Week")) {
                        calendar.add(Calendar.DAY_OF_WEEK_IN_MONTH, hours);
                    } else {
                        calendar.add(Calendar.MINUTE, hours);
                    }
                } else {

                    hours = Integer.parseInt(max_time);
                    if (duration_unit.equals("Hour")) {
                        min = "Hrs";
                        calendar.add(Calendar.HOUR, Integer.parseInt(max_time));
                    } else if (duration_unit.equals("Month")) {
                        min = "Months";
                        calendar.add(Calendar.MONTH, Integer.parseInt(max_time));
                        calendar.add(Calendar.DATE, -1);
                    } else if (duration_unit.equals("Day")) {
                        min = "Days";
                        calendar.add(Calendar.DATE, Integer.parseInt(max_time));
                    } else if (duration_unit.equals("Week")) {
                        min = "Weeks";
                        calendar.add(Calendar.DAY_OF_WEEK_IN_MONTH, Integer.parseInt(max_time));
                    } else {
                        min = "Minute";
                        calendar.add(Calendar.MINUTE, Integer.parseInt(max_time));
                    }

                }

                String expridate = sdf.format(calendar.getTime());
                String inputPattern = "yyyy-MM-dd HH:mm:ss";
                SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.US);
                SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                outputFormat.setTimeZone(zone12);
                inputFormat.setTimeZone(zone12);

                Date date = null;
                Date Current = null;
                String final_current_date = null;
                try {
                    date = inputFormat.parse(expridate);
                    Current = inputFormat.parse(entry_time);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                expiretime = outputFormat.format(date);
                final_current_date = outputFormat.format(Current);
            }

          /*  SimpleDateFormat sdf11 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone zone1 = TimeZone.getTimeZone("UTC");
            sdf11.setTimeZone(zone1);
            entry_time = sdf11.format(new Date());*/
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            int us_id = 0;
            if (!user_id.equals("null")) {
                us_id = Integer.parseInt(user_id);
            } else {
                us_id = 0;
            }
            if (parkhere.equals("yes")) {
                parkingtype = "anywhere";
                jsonValues.put("parking_type", "anywhere");
            }

            String plate = platno;
            plate = plate.replaceAll("\\s+", "");
            jsonValues.put("parking_type", parkingtype);
            jsonValues.put("township_code", township_code);
            jsonValues.put("location_id", location_id);
            jsonValues.put("location_code", location_code);
            jsonValues.put("entry_date_time", entry_time);
            jsonValues.put("exit_date_time", "");
            jsonValues.put("expiry_time", expiretime);
            jsonValues.put("max_duration", max_time);
            jsonValues.put("user_id", us_id);
            jsonValues.put("permit_id", 0);
            jsonValues.put("subscription_id", 0);
            jsonValues.put("plate_no", plate);
            jsonValues.put("pl_state", statename);
            jsonValues.put("lat", latitude);
            jsonValues.put("lng", longitude);
            jsonValues.put("address1", adrreslocation);
            jsonValues.put("address2", "");
            jsonValues.put("city", location_name + lost_row1 + lost_no1);
            jsonValues.put("state", statename);
            jsonValues.put("zip", zip_code);
            jsonValues.put("country", "USA");
            if (selectedLocationLot != null) {
                if (!TextUtils.isEmpty(selectedLocationLot.getLot_id())) {
                    jsonValues.put("lot_id", selectedLocationLot.getLot_id());
                }
                if (!TextUtils.isEmpty(selectedLocationLot.getLot_number())) {
                    jsonValues.put("lot_number", selectedLocationLot.getLot_number());
                }
                if (!TextUtils.isEmpty(selectedLocationLot.getLot_row())) {
                    jsonValues.put("lot_row", selectedLocationLot.getLot_row());
                }
                jsonValues.put("location_lot_id", selectedLocationLot.getId());
            }
            jsonValues.put("ip", getLocalIpAddress());
            jsonValues.put("parking_token", token);
            jsonValues.put("parking_status", "ENTRY");
            jsonValues.put("payment_method", "");
            jsonValues.put("parking_rate", 0);
            jsonValues.put("parking_units", "ENTRY");
            jsonValues.put("parking_qty", 0);
            jsonValues.put("wallet_trx_id", 0);
            jsonValues.put("tr_percent", 0);
            jsonValues.put("tr_fee", 0);
            jsonValues.put("parking_total", 0);
            jsonValues.put("ipn_custom", "");
            jsonValues.put("ipn_txn_id", "");
            jsonValues.put("ipn_payment", "");
            jsonValues.put("ipn_status", "");
            jsonValues.put("ipn_address", "");
            jsonValues.put("distance_to_marker", getdistances(locationaddress, adrreslocation));
            jsonValues.put("marker_address1", locationaddress);
            jsonValues.put("marker_address2", "");
            jsonValues.put("marker_city", city);
            jsonValues.put("marker_state", state);
            jsonValues.put("marker_zip", "");
            jsonValues.put("marker_country", "USA");
            jsonValues.put("token", 0);
            jsonValues.put("duration_unit", duration_unit);
            jsonValues.put("unit_pricing", "0");
            jsonValues.put("selected_duration", String.valueOf(hours));
            String mismatch = "0";
            float dis = getdistances(locationaddress, adrreslocation);
            float ff = dis / 1609;
            if (ff > 0.5) {
                mismatch = "1";
            }
            jsonValues.put("mismatch", mismatch);
            jsonValues.put("marker_lng", lang);
            jsonValues.put("parking_subtotal", 0);
            // jsonValues.put("userName", "");
            jsonValues.put("marker_lat", lat);
            jsonValues.put("ticket_status", "");
            jsonValues.put("date_time", "");
            jsonValues.put("pl_country", p_country);
            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + pov2;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
//this is your response:
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            if (getActivity() != null && json1 != null) {
                if (!id.equals("null")) {
                    ArrayList<item> arraylist;
                    SharedPreferences sharedPrefs = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
                    Gson gson = new Gson();
                    String json = sharedPrefs.getString("car", null);
                    Type type = new TypeToken<ArrayList<item>>() {
                    }.getType();
                    arraylist = gson.fromJson(json, type);
                    if (arraylist == null) {
                        arraylist = new ArrayList<>();
                    }

                    item ii = new item();
                    ii.setPlate_no(platno);
                    ii.setId(id);
                    arraylist.add(ii);

                    SharedPreferences sharedPrefs1 = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPrefs1.edit();
                    Gson gson1 = new Gson();
                    String json1 = gson1.toJson(arraylist);
                    editor.putString("car", json1);
                    editor.commit();
                    if (rl_sp_row.getVisibility() == View.VISIBLE) {
                        new updatemanaledlost(id).execute();
                    } else {
                        new getparked_car(id).execute();
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class exitsparking extends AsyncTask<String, String, String> {
        String exit_status = "_table/parked_cars";
        JSONObject json, json1;
        String re_id = "null";
        String parked_id;

        public exitsparking(String pid) {
            this.parked_id = pid;
        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            currentdate = sdf.format(new Date());
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", parked_id);
            jsonValues.put("parking_status", "EXIT");
            jsonValues.put("exit_date_time", currentdate);
            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.d("re_id", re_id);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);

            if (getActivity() != null && json1 != null) {
                if (!re_id.equals("null")) {
                    canclealarm(Integer.parseInt(parked_id));
                    SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                    String id1 = logindeatl.getString("id", "null");
                    if (id1.equals("null")) {
                        ArrayList<item> arraylist = new ArrayList<>();
                        SharedPreferences sharedPrefs = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
                        Gson gson = new Gson();
                        String json = sharedPrefs.getString("car", null);
                        Type type = new TypeToken<ArrayList<item>>() {
                        }.getType();
                        arraylist = gson.fromJson(json, type);
                        if (arraylist == null) {
                            arraylist = new ArrayList<>();
                        }
                        if (arraylist.size() > 0) {
                            for (int j = 0; j < arraylist.size(); j++) {
                                String id = arraylist.get(j).getId();
                                if (id.equals(re_id)) {
                                    arraylist.remove(j);
                                }
                            }
                        }
                        SharedPreferences sharedPrefs1 = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPrefs1.edit();
                        Gson gson1 = new Gson();
                        String json1 = gson1.toJson(arraylist);
                        editor.putString("car", json1);
                        editor.commit();
                    }
                    new finalupdate().execute();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Something went wrong");
                    alertDialog.setMessage("Please try again");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
            super.onPostExecute(s);
        }
    }

    public class getparked_car extends AsyncTask<String, String, String> {
        String park_id;
        JSONObject json;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String login_id = logindeatl.getString("id", "0");
        String transactionurl = "_table/parked_cars?filter=(user_id%3D" + login_id + ")%20AND%20(parking_status%3D'ENTRY')";
        String id = "";

        public getparked_car(String id) {
            this.park_id = id;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                        String plnp = c.getString("plate_no");
                        String state = c.getString("pl_state");
                        String state1 = c.getString("state");
                        if (!park_id.equals("null")) {
                            if (id.equals(park_id)) {

                                String city = c.getString("city");
                                String country = c.getString("country");
                                String exit = c.getString("exit_date_time");

                                SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
                                SharedPreferences.Editor ed = time.edit();
                                ed.clear();
                                ed.commit();
                                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sh.edit();
                                editor.putString("platno", platno);
                                editor.putString("state", statename);
                                editor.putString("currentdate", entry_time);
                                editor.putString("hr", String.valueOf(hours));
                                editor.putString("max", max_time);
                                editor.putString("id", id);
                                editor.putString("min", min);
                                editor.putString("token", token1);
                                editor.putString("exip", "");
                                editor.putString("lat", String.valueOf(latitude));
                                editor.putString("markertye", marker);
                                editor.putString("log", String.valueOf(longitude));
                                editor.putString("country", country);
                                editor.putString("city", city);
                                editor.putString("parking_type", parkingtype);
                                editor.putString("exit_date_time", exit);
                                editor.putString("address", locationaddress);
                                editor.putString("parked_address", adrreslocation);
                                editor.putString("locationname", location_code);
                                editor.putString("lot_row", lost_row1);
                                editor.putString("lot_no", lost_no1);
                                editor.putString("title", title);
                                editor.putString("parking_free", "yes");
                                editor.putString("entryTime", c.getString("entry_date_time"));
                                editor.putString("exitTime", c.getString("exit_date_time"));
                                editor.putString("locationName", c.getString("location_name"));
                                editor.putString("locationCode", c.getString("location_code"));
                                if (parkhere.equals("yes")) {
                                    editor.putString("parkhere", "yes");
                                } else {
                                    editor.putString("parkhere", "no");
                                }
                                editor.commit();

                                SharedPreferences sh1 = getActivity().getSharedPreferences("token", Context.MODE_PRIVATE);
                                SharedPreferences.Editor ed1 = sh1.edit();
                                ed1.putString("token", token1);
                                ed1.commit();

                                break;
                            }
                        } else {
                            if (state.equals("null")) {
                                state = state1;
                            }
                            if (plnp.equals(platno) && state.equals(statename)) {
                                String city = c.getString("city");
                                String country = c.getString("country");
                                String exit = c.getString("exit_date_time");
                                String extime = c.getString("expiry_time");
                                String max = c.getString("max_duration");
                                String addres = c.getString("address1");
                                String parked_adress = c.getString("marker_address1");
                                String locationcode = c.getString("location_code");
                                String entrytime = c.getString("entry_date_time");
                                String lat = c.getString("lat");
                                String lang = c.getString("lng");
                                String lot = c.getString("lot_number");
                                String lot_row = c.getString("lot_row");
                                String parking_type = c.getString("parking_type");
                                String duration_unit = c.getString("duration_unit");
                                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sh.edit();
                                editor.putString("platno", platno);
                                editor.putString("state", statename);
                                editor.putString("currentdate", entrytime);
                                editor.putString("exip", extime);
                                editor.putString("hr", "0");
                                editor.putString("max", max);
                                editor.putString("lat", lat);
                                editor.putString("log", lang);
                                editor.putString("min", duration_unit);
                                editor.putString("id", id);
                                editor.putString("token", "");
                                editor.putString("country", country);
                                editor.putString("city", city);
                                editor.putString("exit_date_time", exit);
                                editor.putString("address", addres);
                                editor.putString("parked_address", parked_adress);
                                editor.putString("locationname", locationcode);
                                editor.putString("markertye", marker);
                                editor.putString("lot_row", lot_row);
                                editor.putString("lot_no", lot);
                                editor.putString("title", title);
                                editor.putString("parking_type", parking_type);
                                editor.putString("parkhere", "no");
                                editor.putString("entryTime", c.getString("entry_date_time"));
                                editor.putString("exitTime", c.getString("exit_date_time"));
                                editor.putString("locationName", c.getString("location_name"));
                                editor.putString("locationCode", c.getString("location_code"));
                                editor.putString("lotId", c.getString("lot_id"));
                                editor.commit();
                            }
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json != null) {
                if (!id.equals("null") || id != null) {
                    SharedPreferences backtotimer = getActivity().getSharedPreferences("back_parkezly", Context.MODE_PRIVATE);
                    backtotimer.edit().clear().commit();
                    SharedPreferences backtotimer1 = getActivity().getSharedPreferences("back_parking_info", Context.MODE_PRIVATE);
                    backtotimer1.edit().clear().commit();
                    SharedPreferences sdg = getActivity().getSharedPreferences("back_free_parking", Context.MODE_PRIVATE);
                    sdg.edit().clear().commit();
                    if (!login_id.equals("0")) {
                        if (!park_id.equals("null")) {
                            /*final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.replace(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                            ft.commit();*/

                            try {
                                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                timervehicleinfo pay = new timervehicleinfo();
                                //fragmentStack.lastElement().onPause();
                                ft.add(R.id.My_Container_1_ID, pay, "timer");
                                //fragmentStack.lastElement().onPause();
                                //ft.remove(fragmentStack.pop());
                                if (!parkhere.equals("yes")) {
                                    //fragmentStack.lastElement().onPause();
                                    //ft.remove(fragmentStack.pop());
                                }
                                fragmentStack.push(pay);
                                ft.commitAllowingStateLoss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
                            SharedPreferences.Editor ed = time.edit();
                            ed.putString("result", "true");
                            ed.commit();
                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            timervehicleinfo pay = new timervehicleinfo();
                            fragmentStack.lastElement().onPause();
                            ft.add(R.id.My_Container_1_ID, pay, "timer");
                            fragmentStack.lastElement().onPause();
                            ft.remove(fragmentStack.pop());
                            if (!parkhere.equals("yes")) {
                                fragmentStack.lastElement().onPause();
                                ft.remove(fragmentStack.pop());
                            }
                            fragmentStack.push(pay);
                            ft.commitAllowingStateLoss();


                        }
                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Parking Notifications");
                        alertDialog.setMessage("Do you want to register for parking notifications");
                        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ((vchome) getActivity()).open_login_screen();
                            }
                        });
                        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (!park_id.equals("null")) {
                                    /*final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                    ft.replace(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                                    ft.commitAllowingStateLoss();*/

                                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    timervehicleinfo pay = new timervehicleinfo();
                                    ft.add(R.id.My_Container_1_ID, pay, "timer");
                                    if (!parkhere.equals("yes")) {
                                        fragmentStack.lastElement().onPause();
                                        ft.remove(fragmentStack.pop());
                                        fragmentStack.lastElement().onPause();
                                        ft.remove(fragmentStack.pop());
                                    }
                                    fragmentStack.push(pay);
                                    ft.commitAllowingStateLoss();
                                } else {
                                    SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor ed = time.edit();
                                    ed.putString("result", "true");
                                    ed.commit();
                                  /*  final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                    ft.add(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                                    ft.commitAllowingStateLoss();*/

                                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    timervehicleinfo pay = new timervehicleinfo();
                                    fragmentStack.lastElement().onPause();
                                    ft.add(R.id.My_Container_1_ID, pay, "timer");
                                    fragmentStack.lastElement().onPause();
                                    ft.remove(fragmentStack.pop());
                                    if (!parkhere.equals("yes")) {
                                        fragmentStack.lastElement().onPause();
                                        ft.remove(fragmentStack.pop());
                                    }
                                    fragmentStack.push(pay);
                                    ft.commitAllowingStateLoss();

                                }
                            }
                        });
                        alertDialog.show();
                    }
                }
            }
            super.onPostExecute(s);

        }
    }

    public class updatemanaledlost extends AsyncTask<String, String, String> {
        String exit_status = "_table/location_lot";
        JSONObject json, json1;
        String re_id;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id;

        public updatemanaledlost(String id) {
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String entry_time = sdf.format(new Date());
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("lot_row", lost_row1);
            jsonValues.put("lot_number", lost_no1);
            jsonValues.put("occupied", "YES");
            jsonValues.put("location_code", location_code);
            jsonValues.put("location_name", location_name);
            jsonValues.put("plate_no", platno);
            jsonValues.put("plate_state", statename);
            jsonValues.put("date_time", entry_time);
            jsonValues.put("entry_date_time", entry_time);
            jsonValues.put("entry_date_time", expiretime);
            jsonValues.put("expiry_time", expiretime);
            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            Log.e("managed_url", url);
            Log.e("parama", String.valueOf(json));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.d("re_id", re_id);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);

            if (getActivity() != null && json1 != null) {
                if (!re_id.equals("null")) {
                    new getparked_car(id).execute();
                }
                super.onPostExecute(s);
            }
        }
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    public void canclealarm(int id) {
        Intent notificationIntent = new Intent(getActivity(), NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, id);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(getActivity(), id + 1, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent alarmIntent1 = PendingIntent.getBroadcast(getActivity(), id + 2, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent alarmIntent3 = PendingIntent.getBroadcast(getActivity(), id + 3, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
        alarmManager.cancel(alarmIntent);
        alarmIntent.cancel();
        alarmManager.cancel(alarmIntent1);
        alarmIntent1.cancel();
        alarmManager.cancel(alarmIntent3);
        alarmIntent3.cancel();
    }

    private void ShowStatePupup(Activity context, final ArrayList<item> data) {
        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.statepopup_pre, viewGroup, false);
        final PopupWindow popupmanaged = new PopupWindow(context);
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        //  popupmanaged.setAnimationStyle(R.style.animationName);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);
        Button btnparkhere;
        ImageView imageclose;
        GridView grid_managed;
        RelativeLayout rl_popclose;
        ImageView img_state;
        TextView txt_state_;

        btnparkhere = (Button) layout.findViewById(R.id.btn_managed_park);
        grid_managed = (GridView) layout.findViewById(R.id.grid_managed);
        imageclose = (ImageView) layout.findViewById(R.id.img_managed_close);
        RelativeLayout rl_done = (RelativeLayout) layout.findViewById(R.id.rl_done);
        rl_popclose = (RelativeLayout) layout.findViewById(R.id.rl_pop);
        rl_popclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });
        img_state = (ImageView) layout.findViewById(R.id.img_state);
        txt_state_ = (TextView) layout.findViewById(R.id.txt_state_name);

        Resources rs;
        final state_adapter adpter = new state_adapter(getActivity(), data, rs = getResources());
        grid_managed.setAdapter(adpter);

        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  for(int i=0;i<data.size();i++){
                    data.get(i).setSelected_parking(false);
                }*/
                //adpter.notifyDataSetChanged();
                popupmanaged.dismiss();
                // txt_state.setText("State");

            }
        });

        for (int k = 0; k < data.size(); k++) {
            boolean selected_ore = data.get(k).isSelected_parking();

            if (selected_ore) {
                statename = data.get(k).getState();
                img_state.setImageResource(R.mipmap.new_state_icon);
                txt_state_.setText(statename);
                break;
            }
        }

        rl_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();

                for (int k = 0; k < data.size(); k++) {
                    boolean selected_ore = data.get(k).isSelected_parking();

                    if (selected_ore) {
                        statename = data.get(k).getState();
                        txt_state.setText(statename);
                        break;
                    }
                }
                if (!parkhere.equals("yes")) {
                    ShowHours(getActivity(), stateq);
                }
                show_marke_payment_button();
                adpter.notifyDataSetChanged();

            }
        });
        grid_managed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < data.size(); i++) {
                    data.get(i).setSelected_parking(false);
                }
                data.get(position).setSelected_parking(true);
                statename = data.get(position).getState();
                txt_state.setText(statename);
                adpter.notifyDataSetChanged();
                popupmanaged.dismiss();
                if (!parkhere.equals("yes")) {
                    ShowHours(getActivity(), stateq);
                }
                show_marke_payment_button();
            }
        });

    }

    public class state_adapter extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        boolean isselect = false;
        int a = 0;
        private Activity activity;
        private ArrayList<item> data;
        private SparseBooleanArray mCheckStates;
        private LayoutInflater inflater = null;

        public state_adapter(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        /******
         * Depends upon data size called for each row , Create each ListView row
         *****/
        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;


            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_state, null);
                holder = new ViewHolder();

                holder.txt_state = (TextView) vi.findViewById(R.id.txt_state);
                holder.img = (ImageView) vi.findViewById(R.id.img_state);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {

            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String cat = tempValues.getState();
                if (cat == null) {
                } else if (cat != null) {

                    if (tempValues.isSelected_parking()) {
                        holder.img.setImageResource(R.mipmap.new_state_icon);
                        holder.txt_state.setTextColor(getResources().getColor(R.color.selected_text));
                    } else {
                        holder.img.setImageResource(R.mipmap.icon_new_state_selected);
                        holder.txt_state.setTextColor(Color.parseColor("#000000"));
                    }

                    holder.txt_state.setText(tempValues.getState());
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txt_state;
            ImageView img;
        }
    }

    private void ShowHours(Activity context, final ArrayList<item> data1) {

        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.hourpopup_p, viewGroup, false);
        final PopupWindow popupmanaged = new PopupWindow(context);
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        //  popupmanaged.setAnimationStyle(R.style.animationName);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);
        Button btnparkhere;
        ImageView imageclose;
        GridView grid_managed;
        RelativeLayout rl_popclose;
        ImageView img_state;
        TextView txt_state_;
        btnparkhere = (Button) layout.findViewById(R.id.btn_managed_park);
        grid_managed = (GridView) layout.findViewById(R.id.grid_managed);
        imageclose = (ImageView) layout.findViewById(R.id.img_managed_close);
        RelativeLayout rl_done = (RelativeLayout) layout.findViewById(R.id.rl_done);
        rl_popclose = (RelativeLayout) layout.findViewById(R.id.rl_pop);
        rl_popclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });
        img_state = (ImageView) layout.findViewById(R.id.img_state);
        txt_state_ = (TextView) layout.findViewById(R.id.txt_state_name);

        Resources rs;
        final hour_adapter adpter = new hour_adapter(getActivity(), data1, rs = getResources());
        grid_managed.setAdapter(adpter);

        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  for(int i=0;i<data.size();i++){
                    data.get(i).setSelected_parking(false);
                }
                adpter.notifyDataSetChanged();*/
                popupmanaged.dismiss();
                // txt_hours.setText("Select Hour");

            }
        });

        for (int k = 0; k < data1.size(); k++) {
            boolean selected_ore = data1.get(k).isSelected_parking();

            if (selected_ore) {
                shoues = data1.get(k).getState();
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    txt_state_.setText(hr + " Hrs");
                } else {
                    txt_state_.setText(shoues);
                }
                img_state.setImageResource(R.mipmap.new_cook_hr);
                break;
            }
        }

        rl_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();

                for (int k = 0; k < data1.size(); k++) {
                    boolean selected_ore = data1.get(k).isSelected_parking();

                    if (selected_ore) {
                        shoues = data1.get(k).getState();
                        if (shoues.contains("Hour")) {
                            String hr = shoues.substring(0, shoues.indexOf(' '));
                            txt_hours.setText(hr + " Hrs");
                        } else {
                            txt_hours.setText(shoues);
                        }
                        break;
                    }
                }

                show_marke_payment_button();

            }
        });
        grid_managed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                for (int i = 0; i < data1.size(); i++) {
                    data1.get(i).setSelected_parking(false);
                }
                data1.get(position).setSelected_parking(true);
                shoues = data1.get(position).getState();
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    txt_hours.setText(hr + " Hrs");
                } else {
                    txt_hours.setText(shoues);
                }
                adpter.notifyDataSetChanged();
                popupmanaged.dismiss();
                if (rl_sp_row.getVisibility() == View.VISIBLE) {
                    Showmanaedpopup(getActivity(), data);
                    show_marke_payment_button();
                } else {
                    show_marke_payment_button();
                }
            }
        });


    }

    public class hour_adapter extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        boolean isselect = false;
        int a = 0;
        private Activity activity;
        private ArrayList<item> data;
        private SparseBooleanArray mCheckStates;
        private LayoutInflater inflater = null;

        /*************
         * CustomAdapter Constructor
         *****************/
        public hour_adapter(Activity a, ArrayList<item> d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;
            data = d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        /********
         * What is the size of Passed Arraylist Size
         ************/
        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        /******
         * Depends upon data size called for each row , Create each ListView row
         *****/
        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;


            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_state, null);
                holder = new ViewHolder();

                holder.txt_state = (TextView) vi.findViewById(R.id.txt_state);
                holder.img = (ImageView) vi.findViewById(R.id.img_state);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {

            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String cat = tempValues.getState();
                if (cat == null) {
                } else if (cat != null) {

                    if (tempValues.isSelected_parking()) {
                        holder.img.setImageResource(R.mipmap.new_cook_hr);
                        holder.txt_state.setTextColor(getResources().getColor(R.color.selected_text));
                    } else {
                        holder.img.setImageResource(R.mipmap.icon_cook_un);
                        holder.txt_state.setTextColor(Color.parseColor("#000000"));
                    }

                    holder.txt_state.setText(tempValues.getState());
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }


        public class ViewHolder {

            public TextView txt_state;
            ImageView img;
        }
    }

    public class managedlostpop extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        boolean isselect = false;
        int a = 0;
        private Activity activity;
        private ArrayList<item> data;
        private SparseBooleanArray mCheckStates;
        private LayoutInflater inflater = null;

        /*************
         * CustomAdapter Constructor
         *****************/
        public managedlostpop(Activity a, ArrayList<item> d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;
            data = d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        /********
         * What is the size of Passed Arraylist Size
         ************/
        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        /******
         * Depends upon data size called for each row , Create each ListView row
         *****/
        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;


            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapatermanaed, null);
                holder = new ViewHolder();

                holder.txtprices = (TextView) vi.findViewById(R.id.txt_row_no1);
                holder.img = (ImageView) vi.findViewById(R.id.img_managed_car);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {

            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String cat = tempValues.getRow_no();
                if (cat == null) {
                } else if (cat != null) {

                    if (tempValues.getSpaces_mark_reqd().equals("false")) {
                        holder.txtprices.setText("R: " + tempValues.getDisplay());
                    } else if (tempValues.getMenu_img_id() < 2) {
                        holder.txtprices.setText("S: " + tempValues.getDisplay());
                    } else {
                        holder.txtprices.setText(tempValues.getDisplay());
                    }
                    if (tempValues.getIsparked().equals("t")) {
                        if (tempValues.getMarker().equals("red")) {
                            holder.img.setBackgroundResource(R.mipmap.popup_car_red);
                        } else if (tempValues.getMarker().equals("green")) {
                            holder.img.setBackgroundResource(R.mipmap.popup_car_green);
                        }
                    } else {

                        if (tempValues.isSelected_parking()) {
                            holder.img.setBackgroundResource(R.mipmap.popup_car_green);
                        } else {
                            holder.img.setBackgroundResource(R.mipmap.popup_gree_car);
                        }


                    }


                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }


        public class ViewHolder {

            public TextView txtparkinname, txtprices, txtoccie;
            ImageView img;
        }
    }

    private void Showmanaedpopup(Activity context, final ArrayList<item> data) {

        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.managedcarpopup_pre, viewGroup, false);
        final PopupWindow popupmanaged = new PopupWindow(context);
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        //  popupmanaged.setAnimationStyle(R.style.animationName);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int yyy = rl_main.getHeight();
            int yyy1 = rl_title.getHeight();
            int final_position = yyy1 + yyy + 245;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                popupmanaged.showAtLocation(layout, Gravity.NO_GRAVITY, 0, final_position);
            } else {
                popupmanaged.showAsDropDown(rl_main, 0, 0);
            }


        } else {
            popupmanaged.showAsDropDown(rl_main, 0, 0);
        }
        Button btnparkhere;
        ImageView imageclose;
        GridView grid_managed;
        RelativeLayout rl_popclose;
        final ImageView img_car;
        final TextView txt_car;
        btnparkhere = (Button) layout.findViewById(R.id.btn_managed_park);
        grid_managed = (GridView) layout.findViewById(R.id.grid_managed);
        imageclose = (ImageView) layout.findViewById(R.id.img_managed_close);
        RelativeLayout rl_done = (RelativeLayout) layout.findViewById(R.id.rl_done);
        rl_popclose = (RelativeLayout) layout.findViewById(R.id.rl_pop);
        rl_popclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });
        img_car = (ImageView) layout.findViewById(R.id.img_managed_car);
        txt_car = (TextView) layout.findViewById(R.id.txt_row_no1);
        Resources rs;
        final managedlostpop adpter = new managedlostpop(getActivity(), data, rs = getResources());
        grid_managed.setAdapter(adpter);


        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  for(int i=0;i<data.size();i++){
                    data.get(i).setSelected_parking(false);
                }
                adpter.notifyDataSetChanged();*/
                popupmanaged.dismiss();
                //txt_space.setText("Row and Space #");
            }
        });

        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).isSelected_parking()) {
                String row_no = data.get(i).getRow_no();
                String row_name = data.get(i).getRoe_name();
                txt_car.setText(row_name + " : " + row_no);
                img_car.setImageResource(R.mipmap.popup_car_green);
            }
        }

        rl_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();

                for (int k = 0; k < data.size(); k++) {
                    boolean selected_ore = data.get(k).isSelected_parking();

                    if (selected_ore) {
                        String row_no = data.get(k).getRow_no();
                        String row_name = data.get(k).getRoe_name();
                        txt_row.setText(row_name + " : " + row_no);
                        // txt_space.setText("Row "+row_name+" Space # "+row_no);
                        lost_no = row_no;
                        lost_row = row_name;
                        //Toast.makeText(getActivity(), "Row Name:- "+row_name +" Row No "+row_no, Toast.LENGTH_SHORT).show();
                        break;
                    }
                }

                show_marke_payment_button();

            }
        });
        grid_managed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                if (!data.get(position).getIsparked().equals("t")) {

                    for (int i = 0; i < data.size(); i++) {
                        data.get(i).setSelected_parking(false);
                    }
                    data.get(position).setSelected_parking(true);
                    adpter.notifyDataSetChanged();
                    String row_no = data.get(position).getRow_no();
                    String row_name = data.get(position).getRoe_name();
                    // txt_space.setText("Row "+row_name+" Space # "+row_no);
                    if (township_sapce_marked.equals("false")) {
                        lost_no = row_no;
                        txt_row.setText("R : " + row_no);
                    } else if (total_row < 2) {
                        lost_row = row_name;
                        txt_row.setText("S : " + row_name);
                    } else {
                        lost_no = row_no;
                        lost_row = row_name;
                        txt_row.setText(row_name + " : " + row_no);
                    }
                    popupmanaged.dismiss();
                    show_marke_payment_button();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("ParkEZly!!");
                    alertDialog.setMessage("This Lot Seems To Be Occupied. Are you sure you want to park here ?");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            for (int i = 0; i < data.size(); i++) {
                                data.get(i).setSelected_parking(false);
                            }
                            data.get(position).setSelected_parking(true);
                            adpter.notifyDataSetChanged();


                            for (int k = 0; k < data.size(); k++) {
                                boolean selected_ore = data.get(k).isSelected_parking();

                                if (selected_ore) {
                                    String row_no = data.get(k).getRow_no();
                                    String row_name = data.get(k).getRoe_name();
                                    //Toast.makeText(getActivity(), "Row Name:- "+row_name +" Row No "+row_no, Toast.LENGTH_SHORT).show();
                                    break;
                                }
                            }
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    alertDialog.show();
                }
            }
        });


    }

    public class getlistofmanagedloatpopup extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String lat, lang, location_code1, address, title, html, mark, parking_time, no_parking_time, notice;

        String managedlosturl = "_proc/managed_lot_status?&order=(lot_row%20ASC%2C%20lot_number%20ASC)";


        public getlistofmanagedloatpopup(String loc, ArrayList<item> dtata) {
            data.clear();
            this.location_code1 = loc;
            data = dtata;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            managedlostpoparray.clear();
            JSONObject managed = new JSONObject();


            /*  if(latitude==0.0) {*/
            try {
                managed.put("name", "in_location_code");
                managed.put("value", location_code);

                //  student1.put("value", latitude);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            JSONArray jsonArray = new JSONArray();
            jsonArray.put(managed);

            JSONObject manageda = new JSONObject();
            try {
                manageda.put("params", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("managed_lost_url", url);
            Log.e("managed_lodt_param", String.valueOf(manageda));

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(manageda.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);

            HttpResponse response = null;

            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    TimeZone zone = TimeZone.getTimeZone("UTC");
                    sdf.setTimeZone(zone);
                    String currentdate = sdf.format(new Date());
                    Date current = null;
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        current = format.parse(currentdate);
                        System.out.println(current);
                    } catch (android.net.ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json1 = new JSONArray(responseStr);

                    for (int i = 0; i < json1.length(); i++) {
                        item item = new item();
                        JSONObject josnget = json1.getJSONObject(i);
                        String id = josnget.getString("id");
                        String date = josnget.getString("date_time");
                        String township_code = josnget.getString("township_code");
                        String location_code = josnget.getString("location_code");
                        String location_name = josnget.getString("location_name");
                        String lot_row = josnget.getString("lot_row");
                        String lot_number = josnget.getString("lot_number");
                        String lot_id = josnget.getString("lot_id");
                        String occupied = josnget.getString("occupied").toLowerCase();
                        String plate_no = josnget.getString("plate_no");
                        String plate_state = josnget.getString("plate_state");
                        String registered_expiry_time = josnget.getString("entry_date_time");
                        String guest_expiry_time = josnget.getString("expiry_time");
                        String guest_exit_time = josnget.getString("expiry_time");
                        String parking_status = josnget.getString("parking_status").toUpperCase();
                        String type = josnget.getString("parking_type").toUpperCase();

                        if (parking_status.equals("ENTRY") && occupied.equals("yes")) {

                            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date exitre = null;
                            try {
                                exitre = format1.parse(guest_expiry_time);

                            } catch (android.net.ParseException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            } catch (java.text.ParseException e) {
                                e.printStackTrace();
                            }


                            String carcolor;
                            long diff = exitre.getTime() - current.getTime();
                            if (current.after(exitre)) {
                                if (type.equals("anywhere")) {
                                    carcolor = "red";
                                } else {
                                    carcolor = "red";
                                }
                            } else {
                                carcolor = "green";
                                Log.d("current", String.valueOf(current));
                                Log.d("ex", String.valueOf(exitre));
                                System.out.println(current);
                                System.out.println(exitre);
                            }

                            item.setId(id);
                            item.setDate(date);
                            item.setTownshipcode(township_code);
                            item.setLocation_code(location_code);
                            item.setLoationname(location_name);
                            item.setRoe_name(lot_row);
                            item.setRow_no(lot_number);
                            item.setLots_id(lot_id);
                            item.setOccupied(occupied);
                            item.setPlate_no(plate_no);
                            item.setState(plate_state);
                            item.setRespdate(registered_expiry_time);
                            item.setGuest_expi_date(guest_expiry_time);
                            item.setMarker(carcolor);
                            managedlostpoparray.add(item);
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json1 != null) {

                for (int i = 0; i < managedlostpoparray.size(); i++) {
                    String row_no = managedlostpoparray.get(i).getRow_no();
                    if (!row_no.equals("")) {
                        if (isInteger(row_no)) {
                            int jj = Integer.parseInt(row_no);
                            String row_name = managedlostpoparray.get(i).getRoe_name();
                            for (int j = 0; j < data.size(); j++) {
                                String row_no11 = data.get(j).getRow_no();
                                if (!row_no11.equals("")) {
                                    int datarow = Integer.parseInt(row_no11);
                                    String row_name2 = data.get(j).getRoe_name();
                                    if (jj == datarow) {
                                        if (row_name.equals(row_name2)) {
                                            data.get(j).setMarker(managedlostpoparray.get(i).getMarker());
                                            data.get(j).setIsparked("t");
                                        }
                                    }
                                }
                            }
                        } else {
                            String row_name = managedlostpoparray.get(i).getRoe_name();
                            for (int j = 0; j < data.size(); j++) {
                                String row_no11 = data.get(j).getRow_no();
                                if (!row_no11.equals("")) {
                                    String row_name2 = data.get(j).getRoe_name();
                                    if (row_no.equals(row_no11)) {
                                        if (row_name.equals(row_name2)) {
                                            data.get(j).setMarker(managedlostpoparray.get(i).getMarker());
                                            data.get(j).setIsparked("t");
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                if (!renew.equals("yes")) {

                    for (int m = 0; m < data.size(); m++) {
                        if (!data.get(m).getIsparked().equals("t")) {

                            for (int i = 0; i < data.size(); i++) {
                                data.get(i).setSelected_parking(false);
                            }
                            data.get(m).setSelected_parking(true);
                            for (int k = 0; k < data.size(); k++) {
                                boolean selected_ore = data.get(k).isSelected_parking();

                                if (selected_ore) {
                                    String row_no = data.get(k).getRow_no();
                                    String row_name = data.get(k).getRoe_name();

                                    if (township_sapce_marked.equals("false")) {
                                        lost_no = row_no;
                                        txt_row.setText("R : " + lost_no);
                                    } else if (total_row < 2) {
                                        lost_row = row_name;
                                        txt_row.setText("S : " + lost_row);
                                    } else {
                                        lost_no = row_no;
                                        lost_row = row_name;
                                        txt_row.setText(row_name + " : " + row_no);
                                    }
                                    // txt_space.setText("Row "+row_name+" Space # "+row_no);
                                    /*lost_no = row_no;
                                    lost_row = row_name;*/
                                    //Toast.makeText(getActivity(), "Row Name:- "+row_name +" Row No "+row_no, Toast.LENGTH_SHORT).show();
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
            }

            super.onPostExecute(s);
        }
    }

    public class getvehiclenumber extends AsyncTask<String, String, String> {
        JSONObject json = null;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "0");
        String vehiclenourl = "_table/user_vehicles?filter=user_id%3D" + user_id + "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            rl_progressbar.setVisibility(View.VISIBLE);
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            listofvehicle.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("get_vehicle_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);
                    Log.e("rerpones", String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        String plate_no = c.getString("plate_no");
                        String state = c.getString("registered_state");
                        String id = c.getString("id");
                        /*Iterator myVeryOwnIterator = statefullname.keySet().iterator();
                        for (int k = 0; k < all_state.size(); k++) {
                            String state_all = all_state.get(k).getState2Code();
                            if (state_all.equals(state)) {
                                item.setState(all_state.get(k).getStateName());
                            }
                        }*/
                        item.setState(state);
                        item.setPlateno(plate_no);
                        item.setId(id);
                        item.setVehicle_country(c.getString("vehicle_country"));
                        item.setIsselect(false);
                        item.setCheckboxselected(false);
                        listofvehicle.add(item);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            //progressBar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity == null) {
                } else {
                    ShowState(getActivity());
                }
            } else {
                rl_progressbar.setVisibility(View.GONE);
            }
            super.onPostExecute(s);
        }
    }


    private void showenter_vehicle(Activity context) {

        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.enter_tvehicle, viewGroup, false);
        final PopupWindow popup = new PopupWindow(context);
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(true);
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        popup.setFocusable(true);
        popup.update();
        popup.showAtLocation(layout, Gravity.TOP, 0, 0);

        final EditText edit_number;
        final Button btn_park_now;
        RelativeLayout btn_cancel;

        edit_number = (EditText) layout.findViewById(R.id.edit_nu);
        edit_number.setFocusable(true);
        btn_cancel = (RelativeLayout) layout.findViewById(R.id.close);
        btn_park_now = (Button) layout.findViewById(R.id.btn_done);
        btn_park_now.setAlpha((float) 0.3);
        btn_park_now.setEnabled(false);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        btn_park_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edit_number.getWindowToken(), 0);
                String hgv = edit_number.getText().toString();
                editno.setText(hgv);
                popup.dismiss();
                show_marke_payment_button();
                ShowStatePupup(getActivity(), state_array);

            }
        });

        edit_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String ss = s.toString();
                if (!ss.equals(ss.toUpperCase())) {
                    edit_number.setText(ss.toUpperCase());
                    edit_number.setSelection(ss.length());
                }

                if (s.length() > 0) {
                    btn_park_now.setAlpha((float) 1.0);
                    btn_park_now.setEnabled(true);
                } else {
                    btn_park_now.setAlpha((float) 0.3);
                    btn_park_now.setEnabled(false);
                }
            }
        });


        edit_number.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String hgv = edit_number.getText().toString();
                    editno.setText(hgv);
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edit_number.getWindowToken(), 0);
                    ShowStatePupup(getActivity(), state_array);
                    return true;
                }
                return false;
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edit_number.getWindowToken(), 0);
                String hgv = edit_number.getText().toString();
                editno.setText(hgv);
                show_marke_payment_button();
                popup.dismiss();
            }
        });

    }

    private void ShowState(Activity context) {
        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        layout = layoutInflater.inflate(R.layout.parkherepopup, null);
        popup_state = new PopupWindow(context);
        popup_state.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup_state.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup_state.setContentView(layout);
        popup_state.setFocusable(false);
        popup_state.setAnimationStyle(R.style.animationName);
        popup_state.setBackgroundDrawable(null);
        ListView listofvehicleno = (ListView) layout.findViewById(R.id.listselectvehicl);
        TextView txt_add_vi = (TextView) layout.findViewById(R.id.txtaddvehicle);
        txt_add_vi.setVisibility(View.VISIBLE);
        Resources rs;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int yyy = rl_main.getHeight();
            int yyy1 = rl_title.getHeight();
            int final_position = yyy1 + yyy + 265;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                popup_state.showAtLocation(layout, Gravity.NO_GRAVITY, 0, final_position);
            } else {
                popup_state.showAsDropDown(rl_main, 0, 0);
            }


        } else {
            popup_state.showAsDropDown(rl_main, 0, 0);
        }
        if (listofvehicle.size() > 0) {
            Activity activty = getActivity();
            if (activty == null) {
                popup_state.dismiss();
            } else {
                CustomAdapter_state adpater = new CustomAdapter_state(getActivity(), listofvehicle, rs = getResources());
                listofvehicleno.setAdapter(adpater);
            }
        }

        txt_add_vi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                is_add_back = true;
                SharedPreferences add1 = getActivity().getSharedPreferences("addvehicle", Context.MODE_PRIVATE);
                SharedPreferences.Editor ed = add1.edit();
                ed.putString("addvehicle", "yes");
                ed.apply();
                Bundle b = new Bundle();
                b.putString("direct_add", "yes");
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                vehicle_add pay = new vehicle_add();
                pay.registerForListener(freeparkhere.this);
                pay.setArguments(b);
                ft.add(R.id.My_Container_1_ID, pay, "my_vehicle_add");
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
                popup_state.dismiss();
            }
        });

        RelativeLayout imageclose;
        imageclose = (RelativeLayout) layout.findViewById(R.id.close);
        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup_state.dismiss();
            }
        });
        rl_progressbar.setVisibility(View.GONE);
    }

    public class CustomAdapter_state extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdapter_state(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txtplateno, txtstate;
            public RelativeLayout rl_main;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {
                vi = inflater.inflate(R.layout.selectvehicle, null);
                holder = new ViewHolder();

                holder.txtplateno = (TextView) vi.findViewById(R.id.txtlicenseplate);
                holder.txtstate = (TextView) vi.findViewById(R.id.txtstate);
                holder.rl_main = (RelativeLayout) vi.findViewById(R.id.rl_main);
                vi.setTag(holder);
                vi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String platno = holder.txtplateno.getText().toString().toUpperCase();
                        String statename11 = holder.txtstate.getText().toString();
                        editno.setText(platno.toUpperCase());
                        editno.setVisibility(View.VISIBLE);

                       /* //txt_plate_no.setText(platno.toUpperCase());
                        Iterator myVeryOwnIterator = statefullname.keySet().iterator();
                        while (myVeryOwnIterator.hasNext()) {
                            String key = (String) myVeryOwnIterator.next();
                            String value = (String) statefullname.get(key);
                            if (value.equals(statename11)) {
                                for (int i = 1; i < statearray.length; i++) {
                                    String statename1 = statearray[i].toString();
                                    item ii = new item();
                                    ii.setState(statearray[i].toString());
                                    if (statename1.equals(key)) {
                                        ii.setSelected_parking(true);
                                        statename = statename1;

                                    } else {
                                        ii.setSelected_parking(false);
                                    }
                                    state_array.add(ii);
                                }
                            }
                            state_array.clear();
                        }*/

                        txt_state.setText(statename11);
                        popup_state.dismiss();
                        if (!parkhere.equals("yes")) {
                            ShowHours(getActivity(), stateq);
                        }
                        show_marke_payment_button();
                        popup_state.dismiss();

                        /*for (int k = 0; k < all_state.size(); k++) {
                            String state_all = all_state.get(k).getStateName();
                            if (state_all.equals(statename11)) {
                                String state_code = all_state.get(k).getState2Code();
                                for (int i = 1; i < all_state.size(); i++) {
                                    String statename1 = all_state.get(i).getState2Code();
                                    item ii = new item();
                                    ii.setState(all_state.get(i).getState2Code());
                                    if (statename1.equals(state_code)) {
                                        ii.setSelected_parking(true);
                                        statename = statename1;
                                        txt_state.setText(statename);
                                    } else {
                                        ii.setSelected_parking(false);
                                    }
                                    state_array.add(ii);

                                }
                                popup_state.dismiss();
                                ShowHours(getActivity(), stateq);
                            }
                        }*/
                    }
                });
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }
            if (data.size() <= 0) {
                holder.txtplateno.setText("No Data");
            } else {
                /***** Get each Model object from Arraylist ********/
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                /************  Set Model values in Holder elements ***********/
                String cat = tempValues.getPlantno();
                if (cat == null) {
                    holder.txtplateno.setVisibility(View.GONE);
                } else if (cat != null) {
                    Typeface light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Light.otf");
                    holder.txtplateno.setTypeface(light);
                    holder.txtstate.setTypeface(light);
                    holder.txtplateno.setText(tempValues.getPlantno());
                    holder.txtstate.setText(tempValues.getState());
                   /* if (!rate.equals("") && !rate1.equals(""))
                    {
                        Double du=Double.parseDouble( rate1) /60.0;
                        txtrate.setText("Rate: $"+rate+"@"+rate1 +duration_unit);
                    }*/

                    if (position % 2 == 0) {
                        holder.rl_main.setBackgroundColor(Color.parseColor("#ffffff"));
                    } else {
                        holder.rl_main.setBackgroundColor(Color.parseColor("#e0e0e0"));
                    }
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

    @Override
    public void onResume() {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        if (is_add_back) {
            is_add_back = false;
            if (!id.equals("null")) {
                new getvehiclenumber().execute();
            }
        }
        super.onResume();
    }

    public void show_marke_payment_button() {
        platno = editno.getText().toString();
        if (rl_hours.getVisibility() == View.VISIBLE) {
            if (!platno.equals("") && !statename.equals("State") && !shoues.equals("hours") && !shoues.equals("")) {
                if (rl_sp_row.getVisibility() == View.VISIBLE) {
                    txtparknow.setVisibility(View.VISIBLE);
                } else {
                    txtparknow.setVisibility(View.VISIBLE);
                }
            }
        } else {
            if (!platno.equals("")) {
                txtparknow.setVisibility(View.VISIBLE);
            } else {
                txtparknow.setVisibility(View.GONE);
            }
        }
    }

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, final Bundle resultData) {
            if (resultCode == Constants.SUCCESS_RESULT) {
                final Address address = resultData.getParcelable(Constants.RESULT_ADDRESS);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /*infoText.setVisibility(View.VISIBLE);

                        infoText.setText("Latitude: " + address.getLatitude() + "\n" +
                                "Longitude: " + address.getLongitude() + "\n" +
                                "Address: " + resultData.getString(Constants.RESULT_DATA_KEY));*/

                        lat = String.valueOf(address.getLatitude());
                        lang = String.valueOf(address.getLongitude());
                        String statename_ = address.getAdminArea();

/*                     Iterator myVeryOwnIterator = statefullname.keySet().iterator();
                       while (myVeryOwnIterator.hasNext()) {
                            String   key = (String) myVeryOwnIterator.next();
                            String value = (String) statefullname.get(key);
                            if (value.equals(statename_)) {
                                for (int i = 0; i <= statearray.length; i++) {
                                    String state = statearray[i];
                                    if (state.equals(key)) {

                                        //txt_state.setText(state);
                                        //txt_state.setEnabled(false);
                                        break;
                                    }
                                }
                            }
                        }
*/

                     /*  for(int i=0;i<all_state.size();i++)
                       {
                           String country_name=all_state.get(i).getStateName();
                           if(country_name.equals(statename_))
                           {

                           }
                       }*/
                        state_array.clear();
                        for (int i = 1; i < all_state.size(); i++) {
                            String statename_1 = all_state.get(i).getState2Code();
                            String state_ = txt_state.getText().toString();
                            item ii = new item();
                            ii.setState(statename_1);
                            if (state_.equals("State") || state_.equals("")) {
                                if (statename_1.equals("NY")) {
                                    ii.setSelected_parking(true);
                                    // txt_state.setText("NY");
                                    //statename="NY";
                                } else {
                                    ii.setSelected_parking(false);
                                }
                            } else {
                                if (statename_1.equals(state_)) {
                                    ii.setSelected_parking(true);
                                    //statename=state_;
                                } else {
                                    ii.setSelected_parking(false);
                                }
                            }
                            state_array.add(ii);
                        }

                        show_marke_payment_button();
                        // address_=resultData.getString(Constants.RESULT_DATA_KEY);

                    }
                });
            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rl_progressbar.setVisibility(View.GONE);
                       /* infoText.setVisibility(View.VISIBLE);
                        infoText.setText(resultData.getString(Constants.RESULT_DATA_KEY));*/
                    }
                });
            }
        }
    }

    public class Update_Parking extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String rid = logindeatl.getString("id", "null");
        String id1 = "null";
        JSONObject json, json1;
        String pov2 = "_table/parked_cars";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);

            TimeZone zone12 = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone12);
            final String currentDateandTime = Renew_date;
            Date date11 = null;
            try {
                date11 = sdf.parse(currentDateandTime);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date11);
            if (min.equals("Hrs") || min.equals("Hour") || min.equals("Hr")) {
                calendar.add(Calendar.HOUR, hours);
            } else if (min.equals("Months") || min.equals("Month")) {
                calendar.add(Calendar.MONTH, hours);
                calendar.add(Calendar.DATE, -1);
            } else if (min.equals("Days") || min.equals("Day")) {
                calendar.add(Calendar.DATE, hours);
            } else if (min.equals("Weeks") || min.equals("Week")) {
                calendar.add(Calendar.DAY_OF_WEEK_IN_MONTH, hours);
            } else {
                calendar.add(Calendar.MINUTE, hours);
            }

            String expridate = sdf.format(calendar.getTime());
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.US);
            SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            outputFormat.setTimeZone(zone12);
            inputFormat.setTimeZone(zone12);

            Date date = null;
            Date Current = null;
            expiretime = null;
            String final_current_date = null;
            try {
                date = inputFormat.parse(expridate);
                Current = inputFormat.parse(Renew_date);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

            expiretime = outputFormat.format(date);
            final_current_date = outputFormat.format(Current);
            int h = 0;
            if (user_id.equals("null")) {
                h = 0;
            } else {
                h = Integer.parseInt(user_id);
            }

            jsonValues.put("exit_date_time", expiretime);
            jsonValues.put("expiry_time", expiretime);
            jsonValues.put("selected_duration", final_count);
            jsonValues.put("id", ReNew_id);
            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + pov2;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id1 = c.getString("id");

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);

            if (getActivity() != null && json1 != null) {
                if (!id1.equals("null")) {
                    new getparked_car_renew(id1).execute();

                }
            }
            super.onPostExecute(s);
        }
    }

    public class getparked_car_renew extends AsyncTask<String, String, String> {
        String park_id;
        JSONObject json;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String login_id = logindeatl.getString("id", "0");
        String transactionurl = "_table/parked_cars?filter=(user_id%3D" + login_id + ")%20AND%20(parking_status%3D'ENTRY')";
        String id = "null";

        public getparked_car_renew(String id) {
            this.park_id = id;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                        String plnp = c.getString("plate_no");
                        String state = c.getString("pl_state");
                        String state1 = c.getString("state");

                        if (!park_id.equals("null")) {
                            if (id.equals(park_id)) {

                                String city = c.getString("city");
                                String country = c.getString("country");
                                String exit = c.getString("exit_date_time");
                                String extime = c.getString("expiry_time");
                                String max = c.getString("max_duration");
                                String locationcode = c.getString("location_code");
                                String entrytime = c.getString("entry_date_time");
                                String addres = c.getString("address1");
                                String parked_adress = c.getString("marker_address1");
                                String lat = c.getString("lat");
                                String lang = c.getString("lng");
                                String lot = c.getString("lot_number");
                                String lot_row = c.getString("lot_row");
                                String parking_type = c.getString("parking_type");
                                String duration_unit = c.getString("duration_unit");
                                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sh.edit();
                                editor.clear().commit();
                                editor.putString("platno", plnp);
                                editor.putString("state", state);
                                editor.putString("currentdate", entrytime);
                                editor.putString("exip", expiretime);
                                editor.putString("exip1", expiretime);
                                editor.putString("hr", "0");
                                editor.putString("max", max);
                                editor.putString("lat", lat);
                                editor.putString("log", lang);
                                editor.putString("min", duration_unit);
                                editor.putString("id", id);
                                editor.putString("token", "");
                                editor.putString("country", country);
                                editor.putString("city", city);
                                editor.putString("exit_date_time", exit);
                                editor.putString("address", parked_adress);
                                editor.putString("parked_address", addres);
                                editor.putString("locationname", locationcode);
                                editor.putString("markertye", marker);
                                editor.putString("parking_type", parking_type);
                                editor.putString("lot_row", lot_row);
                                editor.putString("lot_no", lot);
                                editor.putString("parkhere", "no");
                                editor.putString("parking_free", "no");
                                editor.putString("parked_time", c.getString("selected_duration"));
                                editor.putString("entryTime", c.getString("entry_date_time"));
                                editor.putString("exitTime", c.getString("exit_date_time"));
                                editor.putString("locationName", c.getString("location_name"));
                                editor.putString("locationCode", c.getString("location_code"));
                                editor.putString("lotId", c.getString("lot_id"));
                                editor.commit();
                                editor.apply();
                                Log.e("cecc", "ecnejnce");
                                break;
                            }
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json != null) {

                if (!id.equals("null")) {
                    if (renew.equals("yes")) {
                        open_timer();
                    } else {
                        direct_open_timer(park_id);
                    }
                }
            }
            super.onPostExecute(s);

        }
    }

    public void open_timer() {
        SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = time.edit();
        ed.putString("result", "true");
        ed.commit();
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.replace(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
        ft.commit();
    }

    public void direct_open_timer(final String park_id) {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String login_id = logindeatl.getString("id", "0");
        if (!login_id.equals("0")) {

            if (!park_id.equals("null")) {
                            /*final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.replace(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                            ft.commitAllowingStateLoss();*/
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                timervehicleinfo pay = new timervehicleinfo();
                fragmentStack.lastElement().onPause();
                ft.add(R.id.My_Container_1_ID, pay, "timer");
                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            } else {
                SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
                SharedPreferences.Editor ed = time.edit();
                ed.putString("result", "true");
                ed.commit();
                           /* final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.add(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                            ft.commitAllowingStateLoss();*/
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                timervehicleinfo pay = new timervehicleinfo();
                fragmentStack.lastElement().onPause();
                ft.add(R.id.My_Container_1_ID, pay, "timer");
                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();

            }
        } else {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Parking Notifications");
            alertDialog.setMessage("Do you want to register for parking notifications");
            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ((vchome) getActivity()).open_login_screen();

                }
            });
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (!park_id.equals("null")) {
                                   /* final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                    ft.replace(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                                    ft.commitAllowingStateLoss();*/

                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        timervehicleinfo pay = new timervehicleinfo();
                        fragmentStack.lastElement().onPause();
                        ft.add(R.id.My_Container_1_ID, pay, "timer");
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                    } else {
                        SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
                        SharedPreferences.Editor ed = time.edit();
                        ed.putString("result", "true");
                        ed.commit();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        timervehicleinfo pay = new timervehicleinfo();
                        fragmentStack.lastElement().onPause();
                        ft.add(R.id.My_Container_1_ID, pay, "timer");
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                                     /*  final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                    ft.add(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                                    ft.commitAllowingStateLoss();*/

                    }
                }
            });
            alertDialog.show();
        }

    }

    public class fetchLocationLotOccupiedData extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String lat, lang, address, title, html, mark, parking_time, no_parking_time, notice;

        String managedlosturl = "_proc/managed_lot_status?&order=(lot_row%20ASC%2C%20lot_number%20ASC)";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            locationLots.clear();
            JSONObject managed = new JSONObject();


            /*  if(latitude==0.0) {*/
            try {
                managed.put("name", "in_location_code");
                managed.put("value", location_code);

                //  student1.put("value", latitude);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            JSONArray jsonArray = new JSONArray();
            jsonArray.put(managed);

            JSONObject manageda = new JSONObject();
            try {
                manageda.put("params", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("managed_lost_url", url);
            Log.e("managed_lodt_param", String.valueOf(manageda));

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(manageda.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);

            HttpResponse response = null;

            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    TimeZone zone = TimeZone.getTimeZone("UTC");
                    sdf.setTimeZone(zone);
                    String currentdate = sdf.format(new Date());
                    Date current = null;
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        current = format.parse(currentdate);
                        System.out.println(current);
                    } catch (android.net.ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json1 = new JSONArray(responseStr);

                    for (int i = 0; i < json1.length(); i++) {
                        LocationLot item = new LocationLot();
                        JSONObject jsonObject = json1.getJSONObject(i);
                        if (jsonObject.has("id")
                                && !TextUtils.isEmpty(jsonObject.getString("id"))
                                && !jsonObject.getString("id").equals("null")) {
                            item.setId(jsonObject.getInt("id"));
                        }
                        if (jsonObject.has("date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("date_time"))
                                && !jsonObject.getString("date_time").equals("null")) {
                            item.setDate_time(jsonObject.getString("date_time"));
                        }

                        if (jsonObject.has("township_code")
                                && !TextUtils.isEmpty(jsonObject.getString("township_code"))
                                && !jsonObject.getString("township_code").equals("null")) {
                            item.setTownship_code(jsonObject.getString("township_code"));
                        }

                        if (jsonObject.has("location_code")
                                && !TextUtils.isEmpty(jsonObject.getString("location_code"))
                                && !jsonObject.getString("location_code").equals("null")) {
                            item.setLocation_code(jsonObject.getString("location_code"));
                        }

                        if (jsonObject.has("location_name")
                                && !TextUtils.isEmpty(jsonObject.getString("location_name"))
                                && !jsonObject.getString("location_name").equals("null")) {
                            item.setLocation_name(jsonObject.getString("location_name"));
                        }

                        if (jsonObject.has("lot_row")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_row"))
                                && !jsonObject.getString("lot_row").equals("null")) {
                            item.setLot_row(jsonObject.getString("lot_row"));
                        }

                        if (jsonObject.has("lot_number")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_number"))
                                && !jsonObject.getString("lot_number").equals("null")) {
                            item.setLot_number(jsonObject.getString("lot_number"));
                        }
                        if (jsonObject.has("lot_id")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_id"))
                                && !jsonObject.getString("lot_id").equals("null")) {
                            item.setLot_id(jsonObject.getString("lot_id"));
                        }

                        if (jsonObject.has("occupied")
                                && !TextUtils.isEmpty(jsonObject.getString("occupied"))
                                && !jsonObject.getString("occupied").equals("null")) {
                            item.setOccupied(jsonObject.getString("occupied").toLowerCase());
                        }

                        if (jsonObject.has("plate_no")
                                && !TextUtils.isEmpty(jsonObject.getString("plate_no"))
                                && !jsonObject.getString("plate_no").equals("null")) {
                            item.setPlate_no(jsonObject.getString("plate_no"));
                        }
                        if (jsonObject.has("plate_state")
                                && !TextUtils.isEmpty(jsonObject.getString("plate_state"))
                                && !jsonObject.getString("plate_state").equals("null")) {
                            item.setPlate_state(jsonObject.getString("plate_state"));
                        }

                        if (jsonObject.has("entry_date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("entry_date_time"))
                                && !jsonObject.getString("entry_date_time").equals("null")) {
                            item.setEntry_date_time(jsonObject.getString("entry_date_time"));
                        }

                        if (jsonObject.has("exit_date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("exit_date_time"))
                                && !jsonObject.getString("exit_date_time").equals("null")) {
                            item.setExit_date_time(jsonObject.getString("exit_date_time"));
                        }

                        if (jsonObject.has("expiry_time")
                                && !TextUtils.isEmpty(jsonObject.getString("expiry_time"))
                                && !jsonObject.getString("expiry_time").equals("null")) {
                            item.setExpiry_time(jsonObject.getString("expiry_time"));
                        }

                        if (jsonObject.has("parking_type")
                                && !TextUtils.isEmpty(jsonObject.getString("parking_type"))
                                && !jsonObject.getString("parking_type").equals("null")) {
                            item.setParking_type(jsonObject.getString("parking_type"));
                        }

                        if (jsonObject.has("parking_status")
                                && !TextUtils.isEmpty(jsonObject.getString("parking_status"))
                                && !jsonObject.getString("parking_status").equals("null")) {
                            item.setParking_status(jsonObject.getString("parking_status").toLowerCase());
                        }

                        if (jsonObject.has("lot_reservable")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_reservable"))
                                && !jsonObject.getString("lot_reservable").equals("null")) {
                            if (jsonObject.getString("lot_reservable").equals("1")) {
                                item.setLot_reservable(true);
                            } else if (jsonObject.getString("lot_reservable").equals("0")) {
                                item.setLot_reservable(false);
                            } else {
                                item.setLot_reservable(jsonObject.getBoolean("lot_reservable"));
                            }
                        }
                        if (jsonObject.has("location_id")) {
                            item.setLocation_id(Integer.parseInt(jsonObject.getString("location_id")));
                        }

                        if (!TextUtils.isEmpty(item.getParking_status())
                                && !TextUtils.isEmpty(item.getOccupied())
                                && item.getParking_status().equals("ENTRY")
                                && item.getOccupied().equals("yes")) {

                            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date exitre = null;
                            try {
                                exitre = format1.parse(item.getExpiry_time());

                            } catch (android.net.ParseException e) {
                                // TODO Auto-generated catch block
                                Log.e("#DEBUG", "  Error:  " + e.getMessage());
                                e.printStackTrace();
                            } catch (java.text.ParseException e) {
                                Log.e("#DEBUG", "  Error:  " + e.getMessage());
                                e.printStackTrace();
                            }

                            if (current.after(exitre)) {
                                item.setExpired(true);
                            } else {
                                item.setExpired(false);
                            }
                        }
                        if (!locationLots.contains(item)) {
                            locationLots.add(item);
                        }
                    }


                } catch (JSONException e) {
                    Log.e("#DEBUG", "  Error:  " + e.getMessage());
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.e("#DEBUG", "  Error:  " + e.getMessage());
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            //progressBar.setVisibility(View.GONE);
            if (getActivity() != null && json1 != null) {

//                for (int i = 0; i < managedlostpoparray.size(); i++) {
//                    String row_no = managedlostpoparray.get(i).getRow_no();
//                    if (!row_no.equals("")) {
//                        if (isInteger(row_no)) {
//                            int jj = Integer.parseInt(row_no);
//                            String row_name = managedlostpoparray.get(i).getRoe_name();
//                            for (int j = 0; j < data.size(); j++) {
//                                String row_no11 = data.get(j).getRow_no();
//                                if (!row_no11.equals("")) {
//                                    int datarow = Integer.parseInt(row_no11);
//                                    String row_name2 = data.get(j).getRoe_name();
//                                    if (jj == datarow) {
//                                        if (row_name.equals(row_name2)) {
//                                            data.get(j).setMarker(managedlostpoparray.get(i).getMarker());
//                                            data.get(j).setIsparked("t");
//                                        }
//                                    }
//                                }
//                            }
//                        } else {
//                            String row_name = managedlostpoparray.get(i).getRoe_name();
//                            for (int j = 0; j < data.size(); j++) {
//                                String row_no11 = data.get(j).getRow_no();
//                                if (!row_no11.equals("")) {
//                                    String row_name2 = data.get(j).getRoe_name();
//                                    if (row_no.equals(row_no11)) {
//                                        if (row_name.equals(row_name2)) {
//                                            data.get(j).setMarker(managedlostpoparray.get(i).getMarker());
//                                            data.get(j).setIsparked("t");
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//
//                }
//                if (!renew.equals("yes")) {
//
//                    for (int m = 0; m < data.size(); m++) {
//                        if (!data.get(m).getIsparked().equals("t")) {
//
//                            for (int i = 0; i < data.size(); i++) {
//                                data.get(i).setSelected_parking(false);
//                            }
//                            data.get(m).setSelected_parking(true);
//                            for (int k = 0; k < data.size(); k++) {
//                                boolean selected_ore = data.get(k).isSelected_parking();
//
//                                if (selected_ore) {
//                                    String row_no = data.get(k).getRow_no();
//                                    String row_name = data.get(k).getRoe_name();
//
//                                    if (township_sapce_marked.equals("false")) {
//                                        lost_no = row_no;
//                                        txt_row.setText("R : " + lost_no);
//                                    } else if (total_row < 2) {
//                                        lost_row = row_name;
//                                        txt_row.setText("S : " + lost_row);
//                                    } else {
//                                        lost_no = row_no;
//                                        lost_row = row_name;
//                                        txt_row.setText(row_name + " : " + row_no);
//                                    }
//                                    // txt_space.setText("Row "+row_name+" Space # "+row_no);
//                                    /*lost_no = row_no;
//                                    lost_row = row_name;*/
//                                    //Toast.makeText(getActivity(), "Row Name:- "+row_name +" Row No "+row_no, Toast.LENGTH_SHORT).show();
//                                    break;
//                                }
//                            }
//                            break;
//                        }
//                    }
//                }
            }

            super.onPostExecute(s);
        }
    }

    private class LocationLotAdapter extends RecyclerView.Adapter<LocationLotAdapter.LocationLotHolder> {
        private Drawable drawableRed, drawableBlue, drawableGreen, drawableGrey;

        @NonNull
        @Override
        public LocationLotAdapter.LocationLotHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            drawableRed = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_red);
            drawableBlue = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_blue);
            drawableGreen = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_green);
            drawableGrey = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_gree_car);
            return new LocationLotHolder(LayoutInflater.from(getActivity())
                    .inflate(R.layout.item_location_lot, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull LocationLotAdapter.LocationLotHolder locationLotHolder, int i) {
            LocationLot locationLot = locationLots.get(i);
            Log.e("#DEBUG", "  onBind:  " + i + "  " + new Gson().toJson(locationLot));
            locationLotHolder.tvReserved.setVisibility(View.GONE);
            if (locationLot != null) {
                if (!TextUtils.isEmpty(locationLot.getLot_id())) {
                    locationLotHolder.tvRow.setText(locationLot.getLot_id());
                }

                if (!TextUtils.isEmpty(locationLot.getOccupied())) {
                    if (locationLot.getOccupied().equalsIgnoreCase("yes")) {
                        //Car already parked at this location
                        if (locationLot.isExpired()) {
                            //RED, parking expired
                            locationLotHolder.ivCar.setImageDrawable(drawableRed);
                        } else {
                            //GREEN, parking valid
                            locationLotHolder.ivCar.setImageDrawable(drawableGreen);
                        }
                        if (locationLot.isLot_reservable()) {
                            locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                        }
                    } else if (locationLot.getOccupied().equalsIgnoreCase("no")) {
                        if (locationLot.isLot_reservable()) {
                            //GREY with R, Available for reservation
                            locationLotHolder.ivCar.setImageDrawable(drawableGrey);
                            locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                        } else {
                            //GREY, Available for parking
                            locationLotHolder.ivCar.setImageDrawable(drawableGrey);
                        }

                    } else if (locationLot.getOccupied().equalsIgnoreCase("reserved")) {
                        //BLUE with R, Reserved for parking
                        locationLotHolder.ivCar.setImageDrawable(drawableBlue);
                        locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (locationLot.isLot_reservable()) {
                        //GREY with R, Available for reservation
                        locationLotHolder.ivCar.setImageDrawable(drawableGrey);
                        locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                    } else {
                        //GREY, Available for parking
                        locationLotHolder.ivCar.setImageDrawable(drawableGrey);
                    }
                }
            }

        }

        @Override
        public int getItemCount() {
            return locationLots.size();
        }

        class LocationLotHolder extends RecyclerView.ViewHolder {
            ImageView ivCar;
            TextView tvRow, tvReserved;

            LocationLotHolder(@NonNull View itemView) {
                super(itemView);

                ivCar = itemView.findViewById(R.id.ivCar);
                tvRow = itemView.findViewById(R.id.tvRow);
                tvReserved = itemView.findViewById(R.id.tvReserved);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
//                            selectedLocationLot = locationLots.get(position);
                            if (selectedLocationLot != null) {
                                locationLots.get(locationLots.indexOf(
                                        new LocationLot(selectedLocationLot.getId()))).setOccupied("no");
                                selectedLocationLot = null;
                                txt_row.setText("Select Space");
                            }
                            if (!TextUtils.isEmpty(locationLots.get(position).getOccupied())) {
                                if (locationLots.get(position).getOccupied().equalsIgnoreCase("no")) {
                                    locationLots.get(position).setOccupied("yes");
                                    selectedLocationLot = locationLots.get(position);
                                    txt_row.setText(selectedLocationLot.getLot_id());
                                    notifyItemChanged(position);
                                    if (dialogLocationLot != null && dialogLocationLot.isShowing()) {
                                        dialogLocationLot.dismiss();
                                    }
                                } else {
                                    if (selectedLocationLot != null &&
                                            selectedLocationLot.getId() == locationLots.get(position).getId()) {
                                        selectedLocationLot = null;
                                        locationLots.get(position).setOccupied("no");
                                        if (dialogLocationLot != null && dialogLocationLot.isShowing()) {
                                            dialogLocationLot.dismiss();
                                        }
                                    } else {
                                        Toast.makeText(getActivity(), "Already occupied!", Toast.LENGTH_SHORT).show();
                                    }
//                                    locationLots.get(position).setOccupied("no");
                                }
                            } else {
                                locationLots.get(position).setOccupied("yes");
                                locationLots.get(position).setOccupied("yes");
                                selectedLocationLot = locationLots.get(position);
                                txt_row.setText(selectedLocationLot.getLot_id());
                                notifyItemChanged(position);
                                if (dialogLocationLot != null && dialogLocationLot.isShowing()) {
                                    dialogLocationLot.dismiss();
                                }
                            }
                            notifyItemChanged(position);
                        }
                    }
                });
            }
        }
    }
}
