package com.softmeasures.eventezlyadminplus.frament.config_setting;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragmentMessagingSettingsBinding;
import com.softmeasures.eventezlyadminplus.models.Manager;
import com.softmeasures.eventezlyadminplus.models.call_chat.ChatConfSettings;
import com.softmeasures.eventezlyadminplus.models.call_chat.ChatConfSettings.ChatConfSettingData;
import com.softmeasures.eventezlyadminplus.models.rest.APIClient;
import com.softmeasures.eventezlyadminplus.models.rest.APIInterface;
import com.softmeasures.eventezlyadminplus.models.users.UserProfile;
import com.softmeasures.eventezlyadminplus.services.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessagingSettingFragment extends BaseFragment {
    FragmentMessagingSettingsBinding binding;

    Manager selectedManger = null;
    UserProfile.UserProfileData selectedUser = null;
    private String TAG = "#DEBUG MessagingSetting";

    public static ChatConfSettingData chatUserSettings = null;
    APIInterface apiInterface;
    SharedPreferences logindeatl;
    String user_id, role;
    private int pos = 0;
    boolean isEdit = false, isInit = true, isMultiple = false;
    ArrayList<Manager> selectedMangers = new ArrayList<>();
    ArrayList<UserProfile.UserProfileData> selectedUsers = new ArrayList<>();
    ArrayList<ChatConfSettingData> tempChatConfSetting = new ArrayList<>();
    private PopupWindow popupmanaged;

    private PartnerAdapter partnerAdapter;
    private UserAdapter userAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_messaging_settings, container, false);
        if (getArguments() != null) {
            isMultiple = getArguments().getBoolean("isMultiple", false);
            selectedManger = new Gson().fromJson(getArguments().getString("selectedManager", null), Manager.class);
            selectedUser = new Gson().fromJson(getArguments().getString("selectedUser", null), UserProfile.UserProfileData.class);
            selectedMangers = new Gson().fromJson(getArguments().getString("selectedManagers", null), new TypeToken<ArrayList<Manager>>() {
            }.getType());
            selectedUsers = new Gson().fromJson(getArguments().getString("selectedUsers", null), new TypeToken<ArrayList<UserProfile.UserProfileData>>() {
            }.getType());
        }
        Log.d(TAG, " dtast: " + getArguments().getString("selectedManager"));
        Log.d(TAG, " user: " + getArguments().getString("selectedUser"));
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        initViews(view);
        setListeners();
        updateViews();
    }

    @Override
    protected void updateViews() {
        if (selectedManger != null) {
            binding.tvTitleName.setText("Company Name");
            binding.tvTitleCode.setText("Company Code");
            binding.etCompanyName.setText(selectedManger.getLot_manager());
            binding.etCompanyCode.setText(selectedManger.getManager_id());
        } else if (selectedUser != null) {
            binding.tvTitleName.setText("User Name");
            binding.tvTitleCode.setText("User Role");
            if (!TextUtils.isEmpty(selectedUser.getUser_name()))
                binding.etCompanyName.setText(selectedUser.getUser_name());
            else binding.etCompanyName.setText(selectedUser.getEmail());
            binding.etCompanyCode.setText(selectedUser.getRole());
        }
        binding.llChatOnLocal.setVisibility(View.GONE);
        binding.llChatOnCloud.setVisibility(View.GONE);
        binding.llChatCloudDataSize.setVisibility(View.GONE);

        if (selectedUsers != null)
        {
            binding.tvTitleName.setText("User Name");
            binding.tvTitleCode.setText("User Role");
        }
        if (selectedMangers != null)
        {
            binding.tvTitleName.setText("Company Name");
            binding.tvTitleCode.setText("Company Code");
        }
    }

    @Override
    protected void setListeners() {
        binding.etCloudAttachSizeTotalLimit.setOnClickListener(v ->
                showEnter_txt("Cloud Attach Total Time Limit", "Enter Cloud Attach Total Time Limit", binding.etCloudAttachSizeTotalLimit, "etCloudAttachSizeTotalLimit", InputType.TYPE_CLASS_NUMBER, false));

        binding.switchLocalData.setOnCheckedChangeListener((buttonView, isChecked) -> {
            binding.llChatOnLocal.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            if (isChecked && !isInit) {
                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Local Data Limit!");
                alertDialog.setCancelable(false);
                alertDialog.setMessage("You can send upto 30 MB size of attachment, If you want to share above limit, you need to enable cloud data");
                alertDialog.setNegativeButton("OK", (dialog1, which) -> {
                });
                alertDialog.show();
            }
        });

        binding.switchCloudData.setOnCheckedChangeListener((buttonView, isChecked) -> {
            binding.llChatOnCloud.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            binding.llChatCloudDataSize.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        });

        binding.etCompanyName.setOnClickListener(v -> {
            if (isMultiple) selectedCompanyiesUsers();
        });
        binding.etCompanyCode.setOnClickListener(v -> {
            if (isMultiple) selectedCompanyiesUsers();
        });
        binding.btnSaveConfig.setOnClickListener(v -> {
            chatUserSettings.setAttach_live_camera(binding.switchAttachLiveCamera.isChecked());
            chatUserSettings.setAttach_live_mic(binding.switchAttachLiveMic.isChecked());
            chatUserSettings.setAttach_zip(binding.switchAttachZip.isChecked());
            chatUserSettings.setAttach_doc(binding.switchAttachDoc.isChecked());
            chatUserSettings.setAttach_image(binding.switchAttachImage.isChecked());
            chatUserSettings.setAttach_audio(binding.switchAttachAudio.isChecked());
            chatUserSettings.setAttach_video(binding.switchAttachVideo.isChecked());
            chatUserSettings.setAllow_share(binding.switchAllowSharing.isChecked());
            chatUserSettings.setAllow_download(binding.switchAllowDownload.isChecked());
            chatUserSettings.setAllow_copying(binding.switchAllowCopying.isChecked());
            chatUserSettings.setAllow_forward(binding.switchAllowForward.isChecked());

            chatUserSettings.setLocal_data(binding.switchLocalData.isChecked());
            chatUserSettings.setCloud_data(binding.switchCloudData.isChecked());
            if (chatUserSettings.isCloud_data()) {
                if (!TextUtils.isEmpty(binding.etCloudAttachSizeTotalLimit.getText()))
                    chatUserSettings.setCloud_attach_size_total_limit(Integer.parseInt(binding.etCloudAttachSizeTotalLimit.getText().toString()));
                else {
                    Toast.makeText(myApp, "Please Enter Cloud Attach Size Total Limit", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            chatUserSettings.setChat_audio_recording_local(binding.switchChatAudioRecordingLocal.isChecked());
            chatUserSettings.setChat_audio_recording_cloud(binding.switchChatAudioRecordingCloud.isChecked());
            chatUserSettings.setChat_video_recording_local(binding.switchChatVideoRecordingLocal.isChecked());
            chatUserSettings.setChat_video_recording_cloud(binding.switchchatVideoRecordingCloud.isChecked());
            chatUserSettings.setChat_file_attach_local(binding.switchchatFileAttachLocal.isChecked());
            chatUserSettings.setChat_file_attach_cloud(binding.switchchatFileAttachCloud.isChecked());
            chatUserSettings.setChat_photo_attach_local(binding.switchchatPhotoAttachLocal.isChecked());
            chatUserSettings.setChat_photo_attach_cloud(binding.switchchatPhotoAttachCloud.isChecked());
            if (isMultiple) {
                if (selectedMangers != null) {
                    Gson gson = new Gson();
                    if (tempChatConfSetting != null) tempChatConfSetting.clear();
                    for (int i = 0; i < selectedMangers.size(); i++) {
                        chatUserSettings.setCompany_id(selectedMangers.get(i).getId());
                        chatUserSettings.setCompany_type_id(selectedMangers.get(i).getManager_type_id());
                        chatUserSettings.setCompany_name(selectedMangers.get(i).getLot_manager());
                        chatUserSettings.setCompany_code(selectedMangers.get(i).getManager_id());
                        chatUserSettings.setId(selectedMangers.get(i).getSelected_id());
                        tempChatConfSetting.add(gson.fromJson(new Gson().toJson(chatUserSettings), ChatConfSettingData.class));
                    }
                    pos=0;
                    add_editUserChatSetting(tempChatConfSetting.get(0), tempChatConfSetting.get(0).getId() != 0);
                } else if (selectedUsers != null) {
                    Gson gson = new Gson();
                    if (tempChatConfSetting != null) tempChatConfSetting.clear();
                    for (int i = 0; i < selectedUsers.size(); i++) {
                        chatUserSettings.setUser_id(selectedUsers.get(i).getUser_id());
                        chatUserSettings.setUser_profile_type(selectedUsers.get(i).getRole());
                        chatUserSettings.setId(selectedUsers.get(i).getSelected_id());
                        tempChatConfSetting.add(gson.fromJson(new Gson().toJson(chatUserSettings), ChatConfSettingData.class));
                    }
                    pos=0;
                    add_editUserChatSetting(tempChatConfSetting.get(0), tempChatConfSetting.get(0).getId() != 0);
                }
            } else add_editUserChatSetting(chatUserSettings, isEdit);
        });
    }

    @Override
    protected void initViews(View v) {
        logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = logindeatl.getString("id", "null");
        role = logindeatl.getString("role", "");
        if (selectedManger != null || selectedUser != null)
            fetchChatSetting(0);
        else if (isMultiple) {
            if (selectedMangers != null) {

                StringBuilder total_company_name = new StringBuilder();
                StringBuilder total_company_code = new StringBuilder();

                for (int i=0;i<selectedMangers.size();i++)
                {
                    total_company_name.append(selectedMangers.get(i).getLot_manager()+" , ");
                    total_company_code.append(selectedMangers.get(i).getManager_id()+" , ");
                }
                String complete_company_name=total_company_name.toString();
                complete_company_name.substring(0,complete_company_name.length() - 2);
                String complete_company_code=total_company_code.toString();
                complete_company_code.substring(0,complete_company_code.length() - 2);

                binding.etCompanyName.setText(complete_company_name);
                binding.etCompanyCode.setText(complete_company_code);

//                binding.etCompanyName.setText(selectedMangers.get(selectedMangers.size() - 1).getLot_manager());
//                binding.etCompanyCode.setText(selectedMangers.get(selectedMangers.size() - 1).getManager_id());
                chatUserSettings = new ChatConfSettingData();
                fetchChatSetting(0);
            }
            if (selectedUsers != null) {

                StringBuilder total_username = new StringBuilder();
                StringBuilder total_email = new StringBuilder();
                StringBuilder total_role = new StringBuilder();

                for(int i=0;i<selectedUsers.size();i++)
                {

                    if (!TextUtils.isEmpty(selectedUsers.get(i).getUser_name()))
                    {
                        total_username.append(selectedUsers.get(i).getUser_name()+" , ");
                    }
                    else
                    {
                        total_email.append(selectedUsers.get(i).getEmail()+" , ");
                    }
                    total_role.append(selectedUsers.get(i).getRole()+" , ");
                }

                String complete_username = null;
                if (!TextUtils.isEmpty(total_username.toString()))
                {
                    complete_username = total_username.toString();
                    complete_username=complete_username.substring(0,complete_username.length() - 2);
                }

                String complete_email = null;
                if (!TextUtils.isEmpty(total_email.toString()))
                {
                    complete_email = total_email.toString();
                    complete_email=complete_email.substring(0,complete_email.length() - 2);
                }

                String complete_role = total_role.toString();
                complete_role=complete_role.substring(0,complete_role.length() - 2);

                String combine_username_email = null;
                if (complete_username != null && !complete_username.isEmpty() && !complete_username.equals("null") && complete_email !=null && !complete_email.isEmpty() && !complete_email.equals("null"))
                {
                    combine_username_email = complete_username+" , "+complete_email;
                }
                else if (complete_username !=null && !complete_username.isEmpty() && !complete_username.equals("null"))
                {
                    combine_username_email = complete_username;
                }
                else if (complete_email != null && !complete_email.isEmpty() && !complete_email.equals("null") )
                {
                    combine_username_email = complete_email;
                }

                binding.etCompanyName.setText(combine_username_email);
                binding.etCompanyCode.setText(complete_role);

//                if (!TextUtils.isEmpty(selectedUsers.get(selectedUsers.size() - 1).getUser_name()))
//                    binding.etCompanyName.setText(selectedUsers.get(selectedUsers.size() - 1).getUser_name());
//                else
//                    binding.etCompanyName.setText(selectedUsers.get(selectedUsers.size() - 1).getEmail());
//                binding.etCompanyCode.setText(selectedUsers.get(selectedUsers.size() - 1).getRole());
                chatUserSettings = new ChatConfSettingData();
                fetchChatSetting(0);
            }
        }
    }

    private void showEnter_txt(String title, String hintText, TextView etTextView, String selectedEditText, int inputType, boolean isParkingFees) {
        RelativeLayout viewGroup = (RelativeLayout) getActivity().findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.enter_txt, viewGroup, false);
        final PopupWindow popup = new PopupWindow(getActivity());
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(true);
        popup.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        popup.setFocusable(true);
        popup.update();
        popup.showAtLocation(layout, Gravity.TOP, 0, 0);

        final EditText edit_text;
        final TextView text_title, tvPaste;
        final Button btn_done;
        RelativeLayout btn_cancel;
        LinearLayout rl_edit;
        ImageView ivPaste;

        tvPaste = (TextView) layout.findViewById(R.id.tvPaste);
        ivPaste = (ImageView) layout.findViewById(R.id.ivPaste);
        edit_text = (EditText) layout.findViewById(R.id.edit_nu);
        text_title = (TextView) layout.findViewById(R.id.txt_title);
        rl_edit = (LinearLayout) layout.findViewById(R.id.rl_edit);

        text_title.setText(title);
        edit_text.setHint(hintText);
        edit_text.setInputType(inputType);
        if (!TextUtils.isEmpty(etTextView.getText()))
            edit_text.setText(etTextView.getText());
        btn_cancel = (RelativeLayout) layout.findViewById(R.id.close);
        btn_done = (Button) layout.findViewById(R.id.btn_done);

        edit_text.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        edit_text.setFocusable(true);
        edit_text.setSelection(edit_text.getText().length());
        edit_text.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            if (imm.isAcceptingText()) {
                Log.d(TAG, "Software Keyboard was shown");
            } else {
                Log.d(TAG, "Software Keyboard was not shown");
            }
        });
        rl_edit.setOnClickListener(v -> tvPaste.setVisibility(View.GONE));
        edit_text.setOnLongClickListener(v -> {
            tvPaste.setVisibility(View.VISIBLE);
            return true;
        });
        tvPaste.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData pData = clipboard.getPrimaryClip();
            if (pData != null) {
                ClipData.Item item = pData.getItemAt(0);
                String txtpaste = item.getText().toString();
                edit_text.getText().insert(edit_text.getSelectionStart(), txtpaste);
                edit_text.setText(edit_text.getText()+txtpaste);
            }
        });
        ivPaste.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData pData = clipboard.getPrimaryClip();
            if (pData != null) {
                ClipData.Item item = pData.getItemAt(0);
                String txtpaste = item.getText().toString();
                edit_text.getText().insert(edit_text.getSelectionStart(), txtpaste);
                edit_text.setText(edit_text.getText()+txtpaste);

            }
        });
        btn_done.setOnClickListener(v -> {
            if (imm.isAcceptingText()) {
                Log.d(TAG, "DONE: Software Keyboard was shown");
            } else {
                Log.d(TAG, "DONE: Software Keyboard was not shown");
            }
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            etTextView.setText(edit_text.getText().toString());
            popup.dismiss();
        });
        btn_cancel.setOnClickListener(v -> {
            InputMethodManager imm1 = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm1.hideSoftInputFromWindow(edit_text.getWindowToken(), 0);
            String txt = edit_text.getText().toString();
            popup.dismiss();
        });
    }

    public void fetchChatSetting(int position) {
        binding.rlProgressbar.setVisibility(View.VISIBLE);
        String url = "";
        if (isMultiple) {
            Log.e(TAG, "pos: " + position);
            if (selectedMangers != null)
                url = "((company_id=" + selectedMangers.get(position).getId() + ")AND(company_type_id=" + selectedMangers.get(position).getManager_type_id() + "))";
            if (selectedUsers != null)
                url = "(user_id=" + selectedUsers.get(position).getUser_id() + ")";
        } else {
            if (selectedManger != null)

            url = "((company_id=" + selectedManger.getId() + ")AND(company_type_id=" + selectedManger.getManager_type_id() + "))";
            if (selectedUser != null)
                url = "(user_id=" + selectedUser.getUser_id() + ")";
        }
        Call<ChatConfSettings> call = apiInterface.getUserChatSetting(url);
        call.enqueue(new Callback<ChatConfSettings>() {
            @Override
            public void onResponse(Call<ChatConfSettings> call, Response<ChatConfSettings> response) {
                if (response.code() == 403) {
                    Toast.makeText(getActivity(), "There is something went to wrong! Please try again", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(() -> {
                        fetchChatSetting(position);
                    }, 1000);
                }
                if (response.isSuccessful()) {
                    int pos = position;
                    Log.e(TAG, " response" + response.code() + " " + response.message() + " \n " + new Gson().toJson(response.body()));
                    List<ChatConfSettingData> chatConfSettings = response.body().getResource();
                    if (chatConfSettings.size() > 0) {
                        chatUserSettings = chatConfSettings.get(0);
                        if (isMultiple) {
                            if (selectedMangers != null)
                                selectedMangers.get(pos).setSelected_id(chatUserSettings.getId());
                            else if (selectedUsers != null)
                                selectedUsers.get(pos).setSelected_id(chatUserSettings.getId());
                        }
                    } else if (isMultiple) {
                        if (selectedMangers != null)
                            selectedMangers.get(pos).setSelected_id(0);
                        else if (selectedUsers != null)
                            selectedUsers.get(pos).setSelected_id(0);
                    }
                    if (isMultiple) {
                        if (selectedUsers != null) {
                            if (pos == selectedUsers.size() - 1)
                                binding.rlProgressbar.setVisibility(View.GONE);
                            else {
                                pos += 1;
                                fetchChatSetting(pos);
                            }
                        } else if (selectedMangers != null) {
                            if (pos == selectedMangers.size() - 1)
                                binding.rlProgressbar.setVisibility(View.GONE);
                            else {
                                pos += 1;
                                fetchChatSetting(pos);
                            }
                        }
                    } else {
                        binding.rlProgressbar.setVisibility(View.GONE);
                        if (chatConfSettings.size() == 0) {
                            isEdit = false;
                            chatUserSettings = new ChatConfSettingData();
                            if (selectedManger != null) {
                                chatUserSettings.setCompany_id(selectedManger.getId());
                                chatUserSettings.setCompany_type_id(selectedManger.getManager_type_id());
                                chatUserSettings.setCompany_name(selectedManger.getLot_manager());
                                chatUserSettings.setCompany_code(selectedManger.getManager_id());

                                binding.etCompanyName.setText(selectedManger.getLot_manager());
                                binding.etCompanyCode.setText(selectedManger.getManager_id());
                            }
                            if (selectedUser != null) {
                                chatUserSettings.setUser_id(selectedUser.getUser_id());
                                chatUserSettings.setUser_profile_type(selectedUser.getRole());

                                if (!TextUtils.isEmpty(selectedUser.getUser_name()))
                                    binding.etCompanyName.setText(selectedUser.getUser_name());
                                else binding.etCompanyName.setText(selectedUser.getEmail());
                                binding.etCompanyCode.setText(selectedUser.getRole());
                            }
                            binding.llChatOnLocal.setVisibility(View.GONE);
                            binding.llChatOnCloud.setVisibility(View.GONE);
                            binding.llChatCloudDataSize.setVisibility(View.GONE);
                        } else {
                            isEdit = true;
                            Log.e(TAG, " " + chatUserSettings.getCompany_name() + " " + chatUserSettings.getCompany_code());
                            if (selectedManger != null) {
                                if (!TextUtils.isEmpty(chatUserSettings.getCompany_code())) {
                                    binding.etCompanyName.setText(chatUserSettings.getCompany_name());
                                    binding.etCompanyCode.setText(chatUserSettings.getCompany_code());
                                }
                            }
                            if (selectedUser != null) {
                                if (chatUserSettings.getUser_id() == selectedUser.getUser_id()) {
                                    if (!TextUtils.isEmpty(selectedUser.getUser_name()))
                                        binding.etCompanyName.setText(selectedUser.getUser_name());
                                    else binding.etCompanyName.setText(selectedUser.getEmail());
                                    binding.etCompanyCode.setText(selectedUser.getRole());
                                }
                            }
//Call chat config
                            binding.switchAttachDoc.setChecked(chatUserSettings.isAttach_doc());
                            binding.switchAttachImage.setChecked(chatUserSettings.isAttach_image());
                            binding.switchAttachAudio.setChecked(chatUserSettings.isAttach_audio());
                            binding.switchAttachVideo.setChecked(chatUserSettings.isAttach_video());
                            binding.switchAttachLiveCamera.setChecked(chatUserSettings.isAttach_live_camera());
                            binding.switchAttachLiveMic.setChecked(chatUserSettings.isAttach_live_mic());
                            binding.switchAttachZip.setChecked(chatUserSettings.isAttach_zip());
                            binding.switchChatAudioRecordingLocal.setChecked(chatUserSettings.isChat_audio_recording_local());
                            binding.switchChatAudioRecordingCloud.setChecked(chatUserSettings.isChat_audio_recording_cloud());
                            binding.switchChatVideoRecordingLocal.setChecked(chatUserSettings.isChat_video_recording_local());
                            binding.switchchatVideoRecordingCloud.setChecked(chatUserSettings.isChat_video_recording_cloud());
                            binding.switchchatFileAttachLocal.setChecked(chatUserSettings.isChat_file_attach_local());
                            binding.switchchatFileAttachCloud.setChecked(chatUserSettings.isChat_file_attach_cloud());
                            binding.switchchatPhotoAttachLocal.setChecked(chatUserSettings.isChat_photo_attach_local());
                            binding.switchchatPhotoAttachCloud.setChecked(chatUserSettings.isChat_photo_attach_cloud());
                            binding.switchAllowSharing.setChecked(chatUserSettings.isAllow_share());
                            binding.switchAllowDownload.setChecked(chatUserSettings.isAllow_download());
                            binding.switchAllowCopying.setChecked(chatUserSettings.isAllow_copying());
                            binding.switchAllowForward.setChecked(chatUserSettings.isAllow_forward());
                            binding.switchLocalData.setChecked(chatUserSettings.isLocal_data());
                            binding.switchCloudData.setChecked(chatUserSettings.isCloud_data());

                            binding.llChatOnLocal.setVisibility(chatUserSettings.isLocal_data() ? View.VISIBLE : View.GONE);
                            binding.llChatOnCloud.setVisibility(chatUserSettings.isCloud_data() ? View.VISIBLE : View.GONE);
                            binding.llChatCloudDataSize.setVisibility(chatUserSettings.isCloud_data() ? View.VISIBLE : View.GONE);

                            binding.etCloudAttachSizeTotalLimit.setText(String.valueOf(chatUserSettings.getCloud_attach_size_total_limit()));
                        }
                    }
                    isInit = false;
                }
            }

            @Override
            public void onFailure(Call<ChatConfSettings> call, Throwable t) {
                Log.e(TAG, " response" + new Gson().toJson(t));
                binding.rlProgressbar.setVisibility(View.GONE);
            }
        });
    }

    public void selectedCompanyiesUsers() {
        if (selectedMangers != null || selectedUsers != null) {
            LinearLayout viewGroup = (LinearLayout) getActivity().findViewById(R.id.popup);
            LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View layout = layoutInflater.inflate(R.layout.popup_selected_company_user_list, viewGroup, false);
            popupmanaged = new PopupWindow(getActivity());
            popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
            popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
            popupmanaged.setContentView(layout);
            popupmanaged.setBackgroundDrawable(null);
            popupmanaged.setFocusable(true);
            popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);
            RecyclerView rvMeetingId;
            TextView txt_title;
            ImageView ivClose;

            txt_title = (TextView) layout.findViewById(R.id.tvTitle);
            rvMeetingId = (RecyclerView) layout.findViewById(R.id.rvMeetingId);
            ivClose = (ImageView) layout.findViewById(R.id.ivClose);

            rvMeetingId.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            if (selectedMangers != null) {
                txt_title.setText("Selected Companies");
                partnerAdapter = new PartnerAdapter();
                rvMeetingId.setAdapter(partnerAdapter);
            } else if (selectedUsers != null) {
                txt_title.setText("Selected Users");
                userAdapter = new UserAdapter();
                rvMeetingId.setAdapter(userAdapter);
            }
            ivClose.setOnClickListener(v1 -> popupmanaged.dismiss());
        } else {
            Toast.makeText(myApp, "You don't have any previous meeting.", Toast.LENGTH_SHORT).show();
        }
    }

    public void add_editUserChatSetting(ChatConfSettingData chatUserSettings, boolean isUserEdit) {
        binding.rlProgressbar.setVisibility(View.VISIBLE);
        JsonObject json = new JsonObject();
        if (isUserEdit)
            json.addProperty("id", chatUserSettings.getId());
        else {
            json.addProperty("datetime", "");
            json.addProperty("company_type_id", chatUserSettings.getCompany_type_id());
            json.addProperty("company_id", chatUserSettings.getCompany_id());
            json.addProperty("company_code", chatUserSettings.getCompany_code());
            json.addProperty("company_name", chatUserSettings.getCompany_name());
            json.addProperty("user_profile_type", chatUserSettings.getUserProfile_type());
            json.addProperty("user_id", chatUserSettings.getUser_id());
            json.addProperty("app_name", "EventEzlyAdmin Plus");
            json.addProperty("app_id", 23);
            json.addProperty("user_access_level", Constants.COMPANY_USER);
        }
        json.addProperty("attach_live_camera", chatUserSettings.isAttach_live_camera());
        json.addProperty("attach_live_mic", chatUserSettings.isAttach_live_mic());
        json.addProperty("attach_zip", chatUserSettings.isAttach_zip());
        json.addProperty("attach_doc", chatUserSettings.isAttach_doc());
        json.addProperty("attach_video", chatUserSettings.isAttach_video());
        json.addProperty("attach_audio", chatUserSettings.isAttach_audio());
        json.addProperty("attach_image", chatUserSettings.isAttach_image());

        json.addProperty("chat_audio_recording_local", chatUserSettings.isChat_audio_recording_local());
        json.addProperty("chat_audio_recording_cloud", chatUserSettings.isChat_audio_recording_cloud());
        json.addProperty("chat_video_recording_local", chatUserSettings.isChat_video_recording_local());
        json.addProperty("chat_video_recording_cloud", chatUserSettings.isChat_video_recording_cloud());
        json.addProperty("chat_file_attach_local", chatUserSettings.isChat_file_attach_local());
        json.addProperty("chat_file_attach_cloud", chatUserSettings.isChat_file_attach_cloud());
        json.addProperty("chat_photo_attach_local", chatUserSettings.isChat_photo_attach_local());
        json.addProperty("chat_photo_attach_cloud", chatUserSettings.isChat_photo_attach_cloud());
        json.addProperty("preview_attach_video", chatUserSettings.isPreview_attach_video());
        json.addProperty("preview_attach_audio", chatUserSettings.isPreview_attach_audio());
        json.addProperty("preview_attach_image", chatUserSettings.isPreview_attach_image());
        json.addProperty("play_attached_video", chatUserSettings.isPlay_attached_video());
        json.addProperty("play_attach_audio", chatUserSettings.isPlay_attach_audio());
        json.addProperty("play_attach_image", chatUserSettings.isPlay_attach_image());
        json.addProperty("play_attach_slide", chatUserSettings.isPlay_attach_slide());
        json.addProperty("cloud_attach_size_total_limit", chatUserSettings.getCloud_attach_size_total_limit());
        json.addProperty("allow_share", chatUserSettings.isAllow_share());
        json.addProperty("allow_download", chatUserSettings.isAllow_download());
        json.addProperty("allow_copying", chatUserSettings.isAllow_copying());
        json.addProperty("allow_forward", chatUserSettings.isAllow_forward());
        json.addProperty("local_data", chatUserSettings.isLocal_data());
        json.addProperty("cloud_data", chatUserSettings.isCloud_data());
        Call<Object> call = null;
        if (isUserEdit) call = apiInterface.updateUserChatSettings(json);
        else call = apiInterface.addUserChatSettings(json);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                int gid = 0;
                Log.e(TAG, "editUserChat: Code: " + response.code() + new Gson().toJson(response.body()));
                if (response.code() == 403) {
                    Toast.makeText(getActivity(), "There is something went to wrong! Please try again", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(() -> {
                        add_editUserChatSetting(chatUserSettings, isUserEdit);
                    }, 1000);
                }
                if (response.isSuccessful()) {
                    try {
                        JSONObject json = new JSONObject(new Gson().toJson(response.body()));
                        JSONArray array = json.getJSONArray("resource");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject c = array.getJSONObject(i);
                            gid = c.getInt("id");
                            Log.e(TAG, " MsgId: " + gid);
//                            getActivity().onBackPressed();
                        }
                        if (isMultiple) {
                            if (tempChatConfSetting.size() > 0) {
                                if (selectedMangers != null)
                                    selectedMangers.get(pos).setSelected_id(gid);
                                if (selectedUsers != null)
                                    selectedUsers.get(pos).setSelected_id(gid);
                                pos += 1;
                                tempChatConfSetting.remove(0);
                                if (tempChatConfSetting.size() == 0) {
                                    binding.rlProgressbar.setVisibility(View.GONE);
                                    Toast.makeText(myApp, "Configuration Save", Toast.LENGTH_LONG).show();
                                } else {
                                    Log.e(TAG, "ChatCongif: " + tempChatConfSetting.get(0).getId());
                                    add_editUserChatSetting(tempChatConfSetting.get(0), tempChatConfSetting.get(0).getId() != 0);
                                }
                            }
                        } else {
                            if (!isEdit) {
                                chatUserSettings.setId(gid);
                                isEdit = true;
                            }
                            binding.rlProgressbar.setVisibility(View.GONE);
                            Toast.makeText(myApp, "Configuration Save", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.e(TAG, "ADDChatUser FailureMsg");
                Log.e(TAG, "error  response" + new Gson().toJson(t));
                call.cancel();
            }
        });
    }

    private class PartnerAdapter extends RecyclerView.Adapter<PartnerAdapter.PartnerHolder> {

        @NonNull
        @Override
        public PartnerAdapter.PartnerHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new PartnerAdapter.PartnerHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_partner_new_manager,
                    viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull PartnerAdapter.PartnerHolder holder, int i) {
            Manager manager = selectedMangers.get(i);
            if (manager != null) {
                holder.ivSelectCompany.setVisibility(View.GONE);
                holder.tvName.setText(manager.getLot_manager());
                holder.tvAddress.setText(manager.getAddress());
                Glide.with(getActivity()).load(manager.getTownship_logo()).into(holder.ivPartner);
            }
        }

        @Override
        public int getItemCount() {
            return selectedMangers.size();
        }

        class PartnerHolder extends RecyclerView.ViewHolder {
            CircleImageView ivPartner;
            ImageView ivSelectCompany;
            TextView tvName, tvAddress;

            PartnerHolder(@NonNull View itemView) {
                super(itemView);
                ivSelectCompany = itemView.findViewById(R.id.ivSelectedCompany);
                ivPartner = itemView.findViewById(R.id.ivPartner);
                tvName = itemView.findViewById(R.id.tvName);
                tvAddress = itemView.findViewById(R.id.tvAddress);
            }
        }
    }

    public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyUserViewHolder> {
        @NonNull
        @Override
        public UserAdapter.MyUserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new UserAdapter.MyUserViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_users, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull UserAdapter.MyUserViewHolder holder, int position) {
            holder.tvUserId.setText("" + selectedUsers.get(position).getUser_id());

            if (!TextUtils.isEmpty(selectedUsers.get(position).getFname()) || !TextUtils.isEmpty(selectedUsers.get(position).getLname()))
                holder.tvUserName.setText(selectedUsers.get(position).getFname() + " " + selectedUsers.get(position).getLname());
            else holder.tvUserName.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(selectedUsers.get(position).getMobile()))
                holder.tvUserCode.setText(selectedUsers.get(position).getMobile());
            else holder.tvUserCode.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(selectedUsers.get(position).getEmail()))
                holder.tvUserEmail.setText(selectedUsers.get(position).getEmail());
            else holder.tvUserEmail.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(selectedUsers.get(position).getRole()))
                holder.tvUserRole.setText(selectedUsers.get(position).getRole());
            else holder.tvUserRole.setVisibility(View.GONE);
        }

        @Override
        public int getItemCount() {
            return selectedUsers.size();
        }

        public class MyUserViewHolder extends RecyclerView.ViewHolder {
            TextView tvUserId, tvUserName, tvUserCode, tvUserEmail, tvUserRole;

            public MyUserViewHolder(@NonNull View itemView) {
                super(itemView);
                tvUserId = itemView.findViewById(R.id.tvUserId);
                tvUserName = itemView.findViewById(R.id.tvUserName);
                tvUserCode = itemView.findViewById(R.id.tvUserContact);
                tvUserEmail = itemView.findViewById(R.id.tvUserEmail);
                tvUserRole = itemView.findViewById(R.id.tvUserRole);
            }
        }
    }
}