package com.softmeasures.eventezlyadminplus.frament.g_youtube;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.DialogAlertBinding;
import com.softmeasures.eventezlyadminplus.databinding.FragGYoutubeScheduleStreamBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

public class GYoutubeScheduleStreamFragment extends BaseFragment {

    private FragGYoutubeScheduleStreamBinding binding;
    private Calendar selectedStartCalender, selectedEndCalender;

    private JSONObject jsonObjectBroadcast;
    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_g_youtube_schedule_stream, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {

        binding.etStartTime.setOnClickListener(view -> showDialogSelectStartDateTime());

        binding.etEndTime.setOnClickListener(view -> showDialogSelectEndDateTime());

        binding.tvBtnCreate.setOnClickListener(view -> {
            if (isValid()) {
                callCreateStreamAPI();
            }
        });

    }

    private void callCreateStreamAPI() {
        progressDialog.show();
        AndroidNetworking.post("https://www.googleapis.com/youtube/v3/liveBroadcasts")
                .addHeaders("Authorization", "Bearer " + myApp.getOAuth())
                .addHeaders("Content-Type", "application/json")
                .addApplicationJsonBody(jsonObjectBroadcast)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.e("#DEBUG", "   callCreateStreamAPI: onResponse: " + response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Log.e("#DEBUG", "   callCreateStreamAPI: onError: " + anError.getMessage());
                        showAlert(anError.getErrorBody());

                    }
                });
    }

    private void showAlert(String error) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogAlertBinding alertBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.dialog_alert, null, false);
        builder.setView(alertBinding.getRoot());

        AlertDialog alertDialog = builder.create();
        try {
            alertBinding.tvMessage.setText(new JSONObject(error).getJSONObject("error").getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        alertBinding.btnOK.setOnClickListener(view -> alertDialog.dismiss());

        alertDialog.show();
    }

    private boolean isValid() {

        if (TextUtils.isEmpty(binding.etTitle.getText())) {
            binding.etTitle.setError("Required");
            binding.etTitle.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(binding.etStartTime.getText())) {
            binding.etStartTime.setError("Required");
            binding.etStartTime.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(binding.etEndTime.getText())) {
            binding.etEndTime.setError("Required");
            binding.etEndTime.requestFocus();
            return false;
        }

        try {

            JSONObject jsonObjectSnippet = new JSONObject();
            jsonObjectSnippet.put("title", binding.etTitle.getText().toString());
            jsonObjectSnippet.put("scheduledStartTime", binding.etStartTime.getText().toString());
            jsonObjectSnippet.put("scheduledEndTime", binding.etEndTime.getText().toString());

            JSONObject jsonObjectStatus = new JSONObject();
            jsonObjectStatus.put("privacyStatus", binding.spPrivacyType.getSelectedItem().toString());

            jsonObjectBroadcast = new JSONObject();
            jsonObjectBroadcast.put("snippet", jsonObjectSnippet);
            jsonObjectBroadcast.put("status", jsonObjectStatus);

            Log.e("#DEBUG", "   CreateStream: jsonObjectBroadcast: " + jsonObjectBroadcast.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }


        return true;
    }

    private void showDialogSelectStartDateTime() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
            selectedStartCalender.set(Calendar.YEAR, i);
            selectedStartCalender.set(Calendar.MONTH, i1);
            selectedStartCalender.set(Calendar.DAY_OF_MONTH, i2);

            int mHour = c.get(Calendar.HOUR_OF_DAY);
            int mMinute = c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                    (view1, hourOfDay, minute) -> {
                        selectedStartCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        selectedStartCalender.set(Calendar.MINUTE, minute);

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
                        binding.etStartTime.setText(simpleDateFormat.format(selectedStartCalender.getTime()));
                        binding.etStartTime.setError(null);

                    }, mHour, mMinute, false);
            timePickerDialog.show();

        }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void showDialogSelectEndDateTime() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
            selectedEndCalender.set(Calendar.YEAR, i);
            selectedEndCalender.set(Calendar.MONTH, i1);
            selectedEndCalender.set(Calendar.DAY_OF_MONTH, i2);

            int mHour = c.get(Calendar.HOUR_OF_DAY);
            int mMinute = c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                    (view1, hourOfDay, minute) -> {
                        selectedEndCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        selectedEndCalender.set(Calendar.MINUTE, minute);

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
                        binding.etEndTime.setText(simpleDateFormat.format(selectedEndCalender.getTime()));
                        binding.etEndTime.setError(null);

                    }, mHour, mMinute, false);
            timePickerDialog.show();

        }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    protected void initViews(View v) {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");

        selectedStartCalender = Calendar.getInstance();
        selectedEndCalender = Calendar.getInstance();

        ArrayAdapter<String> eventOrgnAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,
                Arrays.asList(getResources().getStringArray(R.array.privacy_type)));
        binding.spPrivacyType.setAdapter(eventOrgnAdapter);

    }
}
