package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;

import com.softmeasures.eventezlyadminplus.MyApplication;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.java.Array_class_static;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.PaymentOption;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1.commercial_loc_code;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1.commercial_title;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step3.custome_notices;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step3.duation_unit;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step3.end_time;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step3.isclickeditrules;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step3.max_duation;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step3.parking_rules_id;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step3.parking_rules_location_code;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step3.pricing;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step3.pricing_duation;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step3.start_time;

public class frg_commercial_edit_add_step4 extends BaseFragment {

    RelativeLayout rl_progressbar;
    TextView txt_commercial_day, txt_commericali_pricing_duration;
    RelativeLayout rl_commercial_day_list;
    ImageView img_day_commercial_list_close;
    Spinner sp_commercial_duration, sp_commercial_prining_duatiuon, sp_commercial_start_hour, sp_commercial_end_hour;
    String duration_unit[] = {"Select duration unit", "Minute", "Hour", "Day", "Week", "Month"};
    EditText edit_commercial_princing, edit_commercial_notice, edit_commercial_max;
    String commercial_pricing, commecial_pricing_duration, commerical_duation_unit = "", commercial_end_hours, commercial_start_hours, commercial_notic, commercial_max_duartion;
    Switch sw_commercial_24hr, sw_commerical_active;
    boolean commerical_24hr = true, commercial_active = true;
    LinearLayout ll_commercial_r_start_time, ll_commercial_r_end_time;
    TextView txt_commerical_save_rules, txt_commercial_r_start_time, txt_commercial_r_end_time;
    ListView list_commercial_day_lits;
    int checkedisone = 0;
    ArrayList<item> day_list = new ArrayList<item>();
    boolean is_direct_edit = false;
    RelativeLayout rl_main;

    //Additional rules START ############
    private Switch switchReservation, switchPrePayment, switchPostPayment, switchCancellation,
            switchVerifyLotsAvailability, switchCountReservedSpots;
    public static boolean isReservationAllowed = false, isPrePaymentRequired = true,
            isPostPaymentAllowed = false, isCancellationAllowed = false,
            isVerifyLotAvailability = false, isCountReservedSpot = false;
    private EditText etCancellationCharge, etPostPayLateFeesReservation, etPostPayLateFees,
            etNoOfReservableSpots, etPostPayFeesReservation, etPostPayFees;
    private LinearLayout llCancellationCharge, llPostPaymentTermsReservation,
            llPostPayLateFeeReservation, llPostPayLateFee, llPostPayTerms, llReservationOption,
            llNoOfReservableSpots, llPostPayFeeReservation, llPostPayFee;
    public static String cancellationCharge = "", postPayLateFeesReservation = "",
            postPayLateFees = "", postPayTerm = "", postPayTermReservation = "",
            postPayFeesReservation = "", noOfReservationSpots = "", postPayFees = "";
    private AppCompatSpinner spPostPaymentTermsReservation, spPostPaymentTerms;
    private ArrayList<PaymentOption> paymentOptions = new ArrayList<>();
    private ArrayAdapter<PaymentOption> postPayTermsAdapter;
    private ArrayAdapter<PaymentOption> postPayTermsReservationAdapter;
    //Additional rules END ############

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.commercial_parking_rules_add_edit, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        new fetchPaymentOptions().execute();
    }

    public void setcommercial_pricing_duation() {
        ArrayAdapter<String> spinnerArrayAdapter12 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.pricing_duration_12);
        ArrayAdapter<String> spinnerArrayAdapter24 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.pricing_duration_24);
        ArrayAdapter<String> spinnerArrayAdapterminu = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.pricing_duration_minutes);
        ArrayAdapter<String> spinnerArrayAdapterday = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.pricing_duration_day);
        ArrayAdapter<String> spinnerArrayAdapterweek = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.pricing_duration_week);
        ArrayAdapter<String> spinnerArrayAdaptermonth = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.pricing_duration_month);
        if (commerical_duation_unit.equals("Minute")) {
            sp_commercial_prining_duatiuon.setAdapter(spinnerArrayAdapterminu);
            sp_commercial_prining_duatiuon.setSelection(1);
        } else if (commerical_duation_unit.equals("Hour")) {
            if (commerical_24hr) {
                sp_commercial_prining_duatiuon.setAdapter(spinnerArrayAdapter24);
                sp_commercial_prining_duatiuon.setSelection(1);
            } else {
                sp_commercial_prining_duatiuon.setAdapter(spinnerArrayAdapter12);
                sp_commercial_prining_duatiuon.setSelection(1);
            }
        } else if (commerical_duation_unit.equals("Day")) {
            sp_commercial_prining_duatiuon.setAdapter(spinnerArrayAdapterday);
            sp_commercial_prining_duatiuon.setSelection(1);
        } else if (commerical_duation_unit.equals("Week")) {
            sp_commercial_prining_duatiuon.setAdapter(spinnerArrayAdapterweek);
            sp_commercial_prining_duatiuon.setSelection(1);
        } else if (commerical_duation_unit.equals("Month")) {
            sp_commercial_prining_duatiuon.setAdapter(spinnerArrayAdaptermonth);
            sp_commercial_prining_duatiuon.setSelection(1);
        } else {
            sp_commercial_prining_duatiuon.setAdapter(spinnerArrayAdapterminu);
            sp_commercial_prining_duatiuon.setSelection(1);
        }


    }

    @Override
    protected void updateViews() {
        if (isclickeditrules) {
            commerical_duation_unit = duation_unit;
            if (!start_time.equals("null") && !end_time.equals("null")) {
                commerical_24hr = false;
            } else {
                commerical_24hr = true;
            }
        }
        setcommercial_pricing_duation();
        if (isclickeditrules) {
            is_direct_edit = true;
            if (!start_time.equals("null") && !end_time.equals("null")) {
                ll_commercial_r_end_time.setVisibility(View.VISIBLE);
                ll_commercial_r_start_time.setVisibility(View.VISIBLE);
                txt_commercial_r_start_time.setText(start_time);
                txt_commercial_r_end_time.setText(end_time);
                sw_commercial_24hr.setChecked(false);
            } else {
                sw_commercial_24hr.setChecked(true);
                ll_commercial_r_end_time.setVisibility(View.GONE);
                ll_commercial_r_start_time.setVisibility(View.GONE);
            }

            if (duation_unit.equals("Minute")) {
                sp_commercial_duration.setSelection(1);
            } else if (duation_unit.equals("Hour")) {
                sp_commercial_duration.setSelection(2);
            } else if (duation_unit.equals("Day")) {
                sp_commercial_duration.setSelection(3);
            } else if (duation_unit.equals("Week")) {
                sp_commercial_duration.setSelection(4);
            } else if (duation_unit.equals("Month")) {
                sp_commercial_duration.setSelection(5);
            }


            txt_commericali_pricing_duration.setVisibility(View.VISIBLE);
            txt_commericali_pricing_duration.setText(pricing_duation);
            edit_commercial_max.setText(max_duation);
            edit_commercial_princing.setText(pricing);
            edit_commercial_notice.setText(custome_notices);
            sp_commercial_prining_duatiuon.setSelection(0);

            switchCountReservedSpots.setChecked(isCountReservedSpot);
            switchVerifyLotsAvailability.setChecked(isVerifyLotAvailability);
            switchCancellation.setChecked(isCancellationAllowed);
            switchPostPayment.setChecked(isPostPaymentAllowed);
            switchPrePayment.setChecked(isPrePaymentRequired);
            switchReservation.setChecked(isReservationAllowed);
            if (isReservationAllowed) {
                llReservationOption.setVisibility(View.VISIBLE);
            } else llReservationOption.setVisibility(View.GONE);

            if (isPrePaymentRequired) {
                llPostPaymentTermsReservation.setVisibility(View.GONE);
                llPostPayLateFeeReservation.setVisibility(View.GONE);
                llPostPayFeeReservation.setVisibility(View.GONE);
            } else {
                llPostPaymentTermsReservation.setVisibility(View.VISIBLE);
                llPostPayLateFeeReservation.setVisibility(View.VISIBLE);
                llPostPayFeeReservation.setVisibility(View.VISIBLE);
            }

            if (isCancellationAllowed) {
                llCancellationCharge.setVisibility(View.VISIBLE);
            } else llCancellationCharge.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(postPayLateFeesReservation)) {
                etPostPayLateFeesReservation.setText(postPayLateFeesReservation);
            }

            if (!TextUtils.isEmpty(noOfReservationSpots)) {
                etNoOfReservableSpots.setText(noOfReservationSpots);
            }

            if (!TextUtils.isEmpty(postPayFeesReservation)) {
                etPostPayFeesReservation.setText(postPayFeesReservation);
            }

            if (!TextUtils.isEmpty(postPayLateFees)) {
                etPostPayLateFees.setText(postPayLateFees);
            }

            if (!TextUtils.isEmpty(postPayFees)) {
                etPostPayFees.setText(postPayFees);
            }

            if (!TextUtils.isEmpty(cancellationCharge)) {
                etCancellationCharge.setText(cancellationCharge);
            }
        }
    }

    @Override
    protected void setListeners() {
        sp_commercial_duration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                commerical_duation_unit = (String) sp_commercial_duration.getSelectedItem();
                if (!is_direct_edit) {
                    setcommercial_pricing_duation();
                } else {
                    is_direct_edit = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> paren1t) {
            }
        });

        sw_commercial_24hr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ll_commercial_r_end_time.setVisibility(View.GONE);
                    ll_commercial_r_start_time.setVisibility(View.GONE);
                } else {
                    ll_commercial_r_end_time.setVisibility(View.VISIBLE);
                    ll_commercial_r_start_time.setVisibility(View.VISIBLE);
                }
                commerical_24hr = isChecked;
                setcommercial_pricing_duation();
            }
        });

        sp_commercial_start_hour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                commercial_start_hours = (String) sp_commercial_start_hour.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_commercial_end_hour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                commercial_end_hours = (String) sp_commercial_end_hour.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sw_commerical_active.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                commercial_active = isChecked;
            }
        });


        txt_commerical_save_rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                commercial_max_duartion = edit_commercial_max.getText().toString();
                commercial_pricing = edit_commercial_princing.getText().toString();
                commercial_notic = edit_commercial_notice.getText().toString();
                String pricing_duation = txt_commericali_pricing_duration.getText().toString();
                if (commecial_pricing_duration.equals("")) {
                    if (!pricing_duation.equals("")) {
                        commecial_pricing_duration = pricing_duation;
                    }
                }
                if (isCancellationAllowed) {
                    cancellationCharge = etCancellationCharge.getText().toString();
                }
                postPayLateFeesReservation = etPostPayLateFeesReservation.getText().toString();
                postPayLateFees = etPostPayLateFees.getText().toString();
                postPayFeesReservation = etPostPayFeesReservation.getText().toString();
                noOfReservationSpots = etNoOfReservableSpots.getText().toString();
                postPayFees = etPostPayFees.getText().toString();

                if (!commercial_max_duartion.equals("")
                        && !commerical_duation_unit.equals("")
                        && !commercial_pricing.equals("")
                        && !commecial_pricing_duration.equals("Select pricing duration")
                        && !commercial_notic.equals("")) {

                    if (isReservationAllowed) {
                        if (!isPrePaymentRequired) {
                            if (TextUtils.isEmpty(postPayTermReservation)) {
                                showRequiredFieldsDialog();
                                return;
                            } else if (TextUtils.isEmpty(postPayLateFeesReservation)) {
                                showRequiredFieldsDialog();
                                return;
                            }
                        }
                    } else if (isPostPaymentAllowed) {
                        if (TextUtils.isEmpty(postPayTerm)) {
                            showRequiredFieldsDialog();
                            return;
                        } else if (TextUtils.isEmpty(postPayLateFees)) {
                            showRequiredFieldsDialog();
                            return;
                        }
                    }

                    int pric = Integer.parseInt(commecial_pricing_duration);
                    int max_du = Integer.parseInt(commercial_max_duartion);
                    if (max_du >= pric) {
                        if (commerical_24hr) {
                            if (isclickeditrules) {
                                new update_parking_commercial_rules().execute();
                            } else {
                                new add_commercial_paking_rules().execute();
                            }
                        } else {
                            if (!commercial_start_hours.equals("") && !commercial_end_hours.equals("")) {
                                if (isclickeditrules) {
                                    new update_parking_commercial_rules().execute();
                                } else {
                                    new add_commercial_paking_rules().execute();
                                }
                            } else {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                alertDialog.setMessage("Please fill all fields");
                                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                                alertDialog.show();
                            }
                        }
                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setMessage("Pricing duration should be lesser than maximum duration");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("Please fill all fields");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
        });

        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        txt_commercial_day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_commercial_day_list.setVisibility(View.VISIBLE);
            }
        });
        img_day_commercial_list_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_commercial_day_list.setVisibility(View.GONE);
            }
        });

        //Additional rules START ###########
        switchReservation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //Allowed user to park using reservation
                isReservationAllowed = b;
                if (isReservationAllowed) {
                    llReservationOption.setVisibility(View.VISIBLE);
                } else llReservationOption.setVisibility(View.GONE);
            }
        });

        switchPrePayment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //Is pew payment required for parking?
                isPrePaymentRequired = b;
                if (isPrePaymentRequired) {
                    llPostPaymentTermsReservation.setVisibility(View.GONE);
                    llPostPayLateFeeReservation.setVisibility(View.GONE);
                    llPostPayFeeReservation.setVisibility(View.GONE);
                } else {
                    llPostPaymentTermsReservation.setVisibility(View.VISIBLE);
                    llPostPayLateFeeReservation.setVisibility(View.VISIBLE);
                    llPostPayFeeReservation.setVisibility(View.VISIBLE);
                }
            }
        });

        switchPostPayment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //Is post payment is allowed?
                isPostPaymentAllowed = b;
                if (isPostPaymentAllowed) {
                    llPostPayLateFee.setVisibility(View.VISIBLE);
                    llPostPayFee.setVisibility(View.VISIBLE);
                    llPostPayTerms.setVisibility(View.VISIBLE);
                } else {
                    llPostPayLateFee.setVisibility(View.GONE);
                    llPostPayFee.setVisibility(View.GONE);
                    llPostPayTerms.setVisibility(View.GONE);
                }
            }
        });

        switchCancellation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //Is can cancel? if yes- charges for cancellation
                isCancellationAllowed = b;
                if (isCancellationAllowed) {
                    llCancellationCharge.setVisibility(View.VISIBLE);
                } else llCancellationCharge.setVisibility(View.GONE);
            }
        });

        switchVerifyLotsAvailability.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isVerifyLotAvailability = b;
            }
        });

        switchCountReservedSpots.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isCountReservedSpot = b;
            }
        });

        spPostPaymentTerms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    postPayTerm = paymentOptions.get(i).getPayment_option();
                } else {
                    postPayTerm = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spPostPaymentTermsReservation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    postPayTermReservation = paymentOptions.get(i).getPayment_option();
                } else postPayTermReservation = "";
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        //Additional rules END ###########
    }

    private void showRequiredFieldsDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage("Please fill all required fields.");
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    @Override
    protected void initViews(View view) {

        //Additional rules START ###########
        switchReservation = view.findViewById(R.id.switchReservation);
        switchPrePayment = view.findViewById(R.id.switchPrePayment);
        switchPostPayment = view.findViewById(R.id.switchPostPayment);
        switchCancellation = view.findViewById(R.id.switchCancellation);
        switchVerifyLotsAvailability = view.findViewById(R.id.switchVerifyLotsAvailability);
        switchCountReservedSpots = view.findViewById(R.id.switchCountReservedSpots);
        etCancellationCharge = view.findViewById(R.id.etCancellationCharge);
        etPostPayLateFeesReservation = view.findViewById(R.id.etPostPayLateFeesReservation);
        etPostPayLateFees = view.findViewById(R.id.etPostPayLateFees);
        etNoOfReservableSpots = view.findViewById(R.id.etNoOfReservableSpots);
        etPostPayFeesReservation = view.findViewById(R.id.etPostPayFeesReservation);
        etPostPayFees = view.findViewById(R.id.etPostPayFees);
        spPostPaymentTermsReservation = view.findViewById(R.id.spPostPaymentTermsReservation);
        spPostPaymentTerms = view.findViewById(R.id.spPostPaymentTerms);
        llCancellationCharge = view.findViewById(R.id.llCancellationCharge);
        llCancellationCharge.setVisibility(View.GONE);
        llPostPaymentTermsReservation = view.findViewById(R.id.llPostPaymentTermsReservation);
        llPostPayLateFeeReservation = view.findViewById(R.id.llPostPayLateFeeReservation);
        llPostPayLateFee = view.findViewById(R.id.llPostPayLateFee);
        llPostPayTerms = view.findViewById(R.id.llPostPayTerms);
        llReservationOption = view.findViewById(R.id.llReservationOption);
        llNoOfReservableSpots = view.findViewById(R.id.llNoOfReservableSpots);
        llPostPayFeeReservation = view.findViewById(R.id.llPostPayFeeReservation);
        llPostPayFee = view.findViewById(R.id.llPostPayFee);

        postPayTermsAdapter = new ArrayAdapter<PaymentOption>(getActivity(), android.R.layout.simple_spinner_item, paymentOptions);
        postPayTermsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPostPaymentTerms.setAdapter(postPayTermsAdapter);

        postPayTermsReservationAdapter = new ArrayAdapter<PaymentOption>(getActivity(), android.R.layout.simple_spinner_item, paymentOptions);
        postPayTermsReservationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPostPaymentTermsReservation.setAdapter(postPayTermsReservationAdapter);

        //Additional rules END ###########

        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        txt_commercial_day = (TextView) view.findViewById(R.id.txt_commercial_day);
        rl_commercial_day_list = (RelativeLayout) view.findViewById(R.id.rl_list_of_day_commercial);
        sp_commercial_duration = (Spinner) view.findViewById(R.id.sp_commercial_duration_unit);
        sp_commercial_prining_duatiuon = (Spinner) view.findViewById(R.id.sp_commercial_pricing_unit);
        sp_commercial_start_hour = (Spinner) view.findViewById(R.id.sp_commercial_start_hour);
        sp_commercial_end_hour = (Spinner) view.findViewById(R.id.sp_commercial_end_hour);
        edit_commercial_princing = (EditText) view.findViewById(R.id.edit_commercial_pricing);
        ll_commercial_r_start_time = (LinearLayout) view.findViewById(R.id.ll_commercial_r_start_time);
        ll_commercial_r_end_time = (LinearLayout) view.findViewById(R.id.ll_commercial_r_end_time);
        edit_commercial_notice = (EditText) view.findViewById(R.id.edit_commerical_custom_notice);
        edit_commercial_max = (EditText) view.findViewById(R.id.edit_commercial_max);
        txt_commerical_save_rules = (TextView) view.findViewById(R.id.txt_parking_commercial_rules_save);
        txt_commercial_r_end_time = (TextView) view.findViewById(R.id.text_commercial_start_time);
        txt_commercial_r_start_time = (TextView) view.findViewById(R.id.text_commercial_end_time);
        txt_commericali_pricing_duration = (TextView) view.findViewById(R.id.txt_commericali_pricing_duration);
        list_commercial_day_lits = (ListView) view.findViewById(R.id.list_commercial_day);
        rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        sw_commercial_24hr = (Switch) view.findViewById(R.id.switch_commercila_24hr);
        sw_commerical_active = (Switch) view.findViewById(R.id.switch_Commerical_active);
        img_day_commercial_list_close = (ImageView) view.findViewById(R.id.img_commercial_day_list_close);

        ArrayAdapter<String> sp_private1_start_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.start_hour_array);
        ArrayAdapter<String> sp_private1_end_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.end_hour_array);
        sp_commercial_start_hour.setAdapter(sp_private1_start_adapter);
        sp_commercial_start_hour.setSelection(1);
        sp_commercial_end_hour.setAdapter(sp_private1_end_adapter);
        sp_commercial_end_hour.setSelection(24);

        ArrayAdapter<String> spinnerArrayAdapter111 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, duration_unit);
        sp_commercial_duration.setAdapter(spinnerArrayAdapter111);
        sp_commercial_prining_duatiuon.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                commecial_pricing_duration = (String) sp_commercial_prining_duatiuon.getSelectedItem();
                if (position != 0) {
                    txt_commericali_pricing_duration.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> paren1t) {
            }
        });

        prepareDayTypes();

    }

    private void prepareDayTypes() {
        item i1 = new item();
        i1.setThis_day("EVERYDAY");
        i1.setIsslected(true);
        day_list.add(i1);

        item i2 = new item();
        i2.setThis_day("WEEKDAYS");
        i2.setIsslected(false);
        day_list.add(i2);

        item i3 = new item();
        i3.setThis_day("WEEKENDS");
        i3.setIsslected(false);
        day_list.add(i3);

        item i4 = new item();
        i4.setThis_day("MON");
        i4.setIsslected(false);
        day_list.add(i4);

        item i5 = new item();
        i5.setThis_day("TUE");
        i5.setIsslected(false);
        day_list.add(i5);


        item i6 = new item();
        i6.setThis_day("WED");
        i6.setIsslected(false);
        day_list.add(i6);

        item i7 = new item();
        i7.setThis_day("THD");
        i7.setIsslected(false);
        day_list.add(i7);

        item i8 = new item();
        i8.setThis_day("FRI");
        i8.setIsslected(false);
        day_list.add(i8);

        item i9 = new item();
        i9.setThis_day("SAT");
        i9.setIsslected(false);
        day_list.add(i9);
        list_commercial_day_lits.setAdapter(new CustomAdapterDaycommercial(getActivity(), day_list, getResources()));
    }

    //update commercial parking rules.....................
    public class update_parking_commercial_rules extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        JSONObject json12 = null;
        String walleturl = "_table/google_parking_rules";
        String id = "";
        String start_time = txt_commercial_r_start_time.getText().toString();
        String end_time = txt_commercial_r_end_time.getText().toString();

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("location_code", parking_rules_location_code);
            jsonValues1.put("id", parking_rules_id);
            if (commercial_title.equals("")) {
                commercial_title = "unknown location";
            }
            if (commerical_24hr) {
                jsonValues1.put("start_time", "12:00 AM");
                jsonValues1.put("end_time", "11:59 PM");
                jsonValues1.put("start_hour", "0");
                jsonValues1.put("end_hour", "24");
                jsonValues1.put("time_rule", "24 HRS");
            } else {
                jsonValues1.put("start_time", "");
                jsonValues1.put("end_time", "");
                jsonValues1.put("time_rule", start_time + "-" + end_time);
                String txts = start_time;
                String txte = end_time;
                String strathor = txts.substring(0, txts.indexOf(":"));
                String endhr = txte.substring(0, txte.indexOf(":"));
                jsonValues1.put("start_hour", strathor);
                jsonValues1.put("end_hour", endhr);
            }
            jsonValues1.put("location_name", commercial_title);
            if (commercial_active) {
                jsonValues1.put("active", 1);
            } else {
                jsonValues1.put("active", 0);
            }

            jsonValues1.put("custom_notice", commercial_notic);
            jsonValues1.put("pricing", commercial_pricing);
            jsonValues1.put("duration_unit", commerical_duation_unit);
            jsonValues1.put("max_duration", commercial_max_duartion);
            jsonValues1.put("pricing_duration", commecial_pricing_duration);
            jsonValues1.put("day_type", "EVERYDAY");
            jsonValues1.put("in_effect", "1");
            jsonValues1.put("enforced", "In Effect");
            jsonValues1.put("this_day", "EVERYDAY");
            jsonValues1.put("marker_type", "google");
            jsonValues1.put("location_place_id", parking_rules_location_code);

            jsonValues1.put("ReservationAllowed", isReservationAllowed);
            jsonValues1.put("PrePymntReqd_for_Reservation", isPrePaymentRequired);
            jsonValues1.put("CanCancel_Reservation", isCancellationAllowed);
            jsonValues1.put("Cancellation_Charge", cancellationCharge);
            jsonValues1.put("Reservation_PostPayment_term", postPayTermReservation);
            jsonValues1.put("Reservation_PostPayment_LateFee", postPayLateFeesReservation);
            jsonValues1.put("ParkNow_PostPaymentAllowed", isPostPaymentAllowed);
            jsonValues1.put("ParkNow_PostPayment_term", postPayTerm);
            jsonValues1.put("before_reserve_verify_lot_avbl", isVerifyLotAvailability);
            jsonValues1.put("include_reservations_for_avbl_calc", isCountReservedSpot);
            jsonValues1.put("ParkNow_PostPayment_LateFee", postPayLateFees);

            jsonValues1.put("Reservation_PostPayment_Fee", postPayFeesReservation);
            jsonValues1.put("ParkNow_PostPayment_Fee", postPayFees);
            jsonValues1.put("number_of_reservable_spots", noOfReservationSpots);

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json12 != null) {
                if (json12.has("error")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("Error");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                } else {
                    if (!id.equals("")) {
                        isclickeditrules = false;
                        getActivity().onBackPressed();
                    }
                }

            }
            super.onPostExecute(s);
        }
    }


    //add commercial parking ruels.....................
    public class add_commercial_paking_rules extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        JSONObject json12 = null;
        String walleturl = "_table/google_parking_rules";
        String id = "";
        String str_day = txt_commercial_day.getText().toString();

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            //commercial_loc_code = getRandomString(10);
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("location_code", commercial_loc_code);
            if (commercial_title.equals("")) {
                commercial_title = "unknown location";
            }
            if (commerical_24hr) {
                jsonValues1.put("start_time", "12 Midnight");
                jsonValues1.put("end_time", "11 PM");
                jsonValues1.put("start_hour", "0");
                jsonValues1.put("end_hour", "23");
                jsonValues1.put("time_rule", "24 HRS");
            } else {

                jsonValues1.put("start_time", commercial_start_hours);
                jsonValues1.put("end_time", commercial_end_hours);
                jsonValues1.put("time_rule", commercial_start_hours + "-" + commercial_end_hours);

                String strathor = commercial_start_hours.substring(0, commercial_start_hours.indexOf(" "));
                String endhr = commercial_end_hours.substring(0, commercial_end_hours.indexOf(" "));

                jsonValues1.put("start_hour", strathor);
                jsonValues1.put("end_hour", endhr);
            }
            // jsonValues1.put("location_name", title);
            if (commercial_active) {
                jsonValues1.put("active", 1);
            } else {
                jsonValues1.put("active", 0);
            }

            jsonValues1.put("custom_notice", commercial_notic);
            jsonValues1.put("pricing", commercial_pricing);
            jsonValues1.put("duration_unit", commerical_duation_unit);
            jsonValues1.put("max_duration", commercial_max_duartion);
            jsonValues1.put("pricing_duration", commecial_pricing_duration);
            jsonValues1.put("day_type", str_day);
            jsonValues1.put("in_effect", "1");
            jsonValues1.put("enforced", "In Effect");
            jsonValues1.put("this_day", "EVERYDAY");
            jsonValues1.put("marker_type", "google");
            jsonValues1.put("location_place_id", commercial_loc_code);
            jsonValues1.put("currency", "USD");
            jsonValues1.put("currency_symbol", "$");

            jsonValues1.put("ReservationAllowed", isReservationAllowed);
            jsonValues1.put("PrePymntReqd_for_Reservation", isPrePaymentRequired);
            jsonValues1.put("CanCancel_Reservation", isCancellationAllowed);
            jsonValues1.put("Cancellation_Charge", cancellationCharge);
            jsonValues1.put("Reservation_PostPayment_term", postPayTermReservation);
            jsonValues1.put("Reservation_PostPayment_LateFee", postPayLateFeesReservation);
            jsonValues1.put("ParkNow_PostPaymentAllowed", isPostPaymentAllowed);
            jsonValues1.put("ParkNow_PostPayment_term", postPayTerm);
            jsonValues1.put("before_reserve_verify_lot_avbl", isVerifyLotAvailability);
            jsonValues1.put("include_reservations_for_avbl_calc", isCountReservedSpot);
            jsonValues1.put("ParkNow_PostPayment_LateFee", postPayLateFees);

            jsonValues1.put("Reservation_PostPayment_Fee", postPayFeesReservation);
            jsonValues1.put("ParkNow_PostPayment_Fee", postPayFees);
            jsonValues1.put("number_of_reservable_spots", noOfReservationSpots);

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    if (json12.has("resource")) {
                        JSONArray array = json12.getJSONArray("resource");
                        for (int i = 0; i < array.length(); i++) {

                            JSONObject c = array.getJSONObject(i);
                            id = c.getString("id");
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);

            if (getActivity() != null && json12 != null) {
                if (json12.has("error")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("Error");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                } else {
                    if (!id.equals("")) {
                        MyApplication.getInstance().commercialRuleIds.add(id);
                        getActivity().onBackPressed();
                    }
                }

            }
            super.onPostExecute(s);
        }
    }

    public class CustomAdapterDaycommercial extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdapterDaycommercial(Activity a, ArrayList<item> d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;
            data = d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txt_title;
            ImageView img;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;


            if (convertView == null) {

                vi = inflater.inflate(R.layout.adapater_day, null);
                /****** View Holder Object to contain tabitem.xml file elements ******/

                holder = new ViewHolder();

                holder.txt_title = (TextView) vi.findViewById(R.id.txt_day_name);
                holder.img = (ImageView) vi.findViewById(R.id.img_day_selected);

                list_commercial_day_lits.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        checkedisone = 0;
                        for (int i = 0; i < day_list.size(); i++) {
                            if (day_list.get(i).getIsslected()) {
                                checkedisone++;
                            }
                        }


                        if (checkedisone == 1) {

                            day_list.get(position).setIsslected(true);
                            notifyDataSetChanged();

                        } else {

                            if (day_list.get(position).getIsslected()) {
                                day_list.get(position).setIsslected(false);
                                notifyDataSetChanged();
                            } else {
                                day_list.get(position).setIsslected(true);
                                notifyDataSetChanged();
                            }

                        }

                        StringBuffer daylist = new StringBuffer();
                        ;
                        for (int i = 0; i < day_list.size(); i++) {
                            if (day_list.get(i).getIsslected()) {
                                daylist.append(day_list.get(i).getThis_day() + ",");

                            }
                        }
                        String finaldaylist = daylist.toString();
                        finaldaylist = finaldaylist.substring(0, finaldaylist.length() - 1);
                        txt_commercial_day.setText(finaldaylist);

                    }
                });


                /************  Set holder with LayoutInflater ************/
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {


            } else {
                /***** Get each Model object from Arraylist ********/
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                /************  Set Model values in Holder elements ***********/
                if (tempValues.getIsslected()) {
                    holder.img.setVisibility(View.VISIBLE);
                    holder.img.setImageResource(R.mipmap.selected);
                } else {
                    holder.img.setVisibility(View.GONE);
                }
                holder.txt_title.setText(tempValues.getThis_day());


            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

    //fetch payment options
    private class fetchPaymentOptions extends AsyncTask<String, String, String> {

        JSONObject json;
        JSONArray json1;

        @Override
        protected void onPreExecute() {
            paymentOptions.clear();
            PaymentOption paymentOption = new PaymentOption();
            paymentOption.setPayment_option("Select terms");
            paymentOption.setPayment_option_description("Select terms");
            paymentOptions.add(paymentOption);
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + "_table/payment_options";
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        PaymentOption paymentOption = new PaymentOption();
                        paymentOption.setId(c.getInt("id"));
                        paymentOption.setPayment_option(c.getString("payment_option"));
                        paymentOption.setPayment_option_description(c.getString("payment_option_description"));
                        paymentOptions.add(paymentOption);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                rl_progressbar.setVisibility(View.GONE);
                postPayTermsAdapter.notifyDataSetChanged();
                postPayTermsReservationAdapter.notifyDataSetChanged();
                if (paymentOptions.size() > 0) {
                    if (!TextUtils.isEmpty(postPayTermReservation)) {
                        spPostPaymentTermsReservation.setSelection(paymentOptions.indexOf(new PaymentOption(postPayTermReservation)));
                    }

                    if (!TextUtils.isEmpty(postPayTerm)) {
                        spPostPaymentTerms.setSelection(paymentOptions.indexOf(new PaymentOption(postPayTerm)));
                    }
                }
            }
        }
    }

    @Override
    public void onResume() {
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    if (rl_commercial_day_list.getVisibility() == View.VISIBLE) {
                        rl_commercial_day_list.setVisibility(View.GONE);
                    } else {
                        getActivity().onBackPressed();
                    }
                    return true;
                }

                return false;
            }
        });
        super.onResume();
    }
}
