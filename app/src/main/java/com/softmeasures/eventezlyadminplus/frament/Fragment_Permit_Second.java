package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

/**
 * Created by Significant Info on 3/14/2018.
 */

public class Fragment_Permit_Second extends Fragment {

    View rootView;
    Activity activity;
    Context mContext;
    ConnectionDetector cd;
    String user_id;
    ListView lv_Permit;
    ProgressBar progressbar;
    RelativeLayout rl_progressbar, rl_main;
    Animation animation;
    TextView txt_Title;
    ArrayList<item> township_array_list;
    CustomAdapterTwonship customAdapterTwonship;
    TextView txt_No_Record;
    String Townshipcode = "", Permit_name = "", Cost = "", Expires_by = "",
            Renewable = "", Permit_nextnum = "", Requirements = "", Appl_req_download = "",
            Official_logo = "", township_id = "", twp_id = "", Manager_type, Manager_type_id;

    TextView txt_SelectAll;
    String check_Yes = "NO";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_permit, container, false);

        activity = getActivity();
        mContext = getContext();
        township_array_list = new ArrayList<>();
        cd = new ConnectionDetector(activity);
        SharedPreferences logindeatl = activity.getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = logindeatl.getString("id", "null");

        if (getArguments() != null) {
            township_id = getArguments().getString("township_id");
            Townshipcode = getArguments().getString("Townshipcode");
            Permit_name = getArguments().getString("Permit_name");
            Cost = getArguments().getString("Cost");
            Expires_by = getArguments().getString("Expires_by");
            Renewable = getArguments().getString("Renewable");
            Permit_nextnum = getArguments().getString("permit_nextnum");
            Requirements = getArguments().getString("requirements");
            Appl_req_download = getArguments().getString("appl_req_download");
            Official_logo = getArguments().getString("Official_logo");
            twp_id = getArguments().getString("twp_id");
            Manager_type = getArguments().getString("Manager_type");
            Manager_type_id = getArguments().getString("Manager_type_id");
        }

        rl_main = (RelativeLayout) rootView.findViewById(R.id.rl_main);
        rl_progressbar = (RelativeLayout) rootView.findViewById(R.id.rl_progressbar);
        progressbar = (ProgressBar) rootView.findViewById(R.id.progressbar);
        lv_Permit = (ListView) rootView.findViewById(R.id.lv_Permit);
        txt_Title = (TextView) rootView.findViewById(R.id.txt_Title);
        txt_No_Record = (TextView) rootView.findViewById(R.id.txt_No_Record);
        txt_SelectAll = (TextView) rootView.findViewById(R.id.txt_SelectAll);
        //txt_Title.setText("Covered Locations for " + Permit_name);
        String styledText1 = "Covered Locations for " + "<font color='#3F48CC'>" + Permit_name + "</font>";
        txt_Title.setText(Html.fromHtml(styledText1), TextView.BufferType.SPANNABLE);


        txt_SelectAll.setVisibility(View.VISIBLE);

        txt_SelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (item wp : MyPermits_VehicleUser.permitsarray_All) {
                    if (wp.getTownship_code().contains(Townshipcode) &&
                            wp.getPermit_validity().contains(Expires_by)) {
                        check_Yes = "YES";
                    }
                }

                if (check_Yes.equalsIgnoreCase("YES")) {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Permit Added");
                    alertDialog.setMessage("This permit is already added in my permit. Please select other permit.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                                /*((vchome) getActivity()).show_back_button();
                                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                MyPermits_VehicleUser pay = new MyPermits_VehicleUser();
                                fragmentStack.clear();
                                ft.add(R.id.My_Container_1_ID, pay);
                                ft.commitAllowingStateLoss();*/
                        }
                    });
                    alertDialog.show();
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("Location_id", township_array_list.get(0).getLocation_id());
                    bundle.putString("Location_code", township_array_list.get(0).getLocation_code());
                    if (township_array_list.size() > 1) {
                        bundle.putString("Location_name", township_array_list.get(0).getLocation_name() + " +More");
                    } else {
                        bundle.putString("Location_name", township_array_list.get(0).getLocation_name());
                    }
                    bundle.putString("Township_name", township_array_list.get(0).getTownship_name());
                    bundle.putString("Township_code", township_array_list.get(0).getTownshipcode());
                    bundle.putString("Permit_id", township_array_list.get(0).getPermit_id());
                    bundle.putString("Permit_type", township_array_list.get(0).getPermit_type());
                    bundle.putString("Permit_name", township_array_list.get(0).getPermit_name());
                    bundle.putString("Manager_type", township_array_list.get(0).getManager_type());
                    bundle.putString("cost", Cost);
                    bundle.putString("Expires_by", Expires_by);
                    bundle.putString("Renewable", Renewable);
                    bundle.putString("Permit_nextnum", Permit_nextnum);
                    bundle.putString("requirements", Requirements);
                    bundle.putString("appl_req_download", Appl_req_download);
                    bundle.putString("Official_logo", Official_logo);
                    bundle.putString("township_id", township_id);
                    bundle.putString("twp_id", twp_id);
                    bundle.putString("Manager_type", Manager_type);
                    bundle.putString("Manager_type_id", Manager_type_id);

                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Fragment_Permit_Third pay = new Fragment_Permit_Third();
                    pay.setArguments(bundle);
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                }
                //township_code
                //permit_validity
            }
        });

        if (cd.isConnectingToInternet()) {
            try {
                new getPermitData().execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }
        return rootView;
    }


    public class getPermitData extends AsyncTask<String, String, String> {

        String vehiclenourl = "_table/locations_permit?filter=(township_code%3D'" + URLEncoder.encode(Townshipcode, "utf-8") +
                "')%20AND%20(permit_name%3D'" + URLEncoder.encode(Permit_name, "utf-8") + "')";

        JSONObject json = null;

        public getPermitData() throws UnsupportedEncodingException {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rl_progressbar.setVisibility(View.VISIBLE);
            progressbar.setVisibility(View.VISIBLE);
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("get_vehicle_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;

            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);
                    Log.e("rerpones", String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setId(c.getString("id"));
                        item1.setDate(c.getString("date_time"));
                        item1.setPermit_id(c.getString("permit_id"));
                        item1.setPermit_type(c.getString("permit_type"));
                        item1.setPermit_name(c.getString("permit_name"));
                        item1.setLocation_id(c.getString("location_id"));
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setLocation_name(c.getString("location_name"));
                        item1.setTownship_id(c.getString("township_id"));
                        item1.setTownshipcode(c.getString("township_code"));
                        item1.setTownship_name(c.getString("township_name"));
                        item1.setManager_type(c.getString("manager_type"));
                        item1.setAddress(c.getString("address"));
                        item1.setIsslected(false);
                        township_array_list.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity != null) {
                    if (township_array_list.size() > 0) {
                        customAdapterTwonship = new CustomAdapterTwonship(getActivity(), township_array_list, getResources());
                        lv_Permit.setAdapter(customAdapterTwonship);
                    } else {
                        txt_No_Record.setVisibility(View.VISIBLE);
                        lv_Permit.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

    public class CustomAdapterTwonship extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdapterTwonship(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txt_Location_id, txt_location_code, txt_location_name, txt_township_name,
                    txt_APPLY, txt_townshipCode;
            ImageView img_logo, img_selected;
            RelativeLayout rl_main;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final CustomAdapterTwonship.ViewHolder holder;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_permit, null);
                holder = new CustomAdapterTwonship.ViewHolder();

                holder.txt_Location_id = (TextView) vi.findViewById(R.id.txt_Location_id);
                holder.txt_location_code = (TextView) vi.findViewById(R.id.txt_location_code);
                holder.txt_location_name = (TextView) vi.findViewById(R.id.txt_location_name);
                holder.txt_township_name = (TextView) vi.findViewById(R.id.txt_township_name);
                holder.txt_townshipCode = (TextView) vi.findViewById(R.id.txt_townshipCode);
                holder.txt_APPLY = (TextView) vi.findViewById(R.id.txt_APPLY);
                holder.img_logo = (ImageView) vi.findViewById(R.id.img_logo);
                holder.img_selected = (ImageView) vi.findViewById(R.id.img_selected);
                holder.rl_main = (RelativeLayout) vi.findViewById(R.id.rl_main);

                vi.setTag(holder);
            } else
                holder = (CustomAdapterTwonship.ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.txt_APPLY.setVisibility(View.GONE);


            holder.txt_location_name.setSelected(true);
            holder.txt_Location_id.setText(data.get(position).getTownshipcode() + " " + data.get(position).getPermit_name());
            holder.txt_location_name.setText(data.get(position).getAddress());
            holder.txt_township_name.setText(data.get(position).getTownship_name());

            holder.rl_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                }
            });

            holder.txt_APPLY.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString("Location_id", data.get(position).getLocation_id());
                    bundle.putString("Location_code", data.get(position).getLocation_code());
                    bundle.putString("Location_name", data.get(position).getLocation_name());
                    bundle.putString("Township_name", data.get(position).getTownship_name());
                    bundle.putString("Townshipcode", data.get(position).getTownshipcode());
                    bundle.putString("Permit_id", data.get(position).getPermit_id());
                    bundle.putString("Permit_type", data.get(position).getPermit_type());
                    bundle.putString("Permit_name", data.get(position).getPermit_name());
                    bundle.putString("cost", Cost);
                    bundle.putString("Expires_by", Expires_by);
                    bundle.putString("Renewable", Renewable);
                    bundle.putString("Permit_nextnum", Permit_nextnum);
                    bundle.putString("Date", data.get(position).getDate());
                    bundle.putString("requirements", Requirements);
                    bundle.putString("appl_req_download", Appl_req_download);
                    bundle.putString("Official_logo", Official_logo);
                    bundle.putString("township_id", township_id);


                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Fragment_Permit_Third pay = new Fragment_Permit_Third();
                    pay.setArguments(bundle);
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                }
            });

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }
}
