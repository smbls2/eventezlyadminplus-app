package com.softmeasures.eventezlyadminplus.frament.locations;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragLocationTypesBinding;
import com.softmeasures.eventezlyadminplus.frament.reservation.ReservableSpotsMapFragment;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class LocationTypesFragment extends BaseFragment {

    FragLocationTypesBinding binding;
    private boolean isEditParkingLocationMenu = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_location_types, container, false);
        if (getArguments() != null) {
            isEditParkingLocationMenu = getArguments().getBoolean("isEditParkingLocationMenu", false);
        }
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {
        binding.llBtnCommercial.setOnClickListener(v -> {
            if (isEditParkingLocationMenu)
                openLocationsView("Commercial");
            else openEditManageSpots("Commercial");
        });
        binding.llBtnTownship.setOnClickListener(v -> {
            if (isEditParkingLocationMenu)
                openLocationsView("Township");
            else openEditManageSpots("Township");
        });
        binding.llBtnOthers.setOnClickListener(v -> {
            if (isEditParkingLocationMenu)
                openLocationsView("Other");
            else openEditManageSpots("Other");
        });
    }

    private void openEditManageSpots(String type) {
        ((vchome) getActivity()).show_back_button();
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ReservableSpotsMapFragment reservableSpotFrag = new ReservableSpotsMapFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        reservableSpotFrag.setArguments(bundle);
        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.add(R.id.My_Container_1_ID, reservableSpotFrag, "reservableSpotFrag");
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(reservableSpotFrag);
        ft.commitAllowingStateLoss();
    }

    private void openLocationsView(String type) {
        ((vchome) getActivity()).show_back_button();
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        LocationsByTypeFragment locationsByType = new LocationsByTypeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        locationsByType.setArguments(bundle);
        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.add(R.id.My_Container_1_ID, locationsByType, "locationsByType");
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(locationsByType);
        ft.commitAllowingStateLoss();
    }

    @Override
    protected void initViews(View v) {

    }
}
