package com.softmeasures.eventezlyadminplus.frament.event.AddEdit;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragEventPricingLimitBinding;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.frament.event.AllEventsFragment.isUpdated;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_LOCATION;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_VIRTUALLY;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_BOTH;

public class EventPricingLimitFragment extends BaseFragment {

    public FragEventPricingLimitBinding binding;
    private boolean isEdit = false, isRepeat = false;
    private EventDefinition eventDefinition;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            isEdit = getArguments().getBoolean("isEdit", false);
            isRepeat = getArguments().getBoolean("isRepeat", false);
            eventDefinition = new Gson().fromJson(getArguments().getString("eventDefinition"), EventDefinition.class);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_event_pricing_limit, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {

        if (eventDefinition.getEvent_logi_type() == EVENT_TYPE_AT_VIRTUALLY) {
            binding.llWebEventPrice.setVisibility(View.VISIBLE);
            binding.llLocationEventPrice.setVisibility(View.GONE);
            binding.llParkingPrice.setVisibility(View.GONE);

        } else if (eventDefinition.getEvent_logi_type() == EVENT_TYPE_AT_LOCATION) {
            binding.llWebEventPrice.setVisibility(View.GONE);
            if (eventDefinition.isFree_event()) {
                binding.llLocationEventPrice.setVisibility(View.GONE);
            } else {
                binding.llLocationEventPrice.setVisibility(View.VISIBLE);
            }
            if (eventDefinition != null && eventDefinition.isParking_allowed()) {
                binding.llParkingPrice.setVisibility(View.VISIBLE);
            } else {
                binding.llParkingPrice.setVisibility(View.GONE);
            }
        } else if (eventDefinition.getEvent_logi_type() == EVENT_TYPE_BOTH) {
            binding.llWebEventPrice.setVisibility(View.VISIBLE);
            if (eventDefinition.isFree_event()) {
                binding.llLocationEventPrice.setVisibility(View.GONE);
            } else {
                binding.llLocationEventPrice.setVisibility(View.VISIBLE);
            }
            if (eventDefinition != null && eventDefinition.isParking_allowed()) {
                binding.llParkingPrice.setVisibility(View.VISIBLE);
            } else {
                binding.llParkingPrice.setVisibility(View.GONE);
            }
        }

        if (eventDefinition.getEvent_parking_fee() != null
                && !TextUtils.isEmpty(eventDefinition.getEvent_parking_fee())) {
            binding.etParkingCost.setText(eventDefinition.getEvent_parking_fee());
        }

        if (eventDefinition.getEvent_full_regn_fee() != null
                && !TextUtils.isEmpty(eventDefinition.getEvent_full_regn_fee())) {
            binding.etEventFullFees.setText(eventDefinition.getEvent_full_regn_fee());
        }

        if (eventDefinition.getEvent_full_youth_fee() != null
                && !TextUtils.isEmpty(eventDefinition.getEvent_full_youth_fee())) {
            binding.etEventYouthFee.setText(eventDefinition.getEvent_full_youth_fee());
        }

        if (eventDefinition.getEvent_full_child_fee() != null
                && !TextUtils.isEmpty(eventDefinition.getEvent_full_child_fee())) {
            binding.etEventChildFee.setText(eventDefinition.getEvent_full_child_fee());
        }

        if (eventDefinition.getEvent_full_student_fee() != null
                && !TextUtils.isEmpty(eventDefinition.getEvent_full_student_fee())) {
            binding.etEventStudentFee.setText(eventDefinition.getEvent_full_student_fee());
        }

        if (eventDefinition.getEvent_full_minister_fee() != null
                && !TextUtils.isEmpty(eventDefinition.getEvent_full_minister_fee())) {
            binding.etEventMinisterFee.setText(eventDefinition.getEvent_full_minister_fee());
        }

        if (eventDefinition.getEvent_full_clergy_fee() != null
                && !TextUtils.isEmpty(eventDefinition.getEvent_full_clergy_fee())) {
            binding.etEventClergyFee.setText(eventDefinition.getEvent_full_clergy_fee());
        }

        if (eventDefinition.getEvent_full_promo_fee() != null
                && !TextUtils.isEmpty(eventDefinition.getEvent_full_promo_fee())) {
            binding.etEventPromoFee.setText(eventDefinition.getEvent_full_promo_fee());
        }

        if (eventDefinition.getEvent_full_senior_fee() != null
                && !TextUtils.isEmpty(eventDefinition.getEvent_full_senior_fee())) {
            binding.etEventSeniorFee.setText(eventDefinition.getEvent_full_senior_fee());
        }

        if (eventDefinition.getEvent_full_staff_fee() != null
                && !TextUtils.isEmpty(eventDefinition.getEvent_full_staff_fee())) {
            binding.etEventStaffFee.setText(eventDefinition.getEvent_full_staff_fee());
        }

        if (eventDefinition.getWeb_event_full_regn_fee() != null
                && !TextUtils.isEmpty(eventDefinition.getWeb_event_full_regn_fee())) {
            binding.etWebEventFullRegFee.setText(eventDefinition.getWeb_event_full_regn_fee());
        }

        if (eventDefinition.getCost() != null
                && !TextUtils.isEmpty(eventDefinition.getCost())) {
            binding.etEventCost.setText(String.format("%s", eventDefinition.getCost()));
        }
    }

    @Override
    protected void setListeners() {

        binding.tvBtnNext.setOnClickListener(v -> {

            if (validate()) {
                Fragment fragment = null;
                if (eventDefinition.getEvent_logi_type() == EVENT_TYPE_AT_LOCATION) {
                    fragment = new EventMediaAddEditFragment();
                } else {
                    fragment = new EventMediaAddEditFragment();
                }

                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Bundle bundle = new Bundle();
                bundle.putBoolean("isEdit", isEdit);
                bundle.putBoolean("isRepeat", isRepeat);
                bundle.putString("eventDefinition", new Gson().toJson(eventDefinition));
                fragment.setArguments(bundle);
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.add(R.id.My_Container_1_ID, fragment);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(fragment);
                ft.commitAllowingStateLoss();
            }

        });

    }

    private boolean validate() {

        if (eventDefinition.getEvent_logi_type() == EVENT_TYPE_AT_VIRTUALLY) {
            if (!eventDefinition.isFree_event()) {
                if (!checkWebFields()) return false;
            }
        } else if (eventDefinition.getEvent_logi_type() == EVENT_TYPE_AT_LOCATION) {
            if (!checkLocationFields()) return false;
        } else if (eventDefinition.getEvent_logi_type() == EVENT_TYPE_BOTH) {
            if (!checkLocationFields()) return false;
            if (!eventDefinition.isFree_event()) {
                if (!checkWebFields()) return false;
            }
        }
        return true;
    }

    private boolean checkWebFields() {
        if (!TextUtils.isEmpty(binding.etWebEventFullRegFee.getText())) {
            eventDefinition.setWeb_event_full_regn_fee(String.valueOf(binding.etWebEventFullRegFee.getText()));
        } else {
            binding.etWebEventFullRegFee.setError("Required");
            binding.etWebEventFullRegFee.requestFocus();
            return false;
        }
        return true;
    }

    private boolean checkLocationFields() {

        if (eventDefinition.isFree_event()) {
            eventDefinition.setEvent_full_regn_fee("0");
            eventDefinition.setEvent_full_youth_fee("0");
            eventDefinition.setEvent_full_child_fee("0");
            eventDefinition.setEvent_full_student_fee("0");
            eventDefinition.setEvent_full_staff_fee("0");
            eventDefinition.setEvent_full_minister_fee("0");
            eventDefinition.setEvent_full_clergy_fee("0");
            eventDefinition.setEvent_full_promo_fee("0");
            eventDefinition.setEvent_full_senior_fee("0");
        } else {
            if (!TextUtils.isEmpty(binding.etEventCost.getText())) {
                eventDefinition.setCost(String.format("%s", binding.etEventCost.getText().toString()));
            } else {
                binding.etEventCost.setError("Required");
                binding.etEventCost.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventFullFees.getText())) {
                eventDefinition.setEvent_full_regn_fee(binding.etEventFullFees.getText().toString());
            } else {
                binding.etEventFullFees.setError("Required");
                binding.etEventFullFees.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventYouthFee.getText())) {
                eventDefinition.setEvent_full_youth_fee(binding.etEventYouthFee.getText().toString());
            } else {
                binding.etEventYouthFee.setError("Required");
                binding.etEventYouthFee.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventChildFee.getText())) {
                eventDefinition.setEvent_full_child_fee(binding.etEventChildFee.getText().toString());
            } else {
                binding.etEventChildFee.setError("Required");
                binding.etEventChildFee.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventStudentFee.getText())) {
                eventDefinition.setEvent_full_student_fee(binding.etEventStudentFee.getText().toString());
            } else {
                binding.etEventStudentFee.setError("Required");
                binding.etEventStudentFee.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventStaffFee.getText())) {
                eventDefinition.setEvent_full_staff_fee(binding.etEventStaffFee.getText().toString());
            } else {
                binding.etEventStaffFee.setError("Required");
                binding.etEventStaffFee.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventMinisterFee.getText())) {
                eventDefinition.setEvent_full_minister_fee(binding.etEventMinisterFee.getText().toString());
            } else {
                binding.etEventMinisterFee.setError("Required");
                binding.etEventMinisterFee.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventClergyFee.getText())) {
                eventDefinition.setEvent_full_clergy_fee(binding.etEventClergyFee.getText().toString());
            } else {
                binding.etEventClergyFee.setError("Required");
                binding.etEventClergyFee.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventPromoFee.getText())) {
                eventDefinition.setEvent_full_promo_fee(binding.etEventPromoFee.getText().toString());
            } else {
                binding.etEventPromoFee.setError("Required");
                binding.etEventPromoFee.requestFocus();
                return false;
            }

            if (!TextUtils.isEmpty(binding.etEventSeniorFee.getText())) {
                eventDefinition.setEvent_full_senior_fee(binding.etEventSeniorFee.getText().toString());
            } else {

                binding.etEventSeniorFee.setError("Required");
                binding.etEventSeniorFee.requestFocus();
                return false;
            }
        }

        if (eventDefinition.isParking_allowed()) {
            if (!TextUtils.isEmpty(binding.etParkingCost.getText())) {
                eventDefinition.setEvent_parking_fee(String.valueOf(binding.etParkingCost.getText()));
            } else {
                binding.etParkingCost.setError("Required");
                binding.etParkingCost.requestFocus();
                return false;
            }
        }
        return true;
    }

    @Override
    protected void initViews(View v) {

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("#DEBUG", "   Pricing:  onResume");
        if (isUpdated) {
            if (getActivity() != null) getActivity().onBackPressed();
        }
    }
}
