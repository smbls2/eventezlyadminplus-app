package com.softmeasures.eventezlyadminplus.frament.event;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_VIRTUALLY;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubeIntents;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.panstreetview;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoFacebookBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoGoogleClassroomBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoGoogleMeetBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoSkypeBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoTeleConferenceBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoYoutubeBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoZoomBinding;
import com.softmeasures.eventezlyadminplus.databinding.FragEventInfoViewBinding;
import com.softmeasures.eventezlyadminplus.frament.event.checkin.RegisterNewEntryFragment;
import com.softmeasures.eventezlyadminplus.frament.event.checkin.RegisteredEventsFragment;
import com.softmeasures.eventezlyadminplus.frament.event.session.SessionsFragment;
import com.softmeasures.eventezlyadminplus.frament.f_direction;
import com.softmeasures.eventezlyadminplus.frament.g_classroom.GClassroomActivity;
import com.softmeasures.eventezlyadminplus.frament.g_youtube.GYoutubeMainActivity;
import com.softmeasures.eventezlyadminplus.meeting.ZoomLoginActivity;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.Manager;
import com.softmeasures.eventezlyadminplus.models.MediaDefinition;
import com.softmeasures.eventezlyadminplus.models.ParkingPartner;
import com.softmeasures.eventezlyadminplus.utils.AESEncyption;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;
import com.softmeasures.eventezlyadminplus.utube.UTubeMainActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class EventInfoViewFragment extends BaseFragment {

    private FragEventInfoViewBinding binding;
    private int eventId;
    private ProgressDialog progressDialog;
    private EventDefinition eventDefinition;
    private Manager selectedManager;
    private String parkingType = "";

    private ArrayList<String> eventImages = new ArrayList<>();
    private EventImageAdapter eventImageAdapter;
    SimpleDateFormat dateFormatMain = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd MMM, hh:mm a", Locale.getDefault());
    private boolean isCheckIn = false;
    private ArrayList<MediaDefinition> mediaDefinitions = new ArrayList<>();
    private MediaAdapter mediaAdapter;

    private ArrayList<String> teleNumbers = new ArrayList<>();
    private TeleNumberAdapter teleNumberAdapter;
    private String teleCountryCode = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            parkingType = getArguments().getString("parkingType");
            isCheckIn = getArguments().getBoolean("isCheckIn", isCheckIn);
            eventId = getArguments().getInt("eventId");
            selectedManager = new Gson().fromJson(getArguments().getString("selectedManager"), Manager.class);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_event_info_view, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        new fetchEventDetails().execute();
    }

    private class fetchEventDetails extends AsyncTask<String, String, String> {
        JSONObject json;
        JSONArray json1;
        String eventDefUrl = "_table/event_definitions";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            eventDefUrl = "_table/event_definitions?filter=id=" + eventId;
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + eventDefUrl;
            Log.e("#DEBUG", "      fetchEventDetails:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "    fetchEventDetails:  Response:  " + responseStr);
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");

                    for (int j = 0; j < jsonArray.length(); j++) {
                        eventDefinition = new EventDefinition();
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            eventDefinition.setId(object.getInt("id"));
                        }
                        if (object.has("date_time")
                                && !TextUtils.isEmpty(object.getString("date_time"))
                                && !object.getString("date_time").equals("null")) {
                            eventDefinition.setDate_time(object.getString("date_time"));
                        }
                        if (object.has("manager_type")
                                && !TextUtils.isEmpty(object.getString("manager_type"))
                                && !object.getString("manager_type").equals("null")) {
                            eventDefinition.setManager_type(object.getString("manager_type"));
                        }
                        if (object.has("manager_type_id")
                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
                                && !object.getString("manager_type_id").equals("null")) {
                            eventDefinition.setManager_type_id(object.getInt("manager_type_id"));
                        }
                        if (object.has("twp_id")
                                && !TextUtils.isEmpty(object.getString("twp_id"))
                                && !object.getString("twp_id").equals("null")) {
                            eventDefinition.setTwp_id(object.getInt("twp_id"));
                        }
                        if (object.has("township_code")
                                && !TextUtils.isEmpty(object.getString("township_code"))
                                && !object.getString("township_code").equals("null")) {
                            eventDefinition.setTownship_code(object.getString("township_code"));
                        }
                        if (object.has("township_name")
                                && !TextUtils.isEmpty(object.getString("township_name"))
                                && !object.getString("township_name").equals("null")) {
                            eventDefinition.setTownship_name(object.getString("township_name"));
                        }
                        if (object.has("company_id")
                                && !TextUtils.isEmpty(object.getString("company_id"))
                                && !object.getString("company_id").equals("null")) {
                            eventDefinition.setCompany_id(object.getInt("company_id"));
                        }
                        if (object.has("company_code")
                                && !TextUtils.isEmpty(object.getString("company_code"))
                                && !object.getString("company_code").equals("null")) {
                            eventDefinition.setCompany_code(object.getString("company_code"));
                        }
                        if (object.has("company_name")
                                && !TextUtils.isEmpty(object.getString("company_name"))
                                && !object.getString("company_name").equals("null")) {
                            eventDefinition.setCompany_name(object.getString("company_name"));
                        }
                        if (object.has("event_id")
                                && !TextUtils.isEmpty(object.getString("event_id"))
                                && !object.getString("event_id").equals("null")) {
                            eventDefinition.setEvent_id(object.getInt("event_id"));
                        }

                        if (object.has("event_category_type_id")
                                && !TextUtils.isEmpty(object.getString("event_category_type_id"))
                                && !object.getString("event_category_type_id").equals("null")) {
                            eventDefinition.setEvent_category_type_id(object.getInt("event_category_type_id"));
                        }
                        if (object.has("event_category_type")
                                && !TextUtils.isEmpty(object.getString("event_category_type"))
                                && !object.getString("event_category_type").equals("null")) {
                            eventDefinition.setEvent_category_type(object.getString("event_category_type"));
                        }
                        if (object.has("event_type")
                                && !TextUtils.isEmpty(object.getString("event_type"))
                                && !object.getString("event_type").equals("null")) {
                            eventDefinition.setEvent_type(object.getString("event_type"));
                        }

                        if (object.has("event_code")
                                && !TextUtils.isEmpty(object.getString("event_code"))
                                && !object.getString("event_code").equals("null")) {
                            eventDefinition.setEvent_code(object.getString("event_code"));
                        }

                        if (object.has("event_name")
                                && !TextUtils.isEmpty(object.getString("event_name"))
                                && !object.getString("event_name").equals("null")) {
                            eventDefinition.setEvent_name(object.getString("event_name"));
                        }
                        if (object.has("event_short_description")
                                && !TextUtils.isEmpty(object.getString("event_short_description"))
                                && !object.getString("event_short_description").equals("null")) {
                            eventDefinition.setEvent_short_description(object.getString("event_short_description"));
                        }
                        if (object.has("event_long_description")
                                && !TextUtils.isEmpty(object.getString("event_long_description"))
                                && !object.getString("event_long_description").equals("null")) {
                            eventDefinition.setEvent_long_description(object.getString("event_long_description"));
                        }
                        if (object.has("event_link_on_web")
                                && !TextUtils.isEmpty(object.getString("event_link_on_web"))
                                && !object.getString("event_link_on_web").equals("null")) {
                            eventDefinition.setEvent_link_on_web(object.getString("event_link_on_web"));
                        }
                        if (object.has("event_link_twitter")
                                && !TextUtils.isEmpty(object.getString("event_link_twitter"))
                                && !object.getString("event_link_twitter").equals("null")) {
                            eventDefinition.setEvent_link_twitter(object.getString("event_link_twitter"));
                        }
                        if (object.has("event_link_whatsapp")
                                && !TextUtils.isEmpty(object.getString("event_link_whatsapp"))
                                && !object.getString("event_link_whatsapp").equals("null")) {
                            eventDefinition.setEvent_link_whatsapp(object.getString("event_link_whatsapp"));
                        }
                        if (object.has("event_link_other_media")
                                && !TextUtils.isEmpty(object.getString("event_link_other_media"))
                                && !object.getString("event_link_other_media").equals("null")) {
                            eventDefinition.setEvent_link_other_media(object.getString("event_link_other_media"));
                        }
                        if (object.has("event_link_ytube")
                                && !TextUtils.isEmpty(object.getString("event_link_ytube"))
                                && !object.getString("event_link_ytube").equals("null")) {
                            eventDefinition.setEvent_link_ytube(object.getString("event_link_ytube"));
                        }
                        if (object.has("event_link_zoom")
                                && !TextUtils.isEmpty(object.getString("event_link_zoom"))
                                && !object.getString("event_link_zoom").equals("null")) {
                            eventDefinition.setEvent_link_zoom(object.getString("event_link_zoom"));
                        }
                        if (object.has("event_link_googlemeet")
                                && !TextUtils.isEmpty(object.getString("event_link_googlemeet"))
                                && !object.getString("event_link_googlemeet").equals("null")) {
                            eventDefinition.setEvent_link_googlemeet(object.getString("event_link_googlemeet"));
                        }
                        if (object.has("event_link_googleclassroom")
                                && !TextUtils.isEmpty(object.getString("event_link_googleclassroom"))
                                && !object.getString("event_link_googleclassroom").equals("null")) {
                            eventDefinition.setEvent_link_googleclassroom(object.getString("event_link_googleclassroom"));
                        }
                        if (object.has("event_logi_type")
                                && !TextUtils.isEmpty(object.getString("event_logi_type"))
                                && !object.getString("event_logi_type").equals("null")) {
                            eventDefinition.setEvent_logi_type(object.getInt("event_logi_type"));
                        } else {
                            eventDefinition.setEvent_logi_type(1);
                        }

                        if (object.has("event_regn_limit")
                                && !TextUtils.isEmpty(object.getString("event_regn_limit"))
                                && !object.getString("event_regn_limit").equals("null")) {
                            eventDefinition.setEvent_regn_limit(object.getString("event_regn_limit"));
                        }
                        if (object.has("web_event_full_regn_fee")
                                && !TextUtils.isEmpty(object.getString("web_event_full_regn_fee"))
                                && !object.getString("web_event_full_regn_fee").equals("null")) {
                            eventDefinition.setWeb_event_full_regn_fee(object.getString("web_event_full_regn_fee"));
                        }
                        if (object.has("web_event_regn_limit")
                                && !TextUtils.isEmpty(object.getString("web_event_regn_limit"))
                                && !object.getString("web_event_regn_limit").equals("null")) {
                            eventDefinition.setWeb_event_regn_limit(object.getString("web_event_regn_limit"));
                        }
                        if (object.has("web_event_location_to_show")
                                && !TextUtils.isEmpty(object.getString("web_event_location_to_show"))
                                && !object.getString("web_event_location_to_show").equals("null")) {
                            eventDefinition.setWeb_event_location_to_show(object.getInt("web_event_location_to_show") == 1);
                        }
                        if (object.has("free_web_event")
                                && !TextUtils.isEmpty(object.getString("free_web_event"))
                                && !object.getString("free_web_event").equals("null")) {
                            eventDefinition.setFree_web_event(object.getBoolean("free_web_event"));
                        }
                        if (object.has("company_logo")
                                && !TextUtils.isEmpty(object.getString("company_logo"))
                                && !object.getString("company_logo").equals("null")) {
                            eventDefinition.setCompany_logo(object.getString("company_logo"));
                        }
                        if (object.has("event_logo")
                                && !TextUtils.isEmpty(object.getString("event_logo"))
                                && !object.getString("event_logo").equals("null")) {
                            eventDefinition.setEvent_logo(object.getString("event_logo"));
                        }
                        if (object.has("event_image1")
                                && !TextUtils.isEmpty(object.getString("event_image1"))
                                && !object.getString("event_image1").equals("null")) {
                            eventDefinition.setEvent_image1(object.getString("event_image1"));
                        }
                        if (object.has("event_image2")
                                && !TextUtils.isEmpty(object.getString("event_image2"))
                                && !object.getString("event_image2").equals("null")) {
                            eventDefinition.setEvent_image2(object.getString("event_image2"));
                        }
                        if (object.has("event_blob_image")
                                && !TextUtils.isEmpty(object.getString("event_blob_image"))
                                && !object.getString("event_blob_image").equals("null")) {
                            eventDefinition.setEvent_blob_image(object.getString("event_blob_image"));
                        }
                        if (object.has("event_address")
                                && !TextUtils.isEmpty(object.getString("event_address"))
                                && !object.getString("event_address").equals("null")) {
                            eventDefinition.setEvent_address(object.getString("event_address"));
                        }
                        if (object.has("covered_locations")
                                && !TextUtils.isEmpty(object.getString("covered_locations"))
                                && !object.getString("covered_locations").equals("null")) {
                            eventDefinition.setCovered_locations(object.getString("covered_locations"));
                        }
                        if (object.has("requirements")
                                && !TextUtils.isEmpty(object.getString("requirements"))
                                && !object.getString("requirements").equals("null")) {
                            eventDefinition.setRequirements(object.getString("requirements"));
                        }
                        if (object.has("appl_req_download")
                                && !TextUtils.isEmpty(object.getString("appl_req_download"))
                                && !object.getString("appl_req_download").equals("null")) {
                            eventDefinition.setAppl_req_download(object.getString("appl_req_download"));
                        }
                        if (object.has("cost")
                                && !TextUtils.isEmpty(object.getString("cost"))
                                && !object.getString("cost").equals("null")) {
                            eventDefinition.setCost(object.getString("cost"));
                        }
                        if (object.has("year")
                                && !TextUtils.isEmpty(object.getString("year"))
                                && !object.getString("year").equals("null")) {
                            eventDefinition.setYear(object.getString("year"));
                        }
                        if (object.has("location_address")
                                && !TextUtils.isEmpty(object.getString("location_address"))
                                && !object.getString("location_address").equals("null")) {
                            eventDefinition.setLocation_address(object.getString("location_address"));
                        }
                        if (object.has("scheme_type")
                                && !TextUtils.isEmpty(object.getString("scheme_type"))
                                && !object.getString("scheme_type").equals("null")) {
                            eventDefinition.setScheme_type(object.getString("scheme_type"));
                        }
                        if (object.has("event_prefix")
                                && !TextUtils.isEmpty(object.getString("event_prefix"))
                                && !object.getString("event_prefix").equals("null")) {
                            eventDefinition.setEvent_prefix(object.getString("event_prefix"));
                        }
                        if (object.has("event_nextnum")
                                && !TextUtils.isEmpty(object.getString("event_nextnum"))
                                && !object.getString("event_nextnum").equals("null")) {
                            eventDefinition.setEvent_nextnum(object.getString("event_nextnum"));
                        }
                        if (object.has("event_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_begins_date_time"))
                                && !object.getString("event_begins_date_time").equals("null")) {
                            eventDefinition.setEvent_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_begins_date_time")));
                        }
                        if (object.has("event_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_ends_date_time"))
                                && !object.getString("event_ends_date_time").equals("null")) {
                            eventDefinition.setEvent_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_ends_date_time")));
                        }
                        if (object.has("event_parking_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_parking_begins_date_time"))
                                && !object.getString("event_parking_begins_date_time").equals("null")) {
                            eventDefinition.setEvent_parking_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_parking_begins_date_time")));
                        }
                        if (object.has("event_parking_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_parking_ends_date_time"))
                                && !object.getString("event_parking_ends_date_time").equals("null")) {
                            eventDefinition.setEvent_parking_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_parking_ends_date_time")));
                        }
                        if (object.has("event_multi_dates")
                                && !TextUtils.isEmpty(object.getString("event_multi_dates"))
                                && !object.getString("event_multi_dates").equals("null")) {
                            eventDefinition.setEvent_multi_dates(object.getString("event_multi_dates"));
                        }
                        if (object.has("event_parking_timings")
                                && !TextUtils.isEmpty(object.getString("event_parking_timings"))
                                && !object.getString("event_parking_timings").equals("null")) {
                            eventDefinition.setEvent_parking_timings(object.getString("event_parking_timings"));
                        }
                        if (object.has("event_event_timings")
                                && !TextUtils.isEmpty(object.getString("event_event_timings"))
                                && !object.getString("event_event_timings").equals("null")) {
                            eventDefinition.setEvent_event_timings(object.getString("event_event_timings"));
                        }
                        if (object.has("expires_by")
                                && !TextUtils.isEmpty(object.getString("expires_by"))
                                && !object.getString("expires_by").equals("null")) {
                            eventDefinition.setExpires_by(object.getString("expires_by"));
                        }
                        if (object.has("regn_reqd")
                                && !TextUtils.isEmpty(object.getString("regn_reqd"))
                                && !object.getString("regn_reqd").equals("null")) {
                            eventDefinition.setRegn_reqd(object.getBoolean("regn_reqd"));
                        }
                        if (object.has("regn_reqd_for_parking")
                                && !TextUtils.isEmpty(object.getString("regn_reqd_for_parking"))
                                && !object.getString("regn_reqd_for_parking").equals("null")) {
                            eventDefinition.setRegn_reqd_for_parking(object.getBoolean("regn_reqd_for_parking"));
                        }
                        if (object.has("renewable")
                                && !TextUtils.isEmpty(object.getString("renewable"))
                                && !object.getString("renewable").equals("null")) {
                            eventDefinition.setRenewable(object.getBoolean("renewable"));
                        }
                        if (object.has("active")
                                && !TextUtils.isEmpty(object.getString("active"))
                                && !object.getString("active").equals("null")) {
                            eventDefinition.setActive(object.getBoolean("active"));
                        }

                        if (object.has("event_multi_sessions")
                                && !TextUtils.isEmpty(object.getString("event_multi_sessions"))
                                && !object.getString("event_multi_sessions").equals("null")) {
                            eventDefinition.setEvent_multi_sessions(object.getBoolean("event_multi_sessions"));
                        }

                        if (object.has("event_indiv_session_regn_allowed")
                                && !TextUtils.isEmpty(object.getString("event_indiv_session_regn_allowed"))
                                && !object.getString("event_indiv_session_regn_allowed").equals("null")) {
                            eventDefinition.setEvent_indiv_session_regn_allowed(object.getBoolean("event_indiv_session_regn_allowed"));
                        }

                        if (object.has("event_indiv_session_regn_required")
                                && !TextUtils.isEmpty(object.getString("event_indiv_session_regn_required"))
                                && !object.getString("event_indiv_session_regn_required").equals("null")) {
                            eventDefinition.setEvent_indiv_session_regn_required(object.getBoolean("event_indiv_session_regn_required"));
                        }

                        if (object.has("event_full_regn_required")
                                && !TextUtils.isEmpty(object.getString("event_full_regn_required"))
                                && !object.getString("event_full_regn_required").equals("null")) {
                            eventDefinition.setEvent_full_regn_required(object.getBoolean("event_full_regn_required"));
                        }

                        if (object.has("event_full_regn_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_regn_fee"))
                                && !object.getString("event_full_regn_fee").equals("null")) {
                            eventDefinition.setEvent_full_regn_fee(object.getString("event_full_regn_fee"));
                        }

                        if (object.has("event_parking_fee")
                                && !TextUtils.isEmpty(object.getString("event_parking_fee"))
                                && !object.getString("event_parking_fee").equals("null")) {
                            eventDefinition.setEvent_parking_fee(object.getString("event_parking_fee"));
                        }

                        if (object.has("location_lat_lng")
                                && !TextUtils.isEmpty(object.getString("location_lat_lng"))
                                && !object.getString("location_lat_lng").equals("null")) {
                            eventDefinition.setLocation_lat_lng(object.getString("location_lat_lng"));
                        }

                        if (object.has("regn_user_info_reqd")
                                && !TextUtils.isEmpty(object.getString("regn_user_info_reqd"))
                                && !object.getString("regn_user_info_reqd").equals("null")) {
                            eventDefinition.setRegn_user_info_reqd(object.getBoolean("regn_user_info_reqd"));
                        }

                        if (object.has("regn_addl_user_info_reqd")
                                && !TextUtils.isEmpty(object.getString("regn_addl_user_info_reqd"))
                                && !object.getString("regn_addl_user_info_reqd").equals("null")) {
                            eventDefinition.setRegn_addl_user_info_reqd(object.getBoolean("regn_addl_user_info_reqd"));
                        }

                        if (object.has("free_event")
                                && !TextUtils.isEmpty(object.getString("free_event"))
                                && !object.getString("free_event").equals("null")) {
                            eventDefinition.setFree_event(object.getBoolean("free_event"));
                        }

                        if (object.has("free_event_parking")
                                && !TextUtils.isEmpty(object.getString("free_event_parking"))
                                && !object.getString("free_event_parking").equals("null")) {
                            eventDefinition.setFree_event_parking(object.getBoolean("free_event_parking"));
                        }

                        if (object.has("event_link_array")
                                && !TextUtils.isEmpty(object.getString("event_link_array"))
                                && !object.getString("event_link_array").equals("null")) {
                            eventDefinition.setEvent_link_array(object.getString("event_link_array"));
                        }

                        Log.e("#DEBUG", "  fetchEventDetails:  eventDefinition:  " + new Gson().toJson(eventDefinition));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                progressDialog.dismiss();
                updateViews();
            }
        }
    }

    @Override
    protected void updateViews() {
//        if (eventDefinition != null) {
//            Log.e("#DEBUG", "   updateView:  eventDetails:  " + new Gson().toJson(eventDefinition));
//            if (eventDefinition.getEvent_link_array() != null && !TextUtils.isEmpty(eventDefinition.getEvent_link_array()) && !eventDefinition.getEvent_link_array().equals("null")) {
//                mediaDefinitions.addAll(new Gson().fromJson(eventDefinition.getEvent_link_array(),
//                        new TypeToken<ArrayList<MediaDefinition>>() {
//                        }.getType()));
//                mediaAdapter.notifyDataSetChanged();
//            }
//
//            if (mediaDefinitions.size() > 0) {
//                binding.llMediaLinks.setVisibility(View.VISIBLE);
//                binding.viewMediaLink.setVisibility(View.VISIBLE);
//            } else {
//                binding.llMediaLinks.setVisibility(View.GONE);
//                binding.viewMediaLink.setVisibility(View.GONE);
//            }
//
//            if (eventDefinition.isEvent_multi_sessions()) {
//                binding.tvBtnEventSessions.setVisibility(View.VISIBLE);
//            } else {
//                binding.tvBtnEventSessions.setVisibility(View.GONE);
//            }
//
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_blob_image())) {
//                eventImages.addAll(new Gson().fromJson(eventDefinition.getEvent_blob_image(),
//                        new TypeToken<ArrayList<String>>() {
//                        }.getType()));
//            }
//
//            if (eventDefinition.getEvent_image1() != null
//                    && !TextUtils.isEmpty(eventDefinition.getEvent_image1())
//                    && !eventDefinition.getEvent_image1().equals("null")) {
//                eventImages.add(eventDefinition.getEvent_image1());
//            }
//            if (eventDefinition.getEvent_image2() != null
//                    && !TextUtils.isEmpty(eventDefinition.getEvent_image2())
//                    && !eventDefinition.getEvent_image2().equals("null")) {
//                eventImages.add(eventDefinition.getEvent_image2());
//            }
//
//            eventImageAdapter.notifyDataSetChanged();
//
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_name())) {
//                binding.tvEventTitle.setText(eventDefinition.getEvent_name());
//            }
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_type())) {
//                binding.tvEventType.setText(eventDefinition.getEvent_type());
//            }
//
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_category_type())) {
//                Log.d("#DEBUG", "type: " + eventDefinition.getEvent_category_type());
//                binding.tvEventCategoryType.setText(eventDefinition.getEvent_category_type());
//            }
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_begins_date_time())
//                    && !TextUtils.isEmpty(eventDefinition.getEvent_ends_date_time())) {
//                try {
//                    binding.tvEventTime.setText(String.format("%s to %s",
//                            dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_begins_date_time())),
//                            dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_ends_date_time()))));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            if (eventDefinition.getEvent_logi_type() != EVENT_TYPE_AT_VIRTUALLY) {
//
//                binding.llEventAddress.setVisibility(View.VISIBLE);
//                binding.llParkingAddress.setVisibility(View.VISIBLE);
//                binding.llParkingTime.setVisibility(View.VISIBLE);
//                binding.llEventParkingRate.setVisibility(View.VISIBLE);
//                binding.tvBtnEventRules.setVisibility(View.VISIBLE);
//                binding.llVehicleCheckInMenu.setVisibility(View.VISIBLE);
//                binding.llDirectionStreet.setVisibility(View.VISIBLE);
//
//                if (!TextUtils.isEmpty(eventDefinition.getEvent_parking_begins_date_time())
//                        && !TextUtils.isEmpty(eventDefinition.getEvent_parking_ends_date_time())) {
//                    try {
//                        binding.tvParkingTime.setText(String.format("%s to %s",
//                                dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_parking_begins_date_time())),
//                                dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_parking_ends_date_time()))));
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                if (!TextUtils.isEmpty(eventDefinition.getEvent_address())) {
//                    binding.tvEventAddress.setText(eventDefinition.getEvent_address());
//                    try {
//                        JSONArray jsonArray = new JSONArray(eventDefinition.getEvent_address());
//                        if (jsonArray.length() > 0) {
//                            StringBuilder builder = new StringBuilder();
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                JSONArray jsonArray1 = jsonArray.getJSONArray(i);
//                                builder.append(jsonArray1.getString(0));
//                                builder.append("\n\n");
//                            }
//                            binding.tvEventAddress.setText(builder.toString());
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                if (!TextUtils.isEmpty(eventDefinition.getLocation_address())) {
//                    binding.tvLocationAddress.setText(eventDefinition.getLocation_address());
//                    try {
//                        JSONArray jsonArray = new JSONArray(eventDefinition.getLocation_address());
//                        if (jsonArray.length() > 0) {
//                            StringBuilder builder = new StringBuilder();
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                JSONArray jsonArray1 = jsonArray.getJSONArray(i);
//                                builder.append(jsonArray1.getString(0));
//                                builder.append("\n\n");
//                            }
//                            binding.tvLocationAddress.setText(builder.toString());
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                if (!TextUtils.isEmpty(eventDefinition.getEvent_parking_fee())) {
//                    if (eventDefinition.getEvent_parking_fee().equals("0")) {
//                        binding.tvEventRateParking.setText("FREE");
//                    } else {
//                        binding.tvEventRateParking.setText(String.format("$%s", eventDefinition.getEvent_parking_fee()));
//                    }
//                } else {
//                    binding.tvLabelCostParking.setVisibility(View.GONE);
//                    binding.tvEventRateParking.setVisibility(View.GONE);
//                }
//
//                if (eventDefinition.getEvent_parking_timings() != null) {
//                    try {
//                        JSONArray jsonArrayTime = null;
//                        if (eventDefinition.getEvent_parking_timings() != null) {
//                            StringBuilder stringBuilder = new StringBuilder();
//                            jsonArrayTime = new JSONArray(eventDefinition.getEvent_parking_timings());
//                            for (int i = 0; i < jsonArrayTime.length(); i++) {
//                                JSONArray jsonArray2 = jsonArrayTime.getJSONArray(i);
//                                if (i != 0) {
//                                    stringBuilder.append("\n");
//                                }
//                                if (jsonArray2 != null && jsonArray2.length() > 0) {
////                                    stringBuilder.append(jsonArray2.get(0).toString());
//                                    if (jsonArray2.get(0).toString().length() > 30) {
//                                        String[] s = jsonArray2.get(0).toString().split(" - ");
//                                        for (int j = 0; j < s.length; j++) {
//                                            if (j != 0) {
//                                                stringBuilder.append("-");
//                                            }
//                                            stringBuilder.append(DateTimeUtils.gmtToLocalDateAMPM(s[j]));
//                                        }
//                                    } else {
//                                        stringBuilder.append(jsonArray2.get(0).toString());
//                                    }
//                                }
//                            }
//                            binding.llParkingTime.setVisibility(View.VISIBLE);
//                            binding.tvParkingTime.setText(stringBuilder.toString());
//                        }
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                } else {
//                    binding.llParkingTime.setVisibility(View.GONE);
//                }
//
//                if (eventDefinition.getEvent_event_timings() != null
//                        && !TextUtils.isEmpty(eventDefinition.getEvent_event_timings())) {
//                    try {
//                        JSONArray jsonArrayTime = new JSONArray(eventDefinition.getEvent_event_timings());
//                        StringBuilder stringBuilder = new StringBuilder();
//                        if (jsonArrayTime != null && jsonArrayTime.length() > 0) {
//                            JSONArray jsonArray2 = jsonArrayTime.getJSONArray(0);
//                            if (jsonArray2 != null && jsonArray2.length() > 0) {
//                                if (jsonArray2.get(0).toString().length() > 30) {
//                                    String[] s = jsonArray2.get(0).toString().split(" - ");
//                                    for (int j = 0; j < s.length; j++) {
//                                        if (j != 0) {
//                                            stringBuilder.append("-");
//                                        }
//                                        stringBuilder.append(DateTimeUtils.gmtToLocalDateAMPM(s[j]));
//                                    }
//                                } else {
//                                    stringBuilder.append(jsonArray2.get(0).toString());
//                                }
//                            }
//                        }
//                        binding.llEventTime.setVisibility(View.VISIBLE);
//                        binding.tvEventTime.setText(stringBuilder.toString());
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    binding.llEventTime.setVisibility(View.GONE);
//                }
//
////                if (eventDefinition.getEvent_event_timings() != null) {
////
////                    try {
////                        JSONArray jsonArrayTime = null;
////                        if (eventDefinition.getEvent_event_timings() != null) {
////                            StringBuilder stringBuilder = new StringBuilder();
////                            jsonArrayTime = new JSONArray(eventDefinition.getEvent_event_timings());
////                            for (int i = 0; i < jsonArrayTime.length(); i++) {
////                                JSONArray jsonArray2 = jsonArrayTime.getJSONArray(i);
////                                if (i != 0) {
////                                    stringBuilder.append("\n");
////                                }
////                                if (jsonArray2 != null && jsonArray2.length() > 0) {
////                                    stringBuilder.append(jsonArray2.get(0).toString());
////                                }
////                            }
////                            binding.llEventTime.setVisibility(View.VISIBLE);
////                            binding.tvEventTime.setText(stringBuilder.toString());
////                        }
////
////                    } catch (Exception e) {
////                        e.printStackTrace();
////                    }
////                } else {
////                    binding.llEventTime.setVisibility(View.GONE);
////                }
//
////                if (eventDefinition.getEvent_multi_dates() != null
////                        && !TextUtils.isEmpty(eventDefinition.getEvent_multi_dates())) {
////
////                    try {
////                        JSONArray jsonArrayTime = null;
////                        if (eventDefinition.getEvent_event_timings() != null) {
////                            StringBuilder stringBuilder = new StringBuilder();
////                            jsonArrayTime = new JSONArray(eventDefinition.getEvent_event_timings());
////                            for (int i = 0; i < jsonArrayTime.length(); i++) {
////                                JSONArray jsonArray2 = jsonArrayTime.getJSONArray(i);
////                                if (i != 0) {
////                                    stringBuilder.append("\n");
////                                }
////                                if (jsonArray2 != null && jsonArray2.length() > 0) {
////                                    stringBuilder.append(jsonArray2.get(0).toString());
////                                }
////                            }
////                            binding.tvEventTime.setText(stringBuilder.toString());
////                        }
////
////                    } catch (Exception e) {
////                        e.printStackTrace();
////                    }
////
//////                    try {
//////                        JSONArray jsonArray = null;
//////                        if (eventDefinition.getEvent_multi_dates() != null) {
//////                            jsonArray = new JSONArray(eventDefinition.getEvent_multi_dates());
//////                        }
//////                        JSONArray jsonArrayTime = null;
//////                        if (eventDefinition.getEvent_event_timings() != null) {
//////                            jsonArrayTime = new JSONArray(eventDefinition.getEvent_event_timings());
//////                        }
//////                        JSONArray jsonArrayParking = null;
//////                        if (eventDefinition.getEvent_parking_timings() != null) {
//////                            jsonArrayParking = new JSONArray(eventDefinition.getEvent_parking_timings());
//////                        }
//////                        StringBuilder stringBuilder = new StringBuilder();
//////                        StringBuilder stringBuilderParking = new StringBuilder();
//////                        if (jsonArray != null) {
//////                            for (int i = 0; i < jsonArray.length(); i++) {
//////                                JSONArray jsonArray1 = jsonArray.getJSONArray(i);
//////                                JSONArray jsonArray2 = null;
//////                                if (jsonArrayTime != null) {
//////                                    jsonArray2 = jsonArrayTime.getJSONArray(i);
//////                                }
//////                                JSONArray jsonArray3 = null;
//////                                if (jsonArrayParking != null) {
//////                                    jsonArray3 = jsonArrayParking.getJSONArray(i);
//////                                }
//////                                if (jsonArray1 != null && jsonArray1.length() > 0) {
//////                                    if (i != 0) {
//////                                        stringBuilder.append("\n");
//////                                        stringBuilderParking.append("\n");
//////                                    }
//////                                    stringBuilder.append(DateTimeUtils.getMMMDDYYYY(jsonArray1.get(0).toString()));
//////                                    stringBuilderParking.append(DateTimeUtils.getMMMDDYYYY(jsonArray1.get(0).toString()));
//////                                    stringBuilder.append("      ");
//////                                    stringBuilderParking.append("      ");
//////                                    if (jsonArray2 != null && jsonArray2.length() > 0) {
//////                                        stringBuilder.append(jsonArray2.get(0).toString());
//////                                    }
//////                                    if (jsonArray3 != null && jsonArray3.length() > 0) {
//////                                        stringBuilderParking.append(jsonArray3.get(0).toString());
//////                                    }
//////                                }
//////                            }
//////                        }
//////                        binding.tvEventTime.setText(stringBuilder.toString());
//////                        binding.tvParkingTime.setText(stringBuilderParking.toString());
//////                    } catch (JSONException e) {
//////                        e.printStackTrace();
//////                    }
////                }
//
//                try {
//                    JSONArray jsonArrayLatLng = new JSONArray(eventDefinition.getLocation_lat_lng());
//                    for (int i = 0; i < jsonArrayLatLng.length(); i++) {
//                        JSONArray jsonArray2 = jsonArrayLatLng.getJSONArray(i);
//                        for (int j = 0; j < jsonArray2.length(); j++) {
//                            String[] latLng = jsonArray2.getString(j).split(",");
//                            if (latLng.length == 2) {
//                                eventDefinition.setLat(latLng[0]);
//                                eventDefinition.setLng(latLng[1]);
//
//                                JSONArray jsonArray11 = new JSONArray(eventDefinition.getEvent_address());
//                                JSONArray jsonArray22 = jsonArray11.getJSONArray(i);
//                                eventDefinition.setDisplay_address(jsonArray22.getString(0));
//                            }
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            } else {
//
//                binding.llEventAddress.setVisibility(View.GONE);
//                binding.llParkingAddress.setVisibility(View.GONE);
//                binding.llParkingTime.setVisibility(View.GONE);
//                binding.llEventParkingRate.setVisibility(View.GONE);
//                binding.tvBtnEventRules.setVisibility(View.GONE);
//                binding.llVehicleCheckInMenu.setVisibility(View.GONE);
//                binding.llDirectionStreet.setVisibility(View.GONE);
//
//            }
//
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_short_description())) {
//                binding.tvEventShortDesc.setText(eventDefinition.getEvent_short_description());
//            }
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_long_description())) {
//                binding.tvEventLongDesc.setText(eventDefinition.getEvent_long_description());
//            }
//
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_full_regn_fee())) {
//                if (eventDefinition.getEvent_full_regn_fee().equals("0")) {
//                    binding.tvEventRate.setText("FREE");
//                } else {
//                    binding.tvEventRate.setText(String.format("$%s", eventDefinition.getEvent_full_regn_fee()));
//                }
//            } else {
//                binding.tvLabelCost.setVisibility(View.GONE);
//                binding.tvEventRate.setVisibility(View.GONE);
//            }
//
//            if (!TextUtils.isEmpty(eventDefinition.getYear())) {
//                binding.tvEventYear.setText(eventDefinition.getYear());
//            }
//
//            if (!TextUtils.isEmpty(eventDefinition.getCompany_logo())) {
//                Glide.with(getActivity()).load(eventDefinition.getCompany_logo())
//                        .into(binding.ivCompanyLogo);
//            }
//
//        }
//
//        if (isCheckIn) {
//            binding.llEventBottomMenu.setVisibility(View.GONE);
//            binding.llEventCheckInMenu.setVisibility(View.VISIBLE);
//            binding.llVehicleCheckInMenu.setVisibility(View.VISIBLE);
//        } else {
//            binding.llEventBottomMenu.setVisibility(View.VISIBLE);
//            binding.llEventCheckInMenu.setVisibility(View.GONE);
//            binding.llVehicleCheckInMenu.setVisibility(View.GONE);
//        }

        if (eventDefinition != null) {
            Log.e("#DEBUG", "   updateView:  eventDetails:  " + new Gson().toJson(eventDefinition));
//            if (eventDefinition.isEvent_invite_only()) {
//                binding.tvBtnInviteEvent.setVisibility(View.VISIBLE);
//            } else {
//                binding.tvBtnInviteEvent.setVisibility(View.GONE);
//            }
            if (eventDefinition.getEvent_link_array() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_link_array())
                    && !eventDefinition.getEvent_link_array().equals("null")) {
                mediaDefinitions.addAll(new Gson().fromJson(eventDefinition.getEvent_link_array(),
                        new TypeToken<ArrayList<MediaDefinition>>() {
                        }.getType()));
                mediaAdapter.notifyDataSetChanged();
            }

            if (mediaDefinitions.size() > 0) {
                binding.llMediaLinks.setVisibility(View.VISIBLE);
                binding.viewMediaLink.setVisibility(View.VISIBLE);
            } else {
                binding.llMediaLinks.setVisibility(View.GONE);
                binding.viewMediaLink.setVisibility(View.GONE);
            }

            if (eventDefinition.isEvent_multi_sessions()) {
                binding.tvBtnEventSessions.setVisibility(View.VISIBLE);
            } else {
                binding.tvBtnEventSessions.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(eventDefinition.getEvent_blob_image())) {
                eventImages.addAll(new Gson().fromJson(eventDefinition.getEvent_blob_image(),
                        new TypeToken<ArrayList<String>>() {
                        }.getType()));
            }

            if (eventDefinition.getEvent_image1() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_image1())
                    && !eventDefinition.getEvent_image1().equals("null")) {
                eventImages.add(eventDefinition.getEvent_image1());
            }
            if (eventDefinition.getEvent_image2() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_image2())
                    && !eventDefinition.getEvent_image2().equals("null")) {
                eventImages.add(eventDefinition.getEvent_image2());
            }

            eventImageAdapter.notifyDataSetChanged();

            if (!TextUtils.isEmpty(eventDefinition.getEvent_name())) {
                binding.tvEventTitle.setText(eventDefinition.getEvent_name());
            }
            if (!TextUtils.isEmpty(eventDefinition.getEvent_type())) {
                binding.tvEventType.setText(eventDefinition.getEvent_type());
            }

            if (!TextUtils.isEmpty(eventDefinition.getEvent_category_type())) {
                Log.d("#DEBUG", "type: " + eventDefinition.getEvent_category_type());
                binding.tvEventCategoryType.setText(eventDefinition.getEvent_category_type());
            }

            if (!TextUtils.isEmpty(eventDefinition.getEvent_begins_date_time())
                    && !TextUtils.isEmpty(eventDefinition.getEvent_ends_date_time())) {
                try {
                    binding.tvEventTime.setText(String.format("%s to %s",
                            dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_begins_date_time())),
                            dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_ends_date_time()))));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (eventDefinition.getEvent_logi_type() != EVENT_TYPE_AT_VIRTUALLY) {

                binding.llEventAddress.setVisibility(View.VISIBLE);
                binding.llParkingAddress.setVisibility(View.VISIBLE);
                binding.llParkingTime.setVisibility(View.VISIBLE);
                binding.llEventParkingRate.setVisibility(View.VISIBLE);
                binding.tvBtnEventRules.setVisibility(View.VISIBLE);
                binding.llVehicleCheckInMenu.setVisibility(View.VISIBLE);
                binding.llDirectionStreet.setVisibility(View.VISIBLE);

                if (!TextUtils.isEmpty(eventDefinition.getEvent_parking_begins_date_time())
                        && !TextUtils.isEmpty(eventDefinition.getEvent_parking_ends_date_time())) {
                    try {
                        binding.tvParkingTime.setText(String.format("%s to %s",
                                dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_parking_begins_date_time())),
                                dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_parking_ends_date_time()))));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                if (!TextUtils.isEmpty(eventDefinition.getEvent_address())) {
                    binding.tvEventAddress.setText(eventDefinition.getEvent_address());
                    try {
                        JSONArray jsonArray = new JSONArray(eventDefinition.getEvent_address());
                        if (jsonArray.length() > 0) {
                            StringBuilder builder = new StringBuilder();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONArray jsonArray1 = jsonArray.getJSONArray(i);
                                builder.append(jsonArray1.getString(0));
                                builder.append("\n\n");
                            }
                            binding.tvEventAddress.setText(builder.toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (!TextUtils.isEmpty(eventDefinition.getLocation_address())) {
                    binding.tvLocationAddress.setText(eventDefinition.getLocation_address());
                    try {
                        JSONArray jsonArray = new JSONArray(eventDefinition.getLocation_address());
                        if (jsonArray.length() > 0) {
                            StringBuilder builder = new StringBuilder();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONArray jsonArray1 = jsonArray.getJSONArray(i);
                                builder.append(jsonArray1.getString(0));
                                builder.append("\n\n");
                            }
                            binding.tvLocationAddress.setText(builder.toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (!TextUtils.isEmpty(eventDefinition.getEvent_parking_fee())) {
                    if (eventDefinition.getEvent_parking_fee().equals("0")) {
                        binding.tvEventRateParking.setText("FREE");
                    } else {
                        binding.tvEventRateParking.setText(String.format("$%s", eventDefinition.getEvent_parking_fee()));
                    }
                } else {
                    binding.tvLabelCostParking.setVisibility(View.GONE);
                    binding.tvEventRateParking.setVisibility(View.GONE);
                }

                if (eventDefinition.getEvent_parking_timings() != null) {
                    try {
                        JSONArray jsonArrayTime = null;
                        if (eventDefinition.getEvent_parking_timings() != null) {
                            StringBuilder stringBuilder = new StringBuilder();
                            jsonArrayTime = new JSONArray(eventDefinition.getEvent_parking_timings());
                            for (int i = 0; i < jsonArrayTime.length(); i++) {
                                JSONArray jsonArray2 = jsonArrayTime.getJSONArray(i);
                                if (i != 0) {
                                    stringBuilder.append("\n");
                                }
                                if (jsonArray2 != null && jsonArray2.length() > 0) {
//                                    stringBuilder.append(jsonArray2.get(0).toString());
                                    if (jsonArray2.get(0).toString().length() > 30) {
                                        String[] s = jsonArray2.get(0).toString().split(" - ");
                                        for (int j = 0; j < s.length; j++) {
                                            if (j != 0) {
                                                stringBuilder.append("-");
                                            }
                                            stringBuilder.append(DateTimeUtils.gmtToLocalDateAMPM(s[j]));
                                        }
                                    } else {
                                        stringBuilder.append(jsonArray2.get(0).toString());
                                    }
                                }
                            }
                            binding.llParkingTime.setVisibility(View.VISIBLE);
                            binding.tvParkingTime.setText(stringBuilder.toString());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    binding.llParkingTime.setVisibility(View.GONE);
                }

                if (eventDefinition.getEvent_event_timings() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_event_timings())) {
                    try {
                        JSONArray jsonArrayTime = new JSONArray(eventDefinition.getEvent_event_timings());
                        StringBuilder stringBuilder = new StringBuilder();
                        if (jsonArrayTime != null && jsonArrayTime.length() > 0) {
                            JSONArray jsonArray2 = jsonArrayTime.getJSONArray(0);
                            if (jsonArray2 != null && jsonArray2.length() > 0) {
                                if (jsonArray2.get(0).toString().length() > 30) {
                                    String[] s = jsonArray2.get(0).toString().split(" - ");
                                    for (int j = 0; j < s.length; j++) {
                                        if (j != 0) {
                                            stringBuilder.append("-");
                                        }
                                        stringBuilder.append(DateTimeUtils.gmtToLocalDateAMPM(s[j]));
                                    }
                                } else {
                                    stringBuilder.append(jsonArray2.get(0).toString());
                                }
                            }
                        }
                        binding.llEventTime.setVisibility(View.VISIBLE);
                        binding.tvEventTime.setText(stringBuilder.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    binding.llEventTime.setVisibility(View.GONE);
                }

//                if (eventDefinition.getEvent_event_timings() != null) {
//
//                    try {
//                        JSONArray jsonArrayTime = null;
//                        if (eventDefinition.getEvent_event_timings() != null) {
//                            StringBuilder stringBuilder = new StringBuilder();
//                            jsonArrayTime = new JSONArray(eventDefinition.getEvent_event_timings());
//                            for (int i = 0; i < jsonArrayTime.length(); i++) {
//                                JSONArray jsonArray2 = jsonArrayTime.getJSONArray(i);
//                                if (i != 0) {
//                                    stringBuilder.append("\n");
//                                }
//                                if (jsonArray2 != null && jsonArray2.length() > 0) {
//                                    stringBuilder.append(jsonArray2.get(0).toString());
//                                }
//                            }
//                            binding.llEventTime.setVisibility(View.VISIBLE);
//                            binding.tvEventTime.setText(stringBuilder.toString());
//                        }
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    binding.llEventTime.setVisibility(View.GONE);
//                }

//                if (eventDefinition.getEvent_multi_dates() != null
//                        && !TextUtils.isEmpty(eventDefinition.getEvent_multi_dates())) {
//
//                    try {
//                        JSONArray jsonArrayTime = null;
//                        if (eventDefinition.getEvent_event_timings() != null) {
//                            StringBuilder stringBuilder = new StringBuilder();
//                            jsonArrayTime = new JSONArray(eventDefinition.getEvent_event_timings());
//                            for (int i = 0; i < jsonArrayTime.length(); i++) {
//                                JSONArray jsonArray2 = jsonArrayTime.getJSONArray(i);
//                                if (i != 0) {
//                                    stringBuilder.append("\n");
//                                }
//                                if (jsonArray2 != null && jsonArray2.length() > 0) {
//                                    stringBuilder.append(jsonArray2.get(0).toString());
//                                }
//                            }
//                            binding.tvEventTime.setText(stringBuilder.toString());
//                        }
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
////                    try {
////                        JSONArray jsonArray = null;
////                        if (eventDefinition.getEvent_multi_dates() != null) {
////                            jsonArray = new JSONArray(eventDefinition.getEvent_multi_dates());
////                        }
////                        JSONArray jsonArrayTime = null;
////                        if (eventDefinition.getEvent_event_timings() != null) {
////                            jsonArrayTime = new JSONArray(eventDefinition.getEvent_event_timings());
////                        }
////                        JSONArray jsonArrayParking = null;
////                        if (eventDefinition.getEvent_parking_timings() != null) {
////                            jsonArrayParking = new JSONArray(eventDefinition.getEvent_parking_timings());
////                        }
////                        StringBuilder stringBuilder = new StringBuilder();
////                        StringBuilder stringBuilderParking = new StringBuilder();
////                        if (jsonArray != null) {
////                            for (int i = 0; i < jsonArray.length(); i++) {
////                                JSONArray jsonArray1 = jsonArray.getJSONArray(i);
////                                JSONArray jsonArray2 = null;
////                                if (jsonArrayTime != null) {
////                                    jsonArray2 = jsonArrayTime.getJSONArray(i);
////                                }
////                                JSONArray jsonArray3 = null;
////                                if (jsonArrayParking != null) {
////                                    jsonArray3 = jsonArrayParking.getJSONArray(i);
////                                }
////                                if (jsonArray1 != null && jsonArray1.length() > 0) {
////                                    if (i != 0) {
////                                        stringBuilder.append("\n");
////                                        stringBuilderParking.append("\n");
////                                    }
////                                    stringBuilder.append(DateTimeUtils.getMMMDDYYYY(jsonArray1.get(0).toString()));
////                                    stringBuilderParking.append(DateTimeUtils.getMMMDDYYYY(jsonArray1.get(0).toString()));
////                                    stringBuilder.append("      ");
////                                    stringBuilderParking.append("      ");
////                                    if (jsonArray2 != null && jsonArray2.length() > 0) {
////                                        stringBuilder.append(jsonArray2.get(0).toString());
////                                    }
////                                    if (jsonArray3 != null && jsonArray3.length() > 0) {
////                                        stringBuilderParking.append(jsonArray3.get(0).toString());
////                                    }
////                                }
////                            }
////                        }
////                        binding.tvEventTime.setText(stringBuilder.toString());
////                        binding.tvParkingTime.setText(stringBuilderParking.toString());
////                    } catch (JSONException e) {
////                        e.printStackTrace();
////                    }
//                }

                try {
                    JSONArray jsonArrayLatLng = new JSONArray(eventDefinition.getLocation_lat_lng());
                    for (int i = 0; i < jsonArrayLatLng.length(); i++) {
                        JSONArray jsonArray2 = jsonArrayLatLng.getJSONArray(i);
                        for (int j = 0; j < jsonArray2.length(); j++) {
                            String[] latLng = jsonArray2.getString(j).split(",");
                            if (latLng.length == 2) {
                                eventDefinition.setLat(latLng[0]);
                                eventDefinition.setLng(latLng[1]);

                                JSONArray jsonArray11 = new JSONArray(eventDefinition.getEvent_address());
                                JSONArray jsonArray22 = jsonArray11.getJSONArray(i);
                                eventDefinition.setDisplay_address(jsonArray22.getString(0));
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {

                binding.llEventAddress.setVisibility(View.GONE);
                binding.llParkingAddress.setVisibility(View.GONE);
                binding.llParkingTime.setVisibility(View.GONE);
                binding.llEventParkingRate.setVisibility(View.GONE);
                binding.tvBtnEventRules.setVisibility(View.GONE);
                binding.llVehicleCheckInMenu.setVisibility(View.GONE);
                binding.llDirectionStreet.setVisibility(View.GONE);

            }

            if (!TextUtils.isEmpty(eventDefinition.getEvent_short_description())) {
                binding.tvEventShortDesc.setText(eventDefinition.getEvent_short_description());
            }
            if (!TextUtils.isEmpty(eventDefinition.getEvent_long_description())) {
                binding.tvEventLongDesc.setText(eventDefinition.getEvent_long_description());
            }

            if (!TextUtils.isEmpty(eventDefinition.getEvent_full_regn_fee())) {
                if (eventDefinition.getEvent_full_regn_fee().equals("0")) {
                    binding.tvEventRate.setText("FREE");
                } else {
                    binding.tvEventRate.setText(String.format("$%s", eventDefinition.getEvent_full_regn_fee()));
                }
            } else {
                binding.tvEventRate.setText("FREE");
//                binding.tvEventRate.setVisibility(View.VISIBLE);
//                binding.tvLabelCost.setVisibility(View.VISIBLE);
            }

            if (!TextUtils.isEmpty(eventDefinition.getWeb_event_full_regn_fee())) {
                if (eventDefinition.getWeb_event_full_regn_fee().equals("0")) {
                    binding.tvWebEventRate.setText("FREE");
                } else {
                    binding.tvWebEventRate.setText(String.format("$%s", eventDefinition.getWeb_event_full_regn_fee()));
                }
            } else {
                binding.tvWebEventRate.setText("FREE");
                if (eventDefinition.getEvent_logi_type() == 3) {
                    binding.tvWebEventRate.setVisibility(View.GONE);
                    binding.tvWebLabelCost.setVisibility(View.GONE);
                }
            }
            if (eventDefinition.getEvent_logi_type() == 2) {
                binding.tvLabelCost.setVisibility(View.GONE);
                binding.tvEventRate.setVisibility(View.GONE);
                /*binding.tvWebEventRate.setVisibility(View.VISIBLE);
                binding.tvWebLabelCost.setVisibility(View.VISIBLE);*/
            }
           /* if (!TextUtils.isEmpty(eventDefinition.getWeb_event_full_regn_fee()) && (TextUtils.isEmpty(eventDefinition.getEvent_full_regn_fee()) || eventDefinition.getEvent_full_regn_fee().equals("0"))) {
                if (eventDefinition.getEvent_logi_type() == 2 || eventDefinition.getEvent_logi_type() == 3) {
                    if (eventDefinition.getWeb_event_full_regn_fee().equals("0")) {
                        binding.tvEventRate.setText("FREE");
                    } else {
                        binding.tvEventRate.setText(String.format("$%s", eventDefinition.getWeb_event_full_regn_fee()));
                    }
                }
            } else if (!TextUtils.isEmpty(eventDefinition.getEvent_full_regn_fee())) {
                if (eventDefinition.getEvent_full_regn_fee().equals("0")) {
                    binding.tvEventRate.setText("FREE");
                } else {
                    binding.tvEventRate.setText(String.format("$%s", eventDefinition.getEvent_full_regn_fee()));
                }
            } else {
                if(eventDefinition.getCost().equals("0") || TextUtils.isEmpty(eventDefinition.getCost()))
                    binding.tvEventRate.setText("FREE");
//                binding.tvEventRate.setVisibility(View.GONE);
//                binding.tvLabelCost.setVisibility(View.VISIBLE);
            }*/
            if (!TextUtils.isEmpty(eventDefinition.getYear())) {
                binding.tvEventYear.setText(eventDefinition.getYear());
            }

            if (!TextUtils.isEmpty(eventDefinition.getCompany_logo())) {
                Glide.with(getActivity()).load(eventDefinition.getCompany_logo())
                        .into(binding.ivCompanyLogo);
            }
        }
        if (isCheckIn) {
            binding.llEventBottomMenu.setVisibility(View.GONE);
            binding.llEventCheckInMenu.setVisibility(View.VISIBLE);
            binding.llVehicleCheckInMenu.setVisibility(View.VISIBLE);
        } else {
            binding.llEventBottomMenu.setVisibility(View.VISIBLE);
            binding.llEventCheckInMenu.setVisibility(View.GONE);
            binding.llVehicleCheckInMenu.setVisibility(View.GONE);
        }
    }

    private void openDirections() {
        SharedPreferences sh1 = getActivity().getSharedPreferences("directiontomydirection", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed1 = sh1.edit();
        ed1.putString("lat", eventDefinition.getLat());
        ed1.putString("lang", eventDefinition.getLng());
        ed1.putString("address", eventDefinition.getDisplay_address());
        ed1.commit();
        SharedPreferences backtotimer = getActivity().getSharedPreferences("back_parkezly", Context.MODE_PRIVATE);
        SharedPreferences.Editor eddd = backtotimer.edit();
        eddd.putString("isback", "yes");
        eddd.commit();
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        /* ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                    ft.add(R.id.My_Container_1_ID, new f_direction(), "NewFragmentTag");
                    ft.commit();*/
        f_direction pay = new f_direction();
        ft.add(R.id.My_Container_1_ID, pay, "reservation_details");
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(pay);
        ft.commitAllowingStateLoss();
    }

    private void openStreetView() {
        Intent i = new Intent(getActivity(), panstreetview.class);
        i.putExtra("lat", eventDefinition.getLat());
        i.putExtra("lang", eventDefinition.getLng());
        i.putExtra("title", eventDefinition.getEvent_name());
        i.putExtra("address", eventDefinition.getDisplay_address());
        getActivity().startActivity(i);
    }

    @Override
    protected void setListeners() {

        binding.tvBtnDirection.setOnClickListener(v -> {
            openDirections();
        });

        binding.tvBtnStreetView.setOnClickListener(v -> {
            openStreetView();
        });

        binding.tvBtnCheckInReg.setOnClickListener(v -> {
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString("eventDefinition", new Gson().toJson(eventDefinition));
            bundle.putBoolean("isCheckIn", isCheckIn);
            RegisteredEventsFragment pay = new RegisteredEventsFragment();
            pay.setArguments(bundle);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, pay);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(pay);
            ft.commitAllowingStateLoss();
        });

        binding.tvBtnCheckInNewEntry.setOnClickListener(v -> {
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString("eventDefinition", new Gson().toJson(eventDefinition));
            bundle.putBoolean("isCheckIn", isCheckIn);
            RegisterNewEntryFragment pay = new RegisterNewEntryFragment();
            pay.setArguments(bundle);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, pay);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(pay);
            ft.commitAllowingStateLoss();
        });

        binding.tvBtnEventRules.setOnClickListener(v -> {
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString("type", parkingType);
            bundle.putBoolean("isEventRule", true);
            bundle.putString("eventDefinition", new Gson().toJson(eventDefinition));
            if (selectedManager != null && selectedManager.getManager_id() != null) {
                ParkingPartner parkingPartner = new ParkingPartner();
                parkingPartner.setLocation_code(selectedManager.getManager_id());
                bundle.putString("selectedSpot", new Gson().toJson(parkingPartner));
            }
            EventLocationsFragment pay = new EventLocationsFragment();
            pay.setArguments(bundle);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, pay);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(pay);
            ft.commitAllowingStateLoss();
        });

        binding.tvBtnEventSessions.setOnClickListener(v -> {
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString("type", parkingType);
            bundle.putBoolean("isEventRule", true);
            bundle.putString("eventDefinition", new Gson().toJson(eventDefinition));
            if (selectedManager != null && selectedManager.getManager_id() != null) {
                ParkingPartner parkingPartner = new ParkingPartner();
                parkingPartner.setLocation_code(selectedManager.getManager_id());
                bundle.putString("selectedSpot", new Gson().toJson(parkingPartner));
            }
            SessionsFragment pay = new SessionsFragment();
            pay.setArguments(bundle);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, pay);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(pay);
            ft.commitAllowingStateLoss();
        });

        binding.ivBtnPrev.setOnClickListener(v -> {
            try {
                if (binding.vpEventImage.getCurrentItem() == 0) {
                    binding.vpEventImage.setCurrentItem(1);
                    binding.ivBtnNext.setVisibility(View.GONE);
                    binding.ivBtnPrev.setVisibility(View.VISIBLE);
                } else {
                    binding.vpEventImage.setCurrentItem(0);
                    binding.ivBtnNext.setVisibility(View.VISIBLE);
                    binding.ivBtnPrev.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        binding.ivBtnNext.setOnClickListener(v -> {
            try {
                if (binding.vpEventImage.getCurrentItem() == 0) {
                    binding.vpEventImage.setCurrentItem(1);
                    binding.ivBtnNext.setVisibility(View.GONE);
                    binding.ivBtnPrev.setVisibility(View.VISIBLE);
                } else {
                    binding.ivBtnPrev.setVisibility(View.GONE);
                    binding.ivBtnNext.setVisibility(View.VISIBLE);
                    binding.vpEventImage.setCurrentItem(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    protected void initViews(View v) {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");

        eventImageAdapter = new EventImageAdapter();
        binding.vpEventImage.setAdapter(eventImageAdapter);

        mediaAdapter = new MediaAdapter();
        binding.rvMediaLinks.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        binding.rvMediaLinks.setAdapter(mediaAdapter);

    }

    private class EventImageAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return eventImages.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == ((LinearLayout) o);
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View itemView = LayoutInflater.from(getActivity()).inflate(R.layout.item_event_image, container, false);

            ImageView imageView = itemView.findViewById(R.id.ivEventImage);
            Glide.with(getActivity()).load(eventImages.get(position)).into(imageView);

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((LinearLayout) object);
        }
    }

    private class MediaAdapter extends RecyclerView.Adapter<MediaAdapter.MediaHolder> {

        @NonNull
        @Override
        public MediaAdapter.MediaHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new MediaHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_media_link_info, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MediaAdapter.MediaHolder mediaHolder, int i) {
            mediaHolder.tvBtnPlatform.setText(mediaDefinitions.get(i).getMedia_platform());
        }

        @Override
        public int getItemCount() {
            return mediaDefinitions.size();
        }

        public class MediaHolder extends RecyclerView.ViewHolder {
            TextView tvBtnPlatform;

            public MediaHolder(@NonNull View itemView) {
                super(itemView);
                tvBtnPlatform = itemView.findViewById(R.id.tvBtnPlatform);

                itemView.setOnClickListener(v -> {
                    final int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        MediaDefinition mediaDefinition = mediaDefinitions.get(pos);
                        if (mediaDefinition != null && !TextUtils.isEmpty(mediaDefinition.getMedia_platform())) {
                            if (mediaDefinition.getMedia_platform().equals("Zoom")) {
                                showMediaInfoForZoom(mediaDefinition);
                            } else if (mediaDefinition.getMedia_platform().equals("YouTube")) {
                                showMediaInfoForYoutube(mediaDefinition);
                            } else if (mediaDefinition.getMedia_platform().equals("Facebook")) {
                                showMediaInfoForFacebook(mediaDefinition);
                            } else if (mediaDefinition.getMedia_platform().equals("Google Meet")) {
                                showMediaInfoForGoogleMeet(mediaDefinition);
                            } else if (mediaDefinition.getMedia_platform().equals("Google Classroom")) {
                                showMediaInfoForGoogleClassroom(mediaDefinition);
                            } else if (mediaDefinition.getMedia_platform().equals("Skype")) {
                                showMediaInfoForSkype(mediaDefinition);
                            } else if (mediaDefinition.getMedia_platform().equals("Website")) {
                                openWebUrl(mediaDefinition.getMedia_domain_url());
                            } else if (mediaDefinition.getMedia_platform().equals("PDF")) {
                                PDFViewFragment pdfViewFragment = new PDFViewFragment();
                                Bundle bundle = new Bundle();
                                bundle.putString("mediaDefinition", new Gson().toJson(mediaDefinition));
                                pdfViewFragment.setArguments(bundle);
                                replaceFragment(pdfViewFragment, "pdfView");
                            } else if (mediaDefinition.getMedia_platform().equals("Powerpoint")) {
                                downloadFile(mediaDefinition.getPptFilePath());
                            } else if (mediaDefinition.getMedia_platform().equals("Slideshow")) {
                                downloadFile(mediaDefinition.getFilePath());
                            } else if (mediaDefinition.getMedia_platform().equals("TeleConference")) {
                                showMediaInfoForTeleConference(mediaDefinition);
                            } else if (mediaDefinition.getMedia_platform().equals("Audio/Video")) {
                                String path = mediaDefinition.getAudioVideoPath();
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);//DO NOT FORGET THIS EVER

                                if (mediaDefinition.getAudioVideoType().equals("Audio")) {
                                    intent.setDataAndType(Uri.parse(path), "audio/*");
                                } else if (mediaDefinition.getAudioVideoType().equals("Video")) {
                                    intent.setDataAndType(Uri.parse(path), "video/*");
                                }
                                startActivity(intent);
                            }
                        }
                    }
                });
            }
        }
    }

    private void downloadFile(String url) {
        progressDialog.setMessage("Downloading file...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/parkezly/");
        if (!file.exists()) {
            file.mkdir();
        }
        Log.e("#DEBUG", "  FileName:  " + "test." + url.substring(url.lastIndexOf(".") + 1));
        AndroidNetworking.download(url, file.getAbsolutePath(), "test." + url.substring(url.lastIndexOf(".") + 1))
                .build()
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        progressDialog.dismiss();
                        try {
                            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/parkezly/test.ppt");
                            Uri uri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", file);
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "Sorry, Unable to open file!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Sorry, Unable to open file!", Toast.LENGTH_SHORT).show();
                        Log.e("#DEBUG", "  downloadFile: onError: " + anError.getMessage());
                    }
                });
    }

    private void showMediaInfoForTeleConference(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        DialogEventMediaInfoTeleConferenceBinding teleBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_tele_conference, null, false);
        builder.setView(teleBinding.getRoot());

        teleNumberAdapter = new TeleNumberAdapter();
        teleBinding.rvNumbers.setLayoutManager(new LinearLayoutManager(getActivity()));
        teleBinding.rvNumbers.setAdapter(teleNumberAdapter);
        if (mediaDefinition.getTeleConfNumbers() != null && mediaDefinition.getTeleConfNumbers().size() > 1) {
            teleNumbers.clear();
            teleNumbers.addAll(mediaDefinition.getTeleConfNumbers());
            teleNumberAdapter.notifyDataSetChanged();
        }
        if (mediaDefinition.getTeleConfCountryCode() != null && !TextUtils.isEmpty(mediaDefinition.getTeleConfCountryCode())) {
            teleCountryCode = mediaDefinition.getTeleConfCountryCode();
        }

        teleBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (mediaDefinition.getTeleConfMeetingId() != null && !TextUtils.isEmpty(mediaDefinition.getTeleConfMeetingId())) {
            teleBinding.llMeetingId.setVisibility(View.VISIBLE);
            teleBinding.tvMeetingId.setText(mediaDefinition.getTeleConfMeetingId());
        }
        if (mediaDefinition.getTeleConfMeetingPassword() != null && !TextUtils.isEmpty(mediaDefinition.getTeleConfMeetingPassword())) {
            teleBinding.llMeetingPassword.setVisibility(View.VISIBLE);
            try {
                teleBinding.tvMeetingPassword.setText(AESEncyption.decrypt(mediaDefinition.getTeleConfMeetingPassword()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        AlertDialog alertDialog = builder.create();
        teleBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        alertDialog.show();

    }

    private void showMediaInfoForSkype(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoSkypeBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_skype, null, false);
        builder.setView(zoomBinding.getRoot());

        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getSkypeStreamUrl())) {
            zoomBinding.llSpecialUrl.setVisibility(View.VISIBLE);
            zoomBinding.tvSpecialUrl.setText(mediaDefinition.getSkypeStreamUrl());
        }

        AlertDialog alertDialog = builder.create();
        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnOpen.setOnClickListener(v -> {
        });
        zoomBinding.ivBtnOpenSkypeApp.setOnClickListener(v -> {
            openSkypeUrl(mediaDefinition.getYoutubeStreamUrl());
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
            openWebUrl(mediaDefinition.getSkypeStreamUrl());
        });
        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnOpenSpecialUrl.setOnClickListener(v -> {
            openWebUrl(mediaDefinition.getSkypeStreamUrl());
        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Stream Url", mediaDefinition.getSkypeStreamUrl());
        });

        alertDialog.show();
    }

    private void showMediaInfoForGoogleClassroom(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoGoogleClassroomBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_google_classroom, null, false);
        builder.setView(zoomBinding.getRoot());

        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getGoogleClasstroomCode())) {
            zoomBinding.llClassCode.setVisibility(View.VISIBLE);
            zoomBinding.tvClassCode.setText(mediaDefinition.getGoogleClasstroomCode());
        }

        AlertDialog alertDialog = builder.create();
        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnOpen.setOnClickListener(v -> {
        });

        zoomBinding.ivBtnOpenClassroomApp.setOnClickListener(v -> {
            openGoogleClassroomUrl(mediaDefinition.getMedia_domain_url());
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
            openWebUrl(mediaDefinition.getGoogleClasstroomCode());
        });
        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Classroom Code", mediaDefinition.getGoogleClasstroomCode());
        });

        zoomBinding.btnManage.setOnClickListener(v -> {
            alertDialog.dismiss();
//            if (mediaDefinition.getMedia_platform().equals("Google Classroom")){
//                final FragmentTransaction ft = getFragmentManager().beginTransaction();
//                GClassRoomHomeFragment frag = new GClassRoomHomeFragment();
//                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
//                ft.add(R.id.My_Container_1_ID, frag, "gClassroom");
//                fragmentStack.lastElement().onPause();
//                ft.hide(fragmentStack.lastElement());
//                fragmentStack.push(frag);
//                ft.commitAllowingStateLoss();
//            }
            startActivity(new Intent(getActivity(), GClassroomActivity.class));
        });

        alertDialog.show();
    }

    private void showMediaInfoForGoogleMeet(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoGoogleMeetBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_google_meet, null, false);
        builder.setView(zoomBinding.getRoot());

        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getGoogleMeetUrl())) {
            zoomBinding.llMeetingUrl.setVisibility(View.VISIBLE);
            zoomBinding.tvSpecialUrl.setText(mediaDefinition.getGoogleMeetUrl());
        }
        if (!TextUtils.isEmpty(mediaDefinition.getGoogleMeetPhoneNumber())) {
            zoomBinding.llMeetingPhoneNumber.setVisibility(View.VISIBLE);
            zoomBinding.tvMeetingPhoneNumber.setText(mediaDefinition.getGoogleMeetPhoneNumber());
        }
        if (!TextUtils.isEmpty(mediaDefinition.getGoogleMeetPassword())) {
            zoomBinding.llMeetingPassword.setVisibility(View.VISIBLE);
            try {
                zoomBinding.tvMeetingPassword.setText(AESEncyption.decrypt(mediaDefinition.getGoogleMeetPassword()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        AlertDialog alertDialog = builder.create();
        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnOpen.setOnClickListener(v -> {
        });

        zoomBinding.ivBtnOpenMeetApp.setOnClickListener(v -> {
            openGoogleMeetUrl(mediaDefinition.getGoogleMeetUrl());
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
            openWebUrl(mediaDefinition.getGoogleMeetUrl());
        });
        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnOpenUrl.setOnClickListener(v -> {
            openWebUrl(mediaDefinition.getGoogleMeetUrl());
        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Meeting Url", mediaDefinition.getGoogleMeetUrl());
        });
        zoomBinding.ivBtnCopyMeetingPhoneNumber.setOnClickListener(v -> {
            myApp.copyText("Meeting Phone Number", mediaDefinition.getGoogleMeetPhoneNumber());
        });

        zoomBinding.ivBtnCopyMeetingPassword.setOnClickListener(v -> {
            try {
                myApp.copyText("Meeting Password", AESEncyption.decrypt(mediaDefinition.getGoogleMeetPassword()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        zoomBinding.ivBtnDialMeetingPhoneNumber.setOnClickListener(v -> {
            /*if (mediaDefinition.getZoomPhone().contains(",")) {
                String[] phone = mediaDefinition.getZoomPhone().split(",");
                if (phone.length > 0 && phone[0] != null) {
                    dialNumber("tel:" + phone[0]);
                }
            } else {
                dialNumber("tel:" + mediaDefinition.getZoomPhone());
            }*/
        });

        alertDialog.show();
    }

    private void showMediaInfoForFacebook(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoFacebookBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_facebook, null, false);
        builder.setView(zoomBinding.getRoot());

        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getFacebookServerUrl())) {
            zoomBinding.llStreamUrl.setVisibility(View.VISIBLE);
            zoomBinding.tvStreamUrl.setText(mediaDefinition.getFacebookServerUrl());
        }

        if (!TextUtils.isEmpty(mediaDefinition.getFacebookStreamKey())) {
            zoomBinding.llStreamKey.setVisibility(View.VISIBLE);
            try {
                zoomBinding.tvStreamKey.setText(AESEncyption.decrypt(mediaDefinition.getFacebookStreamKey()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        AlertDialog alertDialog = builder.create();
        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnOpen.setOnClickListener(v -> {
        });

        zoomBinding.ivBtnOpenFaceBookApp.setOnClickListener(v -> {
            openFaceBookUrl(mediaDefinition.getFacebookServerUrl());
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
            openWebUrl(mediaDefinition.getFacebookServerUrl());
        });
        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnOpenStreamUrl.setOnClickListener(v -> {
            openWebUrl(mediaDefinition.getFacebookServerUrl());
        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Streaming Url", mediaDefinition.getFacebookServerUrl());
        });
        zoomBinding.ivBtnCopyStreamKey.setOnClickListener(v -> {
            try {
                myApp.copyText("Streaming Url", AESEncyption.decrypt(mediaDefinition.getFacebookStreamKey()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        alertDialog.show();
    }

    private void showMediaInfoForYoutube(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoYoutubeBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_youtube, null, false);
        builder.setView(zoomBinding.getRoot());
        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getYoutubeStreamUrl())) {
            zoomBinding.llStreamingUrl.setVisibility(View.VISIBLE);
            zoomBinding.tvSpecialUrl.setText(mediaDefinition.getYoutubeStreamUrl());
            zoomBinding.tvUrlTitle.setText(mediaDefinition.getYoutubeStreamTitle());
        }

        AlertDialog alertDialog = builder.create();

        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnManage.setOnClickListener(view -> {
            alertDialog.dismiss();
            startActivity(new Intent(getActivity(), GYoutubeMainActivity.class));
        });
        zoomBinding.btnOpen.setOnClickListener(v -> {
//            Intent videoIntent = new Intent(getActivity(), YoutubeVideoActivity.class);
//            videoIntent.putExtra("videoURL", mediaDefinition.getYoutubeStreamUrl());
//            startActivity(videoIntent);
            String id, url = mediaDefinition.getYoutubeStreamUrl();
            if (url.contains("=")) {
                String ary[] = url.split("=");
                id = ary[ary.length - 1];
            } else {
                String ary[] = url.split("/");
                id = ary[ary.length - 1];
            }
            Log.d("YouTube", "id: " + id);
            Intent intent = YouTubeIntents.createPlayVideoIntentWithOptions(getActivity(), id, true, false);
            startActivity(intent);
        });
        zoomBinding.ivBtnOpenYouTubeApp.setOnClickListener(v -> {
            openYoutubeUrl(mediaDefinition.getYoutubeStreamUrl());
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
            openWebUrl(mediaDefinition.getYoutubeStreamUrl());
        });
        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Url ", mediaDefinition.getYoutubeStreamUrl());
        });
        zoomBinding.ivBtnCopyUrlTitle.setOnClickListener(v -> {
            myApp.copyText("Title ", mediaDefinition.getYoutubeStreamTitle());
        });

        zoomBinding.btnManage.setOnClickListener(v -> {
            if (mediaDefinition.getMedia_platform().equals("YouTube")) {
                startActivity(new Intent(getActivity(), UTubeMainActivity.class));
            }
        });

        alertDialog.show();
    }

    private void showMediaInfoForZoom(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoZoomBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_zoom, null, false);
        builder.setView(zoomBinding.getRoot());

        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getZoomSpecialUrl())) {
            zoomBinding.llSpecialUrl.setVisibility(View.VISIBLE);
            zoomBinding.tvSpecialUrl.setText(mediaDefinition.getZoomSpecialUrl());
        }
        if (!TextUtils.isEmpty(mediaDefinition.getZoomMeetingId())) {
            zoomBinding.llMeetingId.setVisibility(View.VISIBLE);
            zoomBinding.tvMeetingId.setText(mediaDefinition.getZoomMeetingId());
        }
        if (!TextUtils.isEmpty(mediaDefinition.getZoomPassword())) {
            zoomBinding.llMeetingPassword.setVisibility(View.VISIBLE);
            try {
                zoomBinding.tvMeetingPassword.setText(AESEncyption.decrypt(mediaDefinition.getZoomPassword()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(mediaDefinition.getZoomPhone())) {
            zoomBinding.llPhone.setVisibility(View.VISIBLE);
            zoomBinding.tvPhone.setText(mediaDefinition.getZoomPhone());
        }

        AlertDialog alertDialog = builder.create();
        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnOpen.setOnClickListener(v -> {
        });
        zoomBinding.ivBtnOpenZoomApp.setOnClickListener(v -> {
            openZoomUrl(mediaDefinition.getZoomSpecialUrl());
        });
        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
            openWebUrl(mediaDefinition.getZoomSpecialUrl());
        });
        zoomBinding.ivBtnOpenSpecialUrl.setOnClickListener(v -> {
            openWebUrl(mediaDefinition.getZoomSpecialUrl());
        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Stream Url", mediaDefinition.getZoomSpecialUrl());
        });
        zoomBinding.ivBtnCopyMeetingId.setOnClickListener(v -> {
            myApp.copyText("Meeting ID", mediaDefinition.getZoomMeetingId());
        });
        zoomBinding.ivBtnCopyMeetingPassword.setOnClickListener(v -> {
            try {
                myApp.copyText("Meeting Password", AESEncyption.decrypt(mediaDefinition.getZoomPassword()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        zoomBinding.ivBtnCopyPhone.setOnClickListener(v -> {
            myApp.copyText("Meeting Phone", mediaDefinition.getZoomPhone());
        });
        zoomBinding.btnManage.setOnClickListener(v -> {
            if (mediaDefinition.getMedia_platform().equals("Zoom")) {
                startActivity(new Intent(getActivity(), ZoomLoginActivity.class)
                        .putExtra("isManage", false)
                        .putExtra("isCreateMeeting", true)
                        .putExtra("mediaDefinition", new Gson().toJson(mediaDefinition)));
            }
        });
        zoomBinding.ivBtnDialPhone.setOnClickListener(v -> {
            if (mediaDefinition.getZoomPhone().contains(",")) {
                String[] phone = mediaDefinition.getZoomPhone().split(",");
                if (phone.length > 0 && phone[0] != null) {
                    dialNumber("tel:" + phone[0]);
                }
            } else {
                dialNumber("tel:" + mediaDefinition.getZoomPhone());
            }
        });
        alertDialog.show();


    }

    private void dialNumber(String num) {
        Uri number = Uri.parse(num);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
        startActivity(callIntent);
    }

    private void openWebUrl(String url) {
        if (url.contains("www") || url.contains("http") || url.contains("https")) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + url.trim()));
                startActivity(browserIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void openYoutubeUrl(String url) {
        String id;
        if (url.contains("=")) {
            String ary[] = url.split("=");
            id = ary[ary.length - 1];
        } else {
            String ary[] = url.split("/");
            id = ary[ary.length - 1];
        }
        Log.d("YouTube", "id: " + id);
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
        Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + id));
        appIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            getActivity().startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            getActivity().startActivity(webIntent);
        }
    }

    private void openFaceBookUrl(String facebookStreamUrl) {
        String urlFb = facebookStreamUrl;
        String FACEBOOK_PAGE_ID = "appetizerandroid";
        Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
        PackageManager packageManager = getContext().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            Log.d("AppVersion", "FB: " + versionCode);
            if (versionCode >= 3002850) {
                urlFb = "fb://facewebmodal/f?href=" + facebookStreamUrl;
            } else {                    //older versions of fb app
                urlFb = "fb://page/" + FACEBOOK_PAGE_ID;
            }
            facebookIntent.setData(Uri.parse(urlFb));
            startActivity(facebookIntent);
        } catch (PackageManager.NameNotFoundException e) {
            openAppInGooglePlay("com.facebook.katana");
        }
    }

    private void openSkypeUrl(String skypeStreamUrl) {  //https://join.skype.com/XFVtCgaoi464
        Intent skypeIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(skypeStreamUrl));
        skypeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PackageManager packageManager = getContext().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.skype.raider", PackageManager.GET_ACTIVITIES).versionCode;
            Log.d("AppVersion", "Zoom: " + versionCode);
            // Restrict the Intent to being handled by the Skype for Android client only.
            skypeIntent.setComponent(new ComponentName("com.skype.raider", "com.skype.raider.Main"));
            startActivity(skypeIntent);
        } catch (PackageManager.NameNotFoundException e) {
            openAppInGooglePlay("com.skype.raider");
        }
    }

    private void openZoomUrl(String zoomStreamUrl) {
        Intent zoomIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("zoomus://" + zoomStreamUrl));
        zoomIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PackageManager packageManager = getContext().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("us.zoom.videomeetings", 0).versionCode;
            startActivity(zoomIntent);
        } catch (PackageManager.NameNotFoundException e) {
            openAppInGooglePlay("us.zoom.videomeetings");
        }
    }

    private void openGoogleClassroomUrl(String googleClassroomStreamUrl) {
        Intent googleClassroomIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(googleClassroomStreamUrl));
        PackageManager packageManager = getContext().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.google.android.apps.classroom", 0).versionCode;
            startActivity(googleClassroomIntent);
        } catch (PackageManager.NameNotFoundException e) {
            openAppInGooglePlay("com.google.android.apps.classroom");
        }
    }

    private void openGoogleMeetUrl(String googleMeetStreamUrl) {
        Intent googleMeetIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(googleMeetStreamUrl));
        PackageManager packageManager = getContext().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.google.android.apps.meetings", 0).versionCode;
            startActivity(googleMeetIntent);
        } catch (PackageManager.NameNotFoundException e) {
            openAppInGooglePlay("com.google.android.apps.meetings");
        }
    }

    public void openAppInGooglePlay(String packageName) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
        } catch (android.content.ActivityNotFoundException e) { // if there is no Google Play on device
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
        }
    }

    private class TeleNumberAdapter extends RecyclerView.Adapter<TeleNumberAdapter.NumberHolder> {

        @NonNull
        @Override
        public TeleNumberAdapter.NumberHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new NumberHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_tele_number_view, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull TeleNumberAdapter.NumberHolder holder, int position) {
            if (!TextUtils.isEmpty(teleNumbers.get(position))) {
                holder.tvPhone.setText(String.format("+%s%s", teleCountryCode, teleNumbers.get(position)));
            }
        }

        @Override
        public int getItemCount() {
            return teleNumbers.size();
        }

        public class NumberHolder extends RecyclerView.ViewHolder {
            TextView tvPhone;
            ImageView ivBtnDialPhone, ivBtnCopyPhone;

            public NumberHolder(@NonNull View itemView) {
                super(itemView);
                tvPhone = itemView.findViewById(R.id.tvPhone);
                ivBtnDialPhone = itemView.findViewById(R.id.ivBtnDialPhone);
                ivBtnCopyPhone = itemView.findViewById(R.id.ivBtnCopyPhone);

                ivBtnCopyPhone.setOnClickListener(view -> {
                    myApp.copyText("Conference Number", "+" + teleCountryCode + teleNumbers.get(getAdapterPosition()));
                });

                ivBtnDialPhone.setOnClickListener(view -> {
                    dialNumber("tel:" + "+" + teleCountryCode + teleNumbers.get(getAdapterPosition()));
                });
            }
        }
    }
}
