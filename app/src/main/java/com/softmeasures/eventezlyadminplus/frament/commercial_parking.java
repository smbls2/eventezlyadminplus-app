package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.panstreetview;
import com.softmeasures.eventezlyadminplus.java.BounceAnimation;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.softmeasures.eventezlyadminplus.activity.vchome.adrreslocation;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class commercial_parking extends Fragment {

    TextView txttitle, txttoday, txtmax, txtstreetview, txtdrections, txtaddress, txttime, txtmanaged, txt_main_title, txt_park_here, txt_no_parking, txt_parkin_time, txt_custom_notices, txt_status;
    TextView txt_off_peak_discount, txt_off_peak_start_hour, txt_off_peak_end_hour, txt_weekend_special_diff, txt_notices_lable;
    Double latitude = 0.0, longitude = 0.0;
    String Location_code = "", lat1, lang1, address, title, click_on_commercial;
    ArrayList<item> googlepartnerarray = new ArrayList<>();
    ArrayList<item> commercial_parking_rules = new ArrayList<>();
    ArrayList<item> parkingarray = new ArrayList<>();
    ArrayList<item> parking_rules_array = new ArrayList<>();
    String all_start_hour, all_duartion_unit, all_weekend_discount, all_off_peak_discount;
    String all_this_day, all_active = "null", all_pricing, all_pricing_duration, all_max_hours, all_time_rules, rate, time_rules, active, max_hours, parking_time, no_parking_time, notices, status, in_effect, lots_aval, off_peak_disscount, off_start_hour, off_end_hour, week_end_special;
    String all_end_hour, location_id;
    RelativeLayout rl_progressbar, rl_main;
    RelativeLayout rl_info, rl_no_parking_time, rl_parking_time;
    LinearLayout ll_extra_info;
    private Handler mHandler = null;
    private Runnable mAnimation;

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.commercial_parking, container, false);
        mHandler = new Handler();
        currentlocation();
        txttitle = (TextView) layout.findViewById(R.id.txtlable1);
        rl_info = (RelativeLayout) layout.findViewById(R.id.rl_info);
        rl_no_parking_time = (RelativeLayout) layout.findViewById(R.id.rl_private_no_parking);
        rl_parking_time = (RelativeLayout) layout.findViewById(R.id.rl_pivate_parking_time);
        ll_extra_info = (LinearLayout) layout.findViewById(R.id.ll_extra_info);

        txt_status = (TextView) layout.findViewById(R.id.txt_open_close_full);
        txtaddress = (TextView) layout.findViewById(R.id.txt_address);
        txttoday = (TextView) layout.findViewById(R.id.txtpouptoday);
        txtmax = (TextView) layout.findViewById(R.id.txtpopupax);
        txtstreetview = (TextView) layout.findViewById(R.id.txt_street_view);
        txtdrections = (TextView) layout.findViewById(R.id.txt_direction);
        txttime = (TextView) layout.findViewById(R.id.txtpopuptime1);
        txtmanaged = (TextView) layout.findViewById(R.id.txtpopupmanaged);
        txt_main_title = (TextView) layout.findViewById(R.id.txt_main_title);
        txt_park_here = (TextView) layout.findViewById(R.id.txt_park_here);
        txt_no_parking = (TextView) layout.findViewById(R.id.txt_private_no_parking_time);
        txt_parkin_time = (TextView) layout.findViewById(R.id.txt_private_parking_time);
        txt_custom_notices = (TextView) layout.findViewById(R.id.txt_custom_notices);
        txt_off_peak_discount = (TextView) layout.findViewById(R.id.txt_off_peak_discount);
        txt_off_peak_start_hour = (TextView) layout.findViewById(R.id.txt_off_peak_start_hour);
        txt_off_peak_end_hour = (TextView) layout.findViewById(R.id.txt_off_peak_end_hour);
        txt_weekend_special_diff = (TextView) layout.findViewById(R.id.txt_wekend_special_diff);
        txt_notices_lable = (TextView) layout.findViewById(R.id.text_notices);

        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Light.otf");
        //txtaddress.setTypeface(type);
        txttime.setTypeface(type);
        txtmax.setTypeface(type);
        txttoday.setTypeface(type);
        txt_status.setTypeface(type);
        txt_custom_notices.setTypeface(type);
        txt_off_peak_start_hour.setTypeface(type);
        txt_off_peak_end_hour.setTypeface(type);
        txt_off_peak_discount.setTypeface(type);
        txt_weekend_special_diff.setTypeface(type);
        txt_no_parking.setTypeface(type);
        txt_parkin_time.setTypeface(type);
        rl_progressbar = (RelativeLayout) layout.findViewById(R.id.rl_progressbar);
        rl_main = (RelativeLayout) layout.findViewById(R.id.rl_main);
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        SharedPreferences commer = getActivity().getSharedPreferences("commercial_parking", Context.MODE_PRIVATE);
        Location_code = commer.getString("location_code", "null");
        title = commer.getString("name", "null");
        lat1 = commer.getString("lat", "0.0");
        lang1 = commer.getString("lng", "0.0");
        address = commer.getString("address", "null");
        location_id = commer.getString("location_id", "null");
        click_on_commercial = commer.getString("click_on_commercial", "no");
        if (click_on_commercial.equals("no")) {
            new getgooglepartner().execute();
        } else {
            new getall_privatepakring(Double.parseDouble(lat1), Double.parseDouble(lang1)).execute();
        }
        txtdrections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!lat1.equals("0.0") || !lang1.equals("0.0")) {

                    SharedPreferences sh1 = getActivity().getSharedPreferences("directiontomydirection", Context.MODE_PRIVATE);
                    SharedPreferences.Editor ed1 = sh1.edit();
                    ed1.putString("lat", lat1);
                    ed1.putString("lang", lang1);
                    ed1.putString("address", address);
                    ed1.commit();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                   /* ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                    ft.add(R.id.My_Container_1_ID, new f_direction(), "NewFragmentTag");
                    ft.commit();*/

                    f_direction pay = new f_direction();
                    fragmentStack.clear();
                    // parking_first_screen.registerForListener(Vchome.this);
                    ft.add(R.id.My_Container_1_ID, pay, "home");
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();

                } else {
                }
            }
        });

        txtstreetview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), panstreetview.class);
                i.putExtra("lat", lat1);
                i.putExtra("lang", lang1);
                i.putExtra("title", title);
                i.putExtra("address", address);
                getActivity().startActivity(i);

            }
        });

        txt_park_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Confirm Parking");
                alertDialog.setMessage("You selected '" + address + "'. Parking at '" + adrreslocation + "'");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        SharedPreferences s = getActivity().getSharedPreferences("paidparkhere", Context.MODE_PRIVATE);
                        SharedPreferences.Editor ed = s.edit();
                        ed.putString("Time", all_time_rules);
                        ed.putString("Max", all_max_hours);
                        ed.putString("rate", all_pricing);
                        ed.putString("rated", all_pricing_duration);
                        ed.putString("duation_unit", all_duartion_unit);
                        ed.putString("location_code", Location_code);
                        ed.putString("location_name", title);
                        ed.putString("zip_code", "");
                        ed.putString("city", "");
                        ed.putString("lat", lat1);
                        ed.putString("lang", lang1);
                        ed.putString("id", "");
                        ed.putString("location_id", location_id);
                        ed.putString("address", address);
                        ed.putString("title", title);
                        ed.putString("managedloick", "no");
                        ed.putString("weekend_discount", all_weekend_discount);
                        ed.putString("off_peack", all_off_peak_discount);
                        if (click_on_commercial.equals("yes")) {
                            ed.putString("mar", "other parking");
                        } else {
                            ed.putString("mar", "google");
                        }
                        ed.putString("parkhere", "no");
                        ed.commit();

                       /* final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.replace(R.id.My_Container_1_ID, new Google_Paid_Parking(), "NewFragmentTag");
                        ft.commit();*/
                        Google_Paid_Parking pay = new Google_Paid_Parking();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.add(R.id.My_Container_1_ID, pay, "googlePaidParking");
                        fragmentStack.lastElement().onPause();
                        ft.hide(fragmentStack.lastElement());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();

                    }
                });
                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });
        return layout;
    }

    private String getlatandlangtoaddress(Double lat, Double lon) {
        Geocoder gc = new Geocoder(getActivity(), Locale.getDefault());
        String addresstodirectioncar = "";
        try {
            List<Address> addresses = gc.getFromLocation(lat, lon, 1);
            StringBuilder sb = new StringBuilder();
            StringBuilder addressl = new StringBuilder();
            StringBuilder country = new StringBuilder();
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getLatitude()).append("\n");
                    sb.append(address.getLongitude());
                    addressl.append(address.getAddressLine(0)).append(", ");
                    addressl.append(address.getAddressLine(1));
                    country.append(address.getAddressLine(2));
                    break;
                }
                String loc = addressl.toString();
                String countyname = country.toString();

                if (countyname.equals("null")) {
                    addresstodirectioncar = loc;
                } else {
                    addresstodirectioncar = loc + " , " + countyname;
                }
                Resources rs;
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Address not found!");
                alertDialog.setMessage("Could not find location. Try including the exact location information");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alertDialog.show();
            }
        } catch (IOException e) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Address not found!");
            alertDialog.setMessage("Could not find location. Try including the exact location information");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();
            e.printStackTrace();
        }
        return addresstodirectioncar;
    }

    public void currentlocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Failed to fetch user location!");
            alertDialog.setMessage("Please enable user location for app, location is required for app to work properly");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });


            alertDialog.show();
        }
    }

    public void showmap() {
        final LatLng position = new LatLng(Double.parseDouble(lat1), Double.parseDouble(lang1));

        View marker = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
        TextView numTxt = (TextView) marker.findViewById(R.id.text);
        ImageView img = (ImageView) marker.findViewById(R.id.img_other_pin);
        MarkerOptions options = new MarkerOptions();
        options.position(position);
        options.title(title);
        options.snippet(address);
        if (click_on_commercial.equals("no")) {

            if (rl_info.getVisibility() == View.VISIBLE) {
                img.setBackgroundResource(R.mipmap.partner_pin_empt);
                numTxt.setVisibility(View.VISIBLE);
                numTxt.setText("$" + rate);
                options.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getContext(), marker)));
            } else {
                img.setBackgroundResource(R.mipmap.commercial_icon);
                options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.commercial_icon));
            }
        } else {
            options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.parkwhiz_pin_icon));
        }
        SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        fm.getMapAsync(googleMap1 -> {
            final GoogleMap googleMap = googleMap1;
            Marker melbourne = googleMap.addMarker(options);
            final long start = SystemClock.uptimeMillis();
            final long duration = 1500L;
            // Cancels the previous animation
            mHandler.removeCallbacks(mAnimation);
            mAnimation = new BounceAnimation(start, duration, melbourne, mHandler);
            mHandler.post(mAnimation);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 18));
            googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                @Override
                public View getInfoWindow(Marker arg0) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker arg0) {
                    View v = getActivity().getLayoutInflater().inflate(R.layout.otherparkingpin, null);
                    TextView txttitle = (TextView) v.findViewById(R.id.txtvalues);
                    TextView txtcontaint = (TextView) v.findViewById(R.id.txttotle);
                    txtcontaint.setText(arg0.getSnippet());
                    txttitle.setText(arg0.getTitle());
                    return v;
                }
            });
        });
    }

    public class getgooglepartner extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String parkingurl = "_table/google_parking_partners";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            googlepartnerarray.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setPlace_id(c.getString("place_id"));
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setTitle(c.getString("title"));
                        item1.setAddress(c.getString("address"));
                        item1.setEffect(c.getString("in_effect"));
                        item1.setActive(c.getString("active"));
                        item1.setRenew(c.getString("renewable"));
                        item1.setParking_times(c.getString("parking_times"));
                        item1.setNo_parking_times(c.getString("no_parking_times"));
                        item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        item1.setWeek_ebd_discount(c.getString("weekend_special_diff"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setTotal_lots(c.getString("lots_total"));
                        item1.setLots_aval(c.getString("lots_avbl"));
                        item1.setOff_peak_start(c.getString("off_peak_starts"));
                        item1.setOff_peak_end(c.getString("off_peak_ends"));
                        item1.setId(c.getString("id"));
                        googlepartnerarray.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs;
            if (getActivity() != null && json != null) {
                new getall_commercialrulesparking().execute();
                Log.e("googlepartnerarray", String.valueOf(googlepartnerarray.size()));
            }
            super.onPostExecute(s);
        }
    }

    public class getall_commercialrulesparking extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        String parkingurl = "_table/google_parking_rules";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            commercial_parking_rules.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setLoationname("");
                        item1.setTime_rule(c.getString("time_rule"));
                        item1.setPricing_duration(c.getString("pricing_duration"));
                        item1.setDuration_unit(c.getString("duration_unit"));
                        item1.setMax_time(c.getString("max_duration"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        String time_rules = c.getString("time_rule");
                        item1.setPricing(c.getString("pricing"));
                        item1.setId(c.getString("id"));
                        item1.setEffect(c.getString("in_effect"));
                        item1.setThis_day(c.getString("day_type"));
                        item1.setStart_time(c.getString("start_time"));
                        item1.setEnd_time(c.getString("end_time"));
                        item1.setLocation_code(c.getString("location_code"));

                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item1.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }

                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item1.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }

                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item1.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }

                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item1.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }

                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item1.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }

                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item1.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item1.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }

                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item1.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }

                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item1.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }

                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item1.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }

                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item1.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }
                        commercial_parking_rules.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (click_on_commercial.equals("no")) {
                    if (googlepartnerarray.size() > 0) {
                        for (int k = 0; k < googlepartnerarray.size(); k++) {

                            for (int j = 0; j < commercial_parking_rules.size(); j++) {
                                String loc_code = commercial_parking_rules.get(j).getLocation_code();
                                if (Location_code.equals(loc_code)) {
                                    Log.e("#DEBUG", "   commercial_parking:  locationCode:  " + loc_code);
                                    Log.e("#DEBUG", "   commercial_parking:  parkingRule:  "
                                            + new Gson().toJson(commercial_parking_rules.get(j)));
                                    all_start_hour = commercial_parking_rules.get(j).getStart_hour();
                                    all_end_hour = commercial_parking_rules.get(j).getEnd_hour();
                                    all_this_day = commercial_parking_rules.get(j).getThis_day();
                                    all_active = commercial_parking_rules.get(j).getActive();
                                    all_pricing = commercial_parking_rules.get(j).getPricing();
                                    all_pricing_duration = commercial_parking_rules.get(j).getPricing_duration();
                                    all_time_rules = commercial_parking_rules.get(j).getTime_rule();
                                    all_max_hours = commercial_parking_rules.get(j).getMax_time();
                                    all_duartion_unit = commercial_parking_rules.get(j).getDuration_unit();
                                    all_weekend_discount = commercial_parking_rules.get(j).getWeek_ebd_discount();
                                    all_off_peak_discount = commercial_parking_rules.get(j).getOff_peak_discount();
                                    parking_time = googlepartnerarray.get(k).getParking_times();
                                    no_parking_time = googlepartnerarray.get(k).getNo_parking_times();
                                    in_effect = googlepartnerarray.get(k).getEffect();
                                    lots_aval = googlepartnerarray.get(k).getLots_aval();
                                    time_rules = commercial_parking_rules.get(j).getTime_rule();
                                    off_peak_disscount = googlepartnerarray.get(k).getOff_peak_discount();
                                    off_start_hour = googlepartnerarray.get(k).getOff_peak_start();
                                    off_end_hour = googlepartnerarray.get(k).getOff_peak_end();
                                    week_end_special = googlepartnerarray.get(k).getWeek_ebd_discount();
                                    active = googlepartnerarray.get(k).getEffect();
                                    max_hours = commercial_parking_rules.get(j).getMax_time();
                                    notices = googlepartnerarray.get(k).getCustom_notice();
                                    String notices1 = notices != null ? (!notices.equals("") ? (!notices.equals("null") ? notices : "") : "") : "";
                                    txt_custom_notices.setText(notices1);
                                    rate = commercial_parking_rules.get(j).getPricing();
                                    String time_rules1 = time_rules != null ? (!time_rules.equals("") ? time_rules : "") : "";
                                    txttime.setText(time_rules1);
                                    in_effect = in_effect != null ? (in_effect.equals("1") ? "true" : "false") : "false";
                                    lots_aval = lots_aval != null ? (!lots_aval.equals("") ? (!lots_aval.equals("0") ? "o" : "f") : "c") : "c";
                                    if (in_effect.equals("true")) {
                                        txttoday.setBackgroundColor(Color.parseColor("#22B14C"));
                                        txttoday.setText("ALLOWED");
                                    } else {
                                        txttoday.setBackgroundColor(Color.parseColor("#FF7F27"));
                                        txttoday.setText("NO PARKING");
                                    }
                                    if (in_effect.equals("false")) {
                                        txt_status.setText("CLOSED");
                                        txt_status.setBackgroundColor(Color.parseColor("#D33632"));
                                    } else if (lots_aval.equals("c")) {
                                        txt_status.setText("CLOSED");
                                        txt_status.setBackgroundColor(Color.parseColor("#D33632"));
                                    } else if (lots_aval.equals("o")) {
                                        txt_status.setText("OPEN");
                                        txt_status.setBackgroundColor(Color.parseColor("#22B14C"));
                                    } else if (lots_aval.equals("f")) {
                                        txt_status.setText("Full");
                                        txt_status.setBackgroundColor(Color.parseColor("#FF7F27"));
                                    }


                                    String max_hours1 = max_hours != null ? (!max_hours.equals("") ? (!max_hours.equals("null") ? max_hours : "") : "") : "";
                                    String all_max_hours1 = all_max_hours != null ? (!all_max_hours.equals("") ? all_max_hours : "f") : "f";
                                    if (!all_max_hours1.equals("f")) {
                                        String duration_p = "";
                                        double bb = Double.parseDouble(all_max_hours1);
                                        if (bb > 1) {
                                            if (bb == 1) {
                                                duration_p = all_duartion_unit;
                                            } else {
                                                duration_p = all_duartion_unit + "s";
                                            }
                                        } else {
                                            duration_p = all_duartion_unit;
                                        }
                                        txtmax.setText(max_hours1 + " " + duration_p);
                                        String all_pricing1 = all_pricing != null ? (!all_pricing.equals("0") ? (!all_pricing.equals("null") ? "$ " + all_pricing + " / " + all_pricing_duration + " " + duration_p : "FREE") : "FREE") : "FREE";
                                        txttitle.setText(all_pricing1);

                                    }

                                    String finaltime = parking_time != null ? (!parking_time.equals("") ? (!parking_time.equals("null") ? parking_time : "") : "") : "";
                                    String finalnoparking_time = no_parking_time != null ? (!no_parking_time.equals("") ? (!no_parking_time.equals("null") ? no_parking_time : "") : "") : "";
                                    txt_no_parking.setText(finalnoparking_time);
                                    txt_parkin_time.setText(finaltime);

                                    String off_peak_disscount1 = off_peak_disscount != null ? (!off_peak_disscount.equals("") ? (!off_peak_disscount.equals("null") ? off_peak_disscount + "%" : "") : "") : "";
                                    txt_off_peak_discount.setText(off_peak_disscount1);

                                    String off_start_hour1 = off_start_hour != null ? (!off_start_hour.equals("") ? (!off_start_hour.equals("null") ? off_start_hour : "") : "") : "";
                                    txt_off_peak_start_hour.setText(off_start_hour1);

                                    String off_end_hour1 = off_end_hour != null ? (!off_end_hour.equals("") ? (!off_end_hour.equals("null") ? off_end_hour : "") : "") : "";
                                    txt_off_peak_end_hour.setText(off_end_hour1);

                                    String week_end_special1 = week_end_special != null ? (!week_end_special.equals("") ? (!week_end_special.equals("null") ? week_end_special + "%" : "") : "") : "";
                                    txt_weekend_special_diff.setText(week_end_special1);

                                    txt_park_here.setVisibility(View.VISIBLE);
                                    rl_info.setVisibility(View.VISIBLE);
                                    ll_extra_info.setVisibility(View.VISIBLE);
                                    txt_notices_lable.setVisibility(View.VISIBLE);
                                    rl_parking_time.setVisibility(View.VISIBLE);
                                    rl_no_parking_time.setVisibility(View.VISIBLE);
                                    txt_custom_notices.setVisibility(View.VISIBLE);
                                    break;
                                }
                            }
                            break;
                        }

                    }
                }
                txt_main_title.setText(title);
                txtaddress.setText(address);
                showmap();
            }
            super.onPostExecute(s);
        }
    }

    public class getall_privatepakring extends AsyncTask<String, String, String> {

        double lat = 0.0, lang = 0.0;

        public getall_privatepakring(double lat, double lang) {
            this.lat = lat;
            this.lang = lang;
        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            parkingarray.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + "_proc/find_other_parking_partners_nearby?in_lat=" + lat + "&in_lng=" + lang + "";
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            HttpResponse response = null;

            try {
                response = client.execute(post);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                try {
                    HttpEntity resEntity = response.getEntity();
                    String responseStr = null;


                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONArray array = new JSONArray(responseStr);

                    for (int i = 0; i < array.length(); i++) {
                        item item1 = new item();
                        JSONObject c = array.getJSONObject(i);
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setTitle(c.getString("title"));
                        item1.setAddress(c.getString("address"));
                        item1.setLat(c.getString("lat"));
                        item1.setLang(c.getString("lng"));
                        item1.setEffect(c.getString("in_effect"));
                        item1.setActive(c.getString("active"));
                        item1.setRenew(c.getString("renewable"));
                        item1.setParking_times(c.getString("parking_times"));
                        item1.setNo_parking_times(c.getString("no_parking_times"));
                        item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        item1.setWeek_ebd_discount(c.getString("weekend_special_diff"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setTotal_lots(c.getString("lots_total"));
                        item1.setLots_aval(c.getString("lots_avbl"));
                        item1.setOff_peak_start(c.getString("off_peak_starts"));
                        item1.setOff_peak_end(c.getString("off_peak_ends"));
                        parkingarray.add(item1);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {


            Resources rs;

            if (getActivity() != null && parkingarray.size() >= 0) {

                new getprivate_parking_rules().execute();
            } else {
                rl_progressbar.setVisibility(View.GONE);
            }
            super.onPostExecute(s);

        }
    }

    public class getprivate_parking_rules extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        String parkingurl = "_table/other_parking_rules";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            parking_rules_array.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        if (title.equals("") || title.equals("unknown location")) {
                            title = "unknown location";
                            item1.setLoationname(title);
                        } else {
                            item1.setLoationname("");
                        }
                        item1.setTime_rule(c.getString("time_rule"));
                        item1.setPricing_duration(c.getString("pricing_duration"));
                        item1.setDuration_unit(c.getString("duration_unit"));
                        item1.setMax_time(c.getString("max_duration"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setThis_day(c.getString("day_type"));
                        item1.setPricing(c.getString("pricing"));
                        item1.setId(c.getString("id"));
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setStart_time(c.getString("start_time"));
                        item1.setEnd_time(c.getString("end_time"));
                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item1.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }

                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item1.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }

                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item1.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }

                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item1.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }

                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item1.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }

                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item1.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item1.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }

                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item1.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }

                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item1.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }

                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item1.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }

                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item1.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }
                        parking_rules_array.add(item1);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            super.onPostExecute(s);
            if (getActivity() != null && json != null) {
                for (int k = 0; k < parkingarray.size(); k++) {
                    for (int h = 0; h < parking_rules_array.size(); h++) {
                        String lococde = parking_rules_array.get(h).getLocation_code();
                        if (lococde.equals(Location_code)) {

                            all_start_hour = parking_rules_array.get(h).getStart_hour();
                            all_end_hour = parking_rules_array.get(h).getEnd_hour();
                            all_this_day = parking_rules_array.get(h).getThis_day();
                            all_active = parking_rules_array.get(h).getActive();
                            all_pricing = parking_rules_array.get(h).getPricing();
                            all_pricing_duration = parking_rules_array.get(h).getPricing_duration();
                            all_time_rules = parking_rules_array.get(h).getTime_rule();
                            all_max_hours = parking_rules_array.get(h).getMax_time();
                            all_duartion_unit = parking_rules_array.get(h).getDuration_unit();
                            all_weekend_discount = parkingarray.get(k).getWeek_ebd_discount();
                            all_off_peak_discount = parkingarray.get(k).getOff_peak_discount();
                            time_rules = parking_rules_array.get(h).getTime_rule();
                            notices = parkingarray.get(k).getCustom_notice();
                            notices = notices != null ? (!notices.equals("") ? (!notices.equals("null") ? notices : "") : "") : "";
                            txt_custom_notices.setText(notices);
                            in_effect = parkingarray.get(k).getEffect();
                            lots_aval = parkingarray.get(k).getLots_aval();
                            parking_time = parkingarray.get(k).getParking_times();
                            no_parking_time = parkingarray.get(k).getNo_parking_times();
                            off_peak_disscount = parkingarray.get(k).getOff_peak_discount();
                            off_start_hour = parkingarray.get(k).getOff_peak_start();
                            off_end_hour = parkingarray.get(k).getOff_peak_end();
                            week_end_special = parkingarray.get(k).getWeek_ebd_discount();
                            rate = parking_rules_array.get(h).getPricing();
                            String all_max_hours1 = all_max_hours != null ? (!all_max_hours.equals("") ? all_max_hours : "f") : "f";
                            if (!all_max_hours1.equals("f")) {
                                String duration_p = "";
                                double bb = Double.parseDouble(all_max_hours1);
                                if (bb > 1) {
                                    if (bb == 1) {
                                        duration_p = all_duartion_unit;
                                    } else {
                                        duration_p = all_duartion_unit + "s";
                                    }
                                } else {
                                    duration_p = all_duartion_unit;
                                }
                                max_hours = all_max_hours + " " + duration_p;
                                String all_pricing1 = all_pricing != null ? (!all_pricing.equals("0") ? (!all_pricing.equals("null") ? "$ " + all_pricing + " / " + all_pricing_duration + " " + duration_p : "FREE") : "FREE") : "FREE";
                                txttitle.setText(all_pricing1);
                            }

                            String time_rules1 = time_rules != null ? (!time_rules.equals("") ? time_rules : "") : "";
                            txttime.setText(time_rules1);
                            in_effect = in_effect != null ? (in_effect.equals("1") ? "true" : "false") : "false";
                            lots_aval = lots_aval != null ? (!lots_aval.equals("") ? (!lots_aval.equals("0") ? "o" : "f") : "c") : "c";
                            if (in_effect.equals("true")) {
                                txttoday.setBackgroundColor(Color.parseColor("#22B14C"));
                                txttoday.setText("ALLOWED");
                            } else {
                                txttoday.setBackgroundColor(Color.parseColor("#FF7F27"));
                                txttoday.setText("NO PARKING");
                            }

                            if (in_effect.equals("false")) {
                                txt_status.setText("CLOSED");
                                txt_status.setBackgroundColor(Color.parseColor("#D33632"));
                            } else if (lots_aval.equals("c")) {
                                txt_status.setText("CLOSED");
                                txt_status.setBackgroundColor(Color.parseColor("#D33632"));
                            } else if (lots_aval.equals("o")) {
                                txt_status.setText("OPEN");
                                txt_status.setBackgroundColor(Color.parseColor("#22B14C"));
                            } else if (lots_aval.equals("f")) {
                                txt_status.setText("Full");
                                txt_status.setBackgroundColor(Color.parseColor("#FF7F27"));
                            }


                            String max_hours1 = max_hours != null ? (!max_hours.equals("") ? (!max_hours.equals("null") ? max_hours : "") : "") : "";
                            txtmax.setText(max_hours1);

                            String finaltime = parking_time != null ? (!parking_time.equals("") ? (!parking_time.equals("null") ? parking_time : "") : "") : "";
                            String finalnoparking_time = no_parking_time != null ? (!no_parking_time.equals("") ? (!no_parking_time.equals("null") ? no_parking_time : "") : "") : "";
                            txt_no_parking.setText(finalnoparking_time);
                            txt_parkin_time.setText(finaltime);

                            String off_peak_disscount1 = off_peak_disscount != null ? (!off_peak_disscount.equals("") ? (!off_peak_disscount.equals("null") ? off_peak_disscount + "%" : "") : "") : "";
                            txt_off_peak_discount.setText(off_peak_disscount1);

                            String off_start_hour1 = off_start_hour != null ? (!off_start_hour.equals("") ? (!off_start_hour.equals("null") ? off_start_hour : "") : "") : "";
                            txt_off_peak_start_hour.setText(off_start_hour1);

                            String off_end_hour1 = off_end_hour != null ? (!off_end_hour.equals("") ? (!off_end_hour.equals("null") ? off_end_hour : "") : "") : "";
                            txt_off_peak_end_hour.setText(off_end_hour1);

                            String week_end_special1 = week_end_special != null ? (!week_end_special.equals("") ? (!week_end_special.equals("null") ? week_end_special + "%" : "") : "") : "";
                            txt_weekend_special_diff.setText(week_end_special1);

                            txt_park_here.setVisibility(View.VISIBLE);
                            rl_info.setVisibility(View.VISIBLE);
                            ll_extra_info.setVisibility(View.VISIBLE);
                            txt_notices_lable.setVisibility(View.VISIBLE);
                            rl_parking_time.setVisibility(View.VISIBLE);
                            rl_no_parking_time.setVisibility(View.VISIBLE);
                            txt_custom_notices.setVisibility(View.VISIBLE);
                            break;
                        }
                    }
                }
                txt_main_title.setText(title);
                txtaddress.setText(address);
                showmap();

            }
        }
    }

}
