package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class Fragment_permit_definitions_One extends Fragment {

    ConnectionDetector cd;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar;
    View mView;
    String id;
    public static String select_town = "", select_Id = "";
    ListView listofvehicleno;

    ArrayList<item> array_parking_permits;
    AdapterParking_Permit adapterParking_permit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_permit_definitions_one, container, false);

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        id = logindeatl.getString("id", "null");

        ints();

        return mView;
    }

    public void ints() {

        listofvehicleno = (ListView) mView.findViewById(R.id.listofvehicle);


        progressBar = (ProgressBar) mView.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) mView.findViewById(R.id.rl_progressbar);
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        onClicks();
    }

    public void onClicks() {

        new getManager_type().execute();

    }

    public class getManager_type extends AsyncTask<String, String, String> {

        String vehiclenourl = "_table/manager_type";
        JSONObject json = null;

        @Override
        protected void onPreExecute() {
            array_parking_permits = new ArrayList<item>();
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("get_vehicle_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;

            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);
                    Log.e("rerpones", String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setId(c.getString("id"));
                        item1.setManager_type(c.getString("manager_type"));
                        array_parking_permits.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity != null) {
                    if (array_parking_permits.size() > 0) {
                        adapterParking_permit = new AdapterParking_Permit(getActivity(), array_parking_permits, getResources());
                        listofvehicleno.setAdapter(adapterParking_permit);
                    } else {
                        //txt_No_Record.setVisibility(View.VISIBLE);
                        //lv_Permit.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

    public class AdapterParking_Permit extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private ArrayList<item> Olddata;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        Fragment_permit_definitions.CustomFilter filter;

        public AdapterParking_Permit(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        public class ViewHolder {
            TextView txt_Name;
            ImageView img_Icon;
            LinearLayout ll_Manager;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_permit_definition, null);
                holder = new ViewHolder();

                holder.ll_Manager = (LinearLayout) vi.findViewById(R.id.ll_Manager);
                holder.img_Icon = (ImageView) vi.findViewById(R.id.img_Icon);
                holder.txt_Name = (TextView) vi.findViewById(R.id.txt_Name);

                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }

            try {
                state = data.get(position);
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.txt_Name.setText(data.get(position).getManager_type());

            if (data.get(position).getManager_type().toString().equalsIgnoreCase("TOWNSHIP")) {
                holder.img_Icon.setImageDrawable(getResources().getDrawable(R.mipmap.township_icon));
            } else if (data.get(position).getManager_type().toString().equalsIgnoreCase("COMMERCIAL")) {
                holder.img_Icon.setImageDrawable(getResources().getDrawable(R.mipmap.commercial_icon));
            } else if (data.get(position).getManager_type().toString().equalsIgnoreCase("PRIVATE")) {
                holder.img_Icon.setImageDrawable(getResources().getDrawable(R.mipmap.local_icon));
            } else {
                holder.img_Icon.setImageDrawable(getResources().getDrawable(R.mipmap.local_icon));
            }

            holder.ll_Manager.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    select_town = data.get(position).getManager_type().toString();
                    select_Id = "" + data.get(position).getId();
                    Log.e("select_Id " + select_Id, " : " + select_town);

                    Bundle bundle = new Bundle();
                    bundle.putString("select_town", select_town);

                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Fragment_permit_definitions_Two pay = new Fragment_permit_definitions_Two();
                    pay.setArguments(bundle);
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                }
            });
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }


}
