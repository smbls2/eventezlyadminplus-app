package com.softmeasures.eventezlyadminplus.frament;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.services.Constants;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created by Significant Info on 1/31/2018.
 */

public class Forgot_Password extends Fragment {

    View rootView;
    ConnectionDetector cd;
    RelativeLayout rl_progressbar, llMain1, rl_cancel, rl_ok, rl_ok1, rl_cancel1, llMain2;
    EditText edit_email, edit_email1, edit_code, edit_new_password, edit_retype;
    String forgot_email = "";
    ClipboardManager myClipboard;
    TextView txt2, txt_Title;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frg_forgot_password, container, false);

        cd = new ConnectionDetector(getActivity());
        myClipboard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("", "");
        myClipboard.setPrimaryClip(clip);
        rl_progressbar = (RelativeLayout) rootView.findViewById(R.id.rl_progressbar1);
        llMain1 = (RelativeLayout) rootView.findViewById(R.id.llMain1);
        txt_Title = (TextView) rootView.findViewById(R.id.txt_Title);

        edit_email = (EditText) rootView.findViewById(R.id.edit_email);
        rl_cancel = (RelativeLayout) rootView.findViewById(R.id.rl_cancel);
        rl_ok = (RelativeLayout) rootView.findViewById(R.id.rl_ok);

        //Next dialog
        llMain2 = (RelativeLayout) rootView.findViewById(R.id.llMain2);
        edit_email1 = (EditText) rootView.findViewById(R.id.edit_email1);
        //edit_email1.setFocusable(false);
        edit_code = (EditText) rootView.findViewById(R.id.edit_code);
        edit_new_password = (EditText) rootView.findViewById(R.id.edit_new_password);
        edit_retype = (EditText) rootView.findViewById(R.id.edit_confrom_password);
        rl_ok1 = (RelativeLayout) rootView.findViewById(R.id.rl_ok1);
        rl_cancel1 = (RelativeLayout) rootView.findViewById(R.id.rl_cancel1);
        txt2 = (TextView) rootView.findViewById(R.id.txt2);

        if (Constants.mForgot.equalsIgnoreCase("Forgot")) {
            llMain1.setVisibility(View.VISIBLE);
            llMain2.setVisibility(View.GONE);
            txt_Title.setText("Forgot Password");
            edit_email1.setFocusable(false);
        } else if (Constants.mForgot.equalsIgnoreCase("Reset")) {
            llMain1.setVisibility(View.GONE);
            llMain2.setVisibility(View.VISIBLE);
            txt_Title.setText("Reset Password");
            edit_email1.setFocusable(true);
        }

        rl_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgot_email = edit_email.getText().toString().trim();
                if (!forgot_email.equals("")) {
                    closeKeyboard();
                    new ForgotPassword(forgot_email).execute();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Alert!!");
                    alertDialog.setMessage("Please fill in all required fields.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                }
            }
        });

        txt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgot_email = edit_email1.getText().toString().trim();
                if (!forgot_email.equals("")) {
                    closeKeyboard();
                    new ForgotPassword(forgot_email).execute();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Alert!!");
                    alertDialog.setMessage("Please Enter Email Id");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                }
            }
        });

        rl_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });


        rl_ok1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = edit_code.getText().toString();
                String new_password = edit_new_password.getText().toString();
                String retype_password = edit_retype.getText().toString();

                if (code.equals("")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Alert!!");
                    alertDialog.setMessage("Please enter code");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                } else if (new_password.equals("")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Alert!!");
                    alertDialog.setMessage("Please enter New password");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                } else if (new_password.length() < 5) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Alert");
                    alertDialog.setMessage("Password must be greater than 4 characters");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                } else if (retype_password.equals("")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Alert!!");
                    alertDialog.setMessage("Please enter confirm password");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                } else if (retype_password.length() < 5) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Alert");
                    alertDialog.setMessage("Password must be greater than 4 characters");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                } else if (!new_password.equals(retype_password)) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Alert!!");
                    alertDialog.setMessage("Password does not match");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                } else {
                    closeKeyboard();
                    new ForgotPassword_final_setp(code, new_password).execute();
                }
            }
        });

        rl_cancel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        return rootView;
    }

    private class ForgotPassword extends AsyncTask<String, String, String> {
        String id;
        String login = "user/password?reset=true";
        String email, message = "", success = "";

        public ForgotPassword(String email) {
            this.email = email;

        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("email", email);

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + login;
            Log.e("login_url", url);
            Log.e("login_param", String.valueOf(json));

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject jso = new JSONObject(responseStr);
                    if (!jso.has("error")) {
                        success = jso.getString("success");
                    } else {
                        JSONObject ds = jso.getJSONObject("error");
                        message = ds.getString("message");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            super.onPostExecute(s);
            Log.e("Response ", "  : " + s);
            if (getActivity() != null && message.equals("")) {
                if (success.equals("true")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("You will receive code in your registered email !");
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            llMain1.setVisibility(View.GONE);
                            llMain2.setVisibility(View.VISIBLE);
                            edit_email1.setText(forgot_email);
                        }
                    });
                    alertDialog.show();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Please try again");
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Error");
                alertDialog.setMessage(message);
                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }

        }
    }

    private class ForgotPassword_final_setp extends AsyncTask<String, String, String> {
        String id;
        String login = "user/password";
        String code, message = "", success = "", password;

        public ForgotPassword_final_setp(String code, String password) {
            this.code = code;
            this.password = password;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("email", forgot_email);
            jsonValues.put("code", code);
            jsonValues.put("new_password", password);

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + login;
            Log.e("login_url", url);
            Log.e("login_param", String.valueOf(json));

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject jso = new JSONObject(responseStr);
                    if (!jso.has("error")) {
                        success = jso.getString("success");
                    } else {
                        JSONObject ds = jso.getJSONObject("error");
                        message = ds.getString("message");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            super.onPostExecute(s);
            if (getActivity() != null && message.equals("")) {
                if (success.equals("true")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("");
                    alertDialog.setMessage("Successfully changed your password");
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            getActivity().onBackPressed();
                        }
                    });
                    alertDialog.show();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Please try again");
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Error");
                alertDialog.setMessage(message);
                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
        if (clipboard.hasPrimaryClip() == true) {
            ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
            edit_code.setText(item.getText().toString());
        }
    }

    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getActivity().getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }
}
