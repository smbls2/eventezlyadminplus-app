package com.softmeasures.eventezlyadminplus.frament.user_profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.frament.Forgot_Password;
import com.softmeasures.eventezlyadminplus.frament.Frg_Change_password;
import com.softmeasures.eventezlyadminplus.services.Constants;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;

import static android.content.Context.MODE_PRIVATE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class Frg_User_profile extends Fragment implements Frg_User_edit_profile.Done_click {

    TextView txt_first_name, txt_last_name, txt_phone, txt_mobile, txt_email, txt_address, txt_question,
            txt_display_name, txt_answer, txt_City, txt_State, txt_Zip, txt_Country;
    RelativeLayout rl_progressbar;
    Button btn_edit, btn_change_password, btn_reset_password;
    String message = "", g_FirstName = "", g_LastName = "", g_Phone = "", g_Mobile = "", g_FullName = "", g_Email = "",
            g_Address = "", g_City = "", g_State = "", g_Zip = "", g_Country = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_user_profile_view, container, false);

        txt_display_name = (TextView) view.findViewById(R.id.txt_display_name);
        txt_first_name = (TextView) view.findViewById(R.id.txt_first_name);
        txt_last_name = (TextView) view.findViewById(R.id.txt_last_name);
        txt_phone = (TextView) view.findViewById(R.id.txt_phone_number);
        txt_mobile = (TextView) view.findViewById(R.id.txt_Mobile_number);
        txt_email = (TextView) view.findViewById(R.id.txt_email_address);
        txt_address = (TextView) view.findViewById(R.id.txt_address);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        btn_change_password = (Button) view.findViewById(R.id.btn_change_password);
        btn_edit = (Button) view.findViewById(R.id.btn_edit);
        btn_reset_password = (Button) view.findViewById(R.id.btn_reset_password);
        txt_City = (TextView) view.findViewById(R.id.txt_City);
        txt_State = (TextView) view.findViewById(R.id.txt_State);
        txt_Zip = (TextView) view.findViewById(R.id.txt_Zip);
        txt_Country = (TextView) view.findViewById(R.id.txt_Country);


        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("g_FirstName", g_FirstName);
                bundle.putString("g_LastName", g_LastName);
                bundle.putString("g_Phone", g_Phone);
                bundle.putString("g_Mobile", g_Mobile);
                bundle.putString("g_Address", g_Address);
                bundle.putString("g_FullName", g_FullName);
                bundle.putString("g_Email", g_Email);
                bundle.putString("g_City", g_City);
                bundle.putString("g_State", g_State);
                bundle.putString("g_Zip", g_Zip);
                bundle.putString("g_Country", g_Country);

                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Frg_User_edit_profile pay = new Frg_User_edit_profile();
                ft.add(R.id.My_Container_1_ID, pay);
                pay.setArguments(bundle);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        btn_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Frg_Change_password pay = new Frg_Change_password();
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        btn_reset_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.mForgot = "Reset";
                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Forgot_Password pay = new Forgot_Password();
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        new get_user_profile().execute();
    }

    public void hideKeyboard(View view) {
        try {
            if (getActivity() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void show_Alert(String mess) {
        if (getActivity() != null) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Error");
        alertDialog.setMessage(mess);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

            alertDialog.show();
        }
    }

    @Override
    public void OnItemClick() {
        new get_user_profile().execute();
    }

    private class get_user_profile1 extends AsyncTask<String, String, String> {
        String login = "user/profile";
        String email_, passwrod_;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String ins_id = logindeatl.getString("id", "null");
        String session_id = logindeatl.getString("session_id", "");
        String first_name, last_name, phone, question, message = "", email, displayName;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + login;
            Log.e("login_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            post.setHeader("x-dreamfactory-session-token", session_id);

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject jso = new JSONObject(responseStr);
                    Log.e("jso : " + ins_id, " : " + jso.toString());
                    if (!jso.has("error")) {
                        first_name = jso.getString("first_name");
                        last_name = jso.getString("last_name");
                        phone = jso.getString("phone");
                        question = jso.getString("security_question");
                        email = jso.getString("email");
                        displayName = jso.getString("name");
                    } else {
                        JSONObject ds = jso.getJSONObject("error");
                        message = ds.getString("message");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            rl_progressbar.setVisibility(View.GONE);
            if (message.equals("")) {
                txt_first_name.setText("");
                txt_last_name.setText("");
                txt_phone.setText("");
                txt_question.setText("");
                txt_email.setText("");
                txt_address.setText("");
                txt_display_name.setText("");

                first_name = first_name != null ? (!first_name.equals("") ? (!first_name.equals("null") ? first_name : "") : "") : "";
                last_name = last_name != null ? (!last_name.equals("") ? (!last_name.equals("null") ? last_name : "") : "") : "";
                phone = phone != null ? (!phone.equals("") ? (!phone.equals("null") ? phone : "") : "") : "";
                question = question != null ? (!question.equals("") ? (!question.equals("null") ? question : "") : "") : "";
                email = email != null ? (!email.equals("") ? (!email.equals("null") ? email : "") : "") : "";
                displayName = displayName != null ? (!displayName.equals("") ? (!displayName.equals("null") ? displayName : "") : "") : "";

                txt_display_name.setText(displayName);
                txt_first_name.setText(first_name);
                txt_last_name.setText(last_name);
                txt_phone.setText(phone);
                txt_question.setText(question);
                txt_email.setText(email);
            } else {
                show_Alert(message);
            }
        }
    }

    private class get_user_profile extends AsyncTask<String, String, String> {
        String login = "_table/user_profile";

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String ins_id = logindeatl.getString("id", "null");
        String session_id = logindeatl.getString("session_id", "");
        String emial_ID = logindeatl.getString("email", "");

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rl_progressbar.setVisibility(View.VISIBLE);
            try {
                login = "_table/user_profile?filter=(id%3D'" + URLEncoder.encode(ins_id, "utf-8") + "')";
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + login;
            Log.e("login_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject jso = new JSONObject(responseStr);
                    Log.e("jso : " + ins_id, " : " + jso.toString());
                    JSONArray jsonArray = jso.getJSONArray("resource");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        g_Email = object.getString("email");
                        g_FirstName = object.getString("fname");
                        g_LastName = object.getString("lname");
                        g_Phone = object.getString("lphone");
                        g_Mobile = object.getString("mobile");
                        g_FullName = object.getString("full_name");
                        g_Address = object.getString("address");
                        g_City = object.getString("city");
                        g_State = object.getString("state");
                        g_Zip = object.getString("zip");
                        g_Country = object.getString("country");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && message.equals("")) {
                txt_display_name.setText(g_FullName);
                txt_first_name.setText(g_FirstName);
                txt_last_name.setText(g_LastName);
                txt_phone.setText(g_Phone);
                txt_mobile.setText(g_Mobile);
                txt_email.setText(g_Email);
                txt_address.setText(g_Address);
                txt_City.setText(g_City);
                txt_State.setText(g_State);
                txt_Zip.setText(g_Zip);
                txt_Country.setText(g_Country);
            } else {
                show_Alert(message);
            }
        }
    }

}