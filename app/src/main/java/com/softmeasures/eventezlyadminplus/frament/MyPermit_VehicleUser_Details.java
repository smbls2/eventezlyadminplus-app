package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.StrictMode;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.Main_Activity;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.frament.user_profile.Frg_User_edit_profile_Permit;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static android.content.Context.MODE_PRIVATE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

/**
 * Created by Significant Info on 3/20/2018.
 */

public class MyPermit_VehicleUser_Details extends Fragment {

    View rootView;
    ConnectionDetector cd;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar;
    Context mContext;
    TextView txt_Print, txt_PERMIT, txt_ISSUED, txt_FEE, txt_ProfileDetails,
            txt_ExpiryTime;
    TextView txt_AddNotes, txt_UNPAID, txt_PENDING,
            txt_EditApplication, txt_CancelApplication, txt_EXIT,
            txt_PayFeeViaCard, txt_PayFeeViaWallet, txt_User_comments, txt_Town_comments,
            txt_User_comments1, txt_Town_comments1;
    ImageView img_PrintLogo, img_SignatureProof;

    String id, date_time, user_id, township_code, township_name, permit_type, permit_name, residency_proof,
            approved, status, paid, scheme_type, rate, user_name, signature, driver_proof,
            First_contact_date, Permit_nextnum, Full_name, Full_address, email, phone, Manager_type_id, Manager_type, renewable, Doc_requirements,
            User_comments, Town_comments, Logo, permit_id, permit_validity, permit_expires_on, expired;
    AmazonS3 s3;
    TransferUtility transferUtility;
    ArrayList<String> arrayListResidency, arrayListDriver;
    RecyclerView rv_horizontal, rv_DriverProof;
    CustomAdapter horizontalAdapter;
    DriverAdapter driverAdapter;
    int s_pos = 0, d_pos = 0;
    ArrayList<String> listResidency, listDriver;
    Double finalpay = 0.0, payball = 0.0, tr_percentage1 = 0.0, tr_fee1 = 0.0;

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    private static final String TAG = "paymentQlighting";
    private static final String CONFIG_CLIENT_ID = "Aft0eWMQqeJfDnHj6pmSY_8Ta5kARfjF-BEfAFFzIenRq28s6HDlMCENCyUnmp9EvRBuI3P90XG4heGC";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static PayPalConfiguration config = new PayPalConfiguration().environment(CONFIG_ENVIRONMENT).clientId(CONFIG_CLIENT_ID);

    RelativeLayout showsucessmesg;
    String address, newbal = "0", finallab = "", payb = "";
    String timeStamp;
    Bitmap mSignature;
    String str_SignaturePath = "";
    TextView btn_ProfileEdit;
    TextView txt_ValidUntil, txt_Requirements;

    public String file_download_path = "";
    File dir_File;
    String Current_datetime;
    Button btn_SeeMoreCommunication;

    SimpleDateFormat dateFormat, dateFormat1;
    String Expires_Date = "";
    LinearLayout ll_layout1, ll_layout2, ll_layout3;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_managepermit_vehicleuser_details, container, false);
        mContext = getContext();

        StrictMode.VmPolicy policy = new StrictMode.VmPolicy.Builder().detectAll().penaltyLog().build();
        StrictMode.setVmPolicy(policy);

        cd = new ConnectionDetector(mContext);
        s3 = new AmazonS3Client(new BasicAWSCredentials("AKIAJNG22KFRVUAICSEA", "NSrQR/yAg52RPD3kp3FMb3NO+Vrxuty4iAwPU4th"));
        transferUtility = new TransferUtility(s3, getActivity().getApplicationContext());
        arrayListResidency = new ArrayList<String>();
        arrayListDriver = new ArrayList<String>();

        file_download_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" +
                getActivity().getPackageName() + "/Images/";
        dir_File = new File(file_download_path);
        if (!dir_File.exists()) {
            if (!dir_File.mkdirs()) {
            }
        }

        init();

        if (getArguments() != null) {
            id = getArguments().getString("id");
            date_time = getArguments().getString("date_time");
            user_id = getArguments().getString("user_id");
            township_code = getArguments().getString("township_code");
            township_name = getArguments().getString("township_name");
            permit_type = getArguments().getString("permit_type");
            permit_name = getArguments().getString("permit_name");
            residency_proof = getArguments().getString("residency_proof");
            driver_proof = getArguments().getString("driver_proof");
            approved = getArguments().getString("approved");
            status = getArguments().getString("status");
            paid = getArguments().getString("paid");
            scheme_type = getArguments().getString("scheme_type");
            rate = getArguments().getString("rate");
            user_name = getArguments().getString("user_name");
            signature = getArguments().getString("signature");
            First_contact_date = getArguments().getString("First_contact_date");
            Permit_nextnum = getArguments().getString("Permit_nextnum");
            Full_name = getArguments().getString("Full_name");
            Full_address = getArguments().getString("Full_address");
            email = getArguments().getString("email");
            phone = getArguments().getString("phone");
            User_comments = getArguments().getString("User_comments");
            Town_comments = getArguments().getString("Town_comments");
            Logo = getArguments().getString("Logo");

            permit_id = getArguments().getString("permit_id");
            permit_validity = getArguments().getString("permit_validity");
            permit_expires_on = getArguments().getString("permit_expires_on");
            expired = getArguments().getString("expired");
            Doc_requirements = getArguments().getString("Doc_requirements");
            renewable = getArguments().getString("renewable");
            Manager_type = getArguments().getString("Manager_type");
            Manager_type_id = getArguments().getString("Manager_type_id");
        }

        new servicecharge().execute();

        txt_ValidUntil.setText(permit_validity);
        txt_Requirements.setText(Doc_requirements);

        if (!Full_name.equalsIgnoreCase("")) {
            txt_ProfileDetails.setText("Name: " + Full_name + "\nPhone : " + phone + " Email : " + email
                    + "\nAddress : " + Full_address);
        }

        txt_User_comments1.setText("User:");
        if (!User_comments.equalsIgnoreCase("")) {
            txt_User_comments.setText(User_comments);
            txt_User_comments1.setPaintFlags(txt_User_comments1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }

        txt_Town_comments1.setText("Official:");
        if (!Town_comments.equalsIgnoreCase("")) {
            txt_Town_comments.setText(Town_comments);
            txt_Town_comments1.setPaintFlags(txt_Town_comments1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }

        txt_PERMIT.setText(township_code + "-" + permit_name);
        txt_ISSUED.setText(date_time);
        if (rate.equalsIgnoreCase("") || rate.equalsIgnoreCase("null") || rate.isEmpty()) {
            txt_FEE.setText("$0");
        } else {
            txt_FEE.setText("" + rate);
        }

        txt_ExpiryTime.setText(permit_expires_on);

        if (!signature.equalsIgnoreCase("")) {
            img_SignatureProof.setVisibility(View.VISIBLE);
            Signature();
        }

       /* getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                rl_progressbar.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
            }
        });*/

        if (!residency_proof.equalsIgnoreCase("")) {
            listResidency = new ArrayList<String>(Arrays.asList(residency_proof.split(",")));
            if (listResidency.size() > 0) {
                rv_horizontal.setVisibility(View.VISIBLE);
                Residence(0);
            } else {
                rv_horizontal.setVisibility(View.GONE);
            }
        } else {
            rv_horizontal.setVisibility(View.GONE);
        }

        if (!driver_proof.equalsIgnoreCase("")) {
            listDriver = new ArrayList<String>(Arrays.asList(driver_proof.split(",")));
            if (listDriver.size() > 0) {
                rv_horizontal.setVisibility(View.VISIBLE);
                Driver(0);
            } else {
                rv_horizontal.setVisibility(View.GONE);
            }
        } else {
            rv_horizontal.setVisibility(View.GONE);
        }

        //new updateMyPermit().execute();

        onClicks();

        return rootView;
    }

    public void init() {
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) rootView.findViewById(R.id.rl_progressbar);
        showsucessmesg = (RelativeLayout) rootView.findViewById(R.id.sucessfully);
        txt_Print = (TextView) rootView.findViewById(R.id.txt_Print);
        txt_PERMIT = (TextView) rootView.findViewById(R.id.txt_PERMIT);
        txt_ISSUED = (TextView) rootView.findViewById(R.id.txt_ISSUED);
        txt_FEE = (TextView) rootView.findViewById(R.id.txt_FEE);
        txt_ProfileDetails = (TextView) rootView.findViewById(R.id.txt_ProfileDetails);
        txt_ExpiryTime = (TextView) rootView.findViewById(R.id.txt_ExpiryTime);
        img_SignatureProof = (ImageView) rootView.findViewById(R.id.img_SignatureProof);

        img_PrintLogo = (ImageView) rootView.findViewById(R.id.img_PrintLogo);

        rv_horizontal = (RecyclerView) rootView.findViewById(R.id.rv_horizontal);
        horizontalAdapter = new CustomAdapter(getContext(), arrayListResidency);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rv_horizontal.setLayoutManager(horizontalLayoutManagaer);
        rv_horizontal.setAdapter(horizontalAdapter);

        rv_DriverProof = (RecyclerView) rootView.findViewById(R.id.rv_DriverProof);
        driverAdapter = new DriverAdapter(getContext(), arrayListDriver);
        LinearLayoutManager DriverProofLayoutManagaer = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rv_DriverProof.setLayoutManager(DriverProofLayoutManagaer);
        rv_DriverProof.setAdapter(driverAdapter);

        txt_PayFeeViaCard = (TextView) rootView.findViewById(R.id.txt_PayFeeViaCard);
        txt_PayFeeViaWallet = (TextView) rootView.findViewById(R.id.txt_PayFeeViaWallet);

        txt_AddNotes = (TextView) rootView.findViewById(R.id.txt_AddNotes);
        txt_UNPAID = (TextView) rootView.findViewById(R.id.txt_UNPAID);
        txt_PENDING = (TextView) rootView.findViewById(R.id.txt_PENDING);
        txt_EditApplication = (TextView) rootView.findViewById(R.id.txt_EditApplication);
        txt_CancelApplication = (TextView) rootView.findViewById(R.id.txt_CancelApplication);
        txt_EXIT = (TextView) rootView.findViewById(R.id.txt_EXIT);
        txt_User_comments = (TextView) rootView.findViewById(R.id.txt_User_comments);
        txt_Town_comments = (TextView) rootView.findViewById(R.id.txt_Town_comments);
        txt_User_comments1 = (TextView) rootView.findViewById(R.id.txt_User_comments1);
        txt_Town_comments1 = (TextView) rootView.findViewById(R.id.txt_Town_comments1);
        btn_ProfileEdit = (TextView) rootView.findViewById(R.id.btn_ProfileEdit);
        txt_ValidUntil = (TextView) rootView.findViewById(R.id.txt_ValidUntil);
        txt_Requirements = (TextView) rootView.findViewById(R.id.txt_Requirements);
        btn_SeeMoreCommunication = (Button) rootView.findViewById(R.id.btn_SeeMoreCommunication);

        ll_layout1 = (LinearLayout) rootView.findViewById(R.id.ll_layout1);
        ll_layout2 = (LinearLayout) rootView.findViewById(R.id.ll_layout2);
        ll_layout3 = (LinearLayout) rootView.findViewById(R.id.ll_layout3);
    }

    public void onClicks() {

        if (!Logo.equalsIgnoreCase("") && !Logo.equalsIgnoreCase("null")) {
            Picasso.with(getActivity()).load(Logo).into(img_PrintLogo);
        }

        if (expired.equalsIgnoreCase("true")) {
            txt_PayFeeViaCard.setVisibility(View.GONE);
            txt_PayFeeViaWallet.setVisibility(View.GONE);

            ll_layout1.setVisibility(View.VISIBLE);
            ll_layout2.setVisibility(View.VISIBLE);
            ll_layout3.setVisibility(View.VISIBLE);

            txt_UNPAID.setVisibility(View.VISIBLE);
            if (paid.equalsIgnoreCase("1")) {
                txt_UNPAID.setText("PAID");
            } else {
                txt_UNPAID.setText("PYMT DUE");
            }

            txt_EXIT.setVisibility(View.VISIBLE);
            txt_EXIT.setBackgroundResource(R.drawable.button6);
            if (renewable.equalsIgnoreCase("true")) {
                txt_EXIT.setText("RENEW");
            } else {
                txt_EXIT.setVisibility(View.GONE);
                ll_layout3.setVisibility(View.GONE);
            }

            txt_CancelApplication.setVisibility(View.VISIBLE);
            txt_CancelApplication.setText("INACTIVE");

            txt_EditApplication.setVisibility(View.VISIBLE);
            txt_EditApplication.setBackgroundResource(R.drawable.button6);
            txt_EditApplication.setText("RE-APPLY");

            txt_PENDING.setVisibility(View.VISIBLE);
            txt_PENDING.setText("EXPIRED");
            btn_ProfileEdit.setVisibility(View.GONE);

            if (approved.equalsIgnoreCase("0")) {
                txt_AddNotes.setVisibility(View.VISIBLE);
                txt_AddNotes.setText("PENDING");
            } else if (approved.equalsIgnoreCase("1")) {
                txt_AddNotes.setVisibility(View.VISIBLE);
                txt_AddNotes.setText("ACCEPTED");
            } else if (approved.equalsIgnoreCase("2")) {
                txt_AddNotes.setVisibility(View.VISIBLE);
                txt_AddNotes.setText("REJECTED");
            }

        } else {
            if (approved.equalsIgnoreCase("0")) {
                txt_AddNotes.setVisibility(View.VISIBLE);
                txt_UNPAID.setVisibility(View.VISIBLE);
                txt_PENDING.setVisibility(View.VISIBLE);

                ll_layout1.setVisibility(View.VISIBLE);
                ll_layout2.setVisibility(View.VISIBLE);
                ll_layout3.setVisibility(View.VISIBLE);

                txt_EditApplication.setVisibility(View.VISIBLE);
                txt_CancelApplication.setVisibility(View.VISIBLE);
                txt_EXIT.setVisibility(View.VISIBLE);

                txt_PayFeeViaCard.setVisibility(View.GONE);
                txt_PayFeeViaWallet.setVisibility(View.GONE);
                btn_ProfileEdit.setVisibility(View.VISIBLE);

            } else if (approved.equalsIgnoreCase("1")) {

                txt_AddNotes.setVisibility(View.VISIBLE);
                txt_UNPAID.setVisibility(View.VISIBLE);
                txt_PENDING.setVisibility(View.VISIBLE);

                ll_layout1.setVisibility(View.GONE);
                ll_layout2.setVisibility(View.GONE);
                ll_layout3.setVisibility(View.VISIBLE);

                txt_EditApplication.setVisibility(View.GONE);
                txt_CancelApplication.setVisibility(View.GONE);
                txt_EXIT.setVisibility(View.VISIBLE);

                txt_PayFeeViaCard.setVisibility(View.VISIBLE);
                txt_PayFeeViaWallet.setVisibility(View.VISIBLE);
                btn_ProfileEdit.setVisibility(View.GONE);

                txt_PENDING.setText("ACCEPTED");
                if (paid.equalsIgnoreCase("1")) {
                    txt_UNPAID.setText("PAID");
                    txt_PayFeeViaCard.setVisibility(View.GONE);
                    txt_PayFeeViaWallet.setVisibility(View.GONE);
                } else {
                    txt_UNPAID.setText("PYMT DUE");
                    txt_PayFeeViaCard.setVisibility(View.VISIBLE);
                    txt_PayFeeViaWallet.setVisibility(View.VISIBLE);
                    String IfRate = rate.substring(rate.indexOf('$') + 1);
                    if (IfRate.equalsIgnoreCase("0")) {
                        new updateMyPermit().execute();
                    }
                }

                if (status.equalsIgnoreCase("1")) {
                    txt_EXIT.setText("ACTIVE");
                    txt_AddNotes.setVisibility(View.GONE);
                } else {

                }

            } else if (approved.equalsIgnoreCase("2")) {
                txt_AddNotes.setVisibility(View.VISIBLE);
                txt_UNPAID.setVisibility(View.VISIBLE);
                txt_PENDING.setVisibility(View.VISIBLE);

                ll_layout1.setVisibility(View.VISIBLE);
                ll_layout2.setVisibility(View.VISIBLE);
                ll_layout3.setVisibility(View.VISIBLE);

                txt_EditApplication.setVisibility(View.VISIBLE);
                txt_CancelApplication.setVisibility(View.VISIBLE);
                txt_EXIT.setVisibility(View.VISIBLE);

                txt_PayFeeViaCard.setVisibility(View.GONE);
                txt_PayFeeViaWallet.setVisibility(View.GONE);
                btn_ProfileEdit.setVisibility(View.VISIBLE);

                txt_PENDING.setText("REJECTED");
            }
        }


        txt_UNPAID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        txt_PENDING.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        txt_PayFeeViaCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    String ba = rate.substring(rate.indexOf('$') + 1);
                    payball = Double.parseDouble(ba);
                    payball = payball + (payball * tr_percentage1) + tr_fee1;
                    PaypalPaymentIntegration();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Internet Connection Required");
                    alertDialog.setMessage("Please connect to working Internet connection");
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                }
            }
        });

        txt_PayFeeViaWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    String ba = rate.substring(rate.indexOf('$') + 1);
                    payball = Double.parseDouble(ba);
                    new getwalletbal().execute();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Internet Connection Required");
                    alertDialog.setMessage("Please connect to working Internet connection");
                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                }
            }
        });

        txt_Print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPrintOptions(getActivity());
            }
        });

        txt_AddNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txt_AddNotes.getText().toString().trim().equalsIgnoreCase("Add Note")) {
                    addNotes();
                }
            }
        });

        txt_EXIT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txt_EXIT.getText().toString().trim().equalsIgnoreCase("EXIT")) {
                    backticket();
                } else if (txt_EXIT.getText().toString().trim().equalsIgnoreCase("RENEW")) {
                    txt_PayFeeViaCard.setVisibility(View.VISIBLE);
                    txt_PayFeeViaWallet.setVisibility(View.VISIBLE);
                }
            }
        });

        txt_EditApplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txt_EditApplication.getText().toString().trim().equalsIgnoreCase("EDIT")) {
                    Bundle bundle = new Bundle();
                    bundle.putString("id", id);
                    bundle.putString("ListDriver", arrayListDriver.toString());
                    bundle.putString("ListResidency", arrayListResidency.toString());
                    bundle.putString("SignaturePath", str_SignaturePath);

                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    MyPermit_VehicleUser_Details_Edit pay = new MyPermit_VehicleUser_Details_Edit();
                    pay.setArguments(bundle);
                    ft.add(R.id.My_Container_1_ID, pay);
                    ft.commitAllowingStateLoss();
                } else if (txt_EditApplication.getText().toString().trim().equalsIgnoreCase("RE-APPLY")) {
                    Bundle bundle = new Bundle();
                    bundle.putString("id", id);
                    bundle.putString("ListDriver", arrayListDriver.toString());
                    bundle.putString("ListResidency", arrayListResidency.toString());
                    bundle.putString("SignaturePath", str_SignaturePath);

                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    MyPermit_VehicleUser_Details_ReApply pay = new MyPermit_VehicleUser_Details_ReApply();
                    pay.setArguments(bundle);
                    ft.add(R.id.My_Container_1_ID, pay);
                    ft.commitAllowingStateLoss();
                }
            }
        });

        txt_CancelApplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txt_CancelApplication.getText().toString().trim().equalsIgnoreCase("Cancel Application")) {

                }
            }
        });

        btn_ProfileEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Frg_User_edit_profile_Permit pay = new Frg_User_edit_profile_Permit();
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        btn_SeeMoreCommunication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("id", id);

                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                MyPermit_VehicleUser_Details_Communications pay = new MyPermit_VehicleUser_Details_Communications();
                pay.setArguments(bundle);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        //new get_Communications_thread().execute();

    }

    public void addNotes() {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getActivity());
        View mView = layoutInflaterAndroid.inflate(R.layout.add_notes_dialog_box, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(getActivity());
        alertDialogBuilderUserInput.setView(mView);

        final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogBox, int id) {
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
                        Current_datetime = dateformat.format(c.getTime());
                        String addNotes = userInputDialogEditText.getText().toString().trim();
                        if (!addNotes.equalsIgnoreCase("")) {
                            txt_User_comments.setText(addNotes);
                            txt_User_comments1.setPaintFlags(txt_User_comments1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                            new addNotes_Subscription(addNotes).execute();
                        } else {
                            Toast.makeText(getContext(), "Please Enter Notes..", Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogBox, int id) {
                        dialogBox.cancel();
                    }
                });

        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.show();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void showPrintOptions(Activity context) {
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View layout = layoutInflater.inflate(R.layout.select_printer_option, viewGroup, false);
        final PopupWindow popup = new PopupWindow(context);
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(false);
        popup.setFocusable(false);
        popup.setAnimationStyle(R.style.animationName);
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

        ImageView img_close = (ImageView) layout.findViewById(R.id.img_close);
        TextView txt_pdf = (TextView) layout.findViewById(R.id.txt_pdf);
        TextView txt_bluetooth = (TextView) layout.findViewById(R.id.txt_bluetooth);


        final String ss = "Village Of Farmingdale\n\n" + "\nPermit: " + txt_PERMIT.getText().toString() + "\nApplied On: " + txt_ISSUED.getText().toString() +
                "\nFee: " + txt_FEE.getText().toString() + "\nName: " + Full_name + "\nPhone: " + phone +
                "\nEmail: " + email
                + "\nAddress: " + Full_address + "\nUser Comments: " + User_comments +
                "\nTown Comments: " + Town_comments + "\nExpiry Time: " + txt_ExpiryTime.getText().toString() +
                "";

        txt_bluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), Main_Activity.class);
                i.putExtra("date", ss);
                getActivity().startActivity(i);
                popup.dismiss();
            }
        });

        txt_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                createandDisplayPdf();
            }
        });

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

    }

    public void createandDisplayPdf() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        timeStamp = dateFormat.format(new Date());
        com.itextpdf.text.Document doc = new com.itextpdf.text.Document();

        try {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ParkEZly";
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();
            File file1 = new File(path, "logo1.PNG");
            if (file1.exists()) file1.delete();
            FileOutputStream outStream = null;
            try {
                Bitmap bitmap = ((BitmapDrawable) img_PrintLogo.getDrawable()).getBitmap();
                bitmap = Bitmap.createScaledBitmap(bitmap, 120, 120, false);
                outStream = new FileOutputStream(file1);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
                outStream.flush();
                outStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            File file = new File(dir, "view_permit" + timeStamp + ".pdf");
            FileOutputStream fOut = new FileOutputStream(file);
            Font paraFont = new Font(Font.FontFamily.COURIER, 20);

            PdfWriter.getInstance(doc, fOut);

            //open the document
            doc.open();

            String ad = "Village Of Farmingdale\n\n";
            Paragraph pp = new Paragraph();
            Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 22, Font.BOLD);
            pp.setFont(smallBold);
            pp.add(ad);
            pp.setAlignment(Paragraph.ALIGN_CENTER);
            pp.setPaddingTop(30);
            doc.add(pp);

            String getpath = Environment.getExternalStorageDirectory() + "/" + "ParkEZly" + "/" + "logo1.PNG";
            Image image = Image.getInstance(getpath);
            image.setAlignment(Image.MIDDLE | Image.TEXTWRAP);
            image.setWidthPercentage(20);

            doc.add(image);

            String ad1 = "\nPermit: " + txt_PERMIT.getText().toString();
            Paragraph pp1 = new Paragraph();
            Font smallBold1 = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
            pp1.setFont(smallBold1);
            pp1.add(ad1);
            pp1.setAlignment(Paragraph.ALIGN_LEFT);
            pp1.setPaddingTop(30);
            doc.add(pp1);

            Paragraph p1 = new Paragraph("Applied On: " + txt_ISSUED.getText().toString() +
                    "\nFee: " + txt_FEE.getText().toString() + "\nName: " + Full_name + "\nPhone: " + phone +
                    "\nEmail: " + email
                    + "\nAddress: " + Full_address + "\nUser Comments: " + User_comments +
                    "\nTown Comments: " + Town_comments + "\nExpiry Time: " + txt_ExpiryTime.getText().toString() +
                    "");
            p1.setFont(paraFont);
            p1.setAlignment(Paragraph.ALIGN_LEFT);
            p1.setPaddingTop(10);
            doc.add(p1);

            /*PdfPTable table = new PdfPTable(1);
            table.setPaddingTop(10);
            table.getDefaultCell().setBorder(0);
            table.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell("Residence Proof: ");
            PdfPCell cell = new PdfPCell();
            cell.setBorder(Rectangle.NO_BORDER);
            Paragraph pra1 = new Paragraph();
            for (int i = 0; i < arrayListResidency.size(); i++) {
                BitmapFactory.Options bmOptions2 = new BitmapFactory.Options();
                Bitmap bitmap2 = BitmapFactory.decodeFile(arrayListResidency.get(i), bmOptions2);
                bitmap2 = Bitmap.createScaledBitmap(bitmap2, 80, 80, true);
                ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
                bitmap2.compress(Bitmap.CompressFormat.PNG, 100, stream2);
                Image img3 = Image.getInstance(stream2.toByteArray());
                pra1.add(new Chunk(img3, 0, 0, true));
            }
            cell.addElement(pra1);
            table.addCell(cell);
            doc.add(table);

            PdfPTable table1 = new PdfPTable(1);
            table1.setPaddingTop(10);
            table1.getDefaultCell().setBorder(0);
            table1.setHorizontalAlignment(Element.ALIGN_LEFT);
            table1.addCell("Driver Proof: ");
            PdfPCell cell1 = new PdfPCell();
            cell1.setBorder(Rectangle.NO_BORDER);
            Paragraph pra2 = new Paragraph();
            for (int i = 0; i < arrayListDriver.size(); i++) {
                BitmapFactory.Options bmOptions2 = new BitmapFactory.Options();
                Bitmap bitmap2 = BitmapFactory.decodeFile(arrayListDriver.get(i), bmOptions2);
                bitmap2 = Bitmap.createScaledBitmap(bitmap2, 80, 80, true);
                ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
                bitmap2.compress(Bitmap.CompressFormat.PNG, 100, stream2);
                Image img3 = Image.getInstance(stream2.toByteArray());
                pra2.add(new Chunk(img3, 0, 0, true));
            }
            cell1.addElement(pra2);
            table1.addCell(cell1);
            doc.add(table1);

            PdfPTable table2 = new PdfPTable(1);
            table2.setPaddingTop(10);
            table2.getDefaultCell().setBorder(0);
            table2.setHorizontalAlignment(Element.ALIGN_LEFT);
            table2.addCell("Signature Proof: ");
            PdfPCell cell2 = new PdfPCell();
            cell2.setBorder(Rectangle.NO_BORDER);
            Paragraph pra3 = new Paragraph();
            for (int i = 0; i < 1; i++) {
                Bitmap bitmap2 = mSignature;
                bitmap2 = Bitmap.createScaledBitmap(bitmap2, 100, 150, true);
                ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
                bitmap2.compress(Bitmap.CompressFormat.PNG, 100, stream2);
                Image img3 = Image.getInstance(stream2.toByteArray());
                pra3.add(new Chunk(img3, 0, 0, true));
            }
            cell2.addElement(pra3);
            table2.addCell(cell2);
            doc.add(table2);*/

        } catch (DocumentException de) {
            Log.e("PDFCreator", "DocumentException:" + de);
        } catch (IOException e) {
            Log.e("PDFCreator", "ioException:" + e);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            doc.close();
        }
        String getpath = Environment.getExternalStorageDirectory() + "/" + "parkEZly" + "/" + "view_permit" + timeStamp + ".pdf";
        try {
            manipulatePdf(getpath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    public void manipulatePdf(String src) throws IOException, DocumentException {
        try {
            PdfReader reader = new PdfReader(src);
            int n = reader.getNumberOfPages();
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ParkEZly";
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();

            File file = new File(dir, "View_permits" + timeStamp + ".pdf");
            FileOutputStream fOut = new FileOutputStream(file);
            PdfStamper stamper = new PdfStamper(reader, fOut);
            stamper.setRotateContents(false);
            // text watermark
           /* Font f = new Font(Font.FontFamily.HELVETICA, 30);
            Phrase p = new Phrase("My watermark (text)", f);*/
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File file1 = new File(path, "logo.PNG");
            FileOutputStream outStream = null;
            try {
                ImageView imgq = new ImageView(getActivity());
                imgq.setImageResource(R.drawable.rsz_1rsz_splashscreen);
                Bitmap bitmap = ((BitmapDrawable) imgq.getDrawable()).getBitmap();
                outStream = new FileOutputStream(file1);
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, outStream);
                outStream.flush();
                outStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String getpath = Environment.getExternalStorageDirectory() + "/" + "ParkEZly" + "/" + "logo.PNG";
            // image watermark
            Image img = Image.getInstance(getpath);
            float w = img.getScaledWidth();
            float h = img.getScaledHeight();

            // transparency
            PdfGState gs1 = new PdfGState();
            gs1.setFillOpacity(0.2f);
            // properties
            PdfContentByte over;
            Rectangle pagesize;
            float x, y;

            // loop over every page
            for (int i = 1; i <= n; i++) {
                pagesize = reader.getPageSize(i);

                x = (pagesize.getLeft() + pagesize.getRight()) / 2;
                y = (pagesize.getTop() + pagesize.getBottom()) / 2;
                over = stamper.getOverContent(i);
                over.saveState();
                over.setGState(gs1);
                //over.addImage(img,true);
                float gg = x - (w / 2);
                float hh = y - (h / 2);
                Log.e("dcd", String.valueOf(gg));

                Log.e("dcd", String.valueOf(hh));
                over.addImage(img, w, 0, 0, h, x - (w / 2), y - (h / 2));
                //  over.addImage(img, 200,0,200,0,0,0);
                over.restoreState();
            }
            stamper.close();
            reader.close();

            // viewPdf("parking_ticket" + plate_no +timeStamp+"1" + ".pdf", "parkEZly");
            onther_view(file);
            File file11 = new File(dir, "view_permit" + timeStamp + ".pdf");
            file11.delete();
        } catch (IOException os) {
            Log.e("vb", String.valueOf(os));
        }
    }

    public void onther_view(File file) {
        File pdfFile = file;
        try {
            if (pdfFile.exists()) {
                Uri path1 = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", pdfFile);
                Intent objIntent = new Intent(Intent.ACTION_VIEW);
                objIntent.setDataAndType(path1, "application/pdf");
                objIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(objIntent);
            } else {
                Toast.makeText(getActivity(), "The file not exists! ", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PaypalPaymentIntegration() {
        config.acceptCreditCards(true);
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal(payball), "USD", "Parking Payment", paymentIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.i(TAG, confirm.toJSONObject().toString(4));
                    Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                    JSONObject jsonObj = new JSONObject(confirm.getPayment().toJSONObject().toString(4));

                    JSONObject jsonObjResponse = confirm.toJSONObject().getJSONObject("response");
                    String transection_id = jsonObjResponse.getString("id");

                    if (transection_id != null) {
                        if (txt_EXIT.getText().toString().trim().equalsIgnoreCase("RENEW")) {
                            dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                            Calendar getCurrent = Calendar.getInstance();
                            String getDate = dateFormat.format(getCurrent.getTime());
                            try {
                                Date pDate = dateFormat.parse(getDate);
                                if (permit_validity.equalsIgnoreCase("365 days")) {
                                    Calendar c = Calendar.getInstance();
                                    c.setTime(pDate);
                                    c.add(Calendar.DATE, 365);
                                    Date expDate = c.getTime();
                                    Expires_Date = dateFormat1.format(expDate) + " 23:59:59";
                                } else if (permit_validity.equalsIgnoreCase("YearEnd")) {
                                    Calendar c = Calendar.getInstance();
                                    c.setTime(pDate);
                                    Expires_Date = String.valueOf(c.get(Calendar.YEAR)) + "-12-31 23:59:59";
                                } else if (permit_validity.equalsIgnoreCase("MonthEnd")) {
                                    Calendar c = Calendar.getInstance();
                                    c.setTime(pDate);
                                    int thisMonth = c.get(Calendar.MONTH) + 1;
                                    Expires_Date = String.valueOf(c.get(Calendar.YEAR) + "-" + thisMonth
                                            + "-" + c.getActualMaximum(Calendar.DATE)) + " 23:59:59";
                                } else if (permit_validity.equalsIgnoreCase("30 Days")) {
                                    Calendar c = Calendar.getInstance();
                                    c.setTime(pDate);
                                    c.add(Calendar.DATE, 30);
                                    Date expDate = c.getTime();
                                    Expires_Date = dateFormat.format(expDate);
                                } else if (permit_validity.equalsIgnoreCase("7 Days")) {
                                    Calendar c = Calendar.getInstance();
                                    c.setTime(pDate);
                                    c.add(Calendar.DATE, 7);
                                    Date expDate = c.getTime();
                                    Expires_Date = dateFormat.format(expDate);
                                } else if (permit_validity.equalsIgnoreCase("Weekend")) {
                                    Calendar c = Calendar.getInstance();
                                    c.setTime(pDate);

                                    int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                                    if (Calendar.MONDAY == dayOfWeek) {
                                        c.add(Calendar.DATE, 6);
                                    } else if (Calendar.TUESDAY == dayOfWeek) {
                                        c.add(Calendar.DATE, 5);
                                    } else if (Calendar.WEDNESDAY == dayOfWeek) {
                                        c.add(Calendar.DATE, 4);
                                    } else if (Calendar.THURSDAY == dayOfWeek) {
                                        c.add(Calendar.DATE, 3);
                                    } else if (Calendar.FRIDAY == dayOfWeek) {
                                        c.add(Calendar.DATE, 2);
                                    } else if (Calendar.SATURDAY == dayOfWeek) {
                                        c.add(Calendar.DATE, 1);
                                    } else if (Calendar.SUNDAY == dayOfWeek) {
                                        c.add(Calendar.DATE, 0);
                                    }

                                    int thisMonth = c.get(Calendar.MONTH) + 1;
                                    Expires_Date = String.valueOf(c.get(Calendar.YEAR) + "-" + thisMonth
                                            + "-" + c.get(Calendar.DAY_OF_MONTH)) + " 23:59:59";
                                } else if (permit_validity.equalsIgnoreCase("1 Days")) {
                                    Calendar c = Calendar.getInstance();
                                    c.setTime(pDate);
                                    c.add(Calendar.DATE, 1);
                                    Date expDate = c.getTime();
                                    Expires_Date = dateFormat.format(expDate);
                                } else if (permit_validity.equalsIgnoreCase("24 Hours")) {
                                    Calendar c = Calendar.getInstance();
                                    c.setTime(pDate);
                                    c.add(Calendar.DATE, 1);
                                    Date expDate = c.getTime();
                                    Expires_Date = dateFormat.format(expDate);
                                } else if (permit_validity.equalsIgnoreCase("Daily")) {
                                    Calendar c = Calendar.getInstance();
                                    c.setTime(pDate);
                                    int thisMonth = c.get(Calendar.MONTH) + 1;
                                    Expires_Date = String.valueOf(c.get(Calendar.YEAR) + "-" + thisMonth
                                            + "-" + c.get(Calendar.DAY_OF_MONTH)) + " 23:59:59";
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            new RenewUpdateMyPermit().execute();
                        } else {
                            new updateMyPermit().execute();
                        }
                    } else {
                        transactionfailalert();
                    }
                    Log.e("transactionId", ":;;;;" + transection_id);

                } catch (JSONException e) {
                    Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                }
            }
        }
    }

    public void transactionfailalert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Transaction Fail try again");
        alertDialog.setMessage("Fail");
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        alertDialog.show();
    }

    public void Residence(int current_pos) {
        s_pos = current_pos;
        try {
            final File file_exists = new File(file_download_path, listResidency.get(s_pos));
            if (file_exists.exists()) {
                arrayListResidency.add(file_exists.getAbsolutePath());
                horizontalAdapter.notifyDataSetChanged();
                if (listResidency.size() == s_pos + 1) {

                } else {
                    s_pos++;
                    Residence(s_pos);
                }
            } else {
                TransferObserver downloadObserver = transferUtility.download("parkezly-images", "/images/" + listResidency.get(s_pos), file_exists);
                downloadObserver.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (TransferState.COMPLETED == state) {
                            arrayListResidency.add(file_exists.getAbsolutePath());
                            horizontalAdapter.notifyDataSetChanged();
                            if (listResidency.size() == s_pos + 1) {

                            } else {
                                s_pos++;
                                Residence(s_pos);
                            }
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                        int percentDone = (int) percentDonef;
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        ex.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Driver(int current_pos) {
        d_pos = current_pos;
        try {
            final File file_exists = new File(file_download_path, listDriver.get(d_pos));
            if (file_exists.exists()) {
                arrayListResidency.add(file_exists.getAbsolutePath());
                horizontalAdapter.notifyDataSetChanged();
                if (listDriver.size() == d_pos + 1) {

                } else {
                    d_pos++;
                    Driver(d_pos);
                }
            } else {
                TransferObserver downloadObserver = transferUtility.download("parkezly-images", "/images/" + listDriver.get(d_pos), file_exists);

                downloadObserver.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (TransferState.COMPLETED == state) {
                            arrayListResidency.add(file_exists.getAbsolutePath());
                            horizontalAdapter.notifyDataSetChanged();
                            if (listDriver.size() == d_pos + 1) {

                            } else {
                                d_pos++;
                                Driver(d_pos);
                            }
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                        int percentDone = (int) percentDonef;
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        ex.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Signature() {
        try {
            final File file_exists = new File(file_download_path, signature);
            if (file_exists.exists()) {
                Bitmap bmp = BitmapFactory.decodeFile(file_exists.getAbsolutePath());
                str_SignaturePath = file_exists.getAbsolutePath();
                mSignature = bmp;
                img_SignatureProof.setImageBitmap(bmp);
            } else {
                TransferObserver downloadObserver = transferUtility.download("parkezly-images",
                        "/images/" + signature, file_exists);

                downloadObserver.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (TransferState.COMPLETED == state) {
                            Bitmap bmp = BitmapFactory.decodeFile(file_exists.getAbsolutePath());
                            str_SignaturePath = file_exists.getAbsolutePath();
                            mSignature = bmp;
                            img_SignatureProof.setImageBitmap(bmp);
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                        int percentDone = (int) percentDonef;
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        ex.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {
        ArrayList<String> dataSet;
        Context mContext;

        public CustomAdapter(Context context, ArrayList<String> arrayListSignature) {
            mContext = context;
            dataSet = arrayListSignature;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView imageViewIcon;
            TextView txt_Name;

            public MyViewHolder(View itemView) {
                super(itemView);
                this.imageViewIcon = (ImageView) itemView.findViewById(R.id.image);
                txt_Name = (TextView) itemView.findViewById(R.id.txt_Name);
            }
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapater_managepermit_items, parent, false);

            MyViewHolder myViewHolder = new MyViewHolder(view);
            return myViewHolder;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
            //Picasso.with(getContext()).load(dataSet.get(listPosition)).into(holder.imageViewIcon);
            final String extension = dataSet.get(listPosition).toString().substring(dataSet.get(listPosition).toString().lastIndexOf("."));
            if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg")
                    || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".bmp")) {
                Bitmap bmp = BitmapFactory.decodeFile(dataSet.get(listPosition));
                holder.imageViewIcon.setImageBitmap(bmp);
            } else {
                Bitmap bmp = BitmapFactory.decodeFile(dataSet.get(listPosition));
                holder.imageViewIcon.setImageDrawable(getResources().getDrawable(R.mipmap.docs));
            }

            holder.txt_Name.setText((new File(dataSet.get(listPosition)).getName()));

            holder.imageViewIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg")
                            || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".bmp")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(new File(dataSet.get(listPosition))), "image/*");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        MimeTypeMap myMime = MimeTypeMap.getSingleton();
                        Intent newIntent = new Intent(Intent.ACTION_VIEW);
                        String mimeType = myMime.getMimeTypeFromExtension(fileExt(String.valueOf(((dataSet.get(listPosition))))).substring(1));
                        newIntent.setDataAndType(Uri.fromFile((new File(dataSet.get(listPosition)))), mimeType);
                        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        try {
                            getActivity().startActivity(newIntent);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(getActivity(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
                        }
                    }

                }
            });
        }

        @Override
        public int getItemCount() {
            return dataSet.size();
        }
    }

    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();

        }
    }

    public class DriverAdapter extends RecyclerView.Adapter<DriverAdapter.MyViewHolder> {
        ArrayList<String> dataSet;
        Context mContext;

        public DriverAdapter(Context context, ArrayList<String> arrayListSignature) {
            mContext = context;
            dataSet = arrayListSignature;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView imageViewIcon;
            TextView txt_Name;

            public MyViewHolder(View itemView) {
                super(itemView);
                this.imageViewIcon = (ImageView) itemView.findViewById(R.id.image);
                txt_Name = (TextView) itemView.findViewById(R.id.txt_Name);
            }
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapater_managepermit_items, parent, false);

            MyViewHolder myViewHolder = new MyViewHolder(view);
            return myViewHolder;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

            final String extension = dataSet.get(listPosition).toString().substring(dataSet.get(listPosition).toString().lastIndexOf("."));
            if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg")
                    || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".bmp")) {
                Bitmap bmp = BitmapFactory.decodeFile(dataSet.get(listPosition));
                holder.imageViewIcon.setImageBitmap(bmp);
            } else {
                Bitmap bmp = BitmapFactory.decodeFile(dataSet.get(listPosition));
                holder.imageViewIcon.setImageDrawable(getResources().getDrawable(R.mipmap.docs));
            }

            holder.txt_Name.setText((new File(dataSet.get(listPosition)).getName()));

            holder.imageViewIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg")
                            || extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".bmp")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(new File(dataSet.get(listPosition))), "image/*");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        MimeTypeMap myMime = MimeTypeMap.getSingleton();
                        Intent newIntent = new Intent(Intent.ACTION_VIEW);
                        String mimeType = myMime.getMimeTypeFromExtension(fileExt(String.valueOf(((dataSet.get(listPosition))))).substring(1));
                        newIntent.setDataAndType(Uri.fromFile((new File(dataSet.get(listPosition)))), mimeType);
                        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        try {
                            getActivity().startActivity(newIntent);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(getActivity(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
                        }
                    }

                }
            });

        }

        @Override
        public int getItemCount() {
            return dataSet.size();
        }
    }

    public class updateMyPermit extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String managedlosturl = "_table/permit_subscription";
        Date current;
        String res_id = "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("id", id);
            jsonValues1.put("paid", "1");
            jsonValues1.put("status", "1");

            JSONObject json1 = new JSONObject(jsonValues1);

            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        res_id = c.getString("id");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (!res_id.equals("null")) {
                    showsucessmesg.setVisibility(View.VISIBLE);
                    new updateMyPermit_Archive(res_id).execute();
                }

            }
        }
    }

    public class updateMyPermit_Archive extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String managedlosturl = "_table/permit_subscription_archive";
        Date current;
        String res_id = "", permit_sbscription_id = "";

        public updateMyPermit_Archive(String res_id) {
            permit_sbscription_id = res_id;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("permit_sbscription_id", permit_sbscription_id);
            jsonValues1.put("id", id);
            jsonValues1.put("paid", "1");
            jsonValues1.put("status", "1");


            JSONObject json1 = new JSONObject(jsonValues1);

            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    Log.e("json ", " : " + json);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        res_id = c.getString("id");
                    }
                    Log.e("responseStr ", " : " + res_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (!res_id.equals("null")) {
                    showsucessmesg.setVisibility(View.VISIBLE);
                    ActionStartsHere();
                }

            }
        }
    }

    Date current;

    public class RenewUpdateMyPermit extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String managedlosturl = "_table/permit_subscription";

        String res_id = "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            String currentdate = sdf.format(new Date());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            try {
                current = format.parse(currentdate);
                Log.e("current_date", String.valueOf(current));
                System.out.println(current);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("id", id);
            jsonValues1.put("paid", "1");
            jsonValues1.put("status", "1");
            jsonValues1.put("expired", "0");
            jsonValues1.put("permit_expires_on", Expires_Date);
            jsonValues1.put("date_action", current);

            JSONObject json1 = new JSONObject(jsonValues1);

            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        res_id = c.getString("id");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (!res_id.equals("null")) {
                    showsucessmesg.setVisibility(View.VISIBLE);
                    new RenewUpdateMyPermit_Archive(res_id).execute();
                }

            }
        }
    }

    public class RenewUpdateMyPermit_Archive extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String managedlosturl = "_table/permit_subscription_archive";
        String res_id = "", permit_sbscription_id = "";

        public RenewUpdateMyPermit_Archive(String res_id) {
            permit_sbscription_id = res_id;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("permit_sbscription_id", permit_sbscription_id);
            jsonValues1.put("id", id);
            jsonValues1.put("paid", "1");
            jsonValues1.put("status", "1");
            jsonValues1.put("expired", "0");
            jsonValues1.put("permit_expires_on", Expires_Date);
            jsonValues1.put("date_action", current);

            JSONObject json1 = new JSONObject(jsonValues1);

            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    Log.e("json ", " : " + json);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        res_id = c.getString("id");
                    }
                    Log.e("responseStr ", " : " + res_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (!res_id.equals("null")) {
                    showsucessmesg.setVisibility(View.VISIBLE);
                    ActionStartsHere();
                }

            }
        }
    }


    public void ActionStartsHere() {
        againStartGPSAndSendFile();
    }

    public void againStartGPSAndSendFile() {
        new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                showsucessmesg.setVisibility(View.GONE);
                if (getActivity() != null) {
                    TextView tvTitle = new TextView(getActivity());
                    tvTitle.setText("Done ✓");
                    tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                    tvTitle.setTextColor(Color.BLACK);
                    tvTitle.setTextSize(20);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setCustomTitle(tvTitle);
                    alertDialog.setMessage("Permit Update Successfully");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            backticket();
                        }
                    });
                    alertDialog.show();
                }
            }

        }.start();
    }

    public void backticket() {
        getActivity().onBackPressed();
    }

    public class getwalletbal extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        String wallbalurl = "_table/user_wallet?order=date_time%20DESC";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + wallbalurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            HttpEntity resEntity = response.getEntity();
            String responseStr = null;

            try {
                responseStr = EntityUtils.toString(resEntity).trim();

                json = new JSONObject(responseStr);

                JSONArray array = json.getJSONArray("resource");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject c = array.getJSONObject(i);
                    String uid = c.getString("user_id");
                    if (uid.equals(user_id)) {
                        newbal = c.getString("new_balance");
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }

            if (getActivity() != null && json != null) {
                double pay = Double.parseDouble(newbal);
                if (pay > 0) {
                    finalpay = 0.0;
                    //payball = payball + (payball * tr_percentage1) + tr_fee1;
                    //payball = payball + (payball * tr_percentage1) + tr_fee1;
                    finalpay = Double.parseDouble(newbal) - payball;
                    finallab = String.format("%.2f", finalpay);
                    payb = (String.format("%.2f", payball));
                    String wallbal = (String.format("%.2f", Double.parseDouble(newbal)));
                    if (finalpay < 0) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setMessage("You don't have enough funds in your wallet.");
                        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alertDialog.show();
                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Wallet Balance: $" + wallbal);
                        alertDialog.setMessage("Would you like to pay $" + payb + " from your ParkEZly wallet?");
                        alertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new updatewallet().execute();
                            }
                        });
                        alertDialog.setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alertDialog.show();
                    }
                } else {
                    android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("alert");
                    alertDialog.setMessage("You don't have enough funds in your wallet.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
            super.onPostExecute(s);
        }
    }

    public class updatewallet extends AsyncTask<String, String, String> {
        String idr = "null";
        JSONObject json, json1;
        String transactionurl = "_table/user_wallet";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            String currentdate = sdf.format(new Date());
            String ip = getLocalIpAddress();
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("paid_date", currentdate);
            jsonValues.put("last_paid_amt", payb);
            jsonValues.put("ip", ip);
            jsonValues.put("ipn_status", "");
            jsonValues.put("current_balance", finallab);
            jsonValues.put("ipn_txn_id", "");
            jsonValues.put("add_amt", "");
            jsonValues.put("ipn_custom", "");
            jsonValues.put("ipn_payment", "");
            jsonValues.put("date_time", currentdate);
            jsonValues.put("ipn_address", "");
            jsonValues.put("user_id", user_id);
            jsonValues.put("new_balance", finallab);
            jsonValues.put("action", "deduction");
            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    Log.e("response ", " : " + json1.toString());
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        idr = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json1 != null) {
                if (!idr.equals("null")) {
                    if (txt_EXIT.getText().toString().trim().equalsIgnoreCase("RENEW")) {
                        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                        Calendar getCurrent = Calendar.getInstance();
                        String getDate = dateFormat.format(getCurrent.getTime());
                        try {
                            Date pDate = dateFormat.parse(getDate);
                            if (permit_validity.equalsIgnoreCase("365 days")) {
                                Calendar c = Calendar.getInstance();
                                c.setTime(pDate);
                                c.add(Calendar.DATE, 365);
                                Date expDate = c.getTime();
                                Expires_Date = dateFormat1.format(expDate) + " 23:59:59";
                            } else if (permit_validity.equalsIgnoreCase("YearEnd")) {
                                Calendar c = Calendar.getInstance();
                                c.setTime(pDate);
                                Expires_Date = String.valueOf(c.get(Calendar.YEAR)) + "-12-31 23:59:59";
                            } else if (permit_validity.equalsIgnoreCase("MonthEnd")) {
                                Calendar c = Calendar.getInstance();
                                c.setTime(pDate);
                                int thisMonth = c.get(Calendar.MONTH) + 1;
                                Expires_Date = String.valueOf(c.get(Calendar.YEAR) + "-" + thisMonth
                                        + "-" + c.getActualMaximum(Calendar.DATE)) + " 23:59:59";
                            } else if (permit_validity.equalsIgnoreCase("30 Days")) {
                                Calendar c = Calendar.getInstance();
                                c.setTime(pDate);
                                c.add(Calendar.DATE, 30);
                                Date expDate = c.getTime();
                                Expires_Date = dateFormat.format(expDate);
                            } else if (permit_validity.equalsIgnoreCase("7 Days")) {
                                Calendar c = Calendar.getInstance();
                                c.setTime(pDate);
                                c.add(Calendar.DATE, 7);
                                Date expDate = c.getTime();
                                Expires_Date = dateFormat.format(expDate);
                            } else if (permit_validity.equalsIgnoreCase("Weekend")) {
                                Calendar c = Calendar.getInstance();
                                c.setTime(pDate);

                                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                                if (Calendar.MONDAY == dayOfWeek) {
                                    c.add(Calendar.DATE, 6);
                                } else if (Calendar.TUESDAY == dayOfWeek) {
                                    c.add(Calendar.DATE, 5);
                                } else if (Calendar.WEDNESDAY == dayOfWeek) {
                                    c.add(Calendar.DATE, 4);
                                } else if (Calendar.THURSDAY == dayOfWeek) {
                                    c.add(Calendar.DATE, 3);
                                } else if (Calendar.FRIDAY == dayOfWeek) {
                                    c.add(Calendar.DATE, 2);
                                } else if (Calendar.SATURDAY == dayOfWeek) {
                                    c.add(Calendar.DATE, 1);
                                } else if (Calendar.SUNDAY == dayOfWeek) {
                                    c.add(Calendar.DATE, 0);
                                }

                                int thisMonth = c.get(Calendar.MONTH) + 1;
                                Expires_Date = String.valueOf(c.get(Calendar.YEAR) + "-" + thisMonth
                                        + "-" + c.get(Calendar.DAY_OF_MONTH)) + " 23:59:59";
                            } else if (permit_validity.equalsIgnoreCase("1 Days")) {
                                Calendar c = Calendar.getInstance();
                                c.setTime(pDate);
                                c.add(Calendar.DATE, 1);
                                Date expDate = c.getTime();
                                Expires_Date = dateFormat.format(expDate);
                            } else if (permit_validity.equalsIgnoreCase("24 Hours")) {
                                Calendar c = Calendar.getInstance();
                                c.setTime(pDate);
                                c.add(Calendar.DATE, 1);
                                Date expDate = c.getTime();
                                Expires_Date = dateFormat.format(expDate);
                            } else if (permit_validity.equalsIgnoreCase("Daily")) {
                                Calendar c = Calendar.getInstance();
                                c.setTime(pDate);
                                int thisMonth = c.get(Calendar.MONTH) + 1;
                                Expires_Date = String.valueOf(c.get(Calendar.YEAR) + "-" + thisMonth
                                        + "-" + c.get(Calendar.DAY_OF_MONTH)) + " 23:59:59";
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        new RenewUpdateMyPermit().execute();
                    } else {
                        new updateMyPermit().execute();
                    }
                } else {
                }
            }
        }
    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("ip", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("aax", ex.toString());
        }
        return null;
    }

    public class get_Communications_thread extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String responseStr = null;
        String managedlosturl = "_table/communications_thread";

        {
            try {
                managedlosturl = "_table/communications_thread?filter=permit_subscription_id%3D'" + URLEncoder.encode(id, "utf8") + "'";
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("responseStr ", " : " + responseStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return responseStr;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            rl_progressbar.setVisibility(View.GONE);
            if (responseStr != null) {

            }
        }
    }

    public class addNotes_Subscription extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String responseStr = null;
        String managedlosturl = "_table/permit_subscription";
        Date current;
        String mAddNotes = "";

        public addNotes_Subscription(String addNotes) {
            mAddNotes = addNotes;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            MultipartEntity reqEntity = null;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            String currentdate = sdf.format(new Date());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            try {
                current = format.parse(currentdate);
                Log.e("current_date", String.valueOf(current));
                System.out.println(current);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();

            jsonValues1.put("id", id);
            jsonValues1.put("user_comments", mAddNotes);

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("responseStr ", " : " + responseStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return responseStr;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && responseStr != null) {
                boolean errors = responseStr.indexOf("error") > 0;
                if (errors) {
                    Toast.makeText(getContext(), "Some Error..", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getContext(), "Successfully added", Toast.LENGTH_LONG).show();
                    try {
                        JSONObject jsonObject = new JSONObject(responseStr);
                        JSONArray jsonArray = jsonObject.getJSONArray("resource");
                        String jsonObject1 = jsonArray.getJSONObject(0).getString("id");
                        Log.e("jsonObject1", " : " + jsonObject1);
                        new addNotes_Subscription_Archive(mAddNotes, jsonObject1).execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }

    public class addNotes_Subscription_Archive extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String responseStr = null;
        String managedlosturl = "_table/permit_subscription_archive";
        Date current;
        String mAddNotes = "", permit_sbscription_id = "";

        public addNotes_Subscription_Archive(String addNotes, String permit_Id) {
            mAddNotes = addNotes;
            permit_sbscription_id = permit_Id;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("permit_sbscription_id", permit_sbscription_id);
            jsonValues1.put("id", id);
            jsonValues1.put("user_comments", mAddNotes);

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("responseStr ", " : " + responseStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return responseStr;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null)
                new addNotes_Communications_thread(mAddNotes, permit_sbscription_id).execute();
        }
    }

    public class addNotes_Communications_thread extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String responseStr = null;
        String managedlosturl = "_table/communications_thread";
        Date current;
        String mAddNotes = "", DateTime = "";

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String user_role = logindeatl.getString("user_role", "null");
        String name = logindeatl.getString("name", "null");


        public addNotes_Communications_thread(String addNotes, String dateTime) {
            mAddNotes = addNotes;
            DateTime = dateTime;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("date_time", Current_datetime);
            jsonValues1.put("user_id", user_id);
            jsonValues1.put("user_name", user_id);
            jsonValues1.put("email", email);
            jsonValues1.put("phone", phone);
            jsonValues1.put("permit_subscription_id", DateTime);
            jsonValues1.put("profile_name", name);
            jsonValues1.put("profile_type", user_role);
            jsonValues1.put("comments", mAddNotes);

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("responseStr ", " : " + responseStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return responseStr;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
        }
    }

    public class servicecharge extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        String servicechargeurl = "_table/service_charges?filter=manager_id%3D'" + township_code + "'";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + servicechargeurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        String tr_percentage = c.getString("tr_percentage");
                        String tr_fee = c.getString("tr_fee");
                        if (!tr_fee.equals("null")) {
                            tr_fee1 = Double.parseDouble(tr_fee);
                        } else {
                            tr_fee1 = 0.0;
                        }
                        if (!tr_percentage.equals("null")) {
                            tr_percentage1 = Double.parseDouble(tr_percentage);
                        } else {
                            tr_percentage1 = 0.0;
                        }
                        Log.e("tr_percentage1", String.valueOf(tr_percentage1));
                        Log.e("tr_free", String.valueOf(tr_fee1));
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null && json != null) {
            }
            super.onPostExecute(s);
        }
    }

}
