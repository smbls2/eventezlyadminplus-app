package com.softmeasures.eventezlyadminplus.frament.event.AddEdit;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nileshp.multiphotopicker.photopicker.activity.PickImageActivity;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragSessionInfoAddEditBinding;
import com.softmeasures.eventezlyadminplus.frament.event.session.SessionImageAddEditFragment;
import com.softmeasures.eventezlyadminplus.frament.event.session.SessionsFragment;
import com.softmeasures.eventezlyadminplus.models.EventImage;
import com.softmeasures.eventezlyadminplus.models.EventSession;
import com.softmeasures.eventezlyadminplus.models.EventType;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_LOCATION;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_VIRTUALLY;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_BOTH;

public class SessionInfoAddEditFragment extends BaseFragment {

//    public FragSessionInfoAddEditBinding binding;
//    private String imageOption = "";
//    private static final int REQUEST_PICK_IMAGE = 985;
//    public static boolean isEventLogoChange = false, isEventImage1Change = false, isEventImage2Change = false;
//
//    private ArrayList<EventType> eventTypes = new ArrayList<>();
//    private ArrayAdapter<EventType> eventTypeArrayAdapter;
//
//    private EventSession eventSession;
//    private ArrayList<EventImage> eventImages = new ArrayList<>();
//    private EventImageAdapter eventImageAdapter;
//    private boolean isEdit = false, isRepeat = false;
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        if (getArguments() != null) {
//            isEdit = getArguments().getBoolean("isEdit", false);
//            isRepeat = getArguments().getBoolean("isRepeat", false);
//            eventSession = new Gson().fromJson(getArguments().getString("eventSession"), EventSession.class);
//        }
//        binding = DataBindingUtil.inflate(inflater, R.layout.frag_session_info_add_edit, container, false);
//        return binding.getRoot();
//    }
//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        initViews(view);
//        updateViews();
//        setListeners();
//
//        new fetchEventTypes().execute();
//    }
//
//    private class fetchEventTypes extends AsyncTask<String, String, String> {
//        JSONObject json;
//        JSONArray json1;
//        String eventDefUrl = "_table/event_type";
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            eventTypes.clear();
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            String url = getString(R.string.api) + getString(R.string.povlive) + eventDefUrl;
//            Log.e("#DEBUG", "      fetchEventType:  " + url);
//            DefaultHttpClient client = new DefaultHttpClient();
//            HttpGet post = new HttpGet(url);
//            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
//            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
//            HttpResponse response = null;
//            try {
//                response = client.execute(post);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            if (response != null) {
//                HttpEntity resEntity = response.getEntity();
//                String responseStr = null;
//                try {
//                    responseStr = EntityUtils.toString(resEntity).trim();
//                    json = new JSONObject(responseStr);
//                    JSONArray jsonArray = json.getJSONArray("resource");
//
//                    for (int j = 0; j < jsonArray.length(); j++) {
//                        EventType eventType = new EventType();
//                        JSONObject object = jsonArray.getJSONObject(j);
//                        if (object.has("id")
//                                && !TextUtils.isEmpty(object.getString("id"))
//                                && !object.getString("id").equals("null")) {
//                            eventType.setId(object.getInt("id"));
//                        }
//                        if (object.has("event_type")
//                                && !TextUtils.isEmpty(object.getString("event_type"))
//                                && !object.getString("event_type").equals("null")) {
//                            eventType.setEvent_type(object.getString("event_type"));
//                        }
//                        if (object.has("icon_event_type_link")
//                                && !TextUtils.isEmpty(object.getString("icon_event_type_link"))
//                                && !object.getString("icon_event_type_link").equals("null")) {
//                            eventType.setIcon_event_type_link(object.getString("icon_event_type_link"));
//                        }
//
//                        eventTypes.add(eventType);
//
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            if (getActivity() != null) {
//                if (eventTypes.size() != 0) {
//                    if (eventTypeArrayAdapter != null) {
//                        eventTypeArrayAdapter.notifyDataSetChanged();
//                        if (isEdit) {
//                            if (eventSession != null && eventSession.getEvent_session_type() != null
//                                    && !TextUtils.isEmpty(eventSession.getEvent_session_type())) {
//                                if (eventTypes.indexOf(new EventType(eventSession.getEvent_session_type())) != -1) {
//                                    binding.spEventType.setSelection(eventTypes.indexOf(new EventType(eventSession.getEvent_session_type())));
//                                }
//                            }
//                        }
//                    }
//                } else {
//
//                }
//            }
//        }
//    }
//
//    @Override
//    protected void updateViews() {
//        if (isEdit) {
//
//            if (eventSession != null) {
//
//                if (eventSession.getEvent_session_name() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_name())) {
//                    binding.etEventName.setText(eventSession.getEvent_session_name());
//                }
//
//                if (eventSession.getEvent_session_short_description() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_short_description())) {
//                    binding.etEventSortDesc.setText(eventSession.getEvent_session_short_description());
//                }
//
//                if (eventSession.getEvent_session_long_description() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_long_description())) {
//                    binding.etEventLongDecr.setText(eventSession.getEvent_session_long_description());
//                }
//
//                if (eventSession.getRequirements() != null
//                        && !TextUtils.isEmpty(eventSession.getRequirements())) {
//                    binding.etRequirements.setText(eventSession.getRequirements());
//                }
//
//                if (eventSession.getEvent_session_logo() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_logo())) {
//                    binding.rlBtnAddEventLogo.setVisibility(View.GONE);
//                    binding.ivEventLogoClose.setVisibility(View.VISIBLE);
//                    Glide.with(getActivity()).load(eventSession.getEvent_session_logo()).into(binding.ivEventLogo);
//                } else {
//                    binding.rlBtnAddEventLogo.setVisibility(View.VISIBLE);
//                    binding.ivEventLogoClose.setVisibility(View.GONE);
//                }
//
//                if (eventSession.getEvent_session_led_by() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_led_by())) {
//                    binding.etEventLeadBy.setText(eventSession.getEvent_session_led_by());
//                }
//
//                if (eventSession.getEvent_session_leaders_bio() != null
//                        && !TextUtils.isEmpty(eventSession.getEvent_session_leaders_bio())) {
//                    binding.etEventLeaderBio.setText(eventSession.getEvent_session_leaders_bio());
//                }
//
//                if (!TextUtils.isEmpty(eventSession.getEvent_session_blob_image())) {
//                    try {
//                        ArrayList<String> imgs = new Gson().fromJson(eventSession.getEvent_session_blob_image(), new TypeToken<ArrayList<String>>() {
//                        }.getType());
//                        for (int i = 0; i < imgs.size(); i++) {
//                            eventImages.add(new EventImage(imgs.get(i)));
//                        }
//                        eventImageAdapter.notifyDataSetChanged();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//            }
//
//        } else {
//
//        }
//    }
//
//    @Override
//    protected void setListeners() {
//
//        binding.tvBtnEventLocationNext.setOnClickListener(v -> {
//            if (validate()) {
//                Fragment fragment = null;
//                if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_LOCATION) {
//
//                    if (eventSession.isFree_local_session() && !eventSession.isIs_parking_allowed()) {
//                        fragment = new SessionLocationAddEditFragment();
//                    } else {
//                        fragment = new SessionPricingLimitFragment();
//                    }
//
//                } else if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_VIRTUALLY) {
//                    if (eventSession.isFree_web_session()) {
//                        fragment = new SessionMediaAddEditFragment();
//                    } else {
//                        fragment = new SessionPricingLimitFragment();
//                    }
//                } else if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_BOTH) {
//                    if (eventSession.isFree_local_session() && eventSession.isFree_web_session()
//                            && eventSession.isFree_session_parking()) {
//                        fragment = new SessionMediaAddEditFragment();
//                    } else {
//                        if (eventSession.isFree_local_session() && eventSession.isFree_web_session()
//                                && !eventSession.isIs_parking_allowed()) {
//                            fragment = new SessionLocationAddEditFragment();
//                        } else {
//                            fragment = new SessionPricingLimitFragment();
//                        }
//                    }
//                }
//
//                final FragmentTransaction ft = getFragmentManager().beginTransaction();
//                Bundle bundle = new Bundle();
//                bundle.putBoolean("isEdit", isEdit);
//                bundle.putBoolean("isRepeat", isRepeat);
//                bundle.putString("eventSession", new Gson().toJson(eventSession));
//                fragment.setArguments(bundle);
//                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
//                ft.add(R.id.My_Container_1_ID, fragment);
//                fragmentStack.lastElement().onPause();
//                ft.hide(fragmentStack.lastElement());
//                fragmentStack.push(fragment);
//                ft.commitAllowingStateLoss();
//            }
//        });
//
//        binding.spEventType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (eventSession != null) {
//                    eventSession.setEvent_session_type(eventTypes.get(position).getEvent_type());
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//
//        binding.rlBtnAddEventLogo.setOnClickListener(v -> {
//            imageOption = "EventLogo";
//            Intent mIntent = new Intent(getActivity(), PickImageActivity.class);
//            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 1);
//            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
//            startActivityForResult(mIntent, REQUEST_PICK_IMAGE);
//        });
//
//        binding.ivEventLogoClose.setOnClickListener(v -> {
//            binding.ivEventLogo.setImageDrawable(null);
//            binding.rlBtnAddEventLogo.setVisibility(View.VISIBLE);
//            binding.ivEventLogoClose.setVisibility(View.GONE);
//        });
//
//    }
//
//    private boolean validate() {
//
//        if (!TextUtils.isEmpty(binding.etEventName.getText())) {
//            eventSession.setEvent_session_name(binding.etEventName.getText().toString());
//        } else {
//            binding.etEventName.setError("Required");
//            binding.etEventName.requestFocus();
//            return false;
//        }
//
//        if (!TextUtils.isEmpty(binding.etEventSortDesc.getText())) {
//            eventSession.setEvent_session_short_description(binding.etEventSortDesc.getText().toString());
//        } else {
//            binding.etEventSortDesc.setError("Required");
//            binding.etEventSortDesc.requestFocus();
//            return false;
//        }
//        if (!TextUtils.isEmpty(binding.etEventLongDecr.getText())) {
//            eventSession.setEvent_session_long_description(binding.etEventLongDecr.getText().toString());
//        } else {
//            binding.etEventLongDecr.setError("Required");
//            binding.etEventLongDecr.requestFocus();
//            return false;
//        }
//
//        if (!TextUtils.isEmpty(binding.etRequirements.getText())) {
//            eventSession.setRequirements(binding.etRequirements.getText().toString());
//        }
//
//        if (!TextUtils.isEmpty(binding.etEventLeadBy.getText())) {
//            eventSession.setEvent_session_led_by(binding.etEventLeadBy.getText().toString());
//        }
//
//        if (!TextUtils.isEmpty(binding.etEventLeaderBio.getText())) {
//            eventSession.setEvent_session_leaders_bio(binding.etEventLeaderBio.getText().toString());
//        }
//
//        if (binding.rlBtnAddEventLogo.getVisibility() == View.VISIBLE) {
//            Toast.makeText(getActivity(), "Please add Event Logo Image!", Toast.LENGTH_SHORT).show();
//            return false;
//        }
//
//        if (eventImages.size() < 2) {
//            Toast.makeText(getActivity(), "Please add Event Image!", Toast.LENGTH_SHORT).show();
//            return false;
//        }
//
//        eventSession.setEventImages(eventImages);
//
//        return true;
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == REQUEST_PICK_IMAGE && resultCode == RESULT_OK) {
//
//            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
//            if (pathList != null && !pathList.isEmpty()) {
//                if (imageOption.equals("EventLogo")) {
//                    isEventLogoChange = true;
//                    binding.rlBtnAddEventLogo.setVisibility(View.GONE);
//                    binding.ivEventLogoClose.setVisibility(View.VISIBLE);
//                    eventSession.setEvent_session_logo(String.valueOf(pathList.get(0)));
//                    Glide.with(getActivity()).load(eventSession.getEvent_session_logo()).into(binding.ivEventLogo);
//                } else if (imageOption.equals("EventImage")) {
//                    for (int i = 0; i < pathList.size(); i++) {
//                        eventImages.add(new EventImage(pathList.get(i)));
//                    }
//                    eventImageAdapter.notifyDataSetChanged();
//                }
//            }
//        }
//    }
//
//    @Override
//    protected void initViews(View v) {
//
//        eventTypeArrayAdapter = new ArrayAdapter<EventType>(getActivity(),
//                android.R.layout.simple_list_item_1, android.R.id.text1, eventTypes);
//        binding.spEventType.setAdapter(eventTypeArrayAdapter);
//
//        eventImageAdapter = new EventImageAdapter();
//        binding.rvImage.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
//        binding.rvImage.setAdapter(eventImageAdapter);
//
//        eventImages.add(new EventImage(""));
//        eventImageAdapter.notifyDataSetChanged();
//
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        if (SessionsFragment.isSessionUpdated && getActivity() != null) {
//            getActivity().onBackPressed();
//        }
//    }
//
//    private class EventImageAdapter extends RecyclerView.Adapter<EventImageAdapter.ImageHolder> {
//
//        @NonNull
//        @Override
//        public EventImageAdapter.ImageHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//            return new EventImageAdapter.ImageHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_add_event_image, viewGroup, false));
//        }
//
//        @Override
//        public void onBindViewHolder(@NonNull EventImageAdapter.ImageHolder imageHolder, int i) {
//            if (i == 0) {
//                imageHolder.rlBtnAddEventImage.setVisibility(View.VISIBLE);
//                imageHolder.ivEventImageClose.setVisibility(View.GONE);
//            } else {
//                imageHolder.rlBtnAddEventImage.setVisibility(View.GONE);
//                imageHolder.ivEventImageClose.setVisibility(View.VISIBLE);
//                Glide.with(getActivity()).load(eventImages.get(i).getPath()).into(imageHolder.ivEventImage);
//            }
//        }
//
//        @Override
//        public int getItemCount() {
//            return eventImages.size();
//        }
//
//        public class ImageHolder extends RecyclerView.ViewHolder {
//            ImageView ivEventImage, ivEventImageClose;
//            RelativeLayout rlBtnAddEventImage;
//
//            public ImageHolder(@NonNull View itemView) {
//                super(itemView);
//                ivEventImage = itemView.findViewById(R.id.ivEventImage);
//                ivEventImageClose = itemView.findViewById(R.id.ivEventImageClose);
//                rlBtnAddEventImage = itemView.findViewById(R.id.rlBtnAddEventImage);
//
//                rlBtnAddEventImage.setOnClickListener(v -> {
//                    imageOption = "EventImage";
//
//                    Intent mIntent = new Intent(getActivity(), PickImageActivity.class);
//                    mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 10);
//                    mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
//                    startActivityForResult(mIntent, REQUEST_PICK_IMAGE);
//                });
//
//                ivEventImageClose.setOnClickListener(v -> {
//                    if (getAdapterPosition() != 1) {
//                        eventImages.remove(getAdapterPosition());
//                        notifyItemRemoved(getAdapterPosition());
//                    }
//                });
//            }
//        }
//    }

    public FragSessionInfoAddEditBinding binding;
    private String imageOption = "";
    private static final int REQUEST_PICK_IMAGE = 985;
//    public static boolean isEventLogoChange = false, isEventImage1Change = false, isEventImage2Change = false;
    /*private ArrayList<EventType> eventTypes = new ArrayList<>();
    private ArrayAdapter<EventType> eventTypeArrayAdapter;*/

    private ArrayList<EventType> eventTypes = new ArrayList<>();
    private EventTypeAdapter eventTypeAdapter;

    private EventSession eventSession;
    private ArrayList<EventImage> eventImages = new ArrayList<>();
    private EventImageAdapter eventImageAdapter;
    private boolean isEdit = false, isRepeat = false;
    private PopupWindow popupmanaged;
    private int selectedCategory = 0, selectedType = 0;
    ProgressDialog dialog;
    private String TAG = "#DEBUG SessionInfoAddEdit";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            isEdit = getArguments().getBoolean("isEdit", false);
            isRepeat = getArguments().getBoolean("isRepeat", false);
            eventSession = new Gson().fromJson(getArguments().getString("eventSession"), EventSession.class);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_session_info_add_edit, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        initViews(view);
        updateViews();
        setListeners();

        new fetchEventTypes().execute();
    }

    private class fetchEventTypes extends AsyncTask<String, String, String> {
        JSONObject json;
        JSONArray json1;
        String eventDefUrl = "_table/event_type";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            eventTypes.clear();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + eventDefUrl;
            Log.e("#DEBUG", "      fetchEventType:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");

                    for (int j = 0; j < jsonArray.length(); j++) {
                        EventType eventType = new EventType();
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            eventType.setId(object.getInt("id"));
                        }
                        if (object.has("event_type")
                                && !TextUtils.isEmpty(object.getString("event_type"))
                                && !object.getString("event_type").equals("null")) {
                            eventType.setEvent_type(object.getString("event_type"));
                        }
                        if (object.has("icon_event_type_link")
                                && !TextUtils.isEmpty(object.getString("icon_event_type_link"))
                                && !object.getString("icon_event_type_link").equals("null")) {
                            eventType.setIcon_event_type_link(object.getString("icon_event_type_link"));
                        }

                        eventTypes.add(eventType);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                if (eventTypes.size() != 0) {
                    if (eventTypeAdapter != null)
                        eventTypeAdapter.notifyDataSetChanged();
                    if (isEdit) {
                        if (eventSession != null && eventSession.getEvent_session_type() != null && !TextUtils.isEmpty(eventSession.getEvent_session_type())) {
                            binding.etEventType.setText(eventSession.getEvent_session_type());
                            if (eventTypes.indexOf(new EventType(eventSession.getEvent_session_type())) != -1) {
                                selectedType = eventTypes.indexOf(new EventType(eventSession.getEvent_session_type()));
//                                binding.spEventType.setSelection(eventTypes.indexOf(new EventType(eventSession.getEvent_session_type())));
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    protected void updateViews() {
        if (isEdit) {

            if (eventSession != null) {

                if (eventSession.getEvent_session_name() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_name())) {
                    binding.etEventName.setText(eventSession.getEvent_session_name());
                }

                if (eventSession.getEvent_session_short_description() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_short_description())) {
                    binding.etEventSortDesc.setText(eventSession.getEvent_session_short_description());
                }

                if (eventSession.getEvent_session_long_description() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_long_description())) {
                    binding.etEventLongDecr.setText(eventSession.getEvent_session_long_description());
                }

                if (eventSession.getRequirements() != null
                        && !TextUtils.isEmpty(eventSession.getRequirements())) {
                    binding.etRequirements.setText(eventSession.getRequirements());
                }

                if (eventSession.getEvent_session_logo() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_logo())) {
                    binding.rlBtnAddEventLogo.setVisibility(View.GONE);
                    binding.ivEventLogoClose.setVisibility(View.VISIBLE);
                    Glide.with(getActivity()).load(eventSession.getEvent_session_logo()).into(binding.ivEventLogo);
                } else {
                    binding.rlBtnAddEventLogo.setVisibility(View.VISIBLE);
                    binding.ivEventLogoClose.setVisibility(View.GONE);
                }

                if (eventSession.getEvent_session_led_by() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_led_by())) {
                    binding.etEventLeadBy.setText(eventSession.getEvent_session_led_by());
                }

                if (eventSession.getEvent_session_leaders_bio() != null
                        && !TextUtils.isEmpty(eventSession.getEvent_session_leaders_bio())) {
                    binding.etEventLeaderBio.setText(eventSession.getEvent_session_leaders_bio());
                }

                if (!TextUtils.isEmpty(eventSession.getEvent_session_blob_image())) {
                    try {
                        ArrayList<String> imgs = new Gson().fromJson(eventSession.getEvent_session_blob_image(), new TypeToken<ArrayList<String>>() {
                        }.getType());
                        for (int i = 0; i < imgs.size(); i++) {
                            eventImages.add(new EventImage(imgs.get(i)));
                        }
                        eventImageAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

        } else {

        }
    }

    @Override
    protected void setListeners() {

        binding.tvBtnEventLocationNext.setOnClickListener(v -> {
            if (validate()) {
               /*   Fragment fragment = null;
                if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_LOCATION) {

                    if (eventSession.isFree_local_session() && !eventSession.isParking_allowed()) {
                        fragment = new SessionLocationAddEditFragment();
                    } else {
                        fragment = new SessionPricingLimitFragment();
                    }

                } else if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_AT_VIRTUALLY) {
                    if (eventSession.isFree_web_session()) {
                        fragment = new SessionMediaAddEditFragment();
                    } else {
                        fragment = new SessionPricingLimitFragment();
                    }
                } else if (eventSession.getEvent_session_logi_type() == EVENT_TYPE_BOTH) {
                    if (eventSession.isFree_local_session() && eventSession.isFree_web_session()
                            && eventSession.isFree_session_parking()) {
                        fragment = new SessionMediaAddEditFragment();
                    } else {
                        if (eventSession.isFree_local_session() && eventSession.isFree_web_session()
                                && !eventSession.isParking_allowed()) {
                            fragment = new SessionLocationAddEditFragment();
                        } else {
                            fragment = new SessionPricingLimitFragment();
                        }
                    }
                }*/
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment fragment = new SessionImageAddEditFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("isEdit", isEdit);
                bundle.putBoolean("isRepeat", isRepeat);
                bundle.putString("eventSession", new Gson().toJson(eventSession));
                fragment.setArguments(bundle);
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.add(R.id.My_Container_1_ID, fragment);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(fragment);
                ft.commitAllowingStateLoss();
            }
        });

        binding.spEventType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (eventSession != null) {
                    eventSession.setEvent_session_type(eventTypes.get(position).getEvent_type());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.rlBtnAddEventLogo.setOnClickListener(v -> {
            imageOption = "EventLogo";
            Intent mIntent = new Intent(getActivity(), PickImageActivity.class);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 1);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
            startActivityForResult(mIntent, REQUEST_PICK_IMAGE);
        });

        binding.ivEventLogoClose.setOnClickListener(v -> {
            binding.ivEventLogo.setImageDrawable(null);
            binding.rlBtnAddEventLogo.setVisibility(View.VISIBLE);
            binding.ivEventLogoClose.setVisibility(View.GONE);
        });

        binding.etEventName.setOnClickListener(v -> {
            binding.etEventName.setError(null);
            showEnter_txt("Event Name", "Enter Event Name (max 30 chars)", binding.etEventName, "etEventName");
        });
        binding.etEventType.setOnClickListener(v -> showAddEditEventTypePupup());
        binding.etEventSortDesc.setOnClickListener(v -> {
            binding.etEventSortDesc.setError(null);
            showEnter_txt("Event Short Description", "Enter Short Description", binding.etEventSortDesc, "etEventSortDesc");
        });
        binding.etEventLongDecr.setOnClickListener(v -> {
            binding.etEventLongDecr.setError(null);
            showEnter_txt("Event Long Description", "Enter Long Description", binding.etEventLongDecr, "etEventLongDecr");
        });
        binding.etEventLeadBy.setOnClickListener(v -> {
            showEnter_txt("Event Lead By", "Enter Name", binding.etEventLeadBy, "etEventLeadBy");
        });
        binding.etEventLeaderBio.setOnClickListener(v -> showEnter_txt("Event Leader Information", "Leader Info", binding.etEventLeaderBio, "etEventLeaderBio"));
        binding.etRequirements.setOnClickListener(v -> showEnter_txt("Event Requirements", "Enter Requirements", binding.etRequirements, "etRequirements"));
    }

    private boolean validate() {

        if (!TextUtils.isEmpty(binding.etEventName.getText())) {
            eventSession.setEvent_session_name(binding.etEventName.getText().toString());
        } else {
            binding.etEventName.setError("Required");
            binding.etEventName.requestFocus();
            return false;
        }

        if (!TextUtils.isEmpty(binding.etEventSortDesc.getText())) {
            eventSession.setEvent_session_short_description(binding.etEventSortDesc.getText().toString());
        } else {
            binding.etEventSortDesc.setError("Required");
            binding.etEventSortDesc.requestFocus();
            return false;
        }
        if (!TextUtils.isEmpty(binding.etEventLongDecr.getText())) {
            eventSession.setEvent_session_long_description(binding.etEventLongDecr.getText().toString());
        } else {
            binding.etEventLongDecr.setError("Required");
            binding.etEventLongDecr.requestFocus();
            return false;
        }

        if (!TextUtils.isEmpty(binding.etRequirements.getText())) {
            eventSession.setRequirements(binding.etRequirements.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLeadBy.getText())) {
            eventSession.setEvent_session_led_by(binding.etEventLeadBy.getText().toString());
        }

        if (!TextUtils.isEmpty(binding.etEventLeaderBio.getText())) {
            eventSession.setEvent_session_leaders_bio(binding.etEventLeaderBio.getText().toString());
        }

        /*if (binding.rlBtnAddEventLogo.getVisibility() == View.VISIBLE) {
            Toast.makeText(getActivity(), "Please add Event Logo Image!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (eventImages.size() < 2) {
            Toast.makeText(getActivity(), "Please add Event Image!", Toast.LENGTH_SHORT).show();
            return false;
        }

        eventSession.setEventImages(eventImages);*/

        return true;
    }

/*    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PICK_IMAGE && resultCode == RESULT_OK) {

            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                if (imageOption.equals("EventLogo")) {
                    isEventLogoChange = true;
                    binding.rlBtnAddEventLogo.setVisibility(View.GONE);
                    binding.ivEventLogoClose.setVisibility(View.VISIBLE);
                    eventSession.setEvent_session_logo(String.valueOf(pathList.get(0)));
                    Glide.with(getActivity()).load(eventSession.getEvent_session_logo()).into(binding.ivEventLogo);
                } else if (imageOption.equals("EventImage")) {
                    for (int i = 0; i < pathList.size(); i++) {
                        eventImages.add(new EventImage(pathList.get(i)));
                    }
                    eventImageAdapter.notifyDataSetChanged();
                }
            }
        }
    }*/

    @Override
    protected void initViews(View v) {
        /*eventTypeArrayAdapter = new ArrayAdapter<EventType>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, eventTypes);
        binding.spEventType.setAdapter(eventTypeArrayAdapter);*/
        eventImageAdapter = new EventImageAdapter();
        binding.rvImage.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        binding.rvImage.setAdapter(eventImageAdapter);

        eventImages.add(new EventImage(""));
        eventImageAdapter.notifyDataSetChanged();

        dialog = new ProgressDialog(getActivity());
        dialog.setTitle("Loading...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SessionsFragment.isSessionUpdated)
            if (getActivity() != null) getActivity().onBackPressed();
    }

    private void showEnter_txt(String title, String hintText, TextView etTextView, String selectedEditText) {
        RelativeLayout viewGroup = (RelativeLayout) getActivity().findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.enter_txt, viewGroup, false);
        final PopupWindow popup = new PopupWindow(getActivity());
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(true);
        popup.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        popup.setFocusable(true);
        popup.update();
        popup.showAtLocation(layout, Gravity.TOP, 0, 0);

        final EditText edit_text;
        final TextView text_title, tvPaste;
        final Button btn_done;
        RelativeLayout btn_cancel;
        LinearLayout rl_edit;
        ImageView ivPaste;

        tvPaste = (TextView) layout.findViewById(R.id.tvPaste);
        ivPaste = (ImageView) layout.findViewById(R.id.ivPaste);
        edit_text = (EditText) layout.findViewById(R.id.edit_nu);
        text_title = (TextView) layout.findViewById(R.id.txt_title);
        rl_edit = (LinearLayout) layout.findViewById(R.id.rl_edit);
        text_title.setText(title);
        edit_text.setHint(hintText);

        edit_text.setSelection(edit_text.getText().length());
        edit_text.setInputType(InputType.TYPE_CLASS_TEXT);
        if (!TextUtils.isEmpty(etTextView.getText()))
            edit_text.setText(etTextView.getText());
        edit_text.setFocusable(true);
        btn_cancel = (RelativeLayout) layout.findViewById(R.id.close);
        btn_done = (Button) layout.findViewById(R.id.btn_done);

        edit_text.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        edit_text.setOnClickListener(v -> {
//            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            tvPaste.setVisibility(View.GONE);
        });
        rl_edit.setOnClickListener(v -> tvPaste.setVisibility(View.GONE));
        edit_text.setOnLongClickListener(v -> {
            tvPaste.setVisibility(View.VISIBLE);
            return true;
        });
        tvPaste.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData pData = clipboard.getPrimaryClip();
            if (pData != null) {
                ClipData.Item item = pData.getItemAt(0);
                String txtpaste = item.getText().toString();
                edit_text.getText().insert(edit_text.getSelectionStart(), txtpaste);
                edit_text.setText(edit_text.getText()+txtpaste);
            }
        });
        ivPaste.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData pData = clipboard.getPrimaryClip();
            if (pData != null) {
                ClipData.Item item = pData.getItemAt(0);
                String txtpaste = item.getText().toString();
                edit_text.getText().insert(edit_text.getSelectionStart(), txtpaste);
                edit_text.setText(edit_text.getText()+txtpaste);
            }
        });

        btn_done.setOnClickListener(v -> {
                /*InputMethodManager imm1 = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm1.hideSoftInputFromWindow(edit_text.getWindowToken(), 0);*/
            etTextView.setText(edit_text.getText().toString());
            popup.dismiss();

            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            if (selectedEditText.equals("etEventName"))
                showAddEditEventTypePupup();
            if (selectedEditText.equals("etEventSortDesc"))
                binding.etEventLongDecr.performClick();
            if (selectedEditText.equals("etEventLongDecr"))
                binding.etRequirements.performClick();
            if (selectedEditText.equals("etRequirements"))
                binding.etEventLeadBy.performClick();
            if (selectedEditText.equals("etEventLeadBy"))
                binding.etEventLeaderBio.performClick();
        });
        btn_cancel.setOnClickListener(v -> {
            InputMethodManager imm1 = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm1.hideSoftInputFromWindow(edit_text.getWindowToken(), 0);
            popup.dismiss();
        });
    }

    private void showAddEditEventTypePupup() {
        LinearLayout viewGroup = (LinearLayout) getActivity().findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.popup_event_category, viewGroup, false);
        popupmanaged = new PopupWindow(getActivity());
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        //  popupmanaged.setAnimationStyle(R.style.animationName);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);
        RecyclerView rvMediaType;
        TextView txt_title;
        ImageView ivClose;

        TextView tvNext = (TextView) layout.findViewById(R.id.tvNext);
        txt_title = (TextView) layout.findViewById(R.id.tvTitle);
        rvMediaType = (RecyclerView) layout.findViewById(R.id.rvEventTypeList);
        ivClose = (ImageView) layout.findViewById(R.id.ivClose);

        txt_title.setText("Session Media Type");
        eventTypeAdapter = new EventTypeAdapter();
        rvMediaType.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        rvMediaType.setAdapter(eventTypeAdapter);
        rvMediaType.scrollToPosition(selectedType);
        tvNext.setOnClickListener(v -> {
            binding.etEventType.setText(eventTypes.get(selectedType).getEvent_type());
            if (eventSession != null)
                eventSession.setEvent_session_type(eventTypes.get(selectedType).getEvent_type());
            popupmanaged.dismiss();
            showEnter_txt("Session Short Description", "Enter Short Description", binding.etEventSortDesc, "etEventSortDesc");
        });
        ivClose.setOnClickListener(v -> popupmanaged.dismiss());
        //        popupmanaged.dismiss();
    }

    private class EventTypeAdapter extends RecyclerView.Adapter<EventTypeAdapter.EventTypeHolder> {
        @NonNull
        @Override
        public EventTypeAdapter.EventTypeHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new EventTypeAdapter.EventTypeHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_event_category, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull EventTypeAdapter.EventTypeHolder holder, int i) {
            holder.llItem.setBackgroundResource(selectedType == i ? R.drawable.highlight : 0);
            holder.tvCategoryTitle.setTextColor(ContextCompat.getColor(getContext(), selectedType == i ? R.color.itemType : R.color.grey_700));
            holder.tvCategoryTitle.setText(eventTypes.get(i).getEvent_type());
            Glide.with(getActivity()).load(eventTypes.get(i).getIcon_event_type_link()).into(holder.ivEventCategoryImage);
        }

        @Override
        public int getItemCount() {
            return eventTypes.size();
        }

        public class EventTypeHolder extends RecyclerView.ViewHolder {
            ImageView ivEventCategoryImage;
            TextView tvCategoryTitle;
            LinearLayout llItem;

            public EventTypeHolder(@NonNull View itemView) {
                super(itemView);
                ivEventCategoryImage = itemView.findViewById(R.id.ivEvent_category_image);
                tvCategoryTitle = itemView.findViewById(R.id.tvEventCategory);
                llItem = itemView.findViewById(R.id.ll_item);

                itemView.setOnClickListener(v -> {
                    llItem.setBackgroundResource(R.drawable.highlight);
                    selectedType = getBindingAdapterPosition();
                    notifyDataSetChanged();
                });
            }
        }
    }

    private class EventImageAdapter extends RecyclerView.Adapter<EventImageAdapter.ImageHolder> {

        @NonNull
        @Override
        public EventImageAdapter.ImageHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new EventImageAdapter.ImageHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_add_event_image, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull EventImageAdapter.ImageHolder imageHolder, int i) {
            if (i == 0) {
                imageHolder.rlBtnAddEventImage.setVisibility(View.VISIBLE);
                imageHolder.ivEventImageClose.setVisibility(View.GONE);
            } else {
                imageHolder.rlBtnAddEventImage.setVisibility(View.GONE);
                imageHolder.ivEventImageClose.setVisibility(View.VISIBLE);
                Glide.with(getActivity()).load(eventImages.get(i).getPath()).into(imageHolder.ivEventImage);
            }
        }

        @Override
        public int getItemCount() {
            return eventImages.size();
        }

        public class ImageHolder extends RecyclerView.ViewHolder {
            ImageView ivEventImage, ivEventImageClose;
            RelativeLayout rlBtnAddEventImage;

            public ImageHolder(@NonNull View itemView) {
                super(itemView);
                ivEventImage = itemView.findViewById(R.id.ivEventImage);
                ivEventImageClose = itemView.findViewById(R.id.ivEventImageClose);
                rlBtnAddEventImage = itemView.findViewById(R.id.rlBtnAddEventImage);

                rlBtnAddEventImage.setOnClickListener(v -> {
                    imageOption = "EventImage";

                    Intent mIntent = new Intent(getActivity(), PickImageActivity.class);
                    mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 10);
                    mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
                    startActivityForResult(mIntent, REQUEST_PICK_IMAGE);
                });

                ivEventImageClose.setOnClickListener(v -> {
                    if (getAdapterPosition() != 1) {
                        eventImages.remove(getAdapterPosition());
                        notifyItemRemoved(getAdapterPosition());
                    }
                });
            }
        }
    }
}
