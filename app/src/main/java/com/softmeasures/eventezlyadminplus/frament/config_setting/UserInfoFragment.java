package com.softmeasures.eventezlyadminplus.frament.config_setting;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragmentUserInfoBinding;
import com.softmeasures.eventezlyadminplus.frament.ConfigSettingsFragment;
import com.softmeasures.eventezlyadminplus.models.users.TownshipUser;
import com.softmeasures.eventezlyadminplus.models.users.UserProfile.UserProfileData;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class UserInfoFragment extends BaseFragment {
    FragmentUserInfoBinding binding;
    private ArrayList<UserProfileData> userList = new ArrayList<>();
    private ArrayList<UserProfileData> selectedUsers = new ArrayList<>();
    private ArrayList<TownshipUser> townshipUserList = new ArrayList<>();
    private UserAdapter userAdapter = null;
    private ProgressDialog progressDialog;
    SharedPreferences logindeatl;
    String name = "";
    int user_id = 0, userCount = 0;
    private String configType = "";
  //  private String Company_type ="";
    private boolean isSelectAll = false, isSelectAllUser = false, isSelectUser = false;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = Integer.parseInt(logindeatl.getString("id", "null"));
        name = logindeatl.getString("name", "null");
        configType = getArguments().getString("Config_type");
       // Company_type = getArguments().getString("Company_type");
        initViews(view);
        setListeners();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_info, container, false);
        return binding.getRoot();
    }

    @Override
    protected void updateViews() {
    }

    @Override
    protected void setListeners() {
        binding.swipeRefreshLayout.setOnRefreshListener(this::fetchUsers);

        binding.ivSelectAll.setOnClickListener(v -> {
            isSelectAll = !isSelectAll;
            binding.ivSelectAll.setImageResource(isSelectAll ? R.drawable.ic_check : R.drawable.ic_circle_border);
            for (UserProfileData user : userList)
                user.setSelect(isSelectAll);
            userCount = isSelectAll ? userList.size() : 0;
            binding.tvUserCount.setText(" " + userCount + "  User");
            if (isSelectAll) selectedUsers.addAll(userList);
            else selectedUsers = new ArrayList<>();
            userAdapter.notifyDataSetChanged();
        });
        binding.tvCancel.setOnClickListener(view -> {
            isSelectUser = false;
            isSelectAll = false;
            binding.tvUserCount.setVisibility(View.GONE);
            binding.tvUserCount.setVisibility(View.GONE);
            binding.tvCancel.setVisibility(View.GONE);
            binding.ivSelectAll.setVisibility(View.GONE);
            binding.fbNext.setVisibility(View.GONE);
            for (UserProfileData user : userList)
                user.setSelect(false);
            userAdapter.notifyDataSetChanged();
        });
        binding.fbNext.setOnClickListener(v -> {
            isSelectUser = false;
            isSelectAll = false;
            binding.tvUserCount.setVisibility(View.GONE);
            binding.tvUserCount.setVisibility(View.GONE);
            binding.tvCancel.setVisibility(View.GONE);
            binding.ivSelectAll.setVisibility(View.GONE);
            binding.fbNext.setVisibility(View.GONE);
            for (UserProfileData user : userList)
                user.setSelect(false);
            userAdapter.notifyDataSetChanged();
            if (userCount > 0) {
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment frag = null;
                if (configType.equals("Call Config"))
                    frag = new ConfigSettingsFragment();
                else if (configType.equals("Chat Config"))
                    frag = new MessagingSettingFragment();
                else if (configType.equals("Group Config"))
                    frag = new GroupSettingFragment();
                Bundle bundle = new Bundle();
                if (userCount == 1)
                    bundle.putString("selectedUser", new Gson().toJson(selectedUsers.get(selectedUsers.size() - 1)));
                else bundle.putString("selectedUsers", new Gson().toJson(selectedUsers));
                bundle.putBoolean("isMultiple", userCount != 1);
                bundle.putString("Config_type", configType);
                frag.setArguments(bundle);
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.add(R.id.My_Container_1_ID, frag);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(frag);
                ft.commitAllowingStateLoss();
            } else
                Snackbar.make(v, "Please Select Company", Snackbar.LENGTH_SHORT).show();
        });
    }

    @Override
    protected void initViews(View v) {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");

        new fetchTownshipUsers().execute();

        userAdapter = new UserAdapter();
        binding.rvUsers.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        binding.rvUsers.setAdapter(userAdapter);
    }

    private void fetchUsers() {
        new fetchTownshipUsers().execute();
    }

    private class fetchUsers extends AsyncTask<String, String, String> {
        JSONObject json;
        JSONArray json1;
        String userUrl = "_table/user_profile";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            binding.swipeRefreshLayout.setRefreshing(true);
            if (!userList.isEmpty())
                userList.clear();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + userUrl;
            Log.e("#DEBUG", "      fetchGroups:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");
                    for (int i = 0; i < townshipUserList.size(); i++) {
                        for (int j = 0; j < jsonArray.length(); j++) {
                            JSONObject object = jsonArray.getJSONObject(j);
                            if (object.has("user_id") && !TextUtils.isEmpty(object.getString("user_id")) && !object.getString("user_id").equals("null")
                                    && townshipUserList.get(i).getUserId() == object.getInt("user_id")) {

                                UserProfileData user = new UserProfileData();
                                user.setUser_id(object.getInt("user_id"));
                                user.setRole(townshipUserList.get(i).getProfileName());

                                if (object.has("id")
                                        && !TextUtils.isEmpty(object.getString("id"))
                                        && !object.getString("id").equals("null")) {
                                    user.setId(object.getInt("id"));
                                }
                                if (object.has("date_time")
                                        && !TextUtils.isEmpty(object.getString("date_time"))
                                        && !object.getString("date_time").equals("null")) {
                                    user.setDate_time(object.getString("date_time"));
                                }
                                if (object.has("fname")
                                        && !TextUtils.isEmpty(object.getString("fname"))
                                        && !object.getString("fname").equals("null")) {
                                    user.setFname(object.getString("fname"));
                                }
                                if (object.has("lname")
                                        && !TextUtils.isEmpty(object.getString("lname"))
                                        && !object.getString("lname").equals("null")) {
                                    user.setLname(object.getString("lname"));
                                }
                                if (object.has("email")
                                        && !TextUtils.isEmpty(object.getString("email"))
                                        && !object.getString("email").equals("null")) {
                                    user.setEmail(object.getString("email"));
                                }
                                if (object.has("mobile")
                                        && !TextUtils.isEmpty(object.getString("mobile"))
                                        && !object.getString("mobile").equals("null")) {
                                    user.setMobile(object.getString("mobile"));
                                }
                                if (object.has("lphone")
                                        && !TextUtils.isEmpty(object.getString("lphone"))
                                        && !object.getString("lphone").equals("null")) {
                                    user.setLphone(object.getString("lphone"));
                                }
                                if (object.has("address")
                                        && !TextUtils.isEmpty(object.getString("address"))
                                        && !object.getString("address").equals("null")) {
                                    user.setAddress(object.getString("address"));
                                }
                                if (object.has("city")
                                        && !TextUtils.isEmpty(object.getString("city"))
                                        && !object.getString("city").equals("null")) {
                                    user.setCity(object.getString("city"));
                                }
                                if (object.has("state")
                                        && !TextUtils.isEmpty(object.getString("state"))
                                        && !object.getString("state").equals("null")) {
                                    user.setState(object.getString("state"));
                                }
                                if (object.has("zip")
                                        && !TextUtils.isEmpty(object.getString("zip"))
                                        && !object.getString("zip").equals("null")) {
                                    user.setZip(object.getString("zip"));
                                }
                                if (object.has("country")
                                        && !TextUtils.isEmpty(object.getString("country"))
                                        && !object.getString("country").equals("null")) {
                                    user.setCountry(object.getString("country"));
                                }
                                if (object.has("full_address")
                                        && !TextUtils.isEmpty(object.getString("full_address"))
                                        && !object.getString("full_address").equals("null")) {
                                    user.setFull_address(object.getString("full_address"));
                                }
                                if (object.has("ad_proof")
                                        && !TextUtils.isEmpty(object.getString("ad_proof"))
                                        && !object.getString("ad_proof").equals("null")) {
                                    user.setAd_proof(object.getString("ad_proof"));
                                }
                                if (object.has("user_name")
                                        && !TextUtils.isEmpty(object.getString("user_name"))
                                        && !object.getString("user_name").equals("null")) {
                                    user.setUser_name(object.getString("user_name"));
                                }

                                if (object.has("gender")
                                        && !TextUtils.isEmpty(object.getString("gender"))
                                        && !object.getString("gender").equals("null")) {
                                    user.setGender(object.getString("gender"));
                                }
                                if (object.has("ip")
                                        && !TextUtils.isEmpty(object.getString("ip"))
                                        && !object.getString("ip").equals("null")) {
                                    user.setIp(object.getString("ip"));
                                }
                                if (object.has("township_code")
                                        && !TextUtils.isEmpty(object.getString("township_code"))
                                        && !object.getString("township_code").equals("null")) {
                                    user.setTownship_code(object.getString("township_code"));
                                }
                                if (object.has("firebase_token")
                                        && !TextUtils.isEmpty(object.getString("firebase_token"))
                                        && !object.getString("firebase_token").equals("null")) {
                                    user.setFirebase_token(object.getString("firebase_token"));
                                }
                                userList.add(user);
                                break;
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                binding.swipeRefreshLayout.setRefreshing(false);
                if (!userList.isEmpty()) {
                    binding.tvError.setVisibility(View.GONE);
                    userAdapter.notifyDataSetChanged();
                } else {
                    binding.tvError.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private class fetchTownshipUsers extends AsyncTask<String, String, String> {
        String userUrl = "_table/township_users";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            binding.swipeRefreshLayout.setRefreshing(true);
            if (!townshipUserList.isEmpty())
                townshipUserList.clear();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + userUrl;
            Log.e("#DEBUG", "      fetchTownshipUsers:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    townshipUserList = getTownshipUserInfo(responseStr);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                binding.swipeRefreshLayout.setRefreshing(false);
                if (!townshipUserList.isEmpty()) {
                    new fetchUsers().execute();
                } else {
                    binding.tvError.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    /*public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyUserViewHolder> {
        ArrayList<UserInfo> group = new ArrayList<>();

        @NonNull
        @Override
        public MyUserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyUserViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_users, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyUserViewHolder holder, int position) {
            holder.tvUserName.setText(userList.get(position).getFname() + " " + userList.get(position).getLname());
            holder.tvUserCode.setText(userList.get(position).getMobile());
            holder.tvUserEmail.setText(userList.get(position).getEmail());

        }

        @Override
        public int getItemCount() {
            return userList.size();
        }

        public class MyUserViewHolder extends RecyclerView.ViewHolder {
            TextView tvUserName, tvUserCode, tvUserEmail;

            public MyUserViewHolder(@NonNull View itemView) {
                super(itemView);
                tvUserName = itemView.findViewById(R.id.tvUserName);
                tvUserCode = itemView.findViewById(R.id.tvUserContact);
                tvUserEmail = itemView.findViewById(R.id.tvUserEmail);

                itemView.setOnClickListener(v -> {
                    Log.d("#DEBUG", "userProfileData: " + new Gson().toJson(userList.get(getAdapterPosition())));
                    final int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ConfigSettingsFragment frag = new ConfigSettingsFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("selectedUser", new Gson().toJson(userList.get(pos)));
                        frag.setArguments(bundle);
                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.add(R.id.My_Container_1_ID, frag);
                        fragmentStack.lastElement().onPause();
                        ft.hide(fragmentStack.lastElement());
                        fragmentStack.push(frag);
                        ft.commitAllowingStateLoss();
                    }
                });
            }
        }
    }*/
    public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyUserViewHolder> {
        @NonNull
        @Override
        public UserAdapter.MyUserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new UserAdapter.MyUserViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_users, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull UserAdapter.MyUserViewHolder holder, int position) {
            holder.tvUserId.setText("" + userList.get(position).getUser_id());
            if (isSelectUser)
                holder.ivSelectUser.setVisibility(userList.get(position).isSelect() ? View.VISIBLE : View.GONE);
            else holder.ivSelectUser.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(userList.get(position).getFname()) || !TextUtils.isEmpty(userList.get(position).getLname()))
                holder.tvUserName.setText(userList.get(position).getFname() + " " + userList.get(position).getLname());
            else holder.tvUserName.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(userList.get(position).getMobile()))
                holder.tvUserCode.setText(userList.get(position).getMobile());
            else holder.tvUserCode.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(userList.get(position).getEmail()))
                holder.tvUserEmail.setText(userList.get(position).getEmail());
            else holder.tvUserEmail.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(userList.get(position).getRole()))
                holder.tvUserRole.setText(userList.get(position).getRole());
            else holder.tvUserRole.setVisibility(View.GONE);
        }

        @Override
        public int getItemCount() {
            return userList.size();
        }

        public class MyUserViewHolder extends RecyclerView.ViewHolder {
            TextView tvUserId, tvUserName, tvUserCode, tvUserEmail, tvUserRole;
            ImageView ivSelectUser;

            public MyUserViewHolder(@NonNull View itemView) {
                super(itemView);
                tvUserId = itemView.findViewById(R.id.tvUserId);
                tvUserName = itemView.findViewById(R.id.tvUserName);
                tvUserCode = itemView.findViewById(R.id.tvUserContact);
                tvUserEmail = itemView.findViewById(R.id.tvUserEmail);
                tvUserRole = itemView.findViewById(R.id.tvUserRole);
                ivSelectUser = itemView.findViewById(R.id.ivSelectedUser);

                itemView.setOnClickListener(v -> {
                    Log.d("#DEBUG", "userProfileData: " + new Gson().toJson(userList.get(getAdapterPosition())));
                    final int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        if (isSelectUser) {
                            ivSelectUser.setVisibility(ivSelectUser.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                            if (ivSelectUser.getVisibility() == View.VISIBLE) {
                                userList.get(getBindingAdapterPosition()).setSelect(true);
                                selectedUsers.add(userList.get(getBindingAdapterPosition()));
                                userCount += 1;
                            } else {
                                userList.get(getBindingAdapterPosition()).setSelect(false);
                                selectedUsers.remove(userList.get(getBindingAdapterPosition()));
                                userCount -= 1;
                            }
                            binding.ivSelectAll.setImageResource(userCount == userList.size() ? R.drawable.ic_check : R.drawable.ic_circle_border);
                            binding.tvUserCount.setText(" " + userCount + "  User");
                        } else {
                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            Fragment frag = null;
                            if (configType.equals("Call Config"))
                                frag = new ConfigSettingsFragment();
                            else if (configType.equals("Chat Config"))
                                frag = new MessagingSettingFragment();
                            else if (configType.equals("Group Config"))
                                frag = new GroupSettingFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("selectedUser", new Gson().toJson(userList.get(pos)));
                            bundle.putString("Config_type", configType);
                            frag.setArguments(bundle);
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.add(R.id.My_Container_1_ID, frag);
                            fragmentStack.lastElement().onPause();
                            ft.hide(fragmentStack.lastElement());
                            fragmentStack.push(frag);
                            ft.commitAllowingStateLoss();
                        }
                    }
                });

                itemView.setOnLongClickListener(v -> {
                    isSelectUser = true;
                    userList.get(getBindingAdapterPosition()).setSelect(true);
                    ivSelectUser.setVisibility(View.VISIBLE);
                    selectedUsers=new ArrayList<>();
                    userCount =0;
                    selectedUsers.add(userList.get(getBindingAdapterPosition()));
                    userCount += 1;
                    binding.tvUserCount.setText(" " + userCount + "  User");

                    binding.ivSelectAll.setVisibility(View.VISIBLE);
                    binding.tvCancel.setVisibility(View.VISIBLE);
                    binding.tvUserCount.setVisibility(View.VISIBLE);
                    binding.fbNext.setVisibility(View.VISIBLE);
                    return true;
                });
            }
        }
    }
}