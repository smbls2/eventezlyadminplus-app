package com.softmeasures.eventezlyadminplus.frament;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.concurrent.NotThreadSafe;

public class mylocation extends Fragment {

    private static final String LOG_TAG = "ExampleApp";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyC6UabVHdti4zlU6E1iajVuNi6ZdKhDK5g";

    ProgressDialog pdialog;
    ArrayList<item> permitsarray = new ArrayList<>();
    ArrayList<item> tempary_data = new ArrayList<>();
    CustomAdaptercity adpater;
    CustomAdapteredit_location adpater_edit_location;
    CustomAdapterdelete_location adpater_delete_location;
    ListView listofvehicleno, list_edit_location, list_location_delete;
    TextView txttitle, txt_add, txt_show, txt_add_location, txt_search, txt_save, txt_edit, txt_delete, txt_selected_address_from_map;
    int i = 1;
    ConnectionDetector cd;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar, rl_location_list, rl_add, rl_edit, rl_delete;
    ImageView btn_delete, ing_icon;
    ListView list_location_search;
    EditText edit_location;
    String adrreslocation, addressti, addressin;
    String addrsstitle = "null", addresssub = "null", countyname, full_address, str_edit_id, str_location_name, str_lat, str_lang;
    String lat, lang;
    ArrayList<item> searcharray = new ArrayList<>();
    boolean clikondestionation = false;
    Double latitude = 0.0, longitude = 0.0;
    String location_code, loc;
    RelativeLayout rl_saved;
    ArrayList<item> searchplace = new ArrayList<>();
    AutoCompleteTextView autoCompView;
    boolean isfirsttimeload = false;
    String duration_unit[] = {"ANY ADDRESS", "TOWNSHIPS", "COMMERCIAL", "PRIVATE"};
    Spinner sp_select_location;
    String select_location, filterAddress;
    ArrayList<item> managedparkinglist = new ArrayList<>();
    String viewmap = "anyaddrees";
    GoogleMap googleMap;
    boolean managedmapisloaded = false, commercial_map_loaded = false, private_map_is_loaded = false;
    ArrayList<item> commercial_parking_rules = new ArrayList<>();
    ArrayList<item> googleparkingarray = new ArrayList<>();
    ArrayList<item> googleparkingarray1 = new ArrayList<>();
    ArrayList<item> googlepartnerarray = new ArrayList<>();
    ArrayList<item> parkingarray = new ArrayList<>();
    ArrayList<item> parkingarray1 = new ArrayList<>();
    double cenlat = 0.0, cenlang = 0.0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_mylocation, container, false);
        listofvehicleno = (ListView) view.findViewById(R.id.listofvehicle1);
        list_edit_location = (ListView) view.findViewById(R.id.list_edit_location);
        list_location_delete = (ListView) view.findViewById(R.id.list_location_delete);
        sp_select_location = (Spinner) view.findViewById(R.id.sp_select_location);
        txttitle = (TextView) view.findViewById(R.id.txttitte);
        txt_add = (TextView) view.findViewById(R.id.text_add);
        txt_show = (TextView) view.findViewById(R.id.text_show);
        txt_edit = (TextView) view.findViewById(R.id.text_edit);
        txt_delete = (TextView) view.findViewById(R.id.text_delete);
        txt_selected_address_from_map = (TextView) view.findViewById(R.id.txt_address_from_map);
        ing_icon = (ImageView) view.findViewById(R.id.img_icon);
        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Thin.ttf");
        txttitle.setTypeface(type);
        autoCompView = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteTextView);
        btn_delete = (ImageView) view.findViewById(R.id.btn_delete);
        cd = new ConnectionDetector(getActivity());
        final RelativeLayout ta = (RelativeLayout) view.findViewById(R.id.tabhost);
        RelativeLayout nothinglick = (RelativeLayout) view.findViewById(R.id.tt);
        rl_location_list = (RelativeLayout) view.findViewById(R.id.rl_location_list);
        rl_add = (RelativeLayout) view.findViewById(R.id.rl_main_add);
        rl_edit = (RelativeLayout) view.findViewById(R.id.rl_edit);
        rl_delete = (RelativeLayout) view.findViewById(R.id.rl_location_delete);
        txt_add_location = (TextView) view.findViewById(R.id.txt_location_addd);
        txt_search = (TextView) view.findViewById(R.id.txt_location_search);
        edit_location = (EditText) view.findViewById(R.id.edit_location_add);
        list_location_search = (ListView) view.findViewById(R.id.list_location_serach);
        txt_save = (TextView) view.findViewById(R.id.txt_location_save);
        rl_saved = (RelativeLayout) view.findViewById(R.id.rl_saved);
        autoCompView.setAdapter(new CustomAutoplace(getActivity(), getResources()));
        autoCompView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                addrsstitle = searchplace.get(position).getLoationname();
                addresssub = searchplace.get(position).getAddress();
                autoCompView.setText(addresssub);
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
        });
        ArrayAdapter<String> spinnerArrayAdapter111 = new ArrayAdapter<String>(getActivity(), R.layout.spinnerselect_location, R.id.txt_values, duration_unit);
        sp_select_location.setAdapter(spinnerArrayAdapter111);
        RelativeLayout main = (RelativeLayout) view.findViewById(R.id.main);
        nothinglick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        if (cd.isConnectingToInternet()) {
            currentlocation();
            getaddress1(latitude, longitude);
        }
        rl_location_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        txt_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isfirsttimeload = true;
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                rl_add.setVisibility(View.GONE);
                rl_location_list.setVisibility(View.VISIBLE);
                rl_edit.setVisibility(View.GONE);
                rl_delete.setVisibility(View.GONE);
                ta.setBackgroundResource(R.drawable.tabbarwithboder);
                txt_show.setBackgroundResource(R.drawable.tabbarwithselected);
                txt_show.setTextColor(getResources().getColor(R.color.selectedtabtext));
                txt_add.setBackgroundResource(R.drawable.tabbarwithunselected);
                txt_add.setTextColor(getResources().getColor(R.color.unselectedtext));
                txt_edit.setBackgroundResource(R.drawable.tabbarwithunselected);
                txt_edit.setTextColor(getResources().getColor(R.color.unselectedtext));
                txt_delete.setBackgroundResource(R.drawable.tabbarwithunselected);
                txt_delete.setTextColor(getResources().getColor(R.color.unselectedtext));

                for (int i = 0; i < permitsarray.size(); i++) {
                    permitsarray.get(i).setIsselect(false);
                    permitsarray.get(i).setCheckboxselected(false);
                    permitsarray.get(i).setIsclcik(false);
                }
                if (adpater != null) {
                    adpater.notifyDataSetChanged();
                }
            }
        });
        txt_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                rl_add.setVisibility(View.VISIBLE);
                rl_location_list.setVisibility(View.GONE);
                rl_edit.setVisibility(View.GONE);
                rl_delete.setVisibility(View.GONE);
                ta.setBackgroundResource(R.drawable.tabbarwithboder);
                txt_add.setBackgroundResource(R.drawable.tabbarwithselected);
                txt_add.setTextColor(getResources().getColor(R.color.selectedtabtext));
                txt_show.setBackgroundResource(R.drawable.tabbarwithunselected);
                txt_show.setTextColor(getResources().getColor(R.color.unselectedtext));
                txt_edit.setBackgroundResource(R.drawable.tabbarwithunselected);
                txt_edit.setTextColor(getResources().getColor(R.color.unselectedtext));
                txt_delete.setBackgroundResource(R.drawable.tabbarwithunselected);
                txt_delete.setTextColor(getResources().getColor(R.color.unselectedtext));
                showmap();
            }
        });

        txt_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isfirsttimeload = true;
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                rl_add.setVisibility(View.GONE);
                rl_location_list.setVisibility(View.GONE);
                rl_edit.setVisibility(View.VISIBLE);
                rl_delete.setVisibility(View.GONE);
                edit_location.setText("");
                ta.setBackgroundResource(R.drawable.tabbarwithboder);
                txt_edit.setBackgroundResource(R.drawable.tabbarwithselected);
                txt_edit.setTextColor(getResources().getColor(R.color.selectedtabtext));
                txt_show.setBackgroundResource(R.drawable.tabbarwithunselected);
                txt_show.setTextColor(getResources().getColor(R.color.unselectedtext));
                txt_add.setBackgroundResource(R.drawable.tabbarwithunselected);
                txt_add.setTextColor(getResources().getColor(R.color.unselectedtext));
                txt_delete.setBackgroundResource(R.drawable.tabbarwithunselected);
                txt_delete.setTextColor(getResources().getColor(R.color.unselectedtext));
                adpater_edit_location = new CustomAdapteredit_location(getActivity(), permitsarray, getResources());
                list_edit_location.setAdapter(adpater_edit_location);
            }
        });

        sp_select_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                select_location = (String) sp_select_location.getSelectedItem();

                if (select_location.equals("ANY ADDRESS")) {
                    ing_icon.setVisibility(View.VISIBLE);
                    viewmap = "anyaddrees";
                    if (googleMap != null) {
                        googleMap.clear();
                    }
                    showmap();
                } else if (select_location.equals("TOWNSHIPS")) {
                    ing_icon.setVisibility(View.GONE);
                    new getmanagedlist().execute();
                } else if (select_location.equals("COMMERCIAL")) {
                    cenlang = longitude;
                    cenlat = latitude;
                    ing_icon.setVisibility(View.GONE);
                    new getall_commercialrulesparking().execute();
                } else if (select_location.equals("PRIVATE")) {
                    cenlang = longitude;
                    cenlat = latitude;
                    ing_icon.setVisibility(View.GONE);
                    new getprivate(cenlat, cenlang).execute();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        listofvehicleno.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, final int totalItemCount) {
                int total = firstVisibleItem + visibleItemCount;
                int h = totalItemCount + 5;

                if (totalItemCount != tempary_data.size()) {
                    if (!isfirsttimeload) {
                        for (int i = totalItemCount; i < tempary_data.size(); i++) {
                            if (totalItemCount < h) {
                                h++;
                                permitsarray.add(tempary_data.get(i));
                                adpater.notifyDataSetChanged();
                                listofvehicleno.setAdapter(adpater);
                                listofvehicleno.setSelection(totalItemCount);
                                Log.e("data", "isloaded");
                            }

                        }
                    } else {
                        isfirsttimeload = false;
                    }
                }


            }
        });
        list_location_delete.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, final int totalItemCount) {
                int total = firstVisibleItem + visibleItemCount;
                int h = totalItemCount + 5;

                if (totalItemCount != tempary_data.size()) {
                    if (!isfirsttimeload) {
                        for (int i = totalItemCount; i < tempary_data.size(); i++) {
                            if (totalItemCount < h) {
                                h++;
                                permitsarray.add(tempary_data.get(i));
                                adpater_delete_location.notifyDataSetChanged();
                                list_location_delete.setAdapter(adpater_delete_location);
                                list_location_delete.setSelection(totalItemCount);
                                Log.e("data", "isloaded");
                            }

                        }
                    } else {
                        isfirsttimeload = false;
                    }
                }


            }
        });
        list_edit_location.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, final int totalItemCount) {
                int total = firstVisibleItem + visibleItemCount;
                int h = totalItemCount + 5;

                if (totalItemCount != tempary_data.size()) {
                    if (!isfirsttimeload) {
                        for (int i = totalItemCount; i < tempary_data.size(); i++) {
                            if (totalItemCount < h) {
                                h++;
                                permitsarray.add(tempary_data.get(i));
                                adpater_edit_location.notifyDataSetChanged();
                                list_edit_location.setAdapter(adpater_edit_location);
                                list_edit_location.setSelection(totalItemCount);
                                Log.e("data", "isloaded");
                            }

                        }
                    } else {
                        isfirsttimeload = false;
                    }
                }


            }
        });

        txt_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isfirsttimeload = true;
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                rl_add.setVisibility(View.GONE);
                rl_edit.setVisibility(View.GONE);
                rl_delete.setVisibility(View.VISIBLE);
                rl_location_list.setVisibility(View.GONE);
                ta.setBackgroundResource(R.drawable.tabbarwithboder);
                txt_delete.setBackgroundResource(R.drawable.tabbarwithselected);
                txt_delete.setTextColor(getResources().getColor(R.color.selectedtabtext));
                txt_show.setBackgroundResource(R.drawable.tabbarwithunselected);
                txt_show.setTextColor(getResources().getColor(R.color.unselectedtext));
                txt_add.setBackgroundResource(R.drawable.tabbarwithunselected);
                txt_add.setTextColor(getResources().getColor(R.color.unselectedtext));
                txt_edit.setBackgroundResource(R.drawable.tabbarwithunselected);
                txt_edit.setTextColor(getResources().getColor(R.color.unselectedtext));

                for (int i = 0; i < permitsarray.size(); i++) {
                    permitsarray.get(i).setIsselect(false);
                    permitsarray.get(i).setCheckboxselected(false);
                    permitsarray.get(i).setIsclcik(false);
                }
                if (permitsarray.size() > 0) {
                    if (adpater != null) {
                        adpater.notifyDataSetChanged();
                    }
                }
                adpater_delete_location = new CustomAdapterdelete_location(getActivity(), permitsarray, getResources());
                list_location_delete.setAdapter(adpater_delete_location);
            }
        });

        list_edit_location.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String address = permitsarray.get(position).getAddress();
                str_edit_id = permitsarray.get(position).getId();
                str_location_name = permitsarray.get(position).getLoationname();
                str_lang = permitsarray.get(position).getLang();
                str_lat = permitsarray.get(position).getLat();
                edit_location.setText(address);
            }
        });


        txt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                String location_address = edit_location.getText().toString();
                if (!location_address.equals("")) {
                    new edit_location(str_location_name, location_address, str_lat, str_lang, str_edit_id).execute();
                }
            }
        });

        txt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String location = edit_location.getText().toString();
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                if (location.length() < 0) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Please enter location name");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                } else {
                    new getaddressfromapi1(location).execute();
                }
            }
        });


        TextView txt_add_new = (TextView) view.findViewById(R.id.txt_save_location);

        txt_add_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String add = txt_selected_address_from_map.getText().toString();
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                if (!add.equals("")) {

                    new checklocation(add).execute();

                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete information");
                    alertDialog.setMessage("please select address to add in your saved locations.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
        });


        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < permitsarray.size(); i++) {
                    if (permitsarray.get(i).isCheckboxselected()) {
                        String id = permitsarray.get(i).getId();
                        new deletelocation(id).execute();
                    }
                }
            }
        });
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        if (cd.isConnectingToInternet()) {
            new getloc().execute();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }

        return view;
    }

    public static ArrayList<item> autocomplete(String input) {
        ArrayList<item> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            //sb.append("&components=country:gr");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
            resultList = new ArrayList<item>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                String jj = predsJsonArray.getJSONObject(i).getString("structured_formatting");
                JSONObject jjj = new JSONObject(jj);
                String main_address = jjj.getString("main_text");
                System.out.println("=========================" + main_address + "===================================");
                String cghcbdc = predsJsonArray.getJSONObject(i).getString("description");
                item ii = new item();
                ii.setLoationname(main_address);
                ii.setAddress(cghcbdc);
                resultList.add(ii);
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    public void currentlocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Failed to fetch user location!");
            alertDialog.setMessage("Please enable user location for app, location is required for app to work properly");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });

            alertDialog.show();
        }
    }

    public void ActionStartsHere() {
        againStartGPSAndSendFile();
    }

    public void againStartGPSAndSendFile() {
        new CountDownTimer(2000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                rl_saved.setVisibility(View.VISIBLE);
                // Display Data by Every Ten Second
            }

            @Override
            public void onFinish() {
                rl_saved.setVisibility(View.GONE);
            }

        }.start();
    }

    private void getaddress1(Double lat1, Double lon) {
        Geocoder gc = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = gc.getFromLocation(lat1, lon, 1);
            StringBuilder sb = new StringBuilder();
            StringBuilder addressl = new StringBuilder();
            StringBuilder country = new StringBuilder();
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getLatitude()).append("\n");
                    sb.append(address.getLongitude());
                    addrsstitle = address.getFeatureName();
                    addressl.append(address.getAddressLine(0)).append(", ");
                    addressl.append(address.getAddressLine(1));
                    country.append(address.getAddressLine(2));
                    location_code = addresses.get(0).getLocality();
                    break;
                }
                String locationAddress = sb.toString();
                loc = addressl.toString();
                addresssub = loc;
                lang = locationAddress.substring(locationAddress.indexOf('\n') + 1);
                lat = locationAddress.substring(0, locationAddress.indexOf('\n'));

                countyname = country.toString();
                autoCompView.setText(loc + "," + countyname);
                Resources rs;
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Address not found!");
                alertDialog.setMessage("Could not find location. Try including the exact location information");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showmap() {

        SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        fm.getMapAsync(googleMap1 -> {
            googleMap = googleMap1;
            if (googleMap != null) {
                if (viewmap.equals("anyaddrees")) {
                    MarkerOptions options = new MarkerOptions();
                    final LatLng position = new LatLng(latitude, longitude);
                    options.position(position);
                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                            position, 12);
                    googleMap.animateCamera(location);
                    googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                        @Override
                        public void onCameraChange(CameraPosition cameraPosition) {

                            if (viewmap.equals("anyaddrees")) {
                                LatLng center = googleMap.getCameraPosition().target;
                                LatLng center1 = googleMap.getCameraPosition().target;
                                cenlat = center.latitude;
                                cenlang = center.longitude;
                                System.out.println(cenlang);
                                System.out.println(cenlat);
                                addresssub = "";
                                Geocoder geoCoder = new Geocoder(getActivity().getBaseContext(), Locale.getDefault());
                                try {
                                    List<Address> addresses =
                                            geoCoder.getFromLocation(cenlat, cenlang, 1);
                                    if (addresses.size() > 0) {
                                        addresssub += addresses.get(0).getAddressLine(0) + " ";
                                        addrsstitle = addresses.get(0).getFeatureName();
                                        txt_selected_address_from_map.setText(addresssub);
                                    /*for (int i = 0; i < addresses.get(0).getMaxAddressLineIndex(); i++) {
                                        addresssub += addresses.get(0).getAddressLine(i) + " ";
                                        addrsstitle = addresses.get(0).getFeatureName();
                                        txt_selected_address_from_map.setText(addresssub);
                                    }*/
                                    }
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                } catch (Exception e2) {
                                    e2.printStackTrace();
                                }
                            }
                        }
                    });
                } else if (viewmap.equals("managed")) {
                    googleMap.clear();
                    MarkerOptions options = new MarkerOptions();
                    if (managedparkinglist.size() > 0) {

                        for (int i = 0; i < managedparkinglist.size(); i++) {
                            final LatLng position = new LatLng(Double.parseDouble(managedparkinglist.get(i).getLat()), Double.parseDouble(managedparkinglist.get(i).getLang()));

                            options = new MarkerOptions();
                            options.position(position);

                            if (managedparkinglist.get(i).getMarker().equals("ez-free")) {
                                options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.park_free_circle_pin));
                            } else if (managedparkinglist.get(i).getMarker().equals("ez-managed")) {
                                options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.park_managed_circle));
                            } else if (managedparkinglist.get(i).getMarker().equals("ez-paid")) {
                                options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.park_paid_circle_pin));
                            }
                            options.title(managedparkinglist.get(i).getTitle());
                            options.snippet(managedparkinglist.get(i).getAddress());
                            googleMap.addMarker(options);
                            View locationButton = ((View) fm.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                            RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                            rlp.setMargins(0, 0, 30, 20);

                        }

                        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {
                                if (viewmap.equals("managed")) {
                                    addresssub = marker.getSnippet();
                                    addrsstitle = marker.getTitle();
                                    cenlat = marker.getPosition().latitude;
                                    cenlang = marker.getPosition().longitude;
                                    txt_selected_address_from_map.setText(addresssub);
                                }
                                return true;
                            }
                        });

                    }
                    final LatLng position = new LatLng(latitude, longitude);
                    options.position(position);
                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                            position, 12);
                    googleMap.animateCamera(location);
                    googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                        @Override
                        public void onCameraChange(CameraPosition cameraPosition) {
                            LatLng position = googleMap.getCameraPosition().target;
                            LatLng center1 = googleMap.getCameraPosition().target;
                            // zoom = googleMap.getCameraPosition().zoom;
                            if (viewmap.equals("managed")) {
                                if (managedmapisloaded) {
                                    lat = String.valueOf(cameraPosition.target.latitude);
                                    lang = String.valueOf(cameraPosition.target.longitude);
                                    Log.e("api_is_start", "yes");
                                    new getparkingpin(lat, lang).execute();
                                }
                            }
                        }
                    });

                    googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {
                            if (viewmap.equals("managed")) {
                                managedmapisloaded = true;
                            }
                        }
                    });
                } else if (viewmap.equals("mapmove")) {
                    googleMap.clear();
                    MarkerOptions options = null;
                    if (managedparkinglist.size() > 0) {
                        try {


                            for (int i = 0; i < managedparkinglist.size(); i++) {
                                final LatLng position = new LatLng(Double.parseDouble(managedparkinglist.get(i).getLat()), Double.parseDouble(managedparkinglist.get(i).getLang()));
                                options = new MarkerOptions();
                                options.position(position);

                                if (managedparkinglist.get(i).getMarker().equals("ez-free")) {
                                    options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.park_free_circle_pin));
                                } else if (managedparkinglist.get(i).getMarker().equals("ez-managed")) {
                                    options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.park_managed_circle));
                                } else if (managedparkinglist.get(i).getMarker().equals("ez-paid")) {
                                    options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.park_paid_circle_pin));
                                }
                                options.title(managedparkinglist.get(i).getTitle());
                                options.snippet(managedparkinglist.get(i).getAddress());
                                // Adding Marker on the Google Map
                                googleMap.addMarker(options);
                                View locationButton = ((View) fm.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                                RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                                rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                                rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                                rlp.setMargins(0, 0, 30, 20);


                                googleMap.setTrafficEnabled(false);
                                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                    return;
                                }
                                googleMap.setMyLocationEnabled(true);
                                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                                    @Override
                                    public boolean onMarkerClick(Marker marker) {
                                        if (viewmap.equals("mapmove")) {
                                            addresssub = marker.getSnippet();
                                            addrsstitle = marker.getTitle();
                                            cenlat = marker.getPosition().latitude;
                                            cenlang = marker.getPosition().longitude;
                                            txt_selected_address_from_map.setText(addresssub);
                                        }
                                        return true;
                                    }
                                });

                            }
                        } catch (Exception e) {

                        }
                    }
                    googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                        @Override
                        public void onCameraChange(CameraPosition cameraPosition) {
                            if (viewmap.equals("mapmove")) {
                                if (managedmapisloaded) {
                                    lat = String.valueOf(cameraPosition.target.latitude);
                                    lang = String.valueOf(cameraPosition.target.longitude);
                                    Log.e("api_is_start", "yes");
                                    new getparkingpin(lat, lang).execute();
                                }
                            }
                        }
                    });

                    googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {
                            if (viewmap.equals("mapmove")) {
                                managedmapisloaded = true;
                            }
                        }
                    });
                } else if (viewmap.equals("commercial")) {
                    googleMap.clear();
                    MarkerOptions options = null;
                    options = new MarkerOptions();
                    for (int i = 0; i < googleparkingarray.size(); i++) {
                        LatLng position = new LatLng(Double.parseDouble(googleparkingarray.get(i).getGooglelat()), Double.parseDouble(googleparkingarray.get(i).getGooglelan()));
                        options.position(position);

                        View marker = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
                        TextView numTxt = (TextView) marker.findViewById(R.id.text);
                        ImageView img = (ImageView) marker.findViewById(R.id.img_other_pin);
                        numTxt.setText("$10");

                        String markesr = googleparkingarray.get(i).getMarker();
                        if (markesr.equals("google")) {
                            img.setBackgroundResource(R.mipmap.commercial_icon);
                            options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.commercial_icon));
                            options.title("commercial");
                            options.snippet(googleparkingarray.get(i).getPlace_id());
                            googleMap.setMyLocationEnabled(true);
                            googleMap.addMarker(options);
                            Log.e("google", "yes");
                        }


                        if (markesr.equals("partner")) {
                            img.setBackgroundResource(R.mipmap.partner_pin_empt);
                            numTxt.setVisibility(View.VISIBLE);
                            numTxt.setText("$" + googleparkingarray.get(i).getWeekday_12hr_price());
                            options.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), marker)));
                            options.title("commercial");
                            options.snippet(googleparkingarray.get(i).getPlace_id());
                            googleMap.setMyLocationEnabled(false);
                            googleMap.addMarker(options);
                            Log.e("google", "no");
                        }


                        View locationButton = ((View) fm.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                        rlp.setMargins(0, 0, 30, 20);
                    }
                    final LatLng position = new LatLng(latitude, longitude);
                    options.position(position);
                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                            position, 12);
                    googleMap.animateCamera(location);
                    googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                        @Override
                        public void onCameraChange(CameraPosition cameraPosition) {
                            if (commercial_map_loaded) {
                                if (viewmap.equals("commercial")) {
                                    cenlat = cameraPosition.target.latitude;
                                    cenlang = cameraPosition.target.longitude;
                                    System.out.print("location chamges");
                                    Log.e("api_call_yes", "yessss");
                                    new getotherparkinglist1(cenlat, cenlang).execute();


                                }
                            }
                        }
                    });
                    googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {
                            if (viewmap.equals("commercial")) {
                                Log.e("map_is_loaded1", "yes");
                                commercial_map_loaded = true;
                            }

                        }
                    });

                    googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            if (viewmap.equals("commercial")) {
                                String pid = marker.getSnippet();
                                new getplacedeatilCommercial(pid).execute();
                            }
                            return true;
                        }
                    });

                } else if (viewmap.equals("comm_move")) {
                    googleMap.clear();
                    MarkerOptions options = null;
                    options = new MarkerOptions();
                    for (int i = 0; i < googleparkingarray.size(); i++) {
                        LatLng position = new LatLng(Double.parseDouble(googleparkingarray.get(i).getGooglelat()), Double.parseDouble(googleparkingarray.get(i).getGooglelan()));
                        options.position(position);

                        View marker = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
                        TextView numTxt = (TextView) marker.findViewById(R.id.text);
                        ImageView img = (ImageView) marker.findViewById(R.id.img_other_pin);
                        numTxt.setText("$10");

                        String markesr = googleparkingarray.get(i).getMarker();
                        if (markesr.equals("google")) {
                            img.setBackgroundResource(R.mipmap.commercial_icon);
                            options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.commercial_icon));
                            options.title("commercial");
                            options.snippet(googleparkingarray.get(i).getPlace_id());
                            googleMap.setMyLocationEnabled(true);
                            googleMap.addMarker(options);
                            Log.e("google", "yes");
                        }


                        if (markesr.equals("partner")) {
                            img.setBackgroundResource(R.mipmap.partner_pin_empt);
                            numTxt.setVisibility(View.VISIBLE);
                            numTxt.setText("$" + googleparkingarray.get(i).getWeekday_12hr_price());
                            options.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), marker)));
                            options.title("commercial");
                            options.snippet(googleparkingarray.get(i).getPlace_id());
                            googleMap.setMyLocationEnabled(false);
                            googleMap.addMarker(options);
                            Log.e("google", "no");
                        }


                        View locationButton = ((View) fm.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                        rlp.setMargins(0, 0, 30, 20);
                    }
                    googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                        @Override
                        public void onCameraChange(CameraPosition cameraPosition) {
                            if (commercial_map_loaded) {
                                if (viewmap.equals("comm_move")) {
                                    cenlat = cameraPosition.target.latitude;
                                    cenlang = cameraPosition.target.longitude;
                                    System.out.print("location chamges");
                                    Log.e("api_call_yes", "yessss");
                                    new getotherparkinglist1(cenlat, cenlang).execute();


                                }
                            }
                        }
                    });
                    googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {
                            if (viewmap.equals("comm_move")) {
                                Log.e("map_is_loaded1", "yes");
                                commercial_map_loaded = true;
                            }

                        }
                    });

                    googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            if (viewmap.equals("comm_move")) {
                                cenlat = marker.getPosition().latitude;
                                cenlang = marker.getPosition().longitude;
                                String pid = marker.getSnippet();
                                new getplacedeatilCommercial(pid).execute();
                            }
                            return true;
                        }
                    });
                } else if (viewmap.equals("private")) {
                    googleMap.clear();
                    MarkerOptions options = null;
                    options = new MarkerOptions();
                    for (int i = 0; i < parkingarray.size(); i++) {
                        LatLng position = new LatLng(Double.parseDouble(parkingarray.get(i).getLat()), Double.parseDouble(parkingarray.get(i).getLang()));
                        options.position(position);
                        options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.parkwhiz_pin_icon));
                        options.title(parkingarray.get(i).getTitle());
                        options.snippet(parkingarray.get(i).getAddress());
                        googleMap.setMyLocationEnabled(true);
                        // Adding Marker on the Google Map
                        googleMap.addMarker(options);
                        View locationButton = ((View) fm.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                        rlp.setMargins(0, 0, 30, 200);
                    }
                    final LatLng position = new LatLng(latitude, longitude);
                    options.position(position);
                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                            position, 12);
                    googleMap.animateCamera(location);
                    googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                        @Override
                        public void onCameraChange(CameraPosition cameraPosition) {

                            if (private_map_is_loaded) {
                                if (viewmap.equals("private")) {
                                    cenlat = cameraPosition.target.latitude;
                                    cenlang = cameraPosition.target.longitude;
                                    System.out.print("location chamges");
                                    Log.e("api_call_yes", "yessss");
                                    new getprivate1(cenlat, cenlang).execute();


                                }
                            }
                        }
                    });
                    googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {
                            if (viewmap.equals("private")) {
                                Log.e("map_is_loaded1", "yes");
                                private_map_is_loaded = true;
                            }

                        }
                    });

                    googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            if (viewmap.equals("private")) {
                                addrsstitle = marker.getTitle();
                                addresssub = marker.getSnippet();
                                cenlat = marker.getPosition().latitude;
                                cenlang = marker.getPosition().longitude;
                                txt_selected_address_from_map.setText(addresssub);
                            }
                            return true;
                        }
                    });
                } else if (viewmap.equals("private_move")) {
                    googleMap.clear();
                    MarkerOptions options = null;
                    options = new MarkerOptions();
                    for (int i = 0; i < parkingarray.size(); i++) {
                        LatLng position = new LatLng(Double.parseDouble(parkingarray.get(i).getLat()), Double.parseDouble(parkingarray.get(i).getLang()));
                        options.position(position);
                        options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.parkwhiz_pin_icon));
                        options.title(parkingarray.get(i).getTitle());
                        options.snippet(parkingarray.get(i).getAddress());
                        googleMap.setMyLocationEnabled(true);
                        // Adding Marker on the Google Map
                        googleMap.addMarker(options);
                        View locationButton = ((View) fm.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                        rlp.setMargins(0, 0, 30, 200);
                    }

                    googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                        @Override
                        public void onCameraChange(CameraPosition cameraPosition) {
                            if (private_map_is_loaded) {
                                if (viewmap.equals("private_move")) {
                                    cenlat = cameraPosition.target.latitude;
                                    cenlang = cameraPosition.target.longitude;
                                    System.out.print("location chamges");
                                    Log.e("api_call_yes", "yessss");
                                    new getprivate1(cenlat, cenlang).execute();


                                }
                            }
                        }
                    });
                    googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {
                            if (viewmap.equals("private_move")) {
                                Log.e("map_is_loaded1", "yes");
                                private_map_is_loaded = true;
                            }

                        }
                    });

                    googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            if (viewmap.equals("private_move")) {
                                addrsstitle = marker.getTitle();
                                addresssub = marker.getSnippet();
                                cenlat = marker.getPosition().latitude;
                                cenlang = marker.getPosition().longitude;
                                txt_selected_address_from_map.setText(addresssub);
                            }
                            return true;
                        }
                    });
                }

            }
        });


    }

    public class CustomAutoplace extends BaseAdapter implements View.OnClickListener, Filterable {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAutoplace(Activity a, Resources resLocal) {
            activity = a;
            context = a;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.layout, null);
                holder = new ViewHolder();

                holder.txt_commerical_name = (TextView) vi.findViewById(R.id.txt_name);
                holder.txt_deatil = (TextView) vi.findViewById(R.id.address);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }
            if (data.size() <= 0) {


            } else {
                try {
                    tempValues = null;
                    tempValues = (item) data.get(position);
                    holder.txt_commerical_name.setText(tempValues.getLoationname());
                    holder.txt_deatil.setText(tempValues.getAddress());
                    int k = 0;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        data = autocomplete(constraint.toString());
                        filterResults.values = data;
                        filterResults.count = data.size();
                        searchplace.clear();
                        searchplace.addAll(data);
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }

        public class ViewHolder {

            public TextView txt_commerical_name, txt_deatil;

        }
    }

    public class checklocation extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        String street, address, currentlocation, endlocation;
        JSONArray array;
        String tcode = logindeatl.getString("tcode", "null"), username = logindeatl.getString("name", "null");

        public checklocation(String street) {
            this.street = street;

        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            HttpResponse response = null;
            try {
                String add = "null";
                if (!addresssub.equals("") || !addresssub.equals("null")) {
                    add = addresssub.replaceAll("(\n)", "%0A");
                    add = add.replaceAll(" ", "%20");
                } else {
                    add = "null";
                }

                String street1 = "null";
                if (!addrsstitle.equals("") || !addrsstitle.equals("null")) {

                    street1 = addrsstitle.replaceAll("(\n)", "%0A");
                    street1 = street1.replaceAll(" ", "%20");
                } else {
                    street1 = "null";
                }

                String vechiclenourl = "_table/user_locations?filter=(location_name%3D" + street1 + ")%20AND%20(location_address%3D" + add + ")%20AND%20(user_id%3D" + id + ")";
                Log.e("checklocation", vechiclenourl);
                String url = getString(R.string.api) + getString(R.string.povlive) + vechiclenourl;
                DefaultHttpClient client = new DefaultHttpClient();
                HttpGet post = new HttpGet(url);
                post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
                post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {


            if (getActivity() != null && json != null) {
                if (array != null) {
                    if (array.length() > 0) {
                        rl_progressbar.setVisibility(View.GONE);
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Duplicate Location");
                        alertDialog.setMessage("Address already exist in your saved locations.");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alertDialog.show();

                    } else {

                        new addlocation().execute();
                    }
                } else {
                    new addlocation().execute();
                }

            } else {
                rl_progressbar.setVisibility(View.GONE);
            }
            super.onPostExecute(s);

        }


    }

    public class addlocation extends AsyncTask<String, String, String> {
        String add;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        JSONObject json, json1;
        String pov2 = "_table/user_locations";
        String id = logindeatl.getString("id", "null");

        public addlocation(String add) {
            this.add = add;
        }

        public addlocation() {
        }

        @Override
        protected void onPreExecute() {


            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("location_name", addrsstitle);
            jsonValues.put("location_address", addresssub);
            jsonValues.put("lat", cenlat);
            jsonValues.put("user_id", id);
            jsonValues.put("lng", cenlang);
            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + pov2;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            if (getActivity() != null && json1 != null) {
                if (!id.equals("null")) {
                    ActionStartsHere();
                    if (adpater != null) {
                        adpater.data.clear();
                        adpater.notifyDataSetChanged();
                        adpater_edit_location.data.clear();
                        adpater_edit_location.notifyDataSetChanged();
                        adpater_delete_location.data.clear();
                        adpater_delete_location.notifyDataSetChanged();
                    }
                    new getloc().execute();
                }
            }
            super.onPostExecute(s);

        }
    }

    public class edit_location extends AsyncTask<String, String, String> {
        String location_name, location_address, lat, lang, location_id;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        JSONObject json, json1;
        String pov2 = "_table/user_locations";
        String username = logindeatl.getString("name", "null");
        String id = logindeatl.getString("id", "null");
        String tcode = logindeatl.getString("tcode", "null");

        public edit_location(String location_name, String location_address, String lat, String lang, String id) {
            this.location_name = location_name;
            this.location_address = location_address;
            this.lat = lat;
            this.lang = lang;
            this.location_id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("location_name", location_name);
            location_address = location_address.replaceFirst(" ", "");
            jsonValues.put("location_address", location_address);
            jsonValues.put("lat", lat);
            jsonValues.put("user_id", id);
            jsonValues.put("lng", lang);
            jsonValues.put("id", location_id);

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + pov2;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            if (getActivity() != null && json1 != null) {
                if (!id.equals("null")) {
                    ActionStartsHere();
                    if (adpater != null) {
                        adpater.data.clear();
                        adpater.notifyDataSetChanged();
                        adpater_edit_location.data.clear();
                        adpater_edit_location.notifyDataSetChanged();
                        adpater_delete_location.data.clear();
                        adpater_delete_location.notifyDataSetChanged();
                    }
                    new getloc().execute();
                }
            }
            super.onPostExecute(s);
        }
    }

    public class CustomAdaptercity1 extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdaptercity1(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;

            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.destinationad, null);
                holder = new ViewHolder();

                holder.txt_address = (TextView) vi.findViewById(R.id.text_address);
                holder.txt_address_title = (TextView) vi.findViewById(R.id.text_address_title);

                /************  Set holder with LayoutInflater ************/
                vi.setTag(holder);

                vi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String address = holder.txt_address_title.getText().toString();


                        if (v != null) {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                        clikondestionation = true;
                        addrsstitle = address;
                        String address1 = holder.txt_address.getText().toString();
                        addresssub = address1;
                        list_location_search.setVisibility(View.GONE);

                    }
                });


            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }
            if (data.size() <= 0) {
            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                holder.txt_address_title.setText(tempValues.getTitle_address());
                String d = tempValues.getTitle_address();
                if (d.equals("Search Destination")) {
                    holder.txt_address.setText("");
                } else {
                    holder.txt_address.setText(tempValues.getAddress());
                }

            }

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txt_address_title, txt_address;
        }
    }

    public class getloc extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String loginid = logindeatl.getString("id", "null");
        String locationurl = "_table/user_locations?filter=user_id%3D" + loginid + "";

        String gf = "null";
        JSONObject json, json1;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            permitsarray.clear();
            tempary_data.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + locationurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        gf = c.getString("id");
                        String locationname = c.getString("location_name");
                        String locatonaddress = c.getString("location_address");
                        String id = c.getString("id");
                        item.setLoationname(locationname);
                        item.setAddress(locatonaddress);
                        item.setLat(c.getString("lat"));
                        item.setLang(c.getString("lng"));
                        item.setIsselect(false);
                        item.setCheckboxselected(false);
                        item.setId(id);
                        item.setIsclcik(false);
                        tempary_data.add(item);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity != null) {
                    try {
                        for (int i = 0; i < tempary_data.size(); i++) {
                            if (i < 10) {
                                permitsarray.add(tempary_data.get(i));
                            }
                        }
                        adpater = new CustomAdaptercity(getActivity(), permitsarray, rs = getResources());
                        adpater_edit_location = new CustomAdapteredit_location(getActivity(), permitsarray, getResources());
                        adpater_delete_location = new CustomAdapterdelete_location(getActivity(), permitsarray, getResources());
                        list_edit_location.setAdapter(adpater_edit_location);
                        listofvehicleno.setAdapter(adpater);
                        list_location_delete.setAdapter(adpater_delete_location);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class deletelocation extends AsyncTask<String, String, String> {
        String locationurl;
        String id;
        String gf = "null";
        JSONObject json, json1;
        JSONArray array = null;

        public deletelocation(String id) {
            locationurl = "_table/user_locations";
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", id);
            JSONObject json1 = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + locationurl;
            Log.e("delete_uel", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpDelete post = new HttpDelete(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));


            try {
                HttpEntity entity = new StringEntity(json1.toString());
                HttpClient httpClient = new DefaultHttpClient();
                HttpDeleteWithBody httpDeleteWithBody = new HttpDeleteWithBody(url);
                httpDeleteWithBody.setHeader("X-DreamFactory-Application-Name", "parkezly");
                httpDeleteWithBody.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
                httpDeleteWithBody.setEntity(entity);

                HttpResponse response = httpClient.execute(httpDeleteWithBody);
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        gf = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json != null) {
                if (!gf.equals("null")) {
                    if (adpater != null) {
                        adpater.data.clear();
                        adpater.notifyDataSetChanged();
                        adpater_edit_location.data.clear();
                        adpater_edit_location.notifyDataSetChanged();
                        adpater_delete_location.data.clear();
                        adpater_delete_location.notifyDataSetChanged();
                    }
                    new getloc().execute();
                }
            }
            super.onPostExecute(s);
        }
    }

    @NotThreadSafe
    class HttpDeleteWithBody extends HttpEntityEnclosingRequestBase {
        public static final String METHOD_NAME = "DELETE";

        public HttpDeleteWithBody(final String uri) {
            super();
            setURI(URI.create(uri));
        }

        public HttpDeleteWithBody(final URI uri) {
            super();
            setURI(uri);
        }

        public HttpDeleteWithBody() {
            super();
        }

        public String getMethod() {
            return METHOD_NAME;
        }
    }

    public class CustomAdaptercity extends BaseAdapter {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        item _state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdaptercity(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {
                vi = inflater.inflate(R.layout.mylocation, null);
                holder = new ViewHolder();
                holder.txtlocationame = (TextView) vi.findViewById(R.id.txtmylocationname);
                holder.txtlocationaddress = (TextView) vi.findViewById(R.id.txtmylocationaddress);
                holder.txtlocationtitle = (TextView) vi.findViewById(R.id.txtwalletdate);
                holder.rldata = (RelativeLayout) vi.findViewById(R.id.rl_maiun);
                holder.checkBox = (CheckBox) vi.findViewById(R.id.chk_id);
                holder.txt_id = (TextView) vi.findViewById(R.id.txt_id);
                holder.img_delete = (ImageView) vi.findViewById(R.id.img_delete);
                vi.setTag(holder);


                holder.img_delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String id = holder.txt_id.getText().toString();
                        new deletelocation(id).execute();
                    }
                });
            } else {
                holder = (ViewHolder) vi.getTag();
            }
            if (data.size() <= 0) {
                holder.txtlocationaddress.setText("No Data");
                holder.rldata.setVisibility(View.GONE);

            } else {
                tempValues = null;
                tempValues = (item) permitsarray.get(position);
                int k = 0;
                String cat = tempValues.getAddress();
                if (cat == null) {
                    holder.txtlocationaddress.setVisibility(View.GONE);
                } else if (cat != null) {
                    Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvetica-neue-medium.ttf");
                    Typeface light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Light.otf");
                    holder.txtlocationame.setTypeface(medium);
                    holder.txtlocationaddress.setTypeface(light);
                    holder.txtlocationtitle.setTypeface(medium);
                    String locationname1 = tempValues.getLoationname().toString();
                    Log.d("location", locationname1);
                    holder.checkBox.setTag(state);
                    if (tempValues.getLoationname().equals("null")) {
                        holder.txtlocationame.setText("");
                    } else {
                        holder.txtlocationame.setText(tempValues.getLoationname());
                    }
                    holder.txtlocationaddress.setText(tempValues.getAddress());

                }
            }
            return vi;
        }

        public class ViewHolder {
            public TextView txtlocationame, txtlocationaddress, txtlocationtitle, txt_id;
            ImageView img_delete;
            RelativeLayout rldata;
            CheckBox checkBox;
        }
    }

    public class CustomAdapteredit_location extends BaseAdapter {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        item _state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdapteredit_location(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {
                vi = inflater.inflate(R.layout.mylocation, null);
                holder = new ViewHolder();
                holder.txtlocationame = (TextView) vi.findViewById(R.id.txtmylocationname);
                holder.txtlocationaddress = (TextView) vi.findViewById(R.id.txtmylocationaddress);
                holder.txtlocationtitle = (TextView) vi.findViewById(R.id.txtwalletdate);
                holder.rldata = (RelativeLayout) vi.findViewById(R.id.rl_maiun);
                holder.checkBox = (CheckBox) vi.findViewById(R.id.chk_id);
                holder.txt_id = (TextView) vi.findViewById(R.id.txt_id);
                holder.img_delete = (ImageView) vi.findViewById(R.id.img_delete);
                vi.setTag(holder);

            } else {
                holder = (ViewHolder) vi.getTag();
            }
            if (data.size() <= 0) {
                holder.txtlocationaddress.setText("No Data");
                holder.rldata.setVisibility(View.GONE);

            } else {
                tempValues = null;
                tempValues = (item) permitsarray.get(position);
                int k = 0;
                String cat = tempValues.getAddress();
                if (cat == null) {
                    holder.txtlocationaddress.setVisibility(View.GONE);
                } else if (cat != null) {
                    Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvetica-neue-medium.ttf");
                    Typeface light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Light.otf");
                    holder.txtlocationame.setTypeface(medium);
                    holder.txtlocationaddress.setTypeface(light);
                    holder.txtlocationtitle.setTypeface(medium);
                    String locationname1 = tempValues.getLoationname().toString();
                    Log.d("location", locationname1);
                    holder.checkBox.setTag(state);
                    if (tempValues.getLoationname().equals("null")) {
                        holder.txtlocationame.setText("");
                    } else {
                        holder.txtlocationame.setText(tempValues.getLoationname());
                    }
                    holder.txtlocationaddress.setText(tempValues.getAddress());


                    holder.txt_id.setText(tempValues.getId());

                }
            }
            return vi;
        }

        public class ViewHolder {
            public TextView txtlocationame, txtlocationaddress, txtlocationtitle, txt_id;
            ImageView img_delete;
            RelativeLayout rldata;
            CheckBox checkBox;
        }
    }

    public class CustomAdapterdelete_location extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        item _state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdapterdelete_location(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public void onClick(View v) {

        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {
                vi = inflater.inflate(R.layout.mylocation, null);
                holder = new ViewHolder();
                holder.txtlocationame = (TextView) vi.findViewById(R.id.txtmylocationname);
                holder.txtlocationaddress = (TextView) vi.findViewById(R.id.txtmylocationaddress);
                holder.txtlocationtitle = (TextView) vi.findViewById(R.id.txtwalletdate);
                holder.rldata = (RelativeLayout) vi.findViewById(R.id.rl_maiun);
                holder.checkBox = (CheckBox) vi.findViewById(R.id.chk_id);
                holder.txt_id = (TextView) vi.findViewById(R.id.txt_id);
                holder.img_delete = (ImageView) vi.findViewById(R.id.img_delete);
                vi.setTag(holder);
                list_location_delete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Log.e("click", "yes");
                        for (int i = 0; i < permitsarray.size(); i++) {
                            if (i != position) {
                                permitsarray.get(i).setIsclcik(false);
                            }
                        }
                        permitsarray.get(position).setIsclcik(true);
                        notifyDataSetChanged();
                    }
                });

                holder.img_delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String id = holder.txt_id.getText().toString();
                        new deletelocation(id).execute();
                    }
                });
            } else {
                holder = (ViewHolder) vi.getTag();
            }
            if (data.size() <= 0) {
                holder.txtlocationaddress.setText("No Data");
                holder.rldata.setVisibility(View.GONE);

            } else {
                tempValues = null;
                tempValues = (item) permitsarray.get(position);
                int k = 0;
                String cat = tempValues.getAddress();
                if (cat == null) {
                    holder.txtlocationaddress.setVisibility(View.GONE);
                } else if (cat != null) {
                    Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/helvetica-neue-medium.ttf");
                    Typeface light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Light.otf");
                    holder.txtlocationame.setTypeface(medium);
                    holder.txtlocationaddress.setTypeface(light);
                    holder.txtlocationtitle.setTypeface(medium);
                    String locationname1 = tempValues.getLoationname().toString();
                    Log.d("location", locationname1);
                    holder.checkBox.setTag(state);
                    if (tempValues.getLoationname().equals("null")) {
                        holder.txtlocationame.setText("");
                    } else {
                        holder.txtlocationame.setText(tempValues.getLoationname());
                    }
                    holder.txtlocationaddress.setText(tempValues.getAddress());

                    holder.txt_id.setText(tempValues.getId());
                    if (tempValues.isclcik()) {
                        holder.img_delete.setVisibility(View.VISIBLE);
                    } else {
                        holder.img_delete.setVisibility(View.GONE);
                    }
                }
            }
            return vi;
        }

        public class ViewHolder {
            public TextView txtlocationame, txtlocationaddress, txtlocationtitle, txt_id;
            ImageView img_delete;
            RelativeLayout rldata;
            CheckBox checkBox;
        }
    }

    public class getaddressfromapi1 extends AsyncTask<Void, Void, StringBuilder> {
        String place;
        String end;

        public getaddressfromapi1(String end) {
            super();
            this.end = end;

        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String add = "null";
                if (!end.equals("") || !end.equals("null")) {
                    add = URLEncoder.encode(end);
                } else {
                    add = "null";
                }
                String googleMapUrl = "http://maps.googleapis.com/maps/api/geocode/json?address=" + add + "&sensor=false";

                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            try {
                if (result != null) {
                    JSONObject jsonObj = new JSONObject(result.toString());
                    String status = jsonObj.getString("status");

                    if (status.equals("OK")) {
                        rl_progressbar.setVisibility(View.GONE);
                        JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                        JSONObject before_geometry_jsonObj = resultJsonArray
                                .getJSONObject(0);
                        String addgtr = before_geometry_jsonObj.getString("formatted_address");
                        JSONObject geometry_jsonObj = before_geometry_jsonObj
                                .getJSONObject("geometry");
                        JSONObject location_jsonObj = geometry_jsonObj.getJSONObject("location");
                        Resources rs = getResources();
                        addressti = addgtr.substring(0, addgtr.indexOf(","));
                        addressin = addgtr.substring(addgtr.indexOf(",") + 1);
                        addressti = addressti.replace(" ", "");
                        addressin = addressin.replace(" ", "");
                        searcharray.clear();
                        item iii = new item();
                        iii.setTitle_address(addressti);
                        iii.setAddress(addressin);
                        searcharray.add(iii);
                        list_location_search.setVisibility(View.VISIBLE);
                        CustomAdaptercity1 adpater = new CustomAdaptercity1(getActivity(), searcharray, rs = getResources());
                        list_location_search.setAdapter(adpater);
                    } else {
                        rl_progressbar.setVisibility(View.GONE);
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Incomplete Addresses");
                        alertDialog.setMessage("Please select Destination address to proceed");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }
                } else {
                    rl_progressbar.setVisibility(View.GONE);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
            }
        }
    }

    //towmship part///////////
    public class getmanagedlist extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String managedlosturl = "_proc/find_parking_nearby";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            managedparkinglist.clear();
            JSONObject inlat = new JSONObject();
            JSONObject inlang = new JSONObject();

            try {
                inlat.put("name", "in_lat");
                inlat.put("value", latitude);
                // inlat.put("value", "40.7333");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
                inlang.put("name", "in_lng");
                inlang.put("value", longitude);
                //inlang.put("value", "-73.445");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            JSONArray jsonArray = new JSONArray();

            jsonArray.put(inlat);
            jsonArray.put(inlang);

            JSONObject studentsObj = new JSONObject();
            try {
                studentsObj.put("params", jsonArray);
                Log.e("managed_params", String.valueOf(studentsObj));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("find_parking_nearby", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(studentsObj.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONArray(responseStr);
                    for (int i = 0; i < json1.length(); i++) {
                        item item = new item();
                        JSONObject josnget = json1.getJSONObject(i);
                        String lat = josnget.getString("lat");
                        String lag = josnget.getString("lng");
                        String id = josnget.getString("id");
                        String title = josnget.getString("title");
                        String address = josnget.getString("address");
                        String url1 = josnget.getString("url");
                        String html = josnget.getString("html");
                        String category = josnget.getString("category");
                        String marker = josnget.getString("marker");
                        String location_code = josnget.getString("location_code");
                        String distancea = josnget.getString("distancea");
                        item.setId(id);
                        item.setLang(lag);
                        item.setLat(lat);
                        item.setTitle(title);
                        item.setAddress(address);
                        item.setUrl(url1);
                        item.setHtml(html);
                        item.setCategory(category);
                        item.setMarker(marker);
                        item.setLocation_code(location_code);
                        item.setDistancea(distancea);
                        managedparkinglist.add(item);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json1 != null) {
                if (getActivity() != null) {
                    if (managedparkinglist.size() > 0) {
                        managedmapisloaded = false;
                        if (googleMap != null) {
                            googleMap.clear();
                        }
                        viewmap = "managed";
                        showmap();
                    } else {
                        if (googleMap != null) {
                            googleMap.clear();
                        }
                        viewmap = "managed";
                        showmap();
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class getparkingpin extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String managedlosturl = "_proc/find_parking_nearby";
        String lati = "0.0", longi = "0.0";

        public getparkingpin(String lat, String lon) {
            this.lati = lat;
            this.longi = lon;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            managedparkinglist.clear();

            JSONObject inlat = new JSONObject();
            JSONObject inlang = new JSONObject();

            /*  if(latitude==0.0) {*/
            try {
                inlat.put("name", "in_lat");
                inlat.put("value", this.lati);
                // inlat.put("value", "40.7333");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
                inlang.put("name", "in_lng");
                inlang.put("value", this.longi);
                // inlang.put("value", "-73.445");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            JSONArray jsonArray = new JSONArray();

            jsonArray.put(inlat);
            jsonArray.put(inlang);

            JSONObject studentsObj = new JSONObject();
            try {
                studentsObj.put("params", jsonArray);
                Log.e("managed_params", String.valueOf(studentsObj));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("find_parking_nearby", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(studentsObj.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json1 = new JSONArray(responseStr);

                    for (int i = 0; i < json1.length(); i++) {
                        item item = new item();
                        JSONObject josnget = json1.getJSONObject(i);
                        String lat = josnget.getString("lat");
                        String lag = josnget.getString("lng");
                        String id = josnget.getString("id");
                        String title = josnget.getString("title");
                        String address = josnget.getString("address");
                        String url1 = josnget.getString("url");
                        String html = josnget.getString("html");
                        String category = josnget.getString("category");
                        String marker = josnget.getString("marker");
                        String location_code = josnget.getString("location_code");
                        String distancea = josnget.getString("distancea");
                        item.setId(id);
                        item.setLang(lag);
                        item.setLat(lat);
                        item.setTitle(title);
                        item.setAddress(address);
                        item.setUrl(url1);
                        item.setHtml(html);
                        item.setCategory(category);
                        item.setMarker(marker);
                        item.setLocation_code(location_code);
                        item.setDistancea(distancea);
                        managedparkinglist.add(item);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs;

            if (getActivity() != null && json1 != null) {
                if (getActivity() != null) {
                    managedmapisloaded = false;
                    viewmap = "mapmove";
                    // googleMap.clear();
                    showmap();
                }
            }
            super.onPostExecute(s);
        }
    }

    //////township over////////

    //commercial part//////////
    public class getall_commercialrulesparking extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        Activity f = getActivity();

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            if (getActivity() != null) {
                SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                String id = logindeatl.getString("id", "null");
                String parkingurl = "_table/google_parking_rules";
                commercial_parking_rules.clear();
                String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
                Log.e("parking_url", url);

                DefaultHttpClient client = new DefaultHttpClient();
                HttpGet post = new HttpGet(url);
                post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
                post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
                HttpResponse response = null;
                try {
                    response = client.execute(post);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (response != null) {
                    HttpEntity resEntity = response.getEntity();
                    String responseStr = null;
                    try {
                        responseStr = EntityUtils.toString(resEntity).trim();
                        json = new JSONObject(responseStr);
                        JSONArray array = json.getJSONArray("resource");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject c = array.getJSONObject(i);
                            item item1 = new item();
                            item1.setTime_rule(c.getString("time_rule"));
                            item1.setPricing_duration(c.getString("pricing_duration"));
                            item1.setDuration_unit(c.getString("duration_unit"));
                            item1.setMax_time(c.getString("max_duration"));
                            item1.setCustom_notice(c.getString("custom_notice"));
                            String time_rules = c.getString("time_rule");
                            item1.setPricing(c.getString("pricing"));
                            item1.setId(c.getString("id"));
                            item1.setEffect(c.getString("in_effect"));
                            item1.setThis_day(c.getString("day_type"));
                            item1.setLocation_code(c.getString("location_code"));
                            if (time_rules.contains("-")) {
                                String start_time = time_rules.substring(0, time_rules.indexOf("-"));
                                String end_time = time_rules.substring(time_rules.indexOf("-") + 1);
                                item1.setStart_time(start_time);
                                item1.setEnd_time(end_time);
                            } else {
                                item1.setStart_time("null");
                                item1.setEnd_time("null");
                            }
                            if (c.has("ReservationAllowed")
                                    && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                    && !c.getString("ReservationAllowed").equals("null")) {
                                item1.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                            }

                            if (c.has("PrePymntReqd_for_Reservation")
                                    && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                    && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                                item1.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                            }

                            if (c.has("CanCancel_Reservation")
                                    && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                    && !c.getString("CanCancel_Reservation").equals("null")) {
                                item1.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                            }

                            if (c.has("Cancellation_Charge")
                                    && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                    && !c.getString("Cancellation_Charge").equals("null")) {
                                item1.setCancellation_Charge(c.getString("Cancellation_Charge"));
                            }

                            if (c.has("Reservation_PostPayment_term")
                                    && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                    && !c.getString("Reservation_PostPayment_term").equals("null")) {
                                item1.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                            }

                            if (c.has("Reservation_PostPayment_LateFee")
                                    && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                    && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                                item1.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                            }

                            if (c.has("ParkNow_PostPaymentAllowed")
                                    && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                    && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                                item1.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                            }

                            if (c.has("ParkNow_PostPayment_term")
                                    && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                    && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                                item1.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                            }

                            if (c.has("before_reserve_verify_lot_avbl")
                                    && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                    && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                                item1.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                            }

                            if (c.has("include_reservations_for_avbl_calc")
                                    && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                    && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                                item1.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                            }

                            if (c.has("ParkNow_PostPayment_LateFee")
                                    && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                    && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                                item1.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                            }
                            commercial_parking_rules.add(item1);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null && json != null) {
                if (getActivity() != null) {
                    new getgooglepartner().execute();
                }
            }
            super.onPostExecute(s);
        }
    }

    public class getgooglepartner extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String parkingurl = "_table/google_parking_partners";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            googlepartnerarray.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setPlace_id(c.getString("place_id"));
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setTitle(c.getString("title"));
                        item1.setAddress(c.getString("address"));
                        item1.setEffect(c.getString("in_effect"));
                        item1.setActive(c.getString("active"));
                        item1.setRenew(c.getString("renewable"));
                        item1.setParking_times(c.getString("parking_times"));
                        item1.setNo_parking_times(c.getString("no_parking_times"));
                        item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        item1.setWeek_ebd_discount(c.getString("weekend_special_diff"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setTotal_lots(c.getString("lots_total"));
                        item1.setLots_aval(c.getString("lots_avbl"));
                        item1.setOff_peak_start(c.getString("off_peak_starts"));
                        item1.setOff_peak_end(c.getString("off_peak_ends"));
                        item1.setId(c.getString("id"));
                        googlepartnerarray.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs;
            if (getActivity() != null && json != null) {
                if (getActivity() != null) {
                    Log.e("googlepartnerarray", String.valueOf(googlepartnerarray.size()));
                    new getotherparkinglist(cenlat, cenlang).execute();
                }
            }
            super.onPostExecute(s);
        }
    }

    public class getotherparkinglist extends AsyncTask<String, String, String> {
        boolean places_id_match = false;
        JSONObject json, json1, json_township;
        double lat = 0.0, lang = 0.0;

        public getotherparkinglist(double lat, double lang) {
            this.lat = lat;
            this.lang = lang;
        }

        @Override
        protected void onPreExecute() {


            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            googleparkingarray.clear();
            JSONObject inlat = new JSONObject();
            JSONObject inlang = new JSONObject();
            try {
                inlat.put("name", "in_lat");
                inlat.put("value", this.lat);
                // inlat.put("value", "40.7333");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
                inlang.put("name", "in_lng");
                inlang.put("value", this.lang);
                // inlang.put("value", "-73.445");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            JSONArray jsonArray = new JSONArray();

            jsonArray.put(inlat);
            jsonArray.put(inlang);

            JSONObject studentsObj = new JSONObject();
            try {
                studentsObj.put("params", jsonArray);
                Log.e("managed_params", String.valueOf(studentsObj));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String url1;

            if (lat == 0.0 || lang == 0.0) {
                url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=40.74599864,-73.9825673&rankby=distance&distance=50000&sensor=false&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&types=parking";
            } else {
                url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + lat + "," + lang + "&rankby=distance&distance=50000&sensor=false&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&types=parking";
            }

            DefaultHttpClient client1 = new DefaultHttpClient();
            HttpGet post1 = new HttpGet(url1);
            post1.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post1.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            HttpResponse response1 = null;
            try {

                response1 = client1.execute(post1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response1 != null) {
                try {
                    HttpEntity resEntity1 = response1.getEntity();
                    String responseStr1 = null;

                    responseStr1 = EntityUtils.toString(resEntity1).trim();
                    json1 = new JSONObject(responseStr1);
                    JSONArray googleparking = json1.getJSONArray("results");

                    for (int j = 0; j < googleparking.length(); j++) {
                        item item = new item();
                        JSONObject google = googleparking.getJSONObject(j);
                        boolean pid = google.has("place_id");
                        if (pid) {
                            String place_id = google.getString("place_id");
                            for (int i = 0; i < googlepartnerarray.size(); i++) {
                                String plac_id = googlepartnerarray.get(i).getPlace_id();
                                if (place_id.equals(plac_id)) {
                                    String loccode = googlepartnerarray.get(i).getLocation_code();
                                    for (int k = 0; k < commercial_parking_rules.size(); k++) {
                                        String locationcode = commercial_parking_rules.get(k).getLocation_code();
                                        if (loccode.equals(locationcode)) {
                                            places_id_match = true;
                                            item.setLocation_code(googlepartnerarray.get(i).getLocation_code());
                                            item.setWeekday_12hr_price(commercial_parking_rules.get(k).getPricing());
                                            break;
                                        }
                                    }
                                }
                            }


                            item.setPlace_id(place_id);


                        } else {
                            places_id_match = false;
                            //   item.setMarker("google");
                            item.setPlace_id("null");
                        }
                        if (places_id_match) {
                            places_id_match = false;
                            item.setMarker("partner");
                        } else {
                            item.setMarker("google");
                        }
                        String geo = google.getString("geometry");
                        JSONObject geometry = new JSONObject(geo);
                        String location = geometry.getString("location");
                        JSONObject lant = new JSONObject(location);
                        String lat = lant.getString("lat");
                        String lang = lant.getString("lng");
                        String name = google.getString("name");
                        if (google.has("vicinity")) {
                            String vicinity = google.getString("vicinity");
                            item.setVicinity(vicinity);
                        } else {
                            item.setVicinity("");
                        }
                        boolean openh = google.has("opening_hours");
                        item.setGooglelat(lat);
                        item.setGooglelan(lang);
                        item.setName(name);


                        if (openh) {
                            String opening_hours = google.getString("opening_hours");
                            JSONObject openig_h = new JSONObject(opening_hours);
                            String open = openig_h.getString("open_now");
                            item.setOpen_now(open);
                        } else {
                            item.setOpen_now("null");
                        }
                        googleparkingarray.add(item);

                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json1 != null) {
                commercial_map_loaded = false;
                viewmap = "commercial";
                showmap();
            }
            super.onPostExecute(s);

        }
    }

    public class getotherparkinglist1 extends AsyncTask<String, String, String> {
        boolean places_id_match = false;
        JSONObject json, json1, json_township;
        double lat = 0.0, lang = 0.0;

        public getotherparkinglist1(double lat, double lang) {
            this.lat = lat;
            this.lang = lang;
        }

        @Override
        protected void onPreExecute() {


            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            googleparkingarray1.clear();
            JSONObject inlat = new JSONObject();
            JSONObject inlang = new JSONObject();
            try {
                inlat.put("name", "in_lat");
                inlat.put("value", this.lat);
                // inlat.put("value", "40.7333");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
                inlang.put("name", "in_lng");
                inlang.put("value", this.lang);
                // inlang.put("value", "-73.445");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            JSONArray jsonArray = new JSONArray();

            jsonArray.put(inlat);
            jsonArray.put(inlang);

            JSONObject studentsObj = new JSONObject();
            try {
                studentsObj.put("params", jsonArray);
                Log.e("managed_params", String.valueOf(studentsObj));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String url1;

            if (lat == 0.0 || lang == 0.0) {
                url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=40.74599864,-73.9825673&rankby=distance&distance=50000&sensor=false&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&types=parking";
            } else {
                url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + lat + "," + lang + "&rankby=distance&distance=50000&sensor=false&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA&types=parking";
            }

            DefaultHttpClient client1 = new DefaultHttpClient();
            HttpGet post1 = new HttpGet(url1);
            post1.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post1.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            HttpResponse response1 = null;
            try {
                response1 = client1.execute(post1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response1 != null) {
                try {
                    HttpEntity resEntity1 = response1.getEntity();
                    String responseStr1 = null;

                    responseStr1 = EntityUtils.toString(resEntity1).trim();
                    json1 = new JSONObject(responseStr1);
                    JSONArray googleparking = json1.getJSONArray("results");
                    for (int j = 0; j < googleparking.length(); j++) {
                        item item = new item();
                        JSONObject google = googleparking.getJSONObject(j);
                        boolean pid = google.has("place_id");
                        if (pid) {
                            String place_id = google.getString("place_id");
                            for (int i = 0; i < googlepartnerarray.size(); i++) {
                                String plac_id = googlepartnerarray.get(i).getPlace_id();
                                if (place_id.equals(plac_id)) {
                                    String loccode = googlepartnerarray.get(i).getLocation_code();
                                    for (int k = 0; k < commercial_parking_rules.size(); k++) {
                                        String locationcode = commercial_parking_rules.get(k).getLocation_code();
                                        if (loccode.equals(locationcode)) {
                                            places_id_match = true;
                                            item.setLocation_code(googlepartnerarray.get(i).getLocation_code());
                                            item.setWeekday_12hr_price(commercial_parking_rules.get(k).getPricing());
                                            break;
                                        }
                                    }
                                }
                            }
                            item.setPlace_id(place_id);
                        } else {
                            places_id_match = false;
                            //   item.setMarker("google");
                            item.setPlace_id("null");
                        }
                        if (places_id_match) {
                            places_id_match = false;
                            item.setMarker("partner");
                        } else {
                            item.setMarker("google");
                        }
                        String geo = google.getString("geometry");
                        JSONObject geometry = new JSONObject(geo);
                        String location = geometry.getString("location");
                        JSONObject lant = new JSONObject(location);
                        String lat = lant.getString("lat");
                        String lang = lant.getString("lng");
                        String name = google.getString("name");
                        if (google.has("vicinity")) {
                            String vicinity = google.getString("vicinity");
                            item.setVicinity(vicinity);
                        } else {
                            item.setVicinity("");
                        }
                        boolean openh = google.has("opening_hours");
                        item.setGooglelat(lat);
                        item.setGooglelan(lang);
                        item.setName(name);
                        if (openh) {
                            String opening_hours = google.getString("opening_hours");
                            JSONObject openig_h = new JSONObject(opening_hours);
                            String open = openig_h.getString("open_now");
                            item.setOpen_now(open);
                        } else {
                            item.setOpen_now("null");
                        }
                        googleparkingarray1.add(item);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json1 != null) {
                if (getActivity() != null) {
                    if (googleparkingarray1.size() > 0) {
                        googleparkingarray.clear();
                        googleparkingarray.addAll(googleparkingarray1);
                        googleparkingarray1.clear();
                    }
                    commercial_map_loaded = false;
                    viewmap = "comm_move";
                    showmap();
                }
            }
            super.onPostExecute(s);

        }
    }

    public class getplacedeatilCommercial extends AsyncTask<String, String, String> {

        JSONObject json, json1;
        String pid;
        String address = "null", gname = "null", open_ho = "null", places_id = "null";

        public getplacedeatilCommercial(String places_id) {
            this.pid = places_id;
        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            String url1 = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + pid + "&key=AIzaSyA-Z32WSS65dVIRFQbUh3VRWP9A9DQknuA";

            Log.e("placeddeatil", url1);
            DefaultHttpClient client1 = new DefaultHttpClient();
            HttpGet post1 = new HttpGet(url1);
            post1.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post1.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

//this is your response:

            HttpResponse response1 = null;
            try {

                response1 = client1.execute(post1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response1 != null) {
                try {

                    HttpEntity resEntity1 = response1.getEntity();
                    String responseStr1 = null;
                    responseStr1 = EntityUtils.toString(resEntity1).trim();
                    json1 = new JSONObject(responseStr1);
                    JSONObject google = json1.getJSONObject("result");
                    String geo = google.getString("geometry");
                    JSONObject geometry = new JSONObject(geo);
                    String location = geometry.getString("location");
                    JSONObject lant = new JSONObject(location);
                    cenlat = Double.parseDouble(lant.getString("lat"));
                    cenlang = Double.parseDouble(lant.getString("lng"));
                    gname = google.getString("name");
                    boolean openh = google.has("opening_hours");
                    if (openh) {
                        String opening_hours = google.getString("opening_hours");
                        JSONObject openig_h = new JSONObject(opening_hours);
                        open_ho = openig_h.getString("open_now");
                    }
                    boolean pid = google.has("place_id");
                    if (pid) {
                        places_id = google.getString("place_id");
                    }
                    address = google.getString("formatted_address");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;

            if (getActivity() != null && json1 != null) {
                if (getActivity() != null) {
                    addrsstitle = gname;
                    addresssub = address;
                    txt_selected_address_from_map.setText(addresssub);
                }
            } else {

            }
            super.onPostExecute(s);

        }
    }

    //commercial over//////////////////
    public class getprivate extends AsyncTask<String, String, String> {
        boolean places_id_match = false;
        JSONObject json, json1, json_township;
        double lat = 0.0, lang = 0.0;

        public getprivate(double lat, double lang) {
            this.lat = lat;
            this.lang = lang;
        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            parkingarray.clear();
            JSONObject inlat = new JSONObject();
            JSONObject inlang = new JSONObject();
            try {
                inlat.put("name", "in_lat");
                inlat.put("value", this.lat);
                // inlat.put("value", "40.7333");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
                inlang.put("name", "in_lng");
                inlang.put("value", this.lang);
                // inlang.put("value", "-73.445");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(inlat);
            jsonArray.put(inlang);

            JSONObject studentsObj = new JSONObject();
            try {
                studentsObj.put("params", jsonArray);
                Log.e("managed_params", String.valueOf(studentsObj));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + "_proc/find_other_parking_partners_nearby?in_lat=" + lat + "&in_lng=" + lang + "";
            Log.e("parter_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(studentsObj.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                try {
                    HttpEntity resEntity = response.getEntity();
                    String responseStr = null;
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONArray array = new JSONArray(responseStr);
                    for (int i = 0; i < array.length(); i++) {
                        item item1 = new item();
                        JSONObject c = array.getJSONObject(i);
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setTitle(c.getString("title"));
                        item1.setAddress(c.getString("address"));
                        item1.setLat(c.getString("lat"));
                        item1.setLang(c.getString("lng"));
                        item1.setEffect(c.getString("in_effect"));
                        item1.setActive(c.getString("active"));
                        item1.setRenew(c.getString("renewable"));
                        item1.setParking_times(c.getString("parking_times"));
                        item1.setNo_parking_times(c.getString("no_parking_times"));
                        item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        item1.setWeek_ebd_discount(c.getString("weekend_special_diff"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setLots_aval(c.getString("lots_avbl"));
                        item1.setTotal_lots(c.getString("lots_total"));
                        item1.setId(c.getString("id"));
                        parkingarray.add(item1);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            super.onPostExecute(s);
            viewmap = "private";
            if (getActivity() != null) {
                private_map_is_loaded = false;
                showmap();
            }
        }
    }

    public class getprivate1 extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        double lat = 0.0, lang = 0.0;

        public getprivate1(double lat, double lang) {
            this.lat = lat;
            this.lang = lang;
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            parkingarray1.clear();

            JSONObject inlat = new JSONObject();
            JSONObject inlang = new JSONObject();
            try {
                inlat.put("name", "in_lat");
                inlat.put("value", this.lat);
                // inlat.put("value", "40.7333");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
                inlang.put("name", "in_lng");
                inlang.put("value", this.lang);
                // inlang.put("value", "-73.445");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            JSONArray jsonArray = new JSONArray();

            jsonArray.put(inlat);
            jsonArray.put(inlang);

            JSONObject studentsObj = new JSONObject();
            try {
                studentsObj.put("params", jsonArray);
                Log.e("managed_params", String.valueOf(studentsObj));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + "_proc/find_other_parking_partners_nearby?in_lat=" + lat + "&in_lng=" + lang + "";
            Log.e("parter_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(studentsObj.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                try {
                    HttpEntity resEntity = response.getEntity();
                    String responseStr = null;
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONArray array = new JSONArray(responseStr);
                    for (int i = 0; i < array.length(); i++) {
                        item item1 = new item();
                        JSONObject c = array.getJSONObject(i);
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setTitle(c.getString("title"));
                        item1.setAddress(c.getString("address"));
                        item1.setLat(c.getString("lat"));
                        item1.setLang(c.getString("lng"));
                        item1.setEffect(c.getString("in_effect"));
                        item1.setActive(c.getString("active"));
                        item1.setRenew(c.getString("renewable"));
                        item1.setParking_times(c.getString("parking_times"));
                        item1.setNo_parking_times(c.getString("no_parking_times"));
                        item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        item1.setWeek_ebd_discount(c.getString("weekend_special_diff"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setLots_aval(c.getString("lots_avbl"));
                        item1.setTotal_lots(c.getString("lots_total"));
                        item1.setId(c.getString("id"));
                        parkingarray1.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs;
            super.onPostExecute(s);
            viewmap = "private_move";
            private_map_is_loaded = false;
            if (getActivity() != null) {
                if (parkingarray1.size() > 0) {
                    parkingarray.clear();
                    parkingarray.addAll(parkingarray1);
                    parkingarray1.clear();
                }
                showmap();
            }
        }
    }
}
