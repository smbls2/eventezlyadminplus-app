package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.frament.Fragment_permit_definitions.Township_Code;
import static com.softmeasures.eventezlyadminplus.frament.Fragment_permit_definitions.Township_ID;
import static com.softmeasures.eventezlyadminplus.frament.Fragment_permit_definitions.Township_Name;

public class Fragment_permit_definitions_Two extends Fragment {

    ConnectionDetector cd;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar;
    View mView;
    String id, twcode, role, select_town;
    ListView listofvehicleno;
    ArrayList<item> township_array_list, township_array;
    CustomAdapterTwonship customAdapterTwonship;
    ListView lv_Township;
    TextView txtTitte;
    String mURL = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_permit_definitions_two, container, false);

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        id = logindeatl.getString("id", "null");
        role = logindeatl.getString("role", "null");
        twcode = logindeatl.getString("twcode", "null");

        if (getArguments() != null) {
            select_town = getArguments().getString("select_town");
        }

        ints();

        try {
            txtTitte.setText("SELECT " + select_town);
            if (select_town.equalsIgnoreCase("TOWNSHIP")) {
                try {
                    if (role.equalsIgnoreCase("TwpAdmin")) {
                        try {
                            mURL = "_table/township_parking_partners_view?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (role.equalsIgnoreCase("TwpInspector")) {
                        try {
                            mURL = "_table/township_parking_partners_view?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (role.equalsIgnoreCase("TwpBursar")) {
                        try {
                            mURL = "_table/township_parking_partners_view?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        mURL = "_table/township_parking_partners_view";
                    }
                    new gettownship_list(mURL).execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (select_town.equalsIgnoreCase("COMMERCIAL")) {
                try {
                    if (role.equalsIgnoreCase("TwpAdmin")) {
                        try {
                            mURL = "_table/commercial_parking_partners_view?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (role.equalsIgnoreCase("TwpInspector")) {
                        try {
                            mURL = "_table/commercial_parking_partners_view?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (role.equalsIgnoreCase("TwpBursar")) {
                        try {
                            mURL = "_table/commercial_parking_partners_view?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        mURL = "_table/commercial_parking_partners_view";
                    }
                    new gettownship_list(mURL).execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (select_town.equalsIgnoreCase("PRIVATE")) {
                try {
                    if (role.equalsIgnoreCase("TwpAdmin")) {
                        try {
                            mURL = "_table/private_parking_partners_view?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (role.equalsIgnoreCase("TwpInspector")) {
                        try {
                            mURL = "_table/private_parking_partners_view?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (role.equalsIgnoreCase("TwpBursar")) {
                        try {
                            mURL = "_table/private_parking_partners_view?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        mURL = "_table/private_parking_partners_view";
                    }
                    new gettownship_list(mURL).execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (select_town.equalsIgnoreCase("OTHER")) {
                try {
                    if (role.equalsIgnoreCase("TwpAdmin")) {
                        try {
                            mURL = "_table/other_parking_partners_view?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (role.equalsIgnoreCase("TwpInspector")) {
                        try {
                            mURL = "_table/other_parking_partners_view?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (role.equalsIgnoreCase("TwpBursar")) {
                        try {
                            mURL = "_table/other_parking_partners_view?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        mURL = "_table/other_parking_partners_view";
                    }
                    new gettownship_list(mURL).execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                String mOther = select_town + "_parking_partners_view";
                try {
                    if (role.equalsIgnoreCase("TwpAdmin")) {
                        try {

                            mURL = "_table/" + mOther + "?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (role.equalsIgnoreCase("TwpInspector")) {
                        try {
                            mURL = "_table/" + mOther + "?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (role.equalsIgnoreCase("TwpBursar")) {
                        try {
                            mURL = "_table/" + mOther + "?filter=(township_code%3D'" + URLEncoder.encode(twcode, "utf-8") + "')";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        mURL = "_table/" + mOther;
                    }
                    new gettownship_list(mURL).execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return mView;
    }

    public void ints() {

        lv_Township = (ListView) mView.findViewById(R.id.lv_Township);
        txtTitte = (TextView) mView.findViewById(R.id.txttitte);
        progressBar = (ProgressBar) mView.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) mView.findViewById(R.id.rl_progressbar);
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        onClicks();
    }

    public void onClicks() {

    }

    public class gettownship extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String parkingurl = "_table/townships_manager";

        @Override
        protected void onPreExecute() {
            township_array = new ArrayList<item>();
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            township_array.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    Log.e("responseStr ", " : " + responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setId(c.getString("id"));
                        item1.setDate(c.getString("date_time"));
                        item1.setTitle(c.getString("manager_id"));
                        item1.setManager_type(c.getString("manager_type"));
                        item1.setLot_manager(c.getString("lot_manager"));
                        item1.setAddress(c.getString("address"));
                        item1.setState(c.getString("state"));
                        item1.setCity(c.getString("city"));
                        item1.setCountry(c.getString("country"));
                        item1.setZip_code(c.getString("zip"));
                        item1.setTownship_logo(c.getString("township_logo"));
                        item1.setOfficial_logo(c.getString("official_logo"));
                        item1.setV_user_id(c.getString("user_id"));
                        item1.setIsslected(false);
                        township_array.add(item1);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            rl_progressbar.setVisibility(View.GONE);
            if (json != null) {
                try {
                    new gettownship_list("").execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            super.onPostExecute(s);
        }
    }

    public class gettownship_list extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String parkingurl = "_table/township_parking_partners_view";

        public gettownship_list(String partners_view) {
            parkingurl = "" + partners_view;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            township_array_list = new ArrayList<item>();
            township_array_list.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    Log.e("responseStr ", " : " + responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setTwp_id(c.getString("twp_id"));
                        item1.setTownship_code(c.getString("township_code"));
                        item1.setTownship_name(c.getString("township_name"));
                        item1.setAddress(c.getString("address"));
                        item1.setTownship_logo(c.getString("township_logo"));
                        item1.setOfficial_logo(c.getString("official_logo"));
                        item1.setTwp_image1(c.getString("twp_image1"));
                        item1.setTwp_image2(c.getString("twp_image2"));
                        item1.setActive(c.getString("active"));
                        item1.setRelatedLocations(c.getString("RelatedLocations"));

                        item1.setIsslected(false);
                        township_array_list.add(item1);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (township_array_list.size() > 0) {
                    customAdapterTwonship = new CustomAdapterTwonship(getActivity(), township_array_list, getResources());
                    lv_Township.setAdapter(customAdapterTwonship);
                } else {
                    //txt_No_Record.setVisibility(View.VISIBLE);
                    lv_Township.setVisibility(View.GONE);
                }
            }
            super.onPostExecute(s);
        }
    }

    public class CustomAdapterTwonship extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdapterTwonship(Activity a, ArrayList<item> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txt_title, txt_address;
            ImageView img_check, img_township;
            RelativeLayout ll_Main;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_twonship_list, null);
                holder = new ViewHolder();

                holder.ll_Main = (RelativeLayout) vi.findViewById(R.id.ll_Main);
                holder.txt_title = (TextView) vi.findViewById(R.id.txt_code);
                holder.txt_address = (TextView) vi.findViewById(R.id.txt_address);
                holder.img_check = (ImageView) vi.findViewById(R.id.img_check);
                holder.img_township = (ImageView) vi.findViewById(R.id.img_township);

                vi.setTag(holder);
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (data.size() <= 0) {

            } else {
                holder.txt_title.setText(data.get(position).getTownship_code());
                holder.txt_address.setText(data.get(position).getAddress() + " " + data.get(position).getCountry());

                /*if (!data.get(position).getTownship_code().equalsIgnoreCase("")) {
                    int t_pos = getTownshipPos(data.get(position).getTownship_code());
                }*/

                Picasso.with(context).load(data.get(position).getTownship_logo()).placeholder(R.drawable.ic_defulit_logo).into(holder.img_township);

                if (data.get(position).getIsslected()) {
                    holder.img_check.setVisibility(View.VISIBLE);
                } else {
                    holder.img_check.setVisibility(View.GONE);
                }

                holder.ll_Main.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //int t_pos = getTownshipPos(data.get(position).getTownship_code());
                        Township_ID = data.get(position).getTwp_id();
                        Township_Name = data.get(position).getTownship_name();
                        Township_Code = data.get(position).getTownship_code();
                        Bundle bundle = new Bundle();
                        bundle.putString("manager_id", data.get(position).getTownship_code());
                        bundle.putString("Township_name", select_town);

                        ((vchome) getActivity()).show_back_button();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        Fragment_permit_definitions_Three pay = new Fragment_permit_definitions_Three();
                        pay.setArguments(bundle);
                        ft.add(R.id.My_Container_1_ID, pay);
                        fragmentStack.lastElement().onPause();
                        ft.hide(fragmentStack.lastElement());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                    }
                });
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

    public int getTownshipPos(String category) {
        for (int i = 0; i < township_array.size(); i++) {
            if (township_array.get(i).getTitle().equals(category)) {
                return i;
            }
        }
        return 0;
    }

}
