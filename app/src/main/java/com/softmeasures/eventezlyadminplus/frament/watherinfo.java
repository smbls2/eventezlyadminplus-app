package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class watherinfo extends Fragment {
    private static final String LOG_TAG = "ExampleApp";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyC6UabVHdti4zlU6E1iajVuNi6ZdKhDK5g";
    TextView txttitle, txtweathertitle, txttemp, txtupdatedate, txtwinspeed, txtforcast, txtpressare, txthumidity, txtsunrise, txtsunset;
    TextView txtrefresh;
    ImageView imgweather;
    ProgressDialog pdialog;
    double latitude, longitude;
    RelativeLayout refreshdata;
    ConnectionDetector cd;
    ProgressBar progressBar;
    EditText edit_city;
    RelativeLayout rl_progressbar, rl_main;
    String pre, hum, temp, winseed, sunrisetime, sunsettime, country, forecast, countryname, imgweatherurl, currentdate;
    ListView list_destination;
    TextView txt_search;
    RelativeLayout rl_editname;
    TextView btn_destination;
    boolean opendes = false;
    String adrreslocation, addressti, addressin, lang, lat;
    String addrsstitle, addresssub;
    ArrayList<item> directionarray = new ArrayList<>();
    EditText edit_destition_address;
    ArrayList<item> searcharray = new ArrayList<>();
    ArrayList<item> searchplace = new ArrayList<>();
    AutoCompleteTextView autoCompView_des;

    public static ArrayList<item> autocomplete(String input) {
        ArrayList<item> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            URL url = new URL(sb.toString());
            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        try {

            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
            resultList = new ArrayList<item>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                String cghcbdc = predsJsonArray.getJSONObject(i).getString("description");
                if (cghcbdc.contains(",")) {
                    String street = cghcbdc.substring(0, cghcbdc.indexOf(","));
                    String addrress = cghcbdc.substring(cghcbdc.indexOf(",") + 1);
                    item ii = new item();
                    ii.setLoationname(street);
                    ii.setAddress(addrress);
                    resultList.add(ii);
                }
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_mywwather, container, false);
        autoCompView_des = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteTextView);
        GPSTracker tracker = new GPSTracker(getActivity().getApplicationContext());
        txttitle = (TextView) view.findViewById(R.id.txttitte);
        txtweathertitle = (TextView) view.findViewById(R.id.weathercountryname);
        txttemp = (TextView) view.findViewById(R.id.tempvalues);
        txtupdatedate = (TextView) view.findViewById(R.id.weatherdate);
        txtwinspeed = (TextView) view.findViewById(R.id.txtspeed);
        txtforcast = (TextView) view.findViewById(R.id.txtforcast);
        txtpressare = (TextView) view.findViewById(R.id.txtpressure);
        txthumidity = (TextView) view.findViewById(R.id.txthumidity);
        txtsunrise = (TextView) view.findViewById(R.id.txtsunrise);
        txtsunset = (TextView) view.findViewById(R.id.txtsunset);
        txtrefresh = (TextView) view.findViewById(R.id.txtrefreshdata);
        imgweather = (ImageView) view.findViewById(R.id.imgweatherimage);
        refreshdata = (RelativeLayout) view.findViewById(R.id.refreshdata);
        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Thin.ttf");
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);

        edit_destition_address = (EditText) view.findViewById(R.id.edit_destination);
        list_destination = (ListView) view.findViewById(R.id.list_destinatin);
        rl_editname = (RelativeLayout) view.findViewById(R.id.rl_edit_name_with_search);
        btn_destination = (TextView) view.findViewById(R.id.btn_destination);
        txt_search = (TextView) view.findViewById(R.id.txt_search);
        edit_city = (EditText) view.findViewById(R.id.edit_cityname);
        rl_main = (RelativeLayout) view.findViewById(R.id.title);
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        txttitle.setTypeface(type);
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        cd = new ConnectionDetector(getActivity());
        if (cd.isConnectingToInternet()) {

            Calendar c = Calendar.getInstance();
            System.out.println("Current time => " + c.getTime());
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
            currentdate = df.format(c.getTime());
            if (tracker.canGetLocation()) {
                latitude = tracker.getLatitude();
                longitude = tracker.getLongitude();
                //   getaddress1(latitude, longitude);
                new fetchLatLongFromaddress(String.valueOf(latitude), String.valueOf(longitude)).execute();
                new getweatherinfo(String.valueOf(latitude), String.valueOf(longitude)).execute();
                btn_destination.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (opendes == false) {
                            Resources rs;
                            CustomAdaptercity adpater = new CustomAdaptercity(getActivity(), directionarray, rs = getResources());
                            list_destination.setAdapter(adpater);
                            rl_editname.setVisibility(View.GONE);
                            opendes = true;
                            list_destination.setVisibility(View.VISIBLE);
                        } else {
                            opendes = false;
                            list_destination.setVisibility(View.GONE);
                        }
                    }

                });
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Failed to fetch user location!");
                alertDialog.setMessage("Please enable user location for app, location is required for app to work properly");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });

                alertDialog.show();
            }

            txt_search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String a = edit_destition_address.getText().toString();

                    if (!a.equals("")) {
                        if (v != null) {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                        getaddress(a);
                    }
                }
            });
            autoCompView_des.setAdapter(new CustomAutoplace(getActivity(), getResources()));
            autoCompView_des.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String str1 = searchplace.get(position).getLoationname();
                    String str = searchplace.get(position).getAddress();
                    txttitle.setText(str1 + "," + str);
                    rl_editname.setVisibility(View.GONE);
                    autoCompView_des.setVisibility(View.GONE);
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    new fetchLatLongFromService1(str1 + "," + str).execute();

                }
            });

        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();
        }
        return view;
    }

    private void getaddress(String address1) {
        searcharray.clear();

        Geocoder gc = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = gc.getFromLocationName(address1, 1);
            StringBuilder sb = new StringBuilder();
            StringBuilder addressl = new StringBuilder();
            StringBuilder country = new StringBuilder();
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getLatitude()).append("\n");
                    sb.append(address.getLongitude());
                    addressl.append(address.getAddressLine(0)).append(", ");
                    addressl.append(address.getAddressLine(1));
                    country.append(address.getAddressLine(2));
                    break;
                }
                String locationAddress = sb.toString();
                String loc = addressl.toString();
                String countyname = country.toString();
                lang = locationAddress.substring(locationAddress.indexOf('\n') + 1);
                lat = locationAddress.substring(0, locationAddress.indexOf('\n'));
                addressti = loc.substring(0, loc.indexOf(","));
                addressin = loc.substring(loc.indexOf(",") + 1);
                if (countyname.equals("null")) {
                    this.adrreslocation = addressti + ", " + addressin;
                } else {
                    this.adrreslocation = addressin + ", " + adrreslocation + ", " + countyname;
                }

                item i = new item();
                i.setTitle_address(addressti);
                i.setAddress(addressin);
                searcharray.add(i);
                Resources rs;
                rl_editname.setVisibility(View.GONE);
                list_destination.setVisibility(View.VISIBLE);
                CustomAdaptercity adpater = new CustomAdaptercity(getActivity(), searcharray, rs = getResources());
                list_destination.setAdapter(adpater);

            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Address not found!");
                alertDialog.setMessage("Could not find location. Try including the exact location information");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alertDialog.show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getaddress1(Double lat, Double lon) {
        Geocoder gc = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = gc.getFromLocation(lat, lon, 1);
            StringBuilder sb = new StringBuilder();
            StringBuilder addressl = new StringBuilder();
            StringBuilder country = new StringBuilder();
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getLatitude()).append("\n");
                    sb.append(address.getLongitude());
                    addressl.append(address.getAddressLine(0)).append(", ");
                    addressl.append(address.getAddressLine(1));
                    country.append(address.getAddressLine(2));
                    break;
                }

                String loc = addressl.toString();
                Log.e("addrrss", loc);
                String countyname = country.toString();
                String addressti1 = loc.substring(0, loc.indexOf(","));
                String addressin1 = loc.substring(loc.indexOf(",") + 1);
                if (countyname.equals("null")) {
                    this.adrreslocation = addressti1 + ", " + addressin1;
                } else {
                    this.adrreslocation = addressti1 + ", " + addressin1 + ", " + countyname;
                }
                Resources rs;
                btn_destination.setText(this.adrreslocation);
                rl_editname.setVisibility(View.GONE);
                new getdestination().execute();
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Address not found!");
                alertDialog.setMessage("Could not find location. Try including the exact location information");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public class fetchLatLongFromaddress extends AsyncTask<Void, Void, StringBuilder> {

        String lat1, lang1;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        public fetchLatLongFromaddress(String lat, String lang) {
            super();
            this.lat1 = lat;
            this.lang1 = lang;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {

                //  new getsearchotherlocation().execute();
                //
                // ();

                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat1 + "," + lang1 + "&sensor=true&key=AIzaSyBRdCWdlUMy3X5dc-9jMe0jRXJENibjdN8";

                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
                rl_progressbar.setVisibility(View.GONE);

                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");

                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray
                            .getJSONObject(0);
                    adrreslocation = before_geometry_jsonObj.getString("formatted_address");
                    JSONObject geometry_jsonObj = before_geometry_jsonObj
                            .getJSONObject("geometry");

                    JSONObject location_jsonObj = geometry_jsonObj
                            .getJSONObject("location");

                    lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);

                    lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);

                    btn_destination.setText(adrreslocation);
                    rl_editname.setVisibility(View.GONE);
                    new getdestination().execute();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public class fetchLatLongFromService1 extends AsyncTask<Void, Void, StringBuilder> {
        String place;
        String end;


        fetchLatLongFromService1(String place) {
            super();
            this.place = place;
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "http://maps.googleapis.com/maps/api/geocode/json?address="
                        + this.place + "&sensor=false";
                googleMapUrl = googleMapUrl.replaceAll(" ", "%20");
                URL url = new URL(googleMapUrl);
                URLConnection c = url.openConnection();
                c.setConnectTimeout(5000);
                c.setReadTimeout(10000);
                c.getInputStream();
                InputStreamReader in = new InputStreamReader(
                        c.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");

                if (status.equals("OK")) {
                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray.getJSONObject(0);
                    JSONObject geometry_jsonObj = before_geometry_jsonObj.getJSONObject("geometry");
                    JSONObject location_jsonObj = geometry_jsonObj.getJSONObject("location");
                    lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);
                    lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);
                    LatLng point = new LatLng(law, lng);
                    addrsstitle = place.substring(0, place.indexOf(","));
                    addresssub = place.substring(place.indexOf(",") + 1);
                    new getweatherinfo(lat, lang).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                rl_progressbar.setVisibility(View.GONE);
                e.printStackTrace();

            } catch (Exception e) {
                rl_progressbar.setVisibility(View.GONE);
            }
        }

    }

    public class CustomAutoplace extends BaseAdapter implements View.OnClickListener, Filterable {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAutoplace(Activity a, Resources resLocal) {
            activity = a;
            context = a;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.layout, null);
                holder = new ViewHolder();

                holder.txt_commerical_name = (TextView) vi.findViewById(R.id.txt_name);
                holder.txt_deatil = (TextView) vi.findViewById(R.id.address);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }
            if (data.size() <= 0) {
            } else {
                try {
                    tempValues = null;
                    tempValues = (item) data.get(position);
                    holder.txt_commerical_name.setText(tempValues.getLoationname());
                    holder.txt_deatil.setText(tempValues.getAddress());
                    int k = 0;
                } catch (IndexOutOfBoundsException e) {

                } catch (Exception e) {
                }


            }

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        data = autocomplete(constraint.toString());
                        filterResults.values = data;
                        filterResults.count = data.size();
                        searchplace.clear();
                        searchplace.addAll(data);
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;

        }


        public class ViewHolder {
            public TextView txt_commerical_name, txt_deatil;
        }
    }

    public class CustomAdaptercity extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdaptercity(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.destinationad, null);
                holder = new ViewHolder();

                holder.txt_address = (TextView) vi.findViewById(R.id.text_address);
                holder.txt_address_title = (TextView) vi.findViewById(R.id.text_address_title);
                holder.txt_lat = (TextView) vi.findViewById(R.id.txt_lat);
                holder.txt_lang = (TextView) vi.findViewById(R.id.txt_lng);
                vi.setTag(holder);
                vi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String address = holder.txt_address_title.getText().toString();
                        opendes = false;
                        if (address.equals("Enter New Address")) {

                            rl_editname.setVisibility(View.VISIBLE);
                            list_destination.setVisibility(View.GONE);
                        } else {
                            if (v != null) {
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                            }
                            addrsstitle = address;
                            String address1 = holder.txt_address.getText().toString();
                            addresssub = address1;
                            btn_destination.setText(address + " ," + address1);
                            list_destination.setVisibility(View.GONE);
                            String sdd = btn_destination.getText().toString();
                            String sdds = "";
                            if (sdd.contains("null")) {
                                sdds = sdd.substring(0, sdd.lastIndexOf(","));
                                Log.e("ssd", sdds);
                            } else {
                                sdds = sdd;
                            }
                            Log.e("find_address", sdds);
                            String add = URLEncoder.encode(sdds);
                            String curatlat = holder.txt_lat.getText().toString();
                            String curtlang = holder.txt_lang.getText().toString();
                            new getweatherinfo(curatlat, curtlang).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                });


            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {

            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String d = tempValues.getTitle_address();
                if (!d.equals("null")) {
                    vi.setVisibility(View.VISIBLE);
                    holder.txt_address_title.setText(tempValues.getTitle_address());
                    if (d.equals("Enter New Address")) {
                        vi.setBackgroundColor(Color.parseColor("#90caf9"));
                        holder.txt_address.setText("");
                    } else {
                        if (tempValues.isclcik()) {
                            vi.setBackgroundColor(Color.parseColor("#2F5CF2"));
                        } else {
                            vi.setBackgroundColor(Color.parseColor("#ffffff"));
                        }
                        if (tempValues.getAddress().equals("null")) {
                            holder.txt_address.setText("");
                        } else {
                            holder.txt_address.setText(tempValues.getAddress());
                        }
                        holder.txt_lat.setText(tempValues.getLat());
                        holder.txt_lang.setText(tempValues.getLang());
                    }
                } else {
                    vi.setVisibility(View.GONE);
                }

            }

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txt_address_title, txt_address, txt_lat, txt_lang;


        }
    }

    public class getweatherinfo extends AsyncTask<String, String, String> {
        String url;
        JSONObject json, json1;

        public getweatherinfo(String lat, String lang) {
            url = "http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lang + "&appid=0bf93723659ddf0c03095b6a1ddb3089";
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Log.e("url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);

                    String main = json.getString("main");
                    String weather = json.getString("weather");
                    String wind = json.getString("wind");
                    String sys = json.getString("sys");

                    JSONObject main1 = new JSONObject(main);
                    JSONArray wet = new JSONArray(weather);
                    JSONObject weather1 = wet.getJSONObject(0);
                    JSONObject wind1 = new JSONObject(wind);
                    JSONObject sys1 = new JSONObject(sys);

                    pre = main1.getString("pressure");
                    hum = main1.getString("humidity");
                    temp = main1.getString("temp");
                    winseed = wind1.getString("speed");
                    String sunrise = sys1.getString("sunrise");
                    String sunset = sys1.getString("sunset");
                    country = sys1.getString("country");
                    forecast = weather1.getString("description");
                    imgweatherurl = weather1.getString("icon");
                    countryname = json.getString("name");
                    long unixTimestamp1 = Long.parseLong(sunrise);
                    long javaTimestamp1 = unixTimestamp1 * 1000;
                    Date date1 = new Date(javaTimestamp1);
                    long unixTimestamp = Long.parseLong(sunset);
                    long javaTimestamp = unixTimestamp * 1000;
                    Date date = new Date(javaTimestamp);
                    TimeZone tz = TimeZone.getDefault();
                    sunrisetime = new SimpleDateFormat("hh:mm a").format(date1);
                    sunsettime = new SimpleDateFormat("hh:mm a").format(date);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
            }
            Resources rs;
            if (getActivity() != null && json != null) {
                String imgweatherurl1 = "http://openweathermap.org/img/w/" + imgweatherurl + ".png";
                txtweathertitle.setText(countryname + ", " + country);
                double f = (Double.parseDouble(temp) - 273.15) * 1.800000;
                f = f + 32;
                Log.e("f", String.valueOf(f));
                String finallab = String.valueOf(Math.floor(f * 100) / 100);
                txttemp.setText("Temp:" + (String.valueOf(finallab)) + " \u2109");
                txtupdatedate.setText("Updated at: " + currentdate);
                txtwinspeed.setText(winseed + " mph");
                txtforcast.setText(forecast.toUpperCase());
                txtpressare.setText(pre);
                txthumidity.setText(hum + " %");
                txtsunset.setText(sunsettime);
                txtsunrise.setText(sunrisetime);
                Glide.with(getActivity()).load(imgweatherurl1).into(imgweather);
                /*Picasso.with(getActivity()).load(imgweatherurl1).into((ImageView) getView().findViewById(R.id.imgweatherimage), new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                    }
                });*/
            }
            super.onPostExecute(s);
        }
    }

    public class getdestination extends AsyncTask<String, String, String> {
        JSONObject json = null;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "0");
        String vehiclenourl = "_table/user_locations?filter=user_id%3D" + user_id + "";
        String res_id;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            directionarray.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("url_get_wather", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    item ii = new item();
                    ii.setTitle_address("Enter New Address");
                    ii.setAddress("null");
                    ii.setIsclcik(false);
                    directionarray.add(ii);

                    String addd = adrreslocation.substring(0, adrreslocation.indexOf(","));
                    String tit = adrreslocation.substring(adrreslocation.indexOf(",") + 1);
                    String hh = tit.substring(tit.indexOf(" ") + 1);
                    item cu = new item();
                    cu.setTitle_address(addd);
                    cu.setAddress(hh);
                    cu.setLat(String.valueOf(latitude));
                    cu.setLang(String.valueOf(longitude));
                    cu.setIsclcik(true);
                    directionarray.add(cu);

                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item iii = new item();
                        JSONObject c = array.getJSONObject(i);
                        res_id = c.getString("id");
                        String address = c.getString("location_address");
                        String addressname = c.getString("location_name");
                        String lat = c.getString("lat");
                        String lang = c.getString("lng");
                        boolean hr = lat.contains("Optional");
                        if (hr) {
                            Log.e("Optional", "yes");
                            String hr1 = lat.substring(lat.indexOf('(') + 1);
                            String hr2 = hr1.substring(0, hr1.indexOf(')'));
                            String hr3 = lang.substring(lang.indexOf('(') + 1);
                            String hr4 = hr3.substring(0, hr3.indexOf(')'));
                            lat = hr2;
                            lang = hr4;
                        }
                        iii.setTitle_address(addressname);
                        iii.setAddress(address);
                        iii.setLat(lat);
                        iii.setIsclcik(false);
                        iii.setLang(lang);
                        directionarray.add(iii);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json != null) {

                if (json != null) {
                    CustomAdaptercity adpater = new CustomAdaptercity(getActivity(), directionarray, rs = getResources());
                    list_destination.setAdapter(adpater);
                } else {
                    item ii = new item();
                    ii.setTitle_address("Enter New Address");
                    ii.setIsclcik(false);
                    ii.setAddress("null");
                    directionarray.add(ii);
                    CustomAdaptercity adpater = new CustomAdaptercity(getActivity(), directionarray, rs = getResources());
                    list_destination.setAdapter(adpater);
                }
            } else {
                item ii = new item();
                ii.setTitle_address("Enter New Address");
                ii.setIsclcik(false);
                ii.setAddress("null");
                directionarray.add(ii);
                CustomAdaptercity adpater = new CustomAdaptercity(getActivity(), directionarray, rs = getResources());
                list_destination.setAdapter(adpater);
            }
            super.onPostExecute(s);
        }
    }
}