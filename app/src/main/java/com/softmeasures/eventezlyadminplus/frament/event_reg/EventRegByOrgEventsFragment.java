package com.softmeasures.eventezlyadminplus.frament.event_reg;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragEventRegByOrgEventsBinding;
import com.softmeasures.eventezlyadminplus.interfaces.FetchEventListener;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.Manager;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_LOCATION;

public class EventRegByOrgEventsFragment extends BaseFragment {

    private FragEventRegByOrgEventsBinding binding;
    private String type = "Township";
    private ArrayList<EventDefinition> eventDefinitions = new ArrayList<>();
    private ArrayList<EventDefinition> eventDefinitionsMain = new ArrayList<>();
    private EventsAdapter eventsAdapter;
    private String selectedEventCategory = "current";
    private boolean byType = false;
    private int eventLogiType = EVENT_TYPE_AT_LOCATION;
    private Manager manager;
    private String eventType = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            type = getArguments().getString("type", "Township");
            eventType = getArguments().getString("eventType", "");
            if (getArguments().containsKey("byType"))
                byType = getArguments().getBoolean("byType", false);
            if (getArguments().containsKey("eventLogiType"))
                eventLogiType = getArguments().getInt("eventLogiType", EVENT_TYPE_AT_LOCATION);
            if (getArguments().containsKey("selectedManager"))
                manager = new Gson().fromJson(getArguments().getString("selectedManager"), Manager.class);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_event_reg_by_org_events, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        fetchEvents();
    }

    private void fetchEvents() {
        binding.swipeRefreshLayout.setRefreshing(true);
        new fetchEventsCall(byType ? "_table/event_definitions" : "_table/event_definitions?filter=manager_type=" + type, new FetchEventListener() {
            @Override
            public void onSuccess(ArrayList<EventDefinition> objects) {
                binding.swipeRefreshLayout.setRefreshing(false);
                eventDefinitionsMain.clear();
                eventDefinitionsMain.addAll(objects);
                filterEvents();
            }

            @Override
            public void onFailed() {

            }
        }).execute();
    }

    private void filterEvents() {
        eventDefinitions.clear();
        eventsAdapter.notifyDataSetChanged();

        if (byType) {
            for (int i = 0; i < eventDefinitionsMain.size(); i++) {
                if (eventLogiType == eventDefinitionsMain.get(i).getEvent_logi_type()
                        && type.equalsIgnoreCase(eventDefinitionsMain.get(i).getManager_type())
                        && manager.getLot_manager().equalsIgnoreCase(eventDefinitionsMain.get(i).getCompany_name())
                        && eventType.equalsIgnoreCase(eventDefinitionsMain.get(i).getEvent_type())) {
                    if (eventDefinitionsMain.get(i).getEventCategory().equalsIgnoreCase(selectedEventCategory)) {
                        eventDefinitions.add(eventDefinitionsMain.get(i));
                    }
                }
            }
        } else {
            for (int i = 0; i < eventDefinitionsMain.size(); i++) {
                if (eventDefinitionsMain.get(i).getEventCategory().equalsIgnoreCase(selectedEventCategory)) {
                    eventDefinitions.add(eventDefinitionsMain.get(i));
                }
            }
        }

        if (eventDefinitions.size() == 0) {
            binding.tvError.setVisibility(View.VISIBLE);
        } else {
            binding.tvError.setVisibility(View.GONE);
            eventsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {
        binding.swipeRefreshLayout.setOnRefreshListener(this::fetchEvents);

        binding.rgEventStatus.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.rbUpcoming:
                    selectedEventCategory = "upcoming";
                    filterEvents();
                    break;
                case R.id.rbPast:
                    selectedEventCategory = "past";
                    filterEvents();
                    break;
                case R.id.rbCurrent:
                    selectedEventCategory = "current";
                    filterEvents();
                    break;
            }
        });
    }

    @Override
    protected void initViews(View v) {
        eventsAdapter = new EventsAdapter();
        binding.rvEvents.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        binding.rvEvents.setAdapter(eventsAdapter);
    }

    private class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventHolder> {

        @NonNull
        @Override
        public EventsAdapter.EventHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new EventsAdapter.EventHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_event_reg, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull EventsAdapter.EventHolder eventHolder, int i) {
            EventDefinition eventDefinition = eventDefinitions.get(i);
            if (eventDefinition != null) {
                if (eventDefinition.getEvent_name() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_name())) {
                    eventHolder.tvEventTitle.setText(eventDefinition.getEvent_name());
                }
                if (eventDefinition.getEvent_type() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_type())) {
                    eventHolder.tvEventType.setText(eventDefinition.getEvent_type());
                }
                Glide.with(getActivity()).load(eventDefinition.getEvent_logo()).placeholder(R.drawable.place_holder)
                        .error(R.drawable.place_holder).into(eventHolder.ivLogo);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    eventHolder.ivLogo.setClipToOutline(true);
                }

                if (eventDefinition.getEvent_address() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_address())
                        && !eventDefinition.getEvent_address().equals("null")) {
                    eventHolder.tvAddress.setText(eventDefinition.getEvent_address());
                    try {
                        JSONArray jsonArray = new JSONArray(eventDefinition.getEvent_address());
                        if (jsonArray.length() > 0) {
                            JSONArray jsonArray1 = jsonArray.getJSONArray(0);
                            if (jsonArray1.length() > 0) {
                                eventHolder.tvAddress.setText(jsonArray1.getString(0));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    eventHolder.tvAddress.setText("-");
                }
                if (eventDefinition.getCost() != null
                        && !TextUtils.isEmpty(eventDefinition.getCost())) {
                    if (eventDefinition.getCost().equals("0")) {
                        eventHolder.tvCost.setText("FREE");
                    } else {
                        eventHolder.tvCost.setText(String.format("$%s", eventDefinition.getCost()));
                    }
                } else {
                    eventHolder.tvCost.setText("FREE");
                }
                if (eventDefinition.getEvent_begins_date_time() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_begins_date_time())) {
                    eventHolder.tvEventDate.setText(DateTimeUtils.getDDMMM(eventDefinition.getEvent_begins_date_time()));
                } else {
                    eventHolder.tvEventDate.setText("-");
                }
                if (eventDefinition.getEvent_event_timings() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_event_timings())) {
                    try {
                        JSONArray jsonArrayTime = new JSONArray(eventDefinition.getEvent_event_timings());
                        StringBuilder stringBuilder = new StringBuilder();
                        if (jsonArrayTime != null && jsonArrayTime.length() > 0) {
                            JSONArray jsonArray2 = jsonArrayTime.getJSONArray(0);
                            if (jsonArray2 != null && jsonArray2.length() > 0) {
                                if (jsonArray2.get(0).toString().length() > 30) {
                                    String[] s = jsonArray2.get(0).toString().split(" - ");
                                    for (int j = 0; j < s.length; j++) {
                                        if (j != 0) {
                                            stringBuilder.append("-\n");
                                        }
                                        stringBuilder.append(DateTimeUtils.gmtToLocalDateAMPM(s[j]).substring(11));
                                    }
                                } else {
                                    stringBuilder.append(jsonArray2.get(0).toString());
                                }
                            }
                        }
                        eventHolder.tvEventTime.setText(stringBuilder.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    eventHolder.tvEventTime.setText("-");
                }
            }
        }

        @Override
        public int getItemCount() {
            return eventDefinitions.size();
        }

        class EventHolder extends RecyclerView.ViewHolder {
            TextView tvEventTitle, tvEventType, tvAddress, tvCost, tvEventDate, tvEventTime;
            ImageView ivLogo;

            EventHolder(@NonNull View itemView) {
                super(itemView);
                ivLogo = itemView.findViewById(R.id.ivLogo);
                tvEventTitle = itemView.findViewById(R.id.tvEventTitle);
                tvEventType = itemView.findViewById(R.id.tvEventType);
                tvAddress = itemView.findViewById(R.id.tvAddress);
                tvCost = itemView.findViewById(R.id.tvCost);
                tvEventDate = itemView.findViewById(R.id.tvEventDate);
                tvEventTime = itemView.findViewById(R.id.tvEventTime);

                itemView.setOnClickListener(v -> {
                    final int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        EventDefinition eventDefinition = eventDefinitions.get(pos);
                        item itemLocation = new item();
                        if (eventDefinition.getLocation_lat_lng() != null) {
                            try {
                                JSONArray jsonArrayLatLng = new JSONArray(eventDefinition.getLocation_lat_lng());
                                for (int j = 0; j < jsonArrayLatLng.length(); j++) {
                                    JSONArray jsonArray2 = jsonArrayLatLng.getJSONArray(j);
                                    for (int k = 0; k < jsonArray2.length(); k++) {
                                        String[] latLng = jsonArray2.getString(k).split(",");
                                        if (latLng != null && latLng.length > 1) {
                                            itemLocation.setLat(String.valueOf(Double.parseDouble(latLng[0])));
                                            itemLocation.setLng(String.valueOf(Double.parseDouble(latLng[1])));
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        EventSpotDetailsFragment frag = new EventSpotDetailsFragment();
                        Bundle bundle = new Bundle();
//                        bundle.putString("parkingType", parkingType);
                        bundle.putInt("eventId", eventDefinition.getId());
                        bundle.putString("eventDefinition", new Gson().toJson(eventDefinition));
                        bundle.putString("itemLocation", new Gson().toJson(itemLocation));
                        frag.setArguments(bundle);
                        replaceFragment(frag, "eventSpotDetails");
                    }
                });
            }
        }
    }
}
