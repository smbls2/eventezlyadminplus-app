package com.softmeasures.eventezlyadminplus.frament;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.melnykov.fab.FloatingActionButton;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.services.RecordingService;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static android.content.Context.MODE_PRIVATE;

public class frg_voices_add extends Fragment {
    static TextView txt_cancel, txt_save_audio, txt_proof_detail;
    private long startTime = 0L;
    private Handler customHandler = new Handler();
    private FloatingActionButton mRecordButton = null;
    private FloatingActionButton btn_play = null;
    private TextView mRecordingPrompt;
    private int mRecordPromptCount = 0;
    private boolean mStartRecording = true;
    private boolean mPauseRecording = true;
    private Chronometer mChronometer = null;
    private Handler mHandler = new Handler();
    private boolean isPlaying = false;
    long timeWhenPaused = 0;
    MediaPlayer mMediaPlayer = new MediaPlayer();

    private click_save_cancel listener;

    public interface click_save_cancel {
        public void onclick_save();
        public void onlick_cancel();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_voices, container, false);
        txt_cancel = (TextView) view.findViewById(R.id.txt_cancel_audio);
        txt_save_audio = (TextView) view.findViewById(R.id.txt_save_audio);
        mChronometer = (Chronometer) view.findViewById(R.id.chronometer);
        mRecordingPrompt = (TextView) view.findViewById(R.id.recording_status_text);
        mRecordButton = (FloatingActionButton) view.findViewById(R.id.btnRecord);
        btn_play = (FloatingActionButton) view.findViewById(R.id.btnplay);
        btn_play.setAlpha((float) 0.3);
        btn_play.setEnabled(false);

        mRecordButton.setColorNormal(getResources().getColor(R.color.primary));
        mRecordButton.setColorPressed(getResources().getColor(R.color.primary_dark));

        mRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecord(mStartRecording);
                mStartRecording = !mStartRecording;
            }
        });

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String is_new = bundle.getString("is_new");
            if (is_new.equals("yes")) {
                btn_play.setAlpha((float) 0.3);
                btn_play.setEnabled(false);
            } else {
                btn_play.setAlpha((float) 1.0);
                btn_play.setEnabled(true);
            }
        }


        txt_save_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onclick_save();
                getActivity().onBackPressed();
            }
        });

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        btn_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences getdatatt = getActivity().getSharedPreferences("file_url", MODE_PRIVATE);
                String filepath = getdatatt.getString("file_path", "null");

                if (mMediaPlayer.isPlaying()) {
                    btn_play.setImageResource(R.mipmap.ic_media_play);
                    mHandler.removeCallbacks(mRunnable);
                    mMediaPlayer.stop();
                    mMediaPlayer.reset();
                    mMediaPlayer.release();
                    mMediaPlayer = null;
                    mMediaPlayer = new MediaPlayer();
                } else {

                    updateSeekBar();
                    btn_play.setImageResource(R.mipmap.ic_media_pause);
                    try {
                        mMediaPlayer.setDataSource(filepath);
                        mMediaPlayer.prepare();
                        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                Log.e("start", "mp is start");
                                mMediaPlayer.start();
                            }
                        });
                    } catch (IOException e) {
                        Log.e("", "prepare() failed");
                    }

                    mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            btn_play.setImageResource(R.mipmap.ic_media_play);
                            mHandler.removeCallbacks(mRunnable);
                            mMediaPlayer.pause();
                        }
                    });

                }
            }
        });
        return view;
    }


    private void onRecord(boolean start) {
        Intent intent = new Intent(getActivity(), RecordingService.class);
        if (start) {
            mRecordButton.setImageResource(R.mipmap.ic_media_stop);
            Toast.makeText(getActivity(), R.string.toast_recording_start, Toast.LENGTH_SHORT).show();

            File folder = new File(Environment.getExternalStorageDirectory(), "SoundRecorder");
            if (!folder.exists()) {
                folder.mkdir();
            }

            mChronometer.setBase(SystemClock.elapsedRealtime());
            mChronometer.start();
            mChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
                @Override
                public void onChronometerTick(Chronometer chronometer) {
                    if (mRecordPromptCount == 0) {
                        mRecordingPrompt.setText(getString(R.string.record_in_progress) + ".");
                    } else if (mRecordPromptCount == 1) {
                        mRecordingPrompt.setText(getString(R.string.record_in_progress) + "..");
                    } else if (mRecordPromptCount == 2) {
                        mRecordingPrompt.setText(getString(R.string.record_in_progress) + "...");
                        mRecordPromptCount = -1;
                    }
                    mRecordPromptCount++;
                }
            });
            getActivity().startService(intent);
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            mRecordingPrompt.setText(getString(R.string.record_in_progress) + ".");
            mRecordPromptCount++;
        } else {
            mRecordButton.setImageResource(R.mipmap.ic_mic_white_36dp);
            String jhbc = String.valueOf(mChronometer.getText());
            mChronometer.stop();
            mChronometer.setText(jhbc);
            mChronometer.setBase(SystemClock.elapsedRealtime());
            timeWhenPaused = 0;
            mRecordingPrompt.setText(getString(R.string.record_prompt));
            btn_play.setAlpha((float) 1.0);
            btn_play.setEnabled(true);
            txt_save_audio.setAlpha((float) 1.0);
            txt_save_audio.setEnabled(true);
            getActivity().stopService(intent);
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            mChronometer.setText(jhbc);
        }
    }

    private void updateSeekBar() {
        mHandler.postDelayed(mRunnable, 100);
    }

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (mMediaPlayer != null) {
                int mCurrentPosition = mMediaPlayer.getCurrentPosition();
                long minutes = TimeUnit.MILLISECONDS.toMinutes(mCurrentPosition);
                long seconds = TimeUnit.MILLISECONDS.toSeconds(mCurrentPosition)
                        - TimeUnit.MINUTES.toSeconds(minutes);
                mChronometer.setText(String.format("%01d:%01d", minutes, seconds));
                updateSeekBar();
            }
        }
    };
    public void registerForListener(click_save_cancel listener) {
        this.listener = listener;
    }
}
