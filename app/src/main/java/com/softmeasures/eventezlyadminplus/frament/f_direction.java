package com.softmeasures.eventezlyadminplus.frament;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.BounceAnimation;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class f_direction extends Fragment {
    private static final String LOG_TAG = "ExampleApp";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyC6UabVHdti4zlU6E1iajVuNi6ZdKhDK5g";
    double latitude = 0.0, longitude = 0.0;
    ConnectionDetector cd;
    GoogleMap googleMap;
    MarkerOptions options;
    RelativeLayout rl_progressbar;
    ProgressBar progressBar;
    ArrayList<item> directionarray = new ArrayList<>();
    EditText edit_destition_address, edit_location, btn_destination, txt_current_location;
    ListView list_destination, list_location;
    TextView txt_search, txt_search_location;
    RelativeLayout rl_editname, rl_edit_location;
    TextView txt_get_direction, txt_start_location, txt_end_location;
    boolean opendes = false, valuesfound = false, opendes1 = false;
    String adrreslocation = "null", addressti, addressin, addressti1, addressin1;
    String addrsstitle = "null", addresssub, addresstodirectioncar, locationname1 = "null";
    ArrayList<item> searcharray = new ArrayList<>();
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    ArrayList<item> oldarraylist1 = new ArrayList<>();
    String lat, lang, selectmode, curatlat, curtlang, endlag, endlat, cu, en;
    String origin[] = {"driving", "walking", "bicycling", "transit", "bus", "subway", "train", "tram", "rail"};
    boolean clikondestionation = false, sp_oring_click = false, clickondirection = false, clicknewwithstart = false;
    String source_lat = "0.0", source_lang = "0.0", des_lat = "0.0", des_lang = "0.0";
    ArrayList<item> searchplace = new ArrayList<>();
    AutoCompleteTextView autoCompView_des, autoCompView_sor;
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private Handler mHandler = null;
    private Runnable mAnimation;
    ArrayList<item> bottom_menu = new ArrayList<>();
    String click_on_move = "";
    private RecyclerView horizontal_recycler_view;
    CustomAdapter horizontalAdapter;

    private FrameLayout frameLayout;
    public static boolean is_delays = false;
    ArrayList<item> menu_drivezly = new ArrayList<>();
    public static boolean is_click_atob = false;
    public static boolean is_click_close_up = false;
    LinearLayout ll_add_more_destination;
    RelativeLayout rl_more_destionation_add;
    String address_title_des = "", address_full_des = "";
    public static ArrayList<item> add_more_destination_array = new ArrayList<>();
    AutoCompleteTextView autoCompleteTextView_more_destion;
    TextView txt_add;
    int update_position_id = 0;
    PopupWindow popupmanaged;

    public static ArrayList<item> autocomplete(String input) {
        ArrayList<item> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            //sb.append("&components=country:gr");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<item>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                String jj = predsJsonArray.getJSONObject(i).getString("structured_formatting");
                JSONObject jjj = new JSONObject(jj);
                String main_address = jjj.getString("main_text");
                System.out.println("================================" + main_address + "============================");
                String cghcbdc = predsJsonArray.getJSONObject(i).getString("description");
                if (cghcbdc.contains(",")) {
                    String street = cghcbdc.substring(0, cghcbdc.indexOf(","));
                    String addrress = cghcbdc.substring(cghcbdc.indexOf(",") + 1);
                    item ii = new item();
                    ii.setLoationname(main_address);
                    ii.setAddress(cghcbdc);
                    resultList.add(ii);
                }

                //resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    ListView listView_drizly;
    RelativeLayout rl_menu;
    boolean is_menu = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View view = inflater.inflate(R.layout.fragment_direction, container, false);
        currentlocation();
        mHandler = new Handler();
        frameLayout = (FrameLayout) view.findViewById(R.id.child_fragment_container);
        horizontal_recycler_view = (RecyclerView) view.findViewById(R.id.list_bottom);
        RelativeLayout rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        autoCompView_des = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteTextView);
        autoCompView_sor = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteTextView1);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        list_destination = (ListView) view.findViewById(R.id.list_destinatin);
        list_location = (ListView) view.findViewById(R.id.list_location);
        rl_editname = (RelativeLayout) view.findViewById(R.id.rl_srart_location_search);
        rl_edit_location = (RelativeLayout) view.findViewById(R.id.rl_end_location);
        btn_destination = (EditText) view.findViewById(R.id.edit_destination1);
        txt_end_location = (TextView) view.findViewById(R.id.btn_destination);
        txt_start_location = (TextView) view.findViewById(R.id.sp_current);
        edit_destition_address = (EditText) view.findViewById(R.id.edit_destination);
        edit_location = (EditText) view.findViewById(R.id.edit_location);
        txt_search = (TextView) view.findViewById(R.id.txt_search1);
        txt_search_location = (TextView) view.findViewById(R.id.txt_search_location1);
        txt_current_location = (EditText) view.findViewById(R.id.edit_location1);
        txt_get_direction = (TextView) view.findViewById(R.id.txt_get_directions);
        final ListView list_orign = (ListView) view.findViewById(R.id.list_origin);
        final TextView sp_ori = (TextView) view.findViewById(R.id.sp_curren);
        SharedPreferences transit = getActivity().getSharedPreferences("transit", Context.MODE_PRIVATE);
        String transitclick = transit.getString("transit", "no");
        RelativeLayout rl_top = (RelativeLayout) view.findViewById(R.id.top);
        ll_add_more_destination = (LinearLayout) view.findViewById(R.id.ll_view_more_deatination);
        rl_more_destionation_add = (RelativeLayout) view.findViewById(R.id.rl_add_moew_destination);
        autoCompleteTextView_more_destion = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteTextView_more);
        rl_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        final RelativeLayout rl_dealy = (RelativeLayout) view.findViewById(R.id.rl_delay);


        TextView txt_add_more = (TextView) view.findViewById(R.id.txt_add_more);
        TextView txt_view = (TextView) view.findViewById(R.id.txt_view);
        TextView txt_can = (TextView) view.findViewById(R.id.txt_can);
        txt_add = (TextView) view.findViewById(R.id.txt_add);
        RelativeLayout rl_close = (RelativeLayout) view.findViewById(R.id.rl_close);

        txt_add_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_add.setText("Add");
                autoCompleteTextView_more_destion.setText("");
                rl_more_destionation_add.setVisibility(View.VISIBLE);
            }
        });

        rl_more_destionation_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        rl_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rl_more_destionation_add.setVisibility(View.GONE);
            }
        });

        txt_can.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rl_more_destionation_add.setVisibility(View.GONE);
            }
        });

        txt_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rl_more_destionation_add.setVisibility(View.GONE);
                String str_text = txt_add.getText().toString();
                if (str_text.equals("Add")) {
                    new fetchLatLongFromService_more(address_full_des).execute();
                } else {
                    new fetchLatLongFromService_more_update(address_full_des).execute();
                }
            }
        });

        txt_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Show_more_Destination(getActivity(), add_more_destination_array);
            }
        });

        final ListView listView_drivezly = (ListView) view.findViewById(R.id.list_driezly_menu);
        menu_drivezly = DrivEzly();


        listView_drivezly.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Bundle b = new Bundle();
                    b.putString("is_gas", "no");
                    frameLayout.setVisibility(View.VISIBLE);
                    Fragment ChildAlbumFragment = new nearby();
                    ChildAlbumFragment.setArguments(b);
                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                    transaction.add(R.id.child_fragment_container, ChildAlbumFragment).commit();
                } else if (position == 1) {
                    frameLayout.setVisibility(View.VISIBLE);
                    Fragment ChildAlbumFragment = new watherinfo();
                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                    transaction.add(R.id.child_fragment_container, ChildAlbumFragment).commit();
                } else if (position == 2) {
                    Bundle b = new Bundle();
                    b.putString("is_gas", "yes");
                    frameLayout.setVisibility(View.VISIBLE);
                    Fragment ChildAlbumFragment = new nearby();
                    ChildAlbumFragment.setArguments(b);
                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                    transaction.add(R.id.child_fragment_container, ChildAlbumFragment).commit();
                } else if (position == 3) {
                    frameLayout.setVisibility(View.VISIBLE);
                    Fragment ChildAlbumFragment = new f_search_places();
                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                    transaction.add(R.id.child_fragment_container, ChildAlbumFragment).commit();
                }
            }
        });
        rl_dealy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_delays) {
                    is_delays = false;
                    rl_dealy.setBackgroundColor(Color.parseColor("#00000000"));
                    try {
                        show_direction parentFragment = (show_direction) getChildFragmentManager().findFragmentById(R.id.child_fragment_container);
                        if (parentFragment != null) {
                            parentFragment.chnage_map_data_and_view(click_on_move);
                        }
                    } catch (Exception e) {
                    }
                } else {
                    is_delays = true;
                    rl_dealy.setBackgroundColor(Color.parseColor("#7AD875"));
                    try {
                        show_direction parentFragment = (show_direction) getChildFragmentManager().findFragmentById(R.id.child_fragment_container);
                        if (parentFragment != null) {
                            parentFragment.chnage_map_data_and_view(click_on_move);
                        }
                    } catch (Exception e) {
                    }
                }
            }
        });
        TextView txt_gps = (TextView) view.findViewById(R.id.txt_gps);
        txt_gps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_on_move = "map_apps";
                change_data(false);
            }
        });

        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        if (transitclick.equals("yes")) {
            transit.edit().clear().commit();
            sp_ori.setText("transit");
            String origin[] = {"transit", "walking", "bicycling", "driving", "bus", "subway", "train", "tram", "rail"};
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_origin, R.id.textcity, origin);
            list_orign.setAdapter(spinnerArrayAdapter);
        } else {
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_origin, R.id.textcity, origin);
            list_orign.setAdapter(spinnerArrayAdapter);
        }


        SharedPreferences sh1 = getActivity().getSharedPreferences("directiontomydirection", Context.MODE_PRIVATE);
        final String lat = sh1.getString("lat", "0.0");
        String lang = sh1.getString("lang", "0.0");

        if (!lat.equals("0.0") && !lang.equals("0.0")) {
            String add = sh1.getString("address", "null");
            addresssub = add;
            if (!add.equals("null")) {
                txt_end_location.setText(sh1.getString("address", ""));
                des_lat = lat;
                des_lang = lang;
            } else {

                //getlatandlangtoaddress(Double.parseDouble(lat), Double.parseDouble(lang));
                new fetchLatLongFromaddress_end(lat, lang).execute();
            }
            //
            SharedPreferences sh11 = getActivity().getSharedPreferences("directiontomydirection", Context.MODE_PRIVATE);
            sh11.edit().clear().commit();
        }
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        cd = new ConnectionDetector(getActivity());

        final Bundle bundle = this.getArguments();
        if (bundle != null) {
            click_on_move = bundle.getString("clikc_on_map");
        }

        if (cd.isConnectingToInternet()) {
            txt_end_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list_location.setVisibility(View.GONE);
                    if (opendes == false) {

                        Resources rs;
                        CustomAdaptercity adpater = new CustomAdaptercity(getActivity(), oldarraylist1, rs = getResources());
                        list_destination.setAdapter(adpater);
                        rl_editname.setVisibility(View.GONE);
                        opendes = true;
                        list_destination.setVisibility(View.VISIBLE);

                    } else {
                        opendes = false;
                        list_destination.setVisibility(View.GONE);
                    }
                }
            });

            txt_current_location.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() > 0) {
                        if (clikondestionation == false) {
                            txt_search_location.setVisibility(View.VISIBLE);
                        } else {
                            clikondestionation = false;
                            txt_search_location.setVisibility(View.GONE);
                        }
                    } else {
                        txt_search_location.setVisibility(View.GONE);
                    }

                }
            });

            txt_start_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list_destination.setVisibility(View.GONE);
                    if (opendes1 == false) {
                        Resources rs;
                        CustomAdaptercity1 adpater = new CustomAdaptercity1(getActivity(), oldarraylist1, rs = getResources());
                        list_location.setAdapter(adpater);
                        rl_editname.setVisibility(View.GONE);
                        opendes1 = true;
                        list_location.setVisibility(View.VISIBLE);
                    } else {
                        opendes1 = false;
                        list_location.setVisibility(View.GONE);
                    }
                }
            });
            sp_ori.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (sp_oring_click == false) {
                        sp_oring_click = true;
                        list_orign.setVisibility(View.VISIBLE);
                    } else {
                        sp_oring_click = false;
                        list_orign.setVisibility(View.GONE);
                    }
                }
            });

            list_orign.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selectmode = origin[position];
                    Log.e("orgin", selectmode);
                    sp_ori.setText(selectmode);
                    selectmode.toLowerCase();
                    if (selectmode.equals("bus")) {
                        selectmode = "transit-bus";
                    } else if (selectmode.equals("subway")) {
                        selectmode = "transit-subway";
                    } else if (selectmode.equals("train")) {
                        selectmode = "transit-train";
                    } else if (selectmode.equals("tram")) {
                        selectmode = "transit-tram";
                    } else if (selectmode.equals("rail")) {
                        selectmode = "transit-rail";
                    }
                    list_orign.setVisibility(View.GONE);
                    sp_oring_click = false;

                }
            });
            autoCompView_des.setAdapter(new CustomAutoplace(getActivity(), getResources()));
            autoCompleteTextView_more_destion.setAdapter(new CustomAutoplace(getActivity(), getResources()));
            autoCompView_des.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    addrsstitle = searchplace.get(position).getLoationname();
                    addresssub = searchplace.get(position).getAddress();
                    txt_end_location.setText(addresssub);
                    rl_editname.setVisibility(View.GONE);
                    autoCompView_des.setVisibility(View.GONE);
                    ll_add_more_destination.setVisibility(View.VISIBLE);
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                    new fetchLatLongFromService(addresssub).execute();
                }
            });

            autoCompView_sor.setAdapter(new CustomAutoplace(getActivity(), getResources()));
            autoCompView_sor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    addrsstitle = searchplace.get(position).getLoationname();
                    addresssub = searchplace.get(position).getAddress();
                    txt_start_location.setText(addresssub);
                    rl_edit_location.setVisibility(View.GONE);
                    autoCompView_sor.setVisibility(View.GONE);
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                    new fetchLatLongFromService1(addresssub).execute();

                }
            });


            autoCompleteTextView_more_destion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    address_title_des = searchplace.get(position).getLoationname();
                    address_full_des = searchplace.get(position).getAddress();
                    autoCompleteTextView_more_destion.setText(address_full_des);
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);


                }
            });


            String temvalues[] = {"Nearby Places", "Weather", "Fuel", "Place Search"};
            Integer tempimg[] = {R.mipmap.icon_place, R.mipmap.icon_weather, R.mipmap.icon_fuel, R.mipmap.p_search};
            String list_bottom[] = {"Hybrid", "A to B", "Close up", "Details", "Map & Details", "App", "Nearby Places", "Weather", "Fuel", "Place Search", "From current"};
            int img[] = {R.mipmap.icon_hybrid_view, R.mipmap.icon_atob, R.mipmap.icon_close_up, R.mipmap.icon_details, R.mipmap.icon_map_detail, R.mipmap.icon_gps, R.mipmap.icon_place, R.mipmap.icon_weather, R.mipmap.icon_fuel, R.mipmap.p_search, R.mipmap.icon_directions2};
            bottom_menu.clear();
            for (int i = 0; i < list_bottom.length; i++) {
                item ii = new item();
                ii.setName(list_bottom[i]);
                ii.setImg_id(img[i]);
                ii.setSelected_parking(false);
                bottom_menu.add(ii);
            }

            horizontalAdapter = new CustomAdapter(bottom_menu);
            LinearLayoutManager horizontalLayoutManagaer
                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            horizontal_recycler_view.setLayoutManager(horizontalLayoutManagaer);
            horizontal_recycler_view.setAdapter(horizontalAdapter);


            txt_get_direction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    change_data(true);
                }
            });

            // getaddress1(latitude, longitude);

            new fetchLatLongFromaddress(String.valueOf(latitude), String.valueOf(longitude)).execute();


            source_lat = String.valueOf(latitude);
            source_lang = String.valueOf(longitude);
            showmap();

        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }
        return view;
    }

    public void showmap() {
        final SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        fm.getMapAsync(googleMap1 -> {
            googleMap = googleMap1;
            try {
                mapFragment = ((SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map));
                LatLng position = new LatLng(latitude, longitude);
                Marker melbourne = googleMap.addMarker(new MarkerOptions()
                        .position(position));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 12));
                final long start = SystemClock.uptimeMillis();
                final long duration = 2000L;

                // Cancels the previous animation
                mHandler.removeCallbacks(mAnimation);
                mAnimation = new BounceAnimation(start, duration, melbourne, mHandler);
                mHandler.post(mAnimation);
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                }
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                map = googleMap;
            } catch (Exception e) {
                map = googleMap;
            }
        });

    }

    public void currentlocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Failed to fetch user location!");
            alertDialog.setMessage("Please enable user location for app, location is required for app to work properly");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            alertDialog.show();
        }
    }

    private void getaddress1(Double lat, Double lon) {
        Geocoder gc = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = gc.getFromLocation(lat, lon, 1);
            StringBuilder sb = new StringBuilder();
            StringBuilder addressl = new StringBuilder();
            StringBuilder country = new StringBuilder();
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getLatitude()).append("\n");
                    sb.append(address.getLongitude());
                    addressl.append(address.getAddressLine(0)).append(", ");
                    addressl.append(address.getAddressLine(1));
                    country.append(address.getAddressLine(2));
                    break;
                }
                String locationAddress = sb.toString();
                String loc = addressl.toString();
                Log.e("addrrss", loc);
                String countyname = country.toString();
                addressti1 = loc.substring(0, loc.indexOf(","));
                addressin1 = loc.substring(loc.indexOf(",") + 1);
                if (countyname.equals("null")) {
                    this.adrreslocation = addressti1 + ", " + addressin1;
                } else {
                    this.adrreslocation = addressti1 + "," + addressin1 + "," + countyname;
                }
                Resources rs;
                txt_start_location.setText(this.adrreslocation);
                rl_editname.setVisibility(View.GONE);
                list_location.setVisibility(View.GONE);
                if (!adrreslocation.equals("null")) {
                    new getlocation().execute();
                }
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Address not found!");
                alertDialog.setMessage("Could not find location. Try including the exact location information");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public class fetchLatLongFromaddress extends AsyncTask<Void, Void, StringBuilder> {

        String lat1, lang1;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        public fetchLatLongFromaddress(String lat, String lang) {
            super();
            this.lat1 = lat;
            this.lang1 = lang;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {

                //  new getsearchotherlocation().execute();
                //
                // ();

                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat1 + "," + lang1 + "&key=AIzaSyBPQK7umomGsX2lJ-23BnK2Gh8-Q6yekZU";

                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
                if (rl_progressbar != null)
                    rl_progressbar.setVisibility(View.GONE);

                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");

                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray
                            .getJSONObject(0);
                    adrreslocation = before_geometry_jsonObj.getString("formatted_address");
                    JSONObject geometry_jsonObj = before_geometry_jsonObj
                            .getJSONObject("geometry");

                    JSONObject location_jsonObj = geometry_jsonObj
                            .getJSONObject("location");

                    lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);

                    lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);


                    addressti1 = adrreslocation.substring(0, adrreslocation.indexOf(","));
                    addressin1 = adrreslocation.substring(adrreslocation.indexOf(",") + 1);
                    Resources rs;
                    txt_start_location.setText(adrreslocation);
                    rl_editname.setVisibility(View.GONE);
                    list_location.setVisibility(View.GONE);
                    new getlocation().execute();

                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public class fetchLatLongFromaddress_end extends AsyncTask<Void, Void, StringBuilder> {

        String lat1, lang1;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        public fetchLatLongFromaddress_end(String lat, String lang) {
            super();
            this.lat1 = lat;
            this.lang1 = lang;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {

                //  new getsearchotherlocation().execute();
                //
                // ();

                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat1 + "," + lang1 + "&key=AIzaSyBPQK7umomGsX2lJ-23BnK2Gh8-Q6yekZU";
                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {

                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");

                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray
                            .getJSONObject(0);
                    addresstodirectioncar = before_geometry_jsonObj.getString("formatted_address");
                    JSONObject geometry_jsonObj = before_geometry_jsonObj
                            .getJSONObject("geometry");

                    JSONObject location_jsonObj = geometry_jsonObj
                            .getJSONObject("location");

                    lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);

                    lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);


                    addressti1 = addresstodirectioncar.substring(0, addresstodirectioncar.indexOf(","));
                    addressin1 = addresstodirectioncar.substring(addresstodirectioncar.indexOf(",") + 1);
                    locationname1 = addressin1.substring(addressin1.indexOf(",") + 1);

                    txt_end_location.setText(addresstodirectioncar);

                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void getlatandlangtoaddress(Double lat, Double lon) {
        Geocoder gc = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = gc.getFromLocation(lat, lon, 1);
            StringBuilder sb = new StringBuilder();
            StringBuilder addressl = new StringBuilder();
            StringBuilder country = new StringBuilder();
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getLatitude()).append("\n");
                    sb.append(address.getLongitude());
                    addressl.append(address.getAddressLine(0)).append(", ");
                    addressl.append(address.getAddressLine(1));
                    country.append(address.getAddressLine(2));
                    break;
                }
                String loc = addressl.toString();
                String countyname = country.toString();
                addressti1 = loc.substring(0, loc.indexOf(","));
                addressin1 = loc.substring(loc.indexOf(",") + 1);
                locationname1 = addressin1.substring(addressin1.indexOf(",") + 1);
                if (countyname.equals("null")) {
                    this.addresstodirectioncar = addressti1 + ", " + addressin1;
                } else {
                    this.addresstodirectioncar = addressti1 + "," + addressin1 + "," + countyname;
                }
                txt_end_location.setText(this.addresstodirectioncar);

            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Address not found!");
                alertDialog.setMessage("Could not find location. Try including the exact location information");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alertDialog.show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                Log.i("dcnd", "Place: " + place.getName());
                LatLng latlang = place.getLatLng();
                lat = String.valueOf(latlang.latitude);
                lang = String.valueOf(latlang.longitude);
                addrsstitle = String.valueOf(place.getName());
                addresssub = String.valueOf(place.getAddress());
                if (clicknewwithstart) {
                    txt_start_location.setText(addrsstitle + " , " + addresssub);
                } else {
                    txt_end_location.setText(addrsstitle + " , " + addresssub);
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                // TODO: Handle the error.
                Log.i("eroor_place", status.getStatusMessage());

            } else if (resultCode == Activity.RESULT_CANCELED) {
            }
        }
    }

    public class CustomAutoplace extends BaseAdapter implements View.OnClickListener, Filterable {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAutoplace(Activity a, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;

            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.layout, null);
                /****** View Holder Object to contain tabitem.xml file elements ******/
                holder = new ViewHolder();

                holder.txt_commerical_name = (TextView) vi.findViewById(R.id.txt_name);
                holder.txt_deatil = (TextView) vi.findViewById(R.id.address);
                /************  Set holder with LayoutInflater ************/
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }
            if (data.size() <= 0) {


            } else {
                try {
                    tempValues = null;
                    tempValues = (item) data.get(position);
                    holder.txt_commerical_name.setText(tempValues.getLoationname());
                    holder.txt_deatil.setText(tempValues.getAddress());
                    int k = 0;
                } catch (IndexOutOfBoundsException e) {

                } catch (Exception e) {
                }

            }

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        data = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults

                        filterResults.values = data;
                        filterResults.count = data.size();
                        searchplace.clear();
                        searchplace.addAll(data);
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        try {
                            notifyDataSetChanged();
                        } catch (Exception e) {
                        }

                    } else {
                        try {
                            notifyDataSetInvalidated();
                        } catch (Exception e) {
                        }

                    }
                }
            };
            return filter;

        }


        public class ViewHolder {

            public TextView txt_commerical_name, txt_deatil, txt_pricing;
            RelativeLayout rldata;
        }
    }

    public class fetchLatLongFromService extends AsyncTask<Void, Void, StringBuilder> {
        String place;

        public fetchLatLongFromService(String place) {
            super();
            this.place = place;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?address="
                        + this.place + "&key=AIzaSyBPQK7umomGsX2lJ-23BnK2Gh8-Q6yekZU";
                googleMapUrl = googleMapUrl.replaceAll(" ", "%20");
                URL url = new URL(googleMapUrl);
                URLConnection c = url.openConnection();
                c.setConnectTimeout(5000);
                c.setReadTimeout(10000);
                c.getInputStream();
                InputStreamReader in = new InputStreamReader(
                        c.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray
                            .getJSONObject(0);

                    JSONObject geometry_jsonObj = before_geometry_jsonObj
                            .getJSONObject("geometry");

                    JSONObject location_jsonObj = geometry_jsonObj
                            .getJSONObject("location");

                    lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);

                    lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);
                    DecimalFormat df = new DecimalFormat("0.000000");
                    Double l = lng;
                    lang = df.format(l);
                    LatLng point = new LatLng(law, lng);
                    des_lang = lang;
                    des_lat = lat;
                } else {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                rl_progressbar.setVisibility(View.GONE);
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (Exception e) {
                rl_progressbar.setVisibility(View.GONE);
            }

        }

    }

    public class fetchLatLongFromService1 extends AsyncTask<Void, Void, StringBuilder> {
        String place;

        fetchLatLongFromService1(String place) {
            super();
            this.place = place;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?address="
                        + this.place + "&key=AIzaSyBPQK7umomGsX2lJ-23BnK2Gh8-Q6yekZU";
                googleMapUrl = googleMapUrl.replaceAll(" ", "%20");
                URL url = new URL(googleMapUrl);
                URLConnection c = url.openConnection();
                c.setConnectTimeout(5000);
                c.setReadTimeout(10000);
                c.getInputStream();
                InputStreamReader in = new InputStreamReader(
                        c.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray
                            .getJSONObject(0);

                    JSONObject geometry_jsonObj = before_geometry_jsonObj
                            .getJSONObject("geometry");

                    JSONObject location_jsonObj = geometry_jsonObj
                            .getJSONObject("location");

                    lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);

                    lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);
                    DecimalFormat df = new DecimalFormat("0.000000");
                    Double l = lng;
                    lang = df.format(l);
                    LatLng point = new LatLng(law, lng);
                    source_lang = lang;
                    source_lat = lat;

                } else {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                rl_progressbar.setVisibility(View.GONE);
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (Exception e) {
                rl_progressbar.setVisibility(View.GONE);
            }
        }

    }

    public class adddestination extends AsyncTask<String, String, String> {
        String ss, res_id;
        JSONObject json;
        String addvehicleurl = "_table/user_locations";
        String currentlocation, endlocation;

        public adddestination(String currentlocation, String endlocation) {
            this.currentlocation = currentlocation;
            this.endlocation = endlocation;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            directionarray.clear();
            SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
            String user_id = logindeatl.getString("id", "0");
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("lng", des_lang);
            jsonValues.put("lat", des_lat);
            jsonValues.put("user_id", Integer.parseInt(user_id));
            if (!addrsstitle.equals("null")) {
                jsonValues.put("location_name", addrsstitle);
                addresssub = addresssub.replaceFirst(" ", "");
                jsonValues.put("location_address", addresssub);
            } else {
                addresssub = addresssub.replaceFirst(" ", "");
                jsonValues.put("location_name", addresssub);
                jsonValues.put("location_address", addresssub);
            }
            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + addvehicleurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = null;
            HttpPut put = null;
            post = new HttpPost(url);
            Log.d("ccc", json.toString());
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item iii = new item();
                        JSONObject c = array.getJSONObject(i);
                        res_id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ss = String.valueOf(response.getStatusLine());
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && !res_id.equals("null")) {
                SharedPreferences ss = getActivity().getSharedPreferences("show_direction", Context.MODE_PRIVATE);
                SharedPreferences.Editor ed = ss.edit();
                ed.putString("current", cu);
                ed.putString("des", en);
                ed.putString("current_lat", currentlocation);
                ed.putString("des_lat", endlocation);
                ed.putString("mode", selectmode);
                ed.commit();
                Bundle b = new Bundle();
                b.putString("click", click_on_move);
                show_direction sd = new show_direction();
                sd.setArguments(b);
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
             /*   ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.replace(R.id.My_Container_1_ID,sd, "NewFragmentTag");
                ft.commit();*/

               /* ft.add(R.id.My_Container_1_ID, sd);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(sd);
                ft.commit();*/

                frameLayout.setVisibility(View.VISIBLE);
                Fragment ChildAlbumFragment = new show_direction();
                ChildAlbumFragment.setArguments(b);
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.child_fragment_container, ChildAlbumFragment).commit();
            }
            super.onPostExecute(s);
        }
    }

    public class CustomAdaptercity extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdaptercity(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {
                vi = inflater.inflate(R.layout.destinationad, null);
                holder = new ViewHolder();
                holder.txt_address = (TextView) vi.findViewById(R.id.text_address);
                holder.txt_address_title = (TextView) vi.findViewById(R.id.text_address_title);
                holder.txt_lat = (TextView) vi.findViewById(R.id.txt_lat);
                holder.txt_lang = (TextView) vi.findViewById(R.id.txt_lng);
                /************  Set holder with LayoutInflater ************/
                vi.setTag(holder);

                vi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String address = holder.txt_address_title.getText().toString();
                        if (address.equals("Enter New Address")) {
                            rl_edit_location.setVisibility(View.VISIBLE);
                            autoCompView_des.setVisibility(View.VISIBLE);
                            autoCompView_des.setText("");
                            clicknewwithstart = false;
                            list_destination.setVisibility(View.GONE);
                            ll_add_more_destination.setVisibility(View.GONE);
                        } else {
                            if (v != null) {
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                            }
                            clikondestionation = true;
                            addrsstitle = address;
                            String address1 = holder.txt_address.getText().toString();
                            addresssub = address1;
                            address1.replaceFirst("\\s++$", "");
                            address.replaceFirst("\\s++$", "");
                            addrsstitle.replaceFirst("\\s++$", "");
                            addresssub.replaceFirst("\\s++$", "");
                            addrsstitle = addrsstitle.replaceAll("(\n)", "%0A");
                            addresssub = addresssub.replaceAll("(\n)", "%0A");
                            des_lat = holder.txt_lat.getText().toString();
                            des_lang = holder.txt_lang.getText().toString();
                            txt_end_location.setText(address1);
                            list_destination.setVisibility(View.GONE);
                            rl_edit_location.setVisibility(View.GONE);
                            ll_add_more_destination.setVisibility(View.VISIBLE);
                        }
                    }
                });

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                holder.txt_address_title.setText(tempValues.getTitle_address());
                String d = tempValues.getTitle_address();
                if (!d.equals("null")) {
                    vi.setVisibility(View.VISIBLE);
                    holder.txt_address_title.setText(tempValues.getTitle_address());
                    if (d.equals("Enter New Address")) {
                        vi.setBackgroundColor(Color.parseColor("#90caf9"));
                        holder.txt_address.setText("");
                    } else {
                        if (tempValues.isclcik()) {
                            vi.setBackgroundColor(Color.parseColor("#c5000068"));
                        } else {
                            vi.setBackgroundColor(Color.parseColor("#c5000068"));
                        }
                        if (tempValues.getAddress().equals("null")) {
                            holder.txt_address.setText("");
                        } else {
                            holder.txt_address.setText(tempValues.getAddress());
                        }
                    }

                    holder.txt_lat.setText(tempValues.getLat());
                    holder.txt_lang.setText(tempValues.getLang());
                } else {
                    vi.setVisibility(View.GONE);
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txt_address_title, txt_address, txt_lat, txt_lang;
        }
    }

    public class CustomAdaptercity1 extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdaptercity1(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.destinationad, null);
                holder = new ViewHolder();

                holder.txt_address = (TextView) vi.findViewById(R.id.text_address);
                holder.txt_address_title = (TextView) vi.findViewById(R.id.text_address_title);
                holder.txt_lat = (TextView) vi.findViewById(R.id.txt_lat);
                holder.txt_lang = (TextView) vi.findViewById(R.id.txt_lng);
                vi.setTag(holder);

                vi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String address = holder.txt_address_title.getText().toString();

                        if (address.equals("Enter New Address")) {

                            clicknewwithstart = true;
                            list_location.setVisibility(View.GONE);
                            rl_editname.setVisibility(View.VISIBLE);
                            autoCompView_sor.setVisibility(View.VISIBLE);
                            autoCompView_sor.setText("");
                            txt_start_location.setText("Select start address");
                            list_location.setVisibility(View.GONE);
                        } else {
                            addrsstitle = address;
                            clikondestionation = true;
                            String address1 = holder.txt_address.getText().toString();
                            addresssub = address1;
                            address1.replaceFirst("\\s++$", "");
                            address.replaceFirst("\\s++$", "");
                            addrsstitle.replaceFirst("\\s++$", "");
                            addresssub.replaceFirst("\\s++$", "");
                            addrsstitle = addrsstitle.replaceAll("(\n)", "%0A");
                            addresssub = addresssub.replaceAll("(\n)", "%0A");
                            txt_start_location.setText(address1);
                            source_lat = holder.txt_lat.getText().toString();
                            source_lang = holder.txt_lang.getText().toString();
                            list_location.setVisibility(View.GONE);
                            rl_editname.setVisibility(View.GONE);
                        }
                    }
                });
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                String d = tempValues.getTitle_address();
                if (!d.equals("null")) {
                    vi.setVisibility(View.VISIBLE);
                    holder.txt_address_title.setText(tempValues.getTitle_address());
                    if (d.equals("Enter New Address")) {
                        vi.setBackgroundColor(Color.parseColor("#90caf9"));
                        holder.txt_address.setText("");
                    } else {
                        if (tempValues.isclcik()) {
                            vi.setBackgroundColor(Color.parseColor("#c5000068"));
                        } else {
                            vi.setBackgroundColor(Color.parseColor("#c5000068"));
                        }
                        if (tempValues.getAddress().equals("null")) {
                            holder.txt_address.setText("");
                        } else {
                            holder.txt_address.setText(tempValues.getAddress());
                        }
                    }
                    holder.txt_lat.setText(tempValues.getLat());
                    holder.txt_lang.setText(tempValues.getLang());

                } else {
                    vi.setVisibility(View.GONE);
                }
            }
            return vi;
        }


        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txt_address_title, txt_address, txt_lat, txt_lang;


        }
    }

    public class getlocation extends AsyncTask<String, String, String> {
        JSONObject json = null;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "0");
        String vehiclenourl = "_table/user_locations?filter=(user_id%3D" + user_id + ")";
        String res_id;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            directionarray.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    item ii = new item();
                    ii.setTitle_address("Enter New Address");
                    ii.setAddress("null");
                    ii.setIsclcik(false);
                    oldarraylist1.add(ii);

                    String addd = adrreslocation.substring(0, adrreslocation.indexOf(","));
                    String tit = adrreslocation.substring(adrreslocation.indexOf(",") + 1);
                    String hh = tit.substring(tit.indexOf(" ") + 1);


                    item cu = new item();
                    cu.setTitle_address(addd);
                    cu.setAddress(hh);
                    cu.setLat(String.valueOf(latitude));
                    cu.setLang(String.valueOf(longitude));
                    cu.setIsclcik(true);
                    oldarraylist1.add(cu);
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item iii = new item();
                        JSONObject c = array.getJSONObject(i);
                        res_id = c.getString("id");
                        String address = c.getString("location_address");
                        String addressname = c.getString("location_name");
                        iii.setTitle_address(addressname);
                        iii.setAddress(address);
                        iii.setLat(c.getString("lat"));
                        iii.setLang(c.getString("lng"));
                        ii.setIsclcik(false);
                        oldarraylist1.add(iii);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity == null) {
                } else {
                    if (json != null) {
                        CustomAdaptercity1 adpater = new CustomAdaptercity1(getActivity(), oldarraylist1, rs = getResources());
                        list_location.setAdapter(adpater);
                    } else {
                        item ii = new item();
                        ii.setTitle_address("Enter New Address");
                        ii.setAddress("null");
                        ii.setIsclcik(false);
                        oldarraylist1.add(ii);
                        CustomAdaptercity1 adpater = new CustomAdaptercity1(getActivity(), oldarraylist1, rs = getResources());
                        list_location.setAdapter(adpater);
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class checklocation extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "0");
        String street, address = "", currentlocation = "", endlocation = "";
        JSONArray array = new JSONArray();

        public checklocation(String street, String location, String currentlocation, String endlocation) {
            this.street = street;
            this.address = location;
            this.currentlocation = currentlocation;
            this.endlocation = endlocation;
        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            HttpResponse response = null;
            try {
                String add = "null";
                if (!address.equals("") || !address.equals("null")) {
                    address = address.replaceAll("(\n)", "%0A");
                    add = address.replaceAll(" ", "%20");
                } else {
                    add = "null";
                }
                String street1 = "null";
                if (!street.equals("") || !street.equals("null")) {
                    street = street.replaceAll("(\n)", "%0A");
                    street1 = street.replaceAll(" ", "%20");
                } else {
                    street1 = "null";
                }
                String vechiclenourl = "_table/user_locations?filter=(location_name%3D" + street1 + ")%20AND%20(location_address%3D" + add + ")%20AND%20(user_id%3D" + id + ")";
                String url = getString(R.string.api) + getString(R.string.povlive) + vechiclenourl;
                Log.e("check_location_url", url);
                DefaultHttpClient client = new DefaultHttpClient();
                HttpGet post = new HttpGet(url);
                post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
                post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);

            if (getActivity() != null && json != null) {
                if (click_on_move.equals("map_apps")) {
                    try {
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=" + currentlocation + "&daddr=" + endlocation + ""));
                        startActivity(intent);
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), "Google map not found", Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (array.length() > 0) {

                        SharedPreferences ss = getActivity().getSharedPreferences("show_direction", Context.MODE_PRIVATE);
                        SharedPreferences.Editor ed = ss.edit();
                        ed.putString("current", cu);
                        ed.putString("des", en);
                        ed.putString("current_lat", currentlocation);
                        ed.putString("des_lat", endlocation);
                        ed.putString("mode", selectmode);
                        ed.commit();
                        Bundle b = new Bundle();
                        b.putString("click", click_on_move);
                   /*     ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.replace(R.id.My_Container_1_ID, sd, "NewFragmentTag");
                        ft.commitAllowingStateLoss();*/
                     /*   ft.add(R.id.My_Container_1_ID, sd);
                        fragmentStack.lastElement().onPause();
                        ft.hide(fragmentStack.lastElement());
                        fragmentStack.push(sd);
                        ft.commit();*/
                        if (click_on_move.equals("hybrid view") || click_on_move.equals("delay") || click_on_move.equals("Close up") || click_on_move.equals("atob")) {
                           /* frameLayout.setVisibility(View.VISIBLE);
                            Fragment ChildAlbumFragment = new show_transit();
                            ChildAlbumFragment.setArguments(b);
                            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                            transaction.add(R.id.child_fragment_container, ChildAlbumFragment).commit();*/

                            frameLayout.setVisibility(View.VISIBLE);
                            Fragment ChildAlbumFragment = new show_direction();
                            ChildAlbumFragment.setArguments(b);
                            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                            transaction.add(R.id.child_fragment_container, ChildAlbumFragment).commit();
                        } else {
                            frameLayout.setVisibility(View.VISIBLE);
                            Fragment ChildAlbumFragment = new show_direction();
                            ChildAlbumFragment.setArguments(b);
                            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                            transaction.add(R.id.child_fragment_container, ChildAlbumFragment).commit();
                        }
                    } else {
                        new adddestination(currentlocation, endlocation).execute();
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

        private ArrayList<item> dataSet;

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView textViewName;

            ImageView imageViewIcon;
            RelativeLayout rl_view_click;

            public MyViewHolder(View itemView) {
                super(itemView);
                this.textViewName = (TextView) itemView.findViewById(R.id.txt_name);
                //this.textViewVersion = (TextView) itemView.findViewById(R.id.textViewVersion);
                this.imageViewIcon = (ImageView) itemView.findViewById(R.id.imgt9);
                this.rl_view_click = (RelativeLayout) itemView.findViewById(R.id.rl_item_click);


            }
        }

        public CustomAdapter(ArrayList<item> data) {
            this.dataSet = data;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_menu_direction_bottom_part, parent, false);

            //view.setOnClickListener(MainActivity.myOnClickListener);

            MyViewHolder myViewHolder = new MyViewHolder(view);
            return myViewHolder;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

            TextView textViewName = holder.textViewName;
            // TextView textViewVersion = holder.textViewVersion;
            ImageView imageView = holder.imageViewIcon;
            if (dataSet.get(listPosition).isSelected_parking()) {
                holder.rl_view_click.setBackgroundColor(Color.parseColor("#7AD875"));
            } else {
                holder.rl_view_click.setBackgroundColor(Color.parseColor("#000000"));
            }
            textViewName.setText(dataSet.get(listPosition).getName());
            //textViewVersion.setText(dataSet.get(listPosition).getVersion());
            imageView.setImageResource(dataSet.get(listPosition).getImg_id());


            holder.rl_view_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < bottom_menu.size(); i++) {
                        bottom_menu.get(i).setSelected_parking(false);
                        //dataSet.get(i).setSelected_parking(false);

                    }
                    //dataSet.get(listPosition).setSelected_parking(true);
                    bottom_menu.get(listPosition).setSelected_parking(true);
                    notifyDataSetChanged();
                    String name1 = bottom_menu.get(listPosition).getName();
                    if (name1.equals("From current")) {
                        is_click_atob = false;
                        is_click_close_up = false;
                        frameLayout.setVisibility(View.GONE);
                    } else if (name1.equals("Nearby Places")) {
                        is_click_close_up = false;
                        is_click_atob = false;
                        Bundle b = new Bundle();
                        b.putString("is_gas", "no");
                        b.putString("is_menu_click", "no");
                        frameLayout.setVisibility(View.VISIBLE);
                        Fragment ChildAlbumFragment = new nearby();
                        ChildAlbumFragment.setArguments(b);
                        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                        transaction.add(R.id.child_fragment_container, ChildAlbumFragment).commit();
                        return;
                    } else if (name1.equals("Weather")) {
                        is_click_close_up = false;
                        is_click_atob = false;
                        frameLayout.setVisibility(View.VISIBLE);
                        Fragment ChildAlbumFragment = new watherinfo();
                        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                        transaction.add(R.id.child_fragment_container, ChildAlbumFragment).commit();
                        return;
                    } else if (name1.equals("Fuel")) {
                        is_click_close_up = false;
                        is_click_atob = false;
                        Bundle b = new Bundle();
                        b.putString("is_gas", "yes");
                        b.putString("is_menu_click", "no");
                        frameLayout.setVisibility(View.VISIBLE);
                        Fragment ChildAlbumFragment = new nearby();
                        ChildAlbumFragment.setArguments(b);
                        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                        transaction.add(R.id.child_fragment_container, ChildAlbumFragment).commit();
                        return;
                    } else if (name1.equals("Place Search")) {
                        is_click_atob = false;
                        frameLayout.setVisibility(View.VISIBLE);
                        Fragment ChildAlbumFragment = new f_search_places();
                        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                        transaction.add(R.id.child_fragment_container, ChildAlbumFragment).commit();
                        return;
                    } else {
                        for (int i = 0; i < bottom_menu.size(); i++) {
                            boolean dd = bottom_menu.get(i).isSelected_parking();
                            if (dd) {
                                String name = bottom_menu.get(i).getName();
                                if (name.equals("App")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "map_apps";
                                    change_data1(true);
                                    return;
                                } else if (name.equals("Close up")) {
                                    is_click_atob = false;
                                    if (is_click_close_up) {
                                        is_click_close_up = false;

                                    } else {
                                        is_click_close_up = true;
                                    }
                                    click_on_move = "Close up";
                                } else if (name.equals("A to B")) {
                                    is_click_close_up = false;
                                    if (is_click_atob) {
                                        is_click_atob = false;

                                    } else {
                                        is_click_atob = true;
                                    }
                                    click_on_move = "atob";
                                } else if (name.equals("Map & Details")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "map_detail";
                                } else if (name.equals("Details")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "detail";
                                } else if (name.equals("Delays")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "delay";
                                } else if (name.equals("Hybrid")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "hybrid view";
                                } else {
                                    is_click_close_up = false;
                                    is_click_atob = false;
                                    click_on_move = "atob";
                                }

                                //Toast.makeText(getActivity(),String.valueOf(dd),Toast.LENGTH_LONG).show();
                                break;
                            } else {
                                click_on_move = "atob";
                            }
                        }
                        // change_data(true);
                        try {
                            show_direction parentFragment = (show_direction) getChildFragmentManager().findFragmentById(R.id.child_fragment_container);
                          /*  Fragment ChildAlbumFragment = new show_direction();
                            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                            transaction.add(R.id.child_fragment_container, ChildAlbumFragment).commit();
                            show_direction dd=new show_direction();
                            dd.chnage_map_data_and_view(click_on_move);*/
                            if (parentFragment != null) {
                                parentFragment.chnage_map_data_and_view(click_on_move);
                            }
                        } catch (Exception e) {
                            frameLayout.setVisibility(View.GONE);
                        }

                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return dataSet.size();
        }
    }


    public void change_data(boolean is_direct) {
        cu = txt_start_location.getText().toString();
        en = txt_end_location.getText().toString();
        if (cd.isConnectingToInternet()) {
            if (!cu.equals("") && !en.equals("Select End Address")) {
                clickondirection = true;
                if (!source_lat.equals("") && !source_lang.equals("") && !des_lat.equals("") && !des_lang.equals("")) {
                    final String curat = source_lat + "," + source_lang;
                    final String endla = des_lat + "," + des_lang;

                    if (is_direct) {
                        for (int i = 0; i < bottom_menu.size(); i++) {
                            boolean dd = bottom_menu.get(i).isSelected_parking();
                            if (dd) {
                                String name = bottom_menu.get(i).getName();
                                if (name.equals("App")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "map_apps";
                                    change_data1(true);
                                    return;
                                } else if (name.equals("Close up")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "Close up";
                                } else if (name.equals("A to B")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "atob";
                                } else if (name.equals("Map & Details")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "map_detail";
                                } else if (name.equals("Details")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "detail";
                                } else if (name.equals("Delays")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "delay";
                                } else if (name.equals("Hybrid")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "hybrid view";
                                } else {
                                    for (int ii = 0; ii < bottom_menu.size(); ii++) {
                                        String name1 = bottom_menu.get(ii).getName();
                                        is_click_atob = false;
                                        is_click_close_up = false;
                                        if (name1.equals("A to B")) {
                                            bottom_menu.get(ii).setSelected_parking(true);
                                        } else {
                                            bottom_menu.get(ii).setSelected_parking(false);
                                        }
                                        //dataSet.get(i).setSelected_parking(false);

                                    }
                                    horizontalAdapter.notifyDataSetChanged();
                                    click_on_move = "atob";
                                }

                                //Toast.makeText(getActivity(),String.valueOf(dd),Toast.LENGTH_LONG).show();
                                break;
                            } else {
                                for (int ii = 0; ii < bottom_menu.size(); ii++) {
                                    String name = bottom_menu.get(ii).getName();
                                    if (name.equals("A to B")) {
                                        bottom_menu.get(ii).setSelected_parking(true);
                                    } else {
                                        bottom_menu.get(ii).setSelected_parking(false);
                                    }
                                    //dataSet.get(i).setSelected_parking(false);

                                }
                                horizontalAdapter.notifyDataSetChanged();
                                click_on_move = "atob";
                            }
                        }
                    }


                    new checklocation(addrsstitle, addresssub, curat, endla).execute();
                }

            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Incomplete Addresses");
                alertDialog.setMessage("Please select Destination address to proceed");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alertDialog.show();
            }
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }
    }

    public void change_data1(boolean is_direct) {
        cu = txt_start_location.getText().toString();
        en = txt_end_location.getText().toString();
        if (cd.isConnectingToInternet()) {
            if (!cu.equals("") && !en.equals("Select End Address")) {
                clickondirection = true;
                if (!source_lat.equals("") && !source_lang.equals("") && !des_lat.equals("") && !des_lang.equals("")) {
                    final String curat = source_lat + "," + source_lang;
                    final String endla = des_lat + "," + des_lang;

                    if (is_direct) {
                        for (int i = 0; i < bottom_menu.size(); i++) {
                            boolean dd = bottom_menu.get(i).isSelected_parking();
                            if (dd) {
                                String name = bottom_menu.get(i).getName();
                                if (name.equals("App")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "map_apps";
                                    try {
                                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=" + cu + "&daddr=" + en + ""));
                                        startActivity(intent);
                                    } catch (Exception e) {
                                        Toast.makeText(getActivity(), "Google map not found", Toast.LENGTH_LONG).show();
                                    }
                                    break;
                                } else if (name.equals("Close up")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "Close up";
                                } else if (name.equals("A to B")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "atob";
                                } else if (name.equals("Map & Details")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "map_detail";
                                } else if (name.equals("Details")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "detail";
                                } else if (name.equals("Delays")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "delay";
                                } else if (name.equals("Hybrid")) {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "hybrid view";
                                } else {
                                    is_click_atob = false;
                                    is_click_close_up = false;
                                    click_on_move = "atob";
                                }

                                //Toast.makeText(getActivity(),String.valueOf(dd),Toast.LENGTH_LONG).show();
                                break;
                            } else {
                                is_click_atob = false;
                                is_click_close_up = false;
                                click_on_move = "atob";
                            }
                        }
                    }


                }

            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Incomplete Addresses");
                alertDialog.setMessage("Please select Destination address to proceed");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alertDialog.show();
            }
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }
    }

    public ArrayList<item> DrivEzly() {
        ArrayList<item> temp_values = new ArrayList<>();
        String temvalues[] = {"Nearby Places", "Weather", "Fuel", "Place Search"};
        Integer tempimg[] = {R.mipmap.icon_place, R.mipmap.icon_weather, R.mipmap.icon_fuel, R.mipmap.p_search};
        for (int i = 0; i < temvalues.length; i++) {
            item ii = new item();
            ii.setMenu_title(temvalues[i].toString());
            ii.setMenu_img_id(tempimg[i].intValue());
            temp_values.add(ii);
        }
        return temp_values;
    }

    public class CustomAdapterMenu extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdapterMenu(Activity a, ArrayList<item> d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;
            data = d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txt_title;
            ImageView img;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;


            if (convertView == null) {

                vi = inflater.inflate(R.layout.adapter_menu, null);
                /****** View Holder Object to contain tabitem.xml file elements ******/

                holder = new ViewHolder();
                holder.img = (ImageView) vi.findViewById(R.id.imgloginfindparking);
                holder.txt_title = (TextView) vi.findViewById(R.id.loginfindparking);


                /************  Set holder with LayoutInflater ************/
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {


            } else {
                Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Thin.ttf");
                holder.txt_title.setTypeface(type);
                holder.txt_title.setText(data.get(position).getMenu_title());
                holder.img.setBackgroundResource(data.get(position).getMenu_img_id());

            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

    public void show_end_address(String lat, String lang) {
        frameLayout.setVisibility(View.GONE);
        for (int i = 0; i < bottom_menu.size(); i++) {
            bottom_menu.get(i).setSelected_parking(false);
            //dataSet.get(i).setSelected_parking(false);

        }
        //dataSet.get(listPosition).setSelected_parking(true);
        bottom_menu.get(bottom_menu.size() - 1).setSelected_parking(true);
        horizontalAdapter.notifyDataSetChanged();
        new fetchLatLongFromaddress_end(lat, lang).execute();
    }


    public class fetchLatLongFromService_more extends AsyncTask<Void, Void, StringBuilder> {
        String place;

        fetchLatLongFromService_more(String place) {
            super();
            this.place = place;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?address="
                        + this.place + "&key=AIzaSyBPQK7umomGsX2lJ-23BnK2Gh8-Q6yekZU";
                googleMapUrl = googleMapUrl.replaceAll(" ", "%20");
                URL url = new URL(googleMapUrl);
                URLConnection c = url.openConnection();
                c.setConnectTimeout(5000);
                c.setReadTimeout(10000);
                c.getInputStream();
                InputStreamReader in = new InputStreamReader(
                        c.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray
                            .getJSONObject(0);

                    JSONObject geometry_jsonObj = before_geometry_jsonObj
                            .getJSONObject("geometry");

                    JSONObject location_jsonObj = geometry_jsonObj
                            .getJSONObject("location");

                    String lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);

                    String lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);
                    DecimalFormat df = new DecimalFormat("0.000000");
                    Double l = lng;
                    lang = df.format(l);
                    LatLng point = new LatLng(law, lng);
                    // source_lang = lang;
                    //  source_lat = lat;
                    item ii = new item();
                    ii.setTitle_address(address_title_des);
                    ii.setAddress(address_full_des);
                    ii.setLat(lat);
                    ii.setLang(String.valueOf(lng));
                    add_more_destination_array.add(ii);

                    Show_more_Destination(getActivity(), add_more_destination_array);

                } else {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                rl_progressbar.setVisibility(View.GONE);
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (Exception e) {
                rl_progressbar.setVisibility(View.GONE);
            }
        }

    }

    public class fetchLatLongFromService_more_update extends AsyncTask<Void, Void, StringBuilder> {
        String place;

        fetchLatLongFromService_more_update(String place) {
            super();
            this.place = place;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?address="
                        + this.place + "&key=AIzaSyBPQK7umomGsX2lJ-23BnK2Gh8-Q6yekZU";
                googleMapUrl = googleMapUrl.replaceAll(" ", "%20");
                URL url = new URL(googleMapUrl);
                URLConnection c = url.openConnection();
                c.setConnectTimeout(5000);
                c.setReadTimeout(10000);
                c.getInputStream();
                InputStreamReader in = new InputStreamReader(
                        c.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray
                            .getJSONObject(0);

                    JSONObject geometry_jsonObj = before_geometry_jsonObj
                            .getJSONObject("geometry");

                    JSONObject location_jsonObj = geometry_jsonObj
                            .getJSONObject("location");

                    String lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);

                    String lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);
                    DecimalFormat df = new DecimalFormat("0.000000");
                    Double l = lng;
                    lang = df.format(l);
                    LatLng point = new LatLng(law, lng);
                    // source_lang = lang;
                    //  source_lat = lat;
                   /* item ii=new item();
                    ii.setTitle_address(address_title_des);
                    ii.setAddress(address_full_des);
                    ii.setLat(lat);
                    ii.setLang(String.valueOf(lng));*/
                    if (add_more_destination_array.size() > 0) {
                        add_more_destination_array.get(update_position_id).setTitle_address(address_title_des);
                        add_more_destination_array.get(update_position_id).setAddress(address_full_des);
                        add_more_destination_array.get(update_position_id).setLat(lat);
                        add_more_destination_array.get(update_position_id).setLang(String.valueOf(lng));
                    }

                    Show_more_Destination(getActivity(), add_more_destination_array);

                } else {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                rl_progressbar.setVisibility(View.GONE);
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (Exception e) {
                rl_progressbar.setVisibility(View.GONE);
            }
        }

    }

    private void Show_more_Destination(Activity context, final ArrayList<item> data) {

        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.popup_add_more_destination, viewGroup, false);
        popupmanaged = new PopupWindow(context);
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        //  popupmanaged.setAnimationStyle(R.style.animationName);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);

        RelativeLayout rl_close = (RelativeLayout) layout.findViewById(R.id.rl_close);
        RelativeLayout rl_add = (RelativeLayout) layout.findViewById(R.id.rl_add);
        ListView list_destination = (ListView) layout.findViewById(R.id.list_more_destination);
        list_destination.setAdapter(new CustomAdapter_more_destination(getActivity(), data, getResources()));
        rl_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupmanaged.dismiss();
            }
        });

        rl_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupmanaged.dismiss();
                txt_add.setText("Add");
                autoCompleteTextView_more_destion.setText("");
                rl_more_destionation_add.setVisibility(View.VISIBLE);
            }
        });


        list_destination.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (data.size() > 0) {
                    popupmanaged.dismiss();
                    update_position_id = i;
                    txt_add.setText("Update");
                    autoCompleteTextView_more_destion.setText(data.get(i).getAddress());
                    rl_more_destionation_add.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public class CustomAdapter_more_destination extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdapter_more_destination(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {
                vi = inflater.inflate(R.layout.item_more_destination, null);
                holder = new ViewHolder();
                holder.txt_address = (TextView) vi.findViewById(R.id.text_address);
                holder.txt_address_title = (TextView) vi.findViewById(R.id.text_address_title);
                holder.txt_lat = (TextView) vi.findViewById(R.id.txt_lat);
                holder.txt_lang = (TextView) vi.findViewById(R.id.txt_lng);
                holder.rl_view = (RelativeLayout) vi.findViewById(R.id.rl_view);
                holder.rl_delete = (RelativeLayout) vi.findViewById(R.id.rl_delete);
                holder.txt_view_alert = (TextView) vi.findViewById(R.id.txt_view_alert);
                /************  Set holder with LayoutInflater ************/
                vi.setTag(holder);

                holder.txt_view_alert.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        rl_more_destionation_add.setVisibility(View.VISIBLE);
                        txt_add.setText("Add");
                        autoCompleteTextView_more_destion.setText("");
                        popupmanaged.dismiss();
                    }
                });

                holder.rl_delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setMessage("Are you sure you want to delete this destination location?");
                        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                add_more_destination_array.remove(position);
                                notifyDataSetChanged();
                            }
                        });
                        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        alertDialog.show();
                    }
                });
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
                holder.rl_view.setVisibility(View.GONE);
                holder.txt_view_alert.setVisibility(View.VISIBLE);
            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                holder.rl_view.setVisibility(View.VISIBLE);
                holder.txt_view_alert.setVisibility(View.GONE);
                holder.txt_address_title.setText(tempValues.getTitle_address());
                String d = tempValues.getTitle_address();
                if (!d.equals("null")) {
                    vi.setVisibility(View.VISIBLE);
                    holder.txt_address_title.setText(tempValues.getTitle_address());
                    holder.txt_address.setText(tempValues.getAddress());
                    holder.txt_lat.setText(tempValues.getLat());
                    holder.txt_lang.setText(tempValues.getLang());
                } else {
                    vi.setVisibility(View.GONE);
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txt_address_title, txt_address, txt_lat, txt_lang, txt_view_alert;
            RelativeLayout rl_view, rl_delete;

        }
    }

}
