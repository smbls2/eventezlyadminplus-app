package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softmeasures.eventezlyadminplus.MyApplication;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.panstreetview;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.java.BounceAnimation;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.LocationLot;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class parkezly_parking extends BaseFragment {
    private static String TAG = "parkezly_parking";
    Double latitude = 0.0, longitude = 0.0;
    TextView txttitle, txttoday, txtmax, txtstreetview, txtdrections, txtaddress, txttime, txtmanaged, txt_main_title, txt_park_here, txt_no_parking, txt_parkin_time, txt_custom_notices;
    TextView txt_lost;
    String this_day, active = "null", title = "", html, address, pricing = "null", pricing_duration = "null", max_hours, time_rules, cuuretaddres, state_cont = "", loginid, duration_unit = "";
    String start_hour, parking_time, no_parking_time, notices;
    String end_hour, location_code, marker, locationnamne, zip_code, city, state, id, township_code;
    String lat1, lang1;
    ArrayList<item> parking_rules = new ArrayList<>();
    ArrayList<item> filtermanaged = new ArrayList<>();
    RelativeLayout rl_progressbar;
    private Handler mHandler = null;
    private Runnable mAnimation;
    String township_row, township_spaces, township_labeling, township_sapce_marked, township_location_name = "", location_id;
    ArrayList<item> managedlostpoparray = new ArrayList<>();
    ArrayList<item> parked_car = new ArrayList<>();
    ArrayList<Long> datediffent = new ArrayList<Long>();
    String adrreslocation;
    private RelativeLayout rl_main;
    private String parkingType = "";
    private ArrayList<LocationLot> locationLots = new ArrayList<>();
    private LocationLot selectedLocationLot;
    private boolean isReserveSpot = false;
    private String manager_type_id = "";
    private boolean mIsParkingAllowed = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        if (getArguments() != null) {
            parkingType = getArguments().getString("parkingType");
            isReserveSpot = getArguments().getBoolean("isReserveSpot", false);
        }
        currentlocation();
        mHandler = new Handler();

        SharedPreferences appSharedPrefs = getActivity().getSharedPreferences("parking_rules", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = appSharedPrefs.getString("MyObject", "");
        String json1 = appSharedPrefs.getString("managed", "");
        Log.e(TAG, "  managed: " + json1);
        title = appSharedPrefs.getString("title", "");
        address = appSharedPrefs.getString("address", "");
        marker = appSharedPrefs.getString("marker", "").toLowerCase();
        html = appSharedPrefs.getString("html", "");
        lat1 = appSharedPrefs.getString("lat", "");
        lang1 = appSharedPrefs.getString("lang", "");
        township_code = appSharedPrefs.getString("township_code", "");
        location_code = appSharedPrefs.getString("location_code", "");
        parking_time = appSharedPrefs.getString("parking_time", "6 AM- 10 PM (M-F),\n7 AM- 6 PM (Sat-Sun)");
        no_parking_time = appSharedPrefs.getString("no_parking_time", "1 AM - 6 AM");
        notices = appSharedPrefs.getString("notices", "");
        location_id = appSharedPrefs.getString("location_id", "");
        manager_type_id = appSharedPrefs.getString("manager_type_id", "");

        Type type1 = new TypeToken<ArrayList<item>>() {
        }.getType();
        parking_rules = gson.fromJson(json, type1);
        filtermanaged = gson.fromJson(json1, type1);
        return inflater.inflate(R.layout.parkezly_parking, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        new fetchLatLongFromaddress(String.valueOf(latitude), String.valueOf(longitude)).execute();
        showmap();
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    private String getlatandlangtoaddress(Double lat, Double lon) {
        Geocoder gc = new Geocoder(getActivity(), Locale.getDefault());
        String addresstodirectioncar = "";
        try {
            List<Address> addresses = gc.getFromLocation(lat, lon, 1);
            StringBuilder sb = new StringBuilder();
            StringBuilder addressl = new StringBuilder();
            StringBuilder country = new StringBuilder();
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getLatitude()).append("\n");
                    sb.append(address.getLongitude());
                    addressl.append(address.getAddressLine(0)).append(", ");
                    addressl.append(address.getAddressLine(1));
                    country.append(address.getAddressLine(2));
                    break;
                }

                String loc = addressl.toString();
                String countyname = country.toString();
                if (countyname.equals("null")) {
                    addresstodirectioncar = loc;
                } else {
                    addresstodirectioncar = loc + " , " + countyname;
                }
                Resources rs;


            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Address not found!");
                alertDialog.setMessage("Could not find location. Try including the exact location information");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alertDialog.show();
            }
        } catch (IOException e) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Address not found!");
            alertDialog.setMessage("Could not find location. Try including the exact location information");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();
            e.printStackTrace();
        }
        return addresstodirectioncar;
    }

    @Override
    protected void updateViews() {
        if (isReserveSpot) {
            txt_park_here.setText("Reserve");
        }
        txt_main_title.setText(title);
        txtaddress.setText(address);
        if (marker.equals("managed paid") || marker.equals("managed free")) {
            txt_lost.setVisibility(View.VISIBLE);
        } else {
            txt_lost.setVisibility(View.GONE);
        }

        if (parking_rules.size() > 0) {
            for (int k = 0; k < parking_rules.size(); k++) {
                String loccode = parking_rules.get(k).getLoationcode1();
                if (location_code.equals(loccode)) {
                    Log.e(TAG, "   parkezly_parking:   location_code: " + loccode);
                    Log.e(TAG, "    parkezly_parking:  parkingRule: " + new Gson().toJson(parking_rules.get(k)));
                    start_hour = parking_rules.get(k).getStart_hour();
                    end_hour = parking_rules.get(k).getEnd_hour();
                    this_day = parking_rules.get(k).getThis_day();
                    active = parking_rules.get(k).getActive();
                    pricing = parking_rules.get(k).getPricing();
                    pricing_duration = parking_rules.get(k).getPricing_duration();
                    time_rules = parking_rules.get(k).getTime_rule();
                    max_hours = parking_rules.get(k).getMax_time();
                    locationnamne = parking_rules.get(k).getLoationname();
                    //zip_code = parking_rules.get(k).getZip_code();
                    //city = parking_rules.get(k).getCity();
                    duration_unit = parking_rules.get(k).getMax_hours();
                    id = parking_rules.get(k).getId();
                    //parking_time = parking_rules.get(k).getParking_times();
                    //  no_parking_time = parking_rules.get(k).getNo_parking_times();
                    //  notices = parking_rules.get(k).getCustom_notice();

                    if (!start_hour.equals("null") && !end_hour.equals("null") && !this_day.equals("null")) {

                        Calendar c = Calendar.getInstance();
                        int hour = c.get(Calendar.HOUR_OF_DAY);
                        SimpleDateFormat sdf = new SimpleDateFormat("EEE");
                        Date d = new Date();
                        String today = sdf.format(d).toLowerCase();
                        int start_hours = Integer.parseInt(start_hour);
                        int end_hours = Integer.parseInt(end_hour);

                        if (this_day.equalsIgnoreCase("everyday")) {
                            //1. Parking allowed
                            mIsParkingAllowed = true;
                            Log.e("#DEBUG", "   1.  Parking allowed");
                        } else if (this_day.contains(today)) {
                            //2. Parking Allowed
                            mIsParkingAllowed = true;
                            Log.e("#DEBUG", "   2.  Parking allowed");
                        } else if (this_day.equalsIgnoreCase("weekdays")
                                && ((today.equalsIgnoreCase("mon")
                                || today.equalsIgnoreCase("tue")
                                || today.equalsIgnoreCase("wed")
                                || today.equalsIgnoreCase("thd")
                                || today.equalsIgnoreCase("fri")))) {
                            //3. Parking allowed
                            mIsParkingAllowed = true;
                            Log.e("#DEBUG", "   3.  Parking allowed");
                        } else if (this_day.equalsIgnoreCase("weekends")
                                && ((today.equalsIgnoreCase("sat")
                                || today.equalsIgnoreCase("sun")))) {
                            //4. Parking allowed
                            mIsParkingAllowed = true;
                            Log.e("#DEBUG", "   4.  Parking allowed");
                        } else {
                            //5. NOT allowed
                            mIsParkingAllowed = false;
                            Log.e("#DEBUG", "   5.  NO parking allowed");
                        }

                        if (mIsParkingAllowed) {
                            if (hour >= start_hours && hour <= end_hours) {
                                if (!active.equals("null")) {

                                    String pricing1 = pricing != null ? (!pricing.equals("0") ? (!pricing.equals("null") ? "$ " + pricing + " / " + pricing_duration + " " + duration_unit + "" : "FREE") : "FREE") : "FREE";
                                    if (pricing_duration != null && !TextUtils.isEmpty(pricing_duration)
                                            && !pricing_duration.equals("1")) {
                                        pricing1 = pricing1.replace("Hour", "Hours");
                                        pricing1 = pricing1.replace("Minute", "Minutes");
                                        pricing1 = pricing1.replace("Day", "Days");
                                        pricing1 = pricing1.replace("Week", "Weeks");
                                        pricing1 = pricing1.replace("Month", "Months");
                                    }
                                    txttitle.setText(pricing1);
                                    String time_rules1 = time_rules != null ? (!time_rules.equals("") ? time_rules : "") : "";
                                    txttime.setText(time_rules1);
                                    if (!active.equals("")) {
                                        txttoday.setBackgroundColor(Color.parseColor("#22B14C"));
                                        txttoday.setText("ALLOWED");
                                    } else {
                                        txttoday.setBackgroundColor(Color.parseColor("#FF7F27"));
                                        txttoday.setText("NO PARKING");
                                    }

                                    String max_hours1 = max_hours != null ? (!max_hours.equals("") ? (!max_hours.equals("null") ? max_hours + " " + duration_unit : "") : "") : "";
                                    if (max_hours != null && !TextUtils.isEmpty(max_hours)
                                            && !max_hours.equals("1")) {
                                        max_hours1 = max_hours1.replace("Hour", "Hours");
                                        max_hours1 = max_hours1.replace("Minute", "Minutes");
                                        max_hours1 = max_hours1.replace("Day", "Days");
                                        max_hours1 = max_hours1.replace("Week", "Weeks");
                                        max_hours1 = max_hours1.replace("Month", "Months");
                                    }
                                    txtmax.setText(max_hours1);
                               /* if (marker.equals("ez-managed")) {
                                    if (filtermanaged.size() > 0) {
                                        txtmanaged.setVisibility(View.VISIBLE);
                                    }
                                }*/

                                    String finaltime = parking_time != null ? (!parking_time.equals("") ? (!parking_time.equals("null") ? parking_time : "") : "") : "";
                                    String finalnoparking_time = no_parking_time != null ? (!no_parking_time.equals("") ? (!no_parking_time.equals("null") ? no_parking_time : "") : "") : "";
                                    txt_no_parking.setText(finalnoparking_time);
                                    txt_parkin_time.setText(finaltime);
                                    String notices1 = notices != null ? (!notices.equals("") ? (!notices.equals("null") ? notices : "") : "") : "";
                                    txt_custom_notices.setText(notices1);
                                } else {
                                    txttitle.setText("$0.50/30.0 mins");
                                    txtaddress.setText("NYC Mid Town\\nPark your vehicle with parkezly.");
                                    txttoday.setText("NO PARKING or ALLOWED");
                                    txttime.setText("12-Midnight");
                                    txtmax.setText("6 Hours");

                                }
                                break;
                            } else {
                                noParkingData(true);
                            }
                        } else {
                            noParkingData(false);
                        }
                    }
                }
            }
        }

        try {
            if (filtermanaged.size() > 0) {
                for (int i = 0; i < filtermanaged.size(); i++) {
                    township_labeling = filtermanaged.get(i).getLabeling();
                    township_row = filtermanaged.get(i).getTotal_rows();
                    township_spaces = filtermanaged.get(i).getTotal_spaces();
                    township_sapce_marked = "true";
                    township_location_name = filtermanaged.get(i).getLoationname();
                    township_location_name = filtermanaged.get(i).getLoationname();
                    break;
                }
            }
        } catch (Exception e) {
        }
    }

    private void noParkingData(boolean today) {
        txt_park_here.setVisibility(View.GONE);
        if (today) {
            txttime.setText(time_rules);
        } else txttime.setText("Not Allowed");
        txttoday.setText("Not Allowed");
        String max_hours1 = max_hours != null ? (!max_hours.equals("") ? (!max_hours.equals("null") ? max_hours + " " + duration_unit : "") : "") : "";
        if (max_hours != null && !TextUtils.isEmpty(max_hours)
                && !max_hours.equals("1")) {
            max_hours1 = max_hours1.replace("Hour", "Hours");
            max_hours1 = max_hours1.replace("Minute", "Minutes");
            max_hours1 = max_hours1.replace("Day", "Days");
            max_hours1 = max_hours1.replace("Week", "Weeks");
            max_hours1 = max_hours1.replace("Month", "Months");
        }
        txtmax.setText(max_hours1);

        String pricing1 = pricing != null ? (!pricing.equals("0") ? (!pricing.equals("null") ? "$ " + pricing + " / " + pricing_duration + " " + duration_unit + "" : "FREE") : "FREE") : "FREE";
        if (pricing_duration != null && !TextUtils.isEmpty(pricing_duration)
                && !pricing_duration.equals("1")) {
            pricing1 = pricing1.replace("Hour", "Hours");
            pricing1 = pricing1.replace("Minute", "Minutes");
            pricing1 = pricing1.replace("Day", "Days");
            pricing1 = pricing1.replace("Week", "Weeks");
            pricing1 = pricing1.replace("Month", "Months");
        }
        txttitle.setText(pricing1);
        String finaltime = parking_time != null ? (!parking_time.equals("") ? (!parking_time.equals("null") ? parking_time : "") : "") : "";
        String finalnoparking_time = no_parking_time != null ? (!no_parking_time.equals("") ? (!no_parking_time.equals("null") ? no_parking_time : "") : "") : "";
        txt_no_parking.setText(finalnoparking_time);
        txt_parkin_time.setText(finaltime);
        String notices1 = notices != null ? (!notices.equals("") ? (!notices.equals("null") ? notices : "") : "") : "";
        txt_custom_notices.setText(notices1);
    }

    @Override
    protected void setListeners() {
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        txt_lost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_progressbar.setVisibility(View.VISIBLE);
                if (filtermanaged.size() > 0) {
                    for (int i = 0; i < filtermanaged.size(); i++) {
                        township_labeling = filtermanaged.get(i).getLabeling();
                        township_row = filtermanaged.get(i).getTotal_rows();
                        township_spaces = filtermanaged.get(i).getTotal_spaces();
                        township_sapce_marked = "true";
                        township_location_name = filtermanaged.get(i).getLoationname();
                        break;
                    }
//                    if (isInteger(township_spaces) && isInteger(township_row)) {
//                        int total_spces = Integer.parseInt(township_spaces);
//                        int total_row = Integer.parseInt(township_row);
//                        int rowcount = 1;
//                        if (total_row > 0 && total_spces > 0) {
//                            if (total_spces >= total_row) {
//                                rowcount = Integer.parseInt(township_spaces) / Integer.parseInt(township_row);
//                                if ((total_spces % total_row) > 0) {
//                                }
//                            } else {
//                                rowcount = 1;
//                            }
//                        }
//
//                        int curren_row = 0;
//                        int first_row = 1;
//                        String row[] = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
//                        ArrayList<item> array = new ArrayList<item>();
//                        int array_count = 0;
//                        if (township_sapce_marked.equals("false")) {
//                            array_count = total_row;
//                        } else {
//                            array_count = total_spces;
//                        }
//                        //township_labeling="AlphaNumeric";
//                        if (township_labeling.equals("Alpha")) {
//
//                            for (int i = 0; i < array_count; i++) {
//
//                                item ii = new item();
//                                curren_row++;
//                                if (curren_row == rowcount) {
//                                    if (township_sapce_marked.equals("false")) {
//                                        ii.setDisplay(row[first_row - 1].toString());
//                                    } else if (total_row < 2) {
//                                        ii.setDisplay(row[first_row - 1].toString());
//                                    } else {
//                                        ii.setDisplay(row[first_row - 1].toString() + " : " + row[curren_row - 1].toString());
//                                    }
//                                    ii.setIsparked("f");
//                                    ii.setSpaces_mark_reqd(township_sapce_marked);
//                                    ii.setRow_no(row[curren_row - 1].toString());
//                                    ii.setRoe_name(row[first_row - 1].toString());
//                                    //ii.setDisplay(row[first_row - 1].toString() + " : " + row[curren_row - 1].toString());
//                                    // ii.setDisplay(row[first_row - 1].toString());
//                                    ii.setMenu_img_id(total_row);
//                                    ii.setMarker("red");
//                                    array.add(ii);
//                                    first_row++;
//                                    curren_row = 0;
//                                } else {
//                                    ii.setSpaces_mark_reqd(township_sapce_marked);
//                                    ii.setIsparked("f");
//                                    ii.setRow_no(row[curren_row - 1].toString());
//                                    ii.setRoe_name(row[first_row - 1].toString());
//                                    ii.setMenu_img_id(total_row);
//                                    ii.setMarker("red");
//                                    if (township_sapce_marked.equals("false")) {
//                                        ii.setDisplay(row[first_row - 1].toString());
//                                    } else if (total_row < 2) {
//                                        ii.setDisplay(row[first_row - 1].toString());
//                                    } else {
//                                        ii.setDisplay(row[first_row - 1].toString() + " : " + row[curren_row - 1].toString());
//                                    }
//                                    //ii.setDisplay(row[first_row - 1].toString() + " : " + row[curren_row - 1].toString());
//                                    array.add(ii);
//                                }
//                            }
//
//                            for (int j = 0; j < array.size(); j++) {
//                                Log.e("row", array.get(j).toString());
//                            }
//                        } else if (township_labeling.equals("Numeric")) {
//
//
//                            for (int i = 0; i < array_count; i++) {
//                                item ii = new item();
//                                curren_row++;
//                                if (curren_row == rowcount) {
//                                    if (township_sapce_marked.equals("false")) {
//                                        ii.setDisplay(String.valueOf(i + 1));
//                                    } else if (total_row < 2) {
//                                        ii.setDisplay(String.valueOf(first_row));
//                                    } else {
//                                        ii.setDisplay(String.valueOf(first_row) + " : " + String.valueOf(curren_row));
//                                    }
//                                    ii.setSpaces_mark_reqd(township_sapce_marked);
//                                    ii.setIsparked("f");
//                                    ii.setRow_no(String.valueOf(curren_row));
//                                    ii.setRoe_name(String.valueOf(first_row));
//                                    ii.setMenu_img_id(total_row);
//                                    ii.setMarker("red");
//                                    //  ii.setDisplay(String.valueOf(first_row) + " : " + String.valueOf(curren_row));
//                                    // ii.setDisplay(String.valueOf(first_row));
//                                    array.add(ii);
//
//
//                                    first_row++;
//                                    curren_row = 0;
//                                } else {
//                                    if (township_sapce_marked.equals("false")) {
//                                        ii.setDisplay(String.valueOf(i + 1));
//                                    } else if (total_row < 2) {
//                                        ii.setDisplay(String.valueOf(first_row));
//                                    } else {
//                                        ii.setDisplay(String.valueOf(first_row) + " : " + String.valueOf(curren_row));
//                                    }
//                                    ii.setSpaces_mark_reqd(township_sapce_marked);
//                                    ii.setIsparked("f");
//                                    ii.setRow_no(String.valueOf(curren_row));
//                                    ii.setRoe_name(String.valueOf(first_row));
//                                    ii.setMenu_img_id(total_row);
//                                    ii.setMarker("red");
//                                    // ii.setDisplay(String.valueOf(first_row) + " : " + String.valueOf(curren_row));
//                                    array.add(ii);
//
//                                }
//                            }
//
//                            for (int j = 0; j < array.size(); j++) {
//                                Log.e("row", array.get(j).toString());
//                            }
//                        } else if (township_labeling.equals("AlphaNumeric")) {
//
//                            for (int i = 0; i < array_count; i++) {
//                                item ii = new item();
//                                curren_row++;
//                                if (curren_row == rowcount) {
//                                    ii.setSpaces_mark_reqd(township_sapce_marked);
//                                    ii.setIsparked("f");
//                                    ii.setRow_no(String.valueOf(curren_row));
//                                    ii.setRoe_name(row[first_row - 1].toString());
//                                    ii.setMenu_img_id(total_row);
//                                    ii.setMarker("red");
//                                    ii.setDisplay(row[first_row - 1].toString() + " : " + String.valueOf(curren_row));
//                                    array.add(ii);
//                                    first_row++;
//                                    curren_row = 0;
//                                } else {
//                                    ii.setSpaces_mark_reqd(township_sapce_marked);
//                                    ii.setIsparked("f");
//                                    ii.setMarker("red");
//                                    ii.setRow_no(String.valueOf(curren_row));
//                                    ii.setMenu_img_id(total_row);
//                                    ii.setRoe_name(row[first_row - 1].toString());
//                                    ii.setDisplay(row[first_row - 1].toString() + " : " + String.valueOf(curren_row));
//                                    array.add(ii);
//                                }
//                            }
//
//                            for (int j = 0; j < array.size(); j++) {
//                                Log.e("row", array.get(j).getDisplay());
//                            }
//
//
//                        }
//                        for (int i = 0; i < array.size(); i++) {
//                            Log.e(TAG, "    lotRow: " + i + "  " + new Gson().toJson(array.get(i)));
//                        }
//                        new fetchLocationLotOccupiedData(location_code, array).execute();
//
//                    }
//                    new fetchLocationLots(location_id).execute();
                    new fetchLocationLotOccupiedData(location_code).execute();

                } else {
                    rl_progressbar.setVisibility(View.GONE);
                }
            }
        });


        txtdrections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!lat1.equals("0.0") || !lang1.equals("0.0")) {

                    SharedPreferences sh1 = getActivity().getSharedPreferences("directiontomydirection", Context.MODE_PRIVATE);
                    SharedPreferences.Editor ed1 = sh1.edit();
                    ed1.putString("lat", lat1);
                    ed1.putString("lang", lang1);
                    ed1.putString("address", address);
                    ed1.commit();
                    SharedPreferences backtotimer = getActivity().getSharedPreferences("back_parkezly", Context.MODE_PRIVATE);
                    SharedPreferences.Editor eddd = backtotimer.edit();
                    eddd.putString("isback", "yes");
                    eddd.commit();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                   /* ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                    ft.add(R.id.My_Container_1_ID, new f_direction(), "NewFragmentTag");
                    ft.commit();*/
                    f_direction pay = new f_direction();
                    fragmentStack.clear();
                    // parking_first_screen.registerForListener(Vchome.this);
                    ft.add(R.id.My_Container_1_ID, pay, "home");
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                }
            }
        });
        txtstreetview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), panstreetview.class);
                i.putExtra("lat", lat1);
                i.putExtra("lang", lang1);
                i.putExtra("title", title);
                i.putExtra("address", address);
                getActivity().startActivity(i);

            }
        });

        txt_park_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {


                    if (txtmanaged.getVisibility() == View.VISIBLE) {
                        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                        loginid = logindeatl.getString("id", "null");
                        if (loginid.equals("null")) {
                            loginid = "0";
                        }
                        //String currentaddress = getlatandlangtoaddress(latitude, longitude);
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Confirm Parking");
                        alertDialog.setMessage("You Selected " + address + ". parking at " + adrreslocation);
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                new managedparkhere(location_code, address, lat1, lang1).execute();
                            }
                        });
                        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alertDialog.show();

                    } else {
                        //String currentaddress = getlatandlangtoaddress(latitude, longitude);
                        if (!adrreslocation.equals("")) {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("Confirm Parking");
                            alertDialog.setMessage("You selected '" + address + "'. Parking at '" + adrreslocation + "'");
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                /*SharedPreferences sharedPreferences=getActivity().getSharedPreferences("back_parkezly_parking",Context.MODE_PRIVATE);
                                SharedPreferences.Editor ede=sharedPreferences.edit();
                                ede.putString("back","yes");
                                ede.commit();
*/
                                    if (pricing.equals("null") || pricing.equals("0")) {
                                        SharedPreferences s = getActivity().getSharedPreferences("paidparkhere", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor ed = s.edit();
                                        ed.putString("Time", time_rules);
                                        ed.putString("Max", max_hours);
                                        ed.putString("rate", pricing);
                                        ed.putString("rated", pricing_duration);
                                        ed.putString("location_code", location_code);
                                        ed.putString("manager_type_id", manager_type_id);
                                        ed.putString("location_name", township_location_name);
                                        ed.putString("zip_code", "");
                                        ed.putString("city", "");
                                        ed.putString("lat", lat1);
                                        ed.putString("lang", lang1);
                                        ed.putString("id", "");
                                        ed.putString("location_id", location_id);
                                        ed.putString("address", address);
                                        ed.putString("title", title);
                                        ed.putString("township_code", township_code);
                                        ed.putString("duation_unit", duration_unit);
                                        if (txt_lost.getVisibility() == View.VISIBLE) {
                                            ed.putString("managedloick", "yes");
                                            ed.putString("mar", "managed");
                                        } else {
                                            ed.putString("managedloick", "no");
                                            ed.putString("mar", "free");
                                        }
                                        ed.putString("parkhere", "no");
                                        ed.putString("parking_for", "free");
                                        ed.putString("parking_type", marker);
                                        ed.commit();
                                   /* final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                    ft.replace(R.id.My_Container_1_ID, new freeparkhere(), "NewFragmentTag");
                                    ft.commit();*/
                                        freeparkhere pay = new freeparkhere();
                                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.add(R.id.My_Container_1_ID, pay);
                                        fragmentStack.lastElement().onPause();
                                        ft.hide(fragmentStack.lastElement());
                                        fragmentStack.push(pay);
                                        ft.commitAllowingStateLoss();
                                        //free park;
                                    } else {
                                        SharedPreferences s = getActivity().getSharedPreferences("paidparkhere", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor ed = s.edit();
                                        ed.putString("Time", time_rules);
                                        ed.putString("Max", max_hours);
                                        ed.putString("rate", pricing);
                                        ed.putString("rated", pricing_duration);
                                        ed.putString("location_code", location_code);
                                        ed.putString("manager_type_id", manager_type_id);
                                        ed.putString("location_name", township_location_name);
                                        ed.putString("zip_code", zip_code);
                                        ed.putString("city", city);
                                        ed.putString("lat", lat1);
                                        ed.putString("lang", lang1);
                                        ed.putString("id", "");
                                        ed.putString("township_code", township_code);
                                        ed.putString("address", address);
                                        ed.putString("duation_unit", duration_unit);
                                        ed.putString("title", title);
                                        if (txt_lost.getVisibility() == View.VISIBLE) {
                                            ed.putString("managedloick", "yes");
                                            ed.putString("mar", marker);
                                        } else {
                                            ed.putString("managedloick", "no");
                                            ed.putString("mar", "paid");
                                        }
                                        ed.putString("parkhere", "no");
                                        ed.putString("location_id", location_id);
                                        ed.putString("parking_for", "paid");
                                        ed.putString("parking_type", marker);
                                        ed.commit();

                                  /*  final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                    ft.replace(R.id.My_Container_1_ID, new paidparkhere(), "NewFragmentTag");
                                    ft.commit();*/

                                        paidparkhere pay = new paidparkhere();
                                        if (getArguments() != null)
                                            pay.setArguments(getArguments());
                                        MyApplication.getInstance().paidparkhereFragment = pay;
                                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.add(R.id.My_Container_1_ID, pay, "paidParking");
                                        fragmentStack.lastElement().onPause();
                                        ft.hide(fragmentStack.lastElement());
                                        fragmentStack.push(pay);
                                        ft.commitAllowingStateLoss();
                                        //padi park;
                                    }
                                }
                            });
                            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            alertDialog.show();
                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("Address not found!");
                            alertDialog.setMessage("Could not find location. Try including the exact location information");
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    new fetchLatLongFromaddress(String.valueOf(latitude), String.valueOf(longitude)).execute();
                                }
                            });
                        }
                    }
                } catch (Exception w) {
                    Log.e("#DEBUG", "   txt_park_here Click:  Error:  " + w.getMessage());
                }
            }

        });
    }

    @Override
    protected void initViews(View layout) {

        txttitle = (TextView) layout.findViewById(R.id.txtlable1);
        txtaddress = (TextView) layout.findViewById(R.id.txt_address);
        txttoday = (TextView) layout.findViewById(R.id.txtpouptoday);
        txtmax = (TextView) layout.findViewById(R.id.txtpopupax);
        txtstreetview = (TextView) layout.findViewById(R.id.txt_street_view);
        txtdrections = (TextView) layout.findViewById(R.id.txt_direction);
        txttime = (TextView) layout.findViewById(R.id.txtpopuptime1);
        txtmanaged = (TextView) layout.findViewById(R.id.txtpopupmanaged);
        txt_main_title = (TextView) layout.findViewById(R.id.txt_main_title);
        txt_park_here = (TextView) layout.findViewById(R.id.txt_park_here);
        txt_no_parking = (TextView) layout.findViewById(R.id.txt_private_no_parking_time);
        txt_parkin_time = (TextView) layout.findViewById(R.id.txt_private_parking_time);
        txt_custom_notices = (TextView) layout.findViewById(R.id.txt_custom_notices);
        rl_progressbar = (RelativeLayout) layout.findViewById(R.id.rl_progressbar);
        txt_lost = (TextView) layout.findViewById(R.id.txt_lost1);
        rl_main = (RelativeLayout) layout.findViewById(R.id.rl_main);

        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Light.otf");
        txttime.setTypeface(type);
        txtmax.setTypeface(type);
        txttoday.setTypeface(type);
    }


    public class fetchLatLongFromaddress extends AsyncTask<Void, Void, StringBuilder> {

        String lat1, lang1;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        public fetchLatLongFromaddress(String lat, String lang) {
            super();
            this.lat1 = lat;
            this.lang1 = lang;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {

                //  new getsearchotherlocation().execute();
                //
                // ();

                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat1 + "," + lang1 + "&key=AIzaSyBRdCWdlUMy3X5dc-9jMe0jRXJENibjdN8";

                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
                rl_progressbar.setVisibility(View.GONE);

                if (result != null) {
                    JSONObject jsonObj = new JSONObject(result.toString());
                    String status = jsonObj.getString("status");

                    if (status.equals("OK")) {

                        JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                        JSONObject before_geometry_jsonObj = resultJsonArray
                                .getJSONObject(0);
                        adrreslocation = before_geometry_jsonObj.getString("formatted_address");
                        JSONObject geometry_jsonObj = before_geometry_jsonObj
                                .getJSONObject("geometry");

                        JSONObject location_jsonObj = geometry_jsonObj
                                .getJSONObject("location");

                   /* lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);

                    lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);*/


                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Incomplete Addresses");
                        alertDialog.setMessage("Please select Destination address to proceed");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }


    public void currentlocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Failed to fetch user location!");
            alertDialog.setMessage("Please enabl e user location for app, location is required for app to work properly");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });

            alertDialog.show();
        }
    }

    public void showmap() {
        lat1 = lat1 != null ? (!lat1.equals("") ? lat1 : "40.7326609") : "40.7326609";
        lang1 = lang1 != null ? (!lang1.equals("") ? lang1 : "-73.6948277") : "-73.6948277";
        try {


            final LatLng position = new LatLng(Double.parseDouble(lat1), Double.parseDouble(lang1));
            MarkerOptions options = new MarkerOptions();
            options.position(position);
            options.title(title);
            options.snippet(address);

            SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            fm.getMapAsync(googleMap1 -> {
                GoogleMap googleMap = googleMap1;
                if (marker.equals("free")) {
                    options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.free_star1));
                } else if (marker.equals("Managed Paid") || marker.equals("Managed Free")) {
                    options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.rsz_manages_star11));
                } else if (marker.equals("Paid")) {
                    options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.paid_star1));
                }
                Marker melbourne = googleMap.addMarker(options);
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 14));
                final long start = SystemClock.uptimeMillis();
                final long duration = 1500L;
                mHandler.removeCallbacks(mAnimation);
                mAnimation = new BounceAnimation(start, duration, melbourne, mHandler);
                mHandler.post(mAnimation);
                googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                    @Override
                    public View getInfoWindow(Marker arg0) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(Marker arg0) {
                        View v = getActivity().getLayoutInflater().inflate(R.layout.otherparkingpin, null);
                        LatLng latLng = arg0.getPosition();
                        TextView txttitle = (TextView) v.findViewById(R.id.txtvalues);
                        TextView txtcontaint = (TextView) v.findViewById(R.id.txttotle);
                        String sa = arg0.getSnippet();
                        txtcontaint.setText(arg0.getSnippet());
                        txttitle.setText(arg0.getTitle());
                        return v;
                    }
                });
            });

        } catch (Exception e) {
        }

    }

    private void Showmanaedpopup(Activity context, final ArrayList<item> data) {

        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.managedcarpopup, viewGroup, false);
        final PopupWindow popupmanaged = new PopupWindow(context);
        popupmanaged.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(false);
        popupmanaged.setAnimationStyle(R.style.animationName);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);
        Button btnparkhere;
        ImageView imageclose;
        GridView grid_managed;
        RelativeLayout rl_popclose;

        btnparkhere = (Button) layout.findViewById(R.id.btn_managed_park);
        grid_managed = (GridView) layout.findViewById(R.id.grid_managed);
        imageclose = (ImageView) layout.findViewById(R.id.img_managed_close);
        rl_popclose = (RelativeLayout) layout.findViewById(R.id.rl_pop);
        rl_popclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });
        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popupmanaged.dismiss();
            }
        });

        grid_managed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {


                    String isparked = data.get(position).getIsparked();
                    String row_no11 = data.get(position).getRow_no();
                    //int datarow = Integer.parseInt(row_no11);
                    String datarow = row_no11;
                    String row_name2 = data.get(position).getRoe_name();
                    String plate_no, state;
                    if (isparked.equals("t")) {
                        for (int i = 0; i < managedlostpoparray.size(); i++) {
                            String row_no = managedlostpoparray.get(i).getRow_no();
                            if (!row_no.equals("")) {
                                //int jj = Integer.parseInt(row_no);
                                String jj = row_no;
                                String row_name = managedlostpoparray.get(i).getRoe_name();
                                if (datarow.equals(jj)) {
                                    if (row_name.equals(row_name2)) {
                                        plate_no = managedlostpoparray.get(i).getPlate_no();
                                        state = managedlostpoparray.get(i).getState();
                                        popupmanaged.dismiss();
                                        new getparked_car(plate_no, state).execute();
                                        break;
                                    }
                                }
                            }
                        }
                    } else {
                        popupmanaged.dismiss();
                    }
                } catch (Exception e) {

                }
            }
        });


       /* btnparkhere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popupmanaged.dismiss();

                SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);

                loginid = logindeatl.getString("id", "null");

                if (loginid.equals("null")) {
                    loginid = "0";
                }
                String currentaddress = getlatandlangtoaddress(latitude, longitude);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Confirm Parking");
                alertDialog.setMessage("You Selected " + address + ". parking at " + currentaddress);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                      //  new managedparkhere(locationcodr2, address, lat, lang).execute();
                    }
                });
                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();


            }
            //  }
            //}

        });*/


        Resources rs;
        final managedlostpop adpter = new managedlostpop(getActivity(), data, rs = getResources());
        grid_managed.setAdapter(adpter);
    }

    public class getparked_car extends AsyncTask<String, String, String> {

        String plate, state;
        JSONObject json;
        String transactionurl = "";

        public getparked_car(String plate, String state) {
            this.plate = plate;
            this.state = state;
            transactionurl = "_table/parked_cars?filter=(plate_no%3D'" + plate + "')%20AND%20(pl_state='" + state + "')%20AND%20(parking_status='ENTRY')";
        }


        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @SuppressLint("LongLogTag")
        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            parked_car.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            Log.e("parked_card_url_param", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            String currentdate = sdf.format(new Date());
            Date current = null;
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                current = format.parse(currentdate);
                System.out.println(current);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            try {
                response = client.execute(post);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    JSONArray array = json.getJSONArray("resource");
                    for (int j = 0; j < array.length(); j++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(j);
                        String id = c.getString("id");
                        String lat = c.getString("lat");
                        String lang = c.getString("lng");
                        String title = c.getString("plate_no");
                        String address = c.getString("address1");

                        String status = c.getString("ticket_status");
                        String parking_type = c.getString("parking_type");
                        String exntrytimr = c.getString("entry_date_time");
                        String marker = c.getString("expiry_time");
                        String max_time = c.getString("max_duration");

                        String title_state;
                        title_state = c.getString("pl_state");

                        if (title_state.equals("null")) {
                            title_state = c.getString("state");
                        }
                        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date exitre = null;
                        try {
                            exitre = format1.parse(marker);

                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (java.text.ParseException e) {
                            e.printStackTrace();
                        }
                        String carcolor;
                        long diff = exitre.getTime() - current.getTime();
                        datediffent.add(diff);
                        float dayCount = (float) diff / (24 * 60 * 60 * 1000);
                        int finalday = Math.round(dayCount);
                        Log.e("days", String.valueOf(dayCount));
                        Log.e("finaldays", String.valueOf(finalday));
                        long timeInSeconds = diff / 1000;
                        long hours = timeInSeconds / 3600;
                        Log.e("hours", String.valueOf(hours));
                        timeInSeconds = timeInSeconds - (hours * 3600);
                        long minutes = timeInSeconds / 60;
                        Log.e("minutes", String.valueOf(minutes));
                        timeInSeconds = timeInSeconds - (minutes * 60);
                        long seconds = timeInSeconds;
                        Log.e("seconds", String.valueOf(seconds));
                        String type = c.getString("parking_type");


                        String Remaining_time = "";
                        if (finalday > 0) {
                            if (finalday == 1) {
                                Remaining_time = Remaining_time + " " + (int) finalday + " Day";
                            } else {
                                Remaining_time = Remaining_time + " " + (int) finalday + " Days";
                            }

                        } else {
                            if (finalday != 0) {
                                if (finalday == -1) {
                                    Remaining_time = Remaining_time + " " + (int) finalday + " Day";
                                } else {
                                    Remaining_time = Remaining_time + " " + (int) finalday + " Days";
                                }
                            } else {

                                if (hours != 0) {
                                    if (hours == 1) {
                                        Remaining_time = Remaining_time + " " + (int) hours + " hour";
                                    } else {
                                        Remaining_time = Remaining_time + " " + (int) hours + " hours";
                                    }
                                }
                            }
                        }
                        if (minutes > 0) {
                            Remaining_time = Remaining_time + " " + (int) minutes + " minutes";
                        } else {

                            Remaining_time = Remaining_time + " " + (int) minutes + " minutes";
                        }

                        Log.e("Remaining_Time", Remaining_time);


                        if (current.after(exitre)) {
                            if (type.equals("anywhere")) {
                                carcolor = "status-anywhere";
                            } else {
                                if (status.equals("TICKETED")) {
                                    carcolor = "status-red_tic";
                                } else {
                                    carcolor = "status-red";
                                }
                            }
                        } else {

                            carcolor = "status-green";

                            Log.d("current", String.valueOf(current));
                            Log.d("ex", String.valueOf(exitre));
                            System.out.println(current);
                            System.out.println(exitre);
                        }
                        String location_code = c.getString("location_code");
                        boolean hr = lat.contains("Optional");
                        if (hr) {
                            Log.e("Optional", "yes");
                            String hr1 = lat.substring(lat.indexOf('(') + 1);
                            String hr2 = hr1.substring(0, hr1.indexOf(')'));

                            String hr3 = lang.substring(lang.indexOf('(') + 1);
                            String hr4 = hr3.substring(0, hr3.indexOf(')'));
                            lat = hr2;
                            lang = hr4;

                        }
                        LatLng kk = new LatLng(Double.parseDouble(lat), Double.parseDouble(lang));
                        double dist = getDistance(kk);
                        double ff = dist * 0.000621371192;
                        String finallab2 = String.format("%.2f", ff);
                        if (!parking_type.equals("random")) {
                            if (!parking_type.equals("anywhere")) {
                                item.setLat(lat);
                                item.setLang(lang);
                                String row = c.getString("lot_row");
                                String no = c.getString("lot_number");
                                item.setLot_row(row);
                                item.setLot_no(no);
                                //item.setUrl(url1);
                                item.setTitle(title);
                                item.setAddress(address);
                                item.setAddesss1(c.getString("marker_address1"));
                                item.setHtml(html);
                                item.setTicket_satus(status);
                                item.setExdate(marker);
                                item.setRespdate(exntrytimr);
                                item.setMax_time(max_time);
                                item.setMarker(carcolor);
                                item.setTitle_sate(title_state);
                                item.setLocation_code(location_code);
                                item.setId(id);
                                item.setParking_status(c.getString("parking_status"));
                                item.setMismatch(c.getString("mismatch"));
                                item.setParking_type(parking_type);
                                item.setMarker_type("4");
                                item.setDistancea(finallab2);
                                item.setAmount(Remaining_time);
                                item.setDiffent_time(diff);
                                parked_car.add(item);
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }


        public double getDistance(LatLng LatLng1) {
            double distance = 0;
            Location locationA = new Location("A");
            locationA.setLatitude(LatLng1.latitude);
            locationA.setLongitude(LatLng1.longitude);
            Location locationB = new Location("B");
            locationB.setLatitude(latitude);
            locationB.setLongitude(longitude);
            distance = locationA.distanceTo(locationB);
            return distance;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            //   progressBar.setVisibility(View.GONE);
            Resources rs;
            boolean no_match = true;
            if (getActivity() != null && json != null) {
                if (getActivity() != null) {
                    if (parked_car.size() > 0) {
                        String lat = parked_car.get(0).getLat().toString();
                        String lang = parked_car.get(0).getLang().toString();
                        String expri = parked_car.get(0).getExdate();
                        String entry = parked_car.get(0).getRespdate();
                        String status = parked_car.get(0).getTicket_satus();
                        String parking_type = parked_car.get(0).getParking_type();
                        String max_time = parked_car.get(0).getMax_time();
                        Log.e("idaxax", parked_car.get(0).getId());
                        Log.e("street_view", parked_car.get(0).getTicket_satus());
                        String parked_address = parked_car.get(0).getAddress();
                        String selected_address = parked_car.get(0).getAddesss1();
                        String address = parked_car.get(0).getAddress();
                        String lot_row = parked_car.get(0).getLot_row();
                        String lot_no = parked_car.get(0).getLot_no();
                        String marker = parked_car.get(0).getMarker();
                        String state = parked_car.get(0).getTitle_sate();

                        SharedPreferences sh = getActivity().getSharedPreferences("streetview", Context.MODE_PRIVATE);
                        SharedPreferences.Editor ed = sh.edit();
                        ed.putString("marker", marker);
                        ed.putString("statename", state);
                        ed.putString("title", title);
                        ed.putString("la", lat);
                        ed.putString("lang", lang);
                        ed.putString("lat1", String.valueOf(lat));
                        ed.putString("lang1", String.valueOf(lang));
                        ed.putString("lot_row", lot_row);
                        ed.putString("lot_row_no", lot_no);
                        ed.putString("exiry", expri);
                        ed.putString("entry", entry);
                        ed.putString("status", status);
                        ed.putString("parking_type", parking_type);
                        ed.putString("max_time", max_time);
                        ed.putString("parked_address", parked_address);
                        ed.putString("selected_address", selected_address);
                        ed.commit();


                        SharedPreferences shs = getActivity().getSharedPreferences("parkezly_parking", Context.MODE_PRIVATE);
                        SharedPreferences.Editor edd = shs.edit();
                        edd.putString("parkezly_parking_back", "yes");
                        edd.commit();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        vehicledeatil play = new vehicledeatil();
                        ft.add(R.id.My_Container_1_ID, play);
                        fragmentStack.lastElement().onPause();
                        ft.hide(fragmentStack.lastElement());
                        fragmentStack.push(play);
                        ft.commitAllowingStateLoss();
                    }
                }


            }
            super.onPostExecute(s);
        }
    }

    public class managedparkhere extends AsyncTask<String, String, String> {
        String streetviewurl = "null";
        String locationcode4 = "", address;
        String lat, lang;
        JSONArray googleparking1;
        JSONObject json, json1;

        public managedparkhere(String locationcode3, String address, String lat, String lang) {
            this.locationcode4 = locationcode3;
            this.address = address;
            this.lat = lat;
            this.lang = lang;
            streetviewurl = "_table/subscriptions?filter=(location_code%3D'" + locationcode3 + "')%20AND%20(user_id%3D'" + loginid + "')";

        }

        @Override
        protected void onPreExecute() {


            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + streetviewurl;
            Log.e("managed_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    googleparking1 = json.getJSONArray("resource");


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json != null) {
                if (getActivity() != null) {
                    if (googleparking1.isNull(0)) {
                        for (int k = 0; k < parking_rules.size(); k++) {
                            String loccode = parking_rules.get(k).getLoationcode1();
                            if (loccode.equals(locationcode4)) {
                                start_hour = parking_rules.get(k).getStart_hour();
                                end_hour = parking_rules.get(k).getEnd_hour();
                                this_day = parking_rules.get(k).getThis_day();
                                active = parking_rules.get(k).getActive();
                                pricing = parking_rules.get(k).getPricing();
                                pricing_duration = parking_rules.get(k).getPricing_duration();
                                time_rules = parking_rules.get(k).getTime_rule();
                                max_hours = parking_rules.get(k).getMax_hours();
                                locationnamne = parking_rules.get(k).getLoationname();
                                zip_code = parking_rules.get(k).getZip_code();
                                city = parking_rules.get(k).getCity();
                                state = parking_rules.get(k).getState();
                                String finalId = parking_rules.get(k).getId();
                                if (!start_hour.equals("null") && !end_hour.equals("null") && !this_day.equals("null")) {
                                    Calendar c = Calendar.getInstance();
                                    int hour = c.get(Calendar.HOUR);
                                    SimpleDateFormat sdf = new SimpleDateFormat("EEE");
                                    Date d = new Date();
                                    String today = sdf.format(d).toLowerCase();
                                    int start_hours = Integer.parseInt(start_hour);
                                    int end_hours = Integer.parseInt(end_hour);
                                    if (hour >= start_hours && hour <= end_hours && (this_day.equals(today) || this_day.equals("everyday"))) {


                                        SharedPreferences s5 = getActivity().getSharedPreferences("paidparkhere", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor ed = s5.edit();
                                        ed.putString("Time", time_rules);
                                        ed.putString("Max", max_hours);
                                        ed.putString("rate", pricing);
                                        ed.putString("rated", pricing_duration);
                                        ed.putString("location_code", location_code);
                                        ed.putString("manager_type_id", manager_type_id);
                                        ed.putString("location_name", locationnamne);
                                        ed.putString("zip_code", zip_code);
                                        ed.putString("city", city);
                                        ed.putString("lat", lat1);
                                        ed.putString("lang", lang1);
                                        ed.putString("id", "");
                                        ed.putString("address", address);
                                        ed.putString("title", title);
                                        if (txtmanaged.getVisibility() == View.VISIBLE) {
                                            ed.putString("managedloick", "yes");
                                            ed.putString("mar", "managed");
                                        } else {
                                            ed.putString("managedloick", "no");
                                            ed.putString("mar", "free");
                                        }
                                        ed.putString("parkhere", "no");
                                        ed.putString("parking_for", "paid");
                                        ed.commit();
                                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                        paidparkhere pay = new paidparkhere();
                                        if (getArguments() != null)
                                            pay.setArguments(getArguments());
                                        ft.replace(R.id.My_Container_1_ID, pay, "NewFragmentTag");
                                        ft.commit();


                                        break;
                                    }
                                }
                            }
                        }

                    } else {

                        for (int k = 0; k < parking_rules.size(); k++) {
                            String loccode = parking_rules.get(k).getLoationcode1();
                            if (loccode.equals(locationcode4)) {
                                start_hour = parking_rules.get(k).getStart_hour();
                                end_hour = parking_rules.get(k).getEnd_hour();
                                this_day = parking_rules.get(k).getThis_day();
                                active = parking_rules.get(k).getActive();
                                pricing = parking_rules.get(k).getPricing();
                                pricing_duration = parking_rules.get(k).getPricing_duration();
                                time_rules = parking_rules.get(k).getTime_rule();
                                max_hours = parking_rules.get(k).getMax_hours();
                                locationnamne = parking_rules.get(k).getLoationname();
                                zip_code = parking_rules.get(k).getZip_code();
                                city = parking_rules.get(k).getCity();
                                state = parking_rules.get(k).getState();
                                String finalId = parking_rules.get(k).getId();
                                if (!start_hour.equals("null") && !end_hour.equals("null") && !this_day.equals("null")) {
                                    Calendar c = Calendar.getInstance();
                                    int hour = c.get(Calendar.HOUR);
                                    SimpleDateFormat sdf = new SimpleDateFormat("EEE");
                                    Date d = new Date();
                                    String today = sdf.format(d).toLowerCase();
                                    int start_hours = Integer.parseInt(start_hour);
                                    int end_hours = Integer.parseInt(end_hour);
                                    if (hour >= start_hours && hour <= end_hours && (this_day.equals(today) || this_day.equals("everyday"))) {
                                        if (max_hours.equals("null") || max_hours.equals("")) {
                                            max_hours = "24";
                                        }

                                        SharedPreferences s5 = getActivity().getSharedPreferences("paidparkhere", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor ed = s5.edit();
                                        ed.putString("Time", time_rules);
                                        ed.putString("Max", max_hours);
                                        ed.putString("rate", pricing);
                                        ed.putString("rated", pricing_duration);
                                        ed.putString("location_code", location_code);
                                        ed.putString("manager_type_id", manager_type_id);
                                        ed.putString("location_name", locationnamne);
                                        ed.putString("zip_code", zip_code);
                                        ed.putString("city", city);
                                        ed.putString("lat", lat1);
                                        ed.putString("lang", lang1);
                                        ed.putString("id", "");
                                        ed.putString("address", address);
                                        ed.putString("title", title);
                                        if (txtmanaged.getVisibility() == View.VISIBLE) {
                                            ed.putString("managedloick", "yes");
                                            ed.putString("mar", "managed");
                                        } else {
                                            ed.putString("managedloick", "no");
                                            ed.putString("mar", "free");
                                        }
                                        ed.putString("parkhere", "no");
                                        ed.putString("parking_for", "free");
                                        ed.commit();
                                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                        ft.replace(R.id.My_Container_1_ID, new freeparkhere(), "NewFragmentTag");
                                        ft.commit();

                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class managedlostpop extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        boolean isselect = false;
        int a = 0;
        private Activity activity;
        private ArrayList<item> data;
        private SparseBooleanArray mCheckStates;
        private LayoutInflater inflater = null;

        /*************
         * CustomAdapter Constructor
         *****************/
        public managedlostpop(Activity a, ArrayList<item> d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;
            data = d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        /********
         * What is the size of Passed Arraylist Size
         ************/
        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        /******
         * Depends upon data size called for each row , Create each ListView row
         *****/
        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;


            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapatermanaed, null);
                holder = new ViewHolder();

                holder.txtprices = (TextView) vi.findViewById(R.id.txt_row_no1);
                holder.img = (ImageView) vi.findViewById(R.id.img_managed_car);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {

            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String cat = tempValues.getRow_no();
                if (cat == null) {
                } else if (cat != null) {

                    if (tempValues.getSpaces_mark_reqd().equals("false")) {
                        holder.txtprices.setText("R: " + tempValues.getDisplay());
                    } else if (tempValues.getMenu_img_id() < 2) {
                        holder.txtprices.setText("S: " + tempValues.getDisplay());
                    } else {
                        holder.txtprices.setText(tempValues.getDisplay());
                    }
                    if (tempValues.getIsparked().equals("t")) {
                        if (tempValues.getMarker().equals("red")) {
                            holder.img.setBackgroundResource(R.mipmap.popup_car_red);
                        } else if (tempValues.getMarker().equals("green")) {
                            holder.img.setBackgroundResource(R.mipmap.popup_car_green);
                        }
                    } else {
                        holder.img.setBackgroundResource(R.mipmap.popup_gree_car);
                    }
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }


        public class ViewHolder {

            public TextView txtparkinname, txtprices, txtoccie;
            ImageView img;
        }
    }

    public class fetchLocationLotOccupiedData extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String lat, lang, location_code1, address, title, html, mark, parking_time, no_parking_time, notice;

        String managedlosturl = "_proc/managed_lot_status?&order=(lot_row%20ASC%2C%20lot_number%20ASC)";

        ArrayList<item> data = new ArrayList<>();

        public fetchLocationLotOccupiedData(String loc) {
            this.location_code1 = loc;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            locationLots.clear();
            JSONObject managed = new JSONObject();


            /*  if(latitude==0.0) {*/
            try {
                managed.put("name", "in_location_code");
                managed.put("value", location_code1);

                //  student1.put("value", latitude);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            JSONArray jsonArray = new JSONArray();
            jsonArray.put(managed);

            JSONObject manageda = new JSONObject();
            try {
                manageda.put("params", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("managed_lost_url", url);
            Log.e("managed_lodt_param", String.valueOf(manageda));

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(manageda.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);

            HttpResponse response = null;

            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    TimeZone zone = TimeZone.getTimeZone("UTC");
                    sdf.setTimeZone(zone);
                    String currentdate = sdf.format(new Date());
                    Date current = null;
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        current = format.parse(currentdate);
                        System.out.println(current);
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   fetchLocationLotOccupiedData: Response: " + responseStr);

                    json1 = new JSONArray(responseStr);

                    for (int i = 0; i < json1.length(); i++) {
                        LocationLot item = new LocationLot();
                        JSONObject jsonObject = json1.getJSONObject(i);
                        if (jsonObject.has("id")
                                && !TextUtils.isEmpty(jsonObject.getString("id"))
                                && !jsonObject.getString("id").equals("null")) {
                            item.setId(jsonObject.getInt("id"));
                        }
                        if (jsonObject.has("date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("date_time"))
                                && !jsonObject.getString("date_time").equals("null")) {
                            item.setDate_time(jsonObject.getString("date_time"));
                        }

                        if (jsonObject.has("township_code")
                                && !TextUtils.isEmpty(jsonObject.getString("township_code"))
                                && !jsonObject.getString("township_code").equals("null")) {
                            item.setTownship_code(jsonObject.getString("township_code"));
                        }

                        if (jsonObject.has("location_code")
                                && !TextUtils.isEmpty(jsonObject.getString("location_code"))
                                && !jsonObject.getString("location_code").equals("null")) {
                            item.setLocation_code(jsonObject.getString("location_code"));
                        }

                        if (jsonObject.has("location_name")
                                && !TextUtils.isEmpty(jsonObject.getString("location_name"))
                                && !jsonObject.getString("location_name").equals("null")) {
                            item.setLocation_name(jsonObject.getString("location_name"));
                        }

                        if (jsonObject.has("lot_row")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_row"))
                                && !jsonObject.getString("lot_row").equals("null")) {
                            item.setLot_row(jsonObject.getString("lot_row"));
                        }

                        if (jsonObject.has("lot_number")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_number"))
                                && !jsonObject.getString("lot_number").equals("null")) {
                            item.setLot_number(jsonObject.getString("lot_number"));
                        }
                        if (jsonObject.has("lot_id")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_id"))
                                && !jsonObject.getString("lot_id").equals("null")) {
                            item.setLot_id(jsonObject.getString("lot_id"));
                        }

                        if (jsonObject.has("occupied")
                                && !TextUtils.isEmpty(jsonObject.getString("occupied"))
                                && !jsonObject.getString("occupied").equals("null")) {
                            item.setOccupied(jsonObject.getString("occupied").toLowerCase());
                        }

                        if (jsonObject.has("plate_no")
                                && !TextUtils.isEmpty(jsonObject.getString("plate_no"))
                                && !jsonObject.getString("plate_no").equals("null")) {
                            item.setPlate_no(jsonObject.getString("plate_no"));
                        }
                        if (jsonObject.has("plate_state")
                                && !TextUtils.isEmpty(jsonObject.getString("plate_state"))
                                && !jsonObject.getString("plate_state").equals("null")) {
                            item.setPlate_state(jsonObject.getString("plate_state"));
                        }

                        if (jsonObject.has("entry_date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("entry_date_time"))
                                && !jsonObject.getString("entry_date_time").equals("null")) {
                            item.setEntry_date_time(jsonObject.getString("entry_date_time"));
                        }

                        if (jsonObject.has("exit_date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("exit_date_time"))
                                && !jsonObject.getString("exit_date_time").equals("null")) {
                            item.setExit_date_time(jsonObject.getString("exit_date_time"));
                        }

                        if (jsonObject.has("expiry_time")
                                && !TextUtils.isEmpty(jsonObject.getString("expiry_time"))
                                && !jsonObject.getString("expiry_time").equals("null")) {
                            item.setExpiry_time(jsonObject.getString("expiry_time"));
                        }

                        if (jsonObject.has("parking_type")
                                && !TextUtils.isEmpty(jsonObject.getString("parking_type"))
                                && !jsonObject.getString("parking_type").equals("null")) {
                            item.setParking_type(jsonObject.getString("parking_type"));
                        }

                        if (jsonObject.has("parking_status")
                                && !TextUtils.isEmpty(jsonObject.getString("parking_status"))
                                && !jsonObject.getString("parking_status").equals("null")) {
                            item.setParking_status(jsonObject.getString("parking_status").toLowerCase());
                        }

                        if (jsonObject.has("lot_reservable")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_reservable"))
                                && !jsonObject.getString("lot_reservable").equals("null")) {
                            if (jsonObject.getString("lot_reservable").equals("1")) {
                                item.setLot_reservable(true);
                            } else if (jsonObject.getString("lot_reservable").equals("0")) {
                                item.setLot_reservable(false);
                            } else {
                                item.setLot_reservable(jsonObject.getBoolean("lot_reservable"));
                            }
                        }
                        if (jsonObject.has("lot_reservable_only")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_reservable_only"))
                                && !jsonObject.getString("lot_reservable_only").equals("null")) {
                            if (jsonObject.getString("lot_reservable_only").equals("1")) {
                                item.setLot_reservable_only(true);
                            } else if (jsonObject.getString("lot_reservable_only").equals("0")) {
                                item.setLot_reservable_only(false);
                            } else {
                                item.setLot_reservable_only(jsonObject.getBoolean("lot_reservable_only"));
                            }
                        } else {
                            item.setLot_reservable(false);
                        }

                        if (jsonObject.has("premium_lot")
                                && !TextUtils.isEmpty(jsonObject.getString("premium_lot"))
                                && !jsonObject.getString("premium_lot").equals("null")) {
                            if (jsonObject.getString("premium_lot").equals("1")) {
                                item.setPremium_lot(true);
                            } else if (jsonObject.getString("premium_lot").equals("0")) {
                                item.setPremium_lot(false);
                            } else {
                                item.setPremium_lot(jsonObject.getBoolean("premium_lot"));
                            }
                        }
                        if (jsonObject.has("special_need_handi_lot")
                                && !TextUtils.isEmpty(jsonObject.getString("special_need_handi_lot"))
                                && !jsonObject.getString("special_need_handi_lot").equals("null")) {
                            if (jsonObject.getString("special_need_handi_lot").equals("1")) {
                                item.setSpecial_need_handi_lot(true);
                            } else if (jsonObject.getString("special_need_handi_lot").equals("0")) {
                                item.setSpecial_need_handi_lot(false);
                            } else {
                                item.setSpecial_need_handi_lot(jsonObject.getBoolean("premium_lot"));
                            }
                        }
                        if (jsonObject.has("location_id")
                                && !TextUtils.isEmpty(jsonObject.getString("location_id"))
                                && !jsonObject.getString("location_id").equals("null")) {
                            item.setLocation_id(Integer.parseInt(jsonObject.getString("location_id")));
                        }

                        if (!TextUtils.isEmpty(item.getParking_status())
                                && !TextUtils.isEmpty(item.getOccupied())
                                && item.getParking_status().equals("ENTRY")
                                && item.getOccupied().equals("yes")) {

                            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date exitre = null;
                            try {
                                exitre = format1.parse(item.getExpiry_time());

                            } catch (android.net.ParseException e) {
                                // TODO Auto-generated catch block
                                Log.e("#DEBUG", "  Error:  " + e.getMessage());
                                e.printStackTrace();
                            } catch (java.text.ParseException e) {
                                Log.e("#DEBUG", "  Error:  " + e.getMessage());
                                e.printStackTrace();
                            }

                            if (current.after(exitre)) {
                                item.setExpired(true);
                            } else {
                                item.setExpired(false);
                            }
                        }
                        if (!locationLots.contains(item))
                            if (!item.isLot_reservable()) {
                                locationLots.add(item);
                            }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json1 != null) {
                Collections.sort(locationLots, (o1, o2) -> {
                    try {
                        return Integer.valueOf(o1.getLot_id()) - Integer.valueOf(o2.getLot_id());
                    } catch (Exception e) {
                        return o1.getLot_id().compareTo(o2.getLot_id());
                    }
                });

//                for (int i = 0; i < managedlostpoparray.size(); i++) {
//                    String row_no = managedlostpoparray.get(i).getRow_no();
//                    if (!row_no.equals("")) {
//                        if (isInteger(row_no)) {
//                            int jj = Integer.parseInt(row_no);
//                            String row_name = managedlostpoparray.get(i).getRoe_name();
//                            for (int j = 0; j < data.size(); j++) {
//                                String row_no11 = data.get(j).getRow_no();
//                                if (!row_no11.equals("")) {
//                                    int datarow = Integer.parseInt(row_no11);
//                                    String row_name2 = data.get(j).getRoe_name();
//                                    if (jj == datarow) {
//                                        if (row_name.equals(row_name2)) {
//                                            data.get(j).setMarker(managedlostpoparray.get(i).getMarker());
//                                            data.get(j).setIsparked("t");
//                                        }
//                                    }
//                                }
//                            }
//                        } else {
//                            String row_name = managedlostpoparray.get(i).getRoe_name();
//                            for (int j = 0; j < data.size(); j++) {
//                                String row_no11 = data.get(j).getRow_no();
//                                if (!row_no11.equals("")) {
//                                    String row_name2 = data.get(j).getRoe_name();
//                                    if (row_no.equals(row_no11)) {
//                                        if (row_name.equals(row_name2)) {
//                                            data.get(j).setMarker(managedlostpoparray.get(i).getMarker());
//                                            data.get(j).setIsparked("t");
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//
//                }
            }
            if (getActivity() != null)
                showDialogLocationLots();
            super.onPostExecute(s);
        }
    }

    public class fetchLocationLots extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String locationLotsUrl = "_table/location_lot";
        JSONObject json;
        String re_id;
        String id;
        String locationId;

        public fetchLocationLots(String locationId) {
            this.locationId = locationId;
        }

        @Override
        protected void onPreExecute() {
            locationLots.clear();
            rl_progressbar.setVisibility(View.VISIBLE);
            try {
                this.locationLotsUrl = "_table/location_lot?filter=location_id%3D" + URLEncoder.encode(String.valueOf(locationId), "utf-8") + "";
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("#DEBUG", "   fetchLocationLot:  Error:  " + e.getMessage());
            }
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            locationLots.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + locationLotsUrl;
            Log.e("#DEBUG", "  fetchLocationLots:  URL:  " + url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        LocationLot locationLot = new LocationLot();
                        locationLot.setId(object.getInt("id"));

                        if (object.has("date_time")
                                && !TextUtils.isEmpty(object.getString("date_time"))
                                && !object.getString("date_time").equals("null")) {
                            locationLot.setDate_time(object.getString("date_time"));
                        }

                        if (object.has("township_code")
                                && !TextUtils.isEmpty(object.getString("township_code"))
                                && !object.getString("township_code").equals("null")) {
                            locationLot.setTownship_code(object.getString("township_code"));
                        }

                        if (object.has("location_id")
                                && !TextUtils.isEmpty(object.getString("location_id"))
                                && !object.getString("location_id").equals("null")) {
                            locationLot.setLocation_id(Integer.parseInt(object.getString("location_id")));
                        }

                        if (object.has("date_time")
                                && !TextUtils.isEmpty(object.getString("date_time"))
                                && !object.getString("date_time").equals("null")) {
                            locationLot.setDate_time(object.getString("date_time"));
                        }

                        if (object.has("location_code")
                                && !TextUtils.isEmpty(object.getString("location_code"))
                                && !object.getString("location_code").equals("null")) {
                            locationLot.setLocation_code(object.getString("location_code"));
                        }

                        if (object.has("location_name")
                                && !TextUtils.isEmpty(object.getString("location_name"))
                                && !object.getString("location_name").equals("null")) {
                            locationLot.setLocation_name(object.getString("location_name"));
                        }

                        if (object.has("lot_row")
                                && !TextUtils.isEmpty(object.getString("lot_row"))
                                && !object.getString("lot_row").equals("null")) {
                            locationLot.setLot_row(object.getString("lot_row"));
                        }

                        if (object.has("lot_number")
                                && !TextUtils.isEmpty(object.getString("lot_number"))
                                && !object.getString("lot_number").equals("null")) {
                            locationLot.setLot_number(object.getString("lot_number"));
                        }

                        if (object.has("occupied")
                                && !TextUtils.isEmpty(object.getString("occupied"))
                                && !object.getString("occupied").equals("null")) {
                            locationLot.setOccupied(object.getString("occupied"));
                        }

                        if (object.has("plate_no")
                                && !TextUtils.isEmpty(object.getString("plate_no"))
                                && !object.getString("plate_no").equals("null")) {
                            locationLot.setPlate_no(object.getString("plate_no"));
                        }

                        if (object.has("plate_state")
                                && !TextUtils.isEmpty(object.getString("plate_state"))
                                && !object.getString("plate_state").equals("null")) {
                            locationLot.setPlate_state(object.getString("plate_state"));
                        }

                        if (object.has("lot_id")
                                && !TextUtils.isEmpty(object.getString("lot_id"))
                                && !object.getString("lot_id").equals("null")) {
                            locationLot.setLot_id(object.getString("lot_id"));
                        }

                        if (object.has("lot_reservable")
                                && !TextUtils.isEmpty(object.getString("lot_reservable"))
                                && !object.getString("lot_reservable").equals("null")) {
                            locationLot.setLot_reservable(object.getBoolean("lot_reservable"));
                        }

                        if (object.has("lot_reservable_only")
                                && !TextUtils.isEmpty(object.getString("lot_reservable_only"))
                                && !object.getString("lot_reservable_only").equals("null")) {
                            locationLot.setLot_reservable_only(object.getBoolean("lot_reservable_only"));
                        }

                        if (object.has("company_type_id")
                                && !TextUtils.isEmpty(object.getString("company_type_id"))
                                && !object.getString("company_type_id").equals("null")) {
                            locationLot.setCompany_type_id(object.getString("company_type_id"));
                        }

                        if (object.has("manager_type_id")
                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
                                && !object.getString("manager_type_id").equals("null")) {
                            locationLot.setManager_type_id(object.getString("manager_type_id"));
                        }

                        if (object.has("premium_lot")
                                && !TextUtils.isEmpty(object.getString("premium_lot"))
                                && !object.getString("premium_lot").equals("null")) {
                            locationLot.setPremium_lot(object.getBoolean("premium_lot"));
                        }

                        if (object.has("company_id")
                                && !TextUtils.isEmpty(object.getString("company_id"))
                                && !object.getString("company_id").equals("null")) {
                            locationLot.setCompany_id(object.getString("company_id"));
                        }

                        locationLot.setIsparked("t");
                        locationLot.setMarker("red");

                        locationLots.add(locationLot);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   fetchLocationLots:  Error:  " + e.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   fetchLocationLots:  Error:  " + e.getMessage());
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);

            if (getActivity() != null) {
                if (locationLots.size() != 0) {
                    //TODO display lots
                    showDialogLocationLots();
                }
                super.onPostExecute(s);
            }
        }
    }

    private void showDialogLocationLots() {
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        View dialogView = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_locatoin_lot, null);
        builder.setView(dialogView);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RelativeLayout rlClose = dialogView.findViewById(R.id.rlClose);
        RecyclerView rvLocationLot = dialogView.findViewById(R.id.rvLocationLot);
        LocationLotAdapter locationLotAdapter = new LocationLotAdapter();
        rvLocationLot.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        rvLocationLot.setAdapter(locationLotAdapter);
        rlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private class LocationLotAdapter extends RecyclerView.Adapter<LocationLotAdapter.LocationLotHolder> {
        private Drawable drawableRed, drawableBlue, drawableGreen, drawableGrey;

        @NonNull
        @Override
        public LocationLotAdapter.LocationLotHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            drawableRed = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_red);
            drawableBlue = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_blue);
            drawableGreen = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_green);
            drawableGrey = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_gree_car);
            return new LocationLotHolder(LayoutInflater.from(getActivity())
                    .inflate(R.layout.item_location_lot, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull LocationLotAdapter.LocationLotHolder locationLotHolder, int i) {
            LocationLot locationLot = locationLots.get(i);
            locationLotHolder.tvReserved.setVisibility(View.GONE);
            locationLotHolder.ivReserved.setVisibility(View.GONE);
            locationLotHolder.ivReservedOnly.setVisibility(View.GONE);
            locationLotHolder.ivPremium.setVisibility(View.GONE);
            locationLotHolder.ivHandicapped.setVisibility(View.GONE);
            if (locationLot != null) {
                if (!TextUtils.isEmpty(locationLot.getLot_id())) {
                    locationLotHolder.tvRow.setText(locationLot.getLot_id());
                }

                if (!TextUtils.isEmpty(locationLot.getOccupied())) {
                    if (locationLot.getOccupied().equalsIgnoreCase("yes")) {
                        //Car already parked at this location
                        if (locationLot.isExpired()) {
                            //RED, parking expired
                            locationLotHolder.ivCar.setImageDrawable(drawableRed);
                        } else {
                            //GREEN, parking valid
                            locationLotHolder.ivCar.setImageDrawable(drawableGreen);
                        }
                        if (locationLot.isLot_reservable()) {
//                            locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                        }
                    } else if (locationLot.getOccupied().equalsIgnoreCase("no")) {
                        if (locationLot.isLot_reservable()) {
                            //GREY with R, Available for reservation
                            locationLotHolder.ivCar.setImageDrawable(drawableGrey);
//                            locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                        } else {
                            //GREY, Available for parking
                            locationLotHolder.ivCar.setImageDrawable(drawableGrey);
                        }

                    } else if (locationLot.getOccupied().equalsIgnoreCase("reserved")) {
                        //BLUE with R, Reserved for parking
                        locationLotHolder.ivCar.setImageDrawable(drawableBlue);
//                        locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (locationLot.isLot_reservable()) {
                        //GREY with R, Available for reservation
                        locationLotHolder.ivCar.setImageDrawable(drawableGrey);
//                        locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                    } else {
                        //GREY, Available for parking
                        locationLotHolder.ivCar.setImageDrawable(drawableGrey);
                    }
                }

                if (locationLot.isLot_reservable()) {
                    locationLotHolder.ivReserved.setVisibility(View.VISIBLE);
                } else {
                    locationLotHolder.ivReserved.setVisibility(View.GONE);
                }

                if (locationLot.isPremium_lot())
                    locationLotHolder.ivPremium.setVisibility(View.VISIBLE);
                else locationLotHolder.ivPremium.setVisibility(View.GONE);

                if (locationLot.isSpecial_need_handi_lot())
                    locationLotHolder.ivHandicapped.setVisibility(View.VISIBLE);
                else locationLotHolder.ivHandicapped.setVisibility(View.GONE);

                if (locationLot.isLot_reservable_only()) {
                    locationLotHolder.ivReservedOnly.setVisibility(View.VISIBLE);
                } else locationLotHolder.ivReservedOnly.setVisibility(View.GONE);

//                if (locationLot.getIsparked().equals("t")) {
//                    if (locationLot.getMarker().equals("red")) {
//                        locationLotHolder.ivCar.setBackgroundResource(R.mipmap.popup_car_red);
//                    } else if (locationLot.getMarker().equals("green")) {
//                        locationLotHolder.ivCar.setBackgroundResource(R.mipmap.popup_car_green);
//                    }
//                } else {
//                    locationLotHolder.ivCar.setBackgroundResource(R.mipmap.popup_gree_car);
//                }
            }

        }

        @Override
        public int getItemCount() {
            return locationLots.size();
        }

        class LocationLotHolder extends RecyclerView.ViewHolder {
            ImageView ivCar, ivReserved, ivPremium, ivHandicapped, ivReservedOnly;
            TextView tvRow, tvReserved;

            LocationLotHolder(@NonNull View itemView) {
                super(itemView);

                ivCar = itemView.findViewById(R.id.ivCar);
                tvRow = itemView.findViewById(R.id.tvRow);
                tvReserved = itemView.findViewById(R.id.tvReserved);
                ivReserved = itemView.findViewById(R.id.ivReserved);
                ivPremium = itemView.findViewById(R.id.ivPremium);
                ivHandicapped = itemView.findViewById(R.id.ivHandicapped);
                ivReservedOnly = itemView.findViewById(R.id.ivReservedOnly);

//                itemView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        final int position = getAdapterPosition();
//                        if (position != RecyclerView.NO_POSITION) {
////                            selectedLocationLot = locationLots.get(position);
//                            if (!TextUtils.isEmpty(locationLots.get(position).getOccupied())) {
//                                if (locationLots.get(position).getOccupied().equalsIgnoreCase("no")) {
//                                    locationLots.get(position).setOccupied("yes");
//                                } else {
//                                    locationLots.get(position).setOccupied("no");
//                                }
//                            } else {
//                                locationLots.get(position).setOccupied("yes");
//                            }
//
//                            notifyItemChanged(position);
//                        }
//                    }
//                });
            }
        }
    }
}
