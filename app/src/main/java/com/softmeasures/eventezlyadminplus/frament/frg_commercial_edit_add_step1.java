package com.softmeasures.eventezlyadminplus.frament;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class frg_commercial_edit_add_step1 extends BaseFragment {

    RelativeLayout rl_progressbar;
    String id;
    TextView txt_commercial_title_sp1, txt_commercial_lat, txt_commercial_lang, txt_commercial_sp1_next, txt_commercial_place_id;
    EditText txt_commercial_title, txt_commercial_address, edit_commercial_loc_code, txt_commercial_weekday_24hr, txt_commercial_parking_time, txt_commercial_no_parking_time;
    public static String commercial_title, commercial_address, commercail_no_parking_time,
            commercail_parking_time, commercial_lat, commercial_lang, commercial_place_id,
            commercial_loc_code, is_add;
    private RelativeLayout rl_main;
    private Spinner sp_township_type;
    String township_type[] = {"Paid", "Free", "Managed Paid", "Managed Free"};
    private String township_type_sp = "";
    private boolean isNewLocation = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            isNewLocation = getArguments().getBoolean("isNewLocation", isNewLocation);
            commercial_title = bundle.getString("title", "");
            commercial_address = bundle.getString("address", "");
            commercial_loc_code = bundle.getString("code", "");
            commercail_parking_time = bundle.getString("parking_time", "6 AM- 10 PM ( M-F), 7 AM- 6 PM ( Sat-Sun)");
            commercail_no_parking_time = bundle.getString("parking_non_time", "1 AM- 6 AM");
            commercial_place_id = bundle.getString("place_id", "");
            commercial_lang = bundle.getString("lng", "");
            commercial_lat = bundle.getString("lat", "");
            is_add = bundle.getString("is_add", "");
        }

        return inflater.inflate(R.layout.commercial_edit_setep_1, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (myApp.isLocationAdded) {
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
        }
    }

    @Override
    protected void updateViews() {
        txt_commercial_title.setText(commercial_title);
        txt_commercial_title_sp1.setText(commercial_title);
        txt_commercial_address.setText(commercial_address);
        edit_commercial_loc_code.setText(commercial_loc_code);
        txt_commercial_place_id.setText(commercial_place_id);
        txt_commercial_lat.setText(commercial_lat);
        txt_commercial_lang.setText(commercial_lang);
        commercail_no_parking_time = commercail_no_parking_time != null ? (!commercail_no_parking_time.equals("") ? (!commercail_no_parking_time.equals("null") ? commercail_no_parking_time : "1 AM- 6 AM") : "1 AM- 6 AM") : "1 AM- 6 AM";
        commercail_parking_time = commercail_parking_time != null ? (!commercail_parking_time.equals("") ? (!commercail_parking_time.equals("null") ? commercail_parking_time : "6 AM- 10 PM ( M-F), 7 AM- 6 PM ( Sat-Sun)") : "6 AM- 10 PM ( M-F), 7 AM- 6 PM ( Sat-Sun)") : "6 AM- 10 PM ( M-F), 7 AM- 6 PM ( Sat-Sun)";
        txt_commercial_no_parking_time.setText(commercail_no_parking_time);
        txt_commercial_parking_time.setText(commercail_parking_time);
    }

    @Override
    protected void setListeners() {
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        sp_township_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                township_type_sp = (String) sp_township_type.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> paren1t) {
            }
        });

        txt_commercial_sp1_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                commercial_title = txt_commercial_title.getText().toString();
                commercial_address = txt_commercial_address.getText().toString();
                commercail_no_parking_time = txt_commercial_no_parking_time.getText().toString();
                commercail_parking_time = txt_commercial_parking_time.getText().toString();
                if (!commercial_title.equals("")) {
                    if (!commercial_address.equals("")) {
                        if (!commercail_no_parking_time.equals("")) {
                            if (!commercail_parking_time.equals("")) {

                                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                frg_commercial_edit_add_step2 pay = new frg_commercial_edit_add_step2();
                                Bundle bundle = getArguments();
                                bundle.putString("township_type_sp", township_type_sp);
                                bundle.putBoolean("isNewLocation", isNewLocation);
                                pay.setArguments(bundle);
                                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                ft.add(R.id.My_Container_1_ID, pay);
                                fragmentStack.lastElement().onPause();
                                ft.hide(fragmentStack.lastElement());
                                fragmentStack.push(pay);
                                ft.commitAllowingStateLoss();
                            } else {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                alertDialog.setTitle("Parking Times Required");
                                alertDialog.setMessage("Please enter parking times of parking");
                                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                                alertDialog.show();
                            }
                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("No Parking Times Required");
                            alertDialog.setMessage("Please enter no parking times of parking");
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            alertDialog.show();
                        }
                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Address Required");
                        alertDialog.setMessage("Please enter address of parking");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }

                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Title Required");
                    alertDialog.setMessage("Please enter title of parking");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }

            }
        });
    }

    @Override
    protected void initViews(View view) {

        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        txt_commercial_title_sp1 = (TextView) view.findViewById(R.id.txt_commercial_main_title);
        txt_commercial_title = (EditText) view.findViewById(R.id.txt_Commerci_title);
        txt_commercial_address = (EditText) view.findViewById(R.id.txt_Commercial_address);
        edit_commercial_loc_code = (EditText) view.findViewById(R.id.edit_commercial_loc_code);
        txt_commercial_lat = (TextView) view.findViewById(R.id.txt_commercial_lat);
        txt_commercial_lang = (TextView) view.findViewById(R.id.txt_commercial_lang);
        txt_commercial_weekday_24hr = (EditText) view.findViewById(R.id.txt_commercial_day_24hr);
        txt_commercial_parking_time = (EditText) view.findViewById(R.id.txt_commercial_parking_time);
        txt_commercial_no_parking_time = (EditText) view.findViewById(R.id.txt_commercial_no_parking_time);
        txt_commercial_sp1_next = (TextView) view.findViewById(R.id.txt_commercial_sp1);
        txt_commercial_place_id = (TextView) view.findViewById(R.id.txt_place_id);

        rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        sp_township_type = (Spinner) view.findViewById(R.id.sp_type_township);

        ArrayAdapter<String> sp_type_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, township_type);
        sp_township_type.setAdapter(sp_type_adapter);

    }
}
