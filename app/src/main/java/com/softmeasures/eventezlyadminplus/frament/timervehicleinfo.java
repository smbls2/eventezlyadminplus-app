package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;
import com.google.android.gms.maps.model.StreetViewPanoramaLocation;
import com.google.android.gms.maps.model.StreetViewPanoramaOrientation;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.Main_Activity;
import com.softmeasures.eventezlyadminplus.activity.panstreetview;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.BounceAnimation;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;
import com.softmeasures.eventezlyadminplus.services.NotificationPublisher;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import static android.content.Context.ALARM_SERVICE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class timervehicleinfo extends Fragment implements StreetViewPanorama.OnStreetViewPanoramaChangeListener, StreetViewPanorama.OnStreetViewPanoramaCameraChangeListener,
        StreetViewPanorama.OnStreetViewPanoramaClickListener, StreetViewPanorama.OnStreetViewPanoramaLongClickListener {

    public StreetViewPanorama mStreetViewPanorama;
    ProgressDialog pdialog;
    TextView txtplantno, txthide, txtexit, txttimerplantno, txttimerrgiserexpiresdate, txtaprkat, txttimermaxtime, txttimer, txtfindmyvicho, txtaddress, txtlocationname, txt_entry_ticket;
    String platno, statename, lat, lang, currentdate, hr, id, min, user_id, token, markertype,
            parking_free = "", fullsatename, locationname, address, exit_time, country, city, title,
            parkehere, parked_add, parking_type, lot_no, lot_row, location_lot_id, reservation_id;
    RelativeLayout rltimer, rl_deatil, rl_layout_map_view, rl_layout_streetview, rl_progressbar1;
    GoogleMap googleMap;
    String result, parked_time;
    //    vchome.CounterClass timer;
    CounterClass timer1;
    ConnectionDetector cd;
    double latitude = 0.0, longitude = 0.0;
    Double lat1 = 0.0, lang1 = 0.0;
    RelativeLayout rl_progessbar;
    RemoteViews remoteViews;
    Map<String, Object> statefullname;
    SupportStreetViewPanoramaFragment streetViewPanoramaFragment;
    RelativeLayout rlback;
    RelativeLayout btn_street;
    LatLng SYDNEY;
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;
    boolean isFinishing = false;
    FrameLayout fm;
    ArrayList<item> parking_rules = new ArrayList<>();
    RelativeLayout rl_renew;
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private Handler mHandler = new Handler();
    private long startTime = 0L;
    private Handler customHandler = new Handler();
    private StreetViewPanoramaView mStreetViewPanoramaView;
    private float x1, x2;
    private Handler mHandler1 = null;
    private Runnable mAnimation;
    String timeStamp;
    ArrayList<item> township_parking = new ArrayList<>();
    ArrayList<item> googlepartnerarray = new ArrayList<>();
    ArrayList<item> parkingarray = new ArrayList<>();

    private item parkedCar;
    private String nbal, cbal = "0", finallab;
    ArrayList<item> wallbalarray = new ArrayList<>();
    double finalpayamount, newbal, subTotal;
    SimpleDateFormat sdf;
    private String ip;
    private String paymentmethod = "", transection_id, wallet_id = "";
    private boolean mIsPaymentPaid = false;

    private static final int REQUEST_CODE_PAYMENT = 2;
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    private static final String TAG = "paymentQlighting";
    private static final String CONFIG_CLIENT_ID = "AQXvOmlRwvO2AI6H3XzqTe14pp4x6VtP2skKfUkhgjRAoNj0NoT7l0ZGeaRAXkAtYqyPCmxKEUXkrT89";
    private static PayPalConfiguration config = new PayPalConfiguration().environment(CONFIG_ENVIRONMENT).clientId(CONFIG_CLIENT_ID);

    private TextView tvPlatNo, tvParkedAt, tvExpiredAt, tvParkingName, tvLocationCode, tvParkedAddress,
            tvSelectedAddress, tvMaxTime, tvLotId;
    private TextView txt_renew, txt_street_view, txt_parked_address, lable_expri, lable_parkinglable,
            lable_selectedaddress, txt_exit1, txt_max_time, txt_strre1, txt_direction1, txt_hide1;
    private RelativeLayout rl_parkhere, rl_normal;
    private String max_time = "";
    private SharedPreferences sh;
    private String entryTime = "", exitTime = "", locationName = "", locationCode = "", lotId = "";
    private boolean isPaymentPaid = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        final View view = inflater.inflate(R.layout.fragment_timervehicle, container, false);
        mHandler1 = new Handler();
        Typeface light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Light.otf");
        getPrefData();
        initViews(view);
        updateViews();
        setListeners();


        new getparkingrules().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        return view;
    }

    private void updateViews() {

        if (!TextUtils.isEmpty(exitTime)
                && !exitTime.equals("null")) {
            tvExpiredAt.setText(exitTime);
        } else tvExpiredAt.setText(" - ");

        if (!TextUtils.isEmpty(entryTime)
                && !entryTime.equals("null")) {
            tvParkedAt.setText(entryTime);
        } else tvParkedAt.setText(" - ");

        if (!TextUtils.isEmpty(locationCode)
                && !tvLocationCode.equals("null")) {
            tvLocationCode.setText(locationCode);
        } else tvLocationCode.setText(" - ");

        if (!TextUtils.isEmpty(locationName) && !locationName.equals("null")) {
            tvParkingName.setText(locationName);
        } else if (!TextUtils.isEmpty(locationCode)
                && !tvLocationCode.equals("null")) {
            tvParkingName.setText(locationCode);
        } else tvParkingName.setText(" - ");

        if (!TextUtils.isEmpty(lotId)
                && !lotId.equals("null")) {
            tvLotId.setText(lotId);
        } else tvLotId.setText(" - ");

//        ((vchome) getActivity()).showtimerhide();
        cd = new ConnectionDetector(getActivity());
        if (cd.isConnectingToInternet()) {
            currentlocation();
            txtaddress.setText(address);
            tvSelectedAddress.setText(address);
            txt_parked_address.setText(parked_add);
            tvParkedAddress.setText(parked_add);
            txtlocationname.setText(locationname + " " + lot_row + "" + lot_no);
//            if (lot_no != null && !TextUtils.isEmpty(lot_no) && !lot_no.equals("null")) {
//                tvLocationCode.setText(locationname + " " + lot_row + "" + lot_no);
//            } else {
//                tvLocationCode.setText(locationname + " " + lot_row);
//            }
            if (!parking_type.equals("paid")) {
                if (max_time != null) {
                    if (min.equals("Hrs") || min.equals("Hr")) {
                        if (max_time.equals("1")) {
                            tvMaxTime.setText("1 Hour");
                        } else tvMaxTime.setText(max_time + " Hours");
                    } else if (min.equals("Months") || min.equals("Month")) {
                        if (max_time.equals("1")) {
                            tvMaxTime.setText("1 Month");
                        } else tvMaxTime.setText(max_time + " Months");
                    } else if (min.equals("Days") || min.equals("Day")) {
                        if (max_time.equals("1")) {
                            tvMaxTime.setText("1 Day");
                        } else tvMaxTime.setText(max_time + " Days");
                    } else if (min.equals("Weeks") || min.equals("Week")) {
                        if (max_time.equals("1")) {
                            tvMaxTime.setText("1 Week");
                        } else tvMaxTime.setText(max_time + " Weeks");
                    } else {
                        if (max_time.equals("1")) {
                            tvMaxTime.setText("1 Minute");
                        } else tvMaxTime.setText(max_time + " Minutes");
                    }
                }
            } else {
                /*if(max_time!=null)
                {
                    txttimermaxtime.setText(max_time + " Hours");
                }*/

                if (max_time != null) {
                    if (min.equals("Hrs") || min.equals("Hr")) {
                        if (max_time.equals("1")) {
                            tvMaxTime.setText("1 Hour");
                        } else tvMaxTime.setText(max_time + " Hours");
                    } else if (min.equals("Months") || min.equals("Month")) {
                        if (max_time.equals("1")) {
                            tvMaxTime.setText("1 Month");
                        } else tvMaxTime.setText(max_time + " Months");
                    } else if (min.equals("Days") || min.equals("Day")) {
                        if (max_time.equals("1")) {
                            tvMaxTime.setText("1 Day");
                        } else tvMaxTime.setText(max_time + " Days");
                    } else if (min.equals("Weeks") || min.equals("Week")) {
                        if (max_time.equals("1")) {
                            tvMaxTime.setText("1 Weeks");
                        } else tvMaxTime.setText(max_time + " Weeks");
                    } else {
                        if (max_time.equals("1")) {
                            tvMaxTime.setText("1 Minute");
                        } else tvMaxTime.setText(max_time + " Minutes");
                    }
                }
            }
            txtplantno.setText(platno + " " + statename);
            tvPlatNo.setText(platno + " " + statename);
            if (lat.equals("null") || lat1.equals("")) {
                lat1 = 0.0;
            } else {
                lat1 = Double.parseDouble(lat);
            }
            if (lang.equals("null") || lang.equals("")) {
                lang1 = 0.0;
            } else {
                lang1 = Double.parseDouble(lang);
            }
            showmap();
            SharedPreferences isexieornot = getActivity().getSharedPreferences("isexitornot", Context.MODE_PRIVATE);
            final SharedPreferences.Editor is = isexieornot.edit();
            is.putString("paltno", platno);
            is.putString("id", id);
            is.commit();
            SharedPreferences sh1 = getActivity().getSharedPreferences("token", Context.MODE_PRIVATE);
            token = sh1.getString("token", "null");
            if (markertype.equals("free")) {
                rl_renew.setVisibility(View.GONE);
            } else {
                rl_renew.setVisibility(View.VISIBLE);
            }

            if (parking_type.equals("otherPartner")) {
                new getprivate_parking().execute();
            } else if (parking_type.equals("googlePartner")) {
                new getgoogleparking().execute();
            } else {
                new gettownship_parking().execute();
            }
            if (!platno.equals("")) {
                long tim2e = 0;
                Date date1 = null;
                TimeZone zone12 = TimeZone.getTimeZone("UTC");
                if (parkehere.equals("yes")) {
                    try {
                        String inputPattern1 = "yyyy-MM-dd HH:mm:ss";
                        SimpleDateFormat inputFormat1 = new SimpleDateFormat(inputPattern1);
                        SimpleDateFormat outputFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
                        inputFormat1.setTimeZone(zone12);
                        outputFormat1.setTimeZone(zone12);
                        String str1 = null;
                        String datef1 = currentdate;
                        date1 = inputFormat1.parse(currentdate);
                        tim2e = date1.getTime();
                        Log.e("time", String.valueOf(tim2e));
                        str1 = outputFormat1.format(date1);
                        // txttimerrgiserexpiresdate.setText(str1);
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                        Date value = formatter.parse(currentdate);

                        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //this format changeable
                        dateFormatter.setTimeZone(TimeZone.getDefault());
                        String localtime1 = dateFormatter.format(value);

                        txtaprkat.setText(localtime1);
                        //txtaprkat.setText(str1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    SimpleDateFormat df;
                    df = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
                    df.setTimeZone(zone12);
                    Date date2 = null;
                    try {
                        Calendar cal = Calendar.getInstance();
                        Date currentLocalTime = cal.getTime();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        TimeZone zone = TimeZone.getTimeZone("UTC");
                        sdf.setTimeZone(zone12);
                        String currentdate = sdf.format(new Date());
                        //  String sss = txtaprkat.getText().toString();
                        String inputPattern1 = "yyyy-MM-dd HH:mm:ss";
                        SimpleDateFormat inputFormat1 = new SimpleDateFormat(inputPattern1);
                        SimpleDateFormat outputFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        inputFormat1.setTimeZone(zone12);
                        outputFormat1.setTimeZone(zone12);
                        String str1 = null;
                        String datef1 = currentdate;
                        date2 = inputFormat1.parse(currentdate);
                        String str2 = outputFormat1.format(date2);
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat df11 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String formattedDate = df11.format(c.getTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    if (result.equals("false")) {
                        customHandler.removeCallbacks(updateTimerThread);
                        customHandler.removeCallbacks(updateTimerThread);
                        startTime = SystemClock.uptimeMillis();
                        customHandler.postDelayed(updateTimerThread, 0);
                    } else {
                        timeInMilliseconds = date2.getTime() - date1.getTime();
                        customHandler.postDelayed(updateTimerThread1, 0);
                    }
                    lable_expri.setVisibility(View.GONE);
                    lable_parkinglable.setVisibility(View.GONE);
                    lable_selectedaddress.setVisibility(View.GONE);
                    txttimerrgiserexpiresdate.setVisibility(View.GONE);
                    txtlocationname.setVisibility(View.GONE);
                    txtaddress.setVisibility(View.GONE);
                    txt_max_time.setVisibility(View.GONE);
                    txttimermaxtime.setVisibility(View.GONE);
                    rl_parkhere.setVisibility(View.VISIBLE);
                    rl_normal.setVisibility(View.GONE);
                } else {
                    lable_expri.setVisibility(View.VISIBLE);
                    lable_parkinglable.setVisibility(View.VISIBLE);
                    lable_selectedaddress.setVisibility(View.VISIBLE);
                    txttimerrgiserexpiresdate.setVisibility(View.VISIBLE);
                    txtlocationname.setVisibility(View.VISIBLE);
                    txtaddress.setVisibility(View.VISIBLE);
                    rl_parkhere.setVisibility(View.GONE);
                    txttimermaxtime.setVisibility(View.VISIBLE);
                    txt_max_time.setVisibility(View.VISIBLE);
                    rl_normal.setVisibility(View.VISIBLE);
                    if (result.equals("true")) {
                        if (parking_type.equals("anywhere") || parking_type.equals("random")) {
                            lable_expri.setVisibility(View.GONE);
                            lable_parkinglable.setVisibility(View.GONE);
                            lable_selectedaddress.setVisibility(View.GONE);
                            txttimerrgiserexpiresdate.setVisibility(View.GONE);
                            txtlocationname.setVisibility(View.GONE);
                            txtaddress.setVisibility(View.GONE);
                            txt_max_time.setVisibility(View.GONE);
                            txttimermaxtime.setVisibility(View.GONE);
                            rl_parkhere.setVisibility(View.VISIBLE);
                            rl_normal.setVisibility(View.GONE);
                            customHandler.postDelayed(updateTimerThread1, 0);
                        } else {
                            timer();
                        }
                    } else {
                        timer();
                    }
                }

            }

            new fetchParkedCar().execute();


        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();

        }
    }

    private void getPrefData() {
        final SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
        result = time.getString("result", "false");

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = logindeatl.getString("id", "null");

        sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
        platno = sh.getString("platno", "");
        statename = sh.getString("state", "");
        currentdate = sh.getString("currentdate", "");
        hr = sh.getString("hr", "");
        lat = sh.getString("lat", "0");
        lang = sh.getString("log", "0");
        id = sh.getString("id", "0");
        min = sh.getString("min", "null");
        address = sh.getString("address", "null");
        locationname = sh.getString("locationname", "null");
        markertype = sh.getString("markertye", "null").toLowerCase();
        max_time = sh.getString("max", "0");
        exit_time = sh.getString("exit_date_time", "null");
        city = sh.getString("city", "city");
        country = sh.getString("country", "country");
        title = sh.getString("title", "null");
        parked_add = sh.getString("parked_address", "");
        parkehere = sh.getString("parkhere", "no");
        lot_no = sh.getString("lot_no", "");
        lot_row = sh.getString("lot_row", "");
        location_lot_id = sh.getString("location_lot_id", "");
        reservation_id = sh.getString("reservation_id", "");

        Log.e("#DEBUG", "  timervehicleinfo  location_lot_id:  " + location_lot_id);
        Log.e("#DEBUG", "  timervehicleinfo  reservation_id:  " + reservation_id);
        parking_type = sh.getString("parking_type", "null");
        parking_free = sh.getString("parkhere", "");
        parked_time = sh.getString("parked_time", "0");

        entryTime = sh.getString("entryTime", "");
        exitTime = sh.getString("exitTime", "");
        locationName = sh.getString("locationName", "");
        locationCode = sh.getString("locationCode", "");
        lotId = sh.getString("lotId", "");

        prepareStateList();
    }

    private void prepareStateList() {
        statefullname = new HashMap<String, Object>();
        statefullname.put("AL", "Alabama");
        statefullname.put("AK", "Alaska");
        statefullname.put("AZ", "Arizona");
        statefullname.put("AR", "Arkansas");
        statefullname.put("CA", "California");
        statefullname.put("CO", "Colorado");
        statefullname.put("CT", "Connecticut");
        statefullname.put("DE", "Delaware");
        statefullname.put("DC", "District of Columbia");
        statefullname.put("FL", "Florida");
        statefullname.put("GA", "Georgia");
        statefullname.put("HI", "Hawaii");
        statefullname.put("ID", "Idaho");
        statefullname.put("IL", "Illinois");
        statefullname.put("IN", "Indiana");
        statefullname.put("IA", "Iowa");
        statefullname.put("KS", "Kansas");
        statefullname.put("KY", "Kentucky");
        statefullname.put("LA", "Louisiana");
        statefullname.put("ME", "Maine");
        statefullname.put("MD", "Maryland");
        statefullname.put("MA", "Massachusetts");
        statefullname.put("MI", "Michigan");
        statefullname.put("MN", "Minnesota");
        statefullname.put("MS", "Mississippi");
        statefullname.put("MO", "Missouri");
        statefullname.put("MT", "Montana");
        statefullname.put("NE", "Nebraska");
        statefullname.put("NV", "Nevada");
        statefullname.put("NH", "New Hampshire");
        statefullname.put("NJ", "New Jersey");
        statefullname.put("NM", "New Mexico");
        statefullname.put("NY", "New York");
        statefullname.put("NC", "North Carolina");
        statefullname.put("ND", "North Dakota");
        statefullname.put("OH", "Ohio");
        statefullname.put("OK", "Oklahoma");
        statefullname.put("OR", "Oregon");
        statefullname.put("PA", "Pennsylvani");
        statefullname.put("RI", "Rhode Island");
        statefullname.put("SC", "South Carolina");
        statefullname.put("SD", "South Dakota");
        statefullname.put("TN", "Tennessee");
        statefullname.put("TX", "Texas");
        statefullname.put("UT", "Utah");
        statefullname.put("VT", "Vermont");
        statefullname.put("VA", "Virginia");
        statefullname.put("WA", "Washington");
        statefullname.put("WV", "West virginia");
        statefullname.put("WI", "Wisconsin");
        statefullname.put("WY", "Wyoming");
    }

    private void setListeners() {
        rl_deatil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        rltimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        rl_progessbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        txtexit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                user_id = logindeatl.getString("id", "null");
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Exit Parking");
                alertDialog.setMessage("Are you sure you want to exit vehicle from this parking?");
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if (parkedCar != null && parkedCar.isParkedNow_PayLater() &&
                                parkedCar.getParkNow_PostPayment_Status().equals("UNPAID")) {
                            showDialogPaymentDue();
                        } else {
                            exitParking();
                        }

                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
                // new exitsparking().execute();
            }
        });

        txt_exit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                user_id = logindeatl.getString("id", "null");
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("Are you sure you want to exit parking?");
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        isFinishing = true;
                        onDestroy();
                        customHandler.removeCallbacks(updateTimerThread);
                        customHandler.removeCallbacks(updateTimerThread1);

                        SharedPreferences sh1 = getActivity().getSharedPreferences("token", Context.MODE_PRIVATE);
                        token = sh1.getString("token", "null");
                        sh1.edit().clear().commit();
                        if (token.equals("null")) {
                            // token = "1234";
                        }
                        String timerstartornot = txttimer.getText().toString();
                        if (timerstartornot.equals("00 : 00 : 00")) {
                            if (!user_id.equals("null")) {
                                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                                sh.edit().clear().commit();
                                SharedPreferences isexieornot = getActivity().getSharedPreferences("isexitornot", Context.MODE_PRIVATE);
                                isexieornot.edit().clear().commit();
                                new exitsparking().execute();
                            } else {
                                showrandomnumber(getActivity(), token);
                            }
                        } else {

                            canclealarm(Integer.parseInt(id));
                            if (timer1 != null) {
                                timer1.cancel();
                                timer1.onFinish();
                            }
                            if (!user_id.equals("null")) {

                                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                                sh.edit().clear().commit();
                                SharedPreferences isexieornot = getActivity().getSharedPreferences("isexitornot", Context.MODE_PRIVATE);
                                isexieornot.edit().clear().commit();
                                new exitsparking().execute();
                            } else {
                                showrandomnumber(getActivity(), token);
                            }
                        }
                    }
                });

                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();

            }
        });
        txthide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((vchome) getActivity()).showmapandhidefragment();
            }
        });
        txt_hide1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((vchome) getActivity()).showmapandhidefragment();
            }
        });
        txtfindmyvicho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!lat.equals("null") || !lang.equals("null")) {
                    String addd = txt_parked_address.getText().toString();
                    SharedPreferences sh1 = getActivity().getSharedPreferences("directiontomydirection", Context.MODE_PRIVATE);
                    SharedPreferences.Editor ed1 = sh1.edit();
                    ed1.putString("lat", lat);
                    ed1.putString("lang", lang);
                    ed1.putString("address", addd);
                    ed1.commit();

                    SharedPreferences backtotimer = getActivity().getSharedPreferences("backtotimer", Context.MODE_PRIVATE);
                    SharedPreferences.Editor eddd = backtotimer.edit();
                    eddd.putString("isback", "yes");
                    eddd.commit();

                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                      /*  ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.add(R.id.My_Container_1_ID, new f_direction(), "NewFragmentTag");
                        ft.commit();*/
                    f_direction pay = new f_direction();
                    fragmentStack.clear();
                    // parking_first_screen.registerForListener(Vchome.this);
                    ft.add(R.id.My_Container_1_ID, pay, "home");
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();

                } else {
                }
            }
        });

        txt_direction1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!lat.equals("null") || !lang.equals("null")) {
                    String addd = txt_parked_address.getText().toString();
                    SharedPreferences sh1 = getActivity().getSharedPreferences("directiontomydirection", Context.MODE_PRIVATE);
                    SharedPreferences.Editor ed1 = sh1.edit();
                    ed1.putString("lat", lat);
                    ed1.putString("lang", lang);
                    ed1.putString("address", addd);
                    ed1.commit();
                    SharedPreferences backtotimer = getActivity().getSharedPreferences("backtotimer", Context.MODE_PRIVATE);
                    SharedPreferences.Editor eddd = backtotimer.edit();
                    eddd.putString("isback", "yes");
                    eddd.commit();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                       /* ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.add(R.id.My_Container_1_ID, new f_direction(), "NewFragmentTag");
                        ft.commit();*/

                    f_direction pay = new f_direction();
                    fragmentStack.clear();
                    // parking_first_screen.registerForListener(Vchome.this);
                    ft.add(R.id.My_Container_1_ID, pay, "home");
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();

                } else {
                }
            }
        });
        txt_entry_ticket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // createandDisplayPdf();
                showgoogleparking(getActivity());
            }
        });

        txt_renew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                  /*  SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                    user_id = logindeatl.getString("id", "null");
                   if (markertype.equals("free")) {

                    } else {

                            SharedPreferences dd = getActivity().getSharedPreferences("select_vehicle", Context.MODE_PRIVATE);
                            SharedPreferences.Editor ddd = dd.edit();
                            ddd.putString("plate_no", platno);
                            ddd.putString("state", statename);
                            rl_progessbar.setVisibility(View.VISIBLE);
                            SharedPreferences isexieornot = getActivity().getSharedPreferences("isexitornot", Context.MODE_PRIVATE);
                            isexieornot.edit().clear().commit();
                            String timerstartornot = txttimer.getText().toString();
                            if (!timerstartornot.equals("00 : 00 : 00")) {
                                onDestroy();
                                customHandler.removeCallbacks(updateTimerThread);
                                customHandler.removeCallbacks(updateTimerThread1);
                                canclealarm(Integer.parseInt(id));
                                timer1.cancel();
                                timer1.onFinish();
                            }
                            new exitsparkingandrenew().execute();


                    }*/

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("Are you sure you want to Renew parking?");
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String exi = sh.getString("exip", "");
                        //String exi= "2017-10-09 16:43:21";
                        SharedPreferences d = getActivity().getSharedPreferences("renew", Context.MODE_PRIVATE);
                        SharedPreferences.Editor dd = d.edit();
                        dd.putString("renew", "yes");
                        dd.putString("marker", markertype);
                        dd.putString("renew_id", id);
                        dd.putString("cureent_date", exi);
                        dd.putString("parked_time", parked_time);
                        if (markertype.contains("managed")) {
                            dd.putString("managedloick", "yes");
                        }

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        TimeZone zone = TimeZone.getTimeZone("UTC");
                        sdf.setTimeZone(zone);
                        String currentdate = sdf.format(new Date());
                        Date current = null;
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        try {
                            current = format.parse(currentdate);
                            System.out.println(current);
                        } catch (android.net.ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (java.text.ParseException e) {
                            e.printStackTrace();
                        }

                        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date exitre = null;
                        try {
                            exitre = format1.parse(exi);

                        } catch (android.net.ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (java.text.ParseException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (current.after(exitre)) {

                                dd.putString("car_is_valid", "no");
                                Log.e("dare", "scbsjc");
                            } else {
                                dd.putString("car_is_valid", "yes");
                            }
                            dd.commit();
                        } catch (Exception e) {

                        }

                        if (parking_free.equals("yes")) {

                            if (markertype.equals("free") || markertype.equals("Free") || markertype.equals("anywhere") || markertype.equals("managed") || markertype.equals("managed_guest")) {
                                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                ft.replace(R.id.My_Container_1_ID, new freeparkhere(), "NewFragmentTag");
                                ft.commit();
                            }
                        } else {
                            if (markertype.equals("Paid") || markertype.equals("paid") || markertype.contains("Managed") || markertype.contains("managed")) {
                                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                ft.replace(R.id.My_Container_1_ID, new paidparkhere(), "NewFragmentTag");
                                ft.commit();
                            } else if (markertype.equals("otherPartner") || markertype.equals("otherpartner") || markertype.equals("other parking") || markertype.equals("google") || markertype.equals("partner") || markertype.equals("googlePartner") || markertype.equals("googlepartner")) {
                                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                ft.replace(R.id.My_Container_1_ID, new Google_Paid_Parking(), "googlePaidParking");
                                ft.commit();
                            }
                        }
                    }
                });

                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();


            }
        });

        txt_street_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), panstreetview.class);
                i.putExtra("lat", lat);
                i.putExtra("lang", lang);
                i.putExtra("title", title);
                i.putExtra("address", parked_add);
                getActivity().startActivity(i);
            }
        });
        txt_strre1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), panstreetview.class);
                i.putExtra("lat", lat);
                i.putExtra("lang", lang);
                i.putExtra("title", title);
                i.putExtra("address", parked_add);
                getActivity().startActivity(i);

            }
        });
    }

    private void initViews(View view) {
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        currentdate = sdf.format(new Date());

        rltimer = (RelativeLayout) view.findViewById(R.id.opnetimer);
        rl_deatil = (RelativeLayout) view.findViewById(R.id.deatil);
        txthide = (TextView) view.findViewById(R.id.txthide);
        txt_entry_ticket = (TextView) view.findViewById(R.id.txt_entry_ticket);
        txt_renew = (TextView) view.findViewById(R.id.txt_renew);
        txt_street_view = (TextView) view.findViewById(R.id.txt_street);
        rl_layout_map_view = (RelativeLayout) view.findViewById(R.id.layoutmap1);
        rl_layout_streetview = (RelativeLayout) view.findViewById(R.id.layoutmap2);
        btn_street = (RelativeLayout) view.findViewById(R.id.button_street);
        rl_renew = (RelativeLayout) view.findViewById(R.id.rl_renew);
        fm = (FrameLayout) view.findViewById(R.id.fn);
        txttimerplantno = (TextView) view.findViewById(R.id.txtplate);
        txtaprkat = (TextView) view.findViewById(R.id.txtparkedat);
        txttimerrgiserexpiresdate = (TextView) view.findViewById(R.id.txtexpires);
        txttimermaxtime = (TextView) view.findViewById(R.id.txtmaxtime);
        txttimer = (TextView) view.findViewById(R.id.time);
        txtplantno = (TextView) view.findViewById(R.id.txtplate);
        txtfindmyvicho = (TextView) view.findViewById(R.id.txtfindvehicle);
        txtlocationname = (TextView) view.findViewById(R.id.txt_locationname);
        txtaddress = (TextView) view.findViewById(R.id.txt_addreess);
        rl_progessbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        txtexit = (TextView) view.findViewById(R.id.txtexits);
        remoteViews = new RemoteViews(getActivity().getPackageName(),
                R.layout.customnotification);

        txt_parked_address = (TextView) view.findViewById(R.id.txt_prked_addreess);
        lable_expri = (TextView) view.findViewById(R.id.text3);
        lable_parkinglable = (TextView) view.findViewById(R.id.text8);
        lable_selectedaddress = (TextView) view.findViewById(R.id.text5);
        txt_exit1 = (TextView) view.findViewById(R.id.txtexits1);
        txt_max_time = (TextView) view.findViewById(R.id.text4);
        txt_strre1 = (TextView) view.findViewById(R.id.txt_street1);
        txt_direction1 = (TextView) view.findViewById(R.id.txtfindvehicle1);
        txt_hide1 = (TextView) view.findViewById(R.id.txthide1);
        rl_normal = (RelativeLayout) view.findViewById(R.id.rl_normal);
        rl_parkhere = (RelativeLayout) view.findViewById(R.id.rl_parkhere);
        rl_progressbar1 = (RelativeLayout) view.findViewById(R.id.rl_progressbar1);

        tvPlatNo = view.findViewById(R.id.tvPlatNo);
        tvParkedAt = view.findViewById(R.id.tvParkedAt);
        tvExpiredAt = view.findViewById(R.id.tvExpiredAt);
        tvParkingName = view.findViewById(R.id.tvParkingName);
        tvLocationCode = view.findViewById(R.id.tvLocationCode);
        tvParkedAddress = view.findViewById(R.id.tvParkedAddress);
        tvSelectedAddress = view.findViewById(R.id.tvSelectedAddress);
        tvMaxTime = view.findViewById(R.id.tvMaxTime);
        tvLotId = view.findViewById(R.id.tvLotId);
    }

    private void showDialogPaymentDue() {
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        View dialogView = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_payment_due, null);
        builder.setView(dialogView);
        TextView tvAmount = dialogView.findViewById(R.id.tvAmount);
        Button btnPayLater = dialogView.findViewById(R.id.btnPayLater);
        Button btnWallet = dialogView.findViewById(R.id.btnWallet);
        Button btnPayPal = dialogView.findViewById(R.id.btnPayPal);

        tvAmount.setText(String.format("$%s", parkedCar.getParkNow_PostPayment_DueAmount()));

        if (!TextUtils.isEmpty(parkedCar.getParkNow_PostPayment_term())
                && parkedCar.getParkNow_PostPayment_term().equals("EXIT")) {
            btnPayLater.setVisibility(View.GONE);
        } else {
            btnPayLater.setVisibility(View.VISIBLE);
        }

        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();

        btnWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                payWithWallet();
            }
        });

        btnPayLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                SharedPreferences sh1 = getActivity().getSharedPreferences("token", Context.MODE_PRIVATE);
                token = sh1.getString("token", "null");
                sh1.edit().clear().commit();
                if (token.equals("null")) {
                    // token = "1234";
                }
                String timerstartornot = txttimer.getText().toString();
                if (timerstartornot.equals("00 : 00 : 00")) {
                    if (!user_id.equals("null")) {
                        SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                        sh.edit().clear().commit();
                        SharedPreferences isexieornot = getActivity().getSharedPreferences("isexitornot", Context.MODE_PRIVATE);
                        isexieornot.edit().clear().commit();
                        new exitsparking().execute();
                    } else {
                        showrandomnumber(getActivity(), token);
                    }
                } else {

                    canclealarm(Integer.parseInt(id));

                    // timer.cancel();
                    //timer.onFinish();
                    timer1.cancel();
                    timer1.onFinish();
                    if (!user_id.equals("null")) {
                        SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                        sh.edit().clear().commit();
                        SharedPreferences isexieornot = getActivity().getSharedPreferences("isexitornot", Context.MODE_PRIVATE);
                        isexieornot.edit().clear().commit();
                        new exitsparking().execute();
                    } else {
                        showrandomnumber(getActivity(), token);
                    }
                }
            }
        });

        btnPayPal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                payWithPayPal();
            }
        });


        alertDialog.show();
    }

    private void payWithWallet() {
        new getwalletbal().execute();
    }

    private void payWithPayPal() {
        PaypalPaymentIntegration(String.valueOf(parkedCar.getParkNow_PostPayment_DueAmount()));
    }

    private void PaypalPaymentIntegration(String amount) {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE, amount);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent, String payamount) {
        return new PayPalPayment(new BigDecimal(payamount), "USD", "Parking Payment", paymentIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.i("#DEBUG", confirm.toJSONObject().toString(4));
                    Log.i("#DEBUG", confirm.getPayment().toJSONObject().toString(4));
                    JSONObject jsonObj = new JSONObject(confirm.getPayment().toJSONObject().toString(4));
                    Log.e("JSON IS", "LIKE" + jsonObj);
                    Log.e("short_description", "short_description : " + jsonObj.getString("short_description"));
                    Log.e("amount", "amount : " + jsonObj.getString("amount"));
                    Log.e("intent", "intent : " + jsonObj.getString("intent"));
                    Log.e("currency_code", "currency_code : " + jsonObj.getString("currency_code"));
                    JSONObject jsonObjResponse = confirm.toJSONObject().getJSONObject("response");
                    transection_id = jsonObjResponse.getString("id");
                    if (transection_id != null) {
                        ActionStartsHere();
                        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                        String id = logindeatl.getString("id", "null");
                        paymentmethod = "Paypal";
                        mIsPaymentPaid = true;
                        exitParking();

                    } else {
                        transactionfailalert();
                    }
                    Log.e("transactionId", ":;;;;" + transection_id);

                } catch (JSONException e) {
                    Log.e("", "an extremely unlikely failure occurred: ", e);
                }
            }
        } else {
            transactionfailalert();
        }
    }

    private void exitParking() {
        SharedPreferences sh1 = getActivity().getSharedPreferences("token", Context.MODE_PRIVATE);
        token = sh1.getString("token", "null");
        sh1.edit().clear().commit();
        if (token.equals("null")) {
            // token = "1234";
        }
        String timerstartornot = txttimer.getText().toString();
        if (timerstartornot.equals("00 : 00 : 00")) {
            if (!user_id.equals("null")) {
                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                sh.edit().clear().commit();
                SharedPreferences isexieornot = getActivity().getSharedPreferences("isexitornot", Context.MODE_PRIVATE);
                isexieornot.edit().clear().commit();
                new exitsparking().execute();
            } else {
                showrandomnumber(getActivity(), token);
            }
        } else {

            canclealarm(Integer.parseInt(id));

            // timer.cancel();
            //timer.onFinish();
            timer1.cancel();
            timer1.onFinish();
            if (!user_id.equals("null")) {
                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                sh.edit().clear().commit();
                SharedPreferences isexieornot = getActivity().getSharedPreferences("isexitornot", Context.MODE_PRIVATE);
                isexieornot.edit().clear().commit();
                new exitsparking().execute();
            } else {
                showrandomnumber(getActivity(), token);
            }
        }
    }

    public void transactionfailalert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Transaction Fail try again");
        alertDialog.setMessage("Fail");
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alertDialog.show();
    }

    public void currentlocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {

            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();

        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Failed to fetch user location!");
            alertDialog.setMessage("Please enable user location for app, location is required for app to work properly");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });

            alertDialog.show();
        }
    }

    private Runnable updateTimerThread1 = new Runnable() {

        public void run() {

            if (isFinishing == false) {
                long tim2e = 0;
                Date date1 = null;
                TimeZone zone12 = TimeZone.getTimeZone("UTC");

                try {
                    date1 = sdf.parse(currentdate);
                    tim2e = date1.getTime();
                    Date value = sdf.parse(currentdate);
                    String localtime1 = sdf.format(value);
                    txtaprkat.setText(localtime1);

                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }

                Date date2 = null;
                try {
                    String currentdate = sdf.format(new Date());
                    date2 = sdf.parse(currentdate);
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                timeInMilliseconds = date2.getTime() - date1.getTime();

                long millis = timeInMilliseconds;
                String hms1 = String.format("%02d : %02d : %02d", TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                        TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                txttimer.setText(hms1);
                customHandler.postDelayed(this, 1);
            } else {
                onDestroy();
                customHandler.removeCallbacks(this);
            }


        }

    };
    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            if (isFinishing == false) {

                timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

                updatedTime = timeSwapBuff + timeInMilliseconds;

                int secs = (int) (updatedTime / 1000);
                int mins = secs / 60;
                int hor = mins / 60;
                secs = secs % 60;
                int milliseconds = (int) (updatedTime % 1000);
                String hms = String.format("%01d : %02d : %02d", hor, mins, secs);
                txttimer.setText(hms);
                customHandler.postDelayed(this, 1);
            } else {
                onDestroy();
                customHandler.removeCallbacks(this);
            }


        }

    };

    @TargetApi(Build.VERSION_CODES.M)
    public void showgoogleparking(Activity context) {

        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View layout = layoutInflater.inflate(R.layout.select_printer_option, viewGroup, false);
        final PopupWindow popup = new PopupWindow(context);
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(false);
        popup.setFocusable(false);
        popup.setAnimationStyle(R.style.animationName);
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

        ImageView img_close = (ImageView) layout.findViewById(R.id.img_close);
        TextView txt_pdf = (TextView) layout.findViewById(R.id.txt_pdf);
        TextView txt_bluetooth = (TextView) layout.findViewById(R.id.txt_bluetooth);
        final String ss = "Entry Time: " + txtaprkat.getText().toString() + "\nExpiry Time: " + txttimerrgiserexpiresdate.getText().toString() + "\nPlate No: " + platno + " Row#" + lot_row + " Lot#" + lot_no + "\nAddress Selected: " + address + "\nAddress Parked:" + parked_add;
        txt_bluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                Intent i = new Intent(getActivity(), Main_Activity.class);
                i.putExtra("date", ss);
                getActivity().startActivity(i);
                //   createandDisplayPdf1();
            }
        });

        txt_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                createandDisplayPdf();
            }
        });

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

    }

    public void manipulatePdf(String src) throws IOException, DocumentException {

        try {

            PdfReader reader = new PdfReader(src);
            int n = reader.getNumberOfPages();
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ParkEZly";
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();

            File file = new File(dir, "parking_ticket" + platno + timeStamp + "1" + ".pdf");
            FileOutputStream fOut = new FileOutputStream(file);
            PdfStamper stamper = new PdfStamper(reader, fOut);
            stamper.setRotateContents(false);
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File file1 = new File(path, "logo.PNG");
            FileOutputStream outStream = null;
            try {
                ImageView imgq = new ImageView(getActivity());
                imgq.setImageResource(R.drawable.rsz_1rsz_splashscreen);

                Bitmap bitmap = ((BitmapDrawable) imgq.getDrawable()).getBitmap();

                outStream = new FileOutputStream(file1);
                bitmap.compress(Bitmap.CompressFormat.PNG, 95, outStream);
                outStream.flush();
                outStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String getpath = Environment.getExternalStorageDirectory() + "/" + "ParkEZly" + "/" + "logo.PNG";
            // image watermark
            Image img = Image.getInstance(getpath);
            float w = img.getScaledWidth();
            float h = img.getScaledHeight();

            // transparency
            PdfGState gs1 = new PdfGState();
            gs1.setFillOpacity(0.2f);
            // properties
            PdfContentByte over;
            Rectangle pagesize;
            float x, y;
            // loop over every page
            for (int i = 1; i <= n; i++) {
                pagesize = reader.getPageSize(i);

                x = (pagesize.getLeft() + pagesize.getRight()) / 2;
                y = (pagesize.getTop() + pagesize.getBottom()) / 2;
                over = stamper.getOverContent(i);
                over.saveState();
                over.setGState(gs1);
                //over.addImage(img,true);
                float gg = x - (w / 2);
                float hh = y - (h / 2);
                Log.e("dcd", String.valueOf(gg));

                Log.e("dcd", String.valueOf(hh));
                over.addImage(img, w, 0, 0, h, x - (w / 2), y - (h / 2));
                //  over.addImage(img, 200,0,200,0,0,0);
                over.restoreState();
            }
            stamper.close();
            reader.close();
            // viewPdf("parking_ticket" + platno +timeStamp+"1" + ".pdf", "parkEZly");
            onther_view(file);
            File file11 = new File(dir, "parking_ticket" + platno + timeStamp + ".pdf");
            file11.delete();
        } catch (IOException os) {
            Log.e("vb", String.valueOf(os));
        }
    }

    public void createandDisplayPdf() {
        Document doc = new Document();
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
            timeStamp = dateFormat.format(new Date());
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ParkEZly";
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();

            File file = new File(dir, "parking_ticket" + platno + timeStamp + ".pdf");
            FileOutputStream fOut = new FileOutputStream(file);
            Font paraFont = new Font(Font.FontFamily.COURIER, 10);
            PdfWriter.getInstance(doc, fOut);


            //open the document
            doc.open();
            String dd = "Parking Entry\n\n";
            Paragraph p = new Paragraph();
            Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 22, Font.BOLD);
            p.setFont(smallBold);
            p.add(dd);
            p.setAlignment(Paragraph.ALIGN_LEFT);
            // p.setPaddingTop(30);
            doc.add(p);

            String w = "Ticket\n\n";
            Paragraph p2 = new Paragraph();
            Font smallBold2 = new Font(Font.FontFamily.TIMES_ROMAN, 22, Font.BOLD);
            p2.setFont(smallBold2);
            p2.add(w);
            p2.setAlignment(Paragraph.ALIGN_LEFT);
            //  p2.setPaddingTop(30);
            doc.add(p2);


            String ss = "Entry Time: " + txtaprkat.getText().toString() + "Expiry Time: " + txttimerrgiserexpiresdate.getText().toString() + "Plate No: " + platno + " Row#" + lot_row + " Lot#" + lot_no + "Address Selected: " + address + " Address Parked:" + parked_add;
            Paragraph vio_fees = new Paragraph("Entry Time: " + txtaprkat.getText().toString());
            vio_fees.setAlignment(Paragraph.ALIGN_LEFT);
            vio_fees.setFont(paraFont);
            // vio_fees.setPaddingTop(30);
            doc.add(vio_fees);

            Paragraph hearing_loc = new Paragraph("Expiry Time: " + txttimerrgiserexpiresdate.getText().toString());
            hearing_loc.setAlignment(Paragraph.ALIGN_LEFT);
            hearing_loc.setFont(paraFont);
            //  hearing_loc.setPaddingTop(30);
            doc.add(hearing_loc);


            Paragraph p1 = new Paragraph("Plate No: " + platno + " Row#" + lot_row + " Lot#" + lot_no);
            p1.setFont(paraFont);
            p1.setAlignment(Paragraph.ALIGN_LEFT);
            //   p1.setPaddingTop(30);
            doc.add(p1);

            Paragraph hearing_ti = new Paragraph("Address Selected: " + address);
            hearing_ti.setAlignment(Paragraph.ALIGN_LEFT);
            hearing_ti.setFont(paraFont);
            // hearing_ti.setPaddingTop(30);
            doc.add(hearing_ti);

            Paragraph ema = new Paragraph("City: " + locationname + ", " + country);
            ema.setAlignment(Paragraph.ALIGN_LEFT);
            ema.setFont(paraFont);
            //  ema.setPaddingTop(30);
            doc.add(ema);

            Paragraph date_ticket = new Paragraph("Address Parked: " + parked_add);
            date_ticket.setAlignment(Paragraph.ALIGN_LEFT);
            date_ticket.setFont(paraFont);
            //  date_ticket.setPaddingTop(30);
            doc.add(date_ticket);


        } catch (DocumentException de) {
            Log.e("PDFCreator", "DocumentException:" + de);
        } catch (IOException e) {
            Log.e("PDFCreator", "ioException:" + e);
        } finally {
            doc.close();
        }

        String getpath = Environment.getExternalStorageDirectory() + "/" + "parkEZly" + "/" + "parking_ticket" + platno + timeStamp + ".pdf";

        try {
            manipulatePdf(getpath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void viewPdf(String file, String directory) {

        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/" + directory + "/" + file);
        /* Uri path = Uri.fromFile(pdfFile);*/
        Uri path = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", pdfFile);
        // Setting the intent for pdf reader
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        // pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getActivity(), "Can't read pdf file", Toast.LENGTH_SHORT).show();
        }
    }

    public void onther_view(File file) {
        File pdfFile = file;
        try {
            if (pdfFile.exists()) //Checking for the file is exist or not
            {
                // Uri path1 = Uri.fromFile(pdfFile);
                Uri path1 = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", pdfFile);
                Intent objIntent = new Intent(Intent.ACTION_VIEW);
                objIntent.setDataAndType(path1, "application/pdf");
                //  objIntent.setFlags(Intent. FLAG_ACTIVITY_CLEAR_TOP);
                objIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                // objIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(objIntent);//Staring the pdf viewer
            } else {
                Toast.makeText(getActivity(), "The file not exists! ", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
        }
    }

    public void timer() {
        try {


            customHandler.removeCallbacks(updateTimerThread);
            customHandler.removeCallbacks(updateTimerThread1);
            int ii = Integer.parseInt(hr);
            final String currentDateandTime = currentdate;
            Date date11 = null;
            try {
                date11 = sdf.parse(currentDateandTime);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date11);
            if (min.equals("Hrs") || min.equals("Hr")) {
                calendar.add(Calendar.HOUR, ii);
            } else if (min.equals("Months") || min.equals("Month")) {
                calendar.add(Calendar.MONTH, ii);
                calendar.add(Calendar.DATE, -1);
            } else if (min.equals("Days") || min.equals("Day")) {
                calendar.add(Calendar.DATE, ii);
            } else if (min.equals("Weeks") || min.equals("Week")) {
                calendar.add(Calendar.WEEK_OF_MONTH, ii);
            } else {
                calendar.add(Calendar.MINUTE, ii);
            }
            String expridate = exitTime;
            try {

                Date value1 = sdf.parse(expridate);
                String localtime2 = sdf.format(value1);
                txttimerrgiserexpiresdate.setText(localtime2);
                Date value = sdf.parse(currentdate);
                String localtime1 = sdf.format(value);
                txtaprkat.setText(localtime1);

            } catch (ParseException e) {
                e.printStackTrace();
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

            if (result.equals("false")) {

                Date date111 = null;
                Date date22 = null;
                try {
                    String ss = expridate;
                    date111 = sdf.parse(ss);
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                Date date2 = null;
                try {

                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime = cal1.getTime();

                    try {
                        String sss = currentdate;
                        date2 = sdf.parse(sss);
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                try {
                    long diff = date111.getTime() - date2.getTime();
                    long timeInSeconds = diff / 1000;
                    long hours, minutes, seconds;
                    hours = timeInSeconds / 3600;
                    timeInSeconds = timeInSeconds - (hours * 3600);
                    minutes = timeInSeconds / 60;
                    timeInSeconds = timeInSeconds - (minutes * 60);
                    seconds = timeInSeconds;
                    int day = (int) (hours / 24);
                    long date_in_notification = 0;
                    SimpleDateFormat df1;
                    df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                    int parking_id = Integer.parseInt(id);
                    if (hours > 0) {

                        if (diff > 2700000) {
                            date_in_notification = diff - 2700000;
                            Calendar timeout = Calendar.getInstance();
                            int gg = (int) date_in_notification;
                            String dddq = df1.format(timeout.getTime());
                            timeout.add(Calendar.MILLISECOND, gg);
                            String ddd = df1.format(timeout.getTime());
                            Log.e("date1", ddd);
                            //timeout.setTimeInMillis(date_in_notification);
                            long ff = timeout.getTimeInMillis();
                            scheduleNotification(getNotification("Alert Parkezly! Parking is going to expire in 45 mints.", parking_id + 1), ff, parking_id + 1);
                        }
                        if (diff > 1800000) {
                            date_in_notification = diff - 1800000;
                            Calendar timeout = Calendar.getInstance();
                            int gg = (int) date_in_notification;
                            String dddq = df1.format(timeout.getTime());
                            timeout.add(Calendar.MILLISECOND, gg);
                            String ddd = df1.format(timeout.getTime());
                            Log.e("date2", ddd);
                            //timeout.setTimeInMillis(date_in_notification);
                            long ff = timeout.getTimeInMillis();
                            scheduleNotification(getNotification("Alert Parkezly! Parking is going to expire in 30 mints.", parking_id + 2), ff, parking_id + 2);
                        }
                        if (diff > 900000) {
                            date_in_notification = diff - 900000;
                            Calendar timeout = Calendar.getInstance();
                            int gg = (int) date_in_notification;
                            String dddq = df1.format(timeout.getTime());
                            timeout.add(Calendar.MILLISECOND, gg);
                            String ddd = df1.format(timeout.getTime());
                            Log.e("date3", ddd);
                            //timeout.setTimeInMillis(date_in_notification);
                            long ff = timeout.getTimeInMillis();
                            scheduleNotification(getNotification("Alert Parkezly! Parking is going to expire in 15 mints.", parking_id + 3), ff, parking_id + 3);
                            //scheduleNotification(getNotification("15 second delay"), date_in_notification,3);
                        }

                        // timer = new vchome.CounterClass(diff, 1000, id);
                        timer1 = new CounterClass(diff, 1000);
                        //timer.start();
                        timer1.start();
                    } else if (minutes > 0) {
                        if (diff > 2700000) {
                            date_in_notification = diff - 2700000;
                            Calendar timeout = Calendar.getInstance();
                            int gg = (int) date_in_notification;
                            String dddq = df1.format(timeout.getTime());
                            timeout.add(Calendar.MILLISECOND, gg);
                            String ddd = df1.format(timeout.getTime());
                            Log.e("date1", ddd);
                            //timeout.setTimeInMillis(date_in_notification);
                            long ff = timeout.getTimeInMillis();
                            scheduleNotification(getNotification("Alert Parkezly! Parking is going to expire in 45 mints.", parking_id + 1), ff, parking_id + 1);
                        }
                        if (diff > 1800000) {
                            date_in_notification = diff - 1800000;
                            Calendar timeout = Calendar.getInstance();
                            int gg = (int) date_in_notification;
                            String dddq = df1.format(timeout.getTime());
                            timeout.add(Calendar.MILLISECOND, gg);
                            String ddd = df1.format(timeout.getTime());
                            Log.e("date2", ddd);
                            //timeout.setTimeInMillis(date_in_notification);
                            long ff = timeout.getTimeInMillis();
                            scheduleNotification(getNotification("Alert Parkezly! Parking is going to expire in 30 mints.", parking_id + 1), ff, parking_id + 2);
                        }
                        if (diff > 900000) {
                            date_in_notification = diff - 900000;
                            Calendar timeout = Calendar.getInstance();
                            int gg = (int) date_in_notification;
                            String dddq = df1.format(timeout.getTime());
                            timeout.add(Calendar.MILLISECOND, gg);
                            String ddd = df1.format(timeout.getTime());
                            Log.e("date3", ddd);
                            //timeout.setTimeInMillis(date_in_notification);
                            long ff = timeout.getTimeInMillis();
                            scheduleNotification(getNotification("Alert Parkezly! Parking is going to expire in 15 mints.", parking_id + 3), ff, parking_id + 3);
                            //scheduleNotification(getNotification("15 second delay"), date_in_notification,3);
                        }
                        //timer = new vchome.CounterClass(diff, 1000, id);
                        timer1 = new CounterClass(diff, 1000);
                        //timer.start();
                        timer1.start();
                    } else if (seconds > 0) {
                        // timer = new vchome.CounterClass(diff, 1000, id);
                        timer1 = new CounterClass(diff, 1000);
                        //timer.start();
                        timer1.start();
                    }


                } catch (Exception e) {

                }
            } else {
                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                Date date1 = null;
                String str1 = null;
                String datef1 = expridate;
                Date localtime = null;

                try {
                    date1 = sdf.parse(sh.getString("exip", txttimerrgiserexpiresdate.getText().toString()));
                    String localtime1 = sdf.format(date1);
                    txttimerrgiserexpiresdate.setText(localtime1);
                } catch (Exception e) {

                }
                Date date111 = null;
                try {
                    String ss = sh.getString("exip", "");
                    date111 = sdf.parse(ss);
                    Log.e("date111", String.valueOf(date111));
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                Date date2 = null;
                try {
                    String currentdate = sdf.format(new Date());

                    try {
                        date2 = sdf.parse(currentdate);
                        Log.e("cuurentdate", String.valueOf(date2));
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }

                } catch (ParseException e) {

                    e.printStackTrace();
                }
                try {
                    long diff = date111.getTime() - date2.getTime();
                    long timeInSeconds = diff / 1000;

                    long hours, minutes, seconds;
                    hours = timeInSeconds / 3600;
                    timeInSeconds = timeInSeconds - (hours * 3600);
                    minutes = timeInSeconds / 60;
                    timeInSeconds = timeInSeconds - (minutes * 60);
                    seconds = timeInSeconds;
                    int day = (int) (hours / 24);
                    long date_in_notification = 0;
                    if (hours > 0) {
                        if (diff > 2700000) {
                            date_in_notification = diff - 2700000;
                            Calendar timeout = Calendar.getInstance();
                            timeout.setTimeInMillis(date_in_notification);
                            long ff = timeout.getTimeInMillis();
                            Log.e("45 min", String.valueOf(ff));
                            // scheduleNotification(getNotification("45 second delay"), date_in_notification,1);

                        }
                        if (diff > 1800000) {
                            date_in_notification = diff - 1800000;
                            Calendar timeout = Calendar.getInstance();
                            timeout.setTimeInMillis(date_in_notification);
                            String ddd = sdf.format(timeout.getTime());
                            long ff = timeout.getTimeInMillis();
                            Log.e("30 min", String.valueOf(ff));
                            Log.e("30 min add date", String.valueOf(ddd));
                            //scheduleNotification(getNotification("30 second delay"), date_in_notification,2);
                        }
                        if (diff > 900000) {
                            date_in_notification = diff - 900000;
                            Calendar timeout = Calendar.getInstance();
                            timeout.setTimeInMillis(date_in_notification);
                            long ff = timeout.getTimeInMillis();
                            Log.e("15 min", String.valueOf(ff));
                            //scheduleNotification(getNotification("15 second delay"), date_in_notification,3);
                        }
                        //timer = new vchome.CounterClass(diff, 1000, id);
                        timer1 = new CounterClass(diff, 1000);
                        //timer.start();
                        timer1.start();
                    } else if (minutes > 0) {
                        if (diff > 2700000) {
                            date_in_notification = diff - 2700000;
                            Calendar timeout = Calendar.getInstance();
                            timeout.setTimeInMillis(date_in_notification);
                            long ff = timeout.getTimeInMillis();
                            Log.e("45 min", String.valueOf(ff));
                            //scheduleNotification(getNotification("45 second delay"), date_in_notification,1);

                        }
                        if (diff > 1800000) {
                            date_in_notification = diff - 1800000;
                            Calendar timeout = Calendar.getInstance();
                            timeout.setTimeInMillis(date_in_notification);
                            long ff = timeout.getTimeInMillis();
                            Log.e("30 min", String.valueOf(ff));
                            String ddd = sdf.format(timeout.getTime());
                            Log.e("30 min", String.valueOf(ff));
                            Log.e("30 min add date", String.valueOf(ddd));
                            //scheduleNotification(getNotification("30 second delay"), date_in_notification,2);
                        }
                        if (diff > 900000) {
                            date_in_notification = diff - 900000;
                            Calendar timeout = Calendar.getInstance();
                            timeout.setTimeInMillis(date_in_notification);
                            long ff = timeout.getTimeInMillis();
                            Log.e("15 min", String.valueOf(ff));
                            //scheduleNotification(getNotification("15 second delay"), date_in_notification,3);
                        }
                        //timer = new vchome.CounterClass(diff, 1000, id);
                        timer1 = new CounterClass(diff, 1000);
                        //timer.start();
                        timer1.start();
                    } else if (seconds > 0) {

                        //timer = new vchome.CounterClass(diff, 1000, id);
                        timer1 = new CounterClass(diff, 1000);
                        //timer.start();
                        timer1.start();
                    }
                } catch (Exception E) {
                }

            }
        } catch (Exception E) {
        }
    }

    @Override
    public void onResume() {

        super.onResume();
    }

    @Override
    public void onPause() {

        super.onPause();
    }

    public void showmap() {
        SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        fm.getMapAsync(googleMap1 -> {
            final GoogleMap googleMap = googleMap1;
            Iterator myVeryOwnIterator = statefullname.keySet().iterator();
            while (myVeryOwnIterator.hasNext()) {
                String key = (String) myVeryOwnIterator.next();
                if (key.equals(statename)) {
                    fullsatename = (String) statefullname.get(key);
                    Log.e("statename", fullsatename);
                    break;
                }
            }
            final LatLng position = new LatLng(lat1, lang1);

            MarkerOptions options = new MarkerOptions();
            Marker melbourne = null;
            // Setting position for the MarkerOptions
            options.position(position);
            options.title(platno);
            options.snippet(fullsatename);
            if (markertype.equals("free") || markertype.equals("Free") || markertype.equals("anywhere") || parking_type.equals("anywhere") || parking_type.equals("random") || parking_type.equals("otherPartner") || parking_type.equals("google") || parking_type.equals("googlePartner") || markertype.equals("managed_free")) {
                melbourne = googleMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.free_star))
                        .position(position));
            } else if (markertype.equals("Managed Paid") || markertype.equals("managed paid") || markertype.equals("managed_guest") || markertype.equals("Managed Free") || markertype.equals("managed free") || markertype.equals("managed")) {
                melbourne = googleMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.rsz_1manages_star))
                        .position(position));
                options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.rsz_1manages_star));
            } else if (markertype.equals("Paid") || markertype.equals("paid")) {
                melbourne = googleMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.paid_star))
                        .position(position));
            } else if (markertype.equals("guest")) {
                melbourne = googleMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.rsz_1manages_star))
                        .position(position));
            }
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 18));
            final long start = SystemClock.uptimeMillis();
            final long duration = 1500L;

            // Cancels the previous animation
            mHandler1.removeCallbacks(mAnimation);
            if (melbourne != null) {
                mAnimation = new BounceAnimation(start, duration, melbourne, mHandler);
                mHandler1.post(mAnimation);
            }

            googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                @Override
                public View getInfoWindow(Marker arg0) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker arg0) {
                    View v = getActivity().getLayoutInflater().inflate(R.layout.otherparkingpin, null);
                    LatLng latLng = arg0.getPosition();
                    TextView txttitle = (TextView) v.findViewById(R.id.txtvalues);
                    TextView txtcontaint = (TextView) v.findViewById(R.id.txttotle);
                    String sa = arg0.getSnippet();
                    txtcontaint.setText(arg0.getSnippet());
                    txttitle.setText(arg0.getTitle());
                    return v;
                }
            });


            try {

                streetViewPanoramaFragment =
                        (SupportStreetViewPanoramaFragment)
                                getChildFragmentManager().findFragmentById(R.id.streetviewpanorama);
                streetViewPanoramaFragment.getChildFragmentManager().beginTransaction().commitAllowingStateLoss();

                SYDNEY = new LatLng(40.714103, -74.006206);
                streetViewPanoramaFragment.getStreetViewPanoramaAsync(
                        new OnStreetViewPanoramaReadyCallback() {
                            @Override
                            public void onStreetViewPanoramaReady(final StreetViewPanorama panorama) {
                                try {
                                    mStreetViewPanorama = panorama;
                                    mStreetViewPanorama.setOnStreetViewPanoramaChangeListener(
                                            timervehicleinfo.this);
                                    mStreetViewPanorama.setOnStreetViewPanoramaCameraChangeListener(
                                            timervehicleinfo.this);
                                    mStreetViewPanorama.setOnStreetViewPanoramaClickListener(
                                            timervehicleinfo.this);
                                    mStreetViewPanorama.setOnStreetViewPanoramaLongClickListener(
                                            timervehicleinfo.this);

                                    mStreetViewPanorama.setPosition(position);


                                    btn_street.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            btn_street.setVisibility(View.GONE);
                                            //  rl_layout_streetview.setVisibility(View.VISIBLE);
                                        }
                                    }, 8000);
                                    ActionStartsHere();


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                rl_layout_streetview.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN:
                                x1 = event.getX();
                                break;
                            case MotionEvent.ACTION_UP:
                                x2 = event.getX();
                                float deltaX = x2 - x1;
                                if (deltaX < 0) {
                                    Toast.makeText(getActivity(),
                                            "Right to Left swipe",
                                            Toast.LENGTH_SHORT).show();
                                } else if (deltaX > 0) {
                                    Toast.makeText(getActivity(),
                                            "Left to Right swipe",
                                            Toast.LENGTH_SHORT).show();
                                }
                                break;
                        }

                        return false;
                    }
                });
            } catch (Exception e) {
            }
        });

    }

    public void ActionStartsHere() {
        againStartGPSAndSendFile();
    }

    public void againStartGPSAndSendFile() {
        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.e("countdown", "Start:-" + millisUntilFinished);
            }

            @Override
            public void onFinish() {
                //ActionStartsHere();
                showstreetmap();

            }

        }.start();
    }

    @Override
    public void onStreetViewPanoramaCameraChange(StreetViewPanoramaCamera streetViewPanoramaCamera) {

    }

    @Override
    public void onStreetViewPanoramaChange(StreetViewPanoramaLocation streetViewPanoramaLocation) {
        if (streetViewPanoramaLocation != null && streetViewPanoramaLocation.links != null) {
            // location is present
            Log.e("park for free", "location  changed");
            rl_progressbar1.setVisibility(View.GONE);
        } else {
            // location not available
            Log.e("park for free", "location  not chmage");
            rl_progressbar1.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStreetViewPanoramaClick(StreetViewPanoramaOrientation orientation) {
        try {
            Point point = mStreetViewPanorama.orientationToPoint(orientation);

            if (point != null) {
                try {

                    if (mStreetViewPanorama.getLocation() != null) {
                        showstreetmap();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                showstreetmap();
            }
        } catch (Exception E) {
            showstreetmap();
        }
    }

    @Override
    public void onStreetViewPanoramaLongClick(StreetViewPanoramaOrientation streetViewPanoramaOrientation) {

    }

    public void showstreetmap() {
        try {
            streetViewPanoramaFragment =
                    (SupportStreetViewPanoramaFragment)
                            getChildFragmentManager().findFragmentById(R.id.streetviewpanorama);
            SYDNEY = new LatLng(lat1, lang1);
            mStreetViewPanorama = null;
            mStreetViewPanoramaView = new StreetViewPanoramaView(getActivity());
            streetViewPanoramaFragment.getStreetViewPanoramaAsync(
                    new OnStreetViewPanoramaReadyCallback() {
                        @Override
                        public void onStreetViewPanoramaReady(final StreetViewPanorama panorama) {
                            mStreetViewPanorama = null;
                            try {
                                mStreetViewPanorama = panorama;

                                mStreetViewPanorama.setOnStreetViewPanoramaChangeListener(
                                        timervehicleinfo.this);
                                mStreetViewPanorama.setOnStreetViewPanoramaCameraChangeListener(
                                        timervehicleinfo.this);
                                mStreetViewPanorama.setOnStreetViewPanoramaClickListener(
                                        timervehicleinfo.this);
                                mStreetViewPanorama.setOnStreetViewPanoramaLongClickListener(
                                        timervehicleinfo.this);

                                mStreetViewPanorama.setPosition(SYDNEY);
                                Log.e("data", "secondtimelod");
                            } catch (Exception e) {
                            }
                        }

                    });

        } catch (Exception e) {
        }

    }

    private void showrandomnumber(Activity context, String token) {

        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.processtoexit, viewGroup, false);
        final PopupWindow popup = new PopupWindow(context);
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        popup.setAnimationStyle(R.style.animationName);
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

        TextView btn_exit, btn_cancel;
        EditText edit;

        btn_exit = (TextView) layout.findViewById(R.id.btn_exit);
        btn_cancel = (TextView) layout.findViewById(R.id.btn_can);

        edit = (EditText) layout.findViewById(R.id.edit_randomno);
        edit.setText(token);
        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer1 != null) {
                    //timer.cancel();
                    //timer.onFinish();
                    timer1.cancel();
                    timer1.onFinish();
                }
                popup.dismiss();
                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                sh.edit().clear().commit();
                SharedPreferences isexieornot = getActivity().getSharedPreferences("isexitornot", Context.MODE_PRIVATE);
                isexieornot.edit().clear().commit();
                new exitsparking().execute();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (isFinishing) {
            customHandler.removeCallbacks(updateTimerThread);
            customHandler.removeCallbacks(updateTimerThread1);
        }
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @SuppressLint("NewApi")
    public class CounterClass extends CountDownTimer {

        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            txttimer.setText("0 : 00 : 00");

        }

        @SuppressLint("NewApi")
        @TargetApi(Build.VERSION_CODES.GINGERBREAD)
        @Override
        public void onTick(long millisUntilFinished) {

            long millis = millisUntilFinished;
            String hms = String.format("%02d : %02d : %02d", TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
            System.out.println(hms);
            long h = TimeUnit.MILLISECONDS.toHours(millis);
            long min = TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis));

            long s = TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis));
            txttimer.setText(hms);
        }
    }

    public class exitsparking extends AsyncTask<String, String, String> {
        String exit_status = "_table/parked_cars";
        JSONObject json, json1;
        String re_id = "null";

        @Override
        protected void onPreExecute() {

            rl_progessbar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            currentdate = sdf.format(new Date());
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", id);
            jsonValues.put("parking_status", "EXIT");
            jsonValues.put("exit_date_time", currentdate);

            if (mIsPaymentPaid) {
                jsonValues.put("ParkNow_PostPayment_Status", "PAID");
                jsonValues.put("wallet_trx_id", wallet_id);
                if (paymentmethod.equals("Paypal")) {
                    jsonValues.put("ipn_txn_id", transection_id);
                    jsonValues.put("ipn_payment", "Paypal");
                    jsonValues.put("ipn_status", "success");
                    jsonValues.put("ipn_address", "");
                } else {
                    jsonValues.put("ipn_txn_id", "");
                    jsonValues.put("ipn_payment", "");
                    jsonValues.put("ipn_status", "");
                    jsonValues.put("ipn_address", "");
                }
            }

            JSONObject json = new JSONObject(jsonValues);
            //  String url = "http://100.12.26.176:8006/api/v2/pzly01live7/_table/poiv2";
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
//setting json object to post request.
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
//this is your response:
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.d("re_id", re_id);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progessbar != null)
                rl_progessbar.setVisibility(View.GONE);

            if (getActivity() != null && json1 != null) {
                if (!re_id.equals("null")) {
                    SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                    String id1 = logindeatl.getString("id", "null");
                    if (id1.equals("null")) {
                        ArrayList<item> arraylist = new ArrayList<>();
                        SharedPreferences sharedPrefs = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
                        Gson gson = new Gson();
                        String json = sharedPrefs.getString("car", null);
                        Type type = new TypeToken<ArrayList<item>>() {
                        }.getType();
                        arraylist = gson.fromJson(json, type);
                        if (arraylist == null) {
                            arraylist = new ArrayList<>();
                        }
                        if (arraylist.size() > 0) {
                            for (int j = 0; j < arraylist.size(); j++) {
                                String id = arraylist.get(j).getId();
                                if (id.equals(re_id)) {
                                    arraylist.remove(j);
                                }
                            }
                        }
                        SharedPreferences sharedPrefs1 = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPrefs1.edit();
                        Gson gson1 = new Gson();
                        String json1 = gson1.toJson(arraylist);
                        editor.putString("car", json1);
                        editor.commit();
                    }

                    if (parkedCar != null && parkedCar.getReservation_id() != null
                            && !TextUtils.isEmpty(parkedCar.getReservation_id())) {
                        new updateReservationParking().execute();
                    } else {
                        if (location_lot_id != null && !TextUtils.isEmpty(location_lot_id)) {
                            new updateLocationLot().execute();
                        } else {
                            ((vchome) getActivity()).showmapandhidefragment();
                        }
                    }
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Something went wrong");
                    alertDialog.setMessage("Please try again");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
            super.onPostExecute(s);
        }
    }

    public class updateReservationParking extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String rid = logindeatl.getString("id", "null");
        String id1 = "null";
        JSONObject json, json1;
        String pov2 = "_table/reserved_parkings";
        //        String pov2 = "_table/sdafga";
        boolean isPayLater = false;
        String paramOption = "";
        private String dueBy = "";
        SharedPreferences logindeatl1 = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl1.getString("id", "null");

        @Override
        protected void onPreExecute() {
            rl_progessbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            currentdate = sdf.format(new Date());
            if (params.length != 0) {
                paramOption = params[0];
            }
            isPayLater = !TextUtils.isEmpty(paramOption) && paramOption.equals("payLater");
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", parkedCar.getReservation_id());
            jsonValues.put("parking_status", "EXIT");
            jsonValues.put("exit_date_time", currentdate);
            if (mIsPaymentPaid) {
                jsonValues.put("reserve_payment_paid", "PAID");
                jsonValues.put("ParkNow_PostPayment_Status", "PAID");
                jsonValues.put("wallet_trx_id", wallet_id);
                if (paymentmethod.equals("Paypal")) {
                    jsonValues.put("ipn_txn_id", transection_id);
                    jsonValues.put("ipn_payment", "Paypal");
                    jsonValues.put("ipn_status", "success");
                    jsonValues.put("ipn_address", "");
                } else {
                    jsonValues.put("ipn_txn_id", "");
                    jsonValues.put("ipn_payment", "");
                    jsonValues.put("ipn_status", "");
                    jsonValues.put("ipn_address", "");
                }
            }

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + pov2;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut httpPut = new HttpPut(url);
            httpPut.setHeader("X-DreamFactory-Application-Name", "parkezly");
            httpPut.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            httpPut.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(httpPut);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "    updateReserveParking:  Response:  " + responseStr);
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id1 = c.getString("id");

                    }
                } catch (IOException e) {
                    Log.e("#DEBUG", "    reserveNow:  Error:  " + e.getMessage());
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.e("#DEBUG", "    reserveNow:  Error:  " + e.getMessage());
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (getActivity() != null) {
                rl_progessbar.setVisibility(View.GONE);
                if (json1 != null) {
                    if (!id1.equals("null")) {
                        if (location_lot_id != null && !TextUtils.isEmpty(location_lot_id)) {
                            new updateLocationLot().execute();
                        } else {
                            ((vchome) getActivity()).showmapandhidefragment();
                        }
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class updateLocationLot extends AsyncTask<String, String, String> {
        String locationLotUrl = "_table/location_lot";
        JSONObject json, json1;
        String re_id = "null";

        @Override
        protected void onPreExecute() {

            rl_progessbar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", location_lot_id);
            jsonValues.put("plate_no", "");
            jsonValues.put("plate_state", "");
            jsonValues.put("occupied", "NO");
            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + locationLotUrl;
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.d("re_id", re_id);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progessbar != null)
                rl_progessbar.setVisibility(View.GONE);

            if (getActivity() != null && json1 != null) {
                if (!re_id.equals("null")) {
                    ((vchome) getActivity()).showmapandhidefragment();
                }
                super.onPostExecute(s);

            }

        }
    }

    public class exitsparkingandrenew extends AsyncTask<String, String, String> {
        String exit_status = "_table/parked_cars";
        JSONObject json, json1;
        String re_id = "null";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            currentdate = sdf.format(new Date());
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", id);
            jsonValues.put("parking_status", "EXIT");
            jsonValues.put("exit_date_time", currentdate);
            JSONObject json = new JSONObject(jsonValues);
            //  String url = "http://100.12.26.176:8006/api/v2/pzly01live7/_table/poiv2";
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
//setting json object to post request.
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);

//this is your response:
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.d("re_id", re_id);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progessbar != null)
                rl_progessbar.setVisibility(View.GONE);

            if (getActivity() != null && json1 != null) {
                if (!re_id.equals("null")) {
                    SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                    String id1 = logindeatl.getString("id", "null");
                    if (id1.equals("null")) {
                        ArrayList<item> arraylist = new ArrayList<>();
                        SharedPreferences sharedPrefs = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
                        Gson gson = new Gson();
                        String json = sharedPrefs.getString("car", null);
                        Type type = new TypeToken<ArrayList<item>>() {
                        }.getType();
                        arraylist = gson.fromJson(json, type);
                        if (arraylist == null) {
                            arraylist = new ArrayList<>();
                        }
                        if (arraylist.size() > 0) {
                            for (int j = 0; j < arraylist.size(); j++) {
                                String id = arraylist.get(j).getId();
                                if (id.equals(re_id)) {
                                    arraylist.remove(j);
                                }
                            }
                        }
                        SharedPreferences sharedPrefs1 = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPrefs1.edit();
                        Gson gson1 = new Gson();
                        String json1 = gson1.toJson(arraylist);
                        editor.putString("car", json1);
                        editor.commit();
                    }
                    SharedPreferences d = getActivity().getSharedPreferences("renew", Context.MODE_PRIVATE);
                    SharedPreferences.Editor dd = d.edit();
                    dd.putString("renew", "yes");
                    dd.putString("marker", markertype);
                    dd.commit();

                    if (parking_free.equals("yes")) {

                        if (markertype.equals("free") || markertype.equals("Free") || markertype.equals("anywhere") || markertype.equals("managed") || markertype.equals("managed_guest")) {
                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.replace(R.id.My_Container_1_ID, new freeparkhere(), "NewFragmentTag");
                            ft.commit();
                        }
                    } else {
                        if (markertype.equals("Paid") || markertype.equals("paid") || markertype.equals("Managed Paid") || markertype.equals("managed paid") || markertype.equals("managed_guest") || markertype.equals("Managed Free") || markertype.equals("managed free") || markertype.equals("managed")) {
                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.replace(R.id.My_Container_1_ID, new paidparkhere(), "NewFragmentTag");
                            ft.commit();
                        } else if (markertype.equals("otherPartner") || markertype.equals("other parking") || markertype.equals("google") || markertype.equals("partner") || markertype.equals("googlePartner")) {
                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.replace(R.id.My_Container_1_ID, new Google_Paid_Parking(), "googlePaidParking");
                            ft.commit();
                        }
                    }
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Something went wrong");
                    alertDialog.setMessage("Please try again");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.show();
                }
            }
            super.onPostExecute(s);
        }
    }

    public class getparkingrules extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String parkingurl = "_table/township_parking_rules";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            parking_rules.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        String locationcode1 = c.getString("location_code");
                        String id = c.getString("id");
                        // String date_time = c.getString("date_time");
                        // String location_name = c.getString("location_name");
                        String time_rule = c.getString("time_rule");
                        String day_rule = c.getString("day_type");
                        String max_time = c.getString("max_duration");
                        // String enforced = c.getString("enforced");
                        //String date_special_enforce = c.getString("date_special_enforce");
                        String custom_notice = c.getString("custom_notice");
                        String active = c.getString("active");
                        // String date = c.getString("date");
                        // String city = c.getString("city");
                        // String state = c.getString("state");
                        String pricing = c.getString("pricing");
                        String pricing_duration = c.getString("pricing_duration");
                        //String zip_code = c.getString("zip_code");
                        String start_time = c.getString("start_time");
                        String end_time = c.getString("end_time");
                        String this_day = c.getString("day_type").toLowerCase();
                        // String parking_times = c.getString("parking_times");
                        //String no_parking_times = c.getString("no_parking_times");
                        //String start_end_rule = c.getString("start_end_rule");
                        // String this_hour = c.getString("this_hour");
                        String max_hours = c.getString("duration_unit");
                        String start_hour = c.getString("start_hour");
                        String end_hour = c.getString("end_hour");
                        // String area_level = c.getString("area_level");
                        // String area_value = c.getString("area_value");
                        // String marker_type = c.getString("marker_type");

                        item1.setId(id);
                        // item1.setDate_time(date_time);
                        item1.setLocation_code(locationcode1);
                        //  item1.setLoationname(location_name);
                        item1.setTime_rule(time_rule);
                        item1.setDay_rule(day_rule);
                        item1.setMax_time(max_time);
                        //item1.setEnforced(enforced);
                        //item1.setDate_special_enforce(date_special_enforce);
                        item1.setCustom_notice(custom_notice);
                        item1.setActive(active);
                        //item1.setDate(date);
                        // item1.setCity(city);
                        // item1.setState(state);
                        item1.setPricing(pricing);
                        item1.setPricing_duration(pricing_duration);
                        // item1.setZip_code(zip_code);
                        item1.setStart_time(start_time);
                        item1.setEnd_time(end_time);
                        item1.setThis_day(this_day);
                        //  item1.setParking_times(parking_times);
                        //  item1.setNo_parking_times(no_parking_times);
                        // item1.setStart_end_rule(start_end_rule);
                        //  item1.setThis_hour(this_hour);
                        item1.setMax_hours(max_hours);
                        item1.setStart_hour(start_hour);
                        item1.setEnd_hour(end_hour);
                        //   item1.setArea_level(area_level);
                        //   item1.setArea_value(area_value);
                        // item1.setMarker_type(marker_type);
                        item1.setLoationcode1(locationcode1);

                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item1.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }

                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item1.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }

                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item1.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }

                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item1.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }

                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item1.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }

                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item1.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item1.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }

                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item1.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }

                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item1.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }

                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item1.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }

                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item1.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }
                        parking_rules.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs;
            if (getActivity() != null && json != null) {
            }
            super.onPostExecute(s);
        }
    }


    public class gettownship_parking extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String managedlosturl = "_table/township_parking_partners";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("find_parking_nearby", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));


            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);

                    json1 = json.getJSONArray("resource");

                    for (int i = 0; i < json1.length(); i++) {
                        item item = new item();
                        JSONObject josnget = json1.getJSONObject(i);
                        item.setRenew(josnget.getString("renewable"));
                        item.setLocation_code(josnget.getString("location_code"));
                        item.setTownshipcode(josnget.getString("township_id"));
                        township_parking.add(item);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null && township_parking.size() > 0) {
                for (int i = 0; i < township_parking.size(); i++) {
                    String loc_code = township_parking.get(i).getLocation_code();
                    if (loc_code.equals(locationname)) {
                        String renew = township_parking.get(i).getRenew();
                        renew = renew != null ? (!renew.equals("null") ? (!renew.equals("") ? renew : "") : "") : "";
                        if (renew.equals("1")) {
                            rl_renew.setVisibility(View.VISIBLE);
                        } else {
                            rl_renew.setVisibility(View.GONE);
                        }
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class getgoogleparking extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String parkingurl = "_table/google_parking_partners";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            googlepartnerarray.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setPlace_id(c.getString("place_id"));
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setTitle(c.getString("title"));
                        item1.setAddress(c.getString("address"));
                        item1.setEffect(c.getString("in_effect"));
                        item1.setActive(c.getString("active"));
                        item1.setRenew(c.getString("renewable"));
                        item1.setParking_times(c.getString("parking_times"));
                        item1.setNo_parking_times(c.getString("no_parking_times"));
                        item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        item1.setWeek_ebd_discount(c.getString("weekend_special_diff"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setTotal_lots(c.getString("lots_total"));
                        item1.setLots_aval(c.getString("lots_avbl"));
                        item1.setOff_peak_start(c.getString("off_peak_starts"));
                        item1.setOff_peak_end(c.getString("off_peak_ends"));
                        item1.setId(c.getString("id"));
                        googlepartnerarray.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs;
            if (getActivity() != null && json != null) {
                if (googlepartnerarray.size() > 0) {
                    for (int i = 0; i < googlepartnerarray.size(); i++) {
                        String loc_code = googlepartnerarray.get(i).getLocation_code();
                        if (loc_code.equals(locationname)) {
                            String renew = googlepartnerarray.get(i).getRenew();
                            renew = renew != null ? (renew.equals("null") ? (renew.equals("") ? renew : "") : "") : "";
                            if (renew.equals("1")) {
                                rl_renew.setVisibility(View.VISIBLE);
                            } else {
                                rl_renew.setVisibility(View.GONE);
                            }

                            break;
                        }
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class getprivate_parking extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String managedlosturl = "_table/other_parking_partners";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            parkingarray.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("find_parking_nearby", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));


            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item item1 = new item();
                        JSONObject c = array.getJSONObject(i);
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setEffect(c.getString("in_effect"));
                        item1.setActive(c.getString("active"));
                        item1.setRenew(c.getString("renewable"));
                        item1.setId(c.getString("id"));
                        parkingarray.add(item1);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null && parkingarray.size() > 0) {
                for (int i = 0; i < parkingarray.size(); i++) {
                    String loc_code = parkingarray.get(i).getLocation_code();
                    if (loc_code.equals(locationname)) {
                        String renew = parkingarray.get(i).getRenew();
                        renew = renew != null ? (renew.equals("null") ? (renew.equals("") ? renew : "") : "") : "";
                        if (renew.equals("1")) {
                            rl_renew.setVisibility(View.VISIBLE);
                        } else {
                            rl_renew.setVisibility(View.GONE);
                        }

                        break;
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    private void scheduleNotification(Notification notification, long delay, int postion) {

        Intent notificationIntent = new Intent(getActivity(), NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, postion);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), postion, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        GregorianCalendar hbhbh = new GregorianCalendar();
        hbhbh.setTimeInMillis(delay);
        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, hbhbh.getTimeInMillis(), pendingIntent);
    }

    private Notification getNotification(String content, int postion) {
        long[] vibrate = {0, 100, 200, 300};
        String strtitle = getActivity().getString(R.string.customnotificationtitle);
        // Set Notification Text
        String strtext = "Alert Parkezly! Parking is almost expired.";
        Intent intent = new Intent(getActivity().getApplicationContext(), vchome.class);
        intent.putExtra("title", strtitle);
        intent.putExtra("text", strtext);
        intent.putExtra("id", id);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pIntent = PendingIntent.getActivity(getActivity().getApplicationContext(), postion, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        androidx.core.app.NotificationCompat.Builder builder = new NotificationCompat.Builder(getActivity())
                .setSmallIcon(R.mipmap.icon)
                .setTicker(getActivity().getString(R.string.customnotificationticker))
                .setVibrate(vibrate)
                .setContentIntent(pIntent)
                .setContentTitle(strtitle)
                .setContentText(content)
                .setAutoCancel(true);
        return builder.build();
    }

    public void canclealarm(int id) {
        Intent notificationIntent = new Intent(getActivity(), NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, id);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(getActivity(), id + 1, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent alarmIntent1 = PendingIntent.getBroadcast(getActivity(), id + 2, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent alarmIntent3 = PendingIntent.getBroadcast(getActivity(), id + 3, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
        alarmManager.cancel(alarmIntent);
        alarmIntent.cancel();
        alarmManager.cancel(alarmIntent1);
        alarmIntent1.cancel();
        alarmManager.cancel(alarmIntent3);
        alarmIntent3.cancel();
    }

    private class fetchParkedCar extends AsyncTask<String, String, String> {

        JSONObject json;
        String parkedCarsUrl = "_table/parked_cars";

        @Override
        protected void onPreExecute() {
            parkedCarsUrl = parkedCarsUrl + "?filter=id=" + id;
            rl_progessbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            String url = getString(R.string.api) + getString(R.string.povlive) + parkedCarsUrl;
            Log.e("#DEBUG", "  fetchParkedCar:  URL:  " + url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "  fetchParkedCar:  Response:  " + responseStr);
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        parkedCar = new item();
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            parkedCar.setId(object.getString("id"));
                        }
                        if (object.has("reservation_id")
                                && !TextUtils.isEmpty(object.getString("reservation_id"))
                                && !object.getString("reservation_id").equals("null")) {
                            parkedCar.setReservation_id(object.getString("reservation_id"));
                        }
                        if (object.has("ParkedNow_PayLater")
                                && !TextUtils.isEmpty(object.getString("ParkedNow_PayLater"))
                                && !object.getString("ParkedNow_PayLater").equals("null")) {
                            parkedCar.setParkedNow_PayLater(object.getBoolean("ParkedNow_PayLater"));
                            if (parkedCar.isParkedNow_PayLater()) {
                                if (object.has("ParkNow_PostPayment_term")
                                        && !TextUtils.isEmpty(object.getString("ParkNow_PostPayment_term"))
                                        && !object.getString("ParkNow_PostPayment_term").equals("null")) {
                                    parkedCar.setParkNow_PostPayment_term(object.getString("ParkNow_PostPayment_term"));
                                }
                                if (object.has("ParkNow_PostPayment_Fee")
                                        && !TextUtils.isEmpty(object.getString("ParkNow_PostPayment_Fee"))
                                        && !object.getString("ParkNow_PostPayment_Fee").equals("null")) {
                                    parkedCar.setParkNow_PostPayment_Fee(object.getString("ParkNow_PostPayment_Fee"));
                                }
                                if (object.has("ParkNow_PostPayment_LateFee")
                                        && !TextUtils.isEmpty(object.getString("ParkNow_PostPayment_LateFee"))
                                        && !object.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                                    parkedCar.setParkNow_PostPayment_LateFee(object.getString("ParkNow_PostPayment_LateFee"));
                                }
                                if (object.has("ParkNow_PostPayment_DueAmount")
                                        && !TextUtils.isEmpty(object.getString("ParkNow_PostPayment_DueAmount"))
                                        && !object.getString("ParkNow_PostPayment_DueAmount").equals("null")) {
                                    parkedCar.setParkNow_PostPayment_DueAmount(object.getString("ParkNow_PostPayment_DueAmount"));
                                }
                                if (object.has("ParkNow_PostPayment_Status")
                                        && !TextUtils.isEmpty(object.getString("ParkNow_PostPayment_Status"))
                                        && !object.getString("ParkNow_PostPayment_Status").equals("null")) {
                                    parkedCar.setParkNow_PostPayment_Status(object.getString("ParkNow_PostPayment_Status"));
                                }
                            }
                        } else parkedCar.setParkedNow_PayLater(false);

//                        Log.e("#DEBUG", "   ParkedCar:  " + new Gson().toJson(parkedCar));


                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   fetchLocationLots:  Error:  " + e.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   fetchLocationLots:  Error:  " + e.getMessage());
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                rl_progessbar.setVisibility(View.GONE);
                if (json != null) {

                }
            }
        }
    }

    public class getwalletbal extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String userid = logindeatl.getString("id", "null");
        JSONObject json, json1;
        String wallbalurl = "_table/user_wallet?order=date_time%20DESC";
        //String wallbalurl = "_table/user_wallet?filter=(user_id%3D'"+userid+"')";
        String w_id;

        @Override
        protected void onPreExecute() {
            rl_progessbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + wallbalurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            HttpEntity resEntity = response.getEntity();
            String responseStr = null;
            if (response != null) {
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        String user = c.getString("user_id");

                        if (user.equals(userid)) {
                            nbal = c.getString("new_balance");
                            if (!nbal.equals("null")) {
                                cbal = c.getString("current_balance");
                                item.setAmount(nbal);
                                item.setPaidamount(cbal);
                                wallbalarray.add(item);
                                break;
                            }
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progessbar != null)
                rl_progessbar.setVisibility(View.GONE);
            // progressBar.setVisibility(View.GONE);

            if (getActivity() != null && json != null) {
                if (wallbalarray.size() > 0) {
                    double bal = Double.parseDouble(cbal);
                    if (bal > 0) {
                        finalpayamount = Double.parseDouble(parkedCar.getParkNow_PostPayment_DueAmount());
                        newbal = bal - finalpayamount;
                        String finallab2 = String.format("%.2f", bal);
                        finallab = String.format("%.2f", finalpayamount);
                        if (newbal < 0) {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("alert");
                            alertDialog.setMessage("You don't have enough funds in your wallet.");
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialog.show();

                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("Wallet Balance: $" + finallab2);
                            alertDialog.setMessage("Would you like to pay from your ParkEZly wallet ? With service and transaction fee your total will be " + "$" + finallab + "");
                            alertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    ip = getLocalIpAddress();
                                    new updatewallet().execute();
                                }
                            });
                            alertDialog.setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialog.show();
                        }
                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("alert");
                        alertDialog.setMessage("You don't have enough funds in your wallet.");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }

                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("alert");
                    alertDialog.setMessage("You don't have enough funds in your wallet.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
            super.onPostExecute(s);

        }
    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("ip", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("aax", ex.toString());
        }
        return null;
    }

    public class updatewallet extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONObject json12;
        String walleturl = "_table/user_wallet";
        String w_id = "";

        @Override
        protected void onPreExecute() {
            rl_progessbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            currentdate = sdf.format(new Date());
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("paid_date", currentdate);
            jsonValues1.put("last_paid_amt", parkedCar.getParkNow_PostPayment_DueAmount());
            jsonValues1.put("ip", ip);
            jsonValues1.put("remember_me", "");
            jsonValues1.put("current_balance", newbal);
            jsonValues1.put("ipn_txn_id", "");
            jsonValues1.put("ipn_custom", "");
            jsonValues1.put("ipn_payment", "");
            jsonValues1.put("date_time", currentdate);
            jsonValues1.put("ipn_address", "");
            jsonValues1.put("user_id", id);
            jsonValues1.put("new_balance", newbal);
            jsonValues1.put("action", "deduction");

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        wallet_id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            rl_progessbar.setVisibility(View.VISIBLE);
            //progressBar.setVisibility(View.GONE);
            if (getActivity() != null && json12 != null) {

                if (!wallet_id.equals("")) {
                    //Exit parking
                    mIsPaymentPaid = true;
                    exitParking();
                }

            }
            super.onPostExecute(s);
        }
    }

}
