package com.softmeasures.eventezlyadminplus.frament.config_setting;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragmentGroupSettingsBinding;
import com.softmeasures.eventezlyadminplus.databinding.PopupGroupTypeBinding;
import com.softmeasures.eventezlyadminplus.models.Manager;
import com.softmeasures.eventezlyadminplus.models.call_chat.GroupConfSettings;
import com.softmeasures.eventezlyadminplus.models.call_chat.GroupConfSettings.GroupSettingData;
import com.softmeasures.eventezlyadminplus.models.rest.APIClient;
import com.softmeasures.eventezlyadminplus.models.rest.APIInterface;
import com.softmeasures.eventezlyadminplus.models.users.UserProfile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.softmeasures.eventezlyadminplus.services.Constants.GROUP_TYPE_MYCONTACT;
import static com.softmeasures.eventezlyadminplus.services.Constants.GROUP_TYPE_ORGANIZATIONAL;
import static com.softmeasures.eventezlyadminplus.services.Constants.GROUP_TYPE_PUBLIC;

public class GroupSettingFragment extends BaseFragment {
    FragmentGroupSettingsBinding binding;

    Manager selectedManger = null;
    UserProfile.UserProfileData selectedUser = null;
    private String TAG = "#DEBUG GroupSetting";

    public static GroupSettingData groupSettings = null;
    APIInterface apiInterface;
    SharedPreferences logindeatl;
    String user_id, role;
    boolean isEdit = false, isInit = true, isMultiple = false;
    ArrayList<Manager> selectedMangers = new ArrayList<>();
    ArrayList<UserProfile.UserProfileData> selectedUsers = new ArrayList<>();
    ArrayList<GroupSettingData> tempGroupConfSetting = new ArrayList<>();
    ArrayList<GroupSettingData> groupConfSettingList = new ArrayList<>();
    private PopupWindow popupmanaged;
    boolean isOrg = false, isPublic = false, isMyContact = false, isUser = false;

    private PartnerAdapter partnerAdapter;
    private UserAdapter userAdapter;
    private int pos = 0;
    ArrayList<String> seeGroup = new ArrayList<>();
    ArrayList<String> addGroup = new ArrayList<>();
    ArrayList<String> addUser = new ArrayList<>();
    ArrayList<String> seeUser = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_group_settings, container, false);
        if (getArguments() != null) {
            isMultiple = getArguments().getBoolean("isMultiple", false);
            selectedManger = new Gson().fromJson(getArguments().getString("selectedManager", null), Manager.class);
            selectedUser = new Gson().fromJson(getArguments().getString("selectedUser", null), UserProfile.UserProfileData.class);
            selectedMangers = new Gson().fromJson(getArguments().getString("selectedManagers", null), new TypeToken<ArrayList<Manager>>() {
            }.getType());
            selectedUsers = new Gson().fromJson(getArguments().getString("selectedUsers", null), new TypeToken<ArrayList<UserProfile.UserProfileData>>() {
            }.getType());
        }
        Log.d(TAG, " dtast: " + getArguments().getString("selectedManager"));
        Log.d(TAG, " user: " + getArguments().getString("selectedUser"));
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        initViews(view);
        setListeners();
        updateViews();
    }

    @Override
    protected void updateViews() {
        if (selectedManger != null) {
            binding.tvTitleName.setText("Company Name");
            binding.tvTitleCode.setText("Company Code");
            binding.etCompanyName.setText(selectedManger.getLot_manager());
            binding.etCompanyCode.setText(selectedManger.getManager_id());
        } else if (selectedUser != null) {
            binding.tvTitleName.setText("User Name");
            binding.tvTitleCode.setText("User Role");
            if (!TextUtils.isEmpty(selectedUser.getUser_name()))
                binding.etCompanyName.setText(selectedUser.getUser_name());
            else binding.etCompanyName.setText(selectedUser.getEmail());
            binding.etCompanyCode.setText(selectedUser.getRole());
        }
    }

    @Override
    protected void setListeners() {
        binding.switchGroupType.setOnCheckedChangeListener((buttonView, isChecked) -> {
            groupSettings.setGroup_type(isChecked ? 1 : 0);
            binding.llGroupConfig.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        });
        binding.etCompanyName.setOnClickListener(v -> {
            if (isMultiple) selectedCompanyiesUsers();
        });
        binding.etCompanyCode.setOnClickListener(v -> {
            if (isMultiple) selectedCompanyiesUsers();
        });
        binding.etCanAddGroupType.setOnClickListener(v -> {
            isUser = false;
            showAddEditGroupTypePupup(binding.etCanAddGroupType, addGroup);
        });
        binding.etCanSeeGroupType.setOnClickListener(v -> {
            isUser = false;
            showAddEditGroupTypePupup(binding.etCanSeeGroupType, seeGroup);
        });
        binding.etCanAddUserType.setOnClickListener(v -> {
            isUser = true;
            showAddEditGroupTypePupup(binding.etCanAddUserType, addUser);
        });
        binding.etCanSeeUserType.setOnClickListener(v -> {
            isUser = true;
            showAddEditGroupTypePupup(binding.etCanSeeUserType, seeUser);
        });
        binding.btnSaveConfig.setOnClickListener(v -> {
            groupSettings.setCan_add_group_type(new Gson().toJson(addGroup));
            groupSettings.setCan_add_user_type(new Gson().toJson(addUser));
            groupSettings.setCan_see_group_type(new Gson().toJson(seeGroup));
            groupSettings.setCan_see_user_type(new Gson().toJson(seeUser));
            if (isMultiple) {
                if (selectedMangers != null) {
                    Gson gson = new Gson();
                    if (tempGroupConfSetting != null) tempGroupConfSetting.clear();
                    for (int i = 0; i < selectedMangers.size(); i++) {
                        groupSettings.setCompany_id(selectedMangers.get(i).getId());
                        groupSettings.setCompany_name(selectedMangers.get(i).getLot_manager());
                        groupSettings.setCompany_type_id(selectedMangers.get(i).getManager_type_id());
                        groupSettings.setId(selectedMangers.get(i).getSelected_id());
                        tempGroupConfSetting.add(gson.fromJson(new Gson().toJson(groupSettings), GroupSettingData.class));
                    }
                    pos = 0;
                    add_editUserGroupSetting(tempGroupConfSetting.get(0), tempGroupConfSetting.get(0).getId() != 0);
                } else if (selectedUsers != null) {
                    Gson gson = new Gson();
                    if (tempGroupConfSetting != null) tempGroupConfSetting.clear();
                    for (int i = 0; i < selectedUsers.size(); i++) {
                        groupSettings.setUser_id(selectedUsers.get(i).getUser_id());
                        groupSettings.setUser_email(selectedUsers.get(i).getEmail());
                        groupSettings.setId(selectedUsers.get(i).getSelected_id());
                        tempGroupConfSetting.add(gson.fromJson(new Gson().toJson(groupSettings), GroupSettingData.class));
                    }
                    pos=0;
                    add_editUserGroupSetting(tempGroupConfSetting.get(0), tempGroupConfSetting.get(0).getId() != 0);
                }
            } else add_editUserGroupSetting(groupSettings, isEdit);
        });
    }

    @Override
    protected void initViews(View v) {
        logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = logindeatl.getString("id", "null");
        role = logindeatl.getString("role", "");
        if (selectedManger != null || selectedUser != null)
            fetchGroupSetting(0);
        else if (isMultiple) {
            if (selectedMangers != null) {

                StringBuilder total_company_name = new StringBuilder();
                StringBuilder total_company_code = new StringBuilder();

                for (int i=0;i<selectedMangers.size();i++)
                {
                    total_company_name.append(selectedMangers.get(i).getLot_manager()+" , ");
                    total_company_code.append(selectedMangers.get(i).getManager_id()+" , ");
                }

                String complete_company_name = total_company_name.toString();
                complete_company_name = complete_company_name.substring(0,complete_company_name.length() - 2);

                String complete_company_code = total_company_code.toString();
                complete_company_code = complete_company_code.substring(0,complete_company_code.length() - 2);

                binding.etCompanyName.setText(complete_company_name);
                binding.etCompanyCode.setText(complete_company_code);

//                binding.etCompanyName.setText(selectedMangers.get(selectedMangers.size() - 1).getLot_manager());
//                binding.etCompanyCode.setText(selectedMangers.get(selectedMangers.size() - 1).getManager_id());
                groupSettings = new GroupSettingData();
                fetchGroupSetting(0);
            }
            if (selectedUsers != null) {

                StringBuffer total_username = new StringBuffer();
                StringBuffer total_email = new StringBuffer();
                StringBuffer total_role = new StringBuffer();

                for (int i=0;i<selectedUsers.size();i++)
                {
                    if (!TextUtils.isEmpty(selectedUsers.get(i).getUser_name()))
                    {
                        total_username.append(selectedUsers.get(i).getUser_name()+" , ");
                    }
                    else
                    {
                        total_email.append(selectedUsers.get(i).getEmail()+" , ");
                    }
                    total_role.append(selectedUsers.get(i).getRole()+" , ");
                }

                String complete_username = null;
                if (!TextUtils.isEmpty(total_username.toString()))
                {
                    complete_username = total_username.toString();
                    complete_username = complete_username.substring(0 ,complete_username.length() - 2);
                }

                String complete_email = null;
                if (!TextUtils.isEmpty(total_email.toString()))
                {
                    complete_email = total_email.toString();
                    complete_email = complete_email.substring(0 , complete_email.length() - 2);
                }

                String complete_role = total_role.toString();
                complete_role = complete_role.substring(0,complete_role.length() - 2);

                String combine_username_email = null;

                if (complete_username != null && !complete_username.isEmpty() && !complete_username.equals("null") && complete_email != null && !complete_email.isEmpty() && !complete_email.equals("null"))
                {
                    combine_username_email = complete_username+" , "+complete_email;
                }
                else if (complete_username != null && !complete_username.isEmpty() && !complete_username.equals("null"))
                {
                    combine_username_email = complete_username;
                }
                else if (complete_email != null && !complete_email.isEmpty() && !complete_email.equals("null"))
                {
                    combine_username_email =complete_email;
                }

                binding.etCompanyName.setText(combine_username_email);
                binding.etCompanyCode.setText(complete_role);

//                if (!TextUtils.isEmpty(selectedUsers.get(selectedUsers.size() - 1).getUser_name()))
//                    binding.etCompanyName.setText(selectedUsers.get(selectedUsers.size() - 1).getUser_name());
//                else
//                    binding.etCompanyName.setText(selectedUsers.get(selectedUsers.size() - 1).getEmail());
//                binding.etCompanyCode.setText(selectedUsers.get(selectedUsers.size() - 1).getRole());
                groupSettings = new GroupSettingData();
                fetchGroupSetting(0);
            }
            binding.llGroupConfig.setVisibility(View.GONE);
        }
    }

    public void fetchGroupSetting(int position) {
        binding.rlProgressbar.setVisibility(View.VISIBLE);
        String url = "";
        if (isMultiple) {
            Log.e(TAG, "pos: " + position);
            if (selectedMangers != null)
                url = "((company_id=" + selectedMangers.get(position).getId() + ")AND(company_type_id=" + selectedMangers.get(position).getManager_type_id() + "))";
            if (selectedUsers != null)
                url = "(user_id=" + selectedUsers.get(position).getUser_id() + ")";
        } else {
            if (selectedManger != null) url = "((company_id=" + selectedManger.getId() + ")AND(company_type_id=" + selectedManger.getManager_type_id() + "))";
            if (selectedUser != null) url = "(user_id=" + selectedUser.getUser_id() + ")";
        }
        Call<GroupConfSettings> call = apiInterface.getGroupSetting(url);
        call.enqueue(new Callback<GroupConfSettings>() {
            @Override
            public void onResponse(Call<GroupConfSettings> call, Response<GroupConfSettings> response) {
                if (response.code() == 403) {
                    Toast.makeText(getActivity(), "There is something went to wrong! Please try again", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(() -> fetchGroupSetting(position), 1000);
                }
                if (response.isSuccessful()) {
                    int pos = position;
                    Log.e(TAG, " response" + response.code() + " " + response.message() + " \n " + new Gson().toJson(response.body()));
                    List<GroupSettingData> groupSetting = response.body().getResource();
                    if (groupSetting.size() > 0) {
                        groupSettings = groupSetting.get(0);
                        if (isMultiple) {
                            if (selectedMangers != null)
                                selectedMangers.get(pos).setSelected_id(groupSettings.getId());
                            else if (selectedUsers != null)
                                selectedUsers.get(pos).setSelected_id(groupSettings.getId());
                        }
                    } else if (isMultiple) {
                        if (selectedMangers != null)
                            selectedMangers.get(pos).setSelected_id(0);
                        else if (selectedUsers != null)
                            selectedUsers.get(pos).setSelected_id(0);
                    }
                    if (isMultiple) {
                        if (selectedUsers != null) {
                            if (pos == selectedUsers.size() - 1)
                                binding.rlProgressbar.setVisibility(View.GONE);
                            else {
                                pos += 1;
                                fetchGroupSetting(pos);
                            }
                        } else if (selectedMangers != null) {
                            if (pos == selectedMangers.size() - 1)
                                binding.rlProgressbar.setVisibility(View.GONE);
                            else {
                                pos += 1;
                                fetchGroupSetting(pos);
                            }
                        }
                    } else {
                        binding.rlProgressbar.setVisibility(View.GONE);
                        if (groupSetting.size() == 0) {
                            isEdit = false;
                            groupSettings = new GroupSettingData();
                            if (selectedManger != null) {
                                groupSettings.setCompany_id(selectedManger.getId());
                                groupSettings.setCompany_name(selectedManger.getLot_manager());
                                groupSettings.setCompany_type_id(selectedManger.getManager_type_id());
                                binding.etCompanyName.setText(selectedManger.getLot_manager());
                                binding.etCompanyCode.setText(selectedManger.getManager_id());
                            }
                            if (selectedUser != null) {
                                groupSettings.setUser_id(selectedUser.getUser_id());
                                groupSettings.setUser_email(selectedUser.getEmail());

                                if (!TextUtils.isEmpty(selectedUser.getUser_name()))
                                    binding.etCompanyName.setText(selectedUser.getUser_name());
                                else binding.etCompanyName.setText(selectedUser.getEmail());
                                binding.etCompanyCode.setText(selectedUser.getRole());
                            }
                            binding.llGroupConfig.setVisibility(groupSettings.getGroup_type() == 1 ? View.VISIBLE : View.GONE);
                        } else {
                            isEdit = true;
                            Log.e(TAG, " " + groupSettings.getCompany_name() + " ");
                            if (selectedManger != null) {
                                if (!TextUtils.isEmpty(groupSettings.getCompany_name()))
                                    binding.etCompanyName.setText(groupSettings.getCompany_name());
                            }
                            if (selectedUser != null) {
                                if (groupSettings.getUser_id() == selectedUser.getUser_id()) {
                                    if (!TextUtils.isEmpty(selectedUser.getUser_name()))
                                        binding.etCompanyName.setText(selectedUser.getUser_name());
                                    else binding.etCompanyName.setText(selectedUser.getEmail());
                                    binding.etCompanyCode.setText(selectedUser.getRole());
                                }
                            }
                            binding.switchGroupType.setChecked(groupSettings.getGroup_type() == 1);
                            binding.llGroupConfig.setVisibility(groupSettings.getGroup_type() == 1 ? View.VISIBLE : View.GONE);
                            addGroup = new Gson().fromJson(groupSettings.getCan_add_group_type(), new TypeToken<ArrayList<String>>() {
                            }.getType());
                            if (!addGroup.isEmpty())
                                setGroupValue(addGroup, binding.etCanAddGroupType);
                            seeGroup = new Gson().fromJson(groupSettings.getCan_see_group_type(), new TypeToken<ArrayList<String>>() {
                            }.getType());
                            if (!seeGroup.isEmpty())
                                setGroupValue(seeGroup, binding.etCanSeeGroupType);
                            addUser = new Gson().fromJson(groupSettings.getCan_add_user_type(), new TypeToken<ArrayList<String>>() {
                            }.getType());
                            if (!addUser.isEmpty())
                                setGroupValue(addUser, binding.etCanAddUserType);
                            seeUser = new Gson().fromJson(groupSettings.getCan_see_user_type(), new TypeToken<ArrayList<String>>() {
                            }.getType());
                            if (!seeUser.isEmpty())
                                setGroupValue(seeUser, binding.etCanSeeUserType);
                        }
                    }
                    isInit = false;
                }
            }

            @Override
            public void onFailure(Call<GroupConfSettings> call, Throwable t) {
                Log.e(TAG, " response" + new Gson().toJson(t));
                binding.rlProgressbar.setVisibility(View.GONE);
            }
        });
    }

    public void selectedCompanyiesUsers() {
        if (selectedMangers != null || selectedUsers != null) {
            LinearLayout viewGroup = (LinearLayout) getActivity().findViewById(R.id.popup);
            LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View layout = layoutInflater.inflate(R.layout.popup_selected_company_user_list, viewGroup, false);
            popupmanaged = new PopupWindow(getActivity());
            popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
            popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
            popupmanaged.setContentView(layout);
            popupmanaged.setBackgroundDrawable(null);
            popupmanaged.setFocusable(true);
            popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);
            RecyclerView rvMeetingId;
            TextView txt_title;
            ImageView ivClose;

            txt_title = (TextView) layout.findViewById(R.id.tvTitle);
            rvMeetingId = (RecyclerView) layout.findViewById(R.id.rvMeetingId);
            ivClose = (ImageView) layout.findViewById(R.id.ivClose);

            rvMeetingId.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            if (selectedMangers != null) {
                txt_title.setText("Selected Companies");
                partnerAdapter = new PartnerAdapter();
                rvMeetingId.setAdapter(partnerAdapter);
            } else if (selectedUsers != null) {
                txt_title.setText("Selected Users");
                userAdapter = new UserAdapter();
                rvMeetingId.setAdapter(userAdapter);
            }
            ivClose.setOnClickListener(v1 -> popupmanaged.dismiss());
        } else {
            Toast.makeText(myApp, "You don't have any previous meeting.", Toast.LENGTH_SHORT).show();
        }
    }

    public void setGroupValue(ArrayList<String> typeList, TextView textView) {
        for (int i = 0; i < typeList.size(); i++) {
            if (typeList.get(i).equals("" + GROUP_TYPE_ORGANIZATIONAL))
                textView.setText(textView.getText() + " Organizational");
            if (typeList.get(i).equals("" + GROUP_TYPE_PUBLIC))
                textView.setText(textView.getText() + " Public");
            if (typeList.get(i).equals("" + GROUP_TYPE_MYCONTACT))
                textView.setText(textView.getText() + " My Contact");
            if (i != (typeList.size() - 1))
                textView.setText(textView.getText() + ",");
        }
    }

    public void add_editUserGroupSetting(GroupSettingData groupSettings, boolean isUserEdit) {
        binding.rlProgressbar.setVisibility(View.VISIBLE);
        JsonObject json = new JsonObject();
        if (isUserEdit) json.addProperty("id", groupSettings.getId());
        else {
            json.addProperty("datetime", "");
            json.addProperty("company_id", groupSettings.getCompany_id());
            json.addProperty("company_name", groupSettings.getCompany_name());
            json.addProperty("company_type_id", groupSettings.getCompany_type_id());
            json.addProperty("user_id", groupSettings.getUser_id());
            json.addProperty("user_email", groupSettings.getUser_email());
        }
        json.addProperty("group_type", groupSettings.getGroup_type());
        json.addProperty("can_add_group_type", groupSettings.getCan_add_group_type());
        json.addProperty("can_see_group_type", groupSettings.getCan_see_group_type());
        json.addProperty("can_add_user_type", groupSettings.getCan_add_user_type());
        json.addProperty("can_see_user_type", groupSettings.getCan_see_user_type());
        Call<Object> call = null;
        if (isUserEdit) call = apiInterface.updateGroupSettings(json);
        else call = apiInterface.addGroupSettings(json);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                int gid = 0;
                Log.e(TAG, "editUserGroup: Code: " + response.code() + new Gson().toJson(response.body()));
                if (response.code() == 403) {
                    Toast.makeText(getActivity(), "There is something went to wrong! Please try again", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(() -> {
                        add_editUserGroupSetting(groupSettings, isUserEdit);
                    }, 1000);
                }
                if (response.isSuccessful()) {
                    try {
                        JSONObject json = new JSONObject(new Gson().toJson(response.body()));
                        JSONArray array = json.getJSONArray("resource");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject c = array.getJSONObject(i);
                            gid = c.getInt("id");
                            Log.e(TAG, " MsgId: " + gid);
//                            getActivity().onBackPressed();
                        }
                        if (isMultiple) {
                            if (tempGroupConfSetting.size() > 0) {
                                if (selectedMangers != null)
                                    selectedMangers.get(pos).setSelected_id(gid);
                                if (selectedUsers != null)
                                    selectedUsers.get(pos).setSelected_id(gid);
                                pos += 1;
                                tempGroupConfSetting.remove(0);
                                if (tempGroupConfSetting.size() == 0) {
                                    binding.rlProgressbar.setVisibility(View.GONE);
                                    Toast.makeText(myApp, "Configuration Save", Toast.LENGTH_LONG).show();
                                } else {
                                    Log.e(TAG, "GroupCongif: " + tempGroupConfSetting.get(0).getId());
                                    add_editUserGroupSetting(tempGroupConfSetting.get(0), tempGroupConfSetting.get(0).getId() != 0);
                                }
                            }
                        } else {
                            if (!isEdit) {
                                groupSettings.setId(gid);
                                isEdit = true;
                            }
                            binding.rlProgressbar.setVisibility(View.GONE);
                            Toast.makeText(myApp, "Configuration Save", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.e(TAG, "ADDGroupUser FailureMsg");
                Log.e(TAG, "error  response" + new Gson().toJson(t));
                call.cancel();
            }
        });
    }

    private void showAddEditGroupTypePupup(TextView editText, ArrayList<String> typeList) {
        PopupGroupTypeBinding popupBinding;
        PopupWindow popupmanaged;
        LinearLayout viewGroup = (LinearLayout) getActivity().findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        popupBinding = DataBindingUtil.inflate(layoutInflater, R.layout.popup_group_type, viewGroup, false);
        View layout = popupBinding.getRoot();

        popupmanaged = new PopupWindow(getActivity());
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);

        popupBinding.tvTitle.setText(isUser ? "Select Group type " : "Select User type ");
        Typeface typeface = popupBinding.tvOrg.getTypeface();
        for (String type : typeList) {
            if (type.equals("" + GROUP_TYPE_ORGANIZATIONAL)) {
                isOrg = true;
                popupBinding.tvOrg.setTypeface(typeface, Typeface.BOLD);
                popupBinding.llOrg.setBackgroundResource(R.drawable.highlight);
                popupBinding.tvOrg.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_700));
            } else if (type.equals("" + GROUP_TYPE_PUBLIC)) {
                isPublic = true;
                popupBinding.llPublic.setBackgroundResource(R.drawable.highlight);
                popupBinding.tvPublic.setTypeface(typeface, Typeface.BOLD);
                popupBinding.tvPublic.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_700));
            } else if (type.equals("" + GROUP_TYPE_MYCONTACT)) {
                isMyContact = true;
                popupBinding.llMyContact.setBackgroundResource(R.drawable.highlight);
                popupBinding.tvMyContact.setTypeface(typeface, Typeface.BOLD);
                popupBinding.tvMyContact.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_700));
            }
        }
        popupBinding.llOrg.setOnClickListener(v -> {
            isOrg = !isOrg;
            if (isOrg) typeList.add("" + GROUP_TYPE_ORGANIZATIONAL);
            else typeList.remove("" + GROUP_TYPE_ORGANIZATIONAL);
            popupBinding.tvOrg.setTypeface(typeface, isOrg ? Typeface.BOLD : Typeface.NORMAL);
            popupBinding.tvOrg.setTextColor(ContextCompat.getColor(getContext(), isOrg ? R.color.grey_700 : R.color.itemType));
            popupBinding.llOrg.setBackgroundResource(isOrg ? R.drawable.highlight : 0);
        });

        popupBinding.llPublic.setOnClickListener(v -> {
            isPublic = !isPublic;
            if (isPublic) typeList.add("" + GROUP_TYPE_PUBLIC);
            else typeList.remove("" + GROUP_TYPE_PUBLIC);
            popupBinding.tvPublic.setTypeface(typeface, isPublic ? Typeface.BOLD : Typeface.NORMAL);
            popupBinding.tvPublic.setTextColor(ContextCompat.getColor(getContext(), isPublic ? R.color.grey_700 : R.color.itemType));
            popupBinding.llPublic.setBackgroundResource(isPublic ? R.drawable.highlight : 0);
        });

        popupBinding.llMyContact.setOnClickListener(v -> {
            isMyContact = !isMyContact;
            if (isMyContact) typeList.add("" + GROUP_TYPE_MYCONTACT);
            else typeList.remove("" + GROUP_TYPE_MYCONTACT);
            popupBinding.tvMyContact.setTypeface(typeface, isMyContact ? Typeface.BOLD : Typeface.NORMAL);
            popupBinding.tvMyContact.setTextColor(ContextCompat.getColor(getContext(), isMyContact ? R.color.grey_700 : R.color.itemType));
            popupBinding.llMyContact.setBackgroundResource(isMyContact ? R.drawable.highlight : 0);
        });

        popupBinding.tvBtnOk.setOnClickListener(v -> {
            popupmanaged.dismiss();
            editText.setText("");
            for (String type : typeList) {
                if (!TextUtils.isEmpty(editText.getText().toString()))
                    editText.setText(editText.getText().toString() + ",");
                if (type.equals("" + GROUP_TYPE_ORGANIZATIONAL))
                    editText.setText(editText.getText().toString() + " Organizational");
                else if (type.equals("" + GROUP_TYPE_PUBLIC))
                    editText.setText(editText.getText().toString() + " Public");
                else if (type.equals("" + GROUP_TYPE_MYCONTACT))
                    editText.setText(editText.getText().toString() + " My Contact");
            }
        });
        popupBinding.ivClose.setOnClickListener(v -> popupmanaged.dismiss());
    }

    private class PartnerAdapter extends RecyclerView.Adapter<PartnerAdapter.PartnerHolder> {

        @NonNull
        @Override
        public PartnerHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new PartnerHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_partner_new_manager,
                    viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull PartnerHolder holder, int i) {
            Manager manager = selectedMangers.get(i);
            if (manager != null) {
                holder.ivSelectCompany.setVisibility(View.GONE);
                holder.tvName.setText(manager.getLot_manager());
                holder.tvAddress.setText(manager.getAddress());
                Glide.with(getActivity()).load(manager.getTownship_logo()).into(holder.ivPartner);
            }
        }

        @Override
        public int getItemCount() {
            return selectedMangers.size();
        }

        class PartnerHolder extends RecyclerView.ViewHolder {
            CircleImageView ivPartner;
            ImageView ivSelectCompany;
            TextView tvName, tvAddress;

            PartnerHolder(@NonNull View itemView) {
                super(itemView);
                ivSelectCompany = itemView.findViewById(R.id.ivSelectedCompany);
                ivPartner = itemView.findViewById(R.id.ivPartner);
                tvName = itemView.findViewById(R.id.tvName);
                tvAddress = itemView.findViewById(R.id.tvAddress);
            }
        }
    }

    public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyUserViewHolder> {
        @NonNull
        @Override
        public MyUserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyUserViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_users, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyUserViewHolder holder, int position) {
            holder.tvUserId.setText("" + selectedUsers.get(position).getUser_id());

            if (!TextUtils.isEmpty(selectedUsers.get(position).getFname()) || !TextUtils.isEmpty(selectedUsers.get(position).getLname()))
                holder.tvUserName.setText(selectedUsers.get(position).getFname() + " " + selectedUsers.get(position).getLname());
            else holder.tvUserName.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(selectedUsers.get(position).getMobile()))
                holder.tvUserCode.setText(selectedUsers.get(position).getMobile());
            else holder.tvUserCode.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(selectedUsers.get(position).getEmail()))
                holder.tvUserEmail.setText(selectedUsers.get(position).getEmail());
            else holder.tvUserEmail.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(selectedUsers.get(position).getRole()))
                holder.tvUserRole.setText(selectedUsers.get(position).getRole());
            else holder.tvUserRole.setVisibility(View.GONE);
        }

        @Override
        public int getItemCount() {
            return selectedUsers.size();
        }

        public class MyUserViewHolder extends RecyclerView.ViewHolder {
            TextView tvUserId, tvUserName, tvUserCode, tvUserEmail, tvUserRole;

            public MyUserViewHolder(@NonNull View itemView) {
                super(itemView);
                tvUserId = itemView.findViewById(R.id.tvUserId);
                tvUserName = itemView.findViewById(R.id.tvUserName);
                tvUserCode = itemView.findViewById(R.id.tvUserContact);
                tvUserEmail = itemView.findViewById(R.id.tvUserEmail);
                tvUserRole = itemView.findViewById(R.id.tvUserRole);
            }
        }
    }
}