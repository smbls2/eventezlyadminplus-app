package com.softmeasures.eventezlyadminplus.frament.event_reg;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragEventsMapBinding;
import com.softmeasures.eventezlyadminplus.frament.Other_City;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class EventsMapFragment extends BaseFragment implements Other_City.Other_city_listenere {

    public FragEventsMapBinding binding;

    private ImageView ivBtnFindNearBy, ivBtnOtherCity;
    private GoogleMap googleMap;
    private RelativeLayout rlProgressbar;
    private ArrayList<item> parkingSpots = new ArrayList<>();
    private SupportMapFragment mapFragment;
    private double latitude = 0.0, longitude = 0.0;
    private Handler mHandler = null;
    private Runnable mAnimation;
    private double centerlat = 0.0, centerlang = 0.0;
    private boolean mapisloadcallback = false, managedmapisloaded = false, managedpinisclick = false;
    private boolean nearbyclick = false, searchotherlocation = false;
    private ArrayList<EventDefinition> nearbylist = new ArrayList<>();
    private ArrayList<EventDefinition> findnearbyarray = new ArrayList<>();
    private adpaternearby adpater;
    private int c = 0;

    private RelativeLayout rl_nearbylist;
    private ListView list_nearby;
    private ImageView img_close_nearbylist;

    private RelativeLayout rl_searchinfo;
    private ArrayList<item> parking_rules = new ArrayList<>();
    String lat = "null", lang = "null";
    private String parkingType = "";

    private LinearLayout llBottomOptions;
    private boolean isAllOptions = false;
    private ArrayList<EventDefinition> eventDefinitions = new ArrayList<>();
    private ArrayList<EventDefinition> nearByEvents = new ArrayList<>();
    private ArrayList<EventDefinition> nearByEventsWithDistance = new ArrayList<>();
    private ArrayList<EventDefinition> eventDefinitionsSearch = new ArrayList<>();

    private ArrayList<String> managerTypes = new ArrayList<>();
    private int selectedTypeIndex = 0;
    private String selectedEventCategory = "current";
    private EventAdapter eventAdapter;
    private boolean isNearByVisible = false;
    ConnectionDetector cd;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_events_map, container, false);
        if (getArguments() != null) {
            parkingType = getArguments().getString("parkingType", "Township");
            if (parkingType.equals("All")) {
                isAllOptions = true;
                parkingType = "Township";
            }
            Log.e("#DEBUG", "   EventsMapFragment:  parkingType:  " + parkingType);
        }

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
        getCurrentLocation();
        loadMap();

        cd = new ConnectionDetector(getActivity());
        if (cd.isConnectingToInternet()) {
//        new getparkingrules().execute();
            new fetchEvents().execute();
            //        fetchEvents();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();
        }
    }

    public void searchClick() {
        binding.llEventSearch.setVisibility(View.VISIBLE);

        binding.etSearchView.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) {
                if (!TextUtils.isEmpty(binding.etSearchView.getText().toString())) {
                    searchEvent(binding.etSearchView.getText().toString());
                    return true;
                }
            }
            return false;
        });
    }

    private void searchEvent(String string) {
        binding.etSearchView.setText("");
        eventDefinitionsSearch.clear();
        for (int i1 = 0; i1 < eventDefinitions.size(); i1++) {
            if (eventDefinitions.get(i1).getEvent_name().toLowerCase().contains(string.toLowerCase())
                    || (eventDefinitions.get(i1).getCompany_name() != null
                    && !TextUtils.isEmpty(eventDefinitions.get(i1).getCompany_name())
                    && !eventDefinitions.get(i1).getCompany_name().equals("null")
                    && eventDefinitions.get(i1).getCompany_name().toLowerCase().contains(string.toLowerCase()))) {

                try {
                    if (eventDefinitions.get(i1).getLocation_lat_lng() != null
                            && !TextUtils.isEmpty(eventDefinitions.get(i1).getLocation_lat_lng())
                            && !eventDefinitions.get(i1).getLocation_lat_lng().equals("null")) {
                        JSONArray jsonArrayLatLng = new JSONArray(eventDefinitions.get(i1).getLocation_lat_lng());
                        for (int i = 0; i < jsonArrayLatLng.length(); i++) {
                            JSONArray jsonArray2 = jsonArrayLatLng.getJSONArray(i);
                            for (int j = 0; j < jsonArray2.length(); j++) {
                                String[] latLng = jsonArray2.getString(j).split(",");
                                if (latLng.length == 2) {
                                    eventDefinitions.get(i1).setLat(latLng[0]);
                                    eventDefinitions.get(i1).setLng(latLng[1]);

                                    JSONArray jsonArray11 = new JSONArray(eventDefinitions.get(i1).getEvent_address());
                                    JSONArray jsonArray22 = jsonArray11.getJSONArray(i);
                                    eventDefinitions.get(i1).setDisplay_address(jsonArray22.getString(0));
                                    eventDefinitionsSearch.add(eventDefinitions.get(i1));

                                }
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        eventAdapter.notifyDataSetChanged();
    }

    private class fetchEvents extends AsyncTask<String, String, String> {
        JSONObject json;
        JSONArray json1;
        String eventDefUrl = "_table/event_definitions";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rlProgressbar.setVisibility(View.VISIBLE);
            eventDefinitions.clear();
            managerTypes.clear();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + eventDefUrl;
            Log.e("#DEBUG", "      fetchEvents:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");

                    for (int j = 0; j < jsonArray.length(); j++) {
                        EventDefinition eventDefinition = new EventDefinition();
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            eventDefinition.setId(object.getInt("id"));
                            eventDefinition.setEvent_id(object.getInt("id"));
                        }
                        if (object.has("date_time")
                                && !TextUtils.isEmpty(object.getString("date_time"))
                                && !object.getString("date_time").equals("null")) {
                            eventDefinition.setDate_time(object.getString("date_time"));
                        }
                        if (object.has("manager_type")
                                && !TextUtils.isEmpty(object.getString("manager_type"))
                                && !object.getString("manager_type").equals("null")) {
                            eventDefinition.setManager_type(object.getString("manager_type"));
                            if (!managerTypes.contains(object.getString("manager_type"))) {
                                managerTypes.add(object.getString("manager_type"));
                            }
                        }
                        if (object.has("manager_type_id")
                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
                                && !object.getString("manager_type_id").equals("null")) {
                            eventDefinition.setManager_type_id(object.getInt("manager_type_id"));
                        }
                        if (object.has("twp_id")
                                && !TextUtils.isEmpty(object.getString("twp_id"))
                                && !object.getString("twp_id").equals("null")) {
                            eventDefinition.setTwp_id(object.getInt("twp_id"));
                        }
                        if (object.has("township_code")
                                && !TextUtils.isEmpty(object.getString("township_code"))
                                && !object.getString("township_code").equals("null")) {
                            eventDefinition.setTownship_code(object.getString("township_code"));
                        }
                        if (object.has("township_name")
                                && !TextUtils.isEmpty(object.getString("township_name"))
                                && !object.getString("township_name").equals("null")) {
                            eventDefinition.setTownship_name(object.getString("township_name"));
                        }
                        if (object.has("company_id")
                                && !TextUtils.isEmpty(object.getString("company_id"))
                                && !object.getString("company_id").equals("null")) {
                            eventDefinition.setCompany_id(object.getInt("company_id"));
                        }
                        if (object.has("company_code")
                                && !TextUtils.isEmpty(object.getString("company_code"))
                                && !object.getString("company_code").equals("null")) {
                            eventDefinition.setCompany_code(object.getString("company_code"));
                        }
                        if (object.has("company_name")
                                && !TextUtils.isEmpty(object.getString("company_name"))
                                && !object.getString("company_name").equals("null")) {
                            eventDefinition.setCompany_name(object.getString("company_name"));
                        }
                        if (object.has("event_type")
                                && !TextUtils.isEmpty(object.getString("event_type"))
                                && !object.getString("event_type").equals("null")) {
                            eventDefinition.setEvent_type(object.getString("event_type"));
                        }
                        if (object.has("event_name")
                                && !TextUtils.isEmpty(object.getString("event_name"))
                                && !object.getString("event_name").equals("null")) {
                            eventDefinition.setEvent_name(object.getString("event_name"));
                        }
                        if (object.has("event_short_description")
                                && !TextUtils.isEmpty(object.getString("event_short_description"))
                                && !object.getString("event_short_description").equals("null")) {
                            eventDefinition.setEvent_short_description(object.getString("event_short_description"));
                        }
                        if (object.has("event_long_description")
                                && !TextUtils.isEmpty(object.getString("event_long_description"))
                                && !object.getString("event_long_description").equals("null")) {
                            eventDefinition.setEvent_long_description(object.getString("event_long_description"));
                        }
                        if (object.has("event_link_on_web")
                                && !TextUtils.isEmpty(object.getString("event_link_on_web"))
                                && !object.getString("event_link_on_web").equals("null")) {
                            eventDefinition.setEvent_link_on_web(object.getString("event_link_on_web"));
                        }
                        if (object.has("event_link_twitter")
                                && !TextUtils.isEmpty(object.getString("event_link_twitter"))
                                && !object.getString("event_link_twitter").equals("null")) {
                            eventDefinition.setEvent_link_twitter(object.getString("event_link_twitter"));
                        }
                        if (object.has("event_link_whatsapp")
                                && !TextUtils.isEmpty(object.getString("event_link_whatsapp"))
                                && !object.getString("event_link_whatsapp").equals("null")) {
                            eventDefinition.setEvent_link_whatsapp(object.getString("event_link_whatsapp"));
                        }
                        if (object.has("event_link_other_media")
                                && !TextUtils.isEmpty(object.getString("event_link_other_media"))
                                && !object.getString("event_link_other_media").equals("null")) {
                            eventDefinition.setEvent_link_other_media(object.getString("event_link_other_media"));
                        }
                        if (object.has("company_logo")
                                && !TextUtils.isEmpty(object.getString("company_logo"))
                                && !object.getString("company_logo").equals("null")) {
                            eventDefinition.setCompany_logo(object.getString("company_logo"));
                        }
                        if (object.has("event_logo")
                                && !TextUtils.isEmpty(object.getString("event_logo"))
                                && !object.getString("event_logo").equals("null")) {
                            eventDefinition.setEvent_logo(object.getString("event_logo"));
                        }
                        if (object.has("event_image1")
                                && !TextUtils.isEmpty(object.getString("event_image1"))
                                && !object.getString("event_image1").equals("null")) {
                            eventDefinition.setEvent_image1(object.getString("event_image1"));
                        }
                        if (object.has("event_image2")
                                && !TextUtils.isEmpty(object.getString("event_image2"))
                                && !object.getString("event_image2").equals("null")) {
                            eventDefinition.setEvent_image2(object.getString("event_image2"));
                        }
                        if (object.has("event_blob_image")
                                && !TextUtils.isEmpty(object.getString("event_blob_image"))
                                && !object.getString("event_blob_image").equals("null")) {
                            eventDefinition.setEvent_blob_image(object.getString("event_blob_image"));
                        }
                        if (object.has("event_address")
                                && !TextUtils.isEmpty(object.getString("event_address"))
                                && !object.getString("event_address").equals("null")) {
                            eventDefinition.setEvent_address(object.getString("event_address"));
                        }
                        if (object.has("covered_locations")
                                && !TextUtils.isEmpty(object.getString("covered_locations"))
                                && !object.getString("covered_locations").equals("null")) {
                            eventDefinition.setCovered_locations(object.getString("covered_locations"));
                        }
                        if (object.has("requirements")
                                && !TextUtils.isEmpty(object.getString("requirements"))
                                && !object.getString("requirements").equals("null")) {
                            eventDefinition.setRequirements(object.getString("requirements"));
                        }
                        if (object.has("appl_req_download")
                                && !TextUtils.isEmpty(object.getString("appl_req_download"))
                                && !object.getString("appl_req_download").equals("null")) {
                            eventDefinition.setAppl_req_download(object.getString("appl_req_download"));
                        }
                        if (object.has("cost")
                                && !TextUtils.isEmpty(object.getString("cost"))
                                && !object.getString("cost").equals("null")) {
                            eventDefinition.setCost(object.getString("cost"));
                        }
                        if (object.has("year")
                                && !TextUtils.isEmpty(object.getString("year"))
                                && !object.getString("year").equals("null")) {
                            eventDefinition.setYear(object.getString("year"));
                        }
                        if (object.has("location_address")
                                && !TextUtils.isEmpty(object.getString("location_address"))
                                && !object.getString("location_address").equals("null")) {
                            eventDefinition.setLocation_address(object.getString("location_address"));
                        }
                        if (object.has("scheme_type")
                                && !TextUtils.isEmpty(object.getString("scheme_type"))
                                && !object.getString("scheme_type").equals("null")) {
                            eventDefinition.setScheme_type(object.getString("scheme_type"));
                        }
                        if (object.has("event_prefix")
                                && !TextUtils.isEmpty(object.getString("event_prefix"))
                                && !object.getString("event_prefix").equals("null")) {
                            eventDefinition.setEvent_prefix(object.getString("event_prefix"));
                        }
                        if (object.has("event_nextnum")
                                && !TextUtils.isEmpty(object.getString("event_nextnum"))
                                && !object.getString("event_nextnum").equals("null")) {
                            eventDefinition.setEvent_nextnum(object.getString("event_nextnum"));
                        }
                        if (object.has("event_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_begins_date_time"))
                                && !object.getString("event_begins_date_time").equals("null")) {
                            eventDefinition.setEvent_begins_date_time(object.getString("event_begins_date_time"));
                        }
                        if (object.has("event_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_ends_date_time"))
                                && !object.getString("event_ends_date_time").equals("null")) {
                            eventDefinition.setEvent_ends_date_time(object.getString("event_ends_date_time"));
                        }
                        if (object.has("event_parking_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_parking_begins_date_time"))
                                && !object.getString("event_parking_begins_date_time").equals("null")) {
                            eventDefinition.setEvent_parking_begins_date_time(object.getString("event_parking_begins_date_time"));
                        }
                        if (object.has("event_parking_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_parking_ends_date_time"))
                                && !object.getString("event_parking_ends_date_time").equals("null")) {
                            eventDefinition.setEvent_parking_ends_date_time(object.getString("event_parking_ends_date_time"));
                        }

                        if (object.has("event_multi_dates")
                                && !TextUtils.isEmpty(object.getString("event_multi_dates"))
                                && !object.getString("event_multi_dates").equals("null")) {
                            eventDefinition.setEvent_multi_dates(object.getString("event_multi_dates"));
                            String currentDate = "";
                            currentDate = DateTimeUtils.getCurrentDateYYYYMMDD();
                            long currentDateMs = DateTimeUtils.getMilliseconds(currentDate);
//                            Log.e("#DEBUG", "   fetchEvent: currentDate:  " + currentDate);
//                            Log.e("#DEBUG", "   fetchEvent: currentDateMS:  " + currentDateMs);
                            ArrayList<String> dates = new ArrayList<>();
                            try {
                                JSONArray jsonArray2 = new JSONArray(eventDefinition.getEvent_multi_dates());
                                for (int i = 0; i < jsonArray2.length(); i++) {
                                    JSONArray jsonArray1 = jsonArray2.getJSONArray(i);
                                    if (jsonArray1 != null && jsonArray1.length() > 0) {
                                        dates.add(jsonArray1.get(0).toString());
//                                        Log.e("#DEBUG", "   fetchEvent:  eventDate:  " + jsonArray1.get(0).toString());
//                                        Log.e("#DEBUG", "  fetchEvent:  eventDateMS: " + DateTimeUtils.getMilliseconds(jsonArray1.get(0).toString()));
                                        if (currentDateMs == DateTimeUtils.getMilliseconds(jsonArray1.get(0).toString())) {
                                            eventDefinition.setEventCategory("current");
                                        } else if (currentDateMs > DateTimeUtils.getMilliseconds(jsonArray1.get(0).toString())) {
                                            eventDefinition.setEventCategory("past");
                                        } else if (currentDateMs < DateTimeUtils.getMilliseconds(jsonArray1.get(0).toString())) {
                                            eventDefinition.setEventCategory("upcoming");
                                        } else {
                                            eventDefinition.setEventCategory("");
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        if (object.has("event_parking_timings")
                                && !TextUtils.isEmpty(object.getString("event_parking_timings"))
                                && !object.getString("event_parking_timings").equals("null")) {
                            eventDefinition.setEvent_parking_timings(object.getString("event_parking_timings"));
                        }

                        if (object.has("event_event_timings")
                                && !TextUtils.isEmpty(object.getString("event_event_timings"))
                                && !object.getString("event_event_timings").equals("null")) {
                            eventDefinition.setEvent_event_timings(object.getString("event_event_timings"));
                        }

                        if (object.has("expires_by")
                                && !TextUtils.isEmpty(object.getString("expires_by"))
                                && !object.getString("expires_by").equals("null")) {
                            eventDefinition.setExpires_by(object.getString("expires_by"));
                        }
                        if (object.has("regn_reqd")
                                && !TextUtils.isEmpty(object.getString("regn_reqd"))
                                && !object.getString("regn_reqd").equals("null")) {
                            eventDefinition.setRegn_reqd(object.getBoolean("regn_reqd"));
                        }
                        if (object.has("regn_reqd_for_parking")
                                && !TextUtils.isEmpty(object.getString("regn_reqd_for_parking"))
                                && !object.getString("regn_reqd_for_parking").equals("null")) {
                            eventDefinition.setRegn_reqd_for_parking(object.getBoolean("regn_reqd_for_parking"));
                        }
                        if (object.has("renewable")
                                && !TextUtils.isEmpty(object.getString("renewable"))
                                && !object.getString("renewable").equals("null")) {
                            eventDefinition.setRenewable(object.getBoolean("renewable"));
                        }
                        if (object.has("active")
                                && !TextUtils.isEmpty(object.getString("active"))
                                && !object.getString("active").equals("null")) {
                            eventDefinition.setActive(object.getBoolean("active"));
                        }

                        if (object.has("event_multi_sessions")
                                && !TextUtils.isEmpty(object.getString("event_multi_sessions"))
                                && !object.getString("event_multi_sessions").equals("null")) {
                            eventDefinition.setEvent_multi_sessions(object.getBoolean("event_multi_sessions"));
                        }

                        if (object.has("event_indiv_session_regn_allowed")
                                && !TextUtils.isEmpty(object.getString("event_indiv_session_regn_allowed"))
                                && !object.getString("event_indiv_session_regn_allowed").equals("null")) {
                            eventDefinition.setEvent_indiv_session_regn_allowed(object.getBoolean("event_indiv_session_regn_allowed"));
                        }

                        if (object.has("event_indiv_session_regn_required")
                                && !TextUtils.isEmpty(object.getString("event_indiv_session_regn_required"))
                                && !object.getString("event_indiv_session_regn_required").equals("null")) {
                            eventDefinition.setEvent_indiv_session_regn_required(object.getBoolean("event_indiv_session_regn_required"));
                        }

                        if (object.has("event_full_regn_required")
                                && !TextUtils.isEmpty(object.getString("event_full_regn_required"))
                                && !object.getString("event_full_regn_required").equals("null")) {
                            eventDefinition.setEvent_full_regn_required(object.getBoolean("event_full_regn_required"));
                        }

                        if (object.has("event_full_regn_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_regn_fee"))
                                && !object.getString("event_full_regn_fee").equals("null")) {
                            eventDefinition.setEvent_full_regn_fee(object.getString("event_full_regn_fee"));
                        }

                        if (object.has("event_parking_fee")
                                && !TextUtils.isEmpty(object.getString("event_parking_fee"))
                                && !object.getString("event_parking_fee").equals("null")) {
                            eventDefinition.setEvent_parking_fee(object.getString("event_parking_fee"));
                        }

                        if (object.has("location_lat_lng")
                                && !TextUtils.isEmpty(object.getString("location_lat_lng"))
                                && !object.getString("location_lat_lng").equals("null")) {
                            eventDefinition.setLocation_lat_lng(object.getString("location_lat_lng"));
                        }

                        if (object.has("regn_user_info_reqd")
                                && !TextUtils.isEmpty(object.getString("regn_user_info_reqd"))
                                && !object.getString("regn_user_info_reqd").equals("null")) {
                            eventDefinition.setRegn_user_info_reqd(object.getBoolean("regn_user_info_reqd"));
                        }

                        if (object.has("regn_addl_user_info_reqd")
                                && !TextUtils.isEmpty(object.getString("regn_addl_user_info_reqd"))
                                && !object.getString("regn_addl_user_info_reqd").equals("null")) {
                            eventDefinition.setRegn_addl_user_info_reqd(object.getBoolean("regn_addl_user_info_reqd"));
                        }

                        if (object.has("event_full_youth_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_youth_fee"))
                                && !object.getString("event_full_youth_fee").equals("null")) {
                            eventDefinition.setEvent_full_youth_fee(object.getString("event_full_youth_fee"));
                        }

                        if (object.has("event_full_child_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_child_fee"))
                                && !object.getString("event_full_child_fee").equals("null")) {
                            eventDefinition.setEvent_full_child_fee(object.getString("event_full_child_fee"));
                        }

                        if (object.has("event_full_student_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_student_fee"))
                                && !object.getString("event_full_student_fee").equals("null")) {
                            eventDefinition.setEvent_full_student_fee(object.getString("event_full_student_fee"));
                        }

                        if (object.has("event_full_minister_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_minister_fee"))
                                && !object.getString("event_full_minister_fee").equals("null")) {
                            eventDefinition.setEvent_full_minister_fee(object.getString("event_full_minister_fee"));
                        }

                        if (object.has("event_full_clergy_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_clergy_fee"))
                                && !object.getString("event_full_clergy_fee").equals("null")) {
                            eventDefinition.setEvent_full_clergy_fee(object.getString("event_full_clergy_fee"));
                        }

                        if (object.has("event_full_promo_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_promo_fee"))
                                && !object.getString("event_full_promo_fee").equals("null")) {
                            eventDefinition.setEvent_full_promo_fee(object.getString("event_full_promo_fee"));
                        }

                        if (object.has("event_full_senior_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_senior_fee"))
                                && !object.getString("event_full_senior_fee").equals("null")) {
                            eventDefinition.setEvent_full_senior_fee(object.getString("event_full_senior_fee"));
                        }

                        if (object.has("event_full_staff_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_staff_fee"))
                                && !object.getString("event_full_staff_fee").equals("null")) {
                            eventDefinition.setEvent_full_staff_fee(object.getString("event_full_staff_fee"));
                        }

                        if (object.has("free_event")
                                && !TextUtils.isEmpty(object.getString("free_event"))
                                && !object.getString("free_event").equals("null")) {
                            eventDefinition.setFree_event(object.getBoolean("free_event"));
                        }

                        if (object.has("free_event_parking")
                                && !TextUtils.isEmpty(object.getString("free_event_parking"))
                                && !object.getString("free_event_parking").equals("null")) {
                            eventDefinition.setFree_event_parking(object.getBoolean("free_event_parking"));
                        }
                        eventDefinitions.add(eventDefinition);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                rlProgressbar.setVisibility(View.GONE);
                if (eventDefinitions.size() != 0) {
                    Log.e("#DEBUG", "   managerTypes:  " + new Gson().toJson(managerTypes));
                    showParkingSpotsOnMap();
                } else {

                }
            }
        }
    }

    private void loadMap() {
        if (getActivity() != null) {
            try {
                mapFragment = (SupportMapFragment) getChildFragmentManager()
                        .findFragmentById(R.id.fragmentMap);
                if (mapFragment != null) {
                    mapFragment.getMapAsync(map -> {
                        googleMap = map;
                        showParkingSpotsOnMap();
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void OnItemFindClick(String lat, String lang) {
        latitude = Double.parseDouble(lat);
        longitude = Double.parseDouble(lang);
        Log.i("EventEzlyApp", "2 Lat: " + latitude + " Lang: " + longitude);
        centerlat = 0.0;
        centerlang = 0.0;
        new fetchReservationParkingSpots().execute();
//        fetchReservationParkingSpots();
    }

    public class getparkingrules extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String parkingurl = "_table/township_parking_rules";
        ArrayList<item> temp_array = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            if (parkingType.equals("Township")) {
                parkingurl = "_table/township_parking_rules";
            } else if (parkingType.equals("Commercial")) {
                parkingurl = "_table/google_parking_rules";
            } else if (parkingType.equals("Other")) {
                parkingurl = "_table/other_parking_rules";
            }
            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            temp_array.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setId(c.getString("id"));
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setTime_rule(c.getString("time_rule"));
                        item1.setDay_rule(c.getString("day_type"));
                        item1.setMax_time(c.getString("max_duration"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setActive(c.getString("active"));
                        item1.setPricing(c.getString("pricing"));
                        item1.setPricing_duration(c.getString("pricing_duration"));
                        item1.setStart_time(c.getString("start_time"));
                        item1.setEnd_time(c.getString("end_time"));
                        item1.setMax_hours(c.getString("duration_unit"));
                        item1.setStart_hour(c.getString("start_hour"));
                        item1.setEnd_hour(c.getString("end_hour"));
                        item1.setLoationcode1(c.getString("location_code"));
                        item1.setMarker_type(c.getString("marker_type"));
                        item1.setDuration_unit(c.getString("duration_unit"));
                        item1.setMax_duration(c.getString("max_duration"));
                        item1.setLocation_name(c.getString("location_name"));

                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item1.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }

                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item1.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }

                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item1.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }

                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item1.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }

                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item1.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }

                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item1.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item1.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }

                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item1.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }

                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item1.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }

                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item1.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }

                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item1.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }

                        if (c.has("Reservation_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_Fee"))
                                && !c.getString("Reservation_PostPayment_Fee").equals("null")) {
                            item1.setReservation_PostPayment_Fee(c.getString("Reservation_PostPayment_Fee"));
                        }

                        if (c.has("ParkNow_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_Fee"))
                                && !c.getString("ParkNow_PostPayment_Fee").equals("null")) {
                            item1.setParkNow_PostPayment_Fee(c.getString("ParkNow_PostPayment_Fee"));
                        }

                        if (c.has("number_of_reservable_spots")
                                && !TextUtils.isEmpty(c.getString("number_of_reservable_spots"))
                                && !c.getString("number_of_reservable_spots").equals("null")) {
                            item1.setNumber_of_reservable_spots(c.getString("number_of_reservable_spots"));
                        }
                        temp_array.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs;
            if (getActivity() != null) {
                rlProgressbar.setVisibility(View.GONE);
                if (json != null) {
                    if (temp_array.size() > 0) {
                        parking_rules.clear();
                        parking_rules.addAll(temp_array);
                        temp_array.clear();
                        new fetchReservationParkingSpots().execute();
//                        fetchReservationParkingSpots();
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    private class fetchReservationParkingSpots extends AsyncTask<String, String, String> {
        JSONObject jsonObject;
        JSONArray jsonArray;
        String parkingSpotsUrl = "_proc/find_reserv_township_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            if (parkingType.equals("Township")) {
                parkingSpotsUrl = "_proc/find_reserv_township_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";
            } else if (parkingType.equals("Commercial")) {
                parkingSpotsUrl = "_proc/find_reserv_commercial_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";
            } else if (parkingType.equals("Other")) {
                parkingSpotsUrl = "_proc/find_reserv_other_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";
            }
            parkingSpots.clear();
            if (googleMap != null) {
                googleMap.clear();
            }
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingSpotsUrl;
            Log.e("#DEBUG", "  fetchReservationParkingSpots:  Url:  " + url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
//                    jsonObject = new JSONObject(responseStr);
                    jsonArray = new JSONArray(responseStr);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject c = jsonArray.getJSONObject(i);
                        item item1 = new item();
                        if (c.has("id")
                                && !TextUtils.isEmpty(c.getString("id"))
                                && !c.getString("id").equals("null")) {
                            item1.setId(c.getString("id"));
                        }
                        if (c.has("location_id")
                                && !TextUtils.isEmpty(c.getString("location_id"))
                                && !c.getString("location_id").equals("null")) {
                            item1.setLocation_id(c.getString("location_id"));
                        }
                        if (c.has("location_place_id")
                                && !TextUtils.isEmpty(c.getString("location_place_id"))
                                && !c.getString("location_place_id").equals("null")) {
                            item1.setLocation_place_id(c.getString("location_place_id"));
                        }
                        if (c.has("location_code")
                                && !TextUtils.isEmpty(c.getString("location_code"))
                                && !c.getString("location_code").equals("null")) {
                            item1.setLocation_code(c.getString("location_code"));
                        }
                        if (c.has("location_name")
                                && !TextUtils.isEmpty(c.getString("location_name"))
                                && !c.getString("location_name").equals("null")) {
                            item1.setLocation_name(c.getString("location_name"));
                        }
                        if (c.has("place_id")
                                && !TextUtils.isEmpty(c.getString("place_id"))
                                && !c.getString("place_id").equals("null")) {
                            item1.setPlace_id(c.getString("place_id"));
                        }
                        if (c.has("active")
                                && !TextUtils.isEmpty(c.getString("active"))
                                && !c.getString("active").equals("null")) {
                            item1.setActive(c.getString("active"));
                        }
                        if (c.has("address")
                                && !TextUtils.isEmpty(c.getString("address"))
                                && !c.getString("address").equals("null")) {
                            item1.setAddress(c.getString("address"));
                        }
                        if (c.has("custom_notice")
                                && !TextUtils.isEmpty(c.getString("custom_notice"))
                                && !c.getString("custom_notice").equals("null")) {
                            item1.setCustom_notice(c.getString("custom_notice"));
                        }
                        if (c.has("in_effect")
                                && !TextUtils.isEmpty(c.getString("in_effect"))
                                && !c.getString("in_effect").equals("null")) {
                            item1.setIn_effect(c.getString("in_effect"));
                        }
                        if (c.has("lat")
                                && !TextUtils.isEmpty(c.getString("lat"))
                                && !c.getString("lat").equals("null")) {
                            item1.setLat(c.getString("lat"));
                        }
                        if (c.has("lng")
                                && !TextUtils.isEmpty(c.getString("lng"))
                                && !c.getString("lng").equals("null")) {
                            item1.setLng(c.getString("lng"));
                        }
                        if (c.has("lots_avbl")
                                && !TextUtils.isEmpty(c.getString("lots_avbl"))
                                && !c.getString("lots_avbl").equals("null")) {
                            item1.setLots_avbl(c.getString("lots_avbl"));
                        }
                        if (c.has("lot_numbering_type")
                                && !TextUtils.isEmpty(c.getString("lot_numbering_type"))
                                && !c.getString("lot_numbering_type").equals("null")) {
                            item1.setLot_numbering_type(c.getString("lot_numbering_type"));
                        }
                        if (c.has("lot_numbering_description")
                                && !TextUtils.isEmpty(c.getString("lot_numbering_description"))
                                && !c.getString("lot_numbering_description").equals("null")) {
                            item1.setLot_numbering_description(c.getString("lot_numbering_description"));
                        }
                        if (c.has("lots_total")
                                && !TextUtils.isEmpty(c.getString("lots_total"))
                                && !c.getString("lots_total").equals("null")) {
                            item1.setLots_total(c.getString("lots_total"));
                        }
                        if (c.has("marker_type")
                                && !TextUtils.isEmpty(c.getString("marker_type"))
                                && !c.getString("marker_type").equals("null")) {
                            item1.setMarker_type(c.getString("marker_type"));
                        }
                        if (c.has("no_parking_times")
                                && !TextUtils.isEmpty(c.getString("no_parking_times"))
                                && !c.getString("no_parking_times").equals("null")) {
                            item1.setNo_parking_times(c.getString("no_parking_times"));
                        }
                        if (c.has("off_peak_discount")
                                && !TextUtils.isEmpty(c.getString("off_peak_discount"))
                                && !c.getString("off_peak_discount").equals("null")) {
                            item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        }
                        if (c.has("off_peak_ends")
                                && !TextUtils.isEmpty(c.getString("off_peak_ends"))
                                && !c.getString("off_peak_ends").equals("null")) {
                            item1.setOff_peak_ends(c.getString("off_peak_ends"));
                        }
                        if (c.has("off_peak_starts")
                                && !TextUtils.isEmpty(c.getString("off_peak_starts"))
                                && !c.getString("off_peak_starts").equals("null")) {
                            item1.setOff_peak_starts(c.getString("off_peak_starts"));
                        }
                        if (c.has("parking_times")
                                && !TextUtils.isEmpty(c.getString("parking_times"))
                                && !c.getString("parking_times").equals("null")) {
                            item1.setParking_times(c.getString("parking_times"));
                        }
                        if (c.has("renewable")
                                && !TextUtils.isEmpty(c.getString("renewable"))
                                && !c.getString("renewable").equals("null")) {
                            item1.setRenewable(c.getString("renewable"));
                        }
                        if (c.has("title")
                                && !TextUtils.isEmpty(c.getString("title"))
                                && !c.getString("title").equals("null")) {
                            item1.setTitle(c.getString("title"));
                        }
                        if (c.has("user_id")
                                && !TextUtils.isEmpty(c.getString("user_id"))
                                && !c.getString("user_id").equals("null")) {
                            item1.setUser_id(c.getString("user_id"));
                        }
                        if (c.has("weekend_special_diff")
                                && !TextUtils.isEmpty(c.getString("weekend_special_diff"))
                                && !c.getString("weekend_special_diff").equals("null")) {
                            item1.setWeekend_special_diff(c.getString("weekend_special_diff"));
                        }
                        if (c.has("twp_id")
                                && !TextUtils.isEmpty(c.getString("twp_id"))
                                && !c.getString("twp_id").equals("null")) {
                            item1.setTwp_id(c.getString("twp_id"));
                        }
                        if (c.has("township_id")
                                && !TextUtils.isEmpty(c.getString("township_id"))
                                && !c.getString("township_id").equals("null")) {
                            item1.setTownship_id(c.getString("township_id"));
                        }
                        if (c.has("township_code")
                                && !TextUtils.isEmpty(c.getString("township_code"))
                                && !c.getString("township_code").equals("null")) {
                            item1.setTownship_code(c.getString("township_code"));
                        }
                        if (c.has("manager_id")
                                && !TextUtils.isEmpty(c.getString("manager_id"))
                                && !c.getString("manager_id").equals("null")) {
                            item1.setManager_id(c.getString("manager_id"));
                        }
                        if (c.has("company_id")
                                && !TextUtils.isEmpty(c.getString("company_id"))
                                && !c.getString("company_id").equals("null")) {
                            item1.setCompany_id(c.getString("company_id"));
                        }
                        if (c.has("division_id")
                                && !TextUtils.isEmpty(c.getString("division_id"))
                                && !c.getString("division_id").equals("null")) {
                            item1.setDivision_id(c.getString("division_id"));
                        }
                        if (c.has("dept_id")
                                && !TextUtils.isEmpty(c.getString("dept_id"))
                                && !c.getString("dept_id").equals("null")) {
                            item1.setDept_id(c.getString("dept_id"));
                        }
                        if (c.has("isKiosk")
                                && !TextUtils.isEmpty(c.getString("isKiosk"))
                                && !c.getString("isKiosk").equals("null")) {
                            item1.setIsKiosk(c.getString("isKiosk"));
                        }
                        if (c.has("manager_type_id")
                                && !TextUtils.isEmpty(c.getString("manager_type_id"))
                                && !c.getString("manager_type_id").equals("null")) {
                            item1.setManager_type_id(c.getString("manager_type_id"));
                        }
                        if (c.has("company_type_id")
                                && !TextUtils.isEmpty(c.getString("company_type_id"))
                                && !c.getString("company_type_id").equals("null")) {
                            item1.setCompany_type_id(c.getString("company_type_id"));
                        }
                        if (c.has("twp_id")
                                && !TextUtils.isEmpty(c.getString("twp_id"))
                                && !c.getString("twp_id").equals("null")) {
                            item1.setTwp_id(c.getString("twp_id"));
                        }
                        if (c.has("distance")
                                && !TextUtils.isEmpty(c.getString("distance"))
                                && !c.getString("distance").equals("null")) {
                            item1.setDistances(c.getString("distance"));
                        }
                        if (c.has("day_type")
                                && !TextUtils.isEmpty(c.getString("day_type"))
                                && !c.getString("day_type").equals("null")) {
                            item1.setDay_type(c.getString("day_type"));
                        }
                        if (c.has("max_duration")
                                && !TextUtils.isEmpty(c.getString("max_duration"))
                                && !c.getString("max_duration").equals("null")) {
                            item1.setMax_duration(c.getString("max_duration"));
                        }
                        if (c.has("pricing")
                                && !TextUtils.isEmpty(c.getString("pricing"))
                                && !c.getString("pricing").equals("null")) {
                            item1.setPricing(c.getString("pricing"));
                        }
                        if (c.has("pricing_duration")
                                && !TextUtils.isEmpty(c.getString("pricing_duration"))
                                && !c.getString("pricing_duration").equals("null")) {
                            item1.setPricing_duration(c.getString("pricing_duration"));
                        }
                        if (c.has("premium_pricing")
                                && !TextUtils.isEmpty(c.getString("premium_pricing"))
                                && !c.getString("premium_pricing").equals("null")) {
                            item1.setPremium_pricing(c.getString("premium_pricing"));
                        }
                        if (c.has("duration_unit")
                                && !TextUtils.isEmpty(c.getString("duration_unit"))
                                && !c.getString("duration_unit").equals("null")) {
                            item1.setDuration_unit(c.getString("duration_unit"));
                        }
                        if (c.has("start_hour")
                                && !TextUtils.isEmpty(c.getString("start_hour"))
                                && !c.getString("start_hour").equals("null")) {
                            item1.setStart_hour(c.getString("start_hour"));
                        }
                        if (c.has("start_time")
                                && !TextUtils.isEmpty(c.getString("start_time"))
                                && !c.getString("start_time").equals("null")) {
                            item1.setStart_time(c.getString("start_time"));
                        }
                        if (c.has("end_hour")
                                && !TextUtils.isEmpty(c.getString("end_hour"))
                                && !c.getString("end_hour").equals("null")) {
                            item1.setEnd_hour(c.getString("end_hour"));
                        }
                        if (c.has("end_time")
                                && !TextUtils.isEmpty(c.getString("end_time"))
                                && !c.getString("end_time").equals("null")) {
                            item1.setEnd_time(c.getString("end_time"));
                        }
                        if (c.has("time_rule")
                                && !TextUtils.isEmpty(c.getString("time_rule"))
                                && !c.getString("time_rule").equals("null")) {
                            item1.setTime_rule(c.getString("time_rule"));
                        }
                        if (c.has("lot_numbering_type")
                                && !TextUtils.isEmpty(c.getString("lot_numbering_type"))
                                && !c.getString("lot_numbering_type").equals("null")) {
                            item1.setLot_numbering_type(c.getString("lot_numbering_type"));
                        }
                        if (c.has("lot_numbering_description")
                                && !TextUtils.isEmpty(c.getString("lot_numbering_description"))
                                && !c.getString("lot_numbering_description").equals("null")) {
                            item1.setLot_numbering_description(c.getString("lot_numbering_description"));
                        }
                        if (c.has("currency")
                                && !TextUtils.isEmpty(c.getString("currency"))
                                && !c.getString("currency").equals("null")) {
                            item1.setCurrency(c.getString("currency"));
                        }
                        if (c.has("currency_symbol")
                                && !TextUtils.isEmpty(c.getString("currency_symbol"))
                                && !c.getString("currency_symbol").equals("null")) {
                            item1.setCurrency_symbol(c.getString("currency_symbol"));
                        }
                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item1.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }
                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item1.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }
                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item1.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }
                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item1.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }
                        if (c.has("Reservation_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_Fee"))
                                && !c.getString("Reservation_PostPayment_Fee").equals("null")) {
                            item1.setReservation_PostPayment_Fee(c.getString("Reservation_PostPayment_Fee"));
                        }
                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item1.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }
                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item1.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }
                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item1.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }
                        if (c.has("ParkNow_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_Fee"))
                                && !c.getString("ParkNow_PostPayment_Fee").equals("null")) {
                            item1.setParkNow_PostPayment_Fee(c.getString("ParkNow_PostPayment_Fee"));
                        }
                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item1.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }
                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item1.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }
                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item1.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }
                        if (c.has("number_of_reservable_spots")
                                && !TextUtils.isEmpty(c.getString("number_of_reservable_spots"))
                                && !c.getString("number_of_reservable_spots").equals("null")) {
                            item1.setNumber_of_reservable_spots(c.getString("number_of_reservable_spots"));
                        }
                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item1.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }
                        if (c.has("address")
                                && !TextUtils.isEmpty(c.getString("address"))
                                && !c.getString("address").equals("null")) {
                            item1.setAddress(c.getString("address"));
                        }
                        if (c.has("no_parking_times")
                                && !TextUtils.isEmpty(c.getString("no_parking_times"))
                                && !c.getString("no_parking_times").equals("null")) {
                            item1.setNo_parking_times(c.getString("no_parking_times"));
                        }
                        if (c.has("off_peak_discount")
                                && !TextUtils.isEmpty(c.getString("off_peak_discount"))
                                && !c.getString("off_peak_discount").equals("null")) {
                            item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        }
                        if (c.has("off_peak_ends")
                                && !TextUtils.isEmpty(c.getString("off_peak_ends"))
                                && !c.getString("off_peak_ends").equals("null")) {
                            item1.setOff_peak_ends(c.getString("off_peak_ends"));
                        }
                        if (c.has("off_peak_starts")
                                && !TextUtils.isEmpty(c.getString("off_peak_starts"))
                                && !c.getString("off_peak_starts").equals("null")) {
                            item1.setOff_peak_starts(c.getString("off_peak_starts"));
                        }
                        if (c.has("parking_times")
                                && !TextUtils.isEmpty(c.getString("parking_times"))
                                && !c.getString("parking_times").equals("null")) {
                            item1.setParking_times(c.getString("parking_times"));
                        }
                        if (c.has("title")
                                && !TextUtils.isEmpty(c.getString("title"))
                                && !c.getString("title").equals("null")) {
                            item1.setTitle(c.getString("title"));
                        }
                        if (c.has("event_on_location")
                                && !TextUtils.isEmpty(c.getString("event_on_location"))
                                && !c.getString("event_on_location").equals("null")) {
                            item1.setEvent_on_location(c.getInt("event_on_location"));
                        } else {
                            item1.setEvent_on_location(0);
                        }

                        if (c.has("event_dates")
                                && !TextUtils.isEmpty(c.getString("event_dates"))
                                && !c.getString("event_dates").equals("null")) {
                            item1.setEvent_dates(c.getString("event_dates"));
                        }

                        for (int j = 0; j < parking_rules.size(); j++) {
                            if (item1.getLocation_code().equals(parking_rules.get(j).getLocation_code())) {
                                item1.setPricing(parking_rules.get(j).getPricing());
                                item1.setPricing_duration(parking_rules.get(j).getPricing_duration());
                                item1.setDuration_unit(parking_rules.get(j).getDuration_unit());
                                item1.setMax_hours(parking_rules.get(j).getMax_hours());
                                item1.setStart_time(parking_rules.get(j).getStart_time());
                                item1.setStart_hour(parking_rules.get(j).getStart_hour());
                                item1.setEnd_time(parking_rules.get(j).getEnd_time());
                                item1.setEnd_hour(parking_rules.get(j).getEnd_hour());
                            }
                        }

                        parkingSpots.add(item1);
                    }
                } catch (IOException e) {
                    Log.e("#DEBUG", "   Error:  " + e.getMessage());
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.e("#DEBUG", "   Error:  " + e.getMessage());
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                rlProgressbar.setVisibility(View.GONE);
                if (jsonArray != null) {
                    if (parkingSpots.size() != 0) {
                        showParkingSpotsOnMap();
                    } else {
                        Toast.makeText(getActivity(), "Not found!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    private void showParkingSpotsOnMap() {
        if (googleMap != null) {
            Log.e("#DEBUG", "  showParkingSpotsOnMap");
            googleMap.clear();
            MarkerOptions markerOptions;
            for (int i = 0; i < eventDefinitions.size(); i++) {
                EventDefinition eventDefinition = eventDefinitions.get(i);
//                Log.e("#DEBUG", "   showParkingSpotsOnMap:  eventCategory:  " + eventDefinition.getEventCategory());
                if (eventDefinition.getManager_type().equalsIgnoreCase(parkingType)
                        && selectedEventCategory.equals(eventDefinition.getEventCategory())) {
                    if (eventDefinition.getLocation_lat_lng() != null) {
                        try {
                            JSONArray jsonArrayLatLng = new JSONArray(eventDefinition.getLocation_lat_lng());
                            for (int j = 0; j < jsonArrayLatLng.length(); j++) {
                                JSONArray jsonArray2 = jsonArrayLatLng.getJSONArray(j);
                                for (int k = 0; k < jsonArray2.length(); k++) {
                                    String[] latLng = jsonArray2.getString(k).split(",");
                                    if (latLng != null && latLng.length > 1) {
                                        markerOptions = new MarkerOptions();
                                        LatLng position = new LatLng(Double.parseDouble(latLng[0]), Double.parseDouble(latLng[1]));
                                        markerOptions.position(position);
                                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapEvent(i)));
                                        markerOptions.title("township");
                                        markerOptions.snippet(String.valueOf(eventDefinition.getId()));
                                        googleMap.addMarker(markerOptions);
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }

            LatLng po1 = new LatLng(latitude, longitude);
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    po1, 12);
            if (centerlang == 0.0 && centerlat == 0.0) {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(po1, 12));
                googleMap.animateCamera(location);
            }
//            final long start = SystemClock.uptimeMillis();
//            final long duration = 1500L;
            // Cancels the previous animation
//            mHandler.removeCallbacks(mAnimation);
//            mAnimation = new BounceAnimation(start, duration, melbourne, mHandler);
//            mHandler.post(mAnimation);


            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    managedpinisclick = true;
                    managedmapisloaded = false;
                    openDetailView(marker);
                    return true;
                }
            });

            googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    managedmapisloaded = true;
                    managedpinisclick = false;
                }
            });

            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    mapisloadcallback = true;
                }
            });

            googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    lat = String.valueOf(cameraPosition.target.latitude);
                    lang = String.valueOf(cameraPosition.target.longitude);
                    LatLng position = googleMap.getCameraPosition().target;
                    centerlat = position.latitude;
                    centerlang = position.longitude;
                    if (managedmapisloaded) {
                        if (!managedpinisclick) {
                            latitude = cameraPosition.target.latitude;
                            longitude = cameraPosition.target.longitude;
                            managedpinisclick = true;
                            Log.e("#DEBUG", "   api_is_start   yes");
                            new fetchReservationParkingSpots().execute();
//                            fetchReservationParkingSpots();
//                            new getparkingpin(lat, lang).execute();
                        }
                    }
                }
            });


        } else {
            Log.e("#DEBUG", "  showParkingSpotsOnMap:  googleMap null");
        }
    }

    private Bitmap getMarkerBitmapEvent(int pos) {
        LinearLayout distanceMarkerLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.marker_with_price, null);
        TextView tvPrice = distanceMarkerLayout.findViewById(R.id.tvPrice);
        TextView tvLocationName = distanceMarkerLayout.findViewById(R.id.tvLocationName);
        if (!TextUtils.isEmpty(eventDefinitions.get(pos).getEvent_name())) {
            tvLocationName.setVisibility(View.VISIBLE);
            tvLocationName.setText(eventDefinitions.get(pos).getEvent_name());
        } else {
            tvLocationName.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(eventDefinitions.get(pos).getEvent_type())) {
            tvPrice.setText(eventDefinitions.get(pos).getEvent_type());
        }
        distanceMarkerLayout.setDrawingCacheEnabled(true);
        distanceMarkerLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        distanceMarkerLayout.layout(0, 0, distanceMarkerLayout.getMeasuredWidth(), distanceMarkerLayout.getMeasuredHeight());
        distanceMarkerLayout.buildDrawingCache(true);

        Bitmap bitmap = Bitmap.createBitmap(distanceMarkerLayout.getDrawingCache());
        distanceMarkerLayout.setDrawingCacheEnabled(false);
        BitmapDescriptor flagBitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
        return bitmap;
    }

    private void openDetailView(Marker marker) {
        item itemLocation = new item();
        itemLocation.setLat(String.valueOf(marker.getPosition().latitude));
        itemLocation.setLng(String.valueOf(marker.getPosition().longitude));
        ((vchome) getActivity()).show_back_button();
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        EventSpotDetailsFragment pay = new EventSpotDetailsFragment();
        Bundle bundle = new Bundle();
        for (int i = 0; i < eventDefinitions.size(); i++) {
            if (marker.getSnippet().equalsIgnoreCase(String.valueOf(eventDefinitions.get(i).getId()))) {
                bundle.putString("eventDefinition", new Gson().toJson(eventDefinitions.get(i)));
            }
        }
//        bundle.putString("selectedSpot", new Gson().toJson(parkingSpots.get(parkingSpots.indexOf(itemLocation))));
        bundle.putString("parkingType", parkingType);
        bundle.putBoolean("isReserveSpot", true);
        bundle.putString("itemLocation", new Gson().toJson(itemLocation));
        pay.setArguments(bundle);
        ft.add(R.id.My_Container_1_ID, pay);
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(pay);
        ft.commitAllowingStateLoss();
    }

    private Bitmap getMarkerBitmap(int pos) {

        String text = parkingSpots.get(pos).getPricing() != null
                ? (!parkingSpots.get(pos).getPricing().equals("0")
                ? (!parkingSpots.get(pos).getPricing().equals("null")
                ? "$" + parkingSpots.get(pos).getPricing() + "/"
                + parkingSpots.get(pos).getPricing_duration() + " "
                + parkingSpots.get(pos).getDuration_unit() + "" : "FREE") : "FREE") : "FREE";

        LinearLayout distanceMarkerLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.marker_with_price, null);
        TextView positionDistance = distanceMarkerLayout.findViewById(R.id.tvPrice);
        TextView tvLocationName = distanceMarkerLayout.findViewById(R.id.tvLocationName);
        positionDistance.setText(text);
        if (!TextUtils.isEmpty(parkingSpots.get(pos).getLocation_name())) {
            tvLocationName.setVisibility(View.VISIBLE);
            tvLocationName.setText(parkingSpots.get(pos).getLocation_name());
        } else {
            tvLocationName.setVisibility(View.GONE);
        }
        distanceMarkerLayout.setDrawingCacheEnabled(true);
        distanceMarkerLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        distanceMarkerLayout.layout(0, 0, distanceMarkerLayout.getMeasuredWidth(), distanceMarkerLayout.getMeasuredHeight());
        distanceMarkerLayout.buildDrawingCache(true);


        Bitmap bitmap = Bitmap.createBitmap(distanceMarkerLayout.getDrawingCache());
        distanceMarkerLayout.setDrawingCacheEnabled(false);
        BitmapDescriptor flagBitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
        return bitmap;
    }

    @Override
    protected void updateViews() {
        if (isAllOptions) {
            llBottomOptions.setVisibility(View.VISIBLE);
        } else llBottomOptions.setVisibility(View.GONE);
    }

    @Override
    protected void setListeners() {
        ivBtnFindNearBy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isNearByVisible = true;
                mapisloadcallback = false;
                getCurrentLocation();
                searchotherlocation = false;
                nearbylist.clear();
                new fetchNearByList().execute();
            }
        });

        ivBtnOtherCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.getString("click", "commercia");
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Other_City pay = new Other_City();
                pay.setArguments(b);
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                pay.registerForListener(EventsMapFragment.this);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        binding.rlBtnCurrentEvent.setOnClickListener(v -> {
            binding.ivCurrentEvent.setAlpha((float) 1.0);
            binding.ivUpcomingEvent.setAlpha((float) 0.3);
            binding.ivPastEvent.setAlpha((float) 0.3);
            if (managerTypes.size() > 0) {
                parkingType = managerTypes.get(selectedTypeIndex);
                selectedTypeIndex++;
                if (selectedTypeIndex < managerTypes.size()) {
                } else selectedTypeIndex = 0;
            } else {
                parkingType = "Township";
            }
            selectedEventCategory = "current";
            binding.tvSelectedCurrentType.setText(parkingType);
            binding.tvSelectedCurrentType.setVisibility(View.VISIBLE);
            binding.tvSelectedUpcomingType.setVisibility(View.GONE);
            binding.tvSelectedPastType.setVisibility(View.GONE);
            if (isNearByVisible) {
                findNearbyWithCurrent();
            } else {
                showParkingSpotsOnMap();
            }
        });

        binding.rlBtnUpcomingEvent.setOnClickListener(v -> {
            binding.ivCurrentEvent.setAlpha((float) 0.3);
            binding.ivUpcomingEvent.setAlpha((float) 1.0);
            binding.ivPastEvent.setAlpha((float) 0.3);
            if (managerTypes.size() > 0) {
                parkingType = managerTypes.get(selectedTypeIndex);
                selectedTypeIndex++;
                if (selectedTypeIndex < managerTypes.size()) {

                } else selectedTypeIndex = 0;

            } else {
                parkingType = "Township";
            }
            selectedEventCategory = "upcoming";
            binding.tvSelectedUpcomingType.setText(parkingType);
            binding.tvSelectedCurrentType.setVisibility(View.GONE);
            binding.tvSelectedUpcomingType.setVisibility(View.VISIBLE);
            binding.tvSelectedPastType.setVisibility(View.GONE);
            if (isNearByVisible) {
                findNearbyWithCurrent();
            } else {
                showParkingSpotsOnMap();
            }
//            new getparkingrules().execute();
        });

        binding.rlBtnPastEvent.setOnClickListener(v -> {
            binding.ivCurrentEvent.setAlpha((float) 0.3);
            binding.ivUpcomingEvent.setAlpha((float) 0.3);
            binding.ivPastEvent.setAlpha((float) 1.0);
            if (managerTypes.size() > 0) {
                parkingType = managerTypes.get(selectedTypeIndex);
                selectedTypeIndex++;
                if (selectedTypeIndex < managerTypes.size()) {
                } else selectedTypeIndex = 0;

            } else {
                parkingType = "Township";
            }
            selectedEventCategory = "past";
            binding.tvSelectedPastType.setText(parkingType);
            binding.tvSelectedCurrentType.setVisibility(View.GONE);
            binding.tvSelectedUpcomingType.setVisibility(View.GONE);
            binding.tvSelectedPastType.setVisibility(View.VISIBLE);
            if (isNearByVisible) {
                findNearbyWithCurrent();
            } else {
                showParkingSpotsOnMap();
            }
//            new getparkingrules().execute();
        });
    }

    public class fetchNearByList extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String nearbyurl = "_proc/find_nearby_events?in_lat=" + latitude + "&in_lng=" + longitude + "";

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            if (lat.equals("null")) {
                nearbyurl = "_proc/find_nearby_events?in_lat=" + latitude + "&in_lng=" + longitude + "";
            } else {
                nearbyurl = "_proc/find_nearby_events?in_lat=" + lat + "&in_lng=" + lang + "";
            }
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            nearByEvents.clear();
            findnearbyarray.clear();
            nearbylist.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + nearbyurl;
            Log.e("#DEBUG", "  fetchNearByList:  Url:  " + url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONArray jsonArray = new JSONArray(responseStr);
                    for (int j = 0; j < jsonArray.length(); j++) {
                        EventDefinition eventDefinition = new EventDefinition();
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            eventDefinition.setId(object.getInt("id"));
                            eventDefinition.setEvent_id(object.getInt("id"));
                        }
                        if (object.has("date_time")
                                && !TextUtils.isEmpty(object.getString("date_time"))
                                && !object.getString("date_time").equals("null")) {
                            eventDefinition.setDate_time(object.getString("date_time"));
                        }
                        if (object.has("manager_type")
                                && !TextUtils.isEmpty(object.getString("manager_type"))
                                && !object.getString("manager_type").equals("null")) {
                            eventDefinition.setManager_type(object.getString("manager_type"));
                        }
                        if (object.has("manager_type_id")
                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
                                && !object.getString("manager_type_id").equals("null")) {
                            eventDefinition.setManager_type_id(object.getInt("manager_type_id"));
                        }
                        if (object.has("twp_id")
                                && !TextUtils.isEmpty(object.getString("twp_id"))
                                && !object.getString("twp_id").equals("null")) {
                            eventDefinition.setTwp_id(object.getInt("twp_id"));
                        }
                        if (object.has("township_code")
                                && !TextUtils.isEmpty(object.getString("township_code"))
                                && !object.getString("township_code").equals("null")) {
                            eventDefinition.setTownship_code(object.getString("township_code"));
                        }
                        if (object.has("township_name")
                                && !TextUtils.isEmpty(object.getString("township_name"))
                                && !object.getString("township_name").equals("null")) {
                            eventDefinition.setTownship_name(object.getString("township_name"));
                        }
                        if (object.has("company_id")
                                && !TextUtils.isEmpty(object.getString("company_id"))
                                && !object.getString("company_id").equals("null")) {
                            eventDefinition.setCompany_id(object.getInt("company_id"));
                        }
                        if (object.has("company_code")
                                && !TextUtils.isEmpty(object.getString("company_code"))
                                && !object.getString("company_code").equals("null")) {
                            eventDefinition.setCompany_code(object.getString("company_code"));
                        }
                        if (object.has("company_name")
                                && !TextUtils.isEmpty(object.getString("company_name"))
                                && !object.getString("company_name").equals("null")) {
                            eventDefinition.setCompany_name(object.getString("company_name"));
                        }
                        if (object.has("event_type")
                                && !TextUtils.isEmpty(object.getString("event_type"))
                                && !object.getString("event_type").equals("null")) {
                            eventDefinition.setEvent_type(object.getString("event_type"));
                        }
                        if (object.has("event_name")
                                && !TextUtils.isEmpty(object.getString("event_name"))
                                && !object.getString("event_name").equals("null")) {
                            eventDefinition.setEvent_name(object.getString("event_name"));
                        }
                        if (object.has("event_short_description")
                                && !TextUtils.isEmpty(object.getString("event_short_description"))
                                && !object.getString("event_short_description").equals("null")) {
                            eventDefinition.setEvent_short_description(object.getString("event_short_description"));
                        }
                        if (object.has("event_long_description")
                                && !TextUtils.isEmpty(object.getString("event_long_description"))
                                && !object.getString("event_long_description").equals("null")) {
                            eventDefinition.setEvent_long_description(object.getString("event_long_description"));
                        }
                        if (object.has("event_link_on_web")
                                && !TextUtils.isEmpty(object.getString("event_link_on_web"))
                                && !object.getString("event_link_on_web").equals("null")) {
                            eventDefinition.setEvent_link_on_web(object.getString("event_link_on_web"));
                        }
                        if (object.has("event_link_twitter")
                                && !TextUtils.isEmpty(object.getString("event_link_twitter"))
                                && !object.getString("event_link_twitter").equals("null")) {
                            eventDefinition.setEvent_link_twitter(object.getString("event_link_twitter"));
                        }
                        if (object.has("event_link_whatsapp")
                                && !TextUtils.isEmpty(object.getString("event_link_whatsapp"))
                                && !object.getString("event_link_whatsapp").equals("null")) {
                            eventDefinition.setEvent_link_whatsapp(object.getString("event_link_whatsapp"));
                        }
                        if (object.has("event_link_other_media")
                                && !TextUtils.isEmpty(object.getString("event_link_other_media"))
                                && !object.getString("event_link_other_media").equals("null")) {
                            eventDefinition.setEvent_link_other_media(object.getString("event_link_other_media"));
                        }
                        if (object.has("company_logo")
                                && !TextUtils.isEmpty(object.getString("company_logo"))
                                && !object.getString("company_logo").equals("null")) {
                            eventDefinition.setCompany_logo(object.getString("company_logo"));
                        }
                        if (object.has("event_logo")
                                && !TextUtils.isEmpty(object.getString("event_logo"))
                                && !object.getString("event_logo").equals("null")) {
                            eventDefinition.setEvent_logo(object.getString("event_logo"));
                        }
                        if (object.has("event_image1")
                                && !TextUtils.isEmpty(object.getString("event_image1"))
                                && !object.getString("event_image1").equals("null")) {
                            eventDefinition.setEvent_image1(object.getString("event_image1"));
                        }
                        if (object.has("event_image2")
                                && !TextUtils.isEmpty(object.getString("event_image2"))
                                && !object.getString("event_image2").equals("null")) {
                            eventDefinition.setEvent_image2(object.getString("event_image2"));
                        }
                        if (object.has("event_blob_image")
                                && !TextUtils.isEmpty(object.getString("event_blob_image"))
                                && !object.getString("event_blob_image").equals("null")) {
                            eventDefinition.setEvent_blob_image(object.getString("event_blob_image"));
                        }
                        if (object.has("event_address")
                                && !TextUtils.isEmpty(object.getString("event_address"))
                                && !object.getString("event_address").equals("null")) {
                            eventDefinition.setEvent_address(object.getString("event_address"));
                        }
                        if (object.has("covered_locations")
                                && !TextUtils.isEmpty(object.getString("covered_locations"))
                                && !object.getString("covered_locations").equals("null")) {
                            eventDefinition.setCovered_locations(object.getString("covered_locations"));
                        }
                        if (object.has("requirements")
                                && !TextUtils.isEmpty(object.getString("requirements"))
                                && !object.getString("requirements").equals("null")) {
                            eventDefinition.setRequirements(object.getString("requirements"));
                        }
                        if (object.has("appl_req_download")
                                && !TextUtils.isEmpty(object.getString("appl_req_download"))
                                && !object.getString("appl_req_download").equals("null")) {
                            eventDefinition.setAppl_req_download(object.getString("appl_req_download"));
                        }
                        if (object.has("cost")
                                && !TextUtils.isEmpty(object.getString("cost"))
                                && !object.getString("cost").equals("null")) {
                            eventDefinition.setCost(object.getString("cost"));
                        }
                        if (object.has("year")
                                && !TextUtils.isEmpty(object.getString("year"))
                                && !object.getString("year").equals("null")) {
                            eventDefinition.setYear(object.getString("year"));
                        }
                        if (object.has("location_address")
                                && !TextUtils.isEmpty(object.getString("location_address"))
                                && !object.getString("location_address").equals("null")) {
                            eventDefinition.setLocation_address(object.getString("location_address"));
                        }
                        if (object.has("scheme_type")
                                && !TextUtils.isEmpty(object.getString("scheme_type"))
                                && !object.getString("scheme_type").equals("null")) {
                            eventDefinition.setScheme_type(object.getString("scheme_type"));
                        }
                        if (object.has("event_prefix")
                                && !TextUtils.isEmpty(object.getString("event_prefix"))
                                && !object.getString("event_prefix").equals("null")) {
                            eventDefinition.setEvent_prefix(object.getString("event_prefix"));
                        }
                        if (object.has("event_nextnum")
                                && !TextUtils.isEmpty(object.getString("event_nextnum"))
                                && !object.getString("event_nextnum").equals("null")) {
                            eventDefinition.setEvent_nextnum(object.getString("event_nextnum"));
                        }
                        if (object.has("event_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_begins_date_time"))
                                && !object.getString("event_begins_date_time").equals("null")) {
                            eventDefinition.setEvent_begins_date_time(object.getString("event_begins_date_time"));
                        }
                        if (object.has("event_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_ends_date_time"))
                                && !object.getString("event_ends_date_time").equals("null")) {
                            eventDefinition.setEvent_ends_date_time(object.getString("event_ends_date_time"));
                        }
                        if (object.has("event_parking_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_parking_begins_date_time"))
                                && !object.getString("event_parking_begins_date_time").equals("null")) {
                            eventDefinition.setEvent_parking_begins_date_time(object.getString("event_parking_begins_date_time"));
                        }
                        if (object.has("event_parking_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_parking_ends_date_time"))
                                && !object.getString("event_parking_ends_date_time").equals("null")) {
                            eventDefinition.setEvent_parking_ends_date_time(object.getString("event_parking_ends_date_time"));
                        }

                        if (object.has("event_multi_dates")
                                && !TextUtils.isEmpty(object.getString("event_multi_dates"))
                                && !object.getString("event_multi_dates").equals("null")) {
                            eventDefinition.setEvent_multi_dates(object.getString("event_multi_dates"));
                            String currentDate = "";
                            currentDate = DateTimeUtils.getCurrentDateYYYYMMDD();
                            long currentDateMs = DateTimeUtils.getMilliseconds(currentDate);
                            ArrayList<String> dates = new ArrayList<>();
                            try {
                                JSONArray jsonArray2 = new JSONArray(eventDefinition.getEvent_multi_dates());
                                for (int i = 0; i < jsonArray2.length(); i++) {
                                    JSONArray jsonArray1 = jsonArray2.getJSONArray(i);
                                    if (jsonArray1 != null && jsonArray1.length() > 0) {
                                        dates.add(jsonArray1.get(0).toString());
                                        if (currentDateMs == DateTimeUtils.getMilliseconds(jsonArray1.get(0).toString())) {
                                            eventDefinition.setEventCategory("current");
                                        } else if (currentDateMs > DateTimeUtils.getMilliseconds(jsonArray1.get(0).toString())) {
                                            eventDefinition.setEventCategory("past");
                                        } else if (currentDateMs < DateTimeUtils.getMilliseconds(jsonArray1.get(0).toString())) {
                                            eventDefinition.setEventCategory("upcoming");
                                        } else {
                                            eventDefinition.setEventCategory("");
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        if (object.has("event_parking_timings")
                                && !TextUtils.isEmpty(object.getString("event_parking_timings"))
                                && !object.getString("event_parking_timings").equals("null")) {
                            eventDefinition.setEvent_parking_timings(object.getString("event_parking_timings"));
                        }

                        if (object.has("event_event_timings")
                                && !TextUtils.isEmpty(object.getString("event_event_timings"))
                                && !object.getString("event_event_timings").equals("null")) {
                            eventDefinition.setEvent_event_timings(object.getString("event_event_timings"));
                        }

                        if (object.has("expires_by")
                                && !TextUtils.isEmpty(object.getString("expires_by"))
                                && !object.getString("expires_by").equals("null")) {
                            eventDefinition.setExpires_by(object.getString("expires_by"));
                        }
                        if (object.has("regn_reqd")
                                && !TextUtils.isEmpty(object.getString("regn_reqd"))
                                && !object.getString("regn_reqd").equals("null")) {
                            eventDefinition.setRegn_reqd(object.getInt("regn_reqd") == 1);
                        }
                        if (object.has("regn_reqd_for_parking")
                                && !TextUtils.isEmpty(object.getString("regn_reqd_for_parking"))
                                && !object.getString("regn_reqd_for_parking").equals("null")) {
                            eventDefinition.setRegn_reqd_for_parking(object.getInt("regn_reqd_for_parking") == 1);
                        }
                        if (object.has("renewable")
                                && !TextUtils.isEmpty(object.getString("renewable"))
                                && !object.getString("renewable").equals("null")) {
                            eventDefinition.setRenewable(object.getInt("renewable") == 1);
                        }
                        if (object.has("active")
                                && !TextUtils.isEmpty(object.getString("active"))
                                && !object.getString("active").equals("null")) {
                            eventDefinition.setActive(object.getInt("active") == 1);
                        }

                        if (object.has("event_multi_sessions")
                                && !TextUtils.isEmpty(object.getString("event_multi_sessions"))
                                && !object.getString("event_multi_sessions").equals("null")) {
                            eventDefinition.setEvent_multi_sessions(object.getInt("event_multi_sessions") == 1);
                        }

                        if (object.has("event_indiv_session_regn_allowed")
                                && !TextUtils.isEmpty(object.getString("event_indiv_session_regn_allowed"))
                                && !object.getString("event_indiv_session_regn_allowed").equals("null")) {
                            eventDefinition.setEvent_indiv_session_regn_allowed(object.getInt("event_indiv_session_regn_allowed") == 1);
                        }

                        if (object.has("event_indiv_session_regn_required")
                                && !TextUtils.isEmpty(object.getString("event_indiv_session_regn_required"))
                                && !object.getString("event_indiv_session_regn_required").equals("null")) {
                            eventDefinition.setEvent_indiv_session_regn_required(object.getInt("event_indiv_session_regn_required") == 1);
                        }

                        if (object.has("event_full_regn_required")
                                && !TextUtils.isEmpty(object.getString("event_full_regn_required"))
                                && !object.getString("event_full_regn_required").equals("null")) {
                            eventDefinition.setEvent_full_regn_required(object.getInt("event_full_regn_required") == 1);
                        }

                        if (object.has("event_full_regn_fee")
                                && !TextUtils.isEmpty(object.getString("event_full_regn_fee"))
                                && !object.getString("event_full_regn_fee").equals("null")) {
                            eventDefinition.setEvent_full_regn_fee(object.getString("event_full_regn_fee"));
                        }

                        if (object.has("event_parking_fee")
                                && !TextUtils.isEmpty(object.getString("event_parking_fee"))
                                && !object.getString("event_parking_fee").equals("null")) {
                            eventDefinition.setEvent_parking_fee(object.getString("event_parking_fee"));
                        }

                        if (object.has("location_lat_lng")
                                && !TextUtils.isEmpty(object.getString("location_lat_lng"))
                                && !object.getString("location_lat_lng").equals("null")) {
                            eventDefinition.setLocation_lat_lng(object.getString("location_lat_lng"));
                        }

                        if (object.has("lat")
                                && !TextUtils.isEmpty(object.getString("lat"))
                                && !object.getString("lat").equals("null")) {
                            eventDefinition.setLat(object.getString("lat"));
                        }

                        if (object.has("lng")
                                && !TextUtils.isEmpty(object.getString("lng"))
                                && !object.getString("lng").equals("null")) {
                            eventDefinition.setLng(object.getString("lng"));
                        }

                        if (object.has("distance")
                                && !TextUtils.isEmpty(object.getString("distance"))
                                && !object.getString("distance").equals("null")) {
                            eventDefinition.setDistance(object.getDouble("distance"));
                        }

                        nearByEvents.add(eventDefinition);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null) {
                rlProgressbar.setVisibility(View.GONE);
                Log.e("#DEBUG", "   fetchNearByList:  findnearbyarray: Size: " + findnearbyarray.size());
                //progressBar.setVisibility(View.GONE);
                Resources rs;
                mapisloadcallback = false;
                managedmapisloaded = false;
                managedpinisclick = true;
                findNearbyWithCurrent();
            }
            super.onPostExecute(s);
        }
    }

    public void findNearbyWithCurrent() {
        nearByEventsWithDistance.clear();
        for (int j1 = 0; j1 < nearByEvents.size(); j1++) {
            EventDefinition eventDefinition = nearByEvents.get(j1);
            if (eventDefinition.getManager_type().equalsIgnoreCase(parkingType)
                    && selectedEventCategory.equals(eventDefinition.getEventCategory())) {
                try {
                    JSONArray jsonArrayLatLng = new JSONArray(eventDefinition.getLocation_lat_lng());
                    for (int i = 0; i < jsonArrayLatLng.length(); i++) {
                        JSONArray jsonArray2 = jsonArrayLatLng.getJSONArray(i);
                        for (int j = 0; j < jsonArray2.length(); j++) {
                            EventDefinition eventDefinition1 = new EventDefinition();
                            eventDefinition1.setId(nearByEvents.get(j1).getId());
                            eventDefinition1.setEvent_name(nearByEvents.get(j1).getEvent_name());
                            eventDefinition1.setEvent_begins_date_time(nearByEvents.get(j1).getEvent_begins_date_time());
                            eventDefinition1.setEvent_ends_date_time(nearByEvents.get(j1).getEvent_ends_date_time());
                            eventDefinition1.setDistance(nearByEvents.get(j1).getDistance());
                            eventDefinition1.setEvent_full_regn_fee(nearByEvents.get(j1).getEvent_full_regn_fee());
                            eventDefinition1.setEvent_logo(nearByEvents.get(j1).getEvent_logo());
                            eventDefinition1.setActive(nearByEvents.get(j1).isActive());
                            String[] latLng = jsonArray2.getString(j).split(",");
                            if (latLng.length == 2) {
                                eventDefinition1.setLat(latLng[0]);
                                eventDefinition1.setLng(latLng[1]);
                                JSONArray jsonArray11 = new JSONArray(eventDefinition.getEvent_address());
                                JSONArray jsonArray22 = jsonArray11.getJSONArray(i);
                                eventDefinition1.setDisplay_address(jsonArray22.getString(0));
                                Log.e("#DEBUG", "    eventDefinition1:  " + eventDefinition1.getDisplay_address());
                                nearByEventsWithDistance.add(eventDefinition1);
                            }
                        }
                    }
                    for (int i = 0; i < nearByEventsWithDistance.size(); i++) {
                        Log.e("#DEBUG", "  Position:  " + i + "    Display_address:  " + nearByEventsWithDistance.get(i).getDisplay_address());
                        Log.i("ResponseMSG", "  Position:  " + i + "    Display_address:  " + nearByEventsWithDistance.get(i).getDisplay_address());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        Resources rs;
        //list_nearby.setAdapter(null);
        rl_nearbylist.setVisibility(View.VISIBLE);
        adpater = new adpaternearby(getActivity(), nearByEventsWithDistance, rs = getResources());
        list_nearby.setAdapter(adpater);
        list_nearby.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                item itemLocation = new item();
                itemLocation.setLat(nearByEventsWithDistance.get(position).getLat());
                itemLocation.setLng(nearByEventsWithDistance.get(position).getLng());
                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                EventSpotDetailsFragment pay = new EventSpotDetailsFragment();
                Bundle bundle = new Bundle();
                for (int i = 0; i < eventDefinitions.size(); i++) {
                    if (nearByEventsWithDistance.get(position).getId() == eventDefinitions.get(i).getId()) {
                        bundle.putString("eventDefinition", new Gson().toJson(eventDefinitions.get(i)));
                    }
                }
                bundle.putString("parkingType", parkingType);
                bundle.putBoolean("isReserveSpot", true);
                bundle.putString("itemLocation", new Gson().toJson(itemLocation));
                pay.setArguments(bundle);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();

            }
        });
        img_close_nearbylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_nearbylist.setVisibility(View.GONE);
                isNearByVisible = false;
            }
        });
        // shownearbyparkin(vchome.this);
    }

    @Override
    protected void initViews(View v) {
        ivBtnFindNearBy = v.findViewById(R.id.ivBtnFindNearBy);
        ivBtnOtherCity = v.findViewById(R.id.ivBtnOtherCity);
        rlProgressbar = v.findViewById(R.id.rlProgressbar);

        rl_nearbylist = v.findViewById(R.id.rl_nearbylist);
        list_nearby = v.findViewById(R.id.listofnearbyparking);
        img_close_nearbylist = v.findViewById(R.id.close);

        rl_searchinfo = v.findViewById(R.id.rl_searchinfo);
        rl_searchinfo.setVisibility(View.GONE);

        llBottomOptions = v.findViewById(R.id.llBottomOptions);

        eventAdapter = new EventAdapter();
        binding.rvSearchEvent.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvSearchEvent.setAdapter(eventAdapter);
    }

    public void getCurrentLocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
            Log.i("EventEzlyApp", "2 Lat: " + latitude + " Lang: " + longitude);
        }
    }

    public class adpaternearby extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        EventDefinition tempValues = null;
        Context context;
        EventDefinition state;
        private Activity activity;
        private ArrayList<EventDefinition> data;
        private LayoutInflater inflater = null;

        public adpaternearby(Activity a, ArrayList<EventDefinition> d, Resources resLocal) {
            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;

            if (convertView == null) {
                vi = inflater.inflate(R.layout.listofnearby_event, null);
                holder = new ViewHolder();
                holder.label = vi.findViewById(R.id.label);
                holder.tvEventName = vi.findViewById(R.id.tvEventName);
                holder.tvEventAddress = vi.findViewById(R.id.tvEventAddress);
                holder.tvEventTime = vi.findViewById(R.id.tvEventTime);
                holder.tvEventPrice = vi.findViewById(R.id.tvEventPrice);
                holder.tvEventDistance = vi.findViewById(R.id.tvEventDistance);
                holder.ivImage = vi.findViewById(R.id.ivImage);
                holder.tvStatus = vi.findViewById(R.id.tvStatus);
                vi.setTag(holder);
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data != null && data.size() > 0) {
                holder.ivImage.setVisibility(View.VISIBLE);
                Glide.with(activity).load(data.get(position).getEvent_logo())
                        .placeholder(R.drawable.place_holder).into(holder.ivImage);
                holder.tvEventName.setText(data.get(position).getEvent_name());
                holder.tvEventAddress.setText(data.get(position).getDisplay_address());
                holder.tvEventTime.setText(DateTimeUtils.getMMMDDYYYY(data.get(position).getEvent_begins_date_time()));
                holder.tvEventPrice.setText("$" + data.get(position).getEvent_full_regn_fee());
                if (data.get(position).isActive()) {
                    holder.tvStatus.setText("Active");
                } else {
                    holder.tvStatus.setText("Inactive");
                }

                String finalans = "";
                if (!data.get(position).getLat().equals("") && !data.get(position).getLng().equals("")) {
                    LatLng latLng = new LatLng(Double.parseDouble(data.get(position).getLat()), Double.parseDouble(data.get(position).getLng()));
                    double dis = getDistance(latLng);
                    double ff = dis * 0.000621371192;
                    finalans = String.format("%.2f", ff);

                    if (finalans.equals("1.0")) {
                        holder.tvEventDistance.setText(finalans + " Mile");
                    } else {
                        holder.tvEventDistance.setText(finalans + " Miles");
                    }
                }
            } else {
                holder.ivImage.setVisibility(View.GONE);
            }

            if (position == 0) {
                holder.label.setVisibility(View.GONE);
            } else {
                holder.label.setVisibility(View.GONE);
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            TextView label, tvEventName, tvEventAddress, tvEventTime, tvEventPrice, tvEventDistance,
                    tvStatus;
            ImageView ivImage;

        }
    }

    public double getDistance(LatLng LatLng1) {
        double distance = 0;
        Location locationA = new Location("A");
        locationA.setLatitude(LatLng1.latitude);
        locationA.setLongitude(LatLng1.longitude);
        Location locationB = new Location("B");
        locationB.setLatitude(latitude);
        locationB.setLongitude(longitude);
        distance = locationA.distanceTo(locationB);
        return distance;
    }

    private class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventHolder> {

        @NonNull
        @Override
        public EventAdapter.EventHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new EventHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_event_search, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull EventAdapter.EventHolder holder, int i) {
            EventDefinition eventDefinition = eventDefinitionsSearch.get(i);
            if (eventDefinition != null) {

                Glide.with(getActivity()).load(eventDefinition.getEvent_logo())
                        .placeholder(R.drawable.place_holder).into(holder.ivImage);
                holder.tvEventName.setText(eventDefinition.getEvent_name());
                holder.tvEventAddress.setText(eventDefinition.getDisplay_address());
                holder.tvEventTime.setText(DateTimeUtils.getMMMDDYYYY(eventDefinition.getEvent_begins_date_time()));
                holder.tvEventPrice.setText("$" + eventDefinition.getEvent_full_regn_fee());
                if (eventDefinition.isActive()) {
                    holder.tvStatus.setText("Active");
                } else {
                    holder.tvStatus.setText("Inactive");
                }

                String finalans = "";
                if (!eventDefinition.getLat().equals("") && !eventDefinition.getLng().equals("")) {
                    LatLng latLng = new LatLng(Double.parseDouble(eventDefinition.getLat()), Double.parseDouble(eventDefinition.getLng()));
                    double dis = getDistance(latLng);
                    double ff = dis * 0.000621371192;
                    finalans = String.format("%.2f", ff);
                    if (finalans.equals("1.0")) {
                        holder.tvEventDistance.setText(finalans + " Mile");
                    } else {
                        holder.tvEventDistance.setText(finalans + " Miles");
                    }

                }
            }
        }

        @Override
        public int getItemCount() {
            return eventDefinitionsSearch.size();
        }

        class EventHolder extends RecyclerView.ViewHolder {
            TextView tvEventName, tvEventAddress, tvEventTime, tvEventPrice, tvEventDistance,
                    tvStatus;
            ImageView ivImage;

            EventHolder(@NonNull View itemView) {
                super(itemView);

                tvEventName = itemView.findViewById(R.id.tvEventName);
                tvEventAddress = itemView.findViewById(R.id.tvEventAddress);
                tvEventTime = itemView.findViewById(R.id.tvEventTime);
                tvEventPrice = itemView.findViewById(R.id.tvEventPrice);
                tvEventDistance = itemView.findViewById(R.id.tvEventDistance);
                ivImage = itemView.findViewById(R.id.ivImage);
                tvStatus = itemView.findViewById(R.id.tvStatus);

                itemView.setOnClickListener(v -> {
                    binding.llEventSearch.setVisibility(View.GONE);
                    final int position = getAdapterPosition();
                    item itemLocation = new item();
                    itemLocation.setLat(eventDefinitionsSearch.get(position).getLat());
                    itemLocation.setLng(eventDefinitionsSearch.get(position).getLng());
                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    EventSpotDetailsFragment pay = new EventSpotDetailsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("eventDefinition", new Gson().toJson(eventDefinitionsSearch.get(position)));
                    bundle.putString("parkingType", parkingType);
                    bundle.putBoolean("isReserveSpot", true);
                    bundle.putString("itemLocation", new Gson().toJson(itemLocation));
                    pay.setArguments(bundle);
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                });
            }
        }
    }
}
