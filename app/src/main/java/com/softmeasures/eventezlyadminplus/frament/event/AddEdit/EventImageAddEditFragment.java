package com.softmeasures.eventezlyadminplus.frament.event.AddEdit;

import static android.app.Activity.RESULT_OK;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.frament.event.AllEventsFragment.isUpdated;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_LOCATION;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_VIRTUALLY;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_BOTH;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nileshp.multiphotopicker.photopicker.activity.PickImageActivity;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragEventImageAddEditBinding;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.EventImage;

import java.util.ArrayList;

public class EventImageAddEditFragment extends BaseFragment {

    public FragEventImageAddEditBinding binding;
    private String imageOption = "";
    public static boolean isEventLogoChange = false;
    private static final int REQUEST_PICK_IMAGE = 985;

    private EventDefinition eventDefinition;
    private ArrayList<EventImage> eventImages = new ArrayList<>();
    private EventImageAdapter eventImageAdapter;
    private boolean isEdit = false, isRepeat = false;
    ProgressDialog dialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            isEdit = getArguments().getBoolean("isEdit", false);
            isRepeat = getArguments().getBoolean("isRepeat", false);
            eventDefinition = new Gson().fromJson(getArguments().getString("eventDefinition"), EventDefinition.class);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_event_image_add_edit, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {
        if (isEdit) {
            if (eventDefinition != null) {
                if (eventDefinition.getEvent_logo() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_logo())) {
                    binding.rlBtnAddEventLogo.setVisibility(View.GONE);
                    binding.ivEventLogoClose.setVisibility(View.VISIBLE);
                    Glide.with(getActivity()).load(eventDefinition.getEvent_logo()).into(binding.ivEventLogo);
                } else {
                    binding.rlBtnAddEventLogo.setVisibility(View.VISIBLE);
                    binding.ivEventLogoClose.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(eventDefinition.getEvent_blob_image())) {
                    ArrayList<String> imgs = new Gson().fromJson(eventDefinition.getEvent_blob_image(), new TypeToken<ArrayList<String>>() {
                    }.getType());
                    for (int i = 0; i < imgs.size(); i++) {
                        eventImages.add(new EventImage(imgs.get(i)));
                    }
                    eventImageAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    @Override
    protected void setListeners() {
        binding.rlBtnAddEventLogo.setOnClickListener(v -> {
            imageOption = "EventLogo";
            Intent mIntent = new Intent(getActivity(), PickImageActivity.class);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 1);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
            startActivityForResult(mIntent, REQUEST_PICK_IMAGE);
        });
        binding.ivEventLogoClose.setOnClickListener(v -> {
            binding.ivEventLogo.setImageDrawable(null);
            binding.rlBtnAddEventLogo.setVisibility(View.VISIBLE);
            binding.ivEventLogoClose.setVisibility(View.GONE);
        });

        binding.tvBtnEventLocationNext.setOnClickListener(v -> {
            if (eventImages.size() != 0) eventDefinition.setEventImages(eventImages);
            boolean isMediaFrag = false;
            if (eventDefinition.getEvent_logi_type() == EVENT_TYPE_AT_LOCATION) {
                if (eventDefinition.isFree_event())
                    if (!eventDefinition.isParking_allowed() || (eventDefinition.isParking_allowed() && eventDefinition.isFree_event_parking()))
                        isMediaFrag = true;
            } else if (eventDefinition.getEvent_logi_type() == EVENT_TYPE_AT_VIRTUALLY) {
                if (eventDefinition.isFree_event()) isMediaFrag = true;
            } else if (eventDefinition.getEvent_logi_type() == EVENT_TYPE_BOTH) {
                if (eventDefinition.isFree_event())
                    if (!eventDefinition.isParking_allowed() || (eventDefinition.isParking_allowed() && eventDefinition.isFree_event_parking()))
                        isMediaFrag = true;
            }
            Fragment fragment = isMediaFrag ? new EventMediaAddEditFragment() : new EventPricingLimitFragment();
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isEdit", isEdit);
            bundle.putBoolean("isRepeat", isRepeat);
            bundle.putString("eventDefinition", new Gson().toJson(eventDefinition));
            fragment.setArguments(bundle);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, fragment);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(fragment);
            ft.commitAllowingStateLoss();
        });
    }

    @Override
    protected void initViews(View v) {
        dialog = new ProgressDialog(getActivity());
        dialog.setTitle("Loading...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        eventImageAdapter = new EventImageAdapter();
        binding.rvImage.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        binding.rvImage.setAdapter(eventImageAdapter);

        eventImages.add(new EventImage(""));
        eventImageAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PICK_IMAGE && resultCode == RESULT_OK) {
            ArrayList<String> pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                if (imageOption.equals("EventLogo")) {
                    isEventLogoChange = true;
                    binding.rlBtnAddEventLogo.setVisibility(View.GONE);
                    binding.ivEventLogoClose.setVisibility(View.VISIBLE);
                    eventDefinition.setEvent_logo(String.valueOf(pathList.get(0)));
                    Log.d("#DEBUG EventImage", "FilePath Image: " + String.valueOf(pathList.get(0)));
                    Glide.with(getActivity()).load(eventDefinition.getEvent_logo()).into(binding.ivEventLogo);
                } else if (imageOption.equals("EventImage")) {
                    for (int i = 0; i < pathList.size(); i++) {
                        eventImages.add(new EventImage(pathList.get(i)));
                    }
                    eventImageAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    private class EventImageAdapter extends RecyclerView.Adapter<EventImageAdapter.ImageHolder> {

        @NonNull
        @Override
        public ImageHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new ImageHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_add_event_image, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ImageHolder imageHolder, int i) {
            if (i == 0) {
                imageHolder.rlBtnAddEventImage.setVisibility(View.VISIBLE);
                imageHolder.ivEventImageClose.setVisibility(View.GONE);
            } else {
                imageHolder.rlBtnAddEventImage.setVisibility(View.GONE);
                imageHolder.ivEventImageClose.setVisibility(View.VISIBLE);
                Glide.with(getActivity()).load(eventImages.get(i).getPath()).into(imageHolder.ivEventImage);
            }
        }

        @Override
        public int getItemCount() {
            return eventImages.size();
        }

        public class ImageHolder extends RecyclerView.ViewHolder {
            ImageView ivEventImage, ivEventImageClose;
            RelativeLayout rlBtnAddEventImage;

            public ImageHolder(@NonNull View itemView) {
                super(itemView);
                ivEventImage = itemView.findViewById(R.id.ivEventImage);
                ivEventImageClose = itemView.findViewById(R.id.ivEventImageClose);
                rlBtnAddEventImage = itemView.findViewById(R.id.rlBtnAddEventImage);

                rlBtnAddEventImage.setOnClickListener(v -> {
                    imageOption = "EventImage";
                    Intent mIntent = new Intent(getActivity(), PickImageActivity.class);
                    mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 10);
                    mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
                    startActivityForResult(mIntent, REQUEST_PICK_IMAGE);
                });

                ivEventImageClose.setOnClickListener(v -> {
                    if (getAdapterPosition() != 1) {
                        eventImages.remove(getAdapterPosition());
                        notifyItemRemoved(getAdapterPosition());
                    }
                });
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isUpdated) {
            if (getActivity() != null) getActivity().onBackPressed();
        }
    }
}