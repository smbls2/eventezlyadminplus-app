package com.softmeasures.eventezlyadminplus.frament.user_profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;

import com.softmeasures.eventezlyadminplus.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import static android.content.Context.MODE_PRIVATE;

public class Frg_User_edit_profile extends Fragment {

    EditText edit_first_name, edit_last_name, edit_phone, edit_Mobile, edit_email,
            edit_address, edit_display_name, edit_City, edit_State, edit_Zip, edit_Country;
    Button btn_done;
    String first_name, last_name, phone, mobile, email, address, display_name, city, State, Zip, Country;
    RelativeLayout rl_progressbar;
    private Done_click listener;

    String message = "", g_FirstName = "", g_LastName = "", g_Phone = "", g_Mobile = "",
            g_Address = "", g_FullName = "", g_Email = "",
            g_City = "", g_State = "", g_Zip = "", g_Country = "";

    public interface Done_click {
        public void OnItemClick();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.frg_user_edit_profile, container, false);

        edit_display_name = (EditText) view.findViewById(R.id.edit_display_name);
        edit_first_name = (EditText) view.findViewById(R.id.edit_first_name);
        edit_last_name = (EditText) view.findViewById(R.id.edit_last_name);
        edit_phone = (EditText) view.findViewById(R.id.edit_phone_number);
        edit_Mobile = (EditText) view.findViewById(R.id.edit_Mobile_number);
        edit_email = (EditText) view.findViewById(R.id.edit_email_address);
        edit_address = (EditText) view.findViewById(R.id.edit_address);
        edit_City = (EditText) view.findViewById(R.id.edit_City);
        edit_State = (EditText) view.findViewById(R.id.edit_State);
        edit_Zip = (EditText) view.findViewById(R.id.edit_Zip);
        edit_Country = (EditText) view.findViewById(R.id.edit_Country);

        btn_done = (Button) view.findViewById(R.id.btn_done);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_data()) {
                    new putUserProfile().execute();
                }
                hideKeyboard(v);
            }
        });

        if (getArguments() != null) {
            g_FirstName = getArguments().getString("g_FirstName");
            g_LastName = getArguments().getString("g_LastName");
            g_Phone = getArguments().getString("g_Phone");
            g_Mobile = getArguments().getString("g_Mobile");
            g_Address = getArguments().getString("g_Address");
            g_FullName = getArguments().getString("g_FullName");
            g_Email = getArguments().getString("g_Email");
            g_City = getArguments().getString("g_City");
            g_State = getArguments().getString("g_State");
            g_Zip = getArguments().getString("g_Zip");
            g_Country = getArguments().getString("g_Country");
        }


        edit_display_name.setText(g_FullName);
        edit_first_name.setText(g_FirstName);
        edit_last_name.setText(g_LastName);
        edit_phone.setText(g_Phone);
        edit_Mobile.setText(g_Mobile);
        edit_email.setText(g_Email);
        edit_address.setText(g_Address);
        edit_City.setText(g_City);
        edit_State.setText(g_State);
        edit_Zip.setText(g_Zip);
        edit_Country.setText(g_Country);
        edit_email.setEnabled(false);

        return view;
    }

    public boolean check_data() {
        email = edit_email.getText().toString().trim();
        display_name = edit_display_name.getText().toString().trim();
        first_name = edit_first_name.getText().toString().trim();
        last_name = edit_last_name.getText().toString().trim();
        phone = edit_phone.getText().toString().trim();
        mobile = edit_Mobile.getText().toString().trim();
        address = edit_address.getText().toString().trim();
        city = edit_City.getText().toString().trim();
        State = edit_State.getText().toString().trim();
        Zip = edit_Zip.getText().toString().trim();
        Country = edit_Country.getText().toString().trim();

        boolean is_check = false;
        if (first_name.equals("")) {
            is_check = false;
            show_Alert("Please enter first name!");
        } else if (last_name.equals("")) {
            is_check = false;
            show_Alert("Please enter last name!");
        } else if (phone.equals("")) {
            is_check = false;
            show_Alert("Please enter Phone No#!");
        } else if (mobile.equals("")) {
            is_check = false;
            show_Alert("Please enter Mobile No#!");
        } else if (display_name.equals("")) {
            is_check = false;
            show_Alert("Please enter Display Name!");
        } else if (address.equals("")) {
            is_check = false;
            show_Alert("Please enter Address!");
        } else if (city.equals("")) {
            is_check = false;
            show_Alert("Please enter City!");
        } else if (State.equals("")) {
            is_check = false;
            show_Alert("Please enter State!");
        } else if (Zip.equals("")) {
            is_check = false;
            show_Alert("Please enter Zip!");
        } else if (Country.equals("")) {
            is_check = false;
            show_Alert("Please enter Country!");
        } else {
            is_check = true;
        }

        return is_check;
    }

    public void hideKeyboard(View view) {
        Log.e("outtouch1", "yes");
        try {
            if (getActivity() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
        }

    }

    public void show_Alert(String mess) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Error");
        alertDialog.setMessage(mess);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        if (getActivity() != null) {
            alertDialog.show();
        }
    }


    private class Profile_update extends AsyncTask<String, String, String> {
        String login = "user/profile";
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String ins_id = logindeatl.getString("id", "null");
        String session_id = logindeatl.getString("session_id", "");
        String message = "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("first_name", first_name);
            jsonValues.put("last_name", last_name);
            jsonValues.put("email", email);
            jsonValues.put("phone", phone);
            //jsonValues.put("lphone", phone);

            //jsonValues.put("address", edit_address.getText().toString());
           /* jsonValues.put("security_question", edit_question.getText().toString());
            jsonValues.put("security_answer", edit_address.getText().toString());*/

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + login;
            Log.e("login_url", url);
            Log.e("login_param", String.valueOf(json));

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            post.setHeader("x-dreamfactory-session-token", session_id);
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject jso = new JSONObject(responseStr);
                    if (jso.has("success")) {
                        message = jso.getString("success");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            rl_progressbar.setVisibility(View.GONE);
            if (message.equals("")) {
                show_Alert("Try Again");
            } else if (message.equals("false")) {
                show_Alert("Please try again");
            } else {
                //listener.OnItemClick();
                getActivity().onBackPressed();
            }
        }
    }

    public void registerForListener(Done_click listener) {
        this.listener = listener;
    }


    private class putUserProfile extends AsyncTask<String, String, String> {
        String mess, res, res_id;
        String login = "_table/user_profile";

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String ins_id = logindeatl.getString("id", "null");
        String emial_ID = logindeatl.getString("email", "");

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            String currentdate = sdf.format(new Date());
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", ins_id);
            jsonValues.put("email", emial_ID);
            jsonValues.put("fname", first_name);
            jsonValues.put("lname", last_name);
            jsonValues.put("full_name", display_name);
            jsonValues.put("phone", phone);
            jsonValues.put("mobile", mobile);
            jsonValues.put("lphone", phone);
            jsonValues.put("address", address);
            jsonValues.put("city", city);
            jsonValues.put("state", State);
            jsonValues.put("zip", Zip);
            jsonValues.put("country", Country);
            jsonValues.put("full_address", address + " " + city + " " + State + " " + Zip + " " + Country);

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + login;
            Log.e("get_user_url", url);
            Log.e("get_user", String.valueOf(json));
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {

                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    res = String.valueOf(response.getStatusLine());
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject jso = new JSONObject(responseStr);
                    Log.e("res ", " : " + res.toString());
                    if (res.equals("HTTP/1.1 500 Internal Server Error") || res.equals("HTTP/1.1 400 Bad Request")) {
                        JSONObject towers = jso.getJSONObject("error");
                        mess = towers.getString("message");
                    } else if (res.equals("HTTP/1.1 200 OK")) {
                        JSONArray array = jso.getJSONArray("resource");
                        Log.e("array ", " : " + array);
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject c = array.getJSONObject(i);
                            res_id = c.getString("id");
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            rl_progressbar.setVisibility(View.GONE);
            if (res.equals("HTTP/1.1 500 Internal Server Error") || res.equals("HTTP/1.1 400 Bad Request")) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Error");
                alertDialog.setMessage(mess);
                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            } else if (res.equals("HTTP/1.1 200 OK")) {
                //new putUserProfile_Archive().execute();
                getActivity().onBackPressed();
            }
        }
    }


    private class putUserProfile_Archive extends AsyncTask<String, String, String> {
        String mess, res, res_id;
        String login = "_table/user_profile_archive";

        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String ins_id = logindeatl.getString("id", "null");
        String emial_ID = logindeatl.getString("email", "");

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            String currentdate = sdf.format(new Date());
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("permit_sbscription_id", ins_id);
            jsonValues.put("id", ins_id);
            jsonValues.put("email", emial_ID);
            jsonValues.put("fname", first_name);
            jsonValues.put("lname", last_name);
            jsonValues.put("full_name", display_name);
            jsonValues.put("phone", phone);
            jsonValues.put("mobile", mobile);
            jsonValues.put("lphone", phone);
            jsonValues.put("address", address);
            jsonValues.put("city", city);
            jsonValues.put("state", State);
            jsonValues.put("zip", Zip);
            jsonValues.put("country", Country);
            jsonValues.put("full_address", address + " " + city + " " + State + " " + Zip + " " + Country);

            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + login;
            Log.e("get_user_url", url);
            Log.e("get_user", String.valueOf(json));
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {

                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    res = String.valueOf(response.getStatusLine());
                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONObject jso = new JSONObject(responseStr);
                    Log.e("res ", " : " + res.toString());
                    if (res.equals("HTTP/1.1 500 Internal Server Error") || res.equals("HTTP/1.1 400 Bad Request")) {
                        JSONObject towers = jso.getJSONObject("error");
                        mess = towers.getString("message");
                    } else if (res.equals("HTTP/1.1 200 OK")) {
                        JSONArray array = jso.getJSONArray("resource");
                        Log.e("array ", " : " + array);
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject c = array.getJSONObject(i);
                            res_id = c.getString("id");
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            rl_progressbar.setVisibility(View.GONE);
            if (res.equals("HTTP/1.1 500 Internal Server Error") || res.equals("HTTP/1.1 400 Bad Request")) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Error");
                alertDialog.setMessage(mess);
                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            } else if (res.equals("HTTP/1.1 200 OK")) {
                getActivity().onBackPressed();
            }
        }
    }
}