package com.softmeasures.eventezlyadminplus.frament.locations;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.EventLocationAddEditFragment;
import com.softmeasures.eventezlyadminplus.frament.event.AddEdit.SessionLocationAddEditFragment;
import com.softmeasures.eventezlyadminplus.frament.event.EventAddMainViewFragment;
import com.softmeasures.eventezlyadminplus.frament.event.session.SessionAddMainViewFragment;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.EventAddress;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.latitude;
import static com.softmeasures.eventezlyadminplus.activity.vchome.longitude;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step1.private_title;

public class SelectAddressFragment extends Fragment {
    TextView txt_cancel, txt_save_address;
    AutoCompleteTextView autoCompleteTextView_select_location;
    private static final String LOG_TAG = "ExampleApp", PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place", TYPE_AUTOCOMPLETE = "/autocomplete", OUT_JSON = "/json";
    ArrayList<item> searchplace = new ArrayList<>();
    private static final String API_KEY = "AIzaSyBPQK7umomGsX2lJ-23BnK2Gh8-Q6yekZU";
    boolean selected_map_isloaded = false;
    double latitude_selected = 0.0, longitude_selected = 0.0;
    RelativeLayout rl_progressbar;
    String filterAddress;
    TextView txt_select_address;
    private double lat = 0, lang = 0;
    private Boolean isEvent = false;
    private boolean isSession = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.selected_location_map_view, container, false);
        if (getArguments() != null) {
            isEvent = getArguments().getBoolean("isEvent");
            isSession = getArguments().getBoolean("isSession");
        }
        txt_cancel = (TextView) view.findViewById(R.id.txt_cancel);
        txt_save_address = (TextView) view.findViewById(R.id.txt_add_save);
        autoCompleteTextView_select_location = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteTextView1);
        autoCompleteTextView_select_location.setAdapter(new CustomAutoplace(getActivity(), getResources()));
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        txt_select_address = (TextView) view.findViewById(R.id.txt_select_address);
        RelativeLayout rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        txt_save_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* txt_lang.setText(lang);
                txt_lat.setText(lat);*/
                String address = autoCompleteTextView_select_location.getText().toString();
                if (!isEvent) {
                    EditPartnerFragment.parkingPartner.setAddress(address);
                    EditPartnerFragment.binding.etLocationAddress.setText(address);
                    EditPartnerFragment.parkingPartner.setLat(lat);
                    EditPartnerFragment.parkingPartner.setLng(lang);
                    getActivity().onBackPressed();
                } else {
                    if (isSession) {
//                        SessionAddMainViewFragment.locationAddEditFragment.binding.etEventAddress.setText(address);
                        EventAddress eventAddress = new EventAddress();
                        eventAddress.setEventAddress(address);
                        eventAddress.setLat(lat);
                        eventAddress.setLng(lang);
                        SessionLocationAddEditFragment.eventAddresses.add(eventAddress);
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < SessionLocationAddEditFragment.eventAddresses.size(); i++) {
                            if (i != 0) {
                                stringBuilder.append("\n\n");
                            }
                            stringBuilder.append(SessionLocationAddEditFragment.eventAddresses.get(i).getEventAddress());
                        }
                        SessionAddMainViewFragment.locationAddEditFragment.binding.etEventAddress.setText(stringBuilder.toString());
                        if (SessionAddMainViewFragment.locationAddEditFragment.eventAddressAdapter != null) {
                            ((SessionLocationAddEditFragment.EventAddressAdapter) SessionAddMainViewFragment.
                                    locationAddEditFragment.eventAddressAdapter).notifyDataSetChanged();
                        }
                    } else {
                        EventAddress eventAddress = new EventAddress();
                        eventAddress.setEventAddress(address);
                        eventAddress.setLat(lat);
                        eventAddress.setLng(lang);
                        EventLocationAddEditFragment.eventAddresses.add(eventAddress);
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < EventLocationAddEditFragment.eventAddresses.size(); i++) {
                            if (i != 0) {
                                stringBuilder.append("\n\n");
                            }
                            stringBuilder.append(EventLocationAddEditFragment.eventAddresses.get(i).getEventAddress());
                        }
                        EventAddMainViewFragment.locationAddEditFragment.binding.etEventAddress.setText(stringBuilder.toString());
                        if (EventAddMainViewFragment.locationAddEditFragment.eventAddressAdapter != null) {
                            ((EventLocationAddEditFragment.EventAddressAdapter) EventAddMainViewFragment.
                                    locationAddEditFragment.eventAddressAdapter).notifyDataSetChanged();
                        }
                    }
                    getActivity().onBackPressed();
                }
            }
        });

        autoCompleteTextView_select_location.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String str1 = searchplace.get(position).getLoationname();
                String str = searchplace.get(position).getAddress();
                String pid = searchplace.get(position).getPlace_id();
                autoCompleteTextView_select_location.setText(str1 + "," + str);
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                selected_map_isloaded = false;
                new getplacedeatil2(pid).execute();
                //  new fetchLatLongFrom(str).execute();
            }
        });


        latitude_selected = latitude;
        longitude_selected = longitude;

        new fetchLatLongFromaddress1(String.valueOf(latitude_selected), String.valueOf(longitude_selected)).execute();

        return view;
    }

    public class CustomAutoplace extends BaseAdapter implements View.OnClickListener, Filterable {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data = new ArrayList<>();
        private LayoutInflater inflater = null;

        public CustomAutoplace(Activity a, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;

            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.layout, null);
                /****** View Holder Object to contain tabitem.xml file elements ******/
                holder = new ViewHolder();

                holder.txt_commerical_name = (TextView) vi.findViewById(R.id.txt_name);
                holder.txt_deatil = (TextView) vi.findViewById(R.id.address);
                /************  Set holder with LayoutInflater ************/
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }
            if (data.size() <= 0) {


            } else {

                tempValues = null;
                tempValues = (item) data.get(position);
                holder.txt_commerical_name.setText(tempValues.getLoationname());
                holder.txt_deatil.setText(tempValues.getAddress());
                int k = 0;


            }

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        data = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults

                        filterResults.values = data;
                        filterResults.count = data.size();
                        searchplace.clear();
                        searchplace.addAll(data);
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;

        }


        public class ViewHolder {

            public TextView txt_commerical_name, txt_deatil, txt_pricing;
            RelativeLayout rldata;
        }
    }

    public static ArrayList<item> autocomplete(String input) {
        ArrayList<item> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            //sb.append("&components=country:gr");
            input = input.replaceAll(" ", "%20");
            ;
            sb.append("&input=" + input);

            URL url = new URL(sb.toString());

            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<item>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                String jj = predsJsonArray.getJSONObject(i).getString("structured_formatting");
                JSONObject jjj = new JSONObject(jj);
                String main_address = jjj.getString("main_text");
                // System.out.println("================================"+main_address+"============================");
                String cghcbdc = predsJsonArray.getJSONObject(i).getString("description");
                String place_id = predsJsonArray.getJSONObject(i).getString("place_id");
                System.out.println("==============================place id==" + place_id + "============================");
                System.out.println("================================" + place_id + "============================");

                item ii = new item();
                ii.setLoationname(main_address);
                ii.setAddress(cghcbdc);
                ii.setPlace_id(place_id);
                resultList.add(ii);


                //resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    public class getplacedeatil2 extends AsyncTask<String, String, String> {

        JSONObject json, json1;
        String pid, status;


        public getplacedeatil2(String places_id) {
            this.pid = places_id;

        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url1 = "";
            try {
                url1 = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + URLEncoder.encode(pid, "utf-8") + "&key=AIzaSyBPQK7umomGsX2lJ-23BnK2Gh8-Q6yekZU";
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.e("placeddeatil", url1);
            DefaultHttpClient client1 = new DefaultHttpClient();
            HttpGet post1 = new HttpGet(url1);
            post1.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post1.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));


            HttpResponse response1 = null;
            try {

                response1 = client1.execute(post1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response1 != null) {
                try {

                    HttpEntity resEntity1 = response1.getEntity();
                    String responseStr1 = null;
                    responseStr1 = EntityUtils.toString(resEntity1).trim();
                    json1 = new JSONObject(responseStr1);
                    status = json1.getString("status");
                    JSONObject google = json1.getJSONObject("result");
                    String geo = google.getString("geometry");
                    JSONObject geometry = new JSONObject(geo);
                    String location = geometry.getString("location");
                    JSONObject lant = new JSONObject(location);
                    lat = Double.parseDouble(lant.getString("lat"));
                    lang = Double.parseDouble(lant.getString("lng"));

                    latitude_selected = Double.valueOf(lat);
                    longitude_selected = Double.valueOf(lang);


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json1 != null) {
                if (status.equals("OK")) {
                    selected_location_map();
                } else {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
            super.onPostExecute(s);

        }
    }

    //seelected map from private parking map view
    public void selected_location_map() {
        SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map1);
        LatLng position = new LatLng(latitude_selected, longitude_selected);
        fm.getMapAsync(googleMap1 -> {
            GoogleMap googleMap = googleMap1;
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            googleMap.setMyLocationEnabled(true);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));

            googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    LatLng center = cameraPosition.target;
                    double laa = center.latitude;
                    double l = center.longitude;
                    filterAddress = "";
                    if (selected_map_isloaded) {
                        new fetchLatLongFromaddress(String.valueOf(laa), String.valueOf(l)).execute();
                    }

                }
            });
            googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    selected_map_isloaded = true;
                }
            });
        });
    }

    // fetch lat and lang form particulr address..............
    public class fetchLatLongFromaddress extends AsyncTask<Void, Void, StringBuilder> {

        String lat1, lang1;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        public fetchLatLongFromaddress(String lat, String lang) {
            super();
            this.lat1 = lat;
            this.lang1 = lang;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {

                //  new getsearchotherlocation().execute();
                //
                // ();

                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat1 + "," + lang1 + "&sensor=true&key=AIzaSyBPQK7umomGsX2lJ-23BnK2Gh8-Q6yekZU";

                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
                rl_progressbar.setVisibility(View.GONE);
                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");

                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray
                            .getJSONObject(0);
                    JSONArray title_array = before_geometry_jsonObj.getJSONArray("address_components");
                    JSONObject json_title = title_array.getJSONObject(0);
                    String addgtr = before_geometry_jsonObj.getString("formatted_address");

                    private_title = json_title.getString("long_name");
                    JSONObject geometry_jsonObj = before_geometry_jsonObj
                            .getJSONObject("geometry");

                    JSONObject location_jsonObj = geometry_jsonObj
                            .getJSONObject("location");

                    lat = Double.parseDouble(location_jsonObj.getString("lat"));
                    double law = Double.valueOf(lat);

                    lang = Double.parseDouble(location_jsonObj.getString("lng"));
                    double lng = Double.valueOf(lang);
                    txt_select_address.setText(addgtr);
                    autoCompleteTextView_select_location.setText(addgtr);
                    String final_loc_code = "";
                    try {
                        String loca_code1 = addgtr.substring(0, addgtr.indexOf(","));
                        String loc = addgtr.substring(addgtr.indexOf(",") + 1);
                        loca_code1 = loca_code1.replace(" ", "");
                        loc = loc.replace(" ", "");
                        String upToNCharacters = loc.substring(0, Math.min(loc.length(), 2));
//                        Private_location_code = loca_code1 + upToNCharacters;

                    } catch (Exception e) {
//                        Private_location_code = private_title;

                    }
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }


    // fetch lat and lang form particulr address..............
    public class fetchLatLongFromaddress1 extends AsyncTask<Void, Void, StringBuilder> {

        String lat1, lang1;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        public fetchLatLongFromaddress1(String lat, String lang) {
            super();
            this.lat1 = lat;
            this.lang1 = lang;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {

                //  new getsearchotherlocation().execute();
                //
                // ();

                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat1 + "," + lang1
                        + "&key=AIzaSyBPQK7umomGsX2lJ-23BnK2Gh8-Q6yekZU";

                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
                rl_progressbar.setVisibility(View.GONE);

                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");

                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray
                            .getJSONObject(0);
                    JSONArray title_array = before_geometry_jsonObj.getJSONArray("address_components");
                    JSONObject json_title = title_array.getJSONObject(0);
                    String addgtr = before_geometry_jsonObj.getString("formatted_address");

                    private_title = json_title.getString("long_name");
                    JSONObject geometry_jsonObj = before_geometry_jsonObj
                            .getJSONObject("geometry");

                    JSONObject location_jsonObj = geometry_jsonObj
                            .getJSONObject("location");

                    lat = Double.parseDouble(location_jsonObj.getString("lat"));

                    lang = Double.parseDouble(location_jsonObj.getString("lng"));
                    txt_select_address.setText(addgtr);
                    String final_loc_code = "";
                    try {
                        String loca_code1 = addgtr.substring(0, addgtr.indexOf(","));
                        String loc = addgtr.substring(addgtr.indexOf(",") + 1);
                        loca_code1 = loca_code1.replace(" ", "");
                        loc = loc.replace(" ", "");
                        String upToNCharacters = loc.substring(0, Math.min(loc.length(), 2));
//                        Private_location_code = loca_code1 + upToNCharacters;

                    } catch (Exception e) {
//                        Private_location_code = private_title;
                    }

                    autoCompleteTextView_select_location.setText(addgtr);
                    autoCompleteTextView_select_location.dismissDropDown();
                    selected_location_map();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }
}
