package com.softmeasures.eventezlyadminplus.frament;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;

import java.util.List;
import java.util.Locale;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class frg_contact extends Fragment implements vehicle_add.vehivcle_add_Click_listener {

    EditText edit_mobile, edit_address, edit_city, edit, edit_zip_code;
    Spinner sp_state;
    String state_cont;
    String statearray[] = {"Enter State", "All states", "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"};
    ConnectionDetector cd;
    double latitude, longitude;
    Geocoder geocoder;
    List<Address> addresses;
    String Email = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contact_info, container, false);
        edit_address = (EditText) view.findViewById(R.id.edit_address);
        edit_mobile = (EditText) view.findViewById(R.id.edit_mobile);
        edit_city = (EditText) view.findViewById(R.id.edit_city);
        edit_zip_code = (EditText) view.findViewById(R.id.edit_zipcode);
        sp_state = (Spinner) view.findViewById(R.id.sp_con_state);
        cd = new ConnectionDetector(getContext());


        Button txt_continue = (Button) view.findViewById(R.id.text_continue);
        RelativeLayout rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(getActivity(), R.layout.spinner, R.id.textcity, statearray);
        sp_state.setAdapter(spinnerArrayAdapter1);
        sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                state_cont = (String) sp_state.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        txt_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mobile = edit_mobile.getText().toString();
                String add = edit_address.getText().toString();
                String city = edit_city.getText().toString();
                String zip = edit_zip_code.getText().toString();

               /* if (!mobile.equals("") && !add.equals("") && !city.equals("") && !zip.equals("") && !state_cont.equals("Enter State")) {
                    Bundle b=new Bundle();
                    b.putString("direct_add","yes");
                    vehicle_add pay = new vehicle_add();
                    pay.setArguments(b);
                    pay.registerForListener(frg_contact.this);
                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Information Missing");
                    alertDialog.setMessage("Please Fill through all Fields,in order to continue.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }*/

                if (check_data()) {
                    Bundle b = new Bundle();
                    b.putString("direct_add", "yes");
                    vehicle_add pay = new vehicle_add();
                    pay.setArguments(b);
                    pay.registerForListener(frg_contact.this);
                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                }
            }
        });


        if (cd.isConnectingToInternet()) {
            GPSTracker tracker = new GPSTracker(getActivity().getApplicationContext());
            if (tracker.canGetLocation()) {
                try {
                    latitude = tracker.getLatitude();
                    longitude = tracker.getLongitude();

                    geocoder = new Geocoder(getContext(), Locale.getDefault());
                    addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();
                    edit_city.setText(city);
                    edit_zip_code.setText(postalCode);
                    edit_address.setText(knownName);
                    String country_Short = country.substring(0, 2);
                    for (int i = 0; i < statearray.length; i++) {
                        if (country_Short.equalsIgnoreCase(statearray[i])) {
                            sp_state.setSelection(i);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Failed to fetch user location!");
                alertDialog.setMessage("Please enable user location for app, location is required for app to work properly");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });
                alertDialog.show();
            }

        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();

        }

        return view;
    }

    public boolean check_data() {
        boolean is_ckeck = false;
        String mobile = edit_mobile.getText().toString();
        String add = edit_address.getText().toString();
        String city = edit_city.getText().toString();
        String zip = edit_zip_code.getText().toString();
        if (mobile.equals("")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Information Missing");
            alertDialog.setMessage("Please Enter Mobile Number!");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();
            is_ckeck = false;
        } else if (mobile.length() <= 9) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Information Missing");
            alertDialog.setMessage("Please enter minimum 10 digit mobile number!");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();

            return false;
        } else if (add.equals("")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Information Missing");
            alertDialog.setMessage("Please Enter Address!");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();
            is_ckeck = false;
        } else if (city.equals("")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Information Missing");
            alertDialog.setMessage("Please Enter City Name!");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();
            is_ckeck = false;
        } else if (state_cont.equals("Enter State")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Information Missing");
            alertDialog.setMessage("Please Select State Name!");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();
            is_ckeck = false;
        } else if (zip.equals("")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Information Missing");
            alertDialog.setMessage("Please Enter ZIP Code!");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();
            is_ckeck = false;
        } else if (zip.length() <= 4) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Information Missing");
            alertDialog.setMessage("Please Enter Valid ZIP Code!");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();
            is_ckeck = false;
        } else {
            is_ckeck = true;
        }
        return is_ckeck;
    }

    @Override
    public void Onitem_save() {
        ((vchome) getActivity()).hide();
       /* FindParking pay = new FindParking();
        fragmentStack.clear();
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.My_Container_1_ID, pay);
        fragmentStack.push(pay);
        ft.commitAllowingStateLoss();*/
    }

    @Override
    public void Onitem_delete() {

    }

}
