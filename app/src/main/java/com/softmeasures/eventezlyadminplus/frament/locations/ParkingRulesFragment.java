package com.softmeasures.eventezlyadminplus.frament.locations;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragParkingRulesBinding;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.ParkingPartner;
import com.softmeasures.eventezlyadminplus.models.ParkingRule;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class ParkingRulesFragment extends BaseFragment {

    FragParkingRulesBinding binding;
    private ArrayList<ParkingRule> parkingRules = new ArrayList<>();
    private RulesAdapter rulesAdapter;

    private String type = "";

    private ParkingPartner parkingPartner;
    public static boolean isUpdated = false;
    private boolean isEventRule = false;
    private EventDefinition eventDefinition;
    private item location;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            isEventRule = getArguments().getBoolean("isEventRule", false);
            type = getArguments().getString("type");
            parkingPartner = new Gson().fromJson(getArguments().getString("selectedSpot"), ParkingPartner.class);
            eventDefinition = new Gson().fromJson(getArguments().getString("eventDefinition"), EventDefinition.class);
            location = new Gson().fromJson(getArguments().getString("location"), item.class);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_parking_rules, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        fetchParkingRules();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isUpdated) {
            isUpdated = false;
            fetchParkingRules();
        }
    }

    private void fetchParkingRules() {
        new getParkingRules().execute();
    }

    public class getParkingRules extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String parkingurl = "_table/township_parking_rules";

        @Override
        protected void onPreExecute() {
            if (parkingPartner != null) {
                if (isEventRule) {
//                    parkingurl = "_table/event_parking_rules?filter=event_id=" + eventDefinition.getId();
                    parkingurl = "_table/event_parking_rules?filter=(event_id%3D'" + eventDefinition.getEvent_id() + "')%20AND%20(location_id%3D'" + location.getLocation_id() + "')";
                } else {
                    if (type.equals("Township")) {
                        parkingurl = "_table/township_parking_rules?filter=location_code=" + parkingPartner.getLocation_code();
                    } else if (type.equals("Commercial")) {
                        parkingurl = "_table/google_parking_rules?filter=location_code=" + parkingPartner.getLocation_code();
                    } else if (type.equals("Other")) {
                        parkingurl = "_table/other_parking_rules?filter=location_code=" + parkingPartner.getLocation_code();
                    }
                }
            }
            parkingRules.clear();
            rulesAdapter.notifyDataSetChanged();
            binding.rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        ParkingRule parkingRule = new ParkingRule();
                        String locationcode1 = c.getString("location_code");
                        String locationName = c.getString("location_name");
                        String id = c.getString("id");
                        String time_rule = c.getString("time_rule");
                        String day_rule = c.getString("day_type");
                        String max_time = c.getString("max_duration");
                        String custom_notice = c.getString("custom_notice");
                        String active = c.getString("active");
                        String pricing = c.getString("pricing");
                        String pricing_duration = c.getString("pricing_duration");
                        String start_time = c.getString("start_time");
                        String end_time = c.getString("end_time");
                        String this_day = c.getString("day_type").toLowerCase();
                        String max_hours = c.getString("duration_unit");
                        String start_hour = c.getString("start_hour");
                        String end_hour = c.getString("end_hour");

                        parkingRule.setId(Integer.parseInt(id));
                        parkingRule.setLocation_code(locationcode1);
                        parkingRule.setTime_rule(time_rule);
                        parkingRule.setDay_type(day_rule);
                        try {
                            parkingRule.setMax_duration(Integer.parseInt(max_time));
                        }catch (Exception e)
                        {

                        }
                        parkingRule.setCustom_notice(custom_notice);
                        try {
                            parkingRule.setActive(Integer.parseInt(active));
                        }catch (Exception e)
                        {

                        }
                        parkingRule.setPricing(pricing);
                        parkingRule.setPricing_duration(pricing_duration);
                        parkingRule.setStart_time(start_time);
                        parkingRule.setEnd_time(end_time);
                        parkingRule.setDay_type(this_day);
                        parkingRule.setDuration_unit(max_hours);
                        parkingRule.setStart_hour(start_hour);
                        parkingRule.setEnd_hour(end_hour);
                        parkingRule.setLocation_code(locationcode1);
                        parkingRule.setLocation_name(locationName);

                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            parkingRule.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }

                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            parkingRule.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }

                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            parkingRule.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }

                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            parkingRule.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }

                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            parkingRule.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }

                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            parkingRule.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            parkingRule.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }

                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            parkingRule.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }

                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            parkingRule.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }

                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            parkingRule.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }

                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            parkingRule.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }

                        if (c.has("Reservation_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_Fee"))
                                && !c.getString("Reservation_PostPayment_Fee").equals("null")) {
                            parkingRule.setReservation_PostPayment_Fee(c.getDouble("Reservation_PostPayment_Fee"));
                        }

                        if (c.has("ParkNow_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_Fee"))
                                && !c.getString("ParkNow_PostPayment_Fee").equals("null")) {
                            parkingRule.setParkNow_PostPayment_Fee(c.getDouble("ParkNow_PostPayment_Fee"));
                        }

                        if (c.has("number_of_reservable_spots")
                                && !TextUtils.isEmpty(c.getString("number_of_reservable_spots"))
                                && !c.getString("number_of_reservable_spots").equals("null")) {
                            parkingRule.setNumber_of_reservable_spots(c.getInt("number_of_reservable_spots"));
                        }

                        if (isEventRule) {
                            if (c.has("event_id")
                                    && !TextUtils.isEmpty(c.getString("event_id"))
                                    && !c.getString("event_id").equals("null")) {
                                parkingRule.setEvent_id(c.getInt("event_id"));
                            }
                            if (c.has("event_code")
                                    && !TextUtils.isEmpty(c.getString("event_code"))
                                    && !c.getString("event_code").equals("null")) {
                                parkingRule.setEvent_code(c.getString("event_code"));
                            }
                            if (c.has("event_name")
                                    && !TextUtils.isEmpty(c.getString("event_name"))
                                    && !c.getString("event_name").equals("null")) {
                                parkingRule.setEvent_name(c.getString("event_name"));
                            }

                            if (c.has("event_specific_date")
                                    && !TextUtils.isEmpty(c.getString("event_specific_date"))
                                    && !c.getString("event_specific_date").equals("null")) {
                                parkingRule.setEvent_specific_date(c.getString("event_specific_date"));
                            }
                            if (c.has("event_specific_time")
                                    && !TextUtils.isEmpty(c.getString("event_specific_time"))
                                    && !c.getString("event_specific_time").equals("null")) {
                                parkingRule.setEvent_specific_time(c.getString("event_specific_time"));
                            }
                            if (c.has("event_multi_dates")
                                    && !TextUtils.isEmpty(c.getString("event_multi_dates"))
                                    && !c.getString("event_multi_dates").equals("null")) {
                                parkingRule.setEvent_multi_dates(c.getString("event_multi_dates"));
                            }
                            if (c.has("event_multi_timings")
                                    && !TextUtils.isEmpty(c.getString("event_multi_timings"))
                                    && !c.getString("event_multi_timings").equals("null")) {
                                parkingRule.setEvent_multi_timings(c.getString("event_multi_timings"));
                            }
                            if (c.has("event_dt_short_info")
                                    && !TextUtils.isEmpty(c.getString("event_dt_short_info"))
                                    && !c.getString("event_dt_short_info").equals("null")) {
                                parkingRule.setEvent_dt_short_info(c.getString("event_dt_short_info"));
                            }
                            if (c.has("multi_day")
                                    && !TextUtils.isEmpty(c.getString("multi_day"))
                                    && !c.getString("multi_day").equals("null")) {
                                parkingRule.setMulti_day(c.getBoolean("multi_day"));
                            }
                            if (c.has("multi_day_reserve_allowed")
                                    && !TextUtils.isEmpty(c.getString("multi_day_reserve_allowed"))
                                    && !c.getString("multi_day_reserve_allowed").equals("null")) {
                                parkingRule.setMulti_day_reserve_allowed(c.getBoolean("multi_day_reserve_allowed"));
                            }
                            if (c.has("multi_day_parking_allowed")
                                    && !TextUtils.isEmpty(c.getString("multi_day_parking_allowed"))
                                    && !c.getString("multi_day_parking_allowed").equals("null")) {
                                parkingRule.setMulti_day_parking_allowed(c.getBoolean("multi_day_parking_allowed"));
                            }
                            if (c.has("twp_id")
                                    && !TextUtils.isEmpty(c.getString("twp_id"))
                                    && !c.getString("twp_id").equals("null")) {
                                parkingRule.setTwp_id(c.getInt("twp_id"));
                            }
                            if (c.has("township_code")
                                    && !TextUtils.isEmpty(c.getString("township_code"))
                                    && !c.getString("township_code").equals("null")) {
                                parkingRule.setTownship_code(c.getString("township_code"));
                            }
                        }

                        parkingRules.add(parkingRule);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "    getparkingrules:  " + e.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "    getparkingrules:  " + e.getMessage());
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs;
            if (getActivity() != null) {
                binding.rlProgressbar.setVisibility(View.GONE);
                if (parkingRules.size() > 0) {
                    binding.tvError.setVisibility(View.GONE);
                    rulesAdapter.notifyDataSetChanged();
                } else {
                    binding.tvError.setVisibility(View.VISIBLE);
                }
            }
            super.onPostExecute(s);
        }
    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {

        binding.tvBtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Bundle bundle = new Bundle();
                if (parkingRules.size() > 0) {
                    bundle.putString("selectedRule", new Gson().toJson(parkingRules.get(0)));
                }
                bundle.putString("type", type);
                bundle.putBoolean("isEdit", false);
                bundle.putBoolean("isEventRule", isEventRule);
                if (eventDefinition != null) {
                    bundle.putString("eventDefinition", new Gson().toJson(eventDefinition));
                }
                if (location != null) {
                    bundle.putString("location", new Gson().toJson(location));
                }
                EditParkingRuleFragment pay = new EditParkingRuleFragment();
                pay.setArguments(bundle);
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

    }

    @Override
    protected void initViews(View v) {

        rulesAdapter = new RulesAdapter();
        binding.rvRules.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvRules.setAdapter(rulesAdapter);

    }

    private class RulesAdapter extends RecyclerView.Adapter<RulesAdapter.RulesHolder> {

        @NonNull
        @Override
        public RulesAdapter.RulesHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new RulesHolder(LayoutInflater.from(getActivity())
                    .inflate(R.layout.item_parking_rule, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull RulesAdapter.RulesHolder rulesHolder, int i) {
            ParkingRule parkingRule = parkingRules.get(i);
            if (parkingRule != null) {
                rulesHolder.tvLocationName.setText(parkingRule.getLocation_name());
                rulesHolder.tvRulePricing.setText(String.format("$%s/%s %s", parkingRule.getPricing(),
                        parkingRule.getPricing_duration(), parkingRule.getDuration_unit()));
                rulesHolder.tvRuleMaxParkingAllowed.setText(String.format("MAX: %s %s",
                        parkingRule.getMax_duration(), parkingRule.getDuration_unit()));
                rulesHolder.tvRuleTime.setText(String.format("OPEN: %s", parkingRule.getTime_rule()));
            }

        }

        @Override
        public int getItemCount() {
            return parkingRules.size();
        }

        class RulesHolder extends RecyclerView.ViewHolder {
            TextView tvLocationName, tvRulePricing, tvRuleMaxParkingAllowed, tvRuleTime;

            RulesHolder(@NonNull View itemView) {
                super(itemView);

                tvLocationName = itemView.findViewById(R.id.tvLocationName);
                tvRulePricing = itemView.findViewById(R.id.tvRulePricing);
                tvRuleMaxParkingAllowed = itemView.findViewById(R.id.tvRuleMaxParkingAllowed);
                tvRuleTime = itemView.findViewById(R.id.tvRuleTime);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //TODO Open edit parking rules

                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        Bundle bundle = new Bundle();
                        bundle.putString("selectedRule", new Gson().toJson(parkingRules.get(getAdapterPosition())));
                        bundle.putString("type", type);
                        bundle.putBoolean("isEventRule", isEventRule);
                        EditParkingRuleFragment pay = new EditParkingRuleFragment();
                        pay.setArguments(bundle);
                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.add(R.id.My_Container_1_ID, pay);
                        fragmentStack.lastElement().onPause();
                        ft.hide(fragmentStack.lastElement());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                    }
                });
            }
        }
    }
}
