package com.softmeasures.eventezlyadminplus.frament.reservation;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.panstreetview;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.dialogs.PaymentOptionDialog;
import com.softmeasures.eventezlyadminplus.dialogs.PaymentOptionListener;
import com.softmeasures.eventezlyadminplus.frament.f_direction;
import com.softmeasures.eventezlyadminplus.frament.timervehicleinfo;
import com.softmeasures.eventezlyadminplus.java.BounceAnimation;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.LocationLot;
import com.softmeasures.eventezlyadminplus.models.ReserveParking;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;
import com.softmeasures.eventezlyadminplus.services.NotificationPublisher;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.MODE_PRIVATE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.frament.reservation.ReservedParkingFragment.isUpdated;

public class ReservationParkingDetailsFragment extends BaseFragment implements PaymentOptionListener {

    private static final int REQUEST_CODE_PAYMENT = 321;
    private static final String TAG = "paymentQlighting";
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    private static final String CONFIG_CLIENT_ID = "AQXvOmlRwvO2AI6H3XzqTe14pp4x6VtP2skKfUkhgjRAoNj0NoT7l0ZGeaRAXkAtYqyPCmxKEUXkrT89";
    private static PayPalConfiguration config = new PayPalConfiguration().environment(CONFIG_ENVIRONMENT).clientId(CONFIG_CLIENT_ID);
    String paymentmethod = "", transection_id, wallet_id = "";

    private ConnectionDetector cd;
    private TextView tvReservationTime, tvReservationEntryTime, tvReservationExitTime,
            tvPaymentStatus, tvReservationStatus, tvAddress, tvParkingStatus,
            tvPaymentDueBy, tvPlatNo, tvAmountPaid, tvAmountDue, tvLocationName,
            tvReservationID, tvLocationCode, tvLocationTitle, tvLotId;
    private TextView tvBtnParkNow, tvBtnDone, tvBtnRenew;
    private RelativeLayout rlProgressbar, showsucessmesg;
    private NestedScrollView nestedScrollView;
    private TextView tvBtnEditReservation, tvBtnCancelReservation, tvBtnDirection, tvBtnStreetView;
    private LinearLayout llReservationOptions;

    private ReserveParking reserveParking;
    private String renew = "", marker = "", title = "";
    private SimpleDateFormat sdf;
    private String user_id = "";
    private String currentdate = "";
    private boolean isPaymentDone = false;
    private boolean isPayByWallet = false;
    private boolean isPayByPaypal = false;
    private boolean isPrePaid = false;

    private GoogleMap googleMap;
    private Double latitude = 0.0, longitude = 0.0;

    private Handler mHandler = null;
    private Runnable mAnimation;
    private ArrayList<item> myVehicles = new ArrayList<>();
    private androidx.appcompat.app.AlertDialog alertDialogMyVehicles;
    private boolean isUpdateMyVehicle = false;
    private boolean isCancelReservation = false;

    public static boolean isPostPayAllowed = false;
    private String parkNowPostPayTerms = "";

    private boolean mIsPayNow = false; //payNow = true, payLater = false
    private boolean mIsWalletPayment = true; //pay with wallet = true, make payment = false

    private double mTrFee = 0.0, mTrPercent = 0.0, mParkingSubTotal = 0.0, mParkingTotal = 0.0,
            mParkNowPostPayFees = 0.0, mReserveNowPostPayFees = 0.0;

    private String nbal, cbal = "0", ip;
    private ArrayList<item> wallbalarray = new ArrayList<>();
    private double newbal;

    private boolean mIsPaymentPaid = false;
    private double mAmountPaid = 0.0, mCancelationCharge = 0.0, mRefundableAmount = 0.0;
    private boolean mIsProcessCancel = false;
    private double mWalletBalance = 0.0;

    private ArrayList<LocationLot> locationLots = new ArrayList<>();
    private ArrayList<LocationLot> locationLotsMain = new ArrayList<>();
    private ArrayList<LocationLot> locationLotsReservable = new ArrayList<>();
    private LocationLot selectedLocationLot;
    private boolean mIsManagedLocations = false;
    private androidx.appcompat.app.AlertDialog dialogLocationLot;
    private boolean mIsReservationCanceled = false;
    private String reservationId = "";
    private long currentTime = 0, reservationEntryTime = 0, reservationExitTime = 0;

    private String reserveEntryTime = "";
    private String reserveExpiryTime = "";
    private int intHour = 0;
    private String currentDurationUnit = "";
    private long selectedEntryTime, selectedExitTime;
    private boolean isRenew = false;

    private ArrayList<ReserveParking> reserveParkings = new ArrayList<>();
    private boolean isEditReservation = false;
    private boolean isExpired = false;
    private boolean isRenewReservationClicked = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            reserveParking = new Gson().fromJson(getArguments().getString("reserveParking"), ReserveParking.class);
            if (reserveParking != null) {
                Log.e("#DEBUG", "  ReserveParking:  " + new Gson().toJson(reserveParking));
            }
            reservationId = getArguments().getString("reservationId");
        }
        cd = new ConnectionDetector(getActivity());
        SharedPreferences d = getActivity().getSharedPreferences("renew", Context.MODE_PRIVATE);
        renew = d.getString("renew", "no");
        SharedPreferences s = getActivity().getSharedPreferences("paidparkhere", Context.MODE_PRIVATE);
        marker = s.getString("mar", "null");
        title = s.getString("title", "null");
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        user_id = logindeatl.getString("id", "null");
        return inflater.inflate(R.layout.frag_reservation_parking_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mHandler = new Handler();

        initViews(view);
        updateViews();
        setListeners();

        if (!TextUtils.isEmpty(reservationId)) {
            new fetchReservationDetails().execute();
        } else {
            prepareMap();
            getCurrentLocation();
            ip = getLocalIpAddress();
            new fetchReservedParkings().execute();
        }
    }

    private class fetchReservedParkings extends AsyncTask<String, String, String> {
        JSONObject json;
        JSONArray json1;
        String reservedParkingUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rlProgressbar.setVisibility(View.VISIBLE);
            reservedParkingUrl = "_table/reserved_parkings?filter=(location_code%3D" + reserveParking.getLocation_code()
                    + ")%20AND%20(reservation_status%3DACTIVE)";
            reserveParkings.clear();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + reservedParkingUrl;
            Log.e("#DEBUG", "      fetchReserveParking:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");

                    for (int j = 0; j < jsonArray.length(); j++) {
                        ReserveParking parkingUpdate = new ReserveParking();
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            parkingUpdate.setId(object.getString("id"));
                        }

                        if (object.has("lot_row")
                                && !TextUtils.isEmpty(object.getString("lot_row"))
                                && !object.getString("lot_row").equals("null")) {
                            parkingUpdate.setLot_row(object.getString("lot_row"));
                        }
                        if (object.has("lot_number")
                                && !TextUtils.isEmpty(object.getString("lot_number"))
                                && !object.getString("lot_number").equals("null")) {
                            parkingUpdate.setLot_number(object.getString("lot_number"));
                        }
                        if (object.has("lot_id")
                                && !TextUtils.isEmpty(object.getString("lot_id"))
                                && !object.getString("lot_id").equals("null")) {
                            parkingUpdate.setLot_id(object.getString("lot_id"));
                        }
                        if (object.has("reservation_date_time")
                                && !TextUtils.isEmpty(object.getString("reservation_date_time"))
                                && !object.getString("reservation_date_time").equals("null")) {
                            parkingUpdate.setReservation_date_time(object.getString("reservation_date_time"));
                        }
                        if (object.has("reserve_entry_time")
                                && !TextUtils.isEmpty(object.getString("reserve_entry_time"))
                                && !object.getString("reserve_entry_time").equals("null")) {
                            parkingUpdate.setReserve_entry_time(object.getString("reserve_entry_time"));
                        }
                        if (object.has("reserve_expiry_time")
                                && !TextUtils.isEmpty(object.getString("reserve_expiry_time"))
                                && !object.getString("reserve_expiry_time").equals("null")) {
                            parkingUpdate.setReserve_expiry_time(object.getString("reserve_expiry_time"));
                        }
                        if (object.has("reserve_exit_date_time")
                                && !TextUtils.isEmpty(object.getString("reserve_exit_date_time"))
                                && !object.getString("reserve_exit_date_time").equals("null")) {
                            parkingUpdate.setReserve_exit_date_time(object.getString("reserve_exit_date_time"));
                        }

                        if (object.has("reservation_status")
                                && !TextUtils.isEmpty(object.getString("reservation_status"))
                                && !object.getString("reservation_status").equals("null")) {
                            parkingUpdate.setReservation_status(object.getString("reservation_status"));
                        }
                        if (object.has("user_id")
                                && !TextUtils.isEmpty(object.getString("user_id"))
                                && !object.getString("user_id").equals("null")) {
                            parkingUpdate.setUser_id(object.getString("user_id"));
                        }
                        if (object.has("plate_no")
                                && !TextUtils.isEmpty(object.getString("plate_no"))
                                && !object.getString("plate_no").equals("null")) {
                            parkingUpdate.setPlate_no(object.getString("plate_no"));
                        }
                        if (object.has("pl_state")
                                && !TextUtils.isEmpty(object.getString("pl_state"))
                                && !object.getString("pl_state").equals("null")) {
                            parkingUpdate.setPl_state(object.getString("pl_state"));
                        }
                        if (object.has("pl_country")
                                && !TextUtils.isEmpty(object.getString("pl_country"))
                                && !object.getString("pl_country").equals("null")) {
                            parkingUpdate.setPl_country(object.getString("pl_country"));
                        }


                        if (object.has("parking_status")
                                && !TextUtils.isEmpty(object.getString("parking_status"))
                                && !object.getString("parking_status").equals("null")) {
                            parkingUpdate.setParking_status(object.getString("parking_status"));
                        }
                        if (object.has("location_lot_id")
                                && !TextUtils.isEmpty(object.getString("location_lot_id"))
                                && !object.getString("location_lot_id").equals("null")) {
                            parkingUpdate.setLocation_lot_id(object.getString("location_lot_id"));
                        }

                        reserveParkings.add(parkingUpdate);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            rlProgressbar.setVisibility(View.GONE);
            Log.e("#DEBUG", "   fetchReservedParking:  onPost:  Size: " + reserveParkings.size());
            if (reserveParkings.size() != 0) {

            } else {

            }
        }
    }

    private class fetchReservationDetails extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONObject json;
        JSONArray json1;
        String reservedParkingUrl = "_table/reserved_parkings?filter=user_id%3D" + id + "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rlProgressbar.setVisibility(View.VISIBLE);
            reserveParking = new ReserveParking();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive)
                    + "_table/reserved_parkings?filter=id%3D" + reservationId + "";
            Log.e("#DEBUG", "      fetchReserveParking:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");

                    for (int j = 0; j < jsonArray.length(); j++) {
                        ReserveParking parkingUpdate = new ReserveParking();
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            parkingUpdate.setId(object.getString("id"));
                        }
                        if (object.has("company_type_id")
                                && !TextUtils.isEmpty(object.getString("company_type_id"))
                                && !object.getString("company_type_id").equals("null")) {
                            parkingUpdate.setCompany_type_id(object.getString("company_type_id"));
                        }
                        if (object.has("company_type")
                                && !TextUtils.isEmpty(object.getString("company_type"))
                                && !object.getString("company_type").equals("null")) {
                            parkingUpdate.setCompany_type(object.getString("company_type"));
                        }
                        if (object.has("manager_type_id")
                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
                                && !object.getString("manager_type_id").equals("null")) {
                            parkingUpdate.setManager_type_id(object.getString("manager_type_id"));
                        }
                        if (object.has("manager_type")
                                && !TextUtils.isEmpty(object.getString("manager_type"))
                                && !object.getString("manager_type").equals("null")) {
                            parkingUpdate.setManager_type(object.getString("manager_type"));
                        }
                        if (object.has("company_id")
                                && !TextUtils.isEmpty(object.getString("company_id"))
                                && !object.getString("company_id").equals("null")) {
                            parkingUpdate.setCompany_id(object.getString("company_id"));
                        }
                        if (object.has("parking_type")
                                && !TextUtils.isEmpty(object.getString("parking_type"))
                                && !object.getString("parking_type").equals("null")) {
                            parkingUpdate.setParking_type(object.getString("parking_type"));
                        }
                        if (object.has("twp_id")
                                && !TextUtils.isEmpty(object.getString("twp_id"))
                                && !object.getString("twp_id").equals("null")) {
                            parkingUpdate.setTwp_id(object.getString("twp_id"));
                        }
                        if (object.has("township_code")
                                && !TextUtils.isEmpty(object.getString("township_code"))
                                && !object.getString("township_code").equals("null")) {
                            parkingUpdate.setTownship_code(object.getString("township_code"));
                        }
                        if (object.has("location_id")
                                && !TextUtils.isEmpty(object.getString("location_id"))
                                && !object.getString("location_id").equals("null")) {
                            parkingUpdate.setLocation_id(object.getString("location_id"));
                        }
                        if (object.has("location_code")
                                && !TextUtils.isEmpty(object.getString("location_code"))
                                && !object.getString("location_code").equals("null")) {
                            parkingUpdate.setLocation_code(object.getString("location_code"));
                        }
                        if (object.has("location_name")
                                && !TextUtils.isEmpty(object.getString("location_name"))
                                && !object.getString("location_name").equals("null")) {
                            parkingUpdate.setLocation_name(object.getString("location_name"));
                        }
                        if (object.has("lot_row")
                                && !TextUtils.isEmpty(object.getString("lot_row"))
                                && !object.getString("lot_row").equals("null")) {
                            parkingUpdate.setLot_row(object.getString("lot_row"));
                        }
                        if (object.has("lot_number")
                                && !TextUtils.isEmpty(object.getString("lot_number"))
                                && !object.getString("lot_number").equals("null")) {
                            parkingUpdate.setLot_number(object.getString("lot_number"));
                        }
                        if (object.has("lot_id")
                                && !TextUtils.isEmpty(object.getString("lot_id"))
                                && !object.getString("lot_id").equals("null")) {
                            parkingUpdate.setLot_id(object.getString("lot_id"));
                        }
                        if (object.has("reservation_date_time")
                                && !TextUtils.isEmpty(object.getString("reservation_date_time"))
                                && !object.getString("reservation_date_time").equals("null")) {
                            parkingUpdate.setReservation_date_time(object.getString("reservation_date_time"));
                        }
                        if (object.has("reserve_entry_time")
                                && !TextUtils.isEmpty(object.getString("reserve_entry_time"))
                                && !object.getString("reserve_entry_time").equals("null")) {
                            parkingUpdate.setReserve_entry_time(object.getString("reserve_entry_time"));
                        }
                        if (object.has("reserve_expiry_time")
                                && !TextUtils.isEmpty(object.getString("reserve_expiry_time"))
                                && !object.getString("reserve_expiry_time").equals("null")) {
                            parkingUpdate.setReserve_expiry_time(object.getString("reserve_expiry_time"));
                        }
                        if (object.has("reserve_exit_date_time")
                                && !TextUtils.isEmpty(object.getString("reserve_exit_date_time"))
                                && !object.getString("reserve_exit_date_time").equals("null")) {
                            parkingUpdate.setReserve_exit_date_time(object.getString("reserve_exit_date_time"));
                        }
                        if (object.has("reserve_payment_condition")
                                && !TextUtils.isEmpty(object.getString("reserve_payment_condition"))
                                && !object.getString("reserve_payment_condition").equals("null")) {
                            parkingUpdate.setReserve_payment_condition(object.getString("reserve_payment_condition"));
                        }
                        if (object.has("reserve_payment_due_by")
                                && !TextUtils.isEmpty(object.getString("reserve_payment_due_by"))
                                && !object.getString("reserve_payment_due_by").equals("null")) {
                            parkingUpdate.setReserve_payment_due_by(object.getString("reserve_payment_due_by"));
                        }
                        if (object.has("reserve_payment_paid")
                                && !TextUtils.isEmpty(object.getString("reserve_payment_paid"))
                                && !object.getString("reserve_payment_paid").equals("null")) {
                            parkingUpdate.setReserve_payment_paid(object.getString("reserve_payment_paid"));
                        }
                        if (object.has("reservation_status")
                                && !TextUtils.isEmpty(object.getString("reservation_status"))
                                && !object.getString("reservation_status").equals("null")) {
                            parkingUpdate.setReservation_status(object.getString("reservation_status"));
                        }
                        if (object.has("entry_date_time")
                                && !TextUtils.isEmpty(object.getString("entry_date_time"))
                                && !object.getString("entry_date_time").equals("null")) {
                            parkingUpdate.setEntry_date_time(object.getString("entry_date_time"));
                        }
                        if (object.has("exit_date_time")
                                && !TextUtils.isEmpty(object.getString("exit_date_time"))
                                && !object.getString("exit_date_time").equals("null")) {
                            parkingUpdate.setExit_date_time(object.getString("exit_date_time"));
                        }
                        if (object.has("expiry_time")
                                && !TextUtils.isEmpty(object.getString("expiry_time"))
                                && !object.getString("expiry_time").equals("null")) {
                            parkingUpdate.setExpiry_time(object.getString("expiry_time"));
                        }
                        if (object.has("user_id")
                                && !TextUtils.isEmpty(object.getString("user_id"))
                                && !object.getString("user_id").equals("null")) {
                            parkingUpdate.setUser_id(object.getString("user_id"));
                        }
                        if (object.has("permit_id")
                                && !TextUtils.isEmpty(object.getString("permit_id"))
                                && !object.getString("permit_id").equals("null")) {
                            parkingUpdate.setPermit_id(object.getString("permit_id"));
                        }
                        if (object.has("subscription_id")
                                && !TextUtils.isEmpty(object.getString("subscription_id"))
                                && !object.getString("subscription_id").equals("null")) {
                            parkingUpdate.setSubscription_id(object.getString("subscription_id"));
                        }
                        if (object.has("plate_no")
                                && !TextUtils.isEmpty(object.getString("plate_no"))
                                && !object.getString("plate_no").equals("null")) {
                            parkingUpdate.setPlate_no(object.getString("plate_no"));
                        }
                        if (object.has("pl_state")
                                && !TextUtils.isEmpty(object.getString("pl_state"))
                                && !object.getString("pl_state").equals("null")) {
                            parkingUpdate.setPl_state(object.getString("pl_state"));
                        }
                        if (object.has("pl_country")
                                && !TextUtils.isEmpty(object.getString("pl_country"))
                                && !object.getString("pl_country").equals("null")) {
                            parkingUpdate.setPl_country(object.getString("pl_country"));
                        }
                        if (object.has("lat")
                                && !TextUtils.isEmpty(object.getString("lat"))
                                && !object.getString("lat").equals("null")) {
                            parkingUpdate.setLat(object.getString("lat"));
                        }
                        if (object.has("lng")
                                && !TextUtils.isEmpty(object.getString("lng"))
                                && !object.getString("lng").equals("null")) {
                            parkingUpdate.setLng(object.getString("lng"));
                        }
                        if (object.has("address1")
                                && !TextUtils.isEmpty(object.getString("address1"))
                                && !object.getString("address1").equals("null")) {
                            parkingUpdate.setAddress1(object.getString("address1"));
                        }
                        if (object.has("address2")
                                && !TextUtils.isEmpty(object.getString("address2"))
                                && !object.getString("address2").equals("null")) {
                            parkingUpdate.setAddress2(object.getString("address2"));
                        }
                        if (object.has("city")
                                && !TextUtils.isEmpty(object.getString("city"))
                                && !object.getString("city").equals("null")) {
                            parkingUpdate.setCity(object.getString("city"));
                        }
                        if (object.has("district")
                                && !TextUtils.isEmpty(object.getString("district"))
                                && !object.getString("district").equals("null")) {
                            parkingUpdate.setDistrict(object.getString("district"));
                        }
                        if (object.has("state")
                                && !TextUtils.isEmpty(object.getString("state"))
                                && !object.getString("state").equals("null")) {
                            parkingUpdate.setState(object.getString("state"));
                        }
                        if (object.has("zip")
                                && !TextUtils.isEmpty(object.getString("zip"))
                                && !object.getString("zip").equals("null")) {
                            parkingUpdate.setZip(object.getString("zip"));
                        }
                        if (object.has("country")
                                && !TextUtils.isEmpty(object.getString("country"))
                                && !object.getString("country").equals("null")) {
                            parkingUpdate.setCountry(object.getString("country"));
                        }
                        if (object.has("marker_lng")
                                && !TextUtils.isEmpty(object.getString("marker_lng"))
                                && !object.getString("marker_lng").equals("null")) {
                            parkingUpdate.setMarker_lng(object.getString("marker_lng"));
                        }
                        if (object.has("marker_lat")
                                && !TextUtils.isEmpty(object.getString("marker_lat"))
                                && !object.getString("marker_lat").equals("null")) {
                            parkingUpdate.setMarker_lat(object.getString("marker_lat"));
                        }
                        if (object.has("marker_address1")
                                && !TextUtils.isEmpty(object.getString("marker_address1"))
                                && !object.getString("marker_address1").equals("null")) {
                            parkingUpdate.setMarker_address1(object.getString("marker_address1"));
                        }
                        if (object.has("marker_address2")
                                && !TextUtils.isEmpty(object.getString("marker_address2"))
                                && !object.getString("marker_address2").equals("null")) {
                            parkingUpdate.setMarker_address2(object.getString("marker_address2"));
                        }
                        if (object.has("marker_city")
                                && !TextUtils.isEmpty(object.getString("marker_city"))
                                && !object.getString("marker_city").equals("null")) {
                            parkingUpdate.setMarker_city(object.getString("marker_city"));
                        }
                        if (object.has("marker_district")
                                && !TextUtils.isEmpty(object.getString("marker_district"))
                                && !object.getString("marker_district").equals("null")) {
                            parkingUpdate.setMarker_district(object.getString("marker_district"));
                        }
                        if (object.has("marker_state")
                                && !TextUtils.isEmpty(object.getString("marker_state"))
                                && !object.getString("marker_state").equals("null")) {
                            parkingUpdate.setMarker_state(object.getString("marker_state"));
                        }
                        if (object.has("marker_zip")
                                && !TextUtils.isEmpty(object.getString("marker_zip"))
                                && !object.getString("marker_zip").equals("null")) {
                            parkingUpdate.setMarker_zip(object.getString("marker_zip"));
                        }
                        if (object.has("marker_country")
                                && !TextUtils.isEmpty(object.getString("marker_country"))
                                && !object.getString("marker_country").equals("null")) {
                            parkingUpdate.setMarker_country(object.getString("marker_country"));
                        }
                        if (object.has("distance_to_marker")
                                && !TextUtils.isEmpty(object.getString("distance_to_marker"))
                                && !object.getString("distance_to_marker").equals("null")) {
                            parkingUpdate.setDistance_to_marker(object.getString("distance_to_marker"));
                        }
                        if (object.has("ip")
                                && !TextUtils.isEmpty(object.getString("ip"))
                                && !object.getString("ip").equals("null")) {
                            parkingUpdate.setIp(object.getString("ip"));
                        }
                        if (object.has("parking_token")
                                && !TextUtils.isEmpty(object.getString("parking_token"))
                                && !object.getString("parking_token").equals("null")) {
                            parkingUpdate.setParking_token(object.getString("parking_token"));
                        }
                        if (object.has("parking_status")
                                && !TextUtils.isEmpty(object.getString("parking_status"))
                                && !object.getString("parking_status").equals("null")) {
                            parkingUpdate.setParking_status(object.getString("parking_status"));
                        }
                        if (object.has("payment_method")
                                && !TextUtils.isEmpty(object.getString("payment_method"))
                                && !object.getString("payment_method").equals("null")) {
                            parkingUpdate.setPayment_method(object.getString("payment_method"));
                        }
                        if (object.has("parking_rate")
                                && !TextUtils.isEmpty(object.getString("parking_rate"))
                                && !object.getString("parking_rate").equals("null")) {
                            parkingUpdate.setParking_rate(object.getString("parking_rate"));
                        }
                        if (object.has("parking_units")
                                && !TextUtils.isEmpty(object.getString("parking_units"))
                                && !object.getString("parking_units").equals("null")) {
                            parkingUpdate.setParking_units(object.getString("parking_units"));
                        }
                        if (object.has("parking_qty")
                                && !TextUtils.isEmpty(object.getString("parking_qty"))
                                && !object.getString("parking_qty").equals("null")) {
                            parkingUpdate.setParking_qty(object.getString("parking_qty"));
                        }
                        if (object.has("parking_subtotal")
                                && !TextUtils.isEmpty(object.getString("parking_subtotal"))
                                && !object.getString("parking_subtotal").equals("null")) {
                            parkingUpdate.setParking_subtotal(object.getString("parking_subtotal"));
                        }
                        if (object.has("wallet_trx_id")
                                && !TextUtils.isEmpty(object.getString("wallet_trx_id"))
                                && !object.getString("wallet_trx_id").equals("null")) {
                            parkingUpdate.setWallet_trx_id(object.getString("wallet_trx_id"));
                        }
                        if (object.has("tr_percent")
                                && !TextUtils.isEmpty(object.getString("tr_percent"))
                                && !object.getString("tr_percent").equals("null")) {
                            parkingUpdate.setTr_percent(object.getString("tr_percent"));
                        }
                        if (object.has("tr_fee")
                                && !TextUtils.isEmpty(object.getString("tr_fee"))
                                && !object.getString("tr_fee").equals("null")) {
                            parkingUpdate.setTr_fee(object.getString("tr_fee"));
                        }
                        if (object.has("parking_total")
                                && !TextUtils.isEmpty(object.getString("parking_total"))
                                && !object.getString("parking_total").equals("null")) {
                            parkingUpdate.setParking_total(object.getString("parking_total"));
                        }
                        if (object.has("ipn_custom")
                                && !TextUtils.isEmpty(object.getString("ipn_custom"))
                                && !object.getString("ipn_custom").equals("null")) {
                            parkingUpdate.setIpn_custom(object.getString("ipn_custom"));
                        }
                        if (object.has("ipn_txn_id")
                                && !TextUtils.isEmpty(object.getString("ipn_txn_id"))
                                && !object.getString("ipn_txn_id").equals("null")) {
                            parkingUpdate.setIpn_txn_id(object.getString("ipn_txn_id"));
                        }
                        if (object.has("ipn_payment")
                                && !TextUtils.isEmpty(object.getString("ipn_payment"))
                                && !object.getString("ipn_payment").equals("null")) {
                            parkingUpdate.setIpn_payment(object.getString("ipn_payment"));
                        }
                        if (object.has("ipn_status")
                                && !TextUtils.isEmpty(object.getString("ipn_status"))
                                && !object.getString("ipn_status").equals("null")) {
                            parkingUpdate.setIpn_status(object.getString("ipn_status"));
                        }
                        if (object.has("ipn_address")
                                && !TextUtils.isEmpty(object.getString("ipn_address"))
                                && !object.getString("ipn_address").equals("null")) {
                            parkingUpdate.setIpn_address(object.getString("ipn_address"));
                        }
                        if (object.has("ticket_status")
                                && !TextUtils.isEmpty(object.getString("ticket_status"))
                                && !object.getString("ticket_status").equals("null")) {
                            parkingUpdate.setTicket_status(object.getString("ticket_status"));
                        }
                        if (object.has("expiry_status")
                                && !TextUtils.isEmpty(object.getString("expiry_status"))
                                && !object.getString("expiry_status").equals("null")) {
                            parkingUpdate.setExpiry_status(object.getString("expiry_status"));
                        }
                        if (object.has("notified_status")
                                && !TextUtils.isEmpty(object.getString("notified_status"))
                                && !object.getString("notified_status").equals("null")) {
                            parkingUpdate.setNotified_status(object.getString("notified_status"));
                        }
                        if (object.has("user_id_ref")
                                && !TextUtils.isEmpty(object.getString("user_id_ref"))
                                && !object.getString("user_id_ref").equals("null")) {
                            parkingUpdate.setUser_id_ref(object.getString("user_id_ref"));
                        }
                        if (object.has("mismatch")
                                && !TextUtils.isEmpty(object.getString("mismatch"))
                                && !object.getString("mismatch").equals("null")) {
                            parkingUpdate.setMismatch(object.getString("mismatch"));
                        }
                        if (object.has("payment_choice")
                                && !TextUtils.isEmpty(object.getString("payment_choice"))
                                && !object.getString("payment_choice").equals("null")) {
                            parkingUpdate.setPayment_choice(object.getString("payment_choice"));
                        }
                        if (object.has("bursar_trx_id")
                                && !TextUtils.isEmpty(object.getString("bursar_trx_id"))
                                && !object.getString("bursar_trx_id").equals("null")) {
                            parkingUpdate.setBursar_trx_id(object.getString("bursar_trx_id"));
                        }
                        if (object.has("platform")
                                && !TextUtils.isEmpty(object.getString("platform"))
                                && !object.getString("platform").equals("null")) {
                            parkingUpdate.setPlatform(object.getString("platform"));
                        }
                        if (object.has("duration_unit")
                                && !TextUtils.isEmpty(object.getString("duration_unit"))
                                && !object.getString("duration_unit").equals("null")) {
                            parkingUpdate.setDuration_unit(object.getString("duration_unit"));
                        }
                        if (object.has("max_duration")
                                && !TextUtils.isEmpty(object.getString("max_duration"))
                                && !object.getString("max_duration").equals("null")) {
                            parkingUpdate.setMax_duration(object.getString("max_duration"));
                        }
                        if (object.has("selected_duration")
                                && !TextUtils.isEmpty(object.getString("selected_duration"))
                                && !object.getString("selected_duration").equals("null")) {
                            parkingUpdate.setSelected_duration(object.getString("selected_duration"));
                        }
                        if (object.has("user_id_exit")
                                && !TextUtils.isEmpty(object.getString("user_id_exit"))
                                && !object.getString("user_id_exit").equals("null")) {
                            parkingUpdate.setUser_id_exit(object.getString("user_id_exit"));
                        }
                        if (object.has("exit_forced")
                                && !TextUtils.isEmpty(object.getString("exit_forced"))
                                && !object.getString("exit_forced").equals("null")) {
                            parkingUpdate.setExit_forced(object.getString("exit_forced"));
                        }
                        if (object.has("towed")
                                && !TextUtils.isEmpty(object.getString("towed"))
                                && !object.getString("towed").equals("null")) {
                            parkingUpdate.setTowed(object.getString("towed"));
                        }
                        if (object.has("exit_overnight")
                                && !TextUtils.isEmpty(object.getString("exit_overnight"))
                                && !object.getString("exit_overnight").equals("null")) {
                            parkingUpdate.setExit_overnight(object.getString("exit_overnight"));
                        }
                        if (object.has("modified_time")
                                && !TextUtils.isEmpty(object.getString("modified_time"))
                                && !object.getString("modified_time").equals("null")) {
                            parkingUpdate.setModified_time(object.getString("modified_time"));
                        }

                        if (object.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(object.getString("CanCancel_Reservation"))
                                && !object.getString("CanCancel_Reservation").equals("null")) {
                            parkingUpdate.setCanCancel_Reservation(object.getBoolean("CanCancel_Reservation"));
                        }

                        if (object.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(object.getString("Reservation_PostPayment_term"))
                                && !object.getString("Reservation_PostPayment_term").equals("null")) {
                            parkingUpdate.setReservation_PostPayment_term(object.getString("Reservation_PostPayment_term"));
                        }

                        if (object.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(object.getString("Cancellation_Charge"))
                                && !object.getString("Cancellation_Charge").equals("null")) {
                            parkingUpdate.setCancellation_Charge(object.getString("Cancellation_Charge"));
                        }

                        if (object.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(object.getString("Reservation_PostPayment_LateFee"))
                                && !object.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            parkingUpdate.setReservation_PostPayment_LateFee(object.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (object.has("location_lot_id")
                                && !TextUtils.isEmpty(object.getString("location_lot_id"))
                                && !object.getString("location_lot_id").equals("null")) {
                            parkingUpdate.setLocation_lot_id(object.getString("location_lot_id"));
                        }

                        reserveParking = parkingUpdate;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                rlProgressbar.setVisibility(View.GONE);
                tvBtnRenew.setVisibility(View.GONE);
                updateViews();
                prepareMap();
                getCurrentLocation();
                ip = getLocalIpAddress();
                new fetchReservedParkings().execute();
            }
        }
    }

    public void getCurrentLocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Failed to fetch user location!");
            alertDialog.setMessage("Please enabl e user location for app, location is required for app to work properly");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });

            alertDialog.show();
        }
    }

    private void prepareMap() {
        SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (fm != null) {
            fm.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap map) {
                    googleMap = map;
                    displayMapDetails();
                }
            });
        }
    }

    private void displayMapDetails() {
        final LatLng position = new LatLng(Double.parseDouble(reserveParking.getMarker_lat()),
                Double.parseDouble(reserveParking.getMarker_lng()));
        MarkerOptions options = new MarkerOptions();
        options.position(position);
        options.title(reserveParking.getReservation_status());
        options.snippet(reserveParking.getAddress1());
        options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.rsz_manages_star1));

        Marker melbourne = googleMap.addMarker(options);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 12));
        final long start = SystemClock.uptimeMillis();
        final long duration = 1500L;
        mHandler.removeCallbacks(mAnimation);
        mAnimation = new BounceAnimation(start, duration, melbourne, mHandler);
        mHandler.post(mAnimation);
    }

    @Override
    protected void updateViews() {

        if (!TextUtils.isEmpty(reservationId)) {
            tvBtnDone.setVisibility(View.VISIBLE);
        } else {
            tvBtnDone.setVisibility(View.GONE);
        }

        if (reserveParking != null) {

            if (reserveParking.getParking_qty() != null &&
                    !TextUtils.isEmpty(reserveParking.getParking_qty())) {
                try {
                    intHour = Integer.valueOf(reserveParking.getParking_qty());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }

            if (!TextUtils.isEmpty(reserveParking.getDuration_unit())) {
                currentDurationUnit = reserveParking.getDuration_unit().substring(0, 2);
            }

            if (!TextUtils.isEmpty(reserveParking.getId())) {
                tvReservationID.setText(reserveParking.getId());
            } else {
                tvReservationID.setText("");
            }

            if (!TextUtils.isEmpty(reserveParking.getReserve_payment_paid())) {
                if (reserveParking.getReserve_payment_paid().equals("UNPAID")) {
                    mIsPaymentPaid = false;
                    if (!TextUtils.isEmpty(reserveParking.getParking_total()))
                        tvAmountDue.setText(String.format("$%s", reserveParking.getParking_total()));
                    tvAmountPaid.setText("$0");
                    if (!TextUtils.isEmpty(reserveParking.getReserve_payment_condition())
                            && reserveParking.getReserve_payment_condition().equals("ENTRY")) {
                        mIsPayNow = true;
                    }
                } else {
                    mIsPaymentPaid = true;
                    if (!TextUtils.isEmpty(reserveParking.getParking_total())) {
                        mAmountPaid = Double.parseDouble(reserveParking.getParking_total());
                        Log.e("#DEBUG", "   mAmountPaid:  " + mAmountPaid);
                    }
                    if (!TextUtils.isEmpty(reserveParking.getCancellation_Charge())) {
                        mCancelationCharge = Double.parseDouble(reserveParking.getCancellation_Charge());
                        Log.e("#DEBUG", "   mCancelationCharge:  " + mCancelationCharge);
                    }
                    if (!TextUtils.isEmpty(reserveParking.getParking_total()))
                        tvAmountPaid.setText(String.format("$%s", reserveParking.getParking_total()));
                    tvAmountDue.setText("$0");
                }
            }

            if (!TextUtils.isEmpty(reserveParking.getParking_status())) {
                if (reserveParking.getParking_status().equals("ENTRY")) {
                    tvBtnParkNow.setText("PARKED");
                    tvBtnParkNow.setEnabled(false);
                    tvBtnCancelReservation.setVisibility(View.GONE);
                    tvBtnEditReservation.setVisibility(View.GONE);
                } else if (reserveParking.getParking_status().equals("EXIT")) {
                    tvBtnParkNow.setText("EXIT");
                    tvBtnParkNow.setEnabled(false);
                    tvBtnCancelReservation.setVisibility(View.GONE);
                    tvBtnEditReservation.setVisibility(View.GONE);
                } else {
                    tvBtnParkNow.setEnabled(true);
                    tvBtnParkNow.setText("PARK NOW");
                }
            } else {
                tvBtnParkNow.setEnabled(true);
                tvBtnParkNow.setText("PARK NOW");
            }

            if (!TextUtils.isEmpty(reserveParking.getReservation_date_time())) {
                tvReservationTime.setText(DateTimeUtils.parseDateTimeNew(reserveParking.getReservation_date_time()));
            } else {
                tvReservationTime.setText("");
            }
            if (!TextUtils.isEmpty(reserveParking.getReserve_entry_time())) {
                tvReservationEntryTime.setText(DateTimeUtils.parseDateTimeNew(reserveParking.getReserve_entry_time()));
                try {
                    reservationEntryTime = sdf.parse(reserveParking.getReserve_entry_time()).getTime();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                tvReservationEntryTime.setText("");
            }
            if (!TextUtils.isEmpty(reserveParking.getReserve_exit_date_time())) {
                tvReservationExitTime.setText(DateTimeUtils.parseDateTimeNew(reserveParking.getReserve_exit_date_time()));
                try {
                    reservationExitTime = sdf.parse(reserveParking.getReserve_exit_date_time()).getTime();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                tvReservationExitTime.setText("");
            }
            if (!TextUtils.isEmpty(reserveParking.getReserve_payment_paid())) {
                tvPaymentStatus.setText(reserveParking.getReserve_payment_paid());
            } else {
                tvPaymentStatus.setText("");
            }
            if (!TextUtils.isEmpty(reserveParking.getReservation_status())) {
                tvReservationStatus.setText(reserveParking.getReservation_status());
                if (reserveParking.getReservation_status().equals("CANCELLED")) {
                    tvBtnEditReservation.setVisibility(View.GONE);
                    tvBtnCancelReservation.setVisibility(View.GONE);
                    llReservationOptions.setVisibility(View.GONE);
                    tvBtnParkNow.setVisibility(View.GONE);
                } else {
                    llReservationOptions.setVisibility(View.VISIBLE);
                    if (reserveParking.isCanCancel_Reservation()) {
                        tvBtnCancelReservation.setVisibility(View.VISIBLE);
                    } else tvBtnCancelReservation.setVisibility(View.GONE);
                    tvBtnParkNow.setVisibility(View.VISIBLE);

                    if (!TextUtils.isEmpty(reserveParking.getParking_status())) {
                        if (reserveParking.getParking_status().equals("ENTRY")
                                || reserveParking.getParking_status().equals("EXIT")) {
                            llReservationOptions.setVisibility(View.GONE);
                        }
                    }
                }
            } else {
                tvReservationStatus.setText("");
            }
            if (!TextUtils.isEmpty(reserveParking.getMarker_address1())) {
                tvAddress.setText(reserveParking.getMarker_address1());
            } else {
                tvAddress.setText("");
            }
            if (!TextUtils.isEmpty(reserveParking.getLocation_name())) {
                tvLocationTitle.setText(reserveParking.getLocation_name());
            } else if (!TextUtils.isEmpty(reserveParking.getCity())) {
                tvLocationTitle.setText(reserveParking.getCity());
            } else {
                tvLocationTitle.setText("-");
            }
            if (!TextUtils.isEmpty(reserveParking.getLocation_name())) {
                tvLocationName.setText(reserveParking.getLocation_name());
            } else {
                tvLocationName.setText("-");
            }
            if (!TextUtils.isEmpty(reserveParking.getLocation_code())) {
                tvLocationCode.setText(reserveParking.getLocation_code());
            } else {
                tvLocationCode.setText("");
            }
            if (!TextUtils.isEmpty(reserveParking.getParking_status())) {
                tvParkingStatus.setText(reserveParking.getParking_status());
            } else {
                tvParkingStatus.setText("");
            }

            if (!TextUtils.isEmpty(reserveParking.getReserve_payment_due_by())) {
                tvPaymentDueBy.setText(DateTimeUtils.parseDateTimeNew(reserveParking.getReserve_payment_due_by()));
            } else {
                tvPaymentDueBy.setText("");
            }

            if (!TextUtils.isEmpty(reserveParking.getPlate_no())
                    && !TextUtils.isEmpty(reserveParking.getPl_state())) {
                tvPlatNo.setText(String.format("%s(%s)",
                        reserveParking.getPlate_no(), reserveParking.getPl_state()));
            } else {
                tvPlatNo.setText("Select Vehicle");
            }

            if (!TextUtils.isEmpty(reserveParking.getParking_total())) {
                mParkingTotal = Double.parseDouble(reserveParking.getParking_total());
            }

            if (!TextUtils.isEmpty(reserveParking.getParking_type())
                    && (reserveParking.getParking_type().contains("managed"))
                    || reserveParking.getParking_type().contains("Managed")) {
                new fetchLocationLotOccupiedData().execute();
            }

            if (!TextUtils.isEmpty(reserveParking.getLot_id())) {
                tvLotId.setText(reserveParking.getLot_id());
            } else {
                tvLotId.setText("");
            }

            Log.e("#DEBUG", "  currentTime:  " + currentTime
                    + "\n  entry:  " + reservationEntryTime
                    + "\n  exit:   " + reservationExitTime);

            if (currentTime > reservationEntryTime && currentTime < reservationExitTime) {
                //Between reservation time, reservation allowed
                //Show PARK NOW button

                tvBtnParkNow.setEnabled(true);
                tvBtnParkNow.setText("PARK NOW");

                if (!TextUtils.isEmpty(reserveParking.getParking_status())) {
                    if (reserveParking.getParking_status().equals("ENTRY")) {
                        tvBtnParkNow.setText("PARKED");
                        tvBtnParkNow.setEnabled(false);
                        tvBtnCancelReservation.setVisibility(View.GONE);
                        tvBtnEditReservation.setVisibility(View.GONE);
                    } else if (reserveParking.getParking_status().equals("EXIT")) {
                        tvBtnParkNow.setText("EXIT");
                        tvBtnParkNow.setEnabled(false);
                        tvBtnCancelReservation.setVisibility(View.GONE);
                        tvBtnEditReservation.setVisibility(View.GONE);
                    } else {
                        tvBtnParkNow.setEnabled(true);
                        tvBtnParkNow.setText("PARK NOW");
                    }
                } else {
                    tvBtnParkNow.setEnabled(true);
                    tvBtnParkNow.setText("PARK NOW");
                }

            } else if (currentTime < reservationEntryTime) {
                //Before reservation time, reservation not allowed
                //Show RESERVED
                tvBtnParkNow.setEnabled(false);
                tvBtnParkNow.setText("RESERVED");

            } else if ((reserveParking.getReservation_status().equals("EXPIRED")
                    || reserveParking.getReservation_status().equals("ACTIVE"))
                    && currentTime > reservationExitTime) {
                //Outside reservation time, reservation time expired
                //Show EXPIRED
                tvBtnRenew.setVisibility(View.VISIBLE);
                tvBtnEditReservation.setVisibility(View.GONE);
                tvBtnCancelReservation.setVisibility(View.GONE);
                isExpired = true;
                if (mIsPaymentPaid) {
                    tvBtnParkNow.setEnabled(false);
                    tvBtnParkNow.setText("EXPIRED");
                } else {
                    //Show RENEW
                    tvBtnParkNow.setEnabled(false);
                    tvBtnParkNow.setText("EXPIRED");
                    //TODO renew reservation
//                    tvBtnRenew.setVisibility(View.VISIBLE);
                }
                tvBtnParkNow.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.red_700));
            }
        }
    }

    public class fetchLocationLotOccupiedData extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String lat, lang, address, title, html, mark, parking_time, no_parking_time, notice;

        String managedlosturl = "_proc/managed_lot_status?&order=(lot_row%20ASC%2C%20lot_number%20ASC)";

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            locationLots.clear();
            locationLotsMain.clear();
            JSONObject managed = new JSONObject();


            /*  if(latitude==0.0) {*/
            try {
                managed.put("name", "in_location_code");
                managed.put("value", reserveParking.getLocation_code());

                //  student1.put("value", latitude);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            JSONArray jsonArray = new JSONArray();
            jsonArray.put(managed);

            JSONObject manageda = new JSONObject();
            try {
                manageda.put("params", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("managed_lost_url", url);
            Log.e("managed_lodt_param", String.valueOf(manageda));

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(manageda.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);

            HttpResponse response = null;

            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    TimeZone zone = TimeZone.getTimeZone("UTC");
                    sdf.setTimeZone(zone);
                    String currentdate = sdf.format(new Date());
                    Date current = null;
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        current = format.parse(currentdate);
                        System.out.println(current);
                    } catch (android.net.ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json1 = new JSONArray(responseStr);

                    for (int i = 0; i < json1.length(); i++) {
                        LocationLot item = new LocationLot();
                        JSONObject jsonObject = json1.getJSONObject(i);
                        if (jsonObject.has("id")
                                && !TextUtils.isEmpty(jsonObject.getString("id"))
                                && !jsonObject.getString("id").equals("null")) {
                            item.setId(jsonObject.getInt("id"));
                        }
                        if (jsonObject.has("date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("date_time"))
                                && !jsonObject.getString("date_time").equals("null")) {
                            item.setDate_time(jsonObject.getString("date_time"));
                        }

                        if (jsonObject.has("township_code")
                                && !TextUtils.isEmpty(jsonObject.getString("township_code"))
                                && !jsonObject.getString("township_code").equals("null")) {
                            item.setTownship_code(jsonObject.getString("township_code"));
                        }

                        if (jsonObject.has("location_code")
                                && !TextUtils.isEmpty(jsonObject.getString("location_code"))
                                && !jsonObject.getString("location_code").equals("null")) {
                            item.setLocation_code(jsonObject.getString("location_code"));
                        }

                        if (jsonObject.has("location_name")
                                && !TextUtils.isEmpty(jsonObject.getString("location_name"))
                                && !jsonObject.getString("location_name").equals("null")) {
                            item.setLocation_name(jsonObject.getString("location_name"));
                        }

                        if (jsonObject.has("lot_row")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_row"))
                                && !jsonObject.getString("lot_row").equals("null")) {
                            item.setLot_row(jsonObject.getString("lot_row"));
                        }

                        if (jsonObject.has("lot_number")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_number"))
                                && !jsonObject.getString("lot_number").equals("null")) {
                            item.setLot_number(jsonObject.getString("lot_number"));
                        }
                        if (jsonObject.has("lot_id")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_id"))
                                && !jsonObject.getString("lot_id").equals("null")) {
                            item.setLot_id(jsonObject.getString("lot_id"));
                        }

                        if (jsonObject.has("occupied")
                                && !TextUtils.isEmpty(jsonObject.getString("occupied"))
                                && !jsonObject.getString("occupied").equals("null")) {
                            item.setOccupied(jsonObject.getString("occupied").toLowerCase());
                        }

                        if (jsonObject.has("plate_no")
                                && !TextUtils.isEmpty(jsonObject.getString("plate_no"))
                                && !jsonObject.getString("plate_no").equals("null")) {
                            item.setPlate_no(jsonObject.getString("plate_no"));
                        }
                        if (jsonObject.has("plate_state")
                                && !TextUtils.isEmpty(jsonObject.getString("plate_state"))
                                && !jsonObject.getString("plate_state").equals("null")) {
                            item.setPlate_state(jsonObject.getString("plate_state"));
                        }

                        if (jsonObject.has("entry_date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("entry_date_time"))
                                && !jsonObject.getString("entry_date_time").equals("null")) {
                            item.setEntry_date_time(jsonObject.getString("entry_date_time"));
                        }

                        if (jsonObject.has("exit_date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("exit_date_time"))
                                && !jsonObject.getString("exit_date_time").equals("null")) {
                            item.setExit_date_time(jsonObject.getString("exit_date_time"));
                        }

                        if (jsonObject.has("expiry_time")
                                && !TextUtils.isEmpty(jsonObject.getString("expiry_time"))
                                && !jsonObject.getString("expiry_time").equals("null")) {
                            item.setExpiry_time(jsonObject.getString("expiry_time"));
                        }

                        if (jsonObject.has("parking_type")
                                && !TextUtils.isEmpty(jsonObject.getString("parking_type"))
                                && !jsonObject.getString("parking_type").equals("null")) {
                            item.setParking_type(jsonObject.getString("parking_type"));
                        }

                        if (jsonObject.has("parking_status")
                                && !TextUtils.isEmpty(jsonObject.getString("parking_status"))
                                && !jsonObject.getString("parking_status").equals("null")) {
                            item.setParking_status(jsonObject.getString("parking_status").toLowerCase());
                        }

                        if (jsonObject.has("lot_reservable")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_reservable"))
                                && !jsonObject.getString("lot_reservable").equals("null")) {
                            if (jsonObject.getString("lot_reservable").equals("1")) {
                                item.setLot_reservable(true);
                            } else if (jsonObject.getString("lot_reservable").equals("0")) {
                                item.setLot_reservable(false);
                            } else {
                                item.setLot_reservable(jsonObject.getBoolean("lot_reservable"));
                            }
                        }
                        if (jsonObject.has("location_id")
                                && !TextUtils.isEmpty(jsonObject.getString("location_id"))
                                && !jsonObject.getString("location_id").equals("null")) {
                            try {
                                item.setLocation_id(Integer.parseInt(jsonObject.getString("location_id")));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        if (!TextUtils.isEmpty(item.getParking_status())
                                && !TextUtils.isEmpty(item.getOccupied())
                                && item.getParking_status().equals("ENTRY")
                                && item.getOccupied().equals("yes")) {

                            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date exitre = null;
                            try {
                                exitre = format1.parse(item.getExpiry_time());

                            } catch (android.net.ParseException e) {
                                // TODO Auto-generated catch block
                                Log.e("#DEBUG", "  Error:  " + e.getMessage());
                                e.printStackTrace();
                            } catch (java.text.ParseException e) {
                                Log.e("#DEBUG", "  Error:  " + e.getMessage());
                                e.printStackTrace();
                            }

                            if (current.after(exitre)) {
                                item.setExpired(true);
                            } else {
                                item.setExpired(false);
                            }
                        }
                        if (!locationLotsMain.contains(item))
                            locationLotsMain.add(item);
                    }


                } catch (JSONException e) {
                    Log.e("#DEBUG", "  Error:  " + e.getMessage());
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.e("#DEBUG", "  Error:  " + e.getMessage());
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rlProgressbar != null)
                rlProgressbar.setVisibility(View.GONE);
            //progressBar.setVisibility(View.GONE);
            if (getActivity() != null && json1 != null) {
                Collections.sort(locationLotsMain, (o1, o2) -> {
                    try {
                        return Integer.valueOf(o1.getLot_id()) - Integer.valueOf(o2.getLot_id());
                    } catch (Exception e) {
                        return o1.getLot_id().compareTo(o2.getLot_id());
                    }
                });
                mIsManagedLocations = true;
                Log.e("#DEBUG", "   Location_lots:  " + new Gson().toJson(locationLotsMain));
                for (int j = 0; j < locationLotsMain.size(); j++) {
                    if (!locationLotsMain.get(j).isLot_reservable()
                            && !locationLotsMain.get(j).getOccupied().equals("yes")) {
                        locationLots.add(locationLotsMain.get(j));
                    }
                }

//                locationLots.clear();
//                locationLots.addAll(locationLotsMain);
                if (locationLots.size() == 0 && locationLotsMain.size() != 0) {
                    locationLots.add(locationLotsMain.get(locationLotsMain.indexOf(
                            new LocationLot(Integer.valueOf(reserveParking.getLocation_lot_id())))));
                }
            }

            super.onPostExecute(s);
        }
    }

    @Override
    protected void setListeners() {

        tvBtnDirection.setOnClickListener(v -> openDirections());

        tvBtnStreetView.setOnClickListener(v -> openStreetView());

        tvBtnCancelReservation.setOnClickListener(view -> {
            //Show cancel reservation confirmation dialog
            showConfirmationReservationCancelDialog();
        });

        tvBtnEditReservation.setOnClickListener(view -> {
            //TODO open edit reservation screen
            isEditReservation = true;
            isRenew = true;
            isRenewReservationClicked = false;
            showDialogReNewReservation();
        });

        tvPlatNo.setOnClickListener(view -> {
            if (myVehicles.size() != 0) {
                showMyVehiclePopupDialog();
            } else {
                new fetchMyVehicles().execute();
            }
        });

        tvBtnParkNow.setOnClickListener(view -> {
            //TODO update with new payment flow
            if (mIsManagedLocations) {
                showDialogLocationLots();
            } else {
                showConfirmParkingDialog();
            }
        });

        tvBtnDone.setOnClickListener(v -> {
            if (getActivity() != null)
                ((vchome) getActivity()).showmapandhidefragment();
        });

        tvBtnRenew.setOnClickListener(v -> {
//            new renewReservation().execute();
            isRenewReservationClicked = true;
            isRenew = true;
            isEditReservation = false;
            showDialogReNewReservation();
        });

//        tvBtnPayWallet.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (cd.isConnectingToInternet()) {
//                    //TODO check parked car status, and get wallet balance
//                    isPayByWallet = true;
//                    isPayByPaypal = false;
//                    new checkParkedCarsReserved().execute();
//                } else {
//                    showAlertDialog(getActivity(), "No Internet Connection",
//                            "You don't have internet connection.", false);
//                }
//            }
//        });
//
//        tvBtnPaypal.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (cd.isConnectingToInternet()) {
//                    //TODO check parked car status, and get wallet balance
//                    isPayByPaypal = true;
//                    isPayByWallet = false;
//                    new checkParkedCarsReserved().execute();
//                } else {
//                    showAlertDialog(getActivity(), "No Internet Connection",
//                            "You don't have internet connection.", false);
//                }
//            }
//        });
    }

    private void openStreetView() {
        Intent i = new Intent(getActivity(), panstreetview.class);
        i.putExtra("lat", reserveParking.getMarker_lat());
        i.putExtra("lang", reserveParking.getMarker_lng());
        i.putExtra("title", title);
        i.putExtra("address", reserveParking.getMarker_address1());
        getActivity().startActivity(i);
    }

    private void openDirections() {
        if (!reserveParking.getMarker_lat().equals("0.0") || !reserveParking.getMarker_lng().equals("0.0")) {

            SharedPreferences sh1 = getActivity().getSharedPreferences("directiontomydirection", Context.MODE_PRIVATE);
            SharedPreferences.Editor ed1 = sh1.edit();
            ed1.putString("lat", reserveParking.getMarker_lat());
            ed1.putString("lang", reserveParking.getMarker_lng());
            ed1.putString("address", reserveParking.getMarker_address1());
            ed1.commit();
            SharedPreferences backtotimer = getActivity().getSharedPreferences("back_parkezly", Context.MODE_PRIVATE);
            SharedPreferences.Editor eddd = backtotimer.edit();
            eddd.putString("isback", "yes");
            eddd.commit();
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                   /* ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                    ft.add(R.id.My_Container_1_ID, new f_direction(), "NewFragmentTag");
                    ft.commit();*/
            f_direction pay = new f_direction();
            ft.add(R.id.My_Container_1_ID, pay, "reservation_details");
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(pay);
            ft.commitAllowingStateLoss();
        }
    }

    private void showDialogReNewReservation() {
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.date_time_picker_dialog, null);
        final TextView tvSelectedTime = dialogView.findViewById(R.id.tvSelectedTime);
        final TextView tvExitTime = dialogView.findViewById(R.id.tvExitTime);
        final TextView tvTitle = dialogView.findViewById(R.id.tvTitle);
        tvTitle.setText("Select Time");
        TextView tvBtnCancel = dialogView.findViewById(R.id.tvBtnCancel);
        TextView tvBtnConfirm = dialogView.findViewById(R.id.tvBtnConfirm);
        tvBtnConfirm.setText("UPDATE");
        builder.setView(dialogView);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        final String stringTime = "";
        final Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault());
        reserveEntryTime = dateFormat1.format(new Date(calendar.getTimeInMillis()));
        reserveEntryTime = reserveParking.getReserve_entry_time();
        selectedEntryTime = calendar.getTimeInMillis();
        try {
            selectedEntryTime = sdf.parse(reserveParking.getReserve_entry_time()).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        tvSelectedTime.setText(dateFormat2.format(new Date(calendar.getTimeInMillis())));
        try {
            tvSelectedTime.setText(dateFormat2.format(dateFormat1.parse(reserveParking.getReserve_entry_time())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        tvSelectedTime.setText(reserveParking.getReserve_entry_time());
        if (currentDurationUnit.equals("Hr") || currentDurationUnit.equals("Ho") || currentDurationUnit.equals("Hour")) {
            calendar.add(Calendar.HOUR, intHour);
        } else if (currentDurationUnit.equals("Mi") || currentDurationUnit.equals("Minute")) {
            calendar.add(Calendar.MINUTE, intHour);
        } else if (currentDurationUnit.equals("Da") || currentDurationUnit.equals("Day")) {
            calendar.add(Calendar.DAY_OF_MONTH, intHour);
        } else if (currentDurationUnit.equals("We") || currentDurationUnit.equals("Week")) {
            calendar.add(Calendar.WEEK_OF_MONTH, intHour);
        } else if (currentDurationUnit.equals("Mo") || currentDurationUnit.equals("Month")) {
            calendar.add(Calendar.MONTH, intHour);
        }
        tvExitTime.setText(dateFormat2.format(new Date(calendar.getTimeInMillis())));
        try {
            tvExitTime.setText(dateFormat2.format(dateFormat1.parse(reserveParking.getReserve_expiry_time())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        reserveExpiryTime = dateFormat1.format(new Date(calendar.getTimeInMillis()));
        reserveEntryTime = reserveParking.getReserve_expiry_time();
        selectedExitTime = calendar.getTimeInMillis();
        try {
            selectedExitTime = sdf.parse(reserveParking.getReserve_expiry_time()).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        tvSelectedTime.setOnClickListener((View view) -> {

            DatePickerDialog entryDatePickerDialog = new DatePickerDialog(getActivity(), (datePicker, i, i1, i2) -> {
                final Calendar calendar1 = Calendar.getInstance();
                calendar1.set(Calendar.YEAR, i);
                calendar1.set(Calendar.MONTH, i1);
                calendar1.set(Calendar.DAY_OF_MONTH, i2);


                new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        calendar1.set(Calendar.HOUR_OF_DAY, i);
                        calendar1.set(Calendar.MINUTE, i1);

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//                                stringTime = dateFormat.format(new Date(calendar1.getTimeInMillis()));
                        tvSelectedTime.setText(dateFormat2.format(new Date(calendar1.getTimeInMillis())));
                        reserveEntryTime = dateFormat.format(new Date(calendar1.getTimeInMillis()));
                        if (currentDurationUnit.equals("Hr") || currentDurationUnit.equals("Ho") || currentDurationUnit.equals("Hour")) {
                            calendar1.add(Calendar.HOUR, intHour);
                        } else if (currentDurationUnit.equals("Mi") || currentDurationUnit.equals("Minute")) {
                            calendar1.add(Calendar.MINUTE, intHour);
                        } else if (currentDurationUnit.equals("Da") || currentDurationUnit.equals("Day")) {
                            calendar1.add(Calendar.DAY_OF_MONTH, intHour);
                        } else if (currentDurationUnit.equals("We") || currentDurationUnit.equals("Week")) {
                            calendar1.add(Calendar.WEEK_OF_MONTH, intHour);
                        } else if (currentDurationUnit.equals("Mo") || currentDurationUnit.equals("Month")) {
                            calendar1.add(Calendar.MONTH, intHour);
                        }
                        tvExitTime.setText(dateFormat2.format(new Date(calendar1.getTimeInMillis())));
                        reserveExpiryTime = dateFormat.format(new Date(calendar1.getTimeInMillis()));

                        try {
                            selectedEntryTime = dateFormat.parse(reserveEntryTime).getTime();
                            selectedExitTime = dateFormat.parse(reserveExpiryTime).getTime();
                            Log.e("#DEBUG", "   selectedEntryTime:  " + selectedEntryTime
                                    + "\n  selectedExitTime" + selectedExitTime);
                        } catch (ParseException e) {
                            e.printStackTrace();
                            Log.e("#DEBUG", "   parse Error: " + e.getMessage());
                        }

                    }
                }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE),
                        false).show();
            }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
            entryDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            entryDatePickerDialog.show();
        });

        tvBtnConfirm.setOnClickListener(view -> {
            alertDialog.dismiss();
            Log.e("#DEBUG", "   reserveEntryTime:  " + reserveEntryTime + " " +
                    "reserveExpiryTime:  " + reserveExpiryTime);
            showDialogLocationLots();
        });

        tvBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private class renewReservation extends AsyncTask<String, String, String> {
        String exit_status = "_table/reserved_parkings";
        JSONObject json, json1;
        String re_id = "null";

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", reserveParking.getId());

            jsonValues.put("reserve_entry_time", reserveEntryTime);
            jsonValues.put("reserve_expiry_time", reserveExpiryTime);
            jsonValues.put("reserve_exit_date_time", reserveExpiryTime);
            if (selectedLocationLot != null) {
                if (!TextUtils.isEmpty(selectedLocationLot.getLot_id())) {
                    jsonValues.put("lot_id", selectedLocationLot.getLot_id());
                }
                if (!TextUtils.isEmpty(selectedLocationLot.getLot_number())) {
                    jsonValues.put("lot_number", selectedLocationLot.getLot_number());
                }
                if (!TextUtils.isEmpty(selectedLocationLot.getLot_row())) {
                    jsonValues.put("lot_row", selectedLocationLot.getLot_row());
                }
                jsonValues.put("location_lot_id", selectedLocationLot.getId());

            }

            if (isPrePaid) {
                jsonValues.put("reserve_payment_paid", "PAID");
                jsonValues.put("reserve_payment_condition", "PRE-PYMT");
                jsonValues.put("reserve_payment_due_by", currentdate);

                if (paymentmethod.equals("Paypal")) {
                    jsonValues.put("ipn_txn_id", transection_id);
                    jsonValues.put("ipn_payment", "Paypal");
                    jsonValues.put("ipn_status", "success");
                    jsonValues.put("ipn_address", "");
                }
                jsonValues.put("wallet_trx_id", wallet_id);
            }

            JSONObject json = new JSONObject(jsonValues);
            //  String url = "http://100.12.26.176:8006/api/v2/pzly01live7/_table/poiv2";
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpPut put = new HttpPut(url);
            put.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            put.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
//setting json object to put request.
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            put.setEntity(entity);
//this is your response:
            HttpResponse response = null;
            try {
                response = client.execute(put);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.d("re_id", re_id);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rlProgressbar != null)
                rlProgressbar.setVisibility(View.GONE);

            if (getActivity() != null && json1 != null) {
                if (!re_id.equals("null")) {
                    Toast.makeText(getActivity(), "Reservation Renew successfully!", Toast.LENGTH_SHORT).show();
                    reservationId = re_id;
                    new fetchReservationDetails().execute();
                    isUpdated = true;
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Something went wrong");
                    alertDialog.setMessage("Please try again");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
            super.onPostExecute(s);
        }
    }

    private void showDialogLocationLots() {
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        View dialogView = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_select_locatoin_lot, null);
        builder.setView(dialogView);
        dialogLocationLot = builder.create();
        dialogLocationLot.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        RelativeLayout rlClose = dialogView.findViewById(R.id.rlClose);
        RelativeLayout rlDone = dialogView.findViewById(R.id.rlDone);
        RelativeLayout rlSelectedLot = dialogView.findViewById(R.id.rlSelectedLot);
        RecyclerView rvLocationLot = dialogView.findViewById(R.id.rvLocationLot);
        TextView tvSelectedLot = dialogView.findViewById(R.id.tvSelectedLot);
        TextView tvLocationTitle = dialogView.findViewById(R.id.tvLocationTitle);
        tvLocationTitle.setText(reserveParking.getLocation_name());

        if (selectedLocationLot != null) {
            rlSelectedLot.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(selectedLocationLot.getLot_id())) {
                tvSelectedLot.setText(selectedLocationLot.getLot_id());
            }
        } else {
            rlSelectedLot.setVisibility(View.GONE);
        }

        LocationLotAdapter locationLotAdapter = new LocationLotAdapter();
        rvLocationLot.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        rvLocationLot.setAdapter(locationLotAdapter);

        rlDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLocationLot.dismiss();
            }
        });

        rlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLocationLot.dismiss();
            }
        });

        updateManagedLocations(locationLotAdapter);

        dialogLocationLot.show();
    }

    private void updateManagedLocations(LocationLotAdapter locationLotAdapter) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        locationLotsReservable.clear();
        for (int i = 0; i < locationLotsMain.size(); i++) {
            if (locationLotsMain.get(i).isLot_reservable()) {
                locationLotsReservable.add(locationLotsMain.get(i));
            }
        }
        for (int i = 0; i < reserveParkings.size(); i++) {
            try {
                long entryTime = simpleDateFormat.parse(reserveParkings.get(i).getReserve_entry_time()).getTime();
                long exitTime = simpleDateFormat.parse(reserveParkings.get(i).getReserve_expiry_time()).getTime();
                for (int j = 0; j < locationLotsReservable.size(); j++) {
                    if (Integer.valueOf(reserveParkings.get(i).getLocation_lot_id()) == locationLotsReservable.get(j).getId()) {
                        if (selectedEntryTime > entryTime && selectedExitTime < exitTime) {
                            // 2PM               1PM           3PM               4PM
                            locationLotsReservable.get(j).setIs_reserved(true);
                        } else if (selectedEntryTime >= entryTime && selectedEntryTime <= exitTime) {
                            // 2PM                      1PM         2PM                 4PM
                            locationLotsReservable.get(j).setIs_reserved(true);
                        } else if (selectedExitTime >= entryTime && selectedExitTime <= exitTime) {
                            // 2PM                      1PM         2PM                 4PM
                            locationLotsReservable.get(j).setIs_reserved(true);
                        }
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        locationLotAdapter.notifyDataSetChanged();

    }

    private void showConfirmationReservationCancelDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        String message = "Are you sure, you want to cancel this reservation?";
        if (mIsPaymentPaid) {
            message = message + "\n\nAmount paid by you = $" + mAmountPaid;
            message = message + "\nCancellation Charge = $" + mCancelationCharge;
            mRefundableAmount = mAmountPaid - mCancelationCharge;
            message = message + "\nAmount Refundable: $" + mRefundableAmount;
            message = message + "\n\nRefundable amount will be added to your Wallet balance.";
        }
        builder.setMessage(message);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //update reservation with cancel
                dialogInterface.dismiss();
                isCancelReservation = true;
                new updateReserve().execute();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void showMyVehiclePopupDialog() {

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        View dialogView = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_my_vehicles, null);
        builder.setView(dialogView);
        RelativeLayout rlClose = dialogView.findViewById(R.id.rlClose);
        RecyclerView rvMyVehicles = dialogView.findViewById(R.id.rvMyVehicles);
        TextView tvBtnAddVehicles = dialogView.findViewById(R.id.tvBtnAddVehicles);

        alertDialogMyVehicles = builder.create();
        rlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (alertDialogMyVehicles != null)
                    alertDialogMyVehicles.dismiss();
            }
        });

        MyVehicleAdapter myVehicleAdapter = new MyVehicleAdapter();
        rvMyVehicles.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMyVehicles.setAdapter(myVehicleAdapter);

        alertDialogMyVehicles.show();

    }

    public class fetchMyVehicles extends AsyncTask<String, String, String> {
        JSONObject json = null;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "0");
        String vehiclenourl = "_table/user_vehicles?filter=user_id%3D" + user_id + "";

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            myVehicles.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("get_vehicle_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);
                    Log.e("rerpones", String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        String plate_no = c.getString("plate_no");
                        String state = c.getString("registered_state");
                        String id = c.getString("id");
                        item.setState(state);
                        item.setPlateno(plate_no);
                        item.setId(id);
                        item.setVehicle_country(c.getString("vehicle_country"));
                        item.setIsselect(false);
                        item.setCheckboxselected(false);
                        myVehicles.add(item);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (getActivity() != null) {
                rlProgressbar.setVisibility(View.GONE);
                if (json != null) {
                    showMyVehiclePopupDialog();
                }
            }
            super.onPostExecute(s);
        }
    }

    private void showConfirmParkingDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure, you want to Park Car?");
        builder.setPositiveButton("PARK HERE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                if (!TextUtils.isEmpty(reserveParking.getReserve_payment_paid())) {
                    if (reserveParking.getReserve_payment_paid().equals("PAID")) {
                        //park vehicle into parking_cars
                        isPayByPaypal = false;
                        isPayByWallet = false;
                        new checkParkedCarsReserved().execute();

                    } else if (reserveParking.getReserve_payment_paid().equals("UNPAID")) {
                        //check payment terms
                        if (!TextUtils.isEmpty(reserveParking.getReserve_payment_condition())) {
                            if (reserveParking.getReserve_payment_condition().equals("ENTRY")) {
                                //pay and park

                                PaymentOptionDialog dialog = PaymentOptionDialog.newInstance();
                                Bundle bundle = new Bundle();
                                bundle.putBoolean("mIsPayLater", false);
                                dialog.setArguments(bundle);
                                dialog.show(getChildFragmentManager(), "payment");
                            } else {
                                //park vehicle into parking_cars
                                isPayByPaypal = false;
                                isPayByWallet = false;
                                new checkParkedCarsReserved().execute();
                            }
                        } else {
                            // park vehicle into parking_cars
                            isPayByPaypal = false;
                            isPayByWallet = false;
                            new checkParkedCarsReserved().execute();
                        }

                    }
                }
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                selectedLocationLot = null;
                dialogInterface.dismiss();
            }
        });
        androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onWalletClick() {
        new getwalletbal().execute();
    }

    public class getwalletbal extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String userid = logindeatl.getString("id", "null");
        JSONObject json, json1;
        String wallbalurl = "_table/user_wallet?order=date_time%20DESC";
        //String wallbalurl = "_table/user_wallet?filter=(user_id%3D'"+userid+"')";
        String w_id;

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + wallbalurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            HttpEntity resEntity = response.getEntity();
            String responseStr = null;
            if (response != null) {
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        String user = c.getString("user_id");

                        if (user.equals(userid)) {
                            nbal = c.getString("new_balance");
                            if (!nbal.equals("null")) {
                                cbal = c.getString("current_balance");
                                item.setAmount(nbal);
                                item.setPaidamount(cbal);
                                wallbalarray.add(item);
                                break;
                            }
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rlProgressbar != null)
                rlProgressbar.setVisibility(View.GONE);
            // progressBar.setVisibility(View.GONE);

            if (getActivity() != null && json != null) {
                if (!mIsProcessCancel) {
                    if (wallbalarray.size() > 0) {
                        double bal = Double.parseDouble(cbal);
                        if (bal > 0) {
                            newbal = bal - mParkingTotal;
                            String finallab2 = String.format("%.2f", bal);
//                        boolean is_parked_time = true;
//                        int remain_time = 0;
//                        Renew_parked_time = Renew_parked_time != null ? (!Renew_parked_time.equals("") ? (!Renew_parked_time.equals("null") ? Renew_parked_time : "") : "") : "";
//                        if (!Renew_parked_time.equals("")) {
//                            int parked_hours = Integer.parseInt(Renew_parked_time);
//                            final_count = parked_hours + hours;
//                            int max = Integer.parseInt(max_time);
//                            remain_time = max - parked_hours;
//                            if (final_count <= max) {
//                                is_parked_time = true;
//                            } else {
//                                is_parked_time = false;
//                            }
//                        } else {
//                            final_count = hours;
//                        }
                            Calendar calendar = Calendar.getInstance();
                            int day = calendar.get(Calendar.DAY_OF_WEEK);

                            if (newbal < 0) {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                alertDialog.setTitle("alert");
                                alertDialog.setMessage("You don't have enough funds in your wallet.");
                                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                                alertDialog.show();

                            } else {
                                if (true) {

                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                    alertDialog.setTitle("Wallet Balance: $" + finallab2);
                                    alertDialog.setMessage("Would you like to pay from your ParkEZly wallet ? With service and transaction fee your total will be " + "$" + mParkingTotal + "");
                                    alertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            ip = getLocalIpAddress();
                                            showDialogDebitFromWallet();
                                        }
                                    });
                                    alertDialog.setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });
                                    alertDialog.show();
                                }
//                            else {
//                                TextView tvTitle = new TextView(getActivity());
//                                tvTitle.setText("ParkEZly");
//                                tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
//                                tvTitle.setPadding(0, 10, 0, 0);
//                                tvTitle.setTextColor(Color.parseColor("#000000"));
//                                tvTitle.setTextSize(20);
//                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                                alertDialog.setCustomTitle(tvTitle);
//                                alertDialog.setMessage(Html.fromHtml("Duration must be less than<b> maximum park " + max_time + " " + duration_unit + "s</b> and you already parked <b>" + Renew_parked_time + " " + duration_unit + "s</b> so you can park upto <b>" + remain_time + " " + duration_unit + "s</b> more or less!"));
//                                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//
//                                    }
//                                });
//                                final AlertDialog alertd = alertDialog.create();
//                                alertd.show();
//                                TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
//                                messageText.setGravity(Gravity.CENTER_HORIZONTAL);
//                            }
                            }
                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("alert");
                            alertDialog.setMessage("You don't have enough funds in your wallet.");
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialog.show();
                        }

                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("alert");
                        alertDialog.setMessage("You don't have enough funds in your wallet.");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }
                } else {
                    if (!TextUtils.isEmpty(cbal)) {
                        mWalletBalance = Double.parseDouble(cbal);
                    } else {
                        mWalletBalance = 0.0;
                    }
                    mWalletBalance = mWalletBalance + mRefundableAmount;
                    new updatewallet().execute();
                }
            }
            super.onPostExecute(s);

        }
    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("ip", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("aax", ex.toString());
        }
        return null;
    }

    private void showDialogDebitFromWallet() {
        // if yes
        new updatewallet().execute();
    }

    public class updatewallet extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONObject json12;
        String walleturl = "_table/user_wallet";
        String w_id = "";

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            currentdate = sdf.format(new Date());
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            if (mIsProcessCancel) {
                jsonValues1.put("last_paid_amt", mRefundableAmount);
                jsonValues1.put("current_balance", mWalletBalance);
                jsonValues1.put("new_balance", mWalletBalance);
                jsonValues1.put("action", "addition");
                jsonValues1.put("add_amt", mRefundableAmount);
            } else {
                jsonValues1.put("last_paid_amt", mParkingTotal);
                jsonValues1.put("current_balance", newbal);
                jsonValues1.put("new_balance", newbal);
                jsonValues1.put("action", "deduction");
            }
            jsonValues1.put("paid_date", currentdate);
            jsonValues1.put("ip", ip);
            jsonValues1.put("remember_me", "");
            jsonValues1.put("ipn_txn_id", "");
            jsonValues1.put("ipn_custom", "");
            jsonValues1.put("ipn_payment", "");
            jsonValues1.put("date_time", currentdate);
            jsonValues1.put("ipn_address", "");
            jsonValues1.put("user_id", id);

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        wallet_id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rlProgressbar != null)
                rlProgressbar.setVisibility(View.VISIBLE);
            //progressBar.setVisibility(View.GONE);
            if (getActivity() != null && json12 != null) {
                isPrePaid = true;

                if (!wallet_id.equals("")) {
                    if (mIsProcessCancel) {
                        Toast.makeText(getActivity(), "Reservation Cancelled Successfully!", Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();
                    } else {
                        showsucessmesg.setVisibility(View.VISIBLE);
                        ActionStartsHere();
                        paymentmethod = "wallet";
                        //TODO payment done
                        if (isRenewReservationClicked) {
                            new renewReservation().execute();
                        } else {
                            new addParkedCar().execute();
                        }
                    }
//                    updateWalletBalance();
//                        paymentmethod = "wallet";
//                        showsucessmesg.setVisibility(View.VISIBLE);
//                        ActionStartsHere();
//                        if (Renew_is_valid.equals("yes")) {
//                            new Update_Parking().execute();
//                        } else {
//                            new finalupdate().execute();
//                        }
                }

            }
            super.onPostExecute(s);
        }
    }

    @Override
    public void onPaymentClick() {
        mIsWalletPayment = false;
        startPayPalPaymentRequest();

    }

    private void startPayPalPaymentRequest() {

        PaypalPaymentIntegration(String.valueOf(mParkingTotal));
    }

    @Override
    public void onPayLaterClick() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.i(TAG, confirm.toJSONObject().toString(4));
                    Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                    JSONObject jsonObj = new JSONObject(confirm.getPayment().toJSONObject().toString(4));
                    Log.e("JSON IS", "LIKE" + jsonObj);
                    Log.e("short_description", "short_description : " + jsonObj.getString("short_description"));
                    Log.e("amount", "amount : " + jsonObj.getString("amount"));
                    Log.e("intent", "intent : " + jsonObj.getString("intent"));
                    Log.e("currency_code", "currency_code : " + jsonObj.getString("currency_code"));
                    JSONObject jsonObjResponse = confirm.toJSONObject().getJSONObject("response");
                    transection_id = jsonObjResponse.getString("id");
                    if (transection_id != null) {
                        isPrePaid = true;
                        showsucessmesg.setVisibility(View.VISIBLE);
                        ActionStartsHere();
                        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                        String id = logindeatl.getString("id", "null");
                        paymentmethod = "Paypal";
                        //TODO payment done
                        if (isRenewReservationClicked) {
                            new renewReservation().execute();
                        } else {
                            new addParkedCar().execute();
                        }

                    } else {
                        transactionfailalert();
                    }
                    Log.e("transactionId", ":;;;;" + transection_id);

                } catch (JSONException e) {
                    Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                }
            }
        } else {

        }
    }

    public void ActionStartsHere() {
        againStartGPSAndSendFile();
    }

    public void againStartGPSAndSendFile() {
        new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                ActionStartsHere();
                showsucessmesg.setVisibility(View.GONE);

            }

        }.start();
    }

    public void transactionfailalert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Transaction Fail try again");
        alertDialog.setMessage("Fail");
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alertDialog.show();
    }

    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();

    }

    public class checkParkedCarsReserved extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONArray array = new JSONArray();
        String parked_id = "";

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String vechiclenourl = null;
            try {
                String plate = reserveParking.getPlate_no();
                String statename = reserveParking.getState();
                plate = plate.replaceAll("\\s+", "");
                vechiclenourl = "_table/parked_cars?filter=(plate_no%3D" + URLEncoder.encode(plate, "utf-8") + ")%20AND%20(pl_state%3D" + statename + ")%20AND%20(parking_status%3D'ENTRY')";
            } catch (Exception e) {
                e.printStackTrace();
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + vechiclenourl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject v = array.getJSONObject(i);
                        parked_id = v.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rlProgressbar != null)
                rlProgressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (!array.isNull(0)) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Vehicle already parked!");
                    alertDialog.setMessage("This Vehicle is already parked, Do you want to exit it from previous parking and park here ?");

                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            new exitsparking(parked_id).execute();
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });
                    alertDialog.show();
                } else {
                    // post values into parked_cars
                    if (isPayByWallet) {
                        //Get wallet balance
                        new getwalletbal().execute();
                    } else if (isPayByPaypal) {
                        //Pay with PayPal
                        PaypalPaymentIntegration(reserveParking.getParking_total());
                    } else {
                        //No payment required as of now
                        new addParkedCar().execute("payLater");
                    }

                }
            }
            super.onPostExecute(s);

        }
    }

//    public class getwalletbal extends AsyncTask<String, String, String> {
//        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
//        String userid = logindeatl.getString("id", "null");
//        JSONObject json, json1;
//        String wallbalurl = "_table/user_wallet?order=date_time%20DESC";
//        //String wallbalurl = "_table/user_wallet?filter=(user_id%3D'"+userid+"')";
//        String w_id;
//
//        @Override
//        protected void onPreExecute() {
//            rl_progressbar.setVisibility(View.VISIBLE);
//            progressBar.setVisibility(View.VISIBLE);
//            super.onPreExecute();
//        }
//
//        @TargetApi(Build.VERSION_CODES.KITKAT)
//        @Override
//        protected String doInBackground(String... params) {
//            String url = getString(R.string.api) + getString(R.string.povlive) + wallbalurl;
//            DefaultHttpClient client = new DefaultHttpClient();
//            HttpGet post = new HttpGet(url);
//            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
//            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
//            HttpResponse response = null;
//            try {
//                response = client.execute(post);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            HttpEntity resEntity = response.getEntity();
//            String responseStr = null;
//            if (response != null) {
//                try {
//                    responseStr = EntityUtils.toString(resEntity).trim();
//
//                    json = new JSONObject(responseStr);
//
//                    JSONArray array = json.getJSONArray("resource");
//                    for (int i = 0; i < array.length(); i++) {
//                        item item = new item();
//                        JSONObject c = array.getJSONObject(i);
//                        String user = c.getString("user_id");
//
//                        if (user.equals(userid)) {
//                            nbal = c.getString("new_balance");
//                            if (!nbal.equals("null")) {
//                                cbal = c.getString("current_balance");
//                                item.setAmount(nbal);
//                                item.setPaidamount(cbal);
//                                wallbalarray.add(item);
//                                break;
//                            }
//                        }
//                    }
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            if (rl_progressbar != null)
//                rl_progressbar.setVisibility(View.GONE);
//            // progressBar.setVisibility(View.GONE);
//
//            if (getActivity() != null && json != null) {
//                if (wallbalarray.size() > 0) {
//                    double bal = Double.parseDouble(cbal);
//                    if (bal > 0) {
//                        finalpayamount = 0.0;
//                        min = shoues.substring(shoues.indexOf(' ') + 1);
//                        String hr = shoues.substring(0, shoues.indexOf(' '));
//                        Double d = new Double(Double.parseDouble(hr));
//                        hours = d.intValue();
//                        parkingQty = hours;
//                        float minutes;
//                        String h;
//
//                        double hrr = Double.parseDouble(rate1);
//                        parkingUnits = hrr;
//                        double prices = Double.parseDouble(rate);
//                        parkingRate = prices;
//                        double perhrrate = hrr / prices;
//                        finalpayamount = hours / perhrrate;
//                        subTotal = hours / perhrrate;
//                        tr_percentage_for_parked_cars = finalpayamount * tr_percentage1;
//                        finalpayamount = finalpayamount + tr_percentage_for_parked_cars + tr_fee1;
//                        newbal = bal - finalpayamount;
//                        String finallab2 = String.format("%.2f", bal);
//                        finallab = String.format("%.2f", finalpayamount);
//                        boolean is_parked_time = true;
//                        int remain_time = 0;
//                        Renew_parked_time = Renew_parked_time != null ? (!Renew_parked_time.equals("") ? (!Renew_parked_time.equals("null") ? Renew_parked_time : "") : "") : "";
//                        if (!Renew_parked_time.equals("")) {
//                            int parked_hours = Integer.parseInt(Renew_parked_time);
//                            final_count = parked_hours + hours;
//                            int max = Integer.parseInt(max_time);
//                            remain_time = max - parked_hours;
//                            if (final_count <= max) {
//                                is_parked_time = true;
//                            } else {
//                                is_parked_time = false;
//                            }
//                        } else {
//                            final_count = hours;
//                        }
//                        Calendar calendar = Calendar.getInstance();
//                        int day = calendar.get(Calendar.DAY_OF_WEEK);
//
//                        if (newbal < 0) {
//                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                            alertDialog.setTitle("alert");
//                            alertDialog.setMessage("You don't have enough funds in your wallet.");
//                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
//                                }
//                            });
//                            alertDialog.show();
//
//                        } else {
//                            if (is_parked_time) {
//
//                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                                alertDialog.setTitle("Wallet Balance: $" + finallab2);
//                                alertDialog.setMessage("Would you like to pay from your ParkEZly wallet ? With service and transaction fee your total will be " + "$" + finallab + "");
//                                alertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        ip = getLocalIpAddress();
//                                        new updatewallet().execute();
//                                    }
//                                });
//                                alertDialog.setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                    }
//                                });
//                                alertDialog.show();
//                            } else {
//                                TextView tvTitle = new TextView(getActivity());
//                                tvTitle.setText("ParkEZly");
//                                tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
//                                tvTitle.setPadding(0, 10, 0, 0);
//                                tvTitle.setTextColor(Color.parseColor("#000000"));
//                                tvTitle.setTextSize(20);
//                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                                alertDialog.setCustomTitle(tvTitle);
//                                alertDialog.setMessage(Html.fromHtml("Duration must be less than<b> maximum park " + max_time + " " + duration_unit + "s</b> and you already parked <b>" + Renew_parked_time + " " + duration_unit + "s</b> so you can park upto <b>" + remain_time + " " + duration_unit + "s</b> more or less!"));
//                                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//
//                                    }
//                                });
//                                final AlertDialog alertd = alertDialog.create();
//                                alertd.show();
//                                TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
//                                messageText.setGravity(Gravity.CENTER_HORIZONTAL);
//                            }
//                        }
//                    } else {
//                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                        alertDialog.setTitle("alert");
//                        alertDialog.setMessage("You don't have enough funds in your wallet.");
//                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                            }
//                        });
//                        alertDialog.show();
//                    }
//
//                } else {
//                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                    alertDialog.setTitle("alert");
//                    alertDialog.setMessage("You don't have enough funds in your wallet.");
//                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                        }
//                    });
//                    alertDialog.show();
//                }
//            }
//            super.onPostExecute(s);
//
//        }
//    }

    public class exitsparking extends AsyncTask<String, String, String> {
        String exit_status = "_table/parked_cars";
        JSONObject json, json1;
        String re_id = "null";
        String parked_id;

        public exitsparking(String pid) {
            this.parked_id = pid;
        }

        @Override
        protected void onPreExecute() {

            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            currentdate = sdf.format(new Date());
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", parked_id);
            jsonValues.put("parking_status", "EXIT");
            jsonValues.put("exit_date_time", currentdate);
            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.d("re_id", re_id);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null && json1 != null) {
                if (!re_id.equals("null")) {
                    canclealarm(Integer.parseInt(parked_id));
                    if (isPayByWallet) {
                        //Get wallet balance
                    } else if (isPayByPaypal) {
                        //Pay with PayPal
                    } else {
                        //No payment required as of now
                        new addParkedCar().execute("payLater");
                    }

                    SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                    String id1 = logindeatl.getString("id", "null");
                    if (id1.equals("null")) {
                        ArrayList<item> arraylist = new ArrayList<>();
                        SharedPreferences sharedPrefs = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
                        Gson gson = new Gson();
                        String json = sharedPrefs.getString("car", null);
                        Type type = new TypeToken<ArrayList<item>>() {
                        }.getType();
                        arraylist = gson.fromJson(json, type);
                        if (arraylist == null) {
                            arraylist = new ArrayList<>();
                        }
                        if (arraylist.size() > 0) {
                            for (int j = 0; j < arraylist.size(); j++) {
                                String id = arraylist.get(j).getId();
                                if (id.equals(re_id)) {
                                    arraylist.remove(j);
                                }
                            }
                        }
                        SharedPreferences sharedPrefs1 = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPrefs1.edit();
                        Gson gson1 = new Gson();
                        String json1 = gson1.toJson(arraylist);
                        editor.putString("car", json1);
                        editor.commit();
                    }
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Something went wrong");
                    alertDialog.setMessage("Please try again");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
            super.onPostExecute(s);
        }
    }

    private void PaypalPaymentIntegration(String amount) {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE, amount);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent, String payamount) {
        return new PayPalPayment(new BigDecimal(payamount), "USD", "Parking Payment", paymentIntent);
    }

    public void canclealarm(int id) {
        Intent notificationIntent = new Intent(getActivity(), NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, id);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(getActivity(), id + 1, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent alarmIntent1 = PendingIntent.getBroadcast(getActivity(), id + 2, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent alarmIntent3 = PendingIntent.getBroadcast(getActivity(), id + 3, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
        alarmManager.cancel(alarmIntent);
        alarmIntent.cancel();
        alarmManager.cancel(alarmIntent1);
        alarmIntent1.cancel();
        alarmManager.cancel(alarmIntent3);
        alarmIntent3.cancel();
    }

    public class addParkedCar extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String rid = logindeatl.getString("id", "null");
        String id1 = "null";
        JSONObject json, json1;
        String pov2 = "_table/parked_cars";
        boolean isPayLater = false;
        String paramOption = "";

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            if (params.length > 0)
                paramOption = params[0];
            isPayLater = !TextUtils.isEmpty(paramOption) && paramOption.equals("payLater");
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            currentdate = sdf.format(new Date());
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            TimeZone zone12 = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone12);
            final String currentDateandTime = currentdate;
            Date date11 = null;
            try {
                date11 = sdf.parse(currentDateandTime);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date11);

            String expridate = sdf.format(calendar.getTime());
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.US);
            SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            outputFormat.setTimeZone(zone12);
            inputFormat.setTimeZone(zone12);

            Date date = null;
            Date Current = null;
            String expiretime = null;
            String final_current_date = null;
            try {
                date = inputFormat.parse(expridate);
                Current = inputFormat.parse(currentdate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            expiretime = outputFormat.format(date);
            final_current_date = outputFormat.format(Current);
            int h = 0;
            if (user_id.equals("null")) {
                h = 0;
            } else {
                h = Integer.parseInt(user_id);
            }
            String plate = reserveParking.getPlate_no();
            //TODO park now, pay later
            if (isPayLater) {
                jsonValues.put("reservation_id", reserveParking.getId());
                jsonValues.put("entry_date_time", currentdate);
                jsonValues.put("exit_date_time", reserveParking.getReserve_exit_date_time());
                jsonValues.put("expiry_time", reserveParking.getReserve_expiry_time());
                jsonValues.put("max_duration", reserveParking.getMax_duration());
                jsonValues.put("payment_method", "");
                jsonValues.put("payment_choice", "");
                jsonValues.put("parking_rate", reserveParking.getParking_rate());

                jsonValues.put("parking_subtotal", reserveParking.getParking_subtotal());
                jsonValues.put("tr_percent", reserveParking.getTr_percent());
                jsonValues.put("tr_fee", reserveParking.getTr_fee());
                jsonValues.put("parking_total", reserveParking.getParking_total());
                jsonValues.put("wallet_trx_id", "");

                jsonValues.put("ipn_txn_id", "");
                jsonValues.put("ipn_payment", "");
                jsonValues.put("ipn_status", "");
                jsonValues.put("ipn_address", "");

                jsonValues.put("ParkedNow_PayLater", "1");
                jsonValues.put("ParkNow_PostPayment_term", reserveParking.getReservation_PostPayment_term());
                jsonValues.put("ParkNow_PostPayment_Fee", 0.0);
                jsonValues.put("ParkNow_PostPayment_LateFee", reserveParking.getReservation_PostPayment_LateFee());
                jsonValues.put("ParkNow_PostPayment_DueAmount", reserveParking.getParking_total());
                jsonValues.put("ParkNow_PostPayment_Status", "UNPAID");
                jsonValues.put("reserve_payment_condition", reserveParking.getReservation_PostPayment_term());
                jsonValues.put("reserve_payment_paid", "UNPAID");
            } else {
                jsonValues.put("reservation_id", reserveParking.getId());
                jsonValues.put("entry_date_time", currentdate);
                jsonValues.put("exit_date_time", reserveParking.getReserve_exit_date_time());
                jsonValues.put("expiry_time", reserveParking.getReserve_expiry_time());
                jsonValues.put("max_duration", reserveParking.getMax_duration());
                jsonValues.put("payment_method", reserveParking.getPayment_method());
                jsonValues.put("payment_choice", reserveParking.getPayment_choice());
                jsonValues.put("parking_rate", reserveParking.getParking_rate());

                jsonValues.put("parking_subtotal", reserveParking.getParking_subtotal());
                jsonValues.put("tr_percent", reserveParking.getTr_percent());
                jsonValues.put("tr_fee", reserveParking.getTr_fee());
                jsonValues.put("parking_total", reserveParking.getParking_total());
                jsonValues.put("wallet_trx_id", reserveParking.getWallet_trx_id());


                jsonValues.put("ipn_txn_id", reserveParking.getIpn_txn_id());
                jsonValues.put("ipn_payment", reserveParking.getIpn_payment());
                jsonValues.put("ipn_status", reserveParking.getIpn_status());
                jsonValues.put("ipn_address", reserveParking.getIpn_address());
            }

            plate = plate.replaceAll("\\s+", "");
            jsonValues.put("pre_booked", 1);
            jsonValues.put("manager_type", reserveParking.getManager_type());
            jsonValues.put("manager_type_id", reserveParking.getManager_type_id());
            jsonValues.put("company_id", reserveParking.getCompany_id());
            jsonValues.put("twp_id", reserveParking.getTwp_id());

            jsonValues.put("parking_type", reserveParking.getParking_type());
            jsonValues.put("township_code", reserveParking.getTownship_code());
            jsonValues.put("location_code", reserveParking.getLocation_code());
            jsonValues.put("location_name", reserveParking.getLocation_name());
            jsonValues.put("location_id", reserveParking.getLocation_id());

            jsonValues.put("user_id", reserveParking.getUser_id());
            jsonValues.put("permit_id", 0);
            jsonValues.put("subscription_id", 0);
            jsonValues.put("plate_no", reserveParking.getPlate_no());
            jsonValues.put("pl_state", reserveParking.getPl_state());
            jsonValues.put("lat", reserveParking.getLat());
            jsonValues.put("lng", reserveParking.getLng());
            jsonValues.put("address1", reserveParking.getAddress1());
            jsonValues.put("address2", reserveParking.getAddress2());
            jsonValues.put("city", reserveParking.getCity());
            jsonValues.put("state", reserveParking.getState());
            jsonValues.put("zip", reserveParking.getZip());
            jsonValues.put("country", reserveParking.getCountry());
            jsonValues.put("ip", reserveParking.getIp());
            jsonValues.put("parking_token", reserveParking.getParking_token());
            jsonValues.put("parking_status", "ENTRY");
            jsonValues.put("platform", "Android");

            if (selectedLocationLot != null) {
                jsonValues.put("lot_id", selectedLocationLot.getLot_id());
                jsonValues.put("lot_row", selectedLocationLot.getLot_row());
                jsonValues.put("lot_number", selectedLocationLot.getLot_number());
                jsonValues.put("location_lot_id", selectedLocationLot.getId());
            } else {
                jsonValues.put("lot_id", reserveParking.getLot_row());
                jsonValues.put("lot_row", reserveParking.getLot_row());
                jsonValues.put("lot_number", reserveParking.getLot_number());
                jsonValues.put("location_lot_id", reserveParking.getLocation_lot_id());
            }

            jsonValues.put("parking_units", reserveParking.getParking_units());
            jsonValues.put("parking_qty", reserveParking.getParking_qty());

            jsonValues.put("ipn_custom", "");

            jsonValues.put("location_id", reserveParking.getLocation_id());

            jsonValues.put("distance_to_marker", reserveParking.getDistance_to_marker());
            jsonValues.put("marker_address1", reserveParking.getMarker_address1());
            jsonValues.put("marker_address2", reserveParking.getMarker_address2());
            jsonValues.put("marker_city", reserveParking.getMarker_city());
            jsonValues.put("marker_state", reserveParking.getMarker_state());
            jsonValues.put("marker_zip", reserveParking.getMarker_zip());
            jsonValues.put("marker_country", reserveParking.getMarker_country());
            jsonValues.put("token", 0);
            jsonValues.put("mismatch", reserveParking.getMismatch());
            jsonValues.put("marker_lng", reserveParking.getMarker_lng());
            jsonValues.put("marker_lat", reserveParking.getMarker_lat());
            jsonValues.put("ticket_status", "");
            jsonValues.put("date_time", "");
            jsonValues.put("duration_unit", reserveParking.getDuration_unit());

            jsonValues.put("selected_duration", reserveParking.getSelected_duration());
            jsonValues.put("pl_country", reserveParking.getPl_country());
            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + pov2;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   addParkedCar:  Response: " + responseStr);
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id1 = c.getString("id");

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   addParkedCar:  Error: " + e.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   addParkedCar:  Error: " + e.getMessage());
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rlProgressbar != null)
                rlProgressbar.setVisibility(View.GONE);
            //progressBar.setVisibility(View.GONE);
            if (getActivity() != null && json1 != null) {
                if (!id1.equals("null")) {
                    new updateReserve().execute();
                    if (user_id.equals("null")) {
                        ArrayList<item> arraylist = new ArrayList<>();
                        SharedPreferences sharedPrefs = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
                        Gson gson = new Gson();
                        String json = sharedPrefs.getString("car", null);
                        Type type = new TypeToken<ArrayList<item>>() {
                        }.getType();
                        arraylist = gson.fromJson(json, type);
                        if (arraylist == null) {
                            arraylist = new ArrayList<>();
                        }
                        item ii = new item();
                        ii.setPlate_no(reserveParking.getPlate_no());
                        ii.setId(id1);
                        arraylist.add(ii);

                        SharedPreferences sharedPrefs1 = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPrefs1.edit();
                        Gson gson1 = new Gson();
                        String json1 = gson1.toJson(arraylist);
                        editor.putString("car", json1);
                        editor.commit();
                    }

                    if (!TextUtils.isEmpty(reserveParking.getParking_type())) {
                        if (reserveParking.getParking_type().contains("managed")
                                || reserveParking.getParking_type().contains("Managed")) {
                            //update lot
                            new updateLocationLot(id1).execute();
                        } else {
                            //get parked cars
                            new getparked_car(id1).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    } else {
                        //get parked cars
                        new getparked_car(id1).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class getparked_car extends AsyncTask<String, String, String> {
        String park_id;
        JSONObject json;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String login_id = logindeatl.getString("id", "0");
        String transactionurl = "_table/parked_cars?filter=(id%3D" + park_id + ")";
        String id = "null";

        public getparked_car(String id) {
            this.park_id = id;
            transactionurl = "_table/parked_cars?filter=(id%3D" + park_id + ")";
        }

        @Override
        protected void onPreExecute() {
            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
//            if (params.length > 0) {
//                transactionurl = "_table/parked_cars?filter=(id%3D" + params[0] + ")";
//            }

            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            Log.e("#DEBUG", "   getparked_car:  URL:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                        String plnp = c.getString("plate_no");
                        String state = c.getString("pl_state");
                        String state1 = c.getString("state");

                        String city = c.getString("city");
                        String country = c.getString("country");
                        String exit = c.getString("exit_date_time");
                        String extime = c.getString("expiry_time");
                        String max = c.getString("max_duration");
                        String locationcode = c.getString("location_code");
                        String entrytime = c.getString("entry_date_time");
                        String addres = c.getString("marker_address1");
                        String parked_adress = c.getString("address1");
                        String lat = c.getString("lat");
                        String lang = c.getString("lng");
                        String lot = c.getString("lot_number");
                        String lot_row = c.getString("lot_row");
                        String parking_type = c.getString("parking_type");
                        String duration_unit = c.getString("duration_unit");
                        String locationCode = c.getString("location_code");
                        String locationName = c.getString("location_name");
                        String entryDateTime = c.getString("entry_date_time");
                        String exitDateTime = c.getString("exit_date_time");
                        String lotId = c.getString("lot_id");
                        SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sh.edit();
                        editor.putString("platno", plnp);
                        editor.putString("state", state);
                        editor.putString("currentdate", entrytime);
                        editor.putString("exip", extime);
                        editor.putString("hr", "0");
                        editor.putString("max", max);
                        editor.putString("lat", lat);
                        editor.putString("log", lang);
                        editor.putString("min", duration_unit);
                        editor.putString("id", id);
                        editor.putString("token", "");
                        editor.putString("parked_address", parked_adress);
                        editor.putString("country", country);
                        editor.putString("city", city);
                        editor.putString("exit_date_time", exit);
                        editor.putString("address", addres);
                        editor.putString("locationname", locationcode);
                        editor.putString("markertye", marker);
                        editor.putString("title", title);
                        editor.putString("lot_row", lot);
                        editor.putString("lot_no", lot_row);
                        editor.putString("parking_type", parking_type);
                        editor.putString("parkhere", "no");
                        editor.putString("parking_free", "no");
                        editor.putString("parked_time", c.getString("selected_duration"));
                        editor.putString("entryTime", entryDateTime);
                        editor.putString("exitTime", exitDateTime);
                        editor.putString("locationName", locationName);
                        editor.putString("locationCode", locationCode);
                        editor.putString("lotId", lotId);
                        editor.commit();


                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   Error: " + e.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   Error: " + e.getMessage());
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rlProgressbar != null)
                rlProgressbar.setVisibility(View.GONE);
            //progressBar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json != null) {

                if (!id.equals("null")) {
                    if (renew.equals("yes")) {
                        open_timer();
                    } else {
                        direct_open_timer(park_id);
                    }
                }
            }
            super.onPostExecute(s);

        }
    }

    public class updateLocationLot extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String exit_status = "_table/location_lot";
        JSONObject json, json1;
        String re_id;
        String id;

        public updateLocationLot(String idd) {
            this.id = idd;
        }

        @Override
        protected void onPreExecute() {

            if (!mIsReservationCanceled)
                rlProgressbar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues = new HashMap<String, Object>();
            if (!mIsReservationCanceled) {
                jsonValues.put("id", selectedLocationLot.getId());
                jsonValues.put("plate_no", reserveParking.getPlate_no());
                jsonValues.put("plate_state", reserveParking.getState());
                jsonValues.put("occupied", "YES");
            } else {
                jsonValues.put("id", reserveParking.getLocation_lot_id());
                jsonValues.put("is_reserved", 0);
            }
            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            Log.e("managed_url", url);
            Log.e("parama", String.valueOf(json));

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.e("#DEBUG", "  updateManageLot: ResponseID: " + re_id);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rlProgressbar != null)
                rlProgressbar.setVisibility(View.GONE);

            if (getActivity() != null && json1 != null) {
                if (!re_id.equals("null")) {
                    Log.e("#DEBUG", "   Location lot updated");
                    if (!mIsReservationCanceled)
                        new getparked_car(id).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                super.onPostExecute(s);
            }
        }
    }

    public void open_timer() {
        SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = time.edit();
        ed.putString("result", "true");
        ed.commit();
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.replace(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
        ft.commit();
    }


    public void direct_open_timer(final String park_id) {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String login_id = logindeatl.getString("id", "0");
        if (!login_id.equals("0")) {

            if (!park_id.equals("null")) {
                            /*final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.replace(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                            ft.commitAllowingStateLoss();*/
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                timervehicleinfo pay = new timervehicleinfo();
                fragmentStack.lastElement().onPause();
                ft.add(R.id.My_Container_1_ID, pay, "timer");
                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            } else {
                SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
                SharedPreferences.Editor ed = time.edit();
                ed.putString("result", "true");
                ed.commit();
                           /* final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.add(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                            ft.commitAllowingStateLoss();*/
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                timervehicleinfo pay = new timervehicleinfo();
                fragmentStack.lastElement().onPause();
                ft.add(R.id.My_Container_1_ID, pay, "timer");
                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();

            }
        } else {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Parking Notifications");
            alertDialog.setMessage("Do you want to register for parking notifications");
            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ((vchome) getActivity()).open_login_screen();

                }
            });
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (!park_id.equals("null")) {
                                   /* final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                    ft.replace(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                                    ft.commitAllowingStateLoss();*/

                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        timervehicleinfo pay = new timervehicleinfo();
                        fragmentStack.lastElement().onPause();
                        ft.add(R.id.My_Container_1_ID, pay, "timer");
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                    } else {
                        SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
                        SharedPreferences.Editor ed = time.edit();
                        ed.putString("result", "true");
                        ed.commit();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        timervehicleinfo pay = new timervehicleinfo();
                        fragmentStack.lastElement().onPause();
                        ft.add(R.id.My_Container_1_ID, pay, "timer");
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                                     /*  final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                    ft.add(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                                    ft.commitAllowingStateLoss();*/

                    }
                }
            });
            alertDialog.show();
        }

    }

    public class updateReserve extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String rid = logindeatl.getString("id", "null");
        String id1 = "null";
        JSONObject json, json1;
        String pov2 = "_table/reserved_parkings";
        //        String pov2 = "_table/sdafga";
        boolean isPayLater = false;
        String paramOption = "";
        private String dueBy = "";
        SharedPreferences logindeatl1 = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl1.getString("id", "null");

        @Override
        protected void onPreExecute() {
            if (isUpdateMyVehicle || isCancelReservation) {
                rlProgressbar.setVisibility(View.VISIBLE);
            }
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            currentdate = sdf.format(new Date());
            if (params.length != 0) {
                paramOption = params[0];
            }
            isPayLater = !TextUtils.isEmpty(paramOption) && paramOption.equals("payLater");
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", reserveParking.getId());
            if (isUpdateMyVehicle) {
                jsonValues.put("plate_no", reserveParking.getPlate_no());
                jsonValues.put("pl_state", reserveParking.getPl_state());
                jsonValues.put("pl_country", reserveParking.getPl_country());
                jsonValues.put("modified_time", sdf.format(new Date()));
            } else if (isCancelReservation) {
                jsonValues.put("reservation_status", "CANCELLED");
                jsonValues.put("parking_status", "CANCELLED");
                jsonValues.put("cancelled_date_time", currentdate);
            } else {
                jsonValues.put("parking_status", "ENTRY");
                jsonValues.put("reservation_status", "PARKED");
                jsonValues.put("entry_date_time", currentdate);
                jsonValues.put("exit_date_time", reserveParking.getReserve_exit_date_time());
                jsonValues.put("expiry_time", reserveParking.getReserve_expiry_time());

                if (selectedLocationLot != null) {
                    jsonValues.put("lot_id", selectedLocationLot.getLot_id());
                    jsonValues.put("lot_row", selectedLocationLot.getLot_row());
                    jsonValues.put("lot_number", selectedLocationLot.getLot_number());
                    jsonValues.put("location_lot_id", selectedLocationLot.getId());
                }
                if (mIsPaymentPaid) {
                    jsonValues.put("reserve_payment_paid", "PAID");
                }
            }
            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + pov2;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut httpPut = new HttpPut(url);
            httpPut.setHeader("X-DreamFactory-Application-Name", "parkezly");
            httpPut.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            httpPut.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(httpPut);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "    reserveNow:  Response:  " + responseStr);
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id1 = c.getString("id");

                    }
                } catch (IOException e) {
                    Log.e("#DEBUG", "    reserveNow:  Error:  " + e.getMessage());
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.e("#DEBUG", "    reserveNow:  Error:  " + e.getMessage());
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            isUpdated = true;
            if (getActivity() != null && (isUpdateMyVehicle || isCancelReservation)) {
                rlProgressbar.setVisibility(View.GONE);
                if (isCancelReservation) {
                    mIsReservationCanceled = true;
                    if (!TextUtils.isEmpty(reserveParking.getLocation_lot_id())) {
                        new updateLocationLot("").execute();
                    }
                    isCancelReservation = false;
                    mIsProcessCancel = true;
                    if (mIsPaymentPaid) {
                        new getwalletbal().execute();
                    } else {
                        Toast.makeText(getActivity(), "Reservation Canceled Successfully!", Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();
                    }
                } else {
                    Toast.makeText(getActivity(), "Plat # Updated Successfully!", Toast.LENGTH_SHORT).show();
                    isUpdateMyVehicle = false;
                }
            }
            super.onPostExecute(s);
        }
    }

    @Override
    protected void initViews(View v) {

        tvReservationTime = v.findViewById(R.id.tvReservationTime);
        tvReservationEntryTime = v.findViewById(R.id.tvReservationEntryTime);
        tvReservationExitTime = v.findViewById(R.id.tvReservationExitTime);
        tvPaymentStatus = v.findViewById(R.id.tvPaymentStatus);
        tvReservationStatus = v.findViewById(R.id.tvReservationStatus);
        tvAddress = v.findViewById(R.id.tvAddress);
        tvParkingStatus = v.findViewById(R.id.tvParkingStatus);
        tvBtnParkNow = v.findViewById(R.id.tvBtnParkNow);
        tvBtnDone = v.findViewById(R.id.tvBtnDone);
        tvBtnRenew = v.findViewById(R.id.tvBtnRenew);
        rlProgressbar = v.findViewById(R.id.rlProgressbar);
        tvPaymentDueBy = v.findViewById(R.id.tvPaymentDueBy);
        tvPlatNo = v.findViewById(R.id.tvPlatNo);
        tvAmountPaid = v.findViewById(R.id.tvAmountPaid);
        tvAmountDue = v.findViewById(R.id.tvAmountDue);
        tvLocationName = v.findViewById(R.id.tvLocationName);
        tvLocationCode = v.findViewById(R.id.tvLocationCode);
        tvLocationTitle = v.findViewById(R.id.tvLocationTitle);
        tvReservationID = v.findViewById(R.id.tvReservationID);
        tvLotId = v.findViewById(R.id.tvLotId);
        nestedScrollView = v.findViewById(R.id.nestedScrollView);
        llReservationOptions = v.findViewById(R.id.llReservationOptions);

        tvBtnEditReservation = v.findViewById(R.id.tvBtnEditReservation);
        tvBtnCancelReservation = v.findViewById(R.id.tvBtnCancelReservation);
        tvBtnDirection = v.findViewById(R.id.tvBtnDirection);
        tvBtnStreetView = v.findViewById(R.id.tvBtnStreetView);

        showsucessmesg = v.findViewById(R.id.sucessfully);

        tvPlatNo.setPaintFlags(tvPlatNo.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        Calendar calendar = Calendar.getInstance();
        currentTime = calendar.getTimeInMillis();

        tvBtnRenew.setVisibility(View.GONE);

    }

    private class MyVehicleAdapter extends RecyclerView.Adapter<MyVehicleAdapter.MyVehicleHolder> {

        @NonNull
        @Override
        public MyVehicleAdapter.MyVehicleHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new MyVehicleHolder(LayoutInflater.from(getActivity())
                    .inflate(R.layout.item_my_vehicle, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyVehicleAdapter.MyVehicleHolder holder, int i) {
            item myVehicle = myVehicles.get(i);
            if (getActivity() != null && myVehicle != null) {
                Typeface light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Light.otf");
                holder.tvVehicleNo.setTypeface(light);
                holder.tvVehicleState.setTypeface(light);
                holder.tvVehicleNo.setText(myVehicle.getPlantno());
                holder.tvVehicleState.setText(myVehicle.getState());

                if (i % 2 == 0) {
                    holder.rlMain.setBackgroundColor(Color.parseColor("#ffffff"));
                } else {
                    holder.rlMain.setBackgroundColor(Color.parseColor("#e0e0e0"));
                }
            }

        }

        @Override
        public int getItemCount() {
            return myVehicles.size();
        }

        class MyVehicleHolder extends RecyclerView.ViewHolder {
            TextView tvVehicleNo, tvVehicleState;
            RelativeLayout rlMain;

            MyVehicleHolder(@NonNull View itemView) {
                super(itemView);
                tvVehicleNo = itemView.findViewById(R.id.tvVehicleNo);
                tvVehicleState = itemView.findViewById(R.id.tvVehicleState);
                rlMain = itemView.findViewById(R.id.rlMain);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            isUpdateMyVehicle = true;
                            if (!TextUtils.isEmpty(myVehicles.get(position).getPlantno())
                                    && !TextUtils.isEmpty(myVehicles.get(position).getState())) {
                                reserveParking.setPlate_no(myVehicles.get(position).getPlantno());
                                reserveParking.setPl_state(myVehicles.get(position).getState());
                                reserveParking.setPl_country(myVehicles.get(position).getVehicle_country());
                                tvPlatNo.setText(String.format("%s(%s)",
                                        myVehicles.get(position).getPlantno(), myVehicles.get(position).getState()));
                            } else {
                                tvPlatNo.setText("Select Vehicle");
                            }
                            if (alertDialogMyVehicles != null && alertDialogMyVehicles.isShowing()) {
                                alertDialogMyVehicles.dismiss();
                            }

                            new updateReserve().execute();
                        }
                    }
                });
            }
        }
    }

    private class LocationLotAdapter extends RecyclerView.Adapter<LocationLotAdapter.LocationLotHolder> {
        private Drawable drawableRed, drawableBlue, drawableGreen, drawableGrey;

        @NonNull
        @Override
        public LocationLotAdapter.LocationLotHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            drawableRed = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_red);
            drawableBlue = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_blue);
            drawableGreen = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_green);
            drawableGrey = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_gree_car);
            return new LocationLotHolder(LayoutInflater.from(getActivity())
                    .inflate(R.layout.item_location_lot, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull LocationLotAdapter.LocationLotHolder locationLotHolder, int i) {
            LocationLot locationLot = null;
            if (isRenew) {
                locationLot = locationLotsReservable.get(i);
            } else {
                locationLot = locationLots.get(i);
            }
            locationLotHolder.tvReserved.setVisibility(View.GONE);
            locationLotHolder.ivReserved.setVisibility(View.GONE);
            locationLotHolder.ivReservedOnly.setVisibility(View.GONE);
            locationLotHolder.ivPremium.setVisibility(View.GONE);
            locationLotHolder.ivHandicapped.setVisibility(View.GONE);
            if (locationLot != null) {
                if (!TextUtils.isEmpty(locationLot.getLot_id())) {
                    locationLotHolder.tvRow.setText(locationLot.getLot_id());
                }

                if (!TextUtils.isEmpty(locationLot.getOccupied())) {
                    if (locationLot.getOccupied().equalsIgnoreCase("yes")) {
                        //Car already parked at this location
                        if (locationLot.isExpired()) {
                            //RED, parking expired
                            locationLotHolder.ivCar.setImageDrawable(drawableRed);
                        } else {
                            //GREEN, parking valid
                            locationLotHolder.ivCar.setImageDrawable(drawableGreen);
                        }
                        if (locationLot.isLot_reservable()) {
//                            locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                        }
                    } else if (locationLot.getOccupied().equalsIgnoreCase("no")) {
                        if (locationLot.isLot_reservable()) {
                            //GREY with R, Available for reservation
                            locationLotHolder.ivCar.setImageDrawable(drawableGrey);
//                            locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                        } else {
                            //GREY, Available for parking
                            locationLotHolder.ivCar.setImageDrawable(drawableGrey);
                        }

                    } else if (locationLot.getOccupied().equalsIgnoreCase("reserved")) {
                        //BLUE with R, Reserved for parking
                        locationLotHolder.ivCar.setImageDrawable(drawableBlue);
//                        locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (locationLot.isLot_reservable()) {
                        //GREY with R, Available for reservation
                        locationLotHolder.ivCar.setImageDrawable(drawableGrey);
//                        locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                    } else {
                        //GREY, Available for parking
                        locationLotHolder.ivCar.setImageDrawable(drawableGrey);
                    }
                }

                if (locationLot.isIs_reserved()) {
                    locationLotHolder.ivCar.setImageDrawable(drawableBlue);
                    locationLotHolder.viewIsReserved.setVisibility(View.VISIBLE);
                } else {
                    locationLotHolder.viewIsReserved.setVisibility(View.GONE);
//                    locationLotHolder.ivCar.setImageDrawable(drawableGrey);
                }

                if (locationLot.isLot_reservable()) {
                    locationLotHolder.ivReserved.setVisibility(View.VISIBLE);
                } else {
                    locationLotHolder.ivReserved.setVisibility(View.GONE);
                }

                if (locationLot.isPremium_lot())
                    locationLotHolder.ivPremium.setVisibility(View.VISIBLE);
                else locationLotHolder.ivPremium.setVisibility(View.GONE);

                if (locationLot.isSpecial_need_handi_lot())
                    locationLotHolder.ivHandicapped.setVisibility(View.VISIBLE);
                else locationLotHolder.ivHandicapped.setVisibility(View.GONE);

                if (locationLot.isLot_reservable_only()) {
                    locationLotHolder.ivReservedOnly.setVisibility(View.VISIBLE);
                } else locationLotHolder.ivReservedOnly.setVisibility(View.GONE);
            }

        }

        @Override
        public int getItemCount() {
            if (isRenew) {
                return locationLotsReservable.size();
            } else return locationLots.size();
        }

        class LocationLotHolder extends RecyclerView.ViewHolder {
            ImageView ivCar, ivReserved, ivPremium, ivHandicapped, ivReservedOnly;
            TextView tvRow, tvReserved;
            View viewIsReserved;

            LocationLotHolder(@NonNull View itemView) {
                super(itemView);

                ivCar = itemView.findViewById(R.id.ivCar);
                tvRow = itemView.findViewById(R.id.tvRow);
                tvReserved = itemView.findViewById(R.id.tvReserved);
                viewIsReserved = itemView.findViewById(R.id.viewIsReserved);
                ivReserved = itemView.findViewById(R.id.ivReserved);
                ivPremium = itemView.findViewById(R.id.ivPremium);
                ivHandicapped = itemView.findViewById(R.id.ivHandicapped);
                ivReservedOnly = itemView.findViewById(R.id.ivReservedOnly);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
//                            selectedLocationLot = locationLots.get(position);
                            if (isRenew) {
                                //Renew reservation
                                if (locationLotsReservable.get(position).isIs_reserved()) {
                                    Toast.makeText(getActivity(), "Already reserved!", Toast.LENGTH_SHORT).show();
                                } else {
                                    if (selectedLocationLot != null) {
                                        locationLotsMain.get(locationLotsReservable.indexOf(
                                                new LocationLot(selectedLocationLot.getId()))).setOccupied("no");
                                        selectedLocationLot = null;
//                                txt_row.setText("Select Space");
                                    }
                                    if (!TextUtils.isEmpty(locationLotsReservable.get(position).getOccupied())) {
                                        if (locationLotsReservable.get(position).getOccupied().equalsIgnoreCase("no")) {
                                            locationLotsReservable.get(position).setOccupied("yes");
                                            selectedLocationLot = locationLotsReservable.get(position);
//                                    txt_row.setText(selectedLocationLot.getLot_id());
                                            notifyItemChanged(position);
                                            if (dialogLocationLot != null && dialogLocationLot.isShowing()) {
                                                dialogLocationLot.dismiss();
                                                showReNewConfirmParkingDialog();
                                            }
                                        } else {
                                            if (selectedLocationLot != null &&
                                                    selectedLocationLot.getId() == locationLotsReservable.get(position).getId()) {
                                                selectedLocationLot = null;
                                                locationLotsReservable.get(position).setOccupied("no");
                                                if (dialogLocationLot != null && dialogLocationLot.isShowing()) {
                                                    dialogLocationLot.dismiss();
                                                    showReNewConfirmParkingDialog();
                                                }
                                            } else {
                                                Toast.makeText(getActivity(), "Already occupied!", Toast.LENGTH_SHORT).show();
                                            }
//                                    locationLotsReservable.get(position).setOccupied("no");
                                        }
                                    } else {
                                        locationLotsReservable.get(position).setOccupied("yes");
                                        selectedLocationLot = locationLotsReservable.get(position);
//                                txt_row.setText(selectedLocationLot.getLot_id());
                                        notifyItemChanged(position);
                                        if (dialogLocationLot != null && dialogLocationLot.isShowing()) {
                                            dialogLocationLot.dismiss();
                                            showReNewConfirmParkingDialog();
                                        }
                                    }
                                    Log.e("#DEBUG", "     onClickParkNow:  selectedLocationLot:  "
                                            + new Gson().toJson(selectedLocationLot));
                                    notifyItemChanged(position);
                                }
                            } else {
                                //Parking
                                if (selectedLocationLot != null) {
                                    locationLots.get(locationLots.indexOf(
                                            new LocationLot(selectedLocationLot.getId()))).setOccupied("no");
                                    selectedLocationLot = null;
//                                txt_row.setText("Select Space");
                                }
                                if (!TextUtils.isEmpty(locationLots.get(position).getOccupied())) {
                                    if (locationLots.get(position).getOccupied().equalsIgnoreCase("no")) {
                                        locationLots.get(position).setOccupied("yes");
                                        selectedLocationLot = locationLots.get(position);
//                                    txt_row.setText(selectedLocationLot.getLot_id());
                                        notifyItemChanged(position);
                                        if (dialogLocationLot != null && dialogLocationLot.isShowing()) {
                                            dialogLocationLot.dismiss();
                                            showConfirmParkingDialog();
                                        }
                                    } else {
                                        if (selectedLocationLot != null &&
                                                selectedLocationLot.getId() == locationLots.get(position).getId()) {
                                            selectedLocationLot = null;
                                            locationLots.get(position).setOccupied("no");
                                            if (dialogLocationLot != null && dialogLocationLot.isShowing()) {
                                                dialogLocationLot.dismiss();
                                                showConfirmParkingDialog();
                                            }
                                        } else {
                                            Toast.makeText(getActivity(), "Already occupied!", Toast.LENGTH_SHORT).show();
                                        }
//                                    locationLots.get(position).setOccupied("no");
                                    }
                                } else {
                                    locationLots.get(position).setOccupied("yes");
                                    selectedLocationLot = locationLots.get(position);
//                                txt_row.setText(selectedLocationLot.getLot_id());
                                    notifyItemChanged(position);
                                    if (dialogLocationLot != null && dialogLocationLot.isShowing()) {
                                        dialogLocationLot.dismiss();
                                        showConfirmParkingDialog();
                                    }
                                }
                                Log.e("#DEBUG", "     onClickParkNow:  selectedLocationLot:  "
                                        + new Gson().toJson(selectedLocationLot));
                                notifyItemChanged(position);
                            }
                        }
                    }
                });
            }
        }
    }

    private void showReNewConfirmParkingDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View dialogView = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_confirm_renew_reservation, null);
        builder.setView(dialogView);
        TextView tvSelectedTime = dialogView.findViewById(R.id.tvSelectedTime);
        TextView tvExitTime = dialogView.findViewById(R.id.tvExitTime);
        TextView tvLotSpace = dialogView.findViewById(R.id.tvLotSpace);
        TextView tvBtnCancel = dialogView.findViewById(R.id.tvBtnCancel);
        TextView tvBtnConfirm = dialogView.findViewById(R.id.tvBtnConfirm);
        if (isEditReservation) {
            tvBtnConfirm.setText("UPDATE");
        } else {
            tvBtnConfirm.setText("RENEW");
        }
        AlertDialog alertDialog = builder.create();

        tvSelectedTime.setText(reserveEntryTime);
        tvExitTime.setText(reserveExpiryTime);
        tvLotSpace.setText(selectedLocationLot.getLot_id());

        tvBtnCancel.setOnClickListener(v -> alertDialog.dismiss());

        tvBtnConfirm.setOnClickListener(v -> {
            alertDialog.dismiss();

            if (isRenewReservationClicked) {
                PaymentOptionDialog dialog = PaymentOptionDialog.newInstance();
                Bundle bundle = new Bundle();
                bundle.putBoolean("mIsPayLater", false);
                dialog.setArguments(bundle);
                dialog.show(getChildFragmentManager(), "payment");
            } else {
                new renewReservation().execute();
            }
        });

        alertDialog.show();
    }
}
