package com.softmeasures.eventezlyadminplus.frament.event_reg;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragEventRegistrationBeginBinding;

public class EventRegistrationBeginFragment extends BaseFragment {

    private FragEventRegistrationBeginBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_event_registration_begin, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {

        binding.llBtnByOrganization.setOnClickListener(v -> {
            // SEARCH BY ORGANIZATION
            replaceFragment(new EventRegByOrgFragment(), "eventRegByOrg");
        });

        binding.llBtnByType.setOnClickListener(v -> {
            //SEARCH BY TYPE
            replaceFragment(new EventRegByTypeFragment(), "eventRegByType");
        });

        binding.llBtnByLocation.setOnClickListener(v -> {
            //SEARCH BY LOCATION
            Fragment fragment = new EventsMapFragment();
            Bundle bundle = new Bundle();
            bundle.putString("parkingType", "All");
            fragment.setArguments(bundle);
            replaceFragment(fragment, "eventMap");
        });

    }

    @Override
    protected void initViews(View v) {

    }
}
