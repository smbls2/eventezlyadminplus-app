package com.softmeasures.eventezlyadminplus.frament.event_reg;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragEventRegByTypeBinding;
import com.softmeasures.eventezlyadminplus.models.EventType;
import com.softmeasures.eventezlyadminplus.models.Manager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_LOCATION;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_VIRTUALLY;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_BOTH;

public class EventRegByTypeFragment extends BaseFragment {

    private FragEventRegByTypeBinding binding;
    private int eventLogiType = EVENT_TYPE_AT_LOCATION;
    private String parkingType = "TOWNSHIP";
    private String location = "";
    private ArrayList<Manager> managers = new ArrayList<>();
    private Manager selectedManager;
    private ArrayAdapter<Manager> eventManagerAdapter;

    private ArrayList<EventType> eventTypes = new ArrayList<>();
    private ArrayAdapter<EventType> eventTypeArrayAdapter;
    private String selectedEventType = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_event_reg_by_type, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        new fetchEventTypes().execute();

//        new fetchManagers().execute();
    }

    private class fetchEventTypes extends AsyncTask<String, String, String> {
        JSONObject json;
        String eventDefUrl = "_table/event_type";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            eventTypes.clear();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + eventDefUrl;
            Log.e("#DEBUG", "      fetchEventType:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");

                    for (int j = 0; j < jsonArray.length(); j++) {
                        EventType eventType = new EventType();
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            eventType.setId(object.getInt("id"));
                        }
                        if (object.has("event_type")
                                && !TextUtils.isEmpty(object.getString("event_type"))
                                && !object.getString("event_type").equals("null")) {
                            eventType.setEvent_type(object.getString("event_type"));
                        }
                        if (object.has("icon_event_type_link")
                                && !TextUtils.isEmpty(object.getString("icon_event_type_link"))
                                && !object.getString("icon_event_type_link").equals("null")) {
                            eventType.setIcon_event_type_link(object.getString("icon_event_type_link"));
                        }

                        eventTypes.add(eventType);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                if (eventTypes.size() != 0) {
                    if (eventTypeArrayAdapter != null) {
                        eventTypeArrayAdapter.notifyDataSetChanged();
                    }
                } else {

                }
            }
        }
    }

    public class fetchManagers extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String parkingurl = "_table/townships_manager";

        @Override
        protected void onPreExecute() {
            if (parkingType.equals("Commercial")) {
                parkingurl = "_table/commercial_manager";
            } else if (parkingType.equals("Other")) {
                parkingurl = "_table/other_private_manager";
            }
            managers.clear();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            managers.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("#DEBUG", "   fetchManagers:  URL:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        Manager manager = new Manager();
                        manager.setId(c.getInt("id"));
                        manager.setDate_time(c.getString("date_time"));
                        manager.setManager_id(c.getString("manager_id"));
                        manager.setManager_type(c.getString("manager_type"));
                        manager.setManager_type_id(c.getInt("manager_type_id"));
                        manager.setLot_manager(c.getString("lot_manager"));
                        manager.setStreet(c.getString("street"));
                        manager.setAddress(c.getString("address"));
                        manager.setState(c.getString("state"));
                        manager.setState_name(c.getString("state_name"));
                        manager.setCity(c.getString("city"));
                        manager.setCountry(c.getString("country"));
                        manager.setZip(c.getString("zip"));
                        manager.setContact_person(c.getString("contact_person"));
                        manager.setContact_title(c.getString("contact_title"));
                        manager.setContact_phone(c.getString("contact_phone"));
                        manager.setContact_email(c.getString("contact_email"));
                        manager.setTownship_logo(c.getString("township_logo"));
                        manager.setOfficial_logo(c.getString("official_logo"));
                        manager.setUser_id(c.getInt("user_id"));
                        managers.add(manager);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null) {
                if (managers.size() != 0) {
                    eventManagerAdapter.notifyDataSetChanged();
                }
            }
            super.onPostExecute(s);
        }
    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {

        binding.spinnerEventTypeMain.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedEventType = eventTypes.get(position).getEvent_type();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spinnerEventType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        eventLogiType = EVENT_TYPE_AT_LOCATION;
                        break;
                    case 1:
                        eventLogiType = EVENT_TYPE_AT_VIRTUALLY;
                        break;
                    case 2:
                        eventLogiType = EVENT_TYPE_BOTH;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spinnerOrganization.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        parkingType = "Township";
                        new fetchManagers().execute();
                        break;
                    case 1:
                        parkingType = "Commercial";
                        new fetchManagers().execute();
                        break;
                    case 2:
                        parkingType = "Other";
                        new fetchManagers().execute();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spinnerLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (managers.size() > 0) {
                    selectedManager = managers.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.tvBtnSearch.setOnClickListener(v -> {
            if (selectedManager == null) {
                Toast.makeText(getActivity(), "Please select location!", Toast.LENGTH_SHORT).show();
                return;
            } else if (TextUtils.isEmpty(selectedEventType)) {
                Toast.makeText(getActivity(), "Please select event type!", Toast.LENGTH_SHORT).show();
                return;
            }
            EventRegByOrgEventsFragment reg = new EventRegByOrgEventsFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("byType", true);
            bundle.putInt("eventLogiType", eventLogiType);
            bundle.putString("type", parkingType);
            bundle.putString("selectedManager", new Gson().toJson(selectedManager));
            bundle.putString("eventType", selectedEventType);
            reg.setArguments(bundle);
            replaceFragment(reg, "eventRegByTypeEvents");
        });

    }

    @Override
    protected void initViews(View v) {

        ArrayAdapter<String> eventTypeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,
                Arrays.asList(getResources().getStringArray(R.array.event_type)));
        binding.spinnerEventType.setAdapter(eventTypeAdapter);

        ArrayAdapter<String> eventOrgnAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,
                Arrays.asList(getResources().getStringArray(R.array.organization)));
        binding.spinnerOrganization.setAdapter(eventOrgnAdapter);

        eventManagerAdapter = new ArrayAdapter<Manager>(getActivity(), android.R.layout.simple_spinner_dropdown_item,
                managers);
        binding.spinnerLocation.setAdapter(eventManagerAdapter);

        eventTypeArrayAdapter = new ArrayAdapter<EventType>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, eventTypes);
        binding.spinnerEventTypeMain.setAdapter(eventTypeArrayAdapter);

    }
}
