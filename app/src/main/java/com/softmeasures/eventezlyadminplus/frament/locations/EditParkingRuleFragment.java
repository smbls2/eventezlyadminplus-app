package com.softmeasures.eventezlyadminplus.frament.locations;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.MyApplication;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.java.Array_class_static;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.ParkingRule;
import com.softmeasures.eventezlyadminplus.models.PaymentOption;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.softmeasures.eventezlyadminplus.frament.locations.ParkingRulesFragment.isUpdated;

public class EditParkingRuleFragment extends BaseFragment {
    Animation animation;
    TextView txt_day_list, txt_pricing_duation, txt_parking_rules_save;
    RelativeLayout rl_private_day_list;
    ImageView img_private_day_list_close;
    ListView list_private_day;
    int checkedisone = 0;
    Spinner sp_duraion, sp_pricing_duration;
    EditText edit_duration_unit, edit_pricing, edit_start_time, edit_end_time, edit_pricig_duation;
    Switch switch_24hr, switch_active;
    public String max_duation = "", duation_unit = "", pricing_duation = "", pricing = "", custom_notice = "", start_timer, end_time, private_start_hour, private_end_hours, str_day_list, marker_type, parking_rules_id, private_place_id = "";
    boolean isactive = true, is24hr = true, private_in_active = true, private_renew = true, private_in_effect = true;
    LinearLayout ll_strat_time, ll_end_time;
    EditText edit_custom_notice, edit_max;
    Spinner sp_private_start_hour, sp_private_end_hour;
    RelativeLayout rl_progressbar;
    boolean is_direct_edit = false;
    RelativeLayout rl_main;

    ArrayList<item> day_list = new ArrayList<>();

    //Additional rules START ############
    private Switch switchReservation, switchPrePayment, switchPostPayment, switchCancellation,
            switchVerifyLotsAvailability, switchCountReservedSpots;
    public boolean isReservationAllowed = false, isPrePaymentRequired = true,
            isPostPaymentAllowed = false, isCancellationAllowed = false,
            isVerifyLotAvailability = false, isCountReservedSpot = false;
    private EditText etCancellationCharge, etPostPayLateFeesReservation, etPostPayLateFees,
            etNoOfReservableSpots, etPostPayFeesReservation, etPostPayFees;
    private LinearLayout llCancellationCharge, llPostPaymentTermsReservation,
            llPostPayLateFeeReservation, llPostPayLateFee, llPostPayTerms, llReservationOption,
            llNoOfReservableSpots, llPostPayFeeReservation, llPostPayFee;
    public String cancellationCharge = "", postPayLateFeesReservation = "",
            postPayLateFees = "", postPayTerm = "", postPayTermReservation = "",
            postPayFeesReservation = "", noOfReservationSpots = "", postPayFees = "";
    private AppCompatSpinner spPostPaymentTermsReservation, spPostPaymentTerms;
    private ArrayList<PaymentOption> paymentOptions = new ArrayList<>();
    private ArrayAdapter<PaymentOption> postPayTermsAdapter;
    private ArrayAdapter<PaymentOption> postPayTermsReservationAdapter;
    private boolean isEdit = true;
    //Additional rules END ############

    private Switch switchInEffect;


    private ParkingRule parkingRule;
    private String type = "";
    private boolean inEffect = true;
    private boolean isEventRule = false;
    private EventDefinition eventDefinition;

    private LinearLayout llEventRules;
    private Switch switchMultiDay, switchMultiDayReservation, switchMultiDayParking;
    private boolean isMultiDay = false, isMultiDayReservation = false, isMultiDayParking = false;
    private item location;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getArguments() != null) {
            isEventRule = getArguments().getBoolean("isEventRule", false);
            type = getArguments().getString("type");
            isEdit = getArguments().getBoolean("isEdit", true);
            parkingRule = new Gson().fromJson(getArguments().getString("selectedRule"), ParkingRule.class);
            eventDefinition = new Gson().fromJson(getArguments().getString("eventDefinition"), EventDefinition.class);
            location = new Gson().fromJson(getArguments().getString("location"), item.class);
            if (parkingRule != null) {
                Log.e("#DEBUG", "  EditParkingRuleFragmentL:  parkingRule:  " + new Gson().toJson(parkingRule));
            }
        }
        return inflater.inflate(R.layout.frag_edit_parking_rule, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
        new fetchPaymentOptions().execute();
    }

    @Override
    protected void updateViews() {
        if (isEventRule) {
            llEventRules.setVisibility(View.VISIBLE);
        } else {
            llEventRules.setVisibility(View.GONE);
        }
        if (parkingRule != null && !parkingRule.getStart_hour().equals("null") && !parkingRule.getEnd_hour().equals("null")) {
            start_timer = parkingRule.getStart_hour();
            end_time = parkingRule.getEnd_hour();
            is24hr = false;
            switch_24hr.setChecked(false);
            private_start_hour = start_timer;
            private_end_hours = end_time;
        } else {
            is24hr = true;

        }

        if (isEdit) {
            is_direct_edit = true;
            if (start_timer.equals("null") && end_time.equals("null")) {
                ll_end_time.setVisibility(View.GONE);
                ll_strat_time.setVisibility(View.GONE);
            } else {
                ll_strat_time.setVisibility(View.VISIBLE);
                ll_end_time.setVisibility(View.VISIBLE);
                private_start_hour = start_timer;
                private_end_hours = end_time;
                if (parkingRule != null) {

                    for (int i = 0; i < Array_class_static.start_hour_array.length; i++) {
                        if (parkingRule.getStart_time().equals(Array_class_static.start_hour_array[i])) {
                            sp_private_start_hour.setSelection(i);
                        }
                    }
                    for (int i = 0; i < Array_class_static.end_hour_array.length; i++) {
                        if (parkingRule.getEnd_time().equals(Array_class_static.start_hour_array[i])) {
                            sp_private_end_hour.setSelection(i);
                        }
                    }
                }
            }


            if (parkingRule != null) {
                duation_unit = parkingRule.getDuration_unit();
                pricing_duation = parkingRule.getPricing_duration();
                max_duation = String.valueOf(parkingRule.getMax_duration());
                pricing = parkingRule.getPricing();
                custom_notice = parkingRule.getCustom_notice();
                if (parkingRule.getActive() == 1) {
                    isactive = true;
                } else {
                    isactive = false;
                }
                switch_active.setChecked(isactive);

                if (parkingRule.getIn_effect() == 1) {
                    inEffect = true;
                } else inEffect = false;
                switchInEffect.setChecked(inEffect);

                isCancellationAllowed = parkingRule.isCanCancel_Reservation();
                isPostPaymentAllowed = parkingRule.isParkNow_PostPaymentAllowed();
                isPrePaymentRequired = parkingRule.isPrePymntReqd_for_Reservation();
                isReservationAllowed = parkingRule.isReservationAllowed();

                postPayLateFeesReservation = parkingRule.getReservation_PostPayment_LateFee();

                if (!TextUtils.isEmpty(postPayLateFeesReservation)) {
                    etPostPayLateFeesReservation.setText(postPayLateFeesReservation);
                }

                noOfReservationSpots = String.valueOf(parkingRule.getNumber_of_reservable_spots());
                if (!TextUtils.isEmpty(noOfReservationSpots)) {
                    etNoOfReservableSpots.setText(noOfReservationSpots);
                }

                postPayFeesReservation = String.valueOf(parkingRule.getReservation_PostPayment_Fee());
                if (!TextUtils.isEmpty(postPayFeesReservation)) {
                    etPostPayFeesReservation.setText(postPayFeesReservation);
                }

                postPayLateFees = parkingRule.getParkNow_PostPayment_LateFee();
                if (!TextUtils.isEmpty(postPayLateFees)) {
                    etPostPayLateFees.setText(postPayLateFees);
                }
                postPayFees = String.valueOf(parkingRule.getParkNow_PostPayment_Fee());
                if (!TextUtils.isEmpty(postPayFees)) {
                    etPostPayFees.setText(postPayFees);
                }

                cancellationCharge = parkingRule.getCancellation_Charge();
                if (!TextUtils.isEmpty(cancellationCharge)) {
                    etCancellationCharge.setText(cancellationCharge);
                }
                if (duation_unit.equals("Minute")) {
                    sp_duraion.setSelection(1);
                } else if (duation_unit.equals("Hour")) {
                    sp_duraion.setSelection(2);
                } else if (duation_unit.equals("Day")) {
                    sp_duraion.setSelection(3);
                } else if (duation_unit.equals("Week")) {
                    sp_duraion.setSelection(4);
                } else if (duation_unit.equals("Month")) {
                    sp_duraion.setSelection(5);
                }
                duation_unit = (String) sp_duraion.getSelectedItem();
                setpricing_duation();

                if (!TextUtils.isEmpty(parkingRule.getReservation_PostPayment_term())) {
                    postPayTermReservation = parkingRule.getReservation_PostPayment_term();
                }

                if (!TextUtils.isEmpty(parkingRule.getParkNow_PostPayment_term())) {
                    postPayTerm = parkingRule.getParkNow_PostPayment_term();
                }
            }
            sp_pricing_duration.setSelection(0);
            txt_pricing_duation.setVisibility(View.VISIBLE);
            edit_pricig_duation.setText(pricing_duation);
            txt_pricing_duation.setText(pricing_duation);
            edit_max.setText(max_duation);
            edit_duration_unit.setText(duation_unit);
            edit_pricing.setText(pricing);
            edit_custom_notice.setText(custom_notice);
            edit_start_time.setText(start_timer);
            edit_end_time.setText(end_time);

            isCountReservedSpot = false;
            isVerifyLotAvailability = false;


            switchCountReservedSpots.setChecked(isCountReservedSpot);
            switchVerifyLotsAvailability.setChecked(isVerifyLotAvailability);
            switchCancellation.setChecked(isCancellationAllowed);
            switchPostPayment.setChecked(isPostPaymentAllowed);
            switchPrePayment.setChecked(isPrePaymentRequired);
            switchReservation.setChecked(isReservationAllowed);
            if (isReservationAllowed) {
                llReservationOption.setVisibility(View.VISIBLE);
            } else llReservationOption.setVisibility(View.GONE);

            if (isPrePaymentRequired) {
                llPostPaymentTermsReservation.setVisibility(View.GONE);
                llPostPayLateFeeReservation.setVisibility(View.GONE);
                llPostPayFeeReservation.setVisibility(View.GONE);
            } else {
                llPostPaymentTermsReservation.setVisibility(View.VISIBLE);
                llPostPayLateFeeReservation.setVisibility(View.VISIBLE);
                llPostPayFeeReservation.setVisibility(View.VISIBLE);
            }

            if (isCancellationAllowed) {
                llCancellationCharge.setVisibility(View.VISIBLE);
            } else llCancellationCharge.setVisibility(View.GONE);

        }
    }

    @Override
    protected void setListeners() {
        txt_day_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animation = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.sidepannelright);
                rl_private_day_list.setVisibility(View.VISIBLE);
                rl_private_day_list.startAnimation(animation);
            }
        });

        img_private_day_list_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_private_day_list.setVisibility(View.GONE);
            }
        });

        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        sp_pricing_duration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                pricing_duation = (String) sp_pricing_duration.getSelectedItem();

                if (position != 0) {
                    txt_pricing_duation.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> paren1t) {
            }
        });

        sp_duraion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                duation_unit = (String) sp_duraion.getSelectedItem();
                if (!is_direct_edit) {
                    setpricing_duation();
                } else {
                    is_direct_edit = false;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> paren1t) {
            }
        });

        switch_24hr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                is24hr = isChecked;
                if (isChecked) {
                    ll_strat_time.setVisibility(View.GONE);
                    ll_end_time.setVisibility(View.GONE);
                } else {
                    ll_strat_time.setVisibility(View.VISIBLE);
                    ll_end_time.setVisibility(View.VISIBLE);
                }
                setpricing_duation();
            }
        });
        switch_active.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                isactive = isChecked;
            }
        });

        switchInEffect.setOnCheckedChangeListener((buttonView, isChecked) -> {
            inEffect = isChecked;
        });


        sp_private_start_hour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                private_start_hour = (String) sp_private_start_hour.getSelectedItem();
                start_timer = private_start_hour;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_private_end_hour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                private_end_hours = (String) sp_private_end_hour.getSelectedItem();
                end_time = private_end_hours;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        txt_parking_rules_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Toast.makeText(getActivity(), "Coming soon!", Toast.LENGTH_SHORT).show();

                max_duation = edit_max.getText().toString();
                pricing = edit_pricing.getText().toString();
                custom_notice = edit_custom_notice.getText().toString();

                str_day_list = txt_day_list.getText().toString();

                String pricing_duation1 = edit_pricig_duation.getText().toString();
                if (isCancellationAllowed) {
                    cancellationCharge = etCancellationCharge.getText().toString();
                }
                postPayLateFeesReservation = etPostPayLateFeesReservation.getText().toString();
                postPayLateFees = etPostPayLateFees.getText().toString();
                postPayFeesReservation = etPostPayFeesReservation.getText().toString();
                postPayFees = etPostPayFees.getText().toString();
                noOfReservationSpots = etNoOfReservableSpots.getText().toString();
                if (pricing_duation.equals("")) {
                    if (!pricing_duation1.equals("")) {
                        pricing_duation = pricing_duation1;
                    }
                }

                String _duation1 = edit_duration_unit.getText().toString();
                if (duation_unit.equals("")) {
                    if (!_duation1.equals("")) {
                        duation_unit = pricing_duation1;
                    }
                }

                if (!max_duation.equals("")
                        && !duation_unit.equals("Select duration unit")
                        && !pricing.equals("")
                        && !pricing_duation.equals("Select pricing duration")
                        && !custom_notice.equals("")) {

                    if (isReservationAllowed) {
                        if (!isPrePaymentRequired) {
                            if (TextUtils.isEmpty(postPayTermReservation)) {
                                showRequiredFieldsDialog();
                                return;
                            } else if (TextUtils.isEmpty(postPayLateFeesReservation)) {
                                showRequiredFieldsDialog();
                                return;
                            }
                        }
                    } else if (isPostPaymentAllowed) {
                        if (TextUtils.isEmpty(postPayTerm)) {
                            showRequiredFieldsDialog();
                            return;
                        } else if (TextUtils.isEmpty(postPayLateFees)) {
                            showRequiredFieldsDialog();
                            return;
                        }
                    }

                    int pric = Integer.parseInt(pricing_duation);
                    int max_du = Integer.parseInt(max_duation);
                    if (max_du >= pric) {
                        if (is24hr) {
                            if (isEdit) {
                                new update_parking_rules().execute();
                            } else {
                                new add_paking_rules().execute();
                            }
                        } else {
                            if (!private_start_hour.equals("Select start hour for rate") && !private_end_hours.equals("Select end hour for rate")) {
                                if (isEdit) {
                                    new update_parking_rules().execute();
                                } else {
                                    new add_paking_rules().execute();
                                }
                            } else {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                alertDialog.setMessage("Please fill all required fields.");
                                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                                alertDialog.show();
                            }
                        }
                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setMessage("Pricing duration should be lesser than maximum duration");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("Please fill all required fields.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
        });

        //Additional rules START ###########
        switchReservation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //Allowed user to park using reservation
                isReservationAllowed = b;
                if (isReservationAllowed) {
                    llReservationOption.setVisibility(View.VISIBLE);
                } else llReservationOption.setVisibility(View.GONE);
            }
        });

        switchPrePayment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //Is pew payment required for parking?
                isPrePaymentRequired = b;
                if (isPrePaymentRequired) {
                    llPostPaymentTermsReservation.setVisibility(View.GONE);
                    llPostPayLateFeeReservation.setVisibility(View.GONE);
                    llPostPayFeeReservation.setVisibility(View.GONE);
                } else {
                    llPostPaymentTermsReservation.setVisibility(View.VISIBLE);
                    llPostPayLateFeeReservation.setVisibility(View.VISIBLE);
                    llPostPayFeeReservation.setVisibility(View.VISIBLE);
                }
            }
        });

        switchPostPayment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //Is post payment is allowed?
                isPostPaymentAllowed = b;
                if (isPostPaymentAllowed) {
                    llPostPayLateFee.setVisibility(View.VISIBLE);
                    llPostPayFee.setVisibility(View.VISIBLE);
                    llPostPayTerms.setVisibility(View.VISIBLE);
                } else {
                    llPostPayLateFee.setVisibility(View.GONE);
                    llPostPayFee.setVisibility(View.GONE);
                    llPostPayTerms.setVisibility(View.GONE);
                }
            }
        });

        switchCancellation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //Is can cancel? if yes- charges for cancellation
                isCancellationAllowed = b;
                if (isCancellationAllowed) {
                    llCancellationCharge.setVisibility(View.VISIBLE);
                } else llCancellationCharge.setVisibility(View.GONE);
            }
        });

        switchVerifyLotsAvailability.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isVerifyLotAvailability = b;
            }
        });

        switchCountReservedSpots.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isCountReservedSpot = b;
            }
        });

        spPostPaymentTerms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    postPayTerm = paymentOptions.get(i).getPayment_option();
                } else {
                    postPayTerm = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spPostPaymentTermsReservation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    postPayTermReservation = paymentOptions.get(i).getPayment_option();
                } else postPayTermReservation = "";
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        //Additional rules END ###########

        //Event rule
        switchMultiDay.setOnCheckedChangeListener((buttonView, isChecked) -> {
            isMultiDay = isChecked;
        });

        switchMultiDayReservation.setOnCheckedChangeListener((buttonView, isChecked) -> {
            isMultiDayReservation = isChecked;
        });

        switchMultiDayParking.setOnCheckedChangeListener((buttonView, isChecked) -> {
            isMultiDayParking = isChecked;
        });
    }

    private void showRequiredFieldsDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage("Please fill all required fields.");
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    @Override
    protected void initViews(View view) {

        //Additional rules START ###########
        llEventRules = view.findViewById(R.id.llEventRules);
        switchMultiDay = view.findViewById(R.id.switchMultiDay);
        switchMultiDayReservation = view.findViewById(R.id.switchMultiDayReservation);
        switchMultiDayParking = view.findViewById(R.id.switchMultiDayParking);

        switchInEffect = view.findViewById(R.id.switchInEffect);

        switchReservation = view.findViewById(R.id.switchReservation);
        switchPrePayment = view.findViewById(R.id.switchPrePayment);
        switchPostPayment = view.findViewById(R.id.switchPostPayment);
        switchCancellation = view.findViewById(R.id.switchCancellation);
        switchVerifyLotsAvailability = view.findViewById(R.id.switchVerifyLotsAvailability);
        switchCountReservedSpots = view.findViewById(R.id.switchCountReservedSpots);
        etCancellationCharge = view.findViewById(R.id.etCancellationCharge);
        etPostPayLateFeesReservation = view.findViewById(R.id.etPostPayLateFeesReservation);
        etPostPayLateFees = view.findViewById(R.id.etPostPayLateFees);
        etNoOfReservableSpots = view.findViewById(R.id.etNoOfReservableSpots);
        etPostPayFeesReservation = view.findViewById(R.id.etPostPayFeesReservation);
        etPostPayFees = view.findViewById(R.id.etPostPayFees);
        spPostPaymentTermsReservation = view.findViewById(R.id.spPostPaymentTermsReservation);
        spPostPaymentTerms = view.findViewById(R.id.spPostPaymentTerms);
        llCancellationCharge = view.findViewById(R.id.llCancellationCharge);
        llCancellationCharge.setVisibility(View.GONE);
        llPostPaymentTermsReservation = view.findViewById(R.id.llPostPaymentTermsReservation);
        llPostPayLateFeeReservation = view.findViewById(R.id.llPostPayLateFeeReservation);
        llPostPayLateFee = view.findViewById(R.id.llPostPayLateFee);
        llPostPayTerms = view.findViewById(R.id.llPostPayTerms);
        llReservationOption = view.findViewById(R.id.llReservationOption);
        llNoOfReservableSpots = view.findViewById(R.id.llNoOfReservableSpots);
        llPostPayFeeReservation = view.findViewById(R.id.llPostPayFeeReservation);
        llPostPayFee = view.findViewById(R.id.llPostPayFee);

        postPayTermsAdapter = new ArrayAdapter<PaymentOption>(getActivity(), android.R.layout.simple_spinner_item, paymentOptions);
        postPayTermsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPostPaymentTerms.setAdapter(postPayTermsAdapter);

        postPayTermsReservationAdapter = new ArrayAdapter<PaymentOption>(getActivity(), android.R.layout.simple_spinner_item, paymentOptions);
        postPayTermsReservationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPostPaymentTermsReservation.setAdapter(postPayTermsReservationAdapter);

        //Additional rules END ###########

        txt_day_list = view.findViewById(R.id.txt_day_list);
        rl_private_day_list = view.findViewById(R.id.rl_list_of_day);
        img_private_day_list_close = view.findViewById(R.id.img_private_day_list_close);
        list_private_day = view.findViewById(R.id.list_day);
        sp_duraion = view.findViewById(R.id.sp_duration_unit);
        edit_duration_unit = view.findViewById(R.id.edit_duration_unit);
        edit_pricing = view.findViewById(R.id.edit_pricing);
        txt_pricing_duation = view.findViewById(R.id.txt_pricing_duration);
        sp_pricing_duration = view.findViewById(R.id.sp_pricing_unit);
        switch_24hr = view.findViewById(R.id.switch_24hr);
        switch_active = view.findViewById(R.id.switch_active);
        edit_custom_notice = view.findViewById(R.id.edit_custom_notice);
        ll_strat_time = view.findViewById(R.id.ll_start_time);
        ll_end_time = view.findViewById(R.id.ll_end_time);
        edit_start_time = view.findViewById(R.id.edit_start_time);
        edit_end_time = view.findViewById(R.id.edit_endt_time);
        edit_pricig_duation = view.findViewById(R.id.edit_pricing_duartion);
        edit_max = view.findViewById(R.id.edit_max);
        txt_parking_rules_save = view.findViewById(R.id.txt_parking_rules_save);
        rl_progressbar = view.findViewById(R.id.rl_progressbar);
        sp_private_start_hour = view.findViewById(R.id.sp_private_start_hour);
        sp_private_end_hour = view.findViewById(R.id.sp_private_end_hour);
        rl_main = view.findViewById(R.id.rl_main);

        ArrayAdapter<String> spinnerArrayAdapter111 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.duration_unit);
        sp_duraion.setAdapter(spinnerArrayAdapter111);
        ArrayAdapter<String> sp_private_start_adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.start_hour_array);
        sp_private_start_hour.setAdapter(sp_private_start_adapter);
        ArrayAdapter<String> sp_private_end_adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.end_hour_array);
        sp_private_end_hour.setAdapter(sp_private_end_adapter);

        setpricing_duation();

        prepareDayTypes();
    }

    private void prepareDayTypes() {
        item i1 = new item();
        i1.setThis_day("EVERYDAY");
        i1.setIsslected(true);
        day_list.add(i1);

        item i2 = new item();
        i2.setThis_day("WEEKDAYS");
        i2.setIsslected(false);
        day_list.add(i2);

        item i3 = new item();
        i3.setThis_day("WEEKENDS");
        i3.setIsslected(false);
        day_list.add(i3);

        item i4 = new item();
        i4.setThis_day("MON");
        i4.setIsslected(false);
        day_list.add(i4);

        item i5 = new item();
        i5.setThis_day("TUE");
        i5.setIsslected(false);
        day_list.add(i5);


        item i6 = new item();
        i6.setThis_day("WED");
        i6.setIsslected(false);
        day_list.add(i6);

        item i7 = new item();
        i7.setThis_day("THD");
        i7.setIsslected(false);
        day_list.add(i7);

        item i8 = new item();
        i8.setThis_day("FRI");
        i8.setIsslected(false);
        day_list.add(i8);

        item i9 = new item();
        i9.setThis_day("SAT");
        i9.setIsslected(false);
        day_list.add(i9);

        list_private_day.setAdapter(new CustomAdapterDay(getActivity(), day_list, getResources()));
    }

    public class CustomAdapterDay extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        CustomAdapterDay(Activity a, ArrayList<item> d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;
            data = d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txt_title;
            ImageView img;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;


            if (convertView == null) {

                vi = inflater.inflate(R.layout.adapater_day, null);
                /****** View Holder Object to contain tabitem.xml file elements ******/

                holder = new ViewHolder();

                holder.txt_title = vi.findViewById(R.id.txt_day_name);
                holder.img = vi.findViewById(R.id.img_day_selected);

                list_private_day.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        checkedisone = 0;
                        for (int i = 0; i < day_list.size(); i++) {
                            if (day_list.get(i).getIsslected()) {
                                checkedisone++;
                            }
                        }


                        if (checkedisone == 1) {

                            day_list.get(position).setIsslected(true);
                            notifyDataSetChanged();

                        } else {

                            if (day_list.get(position).getIsslected()) {
                                day_list.get(position).setIsslected(false);
                                notifyDataSetChanged();
                            } else {
                                day_list.get(position).setIsslected(true);
                                notifyDataSetChanged();
                            }

                        }

                        StringBuffer daylist = new StringBuffer();
                        ;
                        for (int i = 0; i < day_list.size(); i++) {
                            if (day_list.get(i).getIsslected()) {
                                daylist.append(day_list.get(i).getThis_day() + ",");

                            }
                        }
                        String finaldaylist = daylist.toString();
                        finaldaylist = finaldaylist.substring(0, finaldaylist.length() - 1);
                        txt_day_list.setText(finaldaylist);

                    }
                });


                /************  Set holder with LayoutInflater ************/
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {


            } else {
                /***** Get each Model object from Arraylist ********/
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                /************  Set Model values in Holder elements ***********/
                if (tempValues.getIsslected()) {
                    holder.img.setVisibility(View.VISIBLE);
                    holder.img.setImageResource(R.mipmap.selected);
                } else {
                    holder.img.setVisibility(View.GONE);
                }
                holder.txt_title.setText(tempValues.getThis_day());


            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

    public void setpricing_duation() {
        ArrayAdapter<String> spinnerArrayAdapter12 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.pricing_duration_12);
        ArrayAdapter<String> spinnerArrayAdapter24 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.pricing_duration_24);
        ArrayAdapter<String> spinnerArrayAdapterminu = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.pricing_duration_minutes);
        ArrayAdapter<String> spinnerArrayAdapterday = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.pricing_duration_day);
        ArrayAdapter<String> spinnerArrayAdapterweek = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.pricing_duration_week);
        ArrayAdapter<String> spinnerArrayAdaptermonth = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Array_class_static.pricing_duration_month);
        if (duation_unit.equals("Minute")) {

            sp_pricing_duration.setAdapter(spinnerArrayAdapterminu);
            sp_pricing_duration.setSelection(1);
            for (int i = 0; i < Array_class_static.pricing_duration_minutes.length; i++) {
                if (pricing_duation.equals(Array_class_static.pricing_duration_minutes[i])) {
                    sp_pricing_duration.setSelection(i);
                }
            }

        } else if (duation_unit.equals("Hour")) {
            if (is24hr) {
                sp_pricing_duration.setAdapter(spinnerArrayAdapter24);
                sp_pricing_duration.setSelection(1);
                for (int i = 0; i < Array_class_static.pricing_duration_24.length; i++) {
                    if (pricing_duation.equals(Array_class_static.pricing_duration_24[i])) {
                        sp_pricing_duration.setSelection(i);
                    }
                }
            } else {
                sp_pricing_duration.setAdapter(spinnerArrayAdapter12);
                sp_pricing_duration.setSelection(1);
                for (int i = 0; i < Array_class_static.pricing_duration_12.length; i++) {
                    if (pricing_duation.equals(Array_class_static.pricing_duration_12[i])) {
                        sp_pricing_duration.setSelection(i);
                    }
                }
            }
        } else if (duation_unit.equals("Day")) {
            sp_pricing_duration.setAdapter(spinnerArrayAdapterday);
            sp_pricing_duration.setSelection(1);
            for (int i = 0; i < Array_class_static.pricing_duration_day.length; i++) {
                if (pricing_duation.equals(Array_class_static.pricing_duration_day[i])) {
                    sp_pricing_duration.setSelection(i);
                }
            }
        } else if (duation_unit.equals("Week")) {
            sp_pricing_duration.setAdapter(spinnerArrayAdapterweek);
            sp_pricing_duration.setSelection(1);
            for (int i = 0; i < Array_class_static.pricing_duration_week.length; i++) {
                if (pricing_duation.equals(Array_class_static.pricing_duration_week[i])) {
                    sp_pricing_duration.setSelection(i);
                }
            }
        } else if (duation_unit.equals("Month")) {
            sp_pricing_duration.setAdapter(spinnerArrayAdaptermonth);
            sp_pricing_duration.setSelection(1);
            for (int i = 0; i < Array_class_static.pricing_duration_month.length; i++) {
                if (pricing_duation.equals(Array_class_static.pricing_duration_month[i])) {
                    sp_pricing_duration.setSelection(i);
                }
            }
        } else {
            sp_pricing_duration.setAdapter(spinnerArrayAdapterminu);
            sp_pricing_duration.setSelection(1);
            for (int i = 0; i < Array_class_static.pricing_duration_minutes.length; i++) {
                if (pricing_duation.equals(Array_class_static.pricing_duration_minutes[i])) {
                    sp_pricing_duration.setSelection(i);
                }
            }
        }
    }

    //update private parking rules.....................
    public class update_parking_rules extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        JSONObject json12 = null;
        String parkingurl = "_table/other_parking_rules";
        String id = "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            if (isEventRule) {
                parkingurl = "_table/event_parking_rules";
            } else {
                if (type.equals("Township")) {
                    parkingurl = "_table/township_parking_rules";
                } else if (type.equals("Commercial")) {
                    parkingurl = "_table/google_parking_rules";
                } else if (type.equals("Other")) {
                    parkingurl = "_table/other_parking_rules";
                }
            }
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();

            if (isactive) {
                jsonValues1.put("active", 1);
            } else {
                jsonValues1.put("active", 0);
            }
            if (inEffect) {
                jsonValues1.put("in_effect", 1);
            } else {
                jsonValues1.put("in_effect", 0);
            }
            jsonValues1.put("id", parkingRule.getId());
            jsonValues1.put("custom_notice", custom_notice);
            if (is24hr) {
                jsonValues1.put("start_time", "12 Midnight");
                jsonValues1.put("end_time", "11 PM");
                jsonValues1.put("start_hour", "0");
                jsonValues1.put("end_hour", "23");
            } else {
                jsonValues1.put("start_time", private_start_hour);
                jsonValues1.put("end_time", private_end_hours);
                jsonValues1.put("time_rule", private_start_hour + "-" + private_end_hours);

                String strathor = private_start_hour.substring(0, private_start_hour.indexOf(" "));
                String endhr = private_end_hours.substring(0, private_end_hours.indexOf(" "));
                String timeEnd = private_end_hours.substring(private_end_hours.indexOf(" "));
                String timeStart = private_start_hour.substring(private_start_hour.indexOf(" "));
                try {
                    if (timeStart.equalsIgnoreCase(" PM")) {
                        strathor = String.valueOf((Integer.valueOf(strathor) + 12));
                    }
                    if (timeEnd.equalsIgnoreCase(" PM")) {
                        endhr = String.valueOf((Integer.valueOf(endhr) + 12));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.e("#DEBUG", "  strathor:  " + strathor + "    endhr:  " + endhr);

                jsonValues1.put("start_hour", strathor);
                jsonValues1.put("end_hour", endhr);
            }
            jsonValues1.put("pricing", pricing);
            jsonValues1.put("duration_unit", duation_unit);
            jsonValues1.put("max_duration", max_duation);
            jsonValues1.put("pricing_duration", pricing_duation);
            jsonValues1.put("day_type", str_day_list);
            if (start_timer.equals("null") && end_time.equals("null")) {
                jsonValues1.put("time_rule", "24 HRS");
            } else {
                jsonValues1.put("time_rule", start_timer + "-" + end_time);
            }
            jsonValues1.put("enforced", "In Effect");
            jsonValues1.put("this_day", "EVERYDAY");

            jsonValues1.put("ReservationAllowed", isReservationAllowed);
            jsonValues1.put("PrePymntReqd_for_Reservation", isPrePaymentRequired);
            jsonValues1.put("CanCancel_Reservation", isCancellationAllowed);
            jsonValues1.put("Cancellation_Charge", cancellationCharge);
            jsonValues1.put("Reservation_PostPayment_term", postPayTermReservation);
            jsonValues1.put("Reservation_PostPayment_LateFee", postPayLateFeesReservation);
            jsonValues1.put("ParkNow_PostPaymentAllowed", isPostPaymentAllowed);
            jsonValues1.put("ParkNow_PostPayment_term", postPayTerm);
            jsonValues1.put("before_reserve_verify_lot_avbl", isVerifyLotAvailability);
            jsonValues1.put("include_reservations_for_avbl_calc", isCountReservedSpot);
            jsonValues1.put("ParkNow_PostPayment_LateFee", postPayLateFees);

            jsonValues1.put("Reservation_PostPayment_Fee", postPayFeesReservation);
            jsonValues1.put("ParkNow_PostPayment_Fee", postPayFees);
            jsonValues1.put("number_of_reservable_spots", noOfReservationSpots);

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (getActivity() != null && json12 != null) {
                if (json12.has("error")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("Error");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                } else {
                    if (!id.equals("")) {
                        isUpdated = true;
                        Toast.makeText(getActivity(), "Parking rule updated!", Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();
                    }
                }

            }
            super.onPostExecute(s);
        }
    }

    //add private parking rules.................
    public class add_paking_rules extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        JSONObject json12 = null;
        String parkingurl = "_table/other_parking_rules";
        String id = "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            if (isEventRule) {
                parkingurl = "_table/event_parking_rules";
            } else {
                if (type.equals("Township")) {
                    parkingurl = "_table/township_parking_rules";
                } else if (type.equals("Commercial")) {
                    parkingurl = "_table/google_parking_rules";
                } else if (type.equals("Other")) {
                    parkingurl = "_table/other_parking_rules";
                }
            }
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            //  loc = getRandomString(10);

            private_place_id = Array_class_static.getRandomString(10);
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();

            if (is24hr) {
                jsonValues1.put("start_time", "12 Midnight");
                jsonValues1.put("end_time", "11 PM");
                jsonValues1.put("start_hour", "0");
                jsonValues1.put("end_hour", "23");
                jsonValues1.put("time_rule", "24 HRS");
            } else {
                jsonValues1.put("start_time", private_start_hour);
                jsonValues1.put("end_time", private_end_hours);
                jsonValues1.put("time_rule", private_start_hour + "-" + private_end_hours);

                String strathor = private_start_hour.substring(0, private_start_hour.indexOf(" "));
                String endhr = private_end_hours.substring(0, private_end_hours.indexOf(" "));

                jsonValues1.put("start_hour", strathor);
                jsonValues1.put("end_hour", endhr);
            }

            if (isactive) {
                jsonValues1.put("active", 1);
            } else {
                jsonValues1.put("active", 0);
            }

            if (inEffect) {
                jsonValues1.put("in_effect", 1);
            } else {
                jsonValues1.put("in_effect", 0);
            }

            if (parkingRule != null) {
                try {
                    jsonValues1.put("location_id", parkingRule.getLocation_id());
                    jsonValues1.put("location_code", parkingRule.getLocation_code());
                    jsonValues1.put("location_name", parkingRule.getLocation_name());
                    jsonValues1.put("twp_id", parkingRule.getTwp_id());
                    jsonValues1.put("township_code", parkingRule.getTownship_code());
                    jsonValues1.put("location_place_id", parkingRule.getLocation_place_id());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (location != null) {
                jsonValues1.put("location_id", location.getLocation_id());
                jsonValues1.put("location_code", location.getLocation_code());
                jsonValues1.put("location_name", location.getLocation_name());
                jsonValues1.put("twp_id", location.getTwp_id());
                jsonValues1.put("township_code", location.getTownship_code());
                jsonValues1.put("location_place_id", location.getLocation_place_id());
            }

            if (isEventRule && eventDefinition != null) {
                //Location info
//                jsonValues1.put("location_id", eventDefinition.getLocation_id());
//                jsonValues1.put("location_place_id", eventDefinition.getLocation_place_id());
//                jsonValues1.put("location_code", eventDefinition.getLocation_code());
//                jsonValues1.put("location_name", eventDefinition.getLocation_name());
                jsonValues1.put("twp_id", eventDefinition.getTwp_id());
                jsonValues1.put("township_code", eventDefinition.getTownship_code());
                jsonValues1.put("manager_type", eventDefinition.getManager_type());
                jsonValues1.put("manager_type_id", eventDefinition.getManager_type_id());
                jsonValues1.put("company_id", eventDefinition.getCompany_id());
                jsonValues1.put("company_type_id", eventDefinition.getManager_type_id());

                //Event info
                jsonValues1.put("event_id", eventDefinition.getEvent_id());
                jsonValues1.put("event_code", eventDefinition.getEvent_code());
                jsonValues1.put("event_name", eventDefinition.getEvent_name());
//                jsonValues1.put("event_specific_date", eventDefinition.getTownship_code());
//                jsonValues1.put("event_specific_time", eventDefinition.getTownship_code());
                jsonValues1.put("event_multi_dates", eventDefinition.getEvent_multi_dates());
                jsonValues1.put("event_multi_timings", eventDefinition.getEvent_event_timings());
                jsonValues1.put("event_dt_short_info", eventDefinition.getEvent_begins_date_time()
                        + "," + eventDefinition.getEvent_ends_date_time());
                //Event rule
                jsonValues1.put("multi_day", isMultiDay);
                jsonValues1.put("multi_day_reserve_allowed", isMultiDayReservation);
                jsonValues1.put("multi_day_parking_allowed", isMultiDayParking);

            }

            jsonValues1.put("custom_notice", custom_notice);
            jsonValues1.put("pricing", pricing);
            jsonValues1.put("duration_unit", duation_unit);
            jsonValues1.put("max_duration", max_duation);
            jsonValues1.put("pricing_duration", pricing_duation);
            jsonValues1.put("day_type", str_day_list);
            jsonValues1.put("enforced", "In Effect");
            jsonValues1.put("this_day", "EVERYDAY");
            jsonValues1.put("marker_type", type);
            jsonValues1.put("currency", "USD");
            jsonValues1.put("currency_symbol", "$");

            jsonValues1.put("ReservationAllowed", isReservationAllowed);
            jsonValues1.put("PrePymntReqd_for_Reservation", isPrePaymentRequired);
            jsonValues1.put("CanCancel_Reservation", isCancellationAllowed);
            jsonValues1.put("Cancellation_Charge", cancellationCharge);
            jsonValues1.put("Reservation_PostPayment_term", postPayTermReservation);
            jsonValues1.put("Reservation_PostPayment_LateFee", postPayLateFeesReservation);
            jsonValues1.put("ParkNow_PostPaymentAllowed", isPostPaymentAllowed);
            jsonValues1.put("ParkNow_PostPayment_term", postPayTerm);
            jsonValues1.put("before_reserve_verify_lot_avbl", isVerifyLotAvailability);
            jsonValues1.put("include_reservations_for_avbl_calc", isCountReservedSpot);
            jsonValues1.put("ParkNow_PostPayment_LateFee", postPayLateFees);

            jsonValues1.put("Reservation_PostPayment_Fee", postPayFeesReservation);
            jsonValues1.put("ParkNow_PostPayment_Fee", postPayFees);
            jsonValues1.put("number_of_reservable_spots", noOfReservationSpots);


            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json12 != null) {
                if (json12.has("error")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("Error");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                } else {
                    if (!id.equals("")) {
                        if (!isEventRule) {
                            MyApplication.getInstance().privateRuleIds.add(id);
                        }
                        isUpdated = true;
                        getActivity().onBackPressed();
                    }
                }

            }
            super.onPostExecute(s);
        }
    }

    //fetch payment options
    private class fetchPaymentOptions extends AsyncTask<String, String, String> {

        JSONObject json;
        JSONArray json1;

        @Override
        protected void onPreExecute() {
            paymentOptions.clear();
            PaymentOption paymentOption = new PaymentOption();
            paymentOption.setPayment_option("Select terms");
            paymentOption.setPayment_option_description("Select terms");
            paymentOptions.add(paymentOption);
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + "_table/payment_options";
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        PaymentOption paymentOption = new PaymentOption();
                        paymentOption.setId(c.getInt("id"));
                        paymentOption.setPayment_option(c.getString("payment_option"));
                        paymentOption.setPayment_option_description(c.getString("payment_option_description"));
                        paymentOptions.add(paymentOption);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                rl_progressbar.setVisibility(View.GONE);
                postPayTermsAdapter.notifyDataSetChanged();
                postPayTermsReservationAdapter.notifyDataSetChanged();
                if (paymentOptions.size() > 0) {
                    if (!TextUtils.isEmpty(postPayTermReservation)) {
                        spPostPaymentTermsReservation.setSelection(paymentOptions.indexOf(new PaymentOption(postPayTermReservation)));
                    }

                    if (!TextUtils.isEmpty(postPayTerm)) {
                        spPostPaymentTerms.setSelection(paymentOptions.indexOf(new PaymentOption(postPayTerm)));
                    }
                }
            }
        }
    }

    @Override
    public void onResume() {
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    if (rl_private_day_list.getVisibility() == View.VISIBLE) {
                        rl_private_day_list.setVisibility(View.GONE);
                    } else {
                        getActivity().onBackPressed();
                    }
                    return true;
                }

                return false;
            }
        });
        super.onResume();
    }
}
