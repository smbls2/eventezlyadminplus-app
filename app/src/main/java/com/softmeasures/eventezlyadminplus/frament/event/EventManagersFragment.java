package com.softmeasures.eventezlyadminplus.frament.event;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragEventManagersBinding;
import com.softmeasures.eventezlyadminplus.frament.ConfigSettingsFragment;
import com.softmeasures.eventezlyadminplus.frament.config_setting.GroupSettingFragment;
import com.softmeasures.eventezlyadminplus.frament.config_setting.MessagingSettingFragment;
import com.softmeasures.eventezlyadminplus.models.Manager;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class EventManagersFragment extends BaseFragment {

    private static final String TAG = "#DEBUG EventManagersFrg";
    private int companyCount = 0;
    private PartnerAdapter partnerAdapter;
    private ArrayList<Manager> managers = new ArrayList<>();
    private ArrayList<Manager> selectedManagers = new ArrayList<>();
    private ArrayList<String> companytypesearch = new ArrayList<>();

    private FragEventManagersBinding binding;
    private String parkingType = "";
    private boolean isCheckIn = false, isSelectAll = false, isSelectAllCompany = false, isSelectCopmany = false;
    private String configType;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            parkingType = getArguments().getString("parkingType");
            configType = getArguments().getString("Config_type");
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_event_managers, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {
        if (parkingType.equals("Township")) {
            binding.tvTitle.setText("Select Partner");
        } else if (parkingType.equals("Commercial")) {
            binding.tvTitle.setText("Select Commercial");
        } else if (parkingType.equals("Other")) {
            binding.tvTitle.setText("Select Private");
        }
    }

    @Override
    protected void setListeners() {
        binding.autotvCompanyID.setOnClickListener(v ->
                showEnter_txt("Company ID", "Enter Company ID", binding.autotvCompanyID, InputType.TYPE_CLASS_NUMBER));
        binding.autotvCompanyName.setOnClickListener(v ->
                showEnter_txt("Company Name", "Enter Company Name", binding.autotvCompanyName, InputType.TYPE_CLASS_TEXT));
        binding.autotvCompanyCode.setOnClickListener(v ->
                showEnter_txt("Company Code", "Enter Company Code", binding.autotvCompanyCode, InputType.TYPE_CLASS_TEXT));

        binding.btnfind.setOnClickListener(v -> {
            String cid = "", code = "", name = "";
            /*if (!TextUtils.isEmpty(binding.autotvCompanyCode.getText().toString()))
                code = binding.autotvCompanyCode.getText().toString();
            else {
                Toast.makeText(getActivity(), "Please Enter Company Code", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(binding.autotvCompanyName.getText().toString())) {
                Toast.makeText(getActivity(), "Please Enter Company Name", Toast.LENGTH_SHORT).show();
                return;
            } else name = binding.autotvCompanyName.getText().toString();*/
            if (TextUtils.isEmpty(binding.autotvCompanyID.getText().toString())) {
                if (TextUtils.isEmpty(binding.autotvCompanyName.getText().toString())) {
                    if (TextUtils.isEmpty(binding.autotvCompanyCode.getText().toString())) {
                        Toast.makeText(getActivity(), "Please Enter Above Company ID or Name or Code", Toast.LENGTH_SHORT).show();
                        return;
                    } else code = binding.autotvCompanyCode.getText().toString();
                } else name = binding.autotvCompanyName.getText().toString();
            } else cid = binding.autotvCompanyID.getText().toString();
            binding.tvError.setVisibility(View.GONE);
            new fetchManagers(cid, code, name).execute();
        });
        binding.ivSelectAll.setOnClickListener(v -> {
            isSelectAll = !isSelectAll;
            binding.ivSelectAll.setImageResource(isSelectAll ? R.drawable.ic_check : R.drawable.ic_circle_border);
            for (Manager manager : managers)
                manager.setSelect(isSelectAll);
            companyCount = isSelectAll ? managers.size() : 0;
            binding.tvCompanyCount.setText(" " + companyCount + "  Company");
            if (isSelectAll) selectedManagers.addAll(managers);
            else selectedManagers = new ArrayList<>();
            partnerAdapter.notifyDataSetChanged();
        });
        binding.tvCancel.setOnClickListener(view -> {
            isSelectCopmany = false;
            isSelectAll = false;
            binding.tvCompanyCount.setVisibility(View.GONE);
            binding.tvCancel.setVisibility(View.GONE);
            binding.ivSelectAll.setVisibility(View.GONE);
            binding.fbNext.setVisibility(View.GONE);
            for (Manager manager : managers)
                manager.setSelect(false);
            partnerAdapter.notifyDataSetChanged();
        });
        binding.fbNext.setOnClickListener(v -> {
            isSelectCopmany = false;
            isSelectAll = false;
            binding.tvCompanyCount.setVisibility(View.GONE);
            binding.tvCompanyCount.setVisibility(View.GONE);
            binding.tvCancel.setVisibility(View.GONE);
            binding.ivSelectAll.setVisibility(View.GONE);
            binding.fbNext.setVisibility(View.GONE);
            for (Manager manager : managers)
                manager.setSelect(false);
            partnerAdapter.notifyDataSetChanged();

            if (companyCount > 0) {
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment frag = null;
                if (configType.equals("Call Config"))
                    frag = new ConfigSettingsFragment();
                else if (configType.equals("Chat Config"))
                    frag = new MessagingSettingFragment();
                else if (configType.equals("Group Config"))
                    frag = new GroupSettingFragment();
                Bundle bundle = new Bundle();
                if (companyCount == 1)
                    bundle.putString("selectedManager", new Gson().toJson(selectedManagers.get(selectedManagers.size() - 1)));
                else
                    bundle.putString("selectedManagers", new Gson().toJson(selectedManagers));
                bundle.putBoolean("isMultiple", companyCount != 1);
                frag.setArguments(bundle);
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.add(R.id.My_Container_1_ID, frag);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(frag);
                ft.commitAllowingStateLoss();
            } else
                Snackbar.make(v, "Please Select Company", Snackbar.LENGTH_SHORT).show();
        });

        binding.spinnerCompanyType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                parkingType = companytypesearch.get(position);
                Log.d("checkcompany","=="+parkingType);
                if (parkingType.equals("TOWNSHIP"))
                {
                    parkingType = "SearchTownship";
                }
                else if (parkingType.equals("COMMERCIAL"))
                {
                    parkingType = "SearchCommercial";
                }
                else
                {
                    parkingType = "SearchOther";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    protected void initViews(View v) {
        if (parkingType.equals("Search")) {
            binding.tvTitle.setVisibility(View.GONE);
            binding.llSerachCompany.setVisibility(View.VISIBLE);
        } else {
            binding.llSerachCompany.setVisibility(View.GONE);
            new fetchManagers().execute();
        }
        partnerAdapter = new PartnerAdapter();
        binding.rvPartners.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvPartners.setAdapter(partnerAdapter);

        binding.tvCancel.setVisibility(View.GONE);
        binding.ivSelectAll.setVisibility(View.GONE);
        binding.fbNext.setVisibility(View.GONE);

        companytypesearch.addAll(Arrays.asList(getResources().getStringArray(R.array.companytype)));
        ArrayAdapter<String> occupationAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, companytypesearch);
        binding.spinnerCompanyType.setAdapter(occupationAdapter);
        binding.spinnerCompanyType.setSelection(companytypesearch.indexOf("TOWNSHIP"));


    }

    public class fetchManagers extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String parkingurl = "_table/townships_manager";
        String companyId = "", companyCode = "", companyName = "";

        @Override
        protected void onPreExecute() {
            binding.rlProgressbar.setVisibility(View.VISIBLE);

            Log.d("parkingtype","=="+parkingType);

            if (parkingType.equals("Commercial"))
                parkingurl = "_table/commercial_manager";
            else if (parkingType.equals("Other"))
                parkingurl = "_table/other_private_manager";
            else if (parkingType.equals("SearchTownship")) {
                if (!TextUtils.isEmpty(companyId))
                    parkingurl = "_table/townships_manager?filter=(id=" + companyId + ")";
                else if (!TextUtils.isEmpty(companyName)) {
                    companyName = companyName.replace(" ", "%20");
                    parkingurl = "_table/townships_manager?filter=(lot_manager=" + companyName + ")";
                } else if (!TextUtils.isEmpty(companyCode))
                    parkingurl = "_table/townships_manager?filter=(manager_id=" + companyCode + ")";
            } else if (parkingType.equals("SearchCommercial")) {
                if (!TextUtils.isEmpty(companyId))
                    parkingurl = "_table/commercial_manager?filter=(id=" + companyId + ")";
                else if (!TextUtils.isEmpty(companyName)) {
                    companyName = companyName.replace(" ", "%20");
                    parkingurl = "_table/commercial_manager?filter=(lot_manager=" + companyName + ")";
                } else if (!TextUtils.isEmpty(companyCode))
                    parkingurl = "_table/commercial_manager?filter=(manager_id=" + companyCode + ")";
            } else if (parkingType.equals("SearchOther")) {
                if (!TextUtils.isEmpty(companyId))
                    parkingurl = "_table/other_private_manager?filter=(id=" + companyId + ")";
                else if (!TextUtils.isEmpty(companyName)) {
                    companyName = companyName.replace(" ", "%20");
                    parkingurl = "_table/other_private_manager?filter=(lot_manager=" + companyName + ")";
                } else if (!TextUtils.isEmpty(companyCode))
                    parkingurl = "_table/other_private_manager?filter=(manager_id=" + companyCode + ")";
            }
            super.onPreExecute();
        }

        public fetchManagers() {
        }

        public fetchManagers(String cid, String code, String name) {
            companyId = cid;
            companyCode = code;
            companyName = name;
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            managers.clear();

            Log.d("#DEBUG", " mangerSize: " + managers.size());
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("#DEBUG", "   fetchManagers:  URL:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        Manager manager = new Manager();
                        manager.setId(c.getInt("id"));
                        manager.setDate_time(c.getString("date_time"));
                        manager.setManager_id(c.getString("manager_id"));
                        manager.setManager_type(c.getString("manager_type"));
                        manager.setManager_type_id(c.getInt("manager_type_id"));
                        manager.setLot_manager(c.getString("lot_manager"));
                        manager.setStreet(c.getString("street"));
                        manager.setAddress(c.getString("address"));
                        manager.setState(c.getString("state"));
                        manager.setState_name(c.getString("state_name"));
                        manager.setCity(c.getString("city"));
                        manager.setCountry(c.getString("country"));
                        manager.setZip(c.getString("zip"));
                        manager.setContact_person(c.getString("contact_person"));
                        manager.setContact_title(c.getString("contact_title"));
                        manager.setContact_phone(c.getString("contact_phone"));
                        manager.setContact_email(c.getString("contact_email"));
                        manager.setTownship_logo(c.getString("township_logo"));
                        manager.setOfficial_logo(c.getString("official_logo"));
                        manager.setUser_id(c.getInt("user_id"));
                        managers.add(manager);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null) {
                binding.rlProgressbar.setVisibility(View.GONE);
                if (managers.size() != 0) {
                    if (parkingType.equals("SearchOther") || parkingType.equals("SearchCommercial"))
                       // parkingType = "Search"; //comment by me because of it creating issue during serach.
                    binding.tvError.setVisibility(View.GONE);
                    partnerAdapter.notifyDataSetChanged();
                } else {
                    if (parkingType.equals("Search")) {
                        parkingType = "SearchCommercial";
                        new fetchManagers(companyId, companyCode, companyName).execute();
                    } else if (parkingType.equals("SearchCommercial")) {
                        parkingType = "SearchOther";
                        new fetchManagers(companyId, companyCode, companyName).execute();
                    } else if (parkingType.equals("SearchOther")) {
                        parkingType = "Search";
                        binding.tvError.setVisibility(View.VISIBLE);
                    }
                    partnerAdapter.notifyDataSetChanged();
                    if (managers.size()==0)
                    {
                        binding.tvError.setVisibility(View.VISIBLE);
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    private void showEnter_txt(String title, String hintText, TextView etTextView, int inputType) {
        RelativeLayout viewGroup = (RelativeLayout) getActivity().findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.enter_txt, viewGroup, false);
        final PopupWindow popup = new PopupWindow(getActivity());
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(true);
        popup.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        popup.setFocusable(true);
        popup.update();
        popup.showAtLocation(layout, Gravity.TOP, 0, 0);

        final EditText edit_text;
        final TextView text_title, tvPaste;
        final Button btn_done;
        RelativeLayout btn_cancel;
        LinearLayout rl_edit;
        ImageView ivPaste;

        tvPaste = (TextView) layout.findViewById(R.id.tvPaste);
        ivPaste = (ImageView) layout.findViewById(R.id.ivPaste);
        edit_text = (EditText) layout.findViewById(R.id.edit_nu);
        text_title = (TextView) layout.findViewById(R.id.txt_title);
        rl_edit = (LinearLayout) layout.findViewById(R.id.rl_edit);

        text_title.setText(title);
        edit_text.setHint(hintText);
        edit_text.setInputType(inputType);
        if (!TextUtils.isEmpty(etTextView.getText()))
            edit_text.setText(etTextView.getText());
        btn_cancel = (RelativeLayout) layout.findViewById(R.id.close);
        btn_done = (Button) layout.findViewById(R.id.btn_done);

        edit_text.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        edit_text.setFocusable(true);
        edit_text.setSelection(edit_text.getText().length());
        edit_text.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            if (imm.isAcceptingText()) {
                Log.d(TAG, "Software Keyboard was shown");
            } else {
                Log.d(TAG, "Software Keyboard was not shown");
            }
        });
        rl_edit.setOnClickListener(v -> tvPaste.setVisibility(View.GONE));
        edit_text.setOnLongClickListener(v -> {
            tvPaste.setVisibility(View.VISIBLE);
            return true;
        });
        tvPaste.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData pData = clipboard.getPrimaryClip();
            if (pData != null) {
                ClipData.Item item = pData.getItemAt(0);
                String txtpaste = item.getText().toString();
                edit_text.getText().insert(edit_text.getSelectionStart(), txtpaste);
                edit_text.setText(edit_text.getText()+txtpaste);
            }
        });
        ivPaste.setOnClickListener(v -> {
            tvPaste.setVisibility(View.GONE);
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData pData = clipboard.getPrimaryClip();
            if (pData != null) {
                ClipData.Item item = pData.getItemAt(0);
                String txtpaste = item.getText().toString();
                edit_text.getText().insert(edit_text.getSelectionStart(), txtpaste);
                edit_text.setText(edit_text.getText()+txtpaste);

            }
        });

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imm.isAcceptingText()) {
                    Log.d(TAG, "DONE: Software Keyboard was shown");
                } else {
                    Log.d(TAG, "DONE: Software Keyboard was not shown");
                }
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                etTextView.setText(edit_text.getText().toString());
                popup.dismiss();
            }
        });
        btn_cancel.setOnClickListener(v -> {
            InputMethodManager imm1 = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm1.hideSoftInputFromWindow(edit_text.getWindowToken(), 0);
            String txt = edit_text.getText().toString();
            popup.dismiss();
        });
    }

    private class PartnerAdapter extends RecyclerView.Adapter<PartnerAdapter.PartnerHolder> {

        @NonNull
        @Override
        public PartnerAdapter.PartnerHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new PartnerHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_partner_new_manager,
                    viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull PartnerAdapter.PartnerHolder holder, int i) {
            Manager manager = managers.get(i);
            if (manager != null) {
                if (isSelectCopmany)
                    holder.ivSelectCompany.setVisibility(manager.isSelect() ? View.VISIBLE : View.GONE);
                else holder.ivSelectCompany.setVisibility(View.GONE);
                holder.tvName.setText(manager.getLot_manager());
                holder.tvAddress.setText(manager.getAddress());
                Picasso.with(getActivity()).load(manager.getTownship_logo()).into(holder.ivPartner);
            }
        }

        @Override
        public int getItemCount() {
            return managers.size();
        }

        class PartnerHolder extends RecyclerView.ViewHolder {
            CircleImageView ivPartner;
            ImageView ivSelectCompany;
            TextView tvName, tvAddress;

            PartnerHolder(@NonNull View itemView) {
                super(itemView);

                ivSelectCompany = itemView.findViewById(R.id.ivSelectedCompany);
                ivPartner = itemView.findViewById(R.id.ivPartner);
                tvName = itemView.findViewById(R.id.tvName);
                tvAddress = itemView.findViewById(R.id.tvAddress);

                itemView.setOnClickListener(v -> {
                    final int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        if (isSelectCopmany) {
                            ivSelectCompany.setVisibility(ivSelectCompany.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                            if (ivSelectCompany.getVisibility() == View.VISIBLE) {
                                managers.get(getBindingAdapterPosition()).setSelect(true);
                                selectedManagers.add(managers.get(getBindingAdapterPosition()));
                                companyCount += 1;
                            } else {
                                managers.get(getBindingAdapterPosition()).setSelect(false);
                                selectedManagers.remove(managers.get(getBindingAdapterPosition()));
                                companyCount -= 1;
                            }
                            binding.ivSelectAll.setImageResource(companyCount == managers.size() ? R.drawable.ic_check : R.drawable.ic_circle_border);
                            binding.tvCompanyCount.setText(" " + companyCount + "  Company");
                        } else {
                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            Fragment frag = null;
                            if (configType.equals("Call Config"))
                                frag = new ConfigSettingsFragment();
                            else if (configType.equals("Chat Config"))
                                frag = new MessagingSettingFragment();
                            else if (configType.equals("Group Config"))
                                frag = new GroupSettingFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("selectedManager", new Gson().toJson(managers.get(pos)));
                            frag.setArguments(bundle);
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.add(R.id.My_Container_1_ID, frag);
                            fragmentStack.lastElement().onPause();
                            ft.hide(fragmentStack.lastElement());
                            fragmentStack.push(frag);
                            ft.commitAllowingStateLoss();
                        }
                    }
                });

                itemView.setOnLongClickListener(v -> {
                    isSelectCopmany = true;
                    managers.get(getBindingAdapterPosition()).setSelect(true);
                    ivSelectCompany.setVisibility(View.VISIBLE);
                    selectedManagers=new ArrayList<>();
                    companyCount =0;
                    selectedManagers.add(managers.get(getBindingAdapterPosition()));
                    companyCount += 1;
                    binding.tvCompanyCount.setText(" " + companyCount + "  Company");

                    binding.ivSelectAll.setVisibility(View.VISIBLE);
                    binding.tvCancel.setVisibility(View.VISIBLE);
                    binding.tvCompanyCount.setVisibility(View.VISIBLE);
                    binding.fbNext.setVisibility(View.VISIBLE);
                    return true;
                });
            }
        }
    }
}
