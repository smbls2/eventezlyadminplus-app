package com.softmeasures.eventezlyadminplus.frament.g_classroom;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.api.services.classroom.model.Course;
import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragGClassroomAddCourseBinding;

import java.util.Arrays;

public class GClassRoomAddCourseFragment extends BaseFragment {

    private FragGClassroomAddCourseBinding binding;
    private Course course;

    ArrayAdapter<String> courseState;
    private String selectedCourseState = "";

    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_g_classroom_add_course, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {

        binding.tvBtnAddCourse.setOnClickListener(v -> {
            if (isValidate()) {
                //TODO add course
                addCourse();
            }
        });

        binding.spCourseState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedCourseState = courseState.getItem(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void addCourse() {

        progressDialog.show();
        Log.e("#DEBUG", "  addCourse:  Course:  " + new Gson().toJson(course));
        AndroidNetworking.post("https://classroom.googleapis.com/v1/courses")
                .addHeaders("Authorization", "Bearer " + myApp.getOAuth())
                .addHeaders("Accept", "application/json")
                .addApplicationJsonBody(course)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        GClassRoomCoursesFragment.isNewAdded = true;
                        Log.e("#DEBUG", "   addCourse:  onResponse:  " + response);
                        Toast.makeText(getActivity(), "Added Successfully!", Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Log.e("#DEBUG", "   addCourse:  onError:  " + anError.getErrorCode() + "   " + anError.getErrorBody());
                        Toast.makeText(getActivity(), "Failed to add!", Toast.LENGTH_SHORT).show();
                    }
                });
//                .getAsObject(Course.class, new ParsedRequestListener<Course>() {
//                    @Override
//                    public void onResponse(Course course) {
//                        Log.e("#DEBUG", "   addCourse:  onResponse:  "+new Gson().toJson(course));
//                        Toast.makeText(getActivity(), "Added Successfully!", Toast.LENGTH_SHORT).show();
//                        getActivity().onBackPressed();
//                    }
//
//                    @Override
//                    public void onError(ANError anError) {
//                        Log.e("#DEBUG", "   addCourse:  onError:  "+anError.getErrorCode()+"   "+anError.getErrorBody());
//                        Toast.makeText(getActivity(), "Failed to add!", Toast.LENGTH_SHORT).show();
//                    }
//                });
    }

    private boolean isValidate() {
        if (course == null) {
            course = new Course();
        }
        binding.etCourseName.setError(null);
        binding.etCourseSection.setError(null);
        binding.etCourseDescriptionHeading.setError(null);
        binding.etCourseDescription.setError(null);
        binding.etCourseRoom.setError(null);
        binding.etCourseOwnerId.setError(null);
        binding.etCourseState.setError(null);
        if (TextUtils.isEmpty(binding.etCourseName.getText().toString())) {
            binding.etCourseName.setError("Required!");
            return false;
        } else if (TextUtils.isEmpty(binding.etCourseSection.getText().toString())) {
            binding.etCourseSection.setError("Required!");
            return false;
        } else if (TextUtils.isEmpty(binding.etCourseDescriptionHeading.getText().toString())) {
            binding.etCourseDescriptionHeading.setError("Required!");
            return false;
        } else if (TextUtils.isEmpty(binding.etCourseDescription.getText().toString())) {
            binding.etCourseDescription.setError("Required!");
            return false;
        } else if (TextUtils.isEmpty(binding.etCourseRoom.getText().toString())) {
            binding.etCourseRoom.setError("Required!");
            return false;
        } else if (TextUtils.isEmpty(binding.etCourseOwnerId.getText().toString())) {
            binding.etCourseOwnerId.setError("Required!");
            return false;
        }
        course.setName(binding.etCourseName.getText().toString());
        course.setSection(binding.etCourseSection.getText().toString());
        course.setDescriptionHeading(binding.etCourseDescriptionHeading.getText().toString());
        course.setDescription(binding.etCourseDescription.getText().toString());
        course.setRoom(binding.etCourseRoom.getText().toString());
        course.setOwnerId(binding.etCourseOwnerId.getText().toString());
        course.setCourseState(selectedCourseState);
        return true;
    }

    @Override
    protected void initViews(View v) {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");

        courseState = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,
                Arrays.asList(getResources().getStringArray(R.array.course_state)));
        binding.spCourseState.setAdapter(courseState);

    }
}
