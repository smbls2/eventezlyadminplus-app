package com.softmeasures.eventezlyadminplus.frament;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.frament.Private_parking_main.clickonedit;

public class frg_private_edit_add_step1 extends BaseFragment {

    TextView txt_select_location, txt_lat, txt_lang, txt_sp1_private_next;
    EditText txt_address, txt_title, edit_private_loc_code, txt_private_parking_time, txt_private_no_parking_time;
    public static String private_title, private_address, private_no_parking, private_parking_time, Private_location_code, private_lat, private_lang;
    private RelativeLayout rl_main;

    private Spinner sp_township_type;
    String township_type[] = {"Paid", "Free", "Managed Paid", "Managed Free"};
    private String township_type_sp = "";
    private boolean isNewLocation = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.private_edit_step_1, container, false);
        if (getArguments() != null) {
            isNewLocation = getArguments().getBoolean("isNewLocation", false);
        }

        private_title = private_title != null ? (!private_title.equals("") ? (!private_title.equals("null") ? private_title : "") : "") : "";
        private_address = private_address != null ? (!private_address.equals("") ? (!private_address.equals("null") ? private_address : "") : "") : "";
        private_no_parking = private_no_parking != null ? (!private_no_parking.equals("") ? (!private_no_parking.equals("null") ? private_no_parking : "1 AM- 6 AM") : "1 AM- 6 AM") : "1 AM- 6 AM";
        private_parking_time = private_parking_time != null ? (!private_parking_time.equals("") ? (!private_parking_time.equals("null") ? private_parking_time : "6 AM- 10 PM ( M-F), 7 AM- 6 PM ( Sat-Sun)") : "6 AM- 10 PM ( M-F), 7 AM- 6 PM ( Sat-Sun)") : "6 AM- 10 PM ( M-F), 7 AM- 6 PM ( Sat-Sun)";


        Bundle bundle = this.getArguments();
        if (bundle != null) {
            private_lat = bundle.getString("lat");
            private_lang = bundle.getString("lang");
            private_address = bundle.getString("address");
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (myApp.isLocationAdded) {
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {
        txt_address.setText(private_address);
        txt_lang.setText(private_lat);
        txt_lat.setText(private_lang);
        txt_private_no_parking_time.setText(private_no_parking);
        txt_private_parking_time.setText(private_parking_time);
        txt_title.setText(private_title);
        edit_private_loc_code.setText(Private_location_code);
    }

    @Override
    protected void setListeners() {
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        sp_township_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                township_type_sp = (String) sp_township_type.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> paren1t) {
            }
        });

        txt_sp1_private_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                private_title = txt_title.getText().toString();
                private_address = txt_address.getText().toString();
                private_no_parking = txt_private_no_parking_time.getText().toString();
                private_parking_time = txt_private_parking_time.getText().toString();
                if (!private_title.equals("")) {
                    if (!private_address.equals("")) {
                        if (!private_no_parking.equals("")) {
                            if (!private_parking_time.equals("")) {
                                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                frg_private_edit_add_step2 pay = new frg_private_edit_add_step2();
                                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                Bundle bundle = new Bundle();
                                bundle.putString("township_type_sp", township_type_sp);
                                bundle.putBoolean("isNewLocation", isNewLocation);
                                pay.setArguments(bundle);
                                ft.add(R.id.My_Container_1_ID, pay);
                                fragmentStack.lastElement().onPause();
                                ft.hide(fragmentStack.lastElement());
                                fragmentStack.push(pay);
                                ft.commitAllowingStateLoss();
                            } else {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                alertDialog.setTitle("parking Times Required");
                                alertDialog.setMessage("Please enter  parking time text for parking");
                                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                                alertDialog.show();
                            }
                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("No Parking Times Required");
                            alertDialog.setMessage("Please enter no parking time for parking");
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialog.show();
                        }
                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Address Required");
                        alertDialog.setMessage("Please enter address of parking");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Title Required");
                    alertDialog.setMessage("Please enter title of parking");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
        });
        if (clickonedit) {
            txt_address.setText(private_address);
            txt_title.setText(private_title);
            edit_private_loc_code.setText(Private_location_code);
            txt_private_parking_time.setText(private_parking_time);
            txt_private_no_parking_time.setText(private_no_parking);
            txt_lat.setText(private_lat);
            txt_lat.setText(private_lang);
        }
    }

    @Override
    protected void initViews(View view) {
        txt_select_location = (TextView) view.findViewById(R.id.txt_select_parking);
        txt_address = (EditText) view.findViewById(R.id.txt_address);
        txt_title = (EditText) view.findViewById(R.id.txt_title);
        txt_lat = (TextView) view.findViewById(R.id.txt_lat);
        txt_lang = (TextView) view.findViewById(R.id.txt_lang);
        edit_private_loc_code = (EditText) view.findViewById(R.id.edit_private_loc_code);
        txt_private_parking_time = (EditText) view.findViewById(R.id.txt_private_parking_time);
        txt_private_no_parking_time = (EditText) view.findViewById(R.id.txt_private_no_parking_time);
        txt_sp1_private_next = (TextView) view.findViewById(R.id.txt_sp1_next);
        rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);

        sp_township_type = (Spinner) view.findViewById(R.id.sp_type_township);

        ArrayAdapter<String> sp_type_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, township_type);
        sp_township_type.setAdapter(sp_type_adapter);
    }
}
