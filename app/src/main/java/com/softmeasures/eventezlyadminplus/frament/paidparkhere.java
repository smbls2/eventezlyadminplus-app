package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.Splashscreen;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.dialogs.PaymentOptionDialog;
import com.softmeasures.eventezlyadminplus.dialogs.PaymentOptionListener;
import com.softmeasures.eventezlyadminplus.frament.reservation.ReservationParkingDetailsFragment;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.LocationLot;
import com.softmeasures.eventezlyadminplus.models.ReserveParking;
import com.softmeasures.eventezlyadminplus.services.Constants;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;
import com.softmeasures.eventezlyadminplus.services.GeocodeAddressIntentService;
import com.softmeasures.eventezlyadminplus.services.NotificationPublisher;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static android.content.Context.ALARM_SERVICE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.all_state;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class paidparkhere extends Fragment implements vehicle_add.vehivcle_add_Click_listener, PaymentOptionListener {
    //////////////////paypal///////////////////
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    private static final String TAG = "paymentQlighting";
    private static final String CONFIG_CLIENT_ID = "AQXvOmlRwvO2AI6H3XzqTe14pp4x6VtP2skKfUkhgjRAoNj0NoT7l0ZGeaRAXkAtYqyPCmxKEUXkrT89";
    private static final int REQUEST_CODE_PAYMENT = 2;
    private static PayPalConfiguration config = new PayPalConfiguration().environment(CONFIG_ENVIRONMENT).clientId(CONFIG_CLIENT_ID);
    ProgressDialog pdialog;
    ArrayList<item> listofvehicle = new ArrayList<>();
    ArrayList<item> wallbalarray = new ArrayList<>();
    ListView listofvehicleno;
    TextView txttime, txtmaxhores, txtrate, txt_parking_location_lable;
    EditText editno;
    Spinner spsate;
    int i = 1;
    View layout;
    PopupWindow popup;
    public PopupWindow popup_state;
    String rate1;
    Map<String, Object> statefullname;
    ConnectionDetector cd;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar, rl_progressbar1, rl_sp_row, rl_sp_space;
    ArrayList<String> values = new ArrayList<>();
    double finalpayamount, newbal, subTotal;
    Double tr_percentage1 = 0.0, tr_percentage_for_parked_cars = 0.0, tr_fee1 = 0.0, latitude = 0.0, longitude = 0.0;
    int hours;
    RelativeLayout showsucessmesg;
    SimpleDateFormat sdf;
    Spinner sphoures, sp_row, sp_space;
    RelativeLayout rl_main;
    Animation animation;
    String statename = "State", shoues = "", rate, adrreslocation = "null", countryname, p_country, nbal, cbal = "0", location_code, township_code = "null", ip, currentdate, finallab, id, platno, plateState, state, city, zipcode, locationname = "", max_time, lat, lang, zip_code, user_id, managedpin, token1, locationaddress, pid, marker, title, duration_unit, expiretime;
    String lost_row = "ROW", lost_no = "SPACE #", parkingtype, lost_row1, lost_no1, min, total_hours, renew, marker_type = "", township_code1;
    int token = 0;
    String statearray[] = {"State", "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"};
    String row[] = {"ROW", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    ArrayList<String> space = new ArrayList<>();
    boolean isclickonwallet = false;
    boolean isClickPayPal = false;
    boolean isClickReserved = false;
    String paymentmethod = "", transection_id, wallet_id = "";
    ArrayList<item> filtermanaged = new ArrayList<>();
    ArrayList<item> other_parking_rule = new ArrayList<item>();
    String township_row = "", township_spaces = "", township_labeling = "", township_sapce_marked = "", location_id;
    int total_row = 0;
    ArrayList<String> row_array = new ArrayList<>();
    ArrayList<String> row_number_array = new ArrayList<>();
    TextView txt_state, txt_hours, txt_row, txt_space;
    String township_location_name;
    ArrayList<item> managedlostpoparray = new ArrayList<>();
    ArrayList<item> data = new ArrayList<>();
    RelativeLayout rl_title;
    final ArrayList<item> stateq = new ArrayList<item>();
    final ArrayList<item> state_array = new ArrayList<item>();
    ImageView txt_plate_no;
    AddressResultReceiver mResultReceiver;
    static boolean is_add_back = false;
    String Renew_date, ReNew_id, Renew_parked_time, Renew_is_valid = "";
    int final_count = 0;
    private int parkingQty;
    private double parkingUnits;
    private double parkingRate;
    private TextView tvBtnParkNow, tvBtnReserveNow;
    private LinearLayout llParkOptions;
    private boolean isPayLaterClick = false;
    private boolean isReserved = false;
    private String reserveEntryTime = "";
    private String reserveExpiryTime = "";
    private String reserveCurrentTime = "";
    private int intHour = 0;

    private boolean isReservationAllowed = false;
    public static boolean isPrePaymentRequiredForReservation = true;
    private String reservationPostPayTerms = "";

    public static boolean isParkNowPostPayAllowed = false;
    private String parkNowPostPayTerms = "";

    private String parkingType = "";

    private boolean isPrePaid = false;

    private String companyId = "", twpId = "";

    private boolean isClickParking = true;

    //For new changes
    public static boolean mIsParking = false; // parking = true, reserve = false
    private boolean mIsPayNow = false; //payNow = true, payLater = false
    private boolean mIsWalletPayment = true; //pay with wallet = true, make payment = false

    private double mTrFee = 0.0, mTrPercent = 0.0, mParkingSubTotal = 0.0, mParkingTotal = 0.0,
            mParkNowPostPayFees = 0.0, mReserveNowPostPayFees = 0.0, mCancelationCharges = 0.0,
            mParkNowPostPayLateFees = 0.0, mReserveNowPostPayLateFees = 0.0;
    private boolean mIsCanCancelReservation = false;

    private ArrayList<LocationLot> locationLots = new ArrayList<>();

    private LocationLot selectedLocationLot;

    private androidx.appcompat.app.AlertDialog dialogLocationLot;
    private boolean isReserveSpot = false;
    private boolean isStateSelected = false;
    private String currentDurationUnit = "";

    private String mAddress1 = "", mAddress2 = "", mDistrict = "", mCity = "", mStateCode = "", mCountryCode = "", mZipCode = "";
    private String mMarkerAddress2 = "", mMarkerDistrict = "", mMarkerCity = "",
            mMarkerStateCode = "", mMarkerCountryCode = "", mMarkerZipCode = "";
    private boolean isReserveDateSelected = false;
    private ArrayList<ReserveParking> reserveParkings = new ArrayList<>();
    private long selectedEntryTime, selectedExitTime;
    private String manager_type_id = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        if (getArguments() != null) {
            parkingType = getArguments().getString("parkingType");
            isReserveSpot = getArguments().getBoolean("isReserveSpot");
            Log.e("#DEBUG", "   paidparkhere:  parkingType:  " + parkingType + "  isReserveSpot: " + isReserveSpot);
        }

        View view = inflater.inflate(R.layout.fragment_paidparkhere, container, false);
        SharedPreferences back_select = getActivity().getSharedPreferences("back_paid_parking", Context.MODE_PRIVATE);
        SharedPreferences.Editor edd = back_select.edit();
        edd.putString("back", "yes");
        edd.commit();

        mResultReceiver = new AddressResultReceiver(null);

        llParkOptions = view.findViewById(R.id.llParkOptions);
        tvBtnParkNow = view.findViewById(R.id.tvBtnParkNow);
        tvBtnReserveNow = view.findViewById(R.id.tvBtnReserveNow);
        if (isReserveSpot) tvBtnParkNow.setVisibility(View.GONE);

        rl_title = (RelativeLayout) view.findViewById(R.id.title);
        txttime = (TextView) view.findViewById(R.id.parkheretime);
        txt_plate_no = (ImageView) view.findViewById(R.id.txt_plate);
        txtmaxhores = (TextView) view.findViewById(R.id.parkheremaxhours);
        txtrate = (TextView) view.findViewById(R.id.rate);
        rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        // txtpaypark = (TextView) view.findViewById(R.id.paypark);
        editno = (EditText) view.findViewById(R.id.editparkherenoplantno);
        txt_state = (TextView) view.findViewById(R.id.txt_state);
        txt_hours = (TextView) view.findViewById(R.id.txt_hours);
        txt_row = (TextView) view.findViewById(R.id.txt_row);
        txt_space = (TextView) view.findViewById(R.id.txt_space);
        spsate = (Spinner) view.findViewById(R.id.spsatename);
        sp_row = (Spinner) view.findViewById(R.id.sp_row11);
        sp_space = (Spinner) view.findViewById(R.id.sp_space11);
        showsucessmesg = (RelativeLayout) view.findViewById(R.id.sucessfully);
        rl_sp_row = (RelativeLayout) view.findViewById(R.id.rl_sp_row);
        rl_sp_space = (RelativeLayout) view.findViewById(R.id.rl_sp_space);
        txt_parking_location_lable = (TextView) view.findViewById(R.id.txt_parkinglocation);
        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Thin.ttf");
        SharedPreferences s = getActivity().getSharedPreferences("paidparkhere", Context.MODE_PRIVATE);
        String time = s.getString("Time", "");
        final String max = s.getString("Max", "");
        max_time = s.getString("Max", "");
        rate = s.getString("rate", "");
        lat = s.getString("lat", "");
        lang = s.getString("lang", "");
        zip_code = s.getString("zip_code", "");
        rate1 = s.getString("rated", "");
        location_code = s.getString("location_code", "");
        locationname = s.getString("location_name", "");
        zipcode = s.getString("zip_code", "");
        city = s.getString("city", "");
        pid = s.getString("id", "null");
        location_id = s.getString("location_id", "");
        managedpin = s.getString("managedloick", "null");
        locationaddress = s.getString("address", "null");
        marker = s.getString("mar", "null");
        Log.e("#DEBUG", "   paidparkhere:  marker:  " + marker);
        title = s.getString("title", "null");
        township_code1 = s.getString("township_code", "FDV");
        marker_type = s.getString("parking_type", "");
        duration_unit = s.getString("duation_unit", "Hours");
        manager_type_id = s.getString("manager_type_id", "1");
        if (TextUtils.isEmpty(manager_type_id)) manager_type_id = "1";
        currentDurationUnit = duration_unit;

        Log.e("#DEBUG", "    max_time: " + max_time
                + "\nrate: " + rate + "\nlat: " + lat + "\nlang: " + lang + "\nzip_code: " + zip_code + "\nrate1: " + rate1
                + "\nlocation_code: " + location_code + "\nlocationname: " + locationname + "\nzipcode: " + zipcode
                + "\ncity: " + city + "\npid: " + pid + "\nlocation_id: " + location_id + "\nmanagedpin: " + managedpin
                + "\nlocationaddress: " + locationaddress + "\nmarker: " + marker + "\ntitle: " + title
                + "\ntownship_code1: " + township_code1 + "\nmarker_type: " + marker_type
                + "\nduration_unit: " + duration_unit + "\nmanager_type_id:  " + manager_type_id);

        cd = new ConnectionDetector(getActivity());
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar_car);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        rl_progressbar1 = (RelativeLayout) view.findViewById(R.id.rl_progressbar_car);
        sphoures = (Spinner) view.findViewById(R.id.sphours);
        values.add("hours");
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        id = logindeatl.getString("id", "null");
        user_id = id;

        SharedPreferences appSharedPrefs = getActivity().getSharedPreferences("parking_rules", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json1 = appSharedPrefs.getString("managed", "");
        Type type1 = new TypeToken<ArrayList<item>>() {
        }.getType();
        filtermanaged = gson.fromJson(json1, type1);
        ((vchome) getActivity()).popupopen();
        llParkOptions.setVisibility(View.GONE);
        RelativeLayout rl = (RelativeLayout) view.findViewById(R.id.rlrl);
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        rl_progressbar1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        space.add(("SPACE#"));
        for (int i = 1; i <= 100; i++) {
            space.add(String.valueOf(i));
        }

        editno.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    ShowStatePupup(getActivity(), state_array);
                    return true;
                }
                return false;
            }
        });

        if (cd.isConnectingToInternet()) {
            currentlocation();
            new fetchLatLongFromaddress(String.valueOf(latitude), String.valueOf(longitude)).execute();
            new other_parking_rule().execute();

            final ArrayList<item> array_space = new ArrayList<>();
            final ArrayList<item> array_row = new ArrayList<>();
            ArrayList<item> array = new ArrayList<item>();
            if (managedpin.equals("yes")) {
                txt_parking_location_lable.setVisibility(View.GONE);
                rl_sp_row.setVisibility(View.VISIBLE);
                if (filtermanaged.size() > 0) {
                    for (int i = 0; i < filtermanaged.size(); i++) {
                        township_labeling = filtermanaged.get(i).getLabeling();
                        township_row = filtermanaged.get(i).getTotal_rows();
                        township_spaces = filtermanaged.get(i).getTotal_spaces();
                        township_sapce_marked = "true";
                        township_location_name = filtermanaged.get(i).getLoationname();
                        break;
                    }
//                    if (isInteger(township_spaces) && isInteger(township_row)) {
//                        int total_spces = Integer.parseInt(township_spaces);
//                        total_row = Integer.parseInt(township_row);
//                        int rowcount = 1;
//                        if (total_row > 0 && total_spces > 0) {
//                            if (total_spces >= total_row) {
//                                rowcount = Integer.parseInt(township_spaces) / Integer.parseInt(township_row);
//                                if ((total_spces % total_row) > 0) {
//                                }
//                            } else {
//                                rowcount = 1;
//                            }
//                        }
//
//                        int curren_row = 0;
//                        int first_row = 1;
//                        String row[] = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
//
//                        int array_count = 0;
//                        if (township_sapce_marked.equals("false")) {
//                            array_count = total_row;
//                        } else {
//                            array_count = total_spces;
//                        }
//                        //township_labeling="AlphaNumeric";
//                        if (township_labeling.equals("Alpha")) {
//
//                            for (int i = 0; i < array_count; i++) {
//
//                                item ii = new item();
//                                curren_row++;
//                                if (curren_row == rowcount) {
//                                    if (township_sapce_marked.equals("false")) {
//                                        ii.setDisplay(row[first_row - 1].toString());
//                                    } else if (total_row < 2) {
//                                        ii.setDisplay(row[first_row - 1].toString());
//                                    } else {
//                                        ii.setDisplay(row[first_row - 1].toString() + " : " + row[curren_row - 1].toString());
//                                    }
//                                    ii.setIsparked("f");
//                                    ii.setSelected_parking(false);
//                                    ii.setSpaces_mark_reqd(township_sapce_marked);
//                                    ii.setRow_no(row[curren_row - 1].toString());
//                                    ii.setRoe_name(row[first_row - 1].toString());
//                                    //ii.setDisplay(row[first_row - 1].toString() + " : " + row[curren_row - 1].toString());
//                                    // ii.setDisplay(row[first_row - 1].toString());
//                                    ii.setMenu_img_id(total_row);
//                                    ii.setMarker("red");
//                                    array.add(ii);
//                                    first_row++;
//                                    curren_row = 0;
//                                } else {
//                                    ii.setSpaces_mark_reqd(township_sapce_marked);
//                                    ii.setIsparked("f");
//                                    ii.setRow_no(row[curren_row - 1].toString());
//                                    ii.setRoe_name(row[first_row - 1].toString());
//                                    ii.setMenu_img_id(total_row);
//                                    ii.setMarker("red");
//                                    ii.setSelected_parking(false);
//                                    if (township_sapce_marked.equals("false")) {
//                                        ii.setDisplay(row[first_row - 1].toString());
//                                    } else if (total_row < 2) {
//                                        ii.setDisplay(row[first_row - 1].toString());
//                                    } else {
//                                        ii.setDisplay(row[first_row - 1].toString() + " : " + row[curren_row - 1].toString());
//                                    }
//                                    //ii.setDisplay(row[first_row - 1].toString() + " : " + row[curren_row - 1].toString());
//                                    array.add(ii);
//                                }
//                            }
//
//                            for (int j = 0; j < array.size(); j++) {
//                                Log.e("row", array.get(j).toString());
//                            }
//                        } else if (township_labeling.equals("Numeric")) {
//
//
//                            for (int i = 0; i < array_count; i++) {
//                                item ii = new item();
//                                curren_row++;
//                                if (curren_row == rowcount) {
//                                    if (township_sapce_marked.equals("false")) {
//                                        ii.setDisplay(String.valueOf(i + 1));
//                                    } else if (total_row < 2) {
//                                        ii.setDisplay(String.valueOf(first_row));
//                                    } else {
//                                        ii.setDisplay(String.valueOf(first_row) + " : " + String.valueOf(curren_row));
//                                    }
//                                    ii.setSpaces_mark_reqd(township_sapce_marked);
//                                    ii.setIsparked("f");
//                                    ii.setRow_no(String.valueOf(curren_row));
//                                    ii.setRoe_name(String.valueOf(first_row));
//                                    ii.setMenu_img_id(total_row);
//                                    ii.setMarker("red");
//                                    ii.setSelected_parking(false);
//                                    //  ii.setDisplay(String.valueOf(first_row) + " : " + String.valueOf(curren_row));
//                                    // ii.setDisplay(String.valueOf(first_row));
//                                    array.add(ii);
//                                    first_row++;
//                                    curren_row = 0;
//                                } else {
//                                    if (township_sapce_marked.equals("false")) {
//                                        ii.setDisplay(String.valueOf(i + 1));
//                                    } else if (total_row < 2) {
//                                        ii.setDisplay(String.valueOf(first_row));
//                                    } else {
//                                        ii.setDisplay(String.valueOf(first_row) + " : " + String.valueOf(curren_row));
//                                    }
//                                    ii.setSpaces_mark_reqd(township_sapce_marked);
//                                    ii.setIsparked("f");
//                                    ii.setSelected_parking(false);
//                                    ii.setRow_no(String.valueOf(curren_row));
//                                    ii.setRoe_name(String.valueOf(first_row));
//                                    ii.setMenu_img_id(total_row);
//                                    ii.setMarker("red");
//                                    // ii.setDisplay(String.valueOf(first_row) + " : " + String.valueOf(curren_row));
//                                    array.add(ii);
//
//                                }
//                            }
//
//                            for (int j = 0; j < array.size(); j++) {
//                                Log.e("row", array.get(j).toString());
//                            }
//                        } else if (township_labeling.equals("AlphaNumeric")) {
//
//                            for (int i = 0; i < array_count; i++) {
//                                item ii = new item();
//                                curren_row++;
//                                if (curren_row == rowcount) {
//                                    ii.setSpaces_mark_reqd(township_sapce_marked);
//                                    ii.setIsparked("f");
//                                    ii.setSelected_parking(false);
//                                    ii.setRow_no(String.valueOf(curren_row));
//                                    ii.setRoe_name(row[first_row - 1].toString());
//                                    ii.setMenu_img_id(total_row);
//                                    ii.setMarker("red");
//                                    ii.setDisplay(row[first_row - 1].toString() + " : " + String.valueOf(curren_row));
//                                    array.add(ii);
//                                    first_row++;
//                                    curren_row = 0;
//                                } else {
//                                    ii.setSpaces_mark_reqd(township_sapce_marked);
//                                    ii.setIsparked("f");
//                                    ii.setSelected_parking(false);
//                                    ii.setMarker("red");
//                                    ii.setRow_no(String.valueOf(curren_row));
//                                    ii.setMenu_img_id(total_row);
//                                    ii.setRoe_name(row[first_row - 1].toString());
//                                    ii.setDisplay(row[first_row - 1].toString() + " : " + String.valueOf(curren_row));
//                                    array.add(ii);
//                                }
//                            }
//                            for (int j = 0; j < array.size(); j++) {
//                                Log.e("row", array.get(j).getDisplay());
//                            }
//                        }
//                    new fetchLocationLotOccupiedData(location_code, array).execute();
//                    }

//                    new fetchLocationLots(location_id).execute();
//                    new fetchLocationLotOccupiedData().execute();
                    rl_sp_row.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showDialogLocationLots();
                        }
                    });
                }
                new fetchLocationLotOccupiedData().execute();
            } else {
                txt_parking_location_lable.setVisibility(View.GONE);
                rl_sp_row.setVisibility(View.GONE);
                rl_sp_space.setVisibility(View.GONE);
            }

            new servicecharge().execute();
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            TimeZone zone = TimeZone.getTimeZone("UTC");
            currentdate = sdf.format(new Date());


            editno.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        show_marke_payment_button();
                        ShowStatePupup(getActivity(), state_array);
                        return true;
                    }
                    return false;
                }
            });

            if (!max.equals("") && !rate1.equals("")) {
                try {

                    values.clear();
                    values.add(duration_unit + "s");
                    double limt = Double.parseDouble(max);
                    double pricing_dur = Double.parseDouble(rate1);
                    int ssw = (int) (limt / pricing_dur);
                    for (int y = 1; y <= ssw; y++) {
                        if (duration_unit.equals("Minute")) {
                            int min = (int) (y * pricing_dur);
                            if (min == 1) {
                                values.add(String.valueOf(min) + " Min");
                            } else {
                                values.add(String.valueOf(min) + " Mins");
                            }
                        } else if (duration_unit.equals("Month")) {
                            int min = (int) (y * pricing_dur);
                            if (min == 1) {
                                values.add(String.valueOf(min) + " Month");
                            } else values.add(String.valueOf(min) + " Months");
                        } else if (duration_unit.equals("Hour")) {
                            int min = (int) (y * pricing_dur);
                            if (min == 1) {
                                values.add(String.valueOf(min) + " Hr");
                            } else values.add(String.valueOf(min) + " Hrs");
                        } else if (duration_unit.equals("Day")) {
                            int min = (int) (y * pricing_dur);
                            if (min == 1) {
                                values.add(String.valueOf(min) + " Day");
                            } else values.add(String.valueOf(min) + " Days");
                        } else if (duration_unit.equals("Week")) {
                            int min = (int) (y * pricing_dur);
                            if (min == 1) {
                                values.add(String.valueOf(min) + " Week");
                            } else values.add(String.valueOf(min) + " Weeks");
                        }
                    }
                } catch (ArithmeticException a) {
                } catch (Exception E) {
                }

                if (!rate.equals("") && !rate1.equals("")) {
                    txtrate.setText(String.format("Rate: $%s@%s %s", rate, rate1, duration_unit));
                    if (!rate1.equals("1")) {
                        if (duration_unit.equals("Hour")) {
                            txtrate.setText(String.format("Rate: $%s@%s Hours", rate, rate1));
                        } else if (duration_unit.equals("Minute")) {
                            txtrate.setText(String.format("Rate: $%s@%s Minutes", rate, rate1));
                        } else if (duration_unit.equals("Day")) {
                            txtrate.setText(String.format("Rate: $%s@%s Days", rate, rate1));
                        } else if (duration_unit.equals("Week")) {
                            txtrate.setText(String.format("Rate: $%s@%s Weeks", rate, rate1));
                        } else if (duration_unit.equals("Month")) {
                            txtrate.setText(String.format("Rate: $%s@%s Months", rate, rate1));
                        }
                    } else {
                        txtrate.setText(String.format("Rate: $%s@%s %s", rate, rate1, duration_unit));
                    }
                    Double du = Double.parseDouble(rate1) / 60.0;
                }
                if (!time.equals("")) {
                    txttime.setText(time);
                    if (!max.equals("")) {
                        txtmaxhores.setText(String.format("Max: %s %s", max, duration_unit));
                        if (!max.equals("1")) {
                            if (duration_unit.equals("Hour")) {
                                txtmaxhores.setText(String.format("Max: %s Hours", max));
                            } else if (duration_unit.equals("Minute")) {
                                txtmaxhores.setText(String.format("Max: %s Minutes", max));
                            } else if (duration_unit.equals("Day")) {
                                txtmaxhores.setText(String.format("Max: %s Days", max));
                            } else if (duration_unit.equals("Week")) {
                                txtmaxhores.setText(String.format("Max: %s Weeks", max));
                            } else if (duration_unit.equals("Month")) {
                                txtmaxhores.setText(String.format("Max: %s Months", max));
                            }
                        } else {
                            txtmaxhores.setText(String.format("Max: %s %s", max, duration_unit));
                        }
                    }
                }
            }
            if (marker.equals("google")) {
                values.clear();
                values.add("hours");
                try {
//                   for (int i = 1; i <= 24; i++) {
//                        values.add(String.valueOf(i) + " Hrs");
//                    }
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, values);
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    sphoures.setAdapter(spinnerArrayAdapter);
                } catch (ArithmeticException a) {
                } catch (Exception E) {
                }
            }


            for (int i = 1; i < values.size(); i++) {
                item ii = new item();
                ii.setState(values.get(i).toString());
                ii.setSelected_parking(false);
                stateq.add(ii);
            }

            txt_hours.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ShowHours(getActivity(), stateq);
                }
            });

            if (stateq.size() >= 4) {
                stateq.get(3).setSelected_parking(true);
                shoues = stateq.get(3).getState();
                min = shoues.substring(shoues.indexOf(' ') + 1);
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    txt_hours.setText(hr + " Hrs");
                    Double d = new Double(Double.parseDouble(hr));
                    hours = d.intValue();
                    calculateEntryExitTime();
                } else {
                    txt_hours.setText(shoues);
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    Double d = new Double(Double.parseDouble(hr));
                    hours = d.intValue();
                    calculateEntryExitTime();
                }

            } else if (stateq.size() >= 3) {
                stateq.get(2).setSelected_parking(true);
                shoues = stateq.get(2).getState();
                min = shoues.substring(shoues.indexOf(' ') + 1);
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    txt_hours.setText(hr + " Hrs");
                    Double d = new Double(Double.parseDouble(hr));
                    hours = d.intValue();
                    calculateEntryExitTime();
                } else {
                    txt_hours.setText(shoues);
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    Double d = new Double(Double.parseDouble(hr));
                    hours = d.intValue();
                    calculateEntryExitTime();
                }
            } else if (stateq.size() >= 2) {
                stateq.get(1).setSelected_parking(true);
                shoues = stateq.get(1).getState();
                min = shoues.substring(shoues.indexOf(' ') + 1);
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    txt_hours.setText(hr + " Hrs");
                    Double d = new Double(Double.parseDouble(hr));
                    hours = d.intValue();
                    calculateEntryExitTime();
                } else {
                    txt_hours.setText(shoues);
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    Double d = new Double(Double.parseDouble(hr));
                    hours = d.intValue();
                    calculateEntryExitTime();
                }
            }

            txt_hours.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ShowHours(getActivity(), stateq);
                }
            });
            SharedPreferences d = getActivity().getSharedPreferences("renew", Context.MODE_PRIVATE);
            renew = d.getString("renew", "no");
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, values);
            sphoures.setAdapter(spinnerArrayAdapter);
            sphoures.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    shoues = (String) sphoures.getSelectedItem();
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    Double d = new Double(Double.parseDouble(hr));
                    hours = d.intValue();
                    calculateEntryExitTime();
                }

                @Override
                public void onNothingSelected(AdapterView<?> paren1t) {
                }
            });
            sp_row.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    lost_row = (String) sp_row.getSelectedItem();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            sp_space.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    lost_no = (String) sp_space.getSelectedItem();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, statearray);
            spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spsate.setAdapter(spinnerArrayAdapter1);
            spsate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    statename = (String) spsate.getSelectedItem();
                    platno = editno.getText().toString();
                    plateState = statename;
                    try {
                        ((TextView) view).setTextColor(Color.BLACK);
                    } catch (Exception e) {
                    }


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
            state_array.clear();
            for (int i = 1; i < all_state.size(); i++) {
                String statename = all_state.get(i).getState2Code();
                item ii = new item();
                ii.setState(statename);
                if (statename.equals("NY")) {
                    ii.setSelected_parking(true);
                    this.statename = statename;
                    txt_state.setText("NY");
                } else {
                    ii.setSelected_parking(false);
                }
                state_array.add(ii);
            }

            txt_state.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!isStateSelected) {
                        ShowStatePupup(getActivity(), state_array);
                    }
                }
            });
            if (renew.equals("yes")) {
                show_marke_payment_button();
                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                String platno2 = sh.getString("platno", "");
                String statename2 = sh.getString("state", "");
                String lotrow_ = sh.getString("lot_row", "a");
                String lot_no_ = sh.getString("lot_no", "1");
                Renew_date = d.getString("cureent_date", "0");
                ReNew_id = d.getString("renew_id", "0");
                marker = d.getString("marker", "0");
                Renew_parked_time = d.getString("parked_time", "0");
                Renew_is_valid = d.getString("car_is_valid", "no");
                lost_no = lot_no_;
                lost_row = lotrow_;
                editno.setText(platno2);
                String marker = d.getString("marker", "null");
                if (marker.equals("guest")) {
                    txt_parking_location_lable.setVisibility(View.GONE);
                    rl_sp_row.setVisibility(View.VISIBLE);
                    //  rl_sp_space.setVisibility(View.VISIBLE);
                }

                if (marker.contains("managed")) {
                    rl_sp_row.setVisibility(View.VISIBLE);
                    txt_row.setText(lot_no_ + " : " + lotrow_);
                } else {
                    rl_sp_row.setVisibility(View.GONE);
                }
                for (int i = 0; i < statearray.length; i++) {

                    String ss = statearray[i].toString();
                    if (ss.equals(statename2)) {
                        spsate.setSelection(i);
                        break;
                    }
                }

                txt_state.setText(statename2);

                state_array.clear();
                for (int i = 1; i < all_state.size(); i++) {
                    String statename = all_state.get(i).getState2Code();
                    item ii = new item();
                    ii.setState(statename);
                    if (statename.equals(statename2)) {
                        this.statename = statename;
                        ii.setSelected_parking(true);
                        txt_state.setText("NY");
                    } else {
                        ii.setSelected_parking(false);
                    }
                    state_array.add(ii);
                }
                for (int k = 0; k < row.length; k++) {
                    if (lotrow_.equals("")) {
                        lotrow_ = "a";
                    }
                    String row1 = row[k].toString();

                    if (row1.equals(lotrow_.toUpperCase())) {
                        sp_row.setSelection(k);
                        break;
                    }
                }


                SharedPreferences sh2 = getActivity().getSharedPreferences("renew", Context.MODE_PRIVATE);
                sh2.edit().clear().commit();
                SharedPreferences back_select2 = getActivity().getSharedPreferences("back_paid_parking", Context.MODE_PRIVATE);
                back_select2.edit().clear().commit();
            } else {
                if (!id.equals("null")) {
                    SharedPreferences dd = getActivity().getSharedPreferences("select_vehicle", Context.MODE_PRIVATE);
                    String select_plate_no = dd.getString("plate_no", "");
                    String select_state = dd.getString("state", "");
                    p_country = dd.getString("p_country", "");
                    if (renew.equals("yes")) {
                        SharedPreferences back_select2 = getActivity().getSharedPreferences("back_paid_parking", Context.MODE_PRIVATE);
                        back_select2.edit().clear().commit();
                    }
                    /*editno.setText(select_plate_no.toUpperCase());
                    for (int i = 0; i < statearray.length; i++)
                    {
                        String state = statearray[i];
                        if (state.equals(select_state))
                        {
                            spsate.setSelection(i);
                            statename=select_state;
                            break;
                        }
                    }*/

                    state_array.clear();
                    for (int i = 1; i < all_state.size(); i++) {
                        String statename = all_state.get(i).getState2Code();
                        item ii = new item();
                        ii.setState(statename);
                        if (statename.equals(select_state)) {
                            this.statename = statename;
                            ii.setSelected_parking(true);
                            txt_state.setText(select_state);
                        } else {
                            ii.setSelected_parking(false);
                        }
                        state_array.add(ii);
                    }
                    txt_plate_no.setVisibility(View.VISIBLE);
                    txt_state.setEnabled(true);
                    editno.setVisibility(View.VISIBLE);
                    //new getvehiclenumber().execute();
                } else {
                    txt_plate_no.setVisibility(View.VISIBLE);
                    txt_state.setEnabled(true);
                    editno.setVisibility(View.VISIBLE);
                    SharedPreferences dd = getActivity().getSharedPreferences("select_vehicle", Context.MODE_PRIVATE);
                    String select_plate_no = dd.getString("plate_no", "");
                    String select_state = dd.getString("state", "");
                    p_country = dd.getString("p_country", "");
                   /* editno.setText(select_plate_no.toUpperCase());
                    for (int i = 0; i < statearray.length; i++)
                    {
                        String state = statearray[i];
                        if (state.equals(select_state))
                        {
                            spsate.setSelection(i);
                            break;
                        }
                    }

                    state_array.clear();*/
                  /*  for (int i = 1; i < statearray.length; i++) {
                        String statename = statearray[i].toString();
                        item ii = new item();
                        ii.setState(statearray[i].toString());
                        if (statename.equals(select_state)) {
                            ii.setSelected_parking(true);
                            this.statename=statename;
                            txt_state.setText(select_state);
                        } else {
                            ii.setSelected_parking(false);
                        }
                        state_array.add(ii);
                    }*/

                    SharedPreferences back_select2 = getActivity().getSharedPreferences("back_paid_parking", Context.MODE_PRIVATE);
                    back_select2.edit().clear().commit();
                }

                p_country = p_country != null ? (!p_country.equals("null") ? (!p_country.equals("") ? p_country : "") : "") : "";
            }

            editno.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    show_marke_payment_button();
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String ss = editable.toString();
                    if (!ss.equals(ss.toUpperCase())) {

                        editno.setText(ss.toUpperCase());
                        editno.setSelection(ss.length());
                    }
                }
            });

            editno.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    show_marke_payment_button();
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String ss = editable.toString();
                    if (!ss.equals(ss.toUpperCase())) {

                        editno.setText(ss.toUpperCase());
                        editno.setSelection(ss.length());
                    }
                }
            });

            String user_id = logindeatl.getString("id", "");
            if (!user_id.equals("")) {
                if (!renew.equals("yes")) {
                    new getvehiclenumber().execute();
                }
            } else {
                showenter_vehicle(getActivity());
            }
            txt_plate_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShowState(getActivity());
                }
            });

            //  ActionStartsHere_popup();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connetrction Error");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();
        }
        statefullname = new HashMap<String, Object>();
        statefullname.put("AL", "Alabama");
        statefullname.put("AK", "Alaska");
        statefullname.put("AZ", "Arizona");
        statefullname.put("AR", "Arkansas");
        statefullname.put("CA", "California");
        statefullname.put("CO", "Colorado");
        statefullname.put("CT", "Connecticut");
        statefullname.put("DE", "Delaware");
        statefullname.put("DC", "District of Columbia");
        statefullname.put("FL", "Florida");
        statefullname.put("GA", "Georgia");
        statefullname.put("HI", "Hawaii");
        statefullname.put("ID", "Idaho");
        statefullname.put("IL", "Illinois");
        statefullname.put("IN", "Indiana");
        statefullname.put("IA", "Iowa");
        statefullname.put("KS", "Kansas");
        statefullname.put("KY", "Kentucky");
        statefullname.put("LA", "Louisiana");
        statefullname.put("ME", "Maine");
        statefullname.put("MD", "Maryland");
        statefullname.put("MA", "Massachusetts");
        statefullname.put("MI", "Michigan");
        statefullname.put("MN", "Minnesota");
        statefullname.put("MS", "Mississippi");
        statefullname.put("MO", "Missouri");
        statefullname.put("MT", "Montana");
        statefullname.put("NE", "Nebraska");
        statefullname.put("NV", "Nevada");
        statefullname.put("NH", "New Hampshire");
        statefullname.put("NJ", "New Jersey");
        statefullname.put("NM", "New Mexico");
        statefullname.put("NY", "New York");
        statefullname.put("NC", "North Carolina");
        statefullname.put("ND", "North Dakota");
        statefullname.put("OH", "Ohio");
        statefullname.put("OK", "Oklahoma");
        statefullname.put("OR", "Oregon");
        statefullname.put("PA", "Pennsylvani");
        statefullname.put("RI", "Rhode Island");
        statefullname.put("SC", "South Carolina");
        statefullname.put("SD", "South Dakota");
        statefullname.put("TN", "Tennessee");
        statefullname.put("TX", "Texas");
        statefullname.put("UT", "Utah");
        statefullname.put("VT", "Vermont");
        statefullname.put("VA", "Virginia");
        statefullname.put("WA", "Washington");
        statefullname.put("WV", "West virginia");
        statefullname.put("WI", "Wisconsin");
        statefullname.put("WY", "Wyoming");

        setListeners();


        return view;
    }

    private void setListeners() {
        tvBtnParkNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isReserveDateSelected = false;
                if (!TextUtils.isEmpty(marker)
                        && marker.contains("managed")
                        && selectedLocationLot == null) {
                    Toast.makeText(getActivity(), "Please select space!", Toast.LENGTH_SHORT).show();
                } else {
                    isReserved = false;
                    isPayLaterClick = false;

                    mIsParking = true;
                    PaymentOptionDialog dialog = PaymentOptionDialog.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("mIsPayLater", isParkNowPostPayAllowed);
                    dialog.setArguments(bundle);
                    dialog.show(getChildFragmentManager(), "payment");
                }
            }
        });

//        tvBtnParkNowPayLater.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(getActivity(), "Coming soon!", Toast.LENGTH_SHORT).show();
////                isReserved = false;
////                isPayLaterClick = true;
////                showParkNowPayLaterConfirmationDialog();
//            }
//        });

        tvBtnReserveNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isReserveDateSelected = false;
                if (selectedLocationLot == null) {
                    Toast.makeText(getActivity(), "Please select space!", Toast.LENGTH_SHORT).show();
                } else {
                    String hour = txt_hours.getText().toString();
                    intHour = Integer.valueOf(hour.substring(0, hour.indexOf(" ")));
                    isPayLaterClick = false;

                    mIsParking = false;
                    showReservationDialog(true);
                }
            }
        });

//        tvBtnReserveNowPayLater.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String hour = txt_hours.getText().toString();
//                intHour = Integer.valueOf(hour.substring(0, hour.indexOf(" ")));
//                showReservationDialog(false);
//
//            }
//        });
    }

    private void showReservationDialog(final boolean isPrePayment) {
        // reservation dialog
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.date_time_picker_dialog, null);
        final TextView tvSelectedTime = dialogView.findViewById(R.id.tvSelectedTime);
        final TextView tvExitTime = dialogView.findViewById(R.id.tvExitTime);
        TextView tvBtnCancel = dialogView.findViewById(R.id.tvBtnCancel);
        TextView tvBtnConfirm = dialogView.findViewById(R.id.tvBtnConfirm);
        builder.setView(dialogView);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        final String stringTime = "";
        final Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        reserveCurrentTime = dateFormat1.format(new Date(calendar.getTimeInMillis()));
        reserveEntryTime = dateFormat1.format(new Date(calendar.getTimeInMillis()));
        tvSelectedTime.setText(dateFormat1.format(new Date(calendar.getTimeInMillis())));
        if (currentDurationUnit.equals("Hour")) {
            calendar.add(Calendar.HOUR, intHour);
        } else if (currentDurationUnit.equals("Minute")) {
            calendar.add(Calendar.MINUTE, intHour);
        } else if (currentDurationUnit.equals("Day")) {
            calendar.add(Calendar.DAY_OF_MONTH, intHour);
        } else if (currentDurationUnit.equals("Week")) {
            calendar.add(Calendar.WEEK_OF_MONTH, intHour);
        } else if (currentDurationUnit.equals("Month")) {
            calendar.add(Calendar.MONTH, intHour);
        }
        tvExitTime.setText(dateFormat1.format(new Date(calendar.getTimeInMillis())));
        reserveExpiryTime = dateFormat1.format(new Date(calendar.getTimeInMillis()));

        tvSelectedTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        final Calendar calendar1 = Calendar.getInstance();
                        calendar1.set(Calendar.YEAR, i);
                        calendar1.set(Calendar.MONTH, i1);
                        calendar1.set(Calendar.DAY_OF_MONTH, i2);


                        new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                                calendar1.set(Calendar.HOUR_OF_DAY, i);
                                calendar1.set(Calendar.MINUTE, i1);

                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//                                stringTime = dateFormat.format(new Date(calendar1.getTimeInMillis()));
                                tvSelectedTime.setText(dateFormat.format(new Date(calendar1.getTimeInMillis())));
                                reserveEntryTime = dateFormat.format(new Date(calendar1.getTimeInMillis()));
                                if (currentDurationUnit.equals("Hour")) {
                                    calendar1.add(Calendar.HOUR, intHour);
                                } else if (currentDurationUnit.equals("Minute")) {
                                    calendar1.add(Calendar.MINUTE, intHour);
                                } else if (currentDurationUnit.equals("Day")) {
                                    calendar1.add(Calendar.DAY_OF_MONTH, intHour);
                                } else if (currentDurationUnit.equals("Week")) {
                                    calendar1.add(Calendar.WEEK_OF_MONTH, intHour);
                                } else if (currentDurationUnit.equals("Month")) {
                                    calendar1.add(Calendar.MONTH, intHour);
                                }
                                tvExitTime.setText(dateFormat.format(new Date(calendar1.getTimeInMillis())));
                                reserveExpiryTime = dateFormat.format(new Date(calendar1.getTimeInMillis()));

                                try {
                                    selectedEntryTime = dateFormat.parse(reserveEntryTime).getTime();
                                    selectedExitTime = dateFormat.parse(reserveExpiryTime).getTime();
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE),
                                false).show();
                    }
                }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH),
                        Calendar.getInstance().get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        tvBtnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isReserveDateSelected = true;
                alertDialog.dismiss();
                Log.e("#DEBUG", "   reserveEntryTime:  " + reserveEntryTime + " " +
                        "reserveExpiryTime:  " + reserveExpiryTime);
                Log.e("#DEBUG", "   reserveCurrentTime: " + reserveCurrentTime);
//                if (isPrePayment) {
////                    llPaymentOptions.setVisibility(View.VISIBLE);
//                    llParkOptions.setVisibility(View.GONE);
//                    isReserved = true;
//                    isPayLaterClick = false;
//                } else {
//                    isReserved = true;
//                    getTotalAmountReserved();
//                }

                showDialogLocationLots();
            }
        });

        tvBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();

    }

//    private void processReservationBooking() {
//        if (cd.isConnectingToInternet()) {
//            isclickonwallet = false;
//            isClickPayPal = false;
//            isClickReserved = true;
//            SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
//            user_id = logindeatl.getString("id", "null");
//            if (!rate.equals("")) {
//                String no = editno.getText().toString();
//                if (!no.equals("") && !statename.equals("State")) {
//
//                    if (!shoues.equals(duration_unit + "s") && !shoues.equals("")) {
//                        platno = editno.getText().toString();
//                        if (!user_id.equals("null")) {
//                            if (marker.contains("Managed") || marker.contains("managed")) {
//                                parkingtype = "managed";
//                            } else {
//                                parkingtype = "paid";
//                            }
//                            token = 0;
//                            if (rl_sp_row.getVisibility() == View.VISIBLE) {
//
//
//                                if (township_sapce_marked.equals("false")) {
//                                    if (!lost_no.equals("SPACE #")) {
//                                        //parkingtype = "guest";
//                                        lost_row1 = "";
//                                        lost_no1 = lost_no;
//                                        if (!user_id.equals("null")) {
//                                            if (renew.equals("yes")) {
//                                                if (Renew_is_valid.equals("yes")) {
////                                                    new getwalletbal().execute();
//                                                    getTotalAmountReserved();
//                                                } else {
//                                                    new exitsparking(ReNew_id).execute();
//                                                }
//                                            } else {
//                                                new checkParkedCarsReserved().execute();
////                                                new checkcardparkedornotwallet().execute();
//                                            }
//
//                                        } else {
//                                            new checkParkedCarsReserved().execute();
////                                            new checkcardparkedornotwallet().execute();
//                                            //  showpaypalrandomnumber(getActivity());
//                                        }
//                                    } else {
//                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                                        alertDialog.setTitle("Alert!!");
//                                        alertDialog.setMessage("Please fill in all required fields.");
//                                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int which) {
//                                            }
//                                        });
//                                        alertDialog.show();
//
//                                    }
//                                } else if (total_row < 2) {
//
//                                    if (!lost_row.equals("ROW")) {
//                                        //parkingtype = "guest";
//                                        lost_row1 = lost_row;
//                                        lost_no1 = "";
//                                        if (!user_id.equals("null")) {
//                                            if (renew.equals("yes")) {
//                                                if (Renew_is_valid.equals("yes")) {
////                                                    new getwalletbal().execute();
//                                                    getTotalAmountReserved();
//                                                } else {
//                                                    new exitsparking(ReNew_id).execute();
//                                                }
//                                            } else {
//                                                new checkParkedCarsReserved().execute();
////                                                new checkcardparkedornotwallet().execute();
//                                            }
//                                        } else {
//                                            new checkParkedCarsReserved().execute();
////                                            new checkcardparkedornotwallet().execute();
//                                            //  showpaypalrandomnumber(getActivity());
//                                        }
//                                    } else {
//                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                                        alertDialog.setTitle("Alert!!");
//                                        alertDialog.setMessage("Please fill in all required fields.");
//                                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int which) {
//                                            }
//                                        });
//                                        alertDialog.show();
//
//                                    }
//                                } else {
//                                    if (!lost_no.equals("SPACE #") || !lost_row.equals("ROW")) {
//                                        //parkingtype = "guest";
//                                        lost_row1 = lost_row;
//                                        lost_no1 = lost_no;
//                                        if (!user_id.equals("null")) {
//                                            if (renew.equals("yes")) {
//                                                if (Renew_is_valid.equals("yes")) {
////                                                    new getwalletbal().execute();
//                                                    getTotalAmountReserved();
//                                                } else {
//                                                    new exitsparking(ReNew_id).execute();
//                                                }
//                                            } else {
//                                                new checkParkedCarsReserved().execute();
////                                                new checkcardparkedornotwallet().execute();
//                                            }
//                                        } else {
//                                            new checkParkedCarsReserved().execute();
////                                            new checkcardparkedornotwallet().execute();
//                                            //  showpaypalrandomnumber(getActivity());
//                                        }
//                                    } else {
//                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                                        alertDialog.setTitle("Alert!!");
//                                        alertDialog.setMessage("Please fill in all required fields.");
//                                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int which) {
//                                            }
//                                        });
//                                        alertDialog.show();
//
//                                    }
//                                }
//
//
//                            } else {
//                                lost_row1 = "";
//                                lost_no1 = "";
//                                if (!user_id.equals("null")) {
//                                    if (renew.equals("yes")) {
//                                        if (Renew_is_valid.equals("yes")) {
////                                            new getwalletbal().execute();
//                                            getTotalAmountReserved();
//                                        } else {
//                                            new exitsparking(ReNew_id).execute();
//                                        }
//                                    } else {
//                                        new checkParkedCarsReserved().execute();
////                                        new checkcardparkedornotwallet().execute();
//                                    }
//                                } else {
//                                    new checkParkedCarsReserved().execute();
////                                    new checkcardparkedornotwallet().execute();
//                                    //  showpaypalrandomnumber(getActivity());
//                                }
//                            }
//                        }
//
//
//                    } else {
//                        TextView tvTitle = new TextView(getActivity());
//                        tvTitle.setText("Alert!!!");
//                        tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
//                        tvTitle.setPadding(0, 10, 0, 0);
//                        tvTitle.setTextColor(Color.parseColor("#000000"));
//                        tvTitle.setTextSize(20);
//                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                        alertDialog.setCustomTitle(tvTitle);
//                        alertDialog.setMessage("Select parking hours before proceeding.");
//                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                            }
//                        });
//                        final AlertDialog alertd = alertDialog.create();
//                        alertd.show();
//                        TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
//                        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
//                    }
//                } else {
//                    TextView tvTitle = new TextView(getActivity());
//                    tvTitle.setText("Alert!!!");
//                    tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
//                    tvTitle.setPadding(0, 10, 0, 0);
//                    tvTitle.setTextColor(Color.parseColor("#000000"));
//                    tvTitle.setTextSize(20);
//                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                    alertDialog.setCustomTitle(tvTitle);
//                    alertDialog.setMessage("Fill the all fields, in order to Reserve Parking.");
//                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                        }
//                    });
//                    final AlertDialog alertd = alertDialog.create();
//                    alertd.show();
//                    TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
//                    messageText.setGravity(Gravity.CENTER_HORIZONTAL);
//                }
//            }
//        } else {
//            showAlertDialog(getActivity(), "No Internet Connection",
//                    "You don't have internet connection.", false);
//        }
//    }

    private void showParkNowPayLaterConfirmationDialog() {
        // park now, pay later
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure, you want to Pay Later and Park Now?");
        builder.setPositiveButton("PARK NOW", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                new finalupdate().execute("payLater");
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();

    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("ip", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("aax", ex.toString());
        }
        return null;
    }

    public void currentlocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {

            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
            Intent intent = new Intent(getActivity(), GeocodeAddressIntentService.class);
            intent.putExtra(Constants.RECEIVER, mResultReceiver);
            intent.putExtra(Constants.FETCH_TYPE_EXTRA, Constants.USE_ADDRESS_LOCATION);
            intent.putExtra(Constants.LOCATION_LATITUDE_DATA_EXTRA, latitude);
            intent.putExtra(Constants.LOCATION_LONGITUDE_DATA_EXTRA, longitude);
            getActivity().startService(intent);
        } else {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Failed to fetch user location!");
            alertDialog.setMessage("Please enable user location for app, location is required for app to work properly");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            alertDialog.show();
        }
    }

    public void gettotalamount() {

        String no = editno.getText().toString();
        if (!no.equals("") && !statename.equals("State")) {

            if (!shoues.equals(duration_unit + "s")) {
                min = shoues.substring(shoues.indexOf(' ') + 1);
                String hr = shoues.substring(0, shoues.indexOf(' '));
                Double d = new Double(Double.parseDouble(hr));
                hours = d.intValue();
                parkingQty = hours;
                float minutes;
                String h;

                double hrr = Double.parseDouble(rate1);
                parkingUnits = hrr;
                double prices = Double.parseDouble(rate);
                parkingRate = prices;
                double perhrrate = hrr / prices;
                finalpayamount = hours / perhrrate;
                subTotal = hours / perhrrate;
                tr_percentage_for_parked_cars = finalpayamount * tr_percentage1;
                finalpayamount = finalpayamount + tr_percentage_for_parked_cars + tr_fee1;
                finallab = String.format("%.2f", finalpayamount);
                boolean is_parked_time = true;
                Renew_parked_time = Renew_parked_time != null ? (!Renew_parked_time.equals("") ? (!Renew_parked_time.equals("null") ? Renew_parked_time : "") : "") : "";
                int remain_time = 0;
                if (!Renew_parked_time.equals("")) {
                    int parked_hours = Integer.parseInt(Renew_parked_time);
                    final_count = parked_hours + hours;
                    int max = Integer.parseInt(max_time);
                    remain_time = max - parked_hours;
                    if (final_count <= max) {
                        is_parked_time = true;
                    } else {
                        is_parked_time = false;
                    }
                } else {
                    final_count = hours;
                }

                if (!finallab.equals("") || !finallab.equals(null)) {
                    if (is_parked_time) {
                        PaypalPaymentIntegration(finallab);
                    } else {
                        TextView tvTitle = new TextView(getActivity());
                        tvTitle.setText("ParkEZly");
                        tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                        tvTitle.setPadding(0, 10, 0, 0);
                        tvTitle.setTextColor(Color.parseColor("#000000"));
                        tvTitle.setTextSize(20);
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setCustomTitle(tvTitle);
                        alertDialog.setMessage(Html.fromHtml("Duration must be less than<b> maximum park " + max_time + " " + duration_unit + "s</b> and you already parked <b>" + Renew_parked_time + " " + duration_unit + "s</b> so you can park upto <b>" + remain_time + " " + duration_unit + "s</b> more or less!"));
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        final AlertDialog alertd = alertDialog.create();
                        alertd.show();
                        TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
                    }
                } else {
                    TextView tvTitle = new TextView(getActivity());
                    tvTitle.setText("Payment not processable");
                    tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                    tvTitle.setPadding(0, 10, 0, 0);
                    tvTitle.setTextColor(Color.parseColor("#000000"));
                    tvTitle.setTextSize(20);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setCustomTitle(tvTitle);
                    alertDialog.setMessage("Amount value is $0.00");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    final AlertDialog alertd = alertDialog.create();
                    alertd.show();
                    TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                    messageText.setGravity(Gravity.CENTER_HORIZONTAL);
                }
            } else {
                TextView tvTitle = new TextView(getActivity());
                tvTitle.setText("Alert!!!");
                tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                tvTitle.setPadding(0, 10, 0, 0);
                tvTitle.setTextColor(Color.parseColor("#000000"));
                tvTitle.setTextSize(20);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setCustomTitle(tvTitle);
                alertDialog.setMessage("Select parking hours before proceeding.");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                final AlertDialog alertd = alertDialog.create();
                alertd.show();
                TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                messageText.setGravity(Gravity.CENTER_HORIZONTAL);
            }
        } else {
            TextView tvTitle = new TextView(getActivity());
            tvTitle.setText("Alert!!!");
            tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
            tvTitle.setPadding(0, 10, 0, 0);
            tvTitle.setTextColor(Color.parseColor("#000000"));
            tvTitle.setTextSize(20);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setCustomTitle(tvTitle);
            alertDialog.setMessage("Fill the all fields, in order to Pay & Parking.");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            final AlertDialog alertd = alertDialog.create();
            alertd.show();
            TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
            messageText.setGravity(Gravity.CENTER_HORIZONTAL);
        }
    }

    public void getTotalAmountReserved() {

        String no = editno.getText().toString();
        if (!no.equals("") && !statename.equals("State")) {

            if (!shoues.equals(duration_unit + "s")) {
                min = shoues.substring(shoues.indexOf(' ') + 1);
                String hr = shoues.substring(0, shoues.indexOf(' '));
                Double d = new Double(Double.parseDouble(hr));
                hours = d.intValue();
                parkingQty = hours;
                float minutes;
                String h;

                double hrr = Double.parseDouble(rate1);
                parkingUnits = hrr;
                double prices = Double.parseDouble(rate);
                parkingRate = prices;
                double perhrrate = hrr / prices;
                finalpayamount = hours / perhrrate;
                subTotal = hours / perhrrate;
                tr_percentage_for_parked_cars = finalpayamount * tr_percentage1;
                finalpayamount = finalpayamount + tr_percentage_for_parked_cars + tr_fee1;
                finallab = String.format("%.2f", finalpayamount);
                boolean is_parked_time = true;
                Renew_parked_time = Renew_parked_time != null ? (!Renew_parked_time.equals("") ? (!Renew_parked_time.equals("null") ? Renew_parked_time : "") : "") : "";
                int remain_time = 0;
                if (!Renew_parked_time.equals("")) {
                    int parked_hours = Integer.parseInt(Renew_parked_time);
                    final_count = parked_hours + hours;
                    int max = Integer.parseInt(max_time);
                    remain_time = max - parked_hours;
                    if (final_count <= max) {
                        is_parked_time = true;
                    } else {
                        is_parked_time = false;
                    }
                } else {
                    final_count = hours;
                }

                if (!finallab.equals("") || !finallab.equals(null)) {
                    if (is_parked_time) {
                        new reserveNow().execute();
                    } else {
                        TextView tvTitle = new TextView(getActivity());
                        tvTitle.setText("ParkEZly");
                        tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                        tvTitle.setPadding(0, 10, 0, 0);
                        tvTitle.setTextColor(Color.parseColor("#000000"));
                        tvTitle.setTextSize(20);
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setCustomTitle(tvTitle);
                        alertDialog.setMessage(Html.fromHtml("Duration must be less than<b> maximum park " + max_time + " " + duration_unit + "s</b> and you already parked <b>" + Renew_parked_time + " " + duration_unit + "s</b> so you can park upto <b>" + remain_time + " " + duration_unit + "s</b> more or less!"));
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        final AlertDialog alertd = alertDialog.create();
                        alertd.show();
                        TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
                    }
                } else {
                    TextView tvTitle = new TextView(getActivity());
                    tvTitle.setText("Payment not processable");
                    tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                    tvTitle.setPadding(0, 10, 0, 0);
                    tvTitle.setTextColor(Color.parseColor("#000000"));
                    tvTitle.setTextSize(20);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setCustomTitle(tvTitle);
                    alertDialog.setMessage("Amount value is $0.00");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    final AlertDialog alertd = alertDialog.create();
                    alertd.show();
                    TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                    messageText.setGravity(Gravity.CENTER_HORIZONTAL);
                }
            } else {
                TextView tvTitle = new TextView(getActivity());
                tvTitle.setText("Alert!!!");
                tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                tvTitle.setPadding(0, 10, 0, 0);
                tvTitle.setTextColor(Color.parseColor("#000000"));
                tvTitle.setTextSize(20);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setCustomTitle(tvTitle);
                alertDialog.setMessage("Select parking hours before proceeding.");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                final AlertDialog alertd = alertDialog.create();
                alertd.show();
                TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                messageText.setGravity(Gravity.CENTER_HORIZONTAL);
            }
        } else {
            TextView tvTitle = new TextView(getActivity());
            tvTitle.setText("Alert!!!");
            tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
            tvTitle.setPadding(0, 10, 0, 0);
            tvTitle.setTextColor(Color.parseColor("#000000"));
            tvTitle.setTextSize(20);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setCustomTitle(tvTitle);
            alertDialog.setMessage("Fill the all fields, in order to Pay & Parking.");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            final AlertDialog alertd = alertDialog.create();
            alertd.show();
            TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
            messageText.setGravity(Gravity.CENTER_HORIZONTAL);
        }
    }

    public void ActionStartsHere() {
        againStartGPSAndSendFile();
    }

    public void ActionStartsHere_popup() {
        againStartGPSAnd_popup();
    }

    public void againStartGPSAndSendFile() {
        new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                ActionStartsHere();
                showsucessmesg.setVisibility(View.GONE);

            }

        }.start();
    }

    public void againStartGPSAnd_popup() {
        new CountDownTimer(4000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                ShowStatePupup(getActivity(), state_array);
            }

        }.start();
    }

    private void PaypalPaymentIntegration(String amount) {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE, amount);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent, String payamount) {
        return new PayPalPayment(new BigDecimal(payamount), "USD", "Parking Payment", paymentIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.i(TAG, confirm.toJSONObject().toString(4));
                    Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                    JSONObject jsonObj = new JSONObject(confirm.getPayment().toJSONObject().toString(4));
                    Log.e("JSON IS", "LIKE" + jsonObj);
                    Log.e("short_description", "short_description : " + jsonObj.getString("short_description"));
                    Log.e("amount", "amount : " + jsonObj.getString("amount"));
                    Log.e("intent", "intent : " + jsonObj.getString("intent"));
                    Log.e("currency_code", "currency_code : " + jsonObj.getString("currency_code"));
                    JSONObject jsonObjResponse = confirm.toJSONObject().getJSONObject("response");
                    transection_id = jsonObjResponse.getString("id");
                    if (transection_id != null) {
                        isPrePaid = true;
                        showsucessmesg.setVisibility(View.VISIBLE);
                        ActionStartsHere();
                        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                        String id = logindeatl.getString("id", "null");
                        paymentmethod = "Paypal";

                        if (mIsParking) {
                            //add to parked_cars
                            addParkedCar();
                        } else {
                            //add to reserved_parking
                            addReserveParking();
                        }

//                        if (isReserved) {
//                            new reserveNow().execute();
//                        } else {
//                            if (renew.equals("yes")) {
//                                if (Renew_is_valid.equals("yes")) {
//                                    new Update_Parking().execute();
//                                } else {
//                                    new finalupdate().execute();
//                                }
//                            } else {
//                                new finalupdate().execute();
//                            }
//                        }
                    } else {
                        transactionfailalert();
                    }
                    Log.e("transactionId", ":;;;;" + transection_id);

                } catch (JSONException e) {
                    Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                }
            }
        } else {

        }
    }

    public void transactionfailalert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Transaction Fail try again");
        alertDialog.setMessage("Fail");
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alertDialog.show();
    }

    @Override
    public void Onitem_save() {
        getActivity().onBackPressed();
        new getvehiclenumber().execute();
    }

    @Override
    public void Onitem_delete() {
        getActivity().onBackPressed();
        new getvehiclenumber().execute();
    }

    @Override
    public void onWalletClick() {
        //payment option wallet, payNow true
        mIsPayNow = true;
        mIsWalletPayment = true;
        if (!mIsParking) {
            String message = "Your Cancellation Fee is $" + mCancelationCharges + "\n\nWant to Reserve Now?";
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
            builder.setMessage(message);
            builder.setPositiveButton("RESERVE NOW", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    calculateFees();
                }
            });
            builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            androidx.appcompat.app.AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            calculateFees();
        }

//        if (cd.isConnectingToInternet()) {
//            isclickonwallet = true;
//            isClickReserved = false;
//            isClickPayPal = false;
//
//            SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
//            user_id = logindeatl.getString("id", "null");
//            if (!rate.equals("")) {
//                String no = editno.getText().toString();
//                if (!no.equals("") && !statename.equals("State")) {
//
//                    if (!shoues.equals(duration_unit + "s") && !shoues.equals("")) {
//                        platno = editno.getText().toString();
//                        if (!user_id.equals("null")) {
//                            if (marker.contains("Managed") || marker.contains("managed")) {
//                                parkingtype = "managed";
//                            } else {
//                                parkingtype = "paid";
//                            }
//                            token = 0;
//                            if (rl_sp_row.getVisibility() == View.VISIBLE) {
//
//
//                                if (township_sapce_marked.equals("false")) {
//                                    if (!lost_no.equals("SPACE #")) {
//                                        //parkingtype = "guest";
//                                        lost_row1 = "";
//                                        lost_no1 = lost_no;
//                                        if (!user_id.equals("null")) {
//                                            if (renew.equals("yes")) {
//                                                if (Renew_is_valid.equals("yes")) {
//                                                    new getwalletbal().execute();
//                                                } else {
//                                                    new exitsparking(ReNew_id).execute();
//                                                }
//                                            } else {
//                                                if (isReserved) {
//                                                    new checkParkedCarsReserved().execute();
//                                                } else
//                                                    new checkcardparkedornotwallet().execute();
//                                            }
//
//                                        } else {
//                                            if (isReserved)
//                                                new checkParkedCarsReserved().execute();
//                                            else
//                                                new checkcardparkedornotwallet().execute();
//                                            //  showpaypalrandomnumber(getActivity());
//                                        }
//                                    } else {
//                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                                        alertDialog.setTitle("Alert!!");
//                                        alertDialog.setMessage("Please fill in all required fields.");
//                                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int which) {
//                                            }
//                                        });
//                                        alertDialog.show();
//
//                                    }
//                                } else if (total_row < 2) {
//
//                                    if (!lost_row.equals("ROW")) {
//                                        //parkingtype = "guest";
//                                        lost_row1 = lost_row;
//                                        lost_no1 = "";
//                                        if (!user_id.equals("null")) {
//                                            if (renew.equals("yes")) {
//                                                if (Renew_is_valid.equals("yes")) {
//                                                    new getwalletbal().execute();
//                                                } else {
//                                                    new exitsparking(ReNew_id).execute();
//                                                }
//                                            } else {
//                                                if (isReserved)
//                                                    new checkParkedCarsReserved().execute();
//                                                else
//                                                    new checkcardparkedornotwallet().execute();
//                                            }
//                                        } else {
//                                            if (isReserved)
//                                                new checkParkedCarsReserved().execute();
//                                            else new checkcardparkedornotwallet().execute();
//                                            //  showpaypalrandomnumber(getActivity());
//                                        }
//                                    } else {
//                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                                        alertDialog.setTitle("Alert!!");
//                                        alertDialog.setMessage("Please fill in all required fields.");
//                                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int which) {
//                                            }
//                                        });
//                                        alertDialog.show();
//
//                                    }
//                                } else {
//                                    if (!lost_no.equals("SPACE #") || !lost_row.equals("ROW")) {
//                                        //parkingtype = "guest";
//                                        lost_row1 = lost_row;
//                                        lost_no1 = lost_no;
//                                        if (!user_id.equals("null")) {
//                                            if (renew.equals("yes")) {
//                                                if (Renew_is_valid.equals("yes")) {
//                                                    new getwalletbal().execute();
//                                                } else {
//                                                    new exitsparking(ReNew_id).execute();
//                                                }
//                                            } else {
//                                                if (isReserved)
//                                                    new checkParkedCarsReserved().execute();
//                                                else
//                                                    new checkcardparkedornotwallet().execute();
//                                            }
//                                        } else {
//                                            if (isReserved)
//                                                new checkParkedCarsReserved().execute();
//                                            else new checkcardparkedornotwallet().execute();
//                                            //  showpaypalrandomnumber(getActivity());
//                                        }
//                                    } else {
//                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                                        alertDialog.setTitle("Alert!!");
//                                        alertDialog.setMessage("Please fill in all required fields.");
//                                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int which) {
//                                            }
//                                        });
//                                        alertDialog.show();
//
//                                    }
//                                }
//
//
//                            } else {
//                                lost_row1 = "";
//                                lost_no1 = "";
//                                if (!user_id.equals("null")) {
//                                    if (renew.equals("yes")) {
//                                        if (Renew_is_valid.equals("yes")) {
//                                            new getwalletbal().execute();
//                                        } else {
//                                            new exitsparking(ReNew_id).execute();
//                                        }
//                                    } else {
//                                        if (isReserved)
//                                            new checkParkedCarsReserved().execute();
//                                        else new checkcardparkedornotwallet().execute();
//                                    }
//                                } else {
//                                    if (isReserved)
//                                        new checkParkedCarsReserved().execute();
//                                    else new checkcardparkedornotwallet().execute();
//                                    //  showpaypalrandomnumber(getActivity());
//                                }
//                            }
//                        }
//
//
//                    } else {
//                        TextView tvTitle = new TextView(getActivity());
//                        tvTitle.setText("Alert!!!");
//                        tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
//                        tvTitle.setPadding(0, 10, 0, 0);
//                        tvTitle.setTextColor(Color.parseColor("#000000"));
//                        tvTitle.setTextSize(20);
//                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                        alertDialog.setCustomTitle(tvTitle);
//                        alertDialog.setMessage("Select parking hours before proceeding.");
//                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                            }
//                        });
//                        final AlertDialog alertd = alertDialog.create();
//                        alertd.show();
//                        TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
//                        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
//                    }
//                } else {
//                    TextView tvTitle = new TextView(getActivity());
//                    tvTitle.setText("Alert!!!");
//                    tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
//                    tvTitle.setPadding(0, 10, 0, 0);
//                    tvTitle.setTextColor(Color.parseColor("#000000"));
//                    tvTitle.setTextSize(20);
//                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                    alertDialog.setCustomTitle(tvTitle);
//                    alertDialog.setMessage("Fill the all fields, in order to Pay & Parking.");
//                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                        }
//                    });
//                    final AlertDialog alertd = alertDialog.create();
//                    alertd.show();
//                    TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
//                    messageText.setGravity(Gravity.CENTER_HORIZONTAL);
//                }
//            }
//        } else {
//            showAlertDialog(getActivity(), "No Internet Connection",
//                    "You don't have internet connection.", false);
//        }
    }

    private void getWalletBalance() {
        //
        new getwalletbal().execute();
    }

    private void calculateFees() {

        min = shoues.substring(shoues.indexOf(' ') + 1);
        String hr = shoues.substring(0, shoues.indexOf(' '));
        Double d = new Double(Double.parseDouble(hr));
        hours = d.intValue();
        parkingQty = hours;
        float minutes;
        String h;

        double hrr = Double.parseDouble(rate1);
        parkingUnits = hrr;
        double prices = Double.parseDouble(rate);
        parkingRate = prices;
        double perhrrate = hrr / prices;
        finalpayamount = hours / perhrrate;
        subTotal = hours / perhrrate;
        tr_percentage_for_parked_cars = finalpayamount * tr_percentage1;
        finalpayamount = finalpayamount + tr_percentage_for_parked_cars + tr_fee1;
        finallab = String.format("%.2f", finalpayamount);

        mParkingSubTotal = subTotal;
        mTrFee = tr_fee1;
        mTrPercent = tr_percentage_for_parked_cars;
        mParkingTotal = Double.parseDouble(finallab);

        Log.e("#DEBUG", "  calculateFees:  mTrFee:  " + mTrFee + "  mTrPercent: "
                + mTrPercent + "   mParkingSubTotal:  " + mParkingSubTotal + "  mParkingTotal: " + mParkingTotal);


        if (mIsParking) {
            // follow parking steps
            if (mIsPayNow) {
                //calculate fees
                checkIsCarAlreadyParked();
            } else {
                //calculate fees with pay later charges
                mParkingSubTotal = subTotal + mParkNowPostPayFees;
                mParkingTotal = mParkingTotal + mParkNowPostPayFees;
                checkIsCarAlreadyParked();
            }
        } else {
            //follow reservation steps
            if (mIsPayNow) {
                //calculate fees for reservation
                if (mIsWalletPayment) {
                    //pay with wallet balance
                    getWalletBalance();
                } else {
                    //pay with paypal
                    startPayPalPaymentRequest();
                }

            } else {
                // calculate fees for reservation with pay later charges
                mParkingSubTotal = subTotal + mReserveNowPostPayFees;
                mParkingTotal = mParkingTotal + mReserveNowPostPayFees;
                addReserveParking();

            }
        }
    }

    private void showDialogDebitFromWallet() {
        // if yes
        new updatewallet().execute();
    }

    private void updateWalletBalance() {
        if (mIsParking) {
            //add to parked_cars
            new finalupdate().execute();
        } else {
            //add to reserve_parking
            new reserveNow().execute();
        }
    }

    private void startPayPalPaymentRequest() {

        PaypalPaymentIntegration(String.valueOf(mParkingTotal));
    }

    @Override
    public void onPaymentClick() {
        // payment option= paypal, wallet = false, paynow= true
        mIsPayNow = true;
        mIsWalletPayment = false;
        if (!mIsParking) {
            String message = "Your Cancellation Fee is $" + mCancelationCharges + "\n\nWant to Reserve Now?";
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
            builder.setMessage(message);
            builder.setPositiveButton("RESERVE NOW", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    calculateFees();
                }
            });
            builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            androidx.appcompat.app.AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            calculateFees();
        }
//        if (cd.isConnectingToInternet()) {
//            isclickonwallet = false;
//            isClickPayPal = true;
//            isClickReserved = false;
//            SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
//            user_id = logindeatl.getString("id", "null");
//            platno = editno.getText().toString();
//            SharedPreferences payment = getActivity().getSharedPreferences("paypalpaymentback", Context.MODE_PRIVATE);
//            SharedPreferences.Editor edi = payment.edit();
//            edi.putString("paypalback", "true");
//            edi.commit();
//            platno = editno.getText().toString();
//            if (!platno.equals("") & !statename.equals("State") & !shoues.equals("hours") & !shoues.equals("")) {
//                if (rl_sp_row.getVisibility() == View.VISIBLE) {
//                    if (!user_id.equals("null")) {
//                        if (marker.equals("Managed Paid") || marker.equals("managed paid") || marker.equals("managed free") || marker.equals("Managed Free")) {
//                            parkingtype = "managed";
//                        } else {
//                            parkingtype = "paid";
//                            marker = "paid";
//                        }
//
//                    } else {
//                        if (marker.equals("Managed Paid") || marker.equals("managed paid") || marker.equals("managed free") || marker.equals("Managed Free")) {
//                            parkingtype = "managed_guest";
//                        } else {
//                            parkingtype = "paid";
//                            marker = "paid";
//                        }
//                        //  showpaypalrandomnumber(getActivity());
//                    }
//                    if (township_sapce_marked.equals("false")) {
//                        if (!lost_no.equals("SPACE #")) {
//                            //parkingtype = "guest";
//                            lost_row1 = "";
//                            lost_no1 = lost_no;
//                            if (renew.equals("yes")) {
//                                if (Renew_is_valid.equals("yes")) {
//                                    gettotalamount();
//                                } else {
//                                    new exitsparking(ReNew_id).execute();
//                                }
//                            } else {
//                                if (isReserved)
//                                    new checkParkedCarsReserved().execute();
//                                else new checkcardparkedornotpaidt().execute();
//                            }
//                        } else {
//                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                            alertDialog.setTitle("Alert!!");
//                            alertDialog.setMessage("Please fill in all required fields.");
//                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
//                                }
//                            });
//                            alertDialog.show();
//
//                        }
//                    } else if (total_row < 2) {
//
//                        if (!lost_row.equals("ROW")) {
//                            //parkingtype = "guest";
//                            lost_row1 = lost_row;
//                            lost_no1 = "";
//                            if (renew.equals("yes")) {
//                                if (Renew_is_valid.equals("yes")) {
//                                    gettotalamount();
//                                } else {
//                                    new exitsparking(ReNew_id).execute();
//                                }
//                            } else {
//                                if (isReserved)
//                                    new checkParkedCarsReserved().execute();
//                                else new checkcardparkedornotpaidt().execute();
//                            }
//                        } else {
//                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                            alertDialog.setTitle("Alert!!");
//                            alertDialog.setMessage("Please fill in all required fields.");
//                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
//                                }
//                            });
//                            alertDialog.show();
//
//                        }
//                    } else {
//                        if (!lost_no.equals("SPACE #") || !lost_row.equals("ROW")) {
//                            //parkingtype = "guest";
//                            lost_row1 = lost_row;
//                            lost_no1 = lost_no;
//                            if (renew.equals("yes")) {
//                                if (Renew_is_valid.equals("yes")) {
//                                    gettotalamount();
//                                } else {
//                                    new exitsparking(ReNew_id).execute();
//                                }
//                            } else {
//                                if (isReserved)
//                                    new checkParkedCarsReserved().execute();
//                                else new checkcardparkedornotpaidt().execute();
//                            }
//                        } else {
//                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                            alertDialog.setTitle("Alert!!");
//                            alertDialog.setMessage("Please fill in all required fields.");
//                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
//                                }
//                            });
//                            alertDialog.show();
//                        }
//                    }
//
//                } else {
//                    lost_row1 = lost_row;
//                    lost_no1 = lost_no;
//                    if (renew.equals("yes")) {
//                        if (Renew_is_valid.equals("yes")) {
//                            gettotalamount();
//                        } else {
//                            new exitsparking(ReNew_id).execute();
//                        }
//                    } else {
//                        if (isReserved)
//                            new checkParkedCarsReserved().execute();
//                        else new checkcardparkedornotpaidt().execute();
//                    }
//                }
//            } else {
//                TextView tvTitle = new TextView(getActivity());
//                tvTitle.setText("Alert!!!");
//                tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
//                tvTitle.setPadding(0, 10, 0, 0);
//                tvTitle.setTextColor(Color.parseColor("#000000"));
//                tvTitle.setTextSize(20);
//                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                alertDialog.setCustomTitle(tvTitle);
//                alertDialog.setMessage("Fill the all fields, in order to Pay & Parking.");
//                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                    }
//                });
//                final AlertDialog alertd = alertDialog.create();
//                alertd.show();
//                TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
//                messageText.setGravity(Gravity.CENTER_HORIZONTAL);
//            }
//        } else {
//            showAlertDialog(getActivity(), "No Internet Connection",
//                    "You don't have internet connection.", false);
//        }
    }

    @Override
    public void onPayLaterClick() {
        Log.e(TAG, "    onPayLaterClick");
        String message = "";
        String txtOK = "";
        if (mIsParking) {
            message = "Are you sure, you want to Pay Later and Park Now?";
            txtOK = "PARK NOW";
        } else {
            message = "Your Cancellation Fee is $" + mCancelationCharges + "\n\nWant to Reserve Now and Pay Later?";
            txtOK = "RESERVE NOW";
        }
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setMessage(message);
        builder.setPositiveButton(txtOK, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                mIsPayNow = false;
                calculateFees();
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        androidx.appcompat.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void checkIsCarAlreadyParked() {
        new checkParkedCarsReserved().execute();
    }

    private void addParkedCar() {
        new finalupdate().execute();
    }

    private void addReserveParking() {
        new reserveNow().execute();
    }

    public class fetchAddressFromLatLng extends AsyncTask<Void, Void, StringBuilder> {

        String lat1, lang1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        public fetchAddressFromLatLng(String lat, String lang) {
            super();
            this.lat1 = lat;
            this.lang1 = lang;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {

                //  new getsearchotherlocation().execute();
                //
                // ();

                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat1 + "," + lang1 + "&key=AIzaSyBRdCWdlUMy3X5dc-9jMe0jRXJENibjdN8";

                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
//                rlProgressbar.setVisibility(View.GONE);

                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");

                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray
                            .getJSONObject(0);
//                    mAddress1 = before_geometry_jsonObj.getString("formatted_address");
                    JSONObject geometry_jsonObj = before_geometry_jsonObj
                            .getJSONObject("geometry");

                    JSONObject location_jsonObj = geometry_jsonObj
                            .getJSONObject("location");

                    lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);

                    lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);

                    if (before_geometry_jsonObj.has("address_components")) {
                        JSONArray jsonArrayComponents = before_geometry_jsonObj.getJSONArray("address_components");
                        Log.e("#DEBUG", "   address_components:  Size: " + jsonArrayComponents.length());
                        try {
                            JSONObject objectAddress2 = jsonArrayComponents.getJSONObject(0);
                            mMarkerAddress2 = objectAddress2.getString("long_name");

                            JSONObject objectCity = jsonArrayComponents.getJSONObject(3);
                            mMarkerCity = objectCity.getString("long_name");

                            JSONObject objectDistrict = jsonArrayComponents.getJSONObject(4);
                            mMarkerDistrict = objectDistrict.getString("long_name");
//                            mMarkerCity = mMarkerDistrict;

                            JSONObject objectState = jsonArrayComponents.getJSONObject(5);
                            mMarkerStateCode = objectState.getString("short_name");

                            JSONObject objectCountry = jsonArrayComponents.getJSONObject(6);
                            mMarkerCountryCode = objectCountry.getString("short_name");

                            JSONObject objectZip = jsonArrayComponents.getJSONObject(7);
                            mMarkerZipCode = objectZip.getString("long_name");

                            Log.e("#DEBUG", "\nmMarkerAddress2:  " + mMarkerAddress2
                                    + "\nmMarkerCity:  " + mMarkerCity
                                    + "\nmMarkerDistrict" + mMarkerDistrict
                                    + "\nmMarkerStateCode: " + mMarkerStateCode
                                    + "\nmMarkerCountryCode:  " + mMarkerCountryCode
                                    + "\nmMarkerZipCode: " + mMarkerZipCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("#DEBUG", "  fetchLatLng  Error:  " + e.getMessage());
                        }
                    }


                } else {
                    android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public class fetchLatLongFromaddress extends AsyncTask<Void, Void, StringBuilder> {

        String lat1, lang1;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        public fetchLatLongFromaddress(String lat, String lang) {
            super();
            this.lat1 = lat;
            this.lang1 = lang;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {

                //  new getsearchotherlocation().execute();
                //
                // ();

                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat1 + "," + lang1 + "&key=AIzaSyBRdCWdlUMy3X5dc-9jMe0jRXJENibjdN8";

                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
                rl_progressbar.setVisibility(View.GONE);

                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");

                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray
                            .getJSONObject(0);
                    adrreslocation = before_geometry_jsonObj.getString("formatted_address");
                    JSONObject geometry_jsonObj = before_geometry_jsonObj
                            .getJSONObject("geometry");

                    JSONObject location_jsonObj = geometry_jsonObj
                            .getJSONObject("location");

                    lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);

                    lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);

                    if (before_geometry_jsonObj.has("address_components")) {
                        JSONArray jsonArrayComponents = before_geometry_jsonObj.getJSONArray("address_components");
                        Log.e("#DEBUG", "   address_components:  Size: " + jsonArrayComponents.length());
                        try {
                            JSONObject objectAddress2 = jsonArrayComponents.getJSONObject(0);
                            mAddress2 = objectAddress2.getString("long_name");

                            JSONObject objectCity = jsonArrayComponents.getJSONObject(2);
                            mCity = objectCity.getString("long_name");

                            JSONObject objectDistrict = jsonArrayComponents.getJSONObject(4);
                            mDistrict = objectDistrict.getString("long_name");
                            mCity = mDistrict;

                            JSONObject objectState = jsonArrayComponents.getJSONObject(5);
                            mStateCode = objectState.getString("short_name");

                            JSONObject objectCountry = jsonArrayComponents.getJSONObject(6);
                            mCountryCode = objectCountry.getString("short_name");

                            JSONObject objectZip = jsonArrayComponents.getJSONObject(7);
                            mZipCode = objectZip.getString("long_name");

                            Log.e("#DEBUG", "\nmAddress2:  " + mAddress2
                                    + "\nmCity:  " + mCity
                                    + "\nmDistrict" + mDistrict
                                    + "\nmStateCode: " + mStateCode
                                    + "\nmCountryCode:  " + mCountryCode
                                    + "\nmZipCode: " + mZipCode);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("#DEBUG", "  fetchLatLng  Error:  " + e.getMessage());
                        }
                    }


                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }

                new fetchAddressFromLatLng(lat, lang).execute();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void showpaypalrandomnumber(Activity context) {

        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.randomnumber, viewGroup, false);
        popup = new PopupWindow(context);
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        popup.setAnimationStyle(R.style.animationName);
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

        final EditText edit_code;
        Button btn_cancel, btn_park_now;

        edit_code = (EditText) layout.findViewById(R.id.edit_randomno);
        btn_cancel = (Button) layout.findViewById(R.id.btn_cancel);
        btn_park_now = (Button) layout.findViewById(R.id.btn_popup_park_now);
        int randomPIN = (int) (Math.random() * 9000) + 1000;
        String val = "" + randomPIN;
        edit_code.append(val);
        btn_park_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String editno = edit_code.getText().toString();
                if (!edit_code.equals("")) {
                    popup.dismiss();
                    token = Integer.parseInt(editno);
                    token1 = editno;
                    gettotalamount();
                } else {
                    popup.dismiss();
                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });


    }

    public float getdistances(String address, String address1) {
        GPSTracker tracker = new GPSTracker(getActivity());

        try {
            Location loc1 = new Location(address1);
            loc1.setLatitude(latitude);
            loc1.setLongitude(longitude);
            Location loc2 = new Location(address);
            loc2.setLatitude(Double.parseDouble(lat));
            loc2.setLongitude(Double.parseDouble(lang));
            float distanceInMeters = loc1.distanceTo(loc2);
            return distanceInMeters;
        } catch (Exception e) {
            return 0;
        }

    }

    public class getwalletbal extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String userid = logindeatl.getString("id", "null");
        JSONObject json, json1;
        String wallbalurl = "_table/user_wallet?order=date_time%20DESC";
        //String wallbalurl = "_table/user_wallet?filter=(user_id%3D'"+userid+"')";
        String w_id;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + wallbalurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            HttpEntity resEntity = response.getEntity();
            String responseStr = null;
            if (response != null) {
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        String user = c.getString("user_id");

                        if (user.equals(userid)) {
                            nbal = c.getString("new_balance");
                            if (!nbal.equals("null")) {
                                cbal = c.getString("current_balance");
                                item.setAmount(nbal);
                                item.setPaidamount(cbal);
                                wallbalarray.add(item);
                                break;
                            }
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            // progressBar.setVisibility(View.GONE);

            if (getActivity() != null && json != null) {
                if (wallbalarray.size() > 0) {
                    double bal = Double.parseDouble(cbal);
                    if (bal > 0) {
                        finalpayamount = 0.0;
                        min = shoues.substring(shoues.indexOf(' ') + 1);
                        String hr = shoues.substring(0, shoues.indexOf(' '));
                        Double d = new Double(Double.parseDouble(hr));
                        hours = d.intValue();
                        parkingQty = hours;
                        float minutes;
                        String h;

                        double hrr = Double.parseDouble(rate1);
                        parkingUnits = hrr;
                        double prices = Double.parseDouble(rate);
                        parkingRate = prices;
                        double perhrrate = hrr / prices;
                        finalpayamount = hours / perhrrate;
                        subTotal = hours / perhrrate;
                        tr_percentage_for_parked_cars = finalpayamount * tr_percentage1;
                        finalpayamount = finalpayamount + tr_percentage_for_parked_cars + tr_fee1;
                        newbal = bal - mParkingTotal;
                        String finallab2 = String.format("%.2f", bal);
                        finallab = String.format("%.2f", finalpayamount);
                        boolean is_parked_time = true;
                        int remain_time = 0;
                        Renew_parked_time = Renew_parked_time != null ? (!Renew_parked_time.equals("") ? (!Renew_parked_time.equals("null") ? Renew_parked_time : "") : "") : "";
                        if (!Renew_parked_time.equals("")) {
                            int parked_hours = Integer.parseInt(Renew_parked_time);
                            final_count = parked_hours + hours;
                            int max = Integer.parseInt(max_time);
                            remain_time = max - parked_hours;
                            if (final_count <= max) {
                                is_parked_time = true;
                            } else {
                                is_parked_time = false;
                            }
                        } else {
                            final_count = hours;
                        }
                        Calendar calendar = Calendar.getInstance();
                        int day = calendar.get(Calendar.DAY_OF_WEEK);

                        if (newbal < 0) {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("alert");
                            alertDialog.setMessage("You don't have enough funds in your wallet.");
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialog.show();

                        } else {
                            if (is_parked_time) {

                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                alertDialog.setTitle("Wallet Balance: $" + finallab2);
                                alertDialog.setMessage("Would you like to pay from your ParkEZly wallet ? With service and transaction fee your total will be " + "$" + finallab + "");
                                alertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        ip = getLocalIpAddress();
                                        showDialogDebitFromWallet();
                                    }
                                });
                                alertDialog.setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                                alertDialog.show();
                            } else {
                                TextView tvTitle = new TextView(getActivity());
                                tvTitle.setText("ParkEZly");
                                tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                                tvTitle.setPadding(0, 10, 0, 0);
                                tvTitle.setTextColor(Color.parseColor("#000000"));
                                tvTitle.setTextSize(20);
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                alertDialog.setCustomTitle(tvTitle);
                                alertDialog.setMessage(Html.fromHtml("Duration must be less than<b> maximum park " + max_time + " " + duration_unit + "s</b> and you already parked <b>" + Renew_parked_time + " " + duration_unit + "s</b> so you can park upto <b>" + remain_time + " " + duration_unit + "s</b> more or less!"));
                                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                final AlertDialog alertd = alertDialog.create();
                                alertd.show();
                                TextView messageText = (TextView) alertd.findViewById(android.R.id.message);
                                messageText.setGravity(Gravity.CENTER_HORIZONTAL);
                            }
                        }
                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("alert");
                        alertDialog.setMessage("You don't have enough funds in your wallet.");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }

                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("alert");
                    alertDialog.setMessage("You don't have enough funds in your wallet.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
            super.onPostExecute(s);

        }
    }

    public class other_parking_rule extends AsyncTask<String, String, String> {

        JSONObject json, json1;
        boolean user_selects_parking_location;
        String manageslocation = "_table/other_parking_rules?filter=location_code%3D'" + location_code + "'";

        @Override
        protected void onPreExecute() {

            if (!TextUtils.isEmpty(parkingType) && parkingType.equals("Township")) {
                manageslocation = "_table/township_parking_rules?filter=location_code%3D'" + location_code + "'";
            }

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            other_parking_rule.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + manageslocation;
            Log.e("#DEBUG", "   other_parking_rule:  URL:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   other_parking_rule:  response:  " + responseStr);
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        item.setPricing_duration(c.getString("pricing_duration"));
                        item.setMax_duration(c.getString("max_duration"));
                        item.setDuration_unit(c.getString("duration_unit"));
                        item.setPricing(c.getString("pricing"));
                        if (c.has("twp_id")
                                && !TextUtils.isEmpty(c.getString("twp_id"))
                                && !c.getString("twp_id").equals("null")) {
                            item.setTwp_id(c.getString("twp_id"));
                            item.setCompany_id(c.getString("twp_id"));
                        }

                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }

                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }

                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }

                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }

                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }

                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }

                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }

                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }

                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }

                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }

                        if (c.has("Reservation_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_Fee"))
                                && !c.getString("Reservation_PostPayment_Fee").equals("null")) {
                            item.setReservation_PostPayment_Fee(c.getString("Reservation_PostPayment_Fee"));
                        }

                        if (c.has("ParkNow_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_Fee"))
                                && !c.getString("ParkNow_PostPayment_Fee").equals("null")) {
                            item.setParkNow_PostPayment_Fee(c.getString("ParkNow_PostPayment_Fee"));
                        }

                        if (c.has("number_of_reservable_spots")
                                && !TextUtils.isEmpty(c.getString("number_of_reservable_spots"))
                                && !c.getString("number_of_reservable_spots").equals("null")) {
                            item.setNumber_of_reservable_spots(c.getString("number_of_reservable_spots"));
                        }
                        other_parking_rule.add(item);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   other_parking_rule:  Error:  " + e.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   other_parking_rule:  Error:  " + e.getMessage());
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.e("#DEBUG", "  other_parking_rules Size: " + String.valueOf(other_parking_rule.size()));
            if (getActivity() != null && json != null) {
                if (marker.equals("other parking") || marker.equals("partner")) {
                    values.clear();
                    values.add("hours");
                    for (int k = 0; k < other_parking_rule.size(); k++) {
                        isReservationAllowed = other_parking_rule.get(k).isReservationAllowed();
                        isPrePaymentRequiredForReservation = other_parking_rule.get(k).isPrePymntReqd_for_Reservation();
                        reservationPostPayTerms = other_parking_rule.get(k).getReservation_PostPayment_term();
                        isParkNowPostPayAllowed = other_parking_rule.get(k).isParkNow_PostPaymentAllowed();
                        parkNowPostPayTerms = other_parking_rule.get(k).getParkNow_PostPayment_term();
                        companyId = other_parking_rule.get(k).getCompany_id();
                        twpId = other_parking_rule.get(k).getTwp_id();

                        mIsCanCancelReservation = other_parking_rule.get(k).isCanCancel_Reservation();
                        if (!TextUtils.isEmpty(other_parking_rule.get(k).getCancellation_Charge())) {
                            mCancelationCharges = Double.parseDouble(other_parking_rule.get(k).getCancellation_Charge());
                        }

                        if (!TextUtils.isEmpty(other_parking_rule.get(k).getParkNow_PostPayment_Fee()))
                            mParkNowPostPayFees = Double.parseDouble(other_parking_rule.get(k).getParkNow_PostPayment_Fee());
                        if (!TextUtils.isEmpty(other_parking_rule.get(k).getParkNow_PostPayment_LateFee()))
                            mParkNowPostPayLateFees = Double.parseDouble(other_parking_rule.get(k).getParkNow_PostPayment_LateFee());

                        if (!TextUtils.isEmpty(other_parking_rule.get(k).getReservation_PostPayment_Fee()))
                            mReserveNowPostPayFees = Double.parseDouble(other_parking_rule.get(k).getReservation_PostPayment_Fee());
                        if (!TextUtils.isEmpty(other_parking_rule.get(k).getReservation_PostPayment_LateFee()))
                            mReserveNowPostPayLateFees = Double.parseDouble(other_parking_rule.get(k).getReservation_PostPayment_LateFee());

                        Log.e("#DEBUG", "   mCancelationCharges:  " + mCancelationCharges
                                + "\nmParkNowPostPayFees:  " + mParkNowPostPayFees
                                + "\nmParkNowPostPayLateFees:  " + mParkNowPostPayLateFees
                                + "\nmReserveNowPostPayFees:  " + mReserveNowPostPayFees
                                + "\nmReserveNowPostPayLateFees:  " + mReserveNowPostPayLateFees
                                + "\nmIsCanCancelReservation:  " + mIsCanCancelReservation);

                        String ff = other_parking_rule.get(k).getMax_duration();
                        String dd = other_parking_rule.get(k).getPricing_duration();
                        double limt = Double.parseDouble(ff);
                        double pricing_dur = Double.parseDouble(dd);
                        int ss = (int) (limt / pricing_dur);
                        for (int y = 1; y <= ss; y++) {
                            if (other_parking_rule.get(k).getDuration_unit().contains("Minute")) {
                                int min = (int) (y * pricing_dur);
                                values.add(String.valueOf(min) + " Mins");
                            } else if (other_parking_rule.get(k).getDuration_unit().contains("Month")) {
                                int min = (int) (y * pricing_dur);
                                values.add(String.valueOf(min) + " Months");
                            } else if (other_parking_rule.get(k).getDuration_unit().contains("Hour")) {
                                int min = (int) (y * pricing_dur);
                                values.add(String.valueOf(min) + " Hrs");
                            } else if (other_parking_rule.get(k).getDuration_unit().contains("Day")) {
                                int min = (int) (y * pricing_dur);
                                values.add(String.valueOf(min) + " Days");
                            } else if (other_parking_rule.get(k).getDuration_unit().contains("Week")) {
                                int min = (int) (y * pricing_dur);
                                values.add(String.valueOf(min) + " Weeks");
                            }
                        }
                    }
                    Activity activity = getActivity();
                    if (activity != null) {
                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, values);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        sphoures.setAdapter(spinnerArrayAdapter);
                    }
                } else {
                    for (int j = 0; j < other_parking_rule.size(); j++) {
                        isReservationAllowed = other_parking_rule.get(j).isReservationAllowed();
                        isPrePaymentRequiredForReservation = other_parking_rule.get(j).isPrePymntReqd_for_Reservation();
                        reservationPostPayTerms = other_parking_rule.get(j).getReservation_PostPayment_term();
                        isParkNowPostPayAllowed = other_parking_rule.get(j).isParkNow_PostPaymentAllowed();
                        parkNowPostPayTerms = other_parking_rule.get(j).getParkNow_PostPayment_term();
                        companyId = other_parking_rule.get(j).getCompany_id();
                        twpId = other_parking_rule.get(j).getTwp_id();
                        mIsCanCancelReservation = other_parking_rule.get(j).isCanCancel_Reservation();
                        if (!TextUtils.isEmpty(other_parking_rule.get(j).getCancellation_Charge())) {
                            mCancelationCharges = Double.parseDouble(other_parking_rule.get(j).getCancellation_Charge());
                        }

                        if (!TextUtils.isEmpty(other_parking_rule.get(j).getParkNow_PostPayment_Fee()))
                            mParkNowPostPayFees = Double.parseDouble(other_parking_rule.get(j).getParkNow_PostPayment_Fee());
                        if (!TextUtils.isEmpty(other_parking_rule.get(j).getParkNow_PostPayment_LateFee()))
                            mParkNowPostPayLateFees = Double.parseDouble(other_parking_rule.get(j).getParkNow_PostPayment_LateFee());

                        if (!TextUtils.isEmpty(other_parking_rule.get(j).getReservation_PostPayment_Fee()))
                            mReserveNowPostPayFees = Double.parseDouble(other_parking_rule.get(j).getReservation_PostPayment_Fee());
                        if (!TextUtils.isEmpty(other_parking_rule.get(j).getReservation_PostPayment_LateFee()))
                            mReserveNowPostPayLateFees = Double.parseDouble(other_parking_rule.get(j).getReservation_PostPayment_LateFee());

                        Log.e("#DEBUG", "   mCancelationCharges:  " + mCancelationCharges
                                + "\nmParkNowPostPayFees:  " + mParkNowPostPayFees
                                + "\nmParkNowPostPayLateFees:  " + mParkNowPostPayLateFees
                                + "\nmReserveNowPostPayFees:  " + mReserveNowPostPayFees
                                + "\nmReserveNowPostPayLateFees:  " + mReserveNowPostPayLateFees
                                + "\nmIsCanCancelReservation:  " + mIsCanCancelReservation);
                    }
                }

                if (isReservationAllowed) {
                    tvBtnReserveNow.setVisibility(View.GONE);
                    if (isPrePaymentRequiredForReservation) {
//                        tvBtnReserveNowPayLater.setVisibility(View.GONE);
                    } else {
//                        tvBtnReserveNowPayLater.setVisibility(View.VISIBLE);
                    }
                } else {
                    tvBtnReserveNow.setVisibility(View.GONE);
                }

                if (isParkNowPostPayAllowed) {
//                    tvBtnParkNowPayLater.setVisibility(View.VISIBLE);
                } else {
//                    tvBtnParkNowPayLater.setVisibility(View.GONE);

                }

                new fetchReservedParking().execute();
            }
            super.onPostExecute(s);
        }
    }

    public class servicecharge extends AsyncTask<String, String, String> {

        JSONObject json, json1;
        String servicechargeurl = "_table/service_charges?filter=manager_id%3D'" + township_code1 + "'";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + servicechargeurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        String tr_percentage = c.getString("tr_percentage");
                        String tr_fee = c.getString("tr_fee");
                        if (!tr_fee.equals("null")) {
                            tr_fee1 = Double.parseDouble(tr_fee);
                        } else {
                            tr_fee1 = 0.0;
                        }
                        if (!tr_percentage.equals("null")) {
                            tr_percentage1 = Double.parseDouble(tr_percentage);
                        } else {
                            tr_percentage1 = 0.0;
                        }
                        Log.e("#DEBUG", "    tr_percentage1:  " + String.valueOf(tr_percentage1));
                        Log.e("#DEBUG", "    tr_free: " + String.valueOf(tr_fee1));
                        break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null && json != null) {
            }
            super.onPostExecute(s);
        }
    }

    public class updatewallet extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONObject json12;
        String walleturl = "_table/user_wallet";
        String w_id = "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            currentdate = sdf.format(new Date());
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("paid_date", currentdate);
            jsonValues1.put("last_paid_amt", mParkingTotal);
            jsonValues1.put("ip", ip);
            jsonValues1.put("remember_me", "");
            jsonValues1.put("current_balance", newbal);
            jsonValues1.put("ipn_txn_id", "");
            jsonValues1.put("ipn_custom", "");
            jsonValues1.put("ipn_payment", "");
            jsonValues1.put("date_time", currentdate);
            jsonValues1.put("ipn_address", "");
            jsonValues1.put("user_id", id);
            jsonValues1.put("new_balance", newbal);
            jsonValues1.put("action", "deduction");

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        wallet_id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            rl_progressbar.setVisibility(View.VISIBLE);
            //progressBar.setVisibility(View.GONE);
            if (getActivity() != null && json12 != null) {
                isPrePaid = true;

                if (!wallet_id.equals("")) {
                    updateWalletBalance();
//                        paymentmethod = "wallet";
//                        showsucessmesg.setVisibility(View.VISIBLE);
//                        ActionStartsHere();
//                        if (Renew_is_valid.equals("yes")) {
//                            new Update_Parking().execute();
//                        } else {
//                            new finalupdate().execute();
//                        }
                }

            }
            super.onPostExecute(s);
        }
    }

    public class finalupdate extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String rid = logindeatl.getString("id", "null");
        String id1 = "null";
        JSONObject json, json1;
        String pov2 = "_table/parked_cars";
        boolean isPayLater = false;
        String paramOption = "";
        private String mPlState = "";

        @Override
        protected void onPreExecute() {
            mPlState = txt_state.getText().toString().toUpperCase();
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            if (params.length != 0) {
                paramOption = params[0];
            }
            isPayLater = !TextUtils.isEmpty(paramOption) && paramOption.equals("payLater");
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            currentdate = sdf.format(new Date());
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            TimeZone zone12 = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone12);
            final String currentDateandTime = currentdate;
            Date date11 = null;
            try {
                date11 = sdf.parse(currentDateandTime);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date11);
            if (min.equals("Hrs") || min.equals("Hr")) {
                calendar.add(Calendar.HOUR, hours);
            } else if (min.equals("Months") || min.equals("Month")) {
                calendar.add(Calendar.MONTH, hours);
                calendar.add(Calendar.DATE, -1);
            } else if (min.equals("Days") || min.equals("Day")) {
                calendar.add(Calendar.DATE, hours);
            } else if (min.equals("Weeks") || min.equals("Week")) {
                calendar.add(Calendar.DAY_OF_WEEK_IN_MONTH, hours);
            } else {
                calendar.add(Calendar.MINUTE, hours);
            }

            String expridate = sdf.format(calendar.getTime());
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
            SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

            Date date = null;
            Date Current = null;
            expiretime = null;
            String final_current_date = null;
            try {
                date = inputFormat.parse(expridate);
                Current = inputFormat.parse(currentdate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            expiretime = outputFormat.format(date);
            final_current_date = outputFormat.format(Current);
            int h = 0;
            if (user_id.equals("null")) {
                h = 0;
            } else {
                h = Integer.parseInt(user_id);
            }
            parkingtype = marker_type;
            String plate = platno;
            //TODO park now, pay later
            if (!mIsPayNow) {

                jsonValues.put("entry_date_time", currentdate);
                jsonValues.put("exit_date_time", expiretime);
                jsonValues.put("expiry_time", expiretime);
                jsonValues.put("max_duration", max_time);
                jsonValues.put("payment_method", "");
                jsonValues.put("payment_choice", "");
                jsonValues.put("parking_rate", parkingRate);

                jsonValues.put("parking_subtotal", mParkingSubTotal);
                jsonValues.put("tr_percent", mTrPercent);
                jsonValues.put("tr_fee", mTrFee);
                jsonValues.put("parking_total", mParkingTotal);
                jsonValues.put("wallet_trx_id", "");

                jsonValues.put("ipn_txn_id", "");
                jsonValues.put("ipn_payment", "");
                jsonValues.put("ipn_status", "");
                jsonValues.put("ipn_address", "");

                jsonValues.put("ParkedNow_PayLater", "1");
                jsonValues.put("ParkNow_PostPayment_term", parkNowPostPayTerms);
                jsonValues.put("ParkNow_PostPayment_Fee", mParkNowPostPayFees);
                jsonValues.put("ParkNow_PostPayment_LateFee", mParkNowPostPayLateFees);
                jsonValues.put("ParkNow_PostPayment_DueAmount", mParkingTotal);
                jsonValues.put("ParkNow_PostPayment_Status", "UNPAID");

            } else {
                jsonValues.put("entry_date_time", currentdate);
                jsonValues.put("exit_date_time", expiretime);
                jsonValues.put("expiry_time", expiretime);
                jsonValues.put("max_duration", max_time);
                jsonValues.put("payment_method", paymentmethod);
                jsonValues.put("payment_choice", paymentmethod);
                jsonValues.put("parking_rate", parkingRate);

                jsonValues.put("parking_subtotal", mParkingSubTotal);
                jsonValues.put("tr_percent", mTrPercent);
                jsonValues.put("tr_fee", mTrFee);
                jsonValues.put("parking_total", mParkingTotal);
                jsonValues.put("wallet_trx_id", wallet_id);

                if (paymentmethod.equals("Paypal")) {
                    jsonValues.put("ipn_txn_id", transection_id);
                    jsonValues.put("ipn_payment", "Paypal");
                    jsonValues.put("ipn_status", "success");
                    jsonValues.put("ipn_address", "");
                } else {
                    jsonValues.put("ipn_txn_id", "");
                    jsonValues.put("ipn_payment", "");
                    jsonValues.put("ipn_status", "");
                    jsonValues.put("ipn_address", "");
                }
            }

            try {
                if (!TextUtils.isEmpty(companyId)) {
                    jsonValues.put("company_id", Integer.valueOf(companyId));
                } else {
                    jsonValues.put("company_id", "");
                }

                if (!TextUtils.isEmpty(twpId)) {
                    jsonValues.put("twp_id", Integer.valueOf(twpId));
                } else {
                    jsonValues.put("twp_id", "");
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("#DEBUG", "   reserveNow:  Error:  " + e.getMessage());
            }

            plate = plate.replaceAll("\\s+", "");
            jsonValues.put("parking_type", parkingtype);
            jsonValues.put("township_code", township_code1);
            jsonValues.put("manager_type_id", manager_type_id);
            jsonValues.put("location_code", location_code);
            jsonValues.put("location_name", locationname);

            jsonValues.put("user_id", h);
            jsonValues.put("permit_id", 0);
            jsonValues.put("subscription_id", 0);
            jsonValues.put("plate_no", plate);
            jsonValues.put("pl_state", mPlState);
            jsonValues.put("lat", latitude);
            jsonValues.put("lng", longitude);
            jsonValues.put("address1", adrreslocation);
            jsonValues.put("address2", mAddress2);
            if (!TextUtils.isEmpty(mCity)) {
                jsonValues.put("city", mCity);
            } else {
                jsonValues.put("city", locationname + lost_row1 + lost_no1);
            }
            if (!TextUtils.isEmpty(mDistrict)) {
                jsonValues.put("district", mDistrict);
            } else {
                jsonValues.put("district", "");
            }
            if (!TextUtils.isEmpty(mStateCode)) {
                jsonValues.put("state", mStateCode);
            } else {
                jsonValues.put("state", statename);
            }
            if (!TextUtils.isEmpty(mZipCode)) {
                jsonValues.put("zip", mZipCode);
            } else jsonValues.put("zip", zip_code);
            if (!TextUtils.isEmpty(mCountryCode)) {
                jsonValues.put("country", mCountryCode);
            } else jsonValues.put("country", "USA");
            if (selectedLocationLot != null) {
                if (!TextUtils.isEmpty(selectedLocationLot.getLot_id())) {
                    jsonValues.put("lot_id", selectedLocationLot.getLot_id());
                }
                if (!TextUtils.isEmpty(selectedLocationLot.getLot_number())) {
                    jsonValues.put("lot_number", selectedLocationLot.getLot_number());
                }
                if (!TextUtils.isEmpty(selectedLocationLot.getLot_row())) {
                    jsonValues.put("lot_row", selectedLocationLot.getLot_row());
                }
                jsonValues.put("location_lot_id", selectedLocationLot.getId());
            }
            jsonValues.put("ip", getLocalIpAddress());
            jsonValues.put("parking_token", token);
            jsonValues.put("parking_status", "ENTRY");
            jsonValues.put("platform", "Android");

            jsonValues.put("parking_units", String.valueOf(parkingUnits));
            jsonValues.put("parking_qty", parkingQty);

            jsonValues.put("ipn_custom", "");

            jsonValues.put("location_id", location_id);

            String mismatch = "0";
            float dis = getdistances(locationaddress, adrreslocation);
            float ff = dis / 1609;
            if (ff > 0.5) {
                mismatch = "1";
            }
            jsonValues.put("distance_to_marker", getdistances(locationaddress, adrreslocation));
            jsonValues.put("marker_address1", locationaddress);
            jsonValues.put("marker_address2", mMarkerAddress2);
            jsonValues.put("marker_city", mMarkerCity);
            jsonValues.put("marker_district", mMarkerDistrict);
            jsonValues.put("marker_state", mMarkerStateCode);
            jsonValues.put("marker_zip", mMarkerZipCode);
            jsonValues.put("marker_country", mMarkerCountryCode);
            jsonValues.put("token", 0);
            jsonValues.put("mismatch", mismatch);
            jsonValues.put("marker_lng", lang);
            jsonValues.put("marker_lat", lat);
            jsonValues.put("ticket_status", "");
            jsonValues.put("date_time", "");
            jsonValues.put("duration_unit", min);
            jsonValues.put("unit_pricing", rate);
            if (renew.equals("yes")) {
                jsonValues.put("selected_duration", final_count);
            } else {
                jsonValues.put("selected_duration", hours);
            }
            jsonValues.put("pl_country", p_country);
            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + pov2;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id1 = c.getString("id");

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            //progressBar.setVisibility(View.GONE);
            if (getActivity() != null && json1 != null) {
                if (!id1.equals("null")) {
                    if (user_id.equals("null")) {
                        ArrayList<item> arraylist = new ArrayList<>();
                        SharedPreferences sharedPrefs = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
                        Gson gson = new Gson();
                        String json = sharedPrefs.getString("car", null);
                        Type type = new TypeToken<ArrayList<item>>() {
                        }.getType();
                        arraylist = gson.fromJson(json, type);
                        if (arraylist == null) {
                            arraylist = new ArrayList<>();
                        }
                        item ii = new item();
                        ii.setPlate_no(platno);
                        ii.setId(id1);
                        arraylist.add(ii);

                        SharedPreferences sharedPrefs1 = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPrefs1.edit();
                        Gson gson1 = new Gson();
                        String json1 = gson1.toJson(arraylist);
                        editor.putString("car", json1);
                        editor.commit();
                    }

                    if (rl_sp_row.getVisibility() != View.VISIBLE) {
                        new getparked_car(id1).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new updateLocationLot(id1).execute();
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class getparked_car extends AsyncTask<String, String, String> {
        String park_id;
        JSONObject json;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String login_id = logindeatl.getString("id", "0");
        String transactionurl = "_table/parked_cars?filter=(user_id%3D" + login_id + ")%20AND%20(parking_status%3D'ENTRY')";
        String id = "null";

        public getparked_car(String id) {
            this.park_id = id;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                        String plnp = c.getString("plate_no");
                        String state = c.getString("pl_state");
                        String state1 = c.getString("state");

                        if (!park_id.equals("null")) {
                            if (id.equals(park_id)) {

                                String city = c.getString("city");
                                String country = c.getString("country");
                                String exit = c.getString("exit_date_time");

                                SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
                                SharedPreferences.Editor ed = time.edit();
                                ed.clear();
                                ed.commit();
                                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sh.edit();
                                editor.putString("platno", platno);
                                editor.putString("state", state);
                                editor.putString("currentdate", currentdate);
                                editor.putString("hr", String.valueOf(hours));
                                editor.putString("max", max_time);
                                editor.putString("id", id);
                                editor.putString("min", min);
                                editor.putString("token", token1);
                                editor.putString("exip", expiretime);
                                editor.putString("lat", String.valueOf(latitude));
                                editor.putString("markertye", marker);
                                editor.putString("log", String.valueOf(longitude));
                                editor.putString("country", country);
                                editor.putString("city", city);
                                editor.putString("exit_date_time", exit);
                                editor.putString("address", locationaddress);
                                editor.putString("parked_address", adrreslocation);
                                editor.putString("locationname", location_code);
                                editor.putString("lot_row", lost_row1);
                                editor.putString("lot_no", lost_no1);
                                editor.putString("title", title);
                                editor.putString("parking_type", parkingtype);
                                editor.putString("parkhere", "no");
                                editor.putString("parking_free", "no");
                                editor.putString("parked_time", c.getString("selected_duration"));
                                editor.putString("entryTime", c.getString("entry_date_time"));
                                editor.putString("exitTime", c.getString("exit_date_time"));
                                editor.putString("locationName", c.getString("location_name"));
                                editor.putString("locationCode", c.getString("location_code"));
                                editor.putString("lotId", c.getString("lot_id"));
                                editor.commit();
                                SharedPreferences sh1 = getActivity().getSharedPreferences("token", Context.MODE_PRIVATE);
                                SharedPreferences.Editor ed1 = sh1.edit();
                                ed1.putString("token", token1);
                                ed1.commit();
                                break;
                            }
                        } else {
                            if (state.equals("null")) {
                                state = state1;
                            }
                            if (plnp.equals(platno) && state.equals(statename)) {
                                String city = c.getString("city");
                                String country = c.getString("country");
                                String exit = c.getString("exit_date_time");
                                String extime = c.getString("expiry_time");
                                String max = c.getString("max_duration");
                                String locationcode = c.getString("location_code");
                                String entrytime = c.getString("entry_date_time");
                                String addres = c.getString("address1");
                                String parked_adress = c.getString("marker_address1");
                                String lat = c.getString("lat");
                                String lang = c.getString("lng");
                                String lot = c.getString("lot_number");
                                String lot_row = c.getString("lot_row");
                                String location_lot_id = c.getString("location_lot_id");
                                String parking_type = c.getString("parking_type");
                                String duration_unit = c.getString("duration_unit");
                                String locationCode = c.getString("location_code");
                                String locationName = c.getString("location_name");
                                String entryDateTime = c.getString("entry_date_time");
                                String exitDateTime = c.getString("exit_date_time");
                                String lotId = c.getString("lot_id");
                                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sh.edit();
                                editor.putString("platno", platno);
                                editor.putString("location_lot_id", location_lot_id);
                                editor.putString("state", statename);
                                editor.putString("currentdate", entrytime);
                                editor.putString("exip", extime);
                                editor.putString("hr", "0");
                                editor.putString("max", max);
                                editor.putString("lat", lat);
                                editor.putString("log", lang);
                                editor.putString("min", duration_unit);
                                editor.putString("id", id);
                                editor.putString("token", "");
                                editor.putString("parked_address", parked_adress);
                                editor.putString("country", country);
                                editor.putString("city", city);
                                editor.putString("exit_date_time", exit);
                                editor.putString("address", addres);
                                editor.putString("locationname", locationcode);
                                editor.putString("markertye", marker);
                                editor.putString("title", title);
                                editor.putString("lot_row", lot);
                                editor.putString("lot_no", lot_row);
                                editor.putString("parking_type", parking_type);
                                editor.putString("parkhere", "no");
                                editor.putString("parking_free", "no");
                                editor.putString("parked_time", c.getString("selected_duration"));
                                editor.putString("entryTime", entryDateTime);
                                editor.putString("exitTime", exitDateTime);
                                editor.putString("locationName", locationName);
                                editor.putString("locationCode", locationCode);
                                editor.putString("lotId", lotId);
                                editor.commit();
                            }
                        }

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            //progressBar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json != null) {

                if (!id.equals("null")) {
                    if (renew.equals("yes")) {
                        open_timer();
                    } else {
                        direct_open_timer(park_id);
                    }
                }
            }
            super.onPostExecute(s);

        }
    }


    public class Update_Parking extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String rid = logindeatl.getString("id", "null");
        String id1 = "null";
        JSONObject json, json1;
        String pov2 = "_table/parked_cars";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);

            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            TimeZone zone12 = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone12);
            final String currentDateandTime = Renew_date;
            Date date11 = null;
            try {
                date11 = sdf.parse(currentDateandTime);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date11);
            if (min.equals("Hrs") || min.equals("Hour")) {
                calendar.add(Calendar.HOUR, hours);
            } else if (min.equals("Months") || min.equals("Month")) {
                calendar.add(Calendar.MONTH, hours);
                calendar.add(Calendar.DATE, -1);
            } else if (min.equals("Days") || min.equals("Day")) {
                calendar.add(Calendar.DATE, hours);
            } else if (min.equals("Weeks") || min.equals("Week")) {
                calendar.add(Calendar.DAY_OF_WEEK_IN_MONTH, hours);
            } else {
                calendar.add(Calendar.MINUTE, hours);
            }

            String expridate = sdf.format(calendar.getTime());
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.US);
            SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            outputFormat.setTimeZone(zone12);
            inputFormat.setTimeZone(zone12);

            Date date = null;
            Date Current = null;
            expiretime = null;
            String final_current_date = null;
            try {
                date = inputFormat.parse(expridate);
                Current = inputFormat.parse(Renew_date);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

            expiretime = outputFormat.format(date);
            final_current_date = outputFormat.format(Current);
            int h = 0;
            if (user_id.equals("null")) {
                h = 0;
            } else {
                h = Integer.parseInt(user_id);
            }

            jsonValues.put("exit_date_time", expiretime);
            jsonValues.put("expiry_time", expiretime);
            jsonValues.put("selected_duration", final_count);
            jsonValues.put("id", ReNew_id);
            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + pov2;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id1 = c.getString("id");

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);

            if (getActivity() != null && json1 != null) {
                if (!id1.equals("null")) {
                    new getparked_car_renew(id1).execute();

                }
            }
            super.onPostExecute(s);
        }
    }

    public class getparked_car_renew extends AsyncTask<String, String, String> {
        String park_id;
        JSONObject json;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String login_id = logindeatl.getString("id", "0");
        String transactionurl = "_table/parked_cars?filter=(user_id%3D" + login_id + ")%20AND%20(parking_status%3D'ENTRY')";
        String id = "null";

        public getparked_car_renew(String id) {
            this.park_id = id;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            String url = getString(R.string.api) + getString(R.string.povlive) + transactionurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                        String plnp = c.getString("plate_no");
                        String state = c.getString("pl_state");
                        String state1 = c.getString("state");

                        if (!park_id.equals("null")) {
                            if (id.equals(park_id)) {

                                String city = c.getString("city");
                                String country = c.getString("country");
                                String exit = c.getString("exit_date_time");
                                String extime = c.getString("expiry_time");
                                String max = c.getString("max_duration");
                                String locationcode = c.getString("location_code");
                                String entrytime = c.getString("entry_date_time");
                                String addres = c.getString("address1");
                                String parked_adress = c.getString("marker_address1");
                                String lat = c.getString("lat");
                                String lang = c.getString("lng");
                                String lot = c.getString("lot_number");
                                String lot_row = c.getString("lot_row");
                                String parking_type = c.getString("parking_type");
                                String duration_unit = c.getString("duration_unit");
                                SharedPreferences sh = getActivity().getSharedPreferences("timer", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sh.edit();
                                editor.clear().commit();
                                editor.putString("platno", plnp);
                                editor.putString("state", state);
                                editor.putString("currentdate", entrytime);
                                editor.putString("exip", expiretime);
                                editor.putString("exip1", expiretime);
                                editor.putString("hr", "0");
                                editor.putString("max", max);
                                editor.putString("lat", lat);
                                editor.putString("log", lang);
                                editor.putString("min", duration_unit);
                                editor.putString("id", id);
                                editor.putString("token", "");
                                editor.putString("country", country);
                                editor.putString("city", city);
                                editor.putString("exit_date_time", exit);
                                editor.putString("address", parked_adress);
                                editor.putString("parked_address", addres);
                                editor.putString("locationname", locationcode);
                                editor.putString("markertye", marker);
                                editor.putString("parking_type", parking_type);
                                editor.putString("lot_row", lot_row);
                                editor.putString("lot_no", lot);
                                editor.putString("parkhere", "no");
                                editor.putString("parking_free", "no");
                                editor.putString("parked_time", c.getString("selected_duration"));
                                editor.putString("entryTime", c.getString("entry_date_time"));
                                editor.putString("exitTime", c.getString("exit_date_time"));
                                editor.putString("locationName", c.getString("location_name"));
                                editor.putString("locationCode", c.getString("location_code"));
                                editor.putString("lotId", c.getString("lot_id"));
                                editor.commit();
                                editor.apply();
                                Log.e("cecc", "ecnejnce");
                                break;
                            }
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json != null) {

                if (!id.equals("null")) {
                    if (renew.equals("yes")) {
                        open_timer();
                    } else {
                        direct_open_timer(park_id);
                    }
                }
            }
            super.onPostExecute(s);

        }
    }


    @Override
    public void onResume() {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        id = logindeatl.getString("id", "null");
        if (is_add_back) {
            is_add_back = false;
            if (!id.equals("null")) {
                new getvehiclenumber().execute();
            }
        }
        super.onResume();
    }

    public class updateLocationLot extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String exit_status = "_table/location_lot";
        JSONObject json, json1;
        String re_id;
        String id;

        public updateLocationLot(String idd) {
            this.id = idd;
        }

        @Override
        protected void onPreExecute() {
            if (mIsParking) {
                rl_progressbar.setVisibility(View.VISIBLE);
            }

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", selectedLocationLot.getId());
            if (mIsParking) {
                jsonValues.put("plate_no", platno);
                jsonValues.put("plate_state", statename);
                jsonValues.put("occupied", "YES");
            } else {
                jsonValues.put("is_reserved", 1);
            }
            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            Log.e("managed_url", url);
            Log.e("parama", String.valueOf(json));

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.e("#DEBUG", "  updateManageLot: ResponseID: " + re_id);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);

            if (getActivity() != null && json1 != null) {
                if (!re_id.equals("null")) {
                    if (mIsParking) {
                        new getparked_car(id).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
                super.onPostExecute(s);
            }
        }
    }

    public class checkcardparkedornotwallet extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONArray array = new JSONArray();
        String parked_id = "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String vechiclenourl = null;
            try {
                String plate = platno;
                plate = plate.replaceAll("\\s+", "");
                vechiclenourl = "_table/parked_cars?filter=(plate_no%3D" + URLEncoder.encode(plate, "utf-8") + ")%20AND%20(pl_state%3D" + statename + ")%20AND%20(parking_status%3D'ENTRY')";
            } catch (Exception e) {
                e.printStackTrace();
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + vechiclenourl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject v = array.getJSONObject(i);
                        parked_id = v.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (!array.isNull(0)) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Vehicle already parked!");
                    alertDialog.setMessage("This Vehicle is already parked, Do you want to exit it from previous parking and park here ?");

                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            new exitsparking(parked_id).execute();
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });
                    alertDialog.show();
                } else {
                    if (id.equals("null")) {
                        new getwalletbal().execute();
                    } else {
                        new getwalletbal().execute();
                    }
                }
            }
            super.onPostExecute(s);

        }
    }

    public class checkcardparkedornotpaidt extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONArray array;
        String tcode = logindeatl.getString("tcode", "null"), username = logindeatl.getString("name", "null");
        String parked_id = "null";

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String vechiclenourl = null;
            try {
                String plate = platno;
                plate = plate.replaceAll("\\s+", "");
                vechiclenourl = "_table/parked_cars?filter=(plate_no%3D" + URLEncoder.encode(plate, "utf-8") + ")%20AND%20(pl_state%3D" + statename + ")%20AND%20(parking_status%3D'ENTRY')";
            } catch (Exception e) {
                e.printStackTrace();
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + vechiclenourl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject v = array.getJSONObject(i);
                        parked_id = v.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (array.length() > 0) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Vehicle already parked!");
                    alertDialog.setMessage("This Vehicle is already parked, Do you want to exit it from previous parking and park here ?");
                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            new exitsparking(parked_id).execute();
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });
                    alertDialog.show();
                } else {

                    if (!id.equals("null")) {
                        gettotalamount();
                    } else {

                        showpaypalrandomnumber(getActivity());
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class checkParkedCarsReserved extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONArray array = new JSONArray();
        String parked_id = "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String vechiclenourl = null;
            try {
                String plate = platno;
                plate = plate.replaceAll("\\s+", "");
                vechiclenourl = "_table/parked_cars?filter=(plate_no%3D" + URLEncoder.encode(plate, "utf-8") + ")%20AND%20(pl_state%3D" + statename + ")%20AND%20(parking_status%3D'ENTRY')";
            } catch (Exception e) {
                e.printStackTrace();
            }
            String url = getString(R.string.api) + getString(R.string.povlive) + vechiclenourl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject v = array.getJSONObject(i);
                        parked_id = v.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                if (!array.isNull(0)) {
                    //show dialog exit parking
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Vehicle already parked!");
                    alertDialog.setMessage("This Vehicle is already parked, Do you want to exit it from previous parking and park here ?");

                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            new exitsparking(parked_id).execute();
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });
                    alertDialog.show();
                } else {
                    // post values into reserved_parkings
//                    getTotalAmountReserved();

                    //not parked, park car -> add to parked_cars
                    if (mIsPayNow) {
                        //start payment for parking
                        if (mIsWalletPayment) {
                            //pay with wallet balance
                            getWalletBalance();
                        } else {
                            //pay with paypal
                            startPayPalPaymentRequest();
                        }
                    } else {
                        //add to parked_car with pay later
                        addParkedCar();
                    }

                }
            }
            super.onPostExecute(s);

        }
    }

    public class exitsparking extends AsyncTask<String, String, String> {
        String exit_status = "_table/parked_cars";
        JSONObject json, json1;
        String re_id = "null";
        String parked_id;

        public exitsparking(String pid) {
            this.parked_id = pid;
        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            currentdate = sdf.format(new Date());
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("id", parked_id);
            jsonValues.put("parking_status", "EXIT");
            jsonValues.put("exit_date_time", currentdate);
            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + exit_status;
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.d("re_id", re_id);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json1 != null) {
                if (!re_id.equals("null")) {
                    canclealarm(Integer.parseInt(parked_id));
                    if (mIsPayNow) {
                        //start payment for parking
                        if (mIsWalletPayment) {
                            //pay with wallet balance
                            getWalletBalance();
                        } else {
                            //pay with paypal
                            startPayPalPaymentRequest();
                        }
                    } else {
                        //add to parked_car with pay later
                        addParkedCar();
                    }
//                    if (isclickonwallet) {
//                        if (id.equals("null")) {
//                            new getwalletbal().execute();
//                        } else {
//                            new getwalletbal().execute();
//                        }
//                    } else if (isClickReserved) {
//                        getTotalAmountReserved();
//                    } else {
//                        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
//                        String id1 = logindeatl.getString("id", "null");
//                        if (id1.equals("null")) {
//                            ArrayList<item> arraylist = new ArrayList<>();
//                            SharedPreferences sharedPrefs = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
//                            Gson gson = new Gson();
//                            String json = sharedPrefs.getString("car", null);
//                            Type type = new TypeToken<ArrayList<item>>() {
//                            }.getType();
//                            arraylist = gson.fromJson(json, type);
//                            if (arraylist == null) {
//                                arraylist = new ArrayList<>();
//                            }
//                            if (arraylist.size() > 0) {
//                                for (int j = 0; j < arraylist.size(); j++) {
//                                    String id = arraylist.get(j).getId();
//                                    if (id.equals(re_id)) {
//                                        arraylist.remove(j);
//                                    }
//                                }
//                            }
//                            SharedPreferences sharedPrefs1 = getActivity().getSharedPreferences("parked_vehicle", Context.MODE_PRIVATE);
//                            SharedPreferences.Editor editor = sharedPrefs1.edit();
//                            Gson gson1 = new Gson();
//                            String json1 = gson1.toJson(arraylist);
//                            editor.putString("car", json1);
//                            editor.commit();
//                        }
//                        gettotalamount();
//                    }
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Something went wrong");
                    alertDialog.setMessage("Please try again");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
            super.onPostExecute(s);
        }
    }

    public void canclealarm(int id) {
        Intent notificationIntent = new Intent(getActivity(), NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, id);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(getActivity(), id + 1, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent alarmIntent1 = PendingIntent.getBroadcast(getActivity(), id + 2, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent alarmIntent3 = PendingIntent.getBroadcast(getActivity(), id + 3, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
        alarmManager.cancel(alarmIntent);
        alarmIntent.cancel();
        alarmManager.cancel(alarmIntent1);
        alarmIntent1.cancel();
        alarmManager.cancel(alarmIntent3);
        alarmIntent3.cancel();
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    private void ShowStatePupup(Activity context, final ArrayList<item> data) {

        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.statepopup_pre, viewGroup, false);
        final PopupWindow popupmanaged = new PopupWindow(context);
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        //  popupmanaged.setAnimationStyle(R.style.animationName);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);
        Button btnparkhere;
        ImageView imageclose;
        GridView grid_managed;
        RelativeLayout rl_popclose;
        ImageView img_state;
        TextView txt_state_;


        btnparkhere = (Button) layout.findViewById(R.id.btn_managed_park);
        grid_managed = (GridView) layout.findViewById(R.id.grid_managed);
        imageclose = (ImageView) layout.findViewById(R.id.img_managed_close);
        RelativeLayout rl_done = (RelativeLayout) layout.findViewById(R.id.rl_done);
        rl_popclose = (RelativeLayout) layout.findViewById(R.id.rl_pop);
        rl_popclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });
        img_state = (ImageView) layout.findViewById(R.id.img_state);
        txt_state_ = (TextView) layout.findViewById(R.id.txt_state_name);

        Resources rs;
        final state_adapter adpter = new state_adapter(getActivity(), data, rs = getResources());
        grid_managed.setAdapter(adpter);


        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  for(int i=0;i<data.size();i++){
                    data.get(i).setSelected_parking(false);
                }*/
                //adpter.notifyDataSetChanged();
                popupmanaged.dismiss();
                // txt_state.setText("State");

            }
        });

        for (int k = 0; k < data.size(); k++) {
            boolean selected_ore = data.get(k).isSelected_parking();

            if (selected_ore) {
                statename = data.get(k).getState();
                img_state.setImageResource(R.mipmap.new_state_icon);
                txt_state_.setText(statename);
                break;
            }
        }

        rl_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();

                for (int k = 0; k < data.size(); k++) {
                    boolean selected_ore = data.get(k).isSelected_parking();

                    if (selected_ore) {
                        statename = data.get(k).getState();
                        txt_state.setText(statename);
                        break;
                    }
                }
                ShowHours(getActivity(), stateq);
                show_marke_payment_button();
                adpter.notifyDataSetChanged();

            }
        });
        grid_managed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                for (int i = 0; i < data.size(); i++) {
                    data.get(i).setSelected_parking(false);
                }
                data.get(position).setSelected_parking(true);
                statename = data.get(position).getState();
                plateState = data.get(position).getState();
                txt_state.setText(statename);
                adpter.notifyDataSetChanged();
                popupmanaged.dismiss();
                ShowHours(getActivity(), stateq);
                show_marke_payment_button();
            }
        });


    }

    public class state_adapter extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        boolean isselect = false;
        int a = 0;
        private Activity activity;
        private ArrayList<item> data;
        private SparseBooleanArray mCheckStates;
        private LayoutInflater inflater = null;

        /*************
         * CustomAdapter Constructor
         *****************/
        public state_adapter(Activity a, ArrayList<item> d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;
            data = d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        /********
         * What is the size of Passed Arraylist Size
         ************/
        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        /******
         * Depends upon data size called for each row , Create each ListView row
         *****/
        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;


            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_state, null);
                holder = new ViewHolder();

                holder.txt_state = (TextView) vi.findViewById(R.id.txt_state);
                holder.img = (ImageView) vi.findViewById(R.id.img_state);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {

            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String cat = tempValues.getState();
                if (cat == null) {
                } else if (cat != null) {

                    if (tempValues.isSelected_parking()) {
                        holder.img.setImageResource(R.mipmap.new_state_icon);
                        holder.txt_state.setTextColor(getResources().getColor(R.color.selected_text));
                    } else {
                        holder.img.setImageResource(R.mipmap.icon_new_state_selected);
                        holder.txt_state.setTextColor(Color.parseColor("#000000"));
                    }

                    holder.txt_state.setText(tempValues.getState());
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }


        public class ViewHolder {

            public TextView txt_state;
            ImageView img;
        }
    }

    private void ShowHours(Activity context, final ArrayList<item> data1) {

        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.hourpopup_p, viewGroup, false);
        final PopupWindow popupmanaged = new PopupWindow(context);
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        //  popupmanaged.setAnimationStyle(R.style.animationName);
        popupmanaged.showAtLocation(layout, Gravity.CENTER, 0, 0);
        Button btnparkhere;
        ImageView imageclose;
        GridView grid_managed;
        RelativeLayout rl_popclose;
        ImageView img_state;
        TextView txt_state_;
        btnparkhere = (Button) layout.findViewById(R.id.btn_managed_park);
        grid_managed = (GridView) layout.findViewById(R.id.grid_managed);
        imageclose = (ImageView) layout.findViewById(R.id.img_managed_close);
        RelativeLayout rl_done = (RelativeLayout) layout.findViewById(R.id.rl_done);
        rl_popclose = (RelativeLayout) layout.findViewById(R.id.rl_pop);
        rl_popclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });
        img_state = (ImageView) layout.findViewById(R.id.img_state);
        txt_state_ = (TextView) layout.findViewById(R.id.txt_state_name);

        Resources rs;
        final hour_adapter adpter = new hour_adapter(getActivity(), data1, rs = getResources());
        grid_managed.setAdapter(adpter);


        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  for(int i=0;i<data.size();i++){
                    data.get(i).setSelected_parking(false);
                }
                adpter.notifyDataSetChanged();*/
                popupmanaged.dismiss();
                // txt_hours.setText("Select Hour");

            }
        });

        for (int k = 0; k < data1.size(); k++) {
            boolean selected_ore = data1.get(k).isSelected_parking();

            if (selected_ore) {
                shoues = data1.get(k).getState();
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    txt_state_.setText(hr + " Hrs");
                } else {
                    txt_state_.setText(shoues);
                }
                img_state.setImageResource(R.mipmap.new_cook_hr);
                break;
            }
        }

        rl_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();

                for (int k = 0; k < data1.size(); k++) {
                    boolean selected_ore = data1.get(k).isSelected_parking();

                    if (selected_ore) {
                        shoues = data1.get(k).getState();
                        if (shoues.contains("Hour")) {
                            String hr = shoues.substring(0, shoues.indexOf(' '));
                            txt_hours.setText(hr + " Hrs");
                        } else {
                            txt_hours.setText(shoues);
                        }
                        break;
                    }
                }

                show_marke_payment_button();

            }
        });
        grid_managed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                for (int i = 0; i < data1.size(); i++) {
                    data1.get(i).setSelected_parking(false);
                }
                data1.get(position).setSelected_parking(true);
                shoues = data1.get(position).getState();
                if (shoues.contains("Hour")) {
                    String hr = shoues.substring(0, shoues.indexOf(' '));
                    txt_hours.setText(hr + " Hrs");
                } else {
                    txt_hours.setText(shoues);
                }
                adpter.notifyDataSetChanged();
                popupmanaged.dismiss();
                if (rl_sp_row.getVisibility() == View.VISIBLE) {
//                    Showmanaedpopup(getActivity(), data);
                    showDialogLocationLots();
                    show_marke_payment_button();
                } else {
                    show_marke_payment_button();
                }
            }
        });


    }

    public class hour_adapter extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        boolean isselect = false;
        int a = 0;
        private Activity activity;
        private ArrayList<item> data;
        private SparseBooleanArray mCheckStates;
        private LayoutInflater inflater = null;

        /*************
         * CustomAdapter Constructor
         *****************/
        public hour_adapter(Activity a, ArrayList<item> d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;
            data = d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        /********
         * What is the size of Passed Arraylist Size
         ************/
        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        /******
         * Depends upon data size called for each row , Create each ListView row
         *****/
        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;


            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapter_state, null);
                holder = new ViewHolder();

                holder.txt_state = (TextView) vi.findViewById(R.id.txt_state);
                holder.img = (ImageView) vi.findViewById(R.id.img_state);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {

            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String cat = tempValues.getState();
                if (cat == null) {
                } else if (cat != null) {

                    if (tempValues.isSelected_parking()) {
                        holder.img.setImageResource(R.mipmap.new_cook_hr);
                        holder.txt_state.setTextColor(getResources().getColor(R.color.selected_text));
                    } else {
                        holder.img.setImageResource(R.mipmap.icon_cook_un);
                        holder.txt_state.setTextColor(Color.parseColor("#000000"));
                    }

                    holder.txt_state.setText(tempValues.getState());
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }


        public class ViewHolder {

            public TextView txt_state;
            ImageView img;
        }
    }

    public class managedlostpop extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        boolean isselect = false;
        int a = 0;
        private Activity activity;
        private ArrayList<item> data;
        private SparseBooleanArray mCheckStates;
        private LayoutInflater inflater = null;

        /*************
         * CustomAdapter Constructor
         *****************/
        public managedlostpop(Activity a, ArrayList<item> d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;
            data = d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        /********
         * What is the size of Passed Arraylist Size
         ************/
        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        /******
         * Depends upon data size called for each row , Create each ListView row
         *****/
        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;


            if (convertView == null) {
                vi = inflater.inflate(R.layout.adapatermanaed, null);
                holder = new ViewHolder();

                holder.txtprices = (TextView) vi.findViewById(R.id.txt_row_no1);
                holder.img = (ImageView) vi.findViewById(R.id.img_managed_car);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {

            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String cat = tempValues.getRow_no();
                if (cat == null) {
                } else if (cat != null) {

                    if (tempValues.getSpaces_mark_reqd().equals("false")) {
                        holder.txtprices.setText("R: " + tempValues.getDisplay());
                    } else if (tempValues.getMenu_img_id() < 2) {
                        holder.txtprices.setText("S: " + tempValues.getDisplay());
                    } else {
                        holder.txtprices.setText(tempValues.getDisplay());
                    }
                    if (tempValues.getIsparked().equals("t")) {
                        if (tempValues.getMarker().equals("red")) {
                            holder.img.setBackgroundResource(R.mipmap.popup_car_red);
                        } else if (tempValues.getMarker().equals("green")) {
                            holder.img.setBackgroundResource(R.mipmap.popup_car_green);
                        }
                    } else {

                        if (tempValues.isSelected_parking()) {
                            holder.img.setBackgroundResource(R.mipmap.popup_car_green);
                        } else {
                            holder.img.setBackgroundResource(R.mipmap.popup_gree_car);
                        }


                    }


                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }


        public class ViewHolder {

            public TextView txtparkinname, txtprices, txtoccie;
            ImageView img;
        }
    }

    private void Showmanaedpopup(Activity context, final ArrayList<item> data) {

        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.managedcarpopup_pre, viewGroup, false);
        final PopupWindow popupmanaged = new PopupWindow(context);
        popupmanaged.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupmanaged.setContentView(layout);
        popupmanaged.setBackgroundDrawable(null);
        popupmanaged.setFocusable(true);
        //  popupmanaged.setAnimationStyle(R.style.animationName);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int yyy = rl_main.getHeight();
            int yyy1 = rl_title.getHeight();
            int final_position = yyy1 + yyy + 245;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                popupmanaged.showAtLocation(layout, Gravity.NO_GRAVITY, 0, final_position);
            } else {
                popupmanaged.showAsDropDown(rl_main, 0, 0);
            }


        } else {
            popupmanaged.showAsDropDown(rl_main, 0, 0);
        }
        Button btnparkhere;
        ImageView imageclose;
        GridView grid_managed;
        RelativeLayout rl_popclose;
        final ImageView img_car;
        final TextView txt_car;
        btnparkhere = (Button) layout.findViewById(R.id.btn_managed_park);
        grid_managed = (GridView) layout.findViewById(R.id.grid_managed);
        imageclose = (ImageView) layout.findViewById(R.id.img_managed_close);
        RelativeLayout rl_done = (RelativeLayout) layout.findViewById(R.id.rl_done);
        rl_popclose = (RelativeLayout) layout.findViewById(R.id.rl_pop);
        rl_popclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();
            }
        });
        img_car = (ImageView) layout.findViewById(R.id.img_managed_car);
        txt_car = (TextView) layout.findViewById(R.id.txt_row_no1);
        Resources rs;
        final managedlostpop adpter = new managedlostpop(getActivity(), data, rs = getResources());
        grid_managed.setAdapter(adpter);


        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  for(int i=0;i<data.size();i++){
                    data.get(i).setSelected_parking(false);
                }
                adpter.notifyDataSetChanged();*/
                popupmanaged.dismiss();
                //txt_space.setText("Row and Space #");
            }
        });

        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).isSelected_parking()) {
                String row_no = data.get(i).getRow_no();
                String row_name = data.get(i).getRoe_name();
                txt_car.setText(row_name + " : " + row_no);
                img_car.setImageResource(R.mipmap.popup_car_green);
            }
        }

        rl_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupmanaged.dismiss();

                for (int k = 0; k < data.size(); k++) {
                    boolean selected_ore = data.get(k).isSelected_parking();

                    if (selected_ore) {
                        String row_no = data.get(k).getRow_no();
                        String row_name = data.get(k).getRoe_name();
                        txt_row.setText(row_name + " : " + row_no);
                        // txt_space.setText("Row "+row_name+" Space # "+row_no);
                        lost_no = row_no;
                        lost_row = row_name;
                        //Toast.makeText(getActivity(), "Row Name:- "+row_name +" Row No "+row_no, Toast.LENGTH_SHORT).show();
                        break;
                    }
                }

                show_marke_payment_button();

            }
        });
        grid_managed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                if (!data.get(position).getIsparked().equals("t")) {

                    for (int i = 0; i < data.size(); i++) {
                        data.get(i).setSelected_parking(false);
                    }
                    data.get(position).setSelected_parking(true);
                    adpter.notifyDataSetChanged();
                    String row_no = data.get(position).getRow_no();
                    String row_name = data.get(position).getRoe_name();
                    // txt_space.setText("Row "+row_name+" Space # "+row_no);
                    if (township_sapce_marked.equals("false")) {
                        lost_no = row_no;
                        txt_row.setText("R : " + row_no);
                    } else if (total_row < 2) {
                        lost_row = row_name;
                        txt_row.setText("S : " + row_name);
                    } else {
                        lost_no = row_no;
                        lost_row = row_name;
                        txt_row.setText(row_name + " : " + row_no);
                    }
                    popupmanaged.dismiss();
                    show_marke_payment_button();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("ParkEZly!!");
                    alertDialog.setMessage("This Lot Seems To Be Occupied. Are you sure you want to park here ?");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            for (int i = 0; i < data.size(); i++) {
                                data.get(i).setSelected_parking(false);
                            }
                            data.get(position).setSelected_parking(true);
                            adpter.notifyDataSetChanged();


                            for (int k = 0; k < data.size(); k++) {
                                boolean selected_ore = data.get(k).isSelected_parking();

                                if (selected_ore) {
                                    String row_no = data.get(k).getRow_no();
                                    String row_name = data.get(k).getRoe_name();
                                    //Toast.makeText(getActivity(), "Row Name:- "+row_name +" Row No "+row_no, Toast.LENGTH_SHORT).show();
                                    break;
                                }
                            }
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    alertDialog.show();
                }
            }
        });


    }

    public class fetchLocationLotOccupiedData extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String lat, lang, address, title, html, mark, parking_time, no_parking_time, notice;

        String managedlosturl = "_proc/managed_lot_status?&order=(lot_row%20ASC%2C%20lot_number%20ASC)";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {


            locationLots.clear();
            JSONObject managed = new JSONObject();


            /*  if(latitude==0.0) {*/
            try {
                managed.put("name", "in_location_code");
                managed.put("value", location_code);

                //  student1.put("value", latitude);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            JSONArray jsonArray = new JSONArray();
            jsonArray.put(managed);

            JSONObject manageda = new JSONObject();
            try {
                manageda.put("params", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("managed_lost_url", url);
            Log.e("managed_lodt_param", String.valueOf(manageda));

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            StringEntity entity = null;
            try {
                entity = new StringEntity(manageda.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);

            HttpResponse response = null;

            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    TimeZone zone = TimeZone.getTimeZone("UTC");
                    sdf.setTimeZone(zone);
                    String currentdate = sdf.format(new Date());
                    Date current = null;
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        current = format.parse(currentdate);
                        System.out.println(current);
                    } catch (android.net.ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json1 = new JSONArray(responseStr);

                    for (int i = 0; i < json1.length(); i++) {
                        LocationLot item = new LocationLot();
                        JSONObject jsonObject = json1.getJSONObject(i);
                        if (jsonObject.has("id")
                                && !TextUtils.isEmpty(jsonObject.getString("id"))
                                && !jsonObject.getString("id").equals("null")) {
                            item.setId(jsonObject.getInt("id"));
                        }
                        if (jsonObject.has("date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("date_time"))
                                && !jsonObject.getString("date_time").equals("null")) {
                            item.setDate_time(jsonObject.getString("date_time"));
                        }

                        if (jsonObject.has("township_code")
                                && !TextUtils.isEmpty(jsonObject.getString("township_code"))
                                && !jsonObject.getString("township_code").equals("null")) {
                            item.setTownship_code(jsonObject.getString("township_code"));
                        }

                        if (jsonObject.has("location_code")
                                && !TextUtils.isEmpty(jsonObject.getString("location_code"))
                                && !jsonObject.getString("location_code").equals("null")) {
                            item.setLocation_code(jsonObject.getString("location_code"));
                        }

                        if (jsonObject.has("location_name")
                                && !TextUtils.isEmpty(jsonObject.getString("location_name"))
                                && !jsonObject.getString("location_name").equals("null")) {
                            item.setLocation_name(jsonObject.getString("location_name"));
                        }

                        if (jsonObject.has("lot_row")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_row"))
                                && !jsonObject.getString("lot_row").equals("null")) {
                            item.setLot_row(jsonObject.getString("lot_row"));
                        }

                        if (jsonObject.has("lot_number")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_number"))
                                && !jsonObject.getString("lot_number").equals("null")) {
                            item.setLot_number(jsonObject.getString("lot_number"));
                        }
                        if (jsonObject.has("lot_id")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_id"))
                                && !jsonObject.getString("lot_id").equals("null")) {
                            item.setLot_id(jsonObject.getString("lot_id"));
                        }

                        if (jsonObject.has("occupied")
                                && !TextUtils.isEmpty(jsonObject.getString("occupied"))
                                && !jsonObject.getString("occupied").equals("null")) {
                            item.setOccupied(jsonObject.getString("occupied").toLowerCase());
                        }

                        if (jsonObject.has("plate_no")
                                && !TextUtils.isEmpty(jsonObject.getString("plate_no"))
                                && !jsonObject.getString("plate_no").equals("null")) {
                            item.setPlate_no(jsonObject.getString("plate_no"));
                        }
                        if (jsonObject.has("plate_state")
                                && !TextUtils.isEmpty(jsonObject.getString("plate_state"))
                                && !jsonObject.getString("plate_state").equals("null")) {
                            item.setPlate_state(jsonObject.getString("plate_state"));
                        }

                        if (jsonObject.has("entry_date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("entry_date_time"))
                                && !jsonObject.getString("entry_date_time").equals("null")) {
                            item.setEntry_date_time(jsonObject.getString("entry_date_time"));
                        }

                        if (jsonObject.has("exit_date_time")
                                && !TextUtils.isEmpty(jsonObject.getString("exit_date_time"))
                                && !jsonObject.getString("exit_date_time").equals("null")) {
                            item.setExit_date_time(jsonObject.getString("exit_date_time"));
                        }

                        if (jsonObject.has("expiry_time")
                                && !TextUtils.isEmpty(jsonObject.getString("expiry_time"))
                                && !jsonObject.getString("expiry_time").equals("null")) {
                            item.setExpiry_time(jsonObject.getString("expiry_time"));
                        }

                        if (jsonObject.has("parking_type")
                                && !TextUtils.isEmpty(jsonObject.getString("parking_type"))
                                && !jsonObject.getString("parking_type").equals("null")) {
                            item.setParking_type(jsonObject.getString("parking_type"));
                        }

                        if (jsonObject.has("parking_status")
                                && !TextUtils.isEmpty(jsonObject.getString("parking_status"))
                                && !jsonObject.getString("parking_status").equals("null")) {
                            item.setParking_status(jsonObject.getString("parking_status").toLowerCase());
                        }

                        if (jsonObject.has("lot_reservable")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_reservable"))
                                && !jsonObject.getString("lot_reservable").equals("null")) {
                            if (jsonObject.getString("lot_reservable").equals("1")) {
                                item.setLot_reservable(true);
                            } else if (jsonObject.getString("lot_reservable").equals("0")) {
                                item.setLot_reservable(false);
                            } else {
                                item.setLot_reservable(jsonObject.getBoolean("lot_reservable"));
                            }
                        } else {
                            item.setLot_reservable(false);
                        }

                        if (jsonObject.has("lot_reservable_only")
                                && !TextUtils.isEmpty(jsonObject.getString("lot_reservable_only"))
                                && !jsonObject.getString("lot_reservable_only").equals("null")) {
                            if (jsonObject.getString("lot_reservable_only").equals("1")) {
                                item.setLot_reservable_only(true);
                            } else if (jsonObject.getString("lot_reservable_only").equals("0")) {
                                item.setLot_reservable_only(false);
                            } else {
                                item.setLot_reservable_only(jsonObject.getBoolean("lot_reservable_only"));
                            }
                        }
                        if (jsonObject.has("premium_lot")
                                && !TextUtils.isEmpty(jsonObject.getString("premium_lot"))
                                && !jsonObject.getString("premium_lot").equals("null")) {
                            if (jsonObject.getString("premium_lot").equals("1")) {
                                item.setPremium_lot(true);
                            } else if (jsonObject.getString("premium_lot").equals("0")) {
                                item.setPremium_lot(false);
                            } else {
                                item.setPremium_lot(jsonObject.getBoolean("premium_lot"));
                            }
                        }
                        if (jsonObject.has("special_need_handi_lot")
                                && !TextUtils.isEmpty(jsonObject.getString("special_need_handi_lot"))
                                && !jsonObject.getString("special_need_handi_lot").equals("null")) {
                            if (jsonObject.getString("special_need_handi_lot").equals("1")) {
                                item.setSpecial_need_handi_lot(true);
                            } else if (jsonObject.getString("special_need_handi_lot").equals("0")) {
                                item.setSpecial_need_handi_lot(false);
                            } else {
                                item.setSpecial_need_handi_lot(jsonObject.getBoolean("premium_lot"));
                            }
                        }
                        if (jsonObject.has("location_id")
                                && !TextUtils.isEmpty(jsonObject.getString("location_id"))
                                && !jsonObject.getString("location_id").equals("null")) {
                            item.setLocation_id(Integer.parseInt(jsonObject.getString("location_id")));
                        }

                        if (jsonObject.has("is_reserved")
                                && !TextUtils.isEmpty(jsonObject.getString("is_reserved"))
                                && !jsonObject.getString("is_reserved").equals("null")) {
                            if (jsonObject.getString("is_reserved").equals("1")) {
                                item.setIs_reserved(true);
                            } else {
                                item.setIs_reserved(false);
                            }
                        }
                        item.setIs_reserved(false);
                        if (!TextUtils.isEmpty(item.getParking_status())
                                && !TextUtils.isEmpty(item.getOccupied())
                                && item.getParking_status().equals("ENTRY")
                                && item.getOccupied().equals("yes")) {

                            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date exitre = null;
                            try {
                                exitre = format1.parse(item.getExpiry_time());

                            } catch (android.net.ParseException e) {
                                // TODO Auto-generated catch block
                                Log.e("#DEBUG", "  Error:  " + e.getMessage());
                                e.printStackTrace();
                            } catch (java.text.ParseException e) {
                                Log.e("#DEBUG", "  Error:  " + e.getMessage());
                                e.printStackTrace();
                            }

                            if (current.after(exitre)) {
                                item.setExpired(true);
                            } else {
                                item.setExpired(false);
                            }
                        }
                        if (!locationLots.contains(item))
                            if (!item.isLot_reservable()) {
                                locationLots.add(item);
                            }
//                            if (isReserveSpot) {
//                                if (item.isLot_reservable()) {
//                                    locationLots.add(item);
//                                }
//                            } else {
//                                locationLots.add(item);
//                            }
                    }


                } catch (JSONException e) {
                    Log.e("#DEBUG", "  Error:  " + e.getMessage());
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.e("#DEBUG", "  Error:  " + e.getMessage());
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            //progressBar.setVisibility(View.GONE);
            if (getActivity() != null && json1 != null) {
                Collections.sort(locationLots, (o1, o2) -> {
                    try {
                        return Integer.valueOf(o1.getLot_id()) - Integer.valueOf(o2.getLot_id());
                    } catch (Exception e) {
                        return o1.getLot_id().compareTo(o2.getLot_id());
                    }
                });
//                for (int i = 0; i < managedlostpoparray.size(); i++) {
//                    String row_no = managedlostpoparray.get(i).getRow_no();
//                    if (!row_no.equals("")) {
//                        if (isInteger(row_no)) {
//                            int jj = Integer.parseInt(row_no);
//                            String row_name = managedlostpoparray.get(i).getRoe_name();
//                            for (int j = 0; j < data.size(); j++) {
//                                String row_no11 = data.get(j).getRow_no();
//                                if (!row_no11.equals("")) {
//                                    int datarow = Integer.parseInt(row_no11);
//                                    String row_name2 = data.get(j).getRoe_name();
//                                    if (jj == datarow) {
//                                        if (row_name.equals(row_name2)) {
//                                            data.get(j).setMarker(managedlostpoparray.get(i).getMarker());
//                                            data.get(j).setIsparked("t");
//                                        }
//                                    }
//                                }
//                            }
//                        } else {
//                            String row_name = managedlostpoparray.get(i).getRoe_name();
//                            for (int j = 0; j < data.size(); j++) {
//                                String row_no11 = data.get(j).getRow_no();
//                                if (!row_no11.equals("")) {
//                                    String row_name2 = data.get(j).getRoe_name();
//                                    if (row_no.equals(row_no11)) {
//                                        if (row_name.equals(row_name2)) {
//                                            data.get(j).setMarker(managedlostpoparray.get(i).getMarker());
//                                            data.get(j).setIsparked("t");
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//
//                }
//                if (!renew.equals("yes")) {
//
//                    for (int m = 0; m < data.size(); m++) {
//                        if (!data.get(m).getIsparked().equals("t")) {
//
//                            for (int i = 0; i < data.size(); i++) {
//                                data.get(i).setSelected_parking(false);
//                            }
//                            data.get(m).setSelected_parking(true);
//                            for (int k = 0; k < data.size(); k++) {
//                                boolean selected_ore = data.get(k).isSelected_parking();
//
//                                if (selected_ore) {
//                                    String row_no = data.get(k).getRow_no();
//                                    String row_name = data.get(k).getRoe_name();
//
//                                    if (township_sapce_marked.equals("false")) {
//                                        lost_no = row_no;
//                                        txt_row.setText("R : " + lost_no);
//                                    } else if (total_row < 2) {
//                                        lost_row = row_name;
//                                        txt_row.setText("S : " + lost_row);
//                                    } else {
//                                        lost_no = row_no;
//                                        lost_row = row_name;
//                                        txt_row.setText(row_name + " : " + row_no);
//                                    }
//                                    // txt_space.setText("Row "+row_name+" Space # "+row_no);
//                                    /*lost_no = row_no;
//                                    lost_row = row_name;*/
//                                    //Toast.makeText(getActivity(), "Row Name:- "+row_name +" Row No "+row_no, Toast.LENGTH_SHORT).show();
//                                    break;
//                                }
//                            }
//                            break;
//                        }
//                    }
//                }
            }

            super.onPostExecute(s);
        }
    }

    public class getvehiclenumber extends AsyncTask<String, String, String> {
        JSONObject json = null;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "0");
        String vehiclenourl = "_table/user_vehicles?filter=user_id%3D" + user_id + "";

        @Override
        protected void onPreExecute() {
            rl_progressbar1.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            listofvehicle.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            Log.e("get_vehicle_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);
                    Log.e("rerpones", String.valueOf(json));
                    JSONArray array = json.getJSONArray("resource");

                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        String plate_no = c.getString("plate_no");
                        String state = c.getString("registered_state");
                        String id = c.getString("id");
                        Iterator myVeryOwnIterator = statefullname.keySet().iterator();
/*
                        while (myVeryOwnIterator.hasNext()) {
                            String key = (String) myVeryOwnIterator.next();
                            if (key.equals(state)) {
                                String value = (String) statefullname.get(key);
                                item.setState(value);
                                break;
                            }
                        }
*/
//                        for (int k = 0; k < all_state.size(); k++) {
//                            String state_all = all_state.get(k).getState2Code();
//                            if (state_all.equals(state)) {
//                                item.setState(all_state.get(k).getStateName());
//                            }
//                        }
                        item.setState(state);
                        item.setPlateno(plate_no);
                        item.setId(id);
                        item.setVehicle_country(c.getString("vehicle_country"));
                        item.setIsselect(false);
                        item.setCheckboxselected(false);
                        listofvehicle.add(item);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            //progressBar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity == null) {
                    rl_progressbar1.setVisibility(View.GONE);
                } else {
                    ShowState(getActivity());
                }
            } else {
                rl_progressbar1.setVisibility(View.GONE);
            }
            super.onPostExecute(s);
        }
    }

    private void showenter_vehicle(Activity context) {

        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.enter_tvehicle, viewGroup, false);
        final PopupWindow popup = new PopupWindow(context);
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(true);
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        popup.setFocusable(true);
        popup.update();
        popup.showAtLocation(layout, Gravity.TOP, 0, 0);

        final EditText edit_number;
        final Button btn_park_now;
        RelativeLayout btn_cancel;

        edit_number = (EditText) layout.findViewById(R.id.edit_nu);
        edit_number.setFocusable(true);
        btn_cancel = (RelativeLayout) layout.findViewById(R.id.close);
        btn_park_now = (Button) layout.findViewById(R.id.btn_done);
        btn_park_now.setAlpha((float) 0.3);
        btn_park_now.setEnabled(false);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        btn_park_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edit_number.getWindowToken(), 0);
                String hgv = edit_number.getText().toString();
                editno.setText(hgv);
                popup.dismiss();
                show_marke_payment_button();
                ShowStatePupup(getActivity(), state_array);

            }
        });

        edit_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String ss = s.toString();
                if (!ss.equals(ss.toUpperCase())) {
                    edit_number.setText(ss.toUpperCase());
                    edit_number.setSelection(ss.length());
                }

                if (s.length() > 0) {
                    btn_park_now.setAlpha((float) 1.0);
                    btn_park_now.setEnabled(true);
                } else {
                    btn_park_now.setAlpha((float) 0.3);
                    btn_park_now.setEnabled(false);
                }
            }
        });


        edit_number.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String hgv = edit_number.getText().toString();
                    editno.setText(hgv);
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edit_number.getWindowToken(), 0);
                    ShowStatePupup(getActivity(), state_array);
                    return true;
                }
                return false;
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edit_number.getWindowToken(), 0);
                String hgv = edit_number.getText().toString();
                editno.setText(hgv);
                show_marke_payment_button();
                popup.dismiss();
            }
        });


    }

    private void ShowState(Activity context) {
        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        layout = layoutInflater.inflate(R.layout.parkherepopup, null);
        if (popup_state != null && popup_state.isShowing()) {
            popup_state.dismiss();
            popup_state = null;
        }
        popup_state = new PopupWindow(context);
        popup_state.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup_state.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup_state.setContentView(layout);
        popup_state.setFocusable(false);
        popup_state.setAnimationStyle(R.style.animationName);
        popup_state.setBackgroundDrawable(null);
        listofvehicleno = (ListView) layout.findViewById(R.id.listselectvehicl);
        TextView txt_add_vi = (TextView) layout.findViewById(R.id.txtaddvehicle);
        txt_add_vi.setVisibility(View.VISIBLE);
        Resources rs;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int yyy = rl_main.getHeight();
            int yyy1 = rl_title.getHeight();
            int final_position = yyy1 + yyy + 245;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                popup_state.showAtLocation(layout, Gravity.NO_GRAVITY, 0, final_position);
            } else {
                popup_state.showAsDropDown(rl_main, 0, 0);
            }


        } else {
            popup_state.showAsDropDown(rl_main, 0, 0);
        }
        if (listofvehicle.size() > 0) {
            Activity activty = getActivity();
            if (activty == null) {
                if (popup_state != null && popup_state.isShowing()) {
                    popup_state.dismiss();
                    popup_state = null;
                }
            } else {
                CustomAdapter_state adpater = new CustomAdapter_state(getActivity(), listofvehicle, rs = getResources());
                listofvehicleno.setAdapter(adpater);
            }
        }

        txt_add_vi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                is_add_back = true;
                SharedPreferences add1 = getActivity().getSharedPreferences("addvehicle", Context.MODE_PRIVATE);
                SharedPreferences.Editor ed = add1.edit();
                ed.putString("addvehicle", "yes");
                ed.commit();
                Bundle b = new Bundle();
                b.putString("direct_add", "yes");
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                vehicle_add pay = new vehicle_add();
                pay.registerForListener(paidparkhere.this);
                pay.setArguments(b);
                ft.add(R.id.My_Container_1_ID, pay, "my_vehicle_add");
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
                if (popup_state != null && popup_state.isShowing()) {
                    popup_state.dismiss();
                    popup_state = null;
                }
            }
        });

        RelativeLayout imageclose;
        imageclose = (RelativeLayout) layout.findViewById(R.id.close);
        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    animation = AnimationUtils.loadAnimation(getActivity(), R.anim.popup_off);
                    layout.startAnimation(animation);
                    if (popup_state != null && popup_state.isShowing()) {
                        popup_state.dismiss();
                        popup_state = null;
                    }
                } catch (Exception e) {
                }
            }
        });
        rl_progressbar1.setVisibility(View.GONE);
    }

    public class CustomAdapter_state extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdapter_state(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txtplateno, txtstate;
            public RelativeLayout rl_main;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {
                vi = inflater.inflate(R.layout.selectvehicle, null);
                holder = new ViewHolder();

                holder.txtplateno = (TextView) vi.findViewById(R.id.txtlicenseplate);
                holder.txtstate = (TextView) vi.findViewById(R.id.txtstate);
                holder.rl_main = (RelativeLayout) vi.findViewById(R.id.rl_main);
                vi.setTag(holder);
                vi.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                String platno = holder.txtplateno.getText().toString().toUpperCase();
                                String statename11 = holder.txtstate.getText().toString();
                                plateState = holder.txtstate.getText().toString().toUpperCase();
                                editno.setText(platno.toUpperCase());
                                editno.setVisibility(View.VISIBLE);
                                // txt_plate_no.setText(platno.toUpperCase());
                                Iterator myVeryOwnIterator = statefullname.keySet().iterator();
/*
                                while(myVeryOwnIterator.hasNext())
                                {
                                    String key = (String) myVeryOwnIterator.next();
                                    String value = (String) statefullname.get(key);
                                    if (value.equals(statename11))
                                    {
                                        for (int i = 1; i < statearray.length; i++) {
                                            String statename1 = statearray[i].toString();
                                            item ii = new item();
                                            ii.setState(statearray[i].toString());
                                            if (statename1.equals(key)) {
                                                ii.setSelected_parking(true);
                                                statename=statename1;
                                                txt_state.setText(statename);
                                            } else {
                                                ii.setSelected_parking(false);
                                            }
                                            state_array.add(ii);

                                        }
                                        popup_state.dismiss();
                                        ShowHours(getActivity(),stateq);
                                       // ShowStatePupup(getActivity(),state_array);

                                    }
                                }
*/
                                state_array.clear();
                                for (int k = 0; k < all_state.size(); k++) {
                                    String state_all = all_state.get(k).getStateName();
                                    if (state_all.equals(statename11)) {
                                        String state_code = all_state.get(k).getState2Code();
                                        for (int i = 1; i < all_state.size(); i++) {
                                            String statename1 = all_state.get(i).getState2Code();
                                            item ii = new item();
                                            ii.setState(all_state.get(i).getState2Code());
                                            if (statename1.equals(state_code)) {
                                                ii.setSelected_parking(true);
                                                statename = statename1;
                                                txt_state.setText(statename);
                                            } else {
                                                ii.setSelected_parking(false);
                                            }
                                            state_array.add(ii);

                                        }

                                        popup_state.dismiss();
                                        ShowHours(getActivity(), stateq);
                                    }
                                }
                                txt_state.setText(holder.txtstate.getText().toString().toUpperCase());
                                plateState = holder.txtstate.getText().toString().toUpperCase();
                                isStateSelected = true;
                                ShowHours(getActivity(), stateq);
                                show_marke_payment_button();
                            }
                        });
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }
            if (data.size() <= 0) {
                holder.txtplateno.setText("No Data");
            } else {
                /***** Get each Model object from Arraylist ********/
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                /************  Set Model values in Holder elements ***********/
                String cat = tempValues.getPlantno();
                if (cat == null) {
                    holder.txtplateno.setVisibility(View.GONE);
                } else if (cat != null) {
                    Typeface light = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Light.otf");
                    holder.txtplateno.setTypeface(light);
                    holder.txtstate.setTypeface(light);
                    holder.txtplateno.setText(tempValues.getPlantno());
                    holder.txtstate.setText(tempValues.getState());
                    if (!rate.equals("") && !rate1.equals("")) {
                        Double du = Double.parseDouble(rate1) / 60.0;
                        txtrate.setText(String.format("Rate: $%s@%s%s", rate, rate1, duration_unit));
                        if (!rate1.equals("1")) {
                            if (duration_unit.equals("Hour")) {
                                txtrate.setText(String.format("Rate: $%s@%s Hours", rate, rate1));
                            } else if (duration_unit.equals("Minute")) {
                                txtrate.setText(String.format("Rate: $%s@%s Minutes", rate, rate1));
                            } else if (duration_unit.equals("Day")) {
                                txtrate.setText(String.format("Rate: $%s@%s Days", rate, rate1));
                            } else if (duration_unit.equals("Week")) {
                                txtrate.setText(String.format("Rate: $%s@%s Weeks", rate, rate1));
                            } else if (duration_unit.equals("Month")) {
                                txtrate.setText(String.format("Rate: $%s@%s Months", rate, rate1));
                            }
                        } else {
                            txtrate.setText(String.format("Rate: $%s@%s%s", rate, rate1, duration_unit));
                        }
                    }

                    if (position % 2 == 0) {
                        holder.rl_main.setBackgroundColor(Color.parseColor("#ffffff"));
                    } else {
                        holder.rl_main.setBackgroundColor(Color.parseColor("#e0e0e0"));
                    }
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, final Bundle resultData) {
            if (resultCode == Constants.SUCCESS_RESULT) {
                final Address address = resultData.getParcelable(Constants.RESULT_ADDRESS);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /*infoText.setVisibility(View.VISIBLE);

                        infoText.setText("Latitude: " + address.getLatitude() + "\n" +
                                "Longitude: " + address.getLongitude() + "\n" +
                                "Address: " + resultData.getString(Constants.RESULT_DATA_KEY));*/

                        lat = String.valueOf(address.getLatitude());
                        lang = String.valueOf(address.getLongitude());
                        String statename_ = address.getAdminArea();
                        Iterator myVeryOwnIterator = statefullname.keySet().iterator();

                    /* while (myVeryOwnIterator.hasNext()) {
                            String   key = (String) myVeryOwnIterator.next();
                            String value = (String) statefullname.get(key);
                            if (value.equals(statename_)) {
                                for (int i = 0; i <= statearray.length; i++) {
                                    String state = statearray[i];
                                    if (state.equals(key)) {

                                        //txt_state.setText(state);
                                        //txt_state.setEnabled(false);
                                        break;
                                    }
                                }
                            }
                        }*/

                        state_array.clear();
                        for (int i = 1; i < all_state.size(); i++) {
                            String statename_1 = all_state.get(i).getState2Code();
                            String state_ = txt_state.getText().toString();
                            item ii = new item();
                            ii.setState(statename_1);
                            if (state_.equals("State") || state_.equals("")) {
                                if (statename_1.equals("NY")) {
                                    ii.setSelected_parking(true);
                                    // txt_state.setText("NY");
                                    //statename="NY";
                                } else {
                                    ii.setSelected_parking(false);
                                }
                            } else {
                                if (statename_1.equals(state_)) {
                                    ii.setSelected_parking(true);
                                    //statename=state_;
                                } else {
                                    ii.setSelected_parking(false);
                                }
                            }
                            state_array.add(ii);
                        }

                        show_marke_payment_button();
                        // address_=resultData.getString(Constants.RESULT_DATA_KEY);

                    }
                });
            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rl_progressbar.setVisibility(View.GONE);
                       /* infoText.setVisibility(View.VISIBLE);
                        infoText.setText(resultData.getString(Constants.RESULT_DATA_KEY));*/
                    }
                });
            }
        }
    }


    public void show_marke_payment_button() {
        platno = editno.getText().toString();
        if (!platno.equals("") && !statename.equals("State") && !shoues.equals("hours") && !shoues.equals("")) {
            if (rl_sp_row.getVisibility() == View.VISIBLE) {
//                txtmakpayment.setVisibility(View.VISIBLE);
                llParkOptions.setVisibility(View.VISIBLE);
               /* InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);*/
            } else {
                llParkOptions.setVisibility(View.VISIBLE);

                /*InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);*/
            }
        } else {
            llParkOptions.setVisibility(View.GONE);
        }
        if (popup_state != null && popup_state.isShowing()) {
            popup_state.dismiss();
            popup_state = null;
        }
    }

    public void open_timer() {
        SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = time.edit();
        ed.putString("result", "true");
        ed.commit();
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.replace(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
        ft.commit();
    }


    public void direct_open_timer(final String park_id) {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String login_id = logindeatl.getString("id", "0");
        if (!login_id.equals("0")) {

            if (!park_id.equals("null")) {
                            /*final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.replace(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                            ft.commitAllowingStateLoss();*/
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                timervehicleinfo pay = new timervehicleinfo();
                fragmentStack.lastElement().onPause();
                ft.add(R.id.My_Container_1_ID, pay, "timer");
                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            } else {
                SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
                SharedPreferences.Editor ed = time.edit();
                ed.putString("result", "true");
                ed.commit();
                           /* final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.add(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                            ft.commitAllowingStateLoss();*/
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                timervehicleinfo pay = new timervehicleinfo();
                fragmentStack.lastElement().onPause();
                ft.add(R.id.My_Container_1_ID, pay, "timer");
                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();

            }
        } else {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Parking Notifications");
            alertDialog.setMessage("Do you want to register for parking notifications");
            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ((vchome) getActivity()).open_login_screen();

                }
            });
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (!park_id.equals("null")) {
                                   /* final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                    ft.replace(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                                    ft.commitAllowingStateLoss();*/

                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        timervehicleinfo pay = new timervehicleinfo();
                        fragmentStack.lastElement().onPause();
                        ft.add(R.id.My_Container_1_ID, pay, "timer");
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                    } else {
                        SharedPreferences time = getActivity().getSharedPreferences("timershow", Context.MODE_PRIVATE);
                        SharedPreferences.Editor ed = time.edit();
                        ed.putString("result", "true");
                        ed.commit();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        timervehicleinfo pay = new timervehicleinfo();
                        fragmentStack.lastElement().onPause();
                        ft.add(R.id.My_Container_1_ID, pay, "timer");
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.lastElement().onPause();
                        ft.remove(fragmentStack.pop());
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                                     /*  final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                    ft.add(R.id.My_Container_1_ID, new timervehicleinfo(), "NewFragmentTag");
                                    ft.commitAllowingStateLoss();*/

                    }
                }
            });
            alertDialog.show();
        }

    }

    public void myOnKeyDown(int keyCode) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (popup_state != null && popup_state.isShowing()) {
                popup_state.dismiss();
            }
        }
    }

    public class reserveNow extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String rid = logindeatl.getString("id", "null");
        String id1 = "null";
        JSONObject json, json1;
        String pov2 = "_table/reserved_parkings";
        //        String pov2 = "_table/sdafga";
        boolean isPayLater = false;
        String paramOption = "";
        private String dueBy = "";
        SharedPreferences logindeatl1 = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl1.getString("id", "null");
        String pl_state = "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            pl_state = txt_state.getText().toString();

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//            TimeZone zonen = TimeZone.getTimeZone("UTC");
//            sdf.setTimeZone(zonen);

            if (!isPrePaid) {
                if (!TextUtils.isEmpty(reservationPostPayTerms)) {
                    switch (reservationPostPayTerms) {
                        case "ENTRY":
                            dueBy = reserveEntryTime;
                            break;
                        case "EXIT":
                            dueBy = reserveExpiryTime;
                            break;
                        case "1 Day":
                            calendar = Calendar.getInstance();
                            calendar.add(Calendar.DAY_OF_MONTH, 1);
                            dueBy = sdf.format(new Date(calendar.getTimeInMillis()));
                            break;
                        case "7 Days":
                            calendar = Calendar.getInstance();
                            calendar.add(Calendar.DAY_OF_MONTH, 7);
                            dueBy = sdf.format(new Date(calendar.getTimeInMillis()));
                            break;
                        case "15 Days":
                            calendar = Calendar.getInstance();
                            calendar.add(Calendar.DAY_OF_MONTH, 15);
                            dueBy = sdf.format(new Date(calendar.getTimeInMillis()));
                            break;
                        case "30 Days":
                            calendar = Calendar.getInstance();
                            calendar.add(Calendar.DAY_OF_MONTH, 30);
                            dueBy = sdf.format(new Date(calendar.getTimeInMillis()));
                            break;
                    }
                }
            }

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            if (params.length != 0) {
                paramOption = params[0];
            }
            isPayLater = !TextUtils.isEmpty(paramOption) && paramOption.equals("payLater");
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            TimeZone zone = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(zone);
            currentdate = sdf.format(new Date());
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//            TimeZone zone12 = TimeZone.getTimeZone("UTC");
//            sdf.setTimeZone(zone12);
            final String currentDateandTime = currentdate;
            Date date11 = null;
            try {
                date11 = sdf.parse(currentDateandTime);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date11);
            if (min.equals("Hrs") || min.equals("Hr")) {
                calendar.add(Calendar.HOUR, hours);
            } else if (min.equals("Months") || min.equals("Month")) {
                calendar.add(Calendar.MONTH, hours);
                calendar.add(Calendar.DATE, -1);
            } else if (min.equals("Days") || min.equals("Day")) {
                calendar.add(Calendar.DATE, hours);
            } else if (min.equals("Weeks") || min.equals("Week")) {
                calendar.add(Calendar.DAY_OF_WEEK_IN_MONTH, hours);
            } else {
                calendar.add(Calendar.MINUTE, hours);
            }

            String expridate = sdf.format(calendar.getTime());
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
            SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//            outputFormat.setTimeZone(zone12);
//            inputFormat.setTimeZone(zone12);

            Date date = null;
            Date Current = null;
            expiretime = null;
            String final_current_date = null;
            try {
                date = inputFormat.parse(expridate);
                Current = inputFormat.parse(currentdate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            expiretime = outputFormat.format(date);
            final_current_date = outputFormat.format(Current);
            int h = 0;
            if (user_id.equals("null")) {
                h = 0;
            } else {
                h = Integer.parseInt(user_id);
            }
            parkingtype = marker_type;
            String plate = platno;

            jsonValues.put("CanCancel_Reservation", mIsCanCancelReservation);
            jsonValues.put("Cancellation_Charge", mCancelationCharges);
            jsonValues.put("Reservation_PostPayment_term", reservationPostPayTerms);
            jsonValues.put("Reservation_PostPayment_LateFee", mReserveNowPostPayLateFees);

            jsonValues.put("reserve_entry_time", reserveEntryTime);
            jsonValues.put("reserve_expiry_time", reserveExpiryTime);
            jsonValues.put("reserve_exit_date_time", reserveExpiryTime);
            jsonValues.put("reservation_date_time", reserveCurrentTime);

            Log.e("#DEBUG", "   reserveEntryTime:  " + reserveEntryTime
                    + "   reserveExpiryTime:  " + reserveExpiryTime
                    + "   reserveCurrentTime:  " + reserveCurrentTime
                    + "    dueBy:  " + dueBy);

            Log.e("#DEBUG", "    reservationPostPayTerms:  " + reservationPostPayTerms);

            jsonValues.put("company_type_id", "");
            jsonValues.put("company_type", "");
            jsonValues.put("manager_type", "");
            try {
                if (!TextUtils.isEmpty(companyId)) {
                    jsonValues.put("company_id", Integer.valueOf(companyId));
                } else {
                    jsonValues.put("company_id", "");
                }

                if (!TextUtils.isEmpty(twpId)) {
                    jsonValues.put("twp_id", Integer.valueOf(twpId));
                } else {
                    jsonValues.put("twp_id", "");
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("#DEBUG", "   reserveNow:  Error:  " + e.getMessage());
            }

            if (mIsPayNow) {
                jsonValues.put("reserve_payment_condition", "PRE-PYMT");
                jsonValues.put("reserve_payment_due_by", reserveCurrentTime);
                jsonValues.put("reserve_payment_paid", "PAID");
            } else {
                jsonValues.put("reserve_payment_condition", reservationPostPayTerms);
                jsonValues.put("reserve_payment_due_by", dueBy);
                jsonValues.put("reserve_payment_paid", "UNPAID");
            }

            jsonValues.put("reservation_status", "ACTIVE");

//            jsonValues.put("entry_date_time", "");
//            jsonValues.put("exit_date_time", "");
//            jsonValues.put("expiry_time", "");
            jsonValues.put("max_duration", max_time);
            jsonValues.put("payment_method", paymentmethod);
            jsonValues.put("payment_choice", paymentmethod);
            jsonValues.put("parking_rate", parkingRate);

            jsonValues.put("parking_subtotal", mParkingSubTotal);
            jsonValues.put("tr_percent", mTrPercent);
            jsonValues.put("tr_fee", mTrFee);
            jsonValues.put("parking_total", mParkingTotal);
            jsonValues.put("wallet_trx_id", wallet_id);

            if (paymentmethod.equals("Paypal")) {
                jsonValues.put("ipn_txn_id", transection_id);
                jsonValues.put("ipn_payment", "Paypal");
                jsonValues.put("ipn_status", "success");
                jsonValues.put("ipn_address", "");
            } else {
                jsonValues.put("ipn_txn_id", "");
                jsonValues.put("ipn_payment", "");
                jsonValues.put("ipn_status", "");
                jsonValues.put("ipn_address", "");
            }

            plate = plate.replaceAll("\\s+", "");
            jsonValues.put("parking_type", parkingtype);
            jsonValues.put("township_code", township_code1);
            jsonValues.put("manager_type_id", manager_type_id);
            jsonValues.put("location_code", location_code);
            jsonValues.put("location_name", locationname);

            jsonValues.put("user_id", h);
            jsonValues.put("permit_id", 0);
            jsonValues.put("subscription_id", 0);
            jsonValues.put("plate_no", plate);
            jsonValues.put("pl_state", pl_state);
            jsonValues.put("lat", latitude);
            jsonValues.put("lng", longitude);
            jsonValues.put("address1", adrreslocation);
            jsonValues.put("address2", "");
            if (!TextUtils.isEmpty(mCity)) {
                jsonValues.put("city", mCity);
            } else {
                jsonValues.put("city", locationname + lost_row1 + lost_no1);
            }
            if (!TextUtils.isEmpty(mStateCode)) {
                jsonValues.put("state", mStateCode);
            } else {
                jsonValues.put("state", statename);
            }
            if (!TextUtils.isEmpty(mZipCode)) {
                jsonValues.put("zip", mZipCode);
            } else jsonValues.put("zip", zip_code);
            if (!TextUtils.isEmpty(mCountryCode)) {
                jsonValues.put("country", mCountryCode);
            } else jsonValues.put("country", "USA");
//            jsonValues.put("lot_row", lost_row1);
//            jsonValues.put("lot_number", lost_no1);
//            jsonValues.put("lot_id", lost_row1 + ":" + lost_no1);
            if (selectedLocationLot != null) {
                if (!TextUtils.isEmpty(selectedLocationLot.getLot_id())) {
                    jsonValues.put("lot_id", selectedLocationLot.getLot_id());
                }
                if (!TextUtils.isEmpty(selectedLocationLot.getLot_number())) {
                    jsonValues.put("lot_number", selectedLocationLot.getLot_number());
                }
                if (!TextUtils.isEmpty(selectedLocationLot.getLot_row())) {
                    jsonValues.put("lot_row", selectedLocationLot.getLot_row());
                }
                jsonValues.put("location_lot_id", selectedLocationLot.getId());
            }
            jsonValues.put("ip", getLocalIpAddress());
            jsonValues.put("parking_token", token);
            jsonValues.put("parking_status", "RESERVED");
            jsonValues.put("platform", "Android");

            jsonValues.put("parking_units", String.valueOf(parkingUnits));
            jsonValues.put("parking_qty", parkingQty);

            jsonValues.put("ipn_custom", "");

            jsonValues.put("location_id", location_id);

            String mismatch = "0";
            float dis = getdistances(locationaddress, adrreslocation);
            float ff = dis / 1609;
            if (ff > 0.5) {
                mismatch = "1";
            }
            jsonValues.put("distance_to_marker", getdistances(locationaddress, adrreslocation));
            jsonValues.put("marker_address1", locationaddress);
            jsonValues.put("marker_address2", mMarkerAddress2);
            jsonValues.put("marker_city", mMarkerCity);
            jsonValues.put("marker_district", mMarkerDistrict);
            jsonValues.put("marker_state", mMarkerStateCode);
            jsonValues.put("marker_zip", mMarkerZipCode);
            jsonValues.put("marker_country", mMarkerCountryCode);
            jsonValues.put("token", 0);
            jsonValues.put("mismatch", mismatch);
            jsonValues.put("marker_lng", lang);
            jsonValues.put("marker_lat", lat);
            jsonValues.put("ticket_status", "");
            jsonValues.put("date_time", "");
            jsonValues.put("duration_unit", min);
            jsonValues.put("unit_pricing", rate);
            if (renew.equals("yes")) {
                jsonValues.put("selected_duration", final_count);
            } else {
                jsonValues.put("selected_duration", hours);
            }
            jsonValues.put("pl_country", p_country);
            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + pov2;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "    reserveNow:  Response:  " + responseStr);
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id1 = c.getString("id");

                    }
                } catch (IOException e) {
                    Log.e("#DEBUG", "    reserveNow:  Error:  " + e.getMessage());
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.e("#DEBUG", "    reserveNow:  Error:  " + e.getMessage());
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            //progressBar.setVisibility(View.GONE);
            if (getActivity() != null && json1 != null) {
                if (!id1.equals("null")) {

                    if (rl_sp_row.getVisibility() != View.VISIBLE) {
//                        new getparked_car(id1).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new updateLocationLot(id1).execute();
                    }

                    Toast.makeText(getActivity(), "Parking Reserved Successfully!", Toast.LENGTH_SHORT).show();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Bundle bundle = new Bundle();
                    bundle.putString("reservationId", id1);
                    ReservationParkingDetailsFragment pay = new ReservationParkingDetailsFragment();
                    pay.setArguments(bundle);
                    fragmentStack.lastElement().onPause();
                    ft.add(R.id.My_Container_1_ID, pay, "home");
                    fragmentStack.lastElement().onPause();
                    ft.remove(fragmentStack.pop());
                    fragmentStack.lastElement().onPause();
                    ft.remove(fragmentStack.pop());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                    sendNotificationReservationConfirm(id1);

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    final String currentDateandTime = reserveEntryTime;
                    Date date11 = null;
                    try {
                        date11 = sdf.parse(currentDateandTime);
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    final Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date11);
                    calendar.add(Calendar.MINUTE, -5);
                    scheduleNotification(getNotification("Your reservation starts in 5 Min", 0),
                            calendar.getTimeInMillis(), 0);

                    if (!mIsPayNow) {
                        String dueDate = dueBy;
                        try {
                            date11 = sdf.parse(dueDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        calendar.setTime(date11);
                        calendar.add(Calendar.MINUTE, -5);
                        scheduleNotification(getNotification("Your reservation payment due in 5 Min", 0),
                                calendar.getTimeInMillis(), 0);
                    }

                }
            }
            super.onPostExecute(s);
        }
    }

    private void sendNotificationReservationConfirm(String id1) {
        int notificationId = (int) Calendar.getInstance().getTimeInMillis();
        if (!TextUtils.isEmpty(id1)) {
            try {
                notificationId = Integer.parseInt(id1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Intent intent = new Intent(getActivity(), Splashscreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 0, intent, 0);

        NotificationChannel channel = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            channel = new NotificationChannel("reservation", "reservation", importance);
            channel.setDescription("Notification for reservation");
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getActivity(), "reservation")
                .setSmallIcon(R.mipmap.icon)
                .setContentTitle("ParkEzly")
                .setContentText("Thank you, Your reservation is confirmed!")
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getActivity());
        notificationManager.notify(notificationId, builder.build());
        if (channel != null) {
            NotificationManager notificationManager1 = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                notificationManager1 = getActivity().getSystemService(NotificationManager.class);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationManager1.createNotificationChannel(channel);
            }
            notificationManager1.notify(notificationId, builder.build());
        }

    }

    private void scheduleNotification(Notification notification, long delay, int postion) {

        Intent notificationIntent = new Intent(getActivity(), NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, postion);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), postion, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        GregorianCalendar hbhbh = new GregorianCalendar();
        hbhbh.setTimeInMillis(delay);
        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, hbhbh.getTimeInMillis(), pendingIntent);
    }

    private Notification getNotification(String content, int postion) {
        long[] vibrate = {0, 100, 200, 300};
        String strtitle = getActivity().getString(R.string.customnotificationtitle);
        // Set Notification Text
        String strtext = "Alert Parkezly!";
        Intent intent = new Intent(getActivity().getApplicationContext(), vchome.class);
        intent.putExtra("title", strtitle);
        intent.putExtra("text", strtext);
        intent.putExtra("id", id);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pIntent = PendingIntent.getActivity(getActivity().getApplicationContext(), postion, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        androidx.core.app.NotificationCompat.Builder builder = new NotificationCompat.Builder(getActivity())
                .setSmallIcon(R.mipmap.icon)
                .setTicker(getActivity().getString(R.string.customnotificationticker))
                .setVibrate(vibrate)
                .setContentIntent(pIntent)
                .setContentTitle(strtitle)
                .setContentText(content)
                .setAutoCancel(true);
        return builder.build();
    }

    public class fetchLocationLots extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String locationLotsUrl = "_table/location_lot";
        JSONObject json;
        String re_id;
        String id;
        String locationId;

        public fetchLocationLots(String locationId) {
            this.locationId = locationId;
        }

        @Override
        protected void onPreExecute() {
            locationLots.clear();
            rl_progressbar.setVisibility(View.VISIBLE);
            try {
                this.locationLotsUrl = "_table/location_lot?filter=location_id%3D" + URLEncoder.encode(String.valueOf(locationId), "utf-8") + "";
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("#DEBUG", "   fetchLocationLot:  Error:  " + e.getMessage());
            }
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            locationLots.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + locationLotsUrl;
            Log.e("#DEBUG", "  fetchLocationLots:  URL:  " + url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        LocationLot locationLot = new LocationLot();
                        locationLot.setId(object.getInt("id"));

                        if (object.has("date_time")
                                && !TextUtils.isEmpty(object.getString("date_time"))
                                && !object.getString("date_time").equals("null")) {
                            locationLot.setDate_time(object.getString("date_time"));
                        }

                        if (object.has("township_code")
                                && !TextUtils.isEmpty(object.getString("township_code"))
                                && !object.getString("township_code").equals("null")) {
                            locationLot.setTownship_code(object.getString("township_code"));
                        }

                        if (object.has("location_id")
                                && !TextUtils.isEmpty(object.getString("location_id"))
                                && !object.getString("location_id").equals("null")) {
                            locationLot.setLocation_id(Integer.parseInt(object.getString("location_id")));
                        }

                        if (object.has("date_time")
                                && !TextUtils.isEmpty(object.getString("date_time"))
                                && !object.getString("date_time").equals("null")) {
                            locationLot.setDate_time(object.getString("date_time"));
                        }

                        if (object.has("location_code")
                                && !TextUtils.isEmpty(object.getString("location_code"))
                                && !object.getString("location_code").equals("null")) {
                            locationLot.setLocation_code(object.getString("location_code"));
                        }

                        if (object.has("location_name")
                                && !TextUtils.isEmpty(object.getString("location_name"))
                                && !object.getString("location_name").equals("null")) {
                            locationLot.setLocation_name(object.getString("location_name"));
                        }

                        if (object.has("lot_row")
                                && !TextUtils.isEmpty(object.getString("lot_row"))
                                && !object.getString("lot_row").equals("null")) {
                            locationLot.setLot_row(object.getString("lot_row"));
                        }

                        if (object.has("lot_number")
                                && !TextUtils.isEmpty(object.getString("lot_number"))
                                && !object.getString("lot_number").equals("null")) {
                            locationLot.setLot_number(object.getString("lot_number"));
                        }

                        if (object.has("occupied")
                                && !TextUtils.isEmpty(object.getString("occupied"))
                                && !object.getString("occupied").equals("null")) {
                            locationLot.setOccupied(object.getString("occupied"));
                        }

                        if (object.has("plate_no")
                                && !TextUtils.isEmpty(object.getString("plate_no"))
                                && !object.getString("plate_no").equals("null")) {
                            locationLot.setPlate_no(object.getString("plate_no"));
                        }

                        if (object.has("plate_state")
                                && !TextUtils.isEmpty(object.getString("plate_state"))
                                && !object.getString("plate_state").equals("null")) {
                            locationLot.setPlate_state(object.getString("plate_state"));
                        }

                        if (object.has("lot_id")
                                && !TextUtils.isEmpty(object.getString("lot_id"))
                                && !object.getString("lot_id").equals("null")) {
                            locationLot.setLot_id(object.getString("lot_id"));
                        }

                        if (object.has("lot_reservable")
                                && !TextUtils.isEmpty(object.getString("lot_reservable"))
                                && !object.getString("lot_reservable").equals("null")) {
                            locationLot.setLot_reservable(object.getBoolean("lot_reservable"));
                        }

                        if (object.has("lot_reservable_only")
                                && !TextUtils.isEmpty(object.getString("lot_reservable_only"))
                                && !object.getString("lot_reservable_only").equals("null")) {
                            locationLot.setLot_reservable_only(object.getBoolean("lot_reservable_only"));
                        }

                        if (object.has("company_type_id")
                                && !TextUtils.isEmpty(object.getString("company_type_id"))
                                && !object.getString("company_type_id").equals("null")) {
                            locationLot.setCompany_type_id(object.getString("company_type_id"));
                        }

                        if (object.has("manager_type_id")
                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
                                && !object.getString("manager_type_id").equals("null")) {
                            locationLot.setManager_type_id(object.getString("manager_type_id"));
                        }

                        if (object.has("premium_lot")
                                && !TextUtils.isEmpty(object.getString("premium_lot"))
                                && !object.getString("premium_lot").equals("null")) {
                            locationLot.setPremium_lot(object.getBoolean("premium_lot"));
                        }

                        if (object.has("company_id")
                                && !TextUtils.isEmpty(object.getString("company_id"))
                                && !object.getString("company_id").equals("null")) {
                            locationLot.setCompany_id(object.getString("company_id"));
                        }

                        locationLot.setIsparked("t");
                        locationLot.setMarker("red");

                        locationLots.add(locationLot);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   fetchLocationLots:  Error:  " + e.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   fetchLocationLots:  Error:  " + e.getMessage());
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);

            if (getActivity() != null) {
                if (locationLots.size() != 0) {
                    //TODO display lots
//                    showDialogLocationLots();
                }
                super.onPostExecute(s);
            }
        }
    }

    private void showDialogLocationLots() {
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        View dialogView = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_select_locatoin_lot, null);
        builder.setView(dialogView);
        dialogLocationLot = builder.create();
        dialogLocationLot.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        RelativeLayout rlClose = dialogView.findViewById(R.id.rlClose);
        RelativeLayout rlDone = dialogView.findViewById(R.id.rlDone);
        RelativeLayout rlSelectedLot = dialogView.findViewById(R.id.rlSelectedLot);
        RecyclerView rvLocationLot = dialogView.findViewById(R.id.rvLocationLot);
        TextView tvSelectedLot = dialogView.findViewById(R.id.tvSelectedLot);
        TextView tvLocationTitle = dialogView.findViewById(R.id.tvLocationTitle);
        tvLocationTitle.setText(locationname);

        if (selectedLocationLot != null) {
            rlSelectedLot.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(selectedLocationLot.getLot_id())) {
                tvSelectedLot.setText(selectedLocationLot.getLot_id());
            }
        } else {
            rlSelectedLot.setVisibility(View.GONE);
        }

        LocationLotAdapter locationLotAdapter = new LocationLotAdapter();
        rvLocationLot.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        rvLocationLot.setAdapter(locationLotAdapter);

        rlDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLocationLot.dismiss();
                if (isReserveDateSelected) {
                    showPaymentOptionReservation();
                }
            }
        });

        rlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLocationLot.dismiss();
            }
        });

        dialogLocationLot.show();

        updateManagedLocations(locationLotAdapter);
    }

    private class LocationLotAdapter extends RecyclerView.Adapter<LocationLotAdapter.LocationLotHolder> {
        private Drawable drawableRed, drawableBlue, drawableGreen, drawableGrey;

        @NonNull
        @Override
        public LocationLotAdapter.LocationLotHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            drawableRed = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_red);
            drawableBlue = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_blue);
            drawableGreen = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_car_green);
            drawableGrey = ContextCompat.getDrawable(getActivity(), R.mipmap.popup_gree_car);
            return new LocationLotHolder(LayoutInflater.from(getActivity())
                    .inflate(R.layout.item_location_lot, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull LocationLotAdapter.LocationLotHolder locationLotHolder, int i) {
            LocationLot locationLot = locationLots.get(i);
            locationLotHolder.tvReserved.setVisibility(View.GONE);
            locationLotHolder.ivReserved.setVisibility(View.GONE);
            locationLotHolder.ivReservedOnly.setVisibility(View.GONE);
            locationLotHolder.ivPremium.setVisibility(View.GONE);
            locationLotHolder.ivHandicapped.setVisibility(View.GONE);
            if (locationLot != null) {
                if (!TextUtils.isEmpty(locationLot.getLot_id())) {
                    locationLotHolder.tvRow.setText(locationLot.getLot_id());
                }

                if (!TextUtils.isEmpty(locationLot.getOccupied())) {
                    if (locationLot.getOccupied().equalsIgnoreCase("yes")) {
                        //Car already parked at this location
                        if (locationLot.isExpired()) {
                            //RED, parking expired
                            locationLotHolder.ivCar.setImageDrawable(drawableRed);
                        } else {
                            //GREEN, parking valid
                            locationLotHolder.ivCar.setImageDrawable(drawableGreen);
                        }
                        if (locationLot.isLot_reservable()) {
//                            locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                        }
                    } else if (locationLot.getOccupied().equalsIgnoreCase("no")) {
                        if (locationLot.isLot_reservable()) {
                            //GREY with R, Available for reservation
                            locationLotHolder.ivCar.setImageDrawable(drawableGrey);
//                            locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                        } else {
                            //GREY, Available for parking
                            locationLotHolder.ivCar.setImageDrawable(drawableGrey);
                        }

                    } else if (locationLot.getOccupied().equalsIgnoreCase("reserved")) {
                        //BLUE with R, Reserved for parking
                        locationLotHolder.ivCar.setImageDrawable(drawableBlue);
//                        locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (locationLot.isLot_reservable()) {
                        //GREY with R, Available for reservation
                        locationLotHolder.ivCar.setImageDrawable(drawableGrey);
//                        locationLotHolder.tvReserved.setVisibility(View.VISIBLE);
                    } else {
                        //GREY, Available for parking
                        locationLotHolder.ivCar.setImageDrawable(drawableGrey);
                    }
                }

                if (locationLot.isIs_reserved()) {
                    locationLotHolder.ivCar.setImageDrawable(drawableBlue);
                    locationLotHolder.viewIsReserved.setVisibility(View.VISIBLE);
                } else {
                    locationLotHolder.viewIsReserved.setVisibility(View.GONE);
//                    locationLotHolder.ivCar.setImageDrawable(drawableGrey);
                }

                if (locationLot.isLot_reservable()) {
                    locationLotHolder.ivReserved.setVisibility(View.VISIBLE);
                } else {
                    locationLotHolder.ivReserved.setVisibility(View.GONE);
                }

                if (locationLot.isPremium_lot())
                    locationLotHolder.ivPremium.setVisibility(View.VISIBLE);
                else locationLotHolder.ivPremium.setVisibility(View.GONE);

                if (locationLot.isSpecial_need_handi_lot())
                    locationLotHolder.ivHandicapped.setVisibility(View.VISIBLE);
                else locationLotHolder.ivHandicapped.setVisibility(View.GONE);

                if (locationLot.isLot_reservable_only()) {
                    locationLotHolder.ivReservedOnly.setVisibility(View.VISIBLE);
                } else locationLotHolder.ivReservedOnly.setVisibility(View.GONE);
            }

        }

        @Override
        public int getItemCount() {
            return locationLots.size();
        }

        class LocationLotHolder extends RecyclerView.ViewHolder {
            ImageView ivCar, ivReserved, ivPremium, ivHandicapped, ivReservedOnly;
            TextView tvRow, tvReserved;
            View viewIsReserved;

            LocationLotHolder(@NonNull View itemView) {
                super(itemView);

                ivCar = itemView.findViewById(R.id.ivCar);
                tvRow = itemView.findViewById(R.id.tvRow);
                tvReserved = itemView.findViewById(R.id.tvReserved);
                viewIsReserved = itemView.findViewById(R.id.viewIsReserved);
                ivReserved = itemView.findViewById(R.id.ivReserved);
                ivPremium = itemView.findViewById(R.id.ivPremium);
                ivHandicapped = itemView.findViewById(R.id.ivHandicapped);
                ivReservedOnly = itemView.findViewById(R.id.ivReservedOnly);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
//                            selectedLocationLot = locationLots.get(position);
                            if (locationLots.get(position).isIs_reserved()) {
                                Toast.makeText(getActivity(), "Space already reserved!", Toast.LENGTH_SHORT).show();
                            } else {
                                if (selectedLocationLot != null) {
                                    locationLots.get(locationLots.indexOf(
                                            new LocationLot(selectedLocationLot.getId()))).setOccupied("no");
                                    selectedLocationLot = null;
                                    txt_row.setText("Select Space");
                                }
                                if (!TextUtils.isEmpty(locationLots.get(position).getOccupied())) {
                                    if (locationLots.get(position).getOccupied().equalsIgnoreCase("no")) {
                                        locationLots.get(position).setOccupied("yes");
                                        selectedLocationLot = locationLots.get(position);
                                        txt_row.setText(selectedLocationLot.getLot_id());
                                        notifyItemChanged(position);
                                        if (dialogLocationLot != null && dialogLocationLot.isShowing()) {
                                            dialogLocationLot.dismiss();
                                            if (isReserveDateSelected) {
                                                showPaymentOptionReservation();
                                            }
                                        }
                                    } else {
                                        if (selectedLocationLot != null &&
                                                selectedLocationLot.getId() == locationLots.get(position).getId()) {
                                            selectedLocationLot = null;
                                            locationLots.get(position).setOccupied("no");
                                            if (dialogLocationLot != null && dialogLocationLot.isShowing()) {
                                                dialogLocationLot.dismiss();
                                                if (isReserveDateSelected) {
                                                    showPaymentOptionReservation();
                                                }
                                            }
                                        } else {
                                            Toast.makeText(getActivity(), "Already occupied!", Toast.LENGTH_SHORT).show();
                                        }
//                                    locationLots.get(position).setOccupied("no");
                                    }
                                } else {
                                    locationLots.get(position).setOccupied("yes");
                                    locationLots.get(position).setOccupied("yes");
                                    selectedLocationLot = locationLots.get(position);
                                    txt_row.setText(selectedLocationLot.getLot_id());
                                    notifyItemChanged(position);
                                    if (dialogLocationLot != null && dialogLocationLot.isShowing()) {
                                        dialogLocationLot.dismiss();
                                        if (isReserveDateSelected) {
                                            showPaymentOptionReservation();
                                        }
                                    }
                                }
                                notifyItemChanged(position);
                            }
                        }
                    }
                });
            }
        }
    }

    private void showPaymentOptionReservation() {
        isClickParking = false;
        PaymentOptionDialog dialog = PaymentOptionDialog.newInstance();
        Bundle bundle = new Bundle();
        bundle.putBoolean("mIsPayLater", !isPrePaymentRequiredForReservation);
        dialog.setArguments(bundle);
        dialog.show(getChildFragmentManager(), "payment");
    }

    private class addParkedCar extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            Map<String, Object> parkedCar = new HashMap<String, Object>();
            parkedCar.put("company_type_id", "");
            parkedCar.put("company_type", "");
            parkedCar.put("manager_type_id", "");
            parkedCar.put("manager_type", "");
            parkedCar.put("company_id", "");
            parkedCar.put("parking_type", "");
            parkedCar.put("twp_id", "");
            parkedCar.put("township_code", "");
            parkedCar.put("location_id", "");
            parkedCar.put("location_code", "");
            parkedCar.put("lot_row", "");
            parkedCar.put("lot_number", "");
            parkedCar.put("lot_id", "");
            parkedCar.put("location_lot_id", "");
            parkedCar.put("entry_date_time", "");
            parkedCar.put("exit_date_time", "");
            parkedCar.put("expiry_time", "");
            parkedCar.put("user_id", "");
            parkedCar.put("permit_id", "");
            parkedCar.put("subscription_id", "");
            parkedCar.put("plate_no", "");
            parkedCar.put("pl_state", "");
            parkedCar.put("pl_country", "");
            parkedCar.put("lat", "");
            parkedCar.put("lng", "");
            parkedCar.put("address1", "");
            parkedCar.put("address2", "");
            parkedCar.put("city", "");
            parkedCar.put("state", "");
            parkedCar.put("zip", "");
            parkedCar.put("country", "");
            parkedCar.put("marker_lng", "");
            parkedCar.put("marker_lat", "");
            parkedCar.put("marker_address1", "");
            parkedCar.put("marker_address2", "");
            parkedCar.put("marker_city", "");
            parkedCar.put("marker_state", "");
            parkedCar.put("marker_zip", "");
            parkedCar.put("marker_country", "");
            parkedCar.put("distance_to_marker", "");
            parkedCar.put("ip", "");
            parkedCar.put("parking_token", "");
            parkedCar.put("parking_status", "");
            parkedCar.put("payment_method", "");
            parkedCar.put("parking_rate", "");
            parkedCar.put("parking_units", "");
            parkedCar.put("parking_qty", "");
            parkedCar.put("parking_subtotal", "");
            parkedCar.put("wallet_trx_id", "");
            parkedCar.put("tr_percent", "");
            parkedCar.put("tr_fee", "");
            parkedCar.put("parking_total", "");
            parkedCar.put("ipn_custom", "");
            parkedCar.put("ipn_txn_id", "");
            parkedCar.put("ipn_payment", "");
            parkedCar.put("ipn_status", "");
            parkedCar.put("ipn_address", "");
            parkedCar.put("ticket_status", "");
            parkedCar.put("expiry_status", "");
            parkedCar.put("notified_status", "");
            parkedCar.put("user_id_ref", "");
            parkedCar.put("mismatch", "");
            parkedCar.put("payment_choice", "");
            parkedCar.put("bursar_trx_id", "");
            parkedCar.put("platform", "");
            parkedCar.put("max_duration", "");
            parkedCar.put("selected_duration", "");
            parkedCar.put("user_id_exit", "");
            parkedCar.put("exit_forced", "");
            parkedCar.put("towed", "");
            parkedCar.put("exit_overnight", "");
            parkedCar.put("pre_booked", "");
            parkedCar.put("reservation_id", "");
            parkedCar.put("ParkedNow_PayLater", "");
            parkedCar.put("ParkNow_PostPayment_term", "");
            parkedCar.put("ParkNow_PostPayment_Fee", "");
            parkedCar.put("ParkNow_PostPayment_LateFee", "");
            parkedCar.put("ParkNow_PostPayment_DueAmount", "");
            parkedCar.put("ParkNow_PostPayment_Status", "");
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

    private void updateManagedLocations(LocationLotAdapter locationLotAdapter) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        for (int i = 0; i < reserveParkings.size(); i++) {
            try {
                long entryTime = simpleDateFormat.parse(reserveParkings.get(i).getReserve_entry_time()).getTime();
                long exitTime = simpleDateFormat.parse(reserveParkings.get(i).getReserve_expiry_time()).getTime();
                for (int j = 0; j < locationLots.size(); j++) {
                    if (Integer.valueOf(reserveParkings.get(i).getLocation_lot_id()) == locationLots.get(j).getId()) {
                        if (selectedEntryTime > entryTime && selectedExitTime < exitTime) {
                            // 2PM               1PM           3PM               4PM
                            locationLots.get(j).setIs_reserved(true);
                        } else if (selectedEntryTime >= entryTime && selectedEntryTime <= exitTime) {
                            // 2PM                      1PM         2PM                 4PM
                            locationLots.get(j).setIs_reserved(true);
                        } else if (selectedExitTime >= entryTime && selectedExitTime <= exitTime) {
                            // 2PM                      1PM         2PM                 4PM
                            locationLots.get(j).setIs_reserved(true);
                        }
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        locationLotAdapter.notifyDataSetChanged();

    }

    private class fetchReservedParking extends AsyncTask<String, String, String> {
        JSONObject json;
        JSONArray json1;
        String reservedParkingUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rl_progressbar.setVisibility(View.VISIBLE);
            reservedParkingUrl = "_table/reserved_parkings?filter=(location_code%3D" + location_code
                    + ")%20AND%20(reservation_status%3DACTIVE)";
            reserveParkings.clear();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + reservedParkingUrl;
            Log.e("#DEBUG", "      fetchReserveParking:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");

                    for (int j = 0; j < jsonArray.length(); j++) {
                        ReserveParking parkingUpdate = new ReserveParking();
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            parkingUpdate.setId(object.getString("id"));
                        }

                        if (object.has("lot_row")
                                && !TextUtils.isEmpty(object.getString("lot_row"))
                                && !object.getString("lot_row").equals("null")) {
                            parkingUpdate.setLot_row(object.getString("lot_row"));
                        }
                        if (object.has("lot_number")
                                && !TextUtils.isEmpty(object.getString("lot_number"))
                                && !object.getString("lot_number").equals("null")) {
                            parkingUpdate.setLot_number(object.getString("lot_number"));
                        }
                        if (object.has("lot_id")
                                && !TextUtils.isEmpty(object.getString("lot_id"))
                                && !object.getString("lot_id").equals("null")) {
                            parkingUpdate.setLot_id(object.getString("lot_id"));
                        }
                        if (object.has("reservation_date_time")
                                && !TextUtils.isEmpty(object.getString("reservation_date_time"))
                                && !object.getString("reservation_date_time").equals("null")) {
                            parkingUpdate.setReservation_date_time(object.getString("reservation_date_time"));
                        }
                        if (object.has("reserve_entry_time")
                                && !TextUtils.isEmpty(object.getString("reserve_entry_time"))
                                && !object.getString("reserve_entry_time").equals("null")) {
                            parkingUpdate.setReserve_entry_time(object.getString("reserve_entry_time"));
                        }
                        if (object.has("reserve_expiry_time")
                                && !TextUtils.isEmpty(object.getString("reserve_expiry_time"))
                                && !object.getString("reserve_expiry_time").equals("null")) {
                            parkingUpdate.setReserve_expiry_time(object.getString("reserve_expiry_time"));
                        }
                        if (object.has("reserve_exit_date_time")
                                && !TextUtils.isEmpty(object.getString("reserve_exit_date_time"))
                                && !object.getString("reserve_exit_date_time").equals("null")) {
                            parkingUpdate.setReserve_exit_date_time(object.getString("reserve_exit_date_time"));
                        }

                        if (object.has("reservation_status")
                                && !TextUtils.isEmpty(object.getString("reservation_status"))
                                && !object.getString("reservation_status").equals("null")) {
                            parkingUpdate.setReservation_status(object.getString("reservation_status"));
                        }
                        if (object.has("user_id")
                                && !TextUtils.isEmpty(object.getString("user_id"))
                                && !object.getString("user_id").equals("null")) {
                            parkingUpdate.setUser_id(object.getString("user_id"));
                        }
                        if (object.has("plate_no")
                                && !TextUtils.isEmpty(object.getString("plate_no"))
                                && !object.getString("plate_no").equals("null")) {
                            parkingUpdate.setPlate_no(object.getString("plate_no"));
                        }
                        if (object.has("pl_state")
                                && !TextUtils.isEmpty(object.getString("pl_state"))
                                && !object.getString("pl_state").equals("null")) {
                            parkingUpdate.setPl_state(object.getString("pl_state"));
                        }
                        if (object.has("pl_country")
                                && !TextUtils.isEmpty(object.getString("pl_country"))
                                && !object.getString("pl_country").equals("null")) {
                            parkingUpdate.setPl_country(object.getString("pl_country"));
                        }


                        if (object.has("parking_status")
                                && !TextUtils.isEmpty(object.getString("parking_status"))
                                && !object.getString("parking_status").equals("null")) {
                            parkingUpdate.setParking_status(object.getString("parking_status"));
                        }
                        if (object.has("location_lot_id")
                                && !TextUtils.isEmpty(object.getString("location_lot_id"))
                                && !object.getString("location_lot_id").equals("null")) {
                            parkingUpdate.setLocation_lot_id(object.getString("location_lot_id"));
                        }

                        reserveParkings.add(parkingUpdate);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            rl_progressbar.setVisibility(View.GONE);
            Log.e("#DEBUG", "   fetchReservedParking:  onPost:  Size: " + reserveParkings.size());
            if (reserveParkings.size() != 0) {

            } else {

            }
        }
    }

    private void calculateEntryExitTime() {
        Calendar calendar1 = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        selectedEntryTime = calendar1.getTimeInMillis();
        reserveEntryTime = dateFormat.format(new Date(calendar1.getTimeInMillis()));
        if (min.equals("Hrs") || min.equals("Hr")) {
            calendar1.add(Calendar.HOUR, hours);
        } else if (min.equals("Months") || min.equals("Month")) {
            calendar1.add(Calendar.MONTH, hours);
            calendar1.add(Calendar.DATE, -1);
        } else if (min.equals("Days") || min.equals("Day")) {
            calendar1.add(Calendar.DATE, hours);
        } else if (min.equals("Weeks") || min.equals("Week")) {
            calendar1.add(Calendar.DAY_OF_WEEK_IN_MONTH, hours);
        } else {
            calendar1.add(Calendar.MINUTE, hours);
        }

        reserveExpiryTime = dateFormat.format(new Date(calendar1.getTimeInMillis()));
        selectedExitTime = calendar1.getTimeInMillis();

        Log.e("#DEBUG", "   entryTime: " + selectedEntryTime);
        Log.e("#DEBUG", "   exitTime: " + selectedExitTime);

    }
}
