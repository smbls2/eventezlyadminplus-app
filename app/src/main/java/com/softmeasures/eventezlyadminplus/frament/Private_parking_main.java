package com.softmeasures.eventezlyadminplus.frament;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.panstreetview;
import com.softmeasures.eventezlyadminplus.frament.partner.PartnersFragment;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.activity.vchome.latitude;
import static com.softmeasures.eventezlyadminplus.activity.vchome.longitude;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step1.Private_location_code;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step1.private_address;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step1.private_lang;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step1.private_lat;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step1.private_no_parking;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step1.private_parking_time;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step1.private_title;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step2.private_lost_avali;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step2.private_lot_total;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step2.private_of_peack_week_end;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step2.private_off_peak_discount;

public class Private_parking_main extends Fragment implements Other_City.Other_city_listenere {
    ArrayList<item> parking_rules_array = new ArrayList<>();
    ArrayList<item> parkingarray = new ArrayList<>();
    ArrayList<item> parkingarray1 = new ArrayList<>();
    RelativeLayout rl_progressbar;
    double centerlang = 0.0, centerlat = 0.0, Private_lat = 0.0, Private_lang = 0.0, latitude_selected = 0.0, longitude_selected = 0.0;
    String privatemap = "private1";
    boolean PrivateMarkerClick = false, PrivateMapLoaded = false, PrivatePopup = false, localparkingclick = false;
    public static String lat = "null", lang = "null", title = "", private_id, effect = "1", active = "1", renew = "1";
    ImageView img_nearby_private;
    Animation animation;
    RelativeLayout rl_private_nearby_list;
    ListView list_private_nearby;
    ImageView img_private_nearby_close, img_private_searchother, img_add_private_parking;
    public static boolean clickonedit = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.private_map, container, false);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        img_nearby_private = (ImageView) view.findViewById(R.id.findnearby_private);
        rl_private_nearby_list = (RelativeLayout) view.findViewById(R.id.rl_private_nearby);
        list_private_nearby = (ListView) view.findViewById(R.id.list_nearby_private);
        img_private_nearby_close = (ImageView) view.findViewById(R.id.img_private_map_close);
        img_private_searchother = (ImageView) view.findViewById(R.id.searchotherlocatiopruvate);
        img_add_private_parking = (ImageView) view.findViewById(R.id.add_private);
        RelativeLayout rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        new getprivate_parking_rules().execute();
        new getall_privatepakring(latitude, longitude).execute();
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        img_nearby_private.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (parkingarray.size() > 0) {
                    animation = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.sidepannelright);
                    rl_private_nearby_list.setAnimation(animation);
                    rl_private_nearby_list.setVisibility(View.VISIBLE);
                    Resources rs = getResources();
                    list_private_nearby.setAdapter(new CustomAdapterprivate(getActivity(), parkingarray, rs));
                }
                list_private_nearby.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int k, long id) {
                        rl_private_nearby_list.setVisibility(View.GONE);
                        localparkingclick = false;
                        ShowPrivateParking(getActivity(), parkingarray.get(k).getLocation_code(), parkingarray.get(k).getLat(), parkingarray.get(k).getLang());
                    }
                });
            }
        });
        img_private_nearby_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animation = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.sidepannelleft);
                rl_private_nearby_list.setAnimation(animation);
                rl_private_nearby_list.setVisibility(View.GONE);
            }
        });

        img_private_searchother.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.getString("click", "commercia");
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                Other_City pay = new Other_City();
                pay.setArguments(b);
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                pay.registerForListener(Private_parking_main.this);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });


        img_add_private_parking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                PartnersFragment pay = new PartnersFragment();
                Bundle bundle = new Bundle();
                bundle.putString("parkingType", "Other");
                pay.setArguments(bundle);
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });
        return view;
    }

    @Override
    public void OnItemFindClick(String lat, String lang) {
        this.lat = lat;
        this.lang = lang;
        new getall_privatepakring2(Double.valueOf(lat), Double.parseDouble(lang)).execute();
    }

    public class getprivate_parking_rules extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        // http://108.30.248.212:8006/api/v2/pzly01live7/_table/user_locations?filter=user_id%3D5%20AND%20location_code%3DNY-NHP-03
        String id = logindeatl.getString("id", "null");
        String parkingurl = "_table/other_parking_rules";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            parking_rules_array.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setLoationname("");
                        item1.setTime_rule(c.getString("time_rule"));
                        item1.setPricing_duration(c.getString("pricing_duration"));
                        item1.setDuration_unit(c.getString("duration_unit"));
                        item1.setMax_time(c.getString("max_duration"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setThis_day(c.getString("day_type"));
                        String time_rules = c.getString("time_rule");
                        item1.setPricing(c.getString("pricing"));
                        item1.setId(c.getString("id"));
                        item1.setLocation_code(c.getString("location_code"));
                        if (time_rules.contains("-")) {
                            String start_time = time_rules.substring(0, time_rules.indexOf("-"));
                            String end_time = time_rules.substring(time_rules.indexOf("-") + 1);
                            item1.setStart_time(start_time);
                            item1.setEnd_time(end_time);
                        } else {
                            item1.setStart_time("null");
                            item1.setEnd_time("null");
                        }

                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item1.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }

                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item1.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }

                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item1.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }

                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item1.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }

                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item1.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }

                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item1.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item1.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }

                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item1.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }

                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item1.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }

                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item1.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }

                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item1.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }

                        parking_rules_array.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

    //show private parking data from private map view
    public class getall_privatepakring extends AsyncTask<String, String, String> {

        double lat = 0.0, lang = 0.0;

        public getall_privatepakring(double lat, double lang) {
            this.lat = lat;
            this.lang = lang;
        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            parkingarray.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + "_proc/find_other_parking_partners_nearby?in_lat=" + lat + "&in_lng=" + lang + "";
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            Log.e("private_url", url);
            HttpResponse response = null;

            try {
                response = client.execute(post);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                try {
                    HttpEntity resEntity = response.getEntity();
                    String responseStr = null;


                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONArray array = new JSONArray(responseStr);

                    for (int i = 0; i < array.length(); i++) {
                        item item1 = new item();
                        JSONObject c = array.getJSONObject(i);
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setTitle(c.getString("title"));
                        item1.setAddress(c.getString("address"));
                        item1.setLat(c.getString("lat"));
                        item1.setLang(c.getString("lng"));
                        item1.setEffect(c.getString("in_effect"));
                        item1.setActive(c.getString("active"));
                        item1.setRenew(c.getString("renewable"));
                        item1.setParking_times(c.getString("parking_times"));
                        item1.setNo_parking_times(c.getString("no_parking_times"));
                        item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        item1.setWeek_ebd_discount(c.getString("weekend_special_diff"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setLots_aval(c.getString("lots_avbl"));
                        item1.setTotal_lots(c.getString("lots_total"));
                        item1.setOff_peak_start(c.getString("off_peak_starts"));
                        item1.setOff_peak_end(c.getString("off_peak_ends"));
                        item1.setLocation_place_id(c.getString("location_place_id"));
                        item1.setId(c.getString("id"));
                        parkingarray.add(item1);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;

            if (getActivity() != null && parkingarray.size() >= 0) {
                centerlang = 0.0;
                centerlat = 0.0;
                privatemap = "private1";
                ShowPrivateMap();

            }
            super.onPostExecute(s);

        }
    }

    // show map from private parking.............
    public void ShowPrivateMap() {
        SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map3);
        fm.getMapAsync(googleMap -> {
            GoogleMap map = googleMap;
            MarkerOptions options = null;
            options = new MarkerOptions();
            if (privatemap.equals("private1")) {
                for (int i = 0; i < parkingarray.size(); i++) {
                    LatLng position = new LatLng(Double.parseDouble(parkingarray.get(i).getLat()), Double.parseDouble(parkingarray.get(i).getLang()));
                    options.position(position);
                    options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.parkwhiz_pin_icon));
                    options.title("private");
                    options.snippet(parkingarray.get(i).getLat() + "-" + parkingarray.get(i).getLang());
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    map.setMyLocationEnabled(false);
                    map.addMarker(options);
                    View locationButton = ((View) fm.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                    RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                    rlp.setMargins(0, 0, 30, 20);

                }
                LatLng po1 = new LatLng(latitude, longitude);
                if (centerlang == 0.0 && centerlat == 0.0) {
                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(po1, 12);
                    map.animateCamera(location);
                }

                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker arg0) {
                        String title = arg0.getTitle();
                        String lang = arg0.getSnippet();
                        String marker_lat = null, marker_lang = null;
                        PrivateMarkerClick = true;
                        PrivateMapLoaded = false;
                        if (lang.contains("-")) {
                            marker_lat = lang.substring(0, lang.indexOf("-"));
                            marker_lang = lang.substring(lang.indexOf("-") + 1);
                        }
                        if (title.equals("private")) {
                            for (int i = 0; i < parkingarray.size(); i++) {
                                String goolat = parkingarray.get(i).getLat();
                                String glang = parkingarray.get(i).getLang();

                                if (marker_lat.equals(goolat) && marker_lang.equals(glang)) {

                                    if (PrivatePopup == false) {
                                        if (privatemap.equals("private1")) {
                                            PrivatePopup = false;
                                            ShowPrivateParking(getActivity(), parkingarray.get(i).getLocation_code(), goolat, glang);
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                        return true;
                    }
                });
                final GoogleMap finalMap = map;
                map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        Private_lat = cameraPosition.target.latitude;
                        Private_lang = cameraPosition.target.longitude;
                        if (PrivateMapLoaded) {
                            if (PrivateMarkerClick == false) {
                                centerlat = cameraPosition.target.latitude;
                                centerlang = cameraPosition.target.longitude;
                                System.out.print("location chamges");
                                Log.e("Private", "yessss");
                                new getall_privatepakring3(Private_lat, Private_lang).execute();

                            }
                        }
                    }
                });

                map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        if (privatemap.equals("private1")) {
                            Log.e("Private_map_loaded", "yes");
                            PrivateMapLoaded = true;
                        }

                    }
                });

            } else if (privatemap.equals("private2")) {
                map.clear();
                for (int i = 0; i < parkingarray.size(); i++) {
                    LatLng position = new LatLng(Double.parseDouble(parkingarray.get(i).getLat()), Double.parseDouble(parkingarray.get(i).getLang()));
                    options.position(position);
                    options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.parkwhiz_pin_icon));
                    options.title("private");
                    options.snippet(parkingarray.get(i).getLat() + "-" + parkingarray.get(i).getLang());
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    map.setMyLocationEnabled(false);
                    map.addMarker(options);
                    View locationButton = ((View) fm.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                    RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                    rlp.setMargins(0, 0, 30, 20);

                }
                LatLng po1 = new LatLng(latitude, longitude);


                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker arg0) {
                        String title = arg0.getTitle();
                        String lang = arg0.getSnippet();
                        String marker_lat = null, marker_lang = null;
                        PrivateMarkerClick = true;
                        PrivateMapLoaded = false;
                        if (lang.contains("-")) {
                            marker_lat = lang.substring(0, lang.indexOf("-"));
                            marker_lang = lang.substring(lang.indexOf("-") + 1);
                        }
                        if (title.equals("private")) {
                            for (int i = 0; i < parkingarray.size(); i++) {
                                String goolat = parkingarray.get(i).getLat();
                                String glang = parkingarray.get(i).getLang();

                                if (marker_lat.equals(goolat) && marker_lang.equals(glang)) {

                                    if (PrivatePopup == false) {
                                        if (privatemap.equals("private2")) {
                                            PrivatePopup = true;
                                            ShowPrivateParking(getActivity(), parkingarray.get(i).getLocation_code(), goolat, glang);
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }

                        }
                        return true;
                    }
                });
                final GoogleMap finalMap = map;
                map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        Private_lat = cameraPosition.target.latitude;
                        Private_lang = cameraPosition.target.longitude;
                        if (PrivateMapLoaded) {
                            if (PrivateMarkerClick == false) {
                                centerlat = cameraPosition.target.latitude;
                                centerlang = cameraPosition.target.longitude;
                                System.out.print("location chamges");
                                Log.e("Private", "yessss");
                                new getall_privatepakring3(Private_lat, Private_lang).execute();

                            }
                        }
                    }
                });

                map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        if (privatemap.equals("private2")) {
                            Log.e("Private_map_loaded", "yes");
                            PrivateMapLoaded = true;
                        }

                    }
                });

            } else if (privatemap.equals("private3")) {

                map.clear();
                for (int i = 0; i < parkingarray.size(); i++) {
                    LatLng position = new LatLng(Double.parseDouble(parkingarray.get(i).getLat()), Double.parseDouble(parkingarray.get(i).getLang()));
                    options.position(position);
                    options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.parkwhiz_pin_icon));
                    options.title("private");
                    options.snippet(parkingarray.get(i).getLat() + "-" + parkingarray.get(i).getLang());
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                    map.setMyLocationEnabled(false);
                    map.addMarker(options);
                    View locationButton = ((View) fm.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                    RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                    rlp.setMargins(0, 0, 30, 20);

                }
                LatLng po1 = new LatLng(Double.parseDouble(lat), Double.parseDouble(lang));
                if (centerlang == 0.0 && centerlat == 0.0) {
                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(po1, 12);
                    map.animateCamera(location);
                }

                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker arg0) {
                        String title = arg0.getTitle();
                        String lang = arg0.getSnippet();
                        String marker_lat = null, marker_lang = null;
                        PrivateMarkerClick = true;
                        PrivateMapLoaded = false;
                        if (lang.contains("-")) {
                            marker_lat = lang.substring(0, lang.indexOf("-"));
                            marker_lang = lang.substring(lang.indexOf("-") + 1);
                        }
                        if (title.equals("private")) {
                            for (int i = 0; i < parkingarray.size(); i++) {
                                String goolat = parkingarray.get(i).getLat();
                                String glang = parkingarray.get(i).getLang();

                                if (marker_lat.equals(goolat) && marker_lang.equals(glang)) {

                                    if (PrivatePopup == false) {
                                        if (privatemap.equals("private3")) {
                                            PrivatePopup = false;
                                            ShowPrivateParking(getActivity(), parkingarray.get(i).getLocation_code(), goolat, glang);
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }

                        }
                        return true;
                    }
                });
                final GoogleMap finalMap = map;
                map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        Private_lat = cameraPosition.target.latitude;
                        Private_lang = cameraPosition.target.longitude;
                        if (PrivateMapLoaded) {
                            if (PrivateMarkerClick == false) {
                                centerlat = cameraPosition.target.latitude;
                                centerlang = cameraPosition.target.longitude;
                                System.out.print("location chamges");
                                Log.e("Private", "yessss");
                                new getall_privatepakring3(Private_lat, Private_lang).execute();
                            }
                        }
                    }
                });

                map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        if (privatemap.equals("private3")) {
                            Log.e("Private_map_loaded", "yes");
                            PrivateMapLoaded = true;
                        }
                    }
                });
            }
        });
    }

    public class getall_privatepakring3 extends AsyncTask<String, String, String> {

        double lat = 0.0, lang = 0.0;

        public getall_privatepakring3(double lat, double lang) {
            this.lat = lat;
            this.lang = lang;
        }

        @Override
        protected void onPreExecute() {


            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            parkingarray1.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + "_proc/find_other_parking_partners_nearby?in_lat=" + lat + "&in_lng=" + lang + "";
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            HttpResponse response = null;

            try {
                response = client.execute(post);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                try {
                    HttpEntity resEntity = response.getEntity();
                    String responseStr = null;

                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONArray array = new JSONArray(responseStr);

                    for (int i = 0; i < array.length(); i++) {
                        item item1 = new item();
                        JSONObject c = array.getJSONObject(i);
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setTitle(c.getString("title"));
                        item1.setAddress(c.getString("address"));
                        item1.setLat(c.getString("lat"));
                        item1.setLang(c.getString("lng"));
                        item1.setEffect(c.getString("in_effect"));
                        item1.setActive(c.getString("active"));
                        item1.setRenew(c.getString("renewable"));
                        item1.setParking_times(c.getString("parking_times"));
                        item1.setNo_parking_times(c.getString("no_parking_times"));
                        item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        item1.setWeek_ebd_discount(c.getString("weekend_special_diff"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setLots_aval(c.getString("lots_avbl"));
                        item1.setTotal_lots(c.getString("lots_total"));
                        item1.setId(c.getString("id"));
                        parkingarray1.add(item1);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Resources rs;
            if (getActivity() != null && parkingarray1.size() >= 0) {
                centerlang = 0.0;
                centerlat = 0.0;
                privatemap = "private2";
                parkingarray.clear();
                parkingarray.addAll(parkingarray1);
                parkingarray1.clear();
                ShowPrivateMap();

            }
            super.onPostExecute(s);

        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void ShowPrivateParking(Activity context, String location_code, final String lat, final String lang) {
        context = getActivity();
        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popupfree);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View layout = layoutInflater.inflate(R.layout.popup_private, viewGroup, false);
        final PopupWindow popup = new PopupWindow(context);
        popup.setBackgroundDrawable(null);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(layout);
        popup.setOutsideTouchable(false);
        popup.setFocusable(false);
        popup.setAnimationStyle(R.style.animationName);
        String address = null;
        String weekend_discount = null;
        String parking_time = null;
        String pricing = null;
        String pricingduation = null;
        String duration_unit = null;
        String max_duration = null;
        String off_peak_discont = null;
        String no_parking_time = null;
        String notices = null;
        String time_rules = null;
        String day_type = null;
        String totla_lots = null;
        String total_aval = null;
        final TextView txttitle, txt_day_rules, txttoday, txtmax, txtstreetview, txtdrections, txtaddress, txttime, txtmanaged, txt_parking_time, txt_no_parking_time, txt_off_peak_discount, txt_week_end_discount, txt_coustom_notice, txt_effect;
        Button btnadd;
        ImageView imageclose;
        RelativeLayout rl_popupclose;
        String id = null;
        txttitle = (TextView) layout.findViewById(R.id.txtlable);
        txtstreetview = (TextView) layout.findViewById(R.id.txtpopupstreetview);
        txtdrections = (TextView) layout.findViewById(R.id.txtpopupdirection);
        txtaddress = (TextView) layout.findViewById(R.id.txtpopupaddress);
        txttime = (TextView) layout.findViewById(R.id.txtpopuptime);
        txtmax = (TextView) layout.findViewById(R.id.txtpopupax);
        txt_day_rules = (TextView) layout.findViewById(R.id.txtpouptoday1);
        txt_parking_time = (TextView) layout.findViewById(R.id.txt_parking_time);
        txt_no_parking_time = (TextView) layout.findViewById(R.id.txt_no_parking_time);
        txt_off_peak_discount = (TextView) layout.findViewById(R.id.txt_off_peak);
        txt_week_end_discount = (TextView) layout.findViewById(R.id.txt_weekend_discount);
        txt_coustom_notice = (TextView) layout.findViewById(R.id.txt_custome_notice);
        txt_effect = (TextView) layout.findViewById(R.id.txt_effect);
        btnadd = (Button) layout.findViewById(R.id.btn_edit);
        rl_popupclose = (RelativeLayout) layout.findViewById(R.id.rl_pop);
        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeue-Light.otf");
        txttitle.setTypeface(type);
        txttime.setTypeface(type);
        txtmax.setTypeface(type);
        txt_day_rules.setTypeface(type);
        txt_parking_time.setTypeface(type);
        txt_off_peak_discount.setTypeface(type);
        txt_no_parking_time.setTypeface(type);
        txt_week_end_discount.setTypeface(type);
        txt_coustom_notice.setTypeface(type);
        txt_effect.setTypeface(type);
        popup.showAtLocation(layout, Gravity.CENTER, 0, 0);
        int postion = 0;
        imageclose = (ImageView) layout.findViewById(R.id.popupclose);

        for (int i = 0; i < parkingarray.size(); i++) {
            String locode = parkingarray.get(i).getLocation_code();
            if (location_code.equals(locode)) {
                for (int k = 0; k < parking_rules_array.size(); k++) {
                    Private_location_code = parking_rules_array.get(k).getLocation_code();
                    if (locode.equals(Private_location_code)) {
                        day_type = parking_rules_array.get(k).getThis_day();
                        pricing = parking_rules_array.get(k).getPricing();
                        pricingduation = parking_rules_array.get(k).getPricing_duration();
                        duration_unit = parking_rules_array.get(k).getDuration_unit();
                        max_duration = parking_rules_array.get(k).getMax_time();
                        time_rules = parking_rules_array.get(k).getTime_rule();
                        private_title = parkingarray.get(i).getTitle();
                        private_address = parkingarray.get(i).getAddress();
                        private_parking_time = parkingarray.get(i).getParking_times();
                        private_no_parking = parkingarray.get(i).getNo_parking_times();
                        private_of_peack_week_end = parkingarray.get(i).getWeek_ebd_discount();
                        private_off_peak_discount = parkingarray.get(i).getOff_peak_discount();
                        private_lost_avali = parkingarray.get(i).getLots_aval();
                        private_lot_total = parkingarray.get(i).getTotal_lots();
                        notices = parkingarray.get(i).getCustom_notice();
                        effect = parkingarray.get(i).getEffect();
                        postion = i;
                        private_id = parkingarray.get(i).getId();
                        this.lat = parkingarray.get(i).getLat();
                        this.lang = parkingarray.get(i).getLang();
                        private_lat = parkingarray.get(i).getLat();
                        private_lang = parkingarray.get(i).getLang();
                        break;
                    }
                }
                break;
            }
        }
        double weekend = Double.parseDouble(private_of_peack_week_end);
        String week = String.format("%.1f", weekend);
        if (off_peak_discont != null) {
            if (!private_off_peak_discount.equals("null")) {
                double off = Double.parseDouble(private_off_peak_discount);
                String oddpeak = String.format("%.1f", off);
                txt_off_peak_discount.setText("Off Peak Discount: " + oddpeak + "%");
            }
        }
        txttitle.setText(private_title + "\n" + private_address);
        txt_parking_time.setText("Parking Rimes: " + private_parking_time);
        txt_no_parking_time.setText("No Parking Times: " + private_no_parking);

        txt_week_end_discount.setText("Weekend Discount: " + week + "%");
        if (notices.equals(null) || notices.equals("null")) {
            txt_coustom_notice.setText("Custom Notices:");
        } else {
            txt_coustom_notice.setText("Custom Notices: " + notices);
        }
        txt_day_rules.setText(day_type + ":");
        txtaddress.setText(time_rules);
        txttime.setText("$" + pricing + " @ " + pricingduation + duration_unit);
        txtmax.setText(max_duration + duration_unit);
        txt_effect.setText("In Effect " + effect);
        final int finalPostion = postion;
        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickonedit = true;
                PrivateMarkerClick = false;
                PrivateMapLoaded = true;
                PrivatePopup = false;
                Private_location_code = parkingarray.get(finalPostion).getLocation_code();
                active = parkingarray.get(finalPostion).getActive();
                renew = parkingarray.get(finalPostion).getRenew();
                popup.dismiss();

                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                frg_private_edit_add_step1 pay = new frg_private_edit_add_step1();
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

        imageclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PrivateMarkerClick = false;
                PrivateMapLoaded = true;
                PrivatePopup = false;
                popup.dismiss();
            }
        });
        rl_popupclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrivateMarkerClick = false;
                PrivateMapLoaded = true;
                popup.dismiss();
            }
        });
        final String finalAddress1 = address;
        txtdrections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrivateMarkerClick = false;
                PrivateMapLoaded = true;
                PrivatePopup = false;
                SharedPreferences sh1 = getActivity().getSharedPreferences("directiontomydirection", Context.MODE_PRIVATE);
                SharedPreferences.Editor ed1 = sh1.edit();
                ed1.putString("lat", lat);
                ed1.putString("lang", lang);
                ed1.putString("address", finalAddress1);
                ed1.commit();
                f_direction pay = new f_direction();
               /* transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                transaction.replace(R.id.My_Container_1_ID, pay, "Frag_Middle_tag");
                transaction.commit();*/
                // manageddorection();
            }
        });

        final String finalTitle1 = title;
        final String finalAddress2 = address;
        txtstreetview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrivateMarkerClick = false;
                PrivateMapLoaded = true;
                PrivatePopup = false;
                Intent i = new Intent(getActivity(), panstreetview.class);
                i.putExtra("lat", lat);
                i.putExtra("lang", lang);
                i.putExtra("title", finalTitle1);
                i.putExtra("address", finalAddress2);
                startActivity(i);
            }
        });

        // markerclick=false;
    }

    public class CustomAdapterprivate extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdapterprivate(Activity a, ArrayList<item> d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;
            data = d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.adapter_nearby_private_parking, null);
                /****** View Holder Object to contain tabitem.xml file elements ******/
                holder = new ViewHolder();

                holder.tvLocationName1 = vi.findViewById(R.id.tvLocationName1);
                holder.tvPrice = vi.findViewById(R.id.tvPrice);
                holder.tvAddress = vi.findViewById(R.id.tvAddress);
                holder.tvDistance = vi.findViewById(R.id.tvDistance);
                holder.tvTimingRules = vi.findViewById(R.id.tvTimingRules);
                holder.tvStatus = vi.findViewById(R.id.tvStatus);

                holder.txt_commerical_name = (TextView) vi.findViewById(R.id.txt_comm_name);
                holder.txt_deatil = (TextView) vi.findViewById(R.id.txt_comm_add);
                holder.rldata = (RelativeLayout) vi.findViewById(R.id.rl_main);
                holder.txt_pricing = (TextView) vi.findViewById(R.id.txt_pricing);
                holder.txt_mile = (TextView) vi.findViewById(R.id.txt_mile);
                holder.txt_parking_status = (TextView) vi.findViewById(R.id.txt_commercial_view);
                holder.txt_la = (TextView) vi.findViewById(R.id.txt_lat);
                holder.txt_lang = (TextView) vi.findViewById(R.id.txt_lang);
                holder.txt_location_code = (TextView) vi.findViewById(R.id.txt_location_code);
                holder.txt_place_id = (TextView) vi.findViewById(R.id.txt_place_id);
                holder.txt_marker_type = (TextView) vi.findViewById(R.id.txt_marker_type);

                holder.txt_parking_status.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String status = holder.txt_parking_status.getText().toString();
                        rl_private_nearby_list.setVisibility(View.GONE);
                        ShowPrivateParking(getActivity(), holder.txt_location_code.getText().toString(), holder.txt_la.getText().toString(), holder.txt_lang.getText().toString());
                    }
                });
                /************  Set holder with LayoutInflater ************/
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }
            if (data.size() <= 0) {
                rl_private_nearby_list.setVisibility(View.GONE);
            } else {

                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String cat = tempValues.getTitle();
                if (cat == null) {

                } else if (cat != null) {
                    for (int i = 0; i < data.size(); i++) {
                        String loc_code = data.get(position).getLocation_code();
                        for (int j = 0; j < parking_rules_array.size(); j++) {
                            String loccode = parking_rules_array.get(j).getLocation_code();
                            if (loccode.equals(loc_code)) {
                                holder.txt_commerical_name.setText(tempValues.getTitle());
                                holder.tvLocationName1.setText(tempValues.getTitle());
                                holder.txt_deatil.setText(tempValues.getAddress());
                                holder.tvAddress.setText(tempValues.getAddress());
                                String effect = data.get(position).getEffect();


                                if (position % 2 == 0) {
                                    holder.rldata.setBackgroundColor(Color.parseColor("#949494"));
                                } else {
                                    holder.rldata.setBackgroundColor(Color.parseColor("#434343"));
                                }
                                if (!parking_rules_array.get(j).getPricing().equals("")) {
                                    double prices = Double.parseDouble(parking_rules_array.get(j).getPricing());
                                    String lots_aval = data.get(position).getLots_aval();
                                    if (effect.equals("0")) {
                                        holder.txt_parking_status.setVisibility(View.VISIBLE);
                                        holder.txt_parking_status.setText("CLOSED");
                                        holder.txt_parking_status.setBackgroundColor(Color.parseColor("#E9967A"));
                                    } else if (lots_aval.equals("0")) {
                                        holder.txt_parking_status.setVisibility(View.VISIBLE);
                                        holder.txt_parking_status.setText("CLOSED");
                                        holder.txt_parking_status.setBackgroundColor(Color.parseColor("#E9967A"));
                                    } else if (prices != -1.0 || prices != 1) {
                                        holder.txt_parking_status.setVisibility(View.VISIBLE);
                                        holder.txt_parking_status.setText("OPEN");
                                        holder.txt_parking_status.setBackgroundColor(Color.parseColor("#3CB385"));
                                    } else {
                                        holder.txt_parking_status.setVisibility(View.VISIBLE);
                                        holder.txt_parking_status.setText("CLOSED");
                                        holder.txt_parking_status.setBackgroundColor(Color.parseColor("#E9967A"));
                                    }
                                    holder.txt_pricing.setText("$" + parking_rules_array.get(j).getPricing());
                                    holder.tvPrice.setText("$" + parking_rules_array.get(j).getPricing());
                                    holder.tvTimingRules.setText(parking_rules_array.get(j).getTime_rule());
                                } else {
                                    holder.txt_parking_status.setVisibility(View.GONE);
                                    holder.txt_pricing.setText("");
                                }
                                break;
                            } else {
                                holder.txt_parking_status.setVisibility(View.GONE);
                            }
                        }
                    }
                    String data1 = tempValues.getPricing();

                    if (!tempValues.getLat().equals("") && !tempValues.getLang().equals("")) {
                        holder.txt_mile.setVisibility(View.VISIBLE);
                        LatLng kk = new LatLng(Double.parseDouble(tempValues.getLat()), Double.parseDouble(tempValues.getLang()));
                        double dist = getDistance(kk);
                        double ff = dist * 0.000621371192;
                        String finallab2 = String.format("%.2f", ff);
                        holder.txt_mile.setText(finallab2 + " Mile");
                        holder.tvDistance.setText(finallab2 + " Mile");
                    } else {
                        holder.txt_mile.setVisibility(View.GONE);

                    }
                    holder.txt_la.setText(tempValues.getLat());
                    holder.txt_lang.setText(tempValues.getLang());
                    holder.txt_location_code.setText(tempValues.getLocation_code());
                    holder.txt_place_id.setText(tempValues.getPlace_id());
                    holder.txt_marker_type.setText(tempValues.getMarker_type());
                }


            }

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {

            public TextView txt_commerical_name, txt_deatil, txt_pricing, txt_mile, txt_parking_status, txt_la, txt_lang, txt_location_code, txt_place_id, txt_marker_type;
            ;
            RelativeLayout rldata;
            TextView tvLocationName1, tvPrice, tvAddress, tvDistance, tvTimingRules, tvStatus;
        }
    }

    public double getDistance(LatLng LatLng1) {
        double distance = 0;
        Location locationA = new Location("A");
        locationA.setLatitude(LatLng1.latitude);
        locationA.setLongitude(LatLng1.longitude);
        Location locationB = new Location("B");
        locationB.setLatitude(latitude);
        locationB.setLongitude(longitude);
        distance = locationA.distanceTo(locationB);
        return distance;
    }

    //show private parking data from private map view with search other city
    public class getall_privatepakring2 extends AsyncTask<String, String, String> {
        boolean places_id_match = false;
        JSONObject json, json1, json_township;
        double lat = 0.0, lang = 0.0;

        public getall_privatepakring2(double lat, double lang) {
            this.lat = lat;
            this.lang = lang;
        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            parkingarray.clear();

            String url = getString(R.string.api) + getString(R.string.povlive) + "_proc/find_other_parking_partners_nearby?in_lat=" + lat + "&in_lng=" + lang + "";

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            HttpResponse response = null;

            try {
                response = client.execute(post);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                try {
                    HttpEntity resEntity = response.getEntity();
                    String responseStr = null;


                    responseStr = EntityUtils.toString(resEntity).trim();
                    JSONArray array = new JSONArray(responseStr);

                    for (int i = 0; i < array.length(); i++) {
                        item item1 = new item();
                        JSONObject c = array.getJSONObject(i);
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setTitle(c.getString("title"));
                        item1.setAddress(c.getString("address"));
                        item1.setLat(c.getString("lat"));
                        item1.setLang(c.getString("lng"));
                        item1.setEffect(c.getString("in_effect"));
                        item1.setActive(c.getString("active"));
                        item1.setRenew(c.getString("renewable"));
                        item1.setParking_times(c.getString("parking_times"));
                        item1.setNo_parking_times(c.getString("no_parking_times"));
                        item1.setOff_peak_discount(c.getString("off_peak_discount"));
                        item1.setWeek_ebd_discount(c.getString("weekend_special_diff"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setLots_aval(c.getString("lots_avbl"));
                        item1.setTotal_lots(c.getString("lots_total"));
                        item1.setId(c.getString("id"));
                        parkingarray.add(item1);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            Resources rs;
            if (getActivity() != null && parkingarray.size() >= 0) {
                centerlang = 0.0;
                centerlat = 0.0;
                privatemap = "private3";
                ShowPrivateMap();
            }
            super.onPostExecute(s);

        }
    }

    @Override
    public void onResume() {
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    if (rl_private_nearby_list.getVisibility() == View.VISIBLE) {
                        rl_private_nearby_list.setVisibility(View.GONE);
                    } else {
                        getActivity().onBackPressed();
                    }
                    return true;
                }

                return false;
            }
        });
        super.onResume();
    }
}
