package com.softmeasures.eventezlyadminplus.frament;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.BounceAnimation;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;


public class f_transit extends Fragment {
    private static final String LOG_TAG = "ExampleApp";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyC6UabVHdti4zlU6E1iajVuNi6ZdKhDK5g";
    double latitude = 0.0, longitude = 0.0;
    ConnectionDetector cd;
    GoogleMap googleMap;
    MarkerOptions options;
    RelativeLayout rl_progressbar;
    ProgressBar progressBar;
    ArrayList<item> directionarray = new ArrayList<>();
    EditText edit_destition_address, edit_location, btn_destination, txt_current_location;
    ListView list_destination, list_location;
    TextView txt_search, txt_search_location;
    RelativeLayout rl_editname, rl_edit_location;
    TextView txt_get_direction, txt_start_location, txt_end_location;
    boolean opendes = false, valuesfound = false, opendes1 = false;
    String adrreslocation = "null", addressti, addressin, addressti1, addressin1;
    String addrsstitle = "null", addresssub, addresstodirectioncar, locationname1 = "null";
    ArrayList<item> searcharray = new ArrayList<>();
    ArrayList<item> oldarraylist1 = new ArrayList<>();
    String lat, lang, selectmode = "driving", curatlat, curtlang, endlag, endlat, cu, en;
    String origin[] = {"driving", "walking", "bicycling", "transit", "bus", "subway", "train", "tram", "rail"};
    boolean clikondestionation = false, clicknewwithstart = false;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    String source_lat = "0.0", source_lang = "0.0", des_lat = "0.0", des_lang = "0.0";
    ArrayList<item> searchplace = new ArrayList<>();
    AutoCompleteTextView autoCompView_des, autoCompView_sor;
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private Handler mHandler = null;
    private Runnable mAnimation;
    String click_on_move = "";

    public static ArrayList<item> autocomplete(String input) {
        ArrayList<item> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);

            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            resultList = new ArrayList<item>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                String cghcbdc = predsJsonArray.getJSONObject(i).getString("description");
                String jj = predsJsonArray.getJSONObject(i).getString("structured_formatting");
                JSONObject jjj = new JSONObject(jj);
                String main_address = jjj.getString("main_text");
                item ii = new item();
                ii.setLoationname(main_address);
                ii.setAddress(cghcbdc);
                resultList.add(ii);
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }


        return resultList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View view = inflater.inflate(R.layout.fragment_transit, container, false);
        mHandler = new Handler();
        autoCompView_des = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteTextView);
        autoCompView_sor = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteTextView1);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        list_destination = (ListView) view.findViewById(R.id.list_destinatin);
        list_location = (ListView) view.findViewById(R.id.list_location);
        rl_editname = (RelativeLayout) view.findViewById(R.id.rl_srart_location_search);
        rl_edit_location = (RelativeLayout) view.findViewById(R.id.rl_end_location);
        btn_destination = (EditText) view.findViewById(R.id.edit_destination1);
        edit_destition_address = (EditText) view.findViewById(R.id.edit_destination);
        edit_location = (EditText) view.findViewById(R.id.edit_location);
        txt_search = (TextView) view.findViewById(R.id.txt_search1);
        txt_search_location = (TextView) view.findViewById(R.id.txt_search_location1);
        txt_current_location = (EditText) view.findViewById(R.id.edit_location1);
        txt_get_direction = (TextView) view.findViewById(R.id.txt_get_directions);

        txt_end_location = (TextView) view.findViewById(R.id.btn_destination);
        txt_start_location = (TextView) view.findViewById(R.id.sp_current);
        rl_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        RelativeLayout top = (RelativeLayout) view.findViewById(R.id.top);
        top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        SharedPreferences sh1 = getActivity().getSharedPreferences("directiontomydirection", Context.MODE_PRIVATE);
        String lat = sh1.getString("lat", "0.0");
        String lang = sh1.getString("lang", "0.0");
        if (!lat.equals("0.0") && !lang.equals("0.0")) {
            getlatandlangtoaddress(Double.parseDouble(lat), Double.parseDouble(lang));
            SharedPreferences sh11 = getActivity().getSharedPreferences("directiontomydirection", Context.MODE_PRIVATE);
            sh11.edit().clear().commit();
        }
        cd = new ConnectionDetector(getActivity());
        if (cd.isConnectingToInternet()) {
            txt_end_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    list_location.setVisibility(View.GONE);
                    if (opendes == false) {
                        Resources rs;
                        CustomAdaptercity adpater = new CustomAdaptercity(getActivity(), oldarraylist1, rs = getResources());
                        list_destination.setAdapter(adpater);
                        rl_editname.setVisibility(View.GONE);
                        opendes = true;
                        list_destination.setVisibility(View.VISIBLE);
                    } else {
                        opendes = false;
                        list_destination.setVisibility(View.GONE);
                    }
                }
            });
            btn_destination.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() > 0) {
                        if (clikondestionation == false) {
                            txt_search.setVisibility(View.VISIBLE);
                        } else {
                            clikondestionation = false;
                            txt_search.setVisibility(View.GONE);
                        }

                    } else {
                        txt_search.setVisibility(View.GONE);
                    }
                }
            });

            txt_current_location.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() > 0) {
                        if (clikondestionation == false) {
                            txt_search_location.setVisibility(View.VISIBLE);
                        } else {
                            clikondestionation = false;
                            txt_search_location.setVisibility(View.GONE);
                        }
                    } else {
                        txt_search_location.setVisibility(View.GONE);
                    }
                }
            });

            txt_start_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list_destination.setVisibility(View.GONE);
                    if (opendes1 == false) {
                        Resources rs;
                        CustomAdaptercity1 adpater = new CustomAdaptercity1(getActivity(), oldarraylist1, rs = getResources());
                        list_location.setAdapter(adpater);
                        rl_editname.setVisibility(View.GONE);
                        opendes1 = true;
                        list_location.setVisibility(View.VISIBLE);
                    } else {
                        opendes1 = false;
                        list_location.setVisibility(View.GONE);
                    }
                }
            });

            txt_search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String a = btn_destination.getText().toString();
                    if (!a.equals("")) {
                        new getaddressfromapi(a).execute();
                        if (v != null) {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
            });
            txt_search_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String a = txt_current_location.getText().toString();
                    if (!a.equals("")) {
                        new getaddressfromapi1(a).execute();
                        if (v != null) {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
            });
            autoCompView_des.setAdapter(new CustomAutoplace(getActivity(), getResources()));
            autoCompView_des.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    addrsstitle = searchplace.get(position).getLoationname();
                    addresssub = searchplace.get(position).getAddress();
                    txt_end_location.setText(addresssub);
                    rl_editname.setVisibility(View.GONE);
                    autoCompView_des.setVisibility(View.GONE);
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                    new fetchLatLongFromService(addresssub).execute();
                }
            });

            autoCompView_sor.setAdapter(new CustomAutoplace(getActivity(), getResources()));
            autoCompView_sor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    addrsstitle = searchplace.get(position).getLoationname();
                    addresssub = searchplace.get(position).getAddress();
                    txt_start_location.setText(addresssub);
                    rl_edit_location.setVisibility(View.GONE);
                    autoCompView_sor.setVisibility(View.GONE);
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                    new fetchLatLongFromService1(addresssub).execute();
                }
            });

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                click_on_move = bundle.getString("clikc_on_map");
            }


            txt_get_direction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cu = txt_start_location.getText().toString();
                    en = txt_end_location.getText().toString();
                    if (cd.isConnectingToInternet()) {
                        if (!cu.equals("") && !en.equals("Select End Address")) {
                            if (!source_lat.equals("") && !source_lang.equals("") && !des_lat.equals("") && !des_lang.equals("")) {
                                final String curat = source_lat + "," + source_lang;
                                final String endla = des_lat + "," + des_lang;
                                new checklocation(addrsstitle, addresssub, curat, endla).execute();
                            }

                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("Incomplete Addresses");
                            alertDialog.setMessage("Please select Destination address to proceed");
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialog.show();
                        }
                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Internet Connection Required");
                        alertDialog.setMessage("Please connect to working Internet connection");
                        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        alertDialog.show();
                    }
                }

            });
            currentlocation();
            getaddress1(latitude, longitude);
            showmap();
            new getlocation().execute();
            source_lat = String.valueOf(latitude);
            source_lang = String.valueOf(longitude);
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Internet Connection Required");
            alertDialog.setMessage("Please connect to working Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }
        return view;
    }

    public void showmap() {
        final SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        fm.getMapAsync(googleMap1 -> {
            try {
                mapFragment = ((SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map));
                LatLng position = new LatLng(latitude, longitude);
                options = new MarkerOptions();
                options.position(position);
                Marker melbourne = googleMap.addMarker(new MarkerOptions()
                        .position(position));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 12));
                final long start = SystemClock.uptimeMillis();
                final long duration = 2000L;
                mHandler.removeCallbacks(mAnimation);
                mAnimation = new BounceAnimation(start, duration, melbourne, mHandler);
                mHandler.post(mAnimation);
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                }
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                map = googleMap;
            } catch (Exception e) {
                googleMap = googleMap1;
                map = googleMap;
            }
        });

    }

    public void currentlocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Failed to fetch user location!");
            alertDialog.setMessage("Please enable user location for app, location is required for app to work properly");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            alertDialog.show();
        }
    }

    private void getaddress1(Double lat, Double lon) {
        Geocoder gc = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = gc.getFromLocation(lat, lon, 1);
            StringBuilder sb = new StringBuilder();
            StringBuilder addressl = new StringBuilder();
            StringBuilder country = new StringBuilder();
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getLatitude()).append("\n");
                    sb.append(address.getLongitude());
                    addressl.append(address.getAddressLine(0)).append(", ");
                    addressl.append(address.getAddressLine(1));
                    country.append(address.getAddressLine(2));
                    break;
                }
                String loc = addressl.toString();
                Log.e("addrrss", loc);
                String countyname = country.toString();
                addressti1 = loc.substring(0, loc.indexOf(","));
                addressin1 = loc.substring(loc.indexOf(",") + 1);
                if (countyname.equals("null")) {
                    this.adrreslocation = addressti1 + ", " + addressin1;
                } else {
                    this.adrreslocation = addressti1 + ", " + addressin1 + ", " + countyname;
                }
                Resources rs;
                txt_start_location.setText(this.adrreslocation);
                rl_editname.setVisibility(View.GONE);
                list_location.setVisibility(View.GONE);
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Address not found!");
                alertDialog.setMessage("Could not find location. Try including the exact location information");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alertDialog.show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getlatandlangtoaddress(Double lat, Double lon) {
        Geocoder gc = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = gc.getFromLocation(lat, lon, 1);
            StringBuilder sb = new StringBuilder();
            StringBuilder addressl = new StringBuilder();
            StringBuilder country = new StringBuilder();
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getLatitude()).append("\n");
                    sb.append(address.getLongitude());
                    addressl.append(address.getAddressLine(0)).append(", ");
                    addressl.append(address.getAddressLine(1));
                    country.append(address.getAddressLine(2));
                    break;
                }
                String loc = addressl.toString();
                String countyname = country.toString();
                addressti1 = loc.substring(0, loc.indexOf(","));
                addressin1 = loc.substring(loc.indexOf(",") + 1);
                locationname1 = addressin1.substring(addressin1.indexOf(",") + 1);
                if (countyname.equals("null")) {
                    this.addresstodirectioncar = addressti1 + ", " + addressin1;
                } else {
                    this.addresstodirectioncar = addressti1 + "," + addressin1 + "," + countyname;
                }
                btn_destination.setText(this.addresstodirectioncar);

            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Address not found!");
                alertDialog.setMessage("Could not find location. Try including the exact location information");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alertDialog.show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                Log.i("dcnd", "Place: " + place.getName());
                LatLng latlang = place.getLatLng();
                lat = String.valueOf(latlang.latitude);
                lang = String.valueOf(latlang.longitude);
                addrsstitle = String.valueOf(place.getName());
                addresssub = String.valueOf(place.getAddress());
                if (clicknewwithstart) {
                    txt_start_location.setText(addrsstitle + " , " + addresssub);
                } else {
                    txt_end_location.setText(addrsstitle + " , " + addresssub);
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                // TODO: Handle the error.
                Log.i("eroor_place", status.getStatusMessage());

            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }

    public class getaddressfromapi extends AsyncTask<Void, Void, StringBuilder> {
        String place;
        String end;

        public getaddressfromapi(String end) {
            super();
            this.end = end;

        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String add = "null";
                if (!end.equals("") || !end.equals("null")) {
                    add = URLEncoder.encode(end);
                } else {
                    add = "null";
                }
                String googleMapUrl = "http://maps.googleapis.com/maps/api/geocode/json?address=" + add + "&sensor=false";
                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            try {
                if (result != null) {
                    JSONObject jsonObj = new JSONObject(result.toString());
                    String status = jsonObj.getString("status");

                    if (status.equals("OK")) {
                        rl_progressbar.setVisibility(View.GONE);
                        JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                        JSONObject before_geometry_jsonObj = resultJsonArray
                                .getJSONObject(0);
                        String addgtr = before_geometry_jsonObj.getString("formatted_address");
                        JSONObject geometry_jsonObj = before_geometry_jsonObj
                                .getJSONObject("geometry");
                        JSONObject location_jsonObj = geometry_jsonObj
                                .getJSONObject("location");
                        endlat = location_jsonObj.getString("lat");
                        double lat = Double.valueOf(endlat);
                        endlag = location_jsonObj.getString("lng");
                        double lng = Double.valueOf(endlag);
                        LatLng point = new LatLng(lat, lng);
                        Resources rs = getResources();
                        addressti = addgtr.substring(0, addgtr.indexOf(","));
                        addressin = addgtr.substring(addgtr.indexOf(",") + 1);
                        searcharray.clear();
                        item iii = new item();
                        iii.setTitle_address(addressti);
                        iii.setAddress(addressin);
                        iii.setLat(endlat);
                        iii.setLang(endlag);
                        searcharray.add(iii);
                        rl_editname.setVisibility(View.GONE);
                        list_destination.setVisibility(View.VISIBLE);
                        CustomAdaptercity adpater = new CustomAdaptercity(getActivity(), searcharray, rs);
                        list_destination.setAdapter(adpater);
                    } else {
                        rl_progressbar.setVisibility(View.GONE);
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Incomplete Addresses");
                        alertDialog.setMessage("Please select Destination address to proceed");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }
                } else {
                    rl_progressbar.setVisibility(View.GONE);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
            }
        }
    }

    public class getaddressfromapi1 extends AsyncTask<Void, Void, StringBuilder> {
        String place;
        String end;

        public getaddressfromapi1(String end) {
            super();
            this.end = end;

        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            this.cancel(true);
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String add = "null";
                if (!end.equals("") || !end.equals("null")) {
                    add = URLEncoder.encode(end);
                } else {
                    add = "null";
                }
                String googleMapUrl = "http://maps.googleapis.com/maps/api/geocode/json?address=" + add + "&sensor=false";
                URL url = new URL(googleMapUrl);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(
                        conn.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            try {
                if (result != null) {
                    JSONObject jsonObj = new JSONObject(result.toString());
                    String status = jsonObj.getString("status");

                    if (status.equals("OK")) {
                        rl_progressbar.setVisibility(View.GONE);
                        JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                        JSONObject before_geometry_jsonObj = resultJsonArray
                                .getJSONObject(0);
                        String addgtr = before_geometry_jsonObj.getString("formatted_address");
                        JSONObject geometry_jsonObj = before_geometry_jsonObj
                                .getJSONObject("geometry");
                        JSONObject location_jsonObj = geometry_jsonObj
                                .getJSONObject("location");
                        endlat = location_jsonObj.getString("lat");
                        double lat = Double.valueOf(endlat);
                        endlag = location_jsonObj.getString("lng");
                        double lng = Double.valueOf(endlag);
                        Resources rs = getResources();
                        addressti = addgtr.substring(0, addgtr.indexOf(","));
                        addressin = addgtr.substring(addgtr.indexOf(",") + 1);
                        searcharray.clear();
                        item iii = new item();
                        iii.setTitle_address(addressti);
                        iii.setAddress(addressin);
                        iii.setLat(endlat);
                        iii.setLang(endlag);
                        searcharray.add(iii);
                        rl_edit_location.setVisibility(View.GONE);
                        list_location.setVisibility(View.VISIBLE);
                        CustomAdaptercity1 adpater = new CustomAdaptercity1(getActivity(), searcharray, rs = getResources());
                        list_location.setAdapter(adpater);
                    } else {
                        rl_progressbar.setVisibility(View.GONE);
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Incomplete Addresses");
                        alertDialog.setMessage("Please select Destination address to proceed");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }
                } else {
                    rl_progressbar.setVisibility(View.GONE);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
            }
        }
    }

    public class checklocation extends AsyncTask<String, String, String> {
        JSONObject json, json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        String street, address, currentlocation, endlocation;
        JSONArray array = new JSONArray();

        public checklocation(String street, String location, String currentlocation, String endlocation) {
            this.street = street;
            this.address = location;
            this.currentlocation = currentlocation;
            this.endlocation = endlocation;
        }

        @Override
        protected void onPreExecute() {

            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            HttpResponse response = null;
            try {
                String add = "null";
                if (!address.equals("") || !address.equals("null")) {
                    address = address.replaceAll("(\n)", "%0A");
                    add = address.replaceAll(" ", "%20");
                    ;
                } else {
                    add = "null";
                }

                String street1 = "null";
                if (!street.equals("") || !street.equals("null")) {

                    street = street.replaceAll("(\n)", "%0A");
                    street1 = street.replaceAll(" ", "%20");

                } else {
                    street1 = "null";
                }
                String vechiclenourl = "_table/user_locations?filter=(location_name%3D" + street1 + ")%20AND%20(location_address%3D" + add + ")%20AND%20(user_id%3D" + id + ")";
                String url = getString(R.string.api) + getString(R.string.povlive) + vechiclenourl;
                DefaultHttpClient client = new DefaultHttpClient();
                HttpGet post = new HttpGet(url);
                post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
                post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    array = json.getJSONArray("resource");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            if (getActivity() != null && json != null) {
                if (array.length() > 0) {

                    SharedPreferences ss = getActivity().getSharedPreferences("show_direction", Context.MODE_PRIVATE);
                    SharedPreferences.Editor ed = ss.edit();
                    ed.putString("current", cu);
                    ed.putString("des", en);
                    ed.putString("mode", selectmode);
                    ed.putString("current_lat", currentlocation);
                    ed.commit();
                    Bundle b = new Bundle();
                    b.putString("click", click_on_move);
                    show_transit sd = new show_transit();
                    sd.setArguments(b);
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                   /* ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                    ft.replace(R.id.My_Container_1_ID, sd, "NewFragmentTag");
                    ft.commitAllowingStateLoss();*/

                    ft.add(R.id.My_Container_1_ID, sd);
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    fragmentStack.push(sd);
                    ft.commit();
                } else {
                    new adddestination(currentlocation, endlocation).execute();
                }

            }
            super.onPostExecute(s);

        }


    }

    public class adddestination extends AsyncTask<String, String, String> {
        String ss, res_id;
        JSONObject json;
        String addvehicleurl = "_table/user_locations";
        String end, currentlocation, endlocation;

        public adddestination(String currentlocation, String endlocation) {
            this.currentlocation = currentlocation;
            this.endlocation = endlocation;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            directionarray.clear();
            SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
            String user_id = logindeatl.getString("id", "0");
            Map<String, Object> jsonValues = new HashMap<String, Object>();
            jsonValues.put("lng", des_lang);
            jsonValues.put("lat", des_lat);
            jsonValues.put("user_id", Integer.parseInt(user_id));
            if (!addrsstitle.equals("null")) {
                jsonValues.put("location_name", addrsstitle);
                addresssub = addresssub.replaceFirst(" ", "");
                jsonValues.put("location_address", addresssub);
            } else {
                addresssub = addresssub.replaceFirst(" ", "");
                jsonValues.put("location_name", addresssub);
                jsonValues.put("location_address", addresssub);
            }
            JSONObject json = new JSONObject(jsonValues);
            String url = getString(R.string.api) + getString(R.string.povlive) + addvehicleurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = null;
            HttpPut put = null;
            post = new HttpPost(url);
            Log.d("ccc", json.toString());
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(entity);

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item iii = new item();
                        JSONObject c = array.getJSONObject(i);
                        res_id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ss = String.valueOf(response.getStatusLine());
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && !res_id.equals("null")) {
                SharedPreferences ss = getActivity().getSharedPreferences("show_direction", Context.MODE_PRIVATE);
                SharedPreferences.Editor ed = ss.edit();
                ed.putString("current", cu);
                ed.putString("des", en);
                ed.putString("mode", selectmode);
                ed.putString("current_lat", currentlocation);
                ed.commit();
                Bundle b = new Bundle();
                b.putString("click", click_on_move);
                show_transit sd = new show_transit();
                sd.setArguments(b);
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
              /*  ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.replace(R.id.My_Container_1_ID, sd, "NewFragmentTag");
                ft.commit();*/

                ft.add(R.id.My_Container_1_ID, sd);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(sd);
                ft.commit();
            }
            super.onPostExecute(s);
        }
    }

    public class CustomAdaptercity extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdaptercity(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {
                vi = inflater.inflate(R.layout.destinationad, null);
                holder = new ViewHolder();
                holder.txt_address = (TextView) vi.findViewById(R.id.text_address);
                holder.txt_address_title = (TextView) vi.findViewById(R.id.text_address_title);
                holder.txt_lat = (TextView) vi.findViewById(R.id.txt_lat);
                holder.txt_lang = (TextView) vi.findViewById(R.id.txt_lng);
                /************  Set holder with LayoutInflater ************/
                vi.setTag(holder);
                vi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String address = holder.txt_address_title.getText().toString();
                        if (address.equals("Enter New Address")) {
                            rl_edit_location.setVisibility(View.VISIBLE);
                            autoCompView_des.setVisibility(View.VISIBLE);
                            autoCompView_des.setText("");
                            txt_end_location.setText("Select End address");
                            clicknewwithstart = false;
                            list_destination.setVisibility(View.GONE);
                            rl_editname.setVisibility(View.GONE);
                        } else {
                            if (v != null) {
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                            }
                            clikondestionation = true;
                            addrsstitle = address;
                            String address1 = holder.txt_address.getText().toString();
                            addresssub = address1;
                            address1.replaceFirst("\\s++$", "");
                            address.replaceFirst("\\s++$", "");
                            addrsstitle.replaceFirst("\\s++$", "");
                            addresssub.replaceFirst("\\s++$", "");
                            addrsstitle = addrsstitle.replaceAll("(\n)", "%0A");
                            addresssub = addresssub.replaceAll("(\n)", "%0A");
                            des_lat = holder.txt_lat.getText().toString();
                            des_lang = holder.txt_lang.getText().toString();
                            txt_end_location.setText(address1);
                            list_destination.setVisibility(View.GONE);
                            rl_edit_location.setVisibility(View.GONE);
                        }
                    }
                });

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                holder.txt_address_title.setText(tempValues.getTitle_address());
                String d = tempValues.getTitle_address();
                if (!d.equals("null")) {
                    vi.setVisibility(View.VISIBLE);
                    holder.txt_address_title.setText(tempValues.getTitle_address());
                    if (d.equals("Enter New Address")) {
                        vi.setBackgroundColor(Color.parseColor("#90caf9"));
                        holder.txt_address.setText("");
                    } else {
                        if (tempValues.isclcik()) {
                            vi.setBackgroundColor(Color.parseColor("#2F5CF2"));
                        } else {
                            vi.setBackgroundColor(Color.parseColor("#ffffff"));
                        }
                        if (tempValues.getAddress().equals("null")) {
                            holder.txt_address.setText("");
                        } else {
                            holder.txt_address.setText(tempValues.getAddress());
                        }
                    }

                    holder.txt_lat.setText(tempValues.getLat());
                    holder.txt_lang.setText(tempValues.getLang());
                } else {
                    vi.setVisibility(View.GONE);
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txt_address_title, txt_address, txt_lat, txt_lang;
        }
    }

    public class CustomAdaptercity1 extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdaptercity1(Activity a, ArrayList<item> d, Resources resLocal) {

            activity = a;
            context = a;
            data = d;
            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.destinationad, null);
                holder = new ViewHolder();

                holder.txt_address = (TextView) vi.findViewById(R.id.text_address);
                holder.txt_address_title = (TextView) vi.findViewById(R.id.text_address_title);
                holder.txt_lat = (TextView) vi.findViewById(R.id.txt_lat);
                holder.txt_lang = (TextView) vi.findViewById(R.id.txt_lng);
                vi.setTag(holder);

                vi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String address = holder.txt_address_title.getText().toString();

                        if (address.equals("Enter New Address")) {
                            clicknewwithstart = true;
                            list_location.setVisibility(View.GONE);
                            rl_editname.setVisibility(View.VISIBLE);
                            txt_start_location.setText("Select start address");
                            list_location.setVisibility(View.GONE);
                            autoCompView_sor.setVisibility(View.VISIBLE);
                            autoCompView_sor.setText("");
                            rl_edit_location.setVisibility(View.GONE);

                        } else {
                            addrsstitle = address;
                            clikondestionation = true;
                            String address1 = holder.txt_address.getText().toString();
                            addresssub = address1;
                            address1.replaceFirst("\\s++$", "");
                            address.replaceFirst("\\s++$", "");
                            addrsstitle.replaceFirst("\\s++$", "");
                            addresssub.replaceFirst("\\s++$", "");
                            addrsstitle = addrsstitle.replaceAll("(\n)", "%0A");
                            addresssub = addresssub.replaceAll("(\n)", "%0A");
                            txt_start_location.setText(address1);
                            source_lat = holder.txt_lat.getText().toString();
                            source_lang = holder.txt_lang.getText().toString();
                            list_location.setVisibility(View.GONE);
                            rl_editname.setVisibility(View.GONE);
                        }
                    }
                });
            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
            } else {
                tempValues = null;
                tempValues = (item) data.get(position);
                String d = tempValues.getTitle_address();
                if (!d.equals("null")) {
                    vi.setVisibility(View.VISIBLE);
                    holder.txt_address_title.setText(tempValues.getTitle_address());
                    if (d.equals("Enter New Address")) {
                        vi.setBackgroundColor(Color.parseColor("#90caf9"));
                        holder.txt_address.setText("");
                    } else {
                        if (tempValues.isclcik()) {
                            vi.setBackgroundColor(Color.parseColor("#2F5CF2"));
                        } else {
                            vi.setBackgroundColor(Color.parseColor("#ffffff"));
                        }
                        if (tempValues.getAddress().equals("null")) {
                            holder.txt_address.setText("");
                        } else {
                            holder.txt_address.setText(tempValues.getAddress());
                        }
                    }
                    holder.txt_lat.setText(tempValues.getLat());
                    holder.txt_lang.setText(tempValues.getLang());

                } else {
                    vi.setVisibility(View.GONE);
                }
            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {
            public TextView txt_address_title, txt_address, txt_lat, txt_lang;


        }
    }

    public class getlocation extends AsyncTask<String, String, String> {
        JSONObject json = null;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "0");
        String vehiclenourl = "_table/user_locations?filter=(user_id%3D" + user_id + ")";
        String res_id;

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            directionarray.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + vehiclenourl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    item ii = new item();
                    ii.setTitle_address("Enter New Address");
                    ii.setAddress("null");
                    ii.setIsclcik(false);
                    oldarraylist1.add(ii);
                    if (adrreslocation.contains(",")) {
                        String addd = adrreslocation.substring(0, adrreslocation.indexOf(","));
                        String tit = adrreslocation.substring(adrreslocation.indexOf(",") + 1);
                        String hh = tit.substring(tit.indexOf(" ") + 1);
                        item cu = new item();
                        cu.setTitle_address(addd);
                        cu.setAddress(hh);
                        cu.setLat(String.valueOf(latitude));
                        cu.setLang(String.valueOf(longitude));
                        cu.setIsclcik(true);
                        oldarraylist1.add(cu);
                    } else {
                        item cu = new item();
                        cu.setTitle_address(adrreslocation);
                        cu.setAddress(adrreslocation);
                        cu.setLat(String.valueOf(latitude));
                        cu.setLang(String.valueOf(longitude));
                        cu.setIsclcik(true);
                        oldarraylist1.add(cu);
                    }

                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item iii = new item();
                        JSONObject c = array.getJSONObject(i);
                        res_id = c.getString("id");
                        String address = c.getString("location_address");
                        String addressname = c.getString("location_name");
                        iii.setTitle_address(addressname);
                        iii.setAddress(address);
                        iii.setLat(c.getString("lat"));
                        iii.setLang(c.getString("lng"));
                        ii.setIsclcik(false);
                        oldarraylist1.add(iii);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null && progressBar != null) {
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
            Resources rs;
            if (getActivity() != null && json != null) {
                Activity activity = getActivity();
                if (activity == null) {
                } else {
                    if (json != null) {
                        CustomAdaptercity1 adpater = new CustomAdaptercity1(getActivity(), oldarraylist1, rs = getResources());
                        list_location.setAdapter(adpater);
                    } else {
                        item ii = new item();
                        ii.setTitle_address("Enter New Address");
                        ii.setAddress("null");
                        ii.setIsclcik(false);
                        oldarraylist1.add(ii);
                        CustomAdaptercity1 adpater = new CustomAdaptercity1(getActivity(), oldarraylist1, rs = getResources());
                        list_location.setAdapter(adpater);
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class fetchLatLongFromService extends AsyncTask<Void, Void, StringBuilder> {
        String place;
        String end;

        public fetchLatLongFromService(String place) {
            super();
            this.place = place;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "http://maps.googleapis.com/maps/api/geocode/json?address="
                        + this.place + "&sensor=false";
                googleMapUrl = googleMapUrl.replaceAll(" ", "%20");
                URL url = new URL(googleMapUrl);
                URLConnection c = url.openConnection();
                c.setConnectTimeout(5000);
                c.setReadTimeout(10000);
                c.getInputStream();
                InputStreamReader in = new InputStreamReader(
                        c.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);

                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");
                    JSONObject before_geometry_jsonObj = resultJsonArray.getJSONObject(0);
                    JSONObject geometry_jsonObj = before_geometry_jsonObj.getJSONObject("geometry");
                    JSONObject location_jsonObj = geometry_jsonObj.getJSONObject("location");
                    lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);
                    lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);
                    DecimalFormat df = new DecimalFormat("0.000000");
                    Double l = lng;
                    lang = df.format(l);
                    LatLng point = new LatLng(law, lng);
                    des_lang = lang;
                    des_lat = lat;
                } else {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                rl_progressbar.setVisibility(View.GONE);
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (Exception e) {
                rl_progressbar.setVisibility(View.GONE);
            }
        }

    }

    public class CustomAutoplace extends BaseAdapter implements View.OnClickListener, Filterable {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAutoplace(Activity a, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;

            res = resLocal;
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.layout, null);
                holder = new ViewHolder();

                holder.txt_commerical_name = (TextView) vi.findViewById(R.id.txt_name);
                holder.txt_deatil = (TextView) vi.findViewById(R.id.address);
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }
            if (data.size() <= 0) {
            } else {
                try {
                    tempValues = null;
                    tempValues = (item) data.get(position);
                    holder.txt_commerical_name.setText(tempValues.getLoationname());
                    holder.txt_deatil.setText(tempValues.getAddress());
                    int k = 0;
                } catch (IndexOutOfBoundsException e) {

                } catch (Exception e) {
                }


            }

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        data = autocomplete(constraint.toString());
                        filterResults.values = data;
                        filterResults.count = data.size();
                        searchplace.clear();
                        searchplace.addAll(data);
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        try {
                            notifyDataSetChanged();
                        } catch (Exception e) {
                        }

                    } else {
                        try {
                            notifyDataSetInvalidated();
                        } catch (Exception e) {
                        }

                    }
                }
            };
            return filter;

        }


        public class ViewHolder {

            public TextView txt_commerical_name, txt_deatil;
        }
    }

    public class fetchLatLongFromService1 extends AsyncTask<Void, Void, StringBuilder> {
        String place;

        public fetchLatLongFromService1(String place) {
            super();
            this.place = place;
        }

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected StringBuilder doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                HttpURLConnection conn = null;
                StringBuilder jsonResults = new StringBuilder();
                String googleMapUrl = "http://maps.googleapis.com/maps/api/geocode/json?address=" + this.place + "&sensor=false";
                googleMapUrl = googleMapUrl.replaceAll(" ", "%20");
                URL url = new URL(googleMapUrl);
                URLConnection c = url.openConnection();
                c.setConnectTimeout(5000);
                c.setReadTimeout(10000);
                c.getInputStream();
                InputStreamReader in = new InputStreamReader(c.getInputStream());
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
                String a = "";
                return jsonResults;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(StringBuilder result) {
            // TODO Auto-generated method stub
            try {
                JSONObject jsonObj = new JSONObject(result.toString());
                String status = jsonObj.getString("status");
                rl_progressbar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                if (status.equals("OK")) {

                    JSONArray resultJsonArray = jsonObj.getJSONArray("results");

                    JSONObject before_geometry_jsonObj = resultJsonArray.getJSONObject(0);
                    JSONObject geometry_jsonObj = before_geometry_jsonObj.getJSONObject("geometry");
                    JSONObject location_jsonObj = geometry_jsonObj.getJSONObject("location");
                    lat = location_jsonObj.getString("lat");
                    double law = Double.valueOf(lat);
                    lang = location_jsonObj.getString("lng");
                    double lng = Double.valueOf(lang);
                    DecimalFormat df = new DecimalFormat("0.000000");
                    Double l = lng;
                    lang = df.format(l);
                    LatLng point = new LatLng(law, lng);
                    source_lang = lang;
                    source_lat = lat;


                } else {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Incomplete Addresses");
                    alertDialog.setMessage("Please select Destination address to proceed");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            } catch (JSONException e) {
                rl_progressbar.setVisibility(View.GONE);
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (Exception e) {
                rl_progressbar.setVisibility(View.GONE);
            }
        }

    }
}
