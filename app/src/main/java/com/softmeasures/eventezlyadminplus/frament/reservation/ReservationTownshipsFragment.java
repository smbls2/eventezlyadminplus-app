package com.softmeasures.eventezlyadminplus.frament.reservation;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.ReserveParking;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;
import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class ReservationTownshipsFragment extends BaseFragment {

    private ListView list_managed_lots;
    private RelativeLayout rlProgressbar;
    private TextView tvError, tvTitle;
    private ArrayList<item> managed_space_array = new ArrayList<>();
    private ArrayList<ReserveParking> reserveParkings = new ArrayList<>();
    private boolean isMyReservations = false;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            isMyReservations = getArguments().getBoolean("isMyReservations", false);
        }
        return inflater.inflate(R.layout.frag_reservation_townships, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        new fetchReservedParking().execute();

    }

    public class fetchTownships extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json = new JSONObject();
        JSONArray json1 = new JSONArray();
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String tcode = logindeatl.getString("twcode", "FDV");
        String role = logindeatl.getString("role", "");

        String managedlosturl = "_table/township_parking_partners?filter=(township_id%3D" + tcode + ")%20AND%20((marker_type%3DManaged%20Paid)%20OR%20(marker_type%3DManaged%20Free))";

        @Override
        protected void onPreExecute() {
            role = logindeatl.getString("role", "");
            if (!TextUtils.isEmpty(role) && role.equals("SuperAdmin")) {
                managedlosturl = "_table/township_parking_partners?filter=(marker_type%3DManaged%20Paid)%20OR%20(marker_type%3DManaged%20Free)";
            } else if (!TextUtils.isEmpty(role) && role.equals("TwpAdmin")) {
                managedlosturl = "_table/township_parking_partners?filter=(township_id%3D" + tcode + ")%20AND%20((marker_type%3DManaged%20Paid)%20OR%20(marker_type%3DManaged%20Free))";
            } else {
                managedlosturl = "_table/township_parking_partners?filter=(marker_type%3DManaged%20Paid)%20OR%20(marker_type%3DManaged%20Free)";
            }
            rlProgressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            managed_space_array.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("managed_lots", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;

                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    json1 = json.getJSONArray("resource");

                    for (int i = 0; i < json1.length(); i++) {
                        item item = new item();
                        JSONObject josnget = json1.getJSONObject(i);
                        String lat = josnget.getString("lat");
                        String lag = josnget.getString("lng");
                        String id = josnget.getString("id");
                        String title = josnget.getString("title");
                        String address = josnget.getString("address");
                        String html = "";
                        String category = "";
                        String marker = josnget.getString("marker_type");
                        String location_code = josnget.getString("location_code");
                        String location_id = josnget.getString("location_id");
                        String location_name = josnget.getString("location_name");
                        if (marker.equals("Managed Paid") || marker.equals("Managed Free")) {
                            item.setId(id);
                            item.setLots_aval(josnget.getString("lots_avbl"));
                            item.setTotal_lots(josnget.getString("lots_total"));
                            item.setNo_parking_times(josnget.getString("no_parking_times"));
                            item.setParking_times(josnget.getString("parking_times"));
                            item.setRenew(josnget.getString("renewable"));
                            item.setWeek_ebd_discount(josnget.getString("weekend_special_diff"));
                            item.setOff_peak_discount(josnget.getString("off_peak_discount"));
                            item.setOff_peak_start(josnget.getString("off_peak_starts"));
                            item.setOff_peak_end(josnget.getString("off_peak_ends"));
                            item.setCustom_notice(josnget.getString("custom_notice"));
                            item.setEffect(josnget.getString("in_effect"));
                            item.setLocation_name(location_name);
                            item.setLang(lag);
                            item.setLat(lat);
                            item.setTitle(title);
                            item.setAddress(address);
                            item.setUrl("");
                            item.setHtml(html);
                            item.setCategory(category);
                            item.setMarker(marker);
                            item.setLocation_code(location_code);
                            item.setLocation_place_id(josnget.getString("location_place_id"));
                            item.setLocation_id(josnget.getString("location_id"));
                            if (reserveParkings.contains(new ReserveParking(item.getLocation_code()))) {
                                managed_space_array.add(item);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null) {
                rlProgressbar.setVisibility(View.GONE);
                if (json != null) {
                    if (managed_space_array != null) {
                        if (managed_space_array.size() > 0) {
                            tvError.setVisibility(View.GONE);
                            list_managed_lots.setAdapter(new CustomAdapterManaged_Lost(getActivity(), managed_space_array, getResources()));
                        } else {
                            tvError.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    private class fetchReservedParking extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        String tcode = logindeatl.getString("twcode", "FDV");
        String role = logindeatl.getString("role", "");
        JSONObject json;
        JSONArray json1;
        String reservedParkingUrl = "_table/reserved_parkings";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rlProgressbar.setVisibility(View.VISIBLE);

            if (!TextUtils.isEmpty(role) && role.equals("SuperAdmin")) {
                reservedParkingUrl = "_table/reserved_parkings";
            } else if (!TextUtils.isEmpty(role) && role.equals("TwpAdmin")) {
                reservedParkingUrl = "_table/reserved_parkings?filter=(township_code%3D" + tcode + ")";
            } else {
                reservedParkingUrl = "_table/reserved_parkings?filter=(user_id%3D" + id + ")";
            }

            reserveParkings.clear();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + reservedParkingUrl;
            Log.e("#DEBUG", "      fetchReserveParking:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");

                    for (int j = 0; j < jsonArray.length(); j++) {
                        ReserveParking parkingUpdate = new ReserveParking();
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            parkingUpdate.setId(object.getString("id"));
                        }
                        if (object.has("company_type_id")
                                && !TextUtils.isEmpty(object.getString("company_type_id"))
                                && !object.getString("company_type_id").equals("null")) {
                            parkingUpdate.setCompany_type_id(object.getString("company_type_id"));
                        }
                        if (object.has("company_type")
                                && !TextUtils.isEmpty(object.getString("company_type"))
                                && !object.getString("company_type").equals("null")) {
                            parkingUpdate.setCompany_type(object.getString("company_type"));
                        }
                        if (object.has("manager_type_id")
                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
                                && !object.getString("manager_type_id").equals("null")) {
                            parkingUpdate.setManager_type_id(object.getString("manager_type_id"));
                        }
                        if (object.has("manager_type")
                                && !TextUtils.isEmpty(object.getString("manager_type"))
                                && !object.getString("manager_type").equals("null")) {
                            parkingUpdate.setManager_type(object.getString("manager_type"));
                        }
                        if (object.has("company_id")
                                && !TextUtils.isEmpty(object.getString("company_id"))
                                && !object.getString("company_id").equals("null")) {
                            parkingUpdate.setCompany_id(object.getString("company_id"));
                        }
                        if (object.has("parking_type")
                                && !TextUtils.isEmpty(object.getString("parking_type"))
                                && !object.getString("parking_type").equals("null")) {
                            parkingUpdate.setParking_type(object.getString("parking_type"));
                        }
                        if (object.has("twp_id")
                                && !TextUtils.isEmpty(object.getString("twp_id"))
                                && !object.getString("twp_id").equals("null")) {
                            parkingUpdate.setTwp_id(object.getString("twp_id"));
                        }
                        if (object.has("township_code")
                                && !TextUtils.isEmpty(object.getString("township_code"))
                                && !object.getString("township_code").equals("null")) {
                            parkingUpdate.setTownship_code(object.getString("township_code"));
                        }
                        if (object.has("location_id")
                                && !TextUtils.isEmpty(object.getString("location_id"))
                                && !object.getString("location_id").equals("null")) {
                            parkingUpdate.setLocation_id(object.getString("location_id"));
                        }
                        if (object.has("location_code")
                                && !TextUtils.isEmpty(object.getString("location_code"))
                                && !object.getString("location_code").equals("null")) {
                            parkingUpdate.setLocation_code(object.getString("location_code"));
                        }
                        if (object.has("lot_row")
                                && !TextUtils.isEmpty(object.getString("lot_row"))
                                && !object.getString("lot_row").equals("null")) {
                            parkingUpdate.setLot_row(object.getString("lot_row"));
                        }
                        if (object.has("lot_number")
                                && !TextUtils.isEmpty(object.getString("lot_number"))
                                && !object.getString("lot_number").equals("null")) {
                            parkingUpdate.setLot_number(object.getString("lot_number"));
                        }
                        if (object.has("lot_id")
                                && !TextUtils.isEmpty(object.getString("lot_id"))
                                && !object.getString("lot_id").equals("null")) {
                            parkingUpdate.setLot_id(object.getString("lot_id"));
                        }
                        if (object.has("reservation_date_time")
                                && !TextUtils.isEmpty(object.getString("reservation_date_time"))
                                && !object.getString("reservation_date_time").equals("null")) {
                            parkingUpdate.setReservation_date_time(object.getString("reservation_date_time"));
                        }
                        if (object.has("reserve_entry_time")
                                && !TextUtils.isEmpty(object.getString("reserve_entry_time"))
                                && !object.getString("reserve_entry_time").equals("null")) {
                            parkingUpdate.setReserve_entry_time(object.getString("reserve_entry_time"));
                        }
                        if (object.has("reserve_expiry_time")
                                && !TextUtils.isEmpty(object.getString("reserve_expiry_time"))
                                && !object.getString("reserve_expiry_time").equals("null")) {
                            parkingUpdate.setReserve_expiry_time(object.getString("reserve_expiry_time"));
                        }
                        if (object.has("reserve_exit_date_time")
                                && !TextUtils.isEmpty(object.getString("reserve_exit_date_time"))
                                && !object.getString("reserve_exit_date_time").equals("null")) {
                            parkingUpdate.setReserve_exit_date_time(object.getString("reserve_exit_date_time"));
                        }
                        if (object.has("reserve_payment_condition")
                                && !TextUtils.isEmpty(object.getString("reserve_payment_condition"))
                                && !object.getString("reserve_payment_condition").equals("null")) {
                            parkingUpdate.setReserve_payment_condition(object.getString("reserve_payment_condition"));
                        }
                        if (object.has("reserve_payment_due_by")
                                && !TextUtils.isEmpty(object.getString("reserve_payment_due_by"))
                                && !object.getString("reserve_payment_due_by").equals("null")) {
                            parkingUpdate.setReserve_payment_due_by(object.getString("reserve_payment_due_by"));
                        }
                        if (object.has("reserve_payment_paid")
                                && !TextUtils.isEmpty(object.getString("reserve_payment_paid"))
                                && !object.getString("reserve_payment_paid").equals("null")) {
                            parkingUpdate.setReserve_payment_paid(object.getString("reserve_payment_paid"));
                        }
                        if (object.has("reservation_status")
                                && !TextUtils.isEmpty(object.getString("reservation_status"))
                                && !object.getString("reservation_status").equals("null")) {
                            parkingUpdate.setReservation_status(object.getString("reservation_status"));
                        }
                        if (object.has("entry_date_time")
                                && !TextUtils.isEmpty(object.getString("entry_date_time"))
                                && !object.getString("entry_date_time").equals("null")) {
                            parkingUpdate.setEntry_date_time(object.getString("entry_date_time"));
                        }
                        if (object.has("exit_date_time")
                                && !TextUtils.isEmpty(object.getString("exit_date_time"))
                                && !object.getString("exit_date_time").equals("null")) {
                            parkingUpdate.setExit_date_time(object.getString("exit_date_time"));
                        }
                        if (object.has("expiry_time")
                                && !TextUtils.isEmpty(object.getString("expiry_time"))
                                && !object.getString("expiry_time").equals("null")) {
                            parkingUpdate.setExpiry_time(object.getString("expiry_time"));
                        }
                        if (object.has("user_id")
                                && !TextUtils.isEmpty(object.getString("user_id"))
                                && !object.getString("user_id").equals("null")) {
                            parkingUpdate.setUser_id(object.getString("user_id"));
                        }
                        if (object.has("permit_id")
                                && !TextUtils.isEmpty(object.getString("permit_id"))
                                && !object.getString("permit_id").equals("null")) {
                            parkingUpdate.setPermit_id(object.getString("permit_id"));
                        }
                        if (object.has("subscription_id")
                                && !TextUtils.isEmpty(object.getString("subscription_id"))
                                && !object.getString("subscription_id").equals("null")) {
                            parkingUpdate.setSubscription_id(object.getString("subscription_id"));
                        }
                        if (object.has("plate_no")
                                && !TextUtils.isEmpty(object.getString("plate_no"))
                                && !object.getString("plate_no").equals("null")) {
                            parkingUpdate.setPlate_no(object.getString("plate_no"));
                        }
                        if (object.has("pl_state")
                                && !TextUtils.isEmpty(object.getString("pl_state"))
                                && !object.getString("pl_state").equals("null")) {
                            parkingUpdate.setPl_state(object.getString("pl_state"));
                        }
                        if (object.has("pl_country")
                                && !TextUtils.isEmpty(object.getString("pl_country"))
                                && !object.getString("pl_country").equals("null")) {
                            parkingUpdate.setPl_country(object.getString("pl_country"));
                        }
                        if (object.has("lat")
                                && !TextUtils.isEmpty(object.getString("lat"))
                                && !object.getString("lat").equals("null")) {
                            parkingUpdate.setLat(object.getString("lat"));
                        }
                        if (object.has("lng")
                                && !TextUtils.isEmpty(object.getString("lng"))
                                && !object.getString("lng").equals("null")) {
                            parkingUpdate.setLng(object.getString("lng"));
                        }
                        if (object.has("address1")
                                && !TextUtils.isEmpty(object.getString("address1"))
                                && !object.getString("address1").equals("null")) {
                            parkingUpdate.setAddress1(object.getString("address1"));
                        }
                        if (object.has("address2")
                                && !TextUtils.isEmpty(object.getString("address2"))
                                && !object.getString("address2").equals("null")) {
                            parkingUpdate.setAddress2(object.getString("address2"));
                        }
                        if (object.has("city")
                                && !TextUtils.isEmpty(object.getString("city"))
                                && !object.getString("city").equals("null")) {
                            parkingUpdate.setCity(object.getString("city"));
                        }
                        if (object.has("state")
                                && !TextUtils.isEmpty(object.getString("state"))
                                && !object.getString("state").equals("null")) {
                            parkingUpdate.setState(object.getString("state"));
                        }
                        if (object.has("zip")
                                && !TextUtils.isEmpty(object.getString("zip"))
                                && !object.getString("zip").equals("null")) {
                            parkingUpdate.setZip(object.getString("zip"));
                        }
                        if (object.has("country")
                                && !TextUtils.isEmpty(object.getString("country"))
                                && !object.getString("country").equals("null")) {
                            parkingUpdate.setCountry(object.getString("country"));
                        }
                        if (object.has("marker_lng")
                                && !TextUtils.isEmpty(object.getString("marker_lng"))
                                && !object.getString("marker_lng").equals("null")) {
                            parkingUpdate.setMarker_lng(object.getString("marker_lng"));
                        }
                        if (object.has("marker_lat")
                                && !TextUtils.isEmpty(object.getString("marker_lat"))
                                && !object.getString("marker_lat").equals("null")) {
                            parkingUpdate.setMarker_lat(object.getString("marker_lat"));
                        }
                        if (object.has("marker_address1")
                                && !TextUtils.isEmpty(object.getString("marker_address1"))
                                && !object.getString("marker_address1").equals("null")) {
                            parkingUpdate.setMarker_address1(object.getString("marker_address1"));
                        }
                        if (object.has("marker_address2")
                                && !TextUtils.isEmpty(object.getString("marker_address2"))
                                && !object.getString("marker_address2").equals("null")) {
                            parkingUpdate.setMarker_address2(object.getString("marker_address2"));
                        }
                        if (object.has("marker_city")
                                && !TextUtils.isEmpty(object.getString("marker_city"))
                                && !object.getString("marker_city").equals("null")) {
                            parkingUpdate.setMarker_city(object.getString("marker_city"));
                        }
                        if (object.has("marker_state")
                                && !TextUtils.isEmpty(object.getString("marker_state"))
                                && !object.getString("marker_state").equals("null")) {
                            parkingUpdate.setMarker_state(object.getString("marker_state"));
                        }
                        if (object.has("marker_zip")
                                && !TextUtils.isEmpty(object.getString("marker_zip"))
                                && !object.getString("marker_zip").equals("null")) {
                            parkingUpdate.setMarker_zip(object.getString("marker_zip"));
                        }
                        if (object.has("marker_country")
                                && !TextUtils.isEmpty(object.getString("marker_country"))
                                && !object.getString("marker_country").equals("null")) {
                            parkingUpdate.setMarker_country(object.getString("marker_country"));
                        }
                        if (object.has("distance_to_marker")
                                && !TextUtils.isEmpty(object.getString("distance_to_marker"))
                                && !object.getString("distance_to_marker").equals("null")) {
                            parkingUpdate.setDistance_to_marker(object.getString("distance_to_marker"));
                        }
                        if (object.has("ip")
                                && !TextUtils.isEmpty(object.getString("ip"))
                                && !object.getString("ip").equals("null")) {
                            parkingUpdate.setIp(object.getString("ip"));
                        }
                        if (object.has("parking_token")
                                && !TextUtils.isEmpty(object.getString("parking_token"))
                                && !object.getString("parking_token").equals("null")) {
                            parkingUpdate.setParking_token(object.getString("parking_token"));
                        }
                        if (object.has("parking_status")
                                && !TextUtils.isEmpty(object.getString("parking_status"))
                                && !object.getString("parking_status").equals("null")) {
                            parkingUpdate.setParking_status(object.getString("parking_status"));
                        }
                        if (object.has("payment_method")
                                && !TextUtils.isEmpty(object.getString("payment_method"))
                                && !object.getString("payment_method").equals("null")) {
                            parkingUpdate.setPayment_method(object.getString("payment_method"));
                        }
                        if (object.has("parking_rate")
                                && !TextUtils.isEmpty(object.getString("parking_rate"))
                                && !object.getString("parking_rate").equals("null")) {
                            parkingUpdate.setParking_rate(object.getString("parking_rate"));
                        }
                        if (object.has("parking_units")
                                && !TextUtils.isEmpty(object.getString("parking_units"))
                                && !object.getString("parking_units").equals("null")) {
                            parkingUpdate.setParking_units(object.getString("parking_units"));
                        }
                        if (object.has("parking_qty")
                                && !TextUtils.isEmpty(object.getString("parking_qty"))
                                && !object.getString("parking_qty").equals("null")) {
                            parkingUpdate.setParking_qty(object.getString("parking_qty"));
                        }
                        if (object.has("parking_subtotal")
                                && !TextUtils.isEmpty(object.getString("parking_subtotal"))
                                && !object.getString("parking_subtotal").equals("null")) {
                            parkingUpdate.setParking_subtotal(object.getString("parking_subtotal"));
                        }
                        if (object.has("wallet_trx_id")
                                && !TextUtils.isEmpty(object.getString("wallet_trx_id"))
                                && !object.getString("wallet_trx_id").equals("null")) {
                            parkingUpdate.setWallet_trx_id(object.getString("wallet_trx_id"));
                        }
                        if (object.has("tr_percent")
                                && !TextUtils.isEmpty(object.getString("tr_percent"))
                                && !object.getString("tr_percent").equals("null")) {
                            parkingUpdate.setTr_percent(object.getString("tr_percent"));
                        }
                        if (object.has("tr_fee")
                                && !TextUtils.isEmpty(object.getString("tr_fee"))
                                && !object.getString("tr_fee").equals("null")) {
                            parkingUpdate.setTr_fee(object.getString("tr_fee"));
                        }
                        if (object.has("parking_total")
                                && !TextUtils.isEmpty(object.getString("parking_total"))
                                && !object.getString("parking_total").equals("null")) {
                            parkingUpdate.setParking_total(object.getString("parking_total"));
                        }
                        if (object.has("ipn_custom")
                                && !TextUtils.isEmpty(object.getString("ipn_custom"))
                                && !object.getString("ipn_custom").equals("null")) {
                            parkingUpdate.setIpn_custom(object.getString("ipn_custom"));
                        }
                        if (object.has("ipn_txn_id")
                                && !TextUtils.isEmpty(object.getString("ipn_txn_id"))
                                && !object.getString("ipn_txn_id").equals("null")) {
                            parkingUpdate.setIpn_txn_id(object.getString("ipn_txn_id"));
                        }
                        if (object.has("ipn_payment")
                                && !TextUtils.isEmpty(object.getString("ipn_payment"))
                                && !object.getString("ipn_payment").equals("null")) {
                            parkingUpdate.setIpn_payment(object.getString("ipn_payment"));
                        }
                        if (object.has("ipn_status")
                                && !TextUtils.isEmpty(object.getString("ipn_status"))
                                && !object.getString("ipn_status").equals("null")) {
                            parkingUpdate.setIpn_status(object.getString("ipn_status"));
                        }
                        if (object.has("ipn_address")
                                && !TextUtils.isEmpty(object.getString("ipn_address"))
                                && !object.getString("ipn_address").equals("null")) {
                            parkingUpdate.setIpn_address(object.getString("ipn_address"));
                        }
                        if (object.has("ticket_status")
                                && !TextUtils.isEmpty(object.getString("ticket_status"))
                                && !object.getString("ticket_status").equals("null")) {
                            parkingUpdate.setTicket_status(object.getString("ticket_status"));
                        }
                        if (object.has("expiry_status")
                                && !TextUtils.isEmpty(object.getString("expiry_status"))
                                && !object.getString("expiry_status").equals("null")) {
                            parkingUpdate.setExpiry_status(object.getString("expiry_status"));
                        }
                        if (object.has("notified_status")
                                && !TextUtils.isEmpty(object.getString("notified_status"))
                                && !object.getString("notified_status").equals("null")) {
                            parkingUpdate.setNotified_status(object.getString("notified_status"));
                        }
                        if (object.has("user_id_ref")
                                && !TextUtils.isEmpty(object.getString("user_id_ref"))
                                && !object.getString("user_id_ref").equals("null")) {
                            parkingUpdate.setUser_id_ref(object.getString("user_id_ref"));
                        }
                        if (object.has("mismatch")
                                && !TextUtils.isEmpty(object.getString("mismatch"))
                                && !object.getString("mismatch").equals("null")) {
                            parkingUpdate.setMismatch(object.getString("mismatch"));
                        }
                        if (object.has("payment_choice")
                                && !TextUtils.isEmpty(object.getString("payment_choice"))
                                && !object.getString("payment_choice").equals("null")) {
                            parkingUpdate.setPayment_choice(object.getString("payment_choice"));
                        }
                        if (object.has("bursar_trx_id")
                                && !TextUtils.isEmpty(object.getString("bursar_trx_id"))
                                && !object.getString("bursar_trx_id").equals("null")) {
                            parkingUpdate.setBursar_trx_id(object.getString("bursar_trx_id"));
                        }
                        if (object.has("platform")
                                && !TextUtils.isEmpty(object.getString("platform"))
                                && !object.getString("platform").equals("null")) {
                            parkingUpdate.setPlatform(object.getString("platform"));
                        }
                        if (object.has("duration_unit")
                                && !TextUtils.isEmpty(object.getString("duration_unit"))
                                && !object.getString("duration_unit").equals("null")) {
                            parkingUpdate.setDuration_unit(object.getString("duration_unit"));
                        }
                        if (object.has("max_duration")
                                && !TextUtils.isEmpty(object.getString("max_duration"))
                                && !object.getString("max_duration").equals("null")) {
                            parkingUpdate.setMax_duration(object.getString("max_duration"));
                        }
                        if (object.has("selected_duration")
                                && !TextUtils.isEmpty(object.getString("selected_duration"))
                                && !object.getString("selected_duration").equals("null")) {
                            parkingUpdate.setSelected_duration(object.getString("selected_duration"));
                        }
                        if (object.has("user_id_exit")
                                && !TextUtils.isEmpty(object.getString("user_id_exit"))
                                && !object.getString("user_id_exit").equals("null")) {
                            parkingUpdate.setUser_id_exit(object.getString("user_id_exit"));
                        }
                        if (object.has("exit_forced")
                                && !TextUtils.isEmpty(object.getString("exit_forced"))
                                && !object.getString("exit_forced").equals("null")) {
                            parkingUpdate.setExit_forced(object.getString("exit_forced"));
                        }
                        if (object.has("towed")
                                && !TextUtils.isEmpty(object.getString("towed"))
                                && !object.getString("towed").equals("null")) {
                            parkingUpdate.setTowed(object.getString("towed"));
                        }
                        if (object.has("exit_overnight")
                                && !TextUtils.isEmpty(object.getString("exit_overnight"))
                                && !object.getString("exit_overnight").equals("null")) {
                            parkingUpdate.setExit_overnight(object.getString("exit_overnight"));
                        }
                        if (object.has("modified_time")
                                && !TextUtils.isEmpty(object.getString("modified_time"))
                                && !object.getString("modified_time").equals("null")) {
                            parkingUpdate.setModified_time(object.getString("modified_time"));
                        }

                        if (object.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(object.getString("CanCancel_Reservation"))
                                && !object.getString("CanCancel_Reservation").equals("null")) {
                            parkingUpdate.setCanCancel_Reservation(object.getBoolean("CanCancel_Reservation"));
                        }

                        if (object.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(object.getString("Reservation_PostPayment_term"))
                                && !object.getString("Reservation_PostPayment_term").equals("null")) {
                            parkingUpdate.setReservation_PostPayment_term(object.getString("Reservation_PostPayment_term"));
                        }

                        if (object.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(object.getString("Cancellation_Charge"))
                                && !object.getString("Cancellation_Charge").equals("null")) {
                            parkingUpdate.setCancellation_Charge(object.getString("Cancellation_Charge"));
                        }

                        if (object.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(object.getString("Reservation_PostPayment_LateFee"))
                                && !object.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            parkingUpdate.setReservation_PostPayment_LateFee(object.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (object.has("location_lot_id")
                                && !TextUtils.isEmpty(object.getString("location_lot_id"))
                                && !object.getString("location_lot_id").equals("null")) {
                            parkingUpdate.setLocation_lot_id(object.getString("location_lot_id"));
                        }

                        reserveParkings.add(parkingUpdate);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                if (reserveParkings.size() != 0) {
                    Log.e("#DEBUG", "   onResponse:  reserveParkings Size: " + reserveParkings.size());
                    tvError.setVisibility(View.GONE);
                    new fetchTownships().execute();
                } else {
                    rlProgressbar.setVisibility(View.GONE);
                    tvError.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    protected void updateViews() {
        if (isMyReservations) {
            tvTitle.setText("My Parking Reservations");
        } else {
            tvTitle.setText("Parking Reservations");
        }

    }

    @Override
    protected void setListeners() {

        list_managed_lots.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String loc_code = managed_space_array.get(position).getLocation_code();
                Log.e("#DEBUG", "   selected LocationCode:  " + loc_code);

                ((vchome) getActivity()).show_back_button();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ReservedParkingFragment pay = new ReservedParkingFragment();
                Bundle bundle = new Bundle();
                bundle.putString("locationCode", loc_code);
                bundle.putBoolean("isMyReservations", isMyReservations);
                pay.setArguments(bundle);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });

    }

    @Override
    protected void initViews(View v) {

        list_managed_lots = v.findViewById(R.id.list_managed_lots);
        rlProgressbar = v.findViewById(R.id.rlProgressbar);
        tvError = v.findViewById(R.id.tvError);
        tvTitle = v.findViewById(R.id.tvTitle);
    }

    public class CustomAdapterManaged_Lost extends BaseAdapter implements View.OnClickListener {
        public Resources res;
        item tempValues = null;
        Context context;
        item state;
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;

        public CustomAdapterManaged_Lost(Activity a, ArrayList<item> d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;
            data = d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;
            if (convertView == null) {

                vi = inflater.inflate(R.layout.item_townships, null);
                /****** View Holder Object to contain tabitem.xml file elements ******/
                holder = new ViewHolder();

                holder.txt_commerical_name = vi.findViewById(R.id.txt_comm_name);
                holder.txt_deatil = vi.findViewById(R.id.txt_comm_add);
                holder.rldata = vi.findViewById(R.id.rl_main);
                holder.txt_pricing = vi.findViewById(R.id.txt_pricing);
                holder.txt_mile = vi.findViewById(R.id.txt_mile);
                holder.txt_parking_status = vi.findViewById(R.id.txt_commercial_view);
                holder.img_logo = vi.findViewById(R.id.img_logo);
                holder.tvLocationName = (TextView) vi.findViewById(R.id.tvLocationName);


                /************  Set holder with LayoutInflater ************/
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }
            if (data.size() <= 0) {
                //TODO show empty message

            } else {

                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                String cat = tempValues.getTitle();
                if (cat == null) {

                } else if (cat != null) {
                    if (!TextUtils.isEmpty(tempValues.getLocation_name())
                            && !tempValues.getLocation_name().equals("null")) {
                        holder.tvLocationName.setVisibility(View.VISIBLE);
                        holder.tvLocationName.setText(tempValues.getLocation_name());
                    } else {
                        holder.tvLocationName.setVisibility(View.GONE);
                    }

                    if (!TextUtils.isEmpty(tempValues.getLocation_code())) {
                        holder.txt_commerical_name.setText(tempValues.getLocation_code());
                        if (!TextUtils.isEmpty(tempValues.getLocation_id())) {
                            holder.txt_commerical_name.setText(String.format("%s (%s)",
                                    tempValues.getLocation_code(), tempValues.getLocation_id()));
                        }
                    }
//                    holder.txt_commerical_name.setText(tempValues.getTitle());
                    holder.txt_deatil.setText(tempValues.getAddress());
                    String location_code = tempValues.getLocation_code();
                    String effect = tempValues.getEffect();
//                    for (int i = 0; i < parking_rules.size(); i++) {
//                        String loc = parking_rules.get(i).getLocation_code();
//                        if (loc.equals(location_code)) {
//                            if (!parking_rules.get(i).getPricing().equals("")) {
//                                double prices = Double.parseDouble(parking_rules.get(i).getPricing());
//                                String lots_aval = tempValues.getLots_aval();
//                                if (effect.equals("0")) {
//                                    holder.txt_parking_status.setVisibility(View.VISIBLE);
//                                    holder.txt_parking_status.setText("CLOSED");
//                                    holder.txt_parking_status.setBackgroundColor(Color.parseColor("#E9967A"));
//                                } else if (lots_aval.equals("0")) {
//                                    holder.txt_parking_status.setVisibility(View.VISIBLE);
//                                    holder.txt_parking_status.setText("CLOSED");
//                                    holder.txt_parking_status.setBackgroundColor(Color.parseColor("#E9967A"));
//                                } else if (prices != -1.0 || prices != 1) {
//                                    holder.txt_parking_status.setVisibility(View.VISIBLE);
//                                    holder.txt_parking_status.setText("OPEN");
//                                    holder.txt_parking_status.setBackgroundColor(Color.parseColor("#3CB385"));
//                                } else {
//                                    holder.txt_parking_status.setVisibility(View.VISIBLE);
//                                    holder.txt_parking_status.setText("CLOSED");
//                                    holder.txt_parking_status.setBackgroundColor(Color.parseColor("#E9967A"));
//                                }
//                                holder.txt_pricing.setText("$" + parking_rules.get(i).getPricing());
//                            } else {
//                                holder.txt_parking_status.setVisibility(View.GONE);
//                                holder.txt_pricing.setText("");
//                            }
//                            break;
//                        }
//                    }

                    holder.txt_pricing.setVisibility(View.GONE);
                    holder.txt_mile.setVisibility(View.GONE);

                    if (tempValues.getMarker().equals("Free")) {
                        holder.img_logo.setImageResource(R.mipmap.free1);
                    } else if (tempValues.getMarker().equals("Managed Paid") || tempValues.getMarker().equals("Managed Free")) {
                        holder.img_logo.setImageResource(R.mipmap.managed1);
                    } else if (tempValues.getMarker().equals("Paid")) {
                        holder.img_logo.setImageResource(R.mipmap.paid);
                    }
                    if (position % 2 == 0) {
                        holder.rldata.setBackgroundColor(Color.parseColor("#c2c2c2"));
                    } else {
                        holder.rldata.setBackgroundColor(Color.parseColor("#b0000000"));
                    }
                }
            }

            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        public class ViewHolder {

            public TextView txt_commerical_name, txt_deatil, txt_pricing, txt_mile, txt_parking_status, txt_la, txt_lang, txt_location_code, txt_place_id, txt_marker_type,
                    tvLocationName;
            ImageView img_logo;
            RelativeLayout rldata;
        }
    }
}
