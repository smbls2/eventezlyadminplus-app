package com.softmeasures.eventezlyadminplus.frament.reservation;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.models.LocationLot;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MarkReservableSpotsFragment extends BaseFragment {

    private RecyclerView rvLocationLot;
    private TextView tvError, tvSpots;
    private ArrayList<LocationLot> locationLots = new ArrayList<>();
    private LocationLotAdapter locationLotAdapter;
    private String locationId = "", noOfReservableSpots = "";
    private RelativeLayout rlProgressbar;
    private LocationLot updateLocationLot;
    private int countReservableSpots = 0;
    private String type;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            locationId = getArguments().getString("locationId");
            type = getArguments().getString("type");
            noOfReservableSpots = getArguments().getString("noOfReservableSpots");
        }
        return inflater.inflate(R.layout.frag_mark_reservable_spots, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        new fetchLocationLots(locationId).execute();

    }

    public class fetchLocationLots extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String locationLotsUrl = "_table/location_lot";
        JSONObject json;
        String re_id;
        String id;
        String locationId;

        public fetchLocationLots(String locationId) {
            this.locationId = locationId;
        }

        @Override
        protected void onPreExecute() {
            locationLots.clear();
            rlProgressbar.setVisibility(View.VISIBLE);
            try {
                if (type.equals("Commercial")) {
                    this.locationLotsUrl = "_table/location_lot_commercial?filter=location_id%3D" + URLEncoder.encode(String.valueOf(locationId), "utf-8") + "";
                } else if (type.equals("Township")) {
                    this.locationLotsUrl = "_table/location_lot?filter=location_id%3D" + URLEncoder.encode(String.valueOf(locationId), "utf-8") + "";
                } else if (type.equals("Other")) {
                    this.locationLotsUrl = "_table/location_lot_other?filter=location_id%3D" + URLEncoder.encode(String.valueOf(locationId), "utf-8") + "";
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("#DEBUG", "   fetchLocationLot:  Error:  " + e.getMessage());
            }
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            locationLots.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + locationLotsUrl;
            Log.e("#DEBUG", "  fetchLocationLots:  URL:  " + url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        LocationLot locationLot = new LocationLot();
                        locationLot.setId(object.getInt("id"));

                        if (object.has("date_time")
                                && !TextUtils.isEmpty(object.getString("date_time"))
                                && !object.getString("date_time").equals("null")) {
                            locationLot.setDate_time(object.getString("date_time"));
                        }

                        if (object.has("township_code")
                                && !TextUtils.isEmpty(object.getString("township_code"))
                                && !object.getString("township_code").equals("null")) {
                            locationLot.setTownship_code(object.getString("township_code"));
                        }

                        if (object.has("location_id")
                                && !TextUtils.isEmpty(object.getString("location_id"))
                                && !object.getString("location_id").equals("null")) {
                            locationLot.setLocation_id(Integer.parseInt(object.getString("location_id")));
                        }

                        if (object.has("date_time")
                                && !TextUtils.isEmpty(object.getString("date_time"))
                                && !object.getString("date_time").equals("null")) {
                            locationLot.setDate_time(object.getString("date_time"));
                        }

                        if (object.has("location_code")
                                && !TextUtils.isEmpty(object.getString("location_code"))
                                && !object.getString("location_code").equals("null")) {
                            locationLot.setLocation_code(object.getString("location_code"));
                        }

                        if (object.has("location_name")
                                && !TextUtils.isEmpty(object.getString("location_name"))
                                && !object.getString("location_name").equals("null")) {
                            locationLot.setLocation_name(object.getString("location_name"));
                        }

                        if (object.has("lot_row")
                                && !TextUtils.isEmpty(object.getString("lot_row"))
                                && !object.getString("lot_row").equals("null")) {
                            locationLot.setLot_row(object.getString("lot_row"));
                        }

                        if (object.has("lot_number")
                                && !TextUtils.isEmpty(object.getString("lot_number"))
                                && !object.getString("lot_number").equals("null")) {
                            locationLot.setLot_number(object.getString("lot_number"));
                        }

                        if (object.has("occupied")
                                && !TextUtils.isEmpty(object.getString("occupied"))
                                && !object.getString("occupied").equals("null")) {
                            locationLot.setOccupied(object.getString("occupied"));
                        }

                        if (object.has("plate_no")
                                && !TextUtils.isEmpty(object.getString("plate_no"))
                                && !object.getString("plate_no").equals("null")) {
                            locationLot.setPlate_no(object.getString("plate_no"));
                        }

                        if (object.has("plate_state")
                                && !TextUtils.isEmpty(object.getString("plate_state"))
                                && !object.getString("plate_state").equals("null")) {
                            locationLot.setPlate_state(object.getString("plate_state"));
                        }

                        if (object.has("lot_id")
                                && !TextUtils.isEmpty(object.getString("lot_id"))
                                && !object.getString("lot_id").equals("null")) {
                            locationLot.setLot_id(object.getString("lot_id"));
                        }

                        if (object.has("lot_reservable")
                                && !TextUtils.isEmpty(object.getString("lot_reservable"))
                                && !object.getString("lot_reservable").equals("null")) {
                            locationLot.setLot_reservable(object.getBoolean("lot_reservable"));
                        }

                        if (object.has("lot_reservable_only")
                                && !TextUtils.isEmpty(object.getString("lot_reservable_only"))
                                && !object.getString("lot_reservable_only").equals("null")) {
                            locationLot.setLot_reservable_only(object.getBoolean("lot_reservable_only"));
                        }

                        if (object.has("company_type_id")
                                && !TextUtils.isEmpty(object.getString("company_type_id"))
                                && !object.getString("company_type_id").equals("null")) {
                            locationLot.setCompany_type_id(object.getString("company_type_id"));
                        }

                        if (object.has("manager_type_id")
                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
                                && !object.getString("manager_type_id").equals("null")) {
                            locationLot.setManager_type_id(object.getString("manager_type_id"));
                        }

                        if (object.has("premium_lot")
                                && !TextUtils.isEmpty(object.getString("premium_lot"))
                                && !object.getString("premium_lot").equals("null")) {
                            locationLot.setPremium_lot(object.getBoolean("premium_lot"));
                        }

                        if (object.has("special_need_handi_lot")
                                && !TextUtils.isEmpty(object.getString("special_need_handi_lot"))
                                && !object.getString("special_need_handi_lot").equals("null")) {
                            locationLot.setSpecial_need_handi_lot(object.getBoolean("special_need_handi_lot"));
                        }

                        if (object.has("company_id")
                                && !TextUtils.isEmpty(object.getString("company_id"))
                                && !object.getString("company_id").equals("null")) {
                            locationLot.setCompany_id(object.getString("company_id"));
                        }

                        locationLots.add(locationLot);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   fetchLocationLots:  Error:  " + e.getMessage());
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "   fetchLocationLots:  Error:  " + e.getMessage());
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rlProgressbar != null)
                rlProgressbar.setVisibility(View.GONE);

            if (getActivity() != null) {
                if (locationLots.size() != 0) {
                    //TODO display lots
                    countReservableSpots = 0;
                    for (int i = 0; i < locationLots.size(); i++) {
                        if (locationLots.get(i).isLot_reservable()) {
                            countReservableSpots++;
                        }
                    }
                    tvError.setVisibility(View.GONE);
                    locationLotAdapter.notifyDataSetChanged();
                } else {
                    countReservableSpots = 0;
                    locationLotAdapter.notifyDataSetChanged();
                    tvError.setVisibility(View.VISIBLE);
                }
                updateViews();
                super.onPostExecute(s);
            }
        }
    }

    @Override
    protected void updateViews() {
        if (!TextUtils.isEmpty(noOfReservableSpots)) {
            tvSpots.setText("Number of Mark Reservable Spots : " + countReservableSpots + "/" + noOfReservableSpots);
        } else tvSpots.setText("Number of Mark Reservable Spots : 0/0");

    }

    @Override
    protected void setListeners() {

    }

    @Override
    protected void initViews(View v) {
        rlProgressbar = v.findViewById(R.id.rlProgressbar);
        tvError = v.findViewById(R.id.tvError);
        tvSpots = v.findViewById(R.id.tvSpots);
        rvLocationLot = v.findViewById(R.id.rvLocationLot);

        locationLotAdapter = new LocationLotAdapter();
        rvLocationLot.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        rvLocationLot.setAdapter(locationLotAdapter);
    }

    public class updateLocationLotReservable extends AsyncTask<String, String, String> {
        String locationLotUrl = "_table/location_lot";
        JSONObject json, json1;
        String re_id;
        String id;

        @Override
        protected void onPreExecute() {
            if (type.equals("Commercial")) {
                locationLotUrl = "location_lot_commercial";
            } else if (type.equals("Township")) {
                locationLotUrl = "location_lot";
            } else if (type.equals("Other")) {
                locationLotUrl = "location_lot_other";
            }
            rlProgressbar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues = new HashMap<String, Object>();

            jsonValues.put("id", updateLocationLot.getId());
            if (updateLocationLot.isLot_reservable()) jsonValues.put("lot_reservable", "1");
            else jsonValues.put("lot_reservable", "0");


            if (updateLocationLot.isLot_reservable_only())
                jsonValues.put("lot_reservable_only", "1");
            else jsonValues.put("lot_reservable_only", "0");

            if (updateLocationLot.isPremium_lot()) jsonValues.put("premium_lot", "1");
            else jsonValues.put("premium_lot", "0");

            if (updateLocationLot.isSpecial_need_handi_lot())
                jsonValues.put("special_need_handi_lot", "1");
            else jsonValues.put("special_need_handi_lot", "0");

            jsonValues.put("lot_row", updateLocationLot.getLot_row());
            jsonValues.put("lot_number", updateLocationLot.getLot_number());
            jsonValues.put("lot_id", updateLocationLot.getLot_id());

            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + locationLotUrl;
            HttpPut post = new HttpPut(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(entity);
            Log.e("managed_url", url);
            Log.e("parama", String.valueOf(json));

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.e("#DEBUG", "   updateLocationLotReservable:  Response ID: " + re_id);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rlProgressbar != null)
                rlProgressbar.setVisibility(View.GONE);

            if (getActivity() != null && json1 != null) {
                if (re_id != null && !TextUtils.isEmpty(re_id) && !re_id.equals("null")) {
                    Toast.makeText(getActivity(), "Updated Successfully!", Toast.LENGTH_SHORT).show();
                    new fetchLocationLots(locationId).execute();
                } else {
                    Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            }
            super.onPostExecute(s);
        }
    }

    private class LocationLotAdapter extends RecyclerView.Adapter<LocationLotAdapter.LocationLotHolder> {

        @NonNull
        @Override
        public LocationLotAdapter.LocationLotHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new LocationLotHolder(LayoutInflater.from(getActivity())
                    .inflate(R.layout.item_location_lot, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull LocationLotAdapter.LocationLotHolder holder, int i) {
            LocationLot locationLot = locationLots.get(i);
            if (locationLot != null) {
                if (!TextUtils.isEmpty(locationLot.getLot_id())) {
                    holder.tvRow.setText(locationLot.getLot_id());
                }

                holder.ivCar.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.mipmap.popup_gree_car));
                if (locationLot.isLot_reservable()) {
                    holder.ivReserved.setVisibility(View.VISIBLE);
                } else {
                    holder.ivReserved.setVisibility(View.GONE);
                }

                if (locationLot.isPremium_lot())
                    holder.ivPremium.setVisibility(View.VISIBLE);
                else holder.ivPremium.setVisibility(View.GONE);

                if (locationLot.isSpecial_need_handi_lot())
                    holder.ivHandicapped.setVisibility(View.VISIBLE);
                else holder.ivHandicapped.setVisibility(View.GONE);

                if (locationLot.isLot_reservable_only()) {
                    holder.ivReservedOnly.setVisibility(View.VISIBLE);
                } else holder.ivReservedOnly.setVisibility(View.GONE);

            }

        }

        @Override
        public int getItemCount() {
            return locationLots.size();
        }

        class LocationLotHolder extends RecyclerView.ViewHolder {
            ImageView ivCar, ivReserved, ivPremium, ivHandicapped, ivReservedOnly;
            TextView tvRow;

            LocationLotHolder(@NonNull View itemView) {
                super(itemView);

                ivCar = itemView.findViewById(R.id.ivCar);
                tvRow = itemView.findViewById(R.id.tvRow);
                ivReserved = itemView.findViewById(R.id.ivReserved);
                ivPremium = itemView.findViewById(R.id.ivPremium);
                ivHandicapped = itemView.findViewById(R.id.ivHandicapped);
                ivReservedOnly = itemView.findViewById(R.id.ivReservedOnly);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            updateLocationLot = locationLots.get(position);
                            showDialogUpdateLot();

//                            if (countReservableSpots != Integer.valueOf(noOfReservableSpots)) {
//                                updateLocationLot = new LocationLot();
//                                updateLocationLot.setId(locationLots.get(position).getId());
//                                if (locationLots.get(position).isLot_reservable()) {
//                                    updateLocationLot.setLot_reservable(false);
//                                } else updateLocationLot.setLot_reservable(true);
//                                new updateLocationLotReservable().execute();
//                            } else {
//                                Toast.makeText(getActivity(), "Sorry, All reservable spots assigned!", Toast.LENGTH_SHORT).show();
//                            }
                        }
                    }
                });
            }
        }
    }

    private void showDialogUpdateLot() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View dialogView = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_update_location_lot, null);
        builder.setView(dialogView);
        final TextView tvLotId = dialogView.findViewById(R.id.tvLotId);

        final LinearLayout llEditRows = dialogView.findViewById(R.id.llEditRows);
        ImageView ivBtnEditRow = dialogView.findViewById(R.id.ivBtnEditRow);
        final TextInputEditText etRow = dialogView.findViewById(R.id.etRow);
        final TextInputEditText etNumber = dialogView.findViewById(R.id.etNumber);
        ImageView ivBtnRowDone = dialogView.findViewById(R.id.ivBtnRowDone);

        tvLotId.setText(updateLocationLot.getLot_id());
        etRow.setText(updateLocationLot.getLot_row());
        etNumber.setText(updateLocationLot.getLot_number());

        AppCompatCheckBox cbReservable = dialogView.findViewById(R.id.cbReservable);
        AppCompatCheckBox cbReservableOnly = dialogView.findViewById(R.id.cbReservableOnly);
        AppCompatCheckBox cbPremium = dialogView.findViewById(R.id.cbPremium);
        AppCompatCheckBox cbHandicapped = dialogView.findViewById(R.id.cbHandicapped);

        cbReservable.setChecked(updateLocationLot.isLot_reservable());
        cbReservableOnly.setChecked(updateLocationLot.isLot_reservable_only());
        cbPremium.setChecked(updateLocationLot.isPremium_lot());
        cbHandicapped.setChecked(updateLocationLot.isSpecial_need_handi_lot());

        Button btnUpdate = dialogView.findViewById(R.id.btnUpdate);
        Button btnCancel = dialogView.findViewById(R.id.btnCancel);

        final AlertDialog alertDialog = builder.create();

        ivBtnEditRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (llEditRows.getVisibility() == View.VISIBLE) {
                    llEditRows.setVisibility(View.GONE);
                } else llEditRows.setVisibility(View.VISIBLE);
            }
        });

        ivBtnRowDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(etRow.getText()))
                    updateLocationLot.setLot_row(etRow.getText().toString());
                if (!TextUtils.isEmpty(etNumber.getText()))
                    updateLocationLot.setLot_number(etNumber.getText().toString());
                updateLocationLot.setLot_id(updateLocationLot.getLot_row() + ":" + updateLocationLot.getLot_number());
                tvLotId.setText(updateLocationLot.getLot_id());
                llEditRows.setVisibility(View.GONE);
            }
        });

        cbReservable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                updateLocationLot.setLot_reservable(b);
            }
        });

        cbReservableOnly.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                updateLocationLot.setLot_reservable_only(b);
            }
        });

        cbPremium.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                updateLocationLot.setPremium_lot(b);
            }
        });

        cbHandicapped.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                updateLocationLot.setSpecial_need_handi_lot(b);
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                new updateLocationLotReservable().execute();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }
}
