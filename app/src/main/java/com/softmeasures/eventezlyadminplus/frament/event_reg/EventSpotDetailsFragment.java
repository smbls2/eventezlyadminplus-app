package com.softmeasures.eventezlyadminplus.frament.event_reg;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.youtube.player.YouTubeIntents;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.panstreetview;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoFacebookBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoGoogleClassroomBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoGoogleMeetBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoSkypeBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoTeleConferenceBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoYoutubeBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoZoomBinding;
import com.softmeasures.eventezlyadminplus.databinding.FragEventSpotDetailsBinding;
import com.softmeasures.eventezlyadminplus.frament.event.PDFViewFragment;
import com.softmeasures.eventezlyadminplus.frament.f_direction;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.meeting.ZoomLoginActivity;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.MediaDefinition;
import com.softmeasures.eventezlyadminplus.models.R_Parking;
import com.softmeasures.eventezlyadminplus.utils.AESEncyption;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_LOCATION;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_VIRTUALLY;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_BOTH;

public class EventSpotDetailsFragment extends BaseFragment {

    FragEventSpotDetailsBinding binding;
    private EventDefinition eventDefinition;
    private SimpleDateFormat dateFormat;
    private SimpleDateFormat dateFormatInput;
    private item selectedSpot;
    SimpleDateFormat dateFormatMain = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd MMM, hh:mm a", Locale.getDefault());
    private ArrayList<String> eventImages = new ArrayList<>();
    private EventImageAdapter eventImageAdapter;
    private item parkingRule;
    private ArrayList<item> parking_rules = new ArrayList<>();
    private R_Parking rParking;
    private item itemLocation;
    private SupportMapFragment mapFragment;
    private GoogleMap googleMap;
    private ProgressDialog progressDialog;
    private String selectedPaymentMethod = "";
    private String transection_id = "";
    private String wallet_id = "";
    private ArrayList<item> wallbalarray = new ArrayList<>();
    private String finallab, nbal, cbal = "0";
    private double finalpayamount, newbal, subTotal;

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    private static final String CONFIG_CLIENT_ID = "AQXvOmlRwvO2AI6H3XzqTe14pp4x6VtP2skKfUkhgjRAoNj0NoT7l0ZGeaRAXkAtYqyPCmxKEUXkrT89";
    private PayPalConfiguration config = new PayPalConfiguration().environment(CONFIG_ENVIRONMENT).clientId(CONFIG_CLIENT_ID);
    private static final int REQUEST_CODE_PAYMENT = 2;

    private ArrayList<MediaDefinition> mediaDefinitions = new ArrayList<>();
    private MediaAdapter mediaAdapter;
    private String API_KEY = "AIzaSyBgK4WrJmik2dy50f84FeWaxAZz0bnfyMc";
    private YouTubePlayerFragment youTubePlayerFragment;
    private YouTubePlayer youTubePlayer;
    private int RECOVERY_DIALOG_REQUEST = 1;

    private ArrayList<String> teleNumbers = new ArrayList<>();
    private TeleNumberAdapter teleNumberAdapter;
    private String teleCountryCode = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            eventDefinition = new Gson().fromJson(getArguments().getString("eventDefinition"), EventDefinition.class);
            if (eventDefinition != null) {
                Log.e("#DEBUG", "   EventSpotDetailsFragment:  eventDefinition:  " + new Gson().toJson(eventDefinition));
                if (eventDefinition.getEvent_link_array() != null && !TextUtils.isEmpty(eventDefinition.getEvent_link_array())) {
                    mediaDefinitions.clear();
                    mediaDefinitions.addAll(new Gson().fromJson(eventDefinition.getEvent_link_array(),
                            new TypeToken<ArrayList<MediaDefinition>>() {
                            }.getType()));
                }
            }
            selectedSpot = new Gson().fromJson(getArguments().getString("selectedSpot"), item.class);
            if (selectedSpot != null) {
                Log.e("#DEBUG", "   EventSpotDetailsFragment:  selectedSpot:  " + new Gson().toJson(selectedSpot));
            }
//            parkingRule = new Gson().fromJson(getArguments().getString("parkingRule"), item.class);
            rParking = new Gson().fromJson(getArguments().getString("rParking"), R_Parking.class);
            itemLocation = new Gson().fromJson(getArguments().getString("itemLocation"), item.class);
        }

        binding = DataBindingUtil.inflate(inflater, R.layout.frag_event_spot_details, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        updateViews();
        setListeners();
//        new fetchEventDetails().execute();
        new getparkingrules().execute();
        loadMap();
    }

    private void loadMap() {
        if (getActivity() != null) {
            try {
                mapFragment = (SupportMapFragment) getChildFragmentManager()
                        .findFragmentById(R.id.map);
                if (mapFragment != null) {
                    mapFragment.getMapAsync(map -> {
                        googleMap = map;
                        showLocationMarkerOnMap();
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showLocationMarkerOnMap() {
        MarkerOptions markerOptions = new MarkerOptions();
        if (itemLocation != null && itemLocation.getLat() != null && !TextUtils.isEmpty(itemLocation.getLat())) {
            LatLng position = new LatLng(Double.parseDouble(itemLocation.getLat()), Double.parseDouble(itemLocation.getLng()));
            markerOptions.position(position);
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapEvent()));
//        markerOptions.title("");
            markerOptions.snippet(String.valueOf(eventDefinition.getId()));
            googleMap.addMarker(markerOptions);

            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    position, 12);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 12));
            googleMap.animateCamera(location);
        }
    }

    private Bitmap getMarkerBitmapEvent() {
        LinearLayout distanceMarkerLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.marker_with_price, null);
        TextView tvPrice = distanceMarkerLayout.findViewById(R.id.tvPrice);
        TextView tvLocationName = distanceMarkerLayout.findViewById(R.id.tvLocationName);
        if (!TextUtils.isEmpty(eventDefinition.getEvent_name())) {
            tvLocationName.setVisibility(View.VISIBLE);
            tvLocationName.setText(eventDefinition.getEvent_name());
        } else {
            tvLocationName.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(eventDefinition.getEvent_type())) {
            tvPrice.setText(eventDefinition.getEvent_type());
        }
        distanceMarkerLayout.setDrawingCacheEnabled(true);
        distanceMarkerLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        distanceMarkerLayout.layout(0, 0, distanceMarkerLayout.getMeasuredWidth(), distanceMarkerLayout.getMeasuredHeight());
        distanceMarkerLayout.buildDrawingCache(true);

        Bitmap bitmap = Bitmap.createBitmap(distanceMarkerLayout.getDrawingCache());
        distanceMarkerLayout.setDrawingCacheEnabled(false);
        BitmapDescriptor flagBitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
        return bitmap;
    }

//    private class fetchEventDetails extends AsyncTask<String, String, String> {
//        JSONObject json;
//        JSONArray json1;
//        String eventDefUrl = "_table/event_definitions";
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            binding.rlProgressbar.setVisibility(View.VISIBLE);
//            if (selectedSpot != null && selectedSpot.getTownship_code() != null
//                    && !TextUtils.isEmpty(selectedSpot.getTownship_code())
//                    && !selectedSpot.getTownship_code().equals("null")) {
//                eventDefUrl = "_table/event_definitions?filter=township_code=" + selectedSpot.getTownship_code();
//            }
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            String url = getString(R.string.api) + getString(R.string.povlive) + eventDefUrl;
//            Log.e("#DEBUG", "      fetchEventDetails:  " + url);
//            DefaultHttpClient client = new DefaultHttpClient();
//            HttpGet post = new HttpGet(url);
//            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
//            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
//            HttpResponse response = null;
//            try {
//                response = client.execute(post);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            if (response != null) {
//                HttpEntity resEntity = response.getEntity();
//                String responseStr = null;
//                try {
//                    responseStr = EntityUtils.toString(resEntity).trim();
//                    json = new JSONObject(responseStr);
//                    JSONArray jsonArray = json.getJSONArray("resource");
//
//                    for (int j = 0; j < jsonArray.length(); j++) {
//                        eventDefinition = new EventDefinition();
//                        JSONObject object = jsonArray.getJSONObject(j);
//                        if (object.has("id")
//                                && !TextUtils.isEmpty(object.getString("id"))
//                                && !object.getString("id").equals("null")) {
//                            eventDefinition.setId(object.getInt("id"));
//                        }
//                        if (object.has("date_time")
//                                && !TextUtils.isEmpty(object.getString("date_time"))
//                                && !object.getString("date_time").equals("null")) {
//                            eventDefinition.setDate_time(object.getString("date_time"));
//                        }
//                        if (object.has("manager_type")
//                                && !TextUtils.isEmpty(object.getString("manager_type"))
//                                && !object.getString("manager_type").equals("null")) {
//                            eventDefinition.setManager_type(object.getString("manager_type"));
//                        }
//                        if (object.has("manager_type_id")
//                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
//                                && !object.getString("manager_type_id").equals("null")) {
//                            eventDefinition.setManager_type_id(object.getInt("manager_type_id"));
//                        }
//                        if (object.has("twp_id")
//                                && !TextUtils.isEmpty(object.getString("twp_id"))
//                                && !object.getString("twp_id").equals("null")) {
//                            eventDefinition.setTwp_id(object.getInt("twp_id"));
//                        }
//                        if (object.has("township_code")
//                                && !TextUtils.isEmpty(object.getString("township_code"))
//                                && !object.getString("township_code").equals("null")) {
//                            eventDefinition.setTownship_code(object.getString("township_code"));
//                        }
//                        if (object.has("township_name")
//                                && !TextUtils.isEmpty(object.getString("township_name"))
//                                && !object.getString("township_name").equals("null")) {
//                            eventDefinition.setTownship_name(object.getString("township_name"));
//                        }
//                        if (object.has("company_id")
//                                && !TextUtils.isEmpty(object.getString("company_id"))
//                                && !object.getString("company_id").equals("null")) {
//                            eventDefinition.setCompany_id(object.getInt("company_id"));
//                        }
//                        if (object.has("company_code")
//                                && !TextUtils.isEmpty(object.getString("company_code"))
//                                && !object.getString("company_code").equals("null")) {
//                            eventDefinition.setCompany_code(object.getString("company_code"));
//                        }
//                        if (object.has("company_name")
//                                && !TextUtils.isEmpty(object.getString("company_name"))
//                                && !object.getString("company_name").equals("null")) {
//                            eventDefinition.setCompany_name(object.getString("company_name"));
//                        }
//                        if (object.has("event_id")
//                                && !TextUtils.isEmpty(object.getString("event_id"))
//                                && !object.getString("event_id").equals("null")) {
//                            eventDefinition.setEvent_id(object.getInt("event_id"));
//                        }
//                        if (object.has("event_type")
//                                && !TextUtils.isEmpty(object.getString("event_type"))
//                                && !object.getString("event_type").equals("null")) {
//                            eventDefinition.setEvent_type(object.getString("event_type"));
//                        }
//                        if (object.has("event_code")
//                                && !TextUtils.isEmpty(object.getString("event_code"))
//                                && !object.getString("event_code").equals("null")) {
//                            eventDefinition.setEvent_code(object.getString("event_code"));
//                        }
//                        if (object.has("event_name")
//                                && !TextUtils.isEmpty(object.getString("event_name"))
//                                && !object.getString("event_name").equals("null")) {
//                            eventDefinition.setEvent_name(object.getString("event_name"));
//                        }
//                        if (object.has("event_short_description")
//                                && !TextUtils.isEmpty(object.getString("event_short_description"))
//                                && !object.getString("event_short_description").equals("null")) {
//                            eventDefinition.setEvent_short_description(object.getString("event_short_description"));
//                        }
//                        if (object.has("event_long_description")
//                                && !TextUtils.isEmpty(object.getString("event_long_description"))
//                                && !object.getString("event_long_description").equals("null")) {
//                            eventDefinition.setEvent_long_description(object.getString("event_long_description"));
//                        }
//                        if (object.has("event_led_by")
//                                && !TextUtils.isEmpty(object.getString("event_led_by"))
//                                && !object.getString("event_led_by").equals("null")) {
//                            eventDefinition.setEvent_led_by(object.getString("event_led_by"));
//                        }
//                        if (object.has("event_leaders_bio")
//                                && !TextUtils.isEmpty(object.getString("event_leaders_bio"))
//                                && !object.getString("event_leaders_bio").equals("null")) {
//                            eventDefinition.setEvent_leaders_bio(object.getString("event_leaders_bio"));
//                        }
//                        if (object.has("event_link_on_web")
//                                && !TextUtils.isEmpty(object.getString("event_link_on_web"))
//                                && !object.getString("event_link_on_web").equals("null")) {
//                            eventDefinition.setEvent_link_on_web(object.getString("event_link_on_web"));
//                        }
//                        if (object.has("event_link_twitter")
//                                && !TextUtils.isEmpty(object.getString("event_link_twitter"))
//                                && !object.getString("event_link_twitter").equals("null")) {
//                            eventDefinition.setEvent_link_twitter(object.getString("event_link_twitter"));
//                        }
//                        if (object.has("event_link_whatsapp")
//                                && !TextUtils.isEmpty(object.getString("event_link_whatsapp"))
//                                && !object.getString("event_link_whatsapp").equals("null")) {
//                            eventDefinition.setEvent_link_whatsapp(object.getString("event_link_whatsapp"));
//                        }
//                        if (object.has("event_link_other_media")
//                                && !TextUtils.isEmpty(object.getString("event_link_other_media"))
//                                && !object.getString("event_link_other_media").equals("null")) {
//                            eventDefinition.setEvent_link_other_media(object.getString("event_link_other_media"));
//                        }
//                        if (object.has("company_logo")
//                                && !TextUtils.isEmpty(object.getString("company_logo"))
//                                && !object.getString("company_logo").equals("null")) {
//                            eventDefinition.setCompany_logo(object.getString("company_logo"));
//                        }
//                        if (object.has("event_logo")
//                                && !TextUtils.isEmpty(object.getString("event_logo"))
//                                && !object.getString("event_logo").equals("null")) {
//                            eventDefinition.setEvent_logo(object.getString("event_logo"));
//                        }
//                        if (object.has("event_image1")
//                                && !TextUtils.isEmpty(object.getString("event_image1"))
//                                && !object.getString("event_image1").equals("null")) {
//                            eventDefinition.setEvent_image1(object.getString("event_image1"));
//                        }
//                        if (object.has("event_image2")
//                                && !TextUtils.isEmpty(object.getString("event_image2"))
//                                && !object.getString("event_image2").equals("null")) {
//                            eventDefinition.setEvent_image2(object.getString("event_image2"));
//                        }
//                        if (object.has("event_blob_image")
//                                && !TextUtils.isEmpty(object.getString("event_blob_image"))
//                                && !object.getString("event_blob_image").equals("null")) {
//                            eventDefinition.setEvent_blob_image(object.getString("event_blob_image"));
//                        }
//                        if (object.has("event_address")
//                                && !TextUtils.isEmpty(object.getString("event_address"))
//                                && !object.getString("event_address").equals("null")) {
//                            eventDefinition.setEvent_address(object.getString("event_address"));
//                        }
//                        if (object.has("covered_locations")
//                                && !TextUtils.isEmpty(object.getString("covered_locations"))
//                                && !object.getString("covered_locations").equals("null")) {
//                            eventDefinition.setCovered_locations(object.getString("covered_locations"));
//                        }
//                        if (object.has("regn_needed_approval")
//                                && !TextUtils.isEmpty(object.getString("regn_needed_approval"))
//                                && !object.getString("regn_needed_approval").equals("null")) {
//                            eventDefinition.setRegn_needed_approval(object.getBoolean("regn_needed_approval"));
//                        }
//                        if (object.has("requirements")
//                                && !TextUtils.isEmpty(object.getString("requirements"))
//                                && !object.getString("requirements").equals("null")) {
//                            eventDefinition.setRequirements(object.getString("requirements"));
//                        }
//                        if (object.has("appl_req_download")
//                                && !TextUtils.isEmpty(object.getString("appl_req_download"))
//                                && !object.getString("appl_req_download").equals("null")) {
//                            eventDefinition.setAppl_req_download(object.getString("appl_req_download"));
//                        }
//                        if (object.has("cost")
//                                && !TextUtils.isEmpty(object.getString("cost"))
//                                && !object.getString("cost").equals("null")) {
//                            eventDefinition.setCost(object.getString("cost"));
//                        }
//                        if (object.has("year")
//                                && !TextUtils.isEmpty(object.getString("year"))
//                                && !object.getString("year").equals("null")) {
//                            eventDefinition.setYear(object.getString("year"));
//                        }
//                        if (object.has("location_address")
//                                && !TextUtils.isEmpty(object.getString("location_address"))
//                                && !object.getString("location_address").equals("null")) {
//                            eventDefinition.setLocation_address(object.getString("location_address"));
//                        }
//                        if (object.has("scheme_type")
//                                && !TextUtils.isEmpty(object.getString("scheme_type"))
//                                && !object.getString("scheme_type").equals("null")) {
//                            eventDefinition.setScheme_type(object.getString("scheme_type"));
//                        }
//                        if (object.has("event_prefix")
//                                && !TextUtils.isEmpty(object.getString("event_prefix"))
//                                && !object.getString("event_prefix").equals("null")) {
//                            eventDefinition.setEvent_prefix(object.getString("event_prefix"));
//                        }
//                        if (object.has("event_nextnum")
//                                && !TextUtils.isEmpty(object.getString("event_nextnum"))
//                                && !object.getString("event_nextnum").equals("null")) {
//                            eventDefinition.setEvent_nextnum(object.getString("event_nextnum"));
//                        }
//                        if (object.has("event_begins_date_time")
//                                && !TextUtils.isEmpty(object.getString("event_begins_date_time"))
//                                && !object.getString("event_begins_date_time").equals("null")) {
//                            eventDefinition.setEvent_begins_date_time(object.getString("event_begins_date_time"));
//                        }
//                        if (object.has("event_ends_date_time")
//                                && !TextUtils.isEmpty(object.getString("event_ends_date_time"))
//                                && !object.getString("event_ends_date_time").equals("null")) {
//                            eventDefinition.setEvent_ends_date_time(object.getString("event_ends_date_time"));
//                        }
//                        if (object.has("event_parking_begins_date_time")
//                                && !TextUtils.isEmpty(object.getString("event_parking_begins_date_time"))
//                                && !object.getString("event_parking_begins_date_time").equals("null")) {
//                            eventDefinition.setEvent_parking_begins_date_time(object.getString("event_parking_begins_date_time"));
//                        }
//                        if (object.has("event_parking_ends_date_time")
//                                && !TextUtils.isEmpty(object.getString("event_parking_ends_date_time"))
//                                && !object.getString("event_parking_ends_date_time").equals("null")) {
//                            eventDefinition.setEvent_parking_ends_date_time(object.getString("event_parking_ends_date_time"));
//                        }
//                        if (object.has("event_multi_sessions")
//                                && !TextUtils.isEmpty(object.getString("event_multi_sessions"))
//                                && !object.getString("event_multi_sessions").equals("null")) {
//                            eventDefinition.setEvent_multi_sessions(object.getBoolean("event_multi_sessions"));
//                        }
//                        if (object.has("event_indiv_session_regn_allowed")
//                                && !TextUtils.isEmpty(object.getString("event_indiv_session_regn_allowed"))
//                                && !object.getString("event_indiv_session_regn_allowed").equals("null")) {
//                            eventDefinition.setEvent_indiv_session_regn_allowed(object.getBoolean("event_indiv_session_regn_allowed"));
//                        }
//                        if (object.has("event_indiv_session_regn_required")
//                                && !TextUtils.isEmpty(object.getString("event_indiv_session_regn_required"))
//                                && !object.getString("event_indiv_session_regn_required").equals("null")) {
//                            eventDefinition.setEvent_indiv_session_regn_required(object.getBoolean("event_indiv_session_regn_required"));
//                        }
//                        if (object.has("event_full_regn_required")
//                                && !TextUtils.isEmpty(object.getString("event_full_regn_required"))
//                                && !object.getString("event_full_regn_required").equals("null")) {
//                            eventDefinition.setEvent_full_regn_required(object.getBoolean("event_full_regn_required"));
//                        }
//                        if (object.has("event_full_regn_fee")
//                                && !TextUtils.isEmpty(object.getString("event_full_regn_fee"))
//                                && !object.getString("event_full_regn_fee").equals("null")) {
//                            eventDefinition.setEvent_full_regn_fee(object.getString("event_full_regn_fee"));
//                        }
//
//                        if (object.has("event_multi_dates")
//                                && !TextUtils.isEmpty(object.getString("event_multi_dates"))
//                                && !object.getString("event_multi_dates").equals("null")) {
//                            eventDefinition.setEvent_multi_dates(object.getString("event_multi_dates"));
//                        }
//
//                        if (object.has("event_parking_timings")
//                                && !TextUtils.isEmpty(object.getString("event_parking_timings"))
//                                && !object.getString("event_parking_timings").equals("null")) {
//                            eventDefinition.setEvent_parking_timings(object.getString("event_parking_timings"));
//                        }
//
//                        if (object.has("event_event_timings")
//                                && !TextUtils.isEmpty(object.getString("event_event_timings"))
//                                && !object.getString("event_event_timings").equals("null")) {
//                            eventDefinition.setEvent_event_timings(object.getString("event_event_timings"));
//                        }
//
//                        if (object.has("expires_by")
//                                && !TextUtils.isEmpty(object.getString("expires_by"))
//                                && !object.getString("expires_by").equals("null")) {
//                            eventDefinition.setExpires_by(object.getString("expires_by"));
//                        }
//                        if (object.has("regn_reqd")
//                                && !TextUtils.isEmpty(object.getString("regn_reqd"))
//                                && !object.getString("regn_reqd").equals("null")) {
//                            eventDefinition.setRegn_reqd(object.getBoolean("regn_reqd"));
//                        }
//                        if (object.has("regn_reqd_for_parking")
//                                && !TextUtils.isEmpty(object.getString("regn_reqd_for_parking"))
//                                && !object.getString("regn_reqd_for_parking").equals("null")) {
//                            eventDefinition.setRegn_reqd_for_parking(object.getBoolean("regn_reqd_for_parking"));
//                        }
//                        if (object.has("renewable")
//                                && !TextUtils.isEmpty(object.getString("renewable"))
//                                && !object.getString("renewable").equals("null")) {
//                            eventDefinition.setRenewable(object.getBoolean("renewable"));
//                        }
//                        if (object.has("active")
//                                && !TextUtils.isEmpty(object.getString("active"))
//                                && !object.getString("active").equals("null")) {
//                            eventDefinition.setActive(object.getBoolean("active"));
//                        }
//
//                        if (object.has("event_multi_sessions")
//                                && !TextUtils.isEmpty(object.getString("event_multi_sessions"))
//                                && !object.getString("event_multi_sessions").equals("null")) {
//                            eventDefinition.setEvent_multi_sessions(object.getBoolean("event_multi_sessions"));
//                        }
//
//                        if (object.has("event_indiv_session_regn_allowed")
//                                && !TextUtils.isEmpty(object.getString("event_indiv_session_regn_allowed"))
//                                && !object.getString("event_indiv_session_regn_allowed").equals("null")) {
//                            eventDefinition.setEvent_indiv_session_regn_allowed(object.getBoolean("event_indiv_session_regn_allowed"));
//                        }
//
//                        if (object.has("event_indiv_session_regn_required")
//                                && !TextUtils.isEmpty(object.getString("event_indiv_session_regn_required"))
//                                && !object.getString("event_indiv_session_regn_required").equals("null")) {
//                            eventDefinition.setEvent_indiv_session_regn_required(object.getBoolean("event_indiv_session_regn_required"));
//                        }
//
//                        if (object.has("event_full_regn_required")
//                                && !TextUtils.isEmpty(object.getString("event_full_regn_required"))
//                                && !object.getString("event_full_regn_required").equals("null")) {
//                            eventDefinition.setEvent_full_regn_required(object.getBoolean("event_full_regn_required"));
//                        }
//
//                        if (object.has("event_full_regn_fee")
//                                && !TextUtils.isEmpty(object.getString("event_full_regn_fee"))
//                                && !object.getString("event_full_regn_fee").equals("null")) {
//                            eventDefinition.setEvent_full_regn_fee(object.getString("event_full_regn_fee"));
//                        }
//
//                        if (object.has("event_parking_fee")
//                                && !TextUtils.isEmpty(object.getString("event_parking_fee"))
//                                && !object.getString("event_parking_fee").equals("null")) {
//                            eventDefinition.setEvent_parking_fee(object.getString("event_parking_fee"));
//                        }
//
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            if (getActivity() != null) {
//                binding.rlProgressbar.setVisibility(View.GONE);
//                if (eventDefinition != null) {
//                    Log.e("#DEBUG", "   eventDefinition:  " + new Gson().toJson(eventDefinition));
//                }
//                new getparkingrules().execute();
//            }
//        }
//    }

    public class getparkingrules extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        JSONArray json1;
        String parkingurl = "_table/event_parking_rules";
        ArrayList<item> temp_array = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            binding.rlProgressbar.setVisibility(View.VISIBLE);
//            parkingurl = "_table/event_parking_rules?filter=(location_code%3D" + eventDefinition.getTownship_code() + ")%20AND%20(event_id%3D" + eventDefinition.getId() + ")";
            parkingurl = "_table/event_parking_rules?filter=(event_id%3D" + eventDefinition.getId() + ")";
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            temp_array.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("#DEBUG", "   getparkingrules:  " + url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setId(c.getString("id"));
                        item1.setLocation_code(c.getString("location_code"));
                        item1.setTime_rule(c.getString("time_rule"));
                        item1.setDay_rule(c.getString("day_type"));
                        item1.setMax_time(c.getString("max_duration"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        item1.setActive(c.getString("active"));
                        item1.setPricing(c.getString("pricing"));
                        item1.setPricing_duration(c.getString("pricing_duration"));
                        item1.setStart_time(c.getString("start_time"));
                        item1.setEnd_time(c.getString("end_time"));
                        item1.setMax_hours(c.getString("duration_unit"));
                        item1.setStart_hour(c.getString("start_hour"));
                        item1.setEnd_hour(c.getString("end_hour"));
                        item1.setLoationcode1(c.getString("location_code"));
                        item1.setMarker_type(c.getString("marker_type"));
                        item1.setDuration_unit(c.getString("duration_unit"));
                        item1.setMax_duration(c.getString("max_duration"));

                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item1.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }

                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item1.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }

                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item1.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }

                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item1.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }

                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item1.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }

                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item1.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item1.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }

                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item1.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }

                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item1.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }

                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item1.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }

                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item1.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }

                        if (c.has("Reservation_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_Fee"))
                                && !c.getString("Reservation_PostPayment_Fee").equals("null")) {
                            item1.setReservation_PostPayment_Fee(c.getString("Reservation_PostPayment_Fee"));
                        }

                        if (c.has("ParkNow_PostPayment_Fee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_Fee"))
                                && !c.getString("ParkNow_PostPayment_Fee").equals("null")) {
                            item1.setParkNow_PostPayment_Fee(c.getString("ParkNow_PostPayment_Fee"));
                        }

                        if (c.has("number_of_reservable_spots")
                                && !TextUtils.isEmpty(c.getString("number_of_reservable_spots"))
                                && !c.getString("number_of_reservable_spots").equals("null")) {
                            item1.setNumber_of_reservable_spots(c.getString("number_of_reservable_spots"));
                        }

                        if (c.has("event_id")
                                && !TextUtils.isEmpty(c.getString("event_id"))
                                && !c.getString("event_id").equals("null")) {
                            item1.setEvent_id(c.getInt("event_id"));
                        }

                        if (c.has("event_code")
                                && !TextUtils.isEmpty(c.getString("event_code"))
                                && !c.getString("event_code").equals("null")) {
                            item1.setEvent_code(c.getString("event_code"));
                        }

                        if (c.has("event_name")
                                && !TextUtils.isEmpty(c.getString("event_name"))
                                && !c.getString("event_name").equals("null")) {
                            item1.setEvent_name(c.getString("event_name"));
                        }

                        if (c.has("event_specific_date")
                                && !TextUtils.isEmpty(c.getString("event_specific_date"))
                                && !c.getString("event_specific_date").equals("null")) {
                            item1.setEvent_specific_date(c.getString("event_specific_date"));
                        }

                        if (c.has("event_multi_dates")
                                && !TextUtils.isEmpty(c.getString("event_multi_dates"))
                                && !c.getString("event_multi_dates").equals("null")) {
                            item1.setEvent_multi_dates(c.getString("event_multi_dates"));
                        }

                        if (c.has("event_dt_short_info")
                                && !TextUtils.isEmpty(c.getString("event_dt_short_info"))
                                && !c.getString("event_dt_short_info").equals("null")) {
                            item1.setEvent_dt_short_info(c.getString("event_dt_short_info"));
                        }

                        if (c.has("multi_day")
                                && !TextUtils.isEmpty(c.getString("multi_day"))
                                && !c.getString("multi_day").equals("null")) {
                            item1.setMulti_day(c.getBoolean("multi_day"));
                        }

                        if (c.has("multi_day_reserve_allowed")
                                && !TextUtils.isEmpty(c.getString("multi_day_reserve_allowed"))
                                && !c.getString("multi_day_reserve_allowed").equals("null")) {
                            item1.setMulti_day_reserve_allowed(c.getBoolean("multi_day_reserve_allowed"));
                        }

                        if (c.has("multi_day_parking_allowed")
                                && !TextUtils.isEmpty(c.getString("multi_day_parking_allowed"))
                                && !c.getString("multi_day_parking_allowed").equals("null")) {
                            item1.setMulti_day_parking_allowed(c.getBoolean("multi_day_parking_allowed"));
                        }

                        if (c.has("max_days")
                                && !TextUtils.isEmpty(c.getString("max_days"))
                                && !c.getString("max_days").equals("null")) {
                            item1.setMax_days(c.getString("max_days"));
                        }

//                        if (selectedSpot.getLocation_code().equals(item1.getLocation_code())) {
                        temp_array.add(item1);
//                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (getActivity() != null) {
                binding.rlProgressbar.setVisibility(View.GONE);
                if (json != null) {
                    if (temp_array.size() > 0) {
                        parking_rules.clear();
                        parking_rules.addAll(temp_array);
                        parkingRule = parking_rules.get(0);
//                    Log.e("#DEBUG", "  getparkingrules:  onPost:  parking_rules: " + new Gson().toJson(parking_rules));
                        Log.e("#DEBUG", "   parking_rules: Size:  " + parking_rules.size());
                        temp_array.clear();
                        updateViews();
                    } else {
//                        Toast.makeText(getActivity(), "Sorry, No rules define for this location!", Toast.LENGTH_SHORT).show();
//                        getActivity().onBackPressed();
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    @Override
    protected void updateViews() {

        if (eventDefinition != null) {
            eventImages.clear();

            if (eventDefinition.getEvent_logi_type() == EVENT_TYPE_AT_LOCATION) {
                binding.llParkingRate.setVisibility(View.VISIBLE);
                binding.llParkingTime.setVisibility(View.VISIBLE);
                binding.llParkingAddress.setVisibility(View.VISIBLE);
                binding.llEventAddress.setVisibility(View.VISIBLE);
            } else if (eventDefinition.getEvent_logi_type() == EVENT_TYPE_AT_VIRTUALLY) {
                binding.llParkingRate.setVisibility(View.GONE);
                binding.llParkingTime.setVisibility(View.GONE);
                binding.llParkingAddress.setVisibility(View.GONE);
                binding.llEventAddress.setVisibility(View.GONE);
            } else if (eventDefinition.getEvent_logi_type() == EVENT_TYPE_BOTH) {
                binding.llParkingRate.setVisibility(View.VISIBLE);
                binding.llParkingTime.setVisibility(View.VISIBLE);
                binding.llParkingAddress.setVisibility(View.VISIBLE);
                binding.llEventAddress.setVisibility(View.VISIBLE);
            }

            if (eventDefinition.getEventCategory() != null
                    && !TextUtils.isEmpty(eventDefinition.getEventCategory())
                    && !eventDefinition.getEventCategory().equals("null")) {
                if (eventDefinition.getEventCategory().equals("Past Events")
                        || eventDefinition.getEventCategory().equals("Past Virtual Events")) {
                    binding.llBottomOptions.setVisibility(View.GONE);
                } else {
                    binding.llBottomOptions.setVisibility(View.VISIBLE);
                }
            }


            if (eventDefinition.getEvent_image1() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_image1())
                    && !eventDefinition.getEvent_image1().equals("null")) {
                eventImages.add(eventDefinition.getEvent_image1());
            }
            if (eventDefinition.getEvent_image2() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_image2())
                    && !eventDefinition.getEvent_image2().equals("null")) {
                eventImages.add(eventDefinition.getEvent_image2());
            }
            if (eventDefinition.getEvent_blob_image() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_blob_image())) {
                eventImages.addAll(new Gson().fromJson(eventDefinition.getEvent_blob_image(),
                        new TypeToken<ArrayList<String>>() {
                        }.getType()));
            }
            eventImageAdapter.notifyDataSetChanged();

            if (eventDefinition.getCompany_logo() != null
                    && !TextUtils.isEmpty(eventDefinition.getCompany_logo())) {
                Glide.with(getActivity()).load(eventDefinition.getCompany_logo())
                        .into(binding.ivEventCompanyLogo);
            }
            if (!TextUtils.isEmpty(eventDefinition.getEvent_name())) {
                binding.tvEventTitle.setText(eventDefinition.getEvent_name());
            }
            if (!TextUtils.isEmpty(eventDefinition.getEvent_type())) {
                binding.tvEventType.setText(eventDefinition.getEvent_type());
            }
            if (!TextUtils.isEmpty(eventDefinition.getEvent_short_description())) {
                binding.tvEventShortDesc.setText(eventDefinition.getEvent_short_description());
            }
            if (!TextUtils.isEmpty(eventDefinition.getEvent_long_description())) {
                binding.tvEventLongDesc.setText(eventDefinition.getEvent_long_description());
            }
            if (eventDefinition.getEvent_begins_date_time() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_begins_date_time())) {
                try {
                    binding.tvEventStartAt.setText(dateFormat.format(dateFormatInput.parse(eventDefinition.getEvent_begins_date_time())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (eventDefinition.getEvent_ends_date_time() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_ends_date_time())) {
                try {
                    binding.tvEventEndAt.setText(dateFormat.format(dateFormatInput.parse(eventDefinition.getEvent_ends_date_time())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (eventDefinition.getLocation_lat_lng() != null
                    && !TextUtils.isEmpty(eventDefinition.getLocation_lat_lng())) {
                try {
                    JSONArray jsonArrayLatLng = new JSONArray(eventDefinition.getLocation_lat_lng());
                    for (int i = 0; i < jsonArrayLatLng.length(); i++) {
                        JSONArray jsonArray2 = jsonArrayLatLng.getJSONArray(i);
                        for (int j = 0; j < jsonArray2.length(); j++) {
                            String[] latLng = jsonArray2.getString(j).split(",");
                            if (latLng.length == 2) {
                                String lat = latLng[0];
                                String lng = latLng[1];
                                if (lat.equals(itemLocation.getLat()) && lng.equals(itemLocation.getLng())) {
                                    Log.e("#DEBUG", "    POS:  " + i);
                                    JSONArray jsonArray = new JSONArray(eventDefinition.getLocation_address());
                                    JSONArray jsonArray1 = jsonArray.getJSONArray(i);
                                    Log.e("#DEBUG", "    Address at POS: " + i + "  :" + jsonArray1.getString(0));
                                    binding.tvLocationAddress.setText(jsonArray1.getString(0));

                                    JSONArray jsonArray11 = new JSONArray(eventDefinition.getEvent_address());
                                    JSONArray jsonArray22 = jsonArray11.getJSONArray(i);
                                    binding.tvEventAddress.setText(jsonArray22.getString(0));
                                    itemLocation.setAddesss1(jsonArray22.getString(0));

                                }
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (!TextUtils.isEmpty(eventDefinition.getEvent_parking_begins_date_time())
                    && !TextUtils.isEmpty(eventDefinition.getEvent_parking_ends_date_time())) {
                try {
                    binding.tvParkingTime.setText(String.format("%s to %s",
                            dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_parking_begins_date_time())),
                            dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_parking_ends_date_time()))));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

//            if (!TextUtils.isEmpty(eventDefinition.getLocation_address())) {
//                binding.tvLocationAddress.setText(eventDefinition.getLocation_address());
//                try {
//                    JSONArray jsonArray = new JSONArray(eventDefinition.getLocation_address());
//                    if (jsonArray.length() > 0) {
//                        StringBuilder builder = new StringBuilder();
//                        for (int i = 0; i < jsonArray.length(); i++) {
//                            JSONArray jsonArray1 = jsonArray.getJSONArray(i);
//                            builder.append(jsonArray1.getString(0));
//                            builder.append("\n\n");
//                        }
//                        binding.tvLocationAddress.setText(builder.toString());
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }

//            if (!TextUtils.isEmpty(eventDefinition.getEvent_address())) {
//                binding.tvEventAddress.setText(eventDefinition.getEvent_address());
//                try {
//                    JSONArray jsonArray = new JSONArray(eventDefinition.getEvent_address());
//                    if (jsonArray.length() > 0) {
//                        StringBuilder builder = new StringBuilder();
//                        for (int i = 0; i < jsonArray.length(); i++) {
//                            JSONArray jsonArray1 = jsonArray.getJSONArray(i);
//                            builder.append(jsonArray1.getString(0));
//                            builder.append("\n\n");
//                        }
//                        binding.tvEventAddress.setText(builder.toString());
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }

            if (eventDefinition.isEvent_multi_sessions()) {
                binding.tvBtnEventSessions.setVisibility(View.VISIBLE);
                binding.tvBtnSessionParking.setVisibility(View.VISIBLE);
            } else {
                binding.tvBtnEventSessions.setVisibility(View.GONE);
                binding.tvBtnSessionParking.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(eventDefinition.getEvent_full_regn_fee())) {
                if (eventDefinition.getEvent_full_regn_fee().equals("0")) {
                    binding.tvEventRate.setText("FREE");
                } else {
                    binding.tvEventRate.setText(String.format("$%s", eventDefinition.getEvent_full_regn_fee()));
                }
            } else {
                binding.tvEventRate.setVisibility(View.GONE);
                binding.tvLabelCost.setVisibility(View.VISIBLE);
            }

            if (eventDefinition.getEvent_multi_dates() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_multi_dates())) {
                try {
                    JSONArray jsonArray = new JSONArray(eventDefinition.getEvent_multi_dates());
                    StringBuilder stringBuilderParking = new StringBuilder();
                    if (eventDefinition.getEvent_parking_timings() != null
                            && !TextUtils.isEmpty(eventDefinition.getEvent_parking_timings())) {
                        JSONArray jsonArrayParking = new JSONArray(eventDefinition.getEvent_parking_timings());
                        for (int i = 0; i < jsonArrayParking.length(); i++) {
                            JSONArray jsonArray2 = jsonArrayParking.getJSONArray(i);
                            if (i != 0) {
                                stringBuilderParking.append("\n");
                            }
                            if (jsonArray2 != null && jsonArray2.length() > 0) {
//                                    stringBuilderParking.append(jsonArray2.get(0).toString());
                                if (jsonArray2.get(0).toString().length() > 30) {
                                    String[] s = jsonArray2.get(0).toString().split(" - ");
                                    for (int j = 0; j < s.length; j++) {
                                        if (j != 0) {
                                            stringBuilderParking.append("-");
                                        }
                                        stringBuilderParking.append(DateTimeUtils.gmtToLocalDateAMPM(s[j]));
                                    }
                                } else {
                                    stringBuilderParking.append(jsonArray2.get(0).toString());
                                }
                            }
                        }
                    }
                    binding.tvParkingTime.setText(stringBuilderParking.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        if (mediaDefinitions.size() > 0) {
            binding.llMediaLinks.setVisibility(View.VISIBLE);
            binding.viewMediaLink.setVisibility(View.VISIBLE);
        } else {
            binding.llMediaLinks.setVisibility(View.GONE);
            binding.viewMediaLink.setVisibility(View.GONE);
        }
        if (parkingRule != null) {
            if (parkingRule.getPricing().equals("0")) {
                binding.tvParkingRate.setText("FREE");
            } else {
                binding.tvParkingRate.setText(String.format("$%s/%s %s", parkingRule.getPricing(),
                        parkingRule.getPricing_duration(), parkingRule.getDuration_unit()));
            }
        }

    }

    private void openDirections() {
        SharedPreferences sh1 = getActivity().getSharedPreferences("directiontomydirection", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed1 = sh1.edit();
        ed1.putString("lat", itemLocation.getLat());
        ed1.putString("lang", itemLocation.getLng());
        ed1.putString("address", itemLocation.getAddesss1());
        ed1.commit();
        SharedPreferences backtotimer = getActivity().getSharedPreferences("back_parkezly", Context.MODE_PRIVATE);
        SharedPreferences.Editor eddd = backtotimer.edit();
        eddd.putString("isback", "yes");
        eddd.commit();
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                   /* ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                    ft.add(R.id.My_Container_1_ID, new f_direction(), "NewFragmentTag");
                    ft.commit();*/
        f_direction pay = new f_direction();
        ft.add(R.id.My_Container_1_ID, pay, "reservation_details");
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(pay);
        ft.commitAllowingStateLoss();
    }

    private void openStreetView() {
        Intent i = new Intent(getActivity(), panstreetview.class);
        i.putExtra("lat", itemLocation.getLat());
        i.putExtra("lang", itemLocation.getLng());
        i.putExtra("title", eventDefinition.getEvent_name());
        i.putExtra("address", itemLocation.getAddesss1());
        getActivity().startActivity(i);
    }

    @Override
    protected void setListeners() {

        binding.tvBtnDirection.setOnClickListener(v -> {
            openDirections();
        });

        binding.tvBtnStreetView.setOnClickListener(v -> {
            openStreetView();
        });

        binding.ivBtnPrev.setOnClickListener(v -> {
            try {
                if (binding.vpEventImage.getCurrentItem() == 0) {
                    binding.vpEventImage.setCurrentItem(1);
                    binding.ivBtnNext.setVisibility(View.GONE);
                    binding.ivBtnPrev.setVisibility(View.VISIBLE);
                } else {
                    binding.vpEventImage.setCurrentItem(0);
                    binding.ivBtnNext.setVisibility(View.VISIBLE);
                    binding.ivBtnPrev.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        binding.ivBtnNext.setOnClickListener(v -> {
            try {
                if (binding.vpEventImage.getCurrentItem() == 0) {
                    binding.vpEventImage.setCurrentItem(1);
                    binding.ivBtnNext.setVisibility(View.GONE);
                    binding.ivBtnPrev.setVisibility(View.VISIBLE);
                } else {
                    binding.ivBtnPrev.setVisibility(View.GONE);
                    binding.ivBtnNext.setVisibility(View.VISIBLE);
                    binding.vpEventImage.setCurrentItem(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        binding.tvBtnEventParking.setOnClickListener(v -> {
//            if (getActivity() != null) {
//                Fragment fragment = new FinalReservationFragment();
//                Bundle bundle = getArguments();
//                if (bundle != null) {
//                    bundle.putBoolean("isEventParking", true);
//                    bundle.putString("eventDefinition", new Gson().toJson(eventDefinition));
//                }
//                if (parkingRule != null) {
//                    rParking.setMax_days(parkingRule.getMax_days());
//                    rParking.setMax_days(parkingRule.getMax_days());
//                    rParking.setEvent_id(parkingRule.getEvent_id());
//                    if (!TextUtils.isEmpty(parkingRule.getEvent_code())) {
//                        rParking.setEvent_code(parkingRule.getEvent_code());
//                    }
//                    if (!TextUtils.isEmpty(parkingRule.getEvent_name())) {
//                        rParking.setEvent_name(parkingRule.getEvent_name());
//                    }
//                    if (!TextUtils.isEmpty(parkingRule.getEvent_specific_date())) {
//                        rParking.setEvent_specific_date(parkingRule.getEvent_specific_date());
//                    }
//                    if (!TextUtils.isEmpty(parkingRule.getEvent_multi_dates())) {
//                        rParking.setEvent_multi_dates(parkingRule.getEvent_multi_dates());
//                    }
//                    if (!TextUtils.isEmpty(parkingRule.getEvent_dt_short_info())) {
//                        rParking.setEvent_dt_short_info(parkingRule.getEvent_dt_short_info());
//                    }
//
//                    rParking.setMulti_day(parkingRule.isMulti_day());
//                    rParking.setMulti_day_reserve_allowed(parkingRule.isMulti_day_reserve_allowed());
//                    rParking.setMulti_day_parking_allowed(parkingRule.isMulti_day_parking_allowed());
//
//                    selectedSpot.setPricing(parkingRule.getPricing());
//                    selectedSpot.setPricing_duration(parkingRule.getPricing_duration());
//                    selectedSpot.setDuration_unit(parkingRule.getDuration_unit());
//                    selectedSpot.setMax_hours(parkingRule.getMax_hours());
//                    selectedSpot.setStart_time(parkingRule.getStart_time());
//                    selectedSpot.setStart_hour(parkingRule.getStart_hour());
//                    selectedSpot.setEnd_time(parkingRule.getEnd_time());
//                    selectedSpot.setEnd_hour(parkingRule.getEnd_hour());
//                    rParking.setParkingAvailability(parkingRule.getTime_rule());
//                    rParking.setMaxTime(parkingRule.getMax_duration());
//                    rParking.setParkingRate(parkingRule.getPricing());
//                    rParking.setParkingDuration(parkingRule.getPricing_duration());
//                    rParking.setParkingDurationUnit(parkingRule.getDuration_unit());
//                }
//
//                if (bundle != null) {
//                    bundle.putString("rParking", new Gson().toJson(rParking));
//                }
//
//                fragment.setArguments(bundle);
////                                        MyApplication.getInstance().paidparkhereFragment = pay;
//                final FragmentTransaction ft = getFragmentManager().beginTransaction();
//                ft.add(R.id.My_Container_1_ID, fragment, "rParking1");
//                fragmentStack.lastElement().onPause();
//                ft.hide(fragmentStack.lastElement());
//                fragmentStack.push(fragment);
//                ft.commitAllowingStateLoss();
//            }
        });

        binding.tvBtnEventSessions.setOnClickListener(v -> {
            ((vchome) getActivity()).show_back_button();
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            EventSessionsFragment pay = new EventSessionsFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isEventParking", true);
            bundle.putString("eventDefinition", new Gson().toJson(eventDefinition));
            bundle.putString("parkingRule", new Gson().toJson(parkingRule));
            bundle.putString("itemLocation", new Gson().toJson(itemLocation));
            bundle.putString("parkingRule", new Gson().toJson(parkingRule));
            bundle.putString("rParking", new Gson().toJson(rParking));
            pay.setArguments(bundle);
            ft.add(R.id.My_Container_1_ID, pay);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(pay);
            ft.commitAllowingStateLoss();
        });

        binding.tvBtnEventRegistration.setOnClickListener(v -> {
//            Toast.makeText(getActivity(), "Coming soon!", Toast.LENGTH_SHORT).show();
            ((vchome) getActivity()).show_back_button();
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            EventRegistrationFragment pay = new EventRegistrationFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isEventParking", true);
            bundle.putString("eventDefinition", new Gson().toJson(eventDefinition));
            bundle.putString("itemLocation", new Gson().toJson(itemLocation));
            bundle.putString("parkingRule", new Gson().toJson(parkingRule));
//            bundle.putString("rParking", new Gson().toJson(rParking));
            pay.setArguments(bundle);
            ft.add(R.id.My_Container_1_ID, pay);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(pay);
            ft.commitAllowingStateLoss();

//            showDialogConfirmRegistration();
        });
    }

    private void showDialogConfirmRegistration() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_event_reg_confirm, null);
        builder.setView(dialogView);
        ImageView ivBtnCancel = dialogView.findViewById(R.id.ivBtnCancel);
        TextView tvMessage = dialogView.findViewById(R.id.tvMessage);
        TextView tvCost = dialogView.findViewById(R.id.tvCost);
        Button btnWallet = dialogView.findViewById(R.id.btnWallet);
        Button btnPayPal = dialogView.findViewById(R.id.btnPayPal);
        String message = "Are you sure, you want to register for event " + eventDefinition.getEvent_name();
        message = message + "\nRegistration cost for this event is";
        tvMessage.setText(message);
        tvCost.setText("$" + eventDefinition.getEvent_full_regn_fee());

        AlertDialog alertDialog = builder.create();

        ivBtnCancel.setOnClickListener(v -> {
            alertDialog.dismiss();
        });
        btnWallet.setOnClickListener(v -> {
            alertDialog.dismiss();
            selectedPaymentMethod = "Wallet";
            new fetchWalletBalance().execute();
//            new registerForEvent().execute();

        });
        btnPayPal.setOnClickListener(v -> {
            alertDialog.dismiss();
            selectedPaymentMethod = "Paypal";
            PaypalPaymentIntegration(eventDefinition.getEvent_full_regn_fee());
        });
        alertDialog.show();
    }

    private void PaypalPaymentIntegration(String amount) {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE, amount);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.e("#DEBUG", confirm.toJSONObject().toString(4));
                    Log.e("#DEBUG", confirm.getPayment().toJSONObject().toString(4));
                    JSONObject jsonObj = new JSONObject(confirm.getPayment().toJSONObject().toString(4));
                    Log.e("#DEBUG", "  JSON IS LIKE" + jsonObj);
                    Log.e("#DEBUG", "  short_description : " + jsonObj.getString("short_description"));
                    Log.e("#DEBUG", "  amount : " + jsonObj.getString("amount"));
                    Log.e("#DEBUG", "  intent : " + jsonObj.getString("intent"));
                    Log.e("#DEBUG", "  currency_code : " + jsonObj.getString("currency_code"));
                    JSONObject jsonObjResponse = confirm.toJSONObject().getJSONObject("response");
                    transection_id = jsonObjResponse.getString("id");
                    if (transection_id != null) {
                        selectedPaymentMethod = "Paypal";
                        new registerForEvent().execute();
                    } else {
                        transactionFailedAlert();
                    }
                    Log.e("#DEBUG", "  transactionId: " + transection_id);

                } catch (JSONException e) {
                    Log.e("#DEBUG", "an extremely unlikely failure occurred: ", e);
                }
            }
        } else {
            transactionFailedAlert();
        }
    }

    public void transactionFailedAlert() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Transaction Fail try again");
        alertDialog.setMessage("Fail");
        alertDialog.setPositiveButton("OK", (dialog, which) -> {
        });

        alertDialog.show();
    }

    private PayPalPayment getThingToBuy(String paymentIntent, String payamount) {
        return new PayPalPayment(new BigDecimal(payamount), "USD", "Parking Payment", paymentIntent);
    }

    public class registerForEvent extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String urlReg = "_table/event_registration";
        JSONObject json1;
        String re_id;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues = new HashMap<String, Object>();

            jsonValues.put("date_time", DateTimeUtils.getSqlFormatDate(Calendar.getInstance().getTime()));
            jsonValues.put("manager_type", eventDefinition.getManager_type());
            jsonValues.put("manager_type_id", eventDefinition.getManager_type_id());
            jsonValues.put("twp_id", eventDefinition.getTwp_id());
            jsonValues.put("township_code", eventDefinition.getTownship_code());
            jsonValues.put("township_name", eventDefinition.getTownship_name());
            jsonValues.put("company_id", eventDefinition.getCompany_id());
            jsonValues.put("company_code", eventDefinition.getCompany_code());
            jsonValues.put("company_name", eventDefinition.getCompany_name());

            jsonValues.put("event_id", eventDefinition.getEvent_id());
            jsonValues.put("event_type", eventDefinition.getEvent_type());
            jsonValues.put("event_name", eventDefinition.getEvent_name());
            if (eventDefinition.getEvent_begins_date_time() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_begins_date_time())) {
                jsonValues.put("event_begins_date_time", DateTimeUtils.gmtToLocalDate(eventDefinition.getEvent_begins_date_time()));
            }
            if (eventDefinition.getEvent_ends_date_time() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_ends_date_time())) {
                jsonValues.put("event_ends_date_time", DateTimeUtils.gmtToLocalDate(eventDefinition.getEvent_ends_date_time()));
            }
            jsonValues.put("lat", itemLocation.getLat());
            jsonValues.put("lng", itemLocation.getLng());
            jsonValues.put("address1", itemLocation.getAddesss1());

            jsonValues.put("event_regn_status", "Active");
            jsonValues.put("user_id", logindeatl.getString("id", ""));
            jsonValues.put("user_phone_num", "");
            jsonValues.put("user_device_num", "");
            jsonValues.put("user_email", logindeatl.getString("email", ""));
            jsonValues.put("username", logindeatl.getString("name", ""));
            jsonValues.put("ip", getLocalIpAddress());

            jsonValues.put("payment_method", selectedPaymentMethod);
            jsonValues.put("event_rate", eventDefinition.getEvent_full_regn_fee());
            jsonValues.put("regn_subtotal", eventDefinition.getEvent_full_regn_fee());
            jsonValues.put("wallet_trx_id", wallet_id);
            jsonValues.put("event_price_total", eventDefinition.getEvent_full_regn_fee());
            jsonValues.put("payment_choice", "");
            jsonValues.put("platform", "Android");


            if (selectedPaymentMethod.equals("Paypal")) {
                jsonValues.put("ipn_txn_id", transection_id);
                jsonValues.put("ipn_payment", "Paypal");
                jsonValues.put("ipn_status", "success");
                jsonValues.put("ipn_address", "");
            } else {
                jsonValues.put("ipn_txn_id", "");
                jsonValues.put("ipn_payment", "");
                jsonValues.put("ipn_status", "");
                jsonValues.put("ipn_address", "");
            }

            jsonValues.put("event_regn_fee_category_id", "1");
            jsonValues.put("event_regn_fee_category", "event_regn_fee_full");
            jsonValues.put("event_regn_approval_reqd", eventDefinition.isRegn_needed_approval());

            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + urlReg;
            HttpResponse response = null;
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            post.setEntity(entity);
            Log.e("#DEBUG", "  updateURL:  " + url);
            Log.e("#DEBUG", "   update Params: " + String.valueOf(json));
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   Add/Edit Event:  Response:  " + responseStr);
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.e("#DEBUG", "  update: ResponseID: " + re_id);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.hide();
            if (getActivity() != null && json1 != null) {
                if (re_id != null && !re_id.equals("null")) {
                    Toast.makeText(getActivity(), "Registration Success!", Toast.LENGTH_SHORT).show();
                }
                super.onPostExecute(s);
            }
        }
    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("ip", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("aax", ex.toString());
        }
        return null;
    }

    public class fetchWalletBalance extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String userid = logindeatl.getString("id", "null");
        JSONObject json, json1;
        String wallbalurl = "_table/user_wallet?order=date_time%20DESC";
        //String wallbalurl = "_table/user_wallet?filter=(user_id%3D'"+userid+"')";
        String w_id;

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + wallbalurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            HttpEntity resEntity = response.getEntity();
            String responseStr = null;
            if (response != null) {
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        String user = c.getString("user_id");

                        if (user.equals(userid)) {
                            nbal = c.getString("new_balance");
                            if (!nbal.equals("null")) {
                                cbal = c.getString("current_balance");
                                item.setAmount(nbal);
                                item.setPaidamount(cbal);
                                wallbalarray.add(item);
                                break;
                            }
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            if (getActivity() != null && json != null) {
                if (wallbalarray.size() > 0) {
                    double bal = Double.parseDouble(cbal);
                    if (bal > 0) {
                        finalpayamount = Double.parseDouble(eventDefinition.getEvent_full_regn_fee());
                        newbal = bal - finalpayamount;
                        String finallab2 = String.format("%.2f", bal);
                        finallab = String.format("%.2f", finalpayamount);
                        if (newbal < 0) {
                            android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("alert");
                            alertDialog.setMessage("You don't have enough funds in your wallet.");
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialog.show();

                        } else {
                            android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("Wallet Balance: $" + finallab2);
                            alertDialog.setMessage("Would you like to pay from your ParkEZly wallet ? With service and transaction fee your total will be " + "$" + finallab + "");
                            alertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    new updateWalletBalance().execute();
                                }
                            });
                            alertDialog.setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialog.show();
                        }
                    } else {
                        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("alert");
                        alertDialog.setMessage("You don't have enough funds in your wallet.");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }

                } else {
                    android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("alert");
                    alertDialog.setMessage("You don't have enough funds in your wallet.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
            super.onPostExecute(s);

        }
    }

    public class updateWalletBalance extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONObject json12;
        String walleturl = "_table/user_wallet";
        String w_id = "";

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("paid_date", DateTimeUtils.getSqlFormatDate(Calendar.getInstance().getTime()));
            jsonValues1.put("last_paid_amt", finalpayamount);
            jsonValues1.put("ip", getLocalIpAddress());
            jsonValues1.put("remember_me", "");
            jsonValues1.put("current_balance", newbal);
            jsonValues1.put("ipn_txn_id", "");
            jsonValues1.put("ipn_custom", "");
            jsonValues1.put("ipn_payment", "");
            jsonValues1.put("date_time", DateTimeUtils.getSqlFormatDate(Calendar.getInstance().getTime()));
            jsonValues1.put("ipn_address", "");
            jsonValues1.put("user_id", id);
            jsonValues1.put("new_balance", newbal);
            jsonValues1.put("action", "deduction");

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        wallet_id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            //progressBar.setVisibility(View.GONE);
            if (getActivity() != null && json12 != null) {

                if (!wallet_id.equals("")) {
//                    printTestData();
                    new registerForEvent().execute();
                }

            }
            super.onPostExecute(s);
        }
    }

    @Override
    protected void initViews(View v) {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Registering for event...");
        progressDialog.setCancelable(false);

        dateFormat = new SimpleDateFormat("dd MMM, hh:mm a", Locale.getDefault());
        dateFormatInput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        eventImageAdapter = new EventImageAdapter();
        binding.vpEventImage.setAdapter(eventImageAdapter);

        mediaAdapter = new MediaAdapter();
        binding.rvMediaLinks.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        binding.rvMediaLinks.setAdapter(mediaAdapter);

    }

    private class EventImageAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return eventImages.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == ((LinearLayout) o);
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View itemView = LayoutInflater.from(getActivity()).inflate(R.layout.item_event_image, container, false);

            ImageView imageView = itemView.findViewById(R.id.ivEventImage);
            Glide.with(getActivity()).load(eventImages.get(position)).into(imageView);

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((LinearLayout) object);
        }
    }

    private class MediaAdapter extends RecyclerView.Adapter<MediaAdapter.MediaHolder> {

        @NonNull
        @Override
        public MediaAdapter.MediaHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new MediaAdapter.MediaHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_media_link_info, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MediaAdapter.MediaHolder mediaHolder, int i) {
            mediaHolder.tvBtnPlatform.setText(mediaDefinitions.get(i).getMedia_platform());
        }

        @Override
        public int getItemCount() {
            return mediaDefinitions.size();
        }

        public class MediaHolder extends RecyclerView.ViewHolder {
            TextView tvBtnPlatform;

            public MediaHolder(@NonNull View itemView) {
                super(itemView);
                tvBtnPlatform = itemView.findViewById(R.id.tvBtnPlatform);

                itemView.setOnClickListener(v -> {
                    final int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        MediaDefinition mediaDefinition = mediaDefinitions.get(pos);
                        if (mediaDefinition != null && !TextUtils.isEmpty(mediaDefinition.getMedia_platform())) {
                            if (mediaDefinition != null && !TextUtils.isEmpty(mediaDefinition.getMedia_platform())) {
                                if (mediaDefinition.getMedia_platform().equals("Zoom")) {
                                    showMediaInfoForZoom(mediaDefinition);
                                } else if (mediaDefinition.getMedia_platform().equals("YouTube")) {
                                    showMediaInfoForYoutube(mediaDefinition);
                                } else if (mediaDefinition.getMedia_platform().equals("Facebook")) {
                                    showMediaInfoForFacebook(mediaDefinition);
                                } else if (mediaDefinition.getMedia_platform().equals("Google Meet")) {
                                    showMediaInfoForGoogleMeet(mediaDefinition);
                                } else if (mediaDefinition.getMedia_platform().equals("Google Classroom")) {
                                    showMediaInfoForGoogleClassroom(mediaDefinition);
                                } else if (mediaDefinition.getMedia_platform().equals("Skype")) {
                                    showMediaInfoForSkype(mediaDefinition);
                                } else if (mediaDefinition.getMedia_platform().equals("Website")) {
                                    openWebUrl(mediaDefinition.getMedia_domain_url());
                                } else if (mediaDefinition.getMedia_platform().equals("PDF")) {
                                    PDFViewFragment pdfViewFragment = new PDFViewFragment();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("mediaDefinition", new Gson().toJson(mediaDefinition));
                                    pdfViewFragment.setArguments(bundle);
                                    replaceFragment(pdfViewFragment, "pdfView");
                                } else if (mediaDefinition.getMedia_platform().equals("Powerpoint")) {
                                    downloadFile(mediaDefinition.getPptFilePath());
                                } else if (mediaDefinition.getMedia_platform().equals("Slideshow")) {
                                    downloadFile(mediaDefinition.getFilePath());
                                } else if (mediaDefinition.getMedia_platform().equals("Audio/Video")) {
                                    String path = mediaDefinition.getAudioVideoPath();
                                    Intent intent = new Intent(Intent.ACTION_VIEW);
                                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);//DO NOT FORGET THIS EVER

                                    if (mediaDefinition.getAudioVideoType().equals("Audio")) {
                                        intent.setDataAndType(Uri.parse(path), "audio/*");
                                    } else if (mediaDefinition.getAudioVideoType().equals("Video")) {
                                        intent.setDataAndType(Uri.parse(path), "video/*");
                                    }
                                    startActivity(intent);
                                }
                            }
                        }
                    }
                });
            }
        }
    }

    private void downloadFile(String url) {
        progressDialog.setMessage("Downloading file...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/parkezly/");
        if (!file.exists()) {
            file.mkdir();
        }
        Log.e("#DEBUG", "  FileName:  " + "test." + url.substring(url.lastIndexOf(".") + 1));
        AndroidNetworking.download(url, file.getAbsolutePath(), "test." + url.substring(url.lastIndexOf(".") + 1))
                .build()
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        progressDialog.dismiss();
                        try {
                            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/parkezly/test.ppt");
                            Uri uri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", file);
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "Sorry, Unable to open file!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Sorry, Unable to open file!", Toast.LENGTH_SHORT).show();
                        Log.e("#DEBUG", "  downloadFile: onError: " + anError.getMessage());
                    }
                });
    }


    private void showMediaInfoForSkype(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoSkypeBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_skype, null, false);
        builder.setView(zoomBinding.getRoot());

        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getSkypeStreamUrl())) {
            zoomBinding.llSpecialUrl.setVisibility(View.VISIBLE);
            zoomBinding.tvSpecialUrl.setText(mediaDefinition.getSkypeStreamUrl());
        }

        AlertDialog alertDialog = builder.create();
        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnOpen.setOnClickListener(v -> {
        });

        zoomBinding.ivBtnOpenSkypeApp.setOnClickListener(v -> {
            openSkypeUrl(mediaDefinition.getYoutubeStreamUrl());
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
            openWebUrl(mediaDefinition.getSkypeStreamUrl());
        });
        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnOpenSpecialUrl.setOnClickListener(v -> {
            openWebUrl(mediaDefinition.getSkypeStreamUrl());
        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Stream Url", mediaDefinition.getSkypeStreamUrl());
        });

        alertDialog.show();
    }

    private void showMediaInfoForGoogleClassroom(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoGoogleClassroomBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_google_classroom, null, false);
        builder.setView(zoomBinding.getRoot());

        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getGoogleClasstroomCode())) {
            zoomBinding.llClassCode.setVisibility(View.VISIBLE);
            zoomBinding.tvClassCode.setText(mediaDefinition.getGoogleClasstroomCode());
        }

        AlertDialog alertDialog = builder.create();
        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnOpen.setOnClickListener(v -> {
        });

        zoomBinding.ivBtnOpenClassroomApp.setOnClickListener(v -> {
            openGoogleClassroomUrl(mediaDefinition.getMedia_domain_url());
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
            openWebUrl(mediaDefinition.getGoogleClasstroomCode());
        });
        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Classroom Code", mediaDefinition.getGoogleClasstroomCode());
        });

        alertDialog.show();
    }

    private void showMediaInfoForGoogleMeet(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoGoogleMeetBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_google_meet, null, false);
        builder.setView(zoomBinding.getRoot());

        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getGoogleMeetUrl())) {
            zoomBinding.llMeetingUrl.setVisibility(View.VISIBLE);
            zoomBinding.tvSpecialUrl.setText(mediaDefinition.getGoogleMeetUrl());
        }

        if (!TextUtils.isEmpty(mediaDefinition.getGoogleMeetPhoneNumber())) {
            zoomBinding.llMeetingPhoneNumber.setVisibility(View.VISIBLE);
            zoomBinding.tvMeetingPhoneNumber.setText(mediaDefinition.getGoogleMeetPhoneNumber());
        }
        if (!TextUtils.isEmpty(mediaDefinition.getGoogleMeetPassword())) {
            zoomBinding.llMeetingPassword.setVisibility(View.VISIBLE);
            try {
                zoomBinding.tvMeetingPassword.setText(AESEncyption.decrypt(mediaDefinition.getGoogleMeetPassword()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        AlertDialog alertDialog = builder.create();
        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnOpen.setOnClickListener(v -> {
        });

        zoomBinding.ivBtnOpenMeetApp.setOnClickListener(v -> {
            openGoogleMeetUrl(mediaDefinition.getGoogleMeetUrl());
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
            openWebUrl(mediaDefinition.getGoogleMeetUrl());
        });
        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnOpenUrl.setOnClickListener(v -> {
            openWebUrl(mediaDefinition.getGoogleMeetUrl());
        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Meeting Url", mediaDefinition.getGoogleMeetUrl());
        });

        zoomBinding.ivBtnCopyMeetingPhoneNumber.setOnClickListener(v -> {
            myApp.copyText("Meeting Phone Number", mediaDefinition.getGoogleMeetPhoneNumber());
        });

        zoomBinding.ivBtnCopyMeetingPassword.setOnClickListener(v -> {
            try {
                myApp.copyText("Meeting Password", AESEncyption.decrypt(mediaDefinition.getGoogleMeetPassword()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        zoomBinding.ivBtnDialMeetingPhoneNumber.setOnClickListener(v -> {
            /*if (mediaDefinition.getZoomPhone().contains(",")) {
                String[] phone = mediaDefinition.getZoomPhone().split(",");
                if (phone.length > 0 && phone[0] != null) {
                    dialNumber("tel:" + phone[0]);
                }
            } else {
                dialNumber("tel:" + mediaDefinition.getZoomPhone());
            }*/
        });

        alertDialog.show();
    }

    private void showMediaInfoForFacebook(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoFacebookBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_facebook, null, false);
        builder.setView(zoomBinding.getRoot());

        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getFacebookServerUrl())) {
            zoomBinding.llStreamUrl.setVisibility(View.VISIBLE);
            zoomBinding.tvStreamUrl.setText(mediaDefinition.getFacebookServerUrl());
        }

        if (!TextUtils.isEmpty(mediaDefinition.getFacebookStreamKey())) {
            zoomBinding.llStreamKey.setVisibility(View.VISIBLE);
            try {
                zoomBinding.tvStreamKey.setText(AESEncyption.decrypt(mediaDefinition.getFacebookStreamKey()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        AlertDialog alertDialog = builder.create();
        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnOpen.setOnClickListener(v -> {
        });

        zoomBinding.ivBtnOpenFaceBookApp.setOnClickListener(v -> {
            openFaceBookUrl(mediaDefinition.getFacebookServerUrl());
        });
        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
            openWebUrl(mediaDefinition.getFacebookServerUrl());
        });
        zoomBinding.ivBtnOpenStreamUrl.setOnClickListener(v -> {
            openWebUrl(mediaDefinition.getFacebookServerUrl());
        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Streaming Url", mediaDefinition.getFacebookServerUrl());
        });
        zoomBinding.ivBtnCopyStreamKey.setOnClickListener(v -> {
            try {
                myApp.copyText("Streaming Url", AESEncyption.decrypt(mediaDefinition.getFacebookStreamKey()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        alertDialog.show();
    }

    private void showMediaInfoForYoutube(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoYoutubeBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_youtube, null, false);
        builder.setView(zoomBinding.getRoot());
        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getYoutubeStreamUrl())) {
            zoomBinding.llStreamingUrl.setVisibility(View.VISIBLE);
            zoomBinding.tvSpecialUrl.setText(mediaDefinition.getYoutubeStreamUrl());
            zoomBinding.tvUrlTitle.setText(mediaDefinition.getYoutubeStreamTitle());
        }

        AlertDialog alertDialog = builder.create();
        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnOpen.setOnClickListener(v -> {
//            Intent videoIntent = new Intent(getActivity(), YoutubeVideoActivity.class);
//            videoIntent.putExtra("videoURL", mediaDefinition.getYoutubeStreamUrl());
//            startActivity(videoIntent);
            String id, url = mediaDefinition.getYoutubeStreamUrl();
            if (url.contains("=")) {
                String ary[] = url.split("=");
                id = ary[ary.length - 1];
            } else {
                String ary[] = url.split("/");
                id = ary[ary.length - 1];
            }
            Log.d("YouTube", "id: " + id);
            Intent intent = YouTubeIntents.createPlayVideoIntentWithOptions(getActivity(), id, true, false);
            startActivity(intent);
        });
        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnOpenYouTubeApp.setOnClickListener(v -> {
            Toast.makeText(myApp, "App", Toast.LENGTH_SHORT).show();
            openYoutubeUrl(mediaDefinition.getYoutubeStreamUrl());
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
            Toast.makeText(myApp, "Browser", Toast.LENGTH_SHORT).show();
            openWebUrl(mediaDefinition.getYoutubeStreamUrl());
        });
//        zoomBinding.ivBtnOpenUrl.setOnClickListener(v -> {
//            openWebUrl(mediaDefinition.getYoutubeStreamUrl());
//        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Url ", mediaDefinition.getYoutubeStreamUrl());
        });
        zoomBinding.ivBtnCopyUrlTitle.setOnClickListener(v -> {
            myApp.copyText("Title ", mediaDefinition.getYoutubeStreamTitle());
        });
        alertDialog.show();
    }

    private void showMediaInfoForZoom(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoZoomBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_zoom, null, false);
        builder.setView(zoomBinding.getRoot());

        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getZoomSpecialUrl())) {
            zoomBinding.llSpecialUrl.setVisibility(View.VISIBLE);
            zoomBinding.tvSpecialUrl.setText(mediaDefinition.getZoomSpecialUrl());
        }
        if (!TextUtils.isEmpty(mediaDefinition.getZoomMeetingId())) {
            zoomBinding.llMeetingId.setVisibility(View.VISIBLE);
            zoomBinding.tvMeetingId.setText(mediaDefinition.getZoomMeetingId());
        }
        if (!TextUtils.isEmpty(mediaDefinition.getZoomPassword())) {
            zoomBinding.llMeetingPassword.setVisibility(View.VISIBLE);
            try {
                zoomBinding.tvMeetingPassword.setText(AESEncyption.decrypt(mediaDefinition.getZoomPassword()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(mediaDefinition.getZoomPhone())) {
            zoomBinding.llPhone.setVisibility(View.VISIBLE);
            zoomBinding.tvPhone.setText(mediaDefinition.getZoomPhone());
        }

        AlertDialog alertDialog = builder.create();
        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnOpen.setOnClickListener(v -> {
        });

        zoomBinding.ivBtnOpenZoomApp.setOnClickListener(v -> {
            openZoomUrl(mediaDefinition.getZoomSpecialUrl());
        });
        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
            openWebUrl(mediaDefinition.getZoomSpecialUrl());
        });
        zoomBinding.ivBtnOpenSpecialUrl.setOnClickListener(v -> {
            openWebUrl(mediaDefinition.getZoomSpecialUrl());
        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Stream Url", mediaDefinition.getZoomSpecialUrl());
        });
        zoomBinding.ivBtnCopyMeetingId.setOnClickListener(v -> {
            myApp.copyText("Meeting ID", mediaDefinition.getZoomMeetingId());
        });
        zoomBinding.ivBtnCopyMeetingPassword.setOnClickListener(v -> {
            try {
                myApp.copyText("Meeting Password", AESEncyption.decrypt(mediaDefinition.getZoomPassword()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        zoomBinding.ivBtnCopyPhone.setOnClickListener(v -> {
            myApp.copyText("Meeting Phone", mediaDefinition.getZoomPhone());
        });
        zoomBinding.btnManage.setOnClickListener(v -> {
            if (mediaDefinition.getMedia_platform().equals("Zoom")) {
                startActivity(new Intent(getActivity(), ZoomLoginActivity.class)
                        .putExtra("mediaDefinition", new Gson().toJson(mediaDefinition))
                        .putExtra("isJoinMeeting", true));
            }
        });
        zoomBinding.ivBtnDialPhone.setOnClickListener(v -> {
            if (mediaDefinition.getZoomPhone().contains(",")) {
                String[] phone = mediaDefinition.getZoomPhone().split(",");
                if (phone.length > 0 && phone[0] != null) {
                    dialNumber("tel:" + phone[0]);
                }
            } else {
                dialNumber("tel:" + mediaDefinition.getZoomPhone());
            }
        });
        alertDialog.show();


    }

    private void showMediaInfoForTeleConference(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoTeleConferenceBinding teleBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_tele_conference, null, false);
        builder.setView(teleBinding.getRoot());

        teleNumberAdapter = new TeleNumberAdapter();
        teleBinding.rvNumbers.setLayoutManager(new LinearLayoutManager(getActivity()));
        teleBinding.rvNumbers.setAdapter(teleNumberAdapter);
        if (mediaDefinition.getTeleConfNumbers() != null && mediaDefinition.getTeleConfNumbers().size() > 1) {
            teleNumbers.clear();
            teleNumbers.addAll(mediaDefinition.getTeleConfNumbers());
            teleNumberAdapter.notifyDataSetChanged();
        }
        if (mediaDefinition.getTeleConfCountryCode() != null && !TextUtils.isEmpty(mediaDefinition.getTeleConfCountryCode())) {
            teleCountryCode = mediaDefinition.getTeleConfCountryCode();
        }

        teleBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (mediaDefinition.getTeleConfMeetingId() != null && !TextUtils.isEmpty(mediaDefinition.getTeleConfMeetingId())) {
            teleBinding.llMeetingId.setVisibility(View.VISIBLE);
            teleBinding.tvMeetingId.setText(mediaDefinition.getTeleConfMeetingId());
        }
        if (mediaDefinition.getTeleConfMeetingPassword() != null && !TextUtils.isEmpty(mediaDefinition.getTeleConfMeetingPassword())) {
            teleBinding.llMeetingPassword.setVisibility(View.VISIBLE);
            try {
                teleBinding.tvMeetingPassword.setText(AESEncyption.decrypt(mediaDefinition.getTeleConfMeetingPassword()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        AlertDialog alertDialog = builder.create();
        teleBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        alertDialog.show();

    }

    private void dialNumber(String num) {
        Uri number = Uri.parse(num);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
        startActivity(callIntent);
    }

    private void openWebUrl(String url) {
        if (url.contains("www") || url.contains("http") || url.contains("https")) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                getActivity().startActivity(browserIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("www." + url.trim()));
                getActivity().startActivity(browserIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void openYoutubeUrl(String url) {
        String id;
        if (url.contains("=")) {
            String ary[] = url.split("=");
            id = ary[ary.length - 1];
        } else {
            String ary[] = url.split("/");
            id = ary[ary.length - 1];
        }
        Log.d("YouTube", "id: " + id);
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
        Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + id));
        appIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            getActivity().startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            getActivity().startActivity(webIntent);
        }
    }

    private void openFaceBookUrl(String facebookStreamUrl) {
        String urlFb = facebookStreamUrl;
        String FACEBOOK_PAGE_ID = "appetizerandroid";
        Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
        PackageManager packageManager = getContext().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            Log.d("AppVersion", "FB: " + versionCode);
            if (versionCode >= 3002850) {
                urlFb = "fb://facewebmodal/f?href=" + facebookStreamUrl;
            } else {                    //older versions of fb app
                urlFb = "fb://page/" + FACEBOOK_PAGE_ID;
            }
            facebookIntent.setData(Uri.parse(urlFb));
            startActivity(facebookIntent);
        } catch (PackageManager.NameNotFoundException e) {
            openAppInGooglePlay("com.facebook.katana");
        }
    }

    private void openSkypeUrl(String skypeStreamUrl) {  //https://join.skype.com/XFVtCgaoi464
        Intent skypeIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(skypeStreamUrl));
        skypeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PackageManager packageManager = getContext().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.skype.raider", PackageManager.GET_ACTIVITIES).versionCode;
            Log.d("AppVersion", "Zoom: " + versionCode);
            // Restrict the Intent to being handled by the Skype for Android client only.
            skypeIntent.setComponent(new ComponentName("com.skype.raider", "com.skype.raider.Main"));
            startActivity(skypeIntent);
        } catch (PackageManager.NameNotFoundException e) {
            openAppInGooglePlay("com.skype.raider");
        }
    }

    private void openZoomUrl(String zoomStreamUrl) {
        Intent zoomIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("zoomus://" + zoomStreamUrl));
        zoomIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PackageManager packageManager = getContext().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("us.zoom.videomeetings", 0).versionCode;
            startActivity(zoomIntent);
        } catch (PackageManager.NameNotFoundException e) {
            openAppInGooglePlay("us.zoom.videomeetings");
        }
    }

    private void openGoogleClassroomUrl(String googleClassroomStreamUrl) {
        Intent googleClassroomIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(googleClassroomStreamUrl));
        PackageManager packageManager = getContext().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.google.android.apps.classroom", 0).versionCode;
            startActivity(googleClassroomIntent);
        } catch (PackageManager.NameNotFoundException e) {
            openAppInGooglePlay("com.google.android.apps.classroom");
        }
    }

    private void openGoogleMeetUrl(String googleMeetStreamUrl) {
        Intent googleMeetIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(googleMeetStreamUrl));
        PackageManager packageManager = getContext().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.google.android.apps.meetings", 0).versionCode;
            startActivity(googleMeetIntent);
        } catch (PackageManager.NameNotFoundException e) {
            openAppInGooglePlay("com.google.android.apps.meetings");
        }
    }

    public void openAppInGooglePlay(String packageName) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
        } catch (android.content.ActivityNotFoundException e) { // if there is no Google Play on device
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
        }
    }

    private class TeleNumberAdapter extends RecyclerView.Adapter<TeleNumberAdapter.NumberHolder> {
        @NonNull
        @Override
        public TeleNumberAdapter.NumberHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new TeleNumberAdapter.NumberHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_tele_number_view, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull TeleNumberAdapter.NumberHolder holder, int position) {
            if (!TextUtils.isEmpty(teleNumbers.get(position))) {
                holder.tvPhone.setText(String.format("+%s%s", teleCountryCode, teleNumbers.get(position)));
            }
        }

        @Override
        public int getItemCount() {
            return teleNumbers.size();
        }

        public class NumberHolder extends RecyclerView.ViewHolder {
            TextView tvPhone;
            ImageView ivBtnDialPhone, ivBtnCopyPhone;

            public NumberHolder(@NonNull View itemView) {
                super(itemView);
                tvPhone = itemView.findViewById(R.id.tvPhone);
                ivBtnDialPhone = itemView.findViewById(R.id.ivBtnDialPhone);
                ivBtnCopyPhone = itemView.findViewById(R.id.ivBtnCopyPhone);

                ivBtnCopyPhone.setOnClickListener(view -> {
                    myApp.copyText("Conference Number", "+" + teleCountryCode + teleNumbers.get(getAdapterPosition()));
                });

                ivBtnDialPhone.setOnClickListener(view -> {
                    dialNumber("tel:" + "+" + teleCountryCode + teleNumbers.get(getAdapterPosition()));
                });
            }
        }
    }
}