package com.softmeasures.eventezlyadminplus.frament.config_setting;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragUserTypesBinding;
import com.softmeasures.eventezlyadminplus.frament.event.EventManagersFragment;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class UserTypesFragment extends BaseFragment {

    private FragUserTypesBinding binding;
    private String menu = "";
    private String configType;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_user_types, container, false);
        if (getArguments() != null) {
            menu = getArguments().getString("menu");
            configType = getArguments().getString("Config_type");
        }
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {
    }

    @Override
    protected void setListeners() {
        binding.llBtnCommercial.setOnClickListener(v -> {
            //openLocationsView("Commercial");
        });
        binding.llBtnTownship.setOnClickListener(v -> {

            Log.d("chhek_configType","=="+configType);
            ((vchome) getActivity()).show_back_button();
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            UserInfoFragment userFindFrg = new UserInfoFragment();
            Bundle bundle = new Bundle();
            bundle.putString("Config_type", configType);
           // bundle.putString("Company_type","commercialtype");
            userFindFrg.setArguments(bundle);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, userFindFrg, "user_info");
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(userFindFrg);
            ft.commitAllowingStateLoss();
        });
        binding.llBtnOthers.setOnClickListener(v -> {
            //openLocationsView("Other");
        });
        binding.llBtnSearch.setOnClickListener(v -> {
            ((vchome) getActivity()).show_back_button();
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            UserFindFragment userFindFrg = new UserFindFragment();
            Bundle bundle = new Bundle();
            bundle.putString("Config_type", configType);
            userFindFrg.setArguments(bundle);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            ft.add(R.id.My_Container_1_ID, userFindFrg, "user_find");
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(userFindFrg);
            ft.commitAllowingStateLoss();
        });
    }

    private void openLocationsView(String type) {
        ((vchome) getActivity()).show_back_button();
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        EventManagersFragment locationsByType = new EventManagersFragment();
        Bundle bundle = new Bundle();
        bundle.putString("parkingType", type);
        bundle.putString("Config_type", configType);
        locationsByType.setArguments(bundle);
        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.add(R.id.My_Container_1_ID, locationsByType, "partnersByType");
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(locationsByType);
        ft.commitAllowingStateLoss();
    }

    @Override
    protected void initViews(View v) {
    }
}
