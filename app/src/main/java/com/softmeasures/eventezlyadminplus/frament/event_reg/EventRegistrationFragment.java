package com.softmeasures.eventezlyadminplus.frament.event_reg;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragEventRegistrationBinding;
import com.softmeasures.eventezlyadminplus.dialogs.PaymentOptionDialog;
import com.softmeasures.eventezlyadminplus.dialogs.PaymentOptionListener;
import com.softmeasures.eventezlyadminplus.java.item;
import com.softmeasures.eventezlyadminplus.models.EventDefinition;
import com.softmeasures.eventezlyadminplus.models.EventSession;
import com.softmeasures.eventezlyadminplus.models.FeeCategory;
import com.softmeasures.eventezlyadminplus.models.User;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class EventRegistrationFragment extends BaseFragment implements PaymentOptionListener {

    private FragEventRegistrationBinding binding;
    private ProgressDialog progressDialog;

    private EventDefinition eventDefinition;
    private item itemLocation;

    private ArrayList<User> users = new ArrayList<>();
    private UserAdapter userAdapter;
    SimpleDateFormat dateFormatMain = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd MMM, hh:mm a", Locale.getDefault());

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    private static final String CONFIG_CLIENT_ID = "AQXvOmlRwvO2AI6H3XzqTe14pp4x6VtP2skKfUkhgjRAoNj0NoT7l0ZGeaRAXkAtYqyPCmxKEUXkrT89";
    private PayPalConfiguration config = new PayPalConfiguration().environment(CONFIG_ENVIRONMENT).clientId(CONFIG_CLIENT_ID);
    private static final int REQUEST_CODE_PAYMENT = 2;

    private String selectedPaymentMethod = "";
    private String transection_id = "";
    private String wallet_id = "";
    private String finallab, nbal, cbal = "0";
    private ArrayList<item> wallbalarray = new ArrayList<>();
    private double finalpayamount, newbal, subTotal;
    private String eventRegistrationId = "";
    private ArrayList<FeeCategory> feeCategories = new ArrayList<>();
    FeeCategory feeCategory;
    private boolean isSessionRegistration = false;
    private EventSession sessionDefinition;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_event_registration, container, false);
        if (getArguments() != null) {
            eventDefinition = new Gson().fromJson(getArguments().getString("eventDefinition"), EventDefinition.class);
            sessionDefinition = new Gson().fromJson(getArguments().getString("sessionDefinition"), EventSession.class);
            itemLocation = new Gson().fromJson(getArguments().getString("itemLocation"), item.class);
            isSessionRegistration = getArguments().getBoolean("isSessionRegistration", false);
        }
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    private void PaypalPaymentIntegration(String amount) {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE, amount);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent, String payamount) {
        return new PayPalPayment(new BigDecimal(payamount), "USD", "Parking Payment", paymentIntent);
    }

    public void transactionFailedAlert() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Transaction Fail try again");
        alertDialog.setMessage("Fail");
        alertDialog.setPositiveButton("OK", (dialog, which) -> {
        });

        alertDialog.show();
    }

    @Override
    public void onWalletClick() {
        new fetchWalletBalance().execute();
    }

    @Override
    public void onPaymentClick() {
        selectedPaymentMethod = "Paypal";
        if (eventDefinition.isRegn_user_info_reqd()) {
            double amount = 0.0;
            for (int i = 0; i < users.size(); i++) {
                amount = amount + Double.parseDouble(users.get(i).getFee());
            }
            finalpayamount = amount;
            PaypalPaymentIntegration(String.valueOf(amount));
        } else {
            PaypalPaymentIntegration(sessionDefinition.getEvent_session_regn_fee());
        }
    }

    @Override
    public void onPayLaterClick() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.e("#DEBUG", confirm.toJSONObject().toString(4));
                    Log.e("#DEBUG", confirm.getPayment().toJSONObject().toString(4));
                    JSONObject jsonObj = new JSONObject(confirm.getPayment().toJSONObject().toString(4));
                    Log.e("#DEBUG", "  JSON IS LIKE" + jsonObj);
                    Log.e("#DEBUG", "  short_description : " + jsonObj.getString("short_description"));
                    Log.e("#DEBUG", "  amount : " + jsonObj.getString("amount"));
                    Log.e("#DEBUG", "  intent : " + jsonObj.getString("intent"));
                    Log.e("#DEBUG", "  currency_code : " + jsonObj.getString("currency_code"));
                    JSONObject jsonObjResponse = confirm.toJSONObject().getJSONObject("response");
                    transection_id = jsonObjResponse.getString("id");
                    if (transection_id != null) {
                        selectedPaymentMethod = "Paypal";
                        new registerForEvent().execute();
                    } else {
                        transactionFailedAlert();
                    }
                    Log.e("#DEBUG", "  transactionId: " + transection_id);

                } catch (JSONException e) {
                    Log.e("#DEBUG", "an extremely unlikely failure occurred: ", e);
                }
            }
        } else {
            transactionFailedAlert();
        }
    }

    public class fetchWalletBalance extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String userid = logindeatl.getString("id", "null");
        JSONObject json, json1;
        String wallbalurl = "_table/user_wallet?order=date_time%20DESC";
        //String wallbalurl = "_table/user_wallet?filter=(user_id%3D'"+userid+"')";
        String w_id;

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + wallbalurl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            HttpEntity resEntity = response.getEntity();
            String responseStr = null;
            if (response != null) {
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();

                    json = new JSONObject(responseStr);

                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        String user = c.getString("user_id");

                        if (user.equals(userid)) {
                            nbal = c.getString("new_balance");
                            if (!nbal.equals("null")) {
                                cbal = c.getString("current_balance");
                                item.setAmount(nbal);
                                item.setPaidamount(cbal);
                                wallbalarray.add(item);
                                break;
                            }
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            if (getActivity() != null && json != null) {
                if (wallbalarray.size() > 0) {
                    double bal = Double.parseDouble(cbal);
                    if (bal > 0) {

                        if (eventDefinition.isRegn_user_info_reqd()) {
                            for (int i = 0; i < users.size(); i++) {
                                finalpayamount = finalpayamount + Double.parseDouble(users.get(i).getFee());
                            }
                        } else {
                            finalpayamount = Double.parseDouble(eventDefinition.getEvent_full_regn_fee());
                        }
                        newbal = bal - finalpayamount;
                        String finallab2 = String.format("%.2f", bal);
                        finallab = String.format("%.2f", finalpayamount);
                        if (newbal < 0) {
                            android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("alert");
                            alertDialog.setMessage("You don't have enough funds in your wallet.");
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialog.show();

                        } else {
                            android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("Wallet Balance: $" + finallab2);
                            alertDialog.setMessage("Would you like to pay from your ParkEZly wallet ? With service and transaction fee your total will be " + "$" + finallab + "");
                            alertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    new updateWalletBalance().execute();
                                }
                            });
                            alertDialog.setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialog.show();
                        }
                    } else {
                        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("alert");
                        alertDialog.setMessage("You don't have enough funds in your wallet.");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }

                } else {
                    android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("alert");
                    alertDialog.setMessage("You don't have enough funds in your wallet.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }
            }
            super.onPostExecute(s);

        }
    }

    public class updateWalletBalance extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String id = logindeatl.getString("id", "null");
        JSONObject json12;
        String walleturl = "_table/user_wallet";
        String w_id = "";

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("paid_date", DateTimeUtils.getSqlFormatDate(Calendar.getInstance().getTime()));
            jsonValues1.put("last_paid_amt", finalpayamount);
            jsonValues1.put("ip", getLocalIpAddress());
            jsonValues1.put("remember_me", "");
            jsonValues1.put("current_balance", newbal);
            jsonValues1.put("ipn_txn_id", "");
            jsonValues1.put("ipn_custom", "");
            jsonValues1.put("ipn_payment", "");
            jsonValues1.put("date_time", DateTimeUtils.getSqlFormatDate(Calendar.getInstance().getTime()));
            jsonValues1.put("ipn_address", "");
            jsonValues1.put("user_id", id);
            jsonValues1.put("new_balance", newbal);
            jsonValues1.put("action", "deduction");

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        wallet_id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            //progressBar.setVisibility(View.GONE);
            if (getActivity() != null && json12 != null) {

                if (!wallet_id.equals("")) {
//                    printTestData();
                    new registerForEvent().execute();
                }

            }
            super.onPostExecute(s);
        }
    }

    public class registerForEvent extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String urlReg = "_table/event_registration";
        JSONObject json1;
        String re_id;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues = new HashMap<String, Object>();

            jsonValues.put("date_time", DateTimeUtils.getSqlFormatDate(Calendar.getInstance().getTime()));
            jsonValues.put("manager_type", eventDefinition.getManager_type());
            jsonValues.put("manager_type_id", eventDefinition.getManager_type_id());
            jsonValues.put("twp_id", eventDefinition.getTwp_id());
            jsonValues.put("township_code", eventDefinition.getTownship_code());
            jsonValues.put("township_name", eventDefinition.getTownship_name());
            jsonValues.put("company_id", eventDefinition.getCompany_id());
            jsonValues.put("company_code", eventDefinition.getCompany_code());
            jsonValues.put("company_name", eventDefinition.getCompany_name());

            if (isSessionRegistration) {
                jsonValues.put("event_session_id", sessionDefinition.getEvent_session_id());
                jsonValues.put("event_session_type", sessionDefinition.getEvent_session_type());
                jsonValues.put("event_session_name", sessionDefinition.getEvent_session_name());
                if (sessionDefinition.getEvent_session_begins_date_time() != null
                        && !TextUtils.isEmpty(sessionDefinition.getEvent_session_begins_date_time())) {
                    jsonValues.put("event_begins_date_time", DateTimeUtils.localToGMT(sessionDefinition.getEvent_session_begins_date_time()));
                }
                if (sessionDefinition.getEvent_session_ends_date_time() != null
                        && !TextUtils.isEmpty(sessionDefinition.getEvent_session_ends_date_time())) {
                    jsonValues.put("event_ends_date_time", DateTimeUtils.localToGMT(sessionDefinition.getEvent_session_ends_date_time()));
                }
                jsonValues.put("lat", itemLocation.getLat());
                jsonValues.put("lng", itemLocation.getLng());
                jsonValues.put("address1", itemLocation.getAddesss1());
            } else {
                jsonValues.put("event_id", eventDefinition.getEvent_id());
                jsonValues.put("event_type", eventDefinition.getEvent_type());
                jsonValues.put("event_name", eventDefinition.getEvent_name());
                if (eventDefinition.getEvent_begins_date_time() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_begins_date_time())) {
                    jsonValues.put("event_begins_date_time", DateTimeUtils.localToGMT(eventDefinition.getEvent_begins_date_time()));
                }
                if (eventDefinition.getEvent_ends_date_time() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_ends_date_time())) {
                    jsonValues.put("event_ends_date_time", eventDefinition.getEvent_ends_date_time());
                }
                jsonValues.put("lat", itemLocation.getLat());
                jsonValues.put("lng", itemLocation.getLng());
                jsonValues.put("address1", itemLocation.getAddesss1());
            }

            jsonValues.put("event_regn_status", "Active");
            jsonValues.put("user_id", logindeatl.getString("id", ""));
            jsonValues.put("user_phone_num", "");
            jsonValues.put("user_device_num", "");
            jsonValues.put("user_email", logindeatl.getString("email", ""));
            jsonValues.put("username", logindeatl.getString("name", ""));
            jsonValues.put("ip", getLocalIpAddress());
            jsonValues.put("regd_for_people_number", users.size());

            if (eventDefinition.isFree_event()) {
                jsonValues.put("payment_method", "");
                jsonValues.put("event_rate", eventDefinition.getEvent_full_regn_fee());
                jsonValues.put("regn_subtotal", "0");
                jsonValues.put("wallet_trx_id", "");
                jsonValues.put("event_price_total", "0");
            } else {
                jsonValues.put("payment_method", selectedPaymentMethod);
                jsonValues.put("event_rate", eventDefinition.getEvent_full_regn_fee());
                jsonValues.put("regn_subtotal", finalpayamount);
                jsonValues.put("wallet_trx_id", wallet_id);
                jsonValues.put("event_price_total", finalpayamount);
            }

            jsonValues.put("payment_choice", "");
            jsonValues.put("platform", "Android");


            if (selectedPaymentMethod.equals("Paypal")) {
                jsonValues.put("ipn_txn_id", transection_id);
                jsonValues.put("ipn_payment", "Paypal");
                jsonValues.put("ipn_status", "success");
                jsonValues.put("ipn_address", "");
            } else {
                jsonValues.put("ipn_txn_id", "");
                jsonValues.put("ipn_payment", "");
                jsonValues.put("ipn_status", "");
                jsonValues.put("ipn_address", "");
            }

            jsonValues.put("event_regn_fee_category_id", "1");
            jsonValues.put("event_regn_fee_category", "event_regn_fee_full");
            jsonValues.put("event_regn_approval_reqd", eventDefinition.isRegn_needed_approval());

            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + urlReg;
            HttpResponse response = null;
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            post.setEntity(entity);
            Log.e("#DEBUG", "  updateURL:  " + url);
            Log.e("#DEBUG", "   update Params: " + String.valueOf(json));
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   Add/Edit Event:  Response:  " + responseStr);
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.e("#DEBUG", "  update: ResponseID: " + re_id);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.hide();
            if (getActivity() != null && json1 != null) {
                if (re_id != null && !re_id.equals("null")) {
                    eventRegistrationId = re_id;
                    if (eventDefinition.isRegn_user_info_reqd() && users.size() != 0) {
                        new addRegisterUserDetails().execute();
                    } else {
                        Toast.makeText(getActivity(), "Registration Success!", Toast.LENGTH_SHORT).show();
                        getActivity().onBackPressed();
                    }
                }
                super.onPostExecute(s);
            }
        }
    }


    public class addRegisterUserDetails extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String urlReg = "_table/event_regn_user_profile";
        JSONObject json1;
        String re_id;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues = new HashMap<String, Object>();

            jsonValues.put("date_time", DateTimeUtils.getSqlFormatDate(Calendar.getInstance().getTime()));
            jsonValues.put("fname", users.get(0).getFirstName());
            jsonValues.put("lname", users.get(0).getLastName());
            jsonValues.put("full_name", users.get(0).getFirstName() + " " + users.get(0).getLastName());
            jsonValues.put("email", users.get(0).getEmail());
            jsonValues.put("mobile", users.get(0).getMobile());
            jsonValues.put("age", users.get(0).getAge());
            jsonValues.put("gender", users.get(0).getGender());
            jsonValues.put("event_fee_selected_type", users.get(0).getEvent_fee_selected_type());
            jsonValues.put("event_fee_selected", users.get(0).getFee());
            jsonValues.put("event_registration_id", eventRegistrationId);
            jsonValues.put("event_id", eventDefinition.getEvent_id());
            jsonValues.put("company_id", eventDefinition.getCompany_id());
            jsonValues.put("event_date", eventDefinition.getEvent_begins_date_time());
            jsonValues.put("ip", getLocalIpAddress());

            jsonValues.put("user_id", logindeatl.getString("id", ""));
            jsonValues.put("username", logindeatl.getString("name", ""));

            JSONObject json = new JSONObject(jsonValues);
            DefaultHttpClient client = new DefaultHttpClient();
            String url = getString(R.string.api) + getString(R.string.povlive) + urlReg;
            HttpResponse response = null;
            StringEntity entity = null;
            try {
                entity = new StringEntity(json.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpPost post = new HttpPost(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            post.setEntity(entity);
            Log.e("#DEBUG", "  addRegisterUserDetails:  " + url);
            Log.e("#DEBUG", "   addRegisterUserDetails Params: " + String.valueOf(json));
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   addRegisterUserDetails:  Response:  " + responseStr);
                    json1 = new JSONObject(responseStr);
                    JSONArray array = json1.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        re_id = c.getString("id");
                        Log.e("#DEBUG", "  addRegisterUserDetails: ResponseID: " + re_id);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.hide();
            if (getActivity() != null && json1 != null) {
                if (re_id != null && !re_id.equals("null")) {
                    if (users.size() != 0) {
                        users.remove(0);
                        if (users.size() == 0) {
                            Toast.makeText(getActivity(), "Registration Success!", Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();
                        } else {
                            new addRegisterUserDetails().execute();
                        }
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("ip", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("aax", ex.toString());
        }
        return null;
    }

    @Override
    protected void updateViews() {

        if (isSessionRegistration) {
            if (sessionDefinition != null) {
                if (!TextUtils.isEmpty(sessionDefinition.getEvent_session_regn_fee())) {
                    if (sessionDefinition.getEvent_session_regn_fee().equals("0")) {
                        binding.tvEventFee.setText("Free Event");
                    } else {
                        binding.tvEventFee.setText(String.format("Fee: $%s", sessionDefinition.getEvent_session_regn_fee()));
                    }
                }

                if (!TextUtils.isEmpty(sessionDefinition.getEvent_session_name())) {
                    binding.tvEventName.setText(sessionDefinition.getEvent_session_name());
                }

                if (!TextUtils.isEmpty(sessionDefinition.getEvent_session_type())) {
                    binding.tvEventType.setText(sessionDefinition.getEvent_session_type());
                }

                if (!TextUtils.isEmpty(sessionDefinition.getEvent_session_begins_date_time())) {
                    try {
                        binding.tvEventBeginsAt.setText(dateFormatOut.format(dateFormatMain.parse(sessionDefinition.getEvent_session_begins_date_time())));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                if (!TextUtils.isEmpty(sessionDefinition.getEvent_session_ends_date_time())) {
                    try {
                        binding.tvEventEndsAt.setText(dateFormatOut.format(dateFormatMain.parse(sessionDefinition.getEvent_session_ends_date_time())));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(sessionDefinition.getLocation_address());
                    for (int i = 0; i < jsonArray.length(); i++) {
//                        JSONArray jsonArray1 = jsonArray.getJSONArray(i);
//                        Log.e("#DEBUG", "    Address at POS: " + i + "  :" + jsonArray1.getString(0));
//                        binding.tvLocationAddress.setText(jsonArray1.getString(0));

                        JSONArray jsonArray11 = new JSONArray(sessionDefinition.getEvent_session_address());
                        JSONArray jsonArray22 = jsonArray11.getJSONArray(i);
                        binding.tvEventAddress.setText(jsonArray22.getString(0));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                feeCategories.clear();
                feeCategories.add(new FeeCategory("REGULAR", sessionDefinition.getEvent_session_regn_fee()));
                feeCategories.add(new FeeCategory("YOUTH", sessionDefinition.getEvent_session_youth_fee()));
                feeCategories.add(new FeeCategory("CHILD", sessionDefinition.getEvent_session_child_fee()));
                feeCategories.add(new FeeCategory("STUDENT", sessionDefinition.getEvent_session_student_fee()));
                feeCategories.add(new FeeCategory("STAFF", sessionDefinition.getEvent_session_staff_fee()));
                feeCategories.add(new FeeCategory("MINISTER", sessionDefinition.getEvent_session_minister_fee()));
                feeCategories.add(new FeeCategory("CLERGY", sessionDefinition.getEvent_session_clergy_fee()));
                feeCategories.add(new FeeCategory("PROMO", sessionDefinition.getEvent_session_promo_fee()));
                feeCategories.add(new FeeCategory("SENIOR", sessionDefinition.getEvent_session_senior_fee()));
            }
        } else {
            if (eventDefinition != null) {
                if (!TextUtils.isEmpty(eventDefinition.getEvent_full_regn_fee())) {
                    if (eventDefinition.getEvent_full_regn_fee().equals("0")) {
                        binding.tvEventFee.setText("Free Event");
                    } else {
                        binding.tvEventFee.setText(String.format("Fee: $%s", eventDefinition.getEvent_full_regn_fee()));
                    }
                }

                if (!TextUtils.isEmpty(eventDefinition.getEvent_name())) {
                    binding.tvEventName.setText(eventDefinition.getEvent_name());
                }

                if (!TextUtils.isEmpty(eventDefinition.getEvent_type())) {
                    binding.tvEventType.setText(eventDefinition.getEvent_type());
                }

                if (!TextUtils.isEmpty(eventDefinition.getEvent_begins_date_time())) {
                    try {
                        binding.tvEventBeginsAt.setText(dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_begins_date_time())));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                if (!TextUtils.isEmpty(eventDefinition.getEvent_ends_date_time())) {
                    try {
                        binding.tvEventEndsAt.setText(dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_ends_date_time())));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                if (eventDefinition.getLocation_lat_lng() != null
                        && !TextUtils.isEmpty(eventDefinition.getLocation_lat_lng())) {
                    try {
                        JSONArray jsonArrayLatLng = new JSONArray(eventDefinition.getLocation_lat_lng());
                        for (int i = 0; i < jsonArrayLatLng.length(); i++) {
                            JSONArray jsonArray2 = jsonArrayLatLng.getJSONArray(i);
                            for (int j = 0; j < jsonArray2.length(); j++) {
                                String[] latLng = jsonArray2.getString(j).split(",");
                                if (latLng.length == 2) {
                                    String lat = latLng[0];
                                    String lng = latLng[1];
                                    if (lat.equals(itemLocation.getLat()) && lng.equals(itemLocation.getLng())) {
                                        Log.e("#DEBUG", "    POS:  " + i);
                                        JSONArray jsonArray = new JSONArray(eventDefinition.getLocation_address());
                                        JSONArray jsonArray1 = jsonArray.getJSONArray(i);

                                        JSONArray jsonArray11 = new JSONArray(eventDefinition.getEvent_address());
                                        JSONArray jsonArray22 = jsonArray11.getJSONArray(i);
                                        binding.tvEventAddress.setText(jsonArray22.getString(0));
                                        itemLocation.setAddesss1(jsonArray22.getString(0));

                                    }
                                }
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                feeCategories.clear();
                feeCategories.add(new FeeCategory("REGULAR", eventDefinition.getEvent_full_regn_fee()));
                feeCategories.add(new FeeCategory("YOUTH", eventDefinition.getEvent_full_youth_fee()));
                feeCategories.add(new FeeCategory("CHILD", eventDefinition.getEvent_full_child_fee()));
                feeCategories.add(new FeeCategory("STUDENT", eventDefinition.getEvent_full_student_fee()));
                feeCategories.add(new FeeCategory("STAFF", eventDefinition.getEvent_full_staff_fee()));
                feeCategories.add(new FeeCategory("MINISTER", eventDefinition.getEvent_full_minister_fee()));
                feeCategories.add(new FeeCategory("CLERGY", eventDefinition.getEvent_full_clergy_fee()));
                feeCategories.add(new FeeCategory("PROMO", eventDefinition.getEvent_full_promo_fee()));
                feeCategories.add(new FeeCategory("SENIOR", eventDefinition.getEvent_full_senior_fee()));
            }
        }

    }

    @Override
    protected void setListeners() {

        binding.cbSelfReg.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                SharedPreferences loginPref = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
                User user = new User();
                user.setFirstName(loginPref.getString("first_name", ""));
                user.setLastName(loginPref.getString("last_name", ""));
                user.setEmail(loginPref.getString("email", ""));
                user.setMobile("9999999999");
                user.setAge("30");
                user.setGender("Male");
                user.setEvent_fee_selected_type(feeCategories.get(0).getName());
                if (feeCategories.get(0).getFee() == null) {
                    user.setFee("0");
                } else {
                    user.setFee(feeCategories.get(0).getFee());
                }
                users.add(user);
                userAdapter.notifyDataSetChanged();

                double amount = 0.0;
                for (int i = 0; i < users.size(); i++) {
                    if (users.get(i).getFee() != null && !TextUtils.isEmpty(users.get(i).getFee())) {
                        amount = amount + Double.parseDouble(users.get(i).getFee());
                    }
                }
                binding.llPaymentDetails.setVisibility(View.VISIBLE);
                binding.tvAmountPayable.setText(String.format("$%s", amount));
            } else {
                if (users.size() > 0) {
                    users.remove(0);
                    userAdapter.notifyDataSetChanged();
                }
                if (users.size() == 0) {
                    binding.llPaymentDetails.setVisibility(View.GONE);
                } else {
                    double amount = 0.0;
                    for (int i = 0; i < users.size(); i++) {
                        if (users.get(i).getFee() != null && !TextUtils.isEmpty(users.get(i).getFee())) {
                            amount = amount + Double.parseDouble(users.get(i).getFee());
                        }
                    }
                    binding.llPaymentDetails.setVisibility(View.VISIBLE);
                    binding.tvAmountPayable.setText(String.format("$%s", amount));
                }
            }
        });

        binding.btnAddMore.setOnClickListener(v -> {
            showDialogAddEditUser(false, -1);
        });

        binding.tvBtnReserveNow.setOnClickListener(v -> {

            if (!isSessionRegistration) {
                //Event Registration
                if (!eventDefinition.isRegn_user_info_reqd()) {
                    PaymentOptionDialog dialog = PaymentOptionDialog.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("mIsPayLater", false);
                    dialog.setArguments(bundle);
                    dialog.show(getChildFragmentManager(), "payment");
                } else if (users.size() == 0) {
                    Toast.makeText(getActivity(), "Please add user details!", Toast.LENGTH_SHORT).show();
                } else {
                    if (eventDefinition.isFree_event()) {
                        //Free Event
                        new AlertDialog.Builder(getActivity())
                                .setMessage("Are you sure, you want to register for this Event?")
                                .setPositiveButton("YES", (dialog, which) -> {
                                    dialog.dismiss();
                                    new registerForEvent().execute();
                                })
                                .setNegativeButton("NO", (dialog, which) -> dialog.dismiss())
                                .create().show();
                    } else {
                        PaymentOptionDialog dialog = PaymentOptionDialog.newInstance();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("mIsPayLater", false);
                        dialog.setArguments(bundle);
                        dialog.show(getChildFragmentManager(), "payment");
                    }
                }
            } else {
                //Session Registration
                if (!eventDefinition.isRegn_user_info_reqd()) {
                    PaymentOptionDialog dialog = PaymentOptionDialog.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("mIsPayLater", false);
                    dialog.setArguments(bundle);
                    dialog.show(getChildFragmentManager(), "payment");
                } else if (users.size() == 0) {
                    Toast.makeText(getActivity(), "Please add user details!", Toast.LENGTH_SHORT).show();
                } else {
                    PaymentOptionDialog dialog = PaymentOptionDialog.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("mIsPayLater", false);
                    dialog.setArguments(bundle);
                    dialog.show(getChildFragmentManager(), "payment");
                }
            }

        });

    }

    private void showDialogAddEditUser(boolean isEdit, int pos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_user_add_edit, null);
        builder.setView(dialogView);
        AppCompatSpinner spinnerCategory = dialogView.findViewById(R.id.spinnerCategory);
        ImageView ivBtnClose = dialogView.findViewById(R.id.ivBtnClose);
        TextView etFirstName = dialogView.findViewById(R.id.etFirstName);
        TextView etLastName = dialogView.findViewById(R.id.etLastName);
        TextView etEmailAddress = dialogView.findViewById(R.id.etEmailAddress);
        TextView etPhoneNumber = dialogView.findViewById(R.id.etPhoneNumber);
        TextView etAge = dialogView.findViewById(R.id.etAge);
        RadioGroup rgGender = dialogView.findViewById(R.id.rgGender);
        Button btnAdd = dialogView.findViewById(R.id.btnAdd);

        ArrayAdapter<FeeCategory> categoryAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, feeCategories);
        spinnerCategory.setAdapter(categoryAdapter);

        if (isEdit) {
            btnAdd.setText("Update");
            User user = users.get(pos);
            etFirstName.setText(user.getFirstName());
            etLastName.setText(user.getLastName());
            etEmailAddress.setText(user.getEmail());
            etPhoneNumber.setText(user.getMobile());
            etAge.setText(user.getAge());
            if (user.getGender().equals("Male")) rgGender.check(R.id.rbMale);
            else rgGender.check(R.id.rbFemale);
        }

        AlertDialog alertDialog = builder.create();

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                feeCategory = feeCategories.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ivBtnClose.setOnClickListener(v -> {
            alertDialog.dismiss();
        });

        btnAdd.setOnClickListener(v -> {

            if (TextUtils.isEmpty(etFirstName.getText().toString())) {
                etFirstName.setError("Required!");
                etFirstName.requestFocus();
            } else if (TextUtils.isEmpty(etLastName.getText().toString())) {
                etLastName.setError("Required!");
                etLastName.requestFocus();
            } else if (TextUtils.isEmpty(etEmailAddress.getText().toString())) {
                etEmailAddress.setError("Required!");
                etEmailAddress.requestFocus();
            } else if (TextUtils.isEmpty(etPhoneNumber.getText().toString())) {
                etPhoneNumber.setError("Required!");
                etPhoneNumber.requestFocus();
            } else if (TextUtils.isEmpty(etAge.getText().toString())) {
                etAge.setError("Required!");
                etAge.requestFocus();
            } else {
                alertDialog.dismiss();

                if (isEdit) {
                    users.get(pos).setFirstName(etFirstName.getText().toString());
                    users.get(pos).setLastName(etLastName.getText().toString());
                    users.get(pos).setEmail(etEmailAddress.getText().toString());
                    users.get(pos).setMobile(etPhoneNumber.getText().toString());
                    users.get(pos).setAge(etAge.getText().toString());
                    users.get(pos).setGender(rgGender.getCheckedRadioButtonId() == R.id.rbMale ? "Male" : "Female");
                    users.get(pos).setEvent_fee_selected_type(feeCategory.getName());
                    users.get(pos).setFee(feeCategory.getFee());
                    userAdapter.notifyItemChanged(pos);
                } else {
                    User user = new User();
                    user.setFirstName(etFirstName.getText().toString());
                    user.setLastName(etLastName.getText().toString());
                    user.setEmail(etEmailAddress.getText().toString());
                    user.setMobile(etPhoneNumber.getText().toString());
                    user.setAge(etAge.getText().toString());
                    user.setGender(rgGender.getCheckedRadioButtonId() == R.id.rbMale ? "Male" : "Female");
                    user.setEvent_fee_selected_type(feeCategory.getName());
                    user.setFee(feeCategory.getFee());
                    users.add(user);
                    userAdapter.notifyDataSetChanged();
                }

                double amount = 0.0;
                for (int i = 0; i < users.size(); i++) {
                    amount = amount + Double.parseDouble(users.get(i).getFee());
                }
                binding.llPaymentDetails.setVisibility(View.VISIBLE);
                binding.tvAmountPayable.setText(String.format("$%s", amount));

            }

        });


        alertDialog.show();
    }

    @Override
    protected void initViews(View v) {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Registering for event...");
        progressDialog.setCancelable(false);

        userAdapter = new UserAdapter();
        binding.rvUser.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvUser.setAdapter(userAdapter);

        if (isSessionRegistration) {
            binding.tvTitle.setText("Session Registration");
            binding.tvEventNameTitle.setText("Session");
            binding.tvEventTypeTitle.setText("Session Type");
            binding.tvEventBeginsAtTitle.setText("Session Begins At");
            binding.tvEventEndsAtTitle.setText("Session Ends At");
            binding.tvEventAddressTitle.setText("Session Address");
        }


    }


    private class UserAdapter extends RecyclerView.Adapter<UserAdapter.UsersHolder> {

        @NonNull
        @Override
        public UserAdapter.UsersHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new UsersHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_user, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull UserAdapter.UsersHolder holder, int i) {
            User user = users.get(i);
            if (user != null) {
                holder.tvName.setText(String.format("%s %s", user.getFirstName(), user.getLastName()));
                holder.tvGenderAge.setText(String.format("%s(%s)", user.getGender().substring(0, 1), user.getAge()));
                holder.tvFees.setText(String.format("$%s", user.getFee()));
                holder.tvCategory.setText(user.getEvent_fee_selected_type());
            }

        }

        @Override
        public int getItemCount() {
            return users.size();
        }

        class UsersHolder extends RecyclerView.ViewHolder {
            TextView tvName, tvGenderAge, tvCategory, tvFees;
            ImageView ivBtnEdit;

            UsersHolder(@NonNull View itemView) {
                super(itemView);
                tvName = itemView.findViewById(R.id.tvName);
                tvGenderAge = itemView.findViewById(R.id.tvGenderAge);
                tvCategory = itemView.findViewById(R.id.tvCategory);
                tvFees = itemView.findViewById(R.id.tvFees);
                ivBtnEdit = itemView.findViewById(R.id.ivBtnEdit);

                ivBtnEdit.setOnClickListener(v -> {
                    final int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        showDialogAddEditUser(true, position);
                    }
                });
            }
        }
    }
}
