package com.softmeasures.eventezlyadminplus.frament.locations;

import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.vchome;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.FragLocationsByTypeBinding;
import com.softmeasures.eventezlyadminplus.databinding.ItemNearByBinding;
import com.softmeasures.eventezlyadminplus.frament.Other_City;
import com.softmeasures.eventezlyadminplus.models.ParkingPartner;
import com.softmeasures.eventezlyadminplus.services.GPSTracker;

import java.util.ArrayList;
import java.util.Locale;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;

public class LocationsByTypeFragment extends BaseFragment implements Other_City.Other_city_listenere {

    public FragLocationsByTypeBinding binding;
    private String type = "";

    private ArrayList<ParkingPartner> partnerLocations = new ArrayList<>();
    private double latitude = 0.0, longitude = 0.0;
    private double centerlat = 0.0, centerlang = 0.0;
    private GoogleMap googleMap;
    private SupportMapFragment mapFragment;
    private boolean mapisloadcallback = false;
    private boolean searchotherlocation = false;
    private ArrayList<ParkingPartner> nearbylist = new ArrayList<>();
    private boolean isNearBySearch = false;

    private NearByAdapter nearByAdapter;
    private boolean managedpinisclick = false;
    private boolean managedmapisloaded = false;

    public static boolean isUpdated = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            type = getArguments().getString("type");
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_locations_by_type, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getCurrentLocation();
        initViews(view);
        updateViews();
        setListeners();
        fetchParkingPartners();
        loadMap();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isUpdated) {
            fetchParkingPartners();
            isUpdated = false;
        }
    }

    private void loadMap() {
        if (getActivity() != null) {
            try {
                mapFragment = (SupportMapFragment) getChildFragmentManager()
                        .findFragmentById(R.id.fragmentMap);
                if (mapFragment != null) {
                    mapFragment.getMapAsync(map -> {
                        googleMap = map;
                        showParkingSpotsOnMap();
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showParkingSpotsOnMap() {
        if (googleMap != null) {
            Log.e("#DEBUG", "  showParkingSpotsOnMap");
            googleMap.clear();
            MarkerOptions markerOptions;
            for (int i = 0; i < partnerLocations.size(); i++) {
                markerOptions = new MarkerOptions();
                LatLng position = new LatLng(partnerLocations.get(i).getLat(),
                        partnerLocations.get(i).getLng());
                markerOptions.position(position);
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmap(i)));
                markerOptions.title(type);
                markerOptions.snippet(partnerLocations.get(i).getLat() + "-" + partnerLocations.get(i).getLng());
                googleMap.addMarker(markerOptions);

            }

            LatLng po1 = new LatLng(latitude, longitude);
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    po1, 12);
            if (centerlang == 0.0 && centerlat == 0.0) {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(po1, 12));
                googleMap.animateCamera(location);
            }

            googleMap.setOnMarkerClickListener(marker -> {
                managedpinisclick = true;
                managedmapisloaded = false;
                openDetailView(marker);
                return true;
            });

            googleMap.setOnMapLoadedCallback(() -> {
                managedmapisloaded = true;
                managedpinisclick = false;
            });

            googleMap.setOnMapClickListener(latLng -> mapisloadcallback = true);

            googleMap.setOnCameraChangeListener(cameraPosition -> {
                latitude = cameraPosition.target.latitude;
                longitude = cameraPosition.target.longitude;
                LatLng position = googleMap.getCameraPosition().target;
                centerlat = position.latitude;
                centerlang = position.longitude;
                if (managedmapisloaded) {
                    if (!managedpinisclick) {
                        latitude = cameraPosition.target.latitude;
                        longitude = cameraPosition.target.longitude;
                        managedpinisclick = true;
                        Log.e("#DEBUG", "   api_is_start   yes");
                        fetchParkingPartners();
//                            new getparkingpin(lat, lang).execute();
                    }
                }
            });


        } else {
            Log.e("#DEBUG", "  showParkingSpotsOnMap:  googleMap null");
        }
    }

    private void openDetailView(Marker marker) {
        ParkingPartner parkingPartner = new ParkingPartner();
        parkingPartner.setLat(marker.getPosition().latitude);
        parkingPartner.setLng(marker.getPosition().longitude);
        ((vchome) getActivity()).show_back_button();
        Log.e("#DEBUG", "   openDetailView:  " + partnerLocations.get(partnerLocations.indexOf(parkingPartner)).getLocation_name());
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        EditPartnerFragment pay = new EditPartnerFragment();
        Bundle bundle = new Bundle();
        bundle.putString("selectedSpot", new Gson().toJson(partnerLocations.get(partnerLocations.indexOf(parkingPartner))));
        bundle.putString("parkingType", type);
        pay.setArguments(bundle);
        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
        ft.add(R.id.My_Container_1_ID, pay);
        fragmentStack.lastElement().onPause();
        ft.hide(fragmentStack.lastElement());
        fragmentStack.push(pay);
        ft.commitAllowingStateLoss();
    }

    private Bitmap getMarkerBitmap(int pos) {

        LinearLayout distanceMarkerLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.marker_with_price, null);
        TextView positionDistance = distanceMarkerLayout.findViewById(R.id.tvPrice);
        ImageView ivMarker = distanceMarkerLayout.findViewById(R.id.ivMarker);
        if (type.equalsIgnoreCase("Township")) {
            ivMarker.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.mipmap.managed));
        } else if (type.equalsIgnoreCase("Commercial")) {
            ivMarker.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.mipmap.commercial_icon));
        } else if (type.equalsIgnoreCase("Other")) {
            ivMarker.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.mipmap.parkwhiz_pin_icon));
        }
        positionDistance.setVisibility(View.GONE);
        TextView tvLocationName = distanceMarkerLayout.findViewById(R.id.tvLocationName);
        if (!TextUtils.isEmpty(partnerLocations.get(pos).getLocation_name())) {
            tvLocationName.setVisibility(View.VISIBLE);
            tvLocationName.setText(partnerLocations.get(pos).getLocation_name());
        } else {
            tvLocationName.setVisibility(View.GONE);
        }
        distanceMarkerLayout.setDrawingCacheEnabled(true);
        distanceMarkerLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        distanceMarkerLayout.layout(0, 0, distanceMarkerLayout.getMeasuredWidth(), distanceMarkerLayout.getMeasuredHeight());
        distanceMarkerLayout.buildDrawingCache(true);


        Bitmap bitmap = Bitmap.createBitmap(distanceMarkerLayout.getDrawingCache());
        distanceMarkerLayout.setDrawingCacheEnabled(false);
        BitmapDescriptor flagBitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
        return bitmap;
    }

    public void getCurrentLocation() {
        GPSTracker tracker = new GPSTracker(getActivity());
        if (tracker.canGetLocation()) {

            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();

        }
    }

    private void fetchParkingPartners() {
        String partnersUrl = "_table/township_parking_partners";
        if (type.equalsIgnoreCase("Township")) {
            partnersUrl = "_proc/edit_township_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";
        } else if (type.equalsIgnoreCase("Commercial")) {
            partnersUrl = "_proc/find_other_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";
        } else if (type.equalsIgnoreCase("Other")) {
            partnersUrl = "_proc/find_other_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";
        }
        Log.e("#DEBUG", "   fetchParkingPartners:  URL:  " + getString(R.string.api) + getString(R.string.povlive) + partnersUrl);
        binding.rlProgressbar.setVisibility(View.VISIBLE);
        AndroidNetworking.get(getString(R.string.api) + getString(R.string.povlive) + partnersUrl)
                .addHeaders(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1))
                .addHeaders(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2))
                .build()
                .getAsObjectList(ParkingPartner.class, new ParsedRequestListener<ArrayList<ParkingPartner>>() {
                    @Override
                    public void onResponse(ArrayList<ParkingPartner> response) {
                        binding.rlProgressbar.setVisibility(View.GONE);
                        if (isNearBySearch) {
                            isNearBySearch = false;
                            nearbylist.clear();
                            nearbylist.addAll(response);
                            Log.e("#DEBUG", "    fetchParkingPartners:  Size:  " + nearbylist.size());
                            showNearByPartnerList();
                        } else {
                            partnerLocations.clear();
                            partnerLocations.addAll(response);
                            showParkingSpotsOnMap();
                            Log.e("#DEBUG", "    fetchParkingPartners:  Size:  " + partnerLocations.size());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        binding.rlProgressbar.setVisibility(View.GONE);
                        Log.e("#DEBUG", "   fetchParkingPartners:  Error:  " + anError.getMessage());
                    }
                });
    }

    private void fetchNearByParkingPartners() {
        String partnersUrl = "_table/township_parking_partners";
        if (type.equalsIgnoreCase("Township")) {
            partnersUrl = "_proc/edit_township_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";
        } else if (type.equalsIgnoreCase("Commercial")) {
            partnersUrl = "_proc/find_other_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";
        } else if (type.equalsIgnoreCase("Other")) {
            partnersUrl = "_proc/find_other_parking_partners_nearby?in_lat=" + latitude + "&in_lng=" + longitude + "";
        }
        Log.e("#DEBUG", "   fetchNearByParkingPartners:  URL:  " + getString(R.string.api) + getString(R.string.povlive) + partnersUrl);
        binding.rlProgressbar.setVisibility(View.VISIBLE);
        AndroidNetworking.get(getString(R.string.api) + getString(R.string.povlive) + partnersUrl)
                .addHeaders(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1))
                .addHeaders(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2))
                .build()
                .getAsObjectList(ParkingPartner.class, new ParsedRequestListener<ArrayList<ParkingPartner>>() {
                    @Override
                    public void onResponse(ArrayList<ParkingPartner> response) {
                        binding.rlProgressbar.setVisibility(View.GONE);
                        isNearBySearch = false;
                        nearbylist.clear();
                        nearbylist.addAll(response);
                        Log.e("#DEBUG", "    fetchParkingPartners:  Size:  " + nearbylist.size());
                        showNearByPartnerList();
                    }

                    @Override
                    public void onError(ANError anError) {
                        binding.rlProgressbar.setVisibility(View.GONE);
                        Log.e("#DEBUG", "   fetchParkingPartners:  Error:  " + anError.getMessage());
                    }
                });
    }

    private void showNearByPartnerList() {
        Log.e("#DEBUG", "   showNearByPartnerList: ");

        binding.rlNearBy.setVisibility(View.VISIBLE);
        nearByAdapter.notifyDataSetChanged();
    }

    @Override
    protected void updateViews() {
        if (!TextUtils.isEmpty(type)) {
            binding.tvTitle.setText(type + " Locations");
        }
    }

    @Override
    protected void setListeners() {

        binding.ivBtnFindNearBy.setOnClickListener(v -> {
            mapisloadcallback = false;
//            getCurrentLocation();
            searchotherlocation = false;
            nearbylist.clear();
            isNearBySearch = true;
            fetchNearByParkingPartners();
        });

        binding.ivNearByClose.setOnClickListener(v -> binding.rlNearBy.setVisibility(View.GONE));

        binding.ivBtnOtherCity.setOnClickListener(v -> {
            Bundle b = new Bundle();
            b.getString("click", "commercia");
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            Other_City pay = new Other_City();
            pay.setArguments(b);
            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
            pay.registerForListener(LocationsByTypeFragment.this);
            ft.add(R.id.My_Container_1_ID, pay);
            fragmentStack.lastElement().onPause();
            ft.hide(fragmentStack.lastElement());
            fragmentStack.push(pay);
            ft.commitAllowingStateLoss();
        });
    }

    @Override
    protected void initViews(View v) {

        nearByAdapter = new NearByAdapter();
        binding.rvNearBy.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvNearBy.setAdapter(nearByAdapter);

    }


    @Override
    public void OnItemFindClick(String lat, String lang) {
        latitude = Double.parseDouble(lat);
        longitude = Double.parseDouble(lang);
        centerlat = 0.0;
        centerlang = 0.0;
        fetchParkingPartners();
    }

    private class NearByAdapter extends RecyclerView.Adapter<NearByAdapter.NearByHolder> {

        private ItemNearByBinding nearByBinding;

        @NonNull
        @Override
        public NearByHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            nearByBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                    R.layout.item_near_by, viewGroup, false);
            return new NearByHolder(nearByBinding);
        }

        @Override
        public void onBindViewHolder(@NonNull NearByHolder nearByHolder, int i) {
            ParkingPartner parkingPartner = nearbylist.get(i);
            nearByHolder.bind(parkingPartner);
        }

        @Override
        public int getItemCount() {
            return nearbylist.size();
        }

        class NearByHolder extends RecyclerView.ViewHolder {
            ItemNearByBinding nearByBinding;

            NearByHolder(@NonNull ItemNearByBinding itemView) {
                super(itemView.getRoot());
                this.nearByBinding = itemView;

                nearByBinding.getRoot().setOnClickListener(v -> {
                    ((vchome) getActivity()).show_back_button();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    EditPartnerFragment locationsByType = new EditPartnerFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("parkingType", type);
                    bundle.putString("selectedSpot", new Gson().toJson(nearbylist.get(getAdapterPosition())));
                    locationsByType.setArguments(bundle);
                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                    ft.add(R.id.My_Container_1_ID, locationsByType);
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    fragmentStack.push(locationsByType);
                    ft.commitAllowingStateLoss();
                });
            }

            void bind(ParkingPartner parkingPartner) {
                nearByBinding.tvLocationName.setText(String.format("%s - %s",
                        parkingPartner.getTitle(), parkingPartner.getLocation_name()));
                nearByBinding.tvAddress.setText(parkingPartner.getAddress());
                nearByBinding.tvTimingRules.setText(parkingPartner.getParking_times());

                String finalans = "";
                LatLng latLng = new LatLng(Double.parseDouble(String.valueOf(
                        parkingPartner.getLat())), Double.parseDouble(String.valueOf(parkingPartner.getLng())));
                double dis = getDistance(latLng);
                double ff = dis * 0.000621371192;
                finalans = String.format(Locale.getDefault(), "%.2f", ff);
                if (finalans.equals("1.0")) {
                    nearByBinding.tvDistance.setText(String.format("%s Mile", finalans));
                } else {
                    nearByBinding.tvDistance.setText(String.format("%s Miles", finalans));
                }

                if (parkingPartner.getMarker_type().equalsIgnoreCase("Free")) {
                    nearByBinding.ivIcon.setBackgroundResource(R.mipmap.park_free_circle_pin);
                } else if (parkingPartner.getMarker_type().equalsIgnoreCase("Managed Paid")
                        || parkingPartner.getMarker_type().equalsIgnoreCase("Managed Free")) {
                    nearByBinding.ivIcon.setBackgroundResource(R.mipmap.park_managed_circle);
                    ;
                } else if (parkingPartner.getMarker_type().equalsIgnoreCase("Paid")) {
                    nearByBinding.ivIcon.setBackgroundResource(R.mipmap.park_paid_circle_pin);
                }
            }
        }
    }

    public double getDistance(LatLng LatLng1) {
        double distance = 0;
        Location locationA = new Location("A");
        locationA.setLatitude(LatLng1.latitude);
        locationA.setLongitude(LatLng1.longitude);
        Location locationB = new Location("B");
        locationB.setLatitude(latitude);
        locationB.setLongitude(longitude);
        distance = locationA.distanceTo(locationB);
        return distance;
    }
}
