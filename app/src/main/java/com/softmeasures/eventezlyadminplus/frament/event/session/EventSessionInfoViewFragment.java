package com.softmeasures.eventezlyadminplus.frament.event.session;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubeIntents;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoFacebookBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoGoogleClassroomBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoGoogleMeetBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoSkypeBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoTeleConferenceBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoYoutubeBinding;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventMediaInfoZoomBinding;
import com.softmeasures.eventezlyadminplus.databinding.FragEventSessionInfoViewBinding;
import com.softmeasures.eventezlyadminplus.frament.event.PDFViewFragment;
import com.softmeasures.eventezlyadminplus.frament.g_classroom.GClassroomActivity;
import com.softmeasures.eventezlyadminplus.models.EventSession;
import com.softmeasures.eventezlyadminplus.models.MediaDefinition;
import com.softmeasures.eventezlyadminplus.utils.AESEncyption;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_VIRTUALLY;

public class EventSessionInfoViewFragment extends BaseFragment {

//    private FragEventSessionInfoViewBinding binding;
//    private int eventSessionId;
//    private ProgressDialog progressDialog;
//    private EventSession eventDefinition;
//
//    private ArrayList<String> eventImages = new ArrayList<>();
//    private EventImageAdapter eventImageAdapter;
//    SimpleDateFormat dateFormatMain = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//    SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd MMM, hh:mm a", Locale.getDefault());
//
//    private ArrayList<MediaDefinition> mediaDefinitions = new ArrayList<>();
//    private MediaAdapter mediaAdapter;
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        if (getArguments() != null) {
//            eventSessionId = getArguments().getInt("eventSessionId");
//        }
//        binding = DataBindingUtil.inflate(inflater, R.layout.frag_event_session_info_view, container, false);
//        return binding.getRoot();
//    }
//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        initViews(view);
//        updateViews();
//        setListeners();
//
//        new fetchSessionEventDetails().execute();
//    }
//
//    private class fetchSessionEventDetails extends AsyncTask<String, String, String> {
//        JSONObject json;
//        JSONArray json1;
//        String eventDefUrl = "_table/event_session_definitions";
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            eventDefUrl = "_table/event_session_definitions?filter=id=" + eventSessionId;
//            progressDialog.show();
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            String url = getString(R.string.api) + getString(R.string.povlive) + eventDefUrl;
//            Log.e("#DEBUG", "      fetchSessionEventDetails:  " + url);
//            DefaultHttpClient client = new DefaultHttpClient();
//            HttpGet post = new HttpGet(url);
//            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
//            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
//            HttpResponse response = null;
//            try {
//                response = client.execute(post);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            if (response != null) {
//                HttpEntity resEntity = response.getEntity();
//                String responseStr = null;
//                try {
//                    responseStr = EntityUtils.toString(resEntity).trim();
//                    json = new JSONObject(responseStr);
//                    JSONArray jsonArray = json.getJSONArray("resource");
//
//                    for (int j = 0; j < jsonArray.length(); j++) {
//                        eventDefinition = new EventSession();
//                        JSONObject object = jsonArray.getJSONObject(j);
//                        if (object.has("id")
//                                && !TextUtils.isEmpty(object.getString("id"))
//                                && !object.getString("id").equals("null")) {
//                            eventDefinition.setId(object.getInt("id"));
//                        }
//                        if (object.has("date_time")
//                                && !TextUtils.isEmpty(object.getString("date_time"))
//                                && !object.getString("date_time").equals("null")) {
//                            eventDefinition.setDate_time(object.getString("date_time"));
//                        }
//                        if (object.has("manager_type")
//                                && !TextUtils.isEmpty(object.getString("manager_type"))
//                                && !object.getString("manager_type").equals("null")) {
//                            eventDefinition.setManager_type(object.getString("manager_type"));
//                        }
//                        if (object.has("manager_type_id")
//                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
//                                && !object.getString("manager_type_id").equals("null")) {
//                            eventDefinition.setManager_type_id(object.getInt("manager_type_id"));
//                        }
//                        if (object.has("twp_id")
//                                && !TextUtils.isEmpty(object.getString("twp_id"))
//                                && !object.getString("twp_id").equals("null")) {
//                            eventDefinition.setTwp_id(object.getInt("twp_id"));
//                        }
//                        if (object.has("township_code")
//                                && !TextUtils.isEmpty(object.getString("township_code"))
//                                && !object.getString("township_code").equals("null")) {
//                            eventDefinition.setTownship_code(object.getString("township_code"));
//                        }
//                        if (object.has("township_name")
//                                && !TextUtils.isEmpty(object.getString("township_name"))
//                                && !object.getString("township_name").equals("null")) {
//                            eventDefinition.setTownship_name(object.getString("township_name"));
//                        }
//                        if (object.has("company_id")
//                                && !TextUtils.isEmpty(object.getString("company_id"))
//                                && !object.getString("company_id").equals("null")) {
//                            eventDefinition.setCompany_id(object.getInt("company_id"));
//                        }
//                        if (object.has("company_code")
//                                && !TextUtils.isEmpty(object.getString("company_code"))
//                                && !object.getString("company_code").equals("null")) {
//                            eventDefinition.setCompany_code(object.getString("company_code"));
//                        }
//                        if (object.has("company_name")
//                                && !TextUtils.isEmpty(object.getString("company_name"))
//                                && !object.getString("company_name").equals("null")) {
//                            eventDefinition.setCompany_name(object.getString("company_name"));
//                        }
//                        if (object.has("event_id")
//                                && !TextUtils.isEmpty(object.getString("event_id"))
//                                && !object.getString("event_id").equals("null")) {
//                            eventDefinition.setEvent_id(object.getInt("event_id"));
//                        }
//                        if (object.has("event_name")
//                                && !TextUtils.isEmpty(object.getString("event_name"))
//                                && !object.getString("event_name").equals("null")) {
//                            eventDefinition.setEvent_name(object.getString("event_name"));
//                        }
//                        if (object.has("event_type")
//                                && !TextUtils.isEmpty(object.getString("event_type"))
//                                && !object.getString("event_type").equals("null")) {
//                            eventDefinition.setEvent_type(object.getString("event_type"));
//                        }
//                        if (object.has("event_code")
//                                && !TextUtils.isEmpty(object.getString("event_code"))
//                                && !object.getString("event_code").equals("null")) {
//                            eventDefinition.setEvent_code(object.getString("event_code"));
//                        }
//                        if (object.has("event_session_id")
//                                && !TextUtils.isEmpty(object.getString("event_session_id"))
//                                && !object.getString("event_session_id").equals("null")) {
//                            eventDefinition.setEvent_session_id(object.getInt("event_session_id"));
//                        }
//                        if (object.has("event_session_type")
//                                && !TextUtils.isEmpty(object.getString("event_session_type"))
//                                && !object.getString("event_session_type").equals("null")) {
//                            eventDefinition.setEvent_session_type(object.getString("event_session_type"));
//                        }
//                        if (object.has("event_session_code")
//                                && !TextUtils.isEmpty(object.getString("event_session_code"))
//                                && !object.getString("event_session_code").equals("null")) {
//                            eventDefinition.setEvent_session_code(object.getString("event_session_code"));
//                        }
//                        if (object.has("event_session_name")
//                                && !TextUtils.isEmpty(object.getString("event_session_name"))
//                                && !object.getString("event_session_name").equals("null")) {
//                            eventDefinition.setEvent_session_name(object.getString("event_session_name"));
//                        }
//                        if (object.has("event_session_short_description")
//                                && !TextUtils.isEmpty(object.getString("event_session_short_description"))
//                                && !object.getString("event_session_short_description").equals("null")) {
//                            eventDefinition.setEvent_session_short_description(object.getString("event_session_short_description"));
//                        }
//                        if (object.has("event_session_long_description")
//                                && !TextUtils.isEmpty(object.getString("event_session_long_description"))
//                                && !object.getString("event_session_long_description").equals("null")) {
//                            eventDefinition.setEvent_session_long_description(object.getString("event_session_long_description"));
//                        }
//                        if (object.has("event_session_link_on_web")
//                                && !TextUtils.isEmpty(object.getString("event_session_link_on_web"))
//                                && !object.getString("event_session_link_on_web").equals("null")) {
//                            eventDefinition.setEvent_session_link_on_web(object.getString("event_session_link_on_web"));
//                        }
//                        if (object.has("event_session_link_twitter")
//                                && !TextUtils.isEmpty(object.getString("event_session_link_twitter"))
//                                && !object.getString("event_session_link_twitter").equals("null")) {
//                            eventDefinition.setEvent_session_link_twitter(object.getString("event_session_link_twitter"));
//                        }
//                        if (object.has("event_session_link_whatsapp")
//                                && !TextUtils.isEmpty(object.getString("event_session_link_whatsapp"))
//                                && !object.getString("event_session_link_whatsapp").equals("null")) {
//                            eventDefinition.setEvent_session_link_whatsapp(object.getString("event_session_link_whatsapp"));
//                        }
//                        if (object.has("event_session_link_other_media")
//                                && !TextUtils.isEmpty(object.getString("event_session_link_other_media"))
//                                && !object.getString("event_session_link_other_media").equals("null")) {
//                            eventDefinition.setEvent_session_link_other_media(object.getString("event_session_link_other_media"));
//                        }
//
//                        if (object.has("company_logo")
//                                && !TextUtils.isEmpty(object.getString("company_logo"))
//                                && !object.getString("company_logo").equals("null")) {
//                            eventDefinition.setCompany_logo(object.getString("company_logo"));
//                        }
//                        if (object.has("event_logo")
//                                && !TextUtils.isEmpty(object.getString("event_logo"))
//                                && !object.getString("event_logo").equals("null")) {
//                            eventDefinition.setEvent_logo(object.getString("event_logo"));
//                        }
//                        if (object.has("event_session_logo")
//                                && !TextUtils.isEmpty(object.getString("event_session_logo"))
//                                && !object.getString("event_session_logo").equals("null")) {
//                            eventDefinition.setEvent_session_logo(object.getString("event_session_logo"));
//                        }
//                        if (object.has("event_session_image1")
//                                && !TextUtils.isEmpty(object.getString("event_session_image1"))
//                                && !object.getString("event_session_image1").equals("null")) {
//                            eventDefinition.setEvent_session_image1(object.getString("event_session_image1"));
//                        }
//                        if (object.has("event_session_image2")
//                                && !TextUtils.isEmpty(object.getString("event_session_image2"))
//                                && !object.getString("event_session_image2").equals("null")) {
//                            eventDefinition.setEvent_session_image2(object.getString("event_session_image2"));
//                        }
//                        if (object.has("event_session_blob_image")
//                                && !TextUtils.isEmpty(object.getString("event_session_blob_image"))
//                                && !object.getString("event_session_blob_image").equals("null")) {
//                            eventDefinition.setEvent_session_blob_image(object.getString("event_session_blob_image"));
//                        }
//                        if (object.has("event_session_address")
//                                && !TextUtils.isEmpty(object.getString("event_session_address"))
//                                && !object.getString("event_session_address").equals("null")) {
//                            eventDefinition.setEvent_session_address(object.getString("event_session_address"));
//                        }
//                        if (object.has("covered_locations")
//                                && !TextUtils.isEmpty(object.getString("covered_locations"))
//                                && !object.getString("covered_locations").equals("null")) {
//                            eventDefinition.setCovered_locations(object.getString("covered_locations"));
//                        }
//                        if (object.has("session_regn_needed_approval")
//                                && !TextUtils.isEmpty(object.getString("session_regn_needed_approval"))
//                                && !object.getString("session_regn_needed_approval").equals("null")) {
//                            eventDefinition.setSession_regn_needed_approval(object.getBoolean("session_regn_needed_approval"));
//                        }
//                        if (object.has("requirements")
//                                && !TextUtils.isEmpty(object.getString("requirements"))
//                                && !object.getString("requirements").equals("null")) {
//                            eventDefinition.setRequirements(object.getString("requirements"));
//                        }
//                        if (object.has("appl_req_download")
//                                && !TextUtils.isEmpty(object.getString("appl_req_download"))
//                                && !object.getString("appl_req_download").equals("null")) {
//                            eventDefinition.setAppl_req_download(object.getString("appl_req_download"));
//                        }
//                        if (object.has("cost")
//                                && !TextUtils.isEmpty(object.getString("cost"))
//                                && !object.getString("cost").equals("null")) {
//                            eventDefinition.setCost(object.getString("cost"));
//                        }
//                        if (object.has("year")
//                                && !TextUtils.isEmpty(object.getString("year"))
//                                && !object.getString("year").equals("null")) {
//                            eventDefinition.setYear(object.getString("year"));
//                        }
//                        if (object.has("location_address")
//                                && !TextUtils.isEmpty(object.getString("location_address"))
//                                && !object.getString("location_address").equals("null")) {
//                            eventDefinition.setLocation_address(object.getString("location_address"));
//                        }
//                        if (object.has("scheme_type")
//                                && !TextUtils.isEmpty(object.getString("scheme_type"))
//                                && !object.getString("scheme_type").equals("null")) {
//                            eventDefinition.setScheme_type(object.getString("scheme_type"));
//                        }
//                        if (object.has("event_prefix")
//                                && !TextUtils.isEmpty(object.getString("event_prefix"))
//                                && !object.getString("event_prefix").equals("null")) {
//                            eventDefinition.setEvent_prefix(object.getString("event_prefix"));
//                        }
//
//                        if (object.has("event_session_prefix")
//                                && !TextUtils.isEmpty(object.getString("event_session_prefix"))
//                                && !object.getString("event_session_prefix").equals("null")) {
//                            eventDefinition.setEvent_session_prefix(object.getString("event_session_prefix"));
//                        }
//                        if (object.has("event_nextnum")
//                                && !TextUtils.isEmpty(object.getString("event_nextnum"))
//                                && !object.getString("event_nextnum").equals("null")) {
//                            eventDefinition.setEvent_nextnum(object.getInt("event_nextnum"));
//                        }
//                        if (object.has("event_session_nextnum")
//                                && !TextUtils.isEmpty(object.getString("event_session_nextnum"))
//                                && !object.getString("event_session_nextnum").equals("null")) {
//                            eventDefinition.setEvent_session_nextnum(object.getInt("event_session_nextnum"));
//                        }
//                        if (object.has("event_session_begins_date_time")
//                                && !TextUtils.isEmpty(object.getString("event_session_begins_date_time"))
//                                && !object.getString("event_session_begins_date_time").equals("null")) {
//                            eventDefinition.setEvent_session_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_begins_date_time")));
//                        }
//                        if (object.has("event_session_ends_date_time")
//                                && !TextUtils.isEmpty(object.getString("event_session_ends_date_time"))
//                                && !object.getString("event_session_ends_date_time").equals("null")) {
//                            eventDefinition.setEvent_session_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_ends_date_time")));
//                        }
//                        if (object.has("event_session_parking_begins_date_time")
//                                && !TextUtils.isEmpty(object.getString("event_session_parking_begins_date_time"))
//                                && !object.getString("event_session_parking_begins_date_time").equals("null")) {
//                            eventDefinition.setEvent_session_parking_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_parking_begins_date_time")));
//                        }
//                        if (object.has("event_session_parking_ends_date_time")
//                                && !TextUtils.isEmpty(object.getString("event_session_parking_ends_date_time"))
//                                && !object.getString("event_session_parking_ends_date_time").equals("null")) {
//                            eventDefinition.setEvent_session_parking_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_parking_ends_date_time")));
//                        }
//
//                        if (object.has("event_session_multi_dates")
//                                && !TextUtils.isEmpty(object.getString("event_session_multi_dates"))
//                                && !object.getString("event_session_multi_dates").equals("null")) {
//                            eventDefinition.setEvent_session_multi_dates(object.getString("event_session_multi_dates"));
//                        }
//
//                        if (object.has("event_session_parking_timings")
//                                && !TextUtils.isEmpty(object.getString("event_session_parking_timings"))
//                                && !object.getString("event_session_parking_timings").equals("null")) {
//                            eventDefinition.setEvent_session_parking_timings(object.getString("event_session_parking_timings"));
//                        }
//
//                        if (object.has("event_session_timings")
//                                && !TextUtils.isEmpty(object.getString("event_session_timings"))
//                                && !object.getString("event_session_timings").equals("null")) {
//                            eventDefinition.setEvent_session_timings(object.getString("event_session_timings"));
//                        }
//
//                        if (object.has("expires_by")
//                                && !TextUtils.isEmpty(object.getString("expires_by"))
//                                && !object.getString("expires_by").equals("null")) {
//                            eventDefinition.setExpires_by(object.getString("expires_by"));
//                        }
//                        if (object.has("regn_reqd")
//                                && !TextUtils.isEmpty(object.getString("regn_reqd"))
//                                && !object.getString("regn_reqd").equals("null")) {
//                            eventDefinition.setRegn_reqd(object.getBoolean("regn_reqd"));
//                        }
//                        if (object.has("event_session_regn_allowed")
//                                && !TextUtils.isEmpty(object.getString("event_session_regn_allowed"))
//                                && !object.getString("event_session_regn_allowed").equals("null")) {
//                            eventDefinition.setEvent_session_regn_allowed(object.getBoolean("event_session_regn_allowed"));
//                        }
//                        if (object.has("event_session_regn_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_regn_fee"))
//                                && !object.getString("event_session_regn_fee").equals("null")) {
//                            eventDefinition.setEvent_session_regn_fee(object.getString("event_session_regn_fee"));
//                        }
//                        if (object.has("event_session_led_by")
//                                && !TextUtils.isEmpty(object.getString("event_session_led_by"))
//                                && !object.getString("event_session_led_by").equals("null")) {
//                            eventDefinition.setEvent_session_led_by(object.getString("event_session_led_by"));
//                        }
//                        if (object.has("event_session_leaders_bio")
//                                && !TextUtils.isEmpty(object.getString("event_session_leaders_bio"))
//                                && !object.getString("event_session_leaders_bio").equals("null")) {
//                            eventDefinition.setEvent_session_leaders_bio(object.getString("event_session_leaders_bio"));
//                        }
//                        if (object.has("regn_reqd_for_parking")
//                                && !TextUtils.isEmpty(object.getString("regn_reqd_for_parking"))
//                                && !object.getString("regn_reqd_for_parking").equals("null")) {
//                            eventDefinition.setRegn_reqd_for_parking(object.getBoolean("regn_reqd_for_parking"));
//                        }
//                        if (object.has("renewable")
//                                && !TextUtils.isEmpty(object.getString("renewable"))
//                                && !object.getString("renewable").equals("null")) {
//                            eventDefinition.setRenewable(object.getBoolean("renewable"));
//                        }
//                        if (object.has("active")
//                                && !TextUtils.isEmpty(object.getString("active"))
//                                && !object.getString("active").equals("null")) {
//                            eventDefinition.setActive(object.getBoolean("active"));
//                        }
//                        if (object.has("event_session_parking_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_parking_fee"))
//                                && !object.getString("event_session_parking_fee").equals("null")) {
//                            eventDefinition.setEvent_session_parking_fee(object.getString("event_session_parking_fee"));
//                        }
//
//                        if (object.has("free_session")
//                                && !TextUtils.isEmpty(object.getString("free_session"))
//                                && !object.getString("free_session").equals("null")) {
//                            eventDefinition.setFree_session(object.getBoolean("free_session"));
//                        }
//
//                        if (object.has("free_session_parking")
//                                && !TextUtils.isEmpty(object.getString("free_session_parking"))
//                                && !object.getString("free_session_parking").equals("null")) {
//                            eventDefinition.setFree_session_parking(object.getBoolean("free_session_parking"));
//                        }
//
//                        if (object.has("event_session_regn_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_regn_fee"))
//                                && !object.getString("event_session_regn_fee").equals("null")) {
//                            eventDefinition.setEvent_session_regn_fee(object.getString("event_session_regn_fee"));
//                        }
//
//                        if (object.has("event_session_youth_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_youth_fee"))
//                                && !object.getString("event_session_youth_fee").equals("null")) {
//                            eventDefinition.setEvent_session_youth_fee(object.getString("event_session_youth_fee"));
//                        }
//
//                        if (object.has("event_session_child_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_child_fee"))
//                                && !object.getString("event_session_child_fee").equals("null")) {
//                            eventDefinition.setEvent_session_child_fee(object.getString("event_session_child_fee"));
//                        }
//
//                        if (object.has("event_session_student_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_student_fee"))
//                                && !object.getString("event_session_student_fee").equals("null")) {
//                            eventDefinition.setEvent_session_student_fee(object.getString("event_session_student_fee"));
//                        }
//
//                        if (object.has("event_session_minister_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_minister_fee"))
//                                && !object.getString("event_session_minister_fee").equals("null")) {
//                            eventDefinition.setEvent_session_minister_fee(object.getString("event_session_minister_fee"));
//                        }
//
//                        if (object.has("event_session_clergy_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_clergy_fee"))
//                                && !object.getString("event_session_clergy_fee").equals("null")) {
//                            eventDefinition.setEvent_session_clergy_fee(object.getString("event_session_clergy_fee"));
//                        }
//
//                        if (object.has("event_session_promo_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_promo_fee"))
//                                && !object.getString("event_session_promo_fee").equals("null")) {
//                            eventDefinition.setEvent_session_promo_fee(object.getString("event_session_promo_fee"));
//                        }
//
//                        if (object.has("event_session_senior_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_senior_fee"))
//                                && !object.getString("event_session_senior_fee").equals("null")) {
//                            eventDefinition.setEvent_session_senior_fee(object.getString("event_session_senior_fee"));
//                        }
//
//                        if (object.has("event_session_staff_fee")
//                                && !TextUtils.isEmpty(object.getString("event_session_staff_fee"))
//                                && !object.getString("event_session_staff_fee").equals("null")) {
//                            eventDefinition.setEvent_session_staff_fee(object.getString("event_session_staff_fee"));
//                        }
//
//                        if (object.has("event_session_link_ytube")
//                                && !TextUtils.isEmpty(object.getString("event_session_link_ytube"))
//                                && !object.getString("event_session_link_ytube").equals("null")) {
//                            eventDefinition.setEvent_session_link_ytube(object.getString("event_session_link_ytube"));
//                        }
//                        if (object.has("event_session_link_zoom")
//                                && !TextUtils.isEmpty(object.getString("event_session_link_zoom"))
//                                && !object.getString("event_session_link_zoom").equals("null")) {
//                            eventDefinition.setEvent_session_link_zoom(object.getString("event_session_link_zoom"));
//                        }
//                        if (object.has("event_session_link_googlemeet")
//                                && !TextUtils.isEmpty(object.getString("event_session_link_googlemeet"))
//                                && !object.getString("event_session_link_googlemeet").equals("null")) {
//                            eventDefinition.setEvent_session_link_googlemeet(object.getString("event_session_link_googlemeet"));
//                        }
//                        if (object.has("event_session_link_googleclassroom")
//                                && !TextUtils.isEmpty(object.getString("event_session_link_googleclassroom"))
//                                && !object.getString("event_session_link_googleclassroom").equals("null")) {
//                            eventDefinition.setEvent_session_link_googleclassroom(object.getString("event_session_link_googleclassroom"));
//                        }
//                        if (object.has("event_session_regn_limit")
//                                && !TextUtils.isEmpty(object.getString("event_session_regn_limit"))
//                                && !object.getString("event_session_regn_limit").equals("null")) {
//                            eventDefinition.setEvent_session_regn_limit(object.getString("event_session_regn_limit"));
//                        }
//                        if (object.has("web_event_session_regn_fee")
//                                && !TextUtils.isEmpty(object.getString("web_event_session_regn_fee"))
//                                && !object.getString("web_event_session_regn_fee").equals("null")) {
//                            eventDefinition.setWeb_event_session_regn_fee(object.getString("web_event_session_regn_fee"));
//                        }
//                        if (object.has("web_event_session_regn_limit")
//                                && !TextUtils.isEmpty(object.getString("web_event_session_regn_limit"))
//                                && !object.getString("web_event_session_regn_limit").equals("null")) {
//                            eventDefinition.setWeb_event_session_regn_limit(object.getString("web_event_session_regn_limit"));
//                        }
//                        if (object.has("web_event_session_location_to_show")
//                                && !TextUtils.isEmpty(object.getString("web_event_session_location_to_show"))
//                                && !object.getString("web_event_session_location_to_show").equals("null")) {
//                            eventDefinition.setWeb_event_session_location_to_show(object.getBoolean("web_event_session_location_to_show"));
//                        }
//                        if (object.has("free_web_session")
//                                && !TextUtils.isEmpty(object.getString("free_web_session"))
//                                && !object.getString("free_web_session").equals("null")) {
//                            eventDefinition.setFree_web_session(object.getBoolean("free_web_session"));
//                        }
//                        if (object.has("event_session_logi_type")
//                                && !TextUtils.isEmpty(object.getString("event_session_logi_type"))
//                                && !object.getString("event_session_logi_type").equals("null")) {
//                            eventDefinition.setEvent_session_logi_type(object.getInt("event_session_logi_type"));
//                        }
//
//                        if (object.has("session_link_array")
//                                && !TextUtils.isEmpty(object.getString("session_link_array"))
//                                && !object.getString("session_link_array").equals("null")) {
//                            eventDefinition.setSession_link_array(object.getString("session_link_array"));
//                        }
//
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            if (getActivity() != null) {
//                progressDialog.dismiss();
//                updateViews();
//            }
//        }
//    }
//
//    @Override
//    protected void updateViews() {
//        if (eventDefinition != null) {
//            Log.e("#DEBUG", "   updateView:  eventDetails:  " + new Gson().toJson(eventDefinition));
//
//            if (eventDefinition.getSession_link_array() != null
//                    && !TextUtils.isEmpty(eventDefinition.getSession_link_array())
//                    && !eventDefinition.getSession_link_array().equals("null")) {
//                mediaDefinitions.addAll(new Gson().fromJson(eventDefinition.getSession_link_array(),
//                        new TypeToken<ArrayList<MediaDefinition>>() {
//                        }.getType()));
//                mediaAdapter.notifyDataSetChanged();
//            }
//
//            if (mediaDefinitions.size() > 0) {
//                binding.llMediaLinks.setVisibility(View.VISIBLE);
//                binding.viewMediaLink.setVisibility(View.VISIBLE);
//            } else {
//                binding.llMediaLinks.setVisibility(View.GONE);
//                binding.viewMediaLink.setVisibility(View.GONE);
//            }
//
//            binding.llParkingTime.setVisibility(View.VISIBLE);
//            binding.llParkingAddress.setVisibility(View.VISIBLE);
//            binding.llEventAddress.setVisibility(View.VISIBLE);
//            binding.llParkingRate.setVisibility(View.VISIBLE);
//
//            if (eventDefinition.getEvent_session_image1() != null
//                    && !TextUtils.isEmpty(eventDefinition.getEvent_session_image1())
//                    && !eventDefinition.getEvent_session_image1().equals("null")) {
//                eventImages.add(eventDefinition.getEvent_session_image1());
//            }
//            if (eventDefinition.getEvent_session_image2() != null
//                    && !TextUtils.isEmpty(eventDefinition.getEvent_session_image2())
//                    && !eventDefinition.getEvent_session_image2().equals("null")) {
//                eventImages.add(eventDefinition.getEvent_session_image2());
//            }
//
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_blob_image())) {
//                eventImages.addAll(new Gson().fromJson(eventDefinition.getEvent_session_blob_image(),
//                        new TypeToken<ArrayList<String>>() {
//                        }.getType()));
//            }
//
//            eventImageAdapter.notifyDataSetChanged();
//
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_name())) {
//                binding.tvEventTitle.setText(eventDefinition.getEvent_session_name());
//            }
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_name())) {
//                binding.tvEventName.setText(eventDefinition.getEvent_name());
//            }
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_type())) {
//                binding.tvEventType.setText(eventDefinition.getEvent_session_type());
//            }
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_begins_date_time())
//                    && !TextUtils.isEmpty(eventDefinition.getEvent_session_ends_date_time())) {
//                try {
//                    binding.tvEventTime.setText(String.format("%s to %s",
//                            dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_session_begins_date_time())),
//                            dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_session_ends_date_time()))));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            if (eventDefinition.getEvent_session_logi_type() != EVENT_TYPE_AT_VIRTUALLY) {
//                if (!TextUtils.isEmpty(eventDefinition.getEvent_session_parking_begins_date_time())
//                        && !TextUtils.isEmpty(eventDefinition.getEvent_session_parking_ends_date_time())) {
//                    try {
//                        binding.tvParkingTime.setText(String.format("%s to %s",
//                                dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_session_parking_begins_date_time())),
//                                dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_session_parking_ends_date_time()))));
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                if (!TextUtils.isEmpty(eventDefinition.getEvent_session_address())) {
//                    binding.tvEventAddress.setText(eventDefinition.getEvent_session_address());
//                    try {
//                        JSONArray jsonArray = new JSONArray(eventDefinition.getEvent_session_address());
//                        if (jsonArray.length() > 0) {
//                            StringBuilder builder = new StringBuilder();
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                JSONArray jsonArray1 = jsonArray.getJSONArray(i);
//                                builder.append(jsonArray1.getString(0));
//                                builder.append("\n\n");
//                            }
//                            binding.tvEventAddress.setText(builder.toString());
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                if (!TextUtils.isEmpty(eventDefinition.getLocation_address())) {
//                    binding.tvLocationAddress.setText(eventDefinition.getLocation_address());
//                    try {
//                        JSONArray jsonArray = new JSONArray(eventDefinition.getLocation_address());
//                        if (jsonArray.length() > 0) {
//                            StringBuilder builder = new StringBuilder();
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                JSONArray jsonArray1 = jsonArray.getJSONArray(i);
//                                builder.append(jsonArray1.getString(0));
//                                builder.append("\n\n");
//                            }
//                            binding.tvLocationAddress.setText(builder.toString());
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                if (!TextUtils.isEmpty(eventDefinition.getEvent_session_parking_fee())) {
//                    if (eventDefinition.getEvent_session_parking_fee().equals("0")) {
//                        binding.tvEventRateParking.setText("FREE");
//                    } else {
//                        binding.tvEventRateParking.setText(String.format("$%s", eventDefinition.getEvent_session_parking_fee()));
//                    }
//                } else {
//                    binding.tvLabelCostParking.setVisibility(View.GONE);
//                    binding.tvEventRateParking.setVisibility(View.GONE);
//                }
//
//                if (eventDefinition.getEvent_session_parking_timings() != null) {
//                    try {
//                        JSONArray jsonArrayTime = null;
//                        if (eventDefinition.getEvent_session_parking_timings() != null) {
//                            StringBuilder stringBuilder = new StringBuilder();
//                            jsonArrayTime = new JSONArray(eventDefinition.getEvent_session_parking_timings());
//                            for (int i = 0; i < jsonArrayTime.length(); i++) {
//                                JSONArray jsonArray2 = jsonArrayTime.getJSONArray(i);
//                                if (i != 0) {
//                                    stringBuilder.append("\n");
//                                }
//                                if (jsonArray2 != null && jsonArray2.length() > 0) {
////                                    stringBuilder.append(jsonArray2.get(0).toString());
//                                    if (jsonArray2.get(0).toString().length() > 30) {
//                                        String[] s = jsonArray2.get(0).toString().split(" - ");
//                                        for (int j = 0; j < s.length; j++) {
//                                            if (j != 0) {
//                                                stringBuilder.append("-");
//                                            }
//                                            stringBuilder.append(DateTimeUtils.gmtToLocalDateAMPM(s[j]));
//                                        }
//                                    } else {
//                                        stringBuilder.append(jsonArray2.get(0).toString());
//                                    }
//                                }
//                            }
//                            binding.llParkingTime.setVisibility(View.VISIBLE);
//                            binding.tvParkingTime.setText(stringBuilder.toString());
//                        }
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                } else {
//                    binding.llParkingTime.setVisibility(View.GONE);
//                }
//
//                if (eventDefinition.getEvent_session_timings() != null
//                        && !TextUtils.isEmpty(eventDefinition.getEvent_session_timings())) {
//                    try {
//                        JSONArray jsonArrayTime = new JSONArray(eventDefinition.getEvent_session_timings());
//                        StringBuilder stringBuilder = new StringBuilder();
//                        if (jsonArrayTime != null && jsonArrayTime.length() > 0) {
//                            JSONArray jsonArray2 = jsonArrayTime.getJSONArray(0);
//                            if (jsonArray2 != null && jsonArray2.length() > 0) {
//                                if (jsonArray2.get(0).toString().length() > 30) {
//                                    String[] s = jsonArray2.get(0).toString().split(" - ");
//                                    for (int j = 0; j < s.length; j++) {
//                                        if (j != 0) {
//                                            stringBuilder.append("-");
//                                        }
//                                        stringBuilder.append(DateTimeUtils.gmtToLocalDateAMPM(s[j]));
//                                    }
//                                } else {
//                                    stringBuilder.append(jsonArray2.get(0).toString());
//                                }
//                            }
//                        }
//                        binding.llEventTime.setVisibility(View.VISIBLE);
//                        binding.tvEventTime.setText(stringBuilder.toString());
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    binding.llEventTime.setVisibility(View.GONE);
//                }
//
////                if (eventDefinition.getEvent_session_multi_dates() != null
////                        && !TextUtils.isEmpty(eventDefinition.getEvent_session_multi_dates())) {
////                    try {
////                        JSONArray jsonArray = new JSONArray(eventDefinition.getEvent_session_multi_dates());
////                        JSONArray jsonArrayTime = new JSONArray(eventDefinition.getEvent_session_timings());
////                        JSONArray jsonArrayParking = new JSONArray(eventDefinition.getEvent_session_parking_timings());
////                        StringBuilder stringBuilder = new StringBuilder();
////                        StringBuilder stringBuilderParking = new StringBuilder();
////                        for (int i = 0; i < jsonArray.length(); i++) {
////                            JSONArray jsonArray1 = jsonArray.getJSONArray(i);
////                            JSONArray jsonArray2 = jsonArrayTime.getJSONArray(i);
////                            JSONArray jsonArray3 = jsonArrayParking.getJSONArray(i);
////                            if (jsonArray1 != null && jsonArray1.length() > 0) {
////                                if (i != 0) {
////                                    stringBuilder.append("\n");
////                                    stringBuilderParking.append("\n");
////                                }
////                                stringBuilder.append(DateTimeUtils.getMMMDDYYYY(jsonArray1.get(0).toString()));
////                                stringBuilderParking.append(DateTimeUtils.getMMMDDYYYY(jsonArray1.get(0).toString()));
////                                stringBuilder.append("      ");
////                                stringBuilderParking.append("      ");
////                                if (jsonArray2 != null && jsonArray2.length() > 0) {
////                                    stringBuilder.append(jsonArray2.get(0).toString());
////                                }
////                                if (jsonArray3 != null && jsonArray3.length() > 0) {
////                                    stringBuilderParking.append(jsonArray3.get(0).toString());
////                                }
////                            }
////                        }
////                        binding.tvEventTime.setText(stringBuilder.toString());
////                        binding.tvParkingTime.setText(stringBuilderParking.toString());
////                    } catch (JSONException e) {
////                        e.printStackTrace();
////                    }
////                }
//
//            } else {
//                binding.llParkingTime.setVisibility(View.GONE);
//                binding.llParkingAddress.setVisibility(View.GONE);
//                binding.llEventAddress.setVisibility(View.GONE);
//                binding.llParkingRate.setVisibility(View.GONE);
//            }
//
//
////            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_address())) {
////                binding.tvEventAddress.setText(eventDefinition.getEvent_session_address());
////            }
////
////            if (!TextUtils.isEmpty(eventDefinition.getLocation_address())) {
////                binding.tvLocationAddress.setText(eventDefinition.getLocation_address());
////            }
//
//
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_short_description())) {
//                binding.tvEventShortDesc.setText(eventDefinition.getEvent_session_short_description());
//            }
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_long_description())) {
//                binding.tvEventLongDesc.setText(eventDefinition.getEvent_session_long_description());
//            }
//
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_regn_fee())) {
//                if (eventDefinition.getEvent_session_regn_fee().equals("0")) {
//                    binding.tvEventRate.setText("FREE");
//                } else {
//                    binding.tvEventRate.setText(String.format("$%s", eventDefinition.getEvent_session_regn_fee()));
//                }
//            }
//
//            if (!TextUtils.isEmpty(eventDefinition.getYear())) {
//                binding.tvEventYear.setText(eventDefinition.getYear());
//            }
//
//            if (!TextUtils.isEmpty(eventDefinition.getCompany_logo())) {
//                Glide.with(getActivity()).load(eventDefinition.getCompany_logo())
//                        .into(binding.ivCompanyLogo);
//            }
//
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_led_by())) {
//                binding.tvSessionLeadBy.setText(eventDefinition.getEvent_session_led_by());
//            }
//
//            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_leaders_bio())) {
//                binding.tvSessionLeaderBio.setText(eventDefinition.getEvent_session_leaders_bio());
//            }
//
//        }
//    }
//
//    @Override
//    protected void setListeners() {
//
//        binding.ivBtnPrev.setOnClickListener(v -> {
//            try {
//                if (binding.vpEventImage.getCurrentItem() == 0) {
//                    binding.vpEventImage.setCurrentItem(1);
//                    binding.ivBtnNext.setVisibility(View.GONE);
//                    binding.ivBtnPrev.setVisibility(View.VISIBLE);
//                } else {
//                    binding.vpEventImage.setCurrentItem(0);
//                    binding.ivBtnNext.setVisibility(View.VISIBLE);
//                    binding.ivBtnPrev.setVisibility(View.GONE);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//
//        binding.ivBtnNext.setOnClickListener(v -> {
//            try {
//                if (binding.vpEventImage.getCurrentItem() == 0) {
//                    binding.vpEventImage.setCurrentItem(1);
//                    binding.ivBtnNext.setVisibility(View.GONE);
//                    binding.ivBtnPrev.setVisibility(View.VISIBLE);
//                } else {
//                    binding.ivBtnPrev.setVisibility(View.GONE);
//                    binding.ivBtnNext.setVisibility(View.VISIBLE);
//                    binding.vpEventImage.setCurrentItem(0);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//
//    }
//
//    @Override
//    protected void initViews(View v) {
//        progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setCancelable(false);
//        progressDialog.setMessage("Loading...");
//
//        eventImageAdapter = new EventImageAdapter();
//        binding.vpEventImage.setAdapter(eventImageAdapter);
//
//        mediaAdapter = new MediaAdapter();
//        binding.rvMediaLinks.setLayoutManager(new GridLayoutManager(getActivity(), 2));
//        binding.rvMediaLinks.setAdapter(mediaAdapter);
//    }
//
//    private class EventImageAdapter extends PagerAdapter {
//
//        @Override
//        public int getCount() {
//            return eventImages.size();
//        }
//
//        @Override
//        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
//            return view == ((LinearLayout) o);
//        }
//
//        @NonNull
//        @Override
//        public Object instantiateItem(@NonNull ViewGroup container, int position) {
//            View itemView = LayoutInflater.from(getActivity()).inflate(R.layout.item_event_image, container, false);
//
//            ImageView imageView = itemView.findViewById(R.id.ivEventImage);
//            Glide.with(getActivity()).load(eventImages.get(position)).into(imageView);
//
//            container.addView(itemView);
//
//            return itemView;
//        }
//
//        @Override
//        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
//            container.removeView((LinearLayout) object);
//        }
//    }
//
//    private class MediaAdapter extends RecyclerView.Adapter<MediaAdapter.MediaHolder> {
//
//        @NonNull
//        @Override
//        public MediaAdapter.MediaHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//            return new MediaAdapter.MediaHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_media_link_info, viewGroup, false));
//        }
//
//        @Override
//        public void onBindViewHolder(@NonNull MediaAdapter.MediaHolder mediaHolder, int i) {
//            mediaHolder.tvBtnPlatform.setText(mediaDefinitions.get(i).getMedia_platform());
//        }
//
//        @Override
//        public int getItemCount() {
//            return mediaDefinitions.size();
//        }
//
//        public class MediaHolder extends RecyclerView.ViewHolder {
//            TextView tvBtnPlatform;
//
//            public MediaHolder(@NonNull View itemView) {
//                super(itemView);
//                tvBtnPlatform = itemView.findViewById(R.id.tvBtnPlatform);
//
//                itemView.setOnClickListener(v -> {
//                    openWebUrl(mediaDefinitions.get(getAdapterPosition()).getMedia_event_link());
//                });
//            }
//        }
//    }
//
//    private void openWebUrl(String url) {
//        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//        startActivity(browserIntent);
//    }
private FragEventSessionInfoViewBinding binding;
    private int eventSessionId;
    private ProgressDialog progressDialog;
    private EventSession eventDefinition;

    private ArrayList<String> eventImages = new ArrayList<>();
    private EventImageAdapter eventImageAdapter;
    SimpleDateFormat dateFormatMain = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd MMM, hh:mm a", Locale.getDefault());

    private ArrayList<MediaDefinition> mediaDefinitions = new ArrayList<>();
    private MediaAdapter mediaAdapter;
    private ArrayList<String> teleNumbers = new ArrayList<>();
    private TeleNumberAdapter teleNumberAdapter;
    private String teleCountryCode = "";
    private String storageDir;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            eventSessionId = getArguments().getInt("eventSessionId");
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.frag_event_session_info_view, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();

        storageDir = Environment.getExternalStorageDirectory().getAbsolutePath();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
            storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
        new fetchSessionEventDetails().execute();
    }

    private class fetchSessionEventDetails extends AsyncTask<String, String, String> {
        JSONObject json;
        JSONArray json1;
        String eventDefUrl = "_table/event_session_definitions";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            eventDefUrl = "_table/event_session_definitions?filter=id=" + eventSessionId;
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String url = getString(R.string.api) + getString(R.string.povlive) + eventDefUrl;
            Log.e("#DEBUG", "      fetchSessionEventDetails:  " + url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray jsonArray = json.getJSONArray("resource");

                    for (int j = 0; j < jsonArray.length(); j++) {
                        eventDefinition = new EventSession();
                        JSONObject object = jsonArray.getJSONObject(j);
                        if (object.has("id")
                                && !TextUtils.isEmpty(object.getString("id"))
                                && !object.getString("id").equals("null")) {
                            eventDefinition.setId(object.getInt("id"));
                        }
                        if (object.has("date_time")
                                && !TextUtils.isEmpty(object.getString("date_time"))
                                && !object.getString("date_time").equals("null")) {
                            eventDefinition.setDate_time(object.getString("date_time"));
                        }
                        if (object.has("manager_type")
                                && !TextUtils.isEmpty(object.getString("manager_type"))
                                && !object.getString("manager_type").equals("null")) {
                            eventDefinition.setManager_type(object.getString("manager_type"));
                        }
                        if (object.has("manager_type_id")
                                && !TextUtils.isEmpty(object.getString("manager_type_id"))
                                && !object.getString("manager_type_id").equals("null")) {
                            eventDefinition.setManager_type_id(object.getInt("manager_type_id"));
                        }
                        if (object.has("twp_id")
                                && !TextUtils.isEmpty(object.getString("twp_id"))
                                && !object.getString("twp_id").equals("null")) {
                            eventDefinition.setTwp_id(object.getInt("twp_id"));
                        }
                        if (object.has("township_code")
                                && !TextUtils.isEmpty(object.getString("township_code"))
                                && !object.getString("township_code").equals("null")) {
                            eventDefinition.setTownship_code(object.getString("township_code"));
                        }
                        if (object.has("township_name")
                                && !TextUtils.isEmpty(object.getString("township_name"))
                                && !object.getString("township_name").equals("null")) {
                            eventDefinition.setTownship_name(object.getString("township_name"));
                        }
                        if (object.has("company_id")
                                && !TextUtils.isEmpty(object.getString("company_id"))
                                && !object.getString("company_id").equals("null")) {
                            eventDefinition.setCompany_id(object.getInt("company_id"));
                        }
                        if (object.has("company_code")
                                && !TextUtils.isEmpty(object.getString("company_code"))
                                && !object.getString("company_code").equals("null")) {
                            eventDefinition.setCompany_code(object.getString("company_code"));
                        }
                        if (object.has("company_name")
                                && !TextUtils.isEmpty(object.getString("company_name"))
                                && !object.getString("company_name").equals("null")) {
                            eventDefinition.setCompany_name(object.getString("company_name"));
                        }
                        if (object.has("event_id")
                                && !TextUtils.isEmpty(object.getString("event_id"))
                                && !object.getString("event_id").equals("null")) {
                            eventDefinition.setEvent_id(object.getInt("event_id"));
                        }
                        if (object.has("event_name")
                                && !TextUtils.isEmpty(object.getString("event_name"))
                                && !object.getString("event_name").equals("null")) {
                            eventDefinition.setEvent_name(object.getString("event_name"));
                        }
                        if (object.has("event_type")
                                && !TextUtils.isEmpty(object.getString("event_type"))
                                && !object.getString("event_type").equals("null")) {
                            eventDefinition.setEvent_type(object.getString("event_type"));
                        }
                        if (object.has("event_code")
                                && !TextUtils.isEmpty(object.getString("event_code"))
                                && !object.getString("event_code").equals("null")) {
                            eventDefinition.setEvent_code(object.getString("event_code"));
                        }
                        if (object.has("event_session_id")
                                && !TextUtils.isEmpty(object.getString("event_session_id"))
                                && !object.getString("event_session_id").equals("null")) {
                            eventDefinition.setEvent_session_id(object.getInt("event_session_id"));
                        }
                        if (object.has("event_session_type")
                                && !TextUtils.isEmpty(object.getString("event_session_type"))
                                && !object.getString("event_session_type").equals("null")) {
                            eventDefinition.setEvent_session_type(object.getString("event_session_type"));
                        }
                        if (object.has("event_session_code")
                                && !TextUtils.isEmpty(object.getString("event_session_code"))
                                && !object.getString("event_session_code").equals("null")) {
                            eventDefinition.setEvent_session_code(object.getString("event_session_code"));
                        }
                        if (object.has("event_session_name")
                                && !TextUtils.isEmpty(object.getString("event_session_name"))
                                && !object.getString("event_session_name").equals("null")) {
                            eventDefinition.setEvent_session_name(object.getString("event_session_name"));
                        }
                        if (object.has("event_session_short_description")
                                && !TextUtils.isEmpty(object.getString("event_session_short_description"))
                                && !object.getString("event_session_short_description").equals("null")) {
                            eventDefinition.setEvent_session_short_description(object.getString("event_session_short_description"));
                        }
                        if (object.has("event_session_long_description")
                                && !TextUtils.isEmpty(object.getString("event_session_long_description"))
                                && !object.getString("event_session_long_description").equals("null")) {
                            eventDefinition.setEvent_session_long_description(object.getString("event_session_long_description"));
                        }
                        if (object.has("event_session_link_on_web")
                                && !TextUtils.isEmpty(object.getString("event_session_link_on_web"))
                                && !object.getString("event_session_link_on_web").equals("null")) {
                            eventDefinition.setEvent_session_link_on_web(object.getString("event_session_link_on_web"));
                        }
                        if (object.has("event_session_link_twitter")
                                && !TextUtils.isEmpty(object.getString("event_session_link_twitter"))
                                && !object.getString("event_session_link_twitter").equals("null")) {
                            eventDefinition.setEvent_session_link_twitter(object.getString("event_session_link_twitter"));
                        }
                        if (object.has("event_session_link_whatsapp")
                                && !TextUtils.isEmpty(object.getString("event_session_link_whatsapp"))
                                && !object.getString("event_session_link_whatsapp").equals("null")) {
                            eventDefinition.setEvent_session_link_whatsapp(object.getString("event_session_link_whatsapp"));
                        }
                        if (object.has("event_session_link_other_media")
                                && !TextUtils.isEmpty(object.getString("event_session_link_other_media"))
                                && !object.getString("event_session_link_other_media").equals("null")) {
                            eventDefinition.setEvent_session_link_other_media(object.getString("event_session_link_other_media"));
                        }

                        if (object.has("company_logo")
                                && !TextUtils.isEmpty(object.getString("company_logo"))
                                && !object.getString("company_logo").equals("null")) {
                            eventDefinition.setCompany_logo(object.getString("company_logo"));
                        }
                        if (object.has("event_logo")
                                && !TextUtils.isEmpty(object.getString("event_logo"))
                                && !object.getString("event_logo").equals("null")) {
                            eventDefinition.setEvent_logo(object.getString("event_logo"));
                        }
                        if (object.has("event_session_logo")
                                && !TextUtils.isEmpty(object.getString("event_session_logo"))
                                && !object.getString("event_session_logo").equals("null")) {
                            eventDefinition.setEvent_session_logo(object.getString("event_session_logo"));
                        }
                        if (object.has("event_session_image1")
                                && !TextUtils.isEmpty(object.getString("event_session_image1"))
                                && !object.getString("event_session_image1").equals("null")) {
                            eventDefinition.setEvent_session_image1(object.getString("event_session_image1"));
                        }
                        if (object.has("event_session_image2")
                                && !TextUtils.isEmpty(object.getString("event_session_image2"))
                                && !object.getString("event_session_image2").equals("null")) {
                            eventDefinition.setEvent_session_image2(object.getString("event_session_image2"));
                        }
                        if (object.has("event_session_blob_image")
                                && !TextUtils.isEmpty(object.getString("event_session_blob_image"))
                                && !object.getString("event_session_blob_image").equals("null")) {
                            eventDefinition.setEvent_session_blob_image(object.getString("event_session_blob_image"));
                        }
                        if (object.has("event_session_address")
                                && !TextUtils.isEmpty(object.getString("event_session_address"))
                                && !object.getString("event_session_address").equals("null")) {
                            eventDefinition.setEvent_session_address(object.getString("event_session_address"));
                        }
                        if (object.has("covered_locations")
                                && !TextUtils.isEmpty(object.getString("covered_locations"))
                                && !object.getString("covered_locations").equals("null")) {
                            eventDefinition.setCovered_locations(object.getString("covered_locations"));
                        }
                        if (object.has("session_regn_needed_approval")
                                && !TextUtils.isEmpty(object.getString("session_regn_needed_approval"))
                                && !object.getString("session_regn_needed_approval").equals("null")) {
                            eventDefinition.setSession_regn_needed_approval(object.getBoolean("session_regn_needed_approval"));
                        }
                        if (object.has("requirements")
                                && !TextUtils.isEmpty(object.getString("requirements"))
                                && !object.getString("requirements").equals("null")) {
                            eventDefinition.setRequirements(object.getString("requirements"));
                        }
                        if (object.has("appl_req_download")
                                && !TextUtils.isEmpty(object.getString("appl_req_download"))
                                && !object.getString("appl_req_download").equals("null")) {
                            eventDefinition.setAppl_req_download(object.getString("appl_req_download"));
                        }
                        if (object.has("cost")
                                && !TextUtils.isEmpty(object.getString("cost"))
                                && !object.getString("cost").equals("null")) {
                            eventDefinition.setCost(object.getString("cost"));
                        }
                        if (object.has("year")
                                && !TextUtils.isEmpty(object.getString("year"))
                                && !object.getString("year").equals("null")) {
                            eventDefinition.setYear(object.getString("year"));
                        }
                        if (object.has("location_address")
                                && !TextUtils.isEmpty(object.getString("location_address"))
                                && !object.getString("location_address").equals("null")) {
                            eventDefinition.setLocation_address(object.getString("location_address"));
                        }
                        if (object.has("scheme_type")
                                && !TextUtils.isEmpty(object.getString("scheme_type"))
                                && !object.getString("scheme_type").equals("null")) {
                            eventDefinition.setScheme_type(object.getString("scheme_type"));
                        }
                        if (object.has("event_prefix")
                                && !TextUtils.isEmpty(object.getString("event_prefix"))
                                && !object.getString("event_prefix").equals("null")) {
                            eventDefinition.setEvent_prefix(object.getString("event_prefix"));
                        }

                        if (object.has("event_session_prefix")
                                && !TextUtils.isEmpty(object.getString("event_session_prefix"))
                                && !object.getString("event_session_prefix").equals("null")) {
                            eventDefinition.setEvent_session_prefix(object.getString("event_session_prefix"));
                        }
                        if (object.has("event_nextnum")
                                && !TextUtils.isEmpty(object.getString("event_nextnum"))
                                && !object.getString("event_nextnum").equals("null")) {
                            eventDefinition.setEvent_nextnum(object.getInt("event_nextnum"));
                        }
                        if (object.has("event_session_nextnum")
                                && !TextUtils.isEmpty(object.getString("event_session_nextnum"))
                                && !object.getString("event_session_nextnum").equals("null")) {
                            eventDefinition.setEvent_session_nextnum(object.getInt("event_session_nextnum"));
                        }
                        if (object.has("event_session_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_session_begins_date_time"))
                                && !object.getString("event_session_begins_date_time").equals("null")) {
                            eventDefinition.setEvent_session_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_begins_date_time")));
                        }
                        if (object.has("event_session_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_session_ends_date_time"))
                                && !object.getString("event_session_ends_date_time").equals("null")) {
                            eventDefinition.setEvent_session_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_ends_date_time")));
                        }
                        if (object.has("event_session_parking_begins_date_time")
                                && !TextUtils.isEmpty(object.getString("event_session_parking_begins_date_time"))
                                && !object.getString("event_session_parking_begins_date_time").equals("null")) {
                            eventDefinition.setEvent_session_parking_begins_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_parking_begins_date_time")));
                        }
                        if (object.has("event_session_parking_ends_date_time")
                                && !TextUtils.isEmpty(object.getString("event_session_parking_ends_date_time"))
                                && !object.getString("event_session_parking_ends_date_time").equals("null")) {
                            eventDefinition.setEvent_session_parking_ends_date_time(DateTimeUtils.gmtToLocalDate(object.getString("event_session_parking_ends_date_time")));
                        }

                        if (object.has("event_session_multi_dates")
                                && !TextUtils.isEmpty(object.getString("event_session_multi_dates"))
                                && !object.getString("event_session_multi_dates").equals("null")) {
                            eventDefinition.setEvent_session_multi_dates(object.getString("event_session_multi_dates"));
                        }

                        if (object.has("event_session_parking_timings")
                                && !TextUtils.isEmpty(object.getString("event_session_parking_timings"))
                                && !object.getString("event_session_parking_timings").equals("null")) {
                            eventDefinition.setEvent_session_parking_timings(object.getString("event_session_parking_timings"));
                        }

                        if (object.has("event_session_timings")
                                && !TextUtils.isEmpty(object.getString("event_session_timings"))
                                && !object.getString("event_session_timings").equals("null")) {
                            eventDefinition.setEvent_session_timings(object.getString("event_session_timings"));
                        }

                        if (object.has("expires_by")
                                && !TextUtils.isEmpty(object.getString("expires_by"))
                                && !object.getString("expires_by").equals("null")) {
                            eventDefinition.setExpires_by(object.getString("expires_by"));
                        }
                        if (object.has("regn_reqd")
                                && !TextUtils.isEmpty(object.getString("regn_reqd"))
                                && !object.getString("regn_reqd").equals("null")) {
                            eventDefinition.setRegn_reqd(object.getBoolean("regn_reqd"));
                        }
                        if (object.has("event_session_regn_allowed")
                                && !TextUtils.isEmpty(object.getString("event_session_regn_allowed"))
                                && !object.getString("event_session_regn_allowed").equals("null")) {
                            eventDefinition.setEvent_session_regn_allowed(object.getBoolean("event_session_regn_allowed"));
                        }
                        if (object.has("event_session_regn_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_regn_fee"))
                                && !object.getString("event_session_regn_fee").equals("null")) {
                            eventDefinition.setEvent_session_regn_fee(object.getString("event_session_regn_fee"));
                        }
                        if (object.has("event_session_led_by")
                                && !TextUtils.isEmpty(object.getString("event_session_led_by"))
                                && !object.getString("event_session_led_by").equals("null")) {
                            eventDefinition.setEvent_session_led_by(object.getString("event_session_led_by"));
                        }
                        if (object.has("event_session_leaders_bio")
                                && !TextUtils.isEmpty(object.getString("event_session_leaders_bio"))
                                && !object.getString("event_session_leaders_bio").equals("null")) {
                            eventDefinition.setEvent_session_leaders_bio(object.getString("event_session_leaders_bio"));
                        }
                        if (object.has("regn_reqd_for_parking")
                                && !TextUtils.isEmpty(object.getString("regn_reqd_for_parking"))
                                && !object.getString("regn_reqd_for_parking").equals("null")) {
                            eventDefinition.setRegn_reqd_for_parking(object.getBoolean("regn_reqd_for_parking"));
                        }
                        if (object.has("renewable")
                                && !TextUtils.isEmpty(object.getString("renewable"))
                                && !object.getString("renewable").equals("null")) {
                            eventDefinition.setRenewable(object.getBoolean("renewable"));
                        }
                        if (object.has("active")
                                && !TextUtils.isEmpty(object.getString("active"))
                                && !object.getString("active").equals("null")) {
                            eventDefinition.setActive(object.getBoolean("active"));
                        }
                        if (object.has("event_session_parking_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_parking_fee"))
                                && !object.getString("event_session_parking_fee").equals("null")) {
                            eventDefinition.setEvent_session_parking_fee(object.getString("event_session_parking_fee"));
                        }

                        if (object.has("free_session")
                                && !TextUtils.isEmpty(object.getString("free_session"))
                                && !object.getString("free_session").equals("null")) {
                            eventDefinition.setFree_session(object.getBoolean("free_session"));
                        }

                        if (object.has("free_session_parking")
                                && !TextUtils.isEmpty(object.getString("free_session_parking"))
                                && !object.getString("free_session_parking").equals("null")) {
                            eventDefinition.setFree_session_parking(object.getBoolean("free_session_parking"));
                        }

                        if (object.has("event_session_regn_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_regn_fee"))
                                && !object.getString("event_session_regn_fee").equals("null")) {
                            eventDefinition.setEvent_session_regn_fee(object.getString("event_session_regn_fee"));
                        }

                        if (object.has("event_session_youth_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_youth_fee"))
                                && !object.getString("event_session_youth_fee").equals("null")) {
                            eventDefinition.setEvent_session_youth_fee(object.getString("event_session_youth_fee"));
                        }

                        if (object.has("event_session_child_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_child_fee"))
                                && !object.getString("event_session_child_fee").equals("null")) {
                            eventDefinition.setEvent_session_child_fee(object.getString("event_session_child_fee"));
                        }

                        if (object.has("event_session_student_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_student_fee"))
                                && !object.getString("event_session_student_fee").equals("null")) {
                            eventDefinition.setEvent_session_student_fee(object.getString("event_session_student_fee"));
                        }

                        if (object.has("event_session_minister_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_minister_fee"))
                                && !object.getString("event_session_minister_fee").equals("null")) {
                            eventDefinition.setEvent_session_minister_fee(object.getString("event_session_minister_fee"));
                        }

                        if (object.has("event_session_clergy_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_clergy_fee"))
                                && !object.getString("event_session_clergy_fee").equals("null")) {
                            eventDefinition.setEvent_session_clergy_fee(object.getString("event_session_clergy_fee"));
                        }

                        if (object.has("event_session_promo_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_promo_fee"))
                                && !object.getString("event_session_promo_fee").equals("null")) {
                            eventDefinition.setEvent_session_promo_fee(object.getString("event_session_promo_fee"));
                        }

                        if (object.has("event_session_senior_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_senior_fee"))
                                && !object.getString("event_session_senior_fee").equals("null")) {
                            eventDefinition.setEvent_session_senior_fee(object.getString("event_session_senior_fee"));
                        }

                        if (object.has("event_session_staff_fee")
                                && !TextUtils.isEmpty(object.getString("event_session_staff_fee"))
                                && !object.getString("event_session_staff_fee").equals("null")) {
                            eventDefinition.setEvent_session_staff_fee(object.getString("event_session_staff_fee"));
                        }

                        if (object.has("event_session_link_ytube")
                                && !TextUtils.isEmpty(object.getString("event_session_link_ytube"))
                                && !object.getString("event_session_link_ytube").equals("null")) {
                            eventDefinition.setEvent_session_link_ytube(object.getString("event_session_link_ytube"));
                        }
                        if (object.has("event_session_link_zoom")
                                && !TextUtils.isEmpty(object.getString("event_session_link_zoom"))
                                && !object.getString("event_session_link_zoom").equals("null")) {
                            eventDefinition.setEvent_session_link_zoom(object.getString("event_session_link_zoom"));
                        }
                        if (object.has("event_session_link_googlemeet")
                                && !TextUtils.isEmpty(object.getString("event_session_link_googlemeet"))
                                && !object.getString("event_session_link_googlemeet").equals("null")) {
                            eventDefinition.setEvent_session_link_googlemeet(object.getString("event_session_link_googlemeet"));
                        }
                        if (object.has("event_session_link_googleclassroom")
                                && !TextUtils.isEmpty(object.getString("event_session_link_googleclassroom"))
                                && !object.getString("event_session_link_googleclassroom").equals("null")) {
                            eventDefinition.setEvent_session_link_googleclassroom(object.getString("event_session_link_googleclassroom"));
                        }
                        if (object.has("event_session_regn_limit")
                                && !TextUtils.isEmpty(object.getString("event_session_regn_limit"))
                                && !object.getString("event_session_regn_limit").equals("null")) {
                            eventDefinition.setEvent_session_regn_limit(object.getString("event_session_regn_limit"));
                        }
                        if (object.has("web_event_session_regn_fee")
                                && !TextUtils.isEmpty(object.getString("web_event_session_regn_fee"))
                                && !object.getString("web_event_session_regn_fee").equals("null")) {
                            eventDefinition.setWeb_event_session_regn_fee(object.getString("web_event_session_regn_fee"));
                        }
                        if (object.has("web_event_session_regn_limit")
                                && !TextUtils.isEmpty(object.getString("web_event_session_regn_limit"))
                                && !object.getString("web_event_session_regn_limit").equals("null")) {
                            eventDefinition.setWeb_event_session_regn_limit(object.getString("web_event_session_regn_limit"));
                        }
                        if (object.has("web_event_session_location_to_show")
                                && !TextUtils.isEmpty(object.getString("web_event_session_location_to_show"))
                                && !object.getString("web_event_session_location_to_show").equals("null")) {
                            eventDefinition.setWeb_event_session_location_to_show(object.getBoolean("web_event_session_location_to_show"));
                        }
                        if (object.has("free_web_session")
                                && !TextUtils.isEmpty(object.getString("free_web_session"))
                                && !object.getString("free_web_session").equals("null")) {
                            eventDefinition.setFree_web_session(object.getBoolean("free_web_session"));
                        }
                        if (object.has("event_session_logi_type")
                                && !TextUtils.isEmpty(object.getString("event_session_logi_type"))
                                && !object.getString("event_session_logi_type").equals("null")) {
                            eventDefinition.setEvent_session_logi_type(object.getInt("event_session_logi_type"));
                        }

                        if (object.has("session_link_array")
                                && !TextUtils.isEmpty(object.getString("session_link_array"))
                                && !object.getString("session_link_array").equals("null")) {
                            eventDefinition.setSession_link_array(object.getString("session_link_array"));
                        }

                        if (object.has("parking_reservation_limit")
                                && !TextUtils.isEmpty(object.getString("parking_reservation_limit"))
                                && !object.getString("parking_reservation_limit").equals("null")) {
                            eventDefinition.setParking_reservation_limit(Integer.parseInt(object.getString("parking_reservation_limit")));
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (getActivity() != null) {
                progressDialog.dismiss();
                updateViews();
            }
        }
    }

    @Override
    protected void updateViews() {
        if (eventDefinition != null) {
            Log.e("#DEBUG", "   updateView:  eventDetails:  " + new Gson().toJson(eventDefinition));

            if (eventDefinition.getSession_link_array() != null
                    && !TextUtils.isEmpty(eventDefinition.getSession_link_array())
                    && !eventDefinition.getSession_link_array().equals("null")) {
                mediaDefinitions.addAll(new Gson().fromJson(eventDefinition.getSession_link_array(),
                        new TypeToken<ArrayList<MediaDefinition>>() {
                        }.getType()));
                mediaAdapter.notifyDataSetChanged();
            }

            if (mediaDefinitions.size() > 0) {
                binding.llMediaLinks.setVisibility(View.VISIBLE);
                binding.viewMediaLink.setVisibility(View.VISIBLE);
            } else {
                binding.llMediaLinks.setVisibility(View.GONE);
                binding.viewMediaLink.setVisibility(View.GONE);
            }

            binding.llParkingTime.setVisibility(View.VISIBLE);
            binding.llParkingAddress.setVisibility(View.VISIBLE);
            binding.llEventAddress.setVisibility(View.VISIBLE);
            binding.llParkingRate.setVisibility(View.VISIBLE);

            if (eventDefinition.getEvent_session_image1() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_session_image1())
                    && !eventDefinition.getEvent_session_image1().equals("null")) {
                eventImages.add(eventDefinition.getEvent_session_image1());
            }
            if (eventDefinition.getEvent_session_image2() != null
                    && !TextUtils.isEmpty(eventDefinition.getEvent_session_image2())
                    && !eventDefinition.getEvent_session_image2().equals("null")) {
                eventImages.add(eventDefinition.getEvent_session_image2());
            }

            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_blob_image())) {
                eventImages.addAll(new Gson().fromJson(eventDefinition.getEvent_session_blob_image(),
                        new TypeToken<ArrayList<String>>() {
                        }.getType()));
            }

            eventImageAdapter.notifyDataSetChanged();

            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_name())) {
                binding.tvEventTitle.setText(eventDefinition.getEvent_session_name());
            }
            if (!TextUtils.isEmpty(eventDefinition.getEvent_name())) {
                binding.tvEventName.setText(eventDefinition.getEvent_name());
            }
            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_type())) {
                binding.tvEventType.setText(eventDefinition.getEvent_session_type());
            }
            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_begins_date_time())
                    && !TextUtils.isEmpty(eventDefinition.getEvent_session_ends_date_time())) {
                try {
                    binding.tvEventTime.setText(String.format("%s to %s",
                            dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_session_begins_date_time())),
                            dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_session_ends_date_time()))));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (eventDefinition.getEvent_session_logi_type() != EVENT_TYPE_AT_VIRTUALLY) {
                if (!TextUtils.isEmpty(eventDefinition.getEvent_session_parking_begins_date_time())
                        && !TextUtils.isEmpty(eventDefinition.getEvent_session_parking_ends_date_time())) {
                    try {
                        binding.tvParkingTime.setText(String.format("%s to %s",
                                dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_session_parking_begins_date_time())),
                                dateFormatOut.format(dateFormatMain.parse(eventDefinition.getEvent_session_parking_ends_date_time()))));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                if (!TextUtils.isEmpty(eventDefinition.getEvent_session_address())) {
                    binding.tvEventAddress.setText(eventDefinition.getEvent_session_address());
                    try {
                        JSONArray jsonArray = new JSONArray(eventDefinition.getEvent_session_address());
                        if (jsonArray.length() > 0) {
                            StringBuilder builder = new StringBuilder();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONArray jsonArray1 = jsonArray.getJSONArray(i);
                                builder.append(jsonArray1.getString(0));
                                builder.append("\n\n");
                            }
                            binding.tvEventAddress.setText(builder.toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (!TextUtils.isEmpty(eventDefinition.getLocation_address())) {
                    binding.tvLocationAddress.setText(eventDefinition.getLocation_address());
                    try {
                        JSONArray jsonArray = new JSONArray(eventDefinition.getLocation_address());
                        if (jsonArray.length() > 0) {
                            StringBuilder builder = new StringBuilder();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONArray jsonArray1 = jsonArray.getJSONArray(i);
                                builder.append(jsonArray1.getString(0));
                                builder.append("\n\n");
                            }
                            binding.tvLocationAddress.setText(builder.toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (!TextUtils.isEmpty(eventDefinition.getEvent_session_parking_fee())) {
                    if (eventDefinition.getEvent_session_parking_fee().equals("0")) {
                        binding.tvEventRateParking.setText("FREE");
                    } else {
                        binding.tvEventRateParking.setText(String.format("$%s", eventDefinition.getEvent_session_parking_fee()));
                    }
                } else {
                    binding.tvLabelCostParking.setVisibility(View.GONE);
                    binding.tvEventRateParking.setVisibility(View.GONE);
                }

                if (eventDefinition.getEvent_session_parking_timings() != null) {
                    try {
                        JSONArray jsonArrayTime = null;
                        if (eventDefinition.getEvent_session_parking_timings() != null) {
                            StringBuilder stringBuilder = new StringBuilder();
                            jsonArrayTime = new JSONArray(eventDefinition.getEvent_session_parking_timings());
                            for (int i = 0; i < jsonArrayTime.length(); i++) {
                                JSONArray jsonArray2 = jsonArrayTime.getJSONArray(i);
                                if (i != 0) {
                                    stringBuilder.append("\n");
                                }
                                if (jsonArray2 != null && jsonArray2.length() > 0) {
//                                    stringBuilder.append(jsonArray2.get(0).toString());
                                    if (jsonArray2.get(0).toString().length() > 30) {
                                        String[] s = jsonArray2.get(0).toString().split(" - ");
                                        for (int j = 0; j < s.length; j++) {
                                            if (j != 0) {
                                                stringBuilder.append("-");
                                            }
                                            stringBuilder.append(DateTimeUtils.gmtToLocalDateAMPM(s[j]));
                                        }
                                    } else {
                                        stringBuilder.append(jsonArray2.get(0).toString());
                                    }
                                }
                            }
                            binding.llParkingTime.setVisibility(View.VISIBLE);
                            binding.tvParkingTime.setText(stringBuilder.toString());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    binding.llParkingTime.setVisibility(View.GONE);
                }

                if (eventDefinition.getEvent_session_timings() != null
                        && !TextUtils.isEmpty(eventDefinition.getEvent_session_timings())) {
                    try {
                        JSONArray jsonArrayTime = new JSONArray(eventDefinition.getEvent_session_timings());
                        StringBuilder stringBuilder = new StringBuilder();
                        if (jsonArrayTime != null && jsonArrayTime.length() > 0) {
                            JSONArray jsonArray2 = jsonArrayTime.getJSONArray(0);
                            if (jsonArray2 != null && jsonArray2.length() > 0) {
                                if (jsonArray2.get(0).toString().length() > 30) {
                                    String[] s = jsonArray2.get(0).toString().split(" - ");
                                    for (int j = 0; j < s.length; j++) {
                                        if (j != 0) {
                                            stringBuilder.append("-");
                                        }
                                        stringBuilder.append(DateTimeUtils.gmtToLocalDateAMPM(s[j]));
                                    }
                                } else {
                                    stringBuilder.append(jsonArray2.get(0).toString());
                                }
                            }
                        }
                        binding.llEventTime.setVisibility(View.VISIBLE);
                        binding.tvEventTime.setText(stringBuilder.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    binding.llEventTime.setVisibility(View.GONE);
                }

//                if (eventDefinition.getEvent_session_multi_dates() != null
//                        && !TextUtils.isEmpty(eventDefinition.getEvent_session_multi_dates())) {
//                    try {
//                        JSONArray jsonArray = new JSONArray(eventDefinition.getEvent_session_multi_dates());
//                        JSONArray jsonArrayTime = new JSONArray(eventDefinition.getEvent_session_timings());
//                        JSONArray jsonArrayParking = new JSONArray(eventDefinition.getEvent_session_parking_timings());
//                        StringBuilder stringBuilder = new StringBuilder();
//                        StringBuilder stringBuilderParking = new StringBuilder();
//                        for (int i = 0; i < jsonArray.length(); i++) {
//                            JSONArray jsonArray1 = jsonArray.getJSONArray(i);
//                            JSONArray jsonArray2 = jsonArrayTime.getJSONArray(i);
//                            JSONArray jsonArray3 = jsonArrayParking.getJSONArray(i);
//                            if (jsonArray1 != null && jsonArray1.length() > 0) {
//                                if (i != 0) {
//                                    stringBuilder.append("\n");
//                                    stringBuilderParking.append("\n");
//                                }
//                                stringBuilder.append(DateTimeUtils.getMMMDDYYYY(jsonArray1.get(0).toString()));
//                                stringBuilderParking.append(DateTimeUtils.getMMMDDYYYY(jsonArray1.get(0).toString()));
//                                stringBuilder.append("      ");
//                                stringBuilderParking.append("      ");
//                                if (jsonArray2 != null && jsonArray2.length() > 0) {
//                                    stringBuilder.append(jsonArray2.get(0).toString());
//                                }
//                                if (jsonArray3 != null && jsonArray3.length() > 0) {
//                                    stringBuilderParking.append(jsonArray3.get(0).toString());
//                                }
//                            }
//                        }
//                        binding.tvEventTime.setText(stringBuilder.toString());
//                        binding.tvParkingTime.setText(stringBuilderParking.toString());
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }

            } else {
                binding.llParkingTime.setVisibility(View.GONE);
                binding.llParkingAddress.setVisibility(View.GONE);
                binding.llEventAddress.setVisibility(View.GONE);
                binding.llParkingRate.setVisibility(View.GONE);
            }


//            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_address())) {
//                binding.tvEventAddress.setText(eventDefinition.getEvent_session_address());
//            }
//
//            if (!TextUtils.isEmpty(eventDefinition.getLocation_address())) {
//                binding.tvLocationAddress.setText(eventDefinition.getLocation_address());
//            }


            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_short_description())) {
                binding.tvEventShortDesc.setText(eventDefinition.getEvent_session_short_description());
            }
            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_long_description())) {
                binding.tvEventLongDesc.setText(eventDefinition.getEvent_session_long_description());
            }

            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_regn_fee())) {
                if (eventDefinition.getEvent_session_regn_fee().equals("0")) {
                    binding.tvEventRate.setText("FREE");
                } else {
                    binding.tvEventRate.setText(String.format("$%s", eventDefinition.getEvent_session_regn_fee()));
                }
            }

            if (!TextUtils.isEmpty(eventDefinition.getYear())) {
                binding.tvEventYear.setText(eventDefinition.getYear());
            }

            if (!TextUtils.isEmpty(eventDefinition.getCompany_logo())) {
                Glide.with(getActivity()).load(eventDefinition.getCompany_logo())
                        .into(binding.ivCompanyLogo);
            }

            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_led_by())) {
                binding.tvSessionLeadBy.setText(eventDefinition.getEvent_session_led_by());
            }

            if (!TextUtils.isEmpty(eventDefinition.getEvent_session_leaders_bio())) {
                binding.tvSessionLeaderBio.setText(eventDefinition.getEvent_session_leaders_bio());
            }

        }
    }

    @Override
    protected void setListeners() {

        binding.ivBtnPrev.setOnClickListener(v -> {
            try {
                if (binding.vpEventImage.getCurrentItem() == 0) {
                    binding.vpEventImage.setCurrentItem(1);
                    binding.ivBtnNext.setVisibility(View.GONE);
                    binding.ivBtnPrev.setVisibility(View.VISIBLE);
                } else {
                    binding.vpEventImage.setCurrentItem(0);
                    binding.ivBtnNext.setVisibility(View.VISIBLE);
                    binding.ivBtnPrev.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        binding.ivBtnNext.setOnClickListener(v -> {
            try {
                if (binding.vpEventImage.getCurrentItem() == 0) {
                    binding.vpEventImage.setCurrentItem(1);
                    binding.ivBtnNext.setVisibility(View.GONE);
                    binding.ivBtnPrev.setVisibility(View.VISIBLE);
                } else {
                    binding.ivBtnPrev.setVisibility(View.GONE);
                    binding.ivBtnNext.setVisibility(View.VISIBLE);
                    binding.vpEventImage.setCurrentItem(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

    @Override
    protected void initViews(View v) {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");

        eventImageAdapter = new EventImageAdapter();
        binding.vpEventImage.setAdapter(eventImageAdapter);

        mediaAdapter = new MediaAdapter();
        binding.rvMediaLinks.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        binding.rvMediaLinks.setAdapter(mediaAdapter);
    }

    private class EventImageAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return eventImages.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == ((LinearLayout) o);
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View itemView = LayoutInflater.from(getActivity()).inflate(R.layout.item_event_image, container, false);

            ImageView imageView = itemView.findViewById(R.id.ivEventImage);
            Glide.with(getActivity()).load(eventImages.get(position)).into(imageView);

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((LinearLayout) object);
        }
    }

    private class MediaAdapter extends RecyclerView.Adapter<MediaAdapter.MediaHolder> {

        @NonNull
        @Override
        public MediaAdapter.MediaHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new MediaAdapter.MediaHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_media_link_info, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MediaAdapter.MediaHolder mediaHolder, int i) {
            mediaHolder.tvBtnPlatform.setText(mediaDefinitions.get(i).getMedia_platform());
        }

        @Override
        public int getItemCount() {
            return mediaDefinitions.size();
        }

        public class MediaHolder extends RecyclerView.ViewHolder {
            TextView tvBtnPlatform;

            public MediaHolder(@NonNull View itemView) {
                super(itemView);
                tvBtnPlatform = itemView.findViewById(R.id.tvBtnPlatform);

                itemView.setOnClickListener(v -> {
                    //  openWebUrl(mediaDefinitions.get(getAdapterPosition()).getMedia_event_link());
                    final int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        MediaDefinition mediaDefinition = mediaDefinitions.get(pos);
                        if (mediaDefinition != null && !TextUtils.isEmpty(mediaDefinition.getMedia_platform())) {
                            if (mediaDefinition.getMedia_platform().equals("Zoom")) {
                                showMediaInfoForZoom(mediaDefinition);
                            } else if (mediaDefinition.getMedia_platform().equals("YouTube")) {
                                showMediaInfoForYoutube(mediaDefinition);
                            } else if (mediaDefinition.getMedia_platform().equals("Facebook")) {
                                showMediaInfoForFacebook(mediaDefinition);
                            } else if (mediaDefinition.getMedia_platform().equals("Google Meet")) {
                                showMediaInfoForGoogleMeet(mediaDefinition);
                            } else if (mediaDefinition.getMedia_platform().equals("Google Classroom")) {
                                showMediaInfoForGoogleClassroom(mediaDefinition);
                            } else if (mediaDefinition.getMedia_platform().equals("Skype")) {
                                showMediaInfoForSkype(mediaDefinition);
                            } else if (mediaDefinition.getMedia_platform().equals("Website")) {
                                openWebUrl(mediaDefinition.getMedia_domain_url());
                            } else if (mediaDefinition.getMedia_platform().equals("PDF")) {
                                PDFViewFragment pdfViewFragment = new PDFViewFragment();
                                Bundle bundle = new Bundle();
                                bundle.putString("mediaDefinition", new Gson().toJson(mediaDefinition));
                                pdfViewFragment.setArguments(bundle);
                                replaceFragment(pdfViewFragment, "pdfView");
                            } else if (mediaDefinition.getMedia_platform().equals("Powerpoint")) {
                                downloadFile(mediaDefinition.getPptFilePath());
                            } else if (mediaDefinition.getMedia_platform().equals("Slideshow")) {
                                downloadFile(mediaDefinition.getFilePath());
                            } else if (mediaDefinition.getMedia_platform().equals("TeleConference")) {
                                showMediaInfoForTeleConference(mediaDefinition);
                            } else if (mediaDefinition.getMedia_platform().equals("Audio/Video")) {
                                String path = mediaDefinition.getAudioVideoPath();
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);//DO NOT FORGET THIS EVER

                                if (mediaDefinition.getAudioVideoType().equals("Audio")) {
                                    intent.setDataAndType(Uri.parse(path), "audio/*");
                                } else if (mediaDefinition.getAudioVideoType().equals("Video")) {
                                    intent.setDataAndType(Uri.parse(path), "video/*");
                                }
                                startActivity(intent);
                            }
                        }
                    }
                });
            }
        }
    }

//    private void openWebUrl(String url) {
//        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//        startActivity(browserIntent);
//    }

    private void showMediaInfoForYoutube(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoYoutubeBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_youtube, null, false);
        builder.setView(zoomBinding.getRoot());
        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getYoutubeStreamUrl())) {
            zoomBinding.llStreamingUrl.setVisibility(View.VISIBLE);
            zoomBinding.tvSpecialUrl.setText(mediaDefinition.getYoutubeStreamUrl());
            zoomBinding.tvUrlTitle.setText(mediaDefinition.getYoutubeStreamTitle());
        }

        AlertDialog alertDialog = builder.create();
        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnOpen.setOnClickListener(v -> {
//            Intent videoIntent = new Intent(getActivity(), YoutubeVideoActivity.class);
//            videoIntent.putExtra("videoURL", mediaDefinition.getYoutubeStreamUrl());
//            startActivity(videoIntent);
            String id, url = mediaDefinition.getYoutubeStreamUrl();
            if (url!=null)
            {
                if (url.contains("=")) {
                    String ary[] = url.split("=");
                    id = ary[ary.length - 1];
                } else {
                    String ary[] = url.split("/");
                    id = ary[ary.length - 1];
                }
                Log.d("YouTube", "id: " + id);
                alertDialog.dismiss();
                Intent intent = YouTubeIntents.createPlayVideoIntentWithOptions(getActivity(), id, true, false);
                startActivity(intent);
            }
            else
            {
                alertDialog.dismiss();
                Toast.makeText(getContext(), "No Link Found", Toast.LENGTH_SHORT).show();

            }
        });
        zoomBinding.ivBtnOpenYouTubeApp.setOnClickListener(v -> {
            if (mediaDefinition.getYoutubeStreamUrl()!= null)
            {
                openYoutubeUrl(mediaDefinition.getYoutubeStreamUrl());
                alertDialog.dismiss();
            }
            else
            {
                alertDialog.dismiss();
                Toast.makeText(getContext(), "No Link Found", Toast.LENGTH_SHORT).show();
            }
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
            if (mediaDefinition.getYoutubeStreamUrl()!= null)
            {
                openWebUrl(mediaDefinition.getYoutubeStreamUrl());
                alertDialog.dismiss();
            }
            else
            {
                alertDialog.dismiss();
                Toast.makeText(getContext(), "No Link Found", Toast.LENGTH_SHORT).show();
            }
        });
        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Url ", mediaDefinition.getYoutubeStreamUrl());
        });
        zoomBinding.ivBtnCopyUrlTitle.setOnClickListener(v -> {
            myApp.copyText("Title ", mediaDefinition.getYoutubeStreamTitle());
        });

        alertDialog.show();
    }
    public void openYoutubeUrl(String url) {
        String id;
        if (url.contains("=")) {
            String ary[] = url.split("=");
            id = ary[ary.length - 1];
        } else {
            String ary[] = url.split("/");
            id = ary[ary.length - 1];
        }
        Log.d("YouTube", "id: " + id);
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
        Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + id));
        appIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            getActivity().startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            getActivity().startActivity(webIntent);
        }
    }

    private void showMediaInfoForZoom(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoZoomBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_zoom, null, false);
        builder.setView(zoomBinding.getRoot());

        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getZoomSpecialUrl())) {
            zoomBinding.llSpecialUrl.setVisibility(View.VISIBLE);
            zoomBinding.tvSpecialUrl.setText(mediaDefinition.getZoomSpecialUrl());
        }
        if (!TextUtils.isEmpty(mediaDefinition.getZoomMeetingId())) {
            zoomBinding.llMeetingId.setVisibility(View.VISIBLE);
            zoomBinding.tvMeetingId.setText(mediaDefinition.getZoomMeetingId());
        }
        if (!TextUtils.isEmpty(mediaDefinition.getZoomPassword())) {
            zoomBinding.llMeetingPassword.setVisibility(View.VISIBLE);
            try {
                zoomBinding.tvMeetingPassword.setText(AESEncyption.decrypt(mediaDefinition.getZoomPassword()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(mediaDefinition.getZoomPhone())) {
            zoomBinding.llPhone.setVisibility(View.VISIBLE);
            zoomBinding.tvPhone.setText(mediaDefinition.getZoomPhone());
        }

        AlertDialog alertDialog = builder.create();
        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnOpen.setOnClickListener(v -> {
        });
        zoomBinding.ivBtnOpenZoomApp.setOnClickListener(v -> {
            openZoomUrl(mediaDefinition);
        });
        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
//            openWebUrl(mediaDefinition.getZoomSpecialUrl());
            openZoomUrlInBrowser(mediaDefinition);
        });
        zoomBinding.ivBtnOpenSpecialUrl.setOnClickListener(v -> {
            openWebUrl(mediaDefinition.getZoomSpecialUrl());
        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Stream Url", mediaDefinition.getZoomSpecialUrl());
        });
        zoomBinding.ivBtnCopyMeetingId.setOnClickListener(v -> {
            myApp.copyText("Meeting ID", mediaDefinition.getZoomMeetingId());
        });
        zoomBinding.ivBtnCopyMeetingPassword.setOnClickListener(v -> {
            try {
                myApp.copyText("Meeting Password", AESEncyption.decrypt(mediaDefinition.getZoomPassword()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        zoomBinding.ivBtnCopyPhone.setOnClickListener(v -> {
            myApp.copyText("Meeting Phone", mediaDefinition.getZoomPhone());
        });
        zoomBinding.ivBtnDialPhone.setOnClickListener(v -> {
            if (mediaDefinition.getZoomPhone().contains(",")) {
                String[] phone = mediaDefinition.getZoomPhone().split(",");
                if (phone.length > 0 && phone[0] != null) {
                    dialNumber("tel:" + phone[0]);
                }
            } else {
                dialNumber("tel:" + mediaDefinition.getZoomPhone());
            }
        });
        alertDialog.show();
    }

    private void openZoomUrl(MediaDefinition mediaDefinition) {
        String pass = "";
        try {
            pass = AESEncyption.decrypt(mediaDefinition.getZoomPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent zoomIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("zoomus://" + "zoom.us/join?confno=" + mediaDefinition.getZoomMeetingId() + "&pwd=" + pass));
        zoomIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PackageManager packageManager = getContext().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("us.zoom.videomeetings", 0).versionCode;
            startActivity(zoomIntent);
        } catch (PackageManager.NameNotFoundException e) {
            openAppInGooglePlay("us.zoom.videomeetings");
        }
    }
    private void openZoomUrlInBrowser(MediaDefinition mediaDefinition) {
        String pass = "";
        try {
            pass = AESEncyption.decrypt(mediaDefinition.getZoomPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String url = "https://zoom.us/join?confno=" + mediaDefinition.getZoomMeetingId() + "&pwd=" + pass;
        Log.e("#DEBUG", "   openZoomUrlInBrowser:  Url: " + url);
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void dialNumber(String num) {
        Uri number = Uri.parse(num);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
        startActivity(callIntent);
    }
    public void openAppInGooglePlay(String packageName) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
        } catch (android.content.ActivityNotFoundException e) { // if there is no Google Play on device
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
        }
    }
    private void showMediaInfoForFacebook(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoFacebookBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_facebook, null, false);
        builder.setView(zoomBinding.getRoot());

        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getFacebookServerUrl())) {
            zoomBinding.llStreamUrl.setVisibility(View.VISIBLE);
            zoomBinding.tvStreamUrl.setText(mediaDefinition.getFacebookServerUrl());
        }

        if (!TextUtils.isEmpty(mediaDefinition.getFacebookStreamKey())) {
            zoomBinding.llStreamKey.setVisibility(View.VISIBLE);
            try {
                zoomBinding.tvStreamKey.setText(AESEncyption.decrypt(mediaDefinition.getFacebookStreamKey()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        AlertDialog alertDialog = builder.create();
        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnOpen.setOnClickListener(v -> {
        });

        zoomBinding.ivBtnOpenFaceBookApp.setOnClickListener(v -> {
            openFaceBookUrl(mediaDefinition.getFacebookServerUrl());
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
            openWebUrl(mediaDefinition.getFacebookServerUrl());
        });
        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnOpenStreamUrl.setOnClickListener(v -> {
            openWebUrl(mediaDefinition.getFacebookServerUrl());
        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Streaming Url", mediaDefinition.getFacebookServerUrl());
        });
        zoomBinding.ivBtnCopyStreamKey.setOnClickListener(v -> {
            try {
                myApp.copyText("Streaming Url", AESEncyption.decrypt(mediaDefinition.getFacebookStreamKey()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        alertDialog.show();
    }
    private void openFaceBookUrl(String facebookStreamUrl) {
        String urlFb = facebookStreamUrl;
        String FACEBOOK_PAGE_ID = "appetizerandroid";
        Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
        PackageManager packageManager = getContext().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            Log.d("AppVersion", "FB: " + versionCode);
            if (versionCode >= 3002850) {
                urlFb = "fb://facewebmodal/f?href=" + facebookStreamUrl;
            } else {                    //older versions of fb app
                urlFb = "fb://page/" + FACEBOOK_PAGE_ID;
            }
            facebookIntent.setData(Uri.parse(urlFb));
            startActivity(facebookIntent);
        } catch (PackageManager.NameNotFoundException e) {
            openAppInGooglePlay("com.facebook.katana");
        }
    }
    private void showMediaInfoForGoogleMeet(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoGoogleMeetBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_google_meet, null, false);
        builder.setView(zoomBinding.getRoot());

        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getGoogleMeetUrl())) {
            zoomBinding.llMeetingUrl.setVisibility(View.VISIBLE);
            zoomBinding.tvSpecialUrl.setText(mediaDefinition.getGoogleMeetUrl());
        }
        if (!TextUtils.isEmpty(mediaDefinition.getGoogleMeetPhoneNumber())) {
            zoomBinding.llMeetingPhoneNumber.setVisibility(View.VISIBLE);
            zoomBinding.tvMeetingPhoneNumber.setText(mediaDefinition.getGoogleMeetPhoneNumber());
        }
        if (!TextUtils.isEmpty(mediaDefinition.getGoogleMeetPassword())) {
            zoomBinding.llMeetingPassword.setVisibility(View.VISIBLE);
            try {
                zoomBinding.tvMeetingPassword.setText(AESEncyption.decrypt(mediaDefinition.getGoogleMeetPassword()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        AlertDialog alertDialog = builder.create();
        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnOpen.setOnClickListener(v -> {
        });

        zoomBinding.ivBtnOpenMeetApp.setOnClickListener(v -> {
            openGoogleMeetUrl(mediaDefinition.getGoogleMeetUrl());
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
//            openWebUrl(mediaDefinition.getGoogleMeetUrl());
            String url = mediaDefinition.getGoogleMeetUrl();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW);
            if (url.contains("www") || url.contains("http") || url.contains("https")) {
                browserIntent.setData(Uri.parse(url));
            } else {
                browserIntent.setData(Uri.parse("https://" + url.trim()));
            }
            try {
                startActivity(browserIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnOpenUrl.setOnClickListener(v -> {
            openWebUrl(mediaDefinition.getGoogleMeetUrl());
        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Meeting Url", mediaDefinition.getGoogleMeetUrl());
        });
        zoomBinding.ivBtnCopyMeetingPhoneNumber.setOnClickListener(v -> {
            myApp.copyText("Meeting Phone Number", mediaDefinition.getGoogleMeetPhoneNumber());
        });

        zoomBinding.ivBtnCopyMeetingPassword.setOnClickListener(v -> {
            try {
                myApp.copyText("Meeting Password", AESEncyption.decrypt(mediaDefinition.getGoogleMeetPassword()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        zoomBinding.ivBtnDialMeetingPhoneNumber.setOnClickListener(v -> {
            /*if (mediaDefinition.getZoomPhone().contains(",")) {
                String[] phone = mediaDefinition.getZoomPhone().split(",");
                if (phone.length > 0 && phone[0] != null) {
                    dialNumber("tel:" + phone[0]);
                }
            } else {
                dialNumber("tel:" + mediaDefinition.getZoomPhone());
            }*/
        });

        alertDialog.show();
    }

    private void openGoogleMeetUrl(String googleMeetStreamUrl) {
        Intent googleMeetIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(googleMeetStreamUrl));
        PackageManager packageManager = getContext().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.google.android.apps.meetings", 0).versionCode;
            startActivity(googleMeetIntent);
        } catch (PackageManager.NameNotFoundException e) {
            openAppInGooglePlay("com.google.android.apps.meetings");
        }
    }

    private void downloadFile(String url) {
        progressDialog.setMessage("Downloading file...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        File file = new File(storageDir + "/parkezly/");
        if (!file.exists()) {
            file.mkdir();
        }
        Log.e("#DEBUG", "  FileName:  " + "test." + url.substring(url.lastIndexOf(".") + 1));
        AndroidNetworking.download(url, file.getAbsolutePath(), "test." + url.substring(url.lastIndexOf(".") + 1))
                .build()
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        progressDialog.dismiss();
                        try {
                            File file = new File(storageDir + "/parkezly/test.ppt");
                            Uri uri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", file);
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "Sorry, Unable to open file!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Sorry, Unable to open file!", Toast.LENGTH_SHORT).show();
                        Log.e("#DEBUG", "  downloadFile: onError: " + anError.getMessage());
                    }
                });
    }

    private void showMediaInfoForTeleConference(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoTeleConferenceBinding teleBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_tele_conference, null, false);
        builder.setView(teleBinding.getRoot());

        teleNumberAdapter = new TeleNumberAdapter();
        teleBinding.rvNumbers.setLayoutManager(new LinearLayoutManager(getActivity()));
        teleBinding.rvNumbers.setAdapter(teleNumberAdapter);
        if (mediaDefinition.getTeleConfNumbers() != null && mediaDefinition.getTeleConfNumbers().size() > 1) {
            teleNumbers.clear();
            teleNumbers.addAll(mediaDefinition.getTeleConfNumbers());
            teleNumberAdapter.notifyDataSetChanged();
        }
        if (mediaDefinition.getTeleConfCountryCode() != null && !TextUtils.isEmpty(mediaDefinition.getTeleConfCountryCode())) {
            teleCountryCode = mediaDefinition.getTeleConfCountryCode();
        }

        teleBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (mediaDefinition.getTeleConfMeetingId() != null && !TextUtils.isEmpty(mediaDefinition.getTeleConfMeetingId())) {
            teleBinding.llMeetingId.setVisibility(View.VISIBLE);
            teleBinding.tvMeetingId.setText(mediaDefinition.getTeleConfMeetingId());
        }
        if (mediaDefinition.getTeleConfMeetingPassword() != null && !TextUtils.isEmpty(mediaDefinition.getTeleConfMeetingPassword())) {
            teleBinding.llMeetingPassword.setVisibility(View.VISIBLE);
            try {
                teleBinding.tvMeetingPassword.setText(AESEncyption.decrypt(mediaDefinition.getTeleConfMeetingPassword()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        AlertDialog alertDialog = builder.create();
        teleBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        alertDialog.show();

    }
    private void openWebUrl(String url) {
//        if (url.contains("http")) {
//            try {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                startActivity(browserIntent);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        } else {
//            try {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + url.trim()));
//                startActivity(browserIntent);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
        Intent browserIntent;
        if (url.contains("www") || url.contains("http") || url.contains("https")) {
            Log.d("#DEBUG", "   if");
            browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        } else {
            Log.d("#DEBUG", "   Else");
            browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://" + url.trim()));
        }
        try {
            browserIntent.setPackage("com.android.chrome");
            getActivity().startActivity(browserIntent);
        } catch (Exception e) {
            Toast.makeText(myApp, "There is something went to wrong", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
    private class TeleNumberAdapter extends RecyclerView.Adapter<TeleNumberAdapter.NumberHolder> {

        @NonNull
        @Override
        public TeleNumberAdapter.NumberHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new TeleNumberAdapter.NumberHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_tele_number_view, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull TeleNumberAdapter.NumberHolder holder, int position) {
            if (!TextUtils.isEmpty(teleNumbers.get(position))) {
                holder.tvPhone.setText(String.format("+%s%s", teleCountryCode, teleNumbers.get(position)));
            }
        }

        @Override
        public int getItemCount() {
            return teleNumbers.size();
        }

        public class NumberHolder extends RecyclerView.ViewHolder {
            TextView tvPhone;
            ImageView ivBtnDialPhone, ivBtnCopyPhone;

            public NumberHolder(@NonNull View itemView) {
                super(itemView);
                tvPhone = itemView.findViewById(R.id.tvPhone);
                ivBtnDialPhone = itemView.findViewById(R.id.ivBtnDialPhone);
                ivBtnCopyPhone = itemView.findViewById(R.id.ivBtnCopyPhone);

                ivBtnCopyPhone.setOnClickListener(view -> {
                    myApp.copyText("Conference Number", "+" + teleCountryCode + teleNumbers.get(getAdapterPosition()));
                });

                ivBtnDialPhone.setOnClickListener(view -> {
                    dialNumber("tel:" + "+" + teleCountryCode + teleNumbers.get(getAdapterPosition()));
                });
            }
        }
    }
    private void showMediaInfoForGoogleClassroom(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoGoogleClassroomBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_google_classroom, null, false);
        builder.setView(zoomBinding.getRoot());

        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getGoogleClasstroomCode())) {
            zoomBinding.llClassCode.setVisibility(View.VISIBLE);
            zoomBinding.tvClassCode.setText(mediaDefinition.getGoogleClasstroomCode());
        }

        AlertDialog alertDialog = builder.create();
        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnOpen.setOnClickListener(v -> {
        });

        zoomBinding.ivBtnOpenClassroomApp.setOnClickListener(v -> {
            openGoogleClassroomUrl("http://www.classroom.google.com");
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
//            openWebUrl("http://www.classroom.google.com");
            String url = mediaDefinition.getGoogleClasstroomCode();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW);
            if (url.contains("www") || url.contains("http") || url.contains("https")) {
                browserIntent.setData(Uri.parse(url));
            } else {
                browserIntent.setData(Uri.parse("https://" + url.trim()));
            }
            try {
                startActivity(browserIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Classroom Code", mediaDefinition.getGoogleClasstroomCode());
        });

        zoomBinding.btnManage.setOnClickListener(v -> {
            alertDialog.dismiss();
//            if (mediaDefinition.getMedia_platform().equals("Google Classroom")){
//                final FragmentTransaction ft = getFragmentManager().beginTransaction();
//                GClassRoomHomeFragment frag = new GClassRoomHomeFragment();
//                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
//                ft.add(R.id.My_Container_1_ID, frag, "gClassroom");
//                fragmentStack.lastElement().onPause();
//                ft.hide(fragmentStack.lastElement());
//                fragmentStack.push(frag);
//                ft.commitAllowingStateLoss();
//            }
            startActivity(new Intent(getActivity(), GClassroomActivity.class));
        });

        alertDialog.show();
    }
    private void openGoogleClassroomUrl(String googleClassroomStreamUrl) {
        Intent googleClassroomIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(googleClassroomStreamUrl));
        PackageManager packageManager = getContext().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.google.android.apps.classroom", 0).versionCode;
            startActivity(googleClassroomIntent);
        } catch (PackageManager.NameNotFoundException e) {
            openAppInGooglePlay("com.google.android.apps.classroom");
        }
    }
    private void showMediaInfoForSkype(MediaDefinition mediaDefinition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogEventMediaInfoSkypeBinding zoomBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_event_media_info_skype, null, false);
        builder.setView(zoomBinding.getRoot());

        zoomBinding.tvTitle.setText(mediaDefinition.getMedia_platform());
        if (!TextUtils.isEmpty(mediaDefinition.getSkypeStreamUrl())) {
            zoomBinding.llSpecialUrl.setVisibility(View.VISIBLE);
            zoomBinding.tvSpecialUrl.setText(mediaDefinition.getSkypeStreamUrl());
        }

        AlertDialog alertDialog = builder.create();
        zoomBinding.btnOK.setOnClickListener(v -> alertDialog.dismiss());
        zoomBinding.btnOpen.setOnClickListener(v -> {
        });
        zoomBinding.ivBtnOpenSkypeApp.setOnClickListener(v -> {
            openSkypeUrl(mediaDefinition.getYoutubeStreamUrl());
        });
        zoomBinding.ivBtnOpenBrowser.setOnClickListener(view -> {
            openWebUrl(mediaDefinition.getSkypeStreamUrl());
        });

        zoomBinding.ivBtnOpenAppUrl.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mediaDefinition.getMedia_domain_url())) {
                openWebUrl(mediaDefinition.getMedia_domain_url());
            }
        });
        zoomBinding.ivBtnOpenSpecialUrl.setOnClickListener(v -> {
            openWebUrl(mediaDefinition.getSkypeStreamUrl());
        });
        zoomBinding.ivBtnCopyUrl.setOnClickListener(v -> {
            myApp.copyText("Stream Url", mediaDefinition.getSkypeStreamUrl());
        });

        alertDialog.show();
    }
    private void openSkypeUrl(String skypeStreamUrl) {  //https://join.skype.com/XFVtCgaoi464
        Intent skypeIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(skypeStreamUrl));
        skypeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PackageManager packageManager = getContext().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.skype.raider", PackageManager.GET_ACTIVITIES).versionCode;
            Log.d("AppVersion", "Zoom: " + versionCode);
            // Restrict the Intent to being handled by the Skype for Android client only.
            skypeIntent.setComponent(new ComponentName("com.skype.raider", "com.skype.raider.Main"));
            startActivity(skypeIntent);
        } catch (PackageManager.NameNotFoundException e) {
            openAppInGooglePlay("com.skype.raider");
        }
    }
}
