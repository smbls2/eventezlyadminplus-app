package com.softmeasures.eventezlyadminplus.frament.event.AddEdit;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.databinding.DialogEventOptionTypeBinding;
import com.softmeasures.eventezlyadminplus.interfaces.EventTypeListener;

import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_LOCATION;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_AT_VIRTUALLY;
import static com.softmeasures.eventezlyadminplus.services.Constants.EVENT_TYPE_BOTH;

public class EventOptionTypeDialog extends BottomSheetDialogFragment {
    private EventTypeListener listener;

    private DialogEventOptionTypeBinding binding;
    private TextView tvTitle;
    private int selectedEventType;
    private boolean isSession = false;
    private boolean isEdit = false;
    private int eventLogiType = EVENT_TYPE_AT_LOCATION;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (EventTypeListener) getParentFragment();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static EventOptionTypeDialog newInstance() {
        return new EventOptionTypeDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            isSession = getArguments().getBoolean("isSession", false);
            isEdit = getArguments().getBoolean("isEdit", false);
            eventLogiType = getArguments().getInt("eventLogiType", EVENT_TYPE_AT_LOCATION);
            selectedEventType = eventLogiType;
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_event_option_type, container,
                false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    private void setListeners() {

        binding.rgEventType.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.rbLocation) {
                selectedEventType = EVENT_TYPE_AT_LOCATION;
            } else if (checkedId == R.id.rbVirtually) {
                selectedEventType = EVENT_TYPE_AT_VIRTUALLY;
            } else if (checkedId == R.id.rbBoth) {
                selectedEventType = EVENT_TYPE_BOTH;
            }
        });

        binding.btnNext.setOnClickListener(v -> {
            getDialog().dismiss();
            if (listener != null) {
                listener.onNext(selectedEventType, isEdit);
            }
        });
    }

    private void updateViews() {

        if (eventLogiType == EVENT_TYPE_AT_LOCATION) {
            binding.rgEventType.check(R.id.rbLocation);
        } else if (eventLogiType == EVENT_TYPE_AT_VIRTUALLY) {
            binding.rgEventType.check(R.id.rbVirtually);
        } else if (eventLogiType == EVENT_TYPE_BOTH) {
            binding.rgEventType.check(R.id.rbBoth);
        }

        if (isSession) {
            tvTitle.setText("Where This Session is Held?");
        } else {
            tvTitle.setText("Where This Event is Held?");
        }

    }

    private void initViews(View view) {
        tvTitle = view.findViewById(R.id.tvTitle);

    }
}
