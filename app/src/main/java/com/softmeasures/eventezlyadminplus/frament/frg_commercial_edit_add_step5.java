package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.MyApplication;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.models.LocationLot;
import com.softmeasures.eventezlyadminplus.models.Manager;
import com.softmeasures.eventezlyadminplus.utils.DateTimeUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.frament.commercial_parking_main.commercial_edit_parking;
import static com.softmeasures.eventezlyadminplus.frament.commercial_parking_main.commercial_id;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1.commercail_no_parking_time;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1.commercail_parking_time;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1.commercial_address;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1.commercial_lang;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1.commercial_lat;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1.commercial_loc_code;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1.commercial_place_id;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step1.commercial_title;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step2.commecial_lots_avali;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step2.commercial_lost_total;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step2.commercial_off_end;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step2.commercial_off_peak_discount;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step2.commercial_off_peak_weekend;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step2.commercial_off_start;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step2.commercial_rows_total;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step2.commercial_space_total;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step2.str_pluse;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step2.township_labeling;
import static com.softmeasures.eventezlyadminplus.frament.frg_commercial_edit_add_step3.isclickeditrules;

public class frg_commercial_edit_add_step5 extends BaseFragment {

    TextView txt_commercial_title_3;
    Switch sw_commercial_effect, sw_commercial_add_active, sw_commercial_renew;
    boolean commercial_renew = true, commercial_add_effect = true, commercial_add_active = true;
    EditText txt_commercial_custome_notices;
    String commercial_add_custome_notices;
    TextView txt_commercial_parking_save;
    RelativeLayout rl_progressbar;
    String locationId;
    private RelativeLayout rl_main;
    private String parkingType = "";
    private String lot_numbering_type = "";

    private ArrayList<LocationLot> locationLots = new ArrayList<>();
    private boolean isNewLocation = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getArguments() != null) {
            isNewLocation = getArguments().getBoolean("isNewLocation", false);
            parkingType = getArguments().getString("township_type_sp");
            Log.e("#DEBUG", "   frg_commercial_edit_add_step5:  parkingType:  " + parkingType);
        }
        return inflater.inflate(R.layout.commercial_edit_step_3, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void updateViews() {
        txt_commercial_title_3.setText(commercial_title);
    }

    @Override
    protected void setListeners() {
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        sw_commercial_renew.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                commercial_renew = isChecked;
            }
        });

        sw_commercial_effect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                commercial_add_effect = isChecked;
            }
        });

        sw_commercial_add_active.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                commercial_add_active = isChecked;
            }
        });

        txt_commercial_parking_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                commercial_add_custome_notices = txt_commercial_custome_notices.getText().toString();

                if (commercial_edit_parking) {
                    new UpdateCommercialParking().execute();
                } else {
                    new AddCommercialParking().execute();
                }


            }
        });
    }

    @Override
    protected void initViews(View view) {

        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        txt_commercial_title_3 = (TextView) view.findViewById(R.id.txt_commercial_title_sp3);
        sw_commercial_effect = (Switch) view.findViewById(R.id.sw_commercial_effect);
        sw_commercial_add_active = (Switch) view.findViewById(R.id.sw_commercial_active);
        sw_commercial_renew = (Switch) view.findViewById(R.id.sw_commercial_renew);
        txt_commercial_custome_notices = (EditText) view.findViewById(R.id.txt_commercial_custom_notices);
        txt_commercial_parking_save = (TextView) view.findViewById(R.id.txt_commercial_save);
        rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);

    }

    public class UpdateCommercialParking extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        JSONObject json12 = null;
        String id = "";
        String walleturl = "_table/google_parking_partners";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("address", commercial_address);
            if (commercial_add_active) {
                jsonValues1.put("active", 1);
            } else {
                jsonValues1.put("active", 0);
            }

            if (commercial_add_effect) {
                jsonValues1.put("in_effect", 1);
            } else {
                jsonValues1.put("in_effect", 0);
            }
            if (commercial_renew) {
                jsonValues1.put("renewable", 1);
            } else {
                jsonValues1.put("renewable", 0);
            }
            jsonValues1.put("parking_times", commercail_parking_time);
            jsonValues1.put("no_parking_times", commercail_no_parking_time);
            jsonValues1.put("off_peak_starts", commercial_off_start);
            jsonValues1.put("off_peak_discount", commercial_off_peak_discount);
            jsonValues1.put("lots_avbl", commecial_lots_avali);
            jsonValues1.put("lots_total", commercial_lost_total);
            jsonValues1.put("weekend_special_diff", str_pluse + commercial_off_peak_weekend);
            jsonValues1.put("off_peak_ends", commercial_off_end);
            jsonValues1.put("custom_notice", commercial_add_custome_notices);
            jsonValues1.put("lat", commercial_lat);
            jsonValues1.put("lng", commercial_lang);
            jsonValues1.put("location_code", commercial_loc_code);
            jsonValues1.put("place_id", commercial_place_id);
            jsonValues1.put("marker_type", "commercial");
            jsonValues1.put("title", commercial_title);
            jsonValues1.put("user_id", user_id);
            jsonValues1.put("id", commercial_id);
            jsonValues1.put("location_place_id", commercial_place_id);
            jsonValues1.put("lot_numbering_type", lot_numbering_type);
            jsonValues1.put("lot_numbering_description", lot_numbering_type);

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("place_id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json12 != null) {
                if (json12.has("error")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("Error");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                } else {
                    if (!id.equals("")) {
                        fragmentStack.clear();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        commercial_parking_main pay = new commercial_parking_main();
                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.add(R.id.My_Container_1_ID, pay);
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                    }
                }

            }
            super.onPostExecute(s);
        }
    }

    // add commercial parking rules.................
    public class AddCommercialParking extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        JSONObject json12 = null;
        String id = "";
        String walleturl = "_table/google_parking_partners";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("location-code", commercial_loc_code);
            jsonValues1.put("place_id", commercial_place_id);
            jsonValues1.put("title", commercial_title);
            jsonValues1.put("address", commercial_address);
            jsonValues1.put("user_id", user_id);
            jsonValues1.put("location_code", commercial_loc_code);
            jsonValues1.put("location_name", commercial_title);
            jsonValues1.put("parking_times", commercail_parking_time);
            jsonValues1.put("no_parking_times", commercail_no_parking_time);
            jsonValues1.put("off_peak_discount", commercial_off_peak_discount);
            jsonValues1.put("custom_notice", commercial_add_custome_notices);
            if (commercial_add_effect) {
                jsonValues1.put("in_effect", 1);
            } else {
                jsonValues1.put("in_effect", 0);
            }
            jsonValues1.put("marker_type", "commercial");
            if (commercial_add_active) {
                jsonValues1.put("active", 1);
            } else {
                jsonValues1.put("active", 0);
            }
            if (commercial_renew) {
                jsonValues1.put("renewable", 1);
            } else {
                jsonValues1.put("renewable", 0);
            }
            jsonValues1.put("weekend_special_diff", str_pluse + commercial_off_peak_weekend);
            jsonValues1.put("off_peak_starts", commercial_off_start);
            jsonValues1.put("off_peak_ends", commercial_off_end);
            jsonValues1.put("lots_total", commercial_lost_total);
            jsonValues1.put("lots_avbl", commecial_lots_avali);
            jsonValues1.put("lot_numbering_type", township_labeling);
            jsonValues1.put("lot_numbering_description", township_labeling);
            jsonValues1.put("lat", commercial_lat);
            jsonValues1.put("lng", commercial_lang);
            if (!commercial_place_id.equals("")) {
                jsonValues1.put("location_place_id", commercial_place_id);
            }
            Manager manager = myApp.getSelectedManager();
            if (manager != null) {
                jsonValues1.put("twp_id", manager.getId());
                jsonValues1.put("township_code", manager.getManager_id());
                jsonValues1.put("manager_id", manager.getId());
                jsonValues1.put("company_id", manager.getId());
                jsonValues1.put("manager_type_id", manager.getManager_type_id());
                jsonValues1.put("company_type_id", manager.getManager_type_id());
            }
            //TODO events
//            jsonValues1.put("event_on_location","");
//            jsonValues1.put("event_dates","");

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   AddCommercialParking:  respo:  " + responseStr);
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject c = array.getJSONObject(i);
                        if (c.has("place_id")) {
                            id = c.getString("place_id");
                        }
                        if (c.has("id")) {
                            id = String.valueOf(c.getInt("id"));
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json12 != null) {
                if (json12.has("error")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("Error");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                } else {
                    if (!id.equals("")) {
                        locationId = id;
                        new updateCommercialParkingLocationId().execute();
                    }
                }

            }
            super.onPostExecute(s);
        }
    }

    //update location id in google_parking_partners
    public class updateCommercialParkingLocationId extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        JSONObject json12 = null;
        String id = "";
        String walleturl = "_table/google_parking_partners";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("id", locationId);
            jsonValues1.put("location_id", locationId);
            try {
                jsonValues1.put("twp_id", Integer.valueOf("100" + locationId));
            } catch (Exception e) {

            }

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   updateCommercialParkingLocationId:  res:  " + responseStr);
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        if (c.has("place_id")) {
                            id = c.getString("place_id");
                        }
                        if (c.has("id")) {
                            id = String.valueOf(c.getInt("id"));
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json12 != null) {
                if (json12.has("error")) {
                    if (MyApplication.getInstance().privateRuleIds.size() != 0) {
                        new update_parking_commercial_rules().execute(
                                MyApplication.getInstance().privateRuleIds.get(0),
                                locationId);
                    } else {
                        if (isNewLocation) {
                            myApp.isLocationAdded = true;
                            getActivity().onBackPressed();
                        } else {
                            fragmentStack.clear();
                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            commercial_parking_main pay = new commercial_parking_main();
                            ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                            ft.add(R.id.My_Container_1_ID, pay);
                            fragmentStack.push(pay);
                            ft.commitAllowingStateLoss();
                        }
                    }
                } else {
                    if (!id.equals("")) {
                        if (MyApplication.getInstance().privateRuleIds.size() != 0) {
                            new update_parking_commercial_rules().execute(
                                    MyApplication.getInstance().privateRuleIds.get(0),
                                    locationId);
                        } else {
                            if (isNewLocation) {
                                myApp.isLocationAdded = true;
                                getActivity().onBackPressed();
                            } else {
                                fragmentStack.clear();
                                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                commercial_parking_main pay = new commercial_parking_main();
                                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                ft.add(R.id.My_Container_1_ID, pay);
                                fragmentStack.push(pay);
                                ft.commitAllowingStateLoss();
                            }
                        }
                    }
                }

            }
            super.onPostExecute(s);
        }
    }

    //update commercial parking rules.....................
    public class update_parking_commercial_rules extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        JSONObject json12 = null;
        String walleturl = "_table/google_parking_rules";
        String id = "";

        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();
            jsonValues1.put("id", params[0]);
            jsonValues1.put("location_id", params[1]);
            jsonValues1.put("twp_id", 100 + (Integer.valueOf(params[0]) - 110));

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json12 != null) {
                if (json12.has("error")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("Error");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                } else {
                    if (!id.equals("")) {
                        isclickeditrules = false;
                        if (parkingType.contains("Managed")) {
                            new addTownship_Managed_location().execute();
                        } else {
                            if (isNewLocation) {
                                myApp.isLocationAdded = true;
                                if (getActivity() != null) {
                                    getActivity().onBackPressed();
                                }
                            } else {
                                fragmentStack.clear();
                                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                commercial_parking_main pay = new commercial_parking_main();
                                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                                ft.add(R.id.My_Container_1_ID, pay);
                                fragmentStack.push(pay);
                                ft.commitAllowingStateLoss();
                            }
                        }

                    }
                }

            }
            super.onPostExecute(s);
        }
    }

    public class addTownship_Managed_location extends AsyncTask<String, String, String> {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        String user_id = logindeatl.getString("id", "null");
        JSONObject json12 = null;
        String id = "";
        String walleturl = "_table/manage_commercial_locations";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            Map<String, Object> jsonValues1 = new HashMap<String, Object>();

            jsonValues1.put("date_time", DateTimeUtils.getSqlFormatDate(new Date()));
            jsonValues1.put("township_code", "");
            jsonValues1.put("location_name", commercial_title);
            jsonValues1.put("location_type", "");
            jsonValues1.put("full_address", commercial_address);
            jsonValues1.put("show_location", "(" + commercial_lat + "," + commercial_lang + "):15");
            jsonValues1.put("location_code", commercial_loc_code);
            jsonValues1.put("marker_type", parkingType);
            jsonValues1.put("total_rows", commercial_rows_total);
            jsonValues1.put("spaces_mark_reqd", true);
            jsonValues1.put("total_spaces", commercial_space_total);
            jsonValues1.put("labeling", township_labeling);
            Manager manager = myApp.getSelectedManager();
            if (manager != null) {
                jsonValues1.put("township_code", manager.getManager_id());
                jsonValues1.put("company_id", manager.getId());
                jsonValues1.put("manager_type_id", manager.getManager_type_id());
                jsonValues1.put("company_type_id", manager.getManager_type_id());
            }
            jsonValues1.put("location_id", locationId);
            if (!commercial_place_id.equals("")) {
                jsonValues1.put("location_place_id", commercial_place_id);
            }

            JSONObject json1 = new JSONObject(jsonValues1);
            String url = getString(R.string.api) + getString(R.string.povlive) + walleturl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                entity = new StringEntity(json1.toString(), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json12 = new JSONObject(responseStr);
                    JSONArray array = json12.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        id = c.getString("id");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            //progressBar.setVisibility(View.GONE);
            if (json12 != null) {
                if (json12.has("error")) {
//                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                    alertDialog.setMessage("Error");
//                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                        }
//                    });
//                    alertDialog.show();
                } else {
                    if (!id.equals("")) {

                        //Calculate and add location_lots
                        LocationLot locationLot = new LocationLot();
                        if (!TextUtils.isEmpty(locationId))
                            locationLot.setLocation_id(Integer.valueOf(locationId));
                        locationLot.setLocation_code(commercial_loc_code);
                        locationLot.setLocation_name(commercial_title);
                        locationLot.setOccupied("NO");
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                        locationLot.setDate_time(sdf.format(new Date(Calendar.getInstance().getTimeInMillis())));
                        calculateLocationLots(commercial_rows_total, commercial_space_total, township_labeling, locationLot);
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    //TODO calculate location lots and prepare list of location lots
    private void calculateLocationLots(String rowsCount, String spaceCount, String township_labeling,
                                       LocationLot locationLotData) {
        locationLots.clear();
        int spaces = Integer.parseInt(spaceCount);
        int rows = Integer.parseInt(rowsCount);
        String[] alpha = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
                "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

        if (township_labeling.equals("Alpha")) {
            if (spaces < alpha.length) {
                for (int j = 0; j < spaces; j++) {
                    LocationLot locationLot = new LocationLot();
                    locationLot.setDate_time(locationLotData.getDate_time());
                    locationLot.setTownship_code(locationLotData.getTownship_code());
                    locationLot.setLocation_id(locationLotData.getLocation_id());
                    locationLot.setLocation_code(locationLotData.getLocation_code());
                    locationLot.setLocation_name(locationLotData.getLocation_name());
                    locationLot.setLot_number(alpha[j]);
                    locationLot.setOccupied("NO");
                    locationLot.setLot_id(alpha[j]);
                    locationLots.add(locationLot);

                }
                Log.e("#DEBUG", "   locationLotsSize:  " + locationLots.size());
            }
        } else if (township_labeling.equals("Numeric")) {
            for (int j = 0; j < spaces; j++) {
                LocationLot locationLot = new LocationLot();
                locationLot.setDate_time(locationLotData.getDate_time());
                locationLot.setTownship_code(locationLotData.getTownship_code());
                locationLot.setLocation_id(locationLotData.getLocation_id());
                locationLot.setLocation_code(locationLotData.getLocation_code());
                locationLot.setLocation_name(locationLotData.getLocation_name());
                locationLot.setLot_number(String.valueOf(j + 1));
                locationLot.setOccupied("NO");
                locationLot.setLot_id(String.valueOf(j + 1));
                locationLots.add(locationLot);

            }
            Log.e("#DEBUG", "   locationLotsSize:  " + locationLots.size());
        } else if (township_labeling.equals("AlphaNumeric")) {
            int inEachRowSpaces = spaces / rows;
            if (rows < alpha.length) {
                for (int j = 0; j < rows; j++) {
                    for (int k = 0; k < inEachRowSpaces; k++) {
                        LocationLot locationLot = new LocationLot();
                        locationLot.setDate_time(locationLotData.getDate_time());
                        locationLot.setTownship_code(locationLotData.getTownship_code());
                        locationLot.setLocation_id(locationLotData.getLocation_id());
                        locationLot.setLocation_code(locationLotData.getLocation_code());
                        locationLot.setLocation_name(locationLotData.getLocation_name());
                        locationLot.setLot_row(alpha[j]);
                        locationLot.setLot_number(String.valueOf(k + 1));
                        locationLot.setLot_id(alpha[j] + ":" + (k + 1));
                        locationLot.setOccupied("NO");
                        locationLots.add(locationLot);
                    }
                }
            }
        } else if (township_labeling.equals("AlphaAlpha")) {
            int inEachRowSpacesAlpha = spaces / rows;
            if (rows < alpha.length + 1) {
                for (int j = 0; j < rows; j++) {
                    for (int k = 0; k < inEachRowSpacesAlpha; k++) {
                        LocationLot locationLot = new LocationLot();
                        locationLot.setDate_time(locationLotData.getDate_time());
                        locationLot.setTownship_code(locationLotData.getTownship_code());
                        locationLot.setLocation_id(locationLotData.getLocation_id());
                        locationLot.setLocation_code(locationLotData.getLocation_code());
                        locationLot.setLocation_name(locationLotData.getLocation_name());
                        locationLot.setLot_row(alpha[j]);
                        locationLot.setLot_number(alpha[k]);
                        locationLot.setLot_id(String.format("%s:%s", alpha[j], alpha[k]));
                        locationLots.add(locationLot);
                    }
                }
            }
        } else if (township_labeling.equals("NumericNumeric")) {
            int inEachRowSpacesNumeric = spaces / rows;
            if (rows < alpha.length + 1) {
                for (int j = 0; j < rows; j++) {
                    for (int k = 0; k < inEachRowSpacesNumeric; k++) {
                        LocationLot locationLot = new LocationLot();
                        locationLot.setDate_time(locationLotData.getDate_time());
                        locationLot.setTownship_code(locationLotData.getTownship_code());
                        locationLot.setLocation_id(locationLotData.getLocation_id());
                        locationLot.setLocation_code(locationLotData.getLocation_code());
                        locationLot.setLocation_name(locationLotData.getLocation_name());
                        locationLot.setLot_row(String.valueOf(j + 1));
                        locationLot.setLot_number(String.valueOf(k + 1));
                        locationLot.setLot_id(String.format("%s:%s", String.valueOf(j + 1), String.valueOf(k + 1)));
                        locationLots.add(locationLot);
                    }
                }
            }
        }

        if (locationLots.size() != 0) {
            new addLocationLots().execute();
        }

    }

    //TODO add location lots
    private class addLocationLots extends AsyncTask<String, String, String> {

        String locationLotUrl = "_table/location_lot_commercial";
        JSONObject json, json1;
        String re_id;

        @Override
        protected void onPreExecute() {
            Log.e("#DEBUG", "  addLocationLots:  onPre: Size: " + locationLots.size());
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {

            String url = getString(R.string.api) + getString(R.string.povlive) + locationLotUrl;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.setHeader("X-DreamFactory-Application-Name", "parkezly");
            post.setHeader("X-DreamFactory-Api-Key", getResources().getString(R.string.headerkeyvalue2));
            StringEntity entity = null;
            try {
                Log.e("#DEBUG", "   body:  location_lots: " + new Gson().toJson(locationLots.get(0)));
                entity = new StringEntity(new Gson().toJson(locationLots.get(0)), "UTF8");
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(entity);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("#DEBUG", "  addLocationLots:  Error:  " + e.getMessage());
            }
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("#DEBUG", "  addLocationLots:  Error:  " + e.getMessage());
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("#DEBUG", "   addLocationLot:  Response:    " + responseStr);
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "  addLocationLots:  Error:  " + e.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("#DEBUG", "  addLocationLots:  Error:  " + e.getMessage());
                }
                System.out.println("Response: " + response.getStatusLine());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("#DEBUG", "   onPost: locationLots: size" + locationLots.size());
            if (locationLots.size() != 0) {
                locationLots.remove(0);
                if (locationLots.size() != 0) {
                    new addLocationLots().execute();
                } else {
                    rl_progressbar.setVisibility(View.GONE);
                    if (isNewLocation) {
                        myApp.isLocationAdded = true;
                        if (getActivity() != null) {
                            getActivity().onBackPressed();
                        }
                    } else {
                        fragmentStack.clear();
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                        commercial_parking_main pay = new commercial_parking_main();
                        ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                        ft.add(R.id.My_Container_1_ID, pay);
                        fragmentStack.push(pay);
                        ft.commitAllowingStateLoss();
                        Log.e("#DEBUG", "   onPost addLocationCompleted! ");
                    }
                }
            } else {
                rl_progressbar.setVisibility(View.GONE);
                if (isNewLocation) {
                    myApp.isLocationAdded = true;
                    if (getActivity() != null) {
                        getActivity().onBackPressed();
                    }
                } else {
                    fragmentStack.clear();
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    commercial_parking_main pay = new commercial_parking_main();
                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                    Log.e("#DEBUG", "   onPost addLocationCompleted! ");
                }
            }
        }
    }
}
