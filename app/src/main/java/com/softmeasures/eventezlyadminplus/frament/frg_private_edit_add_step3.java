package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseFragment;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.softmeasures.eventezlyadminplus.activity.vchome.fragmentStack;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step1.Private_location_code;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.cancellationCharge;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.custom_notice;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.duation_unit;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.end_time;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.isCancellationAllowed;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.isCountReservedSpot;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.isPostPaymentAllowed;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.isPrePaymentRequired;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.isReservationAllowed;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.isVerifyLotAvailability;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.marker_type;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.max_duation;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.parking_rules_id;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.postPayLateFees;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.postPayLateFeesReservation;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.postPayTerm;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.postPayTermReservation;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.pricing;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.pricing_duation;
import static com.softmeasures.eventezlyadminplus.frament.frg_private_edit_add_step4.start_timer;

public class frg_private_edit_add_step3 extends BaseFragment {

    TextView txt_parking_rules_add, txt_sp3_private_next;
    RelativeLayout rl_progressbar;
    ArrayList<item> parking_rules_array = new ArrayList<>();
    ListView list_rules;
    public static String parking_rules_location_code;
    public static boolean isclickeditrules = false;
    private RelativeLayout rl_main;
    private String township_type_sp = "";
    private boolean isNewLocation = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getArguments() != null) {
            township_type_sp = getArguments().getString("township_type_sp");
            isNewLocation = getArguments().getBoolean("isNewLocation", false);
        }
        return inflater.inflate(R.layout.parking_rules_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void setListeners() {
        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        txt_parking_rules_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                frg_private_edit_add_step4 pay = new frg_private_edit_add_step4();
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();
            }
        });


        list_rules.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                isclickeditrules = true;
                max_duation = parking_rules_array.get(position).getMax_time();
                duation_unit = parking_rules_array.get(position).getDuration_unit();
                pricing = parking_rules_array.get(position).getPricing();
                pricing_duation = parking_rules_array.get(position).getPricing_duration();
                start_timer = parking_rules_array.get(position).getStart_time();
                end_time = parking_rules_array.get(position).getEnd_time();
                custom_notice = parking_rules_array.get(position).getCustom_notice();
                parking_rules_location_code = parking_rules_array.get(position).getLocation_code();
                marker_type = parking_rules_array.get(position).getMarker_type();
                parking_rules_id = parking_rules_array.get(position).getId();
                isReservationAllowed = parking_rules_array.get(position).isReservationAllowed();
                isPrePaymentRequired = parking_rules_array.get(position).isPrePymntReqd_for_Reservation();
                isPostPaymentAllowed = parking_rules_array.get(position).isParkNow_PostPaymentAllowed();
                isCancellationAllowed = parking_rules_array.get(position).isCanCancel_Reservation();
                isVerifyLotAvailability = parking_rules_array.get(position).isBefore_reserve_verify_lot_avbl();
                isCountReservedSpot = parking_rules_array.get(position).isInclude_reservations_for_avbl_calc();
                postPayTermReservation = parking_rules_array.get(position).getReservation_PostPayment_term();
                postPayTerm = parking_rules_array.get(position).getParkNow_PostPayment_term();
                postPayLateFees = parking_rules_array.get(position).getParkNow_PostPayment_LateFee();
                postPayLateFeesReservation = parking_rules_array.get(position).getReservation_PostPayment_LateFee();
                cancellationCharge = parking_rules_array.get(position).getCancellation_Charge();

                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                frg_private_edit_add_step4 pay = new frg_private_edit_add_step4();
                ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                ft.add(R.id.My_Container_1_ID, pay);
                fragmentStack.lastElement().onPause();
                ft.hide(fragmentStack.lastElement());
                fragmentStack.push(pay);
                ft.commitAllowingStateLoss();

            }
        });

        txt_sp3_private_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parking_rules_array.size() > 0) {
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    frg_private_edit_add_step5 pay = new frg_private_edit_add_step5();
                    Bundle bundle = getArguments();
                    if (bundle == null) bundle = new Bundle();
                    bundle.putBoolean("isNewLocation", isNewLocation);
                    pay.setArguments(bundle);
                    ft.setCustomAnimations(R.anim.sidepannelright, R.anim.sidepannelright);
                    ft.add(R.id.My_Container_1_ID, pay);
                    fragmentStack.lastElement().onPause();
                    ft.hide(fragmentStack.lastElement());
                    fragmentStack.push(pay);
                    ft.commitAllowingStateLoss();
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage("No parking rule added, please add at least one rule for parking");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alertDialog.show();
                }

            }
        });
    }

    @Override
    protected void initViews(View view) {
        txt_parking_rules_add = (TextView) view.findViewById(R.id.txt_add_parking_rules);
        rl_progressbar = (RelativeLayout) view.findViewById(R.id.rl_progressbar);
        list_rules = (ListView) view.findViewById(R.id.list_parking_rule);
        txt_sp3_private_next = (TextView) view.findViewById(R.id.txt_sp3_private);
        rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
    }

    //get other private parking rules with api
    public class getparking_rules extends AsyncTask<String, String, String> {
        String location;
        int i = 0;
        JSONObject json;
        JSONArray json1;
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        // http://108.30.248.212:8006/api/v2/pzly01live7/_table/user_locations?filter=user_id%3D5%20AND%20location_code%3DNY-NHP-03
        String id = logindeatl.getString("id", "null");
        String parkingurl = "";

        public getparking_rules(String loc) {
            this.location = loc;
            try {
                this.parkingurl = "_table/other_parking_rules?filter=location_code%3D" + URLEncoder.encode(loc, "utf-8") + "";
            } catch (Exception e) {
                e.printStackTrace();
            }
            //  this.parkingurl = "_table/other_parking_rules?filter=location_code%3D" + location + "";
        }


        @Override
        protected void onPreExecute() {
            rl_progressbar.setVisibility(View.VISIBLE);

            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            parking_rules_array.clear();
            String url = getString(R.string.api) + getString(R.string.povlive) + parkingurl;
            Log.e("parking_url", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                String responseStr = null;
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        item item1 = new item();
                        item1.setLocation_name(c.getString("location_name"));
                        item1.setLoationname(c.getString("location_name"));
                        item1.setTime_rule(c.getString("time_rule"));
                        item1.setPricing_duration(c.getString("pricing_duration"));
                        item1.setDuration_unit(c.getString("duration_unit"));
                        item1.setMax_time(c.getString("max_duration"));
                        item1.setCustom_notice(c.getString("custom_notice"));
                        String time_rules = c.getString("time_rule");

                        item1.setPricing(c.getString("pricing"));
                        item1.setId(c.getString("id"));
                        //item1.setMarker_type(c.getString("marker_type"));
                        item1.setLocation_code(c.getString("location_code"));
                        if (time_rules.contains("-")) {
                            String start_time = time_rules.substring(0, time_rules.indexOf("-"));
                            String end_time = time_rules.substring(time_rules.indexOf("-") + 1);
                            item1.setStart_time(start_time);
                            item1.setEnd_time(end_time);
                        } else {
                            item1.setStart_time("null");
                            item1.setEnd_time("null");
                        }

                        if (c.has("ReservationAllowed")
                                && !TextUtils.isEmpty(c.getString("ReservationAllowed"))
                                && !c.getString("ReservationAllowed").equals("null")) {
                            item1.setReservationAllowed(c.getBoolean("ReservationAllowed"));
                        }

                        if (c.has("PrePymntReqd_for_Reservation")
                                && !TextUtils.isEmpty(c.getString("PrePymntReqd_for_Reservation"))
                                && !c.getString("PrePymntReqd_for_Reservation").equals("null")) {
                            item1.setPrePymntReqd_for_Reservation(c.getBoolean("PrePymntReqd_for_Reservation"));
                        }

                        if (c.has("CanCancel_Reservation")
                                && !TextUtils.isEmpty(c.getString("CanCancel_Reservation"))
                                && !c.getString("CanCancel_Reservation").equals("null")) {
                            item1.setCanCancel_Reservation(c.getBoolean("CanCancel_Reservation"));
                        }

                        if (c.has("Cancellation_Charge")
                                && !TextUtils.isEmpty(c.getString("Cancellation_Charge"))
                                && !c.getString("Cancellation_Charge").equals("null")) {
                            item1.setCancellation_Charge(c.getString("Cancellation_Charge"));
                        }

                        if (c.has("Reservation_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_term"))
                                && !c.getString("Reservation_PostPayment_term").equals("null")) {
                            item1.setReservation_PostPayment_term(c.getString("Reservation_PostPayment_term"));
                        }

                        if (c.has("Reservation_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("Reservation_PostPayment_LateFee"))
                                && !c.getString("Reservation_PostPayment_LateFee").equals("null")) {
                            item1.setReservation_PostPayment_LateFee(c.getString("Reservation_PostPayment_LateFee"));
                        }

                        if (c.has("ParkNow_PostPaymentAllowed")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPaymentAllowed"))
                                && !c.getString("ParkNow_PostPaymentAllowed").equals("null")) {
                            item1.setParkNow_PostPaymentAllowed(c.getBoolean("ParkNow_PostPaymentAllowed"));
                        }

                        if (c.has("ParkNow_PostPayment_term")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_term"))
                                && !c.getString("ParkNow_PostPayment_term").equals("null")) {
                            item1.setParkNow_PostPayment_term(c.getString("ParkNow_PostPayment_term"));
                        }

                        if (c.has("before_reserve_verify_lot_avbl")
                                && !TextUtils.isEmpty(c.getString("before_reserve_verify_lot_avbl"))
                                && !c.getString("before_reserve_verify_lot_avbl").equals("null")) {
                            item1.setBefore_reserve_verify_lot_avbl(c.getBoolean("before_reserve_verify_lot_avbl"));
                        }

                        if (c.has("include_reservations_for_avbl_calc")
                                && !TextUtils.isEmpty(c.getString("include_reservations_for_avbl_calc"))
                                && !c.getString("include_reservations_for_avbl_calc").equals("null")) {
                            item1.setInclude_reservations_for_avbl_calc(c.getBoolean("include_reservations_for_avbl_calc"));
                        }

                        if (c.has("ParkNow_PostPayment_LateFee")
                                && !TextUtils.isEmpty(c.getString("ParkNow_PostPayment_LateFee"))
                                && !c.getString("ParkNow_PostPayment_LateFee").equals("null")) {
                            item1.setParkNow_PostPayment_LateFee(c.getString("ParkNow_PostPayment_LateFee"));
                        }

                        parking_rules_array.add(item1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null) {
                if (json != null) {
                    if (parking_rules_array.size() > 0) {
                        Log.e("#DEBUG", "   onPostRules:  " + new Gson().toJson(parking_rules_array));
                        list_rules.setAdapter(new CustomAdapterRules(getActivity(), parking_rules_array, getActivity().getResources()));

                    }
                }
            }
            super.onPostExecute(s);
        }
    }

    public class CustomAdapterRules extends BaseAdapter implements View.OnClickListener {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        public Resources res;
        item tempValues = null;
        Context context;
        item state;

        public CustomAdapterRules(Activity a, ArrayList<item> d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            context = a;
            data = d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {

            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            public TextView txt_title;
            ImageView IMG;
            private TextView tvRulePricing, tvRuleMaxParkingAllowed, tvRuleTime;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            final ViewHolder holder;


            if (convertView == null) {

                vi = inflater.inflate(R.layout.rules_adapter, null);
                /****** View Holder Object to contain tabitem.xml file elements ******/

                holder = new ViewHolder();
                holder.txt_title = (TextView) vi.findViewById(R.id.txt_location_name);
                holder.tvRulePricing = vi.findViewById(R.id.tvRulePricing);
                holder.tvRuleMaxParkingAllowed = vi.findViewById(R.id.tvRuleMaxParkingAllowed);
                holder.tvRuleTime = vi.findViewById(R.id.tvRuleTime);


                /************  Set holder with LayoutInflater ************/
                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();
            try {
                state = data.get(position);
            } catch (Exception e) {
            }

            if (data.size() <= 0) {
                vi.setVisibility(View.GONE);

            } else {
                vi.setVisibility(View.VISIBLE);
                /***** Get each Model object from Arraylist ********/
                tempValues = null;
                tempValues = (item) data.get(position);
                int k = 0;
                /************  Set Model values in Holder elements ***********/
                String cat = tempValues.getAddress();

                if (tempValues.getLoationname().equals("null") || tempValues.getLoationname().equals("")) {
                    holder.txt_title.setText("private_parking_title");
                } else {
                    holder.txt_title.setText(tempValues.getLoationname());
                }
                String time;
                int duration;
                String trails = null;
                if (!tempValues.getMax_time().equals("null") || !tempValues.getMax_time().equals("") || !tempValues.getMax_time().equals(null)) {
                    time = tempValues.getMax_time();
                    duration = Integer.parseInt(time);
                    trails = (duration < 2) ? "" : "s";
                } else {
                    time = "";
                    trails = "";

                }
                holder.tvRulePricing.setText(String.format("$%s/%s %s", tempValues.getPricing(),
                        tempValues.getPricing_duration(), tempValues.getDuration_unit()));
                holder.tvRuleMaxParkingAllowed.setText(String.format("MAX: %s %s",
                        tempValues.getMax_time(), tempValues.getDuration_unit()));
//                holder.txt_address.setText("$" + tempValues.getPricing_duration() + " @ " + time + tempValues.getDuration_unit() + trails);
                holder.tvRuleTime.setText(String.format("OPEN: %s", tempValues.getTime_rule()));


            }
            return vi;
        }

        @Override
        public void onClick(View v) {
            Log.v("CustomAdapter", "=====Row button clicked=====");
        }
    }

    @Override
    public void onResume() {
        if (myApp.isLocationAdded) {
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
        } else {
            new getparking_rules(Private_location_code).execute();
        }
        super.onResume();
    }
}
