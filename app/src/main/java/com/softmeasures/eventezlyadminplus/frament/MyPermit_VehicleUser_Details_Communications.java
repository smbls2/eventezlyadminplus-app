package com.softmeasures.eventezlyadminplus.frament;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.java.ConnectionDetector;
import com.softmeasures.eventezlyadminplus.java.item;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;

public class MyPermit_VehicleUser_Details_Communications extends Fragment {

    View mView;
    ListView lv_Communication;
    ProgressBar progressBar;
    RelativeLayout rl_progressbar;
    ConnectionDetector cd;
    Context mContext;
    String id;
    ArrayList<item> itemArrayList;
    CustomAdaptercity adpater;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_mypermit_communications, container, false);
        mContext = getContext();
        cd = new ConnectionDetector(mContext);

        init();

        if (getArguments() != null) {
            id = getArguments().getString("id");
        }

        new get_Communications_thread().execute();

        return mView;
    }

    public void init() {
        progressBar = (ProgressBar) mView.findViewById(R.id.progressbar);
        rl_progressbar = (RelativeLayout) mView.findViewById(R.id.rl_progressbar);
        lv_Communication = (ListView) mView.findViewById(R.id.lv_Communication);
    }

    public class get_Communications_thread extends AsyncTask<String, String, String> {
        int i = 0;
        JSONObject json;
        String responseStr = null;
        String managedlosturl = "_table/communications_thread";

        {
            try {
                managedlosturl = "_table/communications_thread?filter=permit_subscription_id%3D'" + URLEncoder.encode(id, "utf8") + "'";
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            itemArrayList = new ArrayList<item>();
            rl_progressbar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... params) {
            String url = getString(R.string.api) + getString(R.string.povlive) + managedlosturl;
            Log.e("url", url);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            post.setHeader(getString(R.string.headerkey1), getString(R.string.headerkeyvalue1));
            post.setHeader(getString(R.string.headerkey2), getString(R.string.headerkeyvalue2));

            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.e("responseStr ", " : " + responseStr);
                    json = new JSONObject(responseStr);
                    JSONArray array = json.getJSONArray("resource");
                    for (int i = 0; i < array.length(); i++) {
                        item item = new item();
                        JSONObject c = array.getJSONObject(i);
                        item.setId(c.getString("id"));
                        item.setDate_time(c.getString("date_time"));
                        item.setUser_id(c.getString("user_id"));
                        item.setUser_name(c.getString("user_name"));
                        item.setEmail(c.getString("email"));
                        item.setPhone(c.getString("phone"));
                        item.setPermit_subscription_id(c.getString("permit_subscription_id"));
                        item.setProfile_name(c.getString("profile_name"));
                        item.setProfile_type(c.getString("profile_type"));
                        item.setComments(c.getString("comments"));

                        if (c.getString("profile_type").equals("Registered")) {
                            item.setBgColor("#E6BFB6");
                        } else {
                            item.setBgColor("#FDD835");
                        }
                        itemArrayList.add(item);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return responseStr;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (rl_progressbar != null)
                rl_progressbar.setVisibility(View.GONE);
            if (getActivity() != null && json != null) {
                Collections.reverse(itemArrayList);
                adpater = new CustomAdaptercity(getActivity(), itemArrayList);
                lv_Communication.setAdapter(adpater);
            }
        }
    }

    public class CustomAdaptercity extends BaseAdapter {
        private Activity activity;
        private ArrayList<item> data;
        private LayoutInflater inflater = null;
        item tempValues = null;
        Context context;

        public CustomAdaptercity(Activity a, ArrayList<item> d) {
            activity = a;
            context = a;
            data = d;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (data.size() <= 0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        public class ViewHolder {
            TextView txt_Name, txt_Message, txt_Times;
            RelativeLayout rl_main;

            public ViewHolder(View convertView) {
                rl_main = (RelativeLayout) convertView.findViewById(R.id.rl_main);
                txt_Name = (TextView) convertView.findViewById(R.id.txt_Name);
                txt_Message = (TextView) convertView.findViewById(R.id.txt_Message);
                txt_Times = (TextView) convertView.findViewById(R.id.txt_Times);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.adapter_my_communications, parent, false);
                holder = new CustomAdaptercity.ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if (data.size() <= 0) {
                holder.rl_main.setVisibility(View.GONE);
            } else {
                tempValues = (item) data.get(position);

                if (tempValues.getProfile_type().equalsIgnoreCase("Registered")) {
                    holder.txt_Name.setText("User: ");
                } else {
                    holder.txt_Name.setText("Official: ");
                }

                holder.txt_Message.setText(tempValues.getComments());
                holder.txt_Times.setText(tempValues.getDate_time());

                holder.rl_main.setBackgroundColor(Color.parseColor(tempValues.getBgColor()));

            }

            return convertView;
        }

    }


}
