package com.softmeasures.eventezlyadminplus.frament.g_youtube;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.RestrictionsManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.UserManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.activity.TransferActivity;
import com.softmeasures.eventezlyadminplus.common.BaseActivity;
import com.softmeasures.eventezlyadminplus.databinding.ActivityGYoutubeMainBinding;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationRequest;
import net.openid.appauth.AuthorizationResponse;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.AuthorizationServiceConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class GYoutubeMainActivity extends BaseActivity {

    private ActivityGYoutubeMainBinding binding;

    private static final String SHARED_PREFERENCES_NAME = "AuthStatePreference";
    private static final String AUTH_STATE = "AUTH_STATE";
    private static final String USED_INTENT = "USED_INTENT";
    private static final String LOGIN_HINT = "login_hint";

    protected String mLoginHint;
    BroadcastReceiver mRestrictionsReceiver;
    AuthState mAuthState;

    private ProgressDialog progressDialog;
    public static boolean authExpired = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_g_youtube_main);

        initViews();
        updateViews();
        setListeners();

        enablePostAuthorizationFlows();
        getAppRestrictions();

        refreshToken();
    }

    @Override
    protected void updateViews() {

    }

    @Override
    protected void onResume() {
        super.onResume();

        // Retrieve app restrictions and take appropriate action
        getAppRestrictions();

        // Register a receiver for app restrictions changed broadcast
        registerRestrictionsReceiver();

        if (authExpired) {
            authExpired = false;
            binding.btnLogout.performClick();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Unregister receiver for app restrictions changed broadcast
        unregisterReceiver(mRestrictionsReceiver);
    }

    @Override
    protected void setListeners() {
        binding.rlBtnBack.setOnClickListener(view -> {
            onBackPressed();
        });

        binding.btnAuth.setOnClickListener(view -> {
            auth();

        });

        binding.btnLogout.setOnClickListener(view -> {
            myApp.setOAuth(null);
            myApp.setOAuthRefreshToken(null);
            mAuthState = null;
            clearAuthState();
            enablePostAuthorizationFlows();
        });

        binding.btnManage.setOnClickListener(view -> {
            startActivity(new Intent(activity, TransferActivity.class)
                    .putExtra("type", "youtubeLiveStreams"));
        });

        binding.btnSchedule.setOnClickListener(view -> {
            startActivity(new Intent(activity, TransferActivity.class)
                    .putExtra("type", "youtubeScheduleStream"));
        });
    }

    private void clearAuthState() {
        getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
                .edit()
                .remove(AUTH_STATE)
                .apply();
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkIntent(getIntent());

        // Register a receiver for app restrictions changed broadcast
        registerRestrictionsReceiver();
    }

    private void registerRestrictionsReceiver() {
        IntentFilter restrictionsFilter =
                new IntentFilter(Intent.ACTION_APPLICATION_RESTRICTIONS_CHANGED);

        mRestrictionsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getAppRestrictions();
            }
        };

        registerReceiver(mRestrictionsReceiver, restrictionsFilter);
    }

    private void getAppRestrictions() {
        RestrictionsManager restrictionsManager =
                (RestrictionsManager)
                        getSystemService(Context.RESTRICTIONS_SERVICE);

        Bundle appRestrictions = restrictionsManager.getApplicationRestrictions();

        // Block user if KEY_RESTRICTIONS_PENDING is true, and save login hint if available
        if (!appRestrictions.isEmpty()) {
            if (appRestrictions.getBoolean(UserManager.
                    KEY_RESTRICTIONS_PENDING) != true) {
                mLoginHint = appRestrictions.getString(LOGIN_HINT);
            } else {
                Toast.makeText(activity, R.string.restrictions_pending_block_user,
                        Toast.LENGTH_LONG).show();
                onBackPressed();
            }
        }
    }

    private void handleAuthorizationResponse(@NonNull Intent intent) {
        progressDialog.show();
        AuthorizationResponse response = AuthorizationResponse.fromIntent(intent);
        AuthorizationException error = AuthorizationException.fromIntent(intent);
        final AuthState authState = new AuthState(response, error);
        if (response != null) {
            Log.e("#DEBUG", String.format("Handled Authorization Response %s ", authState.toJsonString()));
            AuthorizationService service = new AuthorizationService(activity);
            service.performTokenRequest(response.createTokenExchangeRequest(), (tokenResponse, exception) -> {
                progressDialog.dismiss();
                if (exception != null) {
                    Log.e("#DEBUG", "Token Exchange failed", exception);
                    Toast.makeText(activity, "Authentication Failed, Please Try again!", Toast.LENGTH_SHORT).show();
                } else {
                    if (tokenResponse != null) {
                        myApp.setOAuth(tokenResponse.accessToken);
                        myApp.setOAuthRefreshToken(tokenResponse.refreshToken);
                        authState.update(tokenResponse, exception);
                        persistAuthState(authState);
                        Log.e("#DEBUG", String.format("Token Response [ Access Token: %s, ID Token: %s ]", tokenResponse.accessToken, tokenResponse.idToken));
                    }
                }
            });
        }
    }

    private void persistAuthState(@NonNull AuthState authState) {
        getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).edit()
                .putString(AUTH_STATE, authState.toJsonString())
                .commit();
        enablePostAuthorizationFlows();
    }

    private void enablePostAuthorizationFlows() {
        mAuthState = restoreAuthState();
        if (mAuthState != null && mAuthState.isAuthorized()) {
            Log.e("#DEBUG", "   isAuthorized: True");
            binding.llMenu.setVisibility(View.VISIBLE);
            binding.btnAuth.setVisibility(View.GONE);
        } else {
            Log.e("#DEBUG", "   isAuthorized: False");
            binding.llMenu.setVisibility(View.GONE);
            binding.btnAuth.setVisibility(View.VISIBLE);
        }
    }

    @Nullable
    private AuthState restoreAuthState() {
        String jsonString = getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
                .getString(AUTH_STATE, null);
        if (!TextUtils.isEmpty(jsonString)) {
            try {
                return AuthState.fromJson(jsonString);
            } catch (JSONException jsonException) {
                // should never happen
            }
        }
        return null;
    }

    private void refreshToken() {
        AndroidNetworking.post("https://oauth2.googleapis.com/token")
                .addQueryParameter("client_id", "967652187664-jn7qc2ikv0vk35uhv0tcmta7f7j6tgcm.apps.googleusercontent.com")
                .addQueryParameter("client_secret", "")
                .addQueryParameter("refresh_token", myApp.getOAuthRefresh())
                .addQueryParameter("grant_type", "refresh_token")
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("#DEBUG", "  refreshToken: onResponse: " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has("access_token")) {
                                myApp.setOAuth(jsonObject.getString("access_token"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("#DEBUG", "  refreshToken: anError: " + anError.getErrorBody());
                    }
                });
    }

    private void auth() {
        AuthorizationServiceConfiguration serviceConfiguration = new AuthorizationServiceConfiguration(
                Uri.parse("https://accounts.google.com/o/oauth2/v2/auth") /* auth endpoint */,
                Uri.parse("https://www.googleapis.com/oauth2/v4/token") /* token endpoint */
        );
        AuthorizationService authorizationService = new AuthorizationService(activity);
        String clientId = "967652187664-jn7qc2ikv0vk35uhv0tcmta7f7j6tgcm.apps.googleusercontent.com";
//        Uri redirectUri = Uri.parse("com.google.codelabs.appauth:/oauth2callback");
        Uri redirectUri = Uri.parse("com.softmeasures.eventezlyadminplus:/oauth2callback");
        AuthorizationRequest.Builder builder = new AuthorizationRequest.Builder(
                serviceConfiguration,
                clientId,
                AuthorizationRequest.RESPONSE_TYPE_CODE,
                redirectUri
        );
        builder.setScopes("https://www.googleapis.com/auth/youtube");

        if (getLoginHint() != null) {
            Map loginHintMap = new HashMap<String, String>();
            loginHintMap.put(LOGIN_HINT, getLoginHint());
            builder.setAdditionalParameters(loginHintMap);

            Log.e("#DEBUG", String.format("login_hint: %s", getLoginHint()));
        }

        AuthorizationRequest request = builder.build();
        String action = "com.softmeasures.eventezlyadminplus.HANDLE_AUTHORIZATION_RESPONSE_YOUTUBE";
        Intent postAuthorizationIntent = new Intent(action);
        PendingIntent pendingIntent = PendingIntent.getActivity(activity, request.hashCode(), postAuthorizationIntent, 0);
        authorizationService.performAuthorizationRequest(request, pendingIntent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        checkIntent(intent);
    }

    public void checkIntent(@Nullable Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            if (action != null) {
                switch (action) {
                    case "com.softmeasures.eventezlyadminplus.HANDLE_AUTHORIZATION_RESPONSE_YOUTUBE":
                        if (!intent.hasExtra(USED_INTENT)) {
                            handleAuthorizationResponse(intent);
                            intent.putExtra(USED_INTENT, true);
                        }
                        break;
                    default:
                        // do nothing
                }
            }
        }
    }

    public String getLoginHint() {
        return mLoginHint;
    }

    @Override
    protected void initViews() {

        progressDialog = new ProgressDialog(activity);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");

    }
}