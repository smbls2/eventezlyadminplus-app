package com.softmeasures.eventezlyadminplus.dialogs;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.softmeasures.eventezlyadminplus.R;

public class PaymentOptionDialog extends BottomSheetDialogFragment {

    private Button btnPayNow, btnPayLater, btnWallet, btnPayment;
    private LinearLayout llPayOptions;
    private boolean isPayLaterAllowed = true;
    private PaymentOptionListener listener;
    private String userId;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (PaymentOptionListener) getParentFragment();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static PaymentOptionDialog newInstance() {
        return new PaymentOptionDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        SharedPreferences logindeatl = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        userId = logindeatl.getString("id", "null");

        if (getArguments() != null) {
            isPayLaterAllowed = getArguments().getBoolean("mIsPayLater", false);
            Log.e("#DEBUG", "   PaymentOptionDialog:  isPayLaterAllowed:  " + isPayLaterAllowed);
        }

        return inflater.inflate(R.layout.dialog_payment_options, container,
                false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        updateViews();
        setListeners();
    }

    private void setListeners() {
        btnPayNow.setOnClickListener(view -> {

        });

        btnPayLater.setOnClickListener(view -> {
            getDialog().dismiss();
            showPayLaterConfirmation();
        });

        btnWallet.setOnClickListener(view -> {
            if (listener != null) {
                getDialog().dismiss();
                listener.onWalletClick();
            }
        });

        btnPayment.setOnClickListener(view -> {
            if (listener != null) {
                getDialog().dismiss();
                listener.onPaymentClick();
            }
        });
    }

    private void showPayLaterConfirmation() {
        if (listener != null) {
            listener.onPayLaterClick();
        }
    }

    private void updateViews() {

        if (isPayLaterAllowed) {
            btnPayLater.setVisibility(View.VISIBLE);
            btnPayNow.setVisibility(View.VISIBLE);
        } else {
            btnPayLater.setVisibility(View.GONE);
            btnPayNow.setVisibility(View.GONE);
        }

    }

    private void initViews(View view) {
        btnPayNow = view.findViewById(R.id.btnPayNow);
        btnPayLater = view.findViewById(R.id.btnPayLater);
        btnWallet = view.findViewById(R.id.btnWallet);
        btnPayment = view.findViewById(R.id.btnPayment);
        llPayOptions = view.findViewById(R.id.llPayOptions);
    }
}