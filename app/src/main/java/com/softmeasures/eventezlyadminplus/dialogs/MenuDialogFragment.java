package com.softmeasures.eventezlyadminplus.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.databinding.FragmentMenuDialogBinding;

public class MenuDialogFragment extends DialogFragment {

    FragmentMenuDialogBinding binding;
    public static final String TAG = "#DEBUG MenuDialog";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_menu_dialog, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}