package com.softmeasures.eventezlyadminplus.dialogs;

public interface PaymentOptionListener {
    void onWalletClick();

    void onPaymentClick();

    void onPayLaterClick();
}
