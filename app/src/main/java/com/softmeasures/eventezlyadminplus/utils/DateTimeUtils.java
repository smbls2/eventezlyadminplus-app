package com.softmeasures.eventezlyadminplus.utils;

import android.text.format.DateUtils;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by CSS on 25-11-2017.
 */

public class DateTimeUtils {

    public static String parseDateTime(String dateString, String originalFormat, String outputFromat) {

        SimpleDateFormat formatter = new SimpleDateFormat(originalFormat, Locale.US);
        Date date = null;
        try {
            date = formatter.parse(dateString);

            SimpleDateFormat dateFormat = new SimpleDateFormat(outputFromat, new Locale("US"));

            return dateFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static Date getDateFromString(String dateString) {
        Date date = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date getDateFromStringAMPM(String dateString) {
        Date date = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd h:mm a", Locale.getDefault());
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date getDateFromStringHH_AM_PM(String dateString) {
        Date date = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("h a", Locale.getDefault());
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date getDateFromString_DD_HH_AM_PM(String dateString) {
        Date date = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd h:mm a", Locale.getDefault());
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date getDateFromStringYMD(String dateString) {
        Date date = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String parseDateTimeNew(String dateString) {

        String originalFormat = "yyyy-MM-dd HH:mm:ss";
        String outputFromat = "MM-dd-yyyy kk:mm";
        SimpleDateFormat formatter = new SimpleDateFormat(originalFormat, Locale.US);
        Date date = null;
        try {
            date = formatter.parse(dateString);

            SimpleDateFormat dateFormat = new SimpleDateFormat(outputFromat, new Locale("US"));

            return dateFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getSqlFormatDate(Date date) {
        String originalFormat = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat formatter = new SimpleDateFormat(originalFormat, Locale.getDefault());
        return formatter.format(date);
    }

    public static String formatDateToString(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
        String date_ = format.format(date);
        return date_;
    }

    public static String formatCurrentDateToStringWithTime() {
        SimpleDateFormat format = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
        String date_ = format.format(new Date());
        return date_;
    }

    public static Date formatStringToDate(String date) throws ParseException {
        return new SimpleDateFormat("dd MMM, yyyy hh:mm a").parse(date);
    }

    public static String getDateFromDateTime(Date dateString) {
        SimpleDateFormat format = new SimpleDateFormat("dd");
        String date_ = format.format(dateString);
        return date_;
    }

    public static String getRelativeTimeSpan(String dateString, String originalFormat) {

        SimpleDateFormat formatter = new SimpleDateFormat(originalFormat, Locale.US);
        Date date = null;
        try {
            date = formatter.parse(dateString);

            return DateUtils.getRelativeTimeSpanString(date.getTime()).toString();

        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static ArrayList<String> getDates(String dateString1, String dateString2) {
        ArrayList<Date> dates = new ArrayList<Date>();
        ArrayList<String> stringDates = new ArrayList<>();
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);


        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while (!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            stringDates.add(df2.format(cal1.getTime()));
            cal1.add(Calendar.DATE, 1);
        }
        return stringDates;
    }

    public static String getMMMDDYYYY(String date) {
        String outDate = "";
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat dfOut = new SimpleDateFormat("dd MMM, yyyy");
        try {
            outDate = dfOut.format(df1.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outDate;
    }

    public static String getDDMMM(String date) {
        String outDate = "";
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat dfOut = new SimpleDateFormat("dd MMM");
        try {
            outDate = dfOut.format(df1.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outDate;
    }

    public static String getCurrentDateYYYYMMDD() {
        String currentDate = "";
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        currentDate = dateFormat.format(Calendar.getInstance().getTime());
        return currentDate;
    }

    public static String getCurrentDateYYYYMMDDHHMMSS() {
        String currentDate = "";
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        currentDate = dateFormat.format(Calendar.getInstance().getTime());
        return currentDate;
    }

    public static long getMilliseconds(String inputDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sdf.parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public static long getMillisecondsWithTime(String inputDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public static String localToGMT(String sDate) {
        Date date = getDateFromString(sDate);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(date);
    }

    public static String localToGMTYYYYMMDD(String sDate) {
        Date date = getDateFromString(sDate);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(date);
    }

    public static String localToGMTYYYYMMDDHHMMSS(String sDate) {
        Date date = getDateFromString(sDate);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(date);
    }

    public static String localToGMTHH_AM_PM(String sDate) {
        Date date = getDateFromStringHH_AM_PM(sDate);
        SimpleDateFormat sdf = new SimpleDateFormat("h a");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(date);
    }

    public static String localToGMT_DD_HH_AM_PM(String sDate) {
        try {
            Log.e("#DEBUG", "  localToGMT_DD_HH_AM_PM:  sDate: " + sDate);
            Date date = getDateFromString_DD_HH_AM_PM(sDate);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd h:mm a");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            return sdf.format(date);
        }
        catch (Exception e)
        {
            Log.d("err1234","===");
        }
       return null ;
    }

    public static String gmtToLocal_DD_HH_AM_PM(String sDate) {
        Date date = getDateFromString_DD_HH_AM_PM(sDate);
        String timeZone = Calendar.getInstance().getTimeZone().getID();
        Date local = new Date(date.getTime() + TimeZone.getTimeZone(timeZone).getOffset(date.getTime()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd h:mm a", Locale.getDefault());
        return sdf.format(local);
    }

    public static String gmtToLocalDate(String sDate) {
        Date date = getDateFromString(sDate);
        String timeZone = Calendar.getInstance().getTimeZone().getID();
        Date local = new Date(date.getTime() + TimeZone.getTimeZone(timeZone).getOffset(date.getTime()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return sdf.format(local);
    }

    public static String gmtToLocalDateAMPM(String sDate) {
        Date date = getDateFromStringAMPM(sDate);
        String timeZone = Calendar.getInstance().getTimeZone().getID();
        Date local = new Date(date.getTime() + TimeZone.getTimeZone(timeZone).getOffset(date.getTime()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd h:mm a", Locale.getDefault());
        return sdf.format(local);
    }

    public static String gmtToLocalDateYYYYMMDD(String sDate) {
        Date date = getDateFromStringYMD(sDate);
        String timeZone = Calendar.getInstance().getTimeZone().getID();
        Date local = new Date(date.getTime() + TimeZone.getTimeZone(timeZone).getOffset(date.getTime()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return sdf.format(local);
    }

    public static String getHH_AM_PM(String date) {
        String outDate = "";
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat dfOut = new SimpleDateFormat("h:mm a");
        try {
            outDate = dfOut.format(df1.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outDate;
    }

    public static String getHH(String date) {
        String outDate = "";
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat dfOut = new SimpleDateFormat("hh");
        try {
            outDate = dfOut.format(df1.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outDate;
    }

    public static String getMM(String date) {
        String outDate = "";
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat dfOut = new SimpleDateFormat("mm");
        try {
            outDate = dfOut.format(df1.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outDate;
    }

    public static String getAMPM(String date) {
        String outDate = "";
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat dfOut = new SimpleDateFormat("a");
        try {
            outDate = dfOut.format(df1.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outDate;
    }
}
