package com.softmeasures.eventezlyadminplus.Custom_contorl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

@SuppressLint("AppCompatCustomView")
public class LatoRegularTextView extends TextView {

    public LatoRegularTextView(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public LatoRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public LatoRegularTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = Typeface.createFromAsset(context.getAssets(), "fonts/arialblack.ttf");
        setTypeface(customFont);
    }
}