package com.softmeasures.eventezlyadminplus.Custom_contorl;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class Menu_Thin_TextView extends TextView {

    public Menu_Thin_TextView(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public Menu_Thin_TextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public Menu_Thin_TextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeue-Thin.ttf");
        setTypeface(customFont);
    }
}