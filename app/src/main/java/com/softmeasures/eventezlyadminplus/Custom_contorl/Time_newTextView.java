package com.softmeasures.eventezlyadminplus.Custom_contorl;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class Time_newTextView extends TextView {

    public Time_newTextView(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public Time_newTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public Time_newTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = Typeface.createFromAsset(context.getAssets(), "fonts/times-new-roman-italic.ttf");
        setTypeface(customFont);
    }
}