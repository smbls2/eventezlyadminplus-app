package com.softmeasures.eventezlyadminplus.Custom_contorl;

import android.content.Context;
import android.graphics.Typeface;
import android.text.InputType;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by work7 on 19/02/17.
 */

public class FontEditText extends EditText {

    public FontEditText(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public FontEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public FontEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = Typeface.createFromAsset(context.getAssets(), "fonts/arialblack.ttf");
        setTypeface(customFont);
        setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
    }


}