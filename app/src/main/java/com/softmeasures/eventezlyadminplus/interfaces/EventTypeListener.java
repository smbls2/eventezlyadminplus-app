package com.softmeasures.eventezlyadminplus.interfaces;

public interface EventTypeListener {
    void onNext(int type, boolean isEdit);
}
