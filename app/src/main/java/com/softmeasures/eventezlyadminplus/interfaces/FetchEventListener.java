package com.softmeasures.eventezlyadminplus.interfaces;

import com.softmeasures.eventezlyadminplus.models.EventDefinition;

import java.util.ArrayList;

public interface FetchEventListener {
    void onSuccess(ArrayList<EventDefinition> objects);

    void onFailed();
}
