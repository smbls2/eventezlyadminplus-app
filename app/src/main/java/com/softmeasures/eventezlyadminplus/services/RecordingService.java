package com.softmeasures.eventezlyadminplus.services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

import com.softmeasures.eventezlyadminplus.java.item;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Daniel on 12/28/2014.
 */
public class RecordingService extends Service {

    private static final String LOG_TAG = "RecordingService";

    private String mFileName = null;
    private String mFilePath = null;

    private MediaRecorder mRecorder = null;


    private long mStartingTimeMillis = 0;
    private long mElapsedMillis = 0;


    private Timer mTimer = null;
    private TimerTask mIncrementTimerTask = null;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startRecording();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (mRecorder != null) {
            stopRecording();

        }

        super.onDestroy();
    }

    public void startRecording() {
        try {
            setFileNameAndPath();

            mRecorder = new MediaRecorder();
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mRecorder.setOutputFile(mFilePath);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mRecorder.setAudioChannels(1);

            try {
                mRecorder.prepare();
                mRecorder.start();
                mStartingTimeMillis = System.currentTimeMillis();
            } catch (IOException e) {
                Log.e(LOG_TAG, "prepare() failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setFileNameAndPath() {
        int count = 0;
        File f;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        String timeStamp = dateFormat.format(new Date());
        ArrayList<item> file_body = new ArrayList<>();
        item ii = new item();
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ParkEZly";
        File dir = new File(path);
        if (!dir.exists())
            dir.mkdirs();
        do {
            count++;
            SharedPreferences logindeatl = getSharedPreferences("login", MODE_PRIVATE);
            // http://108.30.248.212:8006/api/v2/pzly01live7/_table/user_locations?filter=user_id%3D5%20AND%20location_code%3DNY-NHP-03
            String rid = logindeatl.getString("id", "null");
            mFileName = rid + timeStamp + ".mp4";
            mFilePath = Environment.getExternalStorageDirectory().getAbsolutePath();
            mFilePath += "/Parkezly/" + mFileName;
            SharedPreferences getdatatt = getSharedPreferences("file_url", MODE_PRIVATE);
            SharedPreferences.Editor ed = getdatatt.edit();
            ed.putString("file_path", mFilePath);
            ed.commit();
//            f = new File(mFilePath);
            Log.e("#DEBUG", "Path: " + dir.getAbsolutePath() + " " + mFileName);
            f = new File(dir.getAbsolutePath(), mFileName);
            ii.setFile_body(f);
        } while (f.exists() && !f.isDirectory());
    }
    public void stopRecording() {
        try {
            mRecorder.stop();
            mElapsedMillis = (System.currentTimeMillis() - mStartingTimeMillis);
            mRecorder.release();
            if (mIncrementTimerTask != null) {
                mIncrementTimerTask.cancel();
                mIncrementTimerTask = null;
            }
            mRecorder = null;
            try {
            } catch (Exception e) {
                Log.e(LOG_TAG, "exception", e);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface OnTimerChangedListener {
        void onTimerChanged(int seconds);
    }
}