package com.softmeasures.eventezlyadminplus.services;

/**
 * Created by obaro on 7/13/15.
 */
public final class Constants {
    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;

    public static final int USE_ADDRESS_NAME = 1;
    public static final int USE_ADDRESS_LOCATION = 2;

    public static final String PACKAGE_NAME =
            "com.sample.foo.simplelocationapp";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";
    public static final String RESULT_ADDRESS = PACKAGE_NAME + ".RESULT_ADDRESS";
    public static final String LOCATION_LATITUDE_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_LATITUDE_DATA_EXTRA";
    public static final String LOCATION_LONGITUDE_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_LONGITUDE_DATA_EXTRA";
    public static final String LOCATION_NAME_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_NAME_DATA_EXTRA";
    public static final String FETCH_TYPE_EXTRA = PACKAGE_NAME + ".FETCH_TYPE_EXTRA";

    public static String mForgot = "Forgot";

    public static final int EVENT_TYPE_AT_LOCATION = 1;
    public static final int EVENT_TYPE_AT_VIRTUALLY = 2;
    public static final int EVENT_TYPE_BOTH = 3;
    public static final String LINK_TYPE_SCHEDULED = "Scheduled Link";
    public static final String LINK_TYPE_EXISTING = "Existing Link";


    public static final int GROUP_TYPE_ORGANIZATIONAL = 1;
    public static final int GROUP_TYPE_PUBLIC = 2;
    public static final int GROUP_TYPE_MYCONTACT = 3;

    public static final int MENU_BG_MAP = 0;
    public static final int MENU_BG_CUSTOM = 1;
    public static final int MENU_BG_COLOR = 2;

    public static final int COMPANY_USER = 1;
    public static final int TWP_USER = 2;
    public static final int GENERAL_COMPANY_USER = 3;
    public static final int DIVISION_USER = 4;
    public static final int DEPT_USER = 5;
    public static final int ALL_USER = 9;


}