package com.softmeasures.eventezlyadminplus.meeting;

import android.content.Context;
import android.util.Log;

import us.zoom.sdk.ZoomSDK;
import us.zoom.sdk.ZoomSDKInitParams;
import us.zoom.sdk.ZoomSDKInitializeListener;
import us.zoom.sdk.ZoomSDKRawDataMemoryMode;

public class InitAuthSDKHelper implements AuthConstants, ZoomSDKInitializeListener {
    private static final String TAG = "InitAuthSDKHelper";
    private static InitAuthSDKHelper mInitAuthSDKHelper;
    private InitAuthSDKCallback mInitAuthSDKCallback;
    private ZoomSDK mZoomSDK = ZoomSDK.getInstance();

    private InitAuthSDKHelper() {
    }

    public static synchronized InitAuthSDKHelper getInstance() {
        InitAuthSDKHelper initAuthSDKHelper;
        synchronized (InitAuthSDKHelper.class) {
            initAuthSDKHelper = new InitAuthSDKHelper();
            mInitAuthSDKHelper = initAuthSDKHelper;
        }
        return initAuthSDKHelper;
    }

    public void initSDK(Context context, InitAuthSDKCallback callback) {
        if (!this.mZoomSDK.isInitialized()) {
            this.mInitAuthSDKCallback = callback;
            ZoomSDKInitParams initParams = new ZoomSDKInitParams();
            initParams.appKey = AuthConstants.APP_KEY;
            initParams.appSecret = AuthConstants.APP_SECRET;
            initParams.enableLog = true;
            initParams.enableGenerateDump = true;
            initParams.logSize = 50;
            initParams.domain = AuthConstants.WEB_DOMAIN;
            initParams.videoRawDataMemoryMode = ZoomSDKRawDataMemoryMode.ZoomSDKRawDataMemoryModeStack;
            this.mZoomSDK.initialize(context, this, initParams);
        }
    }

    public void onZoomSDKInitializeResult(int errorCode, int internalErrorCode) {
        StringBuilder sb = new StringBuilder();
        sb.append("onZoomSDKInitializeResult, errorCode=");
        sb.append(errorCode);
        sb.append(", internalErrorCode=");
        sb.append(internalErrorCode);
        Log.i(TAG, sb.toString());
        InitAuthSDKCallback initAuthSDKCallback = this.mInitAuthSDKCallback;
        if (initAuthSDKCallback != null) {
            initAuthSDKCallback.onZoomSDKInitializeResult(errorCode, internalErrorCode);
        }
    }

    public void onZoomAuthIdentityExpired() {
        Log.e(TAG, "onZoomAuthIdentityExpired in init");
    }

    public void reset() {
        this.mInitAuthSDKCallback = null;
    }
}
