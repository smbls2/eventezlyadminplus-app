package com.softmeasures.eventezlyadminplus.meeting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.softmeasures.eventezlyadminplus.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import us.zoom.sdk.AccountService;
import us.zoom.sdk.Alternativehost;
import us.zoom.sdk.MeetingItem;
import us.zoom.sdk.PreMeetingService;
import us.zoom.sdk.PreMeetingServiceListener;
import us.zoom.sdk.ZoomSDK;

public class PreMeetingExampleActivity extends AppCompatActivity implements OnClickListener, PreMeetingServiceListener {
    private static final String TAG = "ZoomSDKExample";
    private MeetingsListAdapter mAdapter;
    private Button mBtnSchedule;
    private ListView mListView;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.pre_meeting_activity);
        this.mListView = (ListView) findViewById(R.id.meetingsListView);
        Button button = (Button) findViewById(R.id.btnSchedule);
        this.mBtnSchedule = button;
        button.setOnClickListener(this);
        this.mAdapter = new MeetingsListAdapter(this);
        ZoomSDK zoomSDK = ZoomSDK.getInstance();
        if (zoomSDK.isInitialized()) {
            PreMeetingService preMeetingService = zoomSDK.getPreMeetingService();
            if (preMeetingService != null) {
                preMeetingService.listMeeting();
                preMeetingService.addListener(this);
            } else {
                Toast.makeText(this, "User not login.", Toast.LENGTH_LONG).show();
                finish();
            }
        }
        this.mListView.setAdapter(this.mAdapter);
    }


    public void onResume() {
        super.onResume();
    }

    public void onListMeeting(int result, List<Long> meetingList) {
        StringBuilder sb = new StringBuilder();
        sb.append("onListMeeting, result =");
        sb.append(result);
        Log.i(TAG, sb.toString());
        this.mAdapter.clear();
        PreMeetingService preMeetingService = ZoomSDK.getInstance().getPreMeetingService();
        if (!(preMeetingService == null || meetingList == null)) {
            for (Long longValue : meetingList) {
                MeetingItem item = preMeetingService.getMeetingItemByUniqueId(longValue.longValue());
                if (item != null) {
                    this.mAdapter.addItem(item);
                }
            }
        }
        this.mAdapter.notifyDataSetChanged();
    }

    public void onScheduleMeeting(int result, long meetingUniqueId) {
        StringBuilder sb = new StringBuilder();
        sb.append("onScheduleMeeting result:");
        sb.append(result);
        sb.append(" meetingUniqueId:");
        sb.append(meetingUniqueId);
        Log.d(TAG, sb.toString());
    }

    public void onUpdateMeeting(int result, long meetingUniqueId) {
        StringBuilder sb = new StringBuilder();
        sb.append("onUpdateMeeting result:");
        sb.append(result);
        sb.append(" meetingUniqueId:");
        sb.append(meetingUniqueId);
        Log.d(TAG, sb.toString());
    }

    public void onDeleteMeeting(int result) {
        StringBuilder sb = new StringBuilder();
        sb.append("onDeleteMeeting result:");
        sb.append(result);
        Log.d(TAG, sb.toString());
    }


    public void onClickBtnDelete(MeetingItem item) {
        ZoomSDK zoomSDK = ZoomSDK.getInstance();
        if (zoomSDK.isInitialized()) {
            PreMeetingService preMeetingService = zoomSDK.getPreMeetingService();
            if (preMeetingService != null) {
                preMeetingService.deleteMeeting(item.getMeetingUniqueId());
            }
        }
    }


    public void onDestroy() {
        ZoomSDK zoomSDK = ZoomSDK.getInstance();
        if (zoomSDK.isInitialized()) {
            PreMeetingService preMeetingService = zoomSDK.getPreMeetingService();
            if (preMeetingService != null) {
                preMeetingService.removeListener(this);
            }
        }
        super.onDestroy();
    }

    public void onClick(View arg0) {
        if (arg0.getId() == R.id.btnSchedule) {
            onClickSchedule();
        }
    }

    private void onClickSchedule() {
        startActivity(new Intent(this, ScheduleMeetingExampleActivity.class));
    }


    public String getHostNameByEmail(String email) {
        AccountService accountService = ZoomSDK.getInstance().getAccountService();
        String str = " ";
        if (accountService != null) {
            if (email.equals(accountService.getAccountEmail())) {
                return accountService.getAccountName();
            }
            List<Alternativehost> hostList = accountService.getCanScheduleForUsersList();
            if (hostList.size() < 1) {
                return str;
            }
            for (Alternativehost host : hostList) {
                if (email.equals(host.getEmail())) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(host.getFirstName());
                    sb.append(str);
                    sb.append(host.getLastName());
                    return sb.toString();
                }
            }
        }
        return str;
    }

    class MeetingsListAdapter extends BaseAdapter {

        private ArrayList<MeetingItem> mItems = new ArrayList<MeetingItem>();
        private Context mContext;

        public MeetingsListAdapter(Context context) {
            mContext = context;
        }

        public void clear() {
            mItems.clear();
        }

        public void addItem(MeetingItem item) {
            assert (item != null);
            mItems.add(item);
        }

        @Override
        public int getCount() {
            return mItems.size();
        }

        @Override
        public Object getItem(int position) {
            if (position < 0 || position >= getCount())
                return null;

            return mItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            MeetingItem item = (MeetingItem) getItem(position);
            return item != null ? item.getMeetingUniqueId() : 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final MeetingItem item = (MeetingItem) getItem(position);
            ViewHolder holder = null;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(mContext).inflate(R.layout.meeting_item, null);

                holder.txtTopic = (TextView) convertView.findViewById(R.id.txtTopic);
                holder.txtTime = (TextView) convertView.findViewById(R.id.txtTime);
                holder.txtHostName = (TextView) convertView.findViewById(R.id.txtHostName);
                holder.txtMeetingNo = (TextView) convertView.findViewById(R.id.txtMeetingNo);
                holder.btnDelete = (Button) convertView.findViewById(R.id.btnDelete);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.txtMeetingNo.setText("Meeting number: " + item.getMeetingNumber());
            if (item.isPersonalMeeting()) {
                holder.btnDelete.setVisibility(View.GONE);
                holder.txtTopic.setText("Personal meeting id(PMI)");
                holder.txtHostName.setVisibility(View.GONE);
                holder.txtTime.setVisibility(View.GONE);
            } else {
                holder.txtTopic.setText("Topic: " + item.getMeetingTopic());
                holder.txtHostName.setText(getHostNameByEmail(item.getScheduleForHostEmail()));
                Date date = new Date(item.getStartTime());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                holder.txtTime.setText("Time: " + sdf.format(date));
                if (item.isWebinarMeeting()) {
                    holder.btnDelete.setVisibility(View.GONE);
                } else {
                    holder.btnDelete.setVisibility(View.VISIBLE);
                    holder.btnDelete.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            onClickBtnDelete(item);
                        }
                    });
                }
            }
            return convertView;
        }

        class ViewHolder {
            public TextView txtTopic;
            public TextView txtHostName;
            public TextView txtTime;
            public TextView txtMeetingNo;
            public Button btnDelete;
        }
    }
}
