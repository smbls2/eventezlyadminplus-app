package com.softmeasures.eventezlyadminplus.meeting;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.softmeasures.eventezlyadminplus.R;
import com.softmeasures.eventezlyadminplus.common.BaseActivity;
import com.softmeasures.eventezlyadminplus.databinding.ActivityZoomLoginBinding;
import com.softmeasures.eventezlyadminplus.meeting.UserLoginCallback.ZoomDemoAuthenticationListener;
import com.softmeasures.eventezlyadminplus.meeting.create.LoginUserStartJoinMeetingActivity;
import com.softmeasures.eventezlyadminplus.models.MediaDefinition;

import us.zoom.sdk.InMeetingNotificationHandle;
import us.zoom.sdk.MeetingServiceListener;
import us.zoom.sdk.MeetingStatus;
import us.zoom.sdk.ZoomSDK;

public class ZoomLoginActivity extends BaseActivity implements InitAuthSDKCallback, MeetingServiceListener, ZoomDemoAuthenticationListener {
    private ActivityZoomLoginBinding binding;
    InMeetingNotificationHandle handle = new InMeetingNotificationHandle() {
        public boolean handleReturnToConfNotify(Context context, Intent intent) {
            return true;
        }
    };
    private ZoomSDK mZoomSDK;
    private ProgressDialog progressDialog;
    private boolean isManage = false;
    private boolean isCreateMeeting = false;
    private boolean isJoinMeeting = false;
    private MediaDefinition mediaDefinition;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.binding = (ActivityZoomLoginBinding) DataBindingUtil.setContentView(this, R.layout.activity_zoom_login);
        isManage = getIntent().getBooleanExtra("isManage", false);
        isCreateMeeting = getIntent().getBooleanExtra("isCreateMeeting", false);
        isJoinMeeting = getIntent().getBooleanExtra("isJoinMeeting", false);
        if (isJoinMeeting || isCreateMeeting) {
            mediaDefinition = new Gson().fromJson(getIntent().getStringExtra("mediaDefinition"), MediaDefinition.class);
        }
        initViews();
        updateViews();
        setListeners();
        this.progressDialog.show();
        ZoomSDK instance = ZoomSDK.getInstance();
        this.mZoomSDK = instance;
        if (instance.isLoggedIn()) {
            this.binding.llLogin.setVisibility(View.GONE);
            UserLoginCallback.getInstance().removeListener(this);
            this.progressDialog.dismiss();
            doProcess();
        } else {
            this.binding.llLogin.setVisibility(View.VISIBLE);
        }
        InitAuthSDKHelper.getInstance().initSDK(this, this);
    }

    private void doProcess() {
        if (isManage) {
            startActivity(new Intent(this, LoginUserStartJoinMeetingActivity.class)
                    .putExtra("isManage", isManage)
                    .putExtra("mediaDefinition", new Gson().toJson(mediaDefinition)));
            finish();
        } else if (isCreateMeeting) {
            startActivity(new Intent(this, LoginUserStartJoinMeetingActivity.class)
                    .putExtra("mediaDefinition", new Gson().toJson(mediaDefinition)));
            finish();
        } else if (isJoinMeeting) {
            startActivity(new Intent(this, LoginUserStartJoinMeetingActivity.class)
                    .putExtra("mediaDefinition", new Gson().toJson(mediaDefinition))
                    .putExtra("isJoinMeeting", isJoinMeeting));
            finish();
        }
    }

    public void onResume() {
        super.onResume();
        UserLoginCallback.getInstance().addListener(this);
    }

    public void onPause() {
        super.onPause();
        UserLoginCallback.getInstance().removeListener(this);
    }

    private void onClickBtnLogin() {
        String userName = this.binding.etEmail.getText().toString().trim();
        String password = this.binding.etPassword.getText().toString().trim();
        if (userName.length() == 0 || password.length() == 0) {
            Toast.makeText(this, "You need to enter user name and password.", Toast.LENGTH_LONG).show();
            return;
        }
        this.progressDialog.show();
        int ret = EmailUserLoginHelper.getInstance().login(userName, password);
        StringBuilder sb = new StringBuilder();
        sb.append("   onClickBtnLogin:  ret:  ");
        sb.append(ret);
        Log.e("#DEBUG", sb.toString());
        if (ret != 0) {
            this.progressDialog.dismiss();
            if (ret == 9) {
                Toast.makeText(this, "Account had disable email login ", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "ZoomSDK has not been initialized successfully or sdk is logging in.", Toast.LENGTH_LONG).show();
            }
        } else {
            this.binding.llLogin.setVisibility(View.GONE);
            this.progressDialog.dismiss();
        }
    }

    public void updateViews() {
    }

    public void lambda$setListeners$0$ZoomLoginActivity(View v) {
        onClickBtnLogin();
    }

    public void setListeners() {
        this.binding.btnAdd.setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                ZoomLoginActivity.this.lambda$setListeners$0$ZoomLoginActivity(view);
            }
        });
    }

    public void initViews() {
        ProgressDialog progressDialog2 = new ProgressDialog(this);
        this.progressDialog = progressDialog2;
        progressDialog2.setMessage("Loading...");
        this.progressDialog.setCancelable(false);
    }

    public void onZoomSDKLoginResult(long result) {
        this.progressDialog.dismiss();
        if (result == 0) {
            UserLoginCallback.getInstance().removeListener(this);
            Toast.makeText(this, "Login successfully", Toast.LENGTH_SHORT).show();
            doProcess();
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Login failed result code = ");
        sb.append(result);
        Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show();
        this.binding.llLogin.setVisibility(View.VISIBLE);
    }

    public void onZoomSDKLogoutResult(long result) {
        if (result == 0) {
            Toast.makeText(this, "Logout successfully", Toast.LENGTH_SHORT).show();
            onBackPressed();
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Logout failed result code = ");
        sb.append(result);
        Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show();
    }

    public void onZoomIdentityExpired() {
    }

    public void onMeetingStatusChanged(MeetingStatus meetingStatus, int i, int i1) {
    }

    public void onZoomSDKInitializeResult(int errorCode, int internalErrorCode) {
        StringBuilder sb = new StringBuilder();
        sb.append("onZoomSDKInitializeResult, errorCode=");
        sb.append(errorCode);
        String str = ", internalErrorCode=";
        sb.append(str);
        sb.append(internalErrorCode);
        Log.e("#DEBUG", sb.toString());
        this.progressDialog.dismiss();
        if (errorCode != 0) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to initialize Zoom SDK. Error: ");
            sb2.append(errorCode);
            sb2.append(str);
            sb2.append(internalErrorCode);
            Toast.makeText(this, sb2.toString(), Toast.LENGTH_LONG).show();
            onBackPressed();
            return;
        }
        ZoomSDK.getInstance().getMeetingSettingsHelper().enable720p(false);
        ZoomSDK.getInstance().getMeetingSettingsHelper().enableShowMyMeetingElapseTime(true);
        ZoomSDK.getInstance().getMeetingService().addListener(this);
        ZoomSDK.getInstance().getMeetingSettingsHelper().setCustomizedNotificationData(null, this.handle);
//        Toast.makeText(this, "Initialize Zoom SDK successfully.", Toast.LENGTH_LONG).show();
        if (this.mZoomSDK.tryAutoLoginZoom() == 0) {
            UserLoginCallback.getInstance().addListener(this);
            showProgressPanel(true);
        } else {
            showProgressPanel(false);
        }
        this.binding.llLogin.setVisibility(View.VISIBLE);
    }

    private void showProgressPanel(boolean show) {
    }

    public void onZoomAuthIdentityExpired() {
    }
}
