package com.softmeasures.eventezlyadminplus.meeting;

import us.zoom.sdk.ZoomSDK;

public class EmailUserLoginHelper {
    private static final String TAG = "EmailUserLogin";
    private static EmailUserLoginHelper mEmailUserLoginHelper;
    private ZoomSDK mZoomSDK = ZoomSDK.getInstance();

    private EmailUserLoginHelper() {
    }

    public static synchronized EmailUserLoginHelper getInstance() {
        EmailUserLoginHelper emailUserLoginHelper;
        synchronized (EmailUserLoginHelper.class) {
            emailUserLoginHelper = new EmailUserLoginHelper();
            mEmailUserLoginHelper = emailUserLoginHelper;
        }
        return emailUserLoginHelper;
    }

    public int login(String userName, String password) {
        return this.mZoomSDK.loginWithZoom(userName, password);
    }

    public boolean logout() {
        return this.mZoomSDK.logoutZoom();
    }

    public boolean isLoggedIn() {
        return this.mZoomSDK.isLoggedIn();
    }

    public int tryAutoLoginZoom() {
        return this.mZoomSDK.tryAutoLoginZoom();
    }
}
