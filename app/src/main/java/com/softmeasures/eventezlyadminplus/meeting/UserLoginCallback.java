package com.softmeasures.eventezlyadminplus.meeting;

import java.util.ArrayList;
import java.util.Iterator;

import us.zoom.sdk.ZoomSDK;
import us.zoom.sdk.ZoomSDKAuthenticationListener;

public class UserLoginCallback implements ZoomSDKAuthenticationListener {
    private static final String TAG = "UserLoginCallback";
    private static UserLoginCallback mUserLoginCallback;
    private ArrayList<ZoomDemoAuthenticationListener> mListenerList = new ArrayList<>();

    public interface ZoomDemoAuthenticationListener {
        void onZoomAuthIdentityExpired();

        void onZoomIdentityExpired();

        void onZoomSDKLoginResult(long j);

        void onZoomSDKLogoutResult(long j);
    }

    private UserLoginCallback() {
        ZoomSDK.getInstance().addAuthenticationListener(this);
    }

    public static synchronized UserLoginCallback getInstance() {
        UserLoginCallback userLoginCallback;
        synchronized (UserLoginCallback.class) {
            if (mUserLoginCallback == null) {
                mUserLoginCallback = new UserLoginCallback();
            }
            userLoginCallback = mUserLoginCallback;
        }
        return userLoginCallback;
    }

    public void addListener(ZoomDemoAuthenticationListener listener) {
        if (!this.mListenerList.contains(listener)) {
            this.mListenerList.add(listener);
        }
    }

    public void removeListener(ZoomDemoAuthenticationListener listener) {
        this.mListenerList.remove(listener);
    }

    public void onZoomSDKLoginResult(long result) {
        Iterator it = this.mListenerList.iterator();
        while (it.hasNext()) {
            ZoomDemoAuthenticationListener listener = (ZoomDemoAuthenticationListener) it.next();
            if (listener != null) {
                listener.onZoomSDKLoginResult(result);
            }
        }
    }

    public void onZoomSDKLogoutResult(long result) {
        Iterator it = this.mListenerList.iterator();
        while (it.hasNext()) {
            ZoomDemoAuthenticationListener listener = (ZoomDemoAuthenticationListener) it.next();
            if (listener != null) {
                listener.onZoomSDKLogoutResult(result);
            }
        }
    }

    public void onZoomIdentityExpired() {
        Iterator it = this.mListenerList.iterator();
        while (it.hasNext()) {
            ZoomDemoAuthenticationListener listener = (ZoomDemoAuthenticationListener) it.next();
            if (listener != null) {
                listener.onZoomIdentityExpired();
            }
        }
    }

    public void onZoomAuthIdentityExpired() {
        Iterator it = this.mListenerList.iterator();
        while (it.hasNext()) {
            ZoomDemoAuthenticationListener listener = (ZoomDemoAuthenticationListener) it.next();
            if (listener != null) {
                listener.onZoomAuthIdentityExpired();
            }
        }
    }
}
